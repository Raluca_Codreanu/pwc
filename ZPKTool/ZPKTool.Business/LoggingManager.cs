﻿using System;
using System.Threading.Tasks;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Contains methods used for logging the events.
    /// </summary>
    public static class LoggingManager
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Logs the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        public static void Log(Data.LogEventType eventType)
        {
            LogInternal(eventType, string.Empty);
        }

        /// <summary>
        /// Logs the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="details">The details.</param>
        public static void Log(Data.LogEventType eventType, string details)
        {
            LogInternal(eventType, details);
        }

        /// <summary>
        /// Logs the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="error">The error.</param>
        public static void Log(Data.LogEventType eventType, Exception error)
        {
            LogInternal(eventType, error.ToString());
        }

        /// <summary>
        /// Logs the internal.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="details">The details.</param>
        private static void LogInternal(LogEventType eventType, string details)
        {
            Task.Factory.StartNew(() =>
            {
                var dbLocation = DatabaseHelper.IsCentralDatabaseOnline() ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase;
                var dm = DataAccessFactory.CreateDataSourceManager(dbLocation);

                User dbUser = null;
                if (SecurityManager.Instance.CurrentUser != null)
                {
                    dbUser = dm.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, true);
                }

                var logEntry = new Log
                {
                    Type = eventType,
                    Details = details,
                    User = dbUser,
                    ComputerName = Environment.MachineName
                };

                dm.LogRepository.Add(logEntry);
                dm.SaveChanges();
            }).ContinueWith((task) =>
             {
                 if (task.Exception != null)
                 {
                     var error = task.Exception.InnerException;
                     log.ErrorException("Logging service error.", error);
                 }
             });
        }
    }
}