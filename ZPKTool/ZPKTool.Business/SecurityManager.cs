﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// The SecurityManager manages users, roles and license rights.
    /// </summary>
    public sealed class SecurityManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly SecurityManager instance = new SecurityManager();

        /// <summary>
        /// The path to the license file.
        /// </summary>
        public static readonly string LicenseFilePath;

        /// <summary>
        /// The public key used for decrypting the license file content.
        /// </summary>
        public static readonly string LicensePublicKey;

        /// <summary>
        /// The data manager used to get the current user.
        /// </summary>
        private IDataSourceManager currentUserDataManager;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="SecurityManager"/> class.
        /// </summary>
        static SecurityManager()
        {
            try
            {
                LicenseFilePath = Path.Combine(Constants.ApplicationDataFolderPath, "License.lic");

                string embeddedResourceName = typeof(SecurityManager).Namespace.ToString() + "." + "PublicKey.xml";
                System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
                using (System.IO.Stream xmlStream = asm.GetManifestResourceStream(embeddedResourceName))
                {
                    StreamReader reader = new StreamReader(xmlStream);
                    LicensePublicKey = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Could not get the license public key.", ex);
            }
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="SecurityManager" /> class from being created.
        /// </summary>
        private SecurityManager()
        {
        }

        #endregion

        /// <summary>
        /// Occurs when the current user's UICurrency property has changed.
        /// </summary>
        public event EventHandler<UICurrencyChangedArgs> CurrentUserUICurrencyChanged;

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static SecurityManager Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets the logged in user.
        /// </summary>
        public User CurrentUser { get; private set; }

        /// <summary>
        /// Gets the role used by the current user for this session.
        /// </summary>        
        public Role CurrentUserRole { get; private set; }

        /// <summary>
        /// Gets the UI currency.
        /// </summary>
        public Currency UICurrency
        {
            get { return this.CurrentUser != null ? this.CurrentUser.UICurrency : null; }
        }

        #region Public Methods

        /// <summary>
        /// Checks the access rights and the license rights of the user having username and password.
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The password</param>
        /// <returns>
        /// The user with name, roles and other needed data
        /// </returns>
        public User Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("The username was null or empty.", "username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("The password was null or empty.", "password");
            }

            try
            {
                this.currentUserDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                var user = this.currentUserDataManager.UserRepository.GetByUsername(username, false);
                if (user != null)
                {
                    this.CurrentUser = null;
                    var securedPassword = string.Empty;

                    // If there is no salt the password was encrypted, otherwise it was hashed.
                    if (string.IsNullOrWhiteSpace(user.Salt))
                    {
                        securedPassword = EncryptionManager.Instance.EncodeMD5(password);
                    }
                    else
                    {
                        securedPassword = EncryptionManager.Instance.HashSHA256(password, user.Salt, user.Guid.ToString());
                    }

                    if (!user.Password.Equals(securedPassword))
                    {
                        LoggingManager.Log(LogEventType.LoginFailed, username);
                    }
                    else
                    {
                        LoggingManager.Log(LogEventType.LoginSuccessful, username);

                        this.CurrentUser = user;
                        if (!RoleRights.HasMultipleRoles(this.CurrentUser.Roles))
                        {
                            this.CurrentUserRole = RoleRights.GetFirstRole(this.CurrentUser.Roles);
                        }

                        // If the user has no UI Currency set the default one.
                        if (user.UICurrency == null)
                        {
                            this.CurrentUser.UICurrency = this.currentUserDataManager.CurrencyRepository.GetByIsoCode(Constants.DefaultCurrencyIsoCode);
                            this.currentUserDataManager.SaveChanges();
                        }

                        // Initialize the UI units according to the new current user's UI currency.
                        if (this.CurrentUserUICurrencyChanged != null)
                        {
                            this.CurrentUserUICurrencyChanged(this, new UICurrencyChangedArgs(null, this.UICurrency));
                        }
                    }
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException(string.Format("Could not authenticate user '{0}'", username), ex);
                if (ex.InnerException is System.Data.SqlClient.SqlException)
                {
                    throw new BusinessException(ErrorCodes.UserAuthenticationLocalDatabaseOffline, ex);
                }
                else
                {
                    throw new BusinessException(ex);
                }
            }

            return this.CurrentUser;
        }

        /// <summary>
        /// Logs out the current user.
        /// </summary>
        public void Logout()
        {
            this.CurrentUser = null;
        }

        /// <summary>
        /// Deletes the user from local database if doesn't exist in the central database.
        /// </summary>
        /// <param name="username">The username.</param>
        public void DeleteUser(string username)
        {
            this.currentUserDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralDatabase = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            var userCentralDatabase = centralDatabase.UserRepository.GetByUsername(username, false);
            var userLocalDatabase = this.currentUserDataManager.UserRepository.GetByUsername(username, false);

            if (userCentralDatabase == null && userLocalDatabase != null)
            {
                var allProjectFoldersIds = currentUserDataManager.ProjectFolderRepository.GetProjectFoldersId(userLocalDatabase.Guid);
                foreach (var projectFolder in allProjectFoldersIds)
                {
                    this.currentUserDataManager.ProjectFolderRepository.DeleteByStoredProcedure(new ProjectFolder() { Guid = projectFolder });
                }

                var allProjectsIds = currentUserDataManager.ProjectRepository.GetProjectsId(userLocalDatabase.Guid);
                foreach (var project in allProjectsIds)
                {
                    this.currentUserDataManager.ProjectRepository.DeleteByStoredProcedure(new Project() { Guid = project });
                }

                this.currentUserDataManager.UserRepository.RemoveAll(userLocalDatabase);
                this.currentUserDataManager.SaveChanges();
            }
        }

        /// <summary>
        /// Tries to set a new password for the user with a specified guid and to persist the data update.
        /// </summary>
        /// <param name="userGuid">The user's GUID.</param>
        /// <param name="oldPassword">The string entered as old password of the user with id userId</param>
        /// <param name="newPassword">The new password to be set for the user with id userId</param>        
        /// <exception cref="BusinessConcurrencyException">Thrown if a concurrency error occurred.</exception>
        /// <exception cref="BusinessException">Thrown if the old password is not correct or if
        /// an error occurs during the password update.</exception>
        public void ChangePassword(Guid userGuid, string oldPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(oldPassword))
            {
                throw new ArgumentException("The old password was null or empty.", "oldPassword");
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                throw new ArgumentException("The new password was null or empty.", "newPassword");
            }

            try
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                var user = dataManager.UserRepository.GetById(userGuid, false);
                if (user != null)
                {
                    var oldPasswordSecured = string.Empty;
                    if (string.IsNullOrWhiteSpace(user.Salt))
                    {
                        oldPasswordSecured = EncryptionManager.Instance.EncodeMD5(oldPassword);
                    }
                    else
                    {
                        oldPasswordSecured = EncryptionManager.Instance.HashSHA256(oldPassword, user.Salt, user.Guid.ToString());
                    }

                    if (user.Password.Equals(oldPasswordSecured))
                    {
                        user.Salt = string.IsNullOrWhiteSpace(user.Salt) ? SaltGenerator.GenerateSalt() : user.Salt;
                        var newPasswordEncrypted = EncryptionManager.Instance.HashSHA256(newPassword, user.Salt, user.Guid.ToString());
                        user.Password = newPasswordEncrypted;
                        user.IsPasswordSetByUser = true;

                        if (oldPassword.Equals(newPassword, StringComparison.Ordinal))
                        {
                            throw new BusinessException(ErrorCodes.NewAndOldPasswordsAreIdentical);
                        }
                        else
                        {
                            this.CheckAndUpdatePasswordHistory(user, newPasswordEncrypted, dataManager);
                            dataManager.SaveChanges();
                        }
                    }
                    else
                    {
                        throw new BusinessException(ErrorCodes.WrongCurrentPassword);
                    }
                }
                else
                {
                    throw new BusinessException(ErrorCodes.UserDoesNotExist);
                }
            }
            catch (DataAccessException exception)
            {
                log.ErrorException(string.Format("Could not change password for user '{0}'", userGuid), exception);
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Adds the password in the PasswordsHistory table checking first if the password was used before.
        /// </summary>
        /// <param name="userID">The specified user id.</param>
        /// <param name="password">The password.</param>
        /// <param name="dataManager">The data manager.</param>
        public void CheckAndUpdatePasswordHistory(User userID, string password, IDataSourceManager dataManager)
        {
            var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.CentralDatabase);
            var globalSettings = globalSettingsManager.Get();

            if (globalSettings.NumberOfPreviousPasswordsToCheck == 0)
            {
                return;
            }

            var allPasswordsHistory = dataManager.PasswordsHistoryRepository.GetPasswordsHistory(userID.Guid);
            if (allPasswordsHistory.Any(s => s.Password == password))
            {
                var exception = new BusinessException(ErrorCodes.PasswordHasBeenUsedBefore);
                exception.ErrorMessageFormatArgs.Add(globalSettings.NumberOfPreviousPasswordsToCheck);
                throw exception;
            }

            if (allPasswordsHistory.Count() >= globalSettings.NumberOfPreviousPasswordsToCheck)
            {
                PasswordsHistory userOldestPassword = allPasswordsHistory.FirstOrDefault();

                userOldestPassword.Password = password;
                userOldestPassword.Date = DateTime.Now;
            }
            else
            {
                PasswordsHistory passwordsHistory = new PasswordsHistory();

                passwordsHistory.Password = password;
                passwordsHistory.User = userID;
                dataManager.PasswordsHistoryRepository.Add(passwordsHistory);
            }
        }

        /// <summary>
        /// Verifies if the password set by user is valid.
        /// </summary>
        /// <param name="password">The password set by user.</param>
        /// <exception cref="BusinessException">Throw if the password set by user is not valid.</exception>
        public void ValidatePassword(string password)
        {
            Regex regularExpression = new Regex(Constants.PasswordValidationExpression);
            if (!regularExpression.IsMatch(password) || !Common.Utils.CheckForDuplicateAndSequenceOfCharacters(password))
            {
                throw new BusinessException(ErrorCodes.InvalidPasssword);
            }
        }

        /// <summary>
        /// Refreshes the current user from the database. Should be called whenever the current user may be changed outside this instance (using other ChangeSets).
        /// </summary>
        public void RefreshCurrentUser()
        {
            if (this.CurrentUser == null)
            {
                return;
            }

            try
            {
                var oldUICurrency = this.UICurrency;

                this.currentUserDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                this.CurrentUser = this.currentUserDataManager.UserRepository.GetById(this.CurrentUser.Guid, true);

                if (CurrentUser == null)
                {
                    throw new PISecurityException(ErrorCodes.UserDeleted);
                }

                if (CurrentUser.Disabled)
                {
                    throw new PISecurityException(ErrorCodes.UserDisabled);
                }

                if (this.UICurrency == null)
                {
                    this.CurrentUser.UICurrency = this.currentUserDataManager.CurrencyRepository.GetByIsoCode(Constants.DefaultCurrencyIsoCode);
                    this.currentUserDataManager.SaveChanges();
                }

                // The UI currency might have changed (it is not easy to determine if it has) so signal it for dependent components to refresh.            
                if (this.CurrentUserUICurrencyChanged != null)
                {
                    this.CurrentUserUICurrencyChanged(this, new UICurrencyChangedArgs(oldUICurrency, this.UICurrency));
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Current user refresh failed.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Sets the UI currency of the current user.
        /// </summary>
        /// <param name="newUICurrency">The new UI currency.</param>
        /// <returns>True if the currency was changed; otherwise false.</returns>
        public bool ChangeCurrentUserUICurrency(Currency newUICurrency)
        {
            try
            {
                var oldUICurrency = this.CurrentUser.UICurrency;
                bool currencyChanged = false;

                if (((this.UICurrency == null || newUICurrency == null) && this.UICurrency != newUICurrency)
                    || (this.UICurrency != null && newUICurrency != null
                    && this.UICurrency.Guid != newUICurrency.Guid))
                {
                    // Get the corresponding new UI currency from the change set associated with the current user.
                    Currency currency = null;
                    if (newUICurrency != null)
                    {
                        currency = this.currentUserDataManager.CurrencyRepository.GetById(newUICurrency.Guid);
                    }

                    this.CurrentUser.UICurrency = currency;
                    this.currentUserDataManager.SaveChanges();
                    currencyChanged = true;

                    if (this.CurrentUserUICurrencyChanged != null
                        && currencyChanged)
                    {
                        this.CurrentUserUICurrencyChanged(this, new UICurrencyChangedArgs(oldUICurrency, newUICurrency));
                    }
                }

                return currencyChanged;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException(string.Format("Could not change currency for user '{0}'", this.CurrentUser.Guid), ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Changes the role used by the current user.
        /// </summary>
        /// <param name="newRole">The new role.</param>
        public void ChangeCurrentUserRole(Role newRole)
        {
            this.CurrentUserRole = newRole;
        }

        /// <summary>
        /// Checks if the current user has a specified role.
        /// </summary>
        /// <param name="role">The role for which to check.</param>
        /// <returns>True if the current user has the role, false otherwise.</returns>
        public bool CurrentUserHasRole(Role role)
        {
            return RoleRights.HasRole(this.CurrentUserRole, role);
        }

        /// <summary>
        /// Verifies if the current user has a given right
        /// </summary>
        /// <param name="right">The right to verify.</param>
        /// <returns><c>True</c> if the current user has the right; otherwise <c>False</c></returns>
        public bool CurrentUserHasRight(Right right)
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                return RoleRights.RoleHasRight(SecurityManager.Instance.CurrentUserRole, right);
            }

            return false;
        }

        #endregion Public Methods
    }
}
