﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Handles currency related operations.
    /// </summary>
    public static class CurrencyConversionManager
    {
        /// <summary>
        /// The default base currency, euro.
        /// </summary>
        private static Currency defaultBaseCurrency = new Currency() { Name = "Euro", IsoCode = "EUR", Symbol = "€", ExchangeRate = 1m };

        /// <summary>
        /// Gets the default base currency, euro.
        /// </summary>
        public static Currency DefaultBaseCurrency
        {
            get
            {
                return defaultBaseCurrency;
            }
        }

        /// <summary>
        /// Gets the entity's project currencies and base currency.
        /// If the project has no currencies set or if the entity does not have a parent project the base currencies and default base currency are returned.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="dataManager">The data source manager.</param>
        /// <param name="currencies">The currencies.</param>
        /// <param name="baseCurrency">The base currency.</param>
        public static void GetEntityCurrenciesAndBaseCurrency(object entity, IDataSourceManager dataManager, out ICollection<Currency> currencies, out Currency baseCurrency)
        {
            currencies = new Collection<Currency>();
            baseCurrency = null;

            if (dataManager == null)
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            }

            if (entity != null)
            {
                var parentProject = dataManager.ProjectRepository.GetParentProject(entity);
                if (parentProject != null)
                {
                    currencies = dataManager.CurrencyRepository.GetProjectCurrencies(parentProject.Guid);
                    baseCurrency = dataManager.CurrencyRepository.GetProjectBaseCurrency(parentProject.Guid);
                }
            }

            // If no currencies are set get the currencies from the database.
            if (currencies.Count == 0)
            {
                currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
            }

            // If no base currency was set get the default currency.
            if (baseCurrency == null)
            {
                baseCurrency = currencies.FirstOrDefault(c => c.IsoCode == Common.Constants.DefaultCurrencyIsoCode);
            }
        }

        /// <summary>
        /// Convert all values of a calculation result from the base value to the corresponding current UI unit value.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="conversionFactors">The conversion factors.</param>
        /// <returns>
        /// A new instance of CalculationResult containing the converted result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"> The input result was null or the unit conversion factors were null. </exception>
        public static CalculationResult ConvertFromBaseUnit(CalculationResult result, UnitConversionFactors conversionFactors)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result", "The input result was null.");
            }

            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The unit conversion factors were null.");
            }

            // Clone the input result.
            CalculationResult convertedResult = result.Clone();

            // Apply unit conversion to the cloned result.
            convertedResult.ApplyUnitConversion(conversionFactors);

            return convertedResult;
        }

        /// <summary>
        /// Converts the value to the base value using the base and the UI rates.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="uiRate">The ui rate used for conversion.</param>
        /// <param name="baseRate">The base rate used for conversion.</param>
        /// <returns>/// The converted value./// </returns>
        /// <exception cref="System.ArgumentNullException">uiRate;The UI rate can't be 0</exception>
        public static decimal ConvertToBaseValue(decimal value, decimal uiRate, decimal baseRate)
        {
            if (uiRate == 0)
            {
                throw new ArgumentNullException("uiRate", "The UI rate can't be 0");
            }

            return value / uiRate * baseRate;
        }

        /// <summary>
        /// Converts the base value to the displayed value.
        /// </summary>
        /// <param name="value">The base value.</param>
        /// <param name="baseRate">The base rate used for converting the base value to the displayed value.</param>
        /// <param name="uiRate">The ui rate used for converting the base value to the displayed value.</param>
        /// <returns>The converted value.</returns>
        public static decimal ConvertBaseValueToDisplayValue(decimal value, decimal baseRate, decimal uiRate)
        {
            if (baseRate == 0)
            {
                throw new ArgumentNullException("baseRate", "The base rate can't be 0");
            }

            return value / baseRate * uiRate;
        }

        /// <summary>
        /// Converts the value using the new and the old conversion rates.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="newConversionRate">The new conversion rate.</param>
        /// <param name="oldConversionRate">The old conversion rate.</param>
        /// <returns>The converted value.</returns>
        /// <exception cref="System.ArgumentNullException">oldConversionRate;The old conversion rate can't be 0</exception>
        public static decimal? ConvertBaseValue(decimal? value, decimal newConversionRate, decimal oldConversionRate)
        {
            if (!value.HasValue)
            {
                return null;
            }

            if (oldConversionRate == 0)
            {
                throw new ArgumentNullException("oldConversionRate", "The old conversion rate can't be 0");
            }

            return ConvertBaseValue(value.Value, newConversionRate, oldConversionRate);
        }

        /// <summary>
        /// Converts the value using the new and the old conversion rates.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="newConversionRate">The new conversion rate.</param>
        /// <param name="oldConversionRate">The old conversion rate.</param>
        /// <returns>The converted value.</returns>
        /// <exception cref="System.ArgumentNullException">oldConversionRate;The old conversion rate can't be 0</exception>
        public static decimal ConvertBaseValue(decimal value, decimal newConversionRate, decimal oldConversionRate)
        {
            if (oldConversionRate == 0)
            {
                throw new ArgumentNullException("oldConversionRate", "The old conversion rate can't be 0");
            }

            return value / oldConversionRate * newConversionRate;
        }

        /// <summary>
        /// Converts the objects hierarchy properties that represent money to the new base currency.
        /// Use when you don't have the object currencies and base currency.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="originalSource">The original source.</param>
        /// <param name="source">The source.</param>
        /// <param name="destinationDbId">The destination db id.</param>
        /// <param name="sourceDbId">The source db id.</param>
        public static void ConvertCurrency(object destination, object originalSource, object source, DbIdentifier destinationDbId, DbIdentifier sourceDbId)
        {
            var masterDataDestination = destination as IMasterDataObject;
            var masterDataOriginalSource = originalSource as IMasterDataObject;

            // If the destination or the source is a project or a project folder, or if the destination and the source are from master data there is nothing to convert.
            if (destination is ProjectFolder
                || source is Project
                || source is ProjectFolder
                || (masterDataDestination != null
                    && masterDataDestination.IsMasterData
                    && masterDataOriginalSource != null
                    && masterDataOriginalSource.IsMasterData))
            {
                return;
            }

            var dataManager = DataAccessFactory.CreateDataSourceManager(sourceDbId);
            Currency sourceProjectBaseCurrency = null;
            Project sourceProject = null;

            // If the source is a master data object or the project of the source has no base currency set then its base currency is the default currency;
            // Else get the project of the source and its base currency.
            if (masterDataOriginalSource != null
                && masterDataOriginalSource.IsMasterData)
            {
                sourceProjectBaseCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            }
            else
            {
                sourceProject = dataManager.ProjectRepository.GetParentProject(Utils.GetDBEntity(originalSource, dataManager), true);
                sourceProjectBaseCurrency = dataManager.CurrencyRepository.GetProjectBaseCurrency(sourceProject.Guid);
            }

            List<Currency> sourceProjectCurrencies = null;
            if (sourceProject != null)
            {
                sourceProjectCurrencies = dataManager.CurrencyRepository.GetProjectCurrencies(sourceProject.Guid).ToList();
            }

            ConvertCurrency(destination, source, destinationDbId, sourceProjectCurrencies, sourceProjectBaseCurrency);
        }

        /// <summary>
        /// Converts the objects hierarchy properties that represent money to the new base currency.
        /// Use when you have the object currencies and base currency.
        /// </summary>
        /// <param name="destination">The destination.</param>
        /// <param name="source">The source.</param>
        /// <param name="destinationDbId">The destination db id.</param>
        /// <param name="sourceCurrencies">The source currencies.</param>
        /// <param name="sourceBaseCurrency">The source base currency.</param>
        public static void ConvertCurrency(object destination, object source, DbIdentifier destinationDbId, IEnumerable<Currency> sourceCurrencies, Currency sourceBaseCurrency)
        {
            var masterDataDestination = destination as IMasterDataObject;

            // If the destination or the source is a project or a project folder, or if the destination and the source are from  master data there is nothing to convert.
            if (destination is ProjectFolder
                || source is Project
                || source is ProjectFolder)
            {
                return;
            }

            // If the source currencies do not exist use the base currencies.
            if (sourceCurrencies == null
                || sourceCurrencies.Count() == 0)
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                sourceCurrencies = dataManager.CurrencyRepository.GetBaseCurrencies();
            }

            // If the base currency does not exists the default currency will be used.
            if (sourceBaseCurrency == null)
            {
                sourceBaseCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            }

            // If the destination is a master data object or the project of the source has no base currency set then its base currency is the default currency;
            // Else get the project of the source and its base currency.
            Currency destinationProjectBaseCurrency = null;
            if (masterDataDestination != null
                && masterDataDestination.IsMasterData)
            {
                destinationProjectBaseCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            }
            else
            {
                var destinationDataManager = DataAccessFactory.CreateDataSourceManager(destinationDbId);
                var destinationProject = destination as Project;
                if (destinationProject == null)
                {
                    destinationProject = destinationDataManager.ProjectRepository.GetParentProject(Utils.GetDBEntity(destination, destinationDataManager), true);
                }

                destinationProjectBaseCurrency = destinationDataManager.CurrencyRepository.GetProjectBaseCurrency(destinationProject.Guid);
            }

            CurrencyConversionManager.ConvertObject(source, destinationProjectBaseCurrency, sourceBaseCurrency);
        }

        /// <summary>
        /// Converts the object properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="objectToConvert">The object to convert.</param>
        /// <param name="newCurrency">The new currency.</param>
        /// <param name="oldCurrency">The old currency.</param>
        /// <exception cref="System.ArgumentNullException">
        /// The object to convert can't be null
        /// or
        /// The new currency was null
        /// or
        /// The old currency was null
        /// </exception>
        public static void ConvertObject(object objectToConvert, Currency newCurrency, Currency oldCurrency)
        {
            if (objectToConvert == null)
            {
                throw new ArgumentNullException("objectToConvert", "The object to convert can't be null");
            }

            if (newCurrency == null)
            {
                throw new ArgumentNullException("newCurrency", "The new currency was null");
            }

            if (oldCurrency == null)
            {
                throw new ArgumentNullException("oldCurrency", "The old currency was null");
            }

            // If the currencies or their exchange rates are the same do nothing.
            if (newCurrency.IsSameAs(oldCurrency)
                || newCurrency.ExchangeRate == oldCurrency.ExchangeRate)
            {
                return;
            }

            var proj = objectToConvert as Project;
            if (proj != null)
            {
                ConvertProject(proj, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var assy = objectToConvert as Assembly;
            if (assy != null)
            {
                ConvertAssembly(assy, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var part = objectToConvert as Part;
            if (part != null)
            {
                ConvertPart(part, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var step = objectToConvert as ProcessStep;
            if (step != null)
            {
                ConvertProcessStep(step, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var commodity = objectToConvert as Commodity;
            if (commodity != null)
            {
                ConvertCommodity(commodity, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var consumable = objectToConvert as Consumable;
            if (consumable != null)
            {
                ConvertConsumable(consumable, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var die = objectToConvert as Die;
            if (die != null)
            {
                ConvertDie(die, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var machine = objectToConvert as Machine;
            if (machine != null)
            {
                ConvertMachine(machine, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var rawMaterial = objectToConvert as RawMaterial;
            if (rawMaterial != null)
            {
                ConvertRawMaterial(rawMaterial, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var countrySetting = objectToConvert as CountrySetting;
            if (countrySetting != null)
            {
                ConvertCountrySetting(countrySetting, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var transportCostCalculation = objectToConvert as TransportCostCalculation;
            if (transportCostCalculation != null)
            {
                ConvertTransportCostCalculation(transportCostCalculation, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }

            var transportCostCalculationSetting = objectToConvert as TransportCostCalculationSetting;
            if (transportCostCalculationSetting != null)
            {
                ConvertTransportCostCalculationSetting(transportCostCalculationSetting, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }
        }

        /// <summary>
        /// Converts the project properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertProject(Project project, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "The project can't be null");
            }

            foreach (var assy in project.Assemblies)
            {
                ConvertAssembly(assy, newExchangeRate, oldExchangeRate);
            }

            foreach (var part in project.Parts)
            {
                ConvertPart(part, newExchangeRate, oldExchangeRate);
            }
        }

        /// <summary>
        /// Converts the assembly properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="assy">The assembly.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertAssembly(Assembly assy, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (assy == null)
            {
                throw new ArgumentNullException("assy", "The assembly can't be null");
            }

            assy.EstimatedCost = ConvertBaseValue(assy.EstimatedCost, newExchangeRate, oldExchangeRate);
            assy.PurchasePrice = ConvertBaseValue(assy.PurchasePrice, newExchangeRate, oldExchangeRate);
            assy.TargetPrice = ConvertBaseValue(assy.TargetPrice, newExchangeRate, oldExchangeRate);
            assy.DevelopmentCost = ConvertBaseValue(assy.DevelopmentCost, newExchangeRate, oldExchangeRate);
            assy.ProjectInvest = ConvertBaseValue(assy.ProjectInvest, newExchangeRate, oldExchangeRate);
            assy.OtherCost = ConvertBaseValue(assy.OtherCost, newExchangeRate, oldExchangeRate);
            assy.PackagingCost = ConvertBaseValue(assy.PackagingCost, newExchangeRate, oldExchangeRate);
            assy.LogisticCost = ConvertBaseValue(assy.LogisticCost, newExchangeRate, oldExchangeRate);
            assy.TransportCost = ConvertBaseValue(assy.TransportCost, newExchangeRate, oldExchangeRate);

            foreach (var subAssy in assy.Subassemblies)
            {
                ConvertAssembly(subAssy, newExchangeRate, oldExchangeRate);
            }

            foreach (var part in assy.Parts)
            {
                ConvertPart(part, newExchangeRate, oldExchangeRate);
            }

            foreach (var calculation in assy.TransportCostCalculations)
            {
                ConvertTransportCostCalculation(calculation, newExchangeRate, oldExchangeRate);
            }

            if (assy.Process != null)
            {
                foreach (var step in assy.Process.Steps)
                {
                    ConvertProcessStep(step, newExchangeRate, oldExchangeRate);
                }
            }

            ConvertCountrySetting(assy.CountrySettings, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the part properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertPart(Part part, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part can't be null");
            }

            part.EstimatedCost = ConvertBaseValue(part.EstimatedCost, newExchangeRate, oldExchangeRate);
            part.PurchasePrice = ConvertBaseValue(part.PurchasePrice, newExchangeRate, oldExchangeRate);
            part.TargetPrice = ConvertBaseValue(part.TargetPrice, newExchangeRate, oldExchangeRate);
            part.DevelopmentCost = ConvertBaseValue(part.DevelopmentCost, newExchangeRate, oldExchangeRate);
            part.ProjectInvest = ConvertBaseValue(part.ProjectInvest, newExchangeRate, oldExchangeRate);
            part.OtherCost = ConvertBaseValue(part.OtherCost, newExchangeRate, oldExchangeRate);
            part.PackagingCost = ConvertBaseValue(part.PackagingCost, newExchangeRate, oldExchangeRate);
            part.LogisticCost = ConvertBaseValue(part.LogisticCost, newExchangeRate, oldExchangeRate);
            part.TransportCost = ConvertBaseValue(part.TransportCost, newExchangeRate, oldExchangeRate);

            if (part.Process != null)
            {
                foreach (var step in part.Process.Steps)
                {
                    ConvertProcessStep(step, newExchangeRate, oldExchangeRate);
                }
            }

            foreach (var commodity in part.Commodities)
            {
                ConvertCommodity(commodity, newExchangeRate, oldExchangeRate);
            }

            foreach (var material in part.RawMaterials)
            {
                ConvertRawMaterial(material, newExchangeRate, oldExchangeRate);
            }

            foreach (var calculation in part.TransportCostCalculations)
            {
                ConvertTransportCostCalculation(calculation, newExchangeRate, oldExchangeRate);
            }

            if (part.RawPart != null)
            {
                ConvertPart(part.RawPart, newExchangeRate, oldExchangeRate);
            }

            ConvertCountrySetting(part.CountrySettings, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the step properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="step">The step.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertProcessStep(ProcessStep step, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (step == null)
            {
                throw new ArgumentNullException("step", "The step can't be null");
            }

            step.ShiftCostExceedRatio = ConvertBaseValue(step.ShiftCostExceedRatio, newExchangeRate, oldExchangeRate);
            step.TransportCost = ConvertBaseValue(step.TransportCost, newExchangeRate, oldExchangeRate);
            step.Price = ConvertBaseValue(step.Price, newExchangeRate, oldExchangeRate);

            foreach (var commodity in step.Commodities)
            {
                ConvertCommodity(commodity, newExchangeRate, oldExchangeRate);
            }

            foreach (var consumable in step.Consumables)
            {
                ConvertConsumable(consumable, newExchangeRate, oldExchangeRate);
            }

            foreach (var die in step.Dies)
            {
                ConvertDie(die, newExchangeRate, oldExchangeRate);
            }

            foreach (var machine in step.Machines)
            {
                ConvertMachine(machine, newExchangeRate, oldExchangeRate);
            }
        }

        /// <summary>
        /// Converts the commodity properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertCommodity(Commodity commodity, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "The commodity can't be null");
            }

            commodity.Price = ConvertBaseValue(commodity.Price, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the consumable properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertConsumable(Consumable consumable, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable can't be null");
            }

            consumable.Price = ConvertBaseValue(consumable.Price, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the die properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertDie(Die die, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die can't be null");
            }

            die.Investment = ConvertBaseValue(die.Investment, newExchangeRate, oldExchangeRate);
            die.ReusableInvest = ConvertBaseValue(die.ReusableInvest, newExchangeRate, oldExchangeRate);

            if (!die.IsWearInPercentage.Value)
            {
                die.Wear = ConvertBaseValue(die.Wear, newExchangeRate, oldExchangeRate);
            }

            if (!die.IsMaintenanceInPercentage.Value)
            {
                die.Maintenance = ConvertBaseValue(die.Maintenance, newExchangeRate, oldExchangeRate);
            }
        }

        /// <summary>
        /// Converts the machine properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertMachine(Machine machine, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine can't be null");
            }

            machine.MachineInvestment = ConvertBaseValue(machine.MachineInvestment, newExchangeRate, oldExchangeRate);
            machine.MachineLeaseCosts = ConvertBaseValue(machine.MachineLeaseCosts, newExchangeRate, oldExchangeRate);
            machine.SetupInvestment = ConvertBaseValue(machine.SetupInvestment, newExchangeRate, oldExchangeRate);
            machine.AdditionalEquipmentInvestment = ConvertBaseValue(machine.AdditionalEquipmentInvestment, newExchangeRate, oldExchangeRate);
            machine.FundamentalSetupInvestment = ConvertBaseValue(machine.FundamentalSetupInvestment, newExchangeRate, oldExchangeRate);
            machine.ExternalWorkCost = ConvertBaseValue(machine.ExternalWorkCost, newExchangeRate, oldExchangeRate);
            machine.MaterialCost = ConvertBaseValue(machine.MaterialCost, newExchangeRate, oldExchangeRate);
            machine.ConsumablesCost = ConvertBaseValue(machine.ConsumablesCost, newExchangeRate, oldExchangeRate);
            machine.RepairsCost = ConvertBaseValue(machine.RepairsCost, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the raw material properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="material">The material.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertRawMaterial(RawMaterial material, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (material == null)
            {
                throw new ArgumentNullException("material", "The material can't be null");
            }

            material.Price = ConvertBaseValue(material.Price, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the country settings properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="countrySetting">The country setting.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">countrySetting;The countrySetting can't be null</exception>
        private static void ConvertCountrySetting(CountrySetting countrySetting, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (countrySetting == null)
            {
                throw new ArgumentNullException("countrySetting", "The countrySetting can't be null");
            }

            countrySetting.UnskilledLaborCost = ConvertBaseValue(countrySetting.UnskilledLaborCost, newExchangeRate, oldExchangeRate);
            countrySetting.SkilledLaborCost = ConvertBaseValue(countrySetting.SkilledLaborCost, newExchangeRate, oldExchangeRate);
            countrySetting.ForemanCost = ConvertBaseValue(countrySetting.ForemanCost, newExchangeRate, oldExchangeRate);
            countrySetting.TechnicianCost = ConvertBaseValue(countrySetting.TechnicianCost, newExchangeRate, oldExchangeRate);
            countrySetting.EngineerCost = ConvertBaseValue(countrySetting.EngineerCost, newExchangeRate, oldExchangeRate);
            countrySetting.EnergyCost = ConvertBaseValue(countrySetting.EnergyCost, newExchangeRate, oldExchangeRate);
            countrySetting.AirCost = ConvertBaseValue(countrySetting.AirCost, newExchangeRate, oldExchangeRate);
            countrySetting.WaterCost = ConvertBaseValue(countrySetting.WaterCost, newExchangeRate, oldExchangeRate);
            countrySetting.ProductionAreaRentalCost = ConvertBaseValue(countrySetting.ProductionAreaRentalCost, newExchangeRate, oldExchangeRate);
            countrySetting.OfficeAreaRentalCost = ConvertBaseValue(countrySetting.OfficeAreaRentalCost, newExchangeRate, oldExchangeRate);
        }

        /// <summary>
        /// Converts the transport cost calculation properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">calculation;The transport cost calculation can't be null</exception>
        private static void ConvertTransportCostCalculation(TransportCostCalculation calculation, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (calculation == null)
            {
                throw new ArgumentNullException("calculation", "The transport cost calculation can't be null");
            }

            calculation.CostByPackage = ConvertBaseValue(calculation.CostByPackage, newExchangeRate, oldExchangeRate);
            calculation.TransporterCostPerKm = ConvertBaseValue(calculation.TransporterCostPerKm, newExchangeRate, oldExchangeRate);

            if (calculation.TransportCostCalculationSetting != null)
            {
                ConvertTransportCostCalculationSetting(calculation.TransportCostCalculationSetting, newExchangeRate, oldExchangeRate);
            }
        }

        /// <summary>
        /// Converts the transport cost calculation setting properties that represent money to the new exchange rate.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <param name="newExchangeRate">The new exchange rate.</param>
        /// <param name="oldExchangeRate">The old exchange rate.</param>
        /// <exception cref="System.ArgumentNullException">setting;The transport cost calculation setting can't be null</exception>
        private static void ConvertTransportCostCalculationSetting(TransportCostCalculationSetting setting, decimal newExchangeRate, decimal oldExchangeRate)
        {
            if (setting == null)
            {
                throw new ArgumentNullException("setting", "The transport cost calculation setting can't be null");
            }

            setting.MegaTrailerCost = ConvertBaseValue(setting.MegaTrailerCost, newExchangeRate, oldExchangeRate);
            setting.Truck3_5tCost = ConvertBaseValue(setting.Truck3_5tCost, newExchangeRate, oldExchangeRate);
            setting.Truck7tCost = ConvertBaseValue(setting.Truck7tCost, newExchangeRate, oldExchangeRate);
            setting.TruckTrailerCost = ConvertBaseValue(setting.TruckTrailerCost, newExchangeRate, oldExchangeRate);
            setting.VanTypeCost = ConvertBaseValue(setting.VanTypeCost, newExchangeRate, oldExchangeRate);
        }
    }
}