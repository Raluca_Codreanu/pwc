﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Helper methods related to the database as a whole.
    /// </summary>
    public static class DatabaseHelper
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Checks whether the central database is online.
        /// </summary>
        /// <returns>True if the central server is accessible (a connection to it was established), false if it is not. 
        /// False means one of the following: the server is stopped, the server connection parameters are not correct or not initialized.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The central server must be reported offline if any error occurs, including invalid connection string.")]
        public static bool IsCentralDatabaseOnline()
        {
            var checkResult = CentralDatabaseOnlineCheck();
            return checkResult.IsOnline;
        }

        /// <summary>
        /// Checks whether the central database is online.
        /// </summary>
        /// <returns>An object containing information regarding the state of the central database.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The central server must be reported offline if any error occurs, including invalid connection string.")]
        public static DbOnlineCheckResult CentralDatabaseOnlineCheck()
        {
            var result = new DbOnlineCheckResult();

            try
            {
                // Check if the central database is online and also if its version matches the version required by th application.
                // If the central db version is not compatible with the application we consider the central database to be offline.
                ValidateDatabaseVersion(DbIdentifier.CentralDatabase);
                result.IsOnline = true;
            }
            catch (DatabaseVersionMismatchException)
            {
                result.IsOnline = false;
                result.OfflineReason = DbOfflineReason.IncompatibleVersion;
            }
            catch
            {
                result.IsOnline = false;
                result.OfflineReason = DbOfflineReason.CanNotConnect;
            }

            return result;
        }

        /// <summary>
        /// It checks if the database version and the application version match.
        /// Simply returns if the versions match, but throws a <see cref="DatabaseVersionMismatchException"/> if the version do not match.
        /// </summary>
        /// <param name="databaseId">The database whose version to validate against the application required version.</param>
        /// <exception cref="DatabaseVersionMismatchException">
        /// The version of the database indicated by the <paramref name="databaseId"/> argument and the version expected by the application do not match.
        /// </exception>
        public static void ValidateDatabaseVersion(DbIdentifier databaseId)
        {
            try
            {
                // Get the database version directly from the database (DbGlobalSettingsManager caches the value and it may not be up to date).
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var dbVersionSetting = dataManager.GlobalSettingsRepository.GetVersion();

                var dbVersion = decimal.Parse(dbVersionSetting.Value, System.Globalization.CultureInfo.InvariantCulture);
                var requiredDbVersion = Constants.DataBaseVersion;

                if (dbVersion != requiredDbVersion)
                {
                    if (databaseId == DbIdentifier.LocalDatabase)
                    {
                        log.Warn("Local database and application version mismatch (db version: {0}, required db version: {1}).", dbVersion, requiredDbVersion);
                        throw new DatabaseVersionMismatchException(DatabaseErrorCode.LocalDatabaseVersionMismatch);
                    }
                    else if (databaseId == DbIdentifier.CentralDatabase)
                    {
                        log.Warn("Central Database and application version mismatch (db version: {0}, required db version: {1}).", dbVersion, requiredDbVersion);
                        throw new DatabaseVersionMismatchException(DatabaseErrorCode.CentralDatabaseVersionMismatch);
                    }
                }
            }
            catch (DataAccessException ex)
            {
                SqlException sqlEx = ex.FindInnerException<SqlException>();
                if (sqlEx != null)
                {
                    // This means that the DataBaseInfo table does not exist or does not have the expected schema.
                    if (sqlEx.Number == 207 || sqlEx.Number == 208)
                    {
                        if (databaseId == DbIdentifier.LocalDatabase)
                        {
                            log.Warn("Local database and application version mismatch (DataBaseInfo table does not exist or does not have the expected schema).");
                            throw new DatabaseVersionMismatchException(DatabaseErrorCode.LocalDatabaseVersionMismatch);
                        }
                        else if (databaseId == DbIdentifier.CentralDatabase)
                        {
                            log.Warn("Central Database and application version mismatch (DataBaseInfo table does not exist or does not have the expected schema).");
                            throw new DatabaseVersionMismatchException(DatabaseErrorCode.CentralDatabaseVersionMismatch);
                        }
                    }
                }

                throw;
            }
        }
    }
}
