﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Contains methods for releasing entities.
    /// </summary>
    public sealed class ReleaseManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly ReleaseManager instance = new ReleaseManager();

        /// <summary>
        /// A dictionary containing all instances of shared objects that have been cloned for release.
        /// The key is the id of the original shared object and the value is the released (cloned) shared object.
        /// </summary>
        private Dictionary<Guid, object> sharedObjectsCache;

        /// <summary>
        /// A value indicating the source database of the project to be released.
        /// </summary>
        private DbIdentifier sourceDatabaseId;

        #endregion Attributes

        /// <summary>
        /// Prevents a default instance of the <see cref="ReleaseManager" /> class from being created.
        /// </summary>
        private ReleaseManager()
        {
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static ReleaseManager Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Releases a project. This operation can be done only when connected to the central database.
        /// Makes a new copy of the project and sets the IsReleased flag to true.
        /// </summary>
        /// <param name="projectId">The id of the project to release.</param>
        /// <param name="projectSourceDatabase">The database from where the project to release should be retrieved.</param>
        public void ReleaseProject(Guid projectId, DbIdentifier projectSourceDatabase)
        {
            if (projectId == Guid.Empty)
            {
                throw new ArgumentException("The id of the project to release was empty.", "projectId");
            }

            if (projectSourceDatabase == DbIdentifier.NotSet)
            {
                throw new ArgumentException("The source database of the project to release was not set.", "projectSourceDatabase");
            }

            this.sourceDatabaseId = projectSourceDatabase;
            var releaseDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            try
            {
                // Do not fully load the project because can consume a lot of memory, instead load the project and then fully load each of the project's assemblies and parts.
                var projectDataManager = DataAccessFactory.CreateDataSourceManager(projectSourceDatabase);
                var projectToRelease = projectDataManager.ProjectRepository.GetProjectIncludingTopLevelChildren(projectId, true);
                if (projectToRelease == null)
                {
                    throw new BusinessException(ErrorCodes.ItemNotFound);
                }

                if (projectToRelease.IsReleased)
                {
                    throw new BusinessException(ErrorCodes.InvalidProjectState, "Project is already released.");
                }

                // Initialize the shared objects cache used to establish the same relationships in the copy of the project.
                this.sharedObjectsCache = new Dictionary<Guid, object>();

                // Create a copy of the project to be released and add it into the central database.
                var releasedProject = this.GetCleanCopyOfProject(projectToRelease);
                releaseDataManager.ProjectRepository.Add(releasedProject);
                releaseDataManager.SaveChanges();

                // Fully load and release each of the project assemblies.
                foreach (var assembly in projectToRelease.Assemblies.Where(a => !a.IsDeleted))
                {
                    using (var dataManager = DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId))
                    {
                        var freshAssembly = dataManager.AssemblyRepository.GetAssemblyFull(assembly.Guid);
                        var cleanAssembly = this.RetrieveCopyOfObject(freshAssembly);
                        cleanAssembly.Project = releasedProject;
                        releaseDataManager.AssemblyRepository.Add(cleanAssembly);
                        releaseDataManager.SaveChanges();
                    }
                }

                // Fully load and release each of the project parts.
                foreach (var part in projectToRelease.Parts.Where(p => !p.IsDeleted))
                {
                    using (var dataManager = DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId))
                    {
                        var freshPart = dataManager.PartRepository.GetPartFull(part.Guid);
                        var cleanPart = this.RetrieveCopyOfObject(freshPart);
                        cleanPart.Project = releasedProject;
                        releaseDataManager.PartRepository.Add(cleanPart);
                        releaseDataManager.SaveChanges();
                    }
                }
            }
            catch (DataAccessException exception)
            {
                log.ErrorException("Persistence exception occurred while releasing a project.", exception);
                throw new BusinessException(exception);
            }
            finally
            {
                // Clean up after the release operation.
                this.sharedObjectsCache = null;
                if (releaseDataManager != null)
                {
                    releaseDataManager.Dispose();
                }
            }
        }

        /// <summary>
        /// Un-releases a project.
        /// </summary>
        /// <param name="projectId">The id of the project to un-release.</param>
        public void UnReleaseProject(Guid projectId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            this.UnReleaseProject(projectId, dataManager);
        }

        /// <summary>
        /// Un-release a project.
        /// </summary>
        /// <param name="projectId">The id of the project to un-release.</param>
        /// <param name="dataSourceManager">The data source manager to use for the data related operations.</param>
        public void UnReleaseProject(Guid projectId, IDataSourceManager dataSourceManager)
        {
            if (projectId == Guid.Empty)
            {
                throw new ArgumentException("The id of the project to release was empty.", "projectId");
            }

            if (dataSourceManager == null)
            {
                throw new ArgumentNullException("dataSourceManager", "The data source manager was null.");
            }

            try
            {
                // Get the project to un-release from the central db, fully loaded.
                var centralProject = dataSourceManager.ProjectRepository.GetProjectFull(projectId);
                if (centralProject == null)
                {
                    // If the project no longer exists in the central database then it has already been released.
                    return;
                }

                if (!centralProject.IsReleased)
                {
                    throw new BusinessException(ErrorCodes.InvalidProjectState, "Project is not in the released state thus it can not be unreleased.");
                }

                // Delete the project from the central db.
                this.DeleteSharedObjects(centralProject, dataSourceManager);
                dataSourceManager.ProjectRepository.RemoveAll(centralProject);
                dataSourceManager.SaveChanges();
            }
            catch (ConcurrencyException ex)
            {
                throw new BusinessException(DatabaseErrorCode.OptimisticConcurrency, ex);
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Persistence exception occurred while un-releasing a project.", ex);
                throw new BusinessException(ex);
            }
        }

        #region Release

        /// <summary>
        /// Retrieves the copy of an object from the shared objects cache or creates a copy of it if the cache does not contain it yet.
        /// </summary>
        /// <typeparam name="T">The type of the object to copy.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A copy of the entity.</returns>
        private T RetrieveCopyOfObject<T>(T entity) where T : class, IIdentifiable
        {
            if (entity == null)
            {
                return null;
            }

            // Try to retrieve the element from the shared objects cache.
            object cachedCopy;
            if (this.sharedObjectsCache.TryGetValue(entity.Guid, out cachedCopy))
            {
                // If it was found in the cache, simply return it.
                var copy = cachedCopy as T;
                if (copy == null)
                {
                    throw new InvalidOperationException("The cached copy of a shared object was null.");
                }

                return copy;
            }
            else
            {
                // If it couldn't be found create a copy and cache it.
                var copyOfEntity = this.CreateCopyOfObject(entity);
                this.sharedObjectsCache.Add(entity.Guid, copyOfEntity);
                return copyOfEntity;
            }
        }

        /// <summary>
        /// Creates the copy of the given object.
        /// </summary>
        /// <typeparam name="T">The type of the entity</typeparam>
        /// <param name="entity">The entity to create it's copy.</param>
        /// <returns>A clean copy of the entity.</returns>
        private T CreateCopyOfObject<T>(T entity) where T : IIdentifiable
        {
            var measurementUnit = entity as MeasurementUnit;
            if (measurementUnit != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfMeasurementUnit(measurementUnit), typeof(T));
            }

            var rawPart = entity as RawPart;
            if (rawPart != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfPart(rawPart), typeof(T));
            }

            var part = entity as Part;
            if (part != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfPart(part), typeof(T));
            }

            var deliveryType = entity as RawMaterialDeliveryType;
            if (deliveryType != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfRawMaterialDeliveryType(deliveryType), typeof(T));
            }

            var stepClassification = entity as ProcessStepsClassification;
            if (stepClassification != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfProcessStepClassification(stepClassification), typeof(T));
            }

            var machineClassification = entity as MachinesClassification;
            if (machineClassification != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfMachinesClassification(machineClassification), typeof(T));
            }

            var assembly = entity as Assembly;
            if (assembly != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfAssembly(assembly), typeof(T));
            }

            var user = entity as User;
            if (user != null)
            {
                return (T)Convert.ChangeType(this.GetCleanCopyOfUser(user), typeof(T));
            }

            var materialClassification = entity as MaterialsClassification;
            if (materialClassification != null)
            {
                return (T)Convert.ChangeType(this.CopyMaterialsClassification(materialClassification), typeof(T));
            }

            var currency = entity as Currency;
            if (currency != null)
            {
                return (T)Convert.ChangeType(this.CopyCurrency(currency), typeof(T));
            }

            var transportCost = entity as TransportCostCalculation;
            if (transportCost != null)
            {
                return (T)Convert.ChangeType(this.CopyTransportCostCalculation(transportCost), typeof(T));
            }

            var transportSetting = entity as TransportCostCalculationSetting;
            if (transportSetting != null)
            {
                return (T)Convert.ChangeType(this.CopyTransportCostCalculationSetting(transportSetting), typeof(T));
            }

            throw new InvalidOperationException("CreateCopyOfObject is not implemented for type " + typeof(T).Name);
        }

        /// <summary>
        /// Gets the clean copy of a project.
        /// </summary>
        /// <param name="project">The project to copy.</param>
        /// <returns>The clean copy of a project.</returns>
        private Project GetCleanCopyOfProject(Project project)
        {
            var copy = new Project();
            project.CopyValuesTo(copy);

            copy.Customer = this.GetCleanCopyOfSupplier(project.Customer);
            copy.OverheadSettings = project.OverheadSettings.Copy();

            var mediaDataManager = DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId);
            var mediaManager = new MediaManager(mediaDataManager);
            var media = mediaManager.GetAllMedia(project);
            foreach (var m in media)
            {
                copy.Media.Add(m.Copy());
            }

            if (project.ProjectLeader != null)
            {
                copy.ProjectLeader = this.RetrieveCopyOfObject(project.ProjectLeader);
            }

            if (project.ResponsibleCalculator != null)
            {
                copy.ResponsibleCalculator = this.RetrieveCopyOfObject(project.ResponsibleCalculator);
            }

            foreach (var currency in project.Currencies)
            {
                copy.Currencies.Add(this.RetrieveCopyOfObject(currency));
            }

            if (project.BaseCurrency != null)
            {
                copy.BaseCurrency = this.RetrieveCopyOfObject(project.BaseCurrency);
            }

            copy.SetOwner(null);
            copy.SetIsMasterData(false);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a part.
        /// </summary>
        /// <param name="part">The part to copy.</param>
        /// <returns>A clean copy of the part.</returns>
        private Part GetCleanCopyOfPart(Part part)
        {
            if (part == null)
            {
                return null;
            }

            var copy = part is RawPart ? new RawPart() : new Part();
            part.CopyValuesTo(copy);
            copy.CountrySettings = part.CountrySettings.Copy();
            copy.Manufacturer = part.Manufacturer.Copy();
            copy.OverheadSettings = part.OverheadSettings.Copy();
            copy.CalculatorUser = this.RetrieveCopyOfObject(part.CalculatorUser);
            copy.MeasurementUnit = this.RetrieveCopyOfObject(part.MeasurementUnit);

            foreach (var commodity in part.Commodities.Where(c => !c.IsDeleted))
            {
                copy.Commodities.Add(this.GetCleanCopyOfCommodity(commodity));
            }

            foreach (var rawMaterial in part.RawMaterials.Where(m => !m.IsDeleted))
            {
                copy.RawMaterials.Add(this.GetCleanCopyOfRawMaterial(rawMaterial));
            }

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetAllMedia(part);
            foreach (var m in media)
            {
                copy.Media.Add(m.Copy());
            }

            foreach (var calculation in part.TransportCostCalculations)
            {
                copy.TransportCostCalculations.Add(this.RetrieveCopyOfObject(calculation));
            }

            copy.Process = this.GetCleanCopyOfProcess(part.Process);
            copy.RawPart = this.GetCleanCopyOfPart(part.RawPart);

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a Assembly.
        /// </summary>
        /// <param name="assembly">The assembly to copy.</param>
        /// <returns>The clean copy of a assembly.</returns>
        private Assembly GetCleanCopyOfAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                return null;
            }

            var copy = new Assembly();
            assembly.CopyValuesTo(copy);
            copy.CountrySettings = assembly.CountrySettings.Copy();
            copy.OverheadSettings = assembly.OverheadSettings.Copy();
            copy.Manufacturer = assembly.Manufacturer != null ? assembly.Manufacturer.Copy() : null;
            copy.MeasurementUnit = this.RetrieveCopyOfObject(assembly.MeasurementUnit);

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var mediaList = mediaManager.GetAllMedia(assembly);
            foreach (var media in mediaList)
            {
                copy.Media.Add(media.Copy());
            }

            foreach (var part in assembly.Parts.Where(p => !p.IsDeleted))
            {
                copy.Parts.Add(this.RetrieveCopyOfObject(part));
            }

            foreach (var subAssembly in assembly.Subassemblies.Where(a => !a.IsDeleted))
            {
                copy.Subassemblies.Add(this.RetrieveCopyOfObject(subAssembly));
            }

            foreach (var calculation in assembly.TransportCostCalculations)
            {
                copy.TransportCostCalculations.Add(this.RetrieveCopyOfObject(calculation));
            }

            copy.Process = this.GetCleanCopyOfProcess(assembly.Process);

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a user.
        /// </summary>
        /// <param name="user">The user to copy.</param>
        /// <returns>The copy of the user</returns>
        private object GetCleanCopyOfUser(User user)
        {
            if (user == null)
            {
                return null;
            }

            var copy = new User();
            user.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a raw material delivery type.
        /// </summary>
        /// <param name="rawMaterialDeliveryType">The raw material delivery type to copy.</param>
        /// <returns>The clean copy of a raw material delivery type.</returns>
        private RawMaterialDeliveryType GetCleanCopyOfRawMaterialDeliveryType(RawMaterialDeliveryType rawMaterialDeliveryType)
        {
            if (rawMaterialDeliveryType == null)
            {
                return null;
            }

            var copy = new RawMaterialDeliveryType();
            rawMaterialDeliveryType.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of supplier.
        /// </summary>
        /// <param name="supplier">The supplier to copy.</param>
        /// <returns>A clean copy if the supplier.</returns>
        private Customer GetCleanCopyOfSupplier(Customer supplier)
        {
            if (supplier == null)
            {
                return null;
            }

            var copy = new Customer();
            supplier.CopyValuesTo(copy);
            copy.Owner = null;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a measurement unit.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit to copy.</param>
        /// <returns>A clean copy of the measurement unit.</returns>
        private MeasurementUnit GetCleanCopyOfMeasurementUnit(MeasurementUnit measurementUnit)
        {
            if (measurementUnit == null)
            {
                return null;
            }

            var copy = new MeasurementUnit();
            measurementUnit.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a process.
        /// </summary>
        /// <param name="process">The process to copy.</param>
        /// <returns>A copy of the given process</returns>
        private Process GetCleanCopyOfProcess(Process process)
        {
            if (process == null)
            {
                return null;
            }

            var copy = new Process();
            foreach (var step in process.Steps)
            {
                var newStep = this.GetCleanCopyOfProcessStep(step);
                if (newStep != null)
                {
                    copy.Steps.Add(newStep);
                }
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a process step.
        /// </summary>
        /// <param name="step">The step to copy.</param>
        /// <returns>The clean copy of a process step</returns>
        private ProcessStep GetCleanCopyOfProcessStep(ProcessStep step)
        {
            if (step == null)
            {
                return null;
            }

            ProcessStep copy = null;

            var partProcessStep = step as PartProcessStep;
            if (partProcessStep != null)
            {
                copy = new PartProcessStep();
                partProcessStep.CopyValuesTo(copy);
                this.CopyCommonProcessStepRefs(step, copy);
            }

            var assemblyProcessStep = step as AssemblyProcessStep;
            if (assemblyProcessStep != null)
            {
                copy = new AssemblyProcessStep();
                assemblyProcessStep.CopyValuesTo(copy);
                this.CopyCommonProcessStepRefs(step, copy);
            }

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetPictureOrVideo(step);
            if (media != null)
            {
                copy.Media = media.Copy();
            }

            return copy;
        }

        /// <summary>
        /// Copies the common process step sub-objects and references.
        /// </summary>
        /// <param name="source">The source process step.</param>
        /// <param name="dest">The destination process step.</param>
        private void CopyCommonProcessStepRefs(ProcessStep source, ProcessStep dest)
        {
            if (source == null || dest == null)
            {
                return;
            }

            dest.Type = this.RetrieveCopyOfObject(source.Type);
            dest.SubType = this.RetrieveCopyOfObject(source.SubType);
            dest.CycleTimeUnit = this.RetrieveCopyOfObject(source.CycleTimeUnit);
            dest.ProcessTimeUnit = this.RetrieveCopyOfObject(source.ProcessTimeUnit);
            dest.SetupTimeUnit = this.RetrieveCopyOfObject(source.SetupTimeUnit);
            dest.MaxDownTimeUnit = this.RetrieveCopyOfObject(source.MaxDownTimeUnit);

            foreach (var item in source.Commodities.Where(c => !c.IsDeleted))
            {
                var newCommodity = this.GetCleanCopyOfCommodity(item);
                if (newCommodity != null)
                {
                    dest.Commodities.Add(newCommodity);
                }
            }

            foreach (var item in source.Consumables.Where(c => !c.IsDeleted))
            {
                var newConsumable = this.GetCleanCopyOfConsumable(item);
                if (newConsumable != null)
                {
                    dest.Consumables.Add(newConsumable);
                }
            }

            foreach (var item in source.Dies.Where(d => !d.IsDeleted))
            {
                var newDie = this.GetCleanCopyOfDie(item);
                if (newDie != null)
                {
                    dest.Dies.Add(newDie);
                }
            }

            foreach (var item in source.Machines.Where(m => !m.IsDeleted))
            {
                var newMachine = this.GetCleanCopyOfMachine(item);
                if (newMachine != null)
                {
                    dest.Machines.Add(newMachine);
                }
            }

            foreach (var calculation in source.CycleTimeCalculations)
            {
                var calcCopy = new CycleTimeCalculation();
                calculation.CopyValuesTo(calcCopy);
                dest.CycleTimeCalculations.Add(calcCopy);
            }

            foreach (var amount in source.PartAmounts)
            {
                var newAmount = this.GetCleanCopyOfProcessStepPartAmount(amount);
                if (newAmount != null)
                {
                    dest.PartAmounts.Add(newAmount);
                }
            }

            foreach (var amount in source.AssemblyAmounts)
            {
                var newAmount = this.GetCleanCopyOfProcessStepAssemblyAmount(amount);
                if (newAmount != null)
                {
                    dest.AssemblyAmounts.Add(newAmount);
                }
            }
        }

        /// <summary>
        /// Gets the clean copy of a process step part amount.
        /// </summary>
        /// <param name="item">The item to copy.</param>
        /// <returns>The clean copy of a process step part amount.</returns>
        private ProcessStepPartAmount GetCleanCopyOfProcessStepPartAmount(ProcessStepPartAmount item)
        {
            if (item == null)
            {
                return null;
            }

            var copy = new ProcessStepPartAmount();
            item.CopyValuesTo(copy);
            copy.Part = this.RetrieveCopyOfObject(item.Part);

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a process step Assembly amount.
        /// </summary>
        /// <param name="item">The item to copy.</param>
        /// <returns>The clean copy of a process step Assembly amount.</returns>
        private ProcessStepAssemblyAmount GetCleanCopyOfProcessStepAssemblyAmount(ProcessStepAssemblyAmount item)
        {
            if (item == null)
            {
                return null;
            }

            var copy = new ProcessStepAssemblyAmount();
            item.CopyValuesTo(copy);
            copy.Assembly = this.RetrieveCopyOfObject(item.Assembly);

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a machine.
        /// </summary>
        /// <param name="machine">The machine to copy.</param>
        /// <returns>The clean copy of the machine.</returns>
        private Machine GetCleanCopyOfMachine(Machine machine)
        {
            if (machine == null)
            {
                return null;
            }

            var copy = new Machine();
            machine.CopyValuesTo(copy);
            copy.Manufacturer = machine.Manufacturer != null ? machine.Manufacturer.Copy() : null;
            copy.ClassificationLevel4 = this.RetrieveCopyOfObject(machine.ClassificationLevel4);
            copy.SubClassification = this.RetrieveCopyOfObject(machine.SubClassification);
            copy.TypeClassification = this.RetrieveCopyOfObject(machine.TypeClassification);
            copy.MainClassification = this.RetrieveCopyOfObject(machine.MainClassification);

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetPictureOrVideo(machine);
            if (media != null)
            {
                copy.Media = media.Copy();
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a consumable.
        /// </summary>
        /// <param name="consumable">The consumable to copy.</param>
        /// <returns>The clean copy of a consumable.</returns>
        private Consumable GetCleanCopyOfConsumable(Consumable consumable)
        {
            if (consumable == null)
            {
                return null;
            }

            var copy = new Consumable();
            consumable.CopyValuesTo(copy);
            copy.Manufacturer = consumable.Manufacturer != null ? consumable.Manufacturer.Copy() : null;
            copy.PriceUnitBase = this.RetrieveCopyOfObject(consumable.PriceUnitBase);
            copy.AmountUnitBase = this.RetrieveCopyOfObject(consumable.AmountUnitBase);

            foreach (var history in consumable.PriceHistory)
            {
                var newHistory = new ConsumablesPriceHistory();
                history.CopyValuesTo(newHistory);
                copy.PriceHistory.Add(newHistory);
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a die.
        /// </summary>
        /// <param name="die">The die to copy.</param>
        /// <returns>The clean copy of a die.</returns>
        private Die GetCleanCopyOfDie(Die die)
        {
            if (die == null)
            {
                return null;
            }

            var copy = new Die();
            die.CopyValuesTo(copy);
            copy.Manufacturer = die.Manufacturer != null ? die.Manufacturer.Copy() : null;

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetPictureOrVideo(die);
            if (media != null)
            {
                copy.Media = media.Copy();
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a raw material.
        /// </summary>
        /// <param name="rawMaterial">The raw material to copy.</param>
        /// <returns>A clean copy of the raw material.</returns>
        private RawMaterial GetCleanCopyOfRawMaterial(RawMaterial rawMaterial)
        {
            if (rawMaterial == null)
            {
                return null;
            }

            var copy = new RawMaterial();
            rawMaterial.CopyValuesTo(copy);

            copy.DeliveryType = this.RetrieveCopyOfObject(rawMaterial.DeliveryType);
            copy.Manufacturer = rawMaterial.Manufacturer != null ? rawMaterial.Manufacturer.Copy() : null;
            copy.PriceUnitBase = this.RetrieveCopyOfObject(rawMaterial.PriceUnitBase);
            copy.ParentWeightUnitBase = this.RetrieveCopyOfObject(rawMaterial.ParentWeightUnitBase);
            copy.QuantityUnitBase = this.RetrieveCopyOfObject(rawMaterial.QuantityUnitBase);
            copy.MaterialsClassificationL1 = this.RetrieveCopyOfObject(rawMaterial.MaterialsClassificationL1);
            copy.MaterialsClassificationL2 = this.RetrieveCopyOfObject(rawMaterial.MaterialsClassificationL2);
            copy.MaterialsClassificationL3 = this.RetrieveCopyOfObject(rawMaterial.MaterialsClassificationL3);
            copy.MaterialsClassificationL4 = this.RetrieveCopyOfObject(rawMaterial.MaterialsClassificationL4);

            foreach (var history in rawMaterial.PriceHistory)
            {
                var newHistory = new RawMaterialsPriceHistory();
                history.CopyValuesTo(newHistory);
                copy.PriceHistory.Add(newHistory);
            }

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetPictureOrVideo(rawMaterial);
            if (media != null)
            {
                copy.Media = media.Copy();
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a commodity.
        /// </summary>
        /// <param name="commodity">The commodity to copy.</param>
        /// <returns>A clean copy of the commodity</returns>
        private Commodity GetCleanCopyOfCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                return null;
            }

            var copy = new Commodity();
            commodity.CopyValuesTo(copy);

            copy.Manufacturer = commodity.Manufacturer != null ? commodity.Manufacturer.Copy() : null;
            copy.WeightUnitBase = this.RetrieveCopyOfObject(commodity.WeightUnitBase);

            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.sourceDatabaseId));
            var media = mediaManager.GetPictureOrVideo(commodity);
            if (media != null)
            {
                copy.Media = media.Copy();
            }

            foreach (var history in commodity.PriceHistory)
            {
                var newHistory = new CommoditiesPriceHistory();
                history.CopyValuesTo(newHistory);
                copy.PriceHistory.Add(newHistory);
            }

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a process step classification.
        /// </summary>
        /// <param name="processStepsClassification">The process steps classification.</param>
        /// <returns>The clean copy of a process step classification.</returns>
        private ProcessStepsClassification GetCleanCopyOfProcessStepClassification(ProcessStepsClassification processStepsClassification)
        {
            if (processStepsClassification == null)
            {
                return null;
            }

            var copy = new ProcessStepsClassification();
            processStepsClassification.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Gets the clean copy of a machine classification.
        /// </summary>
        /// <param name="machineClassification">The machine classification.</param>
        /// <returns>The clean copy of the classification</returns>
        private MachinesClassification GetCleanCopyOfMachinesClassification(MachinesClassification machineClassification)
        {
            if (machineClassification == null)
            {
                return null;
            }

            var copy = new MachinesClassification();
            machineClassification.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Creates a copy of a machine classification.
        /// </summary>
        /// <param name="classification">The classification to copy.</param>
        /// <returns>The copy of the classification</returns>
        private MaterialsClassification CopyMaterialsClassification(MaterialsClassification classification)
        {
            if (classification == null)
            {
                return null;
            }

            var copy = new MaterialsClassification();
            classification.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Creates a copy of a currency.
        /// </summary>
        /// <param name="currency">The currency to copy.</param>
        /// <returns>The copy of the currency.</returns>
        private Currency CopyCurrency(Currency currency)
        {
            if (currency == null)
            {
                return null;
            }

            var copy = new Currency();
            currency.CopyValuesTo(copy);
            copy.IsReleased = true;

            return copy;
        }

        /// <summary>
        /// Creates a copy of a transport cost calculation.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <returns>The copy of the calculation.</returns>
        private TransportCostCalculation CopyTransportCostCalculation(TransportCostCalculation calculation)
        {
            if (calculation == null)
            {
                return null;
            }

            var copy = new TransportCostCalculation();
            calculation.CopyValuesTo(copy);
            copy.TransportCostCalculationSetting = this.RetrieveCopyOfObject(calculation.TransportCostCalculationSetting);

            return copy;
        }

        /// <summary>
        /// Creates a copy of a transport cost calculation setting.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>The copy of the setting</returns>
        private TransportCostCalculationSetting CopyTransportCostCalculationSetting(TransportCostCalculationSetting setting)
        {
            if (setting == null)
            {
                return null;
            }

            var copy = new TransportCostCalculationSetting();
            setting.CopyValuesTo(copy);

            return copy;
        }

        #endregion Release

        #region Un-Release

        /// <summary>
        /// Deletes the shared objects from the graph of a project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Project project, IDataSourceManager dataSourceManager)
        {
            if (project == null)
            {
                return;
            }

            if (project.ProjectLeader != null)
            {
                dataSourceManager.UserRepository.RemoveAll(project.ProjectLeader);
            }

            if (project.ResponsibleCalculator != null)
            {
                dataSourceManager.UserRepository.RemoveAll(project.ResponsibleCalculator);
            }

            foreach (var part in project.Parts)
            {
                this.DeleteSharedObjects(part, dataSourceManager);
            }

            foreach (var assembly in project.Assemblies)
            {
                this.DeleteSharedObjects(assembly, dataSourceManager);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a part.
        /// </summary>
        /// <param name="part">The part to delete from.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Part part, IDataSourceManager dataSourceManager)
        {
            if (part == null)
            {
                return;
            }

            if (part.CalculatorUser != null)
            {
                dataSourceManager.UserRepository.RemoveAll(part.CalculatorUser);
            }

            if (part.Process != null)
            {
                foreach (var step in part.Process.Steps)
                {
                    this.DeleteSharedObjects(step, dataSourceManager);
                }
            }

            foreach (var rawMaterial in part.RawMaterials)
            {
                this.DeleteSharedObjects(rawMaterial, dataSourceManager);
            }

            foreach (var commodity in part.Commodities)
            {
                this.DeleteSharedObjects(commodity, dataSourceManager);
            }

            foreach (var calculation in part.TransportCostCalculations)
            {
                this.DeleteSharedObjects(calculation, dataSourceManager);
            }

            if (part.RawPart != null)
            {
                this.DeleteSharedObjects(part.RawPart, dataSourceManager);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of an assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Assembly assembly, IDataSourceManager dataSourceManager)
        {
            if (assembly == null)
            {
                return;
            }

            foreach (var part in assembly.Parts)
            {
                this.DeleteSharedObjects(part, dataSourceManager);
            }

            foreach (var subAssembly in assembly.Subassemblies)
            {
                this.DeleteSharedObjects(subAssembly, dataSourceManager);
            }

            foreach (var calculation in assembly.TransportCostCalculations)
            {
                this.DeleteSharedObjects(calculation, dataSourceManager);
            }

            if (assembly.Process != null)
            {
                foreach (var step in assembly.Process.Steps)
                {
                    this.DeleteSharedObjects(step, dataSourceManager);
                }
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a process step.
        /// </summary>
        /// <param name="step">The process step.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(ProcessStep step, IDataSourceManager dataSourceManager)
        {
            if (step == null)
            {
                return;
            }

            if (step.CycleTimeUnit != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(step.CycleTimeUnit);
            }

            if (step.MaxDownTimeUnit != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(step.MaxDownTimeUnit);
            }

            if (step.ProcessTimeUnit != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(step.ProcessTimeUnit);
            }

            if (step.SetupTimeUnit != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(step.SetupTimeUnit);
            }

            if (step.SubType != null)
            {
                dataSourceManager.ProcessStepsClassificationRepository.RemoveAll(step.SubType);
            }

            if (step.Type != null)
            {
                dataSourceManager.ProcessStepsClassificationRepository.RemoveAll(step.Type);
            }

            foreach (var consumable in step.Consumables)
            {
                this.DeleteSharedObjects(consumable, dataSourceManager);
            }

            foreach (var commodity in step.Commodities)
            {
                this.DeleteSharedObjects(commodity, dataSourceManager);
            }

            foreach (var die in step.Dies)
            {
                this.DeleteSharedObjects(die, dataSourceManager);
            }

            foreach (var mach in step.Machines)
            {
                this.DeleteSharedObjects(mach, dataSourceManager);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a consumable.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Consumable consumable, IDataSourceManager dataSourceManager)
        {
            if (consumable == null)
            {
                return;
            }

            if (consumable.AmountUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(consumable.AmountUnitBase);
            }

            if (consumable.PriceUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(consumable.PriceUnitBase);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a material.
        /// </summary>
        /// <param name="material">The material.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(RawMaterial material, IDataSourceManager dataSourceManager)
        {
            if (material == null)
            {
                return;
            }

            if (material.DeliveryType != null)
            {
                dataSourceManager.RawMaterialDeliveryTypeRepository.RemoveAll(material.DeliveryType);
            }

            if (material.PriceUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(material.PriceUnitBase);
            }

            if (material.ParentWeightUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(material.ParentWeightUnitBase);
            }

            if (material.QuantityUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(material.QuantityUnitBase);
            }

            if (material.MaterialsClassificationL1 != null)
            {
                dataSourceManager.MaterialsClassificationRepository.RemoveAll(material.MaterialsClassificationL1);
            }

            if (material.MaterialsClassificationL2 != null)
            {
                dataSourceManager.MaterialsClassificationRepository.RemoveAll(material.MaterialsClassificationL2);
            }

            if (material.MaterialsClassificationL3 != null)
            {
                dataSourceManager.MaterialsClassificationRepository.RemoveAll(material.MaterialsClassificationL3);
            }

            if (material.MaterialsClassificationL4 != null)
            {
                dataSourceManager.MaterialsClassificationRepository.RemoveAll(material.MaterialsClassificationL4);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a commodity.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Commodity commodity, IDataSourceManager dataSourceManager)
        {
            if (commodity == null)
            {
                return;
            }

            if (commodity.WeightUnitBase != null)
            {
                dataSourceManager.MeasurementUnitRepository.RemoveAll(commodity.WeightUnitBase);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a die.
        /// </summary>
        /// <param name="die">The die to delete from.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "dataSourceManager", Justification = "Code will be added later, when it is necessary.")]
        private void DeleteSharedObjects(Die die, IDataSourceManager dataSourceManager)
        {
            if (die == null)
            {
                return;
            }
        }

        /// <summary>
        /// Deletes the shared objects from the graph of a machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <param name="dataSourceManager">The data source manager to be used for deleting the objects.</param>
        private void DeleteSharedObjects(Machine machine, IDataSourceManager dataSourceManager)
        {
            if (machine == null)
            {
                return;
            }

            if (machine.ClassificationLevel4 != null)
            {
                dataSourceManager.MachinesClassificationRepository.RemoveAll(machine.ClassificationLevel4);
            }

            if (machine.SubClassification != null)
            {
                dataSourceManager.MachinesClassificationRepository.RemoveAll(machine.SubClassification);
            }

            if (machine.TypeClassification != null)
            {
                dataSourceManager.MachinesClassificationRepository.RemoveAll(machine.TypeClassification);
            }

            if (machine.MainClassification != null)
            {
                dataSourceManager.MachinesClassificationRepository.RemoveAll(machine.MainClassification);
            }
        }

        /// <summary>
        /// Deletes the shared objects from the transport cost calculation.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <param name="dataSourceManager">The data source manager.</param>
        private void DeleteSharedObjects(TransportCostCalculation calculation, IDataSourceManager dataSourceManager)
        {
            if (calculation == null)
            {
                return;
            }

            if (calculation.TransportCostCalculationSetting != null)
            {
                dataSourceManager.TransportCostCalculationSettingRepository.RemoveAll(calculation.TransportCostCalculationSetting);
            }
        }

        #endregion Un-Release
    }
}