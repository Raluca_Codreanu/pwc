﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business
{
    /// <summary>
    /// Holds the result of checking whether a database is online.
    /// </summary>
    public class DbOnlineCheckResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the database is online.
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Gets or sets the reason why the database is offline.
        /// <para/>
        /// This value is relevant only if the value of the <see cref="IsOnline"/> property is false.
        /// </summary>
        public DbOfflineReason OfflineReason { get; set; }

        /// <summary>
        /// Gets or sets the version of the database that was checked.
        /// </summary>
        /// TODO: to be added later if necessary.
        //// public decimal DatabaseVersion { get; set; }       
    }

    /// <summary>
    /// The reasons why a database is offline
    /// </summary>
    public enum DbOfflineReason
    {
        /// <summary>
        /// The reson is unknown but the database is definetly offline/inaccesible.
        /// </summary>
        Other = 0,

        /// <summary>
        /// Can not connect to the database.
        /// </summary>
        CanNotConnect,

        /// <summary>
        /// The database version is not compatible with the application version (the database is accessible in this case).
        /// </summary>
        IncompatibleVersion
    }
}
