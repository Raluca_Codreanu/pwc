﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business
{
    /// <summary>
    /// Exposes the global settings stored in the database as class properties.
    /// </summary>
    public class DbGlobalSettings
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public decimal Version { get; set; }

        /// <summary>
        /// Gets or sets the update synchronize scopes.
        /// </summary>
        public string UpdateSyncScopes { get; set; }

        /// <summary>
        /// Gets or sets the number of previous passwords.
        /// </summary>
        public int NumberOfPreviousPasswordsToCheck { get; set; }

        /// <summary>
        /// Gets or sets the contact email used by the application to initiate email communication for the user.
        /// </summary>
        public string ContactEmail { get; set; }
    }
}
