﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.Business
{
    /// <summary>
    /// Contains the data check result performed by <see cref="EntityDataChecker"/> for an entity.
    /// </summary>
    public class EntityDataCheckResult
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="EntityDataCheckResult"/> class from being created.
        /// </summary>
        private EntityDataCheckResult()
        {
            this.Errors = new Collection<EntityDataCheckResult>();
            this.UnusedEntities = new Collection<object>();
            this.NullValueProperties = new Collection<string>();
            this.InvalidValueProperties = new Collection<string>();
            this.OverloadedMachines = new Collection<Machine>();
            this.YearlyProductionQtyErrors = new Collection<YearlyProductionQtyError>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDataCheckResult"/> class.
        /// </summary>
        /// <param name="entity">The object for which this instance represents a check result.</param>
        public EntityDataCheckResult(object entity)
            : this()
        {
            this.Entity = entity;
        }

        /// <summary>
        /// Gets or sets the entity whose missing data is recorded in this instance.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets the errors of the entity's sub-objects.
        /// </summary>
        public Collection<EntityDataCheckResult> Errors { get; private set; }

        /// <summary>
        /// Gets the entity objects (sub-parts and sub-assemblies) which are not used in the process of Entity.
        /// Populated only if the referenced entity is an Assembly.
        /// </summary>
        public Collection<object> UnusedEntities { get; private set; }

        /// <summary>
        /// Gets a list of strings denoting the properties of the object that were null when they shouldn't have been.
        /// </summary>
        public Collection<string> NullValueProperties { get; private set; }

        /// <summary>
        /// Gets a list of strings denoting the properties that have invalid values; e.g. a "0" value might cause division by zero errors.
        /// </summary>
        public Collection<string> InvalidValueProperties { get; private set; }

        /// <summary>
        /// Gets a list errors related to the entity's yearly production qty value (only for Assembly entities).
        /// </summary>
        public Collection<YearlyProductionQtyError> YearlyProductionQtyErrors { get; private set; }

        /// <summary>
        /// Gets the overloaded machines.
        /// </summary>        
        public Collection<Machine> OverloadedMachines { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance contains any data error, including any errors for sub-objects of the entity.
        /// </summary>        
        public bool HasErrors
        {
            get
            {
                return this.HasErrorsLvl1
                    || this.Errors.Count > 0
                    /*|| this.Errors.Any(e => e.HasErrors)*/;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance contains any data error related to the entity. Sub-object errors are not checked.
        /// </summary>
        public bool HasErrorsLvl1
        {
            get
            {
                return this.NullValueProperties.Count > 0
                    || this.InvalidValueProperties.Count > 0
                    || this.UnusedEntities.Count > 0
                    || this.OverloadedMachines.Count > 0
                    || this.YearlyProductionQtyErrors.Count > 0;
            }
        }

        /// <summary>
        /// Returns all errors in this instance's graph (including this error) in a flat list.
        /// </summary>
        /// <returns>A collection of errors.</returns>
        public Collection<EntityDataCheckResult> FlattenErrors()
        {
            if (!this.HasErrors)
            {
                return new Collection<EntityDataCheckResult>();
            }

            Collection<EntityDataCheckResult> flattenedErrorList = new Collection<EntityDataCheckResult>();

            // Add the result to the list only if it actually contains data errors for the entity.
            if (this.HasErrorsLvl1)
            {
                flattenedErrorList.Add(this);
            }

            foreach (var error in this.Errors)
            {
                flattenedErrorList.AddRange(error.FlattenErrors());
            }

            return flattenedErrorList;
        }
    }
}
