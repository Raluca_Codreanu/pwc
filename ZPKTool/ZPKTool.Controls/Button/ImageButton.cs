﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// A button that uses 3 images to represent its states: one for normal state, one for mouse-over and one for disabled.
    /// </summary>
    public class ImageButton : System.Windows.Controls.Button
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageNormalState.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ImageNormalStateProperty =
            DependencyProperty.Register("ImageNormalState", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageMouseoverState.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ImageMouseoverStateProperty =
            DependencyProperty.Register("ImageMouseoverState", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageDisabledState.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ImageDisabledStateProperty =
            DependencyProperty.Register("ImageDisabledState", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageAllStates.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ImageAllStatesProperty =
            DependencyProperty.Register("ImageAllStates", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null, ImageAllChangedCallback));

        /// <summary>
        /// Initializes static members of the <see cref="ImageButton"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static ImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageButton), new FrameworkPropertyMetadata(typeof(ImageButton)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageButton"/> class.
        /// </summary>
        public ImageButton()
        {
        }

        /// <summary>
        /// Gets or sets the image representing the normal state of the button.
        /// </summary>        
        public ImageSource ImageNormalState
        {
            get { return (ImageSource)GetValue(ImageNormalStateProperty); }
            set { SetValue(ImageNormalStateProperty, value); }
        }

        /// <summary>
        /// Gets or sets the image representing the "mouse-over" state of the button.
        /// </summary>        
        public ImageSource ImageMouseoverState
        {
            get { return (ImageSource)GetValue(ImageMouseoverStateProperty); }
            set { SetValue(ImageMouseoverStateProperty, value); }
        }

        /// <summary>
        /// Gets or sets the image representing the disabled state of the button.
        /// </summary>        
        public ImageSource ImageDisabledState
        {
            get { return (ImageSource)GetValue(ImageDisabledStateProperty); }
            set { SetValue(ImageDisabledStateProperty, value); }
        }

        /// <summary>
        /// Gets or sets the image used for all states of the button. Setting this property will sett all other image state properties to this value.
        /// </summary>
        public ImageSource ImageAllStates
        {
            get { return (ImageSource)GetValue(ImageAllStatesProperty); }
            set { SetValue(ImageAllStatesProperty, value); }
        }

        /// <summary>
        /// Callback invoked when the value of the ImageAll property has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ImageAllChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ImageButton button = d as ImageButton;
            if (button != null)
            {
                ImageSource newImage = e.NewValue as ImageSource;
                button.ImageNormalState = newImage;
                button.ImageMouseoverState = newImage;
                button.ImageDisabledState = newImage;
            }
        }
    }
}
