﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Split Button available modes
    /// </summary>
    public enum SplitButtonMode
    {
        /// <summary>
        /// In this Mode the button acts like a regular button.
        /// </summary>
        Button,

        /// <summary>
        /// In this Mode the button has two parts, a normal button and a dropdown which exposes the ContextMenu.
        /// </summary>
        Split,

        /// <summary>
        /// In this Mode the button acts like a combo box, clicking anywhere on the button opens the Context Menu.
        /// </summary>
        Dropdown
    }

    /// <summary>
    /// Implementation of a Split Button
    /// </summary>
    [TemplatePart(Name = "PART_DropDownButton", Type = typeof(Button))]
    [ContentProperty("Items")]
    [DefaultProperty("Items")]
    public class SplitButton : ZPKTool.Controls.Button
    {
        #region Dependency Properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for Mode. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ModeProperty = DependencyProperty.Register(
            "Mode",
            typeof(SplitButtonMode),
            typeof(SplitButton),
            new FrameworkPropertyMetadata(SplitButtonMode.Split));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsOpen. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register(
            "IsOpen",
            typeof(bool),
            typeof(SplitButton),
            new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsOpenChanged)));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Placement. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty PlacementProperty = ContextMenuService.PlacementProperty.AddOwner(
            typeof(SplitButton),
            new FrameworkPropertyMetadata(PlacementMode.Bottom, new PropertyChangedCallback(OnPlacementChanged)));

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="SplitButton"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static SplitButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SplitButton), new FrameworkPropertyMetadata(typeof(SplitButton)));
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating the Mode of operation of the Button (how the button acts)
        /// </summary>
        public SplitButtonMode Mode
        {
            get { return (SplitButtonMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the splitButton Context Menu is open or not. 
        /// </summary>
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Placement of the Context menu
        /// </summary>
        public PlacementMode Placement
        {
            get { return (PlacementMode)GetValue(PlacementProperty); }
            set { SetValue(PlacementProperty, value); }
        }

        /// <summary>
        /// Gets the Split Button's Items. Property that maps to the base classes ContextMenu.Items property
        /// </summary>
        public ItemCollection Items
        {
            get
            {
                this.ValidateContextMenu();
                return this.ContextMenu.Items;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// OnApplyTemplate override, set up the click event for the dropdown if present in the template
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // set up the click event handler for the main button
            ButtonBase actionButton = this.Template.FindName("PART_ActionButton", this) as ButtonBase;
            if (actionButton != null)
            {
                actionButton.Click += ActionButton_Click;
            }

            // set up the click event handler for the dropdown button
            ButtonBase dropDownButton = this.Template.FindName("PART_DropDownButton", this) as ButtonBase;
            if (dropDownButton != null)
            {
                dropDownButton.Click += this.DropDownButton_Click;
            }
        }

        #endregion Public Methods

        #region Event Handlers

        /// <summary>
        /// IsOpen Property changed callback, pass the value through to the buttons context menu
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsOpenChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SplitButton splitButton = (SplitButton)obj;
            if (splitButton == null)
            {
                return;
            }

            splitButton.ValidateContextMenu();
            if (!splitButton.ContextMenu.HasItems)
            {
                return;
            }

            bool value = (bool)e.NewValue;
            if (value && !splitButton.ContextMenu.IsOpen)
            {
                splitButton.ContextMenu.IsOpen = true;
            }
            else if (!value && splitButton.ContextMenu.IsOpen)
            {
                splitButton.ContextMenu.IsOpen = false;
            }
        }

        /// <summary>
        /// Placement Property changed callback, pass the value through to the buttons context menu
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnPlacementChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SplitButton splitButton = obj as SplitButton;
            if (splitButton == null)
            {
                return;
            }

            splitButton.ValidateContextMenu();
            splitButton.ContextMenu.Placement = (PlacementMode)e.NewValue;
        }

        /// <summary>
        /// Handles the Click event of the Action button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (this.Mode == SplitButtonMode.Dropdown)
            {
                this.HandleContextMenu();
            }
            else
            {
                this.OnClick();
            }
        }

        /// <summary>
        /// Handles the Click event of the Dropdown button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void DropDownButton_Click(object sender, RoutedEventArgs e)
        {
            this.HandleContextMenu();
            e.Handled = true;
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Check ContextMenu and open/close it
        /// </summary>
        private void HandleContextMenu()
        {
            this.ValidateContextMenu();
            if (!this.ContextMenu.HasItems)
            {
                return;
            }

            this.ContextMenu.IsOpen = !IsOpen;
        }

        /// <summary>
        /// Make sure the Context menu is not null
        /// </summary>
        private void ValidateContextMenu()
        {
            if (this.ContextMenu == null)
            {
                this.ContextMenu = new ContextMenu();
                this.ContextMenu.PlacementTarget = this;
                this.ContextMenu.Placement = this.Placement;

                this.ContextMenu.Opened += (sender, routedEventArgs) => this.IsOpen = true;
                this.ContextMenu.Closed += (sender, routedEventArgs) => this.IsOpen = false;
            }
        }

        #endregion Private Methods
    }
}