﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPKTool.Controls
{
    /// <summary>
    /// An image button that enlarges the image and applies a shadow when the mouse is over it.
    /// </summary>
    public class AnimatedImageButton : System.Windows.Controls.Button
    {                
        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageSource.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(AnimatedImageButton), new UIPropertyMetadata(null, ImageSourcePropertyChanged));
                
        /// <summary>
        /// Using a DependencyProperty as the backing store for DisableEnlargingEffect.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty DisableEnlargingEffectProperty =
            DependencyProperty.Register("DisableEnlargingEffect", typeof(bool), typeof(AnimatedImageButton), new UIPropertyMetadata(false));
                
        /// <summary>
        /// Using a DependencyProperty as the backing store for DisabledImage.  This enables animation, styling, binding, etc... 
        /// </summary>
        private static readonly DependencyProperty DisabledImageProperty =
            DependencyProperty.Register("DisabledImage", typeof(ImageSource), typeof(AnimatedImageButton), new UIPropertyMetadata(null));
                
        /// <summary>
        /// Using a DependencyProperty as the backing store for DisabledImageOpacityBrush.  This enables animation, styling, binding, etc... 
        /// </summary>
        private static readonly DependencyProperty DisabledImageOpacityBrushProperty =
            DependencyProperty.Register("DisabledImageOpacityBrush", typeof(ImageBrush), typeof(AnimatedImageButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes static members of the <see cref="AnimatedImageButton"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static AnimatedImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AnimatedImageButton), new FrameworkPropertyMetadata(typeof(AnimatedImageButton)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimatedImageButton"/> class.
        /// </summary>
        public AnimatedImageButton()
        {
        }

        /// <summary>
        /// Gets or sets the image to display on the button.
        /// </summary>        
        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disable enlarging effect applied to the button when the mouse is over it.
        /// </summary>        
        public bool DisableEnlargingEffect
        {
            get { return (bool)GetValue(DisableEnlargingEffectProperty); }
            set { SetValue(DisableEnlargingEffectProperty, value); }
        }

        /// <summary>
        /// Gets or sets the image used on the button when it is disabled.
        /// </summary>        
        private ImageSource DisabledImage
        {
            get { return (ImageSource)GetValue(DisabledImageProperty); }
            set { SetValue(DisabledImageProperty, value); }
        }

        /// <summary>
        /// Gets or sets the brush used as opacity mask when DisabledImage is used.
        /// </summary>        
        private ImageBrush DisabledImageOpacityBrush
        {
            get { return (ImageBrush)GetValue(DisabledImageOpacityBrushProperty); }
            set { SetValue(DisabledImageOpacityBrushProperty, value); }
        }

        /// <summary>
        /// Called when the ImageSource property has changed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ImageSourcePropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            AnimatedImageButton button = source as AnimatedImageButton;
            if (button != null)
            {
                BitmapSource newImage = e.NewValue as BitmapSource;
                if (newImage == null)
                {
                    button.DisabledImage = e.NewValue as ImageSource;
                }
                else
                {
                    button.DisabledImage = ConvertToBlackAndWhite(newImage);
                    button.DisabledImageOpacityBrush = new ImageBrush(newImage);
                }
            }
        }

        /// <summary>
        /// Converts an image to black and white.
        /// </summary>
        /// <param name="image">The image to convert.</param>
        /// <returns>The black and white version of <paramref name="image"/>.</returns>
        private static BitmapSource ConvertToBlackAndWhite(BitmapSource image)
        {
            if (image != null)
            {
                FormatConvertedBitmap blackAndWhiteImage = new FormatConvertedBitmap();
                blackAndWhiteImage.BeginInit();
                blackAndWhiteImage.Source = image;
                blackAndWhiteImage.DestinationFormat = PixelFormats.Gray32Float;
                blackAndWhiteImage.EndInit();
                return blackAndWhiteImage;
            }

            return null;
        }
    }
}
