﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The button used in the application, mostly with text content.
    /// This class was created to accomodate any future functionality without creating and integrating a new control.
    /// </summary>
    public class Button : System.Windows.Controls.Button
    {
        /// <summary>
        /// Initializes static members of the <see cref="Button"/> class.
        /// </summary>
        static Button()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Button), new FrameworkPropertyMetadata(typeof(Button)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
        public Button()
        {
        }
    }
}
