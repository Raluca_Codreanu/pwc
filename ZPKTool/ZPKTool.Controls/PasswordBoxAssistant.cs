﻿namespace ZPKTool.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using ZPKTool.Common;

    /// <summary>
    /// This class adds the possibility of data binding on the Password property of a PasswordBox and to specify a necessity level for it.
    /// </summary>
    public static class PasswordBoxAssistant
    {
        /// <summary>
        /// A property indicating whether to enable password binding to the binding set in the Password property.
        /// </summary>
        public static readonly DependencyProperty BindPassword = DependencyProperty.RegisterAttached(
            "BindPassword", typeof(bool), typeof(PasswordBoxAssistant), new PropertyMetadata(false, OnBindPasswordChanged));

        /// <summary>
        /// This property is used for storing the password of the password box to which it is attached.
        /// </summary>
        public static readonly DependencyProperty Password =
            DependencyProperty.RegisterAttached("Password", typeof(string), typeof(PasswordBoxAssistant), new PropertyMetadata(string.Empty, OnPasswordChanged));

        /// <summary>
        /// The password validation expression
        /// </summary>
        public static readonly DependencyProperty PasswordValidationExpression =
            DependencyProperty.RegisterAttached("PasswordValidationExpression", typeof(string), typeof(PasswordBoxAssistant), new PropertyMetadata(null));

        /// <summary>
        /// This property stores a bool value for password validation. 
        /// </summary>
        public static readonly DependencyProperty ValidatePassword =
            DependencyProperty.RegisterAttached("ValidatePassword", typeof(bool), typeof(PasswordBoxAssistant), new PropertyMetadata(false));

        /// <summary>
        /// This property stores a bool value for password validation for duplicate characters.
        /// </summary>
        public static readonly DependencyProperty ValidatePasswordForDuplicateAndSequenceOfCharacters =
            DependencyProperty.RegisterAttached("ValidatePasswordForDuplicateAndSequenceOfCharacters", typeof(bool), typeof(PasswordBoxAssistant), new PropertyMetadata(false));

        /// <summary>
        /// A property indicating the validation for the password of the password box to which it is attached.
        /// </summary>
        public static readonly DependencyProperty IsPasswordValid =
            DependencyProperty.RegisterAttached("IsPasswordValid", typeof(bool?), typeof(PasswordBoxAssistant), new PropertyMetadata(null));

        /// <summary>
        /// Sets a necessity level for the password box to which it is attached.
        /// </summary>
        public static readonly DependencyProperty Necessity = DependencyProperty.RegisterAttached(
            "Necessity",
            typeof(TextBoxControlNecessityLevel),
            typeof(PasswordBoxAssistant),
            new UIPropertyMetadata(TextBoxControlNecessityLevel.Optional));

        /// <summary>
        /// Internal property used to indicate whether the Password dependency property is being updated.
        /// </summary>
        private static readonly DependencyProperty UpdatingPassword =
            DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxAssistant), new PropertyMetadata(false));

        /// <summary>
        /// Called when the BindPassword property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnBindPasswordChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            // When the BindPassword attached property is set on a PasswordBox, start listening to its PasswordChanged event
            PasswordBox passwordBox = depObj as PasswordBox;
            if (passwordBox == null)
            {
                return;
            }

            bool wasBound = (bool)e.OldValue;
            bool needToBind = (bool)e.NewValue;

            if (wasBound)
            {
                passwordBox.PasswordChanged -= HandlePasswordChanged;
            }

            if (needToBind)
            {
                passwordBox.PasswordChanged += HandlePasswordChanged;
            }
        }

        /// <summary>
        /// Called when the Password property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnPasswordChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            PasswordBox passwordBox = depObj as PasswordBox;
            string input = passwordBox.Password;

            // Only handle this event when the property is attached to a PasswordBox and when the BindPassword attached property has been set to true.
            if (passwordBox == null || !GetBindPassword(depObj))
            {
                return;
            }

            if (GetValidatePassword(depObj))
            {
                if (string.IsNullOrWhiteSpace(input))
                {
                    SetIsPasswordValid(passwordBox, null);
                }
                else
                {
                    var isValid = true;
                    if (GetValidatePasswordForDuplicateAndSequenceOfCharacters(depObj))
                    {
                        isValid = Utils.CheckForDuplicateAndSequenceOfCharacters(input);
                    }

                    if (isValid)
                    {
                        Regex regularExpression = new Regex(GetPasswordValidationExpression(depObj));
                        isValid = regularExpression.Match(input).Success;
                    }

                    SetIsPasswordValid(passwordBox, isValid);
                }
            }

            // Avoid recursive updating by ignoring the password box's changed event
            passwordBox.PasswordChanged -= HandlePasswordChanged;

            if (!GetUpdatingPassword(passwordBox))
            {
                passwordBox.Password = e.NewValue as string;
            }

            passwordBox.PasswordChanged += HandlePasswordChanged;
        }

        /// <summary>
        /// Handles the PasswordChanged event of the PasswordBox to which this behavior is attached.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void HandlePasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox box = sender as PasswordBox;

            // Set a flag to indicate that we're updating the password
            SetUpdatingPassword(box, true);

            // Push the new password into the BoundPassword property.
            SetPassword(box, box.Password);

            // Set a flag to indicate that we're finished updating the password
            SetUpdatingPassword(box, false);
        }

        /// <summary>
        /// Gets the value of the BindPassword dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static bool GetBindPassword(DependencyObject depObj)
        {
            return (bool)depObj.GetValue(BindPassword);
        }

        /// <summary>
        /// Sets the value of the BindPassword dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetBindPassword(DependencyObject depObj, bool value)
        {
            depObj.SetValue(BindPassword, value);
        }

        /// <summary>
        /// Gets the value of the Password dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static string GetPassword(DependencyObject depObj)
        {
            return (string)depObj.GetValue(Password);
        }

        /// <summary>
        /// Sets the value of the Password dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetPassword(DependencyObject depObj, string value)
        {
            depObj.SetValue(Password, value);
        }

        /// <summary>
        /// Gets the value of the Necessity dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static TextBoxControlNecessityLevel GetNecessity(DependencyObject depObj)
        {
            return (TextBoxControlNecessityLevel)depObj.GetValue(Necessity);
        }

        /// <summary>
        /// Sets the value of the Necessity dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetNecessity(DependencyObject depObj, TextBoxControlNecessityLevel value)
        {
            depObj.SetValue(Necessity, value);
        }

        /// <summary>
        /// Gets the value of the IsValidPassword dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static bool? GetIsPasswordValid(DependencyObject depObj)
        {
            return (bool?)depObj.GetValue(IsPasswordValid);
        }

        /// <summary>
        /// Sets the value of the IsValidPassword dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object</param>
        /// <param name="value">The value to set.</param>
        public static void SetIsPasswordValid(DependencyObject depObj, bool? value)
        {
            depObj.SetValue(IsPasswordValid, value);
        }

        /// <summary>
        /// Gets the value of the ValidatePassword dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static bool GetValidatePassword(DependencyObject depObj)
        {
            return (bool)depObj.GetValue(ValidatePassword);
        }

        /// <summary>
        /// Sets the value of the ValidatePassword dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetValidatePassword(DependencyObject depObj, bool value)
        {
            depObj.SetValue(ValidatePassword, value);
        }

        /// <summary>
        /// Gets the value of the ValidatePasswordForDuplicateCharacters dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static bool GetValidatePasswordForDuplicateAndSequenceOfCharacters(DependencyObject depObj)
        {
            return (bool)depObj.GetValue(ValidatePasswordForDuplicateAndSequenceOfCharacters);
        }

        /// <summary>
        /// Sets the value of the ValidatePasswordForDuplicateCharacters dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetValidatePasswordForDuplicateAndSequenceOfCharacters(DependencyObject depObj, bool value)
        {
            depObj.SetValue(ValidatePasswordForDuplicateAndSequenceOfCharacters, value);
        }

        /// <summary>
        /// Gets the value of the PasswordValidationExpression dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        public static string GetPasswordValidationExpression(DependencyObject depObj)
        {
            return (string)depObj.GetValue(PasswordValidationExpression);
        }

        /// <summary>
        /// Sets the value of the PasswordValidationExpression dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetPasswordValidationExpression(DependencyObject depObj, string value)
        {
            depObj.SetValue(PasswordValidationExpression, value);
        }

        /// <summary>
        /// Gets the value of the UpdatingPassword dependency property from a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <returns>The value of the property.</returns>
        private static bool GetUpdatingPassword(DependencyObject depObj)
        {
            return (bool)depObj.GetValue(UpdatingPassword);
        }

        /// <summary>
        /// Sets the value of the UpdatingPassword dependency property on a specified dependency object.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        private static void SetUpdatingPassword(DependencyObject depObj, bool value)
        {
            depObj.SetValue(UpdatingPassword, value);
        }
    }
}
