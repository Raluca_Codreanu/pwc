﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Create Balloon decorator.
    /// </summary>
    public class BalloonDecorator : Decorator
    {
        /// <summary>
        /// The horizontal length of the pointer(horizontal gap between 
        /// the tip of the pointer and the left edge of the rounded rectangle). 
        /// </summary>
        private static double pointerLength = 15;

        /// <summary>
        /// Represents the background property of balloon decorator.
        /// </summary>
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.Register("Background", typeof(Brush), typeof(BalloonDecorator));

        /// <summary>
        /// Represents the border brush property of balloon decorator.
        /// </summary>
        public static readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.Register("BorderBrush", typeof(Brush), typeof(BalloonDecorator));

        /// <summary>
        /// Represents the border thickness property of balloon decorator.
        /// </summary>
        public static readonly DependencyProperty BorderThicknessProperty = DependencyProperty.Register(
            "BorderThickness",
            typeof(double),
            typeof(BalloonDecorator),
            new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        /// Represents the corner radius property of balloon decorator.
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            "CornerRadius",
            typeof(double),
            typeof(BalloonDecorator),
            new FrameworkPropertyMetadata(10.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        /// Gets or sets the background.
        /// </summary>
        /// <value>The background.</value>
        public Brush Background
        {
            get { return (Brush)GetValue(BackgroundProperty); }
            set { SetValue(BackgroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the border brush.
        /// </summary>
        /// <value>The border brush.</value>
        public Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the border thickness.
        /// </summary>
        /// <value>The border thickness.</value>
        public double BorderThickness
        {
            get { return (double)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }

        /// <summary>
        /// Gets or sets the corner radius.
        /// </summary>
        /// <value>The corner radius.</value>
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        /// <summary>
        /// Arranges the content of a <see cref="T:System.Windows.Controls.Decorator"/> element.
        /// </summary>
        /// <param name="arrangeSize">The <see cref="T:System.Windows.Size"/> this element uses to arrange its child content.</param>
        /// <returns>
        /// The <see cref="T:System.Windows.Size"/> that represents the arranged size of this <see cref="T:System.Windows.Controls.Decorator"/> element and its child.
        /// </returns>
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            UIElement child = Child;
            if (child != null)
            {
                Rect innerRect = Rect.Inflate(
                    new Rect(0, pointerLength, Math.Max(0, arrangeSize.Width - pointerLength), arrangeSize.Height - pointerLength),
                    0,
                    0);
                child.Arrange(innerRect);
            }

            return arrangeSize;
        }

        /// <summary>
        /// Measures the child element of a <see cref="T:System.Windows.Controls.Decorator"/> to prepare for arranging it during the <see cref="M:System.Windows.Controls.Decorator.ArrangeOverride(System.Windows.Size)"/> pass.
        /// </summary>
        /// <param name="constraint">An upper limit <see cref="T:System.Windows.Size"/> that should not be exceeded.</param>
        /// <returns>
        /// The target <see cref="T:System.Windows.Size"/> of the element.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            UIElement child = Child;
            Size size = new Size();
            if (child != null)
            {
                Size innerSize = new Size(Math.Max(0, constraint.Width - pointerLength), constraint.Height);
                child.Measure(innerSize);
                size.Width += child.DesiredSize.Width;
                size.Height += child.DesiredSize.Height;
            }

            Size borderSize = new Size(2 * BorderThickness, 2 * BorderThickness);
            size.Width += borderSize.Width + pointerLength;
            size.Height += borderSize.Height + pointerLength;
            return size;
        }

        /// <summary>
        /// Called when [render].
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element.
        /// This context is provided to the layout system.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect rect = new Rect(0, 0, RenderSize.Width, RenderSize.Height);
            drawingContext.PushClip(new RectangleGeometry(rect));
            drawingContext.DrawGeometry(Background, new Pen(BorderBrush, BorderThickness), CreateBalloonGeometry(rect));
            drawingContext.Pop();
        }

        /// <summary>
        /// Creates the balloon geometry.
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <returns> The created geometry.</returns>
        private Geometry CreateBalloonGeometry(Rect rect)
        {
            StreamGeometry geometry = new StreamGeometry();
            geometry.FillRule = FillRule.Nonzero;
            using (StreamGeometryContext context = geometry.Open())
            {
                context.BeginFigure(new Point(pointerLength, 0), true, true);
                if (pointerLength > 0)
                {
                    context.LineTo(new Point(pointerLength, pointerLength), true, false);
                    context.LineTo(new Point(2 * pointerLength, pointerLength), true, false);
                }
            }

            Rect rectangle = new Rect(new Point(1, pointerLength), new Point(rect.Width, pointerLength));
            rectangle.Height = rect.Height - pointerLength - 2;
            rectangle.Width = rect.Width - pointerLength;
            RectangleGeometry secondGeometry = new RectangleGeometry(rectangle, this.CornerRadius, this.CornerRadius);

            CombinedGeometry combinedGeometry = new CombinedGeometry(geometry, secondGeometry);

            return combinedGeometry;
        }
    }
}
