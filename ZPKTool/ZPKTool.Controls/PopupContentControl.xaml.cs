﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Interaction logic for PopupContentControl.xaml
    /// </summary>
    public partial class PopupContentControl : UserControl
    {
        /// <summary>
        /// The parent Popup control.
        /// </summary>
        private PopupControl parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="PopupContentControl"/> class.
        /// </summary>
        /// <param name="parent">The parent Popup control.</param>
        /// <param name="messageTypeImage">The image to display along the text content.</param>
        public PopupContentControl(PopupControl parent, ImageSource messageTypeImage)
        {
            InitializeComponent();
            this.parent = parent;            
            this.CloseImage.Source = new BitmapImage(new Uri("Resources/Images/X_black.png", UriKind.Relative));
            this.BaloonDecorator.Background = new SolidColorBrush(Color.FromRgb(252, 234, 178));
            if (messageTypeImage != null)
            {
                this.MessageTypeImage.Source = messageTypeImage;
            }
        }

        /// <summary>
        /// Event raised when the close button is clicked.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            parent.IsOpen = false;
        }

        /// <summary>
        /// Event raised when the mouse is over the close button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void CloseImage_MouseEnter(object sender, MouseEventArgs e)
        {
            CloseImage.Opacity = 1;
        }

        /// <summary>
        /// Event raised when the mouse leaves the close button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void CloseImage_MouseLeave(object sender, MouseEventArgs e)
        {
            CloseImage.Opacity = 0.6;
        }
    }
}
