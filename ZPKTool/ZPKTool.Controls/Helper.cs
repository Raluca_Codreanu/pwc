﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Helper class containing some useful, generic methods
    /// </summary>
    internal static class Helper
    {
        /// <summary>
        /// Focus an UI element. If calling the Focus() method of the element doe not work then use this method.
        /// </summary>
        /// <param name="element">The element.</param>
        public static void FocusElement(UIElement element)
        {
            if (element != null)
            {
                bool focused = element.Focus();
                if (!focused)
                {
                    Action action = () => { element.Focus(); };
                    element.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input, action);
                }
            }
        }

        /// <summary>
        /// Analyzes both visual and logical tree in order to find all elements of a given type that are
        /// descendants of the <paramref name="source"/> item.
        /// </summary>
        /// <typeparam name="T">The type of the queried items.</typeparam>
        /// <param name="source">The root element that marks the source of the search. If the
        /// source is already of the requested type, it will not be included in the result.</param>
        /// <returns>All descendants of <paramref name="source"/> that match the requested type.</returns>
        public static IEnumerable<T> FindChildren<T>(this DependencyObject source) where T : DependencyObject
        {
            if (source != null)
            {
                var children = GetChildObjects(source);
                foreach (DependencyObject child in children)
                {
                    // analyze if children match the requested type
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    // recurse tree
                    foreach (T descendant in FindChildren<T>(child))
                    {
                        yield return descendant;
                    }
                }
            }
        }

        /// <summary>
        /// This method is an alternative to WPF's <see cref="VisualTreeHelper.GetChild"/> method, which also
        /// supports content elements. Keep in mind that for content elements, this method falls back to the
        /// logical tree of the element.
        /// </summary>
        /// <param name="parent">The item to be processed.</param>
        /// <returns>The submitted item's child elements, if available.</returns>
        public static IEnumerable<DependencyObject> GetChildObjects(this DependencyObject parent)
        {
            if (parent == null)
            {
                yield break;
            }

            ContentElement contentElement = parent as ContentElement;

            if (contentElement != null)
            {
                // use the logical tree for content elements
                foreach (object obj in LogicalTreeHelper.GetChildren(contentElement))
                {
                    var depObj = obj as DependencyObject;
                    if (depObj != null)
                    {
                        yield return (DependencyObject)obj;
                    }
                }
            }
            else
            {
                // use the visual tree per default
                int count = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < count; i++)
                {
                    yield return VisualTreeHelper.GetChild(parent, i);
                }
            }
        }

        /// <summary>
        /// Focuses the 1st text box control belonging to the specified parent control.
        /// </summary>
        /// <param name="parent">The parent control.</param>
        public static void FocusFirstTextbox(object parent)
        {
            DependencyObject depObj = parent as DependencyObject;
            if (depObj == null)
            {
                return;
            }

            TextBoxControl firstTextbox = Helper.FindChildren<TextBoxControl>(depObj).FirstOrDefault();
            if (firstTextbox != null)
            {
                Helper.FocusElement(firstTextbox);
            }
        }

        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the
        /// queried item.</param>
        /// <returns>The first parent item that matches the submitted
        /// type parameter. If not matching item can be found, a null
        /// reference is being returned.</returns>
        public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            // get parent item
            DependencyObject parentObject = GetParentObject(child);

            // we've reached the end of the tree
            if (parentObject == null)
            {
                return null;
            }

            // check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                // use recursion to proceed with next level
                return TryFindParent<T>(parentObject);
            }
        }

        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Do note, that for content element,
        /// this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise
        /// null.</returns>
        public static DependencyObject GetParentObject(DependencyObject child)
        {
            if (child == null)
            {
                return null;
            }

            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null)
                {
                    return parent;
                }

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            // if it's not a ContentElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }
    }
}
