﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Controls.Languages;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class represents a wizard, a succession of content controls accessed by using next and previous commands or clicking the step headers.
    /// </summary>
    [ContentProperty("Steps")]
    public class Wizard : Control
    {
        /// <summary>
        /// The "Back" button.
        /// </summary>
        private Button back;

        /// <summary>
        /// The "Next" button.
        /// </summary>
        private Button next;

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedStep.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty SelectedStepProperty =
            DependencyProperty.Register("SelectedStep", typeof(WizardStep), typeof(Wizard), new UIPropertyMetadata(null, SelectedStepChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ExtraButtons.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ButtonsProperty =
            DependencyProperty.Register("Buttons", typeof(Collection<System.Windows.Controls.Button>), typeof(Wizard), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for StepHeaders.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty StepHeadersProperty =
            DependencyProperty.Register("StepHeaders", typeof(List<WizardStepHeader>), typeof(Wizard), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes static members of the <see cref="Wizard"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static Wizard()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Wizard), new FrameworkPropertyMetadata(typeof(Wizard)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Wizard"/> class.
        /// </summary>
        public Wizard()
        {
            this.HorizontalContentAlignment = HorizontalAlignment.Left;
            this.VerticalContentAlignment = VerticalAlignment.Top;

            this.Steps = new Collection<WizardStep>();
            this.StepHeaders = new List<WizardStepHeader>();
            this.Buttons = new Collection<System.Windows.Controls.Button>();
        }

        /// <summary>
        /// Gets the wizard steps belonging to this instance. This is the Content property of this control.
        /// </summary>        
        public Collection<WizardStep> Steps { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected step. This is a dependency property.
        /// </summary>        
        public WizardStep SelectedStep
        {
            get { return (WizardStep)GetValue(SelectedStepProperty); }
            set { SetValue(SelectedStepProperty, value); }
        }

        /// <summary>
        /// Gets or sets the extra buttons that appear to the left of the Back and Next buttons. This is a dependency property.
        /// </summary>        
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "This is a dependency property and needs a public setter.")]
        public Collection<System.Windows.Controls.Button> Buttons
        {
            get { return (Collection<System.Windows.Controls.Button>)GetValue(ButtonsProperty); }
            set { SetValue(ButtonsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the step headers. This is a dependency property.
        /// </summary>        
        private List<WizardStepHeader> StepHeaders
        {
            get { return (List<WizardStepHeader>)GetValue(StepHeadersProperty); }
            set { SetValue(StepHeadersProperty, value); }
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            InitializeStepHeaders();

            base.OnApplyTemplate();

            this.back = this.Template.FindName("PART_BackButton", this) as Button;
            if (this.back != null)
            {
                this.back.Click += new RoutedEventHandler(BackButton_Click);
            }

            this.next = this.Template.FindName("PART_NextButton", this) as Button;
            if (this.next != null)
            {
                this.next.Click += new RoutedEventHandler(NextButton_Click);
            }

            RefreshNavButtonsState();
        }

        /// <summary>
        /// Called when the value of the SelectedStep dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SelectedStepChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Un-select the previously selected step.
            WizardStep unselectedStep = e.OldValue as WizardStep;
            if (unselectedStep != null && unselectedStep.Header != null)
            {
                unselectedStep.Header.IsSelected = false;
            }

            // Select the current step.
            WizardStep selectedStep = e.NewValue as WizardStep;
            if (selectedStep != null && selectedStep.Header != null)
            {
                selectedStep.Header.IsSelected = true;
            }

            Wizard wizard = d as Wizard;
            if (wizard != null)
            {
                wizard.RefreshNavButtonsState();
                if (wizard.SelectedStep != null)
                {
                    Helper.FocusFirstTextbox(wizard.SelectedStep.Content);
                }
            }
        }

        /// <summary>
        /// Initializes the step headers based on the steps collection.
        /// </summary>
        private void InitializeStepHeaders()
        {
            foreach (WizardStep step in this.Steps)
            {
                WizardStepHeader header = new WizardStepHeader();
                header.StepName = step.StepName;
                header.Margin = new Thickness(-6, 0, 0, 0);
                header.Click += StepHeader_Click;
                this.StepHeaders.Add(header);
                step.Header = header;
            }

            this.SelectedStep = this.Steps.FirstOrDefault(s => s.IsEnabled);
        }

        /// <summary>
        /// Determines whether a "Back" action can be performed.
        /// </summary>
        /// <returns> true if the action can be performed; otherwise, false.</returns>
        private bool CanGoBack()
        {
            // Skip all steps that are not enabled.
            for (int i = this.Steps.IndexOf(this.SelectedStep) - 1; i >= 0; i--)
            {
                if (this.Steps[i].IsEnabled)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether a "Next" action can be performed.
        /// </summary>
        /// <returns> true if the action can be performed; otherwise, false.</returns>
        private bool CanGoNext()
        {
            // Skip all steps that are not enabled.
            for (int i = this.Steps.IndexOf(this.SelectedStep) + 1; i < this.Steps.Count; i++)
            {
                if (this.Steps[i].IsEnabled)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Refreshes the state of the navigation buttons (Back and Next).
        /// </summary>
        private void RefreshNavButtonsState()
        {
            if (this.back != null)
            {
                this.back.IsEnabled = CanGoBack();
            }

            if (this.next != null)
            {
                this.next.IsEnabled = CanGoNext();
            }
        }

        /// <summary>
        /// Handles the Click event of a StepHeader control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void StepHeader_Click(object sender, EventArgs e)
        {
            var header = sender as WizardStepHeader;

            // Select the step corresponding to the clicked header.
            WizardStep step = this.Steps.FirstOrDefault(s => s.Header == header);
            if (step != null)
            {
                this.SelectedStep = step;
            }
        }

        /// <summary>
        /// Handles the Click event of the Back button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedStep == null)
            {
                return;
            }

            // Skip all steps that are not enabled.
            for (int i = this.Steps.IndexOf(this.SelectedStep) - 1; i >= 0; i--)
            {
                if (this.Steps[i].IsEnabled)
                {
                    this.SelectedStep = this.Steps[i];
                    break;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the Next button.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedStep == null)
            {
                return;
            }

            // Skip all steps that are not enabled.
            for (int i = this.Steps.IndexOf(this.SelectedStep) + 1; i < this.Steps.Count; i++)
            {
                if (this.Steps[i].IsEnabled)
                {
                    this.SelectedStep = this.Steps[i];
                    break;
                }
            }
        }
    }
}
