﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Navigation;
using ZPKTool.Common;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class extends <see cref="System.Windows.Controls.TextBlock"/> to provide new functionality.
    /// </summary>
    public class LabelControl : TextBlock
    {
        /// <summary>
        /// The pattern for the email address validation.
        /// </summary>
        private const string PatternEMAIL = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";

        /// <summary>
        /// The pattern for the URI validation.
        /// </summary>
        private const string PatternURI = @"^((http://|https://|ftp://|www.))([\w+?\.\w+])+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";

        /// <summary>
        /// Reference to the text property changed notification
        /// </summary>
        private readonly DependencyPropertyChangeNotifier textChangedNotifier;

        /// <summary>
        /// Reference to the fontSize property changed notification
        /// </summary>
        private readonly DependencyPropertyChangeNotifier fontSizeChangedNotifier;

        /// <summary>
        /// The original (not the calculated and set) font size of this control.
        /// </summary>
        private double originalFontSize;

        /// <summary>
        /// Indicates whether to ignore the next font size changed event or not.
        /// </summary>
        private bool ignoreFontSizeChangedEvent;

        /// <summary>
        /// Indicates whether the value needs to be set internally.
        /// </summary>
        private bool isValueSetInternally;

        /// <summary>
        /// Initializes static members of the <see cref="LabelControl"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static LabelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LabelControl), new FrameworkPropertyMetadata(typeof(LabelControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelControl"/> class.
        /// </summary>
        public LabelControl()
        {
            textChangedNotifier = new DependencyPropertyChangeNotifier(this, TextProperty);
            textChangedNotifier.ValueChanged += OnTextChanged;

            fontSizeChangedNotifier = new DependencyPropertyChangeNotifier(this, FontSizeProperty);
            fontSizeChangedNotifier.ValueChanged += OnFontSizeChanged;

            originalFontSize = FontSize;

            this.Loaded += this.LabelControlLoaded;
            this.SizeChanged += (s, e) => this.AdjustFontSizeToFitContent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabelControl"/> class.
        /// </summary>
        /// <param name="text">The text to be set.</param>
        public LabelControl(string text)
            : this()
        {
            this.Text = text;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to auto adjust the font size to fit the content. This is a dependency property.
        /// </summary>        
        public bool AutoAdjustFontSizeToFitContent
        {
            get { return (bool)GetValue(AutoAdjustFontSizeToFitContentProperty); }
            set { SetValue(AutoAdjustFontSizeToFitContentProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for AutoAdjustFontSizeToFitContent.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty AutoAdjustFontSizeToFitContentProperty =
            DependencyProperty.Register("AutoAdjustFontSizeToFitContent", typeof(bool), typeof(LabelControl), new UIPropertyMetadata(false, AutoAdjustFontSizeToFitContentChanged));

        /// <summary>
        /// Gets or sets the minimum font size at which the auto adjusting algorithm will stop. This is a dependency property.
        /// </summary>
        public double AutoAdjustMinFontSize
        {
            get { return (double)GetValue(AutoAdjustMinFontSizeProperty); }
            set { SetValue(AutoAdjustMinFontSizeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for AutoAdjustMinFontSize.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty AutoAdjustMinFontSizeProperty =
            DependencyProperty.Register("AutoAdjustMinFontSize", typeof(double), typeof(LabelControl), new UIPropertyMetadata(9d, AutoAdjustMinFontSizeChanged));

        /// <summary>
        /// Auto adjustment min font size property changed.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AutoAdjustMinFontSizeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LabelControl labelControl = obj as LabelControl;
            if (labelControl != null)
            {
                labelControl.AdjustFontSizeToFitContent();
            }
        }

        /// <summary>
        /// Auto adjustment font size to fit content property changed.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AutoAdjustFontSizeToFitContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LabelControl labelControl = obj as LabelControl;
            if (labelControl != null)
            {
                labelControl.AdjustFontSizeToFitContent();
            }
        }

        /// <summary>
        /// Handles the Loaded event of the LabelControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void LabelControlLoaded(object sender, RoutedEventArgs e)
        {
            AdjustFontSizeToFitContent();
        }

        /// <summary>
        /// Called when the label's text has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnTextChanged(object sender, EventArgs args)
        {
            // If the text is a valid URI creates a hyperlink to make the text to be clickable and also navigable.
            if (!this.isValueSetInternally && this.CheckIfValidURI(this.Text))
            {
                this.isValueSetInternally = true;

                Hyperlink hyperlink = new Hyperlink(this.Inlines.FirstInline);
                hyperlink.NavigateUri = new Uri(this.Text, UriKind.RelativeOrAbsolute);
                hyperlink.RequestNavigate += Hyperlink_RequestNavigate;

                this.Inlines.Clear();
                this.Inlines.Add(hyperlink);

                this.isValueSetInternally = false;
            }

            AdjustFontSizeToFitContent();
        }

        /// <summary>
        /// Handles the RequestNavigate event of the Hyperlink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RequestNavigateEventArgs"/> instance containing the event data.</param>
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            if (e.Uri.ToString().Contains("@"))
            {
                Process.Start(new ProcessStartInfo(string.Format("mailto:{0}", e.Uri.OriginalString)));
            }
            else
            {
                Process.Start(new ProcessStartInfo(e.Uri.OriginalString));
            }

            e.Handled = true;
        }

        /// <summary>
        /// Called when [font size changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnFontSizeChanged(object sender, EventArgs eventArgs)
        {
            if (!ignoreFontSizeChangedEvent)
            {
                this.originalFontSize = this.FontSize;
            }
        }

        /// <summary>
        /// Checks if the text is a valid EMAIL address or a valid URL.
        /// </summary>
        /// <param name="text">The text to check.</param>
        /// <returns>True if the text is a link, false otherwise.</returns>
        private bool CheckIfValidURI(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return false;
            }

            // Check if the text is a valid EMAIL address.
            if (Regex.IsMatch(text, PatternEMAIL, RegexOptions.IgnoreCase))
            {
                return true;
            }

            // Check if the text is a valid URL.
            if (Regex.IsMatch(text, PatternURI, RegexOptions.IgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Adjusts the font size of the control so the text fits in it.
        /// </summary>
        private void AdjustFontSizeToFitContent()
        {
            if (!string.IsNullOrWhiteSpace(this.Text) && this.AutoAdjustFontSizeToFitContent && this.ActualHeight > 0 && this.ActualWidth > 0)
            {
                try
                {
                    this.ignoreFontSizeChangedEvent = true;
                    this.FontSize = CalculateMaximumFontSize(this, this.AutoAdjustMinFontSize, 1, this.originalFontSize);
                }
                finally
                {
                    this.ignoreFontSizeChangedEvent = false;
                }
            }
        }

        /// <summary>
        /// Calculates a maximum font size that will fit in a given space.
        /// </summary>
        /// <param name="textblock">The text block to calculate the font size for.</param>
        /// <param name="minimumFontSize">The minimum size the font is capped at</param>
        /// <param name="reductionStep">The step to de-increment font size with. A higher step is less expensive, whereas a lower step sizes font with greater accuracy</param>
        /// <param name="originalFontSize">Size of the original font.</param>
        /// <returns>
        /// The calculated maximum font size
        /// </returns>
        public static double CalculateMaximumFontSize(
            TextBlock textblock,
            double minimumFontSize,
            double reductionStep,
            double originalFontSize)
        {
            var fontSize = originalFontSize;

            // holds formatted text - Culture info is setup for current UI culture, this can be changed for different language sets
            FormattedText formattedText = new FormattedText(
                textblock.Text,
                Thread.CurrentThread.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(textblock.FontFamily, textblock.FontStyle, textblock.FontWeight, textblock.FontStretch),
                fontSize,
                textblock.Foreground);

            // hold the maximum internal space allocation as an absolute value
            var maximumInternalWidth = Math.Round(textblock.ActualWidth - (textblock.Padding.Left + textblock.Padding.Right), 2);

            // if measured font is too big for container size, with regard for internal margin
            if (Math.Round(formattedText.WidthIncludingTrailingWhitespace, 2) > maximumInternalWidth)
            {
                do
                {
                    // reduce font size by step
                    fontSize -= reductionStep;

                    // set font size ready for re-measure
                    formattedText.SetFontSize(fontSize);
                }
                while ((formattedText.WidthIncludingTrailingWhitespace > maximumInternalWidth) && (fontSize > minimumFontSize));
            }

            // return calculated font size. Note that it might be the original one if there was no need to re-calculate it.
            return fontSize;
        }
    }
}
