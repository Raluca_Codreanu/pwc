﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Generates the color palette used by charts for coloring their items.
    /// </summary>
    public static class ChartPalette
    {
        /// <summary>
        /// The seed used to initialize the instance of <see cref="System.Random"/> class used internally.
        /// </summary>
        private const int RandomSeed = 50;

        /// <summary>
        /// The random number generator used for generating random colors.
        /// </summary>
        private static Random random = new Random(RandomSeed);

        /// <summary>
        /// The number of colors that have been generated.
        /// </summary>
        private static int index = 1;

        /// <summary>
        /// Resets the palette.
        /// </summary>
        public static void ResetPalette()
        {
            random = new Random(RandomSeed);
            index = 1;
        }

        /// <summary>
        /// Gets the next random color brush.
        /// </summary>
        /// <returns>A gradient brush.</returns>
        public static RadialGradientBrush GenerateNextRandomColorBrush()
        {
            RadialGradientBrush radialBrush = new RadialGradientBrush();
            radialBrush.GradientOrigin = new Point(-0.1, -0.1);
            radialBrush.Center = new Point(0.075, 0.015);
            radialBrush.RadiusX = 1.05;
            radialBrush.RadiusY = 0.9;
            GradientStopCollection gradientStops = new GradientStopCollection();
            Color color1;
            Color color2;

            switch (index)
            {
                case 1:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFB9D6F7");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF284B70");
                    break;
                case 2:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFFBB7B5");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF702828");
                    break;
                case 3:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFB8C0AC");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF5F7143");
                    break;
                case 4:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFFDE79C");
                    color2 = (Color)ColorConverter.ConvertFromString("#FFF6BC0C");
                    break;
                case 5:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFA9A3BD");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF382C6C");
                    break;
                case 6:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFB1A1B1");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF50224F");
                    break;
                case 7:
                    color1 = (Color)ColorConverter.ConvertFromString("#FF9DC2B3");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF1D7554");
                    break;
                case 8:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFB5B5B5");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF4C4C4C");
                    break;
                case 9:
                    color1 = (Color)ColorConverter.ConvertFromString("#FF98C1DC");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF0271AE");
                    break;
                case 10:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFC1C0AE");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF706E41");
                    break;
                case 11:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFADBDC0");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF446A73");
                    break;
                case 12:
                    color1 = (Color)ColorConverter.ConvertFromString("#FF2F8CE2");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF0C3E69");
                    break;
                case 13:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFDCDCDC");
                    color2 = (Color)ColorConverter.ConvertFromString("#FF757575");
                    break;
                case 14:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFF4F4F4");
                    color2 = (Color)ColorConverter.ConvertFromString("#FFB7B7B7");
                    break;
                case 15:
                    color1 = (Color)ColorConverter.ConvertFromString("#FFF4F4F4");
                    color2 = (Color)ColorConverter.ConvertFromString("#FFA3A3A3");
                    break;
                default:
                    int red = (byte)random.Next(200);
                    int green = (byte)random.Next(200);
                    int blue = (byte)random.Next(200);
                    color1 = Color.FromRgb((byte)red, (byte)green, (byte)blue);
                    red = red + 40;
                    green = green + 40;
                    blue = blue + 40;
                    color2 = Color.FromRgb((byte)red, (byte)green, (byte)blue);
                    break;
            }

            index++;

            gradientStops.Add(new GradientStop(color1, 0));
            gradientStops.Add(new GradientStop(color2, 1));
            radialBrush.GradientStops = gradientStops;
            radialBrush.Freeze();

            return radialBrush;
        }
    }
}
