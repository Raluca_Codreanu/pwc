﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Represents a control that contains a data series to be rendered in waterfall format.
    /// </summary>
    /// <QualityBand>Preview</QualityBand>
    [SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance", Justification = "Depth of hierarchy is necessary to avoid code duplication.")]
    [StyleTypedProperty(Property = DataPointStyleName, StyleTargetType = typeof(WaterfallDataPoint))]
    [StyleTypedProperty(Property = "LegendItemStyle", StyleTargetType = typeof(LegendItem))]
    [TemplatePart(Name = DataPointSeries.PlotAreaName, Type = typeof(Canvas))]
    public class WaterfallSeries : ColumnBarBaseSeries<WaterfallDataPoint>
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for HasTotalValue.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty HasTotalValueProperty =
            DependencyProperty.Register("HasTotalValue", typeof(bool), typeof(WaterfallSeries), new PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a value indicating whether the last data point from chart's collection represents the total value or not
        /// </summary>
        public bool HasTotalValue
        {
            get { return (bool)GetValue(HasTotalValueProperty); }
            set { SetValue(HasTotalValueProperty, value); }
        }

        /// <summary>
        /// Gets the waterfall data points
        /// </summary>
        public IEnumerable<DataPoint> ChartDataPoints
        {
            get
            {
                return this.ActiveDataPoints;
            }
        }

        /// <summary>
        /// Acquire a horizontal category axis and a vertical linear axis.
        /// </summary>
        /// <param name="firstDataPoint">The first data point.</param>
        protected override void GetAxes(DataPoint firstDataPoint)
        {
            GetAxes(
                firstDataPoint,
                (axis) => axis.Orientation == AxisOrientation.X,
                () => new CategoryAxis { Orientation = AxisOrientation.X },
                (axis) =>
                {
                    IRangeAxis rangeAxis = axis as IRangeAxis;
                    return rangeAxis != null && rangeAxis.Origin != null && axis.Orientation == AxisOrientation.Y;
                },
                () =>
                {
                    IRangeAxis rangeAxis = CreateRangeAxisFromData(firstDataPoint.DependentValue);
                    rangeAxis.Orientation = AxisOrientation.Y;
                    if (rangeAxis == null || rangeAxis.Origin == null)
                    {
                        throw new InvalidOperationException("No suitable axis is available for plotting the dependent value.");
                    }

                    DisplayAxis axis = rangeAxis as DisplayAxis;
                    if (axis != null)
                    {
                        axis.ShowGridLines = true;
                    }

                    return rangeAxis;
                });
        }

        /// <summary>
        /// Updates each point location, width and height
        /// </summary>
        /// <param name="dataPoint">The data point to update.</param>
        protected override void UpdateDataPoint(DataPoint dataPoint)
        {
            if (SeriesHost == null)
            {
                return;
            }

            object category = dataPoint.ActualIndependentValue ?? (IndexOf(this.ActiveDataPoints, dataPoint) + 1);
            Range<UnitValue> coordinateRange = GetCategoryRange(category);

            if (!coordinateRange.HasData)
            {
                return;
            }
            else if (coordinateRange.Maximum.Unit != Unit.Pixels || coordinateRange.Minimum.Unit != Unit.Pixels)
            {
                throw new InvalidOperationException("This series does not support radial axes.");
            }

            double minimum = coordinateRange.Minimum.Value;
            double maximum = coordinateRange.Maximum.Value;

            double zeroPointY = ActualDependentRangeAxis.GetPlotAreaCoordinate(ActualDependentRangeAxis.Origin).Value;
            double dataPointY = 0;
            double basePointY = 0;
            var waterfallDataPoint = dataPoint as WaterfallDataPoint;
            if (waterfallDataPoint != null)
            {
                basePointY = ActualDependentRangeAxis.GetPlotAreaCoordinate(waterfallDataPoint.BaseBound).Value;
                dataPointY = ActualDependentRangeAxis.GetPlotAreaCoordinate(Convert.ToDouble(dataPoint.ActualDependentValue, CultureInfo.InvariantCulture)).Value + basePointY - zeroPointY;
            }
            else
            {
                basePointY = zeroPointY;
                dataPointY = ActualDependentRangeAxis.GetPlotAreaCoordinate(Convert.ToDouble(dataPoint.ActualDependentValue, CultureInfo.InvariantCulture)).Value;
            }

            double plotAreaHeight = ActualDependentRangeAxis.GetPlotAreaCoordinate(ActualDependentRangeAxis.Range.Maximum).Value;
            IEnumerable<WaterfallSeries> columnSeries = SeriesHost.Series.OfType<WaterfallSeries>().Where(series => series.ActualIndependentAxis == ActualIndependentAxis);
            int numberOfSeries = columnSeries.Count();
            double coordinateRangeWidth = maximum - minimum;
            double segmentWidth = coordinateRangeWidth * 0.8;
            double columnWidth = segmentWidth / numberOfSeries;
            int seriesIndex = IndexOf(columnSeries, this);

            double offset = (seriesIndex * Math.Round(columnWidth)) + (coordinateRangeWidth * 0.1);
            double dataPointX = minimum + offset;

            if (GetIsDataPointGrouped(category))
            {
                // Multiple DataPoints share this category; offset and overlap them appropriately
                IGrouping<object, DataPoint> categoryGrouping = GetDataPointGroup(category);
                int index = IndexOf(categoryGrouping, dataPoint);
                dataPointX += (index * (columnWidth * 0.2)) / (categoryGrouping.Count() - 1);
                columnWidth *= 0.8;
                Canvas.SetZIndex(dataPoint, -index);
            }

            if (CanGraph(dataPointY) && CanGraph(dataPointX) && CanGraph(basePointY))
            {
                dataPoint.Visibility = Visibility.Visible;

                double left = Math.Round(dataPointX);
                double width = Math.Round(columnWidth);

                double top = Math.Round(plotAreaHeight - Math.Max(dataPointY, basePointY) + 0.5);
                double bottom = Math.Round(plotAreaHeight - Math.Min(dataPointY, basePointY) + 0.5);
                double height = bottom - top + 1;

                Canvas.SetLeft(dataPoint, left);
                Canvas.SetTop(dataPoint, top);
                dataPoint.Width = width;
                dataPoint.Height = height;
            }
            else
            {
                dataPoint.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary> 
        /// Prepares a data point by extracting binding it to a data context object.
        /// </summary>
        /// <param name="dataPoint">The data point.</param>
        /// <param name="dataContext">The data context object.</param>
        protected override void PrepareDataPoint(DataPoint dataPoint, object dataContext)
        {
            // If the chart item doesn't affect the cost then it will be displayed on the same level
            var waterfallDataPoint = dataPoint as WaterfallDataPoint;
            var chartItem = dataContext as LabeledPieChartItem;
            if (waterfallDataPoint != null && chartItem != null)
            {
                waterfallDataPoint.IsDataPointInline = chartItem.IsNotAffectingCost;
            }

            base.PrepareDataPoint(dataPoint, dataContext);
        }

        /// <summary>
        /// Ensure that if any data points are updated, all data points are updated.
        /// Computes the base and top bound of each data points and draws the lines between each data point
        /// </summary>
        /// <param name="dataPoints">The data points to update.</param>
        protected override void UpdateDataPoints(IEnumerable<DataPoint> dataPoints)
        {
            // Compute the base bound and the top bound values
            double auxValue = 0;
            foreach (WaterfallDataPoint point in dataPoints)
            {
                if (point.IsDataPointInline)
                {
                    point.BaseBound = auxValue - Convert.ToDouble(point.ActualDependentValue, CultureInfo.InvariantCulture);
                    point.TopBound = auxValue;
                }
                else
                {
                    point.BaseBound = auxValue;
                    point.TopBound = Convert.ToDouble(point.ActualDependentValue, CultureInfo.InvariantCulture) + auxValue;
                    auxValue += Convert.ToDouble(point.ActualDependentValue, CultureInfo.InvariantCulture);
                }
            }

            if (this.HasTotalValue)
            {
                var lastDataPoint = dataPoints.LastOrDefault() as WaterfallDataPoint;
                if (lastDataPoint != null)
                {
                    lastDataPoint.BaseBound = 0;
                    lastDataPoint.TopBound = Convert.ToDouble(lastDataPoint.ActualDependentValue, CultureInfo.InvariantCulture);
                }
            }

            base.UpdateDataPoints(dataPoints);

            // Draw the lines between data points
            var waterfallPoints = dataPoints.OfType<WaterfallDataPoint>();
            var plotArea = GetTemplateChild(PlotAreaName) as Panel;

            var dataPointsCount = waterfallPoints.Count();
            var lineList = plotArea.Children.OfType<Line>().ToList();
            foreach (var line in lineList)
            {
                plotArea.Children.Remove(line);
            }

            double plotAreaHeight = 0;

            if (ActualDependentRangeAxis != null)
            {
                plotAreaHeight = ActualDependentRangeAxis.GetPlotAreaCoordinate(ActualDependentRangeAxis.Range.Maximum).Value;

                if (dataPointsCount > 1)
                {
                    for (int i = 1; i < dataPointsCount; i++)
                    {
                        // get first data point
                        var firstDataPoint = waterfallPoints.ElementAtOrDefault(i - 1);

                        // get second data point
                        var secondDataPoint = waterfallPoints.ElementAtOrDefault(i);

                        Line line = new Line();
                        line.Visibility = Visibility.Visible;
                        line.StrokeThickness = 1;
                        line.StrokeDashArray = new DoubleCollection(new List<double> { 2, 2 });
                        line.Stroke = Brushes.Black;

                        if (double.IsNaN(firstDataPoint.Width))
                        {
                            firstDataPoint.Width = 0d;
                        }

                        double dataPointX1 = Canvas.GetLeft(firstDataPoint) + firstDataPoint.Width;
                        double dataPointX2 = Canvas.GetLeft(secondDataPoint);

                        line.X1 = dataPointX1;
                        line.X2 = dataPointX2;
                        line.Y2 = Math.Round(plotAreaHeight - ActualDependentRangeAxis.GetPlotAreaCoordinate(firstDataPoint.TopBound).Value + 1.5);
                        line.Y1 = line.Y2;

                        plotArea.Children.Add(line);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the margins required for each value.
        /// Its used to compute the axes range to catch all data points in chart
        /// The maximum value is given by the biggest TopBound data point
        /// </summary>
        /// <param name="consumer">The consumer to return the value margins for.</param>
        /// <returns>A sequence of margins for each value.</returns>
        protected override IEnumerable<ValueMargin> GetValueMargins(IValueMarginConsumer consumer)
        {
            double dependentValueMargin = this.ActualHeight / 10;
            IAxis axis = consumer as IAxis;
            var waterfallDataPoints = ActiveDataPoints.OfType<WaterfallDataPoint>();

            // compute the base bound and the top bound values
            double auxValue = 0;
            foreach (WaterfallDataPoint point in waterfallDataPoints)
            {
                point.BaseBound = auxValue;
                point.TopBound = Convert.ToDouble(point.ActualDependentValue, CultureInfo.InvariantCulture) + auxValue;
                auxValue += Convert.ToDouble(point.ActualDependentValue, CultureInfo.InvariantCulture);
            }

            if (axis != null && ActiveDataPoints.Any())
            {
                Func<WaterfallDataPoint, IComparable> selector = null;
                if (axis == InternalActualIndependentAxis)
                {
                    selector = (dataPoint) => (IComparable)dataPoint.ActualIndependentValue;

                    var minimumPoint = MinOrNull(waterfallDataPoints, selector);
                    var maximumPoint = MaxOrNull(waterfallDataPoints, selector);

                    double minimumMargin = GetMargin(minimumPoint, axis);
                    yield return new ValueMargin(selector(minimumPoint), minimumMargin, minimumMargin);

                    double maximumMargin = GetMargin(maximumPoint, axis);
                    yield return new ValueMargin(selector(maximumPoint), maximumMargin, maximumMargin);
                }
                else if (axis == InternalActualDependentAxis)
                {
                    selector = (dataPoint) => (IComparable)dataPoint.TopBound;

                    var minimumPoint = MinOrNull(waterfallDataPoints, selector);
                    var maximumPoint = MaxOrNull(waterfallDataPoints, selector);

                    yield return new ValueMargin(selector(minimumPoint), dependentValueMargin, dependentValueMargin);
                    yield return new ValueMargin(selector(maximumPoint), dependentValueMargin, dependentValueMargin);
                }
            }
            else
            {
                yield break;
            }
        }

        /// <summary>
        /// Returns the index of an item in a sequence.
        /// </summary>
        /// <param name="that">The sequence.</param>
        /// <param name="value">The item to search for.</param>
        /// <returns>The index of the item or -1 if not found.</returns>
        private static int IndexOf(IEnumerable that, object value)
        {
            int index = 0;
            foreach (object item in that)
            {
                if (object.ReferenceEquals(value, item) || value.Equals(item))
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        /// <summary>
        /// Returns a value indicating whether this value can be graphed on a linear axis.
        /// </summary>
        /// <param name="value">The value to evaluate.</param>
        /// <returns>A value indicating whether this value can be graphed on a 
        /// linear axis.</returns>
        private static bool CanGraph(double value)
        {
            return !double.IsNaN(value) && !double.IsNegativeInfinity(value) && !double.IsPositiveInfinity(value) && !double.IsInfinity(value);
        }

        /// <summary>
        /// Returns the margin for a given framework element and axis.
        /// </summary>
        /// <param name="element">The framework element.</param>
        /// <param name="axis">The axis along which to return the margin.
        /// </param>
        /// <returns>The margin for a given framework element and axis.
        /// </returns>
        private static double GetMargin(FrameworkElement element, IAxis axis)
        {
            double length = 0.0;
            if (axis.Orientation == AxisOrientation.X)
            {
                length = !double.IsNaN(element.Width) ? element.Width : element.ActualWidth;
            }
            else if (axis.Orientation == AxisOrientation.Y)
            {
                length = !double.IsNaN(element.Height) ? element.Height : element.ActualHeight;
            }

            return length / 2.0;
        }

        /// <summary>
        /// Returns the maximum value in the stream based on the result of a
        /// project function.
        /// </summary>
        /// <typeparam name="T">The stream type.</typeparam>
        /// <param name="that">The stream.</param>
        /// <param name="projectionFunction">The function that transforms the
        /// item.</param>
        /// <returns>The maximum value or null.</returns>
        private static T MaxOrNull<T>(IEnumerable<T> that, Func<T, IComparable> projectionFunction)
            where T : class
        {
            IComparable result = null;
            T maximum = default(T);
            if (!that.Any())
            {
                return maximum;
            }

            maximum = that.First();
            result = projectionFunction(maximum);
            foreach (T item in that.Skip(1))
            {
                IComparable currentResult = projectionFunction(item);
                if (result.CompareTo(currentResult) < 0)
                {
                    result = currentResult;
                    maximum = item;
                }
            }

            return maximum;
        }

        /// <summary>
        /// Returns the minimum value in the stream based on the result of a project function.
        /// </summary>
        /// <typeparam name="T">The stream type.</typeparam>
        /// <param name="that">The stream.</param>
        /// <param name="projectionFunction">The function that transforms the
        /// item.</param>
        /// <returns>The minimum value or null.</returns>
        private static T MinOrNull<T>(IEnumerable<T> that, Func<T, IComparable> projectionFunction)
            where T : class
        {
            IComparable result = null;
            T minimum = default(T);
            if (!that.Any())
            {
                return minimum;
            }

            minimum = that.First();
            result = projectionFunction(minimum);
            foreach (T item in that.Skip(1))
            {
                IComparable currentResult = projectionFunction(item);
                if (result.CompareTo(currentResult) > 0)
                {
                    result = currentResult;
                    minimum = item;
                }
            }

            return minimum;
        }
    }
}
