﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class serves as a data input for the chart
    /// </summary>
    public class LabeledPieChartItem : INotifyPropertyChanged
    {
        #region Members

        /// <summary>
        /// The Name property of the enumMemberName.
        /// </summary>
        private string itemName;

        /// <summary>
        /// The value property of the enumMemberName.
        /// </summary>
        private decimal? itemValue;

        /// <summary>
        /// The additional info property of enumMemberName.
        /// </summary>
        private decimal itemAdditionalInfo;

        /// <summary>
        /// Indicates whether the current item is primary in the group or not.
        /// </summary>
        private bool isPrimaryInGroup;

        /// <summary>
        /// The color of the item.
        /// </summary>
        private RadialGradientBrush itemColor;

        /// <summary>
        /// Indicates if the item should be displayed on chart
        /// </summary>
        private bool displayOnChart;

        #endregion

        /// <summary>
        /// Prevents a default instance of the <see cref="LabeledPieChartItem"/> class from being created.
        /// </summary>
        private LabeledPieChartItem()
        {
            this.FontWeight = FontWeights.Normal;
            this.GroupID = -1;
            this.IsPrimaryInGroup = false;
            this.DisplayAdditionalInfo = false;
            this.Id = Guid.NewGuid();
            this.IsNotAffectingCost = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabeledPieChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="id">The unique identifier</param>
        public LabeledPieChartItem(string name, decimal value, Guid id)
            : this(name, value, true)
        {
            this.Id = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabeledPieChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        public LabeledPieChartItem(string name, decimal value)
            : this(name, value, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabeledPieChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="groupID">The group ID.</param>
        public LabeledPieChartItem(string name, decimal value, int groupID)
            : this(name, value, true)
        {
            this.GroupID = groupID;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabeledPieChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="displayAdditionalInfo">if set to <c>true</c> display additional info item, otherwise don't.</param>
        /// <param name="additonalInfo">The additonal info of the item.</param>
        /// <param name="groupID">The group ID.</param>
        public LabeledPieChartItem(string name, decimal value, bool displayAdditionalInfo, decimal additonalInfo, int groupID)
            : this(name, value, groupID)
        {
            this.ItemAdditionalInfo = additonalInfo;
            this.DisplayAdditionalInfo = displayAdditionalInfo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LabeledPieChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="displayOnChart">If set to <c>true</c> the item will be displayed on chart.</param>
        public LabeledPieChartItem(string name, decimal? value, bool displayOnChart)
            : this()
        {
            this.ItemName = name;
            this.ItemValue = value;
            this.DisplayOnChart = displayOnChart;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this item should be displayed on chart.
        /// </summary>        
        public bool DisplayOnChart
        {
            get
            {
                return displayOnChart;
            }

            set
            {
                displayOnChart = value;

                if (value == true)
                {
                    this.ItemColor = ChartPalette.GenerateNextRandomColorBrush();
                }
                else
                {
                    this.ItemColor = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [display additional info].
        /// </summary>
        public bool DisplayAdditionalInfo { get; set; }

        /// <summary>
        /// Gets or sets the group ID.
        /// </summary>
        /// <value>The group ID.</value>
        public int GroupID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is primary in group.
        /// </summary>
        public bool IsPrimaryInGroup
        {
            get
            {
                return this.isPrimaryInGroup;
            }

            set
            {
                this.isPrimaryInGroup = value;
                this.FontWeight = value ? FontWeights.Bold : FontWeights.Normal;
            }
        }

        /// <summary>
        /// Gets or sets the name of the item.
        /// </summary>
        /// <value>The name of the item.</value>
        public string ItemName
        {
            get
            {
                return this.itemName;
            }

            set
            {
                this.itemName = value;
                RaisePropertyChangeEvent("ItemName");
            }
        }

        /// <summary>
        /// Gets or sets the item value.
        /// </summary>
        /// <value>The item value.</value>
        public decimal? ItemValue
        {
            get
            {
                return this.itemValue;
            }

            set
            {
                this.itemValue = value;
                RaisePropertyChangeEvent("ItemValue");
            }
        }

        /// <summary>
        /// Gets or sets the color of the item.
        /// </summary>
        /// <value>The color of the item.</value>
        public RadialGradientBrush ItemColor
        {
            get
            {
                return this.itemColor;
            }

            set
            {
                if (this.itemColor != value)
                {
                    this.itemColor = value;
                    RaisePropertyChangeEvent("ItemColor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the item additional info
        /// (e.g OH Rate and Margin Rate for Overhead section).
        /// </summary>
        /// <value>The item additional info.</value>
        public decimal ItemAdditionalInfo
        {
            get
            {
                return this.itemAdditionalInfo;
            }

            set
            {
                this.itemAdditionalInfo = value;
                RaisePropertyChangeEvent("ItemAdditionalInfo");
            }
        }

        /// <summary>
        /// Gets or sets the font weight.
        /// </summary>
        public FontWeight FontWeight { get; set; }

        /// <summary>
        /// Gets or sets the unique identifier of an item 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current item cost doesn't affect the entire system cost 
        /// </summary>
        public bool IsNotAffectingCost { get; set; }

        #endregion

        /// <summary>
        /// When a property has been changed, this method notifies the gui to update the contents
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
