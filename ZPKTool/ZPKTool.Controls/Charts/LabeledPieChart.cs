﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Implements the Pie Chart that can display labels associated with its slices.
    /// </summary>
    [TemplatePart(Name = "LabelArea_PART", Type = typeof(PieChartLabelArea))]
    public class LabeledPieChart : Chart
    {
        /// <summary>
        /// Initializes static members of the <see cref="LabeledPieChart"/> class.
        /// </summary>
        static LabeledPieChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LabeledPieChart), new FrameworkPropertyMetadata(typeof(LabeledPieChart)));
        }
    }
}
