﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Canvas where all labels (of type PieChartLabel) will be added. We need a separate
    /// canvas to avoid z-order issues. This prevents having a pie slice overlap a label.
    /// </summary>
    public class PieChartLabelArea : Canvas
    {
        /// <summary>
        /// The dependency property backing the HasSmallArc property.
        /// </summary>
        public static readonly DependencyProperty HasSmallArcProperty =
            DependencyProperty.Register("HasSmallArc", typeof(bool), typeof(PieChartLabelArea), new PropertyMetadata(false, OnHasSmallArcChanged));

        /// <summary>
        /// Gets or sets a value indicating whether at least one pie chart slice is considered to be small 
        /// (it represents a percentage out of the total pie that is less than the PieChartLabel.SmallItemPercentage constant).
        /// </summary>
        /// <remarks>
        /// This property is used in the "Auto" mode. When in this mode, if this property is set to
        /// true, all labels will be connected to the chart by a line.
        /// </remarks>
        public bool HasSmallArc
        {
            get { return (bool)this.GetValue(HasSmallArcProperty); }
            set { this.SetValue(HasSmallArcProperty, value); }
        }

        /// <summary>
        /// Called when The HasSmallArc property has changed.
        /// </summary>
        /// <param name="obj">The object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnHasSmallArcChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            // When the HasSmallArc property is set, all labels should be re-arranged.
            PieChartLabelArea labelArea = obj as PieChartLabelArea;
            if (labelArea != null)
            {
                foreach (PieChartLabel label in labelArea.Children.OfType<PieChartLabel>())
                {
                    label.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Measures the child elements of a <see cref="T:System.Windows.Controls.Canvas" /> in anticipation of arranging them during the <see cref="M:System.Windows.Controls.Canvas.ArrangeOverride(System.Windows.Size)" /> pass.
        /// </summary>
        /// <param name="constraint">An upper limit <see cref="T:System.Windows.Size" /> that should not be exceeded.</param>
        /// <returns>
        /// A <see cref="T:System.Windows.Size" /> that represents the size that is required to arrange child content.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            // On measure, we check if there is at least one child of the canvas that is small, and we set HasSmallArc to reflect that information. 
            // Setting HasSmallArc ends up causing all labels to be re-arranged taking this information into consideration.            
            bool hasSmallArc = false;

            IEnumerable<PieChartLabel> children = this.Children.OfType<PieChartLabel>();
            foreach (PieChartLabel label in children)
            {
                if (label.IsArcSmall)
                {
                    hasSmallArc = true;
                    break;
                }
            }

            this.HasSmallArc = hasSmallArc;

            return base.MeasureOverride(constraint);
        }
    }
}
