﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class represents the data point of a waterfall chart
    /// </summary>
    public class WaterfallDataPoint : ColumnDataPoint
    {
        /// <summary>
        /// Gets or sets a value representing the bottom location of data point on chart
        /// </summary>
        public double BaseBound { get; set; }

        /// <summary>
        /// Gets or sets a value representing the top location of data point on chart
        /// </summary>
        public double TopBound { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data point is on the same level as previous item or not
        /// Used when having data points that not affect the total value in any way but is needed to be shown
        /// </summary>
        public bool IsDataPointInline { get; set; }
    }
}
