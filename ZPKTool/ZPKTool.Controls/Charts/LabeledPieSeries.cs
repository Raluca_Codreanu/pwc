﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Represents a pie chart series that has labels associates with the pie slices.
    /// </summary>
    public class LabeledPieSeries : PieSeries
    {
        /// <summary>
        /// The Dependency Property backing the PieChartLabelStyle property.
        /// </summary>
        public static readonly DependencyProperty PieChartLabelStyleProperty =
            DependencyProperty.Register("PieChartLabelStyle", typeof(Style), typeof(LabeledPieSeries));

        /// <summary>
        /// The Dependency Property backing the PieChartLabelItemTemplate property.
        /// </summary>
        public static readonly DependencyProperty PieChartLabelItemTemplateProperty =
            DependencyProperty.Register("PieChartLabelItemTemplate", typeof(DataTemplate), typeof(LabeledPieSeries));

        /// <summary>
        /// The Dependency Property backing the LabelDisplayMode property.
        /// </summary>
        public static readonly DependencyProperty LabelDisplayModeProperty =
            DependencyProperty.Register("LabelDisplayMode", typeof(DisplayMode), typeof(LabeledPieSeries), new PropertyMetadata(DisplayMode.AutoMixed));

        /// <summary>
        /// The Dependency Property backing the ActualLegendItemStyle property.
        /// </summary>
        public static readonly DependencyProperty ActualLegendItemStyleProperty =
            DependencyProperty.Register("ActualLegendItemStyle", typeof(Style), typeof(LabeledPieSeries));

        /// <summary>
        /// Gets or sets the style to be applied to the Pie Chart Labels. This is a Dependency Property.
        /// </summary>
        public Style PieChartLabelStyle
        {
            get { return (Style)this.GetValue(PieChartLabelStyleProperty); }
            set { this.SetValue(PieChartLabelStyleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the template to be applied to each PieChartLabel. This is a Dependency Property.
        /// </summary>
        public DataTemplate PieChartLabelItemTemplate
        {
            get { return (DataTemplate)this.GetValue(PieChartLabelItemTemplateProperty); }
            set { this.SetValue(PieChartLabelItemTemplateProperty, value); }
        }

        /// <summary>
        /// Gets or sets the display mode to be applied to each PieChartLabel.  This is a Dependency Property.
        /// </summary>
        public DisplayMode LabelDisplayMode
        {
            get { return (DisplayMode)this.GetValue(LabelDisplayModeProperty); }
            set { this.SetValue(LabelDisplayModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the actual legend item style. This is a dependency property.
        /// <para/>
        /// This property seems to be used internally, so it was defined to avoid a binding error, but is never set.
        /// </summary>
        public Style ActualLegendItemStyle
        {
            get { return (Style)GetValue(ActualLegendItemStyleProperty); }
            set { SetValue(ActualLegendItemStyleProperty, value); }
        }

        /// <summary>
        /// Adds the labels to each of its PieDataPoints.
        /// </summary>
        protected override void OnAfterUpdateDataPoints()
        {
            LabeledPieChart chart = this.SeriesHost as LabeledPieChart;
            if (chart != null)
            {
                Canvas labelArea = chart.Template.FindName("LabelArea_PART", chart) as Canvas;
                if (labelArea != null && this.ItemsSource != null)
                {
                    foreach (object dataItem in this.ItemsSource)
                    {
                        PieDataPoint pieDataPoint = this.GetDataPoint(dataItem) as PieDataPoint;

                        if (pieDataPoint != null)
                        {
                            this.AddLabelPieDataPoint(pieDataPoint, labelArea);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when the data points collection has changed.
        /// </summary>
        /// <param name="newDataPoints">The new data points.</param>
        /// <param name="oldDataPoints">The old data points.</param>
        protected override void OnDataPointsChanged(IList<DataPoint> newDataPoints, IList<DataPoint> oldDataPoints)
        {
            LabeledPieChart chart = this.SeriesHost as LabeledPieChart;
            if (chart != null)
            {
                Canvas labelArea = chart.Template.FindName("LabelArea_PART", chart) as Canvas;
                if (labelArea != null)
                {
                    foreach (DataPoint oldDataPoint in oldDataPoints)
                    {
                        labelArea.Children.Remove(labelArea.Children.OfType<PieChartLabel>().FirstOrDefault(l => l.ParentPieDataPoint == oldDataPoint));
                    }
                }
            }

            base.OnDataPointsChanged(newDataPoints, oldDataPoints);
        }

        /// <summary>
        /// Called when the SelectedItem property has changed.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        protected override void OnSelectedItemPropertyChanged(object oldValue, object newValue)
        {
            base.OnSelectedItemPropertyChanged(oldValue, newValue);

            PieDataPoint oldSelectedPieDataPoint = null;
            PieDataPoint newSelectedPieDataPoint = null;

            if (oldValue != null)
            {
                oldSelectedPieDataPoint = this.GetDataPoint(oldValue) as PieDataPoint;
            }

            if (newValue != null)
            {
                newSelectedPieDataPoint = this.GetDataPoint(newValue) as PieDataPoint;
            }

            if (newSelectedPieDataPoint != null)
            {
                newSelectedPieDataPoint.Background = new SolidColorBrush(Colors.Red);
            }

            if (oldSelectedPieDataPoint != null)
            {
                if (oldValue is LabeledPieChartItem)
                {
                    oldSelectedPieDataPoint.Background = (oldValue as LabeledPieChartItem).ItemColor;
                }
            }
        }

        /// <summary>
        /// Creates a new PieChartLabel control and adds it to the "LabelArea_PART" canvas.
        /// </summary>
        /// <param name="pieDataPoint">PieDataPoint that corresponds to PieChartLabel.</param>
        /// <param name="labelArea">The canvas where PieChartLabel will be added.</param>
        private void AddLabelPieDataPoint(PieDataPoint pieDataPoint, Canvas labelArea)
        {
            PieChartLabel label = new PieChartLabel();
            label.Style = this.PieChartLabelStyle;
            label.ContentTemplate = this.PieChartLabelItemTemplate;

            Binding contentBinding = new Binding("DataContext") { Source = pieDataPoint };
            label.SetBinding(ContentControl.ContentProperty, contentBinding);

            Binding dataContextBinding = new Binding("DataContext") { Source = pieDataPoint };
            label.SetBinding(ContentControl.DataContextProperty, dataContextBinding);

            Binding formattedRatioBinding = new Binding("FormattedRatio") { Source = pieDataPoint };
            label.SetBinding(PieChartLabel.FormattedRatioProperty, formattedRatioBinding);

            Binding visibilityBinding = new Binding("Ratio") { Source = pieDataPoint, Converter = new DoubleToVisibilityConverter() };
            label.SetBinding(PieChartLabel.VisibilityProperty, visibilityBinding);

            Binding geometryBinding = new Binding("Geometry") { Source = pieDataPoint };
            label.SetBinding(PieChartLabel.GeometryProperty, geometryBinding);

            Binding displayModeBinding = new Binding("LabelDisplayMode") { Source = this };
            label.SetBinding(PieChartLabel.DisplayModeProperty, displayModeBinding);
            label.ParentPieDataPoint = pieDataPoint;

            labelArea.Children.Add(label);

            pieDataPoint.Unloaded += delegate
            {
                labelArea.Children.Remove(label);
            };

            pieDataPoint.Loaded += delegate
            {
                if (label.Parent == null)
                {
                    labelArea.Children.Add(label);
                }
            };
        }
    }
}
