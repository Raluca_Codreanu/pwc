﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Modes used to control how the labels are positioned.
    /// </summary>
    public enum DisplayMode
    {
        /// <summary>
        /// Center of the label is positioned at the arc midpoint.
        /// </summary>
        ArcMidpoint,

        /// <summary>
        /// A line connecting the arc midpoint and label is displayed.
        /// </summary>
        Connected,

        /// <summary>
        /// If at least one pie slice is very small, all pie slices use the Connected display mode. Otherwise, they all use ArcMidpoint.
        /// </summary>
        Auto,

        /// <summary>
        /// Connected display mode is used for all small pie slices, and ArcMidpoint is used for all other slices.
        /// </summary>
        AutoMixed
    }

    /// <summary>
    /// This is the control that holds the label. It includes an optional line connecting it to the chart, and whatever
    /// content the developer specifies in the DataTemplate.
    /// </summary>
    [TemplatePart(Name = "Canvas_PART", Type = typeof(Canvas))]
    [TemplatePart(Name = "Content_PART", Type = typeof(ContentPresenter))]
    public class PieChartLabel : ContentControl
    {
        /// <summary>
        /// Indicates the minimum pie slice value for which the label is displayed.
        /// </summary>
        private const int SmallItemPercentage = 2;

        /// <summary>
        /// The content part of this control.
        /// </summary>
        private ContentPresenter contentPart;

        /// <summary>
        /// The canvas part of this control.
        /// </summary>
        private Canvas canvasPart;

        /// <summary>
        /// The center point of the pie chart.
        /// </summary>
        private Point center;

        /// <summary>
        /// Initializes static members of the <see cref="PieChartLabel" /> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static PieChartLabel()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PieChartLabel), new FrameworkPropertyMetadata(typeof(PieChartLabel)));
        }

        /// <summary>
        /// The dependency property backing the FormattedRatio property.
        /// </summary>
        public static readonly DependencyProperty FormattedRatioProperty = PieDataPoint.FormattedRatioProperty.AddOwner(typeof(PieChartLabel), null);

        /// <summary>
        /// Gets or sets the percentage of the total pie chart that the associated pie chart slice represents.
        /// When a PieChartLabel is created, this property is data bound to the PieDataPoint's equivalent property. It will typically be used within the DataTemplate for PieChartLabel.
        /// </summary>
        public string FormattedRatio
        {
            get { return (string)this.GetValue(FormattedRatioProperty); }
            set { this.SetValue(FormattedRatioProperty, value); }
        }

        /// <summary>
        /// The dependency property backing the Geometry property.
        /// </summary>
        public static readonly DependencyProperty GeometryProperty = PieDataPoint.GeometryProperty.AddOwner(typeof(PieChartLabel), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsParentMeasure | FrameworkPropertyMetadataOptions.AffectsArrange, OnGeometryChanged));

        /// <summary>
        /// Gets or sets the geometry of the PieDataPoint to which this instance is associated.
        /// When a PieChartLabel is created, this property is data bound to the PieDataPoint's equivalent property. We need this information to calculate the position of the label.
        /// </summary>
        public Geometry Geometry
        {
            get { return (Geometry)this.GetValue(GeometryProperty); }
            set { this.SetValue(GeometryProperty, value); }
        }

        /// <summary>
        /// The dependency property backing the DisplayMode property.
        /// </summary>
        public static readonly DependencyProperty DisplayModeProperty =
            DependencyProperty.Register("DisplayMode", typeof(DisplayMode), typeof(PieChartLabel), new FrameworkPropertyMetadata(DisplayMode.ArcMidpoint, FrameworkPropertyMetadataOptions.AffectsArrange));

        /// <summary>
        /// Gets or sets the display mode for this instance. See the <see cref="DisplayMode"/> enumeration for details on the available options.
        /// </summary>
        public DisplayMode DisplayMode
        {
            get { return (DisplayMode)this.GetValue(DisplayModeProperty); }
            set { this.SetValue(DisplayModeProperty, value); }
        }
                
        /// <summary>
        /// Gets or sets the brush of the line that links this label to its associated pie slice when in connected mode.
        /// </summary>
        public Brush LineStroke
        {
            get { return (Brush)this.GetValue(LineStrokeProperty); }
            set { this.SetValue(LineStrokeProperty, value); }
        }

        /// <summary>
        /// The dependency property backing the LineStroke property.
        /// </summary>
        public static readonly DependencyProperty LineStrokeProperty =
            DependencyProperty.Register("LineStroke", typeof(Brush), typeof(PieChartLabel), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));

        /// <summary>
        /// Gets or sets the thickness of the line that links this label to its associated pie slice when in connected mode.
        /// </summary>
        public double LineStrokeThickness
        {
            get { return (double)this.GetValue(LineStrokeThicknessProperty); }
            set { this.SetValue(LineStrokeThicknessProperty, value); }
        }

        /// <summary>
        /// The dependency property backing the LineStrokeThickness property.
        /// </summary>
        public static readonly DependencyProperty LineStrokeThicknessProperty =
            DependencyProperty.Register("LineStrokeThickness", typeof(double), typeof(PieChartLabel), new PropertyMetadata(1.0));

        /// <summary>
        /// Gets or sets the midpoint of the arc in the pie wedge.
        /// </summary>
        public Point ArcMidpoint { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the arc of the pie wedge is small.
        /// </summary>
        public bool IsArcSmall { get; set; }

        /// <summary>
        /// Gets or sets the parent pie data point.
        /// </summary>
        /// <value>The parent pie data point.</value>
        public PieDataPoint ParentPieDataPoint { get; set; }

        /// <summary>
        /// Called when the Geometry property changed.
        /// </summary>
        /// <param name="obj">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnGeometryChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PieChartLabel label = obj as PieChartLabel;
            if (label != null)
            {
                bool isArcSmall;
                Point arcMidpoint;
                PieChartHelper.GetPieChartInfo(e.NewValue as Geometry, out label.center, out arcMidpoint, out isArcSmall);
                label.IsArcSmall = isArcSmall;
                label.ArcMidpoint = arcMidpoint;
            }
        }

        /// <summary>
        /// Called to arrange and size the content of a <see cref="T:System.Windows.Controls.Control" /> object.
        /// </summary>
        /// <param name="arrangeBounds">The computed size that is used to arrange the content.</param>
        /// <returns>
        /// The size of the control.
        /// </returns>
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this.PositionLabel();
            return base.ArrangeOverride(arrangeBounds);
        }

        /// <summary>
        /// When the template is applied, get the template parts. Also, ensure that if the 
        /// content of the label changes, the label is re-positioned.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            contentPart = this.GetTemplateChild("Content_PART") as ContentPresenter;
            canvasPart = this.GetTemplateChild("Canvas_PART") as Canvas;

            if (contentPart != null)
            {
                contentPart.SizeChanged += delegate(object sender, SizeChangedEventArgs e)
                {
                    this.PositionLabel();
                };
            }
        }

        /// <summary>
        /// Positions the label depending on the display mode specified by the developer.
        /// </summary>
        private void PositionLabel()
        {
            switch (this.DisplayMode)
            {
                case DisplayMode.ArcMidpoint:
                    this.PositionArcMidpoint();
                    break;
                case DisplayMode.Connected:
                    this.PositionConnected();
                    break;
                case DisplayMode.AutoMixed:
                    this.PositionAutoMixed();
                    break;
                case DisplayMode.Auto:
                    this.PositionAuto();
                    break;
            }
        }

        /// <summary>
        /// Positions the label with its center in the same location as the midpoint of the pie slice arc.
        /// </summary>
        private void PositionArcMidpoint()
        {
            this.RemovePolyline();

            if (this.contentPart != null)
            {
                Canvas.SetTop(this.contentPart, this.ArcMidpoint.Y - (0.5 * this.contentPart.DesiredSize.Height));
                Canvas.SetLeft(this.contentPart, this.ArcMidpoint.X - (0.5 * this.contentPart.DesiredSize.Width));
            }
        }

        /// <summary>
        /// Adds a line that connects the arc midpoint to the label and positions the label appropriately.
        /// Ideally, I would add the Polyline in the template and I would only change its points here. Unfortunately,
        /// because of a WPF bug, the Polyline doesn't render in certain corner-case scenarios. As a workaround,
        /// I create a new Polyline everytime the label is positioned, as can be seen in this method.
        /// </summary>
        private void PositionConnected()
        {
            this.RemovePolyline();
            string s = this.FormattedRatio.Replace("%", string.Empty).Trim();
            if (double.Parse(s) > SmallItemPercentage)
            {
                if (this.contentPart != null)
                {
                    PointCollection newPoints = new PointCollection();

                    // First point
                    newPoints.Add(this.SnapPoint(this.ArcMidpoint));

                    // Second point
                    Vector radialDirection = this.ArcMidpoint - this.center;
                    radialDirection.Normalize();
                    Point secondPoint = this.ArcMidpoint + (radialDirection * 10);
                    newPoints.Add(this.SnapPoint(secondPoint));

                    // Third point
                    int sign = Math.Sign(radialDirection.X); // 1 if label is on the right side, -1 if it's on the left.
                    Point thirdPoint = secondPoint + new Vector(sign * 15, 0);
                    newPoints.Add(this.SnapPoint(thirdPoint));

                    double contentX = (sign == 1) ? thirdPoint.X : thirdPoint.X - this.contentPart.DesiredSize.Width;
                    double contentY = thirdPoint.Y - (0.5 * this.contentPart.DesiredSize.Height);
                    Canvas.SetTop(this.contentPart, contentY);
                    Canvas.SetLeft(this.contentPart, contentX);

                    Polyline polyline = new Polyline();
                    polyline.Points = newPoints;
                    polyline.SetBinding(Polyline.StrokeThicknessProperty, new Binding("LineStrokeThickness") { Source = this });
                    polyline.SetBinding(Polyline.StrokeProperty, new Binding("LineStroke") { Source = this });
                    polyline.StrokeLineJoin = PenLineJoin.Round;

                    this.canvasPart.Children.Add(polyline);
                }
            }
            else
            {
                this.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Removes the Polyline from the canvas part.
        /// </summary>
        private void RemovePolyline()
        {
            if (this.canvasPart != null)
            {
                Polyline polyline = canvasPart.Children.OfType<Polyline>().FirstOrDefault();
                if (polyline != null)
                {
                    canvasPart.Children.Remove(polyline);
                }
            }
        }

        /// <summary>
        /// Positions the label in the arc midpoint if it's big enough, and displays it connected by a line if it's
        /// small.
        /// </summary>
        private void PositionAutoMixed()
        {
            if (this.IsArcSmall)
            {
                this.PositionConnected();
            }
            else
            {
                this.PositionArcMidpoint();
            }
        }

        /// <summary>
        /// If at least one arc is small, all labels are displayed connected by a line. Otherwise, they're positioned
        /// in the arc midpoint.
        /// </summary>
        private void PositionAuto()
        {
            Chart chart = this.FindAncestor<Chart>(this);
            if (chart != null)
            {
                PieChartLabelArea labelArea = chart.Template.FindName("LabelArea_PART", chart) as PieChartLabelArea;
                if (labelArea != null && labelArea.HasSmallArc)
                {
                    this.PositionConnected();
                }
                else
                {
                    this.PositionArcMidpoint();
                }
            }
        }

        /// <summary>
        /// Ensures that the line connecting the label is positioned in such a way that whole pixels are used to render
        /// it. This calculation depends on whether the thickness of the line is odd or even.
        /// </summary>
        /// <param name="point">A point belonging to the polyline.</param>
        /// <returns>The point that will cause the line to snap to pixels.</returns>
        private Point SnapPoint(Point point)
        {
            double lineThickness = this.LineStrokeThickness;
            int intLineThickness = (int)lineThickness;
            if (lineThickness == intLineThickness)
            {
                if ((intLineThickness % 2) == 1)
                {
                    return new Point(Math.Floor(point.X) + 0.5, Math.Floor(point.Y) + 0.5);
                }
                else
                {
                    return new Point(Math.Round(point.X), Math.Round(point.Y));
                }
            }

            return point;
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.UIElement.MouseLeftButtonDown" /> routed event is raised on this element. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseButtonEventArgs" /> that contains the event data. The event data reports that the left mouse button was pressed.</param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            // When a label is clicked, the corresponding PieDataPoint becomes selected.
            // This enables master-detail scenario driven by clicking on labels.
            Chart chart = this.FindAncestor<Chart>(this);
            if (chart != null)
            {
                PieSeries pieSeries = chart.Series.OfType<PieSeries>().FirstOrDefault();
                if (pieSeries != null)
                {
                    pieSeries.SelectedItem = this.Content;
                }
            }
        }

        /// <summary>
        /// Finds the first ancestor of the element passed as a parameter that has type T.
        /// </summary>
        /// <typeparam name="T">The type of the ancestor we're looking for.</typeparam>
        /// <param name="element">The element where we start our search.</param>
        /// <returns>The first ancestor of element of type T.</returns>
        private T FindAncestor<T>(DependencyObject element) where T : class
        {
            while (element != null)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(element) as DependencyObject;
                T result = parent as T;
                if (result != null)
                {
                    return result;
                }

                element = parent;
            }

            return null;
        }
    }
}
