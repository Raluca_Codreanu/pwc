﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace ZPKTool.Controls
{
    /// <summary>
    /// A step of the wizard control that defines the content displayed when it is selected.
    /// </summary>
    public class WizardStep : ContentControl
    {
        /// <summary>
        /// The header control corresponding to this instance.
        /// </summary>
        private WizardStepHeader header;
        
        /// <summary>
        /// Using a DependencyProperty as the backing store for StepName.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty StepNameProperty =
            DependencyProperty.Register("StepName", typeof(string), typeof(WizardStep), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes static members of the <see cref="WizardStep"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static WizardStep()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WizardStep), new FrameworkPropertyMetadata(typeof(WizardStep)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WizardStep"/> class.
        /// </summary>
        public WizardStep()
        {
            this.HorizontalContentAlignment = HorizontalAlignment.Left;
            this.VerticalContentAlignment = VerticalAlignment.Top;
            this.IsEnabledChanged += new DependencyPropertyChangedEventHandler(WizardStep_IsEnabledChanged);
        }
                
        /// <summary>
        /// Gets or sets the header of the step.
        /// </summary>
        public WizardStepHeader Header
        {
            get
            {
                return this.header;
            }

            set
            {
                this.header = value;
                this.header.IsEnabled = this.IsEnabled;
            }
        }

        /// <summary>
        /// Gets or sets the name of the step. This is a dependency property.
        /// </summary>
        /// <value>The name of the step.</value>
        public string StepName
        {
            get { return (string)GetValue(StepNameProperty); }
            set { SetValue(StepNameProperty, value); }
        }

        /// <summary>
        /// Sets the focus on first textbox in each wizard step after applying the template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Action action = () => { Helper.FocusFirstTextbox(this.Content); };
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input, action);
        }
                
        /// <summary>
        /// Handles the IsEnabledChanged event of the WizardStep control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void WizardStep_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Header != null)
            {
                this.Header.IsEnabled = this.IsEnabled;
            }
        }
    }
}
