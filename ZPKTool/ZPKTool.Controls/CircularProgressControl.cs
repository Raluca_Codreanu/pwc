﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// A control that shows a circular animation and, optionally, a progress message.
    /// </summary>
    public class CircularProgressControl : Control
    {
        /// <summary>
        /// The default color of the control.
        /// </summary>
        private static readonly SolidColorBrush DefaultColor = new SolidColorBrush(System.Windows.Media.Color.FromRgb(91, 130, 185));

        /// <summary>
        /// Initializes static members of the <see cref="CircularProgressControl"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static CircularProgressControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CircularProgressControl), new FrameworkPropertyMetadata(typeof(CircularProgressControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CircularProgressControl"/> class.
        /// </summary>
        public CircularProgressControl()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the progress animation is started.
        /// Setting this property to true will start the animation and setting it to false will stop it.
        /// </summary>        
        public bool Started
        {
            get { return (bool)GetValue(StartedProperty); }
            set { SetValue(StartedProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Started.  This enables animation, styling, binding, etc...\
        /// </summary>
        public static readonly DependencyProperty StartedProperty =
            DependencyProperty.Register("Started", typeof(bool), typeof(CircularProgressControl), new UIPropertyMetadata(true));

        /// <summary>
        /// Gets or sets the color of the animation.
        /// </summary>
        public Brush Color
        {
            get { return (Brush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Color.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(Brush), typeof(CircularProgressControl), new UIPropertyMetadata(DefaultColor));

        /// <summary>
        /// Gets or sets the progress message. You can use a static message or you can display the progress in percentage.
        /// </summary>
        public string Progress
        {
            get { return (string)GetValue(ProgressProperty); }
            set { SetValue(ProgressProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Progress.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ProgressProperty =
            DependencyProperty.Register("Progress", typeof(string), typeof(CircularProgressControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the color of the progress message.
        /// </summary>
        public Brush ProgressColor
        {
            get { return (Brush)GetValue(ProgressColorProperty); }
            set { SetValue(ProgressColorProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ProgressColor.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ProgressColorProperty =
            DependencyProperty.Register("ProgressColor", typeof(Brush), typeof(CircularProgressControl), new UIPropertyMetadata(new SolidColorBrush(Colors.Black)));

        /// <summary>
        /// Gets or sets the size of the progress message font.
        /// </summary>        
        public double ProgressFontSize
        {
            get { return (double)GetValue(ProgressFontSizeProperty); }
            set { SetValue(ProgressFontSizeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ProgressFontSize.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ProgressFontSizeProperty =
            DependencyProperty.Register("ProgressFontSize", typeof(double), typeof(CircularProgressControl), new UIPropertyMetadata(18d));
    }
}
