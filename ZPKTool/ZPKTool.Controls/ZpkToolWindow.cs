﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Common;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Class extending <see cref="Window"/> in order to add new features such as zooming ability.
    /// </summary>
    public class ZpkToolWindow : Window
    {
        #region Attributes

        /// <summary>
        /// The maximum limit below which the changes performed on window sizes are not considered.
        /// </summary>
        private const double MaxDelta = 20;

        /// <summary>
        /// Stores the zoomed height of the <see cref="ZpkToolWindow"/>.
        /// </summary>
        private double zoomedHeight;

        /// <summary>
        /// Stores the zoomed width of the <see cref="ZpkToolWindow"/>.
        /// </summary>
        private double zoomedWidth;

        /// <summary>
        /// Indicates whether the <see cref="zoomedWidth"/> and 
        /// <see cref="zoomedHeight"/> properties have already been calculated.
        /// </summary>
        private volatile bool zoomedSizeCalculated;

        /// <summary>
        /// Indicates whether the <see cref="zoomedWidth"/> has already been applied to the window.
        /// </summary>
        private volatile bool zoomedWidthApplied;

        /// <summary>
        /// Indicates whether the <see cref="zoomedHeight"/> has already been applied to the window.
        /// </summary>
        private volatile bool zoomedHeightApplied;

        /// <summary>
        /// Indicates whether the first SizeChanged event (after constructor invocation) has been handled.
        /// </summary>
        private volatile bool firstResizeHandled;

        /// <summary>
        /// The root element in the window's template.
        /// </summary>
        private FrameworkElement rootElement;

        /// <summary>
        /// The root scroll viewer in the window's template.
        /// </summary>
        private ScrollViewer rootScrollViewer;

        /// <summary>
        /// Total height of window's horizontal borders
        /// </summary>
        private double horizontalBordersHeight;

        /// <summary>
        /// Total width of window's vertical borders
        /// </summary>
        private double verticalBordersWidth;

        /// <summary>
        /// Content Desired Size
        /// </summary>
        private Size contentDesiredSize;

        /// <summary>
        /// Indicates whether render adjustments were made or not.
        /// </summary>
        private bool doingRenderAdjustments;

        /// <summary>
        /// Indicates whether current window was rendered.
        /// </summary>
        private bool renderPerformed;

        /// <summary>
        /// The initial Width for the ZPK window.
        /// </summary>
        private double initialWidth;

        /// <summary>
        /// The initial Height for the ZPK window.
        /// </summary>
        private double initialHeight;

        /// <summary>
        /// The initial WindowState for the ZPk window.
        /// </summary>
        private WindowState initialState;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="ZpkToolWindow"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", 
            Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static ZpkToolWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ZpkToolWindow),
                new FrameworkPropertyMetadata(typeof(ZpkToolWindow)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZpkToolWindow"/> class.
        /// </summary>
        public ZpkToolWindow()
        {
            this.ZoomLevel = UserSettingsManager.Instance.ZoomLevel;

            if (this.ResizeMode == ResizeMode.CanResize
                || this.ResizeMode == ResizeMode.CanResizeWithGrip)
            {
                this.SizeChanged += this.OnZpkToolWindowSizeChanged;
            }

            this.LayoutUpdated += this.OnZpkToolWindowLayoutUpdated;
            this.Loaded += this.ZpkToolWindowLoaded;
            this.Closing += this.ZpkToolWindowClosing;
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// DependencyProperty for handling the window zoom level.
        /// </summary>
        public static readonly DependencyProperty ZoomLevelProperty = DependencyProperty.Register(
            "ZoomLevel",
            typeof(double),
            typeof(ZpkToolWindow),
            new FrameworkPropertyMetadata(1.0d));

        /// <summary>
        /// Gets or sets the zoom level for the current window.
        /// </summary>
        /// <value>The window zoom level.</value>
        public double ZoomLevel
        {
            get { return (double)GetValue(ZoomLevelProperty); }
            set { SetValue(ZoomLevelProperty, value); }
        }

        /// <summary>
        /// DependencyProperty for storing the window's content width.
        /// </summary>
        public static readonly DependencyProperty ContentWidthProperty = DependencyProperty.Register(
            "ContentWidth",
            typeof(double),
            typeof(ZpkToolWindow),
            new UIPropertyMetadata(0d));

        /// <summary>
        /// Gets or sets the window's content width.
        /// </summary>
        /// <value>The window's content width.</value>
        public double ContentWidth
        {
            get { return (double)GetValue(ContentWidthProperty); }
            set { SetValue(ContentWidthProperty, value); }
        }

        /// <summary>
        /// DependencyProperty for storing the window's content height.
        /// </summary>
        public static readonly DependencyProperty ContentHeightProperty = DependencyProperty.Register(
            "ContentHeight",
            typeof(double),
            typeof(ZpkToolWindow),
            new UIPropertyMetadata(0d));

        /// <summary>
        /// Gets or sets the window's content height.
        /// </summary>
        /// <value>The window's content height.</value>
        public double ContentHeight
        {
            get { return (double)GetValue(ContentHeightProperty); }
            set { SetValue(ContentHeightProperty, value); }
        }

        #endregion Dependency Properties

        #region Event Handlers

        /// <summary>
        /// When overridden in a derived class, participates in rendering operations 
        /// that are directed by the layout system.
        /// Here it is used in order to calculate the <see cref="zoomedWidth"/> and 
        /// <see cref="zoomedHeight"/> properties
        /// based on the ActualWidth and ActualHeight properties (which have concrete values 
        /// only when rendering is performed).
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. 
        /// This context is provided to the layout system.</param>
        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            this.renderPerformed = true;

            if (!this.doingRenderAdjustments)
            {
                this.doingRenderAdjustments = true;

                var titleHeight = SystemParameters.WindowCaptionHeight
                                  + SystemParameters.ResizeFrameHorizontalBorderHeight;
                var bottomBorderHeight = SystemParameters.ResizeFrameHorizontalBorderHeight;

                if (!this.zoomedSizeCalculated)
                {
                    this.zoomedSizeCalculated = true;

                    /*
                     * Get window Borders total Width and Height.
                     */
                    this.horizontalBordersHeight = titleHeight + bottomBorderHeight;
                    this.verticalBordersWidth = bottomBorderHeight * 2;

                    var windowWidth = this.ActualWidth;
                    var windowHeight = this.ActualHeight;

                    /*
                     * Calculate the window size according to the zoom level
                     */
                    var maxHeightArea = SystemParameters.WorkArea.Height - 12d;
                    var maxWidthArea = SystemParameters.WorkArea.Width - 12d;
                    this.zoomedHeight =
                        Math.Round(
                            Math.Min(
                                ((this.ActualHeight - this.horizontalBordersHeight) * this.ZoomLevel)
                                + this.horizontalBordersHeight,
                                maxHeightArea));
                    this.zoomedWidth =
                        Math.Round(
                            Math.Min(
                                ((this.ActualWidth - this.verticalBordersWidth) * this.ZoomLevel)
                                + this.verticalBordersWidth,
                                maxWidthArea));

                    /*
                     * Set content size according to grid root size
                     */
                    this.ContentHeight = this.rootElement.ActualHeight;
                    this.ContentWidth = this.rootElement.ActualWidth;

                    /*
                     * For resizable windows, the MinWidth and MinHeight 
                     * should be zoomedWidth and zoomedHeight
                     */
                    this.MinWidth = this.zoomedWidth;
                    this.MinHeight = this.zoomedHeight;

                    /*
                     * Re-center the window
                     */
                    this.Top -= (this.zoomedHeight - windowHeight) / 2d;
                    this.Left -= (this.zoomedWidth - windowWidth) / 2d;
                    this.Top = Math.Max(this.Top, 0d);
                    this.Left = Math.Max(this.Left, 0d);
                }

                this.doingRenderAdjustments = false;
            }

            base.OnRender(drawingContext);
        }

        /// <summary>
        /// ZpkToolWindow LayoutUpdated Event Handler
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.SizeChangedEventArgs"/> 
        /// instance containing the event data.</param>
        private void OnZpkToolWindowLayoutUpdated(object sender, EventArgs e)
        {
            /*
             * If the current window was not rendered nor 
             * window size have not been changed, do nothing
             */
            var contentPresenter = this.Template.FindName("PART_ContentPresenter", this) as ContentPresenter;
            if (!this.renderPerformed 
                && (contentPresenter == null || this.contentDesiredSize == contentPresenter.DesiredSize))
            {
                return;
            }

            /*
             * Adjust small deviations caused by zoom on the content
             */
            if (this.ZoomLevel >= 1.0d)
            {
                if (contentPresenter != null)
                {
                    this.contentDesiredSize = contentPresenter.DesiredSize;

                    // TODO: find a more accurate deviation value
                    var deviationValue = 5 * this.ZoomLevel;

                    if (contentDesiredSize.Height - deviationValue <= this.rootElement.ActualHeight)
                    {
                        this.rootScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                        this.ContentHeight = this.rootElement.ActualHeight / this.ZoomLevel;
                    }

                    if (contentDesiredSize.Width - deviationValue <= this.rootElement.ActualWidth)
                    {
                        this.rootScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                        this.ContentWidth = this.rootElement.ActualWidth / this.ZoomLevel;
                    }
                }
            }
            else
            {
                /*
                 * If zoom < 100% just disable root ScrollViewer scrollBars and correct content size
                 */
                this.rootScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                this.rootScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                this.ContentHeight = this.rootElement.ActualHeight / this.ZoomLevel;
                this.ContentWidth = this.rootElement.ActualWidth / this.ZoomLevel;
            }

            this.renderPerformed = false;
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or 
        /// internal processes call System.Windows.FrameworkElement.ApplyTemplate()
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var inDesignMode = System.ComponentModel.DesignerProperties.GetIsInDesignMode(this);

            this.rootElement = this.Template.FindName("PART_Root", this) as FrameworkElement;
            if (this.rootElement == null && !inDesignMode)
            {
                throw new InvalidOperationException("The root window element could not be found.");
            }

            this.rootScrollViewer = this.Template.FindName("PART_WindowScrollViewer", this) as ScrollViewer;
            if (this.rootScrollViewer == null && !inDesignMode)
            {
                throw new InvalidOperationException("The root window scroll viewer could not be found.");
            }
        }

        /// <summary>
        /// Measures and applies the zoomed size of the window.
        /// </summary>
        /// <param name="availableSize">A <see cref="System.Windows.Size"/> 
        /// that reflects the available size that this window can give to the child.
        /// Infinity can be given as a value to indicate that the window will size 
        /// to whatever content is available.</param>
        /// <returns>A <see cref="System.Windows.Size"/> that reflects the size 
        /// that this window determines it needs during layout.</returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            if (this.zoomedSizeCalculated)
            {
                if (!this.zoomedWidthApplied)
                {
                    this.zoomedWidthApplied = true;
                    this.Width = this.zoomedWidth;
                }

                if (!this.zoomedHeightApplied)
                {
                    this.zoomedHeightApplied = true;
                    this.Height = this.zoomedHeight;
                }

                if (!this.zoomedWidthApplied || !this.zoomedHeightApplied)
                {
                    return base.MeasureOverride(new Size(this.Width, this.Height));
                }

                return base.MeasureOverride(availableSize);
            }

            return base.MeasureOverride(availableSize);
        }

        /// <summary>
        /// Handles the SizeChanged event for the ZpkToolWindow. 
        /// Used in order to synchronize the content size to the window size.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.SizeChangedEventArgs"/> 
        /// instance containing the event data.</param>
        private void OnZpkToolWindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            /*
             * Skip resizing while window is being initialized
             */
            if (!this.zoomedSizeCalculated || e.PreviousSize.Height == 0d || e.PreviousSize.Width == 0d)
            {
                return;
            }

            /*
             * Don't skip first resize event if the window has been maximized
             */
            if (this.WindowState == WindowState.Maximized)
            {
                firstResizeHandled = true;
            }

            /*
             * First resize event needs to be skipped because it produces weird behavior 
             * (difference between NewSize and PreviousSize is too large)
             */
            if (!firstResizeHandled)
            {
                firstResizeHandled = true;
                return;
            }

            /*
             * Adjust the content size to be in sync with the window size
             */
            ContentHeight += (e.NewSize.Height - e.PreviousSize.Height) / ZoomLevel;
            ContentWidth += (e.NewSize.Width - e.PreviousSize.Width) / ZoomLevel;
        }

        /// <summary>
        /// ZPK Window loaded action.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZpkToolWindowLoaded(object sender, RoutedEventArgs e)
        {
            var windSettings = WindowsSettingsManager.Instance.GetWindowSettings(this.Uid);
            if (windSettings == null)
            {
                this.initialWidth = this.Width;
                this.initialHeight = this.Height;
                this.initialState = this.WindowState;
                return;
            }

            this.WindowState = windSettings.WindowState;
            if (windSettings.WindowState == WindowState.Normal)
            {
                var ignoreLeft = !windSettings.Left.HasValue || !windSettings.Width.HasValue
                                 || windSettings.Width.Value <= this.Width;
                var ignoreTop = !windSettings.Top.HasValue || !windSettings.Height.HasValue
                                || windSettings.Height.Value <= this.Height;

                if (!ignoreLeft)
                {
                    this.Left = windSettings.Left.Value;
                    this.Width = windSettings.Width.Value;
                }

                if (!ignoreTop)
                {
                    this.Top = windSettings.Top.Value;
                    this.Height = windSettings.Height.Value;
                }
            }

            this.initialWidth = this.Width;
            this.initialHeight = this.Height;
            this.initialState = this.WindowState;
        }

        /// <summary>
        /// ZPK Window closing action.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> 
        /// instance containing the event data.</param>
        private void ZpkToolWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var sameWidth = Math.Abs(this.Width - this.initialWidth) < MaxDelta
                            || this.WindowState == WindowState.Maximized;
            var sameHeight = Math.Abs(this.Height - this.initialHeight) < MaxDelta
                             || this.WindowState == WindowState.Maximized;
            var sameState = Equals(this.initialState, this.WindowState);
            if (sameWidth && sameHeight && sameState)
            {
                return;
            }

            var width = sameWidth ? (double?)null : this.Width;
            var height = sameHeight ? (double?)null : this.Height;

            var window = new WindowSettings(this.Uid, width, height, this.WindowState);
            WindowsSettingsManager.Instance.Save(window);
        }

        #endregion
    }
}