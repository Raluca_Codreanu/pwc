﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Controls.Languages;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class extends System.Windows.Controls.ComboBox providing new functionality.
    /// </summary>
    public class ComboBoxControl : ComboBox
    {
        /// <summary>
        /// The resource key for the normal style of the control.
        /// </summary>
        private static readonly ComponentResourceKey NormalStyleKey = new ComponentResourceKey(typeof(ComboBoxControl), "ComboBoxControlStyle");

        /// <summary>
        /// The resource key for the invalid style of the control.
        /// </summary>
        private static readonly ComponentResourceKey InvalidStyleKey = new ComponentResourceKey(typeof(ComboBoxControl), "ComboBoxControlInvalidStyle");

        /// <summary>
        /// The resource key for the recommended style of the control.
        /// </summary>
        private static readonly ComponentResourceKey RecommendedStyleKey = new ComponentResourceKey(typeof(ComboBoxControl), "ComboBoxControlRecommendedStyle");

        /// <summary>
        /// The popup that appears when a validation error occurs.
        /// </summary>
        private PopupControl validationPopup;

        /// <summary>
        /// The necessity level of this combo box.
        /// </summary>
        private TextBoxControlNecessityLevel necessity;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBoxControl"/> class.
        /// </summary>
        public ComboBoxControl()
        {
            SetSuitableStyle();

            this.SelectionChanged += new SelectionChangedEventHandler(ComboBoxControl_SelectionChanged);
        }

        /// <summary>
        /// Gets or sets the necessity level of this combo box.
        /// </summary>
        public TextBoxControlNecessityLevel Necessity
        {
            get
            {
                return this.necessity;
            }

            set
            {
                if (this.necessity != value)
                {
                    this.necessity = value;
                    SetSuitableStyle();
                }
            }
        }

        /// <summary>
        /// Validate the control's selection.
        /// </summary>
        /// <returns><c>true</c> if the selection is valid; otherwise, <c>false</c>.</returns>
        public bool Validate()
        {
            // If the control is not enabled the validation is always successful.
            if (!this.IsEnabled)
            {
                return true;
            }

            if (this.Necessity == TextBoxControlNecessityLevel.Mandatory && this.SelectedItem == null)
            {
                ShowValidationErrorPopup();
                Helper.FocusElement(this);

                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Sets the suitable style for this instance.
        /// </summary>
        private void SetSuitableStyle()
        {
            if (this.SelectedItem == null)
            {
                switch (this.Necessity)
                {
                    case TextBoxControlNecessityLevel.Mandatory:
                        this.SetResourceReference(ComboBoxControl.StyleProperty, InvalidStyleKey);
                        break;

                    case TextBoxControlNecessityLevel.Recommended:
                        this.SetResourceReference(ComboBoxControl.StyleProperty, RecommendedStyleKey);
                        break;

                    case TextBoxControlNecessityLevel.Optional:
                        this.SetResourceReference(ComboBoxControl.StyleProperty, NormalStyleKey);
                        break;
                }
            }
            else
            {
                this.SetResourceReference(ComboBoxControl.StyleProperty, NormalStyleKey);
            }
        }

        /// <summary>
        /// Shows a popup with a message when there is a validation error.
        /// </summary>
        private void ShowValidationErrorPopup()
        {
            if (this.validationPopup == null)
            {
                ImageSource errorImage = new BitmapImage(new Uri("Resources/Images/Error.png", UriKind.Relative));
                this.validationPopup = new PopupControl(LanguageResource.ComboBoxControl_SelectionEmpty, errorImage);

                double rectangleHeight = this.ActualHeight - 5 < 0 ? 0 : this.ActualHeight - 5;
                this.validationPopup.PlacementTarget = this;
                this.validationPopup.PopupAnimation = PopupAnimation.Fade;
                this.validationPopup.Placement = PlacementMode.Bottom;
                this.validationPopup.PlacementRectangle = new Rect(0, 0, this.ActualWidth, rectangleHeight);
                this.validationPopup.AllowsTransparency = true;
                this.validationPopup.StaysOpen = false;
            }

            if (!this.validationPopup.IsOpen)
            {
                this.validationPopup.IsOpen = true;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the ComboBoxControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ComboBoxControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetSuitableStyle();

            // Hide the validation popup
            if (this.validationPopup != null && this.validationPopup.IsOpen)
            {
                this.validationPopup.IsOpen = false;
            }
        }
    }
}
