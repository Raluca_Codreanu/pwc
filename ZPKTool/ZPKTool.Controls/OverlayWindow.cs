﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Simulates an overlay by displaying its content in a modal dialog, centered, over its owner (parent) window.
    /// It is recommended to always set its Owner property to a valid window.
    /// </summary>
    public class OverlayWindow : Window
    {
        /// <summary>
        /// The storyboard that dims the owner when the overlay window is displayed.
        /// </summary>
        private static readonly Storyboard dimmStoryboard = new Storyboard();

        /// <summary>
        /// The storyboard that un-dims the owner when the overlay window is closed.
        /// </summary>
        private static readonly Storyboard undimmStoryboard = new Storyboard();

        /// <summary>
        /// Initializes static members of the <see cref="OverlayWindow"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static OverlayWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OverlayWindow), new FrameworkPropertyMetadata(typeof(OverlayWindow)));

            DoubleAnimation dimmAnimation = new DoubleAnimation();
            dimmAnimation.To = 0.6d;
            dimmAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 100));
            dimmAnimation.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("Opacity"));
            dimmStoryboard.Children.Add(dimmAnimation);

            DoubleAnimation undimmAnimation = new DoubleAnimation();
            undimmAnimation.To = 1.0d;
            undimmAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 100));
            undimmAnimation.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("Opacity"));
            undimmStoryboard.Children.Add(undimmAnimation);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OverlayWindow"/> class.
        /// </summary>
        public OverlayWindow()
        {
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            this.Loaded += new RoutedEventHandler(this.OnLoaded);
        }

        /// <summary>
        /// Opens a window and returns without waiting for the newly opened window to close.
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">
        /// <see cref="M:System.Windows.Window.Show"/> is called on a window that is closing (<see cref="E:System.Windows.Window.Closing"/>) or has been closed (<see cref="E:System.Windows.Window.Closed"/>).</exception>
        public new void Show()
        {
            this.ShowDialog();
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var closeButton = this.Template.FindName("PART_CloseButton", this) as AnimatedImageButton;
            if (closeButton != null)
            {
                closeButton.Click += (s, e) => this.Close();
            }

            var minimizeButton = this.Template.FindName("PART_MinimizeButton", this) as AnimatedImageButton;
            if (minimizeButton != null)
            {
                minimizeButton.Click += (s, e) =>
                    {
                        var owner = this.Owner;
                        if (owner != null)
                        {
                            owner.WindowState = WindowState.Minimized;
                        }
                    };
            }
        }

        /// <summary>
        /// Handles the Loaded event of the OverlayWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (this.Owner != null)
            {
                dimmStoryboard.Begin(this.Owner);
            }
        }

        /// <summary>
        /// Raises the Closed event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            if (this.Owner != null)
            {
                undimmStoryboard.Begin(this.Owner);
            }

            base.OnClosed(e);
        }
    }
}
