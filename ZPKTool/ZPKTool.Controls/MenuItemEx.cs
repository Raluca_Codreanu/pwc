﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class extends <see cref="System.Windows.Controls.MenuItem"/> to provide new functionality for submenu headers.
    /// </summary>
    public class MenuItemEx : MenuItem
    {
        /// <summary>
        /// The grid's name used in menu item ex styles as the arrow container.
        /// </summary>
        private const string GridName = "ArrowContainerGrid";

        /// <summary>
        /// Initializes static members of the <see cref="MenuItemEx"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static MenuItemEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MenuItemEx), new FrameworkPropertyMetadata(typeof(MenuItemEx)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemEx"/> class.
        /// </summary>
        public MenuItemEx()
        {
            this.Loaded += MenuItemEx_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of the MenuItemEx control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">This control cannot be used for top level items or headers into ContextMenus.</exception>
        private void MenuItemEx_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.Role == MenuItemRole.TopLevelItem || this.Role == MenuItemRole.TopLevelHeader)
            {
                throw new InvalidOperationException("This control cannot be used for top level items or headers into ContextMenus.");
            }
        }

        /// <summary>
        /// Called when the left mouse button is pressed.
        /// </summary>
        /// <param name="e">The event data for the <see cref="E:System.Windows.UIElement.MouseLeftButtonDown" /> event.</param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            var grid = e.OriginalSource as Grid;
            if (grid != null && grid.Name == GridName)
            {
                // If the user clicks into the grid's area containing the arrow, nothing will happen.
                return;
            }

            var parentGrid = FindParent<Grid>(e.OriginalSource as DependencyObject);
            if (parentGrid != null && parentGrid.Name == GridName)
            {
                // Also, if the user clicks on an item from the grid's area containing the arrow, nothing will happen.
                return;
            }

            this.ExecuteCommand();
        }

        /// <summary>
        /// Responds to the <see cref="E:System.Windows.UIElement.KeyDown" /> event.
        /// </summary>
        /// <param name="e">The event data for the <see cref="E:System.Windows.UIElement.KeyDown" /> event.</param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.Key == System.Windows.Input.Key.Enter)
            {
                // Execute the command only if the user presses the Enter key.
                this.ExecuteCommand();
            }
        }

        /// <summary>
        /// Executes the command associated to the current menu item.
        /// </summary>
        private void ExecuteCommand()
        {
            if (this.Role == MenuItemRole.SubmenuHeader)
            {
                // The command will be executed only if the menu item has children.
                var command = this.Command;
                var commandParameter = this.CommandParameter;
                if (command != null && command.CanExecute(commandParameter))
                {
                    command.Execute(commandParameter);
                }
            }
        }

        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the queried item.</param>
        /// <returns>The first parent item that matches the submitted type parameter. If not matching item can be found, a null reference is being returned.</returns>
        private T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            // Get parent item.
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                // If we've reached the end of the tree, return null.
                return null;
            }

            // Check if the parent matches the type we're looking for.
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                // Use recursion to proceed with next level.
                return this.FindParent<T>(parentObject);
            }
        }
    }
}
