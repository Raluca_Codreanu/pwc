﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Controls.Languages;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Interaction logic for OpenFileOverlayWindow.xaml
    /// </summary>
    public partial class OpenFileOverlayWindow
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenFileOverlayWindow"/> class.
        /// </summary>
        public OpenFileOverlayWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the path to the last created report.
        /// </summary>
        /// <value>The last created file path.</value>
        public string LastCreatedFilePath
        {
            get { return (string)GetValue(LastCreatedFilePathProperty); }
            set { SetValue(LastCreatedFilePathProperty, value); }
        }

        /// <summary>
        /// Gets or sets the created file status.
        /// </summary>
        /// <value>The created file status.</value>
        public string CreatedFileStatus
        {
            get
            {
                return (string)GetValue(CreatedFileStatusProperty);
            }

            set
            {
                SetValue(CreatedFileStatusProperty, value);
                FileCreationStatus.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is posible to open the file.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is open file available; otherwise, <c>false</c>.
        /// </value>
        public bool IsOpenFileAvailable
        {
            get
            {
                return (bool)GetValue(IsOpenFileAvailableProperty);
            }

            set
            {
                SetValue(IsOpenFileAvailableProperty, value);

                if (value == false)
                {
                    this.OpenCreatedFileLabel.Visibility = System.Windows.Visibility.Collapsed;
                    this.OpenCreatedFileButton.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    this.OpenCreatedFileLabel.Visibility = System.Windows.Visibility.Visible;
                    this.OpenCreatedFileButton.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        #endregion

        #region DependencyProperties

        /// <summary>
        /// The created file status.
        /// </summary>
        public static readonly DependencyProperty CreatedFileStatusProperty =
           DependencyProperty.Register("CreatedFileStatus", typeof(string), typeof(OpenFileOverlayWindow));

        /// <summary>
        /// The path to the last created report.
        /// </summary>
        public static readonly DependencyProperty LastCreatedFilePathProperty =
            DependencyProperty.Register("LastCreatedFilePath", typeof(string), typeof(OpenFileOverlayWindow));

        /// <summary>
        /// Indicats whether it is posible to open the file.
        /// </summary>
        public static readonly DependencyProperty IsOpenFileAvailableProperty =
           DependencyProperty.Register("IsOpenFileAvailable", typeof(bool), typeof(OpenFileOverlayWindow), new UIPropertyMetadata(true));

        #endregion

        #region Events

        /// <summary>
        /// Handles the Click event of the CloseButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the OpenLastCreatedReportButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "No exception should be thrown from the method.")]
        private void OpenLastCreatedFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(this.LastCreatedFilePath);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error occurred while opening last created file.", ex);
                this.CreatedFileStatus = LanguageResource.OpenFileOverlayWindow_ErrorOpeningFile;
            }
        }

        /// <summary>
        /// Handles the Loaded event of the OpenFileOverlayWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OpenFileOverlayWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.CloseButton.Focus();
            Keyboard.Focus(this.CloseButton);
        }

        #endregion
    }
}
