﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The wizard step's header that can be clicked in the wizard.
    /// </summary>
    public class WizardStepHeader : Control
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for StepName.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty StepNameProperty =
            DependencyProperty.Register("StepName", typeof(string), typeof(WizardStepHeader), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(WizardStepHeader), new UIPropertyMetadata(false));

        /// <summary>
        /// Initializes static members of the <see cref="WizardStepHeader"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static WizardStepHeader()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WizardStepHeader), new FrameworkPropertyMetadata(typeof(WizardStepHeader)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WizardStepHeader"/> class.
        /// </summary>
        public WizardStepHeader()
        {
            this.MouseLeftButtonUp += WizardStepHeader_MouseLeftButtonUp;
        }

        /// <summary>
        /// Occurs when the user clicks the header with the left mouse button.
        /// </summary>
        public event EventHandler Click;

        /// <summary>
        /// Gets or sets the name of the step, displayed by this instance. This is a dependency property.
        /// </summary>        
        public string StepName
        {
            get { return (string)GetValue(StepNameProperty); }
            set { SetValue(StepNameProperty, value); }
        }
                
        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected. This is a dependency property.
        /// </summary>
        /// <value>true if this instance is selected; otherwise, false.</value>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
                
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the this control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void WizardStepHeader_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var handlers = this.Click;
            if (handlers != null)
            {
                handlers.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
