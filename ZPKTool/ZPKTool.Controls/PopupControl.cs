﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Represents a popup 
    /// </summary>
    public class PopupControl : Popup
    {
        /// <summary>
        /// The content of the popup control.
        /// </summary>
        private PopupContentControl contentControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="PopupControl"/> class.
        /// </summary>
        /// <param name="text">The text to display in the popup.</param>
        /// <param name="icon">The icon to display with the text.</param>
        public PopupControl(string text, ImageSource icon)
        {
            contentControl = new PopupContentControl(this, icon);
            contentControl.ContentHolder.Text = text;
            this.Child = contentControl;
        }

        /// <summary>
        /// Gets or sets the text that appears in the popup control.
        /// </summary>
        public string Text
        {
            get
            {
                return contentControl.ContentHolder.Text;
            }

            set
            {
                contentControl.ContentHolder.Text = value;
            }            
        }
                
        /// <summary>
        /// Shows a balloon popup in a parent element.
        /// </summary>
        /// <param name="parent">The parent element.</param>
        /// <param name="message">The text that appears in the balloon.</param>
        /// <param name="icon">The icon to show in the balloon.</param>
        public static void Show(FrameworkElement parent, string message, ImageSource icon)
        {
            if (parent == null)
            {
                return;
            }

            PopupControl popup = new PopupControl(message, icon);
            double rectangleHeight = parent.ActualHeight - 10 < 0 ? 0 : parent.ActualHeight - 10;
            popup.PlacementTarget = parent;
            popup.PopupAnimation = PopupAnimation.Fade;
            popup.Placement = PlacementMode.Bottom;
            popup.PlacementRectangle = new Rect(0, 0, parent.ActualWidth, rectangleHeight);
            popup.AllowsTransparency = true;
            popup.StaysOpen = false;

            if (!popup.IsOpen)
            {
                popup.IsOpen = true;
            }
        }
    }
}
