﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The necessity level of the TextBoxControl content
    /// </summary>
    public enum TextBoxControlNecessityLevel
    {
        /// <summary>
        /// Entities will be saved even if the textbox is not filled, and it's really not necessary to 
        /// fill in.
        /// </summary>
        Optional,

        /// <summary>
        /// Entities will be saved even if the textbox is not filled, but it is recommended to fill it
        /// for further work (eg. calculations).
        /// </summary>
        Recommended,

        /// <summary>
        /// Entities can't be saved if the textbox is not filled.
        /// </summary>
        Mandatory
    }
}
