﻿namespace ZPKTool.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// A behavior that displays a watermark over a text box.
    /// </summary>
    public static class TextBoxWatermarkBehavior
    {
        #region Dependency Properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for EnableWatermark.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty EnableWatermarkProperty =
            DependencyProperty.RegisterAttached("EnableWatermark", typeof(bool), typeof(TextBoxWatermarkBehavior), new UIPropertyMetadata(false, OnEnableWatermarkChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for WatermarkText.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty WatermarkTextProperty =
            DependencyProperty.RegisterAttached("WatermarkText", typeof(string), typeof(TextBoxWatermarkBehavior), new UIPropertyMetadata(null, OnWatermarkTextChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for WatermarkColor.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty WatermarkColorProperty =
            DependencyProperty.RegisterAttached("WatermarkColor", typeof(Brush), typeof(TextBoxWatermarkBehavior), new UIPropertyMetadata(null, OnWatermarkColorChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for HideWatermarkWhenFocused.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty HideWatermarkWhenFocusedProperty =
            DependencyProperty.RegisterAttached("HideWatermarkWhenFocused", typeof(bool), typeof(TextBoxWatermarkBehavior), new UIPropertyMetadata(true));

        /// <summary>
        /// Gets a value indicating whether the watermark display is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetEnableWatermark(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnableWatermarkProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the watermark display is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetEnableWatermark(DependencyObject obj, bool value)
        {
            obj.SetValue(EnableWatermarkProperty, value);
        }

        /// <summary>
        /// Gets the watermark text.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The watermark text.</returns>
        public static string GetWatermarkText(DependencyObject obj)
        {
            return (string)obj.GetValue(WatermarkTextProperty);
        }

        /// <summary>
        /// Sets the watermark text.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetWatermarkText(DependencyObject obj, string value)
        {
            obj.SetValue(WatermarkTextProperty, value);
        }

        /// <summary>
        /// Gets the brush describing the color of the watermark.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>A Brush object.</returns>
        public static Brush GetWatermarkColor(DependencyObject obj)
        {
            return (Brush)obj.GetValue(WatermarkColorProperty);
        }

        /// <summary>
        /// Sets the brush describing the color of the watermark.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetWatermarkColor(DependencyObject obj, Brush value)
        {
            obj.SetValue(WatermarkColorProperty, value);
        }

        /// <summary>
        /// Gets a value indicating whether to hide the watermark when the element is focused but empty.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetHideWatermarkWhenFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(HideWatermarkWhenFocusedProperty);
        }

        /// <summary>
        /// Sets a value indicating whether to hide the watermark when the element is focused but empty.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value to set.</param>
        public static void SetHideWatermarkWhenFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(HideWatermarkWhenFocusedProperty, value);
        }

        #endregion Dependency Properties

        /// <summary>
        /// Called when the EnableWatermark property changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnEnableWatermarkChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textBox = d as TextBox;
            if (textBox == null)
            {
                throw new InvalidOperationException("The target control for the watermark was null or not a text box.");
            }

            if (e.OldValue != null)
            {
                bool enabled = (bool)e.OldValue;
                if (enabled)
                {
                    textBox.Loaded -= TextBoxLoaded;
                    textBox.GotFocus -= TextBoxGotFocus;
                    textBox.LostFocus -= TextBoxLostFocus;
                    textBox.TextChanged -= TextBoxTextChanged;
                    textBox.DragEnter -= TextBoxDragEnter;
                    textBox.DragLeave -= TextBoxDragLeave;
                }
            }

            if (e.NewValue != null)
            {
                bool enabled = (bool)e.NewValue;
                if (enabled)
                {
                    textBox.Loaded += TextBoxLoaded;
                    textBox.GotFocus += TextBoxGotFocus;
                    textBox.LostFocus += TextBoxLostFocus;
                    textBox.TextChanged += TextBoxTextChanged;
                    textBox.DragEnter += TextBoxDragEnter;
                    textBox.DragLeave += TextBoxDragLeave;
                }
            }
        }

        /// <summary>
        /// Called when the WatermarkText property changed.
        /// </summary>
        /// <param name="d">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnWatermarkTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textBox = d as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Called when the WatermarkColor property changed.
        /// </summary>
        /// <param name="d">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnWatermarkColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBox textBox = d as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the Loaded event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void TextBoxLoaded(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the GotFocus event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void TextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the LostFocus event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the TextChanged event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.TextChangedEventArgs"/> instance containing the event data.</param>
        private static void TextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the DragEnter event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private static void TextBoxDragEnter(object sender, DragEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            RemoveWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Handles the DragLeave event of a TextBox.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private static void TextBoxDragLeave(object sender, DragEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            UpdateWatermarkAdorner(textBox);
        }

        /// <summary>
        /// Updates the watermark adorner of a specified text box (shows, hides or refreshes it based on internal logic).
        /// </summary>
        /// <param name="textBox">The text box.</param>
        private static void UpdateWatermarkAdorner(TextBox textBox)
        {
            if (textBox == null)
            {
                return;
            }

            bool hasText = !string.IsNullOrEmpty(textBox.Text);
            bool hideWhenFocused = GetHideWatermarkWhenFocused(textBox);
            if (hasText || (!hideWhenFocused && textBox.IsFocused))
            {
                // Hide the Watermark Label if the adorner layer is visible                
                RemoveWatermarkAdorner(textBox);
            }
            else
            {
                // Show/refresh the Watermark Label if the adorner layer is visible                
                RefreshWatermarkAdorner(textBox);
            }
        }

        /// <summary>
        /// Removes the watermark adorner from the specified element's adorner layer.
        /// </summary>
        /// <param name="element">The element.</param>
        private static void RemoveWatermarkAdorner(UIElement element)
        {
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(element);
            if (adornerLayer != null)
            {
                var adorners = adornerLayer.GetAdorners(element);
                if (adorners != null)
                {
                    var watermarkAdorners = adorners.OfType<WatermarkAdorner>();
                    foreach (var adorner in watermarkAdorners)
                    {
                        adornerLayer.Remove(adorner);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the watermark adorner to a specified element's adorner layer. If the adorner is already added, it refreshes it.
        /// </summary>
        /// <param name="element">The element.</param>
        private static void RefreshWatermarkAdorner(UIElement element)
        {
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(element);
            if (adornerLayer == null)
            {
                return;
            }

            var adorners = adornerLayer.GetAdorners(element);
            WatermarkAdorner watermarkAdorner = null;

            if (adorners == null || (watermarkAdorner = adorners.OfType<WatermarkAdorner>().FirstOrDefault()) == null)
            {
                // The watermark adorner does not exist; create it.
                string watermarkText = GetWatermarkText(element);
                WatermarkAdorner adorner = new WatermarkAdorner(element, watermarkText);

                // Bind the visibility of the watermark adorner to the visibility of the adorned element so the watermark is not displayed when the
                // element's visibility is Hidden.
                Binding binding = new Binding("Visibility");
                binding.Source = element;
                binding.Mode = BindingMode.OneWay;
                adorner.SetBinding(Adorner.VisibilityProperty, binding);

                adornerLayer.Add(adorner);
            }
            else
            {
                // The watermark adorner exists; update its content.                
                watermarkAdorner.SetWatermarkText(GetWatermarkText(element));
                watermarkAdorner.SetWatermarkColor(GetWatermarkColor(element));
            }
        }

        #region The adorner class

        /// <summary>
        /// The adorner that displays the watermark.
        /// </summary>
        private class WatermarkAdorner : Adorner
        {
            /// <summary>
            /// The default watermark font color.
            /// </summary>
            private static readonly SolidColorBrush DefaultForegroundColor = new SolidColorBrush(Color.FromRgb(160, 160, 160));

            /// <summary>
            /// The text block that displays the watermark.
            /// </summary>
            private readonly TextBlock watermarkTextBlock;

            /// <summary>
            /// Initializes a new instance of the <see cref="WatermarkAdorner"/> class.
            /// </summary>
            /// <param name="adornedElement">The adorned element.</param>
            /// <param name="watermarkText">The watermark text.</param>
            public WatermarkAdorner(UIElement adornedElement, string watermarkText)
                : this(adornedElement, watermarkText, WatermarkAdorner.DefaultForegroundColor)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="WatermarkAdorner"/> class.
            /// </summary>
            /// <param name="adornedElement">The adorned element.</param>
            /// <param name="watermarkText">The watermark text.</param>
            /// <param name="watermarkColor">Color of the watermark.</param>
            public WatermarkAdorner(UIElement adornedElement, string watermarkText, Brush watermarkColor)
                : base(adornedElement)
            {
                this.watermarkTextBlock = new TextBlock();
                this.watermarkTextBlock.Text = watermarkText;
                this.watermarkTextBlock.Foreground = watermarkColor;
                this.watermarkTextBlock.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                Control adornedControl = adornedElement as Control;
                if (adornedControl != null)
                {
                    // Inherit the font from the adorned control.
                    // Add the adorned text box's left padding and border thickness as margin so the watermark is displayed just after the text box's caret.
                    this.watermarkTextBlock.FontSize = (double)adornedControl.GetValue(Control.FontSizeProperty);
                    this.watermarkTextBlock.FontFamily = (FontFamily)adornedControl.GetValue(Control.FontFamilyProperty);
                    Thickness padding = (Thickness)adornedControl.GetValue(Control.PaddingProperty);
                    Thickness border = (Thickness)adornedControl.GetValue(Control.BorderThicknessProperty);
                    padding.Top = 0;
                    padding.Right = 0;
                    padding.Bottom = 0;
                    padding.Left += border.Left + 2d;
                    this.watermarkTextBlock.Margin = padding;
                }
            }
                        
            /// <summary>
            /// Gets the number of visual child elements within this element.
            /// </summary>
            /// <returns>The number of visual child elements for this element.</returns>
            protected override int VisualChildrenCount
            {
                get { return 1; }
            }

            /// <summary>
            /// Sets the watermark text.
            /// </summary>
            /// <param name="value">The value.</param>
            public void SetWatermarkText(string value)
            {
                if (this.watermarkTextBlock.Text != value)
                {
                    this.watermarkTextBlock.Text = value;
                }
            }

            /// <summary>
            /// Sets the brush describing the color of the watermark.
            /// </summary>
            /// <param name="value">The value.</param>
            public void SetWatermarkColor(Brush value)
            {
                if (value != null && this.watermarkTextBlock.Foreground != value)
                {
                    this.watermarkTextBlock.Foreground = value;
                }
            }

            /// <summary>
            /// Implements any custom measuring behavior for the adorner.
            /// </summary>
            /// <param name="constraint">A size to constrain the adorner to.</param>
            /// <returns>
            /// A <see cref="T:System.Windows.Size"/> object representing the amount of layout space needed by the adorner.
            /// </returns>
            protected override Size MeasureOverride(Size constraint)
            {
                watermarkTextBlock.Measure(constraint);
                return this.AdornedElement.RenderSize;
            }

            /// <summary>
            /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement"/> derived class.
            /// </summary>
            /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
            /// <returns>
            /// The actual size used.
            /// </returns>
            protected override Size ArrangeOverride(Size finalSize)
            {
                watermarkTextBlock.Arrange(new Rect(finalSize));
                return finalSize;
            }

            /// <summary>
            /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)"/>, and returns a child at the specified index from a collection of child elements.
            /// </summary>
            /// <param name="index">The zero-based index of the requested child element in the collection.</param>
            /// <returns>
            /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
            /// </returns>
            protected override Visual GetVisualChild(int index)
            {
                return watermarkTextBlock;
            }
        }

        #endregion The adorner class
    }
}