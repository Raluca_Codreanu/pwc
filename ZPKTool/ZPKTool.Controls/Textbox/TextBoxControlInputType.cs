﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The possible input types for the TextBoxControl.
    /// </summary>
    public enum TextBoxControlInputType
    {
        /// <summary>
        /// General purpose text. The length and accepted characters can be set via the MaxContentLimit and CharacterType.
        /// </summary>
        Text,

        /// <summary>
        /// Text type of length 25.
        /// </summary>
        Text25,

        /// <summary>
        /// Text type of length 50.
        /// </summary>
        Text50,

        /// <summary>
        /// Text type of length 100.
        /// </summary>
        Text100,

        /// <summary>
        /// Text type of length 2000.
        /// </summary>
        Text2000,

        /// <summary>
        /// Integer type
        /// </summary>
        Integer,

        /// <summary>
        /// Decimal type of 13,4.
        /// </summary>
        Money,

        /// <summary>
        /// Decimal type with 2 decimal places and value between 0 and 100.
        /// </summary>
        Percentage,

        /// <summary>
        /// Decimal type. It's limits are given by the MaxContentLimit and the MaxDecimalLimit.
        /// </summary>
        Decimal,

        /// <summary>
        /// Regular expression type.
        /// </summary>
        RegEx
    }
}
