﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The type of the characters the textbox can accept if the "TextBoxControlValueType" is set to "Text"
    /// </summary>
    public enum TextBoxControlCharacterInputType
    {
        /// <summary>
        /// The textbox accepts any character.
        /// </summary>
        All,

        /// <summary>
        /// The textbox accepts only digits.
        /// </summary>
        Digits,

        /// <summary>
        /// The textbox accepts only letters.
        /// </summary>
        Letters
    }
}
