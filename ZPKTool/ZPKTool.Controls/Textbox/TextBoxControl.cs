﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Common;
using ZPKTool.Controls.Languages;

namespace ZPKTool.Controls
{
    /// <summary>
    /// This class extends the functionality of System.Windows.Controls.TextBox
    /// </summary>
    public class TextBoxControl : TextBox
    {
        #region Constants

        /// <summary>
        /// The default width of a text box that contains text.
        /// </summary>
        public const double DefaultWidthText = 180;

        /// <summary>
        /// The width of a text box that contains an integer.
        /// </summary>
        public const double DefaultWidthInteger = 90;

        /// <summary>
        /// The width of a text box that of input of type Money.
        /// </summary>
        public const double DefaultWidthMoney = 135;

        /// <summary>
        /// The width of a text box of input of type Percentage.
        /// </summary>
        public const double DefaultWidthPercentage = 60;

        /// <summary>
        /// The max width of the zoomed content popup.
        /// </summary>
        public const double DefaultZoomedContentPopupMaxWidth = 600;

        /// <summary>
        /// The max height of the zoomed content popup.
        /// </summary>
        public const double DefaultZoomedContentPopupMaxHeight = 300;

        #endregion Constants

        /// <summary>
        /// The popup that appears below the text box when an error occurs.
        /// </summary>
        private PopupControl errorPopup;

        /// <summary>
        /// The popup that displays the full text in bigger font size.
        /// </summary>
        private Popup zoomedContentPopup;

        /// <summary>
        /// The last valid value of the Text property. Used during the TextChanged event to undo an invalid value of the Text property; its value is updated at the end of the event.
        /// </summary>
        private string lastValidTextValue;

        /// <summary>
        /// Flag used to cancel a text changed event when internally restoring the last valid text after the current text has become invalid.
        /// </summary>
        private bool cancelTextChangedEventForRestoringText = false;

        /// <summary>
        /// If this action is defined it must be executed after the control is loaded in order to display an error popup.
        /// </summary>
        private Action showErrorPopupWhenLoaded;

        /// <summary>
        /// The window.
        /// </summary>
        private Window window;

        /// <summary>
        /// Initializes static members of the <see cref="TextBoxControl"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to access dependency properties from the base class.")]
        static TextBoxControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextBoxControl), new FrameworkPropertyMetadata(typeof(TextBoxControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxControl"/> class.
        /// </summary>
        public TextBoxControl()
        {
            this.FontSize = Constants.InputTextSize;
            this.InputBindings.Add(new KeyBinding(ApplicationCommands.NotACommand, Key.Z, ModifierKeys.Control));
            SetDefaultWidth();
            SetContentLimits();

            RegisterEventHandlers();
        }

        #region Dependency Properties

        /// <summary>
        /// Gets or sets a value indicating whether the default width is Auto or not. This is a dependency property.
        /// </summary>
        public bool AutoDefaultWidth
        {
            get { return (bool)GetValue(AutoDefaultWidthProperty); }
            set { SetValue(AutoDefaultWidthProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for AutoDefaultWidth. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AutoDefaultWidthProperty =
            DependencyProperty.Register("AutoDefaultWidth", typeof(bool), typeof(TextBoxControl), new UIPropertyMetadata(false));

        /// <summary>
        /// Gets or sets a value indicating whether the expression is validate or not. This is a dependency property.
        /// </summary>
        public string ValidationExpression
        {
            get { return (string)GetValue(ValidationExpressionProperty); }
            set { SetValue(ValidationExpressionProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ValidateExpression. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ValidationExpressionProperty =
            DependencyProperty.RegisterAttached("ValidationExpression", typeof(string), typeof(TextBoxControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the message to display when the regular expression is not valid.
        /// </summary>
        public string RegExValidationErrorMessage
        {
            get { return (string)GetValue(RegExValidationErrorMessageProperty); }
            set { SetValue(RegExValidationErrorMessageProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for RegExValidationErrorMessage. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty RegExValidationErrorMessageProperty =
            DependencyProperty.RegisterAttached("RegExValidationErrorMessage", typeof(string), typeof(TextBoxControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the necessity level. This is a dependency property.
        /// </summary>
        public TextBoxControlNecessityLevel Necessity
        {
            get { return (TextBoxControlNecessityLevel)GetValue(NecessityProperty); }
            set { SetValue(NecessityProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Necessity.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty NecessityProperty =
            DependencyProperty.Register("Necessity", typeof(TextBoxControlNecessityLevel), typeof(TextBoxControl), new UIPropertyMetadata(TextBoxControlNecessityLevel.Optional));

        /// <summary>
        /// Gets or sets the type of input the text box accepts. This is a dependency property.
        /// </summary>
        public TextBoxControlInputType InputType
        {
            get { return (TextBoxControlInputType)GetValue(InputTypeProperty); }
            set { SetValue(InputTypeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for InputType.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty InputTypeProperty =
            DependencyProperty.Register("InputType", typeof(TextBoxControlInputType), typeof(TextBoxControl), new UIPropertyMetadata(TextBoxControlInputType.Text100, InputTypeChangedCallback));

        /// <summary>
        /// Gets or sets the maximum limit on the control's content.
        /// For text input it represents the maximum number of allowed characters; a value of -1 signifies an unlimited number of characters).
        /// For numeric value input it represents the maximum allowed value. This is a dependency property.
        /// </summary>
        public decimal? MaxContentLimit
        {
            get { return (decimal?)GetValue(MaxContentLimitProperty); }
            set { SetValue(MaxContentLimitProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaxContentLimit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MaxContentLimitProperty =
            DependencyProperty.Register("MaxContentLimit", typeof(decimal?), typeof(TextBoxControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the minimum limit on the control's content.
        /// For text input it represents the minimum number of allowed characters. For numeric value input it represents the minimum allowed value.
        /// The content is not validated against this property value during user input.
        /// This is a dependency property.
        /// </summary>
        public decimal? MinContentLimit
        {
            get { return (decimal?)GetValue(MinContentLimitProperty); }
            set { SetValue(MinContentLimitProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for MinContentLimit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MinContentLimitProperty =
            DependencyProperty.Register("MinContentLimit", typeof(decimal?), typeof(TextBoxControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the maximum number of allowed decimal places, for input types that have decimal places.  This is a dependency property.
        /// </summary>
        public int MaxDecimalPlaces
        {
            get { return (int)GetValue(MaxDecimalPlacesProperty); }
            set { SetValue(MaxDecimalPlacesProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaxDecimalPlaces.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MaxDecimalPlacesProperty =
            DependencyProperty.Register("MaxDecimalPlaces", typeof(int), typeof(TextBoxControl), new UIPropertyMetadata(0));

        /// <summary>
        /// Gets or sets the type of the characters accepted when the InputType is a "Text" type. This is a dependency property.
        /// </summary>
        public TextBoxControlCharacterInputType AcceptedCharactersType
        {
            get { return (TextBoxControlCharacterInputType)GetValue(AcceptedCharactersTypeProperty); }
            set { SetValue(AcceptedCharactersTypeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for AcceptedCharactersType.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AcceptedCharactersTypeProperty =
            DependencyProperty.Register("AcceptedCharactersType", typeof(TextBoxControlCharacterInputType), typeof(TextBoxControl), new UIPropertyMetadata(TextBoxControlCharacterInputType.All));

        /// <summary>
        /// Gets or sets a value indicating whether the TextChanged event should be triggered.
        /// Useful to stop receiving the TextChanged event without unsubscribing your handler(s). This is a dependency property.
        /// </summary>
        public bool SuspendTextChangedEvent
        {
            get { return (bool)GetValue(SuspendTextChangedEventProperty); }
            set { SetValue(SuspendTextChangedEventProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for SuspendTextChangedEvent.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SuspendTextChangedEventProperty =
            DependencyProperty.Register("SuspendTextChangedEvent", typeof(bool), typeof(TextBoxControl), new UIPropertyMetadata(false));

        /// <summary>
        /// Gets or sets a value indicating whether this text box is a text area. This translates into a larger default width and height, content scrolling
        /// and accepting multiple lines of text. This is a dependency property.
        /// </summary>
        public bool IsTextArea
        {
            get { return (bool)GetValue(IsTextAreaProperty); }
            set { SetValue(IsTextAreaProperty, value); }
        }

        /// <summary>
        ///  Using a DependencyProperty as the backing store for SuspendTextChangedEvent.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsTextAreaProperty =
            DependencyProperty.Register("IsTextArea", typeof(bool), typeof(TextBoxControl), new UIPropertyMetadata(false, IsTextAreaChangedCallback));

        /// <summary>
        /// Gets or sets a value indicating whether the text box accepts an out of limit value initialization. This is a dependency property.
        /// </summary>
        public bool AllowOutOfLimitInitializer
        {
            get { return (bool)GetValue(AllowOutOfLimitInitializerProperty); }
            set { SetValue(AllowOutOfLimitInitializerProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for AllowOutOfLimitInitializer.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AllowOutOfLimitInitializerProperty =
            DependencyProperty.Register("AllowOutOfLimitInitializer", typeof(bool), typeof(TextBoxControl), new UIPropertyMetadata(false));

        #endregion Dependency Properties

        #region Public Methods

        /// <summary>
        /// Validate the content against all properties that restrict the input.
        /// </summary>
        /// <returns><c>true</c> if the content is valid; otherwise, <c>false</c>.</returns>
        public bool Validate()
        {
            // Disabled controls are always valid.
            if (!this.IsEnabled)
            {
                return true;
            }

            // A mandatory textbox without text is invalid
            if (string.IsNullOrWhiteSpace(this.Text) && this.Necessity == TextBoxControlNecessityLevel.Mandatory)
            {
                return false;
            }

            // Validate the content.
            bool inputIsValid = true;
            switch (this.InputType)
            {
                case TextBoxControlInputType.Text:
                case TextBoxControlInputType.Text25:
                case TextBoxControlInputType.Text50:
                case TextBoxControlInputType.Text100:
                case TextBoxControlInputType.Text2000:
                    inputIsValid = ValidateCurrentTextAsString(true);
                    break;

                case TextBoxControlInputType.Integer:
                case TextBoxControlInputType.Money:
                case TextBoxControlInputType.Percentage:
                case TextBoxControlInputType.Decimal:
                    inputIsValid = ValidateTextChangeAsNumber(this.Text, true);
                    break;

                case TextBoxControlInputType.RegEx:
                    inputIsValid = ValidateExpressionAsRegEx();
                    break;
            }

            return inputIsValid;
        }

        /// <summary>
        /// Shows an error popup next to the text box.
        /// </summary>
        /// <param name="message">The text that appears in the popup.</param>
        public void ShowErrorPopup(string message)
        {
            // the validation popup draw and placement is executed by the dispatcher
            Action action = () =>
                {
                    if (!this.IsLoaded)
                    {
                        this.showErrorPopupWhenLoaded = () => { ShowErrorPopup(message); };
                        return;
                    }

                    if (this.Visibility != Visibility.Visible)
                    {
                        return;
                    }

                    // if the popup wasn't used before create it
                    if (this.errorPopup == null)
                    {
                        BitmapImage image = new BitmapImage(new Uri("Resources/Images/Error.png", UriKind.Relative));
                        this.errorPopup = new PopupControl(message, image);
                    }
                    else
                    {
                        // else just set the message
                        this.errorPopup.Text = message;
                    }

                    this.errorPopup.PlacementTarget = this;
                    this.errorPopup.PopupAnimation = PopupAnimation.Fade;
                    this.errorPopup.Placement = PlacementMode.Bottom;
                    this.errorPopup.Placement = PlacementMode.Bottom;
                    this.errorPopup.AllowsTransparency = true;

                    double rectangleHeight = this.ActualHeight - 8 < 0 ? 0 : this.ActualHeight - 8;
                    this.errorPopup.PlacementRectangle = new Rect(0, 0, this.ActualWidth, rectangleHeight);

                    if (!this.errorPopup.IsOpen)
                    {
                        this.errorPopup.IsOpen = true;
                    }

                    Helper.FocusElement(this);
                };

            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input, action);
        }

        #endregion Public Methods

        #region Dependency Properties Callbacks

        /// <summary>
        /// Callback that is invoked when the value of the InputType dependency property has changed.
        /// </summary>
        /// <param name="d">The source dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void InputTypeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBoxControl textBox = d as TextBoxControl;
            if (textBox != null)
            {
                textBox.SetDefaultWidth();
                textBox.SetContentLimits();
            }
        }

        /// <summary>
        /// Invoked when the value of the IsTextArea dependency property has changed.
        /// </summary>
        /// <param name="d">The instance whose property value has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void IsTextAreaChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TextBoxControl textBox = d as TextBoxControl;
            bool newVal = (bool)e.NewValue;
            if (textBox != null)
            {
                textBox.AcceptsReturn = newVal;
                textBox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                textBox.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

                if (newVal)
                {
                    textBox.TextWrapping = TextWrapping.Wrap;
                }
                else
                {
                    textBox.TextWrapping = TextWrapping.NoWrap;
                }
            }
        }

        #endregion Dependency Properties Callbacks

        #region Event Handlers

        /// <summary>
        /// Registers event handlers to events of the base TextBox.
        /// </summary>
        private void RegisterEventHandlers()
        {
            this.IsVisibleChanged += (sender, e) => { HidePopup(); };
            this.Loaded += new RoutedEventHandler(TextBoxControl_Loaded);
        }

        /// <summary>
        /// Handles the Loaded event of the TextBoxControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void TextBoxControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.showErrorPopupWhenLoaded != null)
            {
                this.showErrorPopupWhenLoaded();
                this.showErrorPopupWhenLoaded = null;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Controls.Control.MouseDoubleClick"/> routed event.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            if (this.IsTextArea)
            {
                this.ShowZoomedContentPopup();
            }

            base.OnMouseDoubleClick(e);
        }

        #endregion Event Handlers

        #region Overridden Methods

        /// <summary>
        /// Invoked whenever an unhandled <see cref="E:System.Windows.Input.Keyboard.GotKeyboardFocus"/> attached routed event reaches an element derived from this class in its route. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">Provides data about the event.</param>
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            // Select the text in the control when receiving the keyboard focus, if it is not read-only.
            if (this.SelectionLength == 0 && !this.IsReadOnly)
            {
                this.SelectionStart = 0;
                this.SelectionLength = this.Text.Length;
            }

            base.OnGotKeyboardFocus(e);
        }

        /// <summary>
        /// Invoked whenever an unhandled <see cref="E:System.Windows.Input.Keyboard.LostKeyboardFocus"/> attached routed event reaches an element derived from this class in its route. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">Provides data about the event.</param>
        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            HidePopup();
            ApplyFormatToContent();

            base.OnLostKeyboardFocus(e);
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Keyboard.PreviewKeyDown"/> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.KeyEventArgs"/> that contains the event data.</param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            HidePopup();

            // If input value is valid then remove AllowOutOfLimitInitializer
            decimal value;
            if (!decimal.TryParse(this.Text, out value))
            {
                value = 0m;
            }

            if (this.AllowOutOfLimitInitializer
                && this.MaxContentLimit.HasValue
                && value < this.MaxContentLimit)
            {
                this.AllowOutOfLimitInitializer = false;
            }

            // Ignore key that do not modify the text box's text (like shift, alt, F1-F12, etc). They should be ignored so a "wrong input" error is avoided.
            if (ShouldIgnoreKey(e.Key, this.InputType))
            {
                e.Handled = true;
                return;
            }

            // Display "Read-Only" error message if the text box is read only and the pressed key would have changed its text.
            if (this.IsReadOnly
                && (e.Key != Key.Tab || this.AcceptsTab)
                && e.Key != Key.Left
                && e.Key != Key.Right
                && e.Key != Key.Home
                && e.Key != Key.End
                && e.Key != Key.PageUp
                && e.Key != Key.PageDown
                && !(e.Key == Key.A && (e.KeyboardDevice.Modifiers | ModifierKeys.Control) == ModifierKeys.Control)
                && !(e.Key == Key.C && (e.KeyboardDevice.Modifiers | ModifierKeys.Control) == ModifierKeys.Control))
            {
                ShowErrorPopup(LanguageResource.TextBoxControl_ReadOnly);
                e.Handled = true;
                return;
            }

            base.OnPreviewKeyDown(e);
        }

        /// <summary>
        /// Is called when content in this editing control changes.
        /// </summary>
        /// <param name="e">The arguments that are associated with the <see cref="E:System.Windows.Controls.Primitives.TextBoxBase.TextChanged"/> event.</param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (this.zoomedContentPopup != null)
            {
                this.HidePopup();
            }

            if (this.cancelTextChangedEventForRestoringText)
            {
                e.Handled = true;
                return;
            }

            TextChange change = e.Changes.FirstOrDefault();
            if (change != null)
            {
                if (!ValidateCurrentTextOnTextChange(change))
                {
                    // The current text is not valid. Undo all its changes and do not trigger the TextChange event.
                    try
                    {
                        this.cancelTextChangedEventForRestoringText = true;
                        this.Text = this.lastValidTextValue;
                        this.CaretIndex = change.Offset;
                    }
                    finally
                    {
                        this.cancelTextChangedEventForRestoringText = false;
                    }

                    e.Handled = true;
                    return;
                }
            }

            // The current text is valid, so record it as last valid value and trigger the TextChanged event.
            this.lastValidTextValue = this.Text;
            if (!this.SuspendTextChangedEvent)
            {
                base.OnTextChanged(e);
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.UIElement.LostFocus" /> event (using the provided arguments).
        /// </summary>
        /// <param name="e">Provides data about the event.</param>
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            if (this.InputType == TextBoxControlInputType.RegEx)
            {
                this.ValidateExpressionAsRegEx();
            }

            base.OnLostFocus(e);
        }

        /// <summary>
        /// Is called when a control template is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var zoomImg = this.Template.FindName("PART_ZoomImage", this) as Image;
            if (zoomImg != null)
            {
                zoomImg.MouseLeftButtonUp += new MouseButtonEventHandler((s, e) => { this.ShowZoomedContentPopup(); });
            }
        }

        #endregion Overridden Methods

        #region Content Validation

        /// <summary>
        /// Validates the current value of the Text property during the TextChanged event.
        /// </summary>
        /// <param name="change">The change that occurred in the text box.</param>
        /// <returns>
        /// True if the value is valid, false otherwise.
        /// </returns>
        protected bool ValidateCurrentTextOnTextChange(TextChange change)
        {
            bool textIsValid = true;

            switch (this.InputType)
            {
                case TextBoxControlInputType.Text:
                case TextBoxControlInputType.Text25:
                case TextBoxControlInputType.Text50:
                case TextBoxControlInputType.Text100:
                case TextBoxControlInputType.Text2000:
                    textIsValid = ValidateCurrentTextAsString(false);
                    break;

                case TextBoxControlInputType.Integer:
                case TextBoxControlInputType.Money:
                case TextBoxControlInputType.Percentage:
                case TextBoxControlInputType.Decimal:
                    {
                        string addedText = string.Empty;
                        if (change.Offset < this.Text.Length)
                        {
                            addedText = this.Text.Substring(change.Offset, change.AddedLength);
                        }

                        textIsValid = ValidateTextChangeAsNumber(addedText, true);
                        break;
                    }
            }

            return textIsValid;
        }

        /// <summary>
        /// Validates the current value of Text property as a string and displays any error as a pop-up message.
        /// </summary>
        /// <param name="validateMinLength">If set to true also validates the text against the MinContentLimit value.
        /// This should be set to false if the validation is performed during user input.</param>
        /// <returns>
        /// True if the Text is valid, false otherwise.
        /// </returns>
        private bool ValidateCurrentTextAsString(bool validateMinLength)
        {
            // Validate the length of the Text
            if (this.MaxContentLimit.HasValue && this.Text.Length > this.MaxContentLimit)
            {
                ShowErrorPopup(string.Format(LanguageResource.TextBoxControl_MaxTextLengthExceeded, MaxContentLimit));
                return false;
            }

            decimal minLimit = this.MinContentLimit.GetValueOrDefault() >= 0m ? this.MinContentLimit.GetValueOrDefault() : 0m;
            if (validateMinLength && this.Text.Length < minLimit)
            {
                ShowErrorPopup(string.Format(LanguageResource.TextBoxControl_MinTextLengthExceeded, minLimit));
                return false;
            }

            // Validate against the type of characters accepted by the text box.
            if (this.AcceptedCharactersType == TextBoxControlCharacterInputType.Letters)
            {
                foreach (char character in this.Text)
                {
                    if (!char.IsLetter(character))
                    {
                        ShowErrorPopup(LanguageResource.TextBoxControl_OnlyLettersAllowed);
                        return false;
                    }
                }
            }
            else if (this.AcceptedCharactersType == TextBoxControlCharacterInputType.Digits)
            {
                foreach (char character in this.Text)
                {
                    if (!char.IsDigit(character))
                    {
                        ShowErrorPopup(LanguageResource.TextBoxControl_OnlyDigitsAllowed);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the value of Text property as a regular expression and displays any error as a pop-up message.
        /// </summary>
        /// <returns>
        /// True if the Text is valid, false otherwise.
        /// </returns>
        private bool ValidateExpressionAsRegEx()
        {
            if (string.IsNullOrWhiteSpace(ValidationExpression))
            {
                return true;
            }

            Regex regularExpression = new Regex(ValidationExpression);
            if (!regularExpression.Match(this.Text).Success)
            {
                ShowErrorPopup(RegExValidationErrorMessage);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates the current value of Text property as a number.
        /// </summary>
        /// <param name="addedText">The text added to the text box. Set it to string.Empty if no text was added.</param>
        /// <param name="validateMinValue">If set to true also validates the current Text value against the MinContentLimit value.
        /// This should be set to false if the validation is performed during user input.</param>
        /// <returns>
        /// True if the Text is valid, false otherwise.
        /// </returns>
        //// TODO: Rework this method because it is too complex.
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Too risky to rework now.")]
        private bool ValidateTextChangeAsNumber(string addedText, bool validateMinValue)
        {
            // Empty text is always valid.
            if (string.IsNullOrEmpty(this.Text))
            {
                return true;
            }

            // Check if decimal places are allowed
            if (this.MaxDecimalPlaces <= 0 && this.Text.Contains(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            {
                ShowErrorPopup(LanguageResource.TextBoxControl_DecimalPlacesNotAllowed);
                return false;
            }

            int indexOfDecimalPoint = this.Text.IndexOf(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            int lastIndexOfDecimalPoint = this.Text.LastIndexOf(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            int indexOfGroupSeparator = this.Text.IndexOf(CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);
            int lastIndexOfGroupSeparator = this.Text.LastIndexOf(CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

            // Make sure the decimal point is present only once
            if (indexOfDecimalPoint != -1 && indexOfDecimalPoint != lastIndexOfDecimalPoint)
            {
                this.ShowErrorPopup(LanguageResource.TextBoxControl_SeparatorAlreadyPresent);
                return false;
            }

            // Check if the input contains only digits
            string cleanAddedText = addedText.Replace(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, string.Empty)
                .Replace(CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator, string.Empty);
            foreach (char c in cleanAddedText)
            {
                if (c.ToString() != CultureInfo.CurrentCulture.NumberFormat.NegativeSign
                    && !char.IsDigit(c))
                {
                    this.ShowErrorPopup(LanguageResource.TextBoxControl_OnlyDigitsOrSeparators);
                    return false;
                }
            }

            // Check if the decimal point appears after the last separator
            if (indexOfDecimalPoint != -1 && lastIndexOfGroupSeparator != -1 && indexOfDecimalPoint < lastIndexOfGroupSeparator)
            {
                this.ShowErrorPopup(LanguageResource.TextBoxControl_GroupSeparatorAfterDecimalSeparator);
                return false;
            }

            string textToConvert = this.Text;

            // If the decimal point is first in the content, remove it to avoid a conversion error
            if (indexOfDecimalPoint == 0)
            {
                textToConvert = textToConvert.Remove(indexOfDecimalPoint, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.Length);
            }

            // If there is a group separator on the first or last position in the content, remove it to avoid a conversion error
            if (indexOfGroupSeparator == 0)
            {
                textToConvert = textToConvert.Remove(0, CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator.Length);
            }
            else if (lastIndexOfGroupSeparator == this.Text.Length - CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator.Length)
            {
                textToConvert = textToConvert.Remove(lastIndexOfGroupSeparator, CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator.Length);
            }

            if (string.IsNullOrEmpty(textToConvert) || textToConvert == CultureInfo.CurrentCulture.NumberFormat.NegativeSign)
            {
                return true;
            }

            // Check if the negative sign is not the first character in text.
            if (textToConvert.Length > 0)
            {
                for (int i = 1; i <= textToConvert.Length - 1; i++)
                {
                    if (textToConvert[i].ToString() == CultureInfo.CurrentCulture.NumberFormat.NegativeSign)
                    {
                        this.ShowErrorPopup(LanguageResource.TextBoxControl_NegativeSignPozition);
                        return false;
                    }
                }
            }

            // Check if the input is a valid number
            decimal value;
            if (!decimal.TryParse(textToConvert, out value))
            {
                this.ShowErrorPopup("Invalid content.");
                return false;
            }

            // Get the textbox minimum Limit
            decimal? minLimit = this.MinContentLimit ?? -1m * this.MaxContentLimit;

            // Check if the input is not larger than the maximum allowed value
            if (this.MaxContentLimit.HasValue && value > this.MaxContentLimit)
            {
                if (!this.AllowOutOfLimitInitializer &&
                    !string.IsNullOrEmpty(addedText))
                {
                    this.ShowErrorPopup(string.Format(
                         LanguageResource.TextBoxControl_MaxValueExceeded,
                         Formatter.FormatNumber(this.MaxContentLimit, 4)));
                    return false;
                }
                else
                {
                    this.AllowOutOfLimitInitializer = false;
                }
            }
            else if (this.AllowOutOfLimitInitializer
                && value > minLimit)
            {
                this.AllowOutOfLimitInitializer = false;
            }

            // Check if the input is not smaller than the minimum allowed value
            if (validateMinValue && value < minLimit)
            {
                if (!this.AllowOutOfLimitInitializer)
                {
                    var decimalsCount = minLimit.ToString().SkipWhile(d => d != '.').Skip(1).Count();
                    ShowErrorPopup(string.Format(LanguageResource.TextBoxControl_MinValueExceeded, Formatter.FormatNumber(minLimit, decimalsCount)));
                    return false;
                }
                else
                {
                    this.AllowOutOfLimitInitializer = false;
                }
            }
            else if (this.AllowOutOfLimitInitializer)
            {
                this.AllowOutOfLimitInitializer = false;
            }

            // Check if the inputted decimal places do not exceed the maximum number of decimal places
            if (this.MaxDecimalPlaces > 0)
            {
                if (this.Text.Contains(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
                {
                    string[] parts = this.Text.Split(new string[] { CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator }, StringSplitOptions.None);
                    if (parts.Length > 1 && parts[1].Length > this.MaxDecimalPlaces)
                    {
                        this.ShowErrorPopup(string.Format(LanguageResource.TextBoxControl_MaxDecimalPlacesExceeded, this.MaxDecimalPlaces));
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion Content Validation

        #region Utils

        /// <summary>
        /// Checks if a key press should be ignored in a textbox with a specified input type.
        /// </summary>
        /// <param name="key">The pressed key.</param>
        /// <param name="inputType">The type of the value inside a text box.</param>
        /// <returns>True if it must be ignored, false otherwise</returns>
        private bool ShouldIgnoreKey(Key key, TextBoxControlInputType inputType)
        {
            if (key == Key.Space
                && inputType != TextBoxControlInputType.Text100
                && inputType != TextBoxControlInputType.Text2000
                && inputType != TextBoxControlInputType.Text50
                && inputType != TextBoxControlInputType.Text25
                && inputType != TextBoxControlInputType.Text)
            {
                return true;
            }

            if ((key >= Key.F1 && key <= Key.RightAlt)
                || (key >= Key.LWin && key <= Key.Sleep)
                || key == Key.CapsLock
                || key == Key.Escape
                || key == Key.Pause
                || key == Key.PrintScreen
                || key == Key.System)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Hides the error popup that is displayed for the text box.
        /// </summary>
        private void HidePopup()
        {
            if (this.errorPopup != null && this.errorPopup.IsOpen)
            {
                this.errorPopup.IsOpen = false;
            }

            if (this.zoomedContentPopup != null && this.zoomedContentPopup.IsOpen)
            {
                this.zoomedContentPopup.IsOpen = false;
                this.zoomedContentPopup = null;
                this.window.LocationChanged -= (s, e) => { };
                this.LayoutUpdated -= (s, e) => { };
            }
        }

        /// <summary>
        /// Sets the width property according to the input type of this instance.
        /// </summary>
        private void SetDefaultWidth()
        {
            if (this.AutoDefaultWidth)
            {
                this.Width = double.NaN;
                return;
            }

            switch (this.InputType)
            {
                case TextBoxControlInputType.Text:
                case TextBoxControlInputType.Text25:
                case TextBoxControlInputType.Text50:
                case TextBoxControlInputType.Text100:
                    this.Width = DefaultWidthText;
                    break;

                case TextBoxControlInputType.Text2000:
                    this.Width = double.NaN;
                    break;

                case TextBoxControlInputType.Integer:
                    this.Width = DefaultWidthInteger;
                    break;

                case TextBoxControlInputType.Money:
                    this.Width = DefaultWidthMoney;
                    break;

                case TextBoxControlInputType.Percentage:
                    this.Width = DefaultWidthPercentage;
                    break;

                case TextBoxControlInputType.Decimal:
                    this.Width = DefaultWidthMoney;
                    break;
            }
        }

        /// <summary>
        /// Sets the content limits according to the input type of this instance.
        /// Doesn't write over the previously set values.
        /// </summary>
        private void SetContentLimits()
        {
            // Set the maximum limit
            switch (this.InputType)
            {
                case TextBoxControlInputType.Text:
                    this.MaxContentLimit = null;
                    break;

                case TextBoxControlInputType.Text25:
                    this.MaxContentLimit = 25m;
                    break;

                case TextBoxControlInputType.Text50:
                    this.MaxContentLimit = 50m;
                    break;

                case TextBoxControlInputType.Text100:
                    this.MaxContentLimit = 100m;
                    break;

                case TextBoxControlInputType.Text2000:
                    this.MaxContentLimit = 2000m;
                    break;

                case TextBoxControlInputType.Integer:
                    this.MaxContentLimit = Constants.MaxNumberValue;
                    this.MaxDecimalPlaces = 0;
                    break;

                case TextBoxControlInputType.Money:
                    this.MaxContentLimit = Constants.MaxNumberValue;
                    this.MaxDecimalPlaces = UserSettingsManager.Instance.DecimalPlacesNumber;
                    break;

                case TextBoxControlInputType.Decimal:
                    MaxContentLimit = Constants.MaxNumberValue;
                    this.MaxDecimalPlaces = UserSettingsManager.Instance.DecimalPlacesNumber;
                    break;

                case TextBoxControlInputType.Percentage:
                    this.MaxContentLimit = 100m;
                    this.MaxDecimalPlaces = 2;
                    break;
            }
        }

        /// <summary>
        /// Formats the content of the text box.
        /// </summary>
        private void ApplyFormatToContent()
        {
            string formattedText = string.Empty;

            // set the content format according to the value type and system format
            switch (InputType)
            {
                case TextBoxControlInputType.Text:
                case TextBoxControlInputType.Text25:
                case TextBoxControlInputType.Text50:
                case TextBoxControlInputType.Text100:
                case TextBoxControlInputType.Text2000:
                    formattedText = this.Text;
                    break;

                case TextBoxControlInputType.Decimal:
                case TextBoxControlInputType.Integer:
                case TextBoxControlInputType.Percentage:
                case TextBoxControlInputType.Money:
                    {
                        decimal convertedVal;
                        if (decimal.TryParse(this.Text, out convertedVal))
                        {
                            formattedText = Formatter.FormatNumber(convertedVal, this.MaxDecimalPlaces);
                        }

                        break;
                    }

                case TextBoxControlInputType.RegEx:
                    formattedText = this.Text;
                    break;

                default:
                    break;
            }

            if (this.Text != formattedText)
            {
                try
                {
                    this.cancelTextChangedEventForRestoringText = true;
                    this.Text = formattedText;
                }
                finally
                {
                    this.cancelTextChangedEventForRestoringText = false;
                }
            }
        }

        /// <summary>
        /// Shows a bigger text popup.
        /// </summary>
        private void ShowZoomedContentPopup()
        {
            if (string.IsNullOrEmpty(this.Text))
            {
                return;
            }

            double minPopupWidth = 100d;
            double minPopupHeight = 50d;

            if (this.zoomedContentPopup == null)
            {
                // the popup will contain: border -> stackPanel -> textBlock and closeImage
                this.zoomedContentPopup = new Popup();

                StackPanel stackPanel = new StackPanel()
                {
                    MaxHeight = DefaultZoomedContentPopupMaxHeight,
                    MinHeight = minPopupHeight,
                    Margin = new Thickness(5d, 5d, 0d, 5d),
                    Orientation = Orientation.Horizontal,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                };

                TextBlock textBlock = new TextBlock()
                {
                    MaxWidth = DefaultZoomedContentPopupMaxWidth,
                    MinWidth = minPopupWidth,
                    Margin = new Thickness(5d, 5d, 0d, 5d),
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = this.FontSize + 5.0d,
                    Text = this.Text
                };

                Image closeImage = new Image()
                {
                    Margin = new Thickness(5d),
                    Opacity = 1.0d,
                    Width = 10d,
                    Height = 10d,
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Source = new BitmapImage(new Uri("pack://application:,,,/Resources/Images/X_black.png", UriKind.Absolute))
                };
                closeImage.MouseUp += new MouseButtonEventHandler((s, e) => { this.HidePopup(); });

                // add the textBlock (wrapped by a ScrollViewer) to the stackPanel
                stackPanel.Children.Add(new ScrollViewer()
                {
                    VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                    HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled,
                    Content = textBlock
                });

                // add the closeImage to the stackPanel
                stackPanel.Children.Add(closeImage);

                // add the stackPanel (wrapped by a rounded border) to the popup control
                this.zoomedContentPopup.Child = new Border()
                {
                    BorderThickness = new Thickness(1d),
                    BorderBrush = Brushes.Black,
                    CornerRadius = new CornerRadius(15d),
                    Background = new SolidColorBrush(Color.FromRgb(252, 234, 178)),
                    SnapsToDevicePixels = true,
                    Child = stackPanel
                };

                this.zoomedContentPopup.PlacementTarget = this;
                this.zoomedContentPopup.PopupAnimation = PopupAnimation.Fade;
                this.zoomedContentPopup.AllowsTransparency = true;
                this.zoomedContentPopup.LayoutTransform = new ScaleTransform(UserSettingsManager.Instance.ZoomLevel, UserSettingsManager.Instance.ZoomLevel);

                this.window = Window.GetWindow(this.Parent);
                if (this.window != null)
                {
                    this.window.LocationChanged += (s, e) =>
                    {
                        this.RepositionZoomedContentPopup();
                    };
                }

                this.LayoutUpdated += (s, e) =>
                {
                    this.RepositionZoomedContentPopup();
                };
            }

            // show the popup
            if (!this.zoomedContentPopup.IsOpen)
            {
                this.zoomedContentPopup.IsOpen = true;
                this.RepositionZoomedContentPopup();
            }
        }

        /// <summary>
        /// Reposition the zoomed content popup.
        /// </summary>
        private void RepositionZoomedContentPopup()
        {
            if (this.zoomedContentPopup != null)
            {
                // Force the popup to reposition itself.
                this.zoomedContentPopup.HorizontalOffset += 1;
                this.zoomedContentPopup.HorizontalOffset -= 1;

                // Set the placement depending on the available space.
                double actualPopupWidth = this.zoomedContentPopup.Child.DesiredSize.Width;
                double actualPopupHeight = this.zoomedContentPopup.Child.DesiredSize.Height;

                Point textBoxCoordinates = this.TranslatePoint(new Point(0, 0), this.window);
                textBoxCoordinates.X += this.window.Left;
                textBoxCoordinates.Y += this.window.Top;

                if (textBoxCoordinates.X > this.window.Left && textBoxCoordinates.Y - actualPopupHeight > this.window.Top && textBoxCoordinates.X + actualPopupWidth < this.window.Left + this.window.ActualWidth && textBoxCoordinates.Y < this.window.Top + this.window.ActualHeight)
                {
                    this.zoomedContentPopup.PlacementTarget = this;
                    this.zoomedContentPopup.Placement = PlacementMode.Top;
                }
                else if (textBoxCoordinates.X - actualPopupWidth > this.window.Left && textBoxCoordinates.Y > this.window.Top && textBoxCoordinates.X < this.window.Left + this.window.ActualWidth && textBoxCoordinates.Y + actualPopupHeight < this.window.Top + this.window.ActualHeight)
                {
                    this.zoomedContentPopup.PlacementTarget = this;
                    this.zoomedContentPopup.Placement = PlacementMode.Left;
                }
                else if (textBoxCoordinates.X > this.window.Left && textBoxCoordinates.Y + this.ActualHeight + actualPopupHeight < this.window.Top + this.window.ActualHeight && textBoxCoordinates.X + actualPopupWidth < this.window.Left + this.window.ActualWidth && textBoxCoordinates.Y + this.ActualHeight + actualPopupHeight < this.window.Top + this.window.ActualHeight)
                {
                    this.zoomedContentPopup.PlacementTarget = this;
                    this.zoomedContentPopup.Placement = PlacementMode.Bottom;
                }
                else if (textBoxCoordinates.X + this.ActualWidth > this.window.Left && textBoxCoordinates.Y > this.window.Top && textBoxCoordinates.X + this.ActualWidth + actualPopupWidth < this.window.Left + this.window.ActualWidth && textBoxCoordinates.Y + actualPopupHeight < this.window.Top + this.window.ActualHeight)
                {
                    this.zoomedContentPopup.PlacementTarget = this;
                    this.zoomedContentPopup.Placement = PlacementMode.Right;
                }
                else
                {
                    this.zoomedContentPopup.PlacementTarget = this.window;
                    this.zoomedContentPopup.Placement = PlacementMode.Center;
                }
            }
        }

        #endregion Utils
    }
}