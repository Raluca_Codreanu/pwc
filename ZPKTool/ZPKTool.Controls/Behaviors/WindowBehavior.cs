﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Implements the following attached behaviors for the <see cref="Window"/> class:
    /// <para/>
    /// - MinimizeParentToo: when enabled, the window's parent is minimized together with the window when the window is modal (and obviously when the window is minimized).
    /// </summary>
    public static class WindowBehavior
    {
        /// <summary>
        /// A DependencyProperty used as the backing store for MinimizeParentToo.
        /// </summary>
        public static readonly DependencyProperty MinimizeParentTooProperty = DependencyProperty.RegisterAttached(
            "MinimizeParentToo",
            typeof(bool),
            typeof(WindowBehavior),
            new PropertyMetadata(false, OnMinimizeParentTooChanged));

        /// <summary>
        /// Gets a value indicating whether the parent window should be minimized together with the attached window when the attached window is modal
        /// and obviously, when the attached window is minimized.
        /// </summary>
        /// <param name="obj">The object whose property value to get.</param>
        /// <returns>The value of the property attached to the specified object.</returns>
        public static bool GetMinimizeParentToo(DependencyObject obj)
        {
            return (bool)obj.GetValue(MinimizeParentTooProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the parent window should be minimized together with the attached window when the attached window is modal
        /// and obviously, when the attached window is minimized.
        /// </summary>
        /// <param name="obj">The object whose property value to set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetMinimizeParentToo(DependencyObject obj, bool value)
        {
            obj.SetValue(MinimizeParentTooProperty, value);
        }

        /// <summary>
        /// Called when the <see cref="MinimizeParentTooProperty"/> has changed.
        /// </summary>
        /// <param name="obj">The object for which the property value has changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnMinimizeParentTooChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var window = obj as Window;
            if (window == null)
            {
                throw new InvalidOperationException("The MinimizeParentToo behavior can be attached only to instances of Window or derived classes.");
            }

            if ((bool)e.NewValue)
            {
                window.StateChanged += HandleWindowStateChangeForMinimizingParent;
            }
            else
            {
                window.StateChanged -= HandleWindowStateChangeForMinimizingParent;
            }
        }

        /// <summary>
        /// Handles the StateChanged event of a window with the purpose of minimizing its parent together with it based on the
        /// value of the <see cref="MinimizeParentTooProperty"/> attached property.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void HandleWindowStateChangeForMinimizingParent(object sender, EventArgs e)
        {
            var window = sender as Window;
            if (window == null || !GetMinimizeParentToo(window))
            {
                // Do nothing if the window does not have the MinimizeParentToo property set to true or, for some reason, the sender parameter is not a window.
                return;
            }

            // Minimize the window's owner too if it has one, the window has been minimized and the window is modal.
            if (window.Owner != null
                && window.WindowState == WindowState.Minimized
                && IsModal(window))
            {
                window.Owner.WindowState = WindowState.Minimized;
            }
        }

        /// <summary>
        /// Determines whether the specified window is modal.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <returns>
        /// true if the specified window is modal; otherwise, false.
        /// </returns>
        private static bool IsModal(Window window)
        {
            var hiddedIsModalField = typeof(Window).GetField("_showingAsDialog", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if (hiddedIsModalField != null
                && hiddedIsModalField.FieldType == typeof(bool))
            {
                return (bool)hiddedIsModalField.GetValue(window);
            }
            else
            {
#if DEBUG
                throw new InvalidOperationException("The '_showingAsDialog' private field of Window was not found. This code needs to be updated.");
#else
                return false;
#endif
            }
        }
    }
}
