﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ZPKTool.Controls
{
    /// <summary>
    /// The Relative Position On Item enum.
    /// </summary>
    public enum RelativePositionOnItem
    {
        /// <summary>
        /// RelativePositionOnItem Legend.
        /// </summary>
        /// ____ITEM__________
        /// |___Top____________|
        /// |___Center_________|
        /// |___Bottom_________|
        Top = 0,

        /// <summary>
        /// The Center position.
        /// </summary>
        Center = 1,

        /// <summary>
        /// The Bottom position.
        /// </summary>
        Bottom = 2,

        /// <summary>
        /// Not on item position.
        /// </summary>
        NotOnItem = 3
    }

    /// <summary> 
    /// This static class contains methods that can be used to handle drag and drop on a ItemsControl control.
    /// </summary>
    /// <remarks>
    /// First of all, the ItemsControl must have set to True the Property "AllowDrop".
    /// After that there must be wired up the following Events of the ItemsControl:
    /// Button Events :
    /// - PreviewMouseLeftButtonUp
    /// - PreviewMouseMove
    /// - PreviewMouseLeftButtonDown
    /// Drag Events:
    /// - DragLeave
    /// - DragOver
    /// - DragEnter
    /// - QueryContinueDrag
    /// - Drop     
    /// * DragEnter -- happens when cursor/mouse enters;  useful for things like "highlighting" 
    ///                the target by showing an adorner, or similar ...     
    ///                You would think that when mouseEnter, you can query the data object and provide feedback 
    ///                on whether the drop would be allowed... Well, you can, but it is pretty useless. 
    ///                DragOver can override that, so the place to handle the feedback is on DragOver.. 
    /// * DragLeave -- happens when cursor leaves..  Here you should undo any thing you did on DragEnter .. 
    /// * DragOver -- here you can provide feedback to whether the drop would succeed .. 
    /// --
    /// Dragging is initiated by calling the DoDragDrop method for the source control. 
    /// --
    /// The DoDragDrop method takes two parameters: 
    ///  - data, specifying the data to pass 
    ///  - allowedEffects, specifying which operations (copying and/or moving) are allowed 
    ///  A new DataObject object is automatically created. 
    ///  This in turn raises the GiveFeedback event. In most cases you do not need to worry about the GiveFeedback event, but if you 
    ///  wanted to display a custom mouse pointer during the drag, this is where you would add your code. 
    ///     Any control with its AllowDrop property set to True is a potential drop target. 
    /// As the mouse passes over each control, the DragEnter event for that control is raised. 
    ///     The GetDataPresent method is used to make sure that the format of the data is appropriate to the 
    /// target control, and the Effect property is used to display the appropriate mouse pointer. 
    /// If the user releases the mouse button over a valid drop target, the DragDrop event is raised. 
    /// Code in the DragDrop event handler extracts the data from the DataObject object and displays it in the target control.
    /// <para/>
    /// AFTER Handling the drop it is recommended to call: CollectionViewSource.GetDefaultView(YourDataGrid.ItemsSource).Refresh();
    /// </remarks>
    public static class DragAndDropHelper
    {
        #region Fields

        /// <summary>
        /// The drag wait counter limit.
        /// </summary>
        private const int DragWaitCounterLimit = 10;

        /// <summary>
        /// Is dragging indicator flag.
        /// </summary>
        private static bool isDragging;

        /// <summary>
        /// The data contained.
        /// </summary>
        private static object data;

        /// <summary>
        /// The drag start position.
        /// </summary>
        private static Point dragStartPosition;

        /// <summary>
        /// The drag adorner.
        /// </summary>
        private static DragAdorner dragAdorner;

        /// <summary>
        /// The insert adorner.
        /// </summary>
        private static InsertAdorner insertAdorner;

        /// <summary>
        /// the drag scroll wait counter.
        /// </summary>
        private static int dragScrollWaitCounter = DragWaitCounterLimit;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the drag image.
        /// </summary>
        /// <value>
        /// The drag image.
        /// </value>
        public static BitmapFrame DragImage { get; set; }

        #endregion

        #region DataGrid Button Events

        /// <summary>
        /// Handles the PreviewMouseMove event of the itemsControl control.
        /// </summary>
        /// <param name="itemType">Type of the item.</param>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        public static void DataGrid_PreviewMouseMove(Type itemType, DataGrid dataGrid, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !isDragging)
            {
                if (dataGrid != null)
                {
                    Point currentPosition = e.GetPosition(dataGrid);
                    if ((Math.Abs(currentPosition.X - dragStartPosition.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                        (Math.Abs(currentPosition.Y - dragStartPosition.Y) > SystemParameters.MinimumVerticalDragDistance))
                    {
                        DragStarted(itemType, dataGrid);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the itemsControl control.
        /// </summary>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter types must be enforced.")]
        public static void DataGrid_PreviewMouseLeftButtonDown(DataGrid dataGrid, MouseButtonEventArgs e)
        {
            if (dataGrid != null)
            {
                dragStartPosition = e.GetPosition(dataGrid);
            }
        }

        #endregion

        #region DataGrid Drag Drop Events

        /// <summary>
        /// Handles the DragLeave event of the ItemsControl control.
        /// </summary>
        /// <param name="itemType">Type of the data present in the drag event arguments.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        public static void DataGrid_DragLeave(Type itemType, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(itemType))
            {
                if (insertAdorner != null)
                {
                    insertAdorner.Remove();
                    insertAdorner = null;
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Handles the PreviewDragOver event of the ItemsControl control.
        /// </summary>
        /// <param name="itemType">Type of the data present in the drag event arguments.</param>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter type must be enforced.")]
        public static void DataGrid_DragOver(Type itemType, DataGrid dataGrid, DragEventArgs e)
        {
            if (dataGrid != null)
            {
                if (e.Data.GetDataPresent(itemType))
                {
                    UpdateInsertAdorner(dataGrid, e);
                    UpdateDragAdorner(e.GetPosition(dataGrid));
                    HandleDragScrolling(dataGrid, e);
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// Handles the PreviewDragEnter event of the DataGrid control.
        /// </summary>
        /// <param name="itemType">Type of the data present in the drag event arguments.</param>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter type must be enforced.")]
        public static void DataGrid_DragEnter(Type itemType, DataGrid dataGrid, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(itemType))
            {
                if (isDragging)
                {
                    if (dataGrid != null)
                    {
                        InitializeInsertAdorner(dataGrid, e);
                        InitializeDragAdorner(dataGrid, DragImage, e.GetPosition(dataGrid));
                    }
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the PreviewDrop event of the ItemsControl control.
        /// </summary>
        /// <typeparam name="T">The item type from the data grid source.</typeparam>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "This syntax is more convenient than other alternatives.")]
        public static void DataGrid_Drop<T>(DataGrid dataGrid, DragEventArgs e) where T : class
        {
            var itemType = typeof(T);

            if (isDragging == true
                && dataGrid != null)
            {
                if (e.Data.GetDataPresent(itemType))
                {
                    var insertionIndex = FindInsertionIndex(dataGrid, e);
                    if (insertionIndex.HasValue)
                    {
                        var draggedItemContainer = GetItemContainerFromPoint(dataGrid, dragStartPosition) as DataGridRow;
                        var itemToRemove = (draggedItemContainer != null ? draggedItemContainer.Item : data) as T;

                        // Move the item to its new position into the collection.
                        // If the collection is observable we can move it, else we need to use remove and insert it.
                        var gridItems = dataGrid.ItemsSource as ObservableCollection<T>;
                        if (gridItems != null)
                        {
                            var currentIndex = gridItems.IndexOf(itemToRemove);
                            gridItems.Move(currentIndex, insertionIndex.Value);
                        }
                        else
                        {
                            RemoveItem(dataGrid, itemToRemove);

                            var itemToAdd = e.Data.GetData(itemType);
                            AddItem(dataGrid, itemToAdd, insertionIndex.Value);
                        }

                        dataGrid.Items.Refresh();
                        e.Effects = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;
                    }
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }

                CollectionViewSource.GetDefaultView(dataGrid.ItemsSource).Refresh();
                e.Handled = true;
            }
        }

        #endregion DataGrid Drag Drop Events

        #region Drag Events

        /// <summary>
        /// Handles the PreviewQueryContinueDrag event of the ItemsControl control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        public static void QueryContinueDrag(QueryContinueDragEventArgs e)
        {
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
                ResetState();
                RemoveAdorners();
                e.Handled = true;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Drags the started.
        /// </summary>
        /// <param name="itemType">Type of the item.</param>
        /// <param name="dataGrid">The items control.</param>
        public static void DragStarted(Type itemType, DataGrid dataGrid)
        {
            DataGridRow row = null;
            data = GetDataObjectFromItemsControl(dataGrid, dragStartPosition, ref row);

            if (data != null)
            {
                if (data.GetType() != itemType)
                {
                    return;
                }

                if (row != null)
                {
                    using (var stream = row.GenerateImagePng(1))
                    {
                        DragImage = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                    }
                }

                isDragging = true;
                DataObject dataObject = new DataObject(itemType, data);
                DragDrop.DoDragDrop(dataGrid, dataObject, DragDropEffects.Move);

                RemoveAdorners();
                ResetState();
            }
        }

        /// <summary>
        /// Resets the state of the drag operation.
        /// </summary>
        public static void ResetState()
        {
            isDragging = false;
            DragImage = null;
            data = null;
            dragScrollWaitCounter = DragWaitCounterLimit;
        }

        #endregion

        #region Initialize adorners

        /// <summary>
        /// Initializes the insert adorner.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        public static void InitializeInsertAdorner(ItemsControl itemsControl, DragEventArgs e)
        {
            if (insertAdorner == null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(itemsControl);
                UIElement itemContainer = GetItemContainerFromPoint(itemsControl, e.GetPosition(itemsControl));
                if (itemContainer != null)
                {
                    RelativePositionOnItem position = GetRelativePositionOnItem(itemsControl, e);

                    if (position == RelativePositionOnItem.Top)
                    {
                        insertAdorner = new InsertAdorner(true, true, itemContainer, adornerLayer);
                    }
                    else if (position == RelativePositionOnItem.Bottom)
                    {
                        insertAdorner = new InsertAdorner(false, true, itemContainer, adornerLayer);
                    }
                }
            }
        }

        /// <summary>
        /// Initializes the drag adorner.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="dragData">The drag data.</param>
        /// <param name="startPosition">The start position.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter type must be enforced.")]
        public static void InitializeDragAdorner(ItemsControl itemsControl, BitmapFrame dragData, Point startPosition)
        {
            if (dragAdorner == null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(itemsControl);
                dragAdorner = new DragAdorner(dragData, "AdornerDragDataTemplate", itemsControl, adornerLayer);

                dragAdorner.UpdatePosition(startPosition.X, startPosition.Y);
            }
        }

        #endregion Initialize adorners

        #region Update adorners

        /// <summary>
        /// Updates the drag adorner.
        /// </summary>
        /// <param name="currentPosition">The current position.</param>
        public static void UpdateDragAdorner(Point currentPosition)
        {
            if (dragAdorner != null)
            {
                dragAdorner.UpdatePosition(currentPosition.X, currentPosition.Y);
            }
        }

        /// <summary>
        /// Updates the insert adorner.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        public static void UpdateInsertAdorner(ItemsControl itemsControl, DragEventArgs e)
        {
            if (insertAdorner != null)
            {
                RelativePositionOnItem position = GetRelativePositionOnItem(itemsControl, e);

                if (position == RelativePositionOnItem.Top)
                {
                    insertAdorner.IsTopHalf = true;
                }
                else if (position == RelativePositionOnItem.Bottom)
                {
                    insertAdorner.IsTopHalf = false;
                }

                insertAdorner.InvalidateVisual();
            }
        }

        #endregion

        #region Remove adorners

        /// <summary>
        /// Detaches the adorners.
        /// </summary>
        public static void RemoveAdorners()
        {
            if (insertAdorner != null)
            {
                insertAdorner.Remove();
                insertAdorner = null;
            }

            if (dragAdorner != null)
            {
                dragAdorner.Remove();
                dragAdorner = null;
            }
        }

        /// <summary>
        /// Detaches the insert adorner.
        /// </summary>
        public static void RemoveInsertAdorner()
        {
            if (insertAdorner != null)
            {
                insertAdorner.Remove();
                insertAdorner = null;
            }
        }

        #endregion

        #region Handle Scrolling

        /// <summary>
        /// Handles the drag scrolling.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        public static void HandleDragScrolling(ItemsControl itemsControl, DragEventArgs e)
        {
            bool? isMouseAtTop = IsMousePointerAtTop(itemsControl, e.GetPosition(itemsControl));
            if (isMouseAtTop.HasValue)
            {
                if (dragScrollWaitCounter == DragWaitCounterLimit)
                {
                    dragScrollWaitCounter = 0;
                    ScrollViewer scrollViewer = FindScrollViewer(itemsControl);
                    if (scrollViewer != null && scrollViewer.CanContentScroll
                        && scrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Visible)
                    {
                        if (isMouseAtTop.Value)
                        {
                            scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - 1.0);
                        }
                        else
                        {
                            scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset + 1.0);
                        }

                        e.Effects = DragDropEffects.Scroll;
                    }
                }
                else
                {
                    dragScrollWaitCounter++;
                }
            }
            else
            {
                e.Effects = DragDropEffects.Move;
            }
        }

        #endregion

        #region Find Insertion Index

        /// <summary>
        /// Finds the index of the insertion.
        /// </summary>
        /// <param name="dataGrid">The items control.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <returns>The insert index.</returns>
        private static int? FindInsertionIndex(DataGrid dataGrid, DragEventArgs e)
        {
            int? index = null;

            // Find sort direction.
            ListSortDirection direction = ListSortDirection.Ascending;

            ICollectionView view = CollectionViewSource.GetDefaultView(dataGrid.ItemsSource);
            List<SortDescription> sortDescriptions = view.SortDescriptions.ToList();

            foreach (SortDescription description in sortDescriptions)
            {
                direction = description.Direction;
            }

            DataGridRow dataGridRow = GetItemContainerFromPoint(dataGrid, e.GetPosition(dataGrid)) as DataGridRow;

            if (dataGridRow != null)
            {
                IList list = dataGrid.ItemsSource as IList;
                if (list != null)
                {
                    index = list.IndexOf(dataGridRow.Item);
                }
                else
                {
                    Type listType = typeof(IList<>);
                    Type itemControlType = dataGrid.ItemsSource.GetType();
                    Type[] interfaces = itemControlType.GetInterfaces();
                    bool isIListOfT = false;
                    Type genericType;
                    foreach (Type type in interfaces)
                    {
                        if (!type.IsGenericType)
                        {
                            continue;
                        }

                        genericType = type.GetGenericTypeDefinition();
                        if (genericType != listType)
                        {
                            continue;
                        }

                        isIListOfT = true;
                        break;
                    }

                    if (isIListOfT == true)
                    {
                        index = (int)itemControlType.GetMethod("IndexOf").Invoke(dataGrid.ItemsSource, new object[] { dataGridRow.Item });
                    }
                }

                RelativePositionOnItem position = GetRelativePositionOnItem(dataGrid, e);

                if (position == RelativePositionOnItem.Top || position == RelativePositionOnItem.Center)
                {
                    if (direction == ListSortDirection.Ascending)
                    {
                        // keep the calculated index.
                    }
                    else if (direction == ListSortDirection.Descending)
                    {
                        index = index + 1;
                    }
                }
                else if (position == RelativePositionOnItem.Bottom)
                {
                    if (direction == ListSortDirection.Ascending)
                    {
                        index = index + 1;
                    }
                    else if (direction == ListSortDirection.Descending)
                    {
                        // keep the calculated index.
                    }
                }
            }

            if (index > dataGrid.Items.Count - 1)
            {
                return dataGrid.Items.Count - 1;
            }
            else
            {
                return index;
            }
        }

        /// <summary>
        /// Finds the index where to insert the specified tree item based on the specified drag event data.
        /// </summary>
        /// <param name="treeViewItem">The tree view item.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        /// <returns>The index in the parent item source where to insert the specified tree item.</returns>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter type must be enforced.")]
        public static int? FindInsertionIndex(TreeViewItem treeViewItem, DragEventArgs e)
        {
            int? index = null;

            RelativePositionOnItem position = GetRelativePositionOnItem(treeViewItem, e);

            var item = GetItemFromPoint(treeViewItem, e.GetPosition(treeViewItem));
            index = treeViewItem.ItemContainerGenerator.IndexFromContainer(item);
            if (index == -1)
            {
                index = null;
            }
            else
            {
                if (position == RelativePositionOnItem.Top)
                {
                    // In this case the index is not changed
                }
                else if (position == RelativePositionOnItem.Center)
                {
                    index = null;
                }
                else if (position == RelativePositionOnItem.Bottom)
                {
                    index = index + 1;
                }
            }

            return index;
        }

        /// <summary>
        /// Gets the TreeViewItem at the specified point.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="point">The point.</param>
        /// <returns>The TreeViewItem or null if none was found.</returns>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The parameter type must be enforced.")]
        public static TreeViewItem GetItemFromPoint(ItemsControl itemsControl, Point point)
        {
            UIElement element = itemsControl.InputHitTest(point) as UIElement;
            if (element == itemsControl)
            {
                return null;
            }

            return Helper.TryFindParent<TreeViewItem>(element);
        }

        #endregion

        #region Add\Remove Item from DataGrid ItemsSource

        /// <summary>
        /// Adds the item to the item control's items list.
        /// </summary>
        /// <typeparam name="T">The type of the item that needs to be added.</typeparam>
        /// <param name="dataGrid">The items control.</param>
        /// <param name="item">The item to be added.</param>
        /// <param name="insertIndex">Index of the insert.</param>
        private static void AddItem<T>(DataGrid dataGrid, T item, int insertIndex)
        {
            if (dataGrid != null
                && item != null
                && dataGrid.ItemsSource != null)
            {
                var interfaceList = dataGrid.ItemsSource as IList;
                if (interfaceList != null)
                {
                    interfaceList.Insert(insertIndex, item);
                }
                else
                {
                    var interfaceGenericList = dataGrid.ItemsSource as IList<T>;
                    if (interfaceGenericList != null)
                    {
                        interfaceGenericList.Add(item);
                    }
                }
            }
        }

        /// <summary>
        /// Removes the item.
        /// </summary>
        /// <typeparam name="T">The type of the item that needs to be removed.</typeparam>
        /// <param name="dataGrid">The items control.</param>
        /// <param name="itemToRemove">The item to remove.</param>
        private static void RemoveItem<T>(DataGrid dataGrid, T itemToRemove)
        {
            if (dataGrid != null
                && dataGrid.ItemsSource != null)
            {
                var interfaceList = dataGrid.ItemsSource as IList;
                if (interfaceList != null)
                {
                    interfaceList.Remove(itemToRemove);
                }
                else
                {
                    var interfaceGenericList = dataGrid.ItemsSource as IList<T>;
                    if (interfaceGenericList != null)
                    {
                        interfaceGenericList.Remove(itemToRemove);
                    }
                }
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Gets the data object from items control.
        /// </summary>
        /// <param name="dataGrid">The items control.</param>
        /// <param name="point">The point.</param>
        /// <param name="row">The row from the items control.</param>
        /// <returns>Returns the data from the data control.</returns>
        private static object GetDataObjectFromItemsControl(DataGrid dataGrid, Point point, ref DataGridRow row)
        {
            UIElement element = dataGrid.InputHitTest(point) as UIElement;
            while (element != null)
            {
                if (element == dataGrid)
                {
                    return null;
                }

                object data = dataGrid.ItemContainerGenerator.ItemFromContainer(element);
                if (data != DependencyProperty.UnsetValue)
                {
                    return data;
                }
                else
                {
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }

                if (element is DataGridRow)
                {
                    row = element as DataGridRow;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the item container from point.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="p">The point.</param>
        /// <returns>The UIElement that represents the item container.</returns>
        public static UIElement GetItemContainerFromPoint(ItemsControl itemsControl, Point p)
        {
            UIElement element = itemsControl.InputHitTest(p) as UIElement;
            while (element != null)
            {
                if (element == itemsControl)
                {
                    return null;
                }

                object data = itemsControl.ItemContainerGenerator.ItemFromContainer(element);
                if (data != DependencyProperty.UnsetValue)
                {
                    return element;
                }
                else
                {
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether [is point in top half] [the specified items control].
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <returns>
        ///   <c>true</c> if [is point in top half] [the specified items control]; otherwise, <c>false</c>.
        /// </returns>
        private static RelativePositionOnItem GetRelativePositionOnItem(ItemsControl itemsControl, DragEventArgs e)
        {
            if (itemsControl != null)
            {
                UIElement selectedItemContainer = GetItemContainerFromPoint(itemsControl, e.GetPosition(itemsControl));
                if (selectedItemContainer != null)
                {
                    Point relativePosition = e.GetPosition(selectedItemContainer);

                    double totalHeight = ((FrameworkElement)selectedItemContainer).ActualHeight;
                    double partHeight = totalHeight / 4;

                    if (relativePosition.Y < partHeight)
                    {
                        return RelativePositionOnItem.Top;
                    }
                    else if (relativePosition.Y > partHeight && relativePosition.Y < (3 * partHeight))
                    {
                        return RelativePositionOnItem.Center;
                    }
                    else
                    {
                        return RelativePositionOnItem.Bottom;
                    }
                }
                else
                {
                    return RelativePositionOnItem.NotOnItem;
                }
            }
            else
            {
                return RelativePositionOnItem.NotOnItem;
            }
        }

        /// <summary>
        /// Determines whether [is mouse pointer at top] [the specified items control].
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="point">The point.</param>
        /// <returns>True if the mouse pointer is at the top of the items control, false if it is at the bottom, null otherwise.</returns>
        private static bool? IsMousePointerAtTop(ItemsControl itemsControl, Point point)
        {
            if (point != null)
            {
                if (point.Y > 0.0 && point.Y < 25)
                {
                    return true;
                }
                else if (point.Y > itemsControl.ActualHeight - 25 && point.Y < itemsControl.ActualHeight)
                {
                    return false;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the scroll viewer.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <returns>The scroll viewer.</returns>
        private static ScrollViewer FindScrollViewer(ItemsControl itemsControl)
        {
            if (itemsControl != null)
            {
                UIElement ele = itemsControl;
                while (ele != null)
                {
                    if (VisualTreeHelper.GetChildrenCount(ele) == 0)
                    {
                        ele = null;
                    }
                    else
                    {
                        ele = VisualTreeHelper.GetChild(ele, 0) as UIElement;
                        if (ele != null && ele is ScrollViewer)
                        {
                            return ele as ScrollViewer;
                        }
                    }
                }
            }

            return null;
        }

        #endregion
    }
}
