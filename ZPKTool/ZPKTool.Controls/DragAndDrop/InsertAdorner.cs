﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ZPKTool.Controls
{
    /// <summary>
    ///     There’s a way to provide that visual feedback and it’s by using Adorners.
    /// In short, adorners are bound to a UIElement, and it sits on an AdornerLayer 
    /// of that bound element. 
    /// --
    ///     The AdornerLayer has a Z-index that sit on top of your adorned element and 
    /// renders independently, perfect for providing visual cues/feedback for state 
    /// changes like dragging. 
    /// --
    ///     To create a DragAdorner, we subclass the Adorner class under System.Windows.Document namespace,
    /// and it takes a reference of the UIElement you want to adorn.
    /// --
    ///     There are many ways to “create” your adorner’s look. One way is to use a VisualBrush to “clone” your
    /// UIElement’s look, and set it to a Rectangle’s Fill property along with the desired width and height.
    /// Or because I would like my adorner’s look to be mapped to the data layout of the destination target,
    /// I created a new Dependency Property of type DataTemplate in my DragDropDecorator.
    /// --
    ///     To activate the DragAdorner, you will need to hook up to the PreviewDragEnter, PreviewDragOver and
    /// PreviewDragLeave events. In these events, you will instantiate the DragAdorner and update it’s
    /// positions during DragEnter and DragOver, and finally destroying it when DragLeave or DragDrop occurs. 
    /// You use the UpdatePosition and Destroy methods in the DragAdorner class to do that.
    /// </summary>
    public class InsertAdorner : Adorner
    {
        #region Fields

        /// <summary>
        /// The adorner layer.
        /// </summary>
        private AdornerLayer adornerLayer;

        /// <summary>
        /// The pen that describes how a shape is outlined..
        /// </summary>
        private Pen pen;

        /// <summary>
        /// Draw horizontal flag.
        /// </summary>
        private bool drawHorizontal;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertAdorner"/> class.
        /// </summary>
        /// <param name="isTopHalf">if set to <c>true</c> [is top half].</param>
        /// <param name="drawHorizontal">if set to <c>true</c> [draw horizontal].</param>
        /// <param name="adornedElement">The adorned element.</param>
        /// <param name="adornerLayer">The adorner layer.</param>
        public InsertAdorner(bool isTopHalf, bool drawHorizontal, UIElement adornedElement, AdornerLayer adornerLayer)
            : base(adornedElement)
        {
            // The way to make WPF adorner ignore mouse entirely so that the UIElement behind the adorner still gets mouse events as if the adorner didn't exist.
            this.IsHitTestVisible = false;
            this.Focusable = false;

            this.IsTopHalf = isTopHalf;
            this.adornerLayer = adornerLayer;
            this.drawHorizontal = drawHorizontal;
            this.adornerLayer.Add(this);
            this.pen = new Pen(new SolidColorBrush(Colors.Red), 3.0);

            DoubleAnimation animation = new DoubleAnimation(0.5d, 1d, new Duration(TimeSpan.FromSeconds(0.7d)));
            animation.AutoReverse = true;
            animation.RepeatBehavior = RepeatBehavior.Forever;
            this.pen.Brush.BeginAnimation(Brush.OpacityProperty, animation);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is top half.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is top half; otherwise, <c>false</c>.
        /// </value>
        public bool IsTopHalf { get; set; }

        #endregion

        #region On Render

        /// <summary>
        /// When overridden in a derived class, participates in rendering operations that are directed by the layout system. The rendering instructions for this element are not used directly when this method is invoked, and are instead preserved for later asynchronous use by layout and drawing.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            Point startPoint;
            Point endPoint;

            if (drawHorizontal)
            {
                DetermineHorizontalLinePoints(out startPoint, out endPoint);
            }
            else
            {
                DetermineVerticalLinePoints(out startPoint, out endPoint);
            }

            drawingContext.DrawLine(pen, startPoint, endPoint);
        }

        #endregion

        #region Determine Line Position

        /// <summary>
        /// Determines the horizontal line points.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        private void DetermineHorizontalLinePoints(out Point startPoint, out Point endPoint)
        {
            double width = this.AdornedElement.RenderSize.Width;
            double height = this.AdornedElement.RenderSize.Height;

            if (!this.IsTopHalf)
            {
                startPoint = new Point(0, height);
                endPoint = new Point(width, height);
            }
            else
            {
                startPoint = new Point(0, 0);
                endPoint = new Point(width, 0);
            }
        }

        /// <summary>
        /// Determines the vertical line points.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        private void DetermineVerticalLinePoints(out Point startPoint, out Point endPoint)
        {
            double width = this.AdornedElement.RenderSize.Width;
            double height = this.AdornedElement.RenderSize.Height;

            if (!this.IsTopHalf)
            {
                startPoint = new Point(width, 0);
                endPoint = new Point(width, height);
            }
            else
            {
                startPoint = new Point(0, 0);
                endPoint = new Point(0, height);
            }
        }

        #endregion

        #region Remove

        /// <summary>
        /// Removes the adorner.
        /// </summary>
        public void Remove()
        {
            adornerLayer.Remove(this);
        }

        #endregion
    }
}
