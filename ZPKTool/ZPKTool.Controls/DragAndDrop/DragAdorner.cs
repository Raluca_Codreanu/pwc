﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Controls
{
    /// <summary>
    ///     There’s a way to provide that visual feedback and it’s by using Adorners.
    /// In short, adorners are bound to a UIElement, and it sits on an AdornerLayer 
    /// of that bound element. 
    /// -
    ///     The AdornerLayer has a Z-index that sit on top of your adorned element and 
    /// renders independently, perfect for providing visual cues/feedback for state 
    /// changes like dragging. 
    /// -
    ///     To create a DragAdorner, we subclass the Adorner class under System.Windows.Document namespace,
    /// and it takes a reference of the UIElement you want to adorn.
    /// -
    ///     There are many ways to “create” your adorner’s look. One way is to use a VisualBrush to “clone” your
    /// UIElement’s look, and set it to a Rectangle’s Fill property along with the desired width and height.
    /// Or because I would like my adorner’s look to be mapped to the data layout of the destination target,
    /// I created a new Dependency Property of type DataTemplate in my DragDropDecorator.
    /// -
    ///     To activate the DragAdorner, you will need to hook up to the PreviewDragEnter, PreviewDragOver and
    /// PreviewDragLeave events. In these events, you will instantiate the DragAdorner and update it’s
    /// positions during DragEnter and DragOver, and finally destroying it when DragLeave or DragDrop occurs. 
    /// You use the UpdatePosition and Destroy methods in the DragAdorner class to do that.
    /// </summary>
    public class DragAdorner : Adorner
    {
        #region Fields

        /// <summary>
        /// The content presenter.
        /// </summary>
        private ContentPresenter contentPresenter;

        /// <summary>
        /// The adorner layer.
        /// </summary>
        private AdornerLayer adornerLayer;

        /// <summary>
        /// The left offset.
        /// </summary>
        private double leftOffset;

        /// <summary>
        /// The right offset.
        /// </summary>
        private double topOffset;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DragAdorner"/> class.
        /// </summary>
        /// <param name="bitmapFrame">The bitmap.</param>
        /// <param name="dataTemplateKey">The data template key.</param>
        /// <param name="adornedElement">The adorned element.</param>
        /// <param name="adornerLayer">The adorner layer.</param>
        public DragAdorner(BitmapFrame bitmapFrame, string dataTemplateKey, UIElement adornedElement, AdornerLayer adornerLayer)
            : base(adornedElement)
        {
            // The way to make WPF adorner ignore mouse entirely so that the UIElement behind the adorner still gets mouse events as if the adorner didn't exist.
            this.IsHitTestVisible = false;
            this.Focusable = false;

            this.adornerLayer = adornerLayer;
            this.contentPresenter = new ContentPresenter();

            this.contentPresenter.SetResourceReference(
                ContentPresenter.ContentTemplateProperty,
                dataTemplateKey);

            // set data as image
            this.contentPresenter.Content = bitmapFrame;
            this.contentPresenter.Opacity = 0.98d;

            this.adornerLayer.Add(this);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        /// <returns>The number of visual child elements for this element.</returns>
        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        #endregion

        /// <summary>
        /// Implements any custom measuring behavior for the adorner.
        /// </summary>
        /// <param name="constraint">A size to constrain the adorner to.</param>
        /// <returns>
        /// A <see cref="T:System.Windows.Size"/> object representing the amount of layout space needed by the adorner.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            this.contentPresenter.Measure(constraint);
            return contentPresenter.DesiredSize;
        }

        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement"/> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            this.contentPresenter.Arrange(new Rect(finalSize));
            return finalSize;
        }

        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)"/>, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>
        /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            return this.contentPresenter;
        }

        /// <summary>
        /// Updates the position.
        /// </summary>
        /// <param name="left">The left position.</param>
        /// <param name="top">The top position.</param>
        public void UpdatePosition(double left, double top)
        {
            leftOffset = left;
            topOffset = top;
            if (this.adornerLayer != null)
            {
                this.adornerLayer.Update(this.AdornedElement);
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.Windows.Media.Transform"/> for the adorner, based on the transform that is currently applied to the adorned element.
        /// </summary>
        /// <param name="transform">The transform that is currently applied to the adorned element.</param>
        /// <returns>
        /// A transform to apply to the adorner.
        /// </returns>
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(leftOffset, topOffset));
            return result;
        }

        #region Destroy

        /// <summary>
        /// Destroys this instance.
        /// </summary>
        public void Remove()
        {
            this.adornerLayer.Remove(this);
        }

        #endregion
    }
}
