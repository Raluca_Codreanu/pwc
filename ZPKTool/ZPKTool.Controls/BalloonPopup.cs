﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using ZPKTool.Common;

namespace ZPKTool.Controls
{
    /// <summary>
    /// Build a popup with a balloon geometry.
    /// </summary>
    public class BalloonPopup : Popup
    {
        /// <summary>
        /// The popup's content decorator.
        /// </summary>
        private BalloonDecorator decorator = new BalloonDecorator();

        /// <summary>
        /// Initializes a new instance of the <see cref="BalloonPopup"/> class.
        /// </summary>
        public BalloonPopup()
        {
            this.decorator.BorderBrush = new SolidColorBrush(Colors.Black);
            this.decorator.Background = new SolidColorBrush(Color.FromRgb(252, 234, 178));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BalloonPopup"/> class.
        /// </summary>
        /// <param name="placementTarget">The element relative to which the popup is positioned when it opens</param>
        public BalloonPopup(UIElement placementTarget)
            : this()
        {
            this.PlacementTarget = placementTarget;
        }

        /// <summary>
        /// Responds to the condition in which the value of the <see cref="P:System.Windows.Controls.Primitives.Popup.IsOpen"/> property changes from false to true.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnOpened(EventArgs e)
        {
            // Move the popup-s content into the decorator and set the decorator as the popup's child.
            if (this.Child != this.decorator)
            {
                var child = this.Child;
                this.Child = null;
                this.decorator.Child = child;
                this.Child = this.decorator;
            }

            base.OnOpened(e);
        }

        /// <summary>
        /// Shows this instance.
        /// </summary>
        public void Show()
        {
            Show(0, 0);
        }

        /// <summary>
        /// Shows the popup.
        /// </summary>
        /// <param name="placementOffsetX">The horizontal placement offset.</param>
        /// <param name="placementOffsetY">The vertical placement offset.</param>
        public void Show(double placementOffsetX, double placementOffsetY)
        {
            // if the popup control is created from code, it does not inherit the layout transform of its parent, so we need to set the value manually
            this.LayoutTransform = new ScaleTransform(UserSettingsManager.Instance.ZoomLevel, UserSettingsManager.Instance.ZoomLevel);

            this.AllowsTransparency = true;
            this.StaysOpen = false;
            this.PopupAnimation = PopupAnimation.Fade;
            this.Placement = PlacementMode.Bottom;

            FrameworkElement element = this.PlacementTarget as FrameworkElement;
            if (element != null)
            {
                this.PlacementRectangle = new Rect(placementOffsetX, 0, element.ActualWidth, element.ActualHeight + placementOffsetY);
            }

            this.IsOpen = false;
            this.IsOpen = true;
        }

        /// <summary>
        /// Closes the popup.
        /// </summary>
        public void Close()
        {
            this.IsOpen = false;
        }
    }
}
