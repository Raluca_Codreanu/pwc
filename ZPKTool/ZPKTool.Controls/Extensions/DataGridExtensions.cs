﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace System.Windows.Controls
{
    /// <summary>
    /// Extensions for the DataGrid control.
    /// </summary>
    public static class DataGridExtensions
    {
        /// <summary>
        /// Gets a certain cell of a data grid.
        /// </summary>
        /// <param name="grid">The data grid.</param>
        /// <param name="row">The row from which to get the cell.</param>
        /// <param name="column">The column.</param>
        /// <returns>A data grid  cell, or null if it does not exist.</returns>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only a DataGridRow or a more derived type should be passed.")]
        public static DataGridCell GetCell(this DataGrid grid, DataGridRow row, int column)
        {
            if (row != null)
            {
                DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

                if (presenter == null)
                {
                    grid.ScrollIntoView(row, grid.Columns[column]);
                    presenter = GetVisualChild<DataGridCellsPresenter>(row);
                }

                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                return cell;
            }

            return null;
        }

        /// <summary>
        /// Gets the 1st child of the specified type from the parent's visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the child to get.</typeparam>
        /// <param name="parent">The parent.</param>
        /// <returns>The 1st child of type <typeparamref name="T"/> or null if the parent has no child of that type.</returns>
        private static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            // TODO: use this method from UIHelper after is moved to Controls or Common.
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }

                if (child != null)
                {
                    break;
                }
            }

            return child;
        }
    }
}
