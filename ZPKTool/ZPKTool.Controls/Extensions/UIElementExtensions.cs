﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace System.Windows
{
    /// <summary>
    /// Extensions for the UIElement class.
    /// </summary>
    public static class UIElementExtensions
    {
        /// <summary>
        /// Generates a "screen-shot" of the ui element in png format.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="scaleFactor">The scale factor applied to the original size of the element in the generated image.</param>
        /// <returns>A stream containing the png image.</returns>
        public static Stream GenerateImagePng(this UIElement element, double scaleFactor)
        {
            FrameworkElement elementFramework = element as FrameworkElement;
            Rect boundary = VisualTreeHelper.GetDescendantBounds(element);

            double width = boundary.Width;
            double height = boundary.Height;

            // GetContentBoundslement to eliminate any margins applied to it, because the renderer will render them too
            DrawingVisual drawingVisual = new DrawingVisual();
            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                VisualBrush brush = new VisualBrush(elementFramework);
                drawingContext.PushTransform(new ScaleTransform(scaleFactor, scaleFactor));
                drawingContext.DrawRectangle(brush, null, new Rect(new Point(), new Size(width, height)));
            }

            RenderTargetBitmap renderer = new RenderTargetBitmap((int)(width * scaleFactor), (int)(height * scaleFactor), 96, 96, PixelFormats.Pbgra32);
            renderer.Render(drawingVisual);

            // Encode the rendered ui element using the PNG format
            BitmapFrame frame = BitmapFrame.Create(renderer);

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(frame);

            // Create a bitmap image from the encoded content
            MemoryStream output = new MemoryStream();
            encoder.Save(output);
            output.Seek(0, SeekOrigin.Begin);
            return output;
        }
    }
}
