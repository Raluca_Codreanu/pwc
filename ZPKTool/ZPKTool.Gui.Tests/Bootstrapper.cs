﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// Configure and initialize the UI composition framework
    /// </summary>
    public class Bootstrapper
    {
        /// <summary>
        /// The catalog holding all modules.
        /// </summary>        
        private AggregateCatalog moduleCatalog = new AggregateCatalog();

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
        {
            this.CompositionContainer = new CompositionContainer(this.moduleCatalog);
            CompositionBatch batch = new CompositionBatch();
            batch.AddExportedValue(this.CompositionContainer);
            this.CompositionContainer.Compose(batch);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        /// <param name="catalog">The catalog to be initialize with</param>
        public Bootstrapper(ComposablePartCatalog catalog)
            : base()
        {
            this.AddCatalog(catalog);
        }

        /// <summary>
        /// Gets the composition container.
        /// </summary>        
        public CompositionContainer CompositionContainer { get; private set; }

        /// <summary>
        /// Ads a catalog to the module catalog for the <see cref="CompositionContainer"/> instance
        /// </summary>
        /// <param name="catalog">The catalog</param>
        public void AddCatalog(ComposablePartCatalog catalog)
        {
            this.moduleCatalog.Catalogs.Add(catalog);
        }
    }
}
