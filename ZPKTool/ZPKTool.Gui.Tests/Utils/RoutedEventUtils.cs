﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Moq;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// Helper class containing useful methods used in GUI unit tests
    /// For different types of routed events.
    /// </summary>
    public static class RoutedEventUtils
    {
        /// <summary>
        /// Create a custom DragEventArgs 
        /// </summary>
        /// <param name="dataObject">The data object</param>
        /// <returns>The DragEventArgs containing the data object</returns>
        public static DragEventArgs CreateDragEventArgsInstance(DataObjectForMock dataObject)
        {
            DragEventArgs mockEventArgs = (DragEventArgs)System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(DragEventArgs));
            var fields = typeof(DragEventArgs).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            
            var dataField = fields.First(f => f.Name == "_data");
            dataField.SetValue(mockEventArgs, dataObject);

            mockEventArgs.RoutedEvent = UIElement.DropEvent;

            return mockEventArgs;
        }
    }

    /// <summary>
    /// Implementation of IDataObject interface used for mocking the interface.
    /// Can't mock the interface due to some restriction when creating the <see cref="DragEventArgs"/> instance.
    /// </summary>
    public class DataObjectForMock : IDataObject
    {
        public virtual object GetData(string format, bool autoConvert)
        {
            return string.Empty;
        }

        public virtual object GetData(Type format)
        {
            return string.Empty;
        }

        public virtual object GetData(string format)
        {
            return string.Empty;
        }

        public virtual bool GetDataPresent(string format, bool autoConvert)
        {
            return true;
        }

        public virtual bool GetDataPresent(Type format)
        {
            return true;
        }

        public virtual bool GetDataPresent(string format)
        {
            return true;
        }

        public virtual string[] GetFormats(bool autoConvert)
        {
            return new string[0];
        }

        public virtual string[] GetFormats()
        {
            return new string[0];
        }

        public virtual void SetData(string format, object data, bool autoConvert)
        {
        }

        public virtual void SetData(Type format, object data)
        {
        }

        public virtual void SetData(string format, object data)
        {
        }

        public virtual void SetData(object data)
        {
        }
    }
}
