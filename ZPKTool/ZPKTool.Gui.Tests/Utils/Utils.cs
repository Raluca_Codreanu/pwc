﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// Helper class containing useful methods used in GUI unit tests.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Configures the database access for the persistence layer.
        /// Due to implementation details, this method should be called before attempting any call to Persistence methods.
        /// </summary>
        public static void ConfigureDbAccess()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.InitialCatalog = "ZPKTool";
            localConnParams.DataSource = @"localhost\SQLEXPRESS";
            localConnParams.UserId = "PROimpensUser";
            localConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            localConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;

            DbConnectionInfo centralConnParams = new DbConnectionInfo();
            centralConnParams.InitialCatalog = "ZPKToolCentral";
            centralConnParams.DataSource = @"localhost\SQLEXPRESS";
            centralConnParams.UserId = "PROimpensUser";
            centralConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            centralConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;

            ZPKTool.DataAccess.ConnectionConfiguration.Initialize(localConnParams, centralConnParams);
        }

        /// <summary>
        /// Delete a file from a specified path.
        /// </summary>
        /// <param name="path">The file's path.</param>
        public static void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception)
            {
                // Do not throw any exception if the delete operation failed
            }
        }

        /// <summary>
        /// Delete a directory and all contained files from a specified path.
        /// </summary>
        /// <param name="path"> The directory's path.</param>
        public static void DeleteDirectory(string path)
        {
            try
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    DeleteFile(file);
                }

                Directory.Delete(path);
            }
            catch (Exception)
            {
                // Do not throw any exception if the delete operation failed
            }
        }
    }
}
