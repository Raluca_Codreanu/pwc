﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;

namespace System
{
    /// <summary>
    /// Extensions for the <see cref="System.Random"/> class.
    /// </summary>
    public static class RandomExtensions
    {
        /// <summary>
        /// Returns a random string of a specified length.
        /// </summary>
        /// <param name="random">The random instance.</param>
        /// <param name="length">The length of the returned string.</param>
        /// <returns>A random string with the specified length.</returns>
        public static string NextString(this Random random, int length = 10)
        {
            return EncryptionManager.Instance.GenerateRandomString(length, true);
        }
    }
}
