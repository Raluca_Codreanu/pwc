﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The test class for the import confirmation view model
    /// </summary>
    [TestClass]
    public class ImportConfirmationViewModelTest
    {
        #region Additional attributes

        /// <summary>
        ///  Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion

        #region test methods

        /// <summary>
        /// Test loading an assembly entity
        /// </summary>
        [TestMethod]
        public void LoadAssemblyEntityTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Assembly assy = new Assembly();
            assy.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)assy;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(AssemblyViewModel), "The view model loaded is not an Assembly view model");
        }

        /// <summary>
        /// Test loading an project entity
        /// </summary>
        [TestMethod]
        public void LoadProjectEntityTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Project entity = new Project();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            entity.Customer = new Customer();
            entity.OverheadSettings = new OverheadSetting();

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(ProjectViewModel), "The view model loaded is not an Project view model");
        }

        /// <summary>
        /// Test loading an raw part entity
        /// </summary>
        [TestMethod]
        public void LoadRawPartEntityTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            RawPart entity = new RawPart();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            entity.OverheadSettings = new OverheadSetting();

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(PartViewModel), "The view model loaded is not an Part view model");
        }

        /// <summary>
        /// Test loading an part entity
        /// </summary>
        [TestMethod]
        public void LoadPartEntityTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Part entity = new Part();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            entity.OverheadSettings = new OverheadSetting();

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(PartViewModel), "The view model loaded is not an Part view model");
        }

        /// <summary>
        /// Test loading a machine entity
        /// </summary>
        [TestMethod]
        public void LoadMachineEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Machine entity = new Machine();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(MachineViewModel), "The view model loaded is not an Machine view model");
        }

        /// <summary>
        /// Test loading a Raw Material entity
        /// </summary>
        [TestMethod]
        public void LoadRawMaterialEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            RawMaterial entity = new RawMaterial();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(RawMaterialViewModel), "The view model loaded is not an Raw Material view model");
        }

        /// <summary>
        /// Test loading a consumable entity
        /// </summary>
        [TestMethod]
        public void LoadConsumableEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Consumable entity = new Consumable();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(ConsumableViewModel), "The view model loaded is not an Consumable view model");
        }

        /// <summary>
        /// Test loading a Commodity entity
        /// </summary>
        [TestMethod]
        public void LoadCommodityEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Commodity entity = new Commodity();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(CommodityViewModel), "The view model loaded is not an Commodity view model");
        }

        /// <summary>
        /// Test loading a Die entity
        /// </summary>
        [TestMethod]
        public void LoadDieEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Die entity = new Die();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            entity.AreAllPaidByCustomer = false;

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(DieViewModel), "The view model loaded is not an Die view model");
        }

        /// <summary>
        /// Test loading a Manufacturer entity
        /// </summary>
        [TestMethod]
        public void LoadManufacturerEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Manufacturer entity = new Manufacturer();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(ManufacturerViewModel), "The details view model loaded is not an instance of ManufacturerViewModel");
        }

        /// <summary>
        /// Test loading a Supplier entity
        /// </summary>
        [TestMethod]
        public void LoadSupplierEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Customer entity = new Customer();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(SupplierViewModel), "The view model loaded is not an SupplierViewModel view model");
        }

        /// <summary>
        /// Test loading a non supported entity
        /// </summary>
        [TestMethod]
        public void LoadNotSupportedEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            Country entity = new Country();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            Assert.IsTrue(importViewModel.Content == null, "The view model loaded a non supported entity");
        }

        /// <summary>
        /// Test loading a folder with null value
        /// </summary>
        [TestMethod]
        public void LoadFolderEntityTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            ProjectFolder subFolder = new ProjectFolder();
            subFolder.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            folder.Projects.Add(project);
            folder.ChildrenProjectFolders.Add(subFolder);

            // Set the model to a machine entity
            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)folder;

            Assert.IsTrue(importViewModel.Content.GetType() == typeof(List<object>), "The view loaded is not an the folder details list");
        }

        /// <summary>
        /// Test import command
        /// </summary>
        [TestMethod]
        public void ImportCommandTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.ImportCommand.Execute(null);

            Assert.IsTrue(importViewModel.CanImportContinue, "The import command failed");
        }

        /// <summary>
        /// Test cancel command
        /// </summary>
        [TestMethod]
        public void CancelImportCommandTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.CancelImportCommand.Execute(null);

            Assert.IsFalse(importViewModel.CanImportContinue, "The cancel import command failed");
        }

        /// <summary>
        /// Test Show Import Confirmation Screen flag
        /// </summary>
        [TestMethod]
        public void ShowImportConfirmationScreenFlagTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;

            // check if the flag on importedViewModel is properly set
            var flagOnUserSetting = UserSettingsManager.Instance.DontShowImportConfirmation;
            var flagOnViewModel = importViewModel.DontShowImportConfirmationScreen;
            Assert.AreEqual(flagOnUserSetting, flagOnViewModel, "The Show Import Confirmation Screen flag is not properly set on view model ");

            // change flag on view model
            importViewModel.DontShowImportConfirmationScreen = !flagOnViewModel;
            flagOnUserSetting = UserSettingsManager.Instance.DontShowImportConfirmation;
            flagOnViewModel = importViewModel.DontShowImportConfirmationScreen;
            Assert.AreEqual(flagOnUserSetting, flagOnViewModel, "The Show Import Confirmation Screen flag is not properly set on view model ");
        }

        /// <summary>
        /// Test OnUnloaded method
        /// </summary>
        [TestMethod]
        public void OnUnloadedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ImportConfirmationViewModel importViewModel = new ImportConfirmationViewModel(windowService, compositionContainer);
            IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataSourceManager);

            // Create an entity and assigned to view model
            Customer entity = new Customer();
            entity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            importViewModel.DataSourceManager = dataSourceManager;
            importViewModel.IsReadOnly = true;
            importViewModel.Model = (object)entity;

            importViewModel.OnUnloaded();

            Assert.IsTrue(importViewModel.Model == null, "The model was not cleared");
            Assert.IsTrue(importViewModel.Content == null, "The view model content was not cleared");
        }

        #endregion

        #region other methods

        /// <summary>
        /// Create a composition container using some parameters
        /// </summary>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The windows service.</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(out MessageDialogServiceMock messageDialogService, out WindowServiceMock windowService)
        {
            var messenger = new Messenger();
            var compositionContainer = new CompositionContainer();
            messageDialogService = new MessageDialogServiceMock();
            var dispacherServiceMock = new DispatcherServiceMock();
            windowService = new WindowServiceMock(messageDialogService, dispacherServiceMock);
            var pleaseWaitService = new Gui.Services.PleaseWaitService(dispacherServiceMock);
            var fileDialogServiceMock = new FileDialogServiceMock();
            var modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            var unitsService = new UnitsServiceMock();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();

            var supplierViewModel = new SupplierViewModel(windowService, compositionContainer);
            var documentsViewModel = new EntityDocumentsViewModel(windowService, fileDialogServiceMock, modelBrowserHelperService);
            var overheadSettingViewModel = new OverheadSettingsViewModel(messenger, windowService, unitsService);
            var videoViewModel = new VideoViewModel(messenger, windowService, fileDialogServiceMock);
            var pictureViewModel = new PicturesViewModel(messenger, windowService, fileDialogServiceMock);
            var mediaViewModel = new MediaViewModel(videoViewModel, pictureViewModel, messenger, windowService, fileDialogServiceMock, modelBrowserHelperService);
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var countrySettingViewModel = new CountrySettingsViewModel(unitsService);
            var countryBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);
            var masterDataBrowserViewModel = new MasterDataBrowserViewModel(compositionContainer, messenger, windowService, new Gui.Services.OnlineCheckService(messenger), unitsService);

            var projectViewModel = new ProjectViewModel(windowService, pleaseWaitService, messenger, supplierViewModel, documentsViewModel, overheadSettingViewModel, mediaViewModel, unitsService);
            var assemblyViewModel = new AssemblyViewModel(
                                        messenger,
                                        windowService,
                                        modelBrowserHelperService,
                                        costRecalculationCloneManager,
                                        manufacturerViewModel,
                                        documentsViewModel,
                                        overheadSettingViewModel,
                                        countrySettingViewModel,
                                        countryBrowserViewModel,
                                        masterDataBrowserViewModel,
                                        mediaViewModel,
                                        unitsService);
            var partViewModel = new PartViewModel(
                                    messenger,
                                    windowService,
                                    modelBrowserHelperService,
                                    costRecalculationCloneManager,
                                    manufacturerViewModel,
                                    documentsViewModel,
                                    overheadSettingViewModel,
                                    countrySettingViewModel,
                                    countryBrowserViewModel,
                                    masterDataBrowserViewModel,
                                    mediaViewModel,
                                    unitsService);
            var machineViewModel = new MachineViewModel(
                                       windowService,
                                       messenger,
                                       modelBrowserHelperService,
                                       costRecalculationCloneManager, 
                                       manufacturerViewModel,
                                       masterDataBrowserViewModel,
                                       mediaViewModel,
                                       new ClassificationSelectorViewModel(),
                                       unitsService);
            var rawMaterialViewModel = new RawMaterialViewModel(
                                           windowService,
                                           messenger,
                                           modelBrowserHelperService,
                                           costRecalculationCloneManager,
                                           manufacturerViewModel,
                                           masterDataBrowserViewModel,
                                           mediaViewModel,
                                           new ClassificationSelectorViewModel(),
                                           unitsService);
            var consumableViewModel = new ConsumableViewModel(
                windowService,
                messenger,
                modelBrowserHelperService,
                costRecalculationCloneManager,
                manufacturerViewModel,
                masterDataBrowserViewModel,
                unitsService);
            var commodityViewModel = new CommodityViewModel(
                windowService,
                messenger,
                modelBrowserHelperService,
                costRecalculationCloneManager,
                manufacturerViewModel,
                masterDataBrowserViewModel,
                mediaViewModel,
                unitsService);
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                modelBrowserHelperService,
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var rootManufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);

            compositionContainer.ComposeParts(
                        messageDialogService,
                        dispacherServiceMock,
                        projectViewModel,
                        assemblyViewModel,
                        partViewModel,
                        machineViewModel,
                        rawMaterialViewModel,
                        consumableViewModel,
                        commodityViewModel,
                        dieViewModel,
                        supplierViewModel,
                        rootManufacturerViewModel);
            return compositionContainer;
        }

        /// <summary>
        /// Create a user to login
        /// </summary>
        /// <param name="dataSourceManager">The data context</param>
        public void CreateUserToLogin(IDataSourceManager dataSourceManager)
        {
            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");
            user.Roles = Role.Admin;
            dataSourceManager.UserRepository.Add(user);
            dataSourceManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);
        }

        #endregion
    }
}
