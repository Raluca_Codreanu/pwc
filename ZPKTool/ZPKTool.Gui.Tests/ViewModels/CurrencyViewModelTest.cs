﻿using System;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for CurrencyViewModelTest and is intended 
    /// to contain all CurrencyViewModelTest Unit Tests
    /// </summary>
    [TestClass]
    public class CurrencyViewModelTest
    {
        /// <summary>
        /// The test context which provides information about and functionality for the current test run.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion

        #region Test Methods

        /// <summary>
        /// A test for the save command with a valid model
        /// </summary>
        [TestMethod]
        public void SaveValidModelTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;

            currencyViewModel.Model = currency;

            currencyViewModel.SaveCommand.Execute(null);

            // The file is was not saved
            // Verifies if currency  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotSaved = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotSaved, "The currency was not saved");
        }

        /// <summary>
        /// A test for the save command with and invalid model
        /// </summary>
        [TestMethod]
        public void SaveValidModelOnEditModeCreateTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;

            currencyViewModel.EditMode = ViewModelEditMode.Create;
            currencyViewModel.Model = currency;

            currencyViewModel.SaveCommand.Execute(null);

            // The file is was not saved
            // Verifies if currency  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotSaved = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotSaved, "The currency was not saved");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed the, flag for the cancel dialog box is true
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelVMIsChangedShowCancelMessageTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            currencyViewModel.Model = currency;
            currencyViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            currencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);
            currencyViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is no
        /// </summary>
        [TestMethod]
        public void AbortedCancelVMIsChangedShowCancelMessageTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // change the model
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            currencyViewModel.Model = currency;
            currencyViewModel.IsChanged = true;
            currencyViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            currencyViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreNotEqual(symbol, currency.Symbol, "The currency was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is false
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelVMIsChangedNotShowCancelMessageTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            currencyViewModel.Model = currency;
            currencyViewModel.ShowCancelMessageFlag = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            currencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);
            currencyViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is false
        /// and the answer to the message dialog is no
        /// </summary>
        [TestMethod]
        public void AbortedCancelVMIsChangedNotShowCancelMessageTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            currencyViewModel.Model = currency;
            currencyViewModel.IsChanged = true;
            currencyViewModel.ShowCancelMessageFlag = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            // change the currency name
            currencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);

            currencyViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The currencyiewModel is changed and it's in create mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingVMIsChangedEditModeCreateTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;

            currencyViewModel.Model = currency;
            currencyViewModel.IsChanged = true;
            currencyViewModel.EditMode = ViewModelEditMode.Create;
            currencyViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = currencyViewModel.OnUnloading();

            Assert.IsTrue(status && !currencyViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The currencyiewModel is changed and it's in create mode
        /// and the user do not confirms cancel action
        /// </summary>
        [TestMethod]
        public void AbortedOnUnloadingVMIsChangedEditModeCreateTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            currencyViewModel.DataSourceManager = dataContext;

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;

            currencyViewModel.Model = currency;
            currencyViewModel.IsChanged = true;
            currencyViewModel.EditMode = ViewModelEditMode.Create;
            currencyViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var status = currencyViewModel.OnUnloading();

            Assert.IsFalse(status && !currencyViewModel.IsChanged);
        }

        #endregion Test Methods

        /// <summary>
        /// Create a user to login
        /// </summary>
        /// <param name="dataManager">The data context</param>
        public void CreateUserToLogin(IDataSourceManager dataManager)
        {
            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);
        }
    }
}
