﻿using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for SearchToolViewModel and is intended 
    /// to contain unit tests for all commands from  SearchToolViewModel
    /// </summary>
    [TestClass]
    public class SearchToolViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchToolViewModelTest"/> class.
        /// </summary>
        public SearchToolViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests NavigateToObject command
        /// </summary>
        [TestMethod]
        public void NavigateToObjectTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            // Register an action which will handle message of type NavigateToEntityMessage
            bool navigateToEntityMessageSend = false;
            messenger.Register<NavigateToEntityMessage>(msg => { navigateToEntityMessageSend = true; }, GlobalMessengerTokens.MainViewTargetToken);

            // Create a project 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);

            // Creates the SearchTreeItem for created project 
            SearchTreeItem projectItem = new SearchTreeItem(project, false, project.Name, null, null, null);

            // Executes NavigateToObject command
            ICommand navigateToObjectCommand = searchToolViewModel.NavigateToObjectCommand;
            navigateToObjectCommand.Execute(projectItem);

            // Verifies if NavigateToEntityMessage was sent
            Assert.IsTrue(navigateToEntityMessageSend, "Failed to navigate to entity");
        }

        ///// <summary>
        ///// A test for SearchCommand using valid data as input parameter.
        ///// </summary>
        //[TestMethod]
        //public void SearchValidEntities()
        //{
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    IMessenger messenger = new Messenger();
        //    SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

        //    // Create a project 
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    // Create an owner for the project
        //    User user = new User();
        //    user.Name = user.Guid.ToString();
        //    user.Username = user.Guid.ToString();
        //    user.Password = EncryptionManager.Instance.EncodeMD5("password");

        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Role adminRole = dataContext.RoleRepository.GetAll().FirstOrDefault(r => r.Equals(UserRole.Admin));
        //    user.Roles.Add(adminRole);
        //    project.SetOwner(user);

        //    dataContext.ProjectRepository.Add(project);
        //    dataContext.SaveChanges();

        //    // Login in the application with created user because search depends on the current user
        //    SecurityManager.Instance.Login(user.Username, "password");
        //    SecurityManager.Instance.ChangeCurrentUserRole(adminRole);

        //    searchToolViewModel.SearchText = project.Name;
        //    searchToolViewModel.ScopeSelection = SearchScopeFilter.MyProjects;
        //    searchToolViewModel.FilterSelection = SearchFilter.Projects;

        //    ICommand searchCommand = searchToolViewModel.SearchCommand;
        //    searchCommand.Execute(null);

        //    Thread.Sleep(3000);
        //}

        /// <summary>
        /// A test for SearchCommand using null text as input data.
        /// </summary>
        [TestMethod]
        public void SearchNullText()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            searchToolViewModel.SearchText = null;
            messageBoxService.ShowCallCount = 0;

            ICommand searchCommand = searchToolViewModel.SearchCommand;
            searchCommand.Execute(null);

            // Verify that the search operation was not executed and the user was informed about the reason
            Assert.IsTrue(messageBoxService.ShowCallCount == 1, "An info message should be displayed if the 'Searched Text' was null.");
            Assert.IsTrue(searchToolViewModel.TreeItems.Count == 0, "Search operation shouldn't be performed if the 'Searched Text' was null.");
        }

        /// <summary>
        /// A test for SearchCommand using empty text as input data.
        /// </summary>
        [TestMethod]
        public void SearchEmptyText()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            searchToolViewModel.SearchText = string.Empty;
            messageBoxService.ShowCallCount = 0;

            ICommand searchCommand = searchToolViewModel.SearchCommand;
            searchCommand.Execute(null);

            // Verify that the search operation was not executed and the user was informed about the reason
            Assert.IsTrue(messageBoxService.ShowCallCount == 1, "An info message should be displayed if the 'Searched Text' was empty.");
            Assert.IsTrue(searchToolViewModel.TreeItems.Count == 0, "Search operation shouldn't be performed if the 'Searched Text' was empty.");
        }

        /// <summary>
        /// A test for ClearCommand. The following input data were used:
        /// MessageDialogResult = Yes.
        /// </summary>
        [TestMethod]
        public void ClearTest1()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            // Set up the searchToolViewModel 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            SearchTreeItem projectItem = new SearchTreeItem(project, false, project.Name, null, null, null);
            searchToolViewModel.TreeItems.Add(projectItem);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand clearCommand = searchToolViewModel.ClearCommand;
            clearCommand.Execute(null);

            // Verifies that no result is visible and the tree items were clear
            Assert.IsTrue(searchToolViewModel.NoResultsLabelVisibility == System.Windows.Visibility.Visible, "Failed to clear the results.");
            Assert.IsTrue(searchToolViewModel.TreeItems.Count == 0, "Failed to clear the search items.");
        }

        /// <summary>
        /// A test for ClearCommand. The following input data were used:
        /// MessageDialogResult= No.
        /// </summary>
        [TestMethod]
        public void ClearTest2()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            // Set up the searchToolViewModel 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            SearchTreeItem projectItem = new SearchTreeItem(project, false, project.Name, null, null, null);
            searchToolViewModel.TreeItems.Add(projectItem);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            searchToolViewModel.NoResultsLabelVisibility = System.Windows.Visibility.Collapsed;
            ICommand clearCommand = searchToolViewModel.ClearCommand;
            clearCommand.Execute(null);

            // Verifies that no result is visible and the tree items were clear
            Assert.IsTrue(searchToolViewModel.NoResultsLabelVisibility != System.Windows.Visibility.Visible, "The results should not be cleared if No option has been choose.");
            Assert.IsTrue(searchToolViewModel.TreeItems.Count > 0, "The search results items should not have been cleared if No option has been choose.");
        }

        /// <summary>
        /// A test for ShowScopePopupCommand.
        /// </summary>
        [TestMethod]
        public void ShowScopePopupTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            searchToolViewModel.ScopePopupIsOpen = false;
            ICommand showScopePopupCommand = searchToolViewModel.ShowScopePopupCommand;
            showScopePopupCommand.Execute(null);

            Assert.IsTrue(searchToolViewModel.ScopePopupIsOpen, "Failed to show the Scope Popup.");
        }

        /// <summary>
        /// A test for ShowFilterPopupCommand.
        /// </summary>
        [TestMethod]
        public void ShowFilterPopupTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            searchToolViewModel.FilterPopupIsOpen = false;
            ICommand showFilterPopupCommand = searchToolViewModel.ShowFilterPopupCommand;
            showFilterPopupCommand.Execute(null);

            Assert.IsTrue(searchToolViewModel.FilterPopupIsOpen, "Failed to show the Filter Popup.");
        }

        /// <summary>
        /// A test for CopyCommand.
        /// </summary>
        [TestMethod]
        public void CopyTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            SearchToolViewModel searchToolViewModel = new SearchToolViewModel(windowService, messenger);

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            SearchTreeItem assemblyItem = new SearchTreeItem(assembly, false, assembly.Name, null, null, null);
            ICommand copyCommand = searchToolViewModel.CopyCommand;
            copyCommand.Execute(assemblyItem);

            Assembly result = ClipboardManager.Instance.PeekClipboardObjects.First() as Assembly;
            Assert.IsTrue(result != null && result.Guid == assembly.Guid, "Failed to copy to clipboard a search tree item.");
        }
    }
}
