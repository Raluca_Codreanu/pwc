﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for LicenseRequestViewModel and is intended 
    /// to contain unit tests for all public methods from LicenseRequestViewModel.
    /// </summary>
    [TestClass]
    public class LicenseRequestViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseRequestViewModelTest"/> class.
        /// </summary>
        public LicenseRequestViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests CloseLicenseRequest command 
        /// </summary>
        [TestMethod]
        public void CloseLicenseRequestTest()
        {
            CompositionContainer compositionContainer = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            string serialNumber = SerialNumberManager.GenerateSerialNumber();
            LicenseRequestViewModel licenseRequestViewModel = new LicenseRequestViewModel(windowService, serialNumber);

            windowService.IsOpen = true;
            ICommand closeLicenseRequestCommand = licenseRequestViewModel.CloseLicenseRequestCommand;
            closeLicenseRequestCommand.Execute(null);

            Assert.IsFalse(windowService.IsOpen, "Failed to close a license request window.");
        }

        /// <summary>
        /// Tests CloseErrors command
        /// </summary>
        [TestMethod]
        public void CloseErrorsTest()
        {
            CompositionContainer compositionContainer = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            string serialNumber = SerialNumberManager.GenerateSerialNumber();
            LicenseRequestViewModel licenseRequestViewModel = new LicenseRequestViewModel(windowService, serialNumber);

            // Display Validation Errors grid
            licenseRequestViewModel.ValidationErrorsGridVisibility = System.Windows.Visibility.Visible;

            ICommand closeErrorsCommand = licenseRequestViewModel.CloseErrorsCommand;
            closeErrorsCommand.Execute(null);

            // Verify that errors grid was closed
            Assert.IsTrue(licenseRequestViewModel.ValidationErrorsGridVisibility == System.Windows.Visibility.Collapsed, "Failed to close the errors grid.");
        }

        /// <summary>
        /// Tests CopyToClipboard command using valid data
        /// </summary>
        [TestMethod]
        public void CopyToClipboardValidInformationTest()
        {
            CompositionContainer compositionContainer = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            string serialNumber = SerialNumberManager.GenerateSerialNumber();
            LicenseRequestViewModel licenseRequestViewModel = new LicenseRequestViewModel(windowService, serialNumber);

            licenseRequestViewModel.CompanyName = "Company" + DateTime.Now.Ticks;
            licenseRequestViewModel.UserName = "User" + DateTime.Now;
            licenseRequestViewModel.UserPhone = "0362456789";
            licenseRequestViewModel.UserFax = "0362456786";
            licenseRequestViewModel.UserEmail = "test@gmail.com";
            licenseRequestViewModel.UserStreet = "Test Street";
            licenseRequestViewModel.UserPostCode = "1000";
            licenseRequestViewModel.UserCountry = "Austria";
            licenseRequestViewModel.UserCity = "Vienna";
            licenseRequestViewModel.ApplicationVersion = "1.0.17.0";
            licenseRequestViewModel.SelectedLicenseType = licenseRequestViewModel.LicenseTypes.ElementAt(0);
            licenseRequestViewModel.SelectedLicenseLevel = licenseRequestViewModel.LicenseLevels.ElementAt(0);
            licenseRequestViewModel.StartingDate = DateTime.Now;
            licenseRequestViewModel.ExpirationDate = licenseRequestViewModel.StartingDate.AddMonths(2);

            ICommand copyToClipboardCommand = licenseRequestViewModel.CopyToClipboardCommand;
            copyToClipboardCommand.Execute(null);

            string clipboardText = Clipboard.GetText();
            bool completeText = clipboardText.Contains(licenseRequestViewModel.SerialNumber) &&
                                clipboardText.Contains(licenseRequestViewModel.CompanyName) &&
                                clipboardText.Contains(licenseRequestViewModel.UserName) &&
                                clipboardText.Contains(licenseRequestViewModel.UserPhone) &&
                                clipboardText.Contains(licenseRequestViewModel.UserFax) &&
                                clipboardText.Contains(licenseRequestViewModel.UserEmail) &&
                                clipboardText.Contains(licenseRequestViewModel.UserStreet) &&
                                clipboardText.Contains(licenseRequestViewModel.UserPostCode) &&
                                clipboardText.Contains(licenseRequestViewModel.UserCountry) &&
                                clipboardText.Contains(licenseRequestViewModel.UserCity) &&
                                clipboardText.Contains(licenseRequestViewModel.ApplicationVersion) &&
                                clipboardText.Contains(licenseRequestViewModel.SelectedLicenseType.First) &&
                                clipboardText.Contains(licenseRequestViewModel.SelectedLicenseLevel.First) &&
                                clipboardText.Contains(licenseRequestViewModel.StartingDate.ToShortDateString()) &&
                                clipboardText.Contains(licenseRequestViewModel.ExpirationDate.ToShortDateString());

            Assert.IsTrue(completeText, "Failed to copy to clipboard a license request.");
        }

        /// <summary>
        /// Tests CopyToClipboard command using invalid data - not all required information are set
        /// </summary>
        [TestMethod]
        public void CopyToClipboardInvalidInformationTest()
        {
            CompositionContainer compositionContainer = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            string serialNumber = SerialNumberManager.GenerateSerialNumber();
            LicenseRequestViewModel licenseRequestViewModel = new LicenseRequestViewModel(windowService, serialNumber);

            Clipboard.SetText(string.Empty);
            ICommand copyToClipboardCommand = licenseRequestViewModel.CopyToClipboardCommand;
            copyToClipboardCommand.Execute(null);

            // Verifies if 'Validation Errors' grid is visible and no information was copied to clipboard
            Assert.AreEqual<System.Windows.Visibility>(System.Windows.Visibility.Visible, licenseRequestViewModel.ValidationErrorsGridVisibility, "No error popup was displayed");
            Assert.AreEqual<string>(string.Empty, Clipboard.GetText(), "Invalid information was copied to clipboard");
        }

        #endregion Test Methods
    }
}
