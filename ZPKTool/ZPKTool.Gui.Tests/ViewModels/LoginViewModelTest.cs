﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for LoginViewModel and is intended 
    /// to contain all LoginViewModel Unit Test
    /// </summary>
    [TestClass]
    public class LoginViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModelTest"/> class.
        /// </summary>
        public LoginViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests Login Command using valid input parameters
        ///// </summary>
        //[TestMethod]
        //public void LoginCommandTest()
        //{
        //    CompositionContainer container = new CompositionContainer();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();

        //    LoginViewModel loginViewModel = new LoginViewModel(container, messageBoxService, messenger);
        //    loginViewModel.Username = "admin";
        //    loginViewModel.Password = "admin";

        //    // Register an action which will handle message of type NotificationMessage
        //    bool loggedInNotificationSend = false;
        //    messenger.Register<NotificationMessage>((msg) =>
        //    {
        //        if (msg.Notification == Notifications.Notification.LoggedIn)
        //        {
        //            loggedInNotificationSend = true;
        //        }
        //    });

        //    // Executes the Login command
        //    DelegateCommand loginCommand = loginViewModel.LoginCommand;
        //    loginCommand.Execute();

        //    // Login action is executed asynchron => wait until the thread is executed in order to obtain the results
        //    while (loginViewModel.LoginProgressVisibility == Visibility.Visible)
        //    {
        //        Thread.Sleep(1000);
        //    }

        //    // Verifies if a LoggedIn notification was sent
        //    Assert.IsTrue(loggedInNotificationSend, "Failed to send the notification that the user was logged in");
        //}
    }
}
