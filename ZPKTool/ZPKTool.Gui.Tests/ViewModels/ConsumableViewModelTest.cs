﻿using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    using System.Diagnostics;

    /// <summary>
    /// This class is used for test the ConsumableViewModel
    /// </summary>
    [TestClass]
    public class ConsumableViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The Measurement Unit Collection.
        /// </summary>
        private Collection<MeasurementUnit> priceUnitBaseItems;

        /// <summary>
        /// The Test Context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The Manufacturer View Model
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The Cost recalculation Clone Manager
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The Master Data Browser View Model
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Atributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate text context </param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.priceUnitBaseItems = new Collection<MeasurementUnit>();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.messenger = new Messenger();
            this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            this.unitsService = new UnitsServiceMock();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();
            var container = new CompositionContainer();
            this.manufacturer = new ManufacturerViewModel(this.windowService, this.messenger, container);
            this.masterDataBrowser = new MasterDataBrowserViewModel(
                new CompositionContainer(),
                this.messenger,
                this.windowService,
                new OnlineCheckService(this.messenger),
                this.unitsService);
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// This method tests the editable fields of Consumable General tab . Save button is also tested 
        /// </summary>
        [TestMethod]
        public void GeneralTabSaveTest()
        {
            var consumableVm = new ConsumableViewModel(
                this.windowService,
                this.messenger,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.masterDataBrowser,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumableVm.DataSourceManager = localDataManager;
            var consumable = this.CreateNewConsumable(localDataManager);
            consumableVm.Model = consumable;

            consumableVm.PriceUnit.Value = this.priceUnitBaseItems.FirstOrDefault(u => u.Symbol == "ft");
            consumableVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumableVm.Amount.Value = 100;
            consumableVm.AmountUnit.Value = this.priceUnitBaseItems.FirstOrDefault(u => u.Symbol == "ft");
            consumableVm.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumableVm.Price.Value = 10;
            consumableVm.ConsumableParent = 1000;

            var saveCommand = consumableVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(consumable.Name, consumableVm.Name.Value);
            Assert.AreEqual(consumable.Amount, consumableVm.Amount.Value);
            Assert.AreEqual(consumable.AmountUnitBase, consumableVm.AmountUnit.Value);
            Assert.AreEqual(consumable.Description, consumableVm.Description.Value);
            Assert.AreEqual(consumable.PriceUnitBase, consumableVm.PriceUnit.Value);
            Assert.AreEqual(consumable.Price, consumableVm.Price.Value);
        }

        /// <summary>
        /// This method tests the Undo Button 
        /// </summary>
        [TestMethod]
        public void UndoCommandTest()
        {
            var consumableVm = new ConsumableViewModel(
                this.windowService,
                this.messenger,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.masterDataBrowser,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumableVm.DataSourceManager = localDataManager;
            var consumable = this.CreateNewConsumable(localDataManager);
            consumableVm.Model = consumable;

            consumableVm.Amount.Value = 500;
            consumableVm.Price.Value = 50;
            consumableVm.Amount.Value = 300;
            consumableVm.Price.Value = 20;

            var undoCommand = consumableVm.UndoCommand;
            undoCommand.Execute(null);

            Assert.AreEqual(consumableVm.Price.Value, 50);
            Assert.AreEqual(consumableVm.Amount.Value, 300);

            undoCommand.Execute(null);

            Assert.AreEqual(consumableVm.Amount.Value, 500);
            Assert.AreEqual(consumableVm.Price.Value, 50);
        }

        /// <summary>
        /// This method tests the Cancel Button 
        /// </summary>
        [TestMethod]
        public void CancelCommandTest()
        {
            var consumableVm = new ConsumableViewModel(
                this.windowService,
                this.messenger,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.masterDataBrowser,
                this.unitsService);

            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumableVm.DataSourceManager = localDataManager;
            var consumable = this.CreateNewConsumable(localDataManager);
            consumableVm.Model = consumable;

            consumableVm.Amount.Value = 500;
            consumableVm.Price.Value = 50;

            var saveCommand = consumableVm.SaveCommand;
            saveCommand.Execute(null);

            Debug.Assert(consumable.Amount != null, "consumable.Amount != null");
            Assert.AreEqual(consumable.Amount.Value, 500);
            Debug.Assert(consumable.Price != null, "consumable.Price != null");
            Assert.AreEqual(consumable.Price.Value, 50);

            consumableVm.Amount.Value = 300;
            consumableVm.Price.Value = 20;

            var cancelCommand = consumableVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(consumable.Amount.Value, 500);
            Assert.AreEqual(consumable.Price.Value, 50);
        }

        /// <summary>
        /// This method tests the editable fields of Consumable Manufacturer tab . Save button is also tested 
        /// </summary>
        [TestMethod]
        public void ManufacturerTabSaveTest()
        {
            var consumableVm = new ConsumableViewModel(
                this.windowService,
                this.messenger,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.masterDataBrowser,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumableVm.DataSourceManager = localDataManager;
            var consumable = this.CreateNewConsumable(localDataManager);
            consumableVm.Model = consumable;

            this.manufacturer.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.manufacturer.Description.Value = EncryptionManager.Instance.GenerateRandomString(15, true);

            var saveCommand = consumableVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(consumable.Manufacturer.Name, this.manufacturer.Name.Value);
            Assert.AreEqual(consumable.Manufacturer.Description, this.manufacturer.Description.Value);
        }

        #endregion Test Methods

        #region Private Methods
        /// <summary>
        /// This method creates a new consymable 
        /// </summary>
        /// <returns>Returns consumable</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private Consumable CreateNewConsumable(IDataSourceManager dataManager)
        {
            var consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumable.Amount = 100;
            consumable.PriceUnitBase = this.priceUnitBaseItems.FirstOrDefault(u => u.Symbol == "kg");

            var newManufacturer = new Manufacturer { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            consumable.Manufacturer = newManufacturer;

            dataManager.ManufacturerRepository.Save(newManufacturer);
            dataManager.ConsumableRepository.Save(consumable);
            dataManager.SaveChanges();

            return consumable;
        }

        #endregion Private Methods
    }
}