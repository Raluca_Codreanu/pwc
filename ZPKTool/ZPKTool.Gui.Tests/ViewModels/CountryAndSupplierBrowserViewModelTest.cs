﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CountryAndSupplierBrowserViewModel and is intended 
    /// to contain unit tests for all commands from CountryAndSupplierBrowserViewModel
    /// </summary>
    [TestClass]
    public class CountryAndSupplierBrowserViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryAndSupplierBrowserViewModelTest"/> class.
        /// </summary>
        public CountryAndSupplierBrowserViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests CloseScreenCommand
        /// </summary>
        [TestMethod]
        public void CloseScreenTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            CountryAndSupplierBrowserViewModel countryAndSupplierBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);

            windowService.IsOpen = true;
            ICommand closeScreenCommand = countryAndSupplierBrowserViewModel.CloseScreen;
            closeScreenCommand.Execute(null);

            Assert.IsFalse(windowService.IsOpen, "Failed to close the screen.");
        }

        /// <summary>
        /// Tests SelectedTreeItemChanged command
        /// </summary>
        [TestMethod]
        public void SelectedTreeItemChangedTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            CountryAndSupplierBrowserViewModel countryAndSupplierBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country country = dataContext.CountryRepository.GetAll().FirstOrDefault();
            TreeViewDataItem treeViewDataItem = new TreeViewDataItem();
            treeViewDataItem.DataObject = country;

            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, treeViewDataItem);
            ICommand selectedTreeItemChanged = countryAndSupplierBrowserViewModel.SelectedTreeItemChangedCommand;
            selectedTreeItemChanged.Execute(args);
        }

        /// <summary>
        /// Tests SelectItemCommand using valid input data.
        /// </summary>
        [TestMethod]
        public void SelectItemTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            CountryAndSupplierBrowserViewModel countryAndSupplierBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);

            bool countryAndSupplierSelected = false;
            countryAndSupplierBrowserViewModel.CountryOrSupplierSelected += new MasterDataEntitySelectionHandler(delegate { countryAndSupplierSelected = true; });

            countryAndSupplierBrowserViewModel.LoadLocation = DbIdentifier.LocalDatabase;
            countryAndSupplierBrowserViewModel.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int countriesCount = dataContext.CountryRepository.GetAll().Count;
            while (countryAndSupplierBrowserViewModel.TreeItems == null || countryAndSupplierBrowserViewModel.TreeItems.Count < countriesCount)
            {
                Thread.Sleep(2000);
            }

            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, countryAndSupplierBrowserViewModel.TreeItems.FirstOrDefault());
            ICommand selectedTreeItemChanged = countryAndSupplierBrowserViewModel.SelectedTreeItemChangedCommand;
            selectedTreeItemChanged.Execute(args);

            windowService.IsOpen = true;
            ICommand selectItemCommand = countryAndSupplierBrowserViewModel.SelectItem;
            selectItemCommand.Execute(null);

            Assert.IsTrue(countryAndSupplierSelected, "Failed to select a country.");
            Assert.IsFalse(windowService.IsOpen, "Failed to close the CountryAndSupplierBrowserView window after the country selection.");
        }

        /// <summary>
        /// Tests KeyDown command
        /// </summary>
        [TestMethod]
        public void KeyDownTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            CountryAndSupplierBrowserViewModel countryAndSupplierBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);

            bool countryAndSupplierSelected = false;
            countryAndSupplierBrowserViewModel.CountryOrSupplierSelected += new MasterDataEntitySelectionHandler(delegate { countryAndSupplierSelected = true; });

            countryAndSupplierBrowserViewModel.LoadLocation = DbIdentifier.LocalDatabase;
            countryAndSupplierBrowserViewModel.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int countriesCount = dataContext.CountryRepository.GetAll().Count;
            while (countryAndSupplierBrowserViewModel.TreeItems == null || countryAndSupplierBrowserViewModel.TreeItems.Count < countriesCount)
            {
                Thread.Sleep(2000);
            }

            // Simulate the selection of a Tree View Item 
            TreeViewDataItem selectedItem = countryAndSupplierBrowserViewModel.TreeItems.FirstOrDefault();
            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, selectedItem);
            ICommand selectedTreeItemChanged = countryAndSupplierBrowserViewModel.SelectedTreeItemChangedCommand;
            selectedTreeItemChanged.Execute(args);

            // Create a mock KeyEventArgs parameter which will be the input data for KeyDown command
            System.Windows.Interop.HwndSourceParameters param = new System.Windows.Interop.HwndSourceParameters();
            System.Windows.Interop.HwndSource inputSource = new System.Windows.Interop.HwndSource(param);
            System.Windows.Input.KeyEventArgs args1 = new System.Windows.Input.KeyEventArgs(Keyboard.PrimaryDevice, inputSource, 100, Key.Enter);
            args1.RoutedEvent = System.Windows.Input.Keyboard.KeyDownEvent;

            windowService.IsOpen = true;
            ICommand keyDownCommand = countryAndSupplierBrowserViewModel.KeyDownCommand;
            keyDownCommand.Execute(args1);

            // Verifies that the country was selected and the view was close when 'Enter' key was pressed
            Assert.IsTrue(countryAndSupplierSelected, "Failed to select a country.");
            Assert.IsFalse(windowService.IsOpen, "Failed to close the view after the selection was done");

            // Create a mock KeyEventArgs parameter which will be the input data for KeyDown command
            countryAndSupplierSelected = false;
            System.Windows.Input.KeyEventArgs args2 = new System.Windows.Input.KeyEventArgs(Keyboard.PrimaryDevice, inputSource, 100, Key.Escape);
            args2.RoutedEvent = System.Windows.Input.Keyboard.KeyDownEvent;

            windowService.IsOpen = true;
            keyDownCommand.Execute(args2);

            // Verifies that the view was close and no country was selected
            Assert.IsFalse(countryAndSupplierSelected, "If the 'Esc' key is pressed, no country/supplier should be selected.");
            Assert.IsFalse(windowService.IsOpen, "Failed to close the view when Escape key was pressed");
        }

        /// <summary>
        /// Tests DoubleClick command
        /// </summary>
        [TestMethod]
        public void DoubleClickTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            CountryAndSupplierBrowserViewModel countryAndSupplierBrowserViewModel = new CountryAndSupplierBrowserViewModel(windowService);

            bool countryAndSupplierSelected = false;
            countryAndSupplierBrowserViewModel.CountryOrSupplierSelected += new MasterDataEntitySelectionHandler(delegate { countryAndSupplierSelected = true; });

            countryAndSupplierBrowserViewModel.LoadLocation = DbIdentifier.LocalDatabase;
            countryAndSupplierBrowserViewModel.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int countriesCount = dataContext.CountryRepository.GetAll().Count;
            while (countryAndSupplierBrowserViewModel.TreeItems == null || countryAndSupplierBrowserViewModel.TreeItems.Count < countriesCount)
            {
                Thread.Sleep(2000);
            }

            System.Windows.RoutedPropertyChangedEventArgs<object> args1 = new RoutedPropertyChangedEventArgs<object>(null, countryAndSupplierBrowserViewModel.TreeItems.FirstOrDefault());
            ICommand selectedTreeItemChanged = countryAndSupplierBrowserViewModel.SelectedTreeItemChangedCommand;
            selectedTreeItemChanged.Execute(args1);

            // Create a mock MouseEventArgs parameter which will be the input data for DoubleClick command
            System.Windows.Input.MouseEventArgs args2 = new System.Windows.Input.MouseEventArgs(Mouse.PrimaryDevice, 200);
            args2.RoutedEvent = System.Windows.Input.Mouse.MouseDownEvent;

            // Executes DoubleClick command 
            windowService.IsOpen = true;
            ICommand doubleClickCommand = countryAndSupplierBrowserViewModel.MouseDoubleClickCommand;
            doubleClickCommand.Execute(args2);

            // Verifies that the item was selected and the view was closed
            Assert.IsTrue(countryAndSupplierSelected, "Failed to select a Country with double click.");
            Assert.IsFalse(windowService.IsOpen, "Failed to close the view after an item was selected");
        }
    }
}