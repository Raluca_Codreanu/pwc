﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for DieViewModelTest and is intended 
    /// to contain all DieViewModelTest Unit Tests
    /// </summary>
    [TestClass]
    public class DieViewModelTest
    {
        #region Additional attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion

        #region Test Methods

        /// <summary>
        /// Test the cost and the calculated necessary dies per life time
        /// using null or number values for the model properties
        /// </summary>
        [TestMethod]
        public void CostUpdateTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var part = this.CreateNewPart();

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = false;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;
            dieViewModel.InitializeForCreation(false, assembly);

            // test if all values are 0
            var total = dieViewModel.DieTotalCost;
            var calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 0, "The total cost is wrong");
            Assert.AreEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test if all values are null
            var die = new Die();
            die.AllocationRatio = null;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = null;
            die.DiesetsNumberPaidByCustomer = null;
            die.Investment = null;
            die.IsMaintenanceInPercentage = false;
            die.IsWearInPercentage = false;
            die.LifeTime = null;
            die.Maintenance = null;
            die.ReusableInvest = null;
            die.Wear = null;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 0, "The total cost is wrong");
            Assert.AreEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test with null and valid values
            die = new Die();
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = null;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = false;
            die.IsWearInPercentage = false;
            die.LifeTime = 1m;
            die.Maintenance = null;
            die.ReusableInvest = null;
            die.Wear = null;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 200000M, "The total cost is wrong");
            Assert.AreNotEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test with random valid values
            die = new Die();
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.AreAllPaidByCustomer = true;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreNotEqual(total, 0, "The total cost is wrong");
            Assert.AreNotEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // same tests when parent is a part
            dieViewModel.DieParent = part;
            dieViewModel.InitializeForCreation(false, part);

            // test if all values are 0
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 0, "The total cost is wrong");
            Assert.AreEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test if all values are null
            die = new Die();
            die.AllocationRatio = null;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = null;
            die.DiesetsNumberPaidByCustomer = null;
            die.Investment = null;
            die.IsMaintenanceInPercentage = false;
            die.IsWearInPercentage = false;
            die.LifeTime = null;
            die.Maintenance = null;
            die.ReusableInvest = null;
            die.Wear = null;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 0, "The total cost is wrong");
            Assert.AreEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test with null and valid values
            die = new Die();
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = null;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = false;
            die.IsWearInPercentage = false;
            die.LifeTime = 1m;
            die.Maintenance = null;
            die.ReusableInvest = null;
            die.Wear = null;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreEqual(total, 200000M, "The total cost is wrong");
            Assert.AreNotEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");

            // test with random valid values
            die = new Die();
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.AreAllPaidByCustomer = true;
            dieViewModel.Model = die;
            total = dieViewModel.DieTotalCost;
            calcNecessarydies = dieViewModel.CalcDiesPerTime;
            Assert.AreNotEqual(total, 0, "The total cost is wrong");
            Assert.AreNotEqual(calcNecessarydies, 0, "The calculated necessary dies per life time is wrong");
        }

        /// <summary>
        /// Test using invalid data
        /// throw exception is expected
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ParentNullTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = false;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = null;
            dieViewModel.InitializeForCreation(false, null);
        }

        /// <summary>
        /// Test when isReadOnly property changed
        /// </summary>
        [TestMethod]
        public void IsReadOnlyPropertychangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = true;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;
            dieViewModel.InitializeForCreation(false, assembly);

            Assert.IsTrue(dieViewModel.ManufacturerViewModel.IsReadOnly, "IsReadOnly property not changed");
            Assert.IsTrue(dieViewModel.ManufacturerViewModel.IsReadOnly, "IsReadOnly property not changed");
        }

        /// <summary>
        /// Test for the browse the master data command
        /// </summary>
        [TestMethod]
        public void BrowseMasterDataCommandTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = true;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;
            dieViewModel.InitializeForCreation(false, assembly);
            var browseCommand = dieViewModel.BrowseCommand;

            browseCommand.Execute(null);

            Assert.IsTrue(windowService.IsOpen, "Window not open");
        }

        /// <summary>
        /// Test for the save command
        /// using valid data
        /// </summary>
        [TestMethod]
        public void ValidSaveCommandTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = true;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;

            // test with random valid values
            var die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;
            die.AreAllPaidByCustomer = true;
            dieViewModel.Model = die;

            dieViewModel.SaveCommand.Execute(null);

            // The file is was not saved
            // Verifies if currency  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool dieExist = newContext.DieRepository.CheckIfExists(die.Guid);
            Assert.IsTrue(dieExist, "The die was not saved");

            // test with another random valid values
            die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = false;
            die.IsWearInPercentage = false;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;
            die.AreAllPaidByCustomer = true;
            dieViewModel.Model = die;

            dieViewModel.SaveCommand.Execute(null);
            var dieSaved = dieViewModel.Saved;

            // The file is was not saved
            // Verifies if currency  was not saved.
            dieExist = newContext.DieRepository.CheckIfExists(die.Guid);
            Assert.IsTrue(dieExist, "The die was not saved");
            Assert.IsTrue(dieSaved, "The die was not saved");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed the, flag for the cancel dialog box is true
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = false;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;

            // create new die using random valid values
            var die = new Die();
            var name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            dieViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(6, true);
            dieViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if die  was not changed.
            Assert.AreEqual(name, die.Name, "The name was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is no
        /// </summary>
        [TestMethod]
        public void AbortedCancelVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.IsReadOnly = false;
            dieViewModel.IsInViewerMode = false;
            dieViewModel.DieParent = assembly;

            // create new die using random valid values
            var die = new Die();
            string name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = new Manufacturer()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(5, true),
                Description = EncryptionManager.Instance.GenerateRandomString(10, true)
            };

            var step = new AssemblyProcessStep() { Name = EncryptionManager.Instance.GenerateRandomString(5, true) };
            step.Dies.Add(die);
            assembly.Process.Steps.Add(step);

            dataContext.AssemblyRepository.Add(assembly);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            // change the die name
            dieViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(6, true);

            dieViewModel.CancelCommand.Execute(null);

            // The file is vas not changed
            // Verifies if die  was not changed.
            Assert.AreNotEqual(name, dieViewModel.Name.Value, "The name was modified");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The dieViewModel is changed and it's in create mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingVMIsChangedEditModeCreateTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.DieParent = assembly;

            // create new die using random valid values
            var die = new Die();
            var name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;

            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            dieViewModel.EditMode = ViewModelEditMode.Create;
            dieViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = dieViewModel.OnUnloading();

            Assert.IsTrue(status && !dieViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The dieViewModel is changed and it's in create mode
        /// and the user do not confirms cancel action
        /// </summary>
        [TestMethod]
        public void AbortedOnUnloadingVMIsChangedEditModeCreateTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            // create new die using random valid values
            var die = new Die();
            var name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;

            var step = new AssemblyProcessStep() { Dies = { die }, Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            die.ProcessStep = step;
            assembly.Process.Steps.Add(step);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.DieParent = assembly;
            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            dieViewModel.EditMode = ViewModelEditMode.Create;
            dieViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var status = dieViewModel.OnUnloading();

            Assert.IsFalse(status && !dieViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The dieViewModel is changed and it's in EditCreated mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingVMIsChangedEditModeEditTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.DieParent = assembly;

            // create new die using random valid values
            var die = new Die();
            var name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            // change the model
            die.Name = EncryptionManager.Instance.GenerateRandomString(6, true);

            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            dieViewModel.EditMode = ViewModelEditMode.Edit;
            dieViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = dieViewModel.OnUnloading();

            Assert.IsTrue(status && !dieViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The dieViewModel is changed and it's in EditCreated mode
        /// and the user do not confirms cancel action
        /// </summary>
        [TestMethod]
        public void AbortedOnUnloadingVMIsChangedEditModeEditTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var messenger = new Messenger();
            var costRecalculationCloneManager = new CostRecalculationCloneManager();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);

            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);
            var mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var dieViewModel = new DieViewModel(
                windowService,
                manufacturerViewModel,
                new ModelBrowserHelperServiceMock(),
                costRecalculationCloneManager,
                mediaViewModel,
                compositionContainer,
                unitsService,
                messenger);
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assembly = this.CreateNewAssembly();
            var manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            // create new die using random valid values
            var die = new Die();
            var name = EncryptionManager.Instance.GenerateRandomString(5, true);
            die.Name = name;
            die.AllocationRatio = 1m;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;
            die.DiesetsNumberPaidByCustomer = 2;
            die.AreAllPaidByCustomer = false;
            die.Investment = 100m;
            die.IsMaintenanceInPercentage = true;
            die.IsWearInPercentage = true;
            die.LifeTime = 1m;
            die.Maintenance = 2;
            die.ReusableInvest = 1;
            die.Wear = 1;
            die.Manufacturer = manufacturer;
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            dieViewModel.DataSourceManager = dataContext;
            dieViewModel.DieParent = assembly;
            dieViewModel.Model = die;
            dieViewModel.IsChanged = true;
            dieViewModel.EditMode = ViewModelEditMode.Create;
            dieViewModel.IsInViewerMode = false;

            // change the model data
            dieViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(6, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var status = dieViewModel.OnUnloading();

            Assert.IsFalse(status, "The die view model unloading did not stop.");
            Assert.IsTrue(dieViewModel.IsChanged, "The die view mode was not changed after the Cancel operation was aborted.");
        }

        #endregion Test Methods

        #region other methods

        /// <summary>
        /// Create a composition container using some parameters
        /// </summary>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The windows service.</param>
        /// <param name="unitsService">The units service</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(out MessageDialogServiceMock messageDialogService, out WindowServiceMock windowService, out IUnitsService unitsService)
        {
            messageDialogService = new MessageDialogServiceMock();
            windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            unitsService = new UnitsServiceMock();

            var compositionContainer = new CompositionContainer();
            var masterDataBrowserViewModel = new MasterDataBrowserViewModel(compositionContainer, new Messenger(), windowService, new Gui.Services.OnlineCheckService(new Messenger()), unitsService);
            compositionContainer.ComposeParts(messageDialogService, masterDataBrowserViewModel, unitsService);
            return compositionContainer;
        }

        /// <summary>
        /// Add a new assembly to database
        /// </summary>
        /// <returns>A new assembly object</returns>
        public Assembly CreateNewAssembly()
        {
            var assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CalculationVariant = "1.3";
            assembly.BatchSizePerYear = 1000;
            assembly.YearlyProductionQuantity = 2000;
            assembly.LifeTime = 1;
            assembly.CountrySettings = new CountrySetting();
            assembly.Manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(5, true) };
            assembly.Process = new Process();

            return assembly;
        }

        /// <summary>
        /// Add a new part to database
        /// </summary>
        /// <returns>A new part object</returns>
        public Part CreateNewPart()
        {
            var part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CalculationVariant = "1.3";
            part.BatchSizePerYear = 1000;
            part.YearlyProductionQuantity = 2000;
            part.LifeTime = 1;
            part.CountrySettings = new CountrySetting();
            part.Manufacturer = new Manufacturer();
            return part;
        }

        /// <summary>
        /// Create a user to login
        /// </summary>
        /// <param name="dataManager">The data context</param>
        public void CreateUserToLogin(IDataSourceManager dataManager)
        {
            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);
        }

        #endregion
    }
}