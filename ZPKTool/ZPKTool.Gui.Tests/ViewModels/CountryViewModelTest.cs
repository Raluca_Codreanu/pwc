﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CountryViewModel and is intended 
    /// to contain Unit Tests for all the public methods from CountryViewModel class
    /// </summary>
    [TestClass]
    public class CountryViewModelTest
    {
        /// <summary>
        /// The test context
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        /// <summary>
        /// Configure database access before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #endregion

        /// <summary>
        /// Create a new country
        /// </summary>
        /// <returns>The new created country</returns>
        public Country NewCountry()
        {
            Country newCountry = new Country();
            newCountry.CountrySetting = new CountrySetting();
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var currency = new Currency()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true),
                IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true)
            };
            newCountry.Currency = currency;

            var mu = new MeasurementUnit()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true)
            };
            newCountry.FloorMeasurementUnit = mu;
            newCountry.LengthMeasurementUnit = mu;
            newCountry.TimeMeasurementUnit = mu;
            newCountry.VolumeMeasurementUnit = mu;
            newCountry.WeightMeasurementUnit = mu;

            return newCountry;
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command in Create Mode with a valid model
        /// </summary>
        [TestMethod]
        public void SaveCreateModeTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;

            countryViewModel.SaveCommand.Execute(null);

            IDataSourceManager newManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool countrySaved = newManager.CountryRepository.CheckIfExists(newCountry.Guid);
            Assert.IsTrue(countrySaved, "The country was not saved.");
        }

        /// <summary>
        /// A test for the save command in Edit mode with a valid model
        /// </summary>
        [TestMethod]
        public void SaveEditModeTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countryViewModel.EditMode = ViewModelEditMode.Edit;
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            countryViewModel.SaveCommand.Execute(null);

            IDataSourceManager newManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool countrySaved = newManager.CountryRepository.CheckIfExists(newCountry.Guid);
            Assert.IsTrue(countrySaved, "The country was not saved.");
        }

        /// <summary>
        /// A test for the save command in Create Mode when the country is not saved because the name already exists
        /// </summary>
        [TestMethod]
        public void NotSavedCountryExistsTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            countryViewModel.EditMode = ViewModelEditMode.Create;

            Country newCountry1 = new Country();
            newCountry1.CountrySetting = new CountrySetting();
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newCountry1.Name = name;

            var c = new Currency()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true),
                IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true)
            };
            newCountry1.Currency = c;

            var mu = new MeasurementUnit() { Name = EncryptionManager.Instance.GenerateRandomString(10, true), Symbol = EncryptionManager.Instance.GenerateRandomString(2, true) };
            newCountry1.FloorMeasurementUnit = mu;
            newCountry1.LengthMeasurementUnit = mu;
            newCountry1.TimeMeasurementUnit = mu;
            newCountry1.VolumeMeasurementUnit = mu;
            newCountry1.WeightMeasurementUnit = mu;

            countryViewModel.Model = newCountry1;
            countryViewModel.SaveCommand.Execute(null);

            Country newCountry2 = new Country();
            newCountry2.CountrySetting = new CountrySetting();
            newCountry2.Name = name;

            newCountry2.Currency = c;
            newCountry2.FloorMeasurementUnit = mu;
            newCountry2.LengthMeasurementUnit = mu;
            newCountry2.TimeMeasurementUnit = mu;
            newCountry2.VolumeMeasurementUnit = mu;
            newCountry2.WeightMeasurementUnit = mu;

            countryViewModel.Model = newCountry2;
            countryViewModel.SaveCommand.Execute(null);

            IDataSourceManager newManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool country2Saved = newManager.CountryRepository.CheckIfExists(newCountry2.Guid);
            Assert.IsFalse(country2Saved, "The country was saved.");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelShowCancelMessageYesTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var newName = EncryptionManager.Instance.GenerateRandomString(8);
            countryViewModel.Name.Value = newName;
            countryViewModel.CancelCommand.Execute(null);

            Assert.AreNotEqual(newCountry.Name, newName, "The country was modified.");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is false
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelNotShowCancelMessageYesTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.ShowCancelMessageFlag = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var newName = EncryptionManager.Instance.GenerateRandomString(8);
            countryViewModel.Name.Value = newName;
            countryViewModel.CancelCommand.Execute(null);

            Assert.AreNotEqual(newCountry.Name, newName, "The country was modified.");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is no.
        /// </summary>
        [TestMethod]
        public void CancelShowCancelMessageNoTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            var newName = EncryptionManager.Instance.GenerateRandomString(8);
            newCountry.Name = newName;

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            countryViewModel.CancelCommand.Execute(null);

            Assert.AreEqual(newCountry.Name, newName, "The country was not modified.");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is false
        /// and the answer to the message dialog is no.
        /// </summary>
        [TestMethod]
        public void CancelNotShowCancelMessageNoTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.ShowCancelMessageFlag = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            var newName = EncryptionManager.Instance.GenerateRandomString(8);
            countryViewModel.Name.Value = newName;

            countryViewModel.CancelCommand.Execute(null);

            Assert.AreNotEqual(newCountry.Name, newName, "The country was modified.");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountryViewModel is changed and it's in create mode
        /// and the user do not confirms cancel action
        /// </summary>
        [TestMethod]
        public void AbortedChangesOnUnloadingTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            newCountry.CountrySetting = new CountrySetting();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var status = countryViewModel.OnUnloading();

            Assert.IsFalse(status && !countryViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountryViewModel is changed and it's in create mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void ConfirmedChangesOnUnloadingTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            newCountry.CountrySetting = new CountrySetting();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = countryViewModel.OnUnloading();

            Assert.IsTrue(status && !countryViewModel.IsChanged);
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void SaveShowCancelMessageYesTest()
        {
            var unitsService = new UnitsServiceMock();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();

            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countryViewModel.Model = newCountry;
            countryViewModel.IsChanged = true;
            countryViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countryViewModel.Name.Value = newName;
            countryViewModel.CancelCommand.Execute(null);

            Assert.AreNotEqual(newName, newCountry.Name, "The country was modified");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountryViewModel is in create mode, with changed values and the model is in view mode
        /// The message box should not appear
        /// </summary>
        [TestMethod]
        public void OnUnloadingInViewModeTest()
        {
            var unitsService = new UnitsServiceMock();
            var windowServiceMock = new Mock<IWindowService>();
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowServiceMock.Object);
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = new Country();
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newCountry.CountrySetting = new CountrySetting();

            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.IsInViewerMode = true;
            countryViewModel.DataSourceManager = dataManager;
            countryViewModel.Model = newCountry;

            countryViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            bool serviceCallHappened = false;
            windowServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo))
                .Callback(() =>
                {
                    serviceCallHappened = true;
                });

            var unloadingStatus = countryViewModel.OnUnloading();

            Assert.IsFalse(serviceCallHappened);
            Assert.IsTrue(unloadingStatus);
        }

        /// <summary>
        /// A test for the country`s initialization
        /// </summary>
        [TestMethod]
        public void InitializeForCreationTest()
        {
            var unitsService = new UnitsServiceMock();
            var windowServiceMock = new Mock<IWindowService>();
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowServiceMock.Object);
            countryViewModel.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = new Country();
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, false);
            newCountry.CountrySetting = new CountrySetting()
            {
                AirCost = 1,
                EnergyCost = 2,
                EngineerCost = 3,
                ShiftCharge1ShiftModel = 0.5m
            };

            countryViewModel.InitializeForCreation();
            var countryModel = countryViewModel.Model;

            Assert.AreEqual(countryViewModel.EditMode, ViewModelEditMode.Create);
            Assert.IsNotNull(countryModel);
            Assert.IsNotNull(countryModel.CountrySetting);
        }

        /// <summary>
        /// A test for verifying that the program will not throw an exception if the model is null when CancelCommend is executed
        /// </summary>
        [TestMethod]
        public void CancelWithNullModelTest()
        {
            var unitsService = new UnitsServiceMock();
            var windowServiceMock = new Mock<IWindowService>();
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowServiceMock.Object);
            countryViewModel.CancelCommand.Execute(null);
        }

        /// <summary>
        /// A test for creating a new valid country, and then switching the model to another new country created with null CountrySetting
        /// The code should throw an DataAccessException
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NullCountrySettingTest()
        {
            var unitsService = new UnitsServiceMock();
            var windowServiceMock = new Mock<IWindowService>();
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowServiceMock.Object);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countryViewModel.DataSourceManager = dataManager;
            Country newCountry1 = NewCountry();
            countryViewModel.Model = newCountry1;

            Country newCountry = new Country();
            newCountry.CountrySetting = null;
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var c = new Currency() { Name = EncryptionManager.Instance.GenerateRandomString(10, true), Symbol = EncryptionManager.Instance.GenerateRandomString(2, true) };
            newCountry.Currency = c;
            var mu = new MeasurementUnit() { Name = EncryptionManager.Instance.GenerateRandomString(10, true), Symbol = EncryptionManager.Instance.GenerateRandomString(2, true) };
            newCountry.FloorMeasurementUnit = mu;
            newCountry.LengthMeasurementUnit = mu;
            newCountry.TimeMeasurementUnit = mu;
            newCountry.VolumeMeasurementUnit = mu;
            newCountry.WeightMeasurementUnit = mu;

            countryViewModel.Model = newCountry;
        }

        #endregion
    }
}
