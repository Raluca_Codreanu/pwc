﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for OverheadSettingsViewModel and is intended 
    /// to contain unit tests for all commands from OverheadSettingsViewModel
    /// </summary>
    [TestClass]
    public class OverheadSettingsViewModelTest
    {
        /// <summary>
        /// The windows service
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message dialog service
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OverheadSettingsViewModelTest"/> class.
        /// </summary>
        public OverheadSettingsViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        /// <summary>
        ///  Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
        }

        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests PerformCheckForUpdate method using valid input parameters
        /// </summary>
        [TestMethod]
        public void CheckForUpdatesValidViewModel()
        {
            IMessenger messenger = new Messenger();
            OverheadSettingsViewModel overheadSettingsViewModel = new OverheadSettingsViewModel(messenger, this.windowService, new UnitsServiceMock());

            // Creates an OverheadSetting object which will be the model of OHSettingsViewModel
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.CommodityMargin = 1;
            overheadSetting.CommodityOverhead = 2;
            overheadSetting.ConsumableMargin = 3;
            overheadSetting.ConsumableOverhead = 4;
            overheadSetting.ExternalWorkMargin = 5;
            overheadSetting.LogisticOHValue = 6;
            overheadSetting.ManufacturingMargin = 7;
            overheadSetting.ManufacturingOverhead = 8;
            overheadSetting.MaterialMargin = 9;
            overheadSetting.MaterialOverhead = 10;
            overheadSetting.OtherCostOHValue = 11;
            overheadSetting.PackagingOHValue = 12;
            overheadSetting.SalesAndAdministrationOHValue = 13;
            overheadSetting.IsMasterData = false;

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            overheadSettingsViewModel.DataSourceManager = dc;
            overheadSettingsViewModel.Model = overheadSetting;
            overheadSettingsViewModel.CheckForUpdate = true;

            OverheadSetting masterDataOHSettings = dc.OverheadSettingsRepository.GetMasterData();

            // Verifies if updated properties were set to MasterDataOHSettings value
            bool result1 = overheadSettingsViewModel.UpdatedCommodityMargin.Value == masterDataOHSettings.CommodityMargin &&
                           overheadSettingsViewModel.UpdatedCommodityOverhead.Value == masterDataOHSettings.CommodityOverhead &&
                           overheadSettingsViewModel.UpdatedConsumableMargin.Value == masterDataOHSettings.ConsumableMargin &&
                           overheadSettingsViewModel.UpdatedConsumableOverhead.Value == masterDataOHSettings.ConsumableOverhead &&
                           overheadSettingsViewModel.UpdatedExternalWorkMargin.Value == masterDataOHSettings.ExternalWorkMargin &&
                           overheadSettingsViewModel.UpdatedExternalWorkOverhead.Value == masterDataOHSettings.ExternalWorkOverhead &&
                           overheadSettingsViewModel.UpdatedLogisticOverhead.Value == masterDataOHSettings.LogisticOHValue &&
                           overheadSettingsViewModel.UpdatedManufacturingMargin.Value == masterDataOHSettings.ManufacturingMargin &&
                           overheadSettingsViewModel.UpdatedManufacturingOverhead.Value == masterDataOHSettings.ManufacturingOverhead &&
                           overheadSettingsViewModel.UpdatedMaterialMargin.Value == masterDataOHSettings.MaterialMargin &&
                           overheadSettingsViewModel.UpdatedMaterialOverhead.Value == masterDataOHSettings.MaterialOverhead &&
                           overheadSettingsViewModel.UpdatedOtherCostOverhead.Value == masterDataOHSettings.OtherCostOHValue &&
                           overheadSettingsViewModel.UpdatedPackagingOverhead.Value == masterDataOHSettings.PackagingOHValue &&
                           overheadSettingsViewModel.UpdatedSalesAndAdministrationOverhead.Value == masterDataOHSettings.SalesAndAdministrationOHValue;

            Assert.IsTrue(result1, "Failed to load the updated values.");
            Assert.IsTrue(overheadSettingsViewModel.UpdateAvailable, "The update is not available for an editable model.");

            // Check for Updates after the update operation has been performed
            overheadSettingsViewModel.Model = masterDataOHSettings.Copy();
            overheadSettingsViewModel.CheckForUpdate = true;

            // Verify that the update is not available
            Assert.IsFalse(overheadSettingsViewModel.UpdateAvailable, "The update should not be available if OHSettings are identical with MD OHSettings.");
        }

        /// <summary>
        /// Tests UpdateOverheadSettings command 
        /// </summary>
        [TestMethod]
        public void UpdateOverheadSettingsTest()
        {
            IMessenger messenger = new Messenger();
            OverheadSettingsViewModel overheadSettingsViewModel = new OverheadSettingsViewModel(messenger, this.windowService, new UnitsServiceMock());

            // Creates an OverheadSetting object which will be the model of OHSettingsViewModel
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.CommodityMargin = 1;
            overheadSetting.CommodityOverhead = 2;
            overheadSetting.ConsumableMargin = 3;
            overheadSetting.ConsumableOverhead = 4;
            overheadSetting.ExternalWorkMargin = 5;
            overheadSetting.LogisticOHValue = 6;
            overheadSetting.ManufacturingMargin = 7;
            overheadSetting.ManufacturingOverhead = 8;
            overheadSetting.MaterialMargin = 9;
            overheadSetting.MaterialOverhead = 10;
            overheadSetting.OtherCostOHValue = 11;
            overheadSetting.PackagingOHValue = 12;
            overheadSetting.SalesAndAdministrationOHValue = 13;
            overheadSetting.IsMasterData = false;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            overheadSettingsViewModel.DataSourceManager = dataContext;
            overheadSettingsViewModel.Model = overheadSetting;
            overheadSettingsViewModel.CheckForUpdate = true;

            ICommand updateOverheadSettingsCommand = overheadSettingsViewModel.UpdateOverheadSettingsCommand;
            updateOverheadSettingsCommand.Execute(null);

            // Get MasterDataOverheadSetting from Local DB            
            OverheadSetting masterDataOHSettings = dataContext.OverheadSettingsRepository.GetMasterData();

            // Verifies if the overhead setting was updated
            bool overheadSettingUpdated = overheadSettingsViewModel.CommodityMargin.Value == masterDataOHSettings.CommodityMargin &&
                                          overheadSettingsViewModel.CommodityOverhead.Value == masterDataOHSettings.CommodityOverhead &&
                                          overheadSettingsViewModel.ConsumableMargin.Value == masterDataOHSettings.ConsumableMargin &&
                                          overheadSettingsViewModel.ConsumableOverhead.Value == masterDataOHSettings.ConsumableOverhead &&
                                          overheadSettingsViewModel.ExternalWorkMargin.Value == masterDataOHSettings.ExternalWorkMargin &&
                                          overheadSettingsViewModel.LogisticOverhead.Value == masterDataOHSettings.LogisticOHValue &&
                                          overheadSettingsViewModel.ManufacturingMargin.Value == masterDataOHSettings.ManufacturingMargin &&
                                          overheadSettingsViewModel.ManufacturingOverhead.Value == masterDataOHSettings.ManufacturingOverhead &&
                                          overheadSettingsViewModel.MaterialMargin.Value == masterDataOHSettings.MaterialMargin &&
                                          overheadSettingsViewModel.MaterialOverhead.Value == masterDataOHSettings.MaterialOverhead &&
                                          overheadSettingsViewModel.OtherCostOverhead.Value == masterDataOHSettings.OtherCostOHValue &&
                                          overheadSettingsViewModel.OtherCostOverhead.Value == masterDataOHSettings.PackagingOHValue &&
                                          overheadSettingsViewModel.SalesAndAdministrationOverhead.Value == masterDataOHSettings.SalesAndAdministrationOHValue;

            Assert.IsTrue(overheadSettingUpdated, "Failed to update an OverheadSettings object");
        }
    }
}
