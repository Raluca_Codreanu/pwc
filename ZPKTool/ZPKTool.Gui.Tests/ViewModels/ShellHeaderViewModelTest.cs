﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ShellHeaderViewModel and is intended 
    /// to contain unit tests for all commands from  ShellHeaderViewModel
    /// </summary>
    [TestClass]
    public class ShellHeaderViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellHeaderViewModelTest"/> class.
        /// </summary>
        public ShellHeaderViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes
    }

    /// <summary>
    /// A mock implementation of IDatabaseServerOfflineView interface
    /// </summary>
    public class ShellHeaderViewMock
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellHeaderViewMock"/> class.
        /// </summary>
        public ShellHeaderViewMock()
        {
            this.DataSourceManager = DbIdentifier.LocalDatabase;
            this.IsEnabled = false;
        }

        /// <summary>
        /// Gets or sets the data context of the view.
        /// </summary>
        public object DataSourceManager { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this view is enabled in the UI.
        /// </summary>
        /// <value>
        /// true if the view is enabled; otherwise, false.
        /// </value>
        public bool IsEnabled { get; set; }
    }
}
