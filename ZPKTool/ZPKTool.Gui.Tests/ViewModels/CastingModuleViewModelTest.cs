﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CastingModuleViewModel and is intended 
    /// to contain unit tests for all commands from CastingModuleViewModel
    /// </summary>
    [TestClass]
    public class CastingModuleViewModelTest
    {
        /// <summary>
        /// The part instance used in tests
        /// </summary>
        private Part part;

        /// <summary>
        /// The data context
        /// </summary>
        private IDataSourceManager dataContext;

        /// <summary>
        /// The windows service
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The online checker service
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// The casting module view model used in tests
        /// </summary>
        private CastingModuleViewModel castingModuleViewModel;

        /// <summary>
        /// The units service
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CastingModuleViewModelTest"/> class.
        /// </summary>
        public CastingModuleViewModelTest()
        {
            Utils.ConfigureDbAccess();
            ZPKTool.Business.LocalDataCache.Initialize();
        }

        /// <summary>
        /// Adds the CastingModule view-model created steps to the part process.
        /// </summary>
        private void AddCreatedSteps()
        {
            var process = this.part.Process;
            foreach (var step in castingModuleViewModel.CreatedSteps)
            {
                process.Steps.Add(step);
                step.Process = process;
            }
        }

        #region Additional test attributes
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void CastingModuleViewModelTestInitialize()
        {
            this.part = new Part();
            this.part.Name = part.Guid.ToString();
            this.part.OverheadSettings = new OverheadSetting();
            this.part.CountrySettings = new CountrySetting();
            var process = new Process();
            this.part.Process = process;
            process.Parts.Add(this.part);

            this.dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.onlineChecker = new OnlineCheckServiceMock();
            this.unitsService = new UnitsServiceMock();

            this.castingModuleViewModel = new CastingModuleViewModel(this.windowService, this.onlineChecker, this.unitsService);
            this.castingModuleViewModel.Part = this.part;
            this.castingModuleViewModel.PartDataContext = this.dataContext;
            this.castingModuleViewModel.NavigationService = new CastingModuleNavigationService();
        }

        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for CastingMethodSelectedCommand.
        /// </summary>
        [TestMethod]
        public void CastingMethodSelectedTest()
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();
            CastingModuleViewModel castingModuleViewModel = new CastingModuleViewModel(windowService, this.onlineChecker, this.unitsService);
            castingModuleViewModel.Part = part;
            castingModuleViewModel.PartDataContext = dataContext;
            castingModuleViewModel.NavigationService = new CastingModuleNavigationService();

            bool propertyChangedRaised = false;
            castingModuleViewModel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(delegate { propertyChangedRaised = true; });
            CastingMethod castingMethod = CastingMethod.InvestmentCastingFerrous;
            ICommand castingMethodSelectedCommand = castingModuleViewModel.CastingMethodSelectedCommand;
            castingMethodSelectedCommand.Execute(castingMethod);

            Assert.IsTrue(castingModuleViewModel.CastingMethod == castingMethod, "Failed to select the Casting Method.");
            Assert.IsTrue(propertyChangedRaised, "Failed to raise a PropertyChanged event.");
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest1()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData1
            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 0;
            castingModuleViewModel.MaterialType = CastingMaterialType.AluminiumAlloy;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest2()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData2
            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedSandWeight = 1;
            castingModuleViewModel.InputtedMeltingTime = 100;
            castingModuleViewModel.InputtedPouringTime = 90;
            castingModuleViewModel.InputtedSolidificationTime = 80;
            castingModuleViewModel.InputtedCoolingTime = 190;
            castingModuleViewModel.InputtedScrap = 50;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest3()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData3
            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;
            castingModuleViewModel.InputtedSandWeight = 0;
            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 50;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 10;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 55;
            castingModuleViewModel.InputtedCoolingTime = null;
            castingModuleViewModel.ProposedCoolingTime = 50;
            castingModuleViewModel.InputtedScrap = null;
            castingModuleViewModel.ProposedScrap = 79;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest4()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData4
            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest5()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData5
            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.InputtedPouringTime = 10;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 19;
            castingModuleViewModel.InputtedSandWeight = null;
            castingModuleViewModel.ProposedSandWeight = 1;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest6()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData6
            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest7()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData7
            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.InputtedSandWeight = null;
            castingModuleViewModel.ProposedSandWeight = 0;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 55;
            castingModuleViewModel.InputtedSolidificationTime = 77;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest8()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData8
            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest9()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData9
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest10()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData10
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest11()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingNonFerrous;

            // Set up TestData11
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest12()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData12
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest13()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData13
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest14()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData14
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest15()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData15
            castingModuleViewModel.PartWeight = 60;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest16()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData16
            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest17()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData17
            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest18()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData18
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest19()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData19
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest20()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData20
            castingModuleViewModel.PartWeight = 190;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest21()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData21
            castingModuleViewModel.PartWeight = 240;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest22()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData22
            castingModuleViewModel.PartWeight = 300;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest23()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData23
            castingModuleViewModel.PartWeight = 400;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a SandCasting method as the CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishSandCastingTest24()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.SandCastingFerrous;

            // Set up TestData24
            castingModuleViewModel.PartWeight = 400;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using an InvestmentCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishInvestmentCastingTest()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.InvestmentCastingFerrous;

            castingModuleViewModel.InputtedSandWeight = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 2;
            castingModuleViewModel.PartWeight = 10;
            castingModuleViewModel.NumberOfCores = 1;
            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest1()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData1
            castingModuleViewModel.NumberOfCores = 0;
            castingModuleViewModel.PartWeight = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest2()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData2
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = 100;
            castingModuleViewModel.InputtedSolidificationTime = 50;
            castingModuleViewModel.InputtedMeltingTime = 45;
            castingModuleViewModel.InputtedScrap = 40;
            castingModuleViewModel.InputtedCoolingTime = 50;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest3()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData3
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 500;
            castingModuleViewModel.InputtedCoolingTime = null;
            castingModuleViewModel.ProposedCoolingTime = 450;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest4()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData4
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest5()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData5
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 90;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 150;
            castingModuleViewModel.InputtedScrap = null;
            castingModuleViewModel.ProposedScrap = 35;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest6()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData6
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest7()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData7
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 300;
            castingModuleViewModel.InputtedSolidificationTime = 150;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest8()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData8
            castingModuleViewModel.NumberOfCores = 2;
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest9()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingNonFerrous;

            // Set up TestData9
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = 233;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 300;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest10()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData10
            castingModuleViewModel.NumberOfCores = 2;
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest11()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingNonFerrous;

            // Set up TestData11
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest12()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData12
            castingModuleViewModel.NumberOfCores = 2;
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest13()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData13
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 60;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest14()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData14
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest15()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData15
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest16()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData16
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest17()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData17
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 190;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest18()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData18
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 240;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest19()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData19
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 300;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a GravityDieCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishGravityDieCastingTest20()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.GravityDieCastingFerrous;

            // Set up TestData20
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 400;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a CentrifugalCasting method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishCentrifugalCastingTest()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.CentrifugalCastingFerrous;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest1()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 0;
            castingModuleViewModel.PartWeight = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest2()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedSandWeight = 60;
            castingModuleViewModel.InputtedMeltingTime = 300;
            castingModuleViewModel.InputtedPouringTime = 250;
            castingModuleViewModel.InputtedSolidificationTime = 160;
            castingModuleViewModel.InputtedScrap = 230;
            castingModuleViewModel.InputtedCoolingTime = 340;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest3()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;
            castingModuleViewModel.InputtedSandWeight = 0;
            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 200;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 220;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 230;
            castingModuleViewModel.InputtedScrap = null;
            castingModuleViewModel.ProposedScrap = 133;
            castingModuleViewModel.InputtedCoolingTime = null;
            castingModuleViewModel.ProposedCoolingTime = 215;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest4()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest5()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.InputtedSandWeight = null;
            castingModuleViewModel.ProposedSandWeight = 25;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 220;
            castingModuleViewModel.InputtedSolidificationTime = 90;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest6()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest7()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.InputtedPouringTime = 150;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 230;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest8()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest9()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest10()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest11()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest12()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest13()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest14()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 60;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest15()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest16()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest17()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest18()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest19()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 190;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest20()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 240;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest21()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 240;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest22()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 300;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest23()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 300;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureSandCast method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureSandCastTest24()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;

            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.PartWeight = 400;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest1()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.NumberOfCores = 0;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest2()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 1;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = 20;
            castingModuleViewModel.InputtedSolidificationTime = 39;
            castingModuleViewModel.InputtedMeltingTime = 30;
            castingModuleViewModel.InputtedScrap = 100;
            castingModuleViewModel.InputtedCoolingTime = 230;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest3()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 2;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 40;
            castingModuleViewModel.InputtedScrap = null;
            castingModuleViewModel.ProposedScrap = 150;
            castingModuleViewModel.InputtedCoolingTime = null;
            castingModuleViewModel.ProposedCoolingTime = 210;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest4()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 4;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest5()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 8;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 230;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 150;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest6()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 300;
            castingModuleViewModel.InputtedSolidificationTime = 230;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest7()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 15;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest8()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.InputtedPouringTime = 155;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 200;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest9()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 25;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest10()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Manual;
            castingModuleViewModel.NumberOfCores = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest11()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 40;
            castingModuleViewModel.Process = CastingProcessType.Automatic;
            castingModuleViewModel.NumberOfCores = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest12()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 60;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest13()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 90;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest14()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Manual;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest15()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 130;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest16()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 190;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.SemiAutomatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest17()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 240;
            castingModuleViewModel.NumberOfCores = 1;
            castingModuleViewModel.Process = CastingProcessType.Automatic;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest18()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 300;
            castingModuleViewModel.NumberOfCores = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a LowPressureCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishLowPressureCastTest19()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.LowPressureCastNonFerrous;

            castingModuleViewModel.PartWeight = 400;
            castingModuleViewModel.NumberOfCores = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest1()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 200000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest2()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InputtedMeltingTime = 100;
            castingModuleViewModel.InputtedCoolingTime = 45;
            castingModuleViewModel.InputtedPouringTime = 10;
            castingModuleViewModel.InputtedSolidificationTime = 90;
            castingModuleViewModel.InputtedScrap = 45;
            castingModuleViewModel.InnerDiePressureInputted = 150000;
            castingModuleViewModel.ProjectedMaxArea = 2;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest3()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 400000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest4()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InputtedMeltingTime = 100;
            castingModuleViewModel.InputtedCoolingTime = null;
            castingModuleViewModel.ProposedCoolingTime = 120;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 120;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 230;
            castingModuleViewModel.InputtedScrap = null;
            castingModuleViewModel.ProposedScrap = 100;
            castingModuleViewModel.InnerDiePressureInputted = null;
            castingModuleViewModel.InnerDiePressureProposed = 500000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest5()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 600000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest6()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 120;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 120;
            castingModuleViewModel.InputtedSolidificationTime = 300;
            castingModuleViewModel.InnerDiePressureInputted = 800000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest7()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InputtedMeltingTime = null;
            castingModuleViewModel.ProposedMeltingTime = 120;
            castingModuleViewModel.InputtedPouringTime = null;
            castingModuleViewModel.ProposedPouringTime = 120;
            castingModuleViewModel.InputtedSolidificationTime = 300;
            castingModuleViewModel.InnerDiePressureInputted = 900000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest8()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InputtedPouringTime = 630;
            castingModuleViewModel.InputtedSolidificationTime = null;
            castingModuleViewModel.ProposedSolidificationTime = 230;
            castingModuleViewModel.InnerDiePressureInputted = 1000000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest9()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 1400000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest10()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 1800000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest11()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 2200000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest12()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 2800000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for FinishCommand using a PressureDieCast(non-ferrous) method as CastingMethod.
        /// </summary>
        [TestMethod]
        public void FinishPressureDieCastTest13()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            castingModuleViewModel.InnerDiePressureInputted = 3000000;
            castingModuleViewModel.ProjectedMaxArea = 1;

            ICommand finishCommand = castingModuleViewModel.FinishCommand;
            finishCommand.Execute(null);

            this.AddCreatedSteps();

            ValidateCastingProcess(castingModuleViewModel, part, dataContext);
        }

        /// <summary>
        /// A test for CastingMethodSelectedCommand.
        /// </summary>
        [TestMethod]
        public void SelectCastingMethodTest()
        {
            castingModuleViewModel.CastingMethod = CastingMethod.CentrifugalCastingFerrous;
            ICommand selectCommand = castingModuleViewModel.CastingMethodSelectedCommand;
            selectCommand.Execute(CastingMethod.GravityDieCastingFerrous);

            Assert.AreEqual((short)castingModuleViewModel.CastingMethod, (short)CastingMethod.GravityDieCastingFerrous, "Failed to select a casting method.");
        }

        /// <summary>
        /// A test for validating the ProposedNumberOfCavities value.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();

            part.RawMaterials.Add(material);

            var castingVM = new CastingModuleViewModel(windowService, onlineChecker, this.unitsService);
            castingVM.Part = part;
            castingVM.PartDataContext = dsm;

            // For each CastingMethod value force the evaluation of the CalculateProposedSettings method and then validate ProposedNumberOfCavities.
            part.YearlyProductionQuantity = 50000;
            material.ParentWeight = 5m;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(1m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 15m;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(1m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 10m;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(2m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 5m;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(4m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 2m;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(6m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            castingVM.CastingMethod = CastingMethod.InvestmentCastingFerrous;
            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 1m;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(10m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            castingVM.CastingMethod = CastingMethod.InvestmentCastingFerrous;
            part.YearlyProductionQuantity = 150000;
            material.ParentWeight = 0.1m;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(20m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");

            part.YearlyProductionQuantity = 100000;
            material.ParentWeight = 0m;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(8m, castingVM.ProposedNumberOfCavities.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities'.");
        }

        /// <summary>
        /// A test for validating the Proposed Ratio of Sprue Fillers value depending on the Casting Method value.
        /// </summary>
        [TestMethod]
        public void ValidateProposedRatioOfSprueFillers()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var castingVM = new CastingModuleViewModel(windowService, onlineChecker, this.unitsService);
            castingVM.Part = part;
            castingVM.PartDataContext = dsm;

            // For each CastingMethod value force the evaluation of the CalculateProposedSettings method and then validate ProposedRatioOfSprueFillers.
            castingVM.CastingMethod = CastingMethod.InvestmentCastingFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(0.85m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.InvestmentCastingNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(0.85m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.PressureDieCastNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(0.65m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.CentrifugalCastingFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.GravityDieCastingFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.GravityDieCastingNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.LowPressureCastNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.LowPressureSandCastNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.SandCastingFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");

            castingVM.CastingMethod = CastingMethod.SandCastingNonFerrous;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(0.95m, castingVM.ProposedRatioOfSprueFillers.GetValueOrDefault(), "Wrong 'ProposedRatioOfSprueFillers'.");
        }

        /// <summary>
        /// A test for validating the Process value.
        /// </summary>
        [TestMethod]
        public void ValidateProcess()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var castingVM = new CastingModuleViewModel(windowService, onlineChecker, this.unitsService);
            castingVM.Part = part;
            castingVM.PartDataContext = dsm;

            // Force the evaluation of the CalculateProposedSettings method and validate Process depending on the parent part YearlyProductionQuantity.
            part.YearlyProductionQuantity = 1000000;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<CastingProcessType>(CastingProcessType.Automatic, castingVM.Process, "Wrong 'Process'.");

            part.YearlyProductionQuantity = 10000;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<CastingProcessType>(CastingProcessType.SemiAutomatic, castingVM.Process, "Wrong 'Process'.");

            part.YearlyProductionQuantity = 100;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<CastingProcessType>(CastingProcessType.Manual, castingVM.Process, "Wrong 'Process'.");
        }

        /// <summary>
        /// A test for validating the InnerDiePressureProposed value.
        /// </summary>
        [TestMethod]
        public void ValidateInnerDiePressureProposed()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var castingVM = new CastingModuleViewModel(windowService, onlineChecker, this.unitsService);
            castingVM.Part = part;
            castingVM.PartDataContext = dsm;
            castingVM.CastingMethod = CastingMethod.PressureDieCastNonFerrous;

            // First set an evaluated property to a diferent value of the one that will be evaluated to make sure that the CalculateProposedSettings method gets triggered.
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            castingVM.PrecisionDemand = CastingPrecisionDemand.Normal;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(300m, castingVM.InnerDiePressureProposed.Value, "Wrong 'InnerDiePressureProposed'.");

            castingVM.DensityDemand = CastingDensityDemand.Normal;
            castingVM.PrecisionDemand = CastingPrecisionDemand.Normal;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(600m, castingVM.InnerDiePressureProposed.Value, "Wrong 'InnerDiePressureProposed'.");

            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            castingVM.PrecisionDemand = CastingPrecisionDemand.TightTolerances;
            castingVM.DensityDemand = CastingDensityDemand.Normal;
            Assert.AreEqual<decimal>(600m, castingVM.InnerDiePressureProposed.Value, "Wrong 'InnerDiePressureProposed'.");

            castingVM.DensityDemand = CastingDensityDemand.Normal;
            castingVM.PrecisionDemand = CastingPrecisionDemand.TightTolerances;
            castingVM.DensityDemand = CastingDensityDemand.Pressuretight;
            Assert.AreEqual<decimal>(900m, castingVM.InnerDiePressureProposed.Value, "Wrong 'InnerDiePressureProposed'.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// A method that checks if the Casting Process was created properly.
        /// </summary>
        /// <param name="viewModel">The view model for which to check if the generated casting is valid.</param>
        /// <param name="part">The part on which the casting module was worked.</param>
        /// <param name="partChangeSet">The change set of the part.</param>
        private void ValidateCastingProcess(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            switch (viewModel.CastingMethod)
            {
                case CastingMethod.SandCastingFerrous:
                case CastingMethod.SandCastingNonFerrous:
                    ValidateSandCastingProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.InvestmentCastingFerrous:
                case CastingMethod.InvestmentCastingNonFerrous:
                    ValidateInvestmentCastingProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.GravityDieCastingFerrous:
                case CastingMethod.GravityDieCastingNonFerrous:
                    ValidateGravityDieCastingProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.CentrifugalCastingFerrous:
                    ValidateCentrifugalCastingProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.LowPressureSandCastNonFerrous:
                    ValidateLowPressureSandCastProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.LowPressureCastNonFerrous:
                    ValidateLowPressureCastProcessSteps(viewModel, part, partChangeSet);
                    break;
                case CastingMethod.PressureDieCastNonFerrous:
                    ValidatePressureDieCastProcessSteps(viewModel, part, partChangeSet);
                    break;
            }

            foreach (ProcessStep step in part.Process.Steps)
            {
                Assert.AreEqual<bool>(part.IsMasterData, step.IsMasterData, "Failed to set IsMasterData flag.");
                Assert.IsTrue((part.Owner != null && part.Owner.Guid == step.Owner.Guid) || (part.Owner == null && step.Owner == null), "A step which is MasterData doesn't have owner.");
                ValidateProcessStepDefaultValues(step);
            }
        }

        /// <summary>
        /// A method that checks if the Sand Casting Process was created properly.
        /// </summary>
        /// <param name="viewModel">The view model of the SandCastingProcess.</param>
        /// <param name="part">The part on which the casting module was worked.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateSandCastingProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            // Validate the mold manufacturing step
            ProcessStep moldManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MoldManufacturing);
            Assert.IsNotNull(moldManufacturingStep, "SandCasting -> Failed to generate the MoldManufacturingStep.");
            ValidateMoldManufacturingStep(viewModel, moldManufacturingStep, partChangeSet);
            ValidateMoldManufacturingMachines(viewModel, moldManufacturingStep);

            // Validate the CoreManufacturingStep and CoreAndMoldAssemblyStep
            if (viewModel.NumberOfCores > 0)
            {
                ProcessStep coreManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreManufacturing);
                Assert.IsNotNull(coreManufacturingStep, "SandCasting -> Failed to generate the CoreManufacturingStep.");
                ValidateCoreManufacturingStep(viewModel, coreManufacturingStep, partChangeSet);
                ValidateCoreManufacturingMachines(viewModel, coreManufacturingStep);

                ProcessStep coreAndMoldAssemblyStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly);
                Assert.IsNotNull(coreAndMoldAssemblyStep, "SandCasting -> Failed to generate the CoreAndMoldAssemblyStep.");
                ValidateCoreAndMoldAssemblyStepForSandCasting(viewModel, coreAndMoldAssemblyStep, partChangeSet);
                ValidateCoreAndMoldAssemblyMachines(viewModel, coreAndMoldAssemblyStep);
            }

            // Validate the MeltingStep
            ProcessStep meltingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Melting);
            Assert.IsNotNull(meltingStep, "SandCasting -> Failed to generate the MeltingStep.");
            ValidateMeltingStep(viewModel, meltingStep, partChangeSet);
            ValidateMeltingMachines(viewModel, meltingStep);

            // Validate the PouringStep
            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Pouring);
            Assert.IsNotNull(pouringStep, "SandCasting -> Failed to generate the PouringStep.");
            ValidatePouringStep(viewModel, pouringStep, partChangeSet);
            ValidatePouringMachines(viewModel, pouringStep);

            // Validate the CoolingStep
            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "SandCasting -> Failed to generate the CoolingStep.");
            ValidateCoolingStep(viewModel, coolingStep, partChangeSet);
            ValidateCoolingMachines(viewModel, coolingStep);

            // Validate the CastingRemovalStep
            ProcessStep castingRemovalStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CastingRemoval);
            Assert.IsNotNull(castingRemovalStep, "SandCasting -> Failed to generate the CastingRemovalStep.");
            ValidateCastingRemovalStep(viewModel, castingRemovalStep, partChangeSet);
            ValidateCastingRemovalMachines(viewModel, castingRemovalStep);

            // Validate the DeburringStep
            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "SandCasting -> Failed to generate the DeburringStep.");
            ValidateDeburringStep(viewModel, deburringStep, partChangeSet);
            ValidateDeburringMachines(viewModel, deburringStep);

            // Validate the AbrasiveBlastingStep
            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "SandCasting -> Failed to generate the AbrasiveBlastingStep.");
            ValidateAbrasiveBlastingStep(viewModel, abrasiveBlastingStep, partChangeSet);
            ValidateAbrasiveBlastingMachines(viewModel, abrasiveBlastingStep);

            // Validate the XRayStep
            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "SandCasting -> Failed to generate the XRayStep.");
            ValidateXRayStep(viewModel, xRayStep, partChangeSet);
            ValidateXRayMachines(viewModel, xRayStep);

            // Validate the CheckAndPackStep
            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "SandCasting -> Failed to generate the CheckAndPackStep.");
            ValidateCheckAndPackStep(viewModel, checkAndPackStep, partChangeSet);
        }

        /// <summary>
        /// A method that checks if the InvestmentCasting was properly created.
        /// </summary>
        /// <param name="viewModel">The view model of the InvestmentCastingProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateInvestmentCastingProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            ProcessStep patternTreeAssemblingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_PatternTreeAssembling);
            Assert.IsNotNull(patternTreeAssemblingStep, "InvestmentCasting -> Failed to generate the expected step.");

            ProcessStep moldCreationStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MoldCreation);
            Assert.IsNotNull(moldCreationStep, "InvestmentCasting -> MoldCreationStep -> Failed to generate the expected step.");

            ProcessStep meltingOutStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MeltingOut);
            Assert.IsNotNull(meltingOutStep, "InvestmentCasting -> MeltingOutStep -> Failed to generate the expected step.");

            ProcessStep burningMoldStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_BurningMold);
            Assert.IsNotNull(burningMoldStep, "InvestmentCasting -> BurningMoldStep -> Failed to generate the expected step.");

            ProcessStep meltingAlloyStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MeltingAlloy);
            Assert.IsNotNull(meltingAlloyStep, "InvestmentCasting -> MeltingAlloyStep -> Failed to generate the expected step.");

            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Pouring);
            Assert.IsNotNull(pouringStep, "InvestmentCasting -> PouringStep -> Failed to generate the expected step.");
            ValidatePouringStep(viewModel, pouringStep, partChangeSet);

            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "InvestmentCasting -> CoolingStep -> Failed to generate the expected step.");
            ValidateCoolingStep(viewModel, coolingStep, partChangeSet);

            ProcessStep castingRemovalStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CastingRemoval);
            Assert.IsNotNull(castingRemovalStep, "InvestmentCasting -> CastingRemovalStep -> Failed to generate the expected step.");

            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "InvestmentCasting -> DeburringStep -> Failed to generate the expected step.");

            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "InvestmentCasting -> AbrasiveBlastingStep -> Failed to generate the expected step.");

            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "InvestmentCasting -> XRayStep -> Failed to generate the expected step.");

            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "InvestmentCasting -> CheckAndPackStep -> Failed to generate the expected step.");
        }

        /// <summary>
        /// A method that checks if the GravityDieCasting was properly created.
        /// </summary>
        /// <param name="viewModel">The view model of the GravityDieCastingProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateGravityDieCastingProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            if (viewModel.NumberOfCores > 0)
            {
                ProcessStep coreManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreManufacturing);
                Assert.IsNotNull(coreManufacturingStep, "GravityDieCasting -> Failed to generate the CoreManufacturing step.");
                ValidateCoreManufacturingStep(viewModel, coreManufacturingStep, partChangeSet);
                ValidateCoreManufacturingMachines(viewModel, coreManufacturingStep);

                ProcessStep coreAndMoldAssemblyStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly);
                Assert.IsNotNull(coreAndMoldAssemblyStep, "GravityDieCasting -> Failed to generate the CoreAndMoldAssemblyStep.");
                ValidateCoreAndMoldAssemblyMachines(viewModel, coreAndMoldAssemblyStep);
            }

            PartProcessStep meltingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Melting) as PartProcessStep;
            Assert.IsNotNull(meltingStep, "GravityDieCasting -> Failed to generate the MeltingStep.");
            ValidateMeltingStep(viewModel, meltingStep, partChangeSet);
            ValidateMeltingMachines(viewModel, meltingStep);

            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Pouring);
            Assert.IsNotNull(pouringStep, "GravityDieCasting -> Failed to generate the PouringStep.");
            ValidatePouringStep(viewModel, pouringStep, partChangeSet);
            ValidatePouringMachinesForGravityDieCast(viewModel, pouringStep);

            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "GravityDieCasting -> Failed to generate the CoolingStep");
            ValidateCoolingStep(viewModel, coolingStep, partChangeSet);
            ValidateCoolingMachines(viewModel, coolingStep);

            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "GravityDieCasting -> Failed to generate the DeburingStep.");
            ValidateDeburringStep(viewModel, deburringStep, partChangeSet);
            ValidateDeburringMachines(viewModel, deburringStep);

            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "GravityDieCasting -> Failed to generate the AbrasiveBlastingStep.");
            ValidateAbrasiveBlastingStep(viewModel, abrasiveBlastingStep, partChangeSet);
            ValidateAbrasiveBlastingMachines(viewModel, abrasiveBlastingStep);

            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "GravityDieCasting -> Failed to generate the XRayStep.");
            ValidateXRayStep(viewModel, xRayStep, partChangeSet);
            ValidateXRayMachines(viewModel, xRayStep);

            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "GravityDieCasting -> Failed to generate the CheckAndPackStep.");
            ValidateCheckAndPackStep(viewModel, checkAndPackStep, partChangeSet);
        }

        /// <summary>
        /// A method that checks if the CentrifugalCasting was properly created.
        /// </summary>
        /// <param name="viewModel">The view model of the CentrifugalCastingProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateCentrifugalCastingProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            // Validate the MeltingAndFillingStep
            ProcessStep meltingAndFillingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MeltingAndFilling);
            Assert.IsNotNull(meltingAndFillingStep, "CentrifugalCasting -> Failed to generate the MeltingAndFillingStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle and ProductionUnskilledLabour of MeltingAndFillingStep
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
            decimal processMaterial = calculator.CalculateProcessMaterial(part);
            decimal meltingAndFillingCycleTime = (processMaterial / 650m) * 3600m;
            Assert.AreEqual<decimal>(meltingAndFillingCycleTime, meltingAndFillingStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> MeltingAndFillingStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(meltingAndFillingStep.CycleTime.GetValueOrDefault(), meltingAndFillingStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> MeltingAndFillingStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(4, meltingAndFillingStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> MeltingAndFillingStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0.25m, meltingAndFillingStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> MeltingAndFillingStep -> Wrong value of ProductionUnskilledLabour.");

            // Validate the machines of MeltingAndFillingStep
            Machine meltingAndFillingMachine = meltingAndFillingStep.Machines.FirstOrDefault(m => m.Name == "Spcuncast Invest");
            Assert.IsNotNull(meltingAndFillingMachine, "CentrifugalCasting -> MeltingAndFillingStep -> Failed to generate the expected machine.");

            // Validate the PreparationOfDieStep
            ProcessStep preparationOfDieStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_PreparationOfDie);
            Assert.IsNotNull(preparationOfDieStep, "CentrifugalCasting -> Failed to generate the PreparationOfDieStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle,SetupUnskilledLabour and ProductionUnskilledLabour of PreparationOfDieStep
            Assert.AreEqual<decimal>(1800m, preparationOfDieStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(preparationOfDieStep.CycleTime.GetValueOrDefault(), preparationOfDieStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(1, preparationOfDieStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(2m, preparationOfDieStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of ProductionUnskilledLabour.");
            Assert.AreEqual<decimal>(2m, preparationOfDieStep.SetupUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of SetupUnskilledLabour.");

            // Validate the consumable of PreparationOfDieStep
            MeasurementUnit kg = partChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "kg" && !u.IsReleased);
            Consumable preparationOfDieConsumable = preparationOfDieStep.Consumables.FirstOrDefault(c => c.Name == "Holcote 110");
            Assert.IsNotNull(preparationOfDieConsumable, "CentrifugalCasting -> PreparationOfDieStep -> Failed to generate the expected consumable");
            Assert.AreEqual<decimal>(0.9m, preparationOfDieConsumable.Amount.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Consumable Amount.");
            Assert.AreEqual<Guid>(kg.Guid, preparationOfDieConsumable.AmountUnitBase.Guid, "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Consumable AmountUnitBase.");
            Assert.AreEqual<decimal>(2.3555m, preparationOfDieConsumable.Price.GetValueOrDefault(), "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Consumable Price.");
            Assert.AreEqual<Guid>(kg.Guid, preparationOfDieConsumable.PriceUnitBase.Guid, "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Consumable PriceUnitBase.");
            Assert.AreEqual<string>(@"Refractory  (price based on request (Pre Mixed and homogenized)" + Environment.NewLine + "Baise Price Zi-Silcate: 0,8$/kg", preparationOfDieConsumable.Description, "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Consumable Description.");
            Assert.IsNotNull(preparationOfDieConsumable.Manufacturer, "CentrifugalCasting -> PreparationOfDieStep -> Failed to generate a Manufacturer for the consumable.");
            Assert.AreEqual<string>("Foseco", preparationOfDieConsumable.Manufacturer.Name, "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Name of Consumable's Manufacturer.");
            Assert.AreEqual<string>("Refractory on Zircon-Silicate Base", preparationOfDieConsumable.Manufacturer.Description, "CentrifugalCasting -> PreparationOfDieStep -> Wrong value of Description of Consumable's Manufacturer.");

            // Validate the machine of PreparationOfDieStep
            Machine preparationOfDieMachine = preparationOfDieStep.Machines.FirstOrDefault(m => m.Name == "Spin Cast Equipment (1 Cast Place)");
            Assert.IsNotNull(preparationOfDieMachine, "CentrifugalCasting -> PreparationOfDieStep -> Failed to generate the expected machine.");

            // Validate the PouringAndSolidificationStep
            ProcessStep pouringAndSolidificationStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_PouringAndSolidification);
            Assert.IsNotNull(pouringAndSolidificationStep, "CentrifugalCasting ->Failed to generate the PouringAndSolidificationStep.");

            // Validate the CycleTime, ProcessTime, ScrapAmount, PartsPerCycle,SetupUnskilledLabour and ProductionUnskilledLabour of PouringAndSolidificationStep
            decimal pouringAndSolidificationCycleTime = (processMaterial / 650m) * 1.05m * 1069m;
            Assert.AreEqual<decimal>(pouringAndSolidificationCycleTime, pouringAndSolidificationStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(pouringAndSolidificationStep.CycleTime.GetValueOrDefault(), pouringAndSolidificationStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<decimal>(0.02m, pouringAndSolidificationStep.ScrapAmount.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of ScrapAmount.");
            Assert.AreEqual<int>(1, pouringAndSolidificationStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(2m, pouringAndSolidificationStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of ProductionUnskilledLabour.");
            Assert.AreEqual<decimal>(2m, pouringAndSolidificationStep.SetupUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> PouringAndSolidificationStep -> Wrong value of SetupUnskilledLabour.");

            // Validate the machine of PouringAndSolidificationStep
            Machine pouringAndSolidificationMachine = pouringAndSolidificationStep.Machines.FirstOrDefault(m => m.Name == "Spin Cast Equipment (1 Cast Place)");
            Assert.IsNotNull(pouringAndSolidificationMachine, "CentrifugalCasting -> PouringAndSolidificationStep -> Failed to generate the expected machine.");

            // Validate the RemovalFromCastStep
            ProcessStep removalFromCastStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_RemovalFromCast);
            Assert.IsNotNull(removalFromCastStep, "CentrifugalCasting -> Failed to generate the RemovalFromCastStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle and ProductionUnskilledLabour of RemovalFromCastStep
            Assert.AreEqual<decimal>(1800m, removalFromCastStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> RemovalFromCastStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(removalFromCastStep.CycleTime.GetValueOrDefault(), removalFromCastStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> RemovalFromCastStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(1, removalFromCastStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> RemovalFromCastStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(2m, removalFromCastStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> RemovalFromCastStep -> Wrong value of ProductionUnskilledLabour.");

            // Validate the CoolingStep1
            decimal coolingStep1CycleTime = (processMaterial / 650m) * 0.92m * 32619m;
            ProcessStep coolingStep1 = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling && s.CycleTime == coolingStep1CycleTime);
            Assert.IsNotNull(coolingStep1, "CentrifugalCasting -> Failed to generate the first CoolingStep.");

            // Validate the ProcessTime and PartsPerCycle of CoolingStep1
            Assert.AreEqual<decimal>(coolingStep1.CycleTime.GetValueOrDefault(), coolingStep1.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> CoolingStep1 -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(20, coolingStep1.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> CoolingStep1 -> Wrong value of PartsPerCycle.");

            // Validate the machines of the CoolingStep1
            Machine coolingMachine1 = coolingStep1.Machines.FirstOrDefault(m => m.Name == "Cooling Space");
            Assert.IsNotNull(coolingMachine1, "CentrifugalCasting -> CoolingStep1->  Failed to generate the expected machine");

            // Validate the CutOffEndsStep
            ProcessStep cutOffEndsStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CutOffEnds);
            Assert.IsNotNull(cutOffEndsStep, "CentrifugalCasting -> Failed to generate the CutOffEndsStep.");

            // Validate the CycleTime, ProcessTime, ScrapAmount, PartsPerCycle, SetupUnskilledLabour and ProductionUnskilledLabour of CutOffEndsStep
            decimal cutOffEndsCycleTime = (processMaterial / 650m) * 0.7m * 750m;
            Assert.AreEqual<decimal>(cutOffEndsCycleTime, cutOffEndsStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(cutOffEndsStep.CycleTime.GetValueOrDefault(), cutOffEndsStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<decimal>(0.005m, cutOffEndsStep.ScrapAmount.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of ScrapAmount.");
            Assert.AreEqual<int>(1, cutOffEndsStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0.5m, cutOffEndsStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of ProductionUnskilledLabour.");
            Assert.AreEqual<decimal>(1m, cutOffEndsStep.SetupUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of SetupUnskilledLabour.");

            // Validate the die of the CutOffEndsStep
            Die cutOffEndsDie = cutOffEndsStep.Dies.FirstOrDefault(d => d.Name == "Cutting Tools");
            Assert.IsNotNull(cutOffEndsDie, "CentrifugalCasting -> CutOffEndsStep -> Failed to generate the expected Die.");
            Assert.AreEqual(DieType.Tools, cutOffEndsDie.Type.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of Die Type.");
            Assert.AreEqual<decimal>(4.038m, cutOffEndsDie.Investment.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of Investment.");
            Assert.AreEqual<decimal>(0m, cutOffEndsDie.ReusableInvest.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of ReusableInvest.");
            Assert.AreEqual<decimal>(0m, cutOffEndsDie.Wear.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of Wear.");
            Assert.AreEqual<decimal>(0m, cutOffEndsDie.Maintenance.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of Maintenance.");
            Assert.AreEqual<decimal>(1m, cutOffEndsDie.LifeTime.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of LifeTime.");
            Assert.AreEqual<decimal>(1m, cutOffEndsDie.AllocationRatio.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of AllocationRatio.");
            Assert.AreEqual<decimal>(0m, cutOffEndsDie.DiesetsNumberPaidByCustomer.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of DiesetsNumberPaidByCustomer.");
            Assert.IsTrue(cutOffEndsDie.CostAllocationBasedOnPartsPerLifeTime.GetValueOrDefault(), "CentrifugalCasting -> CutOffEndsStep -> Wrong value of CostAllocationBasedOnPartsPerLifeTime.");
            Assert.IsNotNull(cutOffEndsDie.Manufacturer, "CentrifugalCasting -> CutOffEndsStep -> Failed to generate a Manufacturer for the generated Die.");
            Assert.AreEqual<string>("Kenametal", cutOffEndsDie.Manufacturer.Name, "CentrifugalCasting -> CutOffEndsStep -> Wrong Name of Die's Manufacturer.");

            // Validate the machines of CutOffEndsStep
            Machine cutOffEndsMachine = cutOffEndsStep.Machines.FirstOrDefault(m => m.Name == "End Cutter");
            Assert.IsNotNull(cutOffEndsMachine, "CentrifugalCasting -> CutOffEndsStep -> Failed to generate the expected machine.");

            // Validate the BlastStep
            ProcessStep blastStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Blast);
            Assert.IsNotNull(blastStep, "CentrifugalCasting -> Failed to generate the BlastStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle and ProductionUnskilledLabour of BlastStep
            Assert.AreEqual<decimal>(120m, blastStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> BlastStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(blastStep.CycleTime.GetValueOrDefault(), blastStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> BlastStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(1, blastStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> BlastStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0.5m, blastStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> BlastStep -> Wrong value of ProductionUnskilledLabour.");

            // Validate the machines of the BlastStep
            Machine blastMachine = blastStep.Machines.FirstOrDefault(m => m.Name == "WA 400");
            Assert.IsNotNull(blastMachine, "CentrifugalCasting -> BlastStep -> Failed to generate the expected machine.");

            // Validate the HeatTreatementStep
            ProcessStep heatTreatementStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_HeatTreatment);
            Assert.IsNotNull(heatTreatementStep, "CentrifugalCasting -> Failed to generate the HeatTreatementStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle, SetupUnskilledLabour and ProductionUnskilledLabour of HeatTreatementStep
            Assert.AreEqual<decimal>(32400m, heatTreatementStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> HeatTreatementStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(heatTreatementStep.CycleTime.GetValueOrDefault(), heatTreatementStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> HeatTreatementStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(10, heatTreatementStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> HeatTreatementStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0.5m, heatTreatementStep.ProductionUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> HeatTreatementStep -> Wrong value of ProductionUnskilledLabour.");
            Assert.AreEqual<decimal>(1m, heatTreatementStep.SetupUnskilledLabour.GetValueOrDefault(), "CentrifugalCasting -> HeatTreatementStep -> Wrong value of SetupUnskilledLabour.");

            // Validate the machines of 
            Machine heatTreatementMachine = heatTreatementStep.Machines.FirstOrDefault(m => m.Name == "Chamber Oven KSG 5/2-150");
            Assert.IsNotNull(heatTreatementMachine, "CentrifugalCasting -> HeatTreatementStep -> Failed to generate the expected machine.");

            // Validate the CoolingStep2
            decimal coolingStep2CycleTime = (processMaterial / 650m) * 0.92m * 18000m;
            ProcessStep coolingStep2 = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling && s.CycleTime == coolingStep2CycleTime);
            Assert.IsNotNull(coolingStep2, "CentrifugalCasting -> Failed to generate the second CoolingStep.");

            // Validate the ProcessTime and PartsPerCycle of CoolingStep2
            Assert.AreEqual<decimal>(coolingStep2.CycleTime.GetValueOrDefault(), coolingStep2.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> CoolingStep2 -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(20, coolingStep2.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> CoolingStep2 -> Wrong value of PartsPerCycle.");

            // Validate the machines of the CoolingStep1
            Machine coolingMachine2 = coolingStep2.Machines.FirstOrDefault(m => m.Name == "Cooling Space");
            Assert.IsNotNull(coolingMachine2, "CentrifugalCasting -> CoolingStep2->  Failed to generate the expected machine");

            // Validate the MagneticParticleAnalysisStep
            ProcessStep magneticParticleAnalysisStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MagneticParticleAnalysis);
            Assert.IsNotNull(magneticParticleAnalysisStep, "CentrifugalCasting -> Failed to generated the MagneticParticleAnalysisStep.");

            // Validate the CycleTime, ProcessTime, PartsPerCycle, ProductionSkilledLabour and SetupSkilledLabour of MagneticParticleAnalysisStep
            Assert.AreEqual<decimal>(1800m, magneticParticleAnalysisStep.CycleTime.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(magneticParticleAnalysisStep.CycleTime.GetValueOrDefault(), magneticParticleAnalysisStep.ProcessTime.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<int>(1, magneticParticleAnalysisStep.PartsPerCycle.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(2m, magneticParticleAnalysisStep.ProductionSkilledLabour.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(2m, magneticParticleAnalysisStep.SetupSkilledLabour.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of SetupSkilledLabour.");

            // Validate the consumable of MagneticParticleAnalysisStep
            Consumable magneticParticleAnalysisConsumable = magneticParticleAnalysisStep.Consumables.FirstOrDefault(c => c.Name == "Flux  Powder");
            Assert.IsNotNull(magneticParticleAnalysisConsumable, "CentrifugalCasting -> MagneticParticleAnalysisStep -> Failed to generate the expected consumable.");
            Assert.AreEqual<decimal>(0.3m, magneticParticleAnalysisConsumable.Amount.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of Consumable Amount.");
            Assert.AreEqual<Guid>(kg.Guid, magneticParticleAnalysisConsumable.AmountUnitBase.Guid, "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of AmountUnitBase.");
            Assert.AreEqual<decimal>(2.0906m, magneticParticleAnalysisConsumable.Price.GetValueOrDefault(), "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of Consumable Price.");
            Assert.AreEqual<Guid>(kg.Guid, magneticParticleAnalysisConsumable.PriceUnitBase.Guid, "CentrifugalCasting -> MagneticParticleAnalysisStep -> Wrong value of PriceUnitBase.");
            Assert.IsNotNull(magneticParticleAnalysisConsumable.Manufacturer, "CentrifugalCasting -> MagneticParticleAnalysisStep -> Failed to generate a Manufacturer for the generated consumable.");
            Assert.AreEqual<string>("Interfflux", magneticParticleAnalysisConsumable.Manufacturer.Name, "CentrifugalCasting -> MagneticParticleAnalysisStep -> The Name of Consumable's Manufacturer is wrong.");

            // Validate the machines of the MagneticParticleAnalysisStep
            Machine magneticParticleAnalysisMachine = magneticParticleAnalysisStep.Machines.FirstOrDefault(m => m.Name == "Fluxing Equipment");
            Assert.IsNotNull(magneticParticleAnalysisMachine, "CentrifugalCasting -> MagneticParticleAnalysisStep -> Failed to generate the expected machine.");
        }

        /// <summary>
        /// A method that checks if the process steps generated with the LowPressureSandCast type are valid according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model of the LowPressureSandCastProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateLowPressureSandCastProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            // Validate the MoldManufacturingStep
            ProcessStep moldManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_MoldManufacturing);
            Assert.IsNotNull(moldManufacturingStep, "LowPressureSandCast -> Failed to generate the MoldManufacturingStep.");
            ValidateMoldManufacturingStep(viewModel, moldManufacturingStep, partChangeSet);
            ValidateMoldManufacturingMachines(viewModel, moldManufacturingStep);

            // Validate the CoreManufacturingStep and CoreAndMoldAssemblyStep
            if (viewModel.NumberOfCores > 0)
            {
                ProcessStep coreManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreManufacturing);
                Assert.IsNotNull(coreManufacturingStep, "LowPressureSandCast -> Failed to generate the CoreManufacturingStep.");
                ValidateCoreManufacturingStep(viewModel, coreManufacturingStep, partChangeSet);
                ValidateCoreManufacturingMachines(viewModel, coreManufacturingStep);

                ProcessStep coreAndMoldAssemblyStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly);
                Assert.IsNotNull(coreAndMoldAssemblyStep, "LowPressureSandCast -> Failed to generate the CoreAndMoldAssemblyStep.");
                ValidateCoreAndMoldAssemblyStepForSandCasting(viewModel, coreAndMoldAssemblyStep, partChangeSet);
                ValidateCoreAndMoldAssemblyMachines(viewModel, coreAndMoldAssemblyStep);
            }

            ProcessStep meltingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Melting);
            Assert.IsNotNull(meltingStep, "LowPressureSandCast -> Failed to generate the MeltingStep.");
            ValidateMeltingStep(viewModel, meltingStep, partChangeSet);
            ValidateMeltingMachines(viewModel, meltingStep);

            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Pouring);
            Assert.IsNotNull(pouringStep, "LowPressureSandCast -> Failed to generate the PouringStep.");
            ValidatePouringStep(viewModel, pouringStep, partChangeSet);
            ValidatePouringMachinesForLowPressureCast(viewModel, pouringStep);

            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "LowPressureSandCast -> Failed to generate the CoolingStep.");
            ValidateCoolingStep(viewModel, coolingStep, partChangeSet);
            ValidateCoolingMachines(viewModel, coolingStep);

            ProcessStep castingRemovalStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CastingRemoval);
            Assert.IsNotNull(castingRemovalStep, "LowPressureSandCast -> Failed to generate the CastingRemovalStep.");
            ValidateCastingRemovalStep(viewModel, castingRemovalStep, partChangeSet);
            ValidateCastingRemovalMachines(viewModel, castingRemovalStep);

            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "LowPressureSandCast -> Failed to generate the DeburringStep.");
            ValidateDeburringStep(viewModel, deburringStep, partChangeSet);
            ValidateDeburringMachines(viewModel, deburringStep);

            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "LowPressureSandCast -> Failed to generate the AbrasiveBlastingStep.");
            ValidateAbrasiveBlastingStep(viewModel, abrasiveBlastingStep, partChangeSet);
            ValidateAbrasiveBlastingMachines(viewModel, abrasiveBlastingStep);

            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "LowPressureSandCast -> Failed to generate the XRayStep.");
            ValidateXRayStep(viewModel, xRayStep, partChangeSet);
            ValidateXRayMachines(viewModel, xRayStep);

            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "LowPressureSandCast -> Failed to generate the CheckAndPackStep.");
            ValidateCheckAndPackStep(viewModel, checkAndPackStep, partChangeSet);
        }

        /// <summary>
        /// A method that checks if the process steps generated with the LowPressureCast type are valid according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model of the LowPressureCastProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidateLowPressureCastProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Pouring);
            Assert.IsNotNull(pouringStep, "LowPressureCast -> Failed to generate the PouringStep.");
            ValidatePouringStep(viewModel, pouringStep, partChangeSet);
            ValidatePouringMachinesForLowPressureCast(viewModel, pouringStep);

            // Validate the CoreManufacturingStep and CoreAndMoldAssemblyStep
            if (viewModel.NumberOfCores > 0)
            {
                ProcessStep coreManufacturingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreManufacturing);
                Assert.IsNotNull(coreManufacturingStep, "LowPressureCast -> Failed to generate the CoreManufacturingStep.");
                ValidateCoreManufacturingStep(viewModel, coreManufacturingStep, partChangeSet);
                ValidateCoreManufacturingMachines(viewModel, coreManufacturingStep);

                ProcessStep coreAndMoldAssemblyStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly);
                Assert.IsNotNull(coreAndMoldAssemblyStep, "LowPressureCast -> Failed to generate the CoreAndMoldAssemblyStep.");
                ValidateCoreAndMoldAssemblyMachines(viewModel, coreAndMoldAssemblyStep);
                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Assert.AreEqual<decimal>(pouringStep.CycleTime.GetValueOrDefault(), coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of CycleTime.");
                }
                else
                {
                    Assert.AreEqual<decimal>(300m, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of CycleTime.");
                }

                Assert.AreEqual<decimal>(coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), coreAndMoldAssemblyStep.ProcessTime.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of ProcessTime.");
                Assert.AreEqual<decimal>(1m, coreAndMoldAssemblyStep.SetupSkilledLabour.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of SetupSkilledLabour.");
                if (viewModel.Process == CastingProcessType.Manual)
                {
                    decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
                    if (partWeight <= 40m)
                    {
                        Assert.AreEqual<decimal>(1m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of ProductionSkilledLabour.");
                    }
                    else
                    {
                        Assert.AreEqual<decimal>(2m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of ProductionSkilledLabour.");
                    }
                }
                else
                {
                    Assert.AreEqual<decimal>(0.5m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "LowPressureCast -> CoreAndMoldAssemblyStep -> Wrong value of ProductionSkilledLabour.");
                }
            }

            ProcessStep meltingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Melting);
            Assert.IsNotNull(meltingStep, "LowPressureCast -> Failed to generate the MeltingStep.");
            ValidateMeltingStep(viewModel, meltingStep, partChangeSet);
            ValidateMeltingMachines(viewModel, meltingStep);

            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "LowPressureCast -> Failed to generate the CoolingStep.");
            ValidateCoolingStep(viewModel, coolingStep, partChangeSet);
            ValidateCoolingMachines(viewModel, coolingStep);

            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "LowPressureCast -> Failed to generate the DeburringStep.");
            ValidateDeburringStep(viewModel, deburringStep, partChangeSet);
            ValidateDeburringMachines(viewModel, deburringStep);

            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "LowPressureCast -> Failed to generate the AbrasiveBlastingStep.");
            ValidateAbrasiveBlastingStep(viewModel, abrasiveBlastingStep, partChangeSet);
            ValidateAbrasiveBlastingMachines(viewModel, abrasiveBlastingStep);

            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "LowPressureCast -> Failed to generate the XRayStep.");
            ValidateXRayStep(viewModel, xRayStep, partChangeSet);
            ValidateXRayMachines(viewModel, xRayStep);

            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "LowPressureCast -> Failed to generate the CheckAndPackStep.");
            ValidateCheckAndPackStep(viewModel, checkAndPackStep, partChangeSet);
        }

        /// <summary>
        /// A method that checks if the process steps generated with the PressureDieCast type are valid according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model of the PressureDieCastProcess.</param>
        /// <param name="part">The part on which the casting module is working.</param>
        /// <param name="partChangeSet">The change set associated with the part.</param>
        private void ValidatePressureDieCastProcessSteps(CastingModuleViewModel viewModel, Part part, IDataSourceManager partChangeSet)
        {
            decimal innerDiePressure =
               viewModel.InnerDiePressureInputted.HasValue ? viewModel.InnerDiePressureInputted.Value : viewModel.InnerDiePressureProposed.GetValueOrDefault();
            decimal clampingForce = viewModel.ProjectedMaxArea.GetValueOrDefault() * innerDiePressure / 100m;

            // Validate the MeltingStep
            ProcessStep meltingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Melting);
            Assert.IsNotNull(meltingStep, "PressureDieCast -> Failed to generate the PressureDieCast.");
            ValidateMeltingStepForPressureDieCast(viewModel, meltingStep, partChangeSet);
            ValidateMeltingMachineForPressureDieCasting(meltingStep, clampingForce);

            // Validate the PouringStep
            ProcessStep pouringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_PouringPressure);
            Assert.IsNotNull(pouringStep, "PressureDieCast -> Failed to generate the PouringStep.");
            ValidatePouringStepForPressureDieCasting(viewModel, pouringStep, partChangeSet);
            ValidatePouringMachinesForPressureDieCasting(pouringStep, clampingForce);

            // Validate the CoolingStep
            ProcessStep coolingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Cooling);
            Assert.IsNotNull(coolingStep, "PressureDieCast -> Failed to generate the CoolingStep.");
            ValidateCoolingStepForPressureDieCasting(viewModel, coolingStep, partChangeSet);

            // Validate the DeburringStep
            ProcessStep deburringStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_Deburring);
            Assert.IsNotNull(deburringStep, "PressureDieCast -> Failed to generate the DeburringStep.");
            ValidateDeburringStepForPressureDieCasting(viewModel, deburringStep, partChangeSet);
            ValidateDeburringMachinesForPressureDieCasting(deburringStep, clampingForce);

            // Validate the AbrasiveBlastingStep
            ProcessStep abrasiveBlastingStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting);
            Assert.IsNotNull(abrasiveBlastingStep, "PressureDieCast -> Failed to generate the AbrasiveBlastingStep.");
            ValidateAbrasiveBlastingStepForPressureDieCasting(viewModel, abrasiveBlastingStep, partChangeSet);
            ValidateAbrasiveBlastingMachinesForPressureDieCasting(abrasiveBlastingStep, clampingForce);

            // Validate the XRayStep
            ProcessStep xRayStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_XRay);
            Assert.IsNotNull(xRayStep, "PressureDieCast -> Failed to generate the XRayStep.");
            ValidateXRayStepForPressureDieCasting(viewModel, xRayStep, partChangeSet);
            Assert.IsNotNull(xRayStep.Machines.FirstOrDefault(m => m.Name == "Y.MU56 // Y.PXV 5000"), "PressureDieCast -> XRayStep ->  Failed to generate the expected machine.");

            // Validate the CheckAndPackStep
            ProcessStep checkAndPackStep = part.Process.Steps.FirstOrDefault(s => s.Name == LocalizedResources.CastingModule_ProcessStep_CheckAndPack);
            Assert.IsNotNull(checkAndPackStep, "PressureDieCast -> Failed to generate the CheckAndPackStep.");
            ValidateCheckAndPackStepForPressureDieCasting(viewModel, checkAndPackStep, partChangeSet);
        }

        /// <summary>
        /// The method validates a meltingStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the meltingStep was generated.</param>
        /// <param name="meltingStep">The meltingStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateMeltingStepForPressureDieCast(CastingModuleViewModel viewModel, ProcessStep meltingStep, IDataSourceManager stepChangeSet)
        {
            decimal cycleTime = viewModel.InputtedMeltingTime.HasValue ? viewModel.InputtedMeltingTime.Value : viewModel.ProposedMeltingTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<decimal>(cycleTime, meltingStep.CycleTime.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of CycleTime.");
            Assert.AreEqual<decimal>(meltingStep.CycleTime.GetValueOrDefault(), meltingStep.ProcessTime.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, meltingStep.CycleTimeUnit.Guid, "PressureDieCast -> MeltingStep -> Wrong value of CycleTimeUnit.");
            Assert.AreEqual<Guid>(meltingStep.CycleTimeUnit.Guid, meltingStep.ProcessTimeUnit.Guid, "PressureDieCast -> MeltingStep -> Wrong value of ProcessTimeUnit.");
            Assert.AreEqual<int>(1, meltingStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0m, meltingStep.SetupTime.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of SetupTime.");
            Assert.AreEqual<decimal>(0m, meltingStep.MaxDownTime.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of MaxDownTime.");
            Assert.AreEqual<decimal>(1m, meltingStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(1m, meltingStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCast -> MeltingStep -> Wrong value of SetupSkilledLabour.");
        }

        /// <summary>
        /// The method validates the machines of a MeltingStep generated with PressureDieCasting according to the specification.
        /// </summary>
        /// <param name="meltingStep">The parent step of the machine.</param>
        /// <param name="clampingForce">The clamping force.</param>
        private void ValidateMeltingMachineForPressureDieCasting(ProcessStep meltingStep, decimal clampingForce)
        {
            Machine meltingMachine = null;
            if (clampingForce <= 8000m)
            {
                meltingMachine = meltingStep.Machines.FirstOrDefault(m => m.Name == "Strikomelter MHS" && m.PowerConsumption == 454m);
            }
            else
            {
                meltingMachine = meltingStep.Machines.FirstOrDefault(m => m.Name == "Strikomelter MHS" && m.PowerConsumption == 1600m);
            }

            Assert.IsNotNull(meltingMachine, "PressureDieCasting -> MeltingStep -> Failed to generate the expected machine.");
        }

        /// <summary>
        /// The method validates a pouringStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the pouringStep was generated.</param>
        /// <param name="pouringStep">The pouringStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidatePouringStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep pouringStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime =
               viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            decimal scrapAmount = viewModel.InputtedScrap.HasValue ? viewModel.InputtedScrap.Value : viewModel.ProposedScrap.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<decimal>(pouringTime + solidificationTime, pouringStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong CycleTime value.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, pouringStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(scrapAmount, pouringStep.ScrapAmount.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong ScrapAmount value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, pouringStep.CycleTimeUnit.Guid, "PressureDieCasting -> PouringStep -> Wrong CycleTimeUnit value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, pouringStep.ProcessTimeUnit.Guid, "PressureDieCasting -> PouringStep -> Wrong ProcessTimeUnit value.");
            Assert.AreEqual<decimal>(0m, pouringStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(0m, pouringStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(0.5m, pouringStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, pouringStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong SetupSkilledLabour value.");
            Assert.AreEqual<int>(1, pouringStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(0m, pouringStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> PouringStep -> Wrong value of SetupTime.");
        }

        /// <summary>
        /// The method validates the machines of a PouringStep generated with PressureDieCasting according to the specification.
        /// </summary>
        /// <param name="pouringStep">The parent step of the machines.</param>
        /// <param name="clampingForce">The clamping force.</param>
        private void ValidatePouringMachinesForPressureDieCasting(ProcessStep pouringStep, decimal clampingForce)
        {
            // Dosing System
            Machine dosingSystemMachine = null;
            if (clampingForce <= 3000m)
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 450S");
            }
            else if (clampingForce <= 5000m)
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 650S");
            }
            else if (clampingForce <= 9000m)
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 900S");
            }
            else if (clampingForce <= 14000m)
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 1700S");
            }
            else if (clampingForce <= 28000m)
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 2300S");
            }
            else
            {
                dosingSystemMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Westomat 3100S");
            }

            Assert.IsNotNull(dosingSystemMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");

            // Die Casting Machine
            Machine dieCastingMachine = null;
            if (clampingForce <= 2000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 26 Compact");
            }
            else if (clampingForce <= 3000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 34 Compact");
            }
            else if (clampingForce <= 4000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 42 Compact");
            }
            else if (clampingForce <= 5000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 53 Compact");
            }
            else if (clampingForce <= 6000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 66 Compact");
            }
            else if (clampingForce <= 8000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 84 Compact");
            }
            else if (clampingForce <= 9000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Evolution 96 Compact");
            }
            else if (clampingForce <= 10000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 105");
            }
            else if (clampingForce <= 14000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 140");
            }
            else if (clampingForce <= 18000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 180");
            }
            else if (clampingForce <= 22000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 220");
            }
            else if (clampingForce <= 28000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 280");
            }
            else if (clampingForce <= 32000m)
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 320");
            }
            else
            {
                dieCastingMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Carat 440");
            }

            Assert.IsNotNull(dieCastingMachine, "PressureDieCasting ->PouringStep -> Failed to generate the expected machine.");

            // Temperature Controller
            Machine temperatureControllerMachine = null;
            if (clampingForce <= 5000m)
            {
                temperatureControllerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "RT50");
            }
            else if (clampingForce <= 28000m)
            {
                temperatureControllerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "RT61");
            }
            else
            {
                temperatureControllerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "RT100");
            }

            Assert.IsNotNull(temperatureControllerMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");

            // Sprayer
            Machine sprayerMachine = null;
            if (clampingForce <= 3000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V1000H1000");
            }
            else if (clampingForce <= 5000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V1200H1000");
            }
            else if (clampingForce <= 6000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V1400H1200");
            }
            else if (clampingForce <= 8000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V1600H1200");
            }
            else if (clampingForce <= 10000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V2000H1600");
            }
            else if (clampingForce <= 18000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V1600H1600");
            }
            else if (clampingForce <= 28000m)
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V2000H2000");
            }
            else
            {
                sprayerMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "V2500H2000");
            }

            Assert.IsNotNull(sprayerMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");

            // Crop
            Machine cropMachine = null;
            if (clampingForce <= 5000m)
            {
                cropMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Buhlladle 1/12");
                Assert.IsNotNull(cropMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");
            }
            else if (clampingForce <= 28000m)
            {
                cropMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "Buhlladle 2/23");
                Assert.IsNotNull(cropMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");
            }
            else if (clampingForce <= 5000m)
            {
                cropMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "imo-trust imo-6");
                Assert.IsNotNull(cropMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");
            }

            // Robot
            Machine robotMachine = null;
            if (clampingForce <= 5000m)
            {
                robotMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "2400 16-1.5");
            }
            else if (clampingForce <= 9000m)
            {
                robotMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "4400F 45-1.95");
            }
            else if (clampingForce <= 18000m)
            {
                robotMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "6640F 185-2.8");
            }
            else if (clampingForce <= 28000m)
            {
                robotMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "6600F 130-3.2");
            }
            else
            {
                robotMachine = pouringStep.Machines.FirstOrDefault(m => m.Name == "7600F 150-3.5");
            }

            Assert.IsNotNull(robotMachine, "PressureDieCasting -> PouringStep -> Failed to generate the expected machine.");
        }

        /// <summary>
        /// The method validates a coolingStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the coolingStep was generated.</param>
        /// <param name="coolingStep">The coolingStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCoolingStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep coolingStep, IDataSourceManager stepChangeSet)
        {
            decimal coolingTime = viewModel.InputtedCoolingTime.HasValue ? viewModel.InputtedCoolingTime.Value : viewModel.ProposedCoolingTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<decimal>(coolingTime, coolingStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong CycleTime value.");
            Assert.AreEqual<decimal>(coolingTime, coolingStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong ProcessTime value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, coolingStep.CycleTimeUnit.Guid, "PressureDieCasting -> CoolingStep -> Wrong CycleTimeUnit value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, coolingStep.ProcessTimeUnit.Guid, "PressureDieCasting -> CoolingStep -> Wrong ProcessTimeUnit value.");
            Assert.AreEqual<decimal>(0m, coolingStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(0m, coolingStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(1m, coolingStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, coolingStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong SetupSkilledLabour value.");
            Assert.AreEqual<int>(1, coolingStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> CoolingStep -> Wrong PartsPerCycle value.");
        }

        /// <summary>
        /// The method validates a deburringStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the deburringStep was generated.</param>
        /// <param name="deburringStep">The deburringStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateDeburringStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep deburringStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime =
            viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<int>(1, deburringStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, deburringStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of CycleTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, deburringStep.CycleTimeUnit.Guid, "PressureDieCasting -> DeburringStep -> Wrong value of CycleTimeUnit.");
            Assert.AreEqual<decimal>(deburringStep.CycleTime.GetValueOrDefault(), deburringStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, deburringStep.ProcessTimeUnit.Guid, "PressureDieCasting -> DeburringStep -> Wrong value of ProcessTimeUnit.");
            Assert.AreEqual<decimal>(0m, deburringStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of SetupTime.");
            Assert.AreEqual<decimal>(0m, deburringStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of MaxDownTime.");
            Assert.AreEqual<decimal>(0.5m, deburringStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(1m, deburringStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> DeburringStep -> Wrong value of SetupSkilledLabour.");
        }

        /// <summary>
        /// The method validates the machines of a DeburringStep generated with PressureDieCasting according to the specification.
        /// </summary>
        /// <param name="deburringStep">The parent step of the machines.</param>
        /// <param name="clampingForce">The clamping force.</param>
        private void ValidateDeburringMachinesForPressureDieCasting(ProcessStep deburringStep, decimal clampingForce)
        {
            Machine deburringMachine = null;
            if (clampingForce <= 3000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP8/200 kN");
            }
            else if (clampingForce <= 5000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP8/300 kN");
            }
            else if (clampingForce <= 8000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP10/300 kN");
            }
            else if (clampingForce <= 10000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP13/650 kN");
            }
            else if (clampingForce <= 14000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP13/1000 kN");
            }
            else if (clampingForce <= 18000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP16/1000 kN");
            }
            else if (clampingForce <= 22000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP16/2000 kN");
            }
            else if (clampingForce <= 28000m)
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP22/1000 kN");
            }
            else
            {
                deburringMachine = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP22/2000 kN");
            }

            Assert.IsNotNull(deburringMachine, "PressureDieCasting -> DeburringStep -> Failed to generate the expected machine.");
        }

        /// <summary>
        /// The method validates an xRayStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the xRayStep was generated.</param>
        /// <param name="xRayStep">The xRayStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateXRayStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep xRayStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime =
              viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<int>(100, xRayStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, xRayStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of CycleTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, xRayStep.CycleTimeUnit.Guid, "PressureDieCasting -> XRayStep -> Wrong value of CycleTimeUnit.");
            Assert.AreEqual<decimal>(xRayStep.CycleTime.GetValueOrDefault(), xRayStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, xRayStep.ProcessTimeUnit.Guid, "PressureDieCasting -> XRayStep -> Wrong value of ProcessTimeUnit.");
            Assert.AreEqual<decimal>(600m, xRayStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of SetupTime.");
            Assert.AreEqual<decimal>(600m, xRayStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of MaxDownTime.");
            Assert.AreEqual<decimal>(1m, xRayStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(1m, xRayStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> XRayStep -> Wrong value of SetupSkilledLabour.");
        }

        /// <summary>
        /// The method validates a checkAndPackStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the checkAndPackStep was generated.</param>
        /// <param name="checkAndPackStep">The checkAndPackStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCheckAndPackStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep checkAndPackStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime =
           viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<int>(1, checkAndPackStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, checkAndPackStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of CycleTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, checkAndPackStep.CycleTimeUnit.Guid, "PressureDieCasting -> CheckAndPackStep -> Wrong value of CycleTimeUnit.");
            Assert.AreEqual<decimal>(checkAndPackStep.CycleTime.GetValueOrDefault(), checkAndPackStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, checkAndPackStep.ProcessTimeUnit.Guid, "PressureDieCasting -> CheckAndPackStep -> Wrong value of ProcessTimeUnit.");
            Assert.AreEqual<decimal>(0m, checkAndPackStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of SetupTime.");
            Assert.AreEqual<decimal>(0m, checkAndPackStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of MaxDownTime.");
            Assert.AreEqual<decimal>(1m, checkAndPackStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(1m, checkAndPackStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> CheckAndPackStep -> Wrong value of SetupSkilledLabour.");
        }

        /// <summary>
        /// The method validates an abrasiveBlastingStep of PressureDieCast according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the abrasiveBlastingStep was generated.</param>
        /// <param name="abrasiveBlastingStep">The abrasiveBlastingStep to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateAbrasiveBlastingStepForPressureDieCasting(CastingModuleViewModel viewModel, ProcessStep abrasiveBlastingStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime =
                viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<int>(1, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of PartsPerCycle.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, abrasiveBlastingStep.CycleTime.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of CycleTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, abrasiveBlastingStep.CycleTimeUnit.Guid, "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of CycleTimeUnit.");
            Assert.AreEqual<decimal>(abrasiveBlastingStep.CycleTime.GetValueOrDefault(), abrasiveBlastingStep.ProcessTime.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of ProcessTime.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, abrasiveBlastingStep.ProcessTimeUnit.Guid, "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of ProcessTimeUnit.");
            Assert.AreEqual<decimal>(600m, abrasiveBlastingStep.SetupTime.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of SetupTime.");
            Assert.AreEqual<decimal>(600m, abrasiveBlastingStep.MaxDownTime.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of MaxDownTime.");
            Assert.AreEqual<decimal>(0.5m, abrasiveBlastingStep.ProductionSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of ProductionSkilledLabour.");
            Assert.AreEqual<decimal>(1m, abrasiveBlastingStep.SetupSkilledLabour.GetValueOrDefault(), "PressureDieCasting -> AbrasiveBlastingStep -> Wrong value of SetupSkilledLabour.");
        }

        /// <summary>
        /// The method validates the machines of a AbrasiveBlastingStep generated with PressureDieCasting according to the specification.
        /// </summary>
        /// <param name="abrasiveBlastingStep">The parent step of the machines.</param>
        /// <param name="clampingForce">The clamping force.</param>
        private void ValidateAbrasiveBlastingMachinesForPressureDieCasting(ProcessStep abrasiveBlastingStep, decimal clampingForce)
        {
            Machine abrasiveBlastingMachine = null;
            if (clampingForce <= 5000m)
            {
                abrasiveBlastingMachine = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM300/2");
            }
            else if (clampingForce <= 14000m)
            {
                abrasiveBlastingMachine = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2");
            }
            else if (clampingForce <= 28000m)
            {
                abrasiveBlastingMachine = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2/E");
            }
            else
            {
                abrasiveBlastingMachine = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM500/2/E");
            }

            Assert.IsNotNull(abrasiveBlastingMachine, "PressureDieCasting -> AbrasiveBlastingStep -> Failed to generate the expected machine.");
        }

        /// <summary>
        /// The method validates a mold manufacturing step.
        /// </summary>
        /// <param name="viewModel">The view model from which the mold manufacturing step was generated.</param>
        /// <param name="moldManufacturingStep">The mold manufacturing step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateMoldManufacturingStep(CastingModuleViewModel viewModel, ProcessStep moldManufacturingStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the cycle time value was correctly set
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 1)
            {
                Assert.AreEqual<decimal>(15, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 2)
            {
                Assert.AreEqual<decimal>(17, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 4)
            {
                Assert.AreEqual<decimal>(19, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 8)
            {
                Assert.AreEqual<decimal>(21, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 15)
            {
                Assert.AreEqual<decimal>(23, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 25)
            {
                Assert.AreEqual<decimal>(25, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<decimal>(26, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 60)
            {
                Assert.AreEqual<decimal>(28, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(30, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTimeValue.");
            }
            else if (partWeight <= 130)
            {
                Assert.AreEqual<decimal>(32, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 190)
            {
                Assert.AreEqual<decimal>(35, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 240)
            {
                Assert.AreEqual<decimal>(40, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 300)
            {
                Assert.AreEqual<decimal>(45, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(50, moldManufacturingStep.CycleTime.GetValueOrDefault(), "MoldManufacturing -> Wrong CycleTime value.");
            }

            // Verify that the ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour were correctly set
            Assert.AreEqual<decimal>(moldManufacturingStep.CycleTime.GetValueOrDefault(), moldManufacturingStep.ProcessTime.GetValueOrDefault(), "SandCasting -> MoldManufacturing ->Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(1800m, moldManufacturingStep.SetupTime.GetValueOrDefault(), "MoldManufacturing -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(1800m, moldManufacturingStep.MaxDownTime.GetValueOrDefault(), "MoldManufacturing -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(2m, moldManufacturingStep.ProductionSkilledLabour.GetValueOrDefault(), "MoldManufacturing -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(2m, moldManufacturingStep.SetupSkilledLabour.GetValueOrDefault(), "MoldManufacturing -> Wrong SetupTime value.");

            // Verify that the Sand value was correctly calculated
            decimal sandWeight = viewModel.InputtedSandWeight.HasValue ? viewModel.InputtedSandWeight.Value : viewModel.ProposedSandWeight.GetValueOrDefault();
            if (sandWeight > 0)
            {
                Consumable sand = moldManufacturingStep.Consumables.FirstOrDefault(c => c.Name == LocalizedResources.CastingModule_SandAndBinder);
                MeasurementUnit weightUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "kg" && !u.IsReleased);

                Assert.IsNotNull(sand, "Failed to add the sand to the MoldManufacturing.");
                Assert.AreEqual<decimal>(0.025m, sand.Price.GetValueOrDefault(), "MoldManufacturing -> Wrong Sand Price.");
                Assert.AreEqual<decimal>(sandWeight, sand.Amount.GetValueOrDefault(), "MoldManufacturing -> Wrong Sand Amount.");
                Assert.AreEqual<Guid>(sand.PriceUnitBase.Guid, weightUnitBase.Guid, "MoldManufacturing -> Wrong Sand PriceUnitBase.");
                Assert.AreEqual<Guid>(sand.AmountUnitBase.Guid, weightUnitBase.Guid, "MoldManufacturing -> Wrong Sand AmountUnitBase.");
            }
        }

        /// <summary>
        /// The method validates the machines of a moldManufacturingStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="moldManufacturingStep">The parent process step of machines.</param>
        private void ValidateMoldManufacturingMachines(CastingModuleViewModel viewModel, ProcessStep moldManufacturingStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Machine moldManufacturingMachine1 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISAMATIC Vertical Moulding 231/A");
                Assert.IsNotNull(moldManufacturingMachine1, "MoldManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine moldManufacturingMachine2 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(moldManufacturingMachine2, "MoldManufacturing -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 40)
            {
                Machine moldManufacturingMachine1 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISAMATIC Vertical Moulding 231/A");
                Assert.IsNotNull(moldManufacturingMachine1, "MoldManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine moldManufacturingMachine2 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(moldManufacturingMachine2, "MoldManufacturing -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 190)
            {
                Machine moldManufacturingMachine1 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISAMATIC Vertical Moulding 231/C");
                Assert.IsNotNull(moldManufacturingMachine1, "MoldManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine moldManufacturingMachine2 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(moldManufacturingMachine2, "MoldManufacturing -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine moldManufacturingMachine1 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISAMATIC Vertical Moulding 231/C");
                Assert.IsNotNull(moldManufacturingMachine1, "MoldManufacturing -> Failed to generate the expected machine.");

                Machine moldManufacturingMachine2 = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(moldManufacturingMachine2, "MoldManufacturing -> Failed to generate the expected machine.");
            }

            // Sand mixer
            if (partWeight <= 8)
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "SAM-3 / 20");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }
            else if (partWeight <= 130)
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "SAM-6 / 50");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }
            else
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISAMATIC Vertical Moulding 231/C");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }

            // Sand cooler and recycler
            if (partWeight <= 15)
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISACOOL 2400");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }
            else if (partWeight <= 130)
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISACOOL 3200");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }
            else
            {
                Machine moldManufacturingMachine = moldManufacturingStep.Machines.FirstOrDefault(m => m.Name == "DISACOOL 4300");
                Assert.IsNotNull(moldManufacturingMachine, "MoldManufacturing -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates a core manufacturing step.
        /// </summary>
        /// <param name="viewModel">The view model from which the core manufacturing step was generated.</param>
        /// <param name="coreManufacturingStep">The core manufacturing step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCoreManufacturingStep(CastingModuleViewModel viewModel, ProcessStep coreManufacturingStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the cycle time value is correct
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 1)
            {
                Assert.AreEqual<decimal>(15, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 2)
            {
                Assert.AreEqual<decimal>(17, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 4)
            {
                Assert.AreEqual<decimal>(19, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 8)
            {
                Assert.AreEqual<decimal>(21, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 15)
            {
                Assert.AreEqual<decimal>(23, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 25)
            {
                Assert.AreEqual<decimal>(25, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<decimal>(26, coreManufacturingStep.CycleTime.GetValueOrDefault(), "Wrong CycleTime value.");
            }
            else if (partWeight <= 60)
            {
                Assert.AreEqual<decimal>(28, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(30, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 130)
            {
                Assert.AreEqual<decimal>(32, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 190)
            {
                Assert.AreEqual<decimal>(35, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 240)
            {
                Assert.AreEqual<decimal>(40, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else if (partWeight <= 300)
            {
                Assert.AreEqual<decimal>(45, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(50, coreManufacturingStep.CycleTime.GetValueOrDefault(), "CoreManufacturing -> Wrong CycleTime value.");
            }

            // Verify that the ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour are correctly set
            Assert.AreEqual<decimal>(coreManufacturingStep.CycleTime.GetValueOrDefault(), coreManufacturingStep.ProcessTime.GetValueOrDefault(), "SandCasting -> CoreManufacturing -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(1800m, coreManufacturingStep.SetupTime.GetValueOrDefault(), "CoreManufacturing -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(1800m, coreManufacturingStep.MaxDownTime.GetValueOrDefault(), "CoreManufacturing -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(0.5m, coreManufacturingStep.ProductionSkilledLabour.GetValueOrDefault(), "CoreManufacturing -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, coreManufacturingStep.SetupSkilledLabour.GetValueOrDefault(), "CoreManufacturing -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a coreManufacturingStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="coreManufacturingStep">The parent process step of machines.</param>
        private void ValidateCoreManufacturingMachines(CastingModuleViewModel viewModel, ProcessStep coreManufacturingStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were generated
            if (partWeight <= 15)
            {
                Machine coreManufacturingMachine1 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "Core Shooter MP 20");
                Assert.IsNotNull(coreManufacturingMachine1, "CoreManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coreManufacturingMachine2 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(coreManufacturingMachine2, "CoreManufacturing -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 40)
            {
                Machine coreManufacturingMachine1 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "Core Shooter MP 20");
                Assert.IsNotNull(coreManufacturingMachine1, "CoreManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coreManufacturingMachine2 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(coreManufacturingMachine2, "CoreManufacturing -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 190)
            {
                Machine coreManufacturingMachine1 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "Core Shooter MP 80");
                Assert.IsNotNull(coreManufacturingMachine1, "CoreManufacturing -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coreManufacturingMachine2 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(coreManufacturingMachine2, "CoreManufacturing -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine coreManufacturingMachine1 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "Core Shooter MP 80");
                Assert.IsNotNull(coreManufacturingMachine1, "CoreManufacturing -> Failed to generate the expected machine.");

                Machine coreManufacturingMachine2 = coreManufacturingStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(coreManufacturingMachine2, "CoreManufacturing -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates a CoreAndMoldAssembly step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="coreAndMoldAssemblyStep">The CoreAndMoldAssembly step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCoreAndMoldAssemblyStepForSandCasting(CastingModuleViewModel viewModel, ProcessStep coreAndMoldAssemblyStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the CycleTime value was correctly set
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (viewModel.Process == CastingProcessType.Automatic)
            {
                if (partWeight <= 1)
                {
                    Assert.AreEqual<decimal>(15, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 2)
                {
                    Assert.AreEqual<decimal>(17, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 4)
                {
                    Assert.AreEqual<decimal>(19, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 8)
                {
                    Assert.AreEqual<decimal>(21, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 15)
                {
                    Assert.AreEqual<decimal>(23, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 25)
                {
                    Assert.AreEqual<decimal>(25, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 40)
                {
                    Assert.AreEqual<decimal>(26, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 60)
                {
                    Assert.AreEqual<decimal>(28, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 90)
                {
                    Assert.AreEqual<decimal>(30, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTimeValue.");
                }
                else if (partWeight <= 130)
                {
                    Assert.AreEqual<decimal>(32, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 190)
                {
                    Assert.AreEqual<decimal>(35, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 240)
                {
                    Assert.AreEqual<decimal>(40, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 300)
                {
                    Assert.AreEqual<decimal>(45, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else
                {
                    Assert.AreEqual<decimal>(50, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
            }
            else
            {
                if (partWeight <= 8)
                {
                    Assert.AreEqual<decimal>(300, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 90)
                {
                    Assert.AreEqual<decimal>(600, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else if (partWeight <= 240)
                {
                    Assert.AreEqual<decimal>(900, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
                else
                {
                    Assert.AreEqual<decimal>(1200, coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong CycleTime value.");
                }
            }

            // Verify that the ProcessTime, SetupSkilledLabour and ProductionSkilledLabour values were correctly set
            Assert.AreEqual<decimal>(coreAndMoldAssemblyStep.CycleTime.GetValueOrDefault(), coreAndMoldAssemblyStep.ProcessTime.GetValueOrDefault(), "SandCasting -> CoreAndMoldAssembly -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(1m, coreAndMoldAssemblyStep.SetupSkilledLabour.GetValueOrDefault(), "CoreAndMoldAssembly -> Wrong SetupSkilledLabour value.");
            if (viewModel.Process == CastingProcessType.Manual)
            {
                if (partWeight <= 40)
                {
                    Assert.AreEqual<decimal>(1m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "SandCasting -> CoreAndMoldAssembly -> Wrong ProductionSkilledLabour value.");
                }
                else
                {
                    Assert.AreEqual<decimal>(2m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "SandCasting -> CoreAndMoldAssembly -> Wrong ProductionSkilledLabour value.");
                }
            }
            else
            {
                Assert.AreEqual<decimal>(0.5m, coreAndMoldAssemblyStep.ProductionSkilledLabour.GetValueOrDefault(), "SandCasting -> CoreAndMoldAssembly -> Wrong ProductionSkilledLabour value.");
            }
        }

        /// <summary>
        /// The method validates the machines of a coreAndMoldAssemblyStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="coreAndMoldAssemblyStep">The parent process step of machines.</param>
        private void ValidateCoreAndMoldAssemblyMachines(CastingModuleViewModel viewModel, ProcessStep coreAndMoldAssemblyStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were generated
            if (viewModel.Process == CastingProcessType.Automatic)
            {
                if (partWeight <= 15)
                {
                    Machine coreAndMoldAssemblyMachine = coreAndMoldAssemblyStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation I");
                    Assert.IsNotNull(coreAndMoldAssemblyMachine, "CoreAndMoldAssembly -> Failed to generate the expected machine.");
                }
                else if (partWeight <= 130)
                {
                    Machine coreAndMoldAssemblyMachine = coreAndMoldAssemblyStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation II");
                    Assert.IsNotNull(coreAndMoldAssemblyMachine, "CoreAndMoldAssembly -> Failed to generate the expected machine.");
                }
                else
                {
                    Machine coreAndMoldAssemblyMachine = coreAndMoldAssemblyStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation III");
                    Assert.IsNotNull(coreAndMoldAssemblyMachine, "CoreAndMoldAssembly -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine coreAndMoldAssemblyMachine = coreAndMoldAssemblyStep.Machines.FirstOrDefault(m => m.Name == "Casting Manual Mold Prep Place");
                Assert.IsNotNull(coreAndMoldAssemblyMachine, "CoreAndMoldAssembly -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates a Melting step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="meltingStep">The Melting step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateMeltingStep(CastingModuleViewModel viewModel, ProcessStep meltingStep, IDataSourceManager stepChangeSet)
        {
            decimal meltingTime = viewModel.InputtedMeltingTime.HasValue ? viewModel.InputtedMeltingTime.Value : viewModel.ProposedMeltingTime.GetValueOrDefault();
            MeasurementUnit timetUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            // Verify that the values of CycleTime, ProcessTime, CycleTimeUnit and ProcessTimeUnit were correctly calculated
            Assert.AreEqual<decimal>(meltingTime, meltingStep.CycleTime.GetValueOrDefault(), "MeltingStep -> Wrong CycleTime value.");
            Assert.AreEqual<decimal>(meltingTime, meltingStep.ProcessTime.GetValueOrDefault(), "MeltingStep -> Wrong ProcessTime value.");
            Assert.AreEqual<Guid>(timetUnitBase.Guid, meltingStep.CycleTimeUnit.Guid, "SMeltingStep -> Wrong CycleTimeUnit value.");
            Assert.AreEqual<Guid>(timetUnitBase.Guid, meltingStep.ProcessTimeUnit.Guid, "SMeltingStep -> Wrong ProcessTimeUnit value.");
            Assert.AreEqual<decimal>(2m, meltingStep.ProductionSkilledLabour.GetValueOrDefault(), "MeltingStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(2m, meltingStep.SetupSkilledLabour.GetValueOrDefault(), "MeltingStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a meltingStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="meltingStep">The parent process step of machines.</param>
        private void ValidateMeltingMachines(CastingModuleViewModel viewModel, ProcessStep meltingStep)
        {
            // Verify that the corresponding machines were generated
            if (viewModel.CastingMethod == CastingMethod.CentrifugalCastingFerrous ||
                 viewModel.CastingMethod == CastingMethod.GravityDieCastingFerrous ||
                 viewModel.CastingMethod == CastingMethod.InvestmentCastingFerrous ||
                 viewModel.CastingMethod == CastingMethod.SandCastingFerrous)
            {
                Machine meltingMachine = meltingStep.Machines.FirstOrDefault(m => m.Name == "TWIN Power IFM 6");
                Assert.IsNotNull(meltingMachine, "MeltingStep -> Failed to generate the expected machine.");
            }
            else
            {
                decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
                if (partWeight <= 25m)
                {
                    Machine meltingMachine = meltingStep.Machines.FirstOrDefault(m => m.Name == "Strikomelter MHS" && m.PowerConsumption == 454);
                    Assert.IsNotNull(meltingMachine, "MeltingStep -> Failed to generate the expected machine.");
                }
                else
                {
                    Machine meltingMachine = meltingStep.Machines.FirstOrDefault(m => m.Name == "Strikomelter MHS" && m.PowerConsumption == 1600);
                    Assert.IsNotNull(meltingMachine, "MeltingStep -> Failed to generate the expected machine.");
                }
            }
        }

        /// <summary>
        /// The method validates a Pouring step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="pouringStep">The Pouring step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidatePouringStep(CastingModuleViewModel viewModel, ProcessStep pouringStep, IDataSourceManager stepChangeSet)
        {
            decimal pouringTime = viewModel.InputtedPouringTime.HasValue ? viewModel.InputtedPouringTime.Value : viewModel.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime = viewModel.InputtedSolidificationTime.HasValue ? viewModel.InputtedSolidificationTime.Value : viewModel.ProposedSolidificationTime.GetValueOrDefault();
            decimal scrapAmount = viewModel.InputtedScrap.HasValue ? viewModel.InputtedScrap.Value : viewModel.ProposedScrap.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<decimal>(pouringTime + solidificationTime, pouringStep.CycleTime.GetValueOrDefault(), "PouringStep -> Wrong CycleTime value.");
            Assert.AreEqual<decimal>(pouringTime + solidificationTime, pouringStep.ProcessTime.GetValueOrDefault(), "PouringStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(scrapAmount, pouringStep.ScrapAmount.GetValueOrDefault(), "PouringStep -> Wrong ScrapAmount value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, pouringStep.CycleTimeUnit.Guid, "PouringStep -> Wrong CycleTimeUnit value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, pouringStep.ProcessTimeUnit.Guid, "PouringStep -> Wrong ProcessTimeUnit value.");
            Assert.AreEqual<decimal>(600m, pouringStep.SetupTime.GetValueOrDefault(), "PouringStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(600m, pouringStep.MaxDownTime.GetValueOrDefault(), "PouringStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(1m, pouringStep.ProductionSkilledLabour.GetValueOrDefault(), "PouringStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, pouringStep.SetupSkilledLabour.GetValueOrDefault(), "PouringStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The methods validate the machines of the pouringStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="pouringStep">The parent process step of machines.</param>
        private void ValidatePouringMachines(CastingModuleViewModel viewModel, ProcessStep pouringStep)
        {
            // Verify that the expected machines were generated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "CR 140");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 140");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 50");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The methods validate the machines of the pouringStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="pouringStep">The parent process step of machines.</param>
        private void ValidatePouringMachinesForGravityDieCast(CastingModuleViewModel viewModel, ProcessStep pouringStep)
        {
            // Verify that the expected machines were generated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "CR 320");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 320");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 200");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The methods validate the machines of the pouringStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="pouringStep">The parent process step of machines.</param>
        private void ValidatePouringMachinesForLowPressureCast(CastingModuleViewModel viewModel, ProcessStep pouringStep)
        {
            // Verify that the expected machines were generated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 300");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 125");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine pouringMachine1 = pouringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 160");
                Assert.IsNotNull(pouringMachine1, "PouringStep -> Failed to generate the expected machine.");

                Machine pouringMachine2 = pouringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(pouringMachine2, "PouringStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates a Cooling step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="coolingStep">The Cooling step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCoolingStep(CastingModuleViewModel viewModel, ProcessStep coolingStep, IDataSourceManager stepChangeSet)
        {
            decimal coolingTime = viewModel.InputtedCoolingTime.HasValue ? viewModel.InputtedCoolingTime.Value : viewModel.ProposedCoolingTime.GetValueOrDefault();
            MeasurementUnit timeUnitBase = stepChangeSet.MeasurementUnitRepository.GetAll().FirstOrDefault(u => u.Symbol.ToLower() == "s" && !u.IsReleased);

            Assert.AreEqual<decimal>(coolingTime, coolingStep.CycleTime.GetValueOrDefault(), "CoolingStep -> Wrong CycleTime value.");
            Assert.AreEqual<decimal>(coolingTime, coolingStep.ProcessTime.GetValueOrDefault(), "CoolingStep -> Wrong ProcessTime value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, coolingStep.CycleTimeUnit.Guid, "CoolingStep -> Wrong CycleTimeUnit value.");
            Assert.AreEqual<Guid>(timeUnitBase.Guid, coolingStep.ProcessTimeUnit.Guid, "CoolingStep -> Wrong ProcessTimeUnit value.");
            Assert.AreEqual<decimal>(0m, coolingStep.SetupTime.GetValueOrDefault(), "CoolingStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(0m, coolingStep.MaxDownTime.GetValueOrDefault(), "CoolingStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(0m, coolingStep.ProductionSkilledLabour.GetValueOrDefault(), "CoolingStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(0m, coolingStep.SetupSkilledLabour.GetValueOrDefault(), "CoolingStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a coolingStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="coolingStep">The parent process step of machines.</param>
        private void ValidateCoolingMachines(CastingModuleViewModel viewModel, ProcessStep coolingStep)
        {
            // Verify that the expected machines were generated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Machine coolingMachine1 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Cooling Line I");
                Assert.IsNotNull(coolingMachine1, "CoolingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coolingMachine2 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation I");
                    Assert.IsNotNull(coolingMachine2, "CoolingStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine coolingMachine1 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Cooling Line II");
                Assert.IsNotNull(coolingMachine1, "CoolingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coolingMachine2 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation II");
                    Assert.IsNotNull(coolingMachine2, "CoolingStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine coolingMachine1 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Cooling Line III");
                Assert.IsNotNull(coolingMachine1, "CoolingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine coolingMachine2 = coolingStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation III");
                    Assert.IsNotNull(coolingMachine2, "CoolingStep -> Failed to generate the expected machine.");
                }
            }
        }

        /// <summary>
        /// The method validates a CastingRemoval step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="castingRemovalStep">The CastingRemoval step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCastingRemovalStep(CastingModuleViewModel viewModel, ProcessStep castingRemovalStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the CycleTime was correctly set
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (viewModel.Process == CastingProcessType.Automatic)
            {
                if (partWeight <= 1)
                {
                    Assert.AreEqual<decimal>(15, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 2)
                {
                    Assert.AreEqual<decimal>(17, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 4)
                {
                    Assert.AreEqual<decimal>(19, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 8)
                {
                    Assert.AreEqual<decimal>(21, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 15)
                {
                    Assert.AreEqual<decimal>(23, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 25)
                {
                    Assert.AreEqual<decimal>(25, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 40)
                {
                    Assert.AreEqual<decimal>(26, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 60)
                {
                    Assert.AreEqual<decimal>(28, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 90)
                {
                    Assert.AreEqual<decimal>(30, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 130)
                {
                    Assert.AreEqual<decimal>(32, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 190)
                {
                    Assert.AreEqual<decimal>(35, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 240)
                {
                    Assert.AreEqual<decimal>(40, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 300)
                {
                    Assert.AreEqual<decimal>(45, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else
                {
                    Assert.AreEqual<decimal>(50, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
            }
            else
            {
                if (partWeight <= 8)
                {
                    Assert.AreEqual<decimal>(300m, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 90)
                {
                    Assert.AreEqual<decimal>(600m, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else if (partWeight <= 240)
                {
                    Assert.AreEqual<decimal>(900m, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
                else
                {
                    Assert.AreEqual<decimal>(1200m, castingRemovalStep.CycleTime.GetValueOrDefault(), "CastingRemovalStep -> Wrong CycleTime value.");
                }
            }

            // Verify that the ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour were correctly set
            Assert.AreEqual<decimal>(castingRemovalStep.CycleTime.GetValueOrDefault(), castingRemovalStep.ProcessTime.GetValueOrDefault(), "SandCasting -> CastingRemovalStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(1800m, castingRemovalStep.SetupTime.GetValueOrDefault(), "SandCasting -> CastingRemovalStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(1800m, castingRemovalStep.MaxDownTime.GetValueOrDefault(), "SandCasting -> CastingRemovalStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(1m, castingRemovalStep.ProductionSkilledLabour.GetValueOrDefault(), "SandCasting -> CastingRemovalStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, castingRemovalStep.SetupSkilledLabour.GetValueOrDefault(), "SandCasting -> CastingRemovalStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a castingRemovalStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="castingRemovalStep">The parent process step of machines.</param>
        private void ValidateCastingRemovalMachines(CastingModuleViewModel viewModel, ProcessStep castingRemovalStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were generated
            if (partWeight <= 15)
            {
                Machine castingRemovalMachine1 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Cast Removal (Vibration-Desk) I");
                Assert.IsNotNull(castingRemovalMachine1, "CastingRemovalStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine castingRemovalMachine2 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation I");
                    Assert.IsNotNull(castingRemovalMachine2, "CastingRemovalStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine castingRemovalMachine1 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Cast Removal (Vibration-Desk) II");
                Assert.IsNotNull(castingRemovalMachine1, "CastingRemovalStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine castingRemovalMachine2 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation II");
                    Assert.IsNotNull(castingRemovalMachine2, "CastingRemovalStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine castingRemovalMachine1 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Cast Removal (Vibration-Desk) III");
                Assert.IsNotNull(castingRemovalMachine1, "CastingRemovalStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine castingRemovalMachine2 = castingRemovalStep.Machines.FirstOrDefault(m => m.Name == "Casting Automation III");
                    Assert.IsNotNull(castingRemovalMachine2, "CastingRemovalStep -> Failed to generate the expected machine.");
                }
            }
        }

        /// <summary>
        /// The method validates a Deburring step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="deburringStep">The DeburringStep step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateDeburringStep(CastingModuleViewModel viewModel, ProcessStep deburringStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the CycleTime was correctly calculated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                Assert.AreEqual<decimal>(15, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<decimal>(30, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(60, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 190)
            {
                Assert.AreEqual<decimal>(120, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 240)
            {
                Assert.AreEqual<decimal>(150, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 300)
            {
                Assert.AreEqual<decimal>(180, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(210, deburringStep.CycleTime.GetValueOrDefault(), "DeburringStep -> Wrong CycleTime value.");
            }

            // Verify that the ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour were correctly calculated
            Assert.AreEqual<decimal>(deburringStep.CycleTime.GetValueOrDefault(), deburringStep.ProcessTime.GetValueOrDefault(), "DeburringStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(0m, deburringStep.SetupTime.GetValueOrDefault(), "DeburringStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(0m, deburringStep.MaxDownTime.GetValueOrDefault(), "DeburringStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(0.5m, deburringStep.ProductionSkilledLabour.GetValueOrDefault(), "DeburringStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, deburringStep.SetupSkilledLabour.GetValueOrDefault(), "DeburringStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a deburringStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="deburringStep">The parent process step of machines.</param>
        private void ValidateDeburringMachines(CastingModuleViewModel viewModel, ProcessStep deburringStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were generated
            if (partWeight <= 2)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP8/200 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 8)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP8/300 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 15)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP10/300 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 25)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP10/300 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 40)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP13/650 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 90)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP13/1000 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP16/1000 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 190)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP16/2000 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
            }
            else if (partWeight <= 240)
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP22/1000 kN");
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
            }
            else
            {
                Machine deburringMachine1 = deburringStep.Machines.FirstOrDefault(m => m.Name == "SEP22/2000 kN" && m.FloorSize == 27);
                Assert.IsNotNull(deburringMachine1, "DeburringStep -> Failed to generate the expected machine.");

                Machine deburringMachine2 = deburringStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(deburringMachine2, "DeburringStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates an AbrasiveBlasting step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="abrasiveBlastingStep">The AbrasiveBlasting step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateAbrasiveBlastingStep(CastingModuleViewModel viewModel, ProcessStep abrasiveBlastingStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the value of CycleTime was correctly calculated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 8)
            {
                Assert.AreEqual<decimal>(180, abrasiveBlastingStep.CycleTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(220, abrasiveBlastingStep.CycleTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(360, abrasiveBlastingStep.CycleTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong CycleTime value.");
            }

            // Verify that the value of PartsPerCycle was correctly calculated
            if (partWeight <= 1)
            {
                Assert.AreEqual<int>(10, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 2)
            {
                Assert.AreEqual<int>(8, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 4)
            {
                Assert.AreEqual<int>(7, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 8)
            {
                Assert.AreEqual<int>(6, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 15)
            {
                Assert.AreEqual<int>(5, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 25)
            {
                Assert.AreEqual<int>(4, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<int>(3, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<int>(2, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }
            else
            {
                Assert.AreEqual<int>(1, abrasiveBlastingStep.PartsPerCycle.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong PartsPerCycle value.");
            }

            // Verify that the values of ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour
            Assert.AreEqual<decimal>(abrasiveBlastingStep.CycleTime.GetValueOrDefault(), abrasiveBlastingStep.ProcessTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(600m, abrasiveBlastingStep.SetupTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(600m, abrasiveBlastingStep.MaxDownTime.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(0.5m, abrasiveBlastingStep.ProductionSkilledLabour.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, abrasiveBlastingStep.SetupSkilledLabour.GetValueOrDefault(), "AbrasiveBlastingStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of an abrasiveBlastingStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="abrasiveBlastingStep">The parent process step of machines.</param>
        private void ValidateAbrasiveBlastingMachines(CastingModuleViewModel viewModel, ProcessStep abrasiveBlastingStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were returned
            if (partWeight <= 8)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM300/2");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 15)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 90)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2/E");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 190)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM400/2/E");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
            }
            else if (partWeight <= 300)
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM500/2/E");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
            }
            else
            {
                Machine abrasiveBlastingMachine1 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "THM500/2/E");
                Assert.IsNotNull(abrasiveBlastingMachine1, "AbrasiveBlastingStep -> Failed to generate the expected machine.");

                Machine abrasiveBlastingMachine2 = abrasiveBlastingStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 30");
                Assert.IsNotNull(abrasiveBlastingMachine2, "AbrasiveBlastingStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates an XRay step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="xRayStep">The XRay step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateXRayStep(CastingModuleViewModel viewModel, ProcessStep xRayStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the value of CycleTime was correctly calculated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 1)
            {
                Assert.AreEqual<decimal>(15, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 2)
            {
                Assert.AreEqual<decimal>(17, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 4)
            {
                Assert.AreEqual<decimal>(19, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 8)
            {
                Assert.AreEqual<decimal>(21, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 15)
            {
                Assert.AreEqual<decimal>(23, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 25)
            {
                Assert.AreEqual<decimal>(25, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<decimal>(26, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 60)
            {
                Assert.AreEqual<decimal>(28, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(30, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 130)
            {
                Assert.AreEqual<decimal>(32, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 190)
            {
                Assert.AreEqual<decimal>(35, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 240)
            {
                Assert.AreEqual<decimal>(40, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 300)
            {
                Assert.AreEqual<decimal>(45, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(50, xRayStep.CycleTime.GetValueOrDefault(), "XRayStep -> Wrong CycleTime value.");
            }

            // Verify that the values of ProcessTime, PartsPerCycle, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour were correctly calculated
            Assert.AreEqual<decimal>(xRayStep.CycleTime.GetValueOrDefault(), xRayStep.ProcessTime.GetValueOrDefault(), "XRayStep -> Wrong ProcessTime value.");
            Assert.AreEqual<int>(100, xRayStep.PartsPerCycle.GetValueOrDefault(), "XRayStep -> Wrong PartsPerCycle value.");
            Assert.AreEqual<decimal>(600m, xRayStep.SetupTime.GetValueOrDefault(), "XRayStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(600m, xRayStep.MaxDownTime.GetValueOrDefault(), "XRayStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(1m, xRayStep.ProductionSkilledLabour.GetValueOrDefault(), "XRayStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, xRayStep.SetupSkilledLabour.GetValueOrDefault(), "XRayStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// The method validates the machines of a xRayStep according to the specification.
        /// </summary>
        /// <param name="viewModel">The view model from which the machines were generated.</param>
        /// <param name="xRayStep">The parent process step of machines.</param>
        private void ValidateXRayMachines(CastingModuleViewModel viewModel, ProcessStep xRayStep)
        {
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();

            // Verify that the expected machines were returned
            if (partWeight <= 15)
            {
                Machine xRayMachine1 = xRayStep.Machines.FirstOrDefault(m => m.Name == "Y.MU56 // Y.PXV 5000");
                Assert.IsNotNull(xRayMachine1, "XRayStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine xRayMachine2 = xRayStep.Machines.FirstOrDefault(m => m.Name == "PAUSt 16");
                    Assert.IsNotNull(xRayMachine2, "XRayStep -> Failed to generate the expected machine.");
                }
            }
            else if (partWeight <= 130)
            {
                Machine xRayMachine1 = xRayStep.Machines.FirstOrDefault(m => m.Name == "Y.MU56 // Y.PXV 5000");
                Assert.IsNotNull(xRayMachine1, "XRayStep -> Failed to generate the expected machine.");

                if (viewModel.Process == CastingProcessType.Automatic)
                {
                    Machine xRayMachine2 = xRayStep.Machines.FirstOrDefault(m => m.Name == "CR 120");
                    Assert.IsNotNull(xRayMachine2, "XRayStep -> Failed to generate the expected machine.");
                }
            }
            else
            {
                Machine xRayMachine1 = xRayStep.Machines.FirstOrDefault(m => m.Name == "Y.MU56 // Y.PXV 5000");
                Assert.IsNotNull(xRayMachine1, "XRayStep -> Failed to generate the expected machine.");

                Machine xRayMachine2 = xRayStep.Machines.FirstOrDefault(m => m.Name == "WA 120");
                Assert.IsNotNull(xRayMachine2, "XRayStep -> Failed to generate the expected machine.");
            }
        }

        /// <summary>
        /// The method validates a CheckAndPack step.
        /// </summary>
        /// <param name="viewModel">The view model from which the step was generated.</param>
        /// <param name="checkAndPackStep">The CheckAndPack step to be validated according to the specification.</param>
        /// <param name="stepChangeSet">The change set associated with the step.</param>
        private void ValidateCheckAndPackStep(CastingModuleViewModel viewModel, ProcessStep checkAndPackStep, IDataSourceManager stepChangeSet)
        {
            // Verify that the value of CycleTime was correctly calculated
            decimal partWeight = viewModel.PartWeight.GetValueOrDefault();
            if (partWeight <= 1)
            {
                Assert.AreEqual<decimal>(15, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 2)
            {
                Assert.AreEqual<decimal>(17, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 4)
            {
                Assert.AreEqual<decimal>(19, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 8)
            {
                Assert.AreEqual<decimal>(21, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 15)
            {
                Assert.AreEqual<decimal>(23, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 25)
            {
                Assert.AreEqual<decimal>(25, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 40)
            {
                Assert.AreEqual<decimal>(26, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 60)
            {
                Assert.AreEqual<decimal>(28, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 90)
            {
                Assert.AreEqual<decimal>(30, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 130)
            {
                Assert.AreEqual<decimal>(32, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 190)
            {
                Assert.AreEqual<decimal>(35, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 240)
            {
                Assert.AreEqual<decimal>(40, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else if (partWeight <= 300)
            {
                Assert.AreEqual<decimal>(45, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }
            else
            {
                Assert.AreEqual<decimal>(50, checkAndPackStep.CycleTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong CycleTime value.");
            }

            // Verify that the values of ProcessTime, SetupTime, MaxDownTime, ProductionSkilledLabour and SetupSkilledLabour were correctly calculated
            Assert.AreEqual<decimal>(checkAndPackStep.CycleTime.GetValueOrDefault(), checkAndPackStep.ProcessTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong ProcessTime value.");
            Assert.AreEqual<decimal>(0m, checkAndPackStep.SetupTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong SetupTime value.");
            Assert.AreEqual<decimal>(0m, checkAndPackStep.MaxDownTime.GetValueOrDefault(), "CheckAndPackStep -> Wrong MaxDownTime value.");
            Assert.AreEqual<decimal>(1m, checkAndPackStep.ProductionSkilledLabour.GetValueOrDefault(), "CheckAndPackStep -> Wrong ProductionSkilledLabour value.");
            Assert.AreEqual<decimal>(1m, checkAndPackStep.SetupSkilledLabour.GetValueOrDefault(), "CheckAndPackStep -> Wrong SetupSkilledLabour value.");
        }

        /// <summary>
        /// Check if the default values of a process steps were set.
        /// </summary>
        /// <param name="step"> The step for which to verify the default values.</param>
        private void ValidateProcessStepDefaultValues(ProcessStep step)
        {
            Assert.IsNotNull(step.PartsPerCycle, "Failed to set a default value for 'PartsPerCycle' property.");
            Assert.IsNotNull(step.SetupTime, "Failed to set a default value for 'SetupTime' property.");
            Assert.IsNotNull(step.MaxDownTime, "Failed to set a default value for 'MaxDownTime' property.");
            Assert.IsNotNull(step.CycleTimeUnit, "Failed to set a default value for 'CycleTimeUnit' property.");
            Assert.IsNotNull(step.ProcessTimeUnit, "Failed to set a default value for 'ProcessTimeUnit' property.");
            Assert.IsNotNull(step.SetupTimeUnit, "Failed to set a default value for 'SetupTimeUnit' property.");
            Assert.IsNotNull(step.MaxDownTimeUnit, "Failed to set a default value for 'MaxDownTimeUnit' property.");
            Assert.IsNotNull(step.Accuracy, "Failed to set a default value for 'Accuracy' property.");
            Assert.IsNotNull(step.SetupsPerBatch, "Failed to set a default value for 'SetupsPerBatch' property.");
            Assert.IsNotNull(step.ScrapAmount, "Failed to set a default value for 'ScrapAmount' property.");
            Assert.IsNotNull(step.Rework, "Failed to set a default value for 'Rework' property.");
            Assert.IsNotNull(step.ShiftsPerWeek, "Failed to set a default value for 'ShiftsPerWeek' property.");
            Assert.IsNotNull(step.HoursPerShift, "Failed to set a default value for 'HoursPerShift' property.");
            Assert.IsNotNull(step.ProductionDaysPerWeek, "Failed to set a default value for 'ProductionDaysPerWeek' property.");
            Assert.IsNotNull(step.ProductionWeeksPerYear, "Failed to set a default value for 'ProductionWeeksPerYear' property.");
            Assert.IsNotNull(step.ProductionUnskilledLabour, "Failed to set a default value for 'ProductionUnskilledLabour' property.");
            Assert.IsNotNull(step.ProductionSkilledLabour, "Failed to set a default value for 'ProductionSkilledLabour' property.");
            Assert.IsNotNull(step.ProductionForeman, "Failed to set a default value for 'ProductionForeman' property.");
            Assert.IsNotNull(step.ProductionTechnicians, "Failed to set a default value for 'ProductionTechnicians' property.");
            Assert.IsNotNull(step.ProductionEngineers, "Failed to set a default value for 'ProductionEngineers' property.");
            Assert.IsNotNull(step.SetupUnskilledLabour, "Failed to set a default value for 'SetupUnskilledLabour' property.");
            Assert.IsNotNull(step.SetupSkilledLabour, "Failed to set a default value for 'SetupSkilledLabour' property.");
            Assert.IsNotNull(step.SetupForeman, "Failed to set a default value for 'SetupForeman' property.");
            Assert.IsNotNull(step.SetupTechnicians, "Failed to set a default value for 'SetupTechnicians' property.");
            Assert.IsNotNull(step.SetupEngineers, "Failed to set a default value for 'SetupEngineers' property.");

            // Validate the default BatchSize value
            int? batchSizePerYear = null;
            int? yearlyProdQty = null;
            Part parentPart = step.Process.Parts.FirstOrDefault();
            Assembly parentAssembly = step.Process.Assemblies.FirstOrDefault();
            if (parentPart != null)
            {
                batchSizePerYear = parentPart.BatchSizePerYear;
                yearlyProdQty = parentPart.YearlyProductionQuantity;
            }
            else if (parentAssembly != null)
            {
                batchSizePerYear = parentAssembly.BatchSizePerYear;
                yearlyProdQty = parentAssembly.YearlyProductionQuantity;
            }

            if (yearlyProdQty.HasValue && batchSizePerYear.HasValue && batchSizePerYear != 0)
            {
                double batchSize = Math.Ceiling(Convert.ToDouble(yearlyProdQty.Value) / Convert.ToDouble(batchSizePerYear.Value));
                if (batchSize <= int.MaxValue)
                {
                    Assert.AreEqual<int>(Convert.ToInt32(batchSize), step.BatchSize.GetValueOrDefault(), "Failed to calculate the default value for 'BatchSize' property.");
                }
            }
        }

        #endregion Helpers
    }

    #region Inner Classes

    /// <summary>
    /// The service that navigates between the Casting Wizard steps.
    /// </summary>
    [Export(typeof(ICastingModuleNavigationService))]
    public class CastingModuleNavigationServiceMock : ICastingModuleNavigationService
    {
        /// <summary>
        /// Starts the casting wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The casting wizard's view model (necessary to be set as the data context of the casting wizard views).</param>
        public void StartWizard(CastingModuleViewModel viewModel)
        {
            viewModel.FinishCommand.Execute(null);
        }

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        public void CloseWizard()
        {
        }

        /// <summary>
        /// Determines whether it is possible to navigate to the next casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        public bool CanGoToNextStep()
        {
            return true;
        }

        /// <summary>
        /// Navigates to the next casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user.</param>
        public void GoToNextStep(decimal numberOfCores)
        {
        }

        /// <summary>
        /// Determines the next casting step, without navigating to it.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        /// <returns>A value identifying the next step.</returns>
        public CastingWizardStep PredictNextStep(decimal numberOfCores)
        {
            return CastingWizardStep.None;
        }

        /// <summary>
        /// Determines whether it is possible to navigate to the previous casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        public bool CanGoToPreviousStep()
        {
            return true;
        }

        /// <summary>
        /// Navigates to the previous casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        public void GoToPreviousStep(decimal numberOfCores)
        {
        }
    }

    #endregion Inner Classes
}
