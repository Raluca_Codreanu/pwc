﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for BatchReportingViewModel and is intended 
    /// to contain unit tests for all public methods from BatchReportingViewModel class.
    /// </summary>
    [TestClass]
    public class BatchReportingViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BatchReportingViewModelTest"/> class.
        /// </summary>
        public BatchReportingViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for CheckAllCommand. The following input data were used: IsChecked = true.
        /// </summary>
        [TestMethod]
        public void CheckAllTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the DataSource
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the DataSource
            batchReportingViewModel.DataSource = new System.Collections.ObjectModel.ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            BatchReportingDataSourceItem part1Item = new BatchReportingDataSourceItem();
            part1Item.Entity = part1;
            part1Item.DisplayName = part1.Name;
            assemblyItem.Children.Add(part1Item);

            BatchReportingDataSourceItem subassemblyItem = new BatchReportingDataSourceItem();
            subassemblyItem.Entity = subassembly;
            subassemblyItem.DisplayName = subassembly.Name;
            assemblyItem.Children.Add(subassemblyItem);

            BatchReportingDataSourceItem part2Item = new BatchReportingDataSourceItem();
            part2Item.Entity = part2;
            part2Item.DisplayName = part2.Name;
            batchReportingViewModel.DataSource.Add(part2Item);

            ICommand checkAllCommand = batchReportingViewModel.CheckAllCommand;
            checkAllCommand.Execute(true);

            // Verifies if all items were checked
            ItemsChecked(batchReportingViewModel.DataSource, true);
        }

        /// <summary>
        /// A test for CheckAllCommand. The following input data were used: IsChecked = false.
        /// </summary>
        [TestMethod]
        public void CheckAllTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the DataSource
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the DataSource
            batchReportingViewModel.DataSource = new System.Collections.ObjectModel.ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            BatchReportingDataSourceItem part1Item = new BatchReportingDataSourceItem();
            part1Item.Entity = part1;
            part1Item.DisplayName = part1.Name;
            assemblyItem.Children.Add(part1Item);

            BatchReportingDataSourceItem subassemblyItem = new BatchReportingDataSourceItem();
            subassemblyItem.Entity = subassembly;
            subassemblyItem.DisplayName = subassembly.Name;
            assemblyItem.Children.Add(subassemblyItem);

            BatchReportingDataSourceItem part2Item = new BatchReportingDataSourceItem();
            part2Item.Entity = part2;
            part2Item.DisplayName = part2.Name;
            batchReportingViewModel.DataSource.Add(part2Item);

            ICommand checkAllCommand = batchReportingViewModel.CheckAllCommand;
            checkAllCommand.Execute(false);

            // Verifies that no item was checked
            ItemsChecked(batchReportingViewModel.DataSource, false);
        }

        /// <summary>
        /// A test for CreateReportCommand. The following input data were used:
        /// Entity = null.
        /// </summary>
        [TestMethod]
        public void CreateReportsTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the DataSource
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the DataSource
            batchReportingViewModel.DataSource = new System.Collections.ObjectModel.ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            BatchReportingDataSourceItem part1Item = new BatchReportingDataSourceItem();
            part1Item.Entity = part1;
            part1Item.DisplayName = part1.Name;
            assemblyItem.Children.Add(part1Item);

            BatchReportingDataSourceItem subassemblyItem = new BatchReportingDataSourceItem();
            subassemblyItem.Entity = subassembly;
            subassemblyItem.DisplayName = subassembly.Name;
            assemblyItem.Children.Add(subassemblyItem);

            BatchReportingDataSourceItem part2Item = new BatchReportingDataSourceItem();
            part2Item.Entity = part2;
            part2Item.DisplayName = part2.Name;
            batchReportingViewModel.DataSource.Add(part2Item);

            // Set up the view model
            string path = Path.Combine(Path.GetTempPath() + project.Guid.ToString());
            Directory.CreateDirectory(path);
            folderBrowserService.SelectedPath = path;
            folderBrowserService.FolderIsSelected = true;

            try
            {
                ICommand createReportsCommand = batchReportingViewModel.CreateReportsCommand;
                createReportsCommand.Execute(null);

                // Verifies that the reports were not generated
                int reportsNo = Directory.GetFiles(path).Count();
                Assert.IsTrue(reportsNo == 0, "No report should be created if the view model's entity is null.");
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Utils.DeleteDirectory(path);
                }
            }
        }

        /// <summary>
        /// A test for CreateReportCommand. The following input data were used:
        /// DataSource = null.
        /// </summary>
        [TestMethod]
        public void CreateReportsTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the entity of batchReportingViewModel
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Set up the view model
            string path = Path.Combine(Path.GetTempPath() + Guid.NewGuid().ToString());
            Directory.CreateDirectory(path);
            folderBrowserService.SelectedPath = path;
            folderBrowserService.FolderIsSelected = true;
            batchReportingViewModel.Entity = project;
            batchReportingViewModel.EntitySourceDb = dataContext.DatabaseId;

            try
            {
                ICommand createReportsCommand = batchReportingViewModel.CreateReportsCommand;
                createReportsCommand.Execute(null);

                // Verifies that the reports were not generated
                int reportsNo = Directory.GetFiles(path).Count();
                Assert.IsTrue(reportsNo == 0, "No report should be created if the view model's data source is null.");
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Utils.DeleteDirectory(path);
                }
            }
        }

        /// <summary>
        /// A test for CreateReportCommand. The following input data were used:
        /// Entity = Assembly and EntityParent = null.
        /// </summary>
        [TestMethod]
        public void CreateReportsTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Set up the data source
            batchReportingViewModel.DataSource = new ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            // Set up the view model
            string path = Path.Combine(Path.GetTempPath() + assembly.Guid.ToString());
            Directory.CreateDirectory(path);
            folderBrowserService.SelectedPath = path;
            folderBrowserService.FolderIsSelected = true;
            batchReportingViewModel.Entity = assembly;

            try
            {
                ICommand createReportsCommand = batchReportingViewModel.CreateReportsCommand;
                createReportsCommand.Execute(null);

                // Verifies that the reports were not generated
                int reportsNo = Directory.GetFiles(path).Count();
                Assert.IsTrue(reportsNo == 0, "No report should be created if the view model's entity is assembly and the EntityParent is null.");
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Utils.DeleteDirectory(path);
                }
            }
        }

        /// <summary>
        /// A test for CreateReportCommand. The following input data were used:
        /// Entity.Name = string.Empty.
        /// </summary>
        [TestMethod]
        public void CreateReportsTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the DataSource
            Project project = new Project();
            project.Name = string.Empty;
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the DataSource
            batchReportingViewModel.DataSource = new System.Collections.ObjectModel.ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            BatchReportingDataSourceItem part1Item = new BatchReportingDataSourceItem();
            part1Item.Entity = part1;
            part1Item.DisplayName = part1.Name;
            assemblyItem.Children.Add(part1Item);

            BatchReportingDataSourceItem subassemblyItem = new BatchReportingDataSourceItem();
            subassemblyItem.Entity = subassembly;
            subassemblyItem.DisplayName = subassembly.Name;
            assemblyItem.Children.Add(subassemblyItem);

            BatchReportingDataSourceItem part2Item = new BatchReportingDataSourceItem();
            part2Item.Entity = part2;
            part2Item.DisplayName = part2.Name;
            batchReportingViewModel.DataSource.Add(part2Item);

            // Set up the view model
            string path = Path.Combine(Path.GetTempPath() + project.Guid.ToString());
            Directory.CreateDirectory(path);
            folderBrowserService.SelectedPath = path;
            folderBrowserService.FolderIsSelected = true;
            batchReportingViewModel.Entity = project;

            try
            {
                ICommand createReportsCommand = batchReportingViewModel.CreateReportsCommand;
                createReportsCommand.Execute(null);

                // Verifies that the reports were not generated
                int reportsNo = Directory.GetFiles(path).Count();
                Assert.IsTrue(reportsNo == 0, "No report should be created if the view model's entity name is null or empty.");
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Utils.DeleteDirectory(path);
                }
            }
        }

        /// <summary>
        /// A test for CreateReportsCommand. The following input data were used:
        /// Entity = Assembly, FolderIsSelected = false.
        /// </summary>
        [TestMethod]
        public void CreateReportsTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            FolderBrowserServiceMock folderBrowserService = new FolderBrowserServiceMock();
            BatchReportingViewModel batchReportingViewModel = new BatchReportingViewModel(messenger, windowService, folderBrowserService, new UnitsServiceMock());

            // Set up the DataSource
            Project project = new Project();
            project.Name = string.Empty;
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the DataSource
            batchReportingViewModel.DataSource = new System.Collections.ObjectModel.ObservableCollection<BatchReportingDataSourceItem>();
            BatchReportingDataSourceItem assemblyItem = new BatchReportingDataSourceItem();
            assemblyItem.Entity = assembly;
            assemblyItem.DisplayName = assembly.Name;
            batchReportingViewModel.DataSource.Add(assemblyItem);

            BatchReportingDataSourceItem part1Item = new BatchReportingDataSourceItem();
            part1Item.Entity = part1;
            part1Item.DisplayName = part1.Name;
            assemblyItem.Children.Add(part1Item);

            BatchReportingDataSourceItem subassemblyItem = new BatchReportingDataSourceItem();
            subassemblyItem.Entity = subassembly;
            subassemblyItem.DisplayName = subassembly.Name;
            assemblyItem.Children.Add(subassemblyItem);

            // Set up the view model
            string path = Path.Combine(Path.GetTempPath() + project.Guid.ToString());
            Directory.CreateDirectory(path);
            folderBrowserService.SelectedPath = path;
            folderBrowserService.FolderIsSelected = false;
            batchReportingViewModel.Entity = assembly;
            batchReportingViewModel.EntityParent = project;

            try
            {
                ICommand createReportsCommand = batchReportingViewModel.CreateReportsCommand;
                createReportsCommand.Execute(null);

                // Verifies that the reports were not generated
                int reportsNo = Directory.GetFiles(path).Count();
                Assert.IsTrue(reportsNo == 0, "No report should be created if no folder was selected for save.");
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Utils.DeleteDirectory(path);
                }
            }
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verifies if all items belonging to a collection of BatchReportingDataSourceItem are checked or not. 
        /// </summary>
        /// <param name="items">The BatchReportingDataSourceItem Collection.</param>
        /// <param name="isChecked">The value to compare with.</param>
        private void ItemsChecked(ObservableCollection<BatchReportingDataSourceItem> items, bool isChecked)
        {
            foreach (BatchReportingDataSourceItem item in items)
            {
                Assert.AreEqual<bool>(item.IsChecked.GetValueOrDefault(), isChecked, "Failed to set the IsChecked property.");
                ItemsChecked(item.Children, isChecked);
            }
        }

        /// <summary>
        /// Gets the number of files from a specified directory.
        /// </summary>
        /// <param name="path">The directory's path.</param>
        /// <param name="filesCount">The number of files from the specified directory.</param>
        private void GetFilesCount(string path, ref int filesCount)
        {
            if (!Directory.Exists(path))
            {
                return;
            }

            filesCount += Directory.GetFiles(path).Count();
            foreach (string directory in Directory.GetDirectories(path))
            {
                GetFilesCount(directory, ref filesCount);
            }
        }

        #endregion Helpers
    }
}
