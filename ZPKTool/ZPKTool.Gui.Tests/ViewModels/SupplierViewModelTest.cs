﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ProjectSupplierViewModel and is intended 
    /// to contain unit tests for all ProjectSupplierViewModel commands
    /// </summary>
    [TestClass]
    public class SupplierViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "SupplierViewModelTest"/> class.
        /// </summary>
        public SupplierViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests SaveDataToModel command 
        /// </summary>
        [TestMethod]
        public void SaveDataToModelTest()
        {
            var typeCatalog = new TypeCatalog(
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(OnlineCheckServiceMock),
                typeof(WindowServiceMock),
                typeof(MasterDataBrowserViewModel),
                typeof(CountryAndSupplierBrowserViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            CompositionContainer compositionContainer = bootstrapper.CompositionContainer;
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            SupplierViewModel projectSupplierViewModel = new SupplierViewModel(windowService, compositionContainer);

            // Set input data and create a new model
            Customer model = new Customer();
            projectSupplierViewModel.Model = model;
            projectSupplierViewModel.Name.Value = "Test Supplier" + DateTime.Now.Ticks;
            projectSupplierViewModel.Type.Value = SupplierType.Holding;
            projectSupplierViewModel.Number.Value = DateTime.Now.Ticks.ToString();
            projectSupplierViewModel.Description.Value = "Supplier Description" + DateTime.Now.Ticks;
            projectSupplierViewModel.StreetAddress.Value = "Address" + DateTime.Now.Ticks;
            projectSupplierViewModel.City.Value = "City" + DateTime.Now.Ticks;
            projectSupplierViewModel.ZipCode.Value = DateTime.Now.Ticks.ToString();
            projectSupplierViewModel.Country.Value = "Country" + DateTime.Now.Ticks;
            projectSupplierViewModel.CountrySupplier.Value = "Supplier" + DateTime.Now.Ticks;
            projectSupplierViewModel.FirstContactName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectSupplierViewModel.FirstContactPhoneNo.Value = DateTime.Now.Ticks.ToString();
            projectSupplierViewModel.FirstContactFaxNo.Value = DateTime.Now.Ticks.ToString();
            projectSupplierViewModel.FirstContactMobileNo.Value = DateTime.Now.Ticks.ToString();
            projectSupplierViewModel.FirstContactEmail.Value = "test@gmail.com";
            projectSupplierViewModel.FirstContactWebPage.Value = "www.test.com";
            projectSupplierViewModel.SecondContactName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectSupplierViewModel.SecondContactEmail.Value = EncryptionManager.Instance.GenerateRandomString(5, true);
            projectSupplierViewModel.SecondContactPhoneNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectSupplierViewModel.SecondContactFaxNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectSupplierViewModel.SecondContactMobileNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectSupplierViewModel.SecondContactWebPage.Value = EncryptionManager.Instance.GenerateRandomString(9, true);

            windowService.IsOpen = true;
            ICommand saveDataToModelCommand = projectSupplierViewModel.SaveToModelCommand;
            saveDataToModelCommand.Execute(null);

            // Verifies if data was saved into model
            bool result = model.Name == projectSupplierViewModel.Name.Value &&
                          model.Type == projectSupplierViewModel.Type.Value &&
                          model.Number == projectSupplierViewModel.Number.Value &&
                          model.Description == projectSupplierViewModel.Description.Value &&
                          model.StreetAddress == projectSupplierViewModel.StreetAddress.Value &&
                          model.City == projectSupplierViewModel.City.Value &&
                          model.ZipCode == projectSupplierViewModel.ZipCode.Value &&
                          model.Country == projectSupplierViewModel.Country.Value &&
                          model.State == projectSupplierViewModel.CountrySupplier.Value &&
                          model.FirstContactName == projectSupplierViewModel.FirstContactName.Value &&
                          model.FirstContactPhone == projectSupplierViewModel.FirstContactPhoneNo.Value &&
                          model.FirstContactFax == projectSupplierViewModel.FirstContactFaxNo.Value &&
                          model.FirstContactMobile == projectSupplierViewModel.FirstContactMobileNo.Value &&
                          model.FirstContactEmail == projectSupplierViewModel.FirstContactEmail.Value &&
                          model.FirstContactWebAddress == projectSupplierViewModel.FirstContactWebPage.Value &&
                          model.SecondContactName == projectSupplierViewModel.SecondContactName.Value &&
                          model.SecondContactPhone == projectSupplierViewModel.SecondContactPhoneNo.Value &&
                          model.SecondContactFax == projectSupplierViewModel.SecondContactFaxNo.Value &&
                          model.SecondContactMobile == projectSupplierViewModel.SecondContactMobileNo.Value &&
                          model.SecondContactEmail == projectSupplierViewModel.SecondContactEmail.Value &&
                          model.SecondContactWebAddress == projectSupplierViewModel.SecondContactWebPage.Value;

            Assert.IsTrue(result, "Failed to save the input data into the model");
            Assert.IsFalse(projectSupplierViewModel.IsChanged, "Failed to reset the form changed flag.");
        }

        /// <summary>
        /// Tests BrowseSupplierMasterData command
        /// </summary>
        [TestMethod]
        public void BrowseSupplierMasterDataTest()
        {
            var typeCatalog = new TypeCatalog(
                typeof(UnitsServiceMock),
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(OnlineCheckServiceMock),
                typeof(WindowServiceMock),
                typeof(MasterDataBrowserViewModel),
                typeof(CountryAndSupplierBrowserViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            CompositionContainer compositionContainer = bootstrapper.CompositionContainer;
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            SupplierViewModel projectSupplierViewModel = new SupplierViewModel(windowService, compositionContainer);

            windowService.IsOpen = false;
            ICommand browseSupplierMasterDataCommand = projectSupplierViewModel.BrowseSupplierMasterDataCommand;
            browseSupplierMasterDataCommand.Execute(null);
        }

        ///// <summary>
        ///// Tests BrowseCountryMasterData command
        ///// </summary>
        //[TestMethod]
        //public void BrowseCountryMasterDataTest()
        //{
        //    CompositionContainer compositionContainer = this.bootstrapper.CompositionContainer;
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    ProjectSupplierViewModel projectSupplierViewModel = new ProjectSupplierViewModel(windowService, compositionContainer);

        //    windowService.IsOpen = false;
        //    ICommand browseCountryMasterDataCommand = projectSupplierViewModel.BrowseCountryMasterDataCommand;
        //    browseCountryMasterDataCommand.Execute(null);

        //    // Verify that the BrowserMasterData window is open
        //    Assert.IsTrue(windowService.IsOpen, "Failed to open the BrowserMasterData window.");
        //}

        ///// <summary>
        ///// Tests BrowseCountrySupplierMasterData command
        ///// </summary>
        //[TestMethod]
        //public void BrowseCountrySupplierMasterDataTest()
        //{
        //    CompositionContainer compositionContainer = this.bootstrapper.CompositionContainer;
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    SupplierViewModel projectSupplierViewModel = new SupplierViewModel(windowService, compositionContainer);
        //    ICommand browseCountrySupplierMasterDataCommand = projectSupplierViewModel.BrowseCountrySupplierMasterDataCommand;

        /// <summary>
        /// Tests BrowseCountrySupplierMasterData command
        /// </summary>
        [TestMethod]
        public void BrowseCountrySupplierMasterDataTest()
        {
            var typeCatalog = new TypeCatalog(
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(OnlineCheckServiceMock),
                typeof(WindowServiceMock),
                typeof(MasterDataBrowserViewModel),
                typeof(CountryAndSupplierBrowserViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            CompositionContainer compositionContainer = bootstrapper.CompositionContainer;
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            SupplierViewModel projectSupplierViewModel = new SupplierViewModel(windowService, compositionContainer);
            ICommand browseCountrySupplierMasterDataCommand = projectSupplierViewModel.BrowseCountrySupplierMasterDataCommand;

            messageBoxService.ShowCallCount = 0;
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            browseCountrySupplierMasterDataCommand.Execute(null);

            messageBoxService.ShowCallCount = 0;
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            browseCountrySupplierMasterDataCommand.Execute(null);

            // The country was null => a message should be displayed which informs the user to select a country
            Assert.AreEqual<int>(1, messageBoxService.ShowCallCount, "Invalid number of calls of show message box method");
            Assert.IsNull(projectSupplierViewModel.CountrySupplier.Value, "CountryAndSupplierBrowserView was displayed even if Country is null");

            // Add a country to projectSupplierViewModel
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            projectSupplierViewModel.DataSourceManager = dataContext;
            projectSupplierViewModel.Country.Value = dataContext.CountryRepository.GetAll().FirstOrDefault().Name;

            windowService.IsOpen = false;
            browseCountrySupplierMasterDataCommand.Execute(null);

            // TO DO: Simulate the selection of supplier and check that the view model is updated with the selected supplier
            // Verify that the MasterDataBrowser window is open
            Assert.IsTrue(windowService.IsOpen, "Failed to open the MasterDataBrowser.");
        }
    }
}
