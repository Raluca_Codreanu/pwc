﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for DatabaseServerOfflineViewModel and is intended 
    /// to contain unit tests for all DatabaseServerOfflineViewModel commands
    /// </summary>
    [TestClass]
    public class DatabaseServerOfflineViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "DatabaseServerOfflineViewModelTest"/> class.
        /// </summary>
        public DatabaseServerOfflineViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests RetryConnection command 
        /// </summary>
        [TestMethod]
        public void RetryConnectionTest()
        {
            IMessenger messenger = new Messenger();
            IOnlineCheckService onlineChecker = new OnlineCheckService(messenger);
            DatabaseServerOfflineViewModel dbServerOfflineViewModel = new DatabaseServerOfflineViewModel(messenger, onlineChecker);

            // Register an action which will handle message of type NotificationMessage
            bool retryNodeSelectionNotificationSend = false;
            messenger.Register<NotificationMessage>((msg) =>
            {
                if (msg.Notification == Notification.ReloadMainViewCurrentContent)
                {
                    retryNodeSelectionNotificationSend = true;
                }
            });

            // Execute RetryConnection command
            ICommand retryConnectionCommand = dbServerOfflineViewModel.RetryConnectionCommand;
            retryConnectionCommand.Execute(null);

            // RetryConnection  is executed asynchronous => wait until the thread is executed in order to obtain the results
            while (dbServerOfflineViewModel.IsRetrying)
            {
                Thread.Sleep(500);
            }

            // Verifies if the notification was send
            Assert.IsTrue(retryNodeSelectionNotificationSend, "Failed to retry the connection");
        }
    }
}
