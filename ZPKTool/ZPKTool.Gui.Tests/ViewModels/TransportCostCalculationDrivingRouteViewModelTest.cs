﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for TransportCostCalculationDrivingRouteViewModel and is intended to contain all its Unit Tests.
    /// </summary>
    [TestClass]
    public class TransportCostCalculationDrivingRouteViewModelTest
    {
        #region Attributes

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The please wait service.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationDrivingRouteViewModelTest"/> class.
        /// </summary>
        public TransportCostCalculationDrivingRouteViewModelTest()
        {
        }

        #region Test methods

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();

            this.windowService = new WindowServiceMock(messageDialogService, dispatcherService);
            this.messenger = new Messenger();
            this.pleaseWaitService = new PleaseWaitService(dispatcherService);
        }

        /// <summary>
        /// This is a test for closing the map window, to check if the show map setting is saved into UserSettingsManager.
        /// </summary>
        [TestMethod]
        public void SaveShowMapSettingTest()
        {
            UserSettingsManager.Instance.ShowMapWindow = true;
            UserSettingsManager.Instance.Save();

            TransportCostCalculationDrivingRouteViewModel drivingRouteVM = new TransportCostCalculationDrivingRouteViewModel(
                this.windowService,
                this.messenger,
                this.pleaseWaitService);

            drivingRouteVM.ShowMap = false;
            drivingRouteVM.CloseCommand.Execute(null);

            Assert.IsFalse(UserSettingsManager.Instance.ShowMapWindow, "The show map setting was not saved.");
        }

        /// <summary>
        /// This is a test for the GetRouteDirections method, to check if the expected error message is displayed in case of no coordinates passed.
        /// It also checks if the distance is 0, because the distance shouldn't have any value for empty coordinates.
        /// </summary>
        [TestMethod]
        public void GetRouteDirectionsForEmptyCoordinatesTest()
        {
            var drivingRouteVm = new TransportCostCalculationDrivingRouteViewModel(
                this.windowService,
                this.messenger,
                this.pleaseWaitService);
            var routeInfo = drivingRouteVm.GetRouteDirections();

            Assert.AreEqual(routeInfo.ErrorMessage, "We are unable to route with the given locations.", "The error message displayed is incorrect.");
            Assert.AreEqual(0m, routeInfo.Distance, "The distance should not be calculated for inexisting coordinates.");
        }

        /// <summary>
        /// This is a test for the GetRouteDirections method, to check if the expected error message is displayed in case of passing coordinates 
        /// that do not have a valid route between them; it also checks if the distance is 0, because the distance shouldn't have any value for this coordinates.
        /// </summary>
        [TestMethod]
        public void GetRouteDirectionsForInvalidRouteForCoordinatesTest()
        {
            TransportCostCalculationDrivingRouteViewModel drivingRouteVM = new TransportCostCalculationDrivingRouteViewModel(
                this.windowService,
                this.messenger,
                this.pleaseWaitService);

            drivingRouteVM.SourceLatitude = 17.123991m;
            drivingRouteVM.SourceLongitude = -29.039065m;
            drivingRouteVM.DestinationLatitude = 25.625677m;
            drivingRouteVM.DestinationLongitude = 17.718748m;

            var routeInfo = drivingRouteVM.GetRouteDirections();
            var expectedMessage = LocalizedResources.DrivingRoute_InvalidLatLongError;

            Assert.AreEqual(expectedMessage, routeInfo.ErrorMessage, "The error message displayed is incorrect.");
            Assert.AreEqual(0m, routeInfo.Distance, "The distance should not be calculated for incorrect coordinates.");
        }

        /// <summary>
        /// This is a test for the GetRouteDirections method, to check if no error message is displayed for valid route between the coordinates 
        /// and the distance has a value.
        /// </summary>
        [TestMethod]
        public void GetRouteDirectionsForValidCoordinatesTest()
        {
            TransportCostCalculationDrivingRouteViewModel drivingRouteVM = new TransportCostCalculationDrivingRouteViewModel(
                this.windowService,
                this.messenger,
                this.pleaseWaitService);

            drivingRouteVM.SourceLatitude = 48.885669m;
            drivingRouteVM.SourceLongitude = 2.381835m;
            drivingRouteVM.DestinationLatitude = 50.470092m;
            drivingRouteVM.DestinationLongitude = 30.559570m;

            var routeInfo = drivingRouteVM.GetRouteDirections();

            Assert.AreEqual(null, routeInfo.ErrorMessage, "No error message should be displayed.");
            Assert.AreNotEqual(0m, routeInfo.Distance, "The distance should be calculated for valid coordinates.");
        }

        #endregion
    }
}
