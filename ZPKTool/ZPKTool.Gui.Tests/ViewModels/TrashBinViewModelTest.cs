﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for TrashBinViewModel and is intended 
    /// to contain all TrashBinViewModel Unit Tests
    /// </summary>
    [TestClass]
    public class TrashBinViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinViewModelTest"/> class.
        /// </summary>
        public TrashBinViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests CheckItem command using valid input parameters
        ///// </summary>
        //[TestMethod]
        //public void CheckItemTest()
        //{
        //    ITrashBinView view = new TrashBinViewMock();
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();
        //    IBackgroundTaskScheduler taskScheduler = new BackgroundTaskScheduler(messenger);

        //    // Create a project 
        //    Project project = new Project();
        //    project.Name = "Test Project" + DateTime.Now.Ticks;
        //    project.IsDeleted = true;
        //    project.OverheadSettings = new OverheadSetting();
        //    User user1 = new User();
        //    Role adminRole = new Role();
        //    adminRole.Name = "Test Role";
        //    user1.Username = "User" + DateTime.Now.Ticks;
        //    user1.Password = EncryptionManager.Instance.EncodeMD5("Password");
        //    user1.Name = "User" + DateTime.Now.Ticks;
        //    user1.Roles.Add(adminRole);
        //    user1.CurrentRole = adminRole;
        //    project.Owner = user1;
        //    EntityManager.Instance.AddEntity(project, EntityContext.LocalDatabase);
        //    SecurityManager.Instance.Login(user1.Username, "Password");

        //    // Deletes the project and get the trash bin item corresponding to deleted project
        //    TrashBinManager.Instance.DeleteObject(project, false, true);
        //    TrashBinViewModel trashBinViewModel = new TrashBinViewModel(view, container, messageBoxService, messenger, taskScheduler);

        //    // Creates a grid and populate the grid with trash bin item objects and checkboxes
        //    DataGrid trashBinDataGrid = new DataGrid();
        //    DataGridCheckBoxColumn checkBoxColumn = new DataGridCheckBoxColumn();
        //    trashBinDataGrid.ItemsSource = trashBinViewModel.TrashBinItems;
        //    trashBinDataGrid.Columns.Add(checkBoxColumn);

        //    ICommand checkItemCommand = trashBinViewModel.CheckItemCommand;
        //    checkItemCommand.Execute(new CheckBox());
        //}

        ///// <summary>
        ///// Tests Recover Item command using valid input parameters
        ///// </summary>
        //[TestMethod]
        //public void RecoverItemTest()
        //{
        //    ITrashBinView view = new TrashBinViewMock();
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();
        //    IBackgroundTaskScheduler taskScheduler = new BackgroundTaskScheduler(messenger);

        //    Die die = new Die();
        //    die.Name = "Deleted Die" + DateTime.Now.Ticks;

        //    User user1 = new User();
        //    Role adminRole = new Role();
        //    adminRole.Name = "Test Role";//SecurityManager.Instance.GetAllRoles().FirstOrDefault<Role>(r=>r.Equals(UserRole.Admin));
        //    user1.Username = "User" + DateTime.Now.Ticks;
        //    user1.Password = EncryptionManager.Instance.EncodeMD5("Password");
        //    user1.Name = "User" + DateTime.Now.Ticks;
        //    user1.Roles.Add(adminRole);
        //    user1.CurrentRole = adminRole;
        //    die.Owner = user1;
        //    EntityManager.Instance.AddEntity(die, EntityContext.LocalDatabase);
        //    SecurityManager.Instance.Login(user1.Username, "Password");
        //    TrashBinManager.Instance.DeleteObject(die, false, true);
        //    TrashBinItem dieTrashBinItem = TrashBinManager.Instance.GetAllTrashBinItems().FirstOrDefault<TrashBinItem>(i => i.EntityGuid == die.Guid);

        //    TrashBinViewModel trashBinViewModel = new TrashBinViewModel(view, container, messageBoxService, messenger, taskScheduler);
        //    trashBinViewModel.TrashBinItems = new System.Collections.ObjectModel.ObservableCollection<TrashBinItem>();
        //    trashBinViewModel.TrashBinItems.Add(dieTrashBinItem);

        //    messageBoxService.ShowReturnValue = MessageBoxResult.Yes;
        //    messageBoxService.ShowCallCount = 0;
        //    ICommand recoverItemCommand = trashBinViewModel.RecoverItemCommand;
        //    recoverItemCommand.Execute(dieTrashBinItem);

        //    Die itemRecovered = EntityManager.Instance.GetDie(die.Guid, EntityContext.LocalDatabase);
        //    Assert.IsFalse(itemRecovered.IsDeleted, "Fails to recover a trash bin item");
        //    Assert.AreEqual<int>(0, trashBinViewModel.TrashBinItems.Count, "The recovered item was not removed from trash bin");
        //}

        ///// <summary>
        ///// Tests ViewItemDetails command using valid input parameters 
        ///// </summary>
        //[TestMethod]
        //public void ViewItemDetailsTest()
        //{
        //    // Create a project 
        //    Project project = new Project();
        //    project.Name = "Test Project" + DateTime.Now.Ticks;
        //    project.OverheadSettings = new OverheadSetting();

        //    User user1 = new User();
        //    user1.Username = "User" + DateTime.Now.Ticks;
        //    user1.Password = EncryptionManager.Instance.EncodeMD5("Password");
        //    user1.Name = "User" + DateTime.Now.Ticks;
        //    project.SetOwner(user1);

        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Role adminRole = dataContext.RoleRepository.GetAll().FirstOrDefault(r => r.Equals(UserRole.Admin));
        //    user1.Roles.Add(adminRole);
        //    dataContext.ProjectRepository.Add(project);
        //    dataContext.SaveChanges();

        //    SecurityManager.Instance.Login(user1.Username, "Password");
        //    SecurityManager.Instance.ChangeCurrentUserRole(adminRole);

        //    // Deletes the project and get the trash bin item corresponding to deleted project
        //    TrashManager trashManager = new TrashManager(dataContext);
        //    trashManager.DeleteToTrashBin(project);
        //    dataContext.SaveChanges();
        //    TrashBinItem projectItem = trashManager.GetAllTrashBinItems().FirstOrDefault(t => t.EntityGuid == project.Guid);

        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();
        //    IBackgroundTaskScheduler taskScheduler = new BackgroundTaskScheduler(messenger);
        //    TrashBinViewModel trashBinViewModel = new TrashBinViewModel(container, messageBoxService, messenger, taskScheduler);

        //    ICommand viewItemDetailsCommand = trashBinViewModel.ViewItemDetailsCommand;
        //    viewItemDetailsCommand.Execute(projectItem);

        //    Assert.IsTrue(trashBinViewModel.ItemDetails.GetType() == typeof(ViewProjectPanel), "A wrong panel was displayed.");
        //}
    }
}
