﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for UserViewModel and is intended 
    /// to contain unit tests for all public methods from UserViewModel class.
    /// </summary>
    [TestClass]
    public class UserViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new user with roles is added to the database.
        /// </summary>
        [TestMethod]
        public void AddUserWithRolesTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Name = newUser.Guid.ToString();

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.Password.Value = "Password.";

            userViewModel.SelectedAvailableRole = Role.Admin;
            userViewModel.AddRoleCommand.Execute(null);
            userViewModel.SaveCommand.Execute(null);

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            bool userExists = newDataContext.UserRepository.CheckIfExists(newUser.Guid);
            Assert.IsTrue(userExists, "The user was not added.");
        }

        /// <summary>
        /// A test for the save command, when a new user without roles can not be added to the database.
        /// </summary>
        [TestMethod]
        public void AddUserWithoutRolesTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Name = newUser.Guid.ToString();

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;            

            userViewModel.Password.Value = "Password.";


            userViewModel.SaveCommand.Execute(null);

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            bool userExists = newDataContext.UserRepository.CheckIfExists(newUser.Guid);
            Assert.IsFalse(userExists, "The user without roles was added.");
        }

        /// <summary>
        /// A test for the save command, when a current user's username and roles are modified.
        /// </summary>
        [TestMethod]
        public void EditUserTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Name = newUser.Guid.ToString();
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            string username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Username = username;            
            newUser.Roles = Role.ProjectLeader;
            var oldRole = newUser.Roles;

            dataContext.UserRepository.Add(newUser);
            dataContext.SaveChanges();

            newUser.Username = newUser.Guid.ToString();

            userViewModel.EditMode = ViewModelEditMode.Edit;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.IsChanged = true;

            var newRole = userViewModel.AvailableRoles.Value.FirstOrDefault(r => r.Value == Role.Admin);
            if (newRole != null)
            {
                userViewModel.CurrentRoles.Value.Add(newRole);
                userViewModel.SaveCommand.Execute(null);
            }
            
            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetById(newUser.Guid, true);
            Assert.AreNotEqual(username, user.Username, "The username was not changed.");
            Assert.AreNotEqual(oldRole, user.Roles, "The roles were not changed.");
        }

        /// <summary>
        /// A test for the cancel command, when the user confirms it.
        /// The user is in create mode.
        /// </summary>
        [TestMethod]
        public void CancelCommandConfirmedInCreateModeTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Name = newUser.Guid.ToString();
            newUser.Roles = Role.Admin;

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.SelectedAvailableRole = newUser.Roles;
            userViewModel.IsChanged = true;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            userViewModel.CancelCommand.Execute(null);

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            bool userExists = newDataContext.UserRepository.CheckIfExists(newUser.Guid);
            Assert.IsFalse(userExists, "The user was added.");
        }

        /// <summary>
        /// A test for the cancel command, when the user confirms it.
        /// The user is in edit mode.
        /// </summary>
        [TestMethod]
        public void CancelCommandConfirmedInEditModeTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Name = newUser.Guid.ToString();
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.ToString());
            string username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Username = username;
            newUser.Roles = Role.Admin;

            dataContext.UserRepository.Add(newUser);
            dataContext.SaveChanges();

            userViewModel.EditMode = ViewModelEditMode.Edit;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;

            userViewModel.Password.Value = "Password.";
            userViewModel.IsChanged = true;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            userViewModel.CancelCommand.Execute(null);

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetById(newUser.Guid, true);
            Assert.AreEqual(newUser.Password, user.Password, "The password was changed.");
        }

        /// <summary>
        /// A test for the can cancel command, when the model is not set.
        /// </summary>
        [TestMethod]
        public void CanCancelCommandWithNullModelTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            bool canCancel = userViewModel.CancelCommand.CanExecute(null);
            Assert.IsFalse(canCancel, "The cancel command was executed with a null model.");
        }

        /// <summary>
        /// A test for the OnUnloading event, handled successfully and user confirms quitting.
        /// The user is in create mode.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithQuitConfirmedInCreateModeTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Roles = Role.Admin;

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.IsChanged = true;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var unloading = userViewModel.OnUnloading();

            Assert.IsTrue(unloading, "The OnUnloading process did not ended successfully.");
        }

        /// <summary>
        /// A test for the OnUnloading event, handled successfully and user confirms quitting.
        /// The user is in create mode.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithQuitNotConfirmedInCreateModeTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.IsChanged = true;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var unloading = userViewModel.OnUnloading();

            Assert.IsFalse(unloading, "The OnUnloading process ended successfully.");
        }

        /// <summary>
        /// A test for the OnUnloading event, handled successfully and user confirms saving data.
        /// The user is in edit mode.
        /// </summary>
        //[TestMethod]
        //public void OnUnloadingWithSaveConfirmedInEditModeTest()
        //{
        //    MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
        //    UserViewModel userViewModel = new UserViewModel(windowService);
        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        //    User newUser = new User();
        //    newUser.Name = newUser.Guid.ToString();
        //    newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
        //    newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
        //    newUser.Roles = Role.Admin;

        //    dataContext.UserRepository.Add(newUser);
        //    dataContext.SaveChanges();

        //    //userViewModel.EditMode = ViewModelEditMode.Edit;
        //    userViewModel.DataSourceManager = dataContext;
        //    userViewModel.Model = newUser;
        //    userViewModel.Password.Value = "Password.";

        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    var unloading = userViewModel.OnUnloading();

        //    IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
        //    User user = newDataContext.UserRepository.GetById(newUser.Guid, true);

        //    Assert.IsTrue(unloading, "The OnUnloading did not ended successfully.");
        //    Assert.AreNotEqual(newUser.Password, user.Password, "The password was not changed.");
        //}

        /// <summary>
        /// A test for the OnUnloading event, handled successfully and user does not confirm saving data.
        /// The user is in edit mode.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithQuitNotConfirmedInEditModeTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            string name = newUser.Guid.ToString();
            newUser.Name = name;
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Roles = Role.Admin;

            dataContext.UserRepository.Add(newUser);
            dataContext.SaveChanges();

            newUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            userViewModel.EditMode = ViewModelEditMode.Edit;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;
            userViewModel.IsChanged = true;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var unloading = userViewModel.OnUnloading();

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetById(newUser.Guid, true);

            Assert.IsTrue(unloading, "The OnUnloading did not ended successfully.");
            Assert.AreEqual(name, user.Name, "The name was changed.");
        }

        /// <summary>
        /// A test for the add role command, when a new user with no roles is created, and a role from the available ones list 
        /// is moved into the current roles list.
        /// </summary>
        [TestMethod]
        public void AddRoleCommandTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User newUser = new User();
            newUser.Name = newUser.Guid.ToString();
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Roles = Role.Admin;

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;

            userViewModel.SelectedAvailableRole = Role.Admin;
            userViewModel.AddRoleCommand.Execute(null);

            bool isCurrentRole = userViewModel.CurrentRoles.Value.Any(r => r.Value == Role.Admin);
            Assert.IsTrue(isCurrentRole, "The role was not added to the current roles list.");
        }

        /// <summary>
        /// A test for the remove role command, when a new user with roles is created, and a role from the current ones list 
        /// is moved into the available roles list.
        /// </summary>
        [TestMethod]
        public void RemoveRoleCommandTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            UserViewModel userViewModel = new UserViewModel(windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);


            User newUser = new User();
            newUser.Name = newUser.Guid.ToString();
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Roles = Role.Admin;

            userViewModel.EditMode = ViewModelEditMode.Create;
            userViewModel.DataSourceManager = dataContext;
            userViewModel.Model = newUser;

            userViewModel.SelectedCurrentRole = Role.Admin;
            userViewModel.RemoveRoleCommand.Execute(null);

            bool isAvailableRole = userViewModel.AvailableRoles.Value.Any(r => r.Value == Role.Admin);
            Assert.IsTrue(isAvailableRole, "The role was not added to the current roles list.");
        }

        #endregion Test Methods
    }
}