﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ProjectFolderViewModel and is intended 
    /// to contain unit tests for all public methods from ProjectFolderViewModel class.
    /// </summary>
    [TestClass]
    public class ProjectFolderViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "ProjectFolderViewModelTest"/> class.
        /// </summary>
        public ProjectFolderViewModelTest()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }

        /// <summary>
        /// Use TestCleanup to run code after each test has run.
        /// If a user is logged in the application, it should be logged out after the test was done otherwise will influence other tests.
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for SaveCommand using a folder that does not exist in db.
        /// </summary>
        [TestMethod]
        public void SaveNewFolderTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            ProjectFolderViewModel projectFolderViewModel = new ProjectFolderViewModel(messenger, windowService);

            // Create a folder which will be the parent of the model
            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectFolderRepository.Add(folder);
            dataContext.SaveChanges();

            bool notificationSended = false;
            messenger.Register<EntityChangedMessage>((msg) =>
            {
                notificationSended = true;
            });

            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            // Set up the projectFolderViewModel                                    
            projectFolderViewModel.DataSourceManager = dataContext;
            projectFolderViewModel.InitializeForCreation(folder);
            projectFolderViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            windowService.IsOpen = true;
            ICommand saveCommand = projectFolderViewModel.SaveCommand;
            saveCommand.Execute(null);

            // TO DO -> Verify that the model was updated
            // Verify that a notification has been sent 
            Assert.IsTrue(notificationSended, "Failed to send a notification about the folder changing.");

            // Verify that CloseViewWindow method was called
            Assert.IsFalse(windowService.IsOpen, "Failed to close the view window.");
        }

        /// <summary>
        /// A test for SaveCommand using a folder that was saved in db.
        /// </summary>
        [TestMethod]
        public void SaveExistentFolderTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            ProjectFolderViewModel projectFolderViewModel = new ProjectFolderViewModel(messenger, windowService);

            ProjectFolder parentFolder = new ProjectFolder();
            parentFolder.Name = parentFolder.Guid.ToString();

            ProjectFolder childFolder = new ProjectFolder();
            childFolder.Name = childFolder.Guid.ToString();
            parentFolder.ChildrenProjectFolders.Add(childFolder);

            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            parentFolder.SetOwner(user);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectFolderRepository.Add(parentFolder);
            dataContext.SaveChanges();

            bool notificationSended = false;
            messenger.Register<EntityChangedMessage>((msg) =>
            {
                notificationSended = true;
            });

            // Create a folder which will be the new parent of updated folder
            ProjectFolder newParentFolder = new ProjectFolder();
            newParentFolder.Name = newParentFolder.Guid.ToString();
            dataContext.ProjectFolderRepository.Add(newParentFolder);
            dataContext.SaveChanges();

            // Set up the projectFolderViewModel
            projectFolderViewModel.DataSourceManager = dataContext;
            projectFolderViewModel.Model = childFolder;
            projectFolderViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            windowService.IsOpen = true;
            ICommand saveCommand = projectFolderViewModel.SaveCommand;
            saveCommand.Execute(null);

            // TO DO -> Verify that the model was updated
            // Verify that a notification has been sent 
            Assert.IsTrue(notificationSended, "Failed to send a notification about the folder changing.");

            // Verify that the CloseViewWindow method was called
            Assert.IsFalse(windowService.IsOpen, "Failed to close the view window.");
        }

        /// <summary>
        /// A test for SaveCommand using a null change set as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SaveGivenNullDataContextTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            ProjectFolderViewModel projectFolderViewModel = new ProjectFolderViewModel(messenger, windowService);

            projectFolderViewModel.DataSourceManager = null;
            ICommand saveCommand = projectFolderViewModel.SaveCommand;
            saveCommand.Execute(null);
        }

        ///// <summary>
        ///// A test for SaveCommand using an invalid folder(folder with null name) as input data.
        ///// </summary>

        //[TestMethod]
        //public void SaveInvalidFolderTest()
        //{
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    IMessenger messenger = new Messenger();
        //    ProjectFolderViewModel projectFolderViewModel = new ProjectFolderViewModel(messenger, windowService);

        //    bool notificationSended = false;
        //    messenger.Register<EntityChangedMessage>((msg) =>
        //    {
        //        notificationSended = true;
        //    });

        //    // Set up the projectFolderViewModel
        //    projectFolderViewModel.ChangeSet = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    projectFolderViewModel.FolderId = Guid.Empty;

        //    windowService.IsOpen = true;
        //    messageBoxService.ShowCallCount = 0;
        //    ICommand saveCommand = projectFolderViewModel.SaveCommand;
        //    saveCommand.Execute(null);

        //    // Verify that the notification wasn't sent
        //    Assert.IsFalse(notificationSended, "EntityChanged notification shouldn't be sent because the save operation couldn't be performed.");

        //    // Verify that the window is still open
        //    Assert.IsTrue(windowService.IsOpen, "The window shouldn't be closed if the save operation couldn't be performed.");

        //    // Verify that a message was displayed which informs the user about the invalid folder
        //    Assert.IsTrue(messageBoxService.ShowCallCount == 1, "Invalid number of calls of ShowMessageBox method.");
        //}

        /// <summary>
        /// A test for CancelCommand.
        /// </summary>
        [TestMethod]
        public void CancelTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            ProjectFolderViewModel projectFolderViewModel = new ProjectFolderViewModel(messenger, windowService);

            // Set up the projectFolderViewModel 
            string folderName = new Guid().ToString();
            projectFolderViewModel.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            projectFolderViewModel.InitializeForCreation(null);
            projectFolderViewModel.Name.Value = folderName;

            ICommand cancelCommand = projectFolderViewModel.CancelCommand;
            cancelCommand.Execute(null);

            bool changesCanceled = projectFolderViewModel.Name.Value != folderName;
            Assert.IsTrue(changesCanceled, "Failed to cancel the changes.");
        }

        #endregion Test Methods
    }
}
