﻿using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The Manage Settings view model test.
    /// </summary>
    [TestClass]
    public class ManageSettingsViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The Manage Settings View Model.
        /// </summary>
        private ManageSettingsViewModel manageSettingsViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageSettingsViewModelTest"/> class.
        /// </summary>
        public ManageSettingsViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.messageDialogService = new MessageDialogServiceMock();
            this.windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            this.manageSettingsViewModel = new ManageSettingsViewModel(this.windowService);
        }

        #region Test methods

        /// <summary>
        /// Manages the settings with success test.
        /// </summary>
        [TestMethod]
        public void EditSettingsWithSuccessTest()
        {
            this.manageSettingsViewModel = new ManageSettingsViewModel(this.windowService);
            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = true;
            this.manageSettingsViewModel.NumberOfPreviousPasswords = 4;

            ICommand saveCommand = this.manageSettingsViewModel.SaveCommand;
            saveCommand.Execute(null);

            var globalSettings = this.dataManager.GlobalSettingsRepository.GetAllGlobalSettings();
            string numberOfPrerviousPasswords = null;
            foreach (var setting in globalSettings)
            {
                if (setting.Key == "NumberOfPreviousPasswordsToCheck")
                {
                    numberOfPrerviousPasswords = setting.Value;
                }
            }

            Assert.AreEqual(numberOfPrerviousPasswords, this.manageSettingsViewModel.NumberOfPreviousPasswords.ToString());
        }

        /// <summary>
        /// Manages the settings with success test.
        /// </summary>
        [TestMethod]
        public void EditSettingsWithSuccessTest1()
        {
            this.manageSettingsViewModel = new ManageSettingsViewModel(this.windowService);
            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = true;
            this.manageSettingsViewModel.NumberOfPreviousPasswords = 4;

            ICommand saveCommand = this.manageSettingsViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = false;
            ICommand saveCommand1 = this.manageSettingsViewModel.SaveCommand;
            saveCommand1.Execute(null);

            Assert.IsNull(this.manageSettingsViewModel.NumberOfPreviousPasswords);

            var globalSettings = this.dataManager.GlobalSettingsRepository.GetAllGlobalSettings();
            string numberOfPrerviousPasswordsDB = null;
            foreach (var setting in globalSettings)
            {
                if (setting.Key == "NumberOfPreviousPasswordsToCheck")
                {
                    numberOfPrerviousPasswordsDB = setting.Value;
                }
            }

            Assert.AreEqual(numberOfPrerviousPasswordsDB, "0");
        }

        /// <summary>
        /// This method tests undo button.
        /// </summary>
        [TestMethod]
        public void UndoButtonTest()
        {
            this.manageSettingsViewModel = new ManageSettingsViewModel(this.windowService);
            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = true;
            this.manageSettingsViewModel.NumberOfPreviousPasswords = 4;

            ICommand saveCommand = this.manageSettingsViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = false;
            ICommand undoCommand = this.manageSettingsViewModel.UndoCommand;
            undoCommand.Execute(null);

            Assert.AreEqual(this.manageSettingsViewModel.NumberOfPreviousPasswords, 4);
        }

        /// <summary>
        /// This method tests cancel button.
        /// </summary>
        [TestMethod]
        public void CancelButtonTest()
        {
            this.manageSettingsViewModel = new ManageSettingsViewModel(this.windowService);
            ((MessageDialogServiceMock)this.messageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = false;

            ICommand saveCommand = this.manageSettingsViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords = true;
            this.manageSettingsViewModel.NumberOfPreviousPasswords = 4;

            ICommand cancelCommand = this.manageSettingsViewModel.CancelCommand;
            cancelCommand.Execute(null);

            Assert.IsFalse(this.manageSettingsViewModel.ProhibitReuseOfPreviousPasswords);
            Assert.IsNull(this.manageSettingsViewModel.NumberOfPreviousPasswords);
        }

        #endregion
    }
}
