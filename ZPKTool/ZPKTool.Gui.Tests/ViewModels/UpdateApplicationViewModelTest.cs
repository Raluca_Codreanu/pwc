﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Updater;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for UpdateApplicationViewModel and is intended 
    /// to contain unit tests for all UpdateApplicationViewModel commands
    /// </summary>
    [TestClass]
    public class UpdateApplicationViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "UpdateApplicationViewModelTest"/> class.
        /// </summary>
        public UpdateApplicationViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests CheckForUpdate command using valid input parameters ->the update folder contains a version newer than current version
        /// </summary>
        [TestMethod]
        public void CheckForUpdateTest1()
        {
            // Creates the structure of the update directory containing the Update.xml file and the new installer
            string updatePath = Path.GetTempPath() + "ZpkToolUpdate" + DateTime.Now.Ticks;
            string sharedFolderPath = "ShareZpkTool" + DateTime.Now.Ticks;
            string updateXmlPath = updatePath + "\\Update.xml";
            string installerPath = updatePath + "\\PCMSetup.exe";
            if (Directory.Exists(updatePath))
            {
                Utils.DeleteDirectory(updatePath);
            }

            DirectoryInfo directory = System.IO.Directory.CreateDirectory(updatePath);
            XmlDocument updateXmlDoc = CreateUpdateXMLFile();
            XmlNode versionNode = updateXmlDoc.GetElementsByTagName("version").Item(0);
            versionNode.InnerText = "2.0.18.0";
            XmlNode setupSourceNode = updateXmlDoc.GetElementsByTagName("setupsource").Item(0);
            setupSourceNode.InnerText = "PCMSetup.exe";
            XmlNode setupStartFileNode = updateXmlDoc.GetElementsByTagName("setupstartfile").Item(0);
            setupStartFileNode.InnerText = "PCMSetup.exe";
            XmlNode featureNode = updateXmlDoc.GetElementsByTagName("features").Item(0).FirstChild;
            featureNode.InnerText = EncryptionManager.Instance.GenerateRandomString(10, true);
            XmlNode fixNode = updateXmlDoc.GetElementsByTagName("fixes").Item(0).FirstChild;
            fixNode.InnerText = EncryptionManager.Instance.GenerateRandomString(10, true);
            updateXmlDoc.Save(updateXmlPath);
            FileStream fileStream = File.Create(installerPath);
            fileStream.Close();

            var typeCatalog = new TypeCatalog(
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(WindowServiceMock),
                typeof(ShellController));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);
            CompositionContainer container = bootstrapper.CompositionContainer;

            // Set the update path to a valid path and execute CheckForUpdate command
            GlobalSettingsManager.Instance.AutomaticUpdatePath = ShareFolder(updatePath, sharedFolderPath, string.Empty);
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowSevice = new WindowServiceMock(messageBoxService, dispatcherService);
            UpdateApplicationViewModel updateApplicationViewModel = new UpdateApplicationViewModel(container, windowSevice);
            ICommand checkForUpdateCommand = updateApplicationViewModel.CheckForUpdate;
            checkForUpdateCommand.Execute(null);

            // Check for updates is executed asynchronous => wait until the thread is executed in order to obtain the results
            while (updateApplicationViewModel.StatusMessage.Contains(LocalizedResources.Update_CheckingForUpdates))
            {
                Thread.Sleep(1000);
            }

            try
            {
                Assert.IsTrue(updateApplicationViewModel.StatusMessage.Contains(versionNode.InnerText), "Failed to found the new version");
                Assert.IsTrue(updateApplicationViewModel.EnableDownload, "Failed to enabled the download of the new version");
                Assert.IsFalse(string.IsNullOrEmpty(updateApplicationViewModel.ReleaseNotesMessage), "Failed to display the fixes and the features.");
            }
            finally
            {
                // Delete the created files
                if (Directory.Exists(updatePath))
                {
                    UnshareFolder(updatePath);
                    Utils.DeleteDirectory(updatePath);
                }
            }
        }

        /// <summary>
        /// Tests CheckForUpdate command using valid input data ->the update folder contains a version older than the current version
        /// </summary>
        [TestMethod]
        public void CheckForUpdateTest2()
        {
            // Creates the structure of the update directory containing the Update.xml file and an older installer
            string updatePath = Path.GetTempPath() + "ZpkToolUpdate" + DateTime.Now.Ticks;
            string sharedFolderPath = "ShareZpkTool" + DateTime.Now.Ticks;
            string updateXmlPath = updatePath + "\\Update.xml";
            string installerPath = updatePath + "\\PCMSetup.exe";
            if (Directory.Exists(updatePath))
            {
                Utils.DeleteDirectory(updatePath);
            }

            DirectoryInfo directory = System.IO.Directory.CreateDirectory(updatePath);
            XmlDocument updateXmlDoc = CreateUpdateXMLFile();
            XmlNode versionNode = updateXmlDoc.GetElementsByTagName("version").Item(0);
            versionNode.InnerText = "1.0.0.0";
            XmlNode setupSourceNode = updateXmlDoc.GetElementsByTagName("setupsource").Item(0);
            setupSourceNode.InnerText = "PCMSetup.exe";
            XmlNode setupStartFileNode = updateXmlDoc.GetElementsByTagName("setupstartfile").Item(0);
            setupStartFileNode.InnerText = "PCMSetup.exe";
            updateXmlDoc.Save(updateXmlPath);
            FileStream fileStream = File.Create(installerPath);
            fileStream.Close();

            var typeCatalog = new TypeCatalog(
                                    typeof(CompositionContainer),
                                    typeof(DispatcherServiceMock),
                                    typeof(MessageDialogServiceMock),
                                    typeof(Messenger),
                                    typeof(ModelBrowserHelperServiceMock),
                                    typeof(WindowServiceMock),
                                    typeof(ShellController));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);
            CompositionContainer container = bootstrapper.CompositionContainer;

            // Set the update path to a valid path and execute CheckForUpdate command
            GlobalSettingsManager.Instance.AutomaticUpdatePath = ShareFolder(updatePath, sharedFolderPath, string.Empty);
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock wndSevice = new WindowServiceMock(messageBoxService, dispatcherService);
            UpdateApplicationViewModel updateApplicationViewModel = new UpdateApplicationViewModel(container, wndSevice);
            ICommand checkForUpdateCommand = updateApplicationViewModel.CheckForUpdate;
            checkForUpdateCommand.Execute(null);

            // Check for updates is executed asynchronous => wait until the thread is executed in order to obtain the results
            while (updateApplicationViewModel.StatusMessage.Contains(LocalizedResources.Update_CheckingForUpdates))
            {
                Thread.Sleep(500);
            }

            try
            {
                Assert.AreEqual<string>(LocalizedResources.Update_VersionUpToDate, updateApplicationViewModel.StatusMessage, "The user was not informed that the application is up to date");
                Assert.IsFalse(updateApplicationViewModel.EnableDownload, "The application is up to date => download shouldn't be enabled");
            }
            finally
            {
                if (Directory.Exists(updatePath))
                {
                    UnshareFolder(updatePath);
                    Utils.DeleteDirectory(updatePath);
                }
            }
        }

        /// <summary>
        /// Tests CheckForUpdate command using invalid input data -> an empty path.
        /// </summary>
        [TestMethod]
        public void CheckForUpdateTest3()
        {
            var typeCatalog = new TypeCatalog(
                                    typeof(CompositionContainer),
                                    typeof(DispatcherServiceMock),
                                    typeof(MessageDialogServiceMock),
                                    typeof(Messenger),
                                    typeof(ModelBrowserHelperServiceMock),
                                    typeof(WindowServiceMock),
                                    typeof(ShellController));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);
            CompositionContainer container = bootstrapper.CompositionContainer;

            GlobalSettingsManager.Instance.AutomaticUpdatePath = string.Empty;
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock wndSevice = new WindowServiceMock(messageBoxService, dispatcherService);
            UpdateApplicationViewModel updateApplicationViewModel = new UpdateApplicationViewModel(container, wndSevice);

            ICommand checkForUpdateCommand = updateApplicationViewModel.CheckForUpdate;
            checkForUpdateCommand.Execute(null);

            // Check for updates is executed asynchronous => wait until the thread is executed in order to obtain the results
            while (updateApplicationViewModel.StatusMessage.Contains(LocalizedResources.Update_CheckingForUpdates))
            {
                Thread.Sleep(500);
            }

            // Verifies that the application download is disabled and that the user was informed about the reason
            Assert.IsFalse(updateApplicationViewModel.EnableDownload, "The update path is invalid => download shouldn't be enabled");
        }

        /// <summary>
        /// A test for CheckForUpdate command. The following input data were used:
        /// XML Updater having a wrong structure.
        /// </summary>
        [TestMethod]
        public void CheckForUpdateTest4()
        {
            var typeCatalog = new TypeCatalog(
                                       typeof(CompositionContainer),
                                       typeof(DispatcherServiceMock),
                                       typeof(MessageDialogServiceMock),
                                       typeof(Messenger),
                                       typeof(ModelBrowserHelperServiceMock),
                                       typeof(WindowServiceMock),
                                       typeof(ShellController));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);
            CompositionContainer container = bootstrapper.CompositionContainer;

            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            IWindowService wndSevice = new WindowServiceMock(messageBoxService, dispatcherService);
            UpdateApplicationViewModel updateApplicationViewModel = new UpdateApplicationViewModel(container, wndSevice);

            // Creates a shared folder and the additional files Update.xml(having a wrong structure) and the installer
            string updatePath = Path.GetTempPath() + "ZpkToolUpdate" + DateTime.Now.Ticks;
            string sharedFolderPath = "ShareZpkTool" + DateTime.Now.Ticks;
            string updateXmlPath = updatePath + "\\Update.xml";
            string installerPath = updatePath + "\\PCMSetup.exe";
            if (Directory.Exists(updatePath))
            {
                Utils.DeleteDirectory(updatePath);
            }

            DirectoryInfo directory = System.IO.Directory.CreateDirectory(updatePath);
            GlobalSettingsManager.Instance.AutomaticUpdatePath = ShareFolder(updatePath, sharedFolderPath, string.Empty);
            XmlDocument updateXmlDoc = CreateUpdateXMLFile();
            updateXmlDoc.Save(updateXmlPath);
            ICommand checkForUpdateCommand = updateApplicationViewModel.CheckForUpdate;
            checkForUpdateCommand.Execute(null);

            // Check for updates is executed asynchronous => wait until the thread is executed in order to obtain the results
            while (updateApplicationViewModel.StatusMessage.Contains(LocalizedResources.Update_CheckingForUpdates))
            {
                Thread.Sleep(500);
            }

            try
            {
                // Verifies that the application download is disabled 
                Assert.IsFalse(updateApplicationViewModel.EnableDownload, "The update.xml file has a wrong structure => download shouldn't be enabled");
            }
            finally
            {
                if (Directory.Exists(updatePath))
                {
                    UnshareFolder(updatePath);
                    Utils.DeleteDirectory(updatePath);
                }
            }
        }

        /// <summary>
        /// A test for CloseScreenCommand. 
        /// </summary>
        [TestMethod]
        public void CloseWindowTest()
        {
            var typeCatalog = new TypeCatalog(
                                    typeof(CompositionContainer),
                                    typeof(DispatcherServiceMock),
                                    typeof(MessageDialogServiceMock),
                                    typeof(Messenger),
                                    typeof(ModelBrowserHelperServiceMock),
                                    typeof(WindowServiceMock),
                                    typeof(ShellController));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);
            CompositionContainer container = bootstrapper.CompositionContainer;

            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            UpdateApplicationViewModel updateApplicationViewModel = new UpdateApplicationViewModel(container, windowService);

            windowService.IsOpen = true;
            ICommand closeScreenCommand = updateApplicationViewModel.CloseScreen;
            closeScreenCommand.Execute(null);

            Assert.IsFalse(windowService.IsOpen, "Failed to close the screen.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Shares a folder
        /// </summary>
        /// <param name="folderPath">The folder's path</param>
        /// <param name="shareName">The share name</param>
        /// <param name="description">The description</param>
        /// <returns>The path of the shared folder</returns>
        private string ShareFolder(string folderPath, string shareName, string description)
        {
            // Create a ManagementClass object
            ManagementClass managementClass = new ManagementClass("Win32_Share");

            // Create ManagementBaseObjects for in and out parameters
            ManagementBaseObject inParams = managementClass.GetMethodParameters("Create");
            ManagementBaseObject outParams;

            // Set the input parameters
            inParams["Description"] = description;
            inParams["Name"] = shareName;
            inParams["Path"] = folderPath;
            inParams["Type"] = 0x0; // Disk Drive

            // Invoke the method on the ManagementClass object
            outParams = managementClass.InvokeMethod("Create", inParams, null);
            string sharePath = "\\\\" + Environment.MachineName + "\\" + shareName;

            return sharePath;
        }

        /// <summary>
        /// Unshared a folder.
        /// </summary>
        /// <param name="folderPath">The path of the folder to be unshared.</param>
        private void UnshareFolder(string folderPath)
        {
            // Create a ManagementClass object
            ManagementClass managementClass = new ManagementClass("Win32_Share");

            // Create ManagementBaseObjects for in and out parameters
            ManagementBaseObject outParams;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from win32_share");
            foreach (ManagementObject sharedObject in searcher.Get())
            {
                string type = sharedObject["Type"].ToString();

                // Disk Drive
                if (type == "0")
                {
                    // Getting share path
                    string path = sharedObject["Path"].ToString();

                    if (path == folderPath)
                    {
                        outParams = sharedObject.InvokeMethod("delete", null, null);
                    }
                }
            }
        }

        /// <summary>
        /// Creates an XML document having a specific structure.
        /// </summary>
        /// <returns>The created xml document</returns>
        private XmlDocument CreateUpdateXMLFile()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode updateNode = doc.CreateElement("UpdatePROImpens");
            XmlNode versionNode = doc.CreateElement("version");
            XmlNode featuresNode = doc.CreateElement("features");
            XmlNode featureNode = doc.CreateElement("feature");
            XmlNode fixesNode = doc.CreateElement("fixes");
            XmlNode fixNode = doc.CreateElement("fix");
            XmlNode setupSourceNode = doc.CreateElement("setupsource");
            XmlNode setupStartFileNode = doc.CreateElement("setupstartfile");

            doc.AppendChild(updateNode);
            updateNode.AppendChild(versionNode);
            updateNode.AppendChild(featuresNode);
            updateNode.AppendChild(fixesNode);
            featuresNode.AppendChild(featureNode);
            fixesNode.AppendChild(fixNode);
            updateNode.AppendChild(setupSourceNode);
            updateNode.AppendChild(setupStartFileNode);

            return doc;
        }

        #endregion Helpers
    }
}
