﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CompareToolViewModel and is intended 
    /// to contain unit tests for all public methods from CompareToolViewModel class.
    /// </summary>
    [TestClass]
    public class CompareToolViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "CompareToolViewModelTest"/> class.
        /// </summary>
        public CompareToolViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// A test for CompareCommand.
        /// </summary>
        [TestMethod]
        public void CompareEntitiesTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            CompareToolViewModel compareToolViewModel = new CompareToolViewModel(windowService, messenger);

            bool mainViewNotified = false;
            messenger.Register<NotificationMessage>((msg) =>
            {
                if (msg.Notification == Notification.DisplayCompareView)
                {
                    mainViewNotified = true;
                }
            });

            // Add entities for comparison 
            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            CompareToolItem item1 = new CompareToolItem(assembly1.Name, assembly1.Guid, typeof(Assembly), DbIdentifier.LocalDatabase);
            compareToolViewModel.CompareCollection.Add(item1);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            CompareToolItem item2 = new CompareToolItem(assembly2.Name, assembly2.Guid, typeof(Assembly), DbIdentifier.LocalDatabase);
            compareToolViewModel.CompareCollection.Add(item2);

            compareToolViewModel.IsShowedMessage = true;
            windowService.IsOpen = true;
            ICommand compareCommand = compareToolViewModel.CompareCommand;
            compareCommand.Execute(null);

            // Verify that the main view was notified to display the CompareView
            Assert.IsTrue(mainViewNotified, "Failed to notify the main view to display the CompareView.");

            // Verify that the CompareTool is closed
            Assert.IsFalse(windowService.IsOpen, "Failed to close the CompareTool.");

            // Verify that collection of compared entities was cleared and IsShowedMessage flag was set to false
            Assert.IsTrue(compareToolViewModel.CompareCollection.Count == 0, "Failed to clear the collection of compared entities.");
            Assert.IsFalse(compareToolViewModel.IsShowedMessage, "IsShowedMessage should be set to false when CompareCollection is empty.");
        }

        /// <summary>
        /// A test for RemoveItemCommand.
        /// </summary>
        [TestMethod]
        public void RemoveEntityTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            CompareToolViewModel compareToolViewModel = new CompareToolViewModel(windowService, messenger);

            // Add an item in the CompareCollection
            Part part = new Part();
            part.Name = part.Guid.ToString();
            CompareToolItem partItem = new CompareToolItem(part.Name, part.Guid, typeof(Part), DbIdentifier.LocalDatabase);
            compareToolViewModel.CompareCollection.Add(partItem);

            compareToolViewModel.IsShowedMessage = true;
            ICommand removeCommand = compareToolViewModel.RemoveItemCommand;
            removeCommand.Execute(partItem);

            // Verifies that the item was removed from the collection and IsShowedMessage flag was set to false
            Assert.IsFalse(compareToolViewModel.CompareCollection.Contains(partItem), "Failed to remove an item from CompareCollection.");
            Assert.IsFalse(compareToolViewModel.IsShowedMessage, "IsShowedMessage flag was not set properly.");
        }

        /// <summary>
        /// A test for the ClosePopupCommand.
        /// </summary>
        [TestMethod]
        public void ClosePopupTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IMessenger messenger = new Messenger();
            CompareToolViewModel compareToolViewModel = new CompareToolViewModel(windowService, messenger);

            compareToolViewModel.IsShowedMessage = true;
            ICommand closePopupCommand = compareToolViewModel.ClosePopupCommand;
            closePopupCommand.Execute(null);

            // Verify that the popup was closed
            Assert.IsFalse(compareToolViewModel.IsShowedMessage, "Failed to close the popup.");
        }
    }
}
