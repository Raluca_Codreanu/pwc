﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business.TransportCostCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for TransportCostCalculationViewModel and is intended to contain all its Unit Tests.
    /// </summary>
    [TestClass]
    public class TransportCostCalculationViewModelTest
    {
        #region Attributes

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The please wait service.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationViewModelTest"/> class.
        /// </summary>
        public TransportCostCalculationViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        }

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();

            this.windowService = new WindowServiceMock(messageDialogService, dispatcherService);
            this.messenger = new Messenger();
            this.unitsService = new UnitsServiceMock();
            this.compositionContainer = new CompositionContainer();
            this.pleaseWaitService = new PleaseWaitService(dispatcherService);
        }

        #endregion

        #region Test methods

        /// <summary>
        /// This is a test for the initialization of some properties for a new added calculation.
        /// The fields related to carrier, transporter and part type should be enabled by default; also, the popup for settings must be closed.
        /// </summary>
        [TestMethod]
        public void InitializePropertiesTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;

            Assert.IsTrue(calculationViewModel.IsEnabledForManualCarrierType.Value, "The field related to carrier type are not enabled.");
            Assert.IsTrue(calculationViewModel.IsEnabledForManualTransporterType.Value, "The field related to transporter type are not enabled.");
            Assert.IsTrue(calculationViewModel.IsEnabledForPartType.Value, "The field related to part type are not enabled.");
            Assert.IsFalse(calculationViewModel.IsSettingsPopupOpen, "The popup for settings is opened.");
        }

        /// <summary>
        /// This is a test for initialization, to check if the correct settings are set for settings view model.
        /// </summary>
        [TestMethod]
        public void InitializeSettingsViewModelTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;

            Assert.AreEqual(calculationViewModel.SettingViewModel.Model.Guid, setting.Guid, "The model for setting view model in wrong.");
        }

        /// <summary>
        /// This is a test for handling the carrier type changed; 
        /// The values for the carrier id 2 are compared with the expected ones from Business - TransportCostCalculator and the fields must be disabled.
        /// </summary>
        [TestMethod]
        public void SelectedCarrierChangedNotManualTypeTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.CarrierId.Value = 2;

            Assert.AreEqual(800m, calculationViewModel.CarrierLength.Value, "The value for carrier length is incorrect.");
            Assert.AreEqual(500m, calculationViewModel.CarrierHeight.Value, "The value for carrier height is incorrect.");
            Assert.AreEqual(1200m, calculationViewModel.CarrierWidth.Value, "The value for carrier width is incorrect.");
            Assert.AreEqual(55m, calculationViewModel.NetWeight.Value, "The value for net weight is incorrect.");
            Assert.IsFalse(calculationViewModel.IsEnabledForManualCarrierType.Value, "The fields related to carrier type are enabled.");
        }

        /// <summary>
        /// This is a test for handling the carrier type changed; 
        /// The values for the carrier id 0 should be the default ones for manual type: 0 and the fields must be enabled.
        /// </summary>
        [TestMethod]
        public void SelectedCarrierChangedManualTypeTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.CarrierId.Value = 1;
            calculationViewModel.CarrierId.Value = 0;

            Assert.AreEqual(0m, calculationViewModel.CarrierLength.Value, "The value for carrier length is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.CarrierHeight.Value, "The value for carrier height is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.CarrierWidth.Value, "The value for carrier width is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.NetWeight.Value, "The value for net weight is incorrect.");
            Assert.IsTrue(calculationViewModel.IsEnabledForManualCarrierType.Value, "The fields related to carrier type are disabled.");
        }

        /// <summary>
        /// This is a test for handling the transporter type changed; 
        /// The values for the carrier id 3 are compared with the expected ones from Business - TransportCostCalculator and the fields must be disabled.
        /// </summary>
        [TestMethod]
        public void SelectedTransporterChangedNotManualTypeTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.TransporterId.Value = 3;

            Assert.AreEqual(7200m, calculationViewModel.TransporterLength.Value, "The value for transporter length is incorrect.");
            Assert.AreEqual(2480m, calculationViewModel.TransporterWidth.Value, "The value for transporter width is incorrect.");
            Assert.AreEqual(2500m, calculationViewModel.TransporterHeight.Value, "The value for transporter width is incorrect.");
            Assert.AreEqual(7m, calculationViewModel.MaxLoadCapacity.Value, "The value for max load capacity is incorrect.");
            Assert.IsFalse(calculationViewModel.IsEnabledForManualTransporterType.Value, "The fields related to transporter type are enabled.");
        }

        /// <summary>
        /// This is a test for handling the transporter type changed; 
        /// The values for the transporter id 0 should be the default ones for manual type: 0 and the fields must be enabled.
        /// </summary>
        [TestMethod]
        public void SelectedTransporterChangedManualTypeTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.TransporterId.Value = 2;
            calculationViewModel.TransporterId.Value = 0;

            Assert.AreEqual(0m, calculationViewModel.TransporterLength.Value, "The value for transporter length is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.TransporterWidth.Value, "The value for transporter width is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.TransporterHeight.Value, "The value for transporter width is incorrect.");
            Assert.AreEqual(0m, calculationViewModel.MaxLoadCapacity.Value, "The value for max load capacity is incorrect.");
            Assert.IsTrue(calculationViewModel.IsEnabledForManualTransporterType.Value, "The fields related to transporter type are disabled.");
        }

        /// <summary>
        /// This is a test for handling the part type changed, when selected type is single component; 
        /// The interference value should be null and disabled, and packing density 0 and enabled.
        /// </summary>
        [TestMethod]
        public void SelectedPartTypeSingleComponentTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.SelectedPartType.Value = PartType.BulkGoods;
            calculationViewModel.SelectedPartType.Value = PartType.SingleComponent;

            Assert.IsNull(calculationViewModel.Interference.Value, "The interference is not set to null.");
            Assert.AreEqual(0m, calculationViewModel.PackingDensity.Value, "The packing density is not set to default value 0.");
            Assert.IsTrue(calculationViewModel.IsEnabledForPartType.Value, "The wrong field is enabled.");
        }

        /// <summary>
        /// This is a test for handling the part type changed, when selected type is bulk goods; 
        /// The packing density value should be null and disabled, and interference 0 and enabled.
        /// </summary>
        [TestMethod]
        public void SelectedPartTypeBulkGoodsTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.SelectedPartType.Value = PartType.BulkGoods;

            Assert.IsNull(calculationViewModel.PackingDensity.Value, "The packing density is not set to null.");
            Assert.AreEqual(0m, calculationViewModel.Interference.Value, "The interference is not set to default value 0.");
            Assert.IsFalse(calculationViewModel.IsEnabledForPartType.Value, "The wrong field is enabled.");
        }

        /// <summary>
        /// This is a test for BrowseMasterData command, to check if the supplier browser window is not opened when the calculation has no country set.
        /// </summary>
        [TestMethod]
        public void BrowseMasterDataWithoutExistingCountryTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            this.windowService.IsOpen = false;
            calculationViewModel.BrowseMasterDataCommand.Execute(null);

            Assert.IsFalse(windowService.IsOpen, "The supplier browser window was opened although no country is set.");
        }

        /// <summary>
        /// This is a test for the CalculateDistanceCommand, to check if the command can be executed when there are empty coordinates.
        /// </summary>
        [TestMethod]
        public void CanCalculateDrivingDistanceForEmptyCoordinatesTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            calculationViewModel.DestinationLatitude.Value = 54.7788m;
            calculationViewModel.DestinationLongitude.Value = 66.2345m;

            Assert.IsFalse(calculationViewModel.CalculateDistanceCommand.CanExecute(null), "The distance command can execute, although there are empty fields.");
        }

        /// <summary>
        /// This is a test for the CalculateDistanceCommand, to check if the command can be executed when the coordinates are invalid.
        /// It also checks that the range validation is done and the error messages are set.
        /// </summary>
        [TestMethod]
        public void CanCalculateDrivingDistanceForInvalidCoordinatesTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            calculationViewModel.SourceLatitude.Value = 555m;
            calculationViewModel.SourceLongitude.Value = 876.9m;
            calculationViewModel.DestinationLatitude.Value = 1111.99m;
            calculationViewModel.DestinationLongitude.Value = 5466.7m;

            Assert.IsFalse(calculationViewModel.CalculateDistanceCommand.CanExecute(null), "The distance command can execute, although the fields have errors.");

            bool sourceLatHasError = !string.IsNullOrWhiteSpace(calculationViewModel.SourceLatitude.Error);
            bool sourceLngHasError = !string.IsNullOrWhiteSpace(calculationViewModel.SourceLongitude.Error);
            bool destLatHasError = !string.IsNullOrWhiteSpace(calculationViewModel.DestinationLatitude.Error);
            bool destLngHasError = !string.IsNullOrWhiteSpace(calculationViewModel.DestinationLongitude.Error);

            Assert.IsTrue(sourceLatHasError, "The source latitude has no error message.");
            Assert.IsTrue(sourceLngHasError, "The source longitude has no error message.");
            Assert.IsTrue(destLatHasError, "The destination latitude has no error message.");
            Assert.IsTrue(destLngHasError, "The destination longitude has no error message.");
        }

        /// <summary>
        /// This is a test for the CalculateDistanceCommand, to check if the command can be executed when all coordinates have values.
        /// </summary>
        [TestMethod]
        public void CanCalculateDrivingDistanceForValidCoordinatesTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            calculationViewModel.SourceLatitude.Value = 89.4322m;
            calculationViewModel.SourceLongitude.Value = 15.7777m;
            calculationViewModel.DestinationLatitude.Value = 54.7788m;
            calculationViewModel.DestinationLongitude.Value = 66.2345m;

            Assert.IsTrue(calculationViewModel.CalculateDistanceCommand.CanExecute(null), "The distance command cannot be executed, although there are no empty fields.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if the command can be executed when there are errors in the settings view model.
        /// </summary>
        [TestMethod]
        public void CanSaveWithErrorsInSettingsVMTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.SettingViewModel.Error = "Test errors";

            Assert.IsFalse(calculationViewModel.SaveCommand.CanExecute(null), "The save method can be executed, even if errors exist.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if a new calculation is added into the repository.
        /// </summary>
        [TestMethod]
        public void SaveCommandTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.DataSourceManager = this.dataManager;

            calculationViewModel.SourceCountry.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.SourceLocation.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.SourceZipCode.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationCountry.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationLocation.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationZipCode.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.MaxLoadCapacity.Value = 5m;
            var calculationGuid = calculationViewModel.Model.Guid;

            calculationViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(this.dataManager.TransportCostCalculationRepository.GetByKey(calculationGuid), "The calculation was not saved.");
        }

        /// <summary>
        /// This is a test for closing of settings popup, to check if the values from settings view model are saved when the popup is closed and there are no errors.
        /// It also tests the method SetTransporterCostPerKmValue, to check if the selected transporter has the correct cost per km from settings.
        /// </summary>
        [TestMethod]
        public void HandleSettingsPopupClosedWithoutErrorsTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.DataSourceManager = this.dataManager;
            calculationViewModel.SettingViewModel.DataSourceManager = this.dataManager;
            calculationViewModel.TransporterId.Value = 2;

            // Open the settings popup and set some new values.
            calculationViewModel.IsSettingsPopupOpen = true;
            calculationViewModel.SettingViewModel.AverageLoadTarget.Value = 30m;
            calculationViewModel.SettingViewModel.MegaTrailerCost.Value = 250m;

            calculationViewModel.SettingViewModel.CloseSettingsPopupCommand.Execute(null);

            Assert.AreEqual(250m, calculationViewModel.TransporterCostPerKm.Value, "The transporter cost was not saved into settings.");
            Assert.AreEqual(30m, calculationViewModel.SettingViewModel.AverageLoadTarget.Value, "The average load target was not saved into settings.");
        }

        /// <summary>
        /// This is a test for closing of settings popup, when a required field is set to null and the calculation VM error must contain the error from settings VM.
        /// It also tests the method HandleSettingsViewModelsError, to check if the error message is properly formatted.
        /// </summary>
        [TestMethod]
        public void HandleSettingsPopupClosedWithErrorsTest()
        {
            TransportCostCalculationViewModel calculationViewModel = new TransportCostCalculationViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting setting = new TransportCostCalculationSetting();
            calculation.TransportCostCalculationSetting = setting;

            calculationViewModel.Model = calculation;
            calculationViewModel.DataSourceManager = this.dataManager;
            calculationViewModel.SettingViewModel.DataSourceManager = this.dataManager;

            // Open the settings popup and set the mega trailer cost to null.
            calculationViewModel.IsSettingsPopupOpen = true;
            calculationViewModel.SettingViewModel.AverageLoadTarget.Value = 30m;
            calculationViewModel.SettingViewModel.MegaTrailerCost.Value = null;

            calculationViewModel.SettingViewModel.CloseSettingsPopupCommand.Execute(null);

            bool containsErrorMessage1 = calculationViewModel.Error.Contains(string.Format("- {0}", LocalizedResources.RequiredField_MegaTrailerCost));

            // Fill the required values from calculation, so that the calculation VM will have no errors.
            calculationViewModel.SourceCountry.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.SourceLocation.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.SourceZipCode.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationCountry.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationLocation.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.DestinationZipCode.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationViewModel.MaxLoadCapacity.Value = 5m;

            bool containsErrorMessage2 = calculationViewModel.Error == LocalizedResources.RequiredField_MegaTrailerCost;

            Assert.IsTrue(containsErrorMessage1, "The calculation view model error does not contain the correct errors from settings view model.");
            Assert.IsTrue(containsErrorMessage2, "The calculation view model error does not contain the correct errors from settings view model.");
        }

        #endregion
    }
}
