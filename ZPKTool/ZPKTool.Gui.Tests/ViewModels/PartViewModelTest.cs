﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is the class for testing the PartViewModel
    /// </summary>
    [TestClass]
    public class PartViewModelTest
    {
        #region Attributes
        /// <summary>
        /// The test context instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The manufacturer service.
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The messenger
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The Cost recalculation Clone Manager.
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The Entity Documents View Model
        /// </summary>
        private EntityDocumentsViewModel documentsViewModel;

        /// <summary>
        /// The Overhead Settings View Model
        /// </summary>
        private OverheadSettingsViewModel overheadSettings;

        /// <summary>
        /// The Country Settings View Model
        /// </summary>
        private CountrySettingsViewModel countrySettingsViewModel;

        /// <summary>
        /// The Country and Suppplier Browser View Model
        /// </summary>
        private CountryAndSupplierBrowserViewModel countryBrowser;

        /// <summary>
        /// The Master Data Browser View Model
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The media view-model.
        /// </summary>
        private MediaViewModel media;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate the test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();
            var dialog = new FileDialogServiceMock();
            this.unitsService = new UnitsServiceMock();
            this.documentsViewModel = new EntityDocumentsViewModel(this.windowService, dialog, this.modelBrowserHelperService);
            this.countrySettingsViewModel = new CountrySettingsViewModel(this.unitsService);
            this.countryBrowser = new CountryAndSupplierBrowserViewModel(this.windowService);
            this.masterDataBrowser = new MasterDataBrowserViewModel(
                new CompositionContainer(),
                this.messenger,
                this.windowService,
                new OnlineCheckService(this.messenger),
                this.unitsService);
            var videoViewModel = new VideoViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            this.media = new MediaViewModel(
                videoViewModel,
                picturesViewModel,
                this.messenger,
                this.windowService,
                new FileDialogServiceMock(),
                new ModelBrowserHelperServiceMock());
            var container = new CompositionContainer();
            this.manufacturer = new ManufacturerViewModel(this.windowService, this.messenger, container);
            this.overheadSettings = new OverheadSettingsViewModel(this.messenger, this.windowService, this.unitsService);
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region TestMethods

        /// <summary>
        /// This method tests the editable fields of Part General tab for Fine Calculation Accuracy. 
        /// Save command and update calculation variant button are also tested 
        /// </summary>
        [TestMethod]
        public void GeneralTabFineCalculationTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            DateTime dt = new DateTime(2004, 09, 30);
            var description = EncryptionManager.Instance.GenerateRandomString(10, true);

            partVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.Number.Value = "321";
            partVm.Version.Value = 22;
            partVm.VersionDate.Value = dt;
            partVm.Description.Value = description;
            partVm.ManufacturingCountry.Value = "Chile";
            partVm.ManufacturingSupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.CalculationStatus.Value = PartCalculationStatus.InWork;
            partVm.CalculationAccuracy.Value = PartCalculationAccuracy.FineCalculation;
            partVm.CalculationApproach.Value = PartCalculationApproach.Greenfield;
            partVm.CalculationVariant.Value = "1.3";
            partVm.UpdateToLatestCalculationVariantCommand.Execute(null);
            partVm.Calculator.Value = partVm.Users.Value.First(u => u.Guid == newUser.Guid);
            partVm.BatchSize.Value = 222;
            partVm.YearlyProductionQuantity.Value = 100;
            partVm.LifeTime.Value = 200;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Name, partVm.Name.Value);
            Assert.AreEqual(part.Number, partVm.Number.Value);
            Assert.AreEqual(part.Version, partVm.Version.Value);
            Assert.AreEqual(part.VersionDate, partVm.VersionDate.Value);
            Assert.AreEqual(part.Description, partVm.Description.Value);
            Assert.AreEqual(part.ManufacturingCountry, partVm.ManufacturingCountry.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationStatus, partVm.CalculationStatus.Value);
            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);
            Assert.AreEqual(part.CalculationAccuracy, partVm.CalculationAccuracy.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
            Assert.AreEqual(part.CalculatorUser, partVm.Calculator.Value);
            Assert.AreEqual(part.BatchSizePerYear, partVm.BatchSize.Value);
            Assert.AreEqual(part.YearlyProductionQuantity, partVm.YearlyProductionQuantity.Value);
            Assert.AreEqual(part.LifeTime, partVm.LifeTime.Value);
        }

        /// <summary>
        /// This method tests the editable fields of Part General tab for Fine Calculation Accuracy. 
        /// Save command and update calculation variant button are also tested 
        /// </summary>
        [TestMethod]
        public void GeneralTabFineCalculationTest2()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            DateTime dt = new DateTime(2004, 09, 30);
            var description = EncryptionManager.Instance.GenerateRandomString(10, true);

            partVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.Number.Value = "321";
            partVm.Version.Value = 9;
            partVm.VersionDate.Value = dt;
            partVm.Description.Value = description;
            partVm.ManufacturingCountry.Value = "Chile";
            partVm.ManufacturingSupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.CalculationStatus.Value = PartCalculationStatus.InWork;
            partVm.CalculationAccuracy.Value = PartCalculationAccuracy.FineCalculation;
            partVm.CalculationApproach.Value = PartCalculationApproach.Greenfield;
            partVm.CalculationVariant.Value = "1.4";
            partVm.UpdateToLatestCalculationVariantCommand.Execute(null);
            partVm.Calculator.Value = partVm.Users.Value.First(u => u.Guid == newUser.Guid);
            partVm.BatchSize.Value = 222;
            partVm.YearlyProductionQuantity.Value = 100;
            partVm.LifeTime.Value = 200;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Name, partVm.Name.Value);
            Assert.AreEqual(part.Number, partVm.Number.Value);
            Assert.AreEqual(part.Version, partVm.Version.Value);
            Assert.AreEqual(part.VersionDate, partVm.VersionDate.Value);
            Assert.AreEqual(part.Description, partVm.Description.Value);
            Assert.AreEqual(part.ManufacturingCountry, partVm.ManufacturingCountry.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationStatus, partVm.CalculationStatus.Value);
            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);
            Assert.AreEqual(part.CalculationAccuracy, partVm.CalculationAccuracy.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
            Assert.AreEqual(part.CalculatorUser, partVm.Calculator.Value);
            Assert.AreEqual(part.BatchSizePerYear, partVm.BatchSize.Value);
            Assert.AreEqual(part.YearlyProductionQuantity, partVm.YearlyProductionQuantity.Value);
            Assert.AreEqual(part.LifeTime, partVm.LifeTime.Value);
        }

        /// <summary>
        /// This method tests the editable fields of Part General tab for 
        /// Fine Calculation Accuracy, Calculation Status - Approved and calculation approach - Brownfield
        /// </summary>
        [TestMethod]
        public void GeneralTabRoughCalculationTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;
            var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);

            MeasurementUnit weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");

            partVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.ManufacturingCountry.Value = "Canada";
            partVm.ManufacturingSupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.CalculationStatus.Value = PartCalculationStatus.Approved;
            partVm.CalculationAccuracy.Value = PartCalculationAccuracy.RoughCalculation;
            partVm.CalculationApproach.Value = PartCalculationApproach.Brownfield;
            partVm.CalculationVariant.Value = "1.4";
            partVm.EstimatedCost.Value = 100;
            partVm.Weight.Value = 10;
            partVm.WeightUnit = weightUnit;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Name, partVm.Name.Value);
            Assert.AreEqual(part.ManufacturingCountry, partVm.ManufacturingCountry.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationStatus, partVm.CalculationStatus.Value);
            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);
            Assert.AreEqual(part.CalculationAccuracy, partVm.CalculationAccuracy.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
            Assert.AreEqual(part.EstimatedCost, partVm.EstimatedCost.Value);
            Assert.AreEqual(part.Weight, partVm.Weight.Value);
            Assert.AreEqual(part.MeasurementUnit, partVm.WeightUnit);
        }

        /// <summary>
        /// This method tests the editable fields of Part General tab for Estimation Accuracy, Calculation Status - Released and calculation approach - CustomerProcess
        /// </summary>
        [TestMethod]
        public void GeneralTabEstimationTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;
            var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);

            MeasurementUnit weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");

            partVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.ManufacturingCountry.Value = "Canada";
            partVm.ManufacturingSupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.CalculationStatus.Value = PartCalculationStatus.Released;
            partVm.CalculationAccuracy.Value = PartCalculationAccuracy.Estimation;
            partVm.CalculationApproach.Value = PartCalculationApproach.CustomerProcess;
            partVm.CalculationVariant.Value = "1.4";
            partVm.EstimatedCost.Value = 200;
            partVm.Weight.Value = 20;
            partVm.WeightUnit = weightUnit;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Name, partVm.Name.Value);
            Assert.AreEqual(part.ManufacturingCountry, partVm.ManufacturingCountry.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationStatus, partVm.CalculationStatus.Value);
            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);
            Assert.AreEqual(part.CalculationAccuracy, partVm.CalculationAccuracy.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
            Assert.AreEqual(part.EstimatedCost, partVm.EstimatedCost.Value);
            Assert.AreEqual(part.Weight, partVm.Weight.Value);
            Assert.AreEqual(part.MeasurementUnit, partVm.WeightUnit);
        }

        /// <summary>
        /// This method tests the editable fields of Part General tab for 
        /// OfferOrExternalCalculation Accuracy, Calculation Status - Released and calculation approach - CustomerProcess
        /// </summary>
        [TestMethod]
        public void GeneralTabOfferOrExternalCalculationTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            partVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.ManufacturingCountry.Value = "Canada";
            partVm.ManufacturingSupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.CalculationAccuracy.Value = PartCalculationAccuracy.OfferOrExternalCalculation;
            partVm.CalculationVariant.Value = "1.4";

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Name, partVm.Name.Value);
            Assert.AreEqual(part.ManufacturingCountry, partVm.ManufacturingCountry.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationAccuracy, partVm.CalculationAccuracy.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
        }

        /// <summary>
        /// This method tests the undo command. 
        /// </summary>
        [TestMethod]
        public void UndoCommandTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            const PartCalculationApproach Approach = PartCalculationApproach.Greenfield;
            partVm.CalculationApproach.Value = Approach;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);

            partVm.CalculationApproach.Value = PartCalculationApproach.Brownfield;

            var undoCommand = partVm.UndoCommand;
            undoCommand.Execute(null);
            Assert.AreEqual(partVm.CalculationApproach.Value, Approach);
        }

        /// <summary>
        /// This method tests the cancel command. 
        /// </summary>
        [TestMethod]
        public void CancelCommandTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            const PartCalculationApproach Approach = PartCalculationApproach.Greenfield;
            const int ValueLifetime = 5;
            partVm.CalculationApproach.Value = Approach;
            partVm.LifeTime.Value = ValueLifetime;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.CalculationApproach, partVm.CalculationApproach.Value);
            Assert.AreEqual(part.LifeTime, partVm.LifeTime.Value);

            partVm.CalculationApproach.Value = PartCalculationApproach.Brownfield;
            partVm.LifeTime.Value = 20;

            var cancelCommand = partVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(part.CalculationApproach, Approach);
            Assert.AreEqual(part.LifeTime, ValueLifetime);
        }

        /// <summary>
        /// This method tests the editable fields of Manufacturer tab 
        /// </summary>
        [TestMethod]
        public void ManufacturerTabTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            partVm.CalculationVariant.Value = "1.4";
            this.manufacturer.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.manufacturer.Description.Value = EncryptionManager.Instance.GenerateRandomString(20, true);

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.Manufacturer.Name, this.manufacturer.Name.Value);
            Assert.AreEqual(part.Manufacturer.Description, this.manufacturer.Description.Value);
            Assert.AreEqual(part.ManufacturingSupplier, partVm.ManufacturingSupplier.Value);
            Assert.AreEqual(part.CalculationVariant, "1.4");
        }

        /// <summary>
        /// This method tests the editable fields of Settings tab 
        /// </summary>
        [TestMethod]
        public void SettingsTabTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            partVm.CalculationVariant.Value = "1.4";
            partVm.PurchasePrice.Value = 100;
            partVm.DeliveryType.Value = PartDeliveryType.FOB;
            partVm.AssetRate.Value = 50;
            partVm.TargetPrice.Value = 100;
            partVm.ToolingActive.Value = true;
            partVm.IsExternal.Value = true;
            partVm.ExternalSGA.Value = true;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.PurchasePrice, partVm.PurchasePrice.Value);
            Assert.AreEqual(part.DelivertType, partVm.DeliveryType.Value);
            Assert.AreEqual(part.AssetRate, partVm.AssetRate.Value);
            Assert.AreEqual(part.TargetPrice, partVm.TargetPrice.Value);
            Assert.AreEqual(part.IsExternal, partVm.IsExternal.Value);
            Assert.AreEqual(part.ExternalSGA, partVm.ExternalSGA.Value);
        }

        /// <summary>
        /// This method tests the editable fields of Description tab 
        /// </summary>
        [TestMethod]
        public void DescriptionTabTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            partVm.CalculationVariant.Value = "1.4";
            partVm.AdditionalRemarks.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.AdditionalDescription.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            partVm.ManufacturerDescription.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.AdditionalRemarks, partVm.AdditionalRemarks.Value);
            Assert.AreEqual(part.AdditionalDescription, partVm.AdditionalDescription.Value);
            Assert.AreEqual(part.ManufacturerDescription, partVm.ManufacturerDescription.Value);
        }

        /// <summary>
        /// This method tests the editable fields of OverheadSettings tab 
        /// </summary>
        [TestMethod]
        public void OverheadSettingsTabTest()
        {
            var partVm = new PartViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.overheadSettings,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partVm.DataSourceManager = localDataManager;
            var part = this.CreateNewPart(localDataManager);
            partVm.Model = part;

            this.overheadSettings.MaterialOverhead.Value = 200;
            this.overheadSettings.CommodityOverhead.Value = 30;
            this.overheadSettings.ManufacturingOverhead.Value = 23;
            this.overheadSettings.PackagingOverhead.Value = 20;
            this.overheadSettings.SalesAndAdministrationOverhead.Value = 20;
            this.overheadSettings.ConsumableOverhead.Value = 30;
            this.overheadSettings.ExternalWorkOverhead.Value = 44;
            this.overheadSettings.OtherCostOverhead.Value = 1000;
            this.overheadSettings.LogisticOverhead.Value = 99;
            this.overheadSettings.CompanySurchargeOverhead.Value = 10;
            this.overheadSettings.MaterialMargin.Value = 20;
            this.overheadSettings.CommodityMargin.Value = 30;
            this.overheadSettings.ManufacturingMargin.Value = 40;
            this.overheadSettings.ConsumableMargin.Value = 50;
            this.overheadSettings.ExternalWorkMargin.Value = 60;

            var saveCommand = partVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(part.OverheadSettings.MaterialOverhead, this.overheadSettings.MaterialOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.CommodityOverhead, this.overheadSettings.CommodityOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.ManufacturingOverhead, this.overheadSettings.ManufacturingOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.PackagingOHValue, this.overheadSettings.PackagingOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.SalesAndAdministrationOHValue, this.overheadSettings.SalesAndAdministrationOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.ConsumableOverhead, this.overheadSettings.ConsumableOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.ExternalWorkOverhead, this.overheadSettings.ExternalWorkOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.OtherCostOHValue, this.overheadSettings.OtherCostOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.LogisticOHValue, this.overheadSettings.LogisticOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.CompanySurchargeOverhead, this.overheadSettings.CompanySurchargeOverhead.Value);
            Assert.AreEqual(part.OverheadSettings.MaterialMargin, this.overheadSettings.MaterialMargin.Value);
            Assert.AreEqual(part.OverheadSettings.CommodityMargin, this.overheadSettings.CommodityMargin.Value);
            Assert.AreEqual(part.OverheadSettings.ManufacturingMargin, this.overheadSettings.ManufacturingMargin.Value);
            Assert.AreEqual(part.OverheadSettings.ConsumableMargin, this.overheadSettings.ConsumableMargin.Value);
            Assert.AreEqual(part.OverheadSettings.ExternalWorkMargin, this.overheadSettings.ExternalWorkMargin.Value);
        }

        #endregion testMethods

        #region Private Methods
        /// <summary>
        /// This method creates a new part 
        /// </summary>
        /// <returns>Returns part</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private Part CreateNewPart(IDataSourceManager dataManager)
        {
            var part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingCountry = "Austria";

            var newManufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            part.Manufacturer = newManufacturer;

            var countrySettings = new CountrySetting();
            part.CountrySettings = countrySettings;

            var newOhSettings = new OverheadSetting();
            part.OverheadSettings = newOhSettings;

            dataManager.OverheadSettingsRepository.Save(newOhSettings);
            dataManager.ManufacturerRepository.Save(newManufacturer);
            dataManager.PartRepository.Save(part);
            dataManager.SaveChanges();

            return part;
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion Private Methods
    }
}
