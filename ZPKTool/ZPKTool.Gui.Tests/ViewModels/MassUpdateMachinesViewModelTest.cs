﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.DataAccess;
using ZPKTool.Business;
using ZPKTool.Common;
using System.Windows.Input;

namespace PROimpens.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for This is a test class for MassUpdateMachinesViewModel and is intended 
    /// to contain unit tests for all commands from MassUpdateMachinesViewModel.
    /// </summary>
    [TestClass]
    public class MassUpdateMachinesViewModelTest
    {
        /// <summary>
        /// The UI composition framework bootstrapper.
        /// </summary>
        private Bootstrapper bootstrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateMachinesViewModelTest"/> class.
        /// </summary>
        public MassUpdateMachinesViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand -> Cancelling the update.
        /// </summary>
        [TestMethod]
        public void UpdateMachinesTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = true;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 1;

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            // Create a clone of the machine in order to verify that the machine was not updated
            CloneManager cloneManager = new CloneManager(dataContext);
            Machine machineClone = cloneManager.Clone(machine);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(100);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = machine;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "No" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.No;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Machine dbMachine = dataContext2.Context.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);
            MachinesAreEqual(machineClone, dbMachine);
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Machine.
        /// In this case the ApplyUpdatesToSubObjects flag is not important because a machine doesn't have sub objects of Machine type.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = true;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 1;

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Machine machineClone = cloneManager.Clone(machine);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(100);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = machine;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Machine dbMachine = dataContext2.Context.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Machine and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = true;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 1;

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Machine machineClone = cloneManager.Clone(machine);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = machine;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Machine dbMachine = dataContext2.Context.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Machine, ManufacturingYearNewValue = null, DepreciationPeriodNewValue = null,
        /// CalculateWithKValue = false and ManualConsumableCostCalc = false; 
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = false;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = false;
            machine.ManualConsumableCostPercentage = 1;

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Machine machineClone = cloneManager.Clone(machine);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = null;
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = null;
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = machine;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Machine dbMachine = dataContext2.Context.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Process Step.
        /// All machines from the current process step will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = true;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 1;
            step.Machines.Add(machine);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);
            ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(1000);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(1000);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(1000);
            massUpdateMachinesViewModel.CurrentObject = step;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetProcessStepFull(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (Machine dbMachine in dbStep.Machines)
            {
                Machine machineClone = stepClone.Machines.FirstOrDefault(m => m.Name == dbMachine.Name);
                MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Process Step and the new values are expressed in percentages.
        /// All machines from the current process step will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = true;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 1;
            step.Machines.Add(machine);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);
            ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = step;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetProcessStepFull(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (Machine dbMachine in dbStep.Machines)
            {
                Machine machineClone = stepClone.Machines.FirstOrDefault(m => m.Name == dbMachine.Name);
                MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> ProcessStep, ManufacturingYearNewValue = empty, DepreciationPeriodNewValue = empty,
        /// CalculateWithKValue = false and ManualConsumableCostCalc = false; 
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2010;
            machine.DepreciationPeriod = 10;
            machine.DepreciationRate = 100;
            machine.CalculateWithKValue = false;
            machine.KValue = 230;
            machine.ManualConsumableCostCalc = false;
            machine.ManualConsumableCostPercentage = 1;
            step.Machines.Add(machine);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);
            ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = string.Empty;
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = string.Empty;
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(200);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = step;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the current machine
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetProcessStepFull(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (Machine dbMachine in dbStep.Machines)
            {
                Machine machineClone = stepClone.Machines.FirstOrDefault(m => m.Name == dbMachine.Name);
                MachineUpdated(machineClone, dbMachine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Process.
        /// All machines from the current process will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            part.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2010;
            machine1.DepreciationPeriod = 10;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2010;
            machine2.DepreciationPeriod = 10;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(1000);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(1000);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(1000);
            massUpdateMachinesViewModel.CurrentObject = part.Process;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current process
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (ProcessStep dbStep in dbPart.Process.Steps)
            {
                ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == dbStep.Name);
                foreach (Machine machine in dbStep.Machines)
                {
                    Machine machineClone = stepClone.Machines.FirstOrDefault(m => m.Name == machine.Name);
                    MachineUpdated(machineClone, machine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);
                }
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Process and the new values are expressed in percentages.
        /// All machines from the current process will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest9()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            part.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2010;
            machine1.DepreciationPeriod = 10;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2010;
            machine2.DepreciationPeriod = 10;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = part.Process;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current process
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (ProcessStep dbStep in dbPart.Process.Steps)
            {
                ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == dbStep.Name);
                foreach (Machine machine in dbStep.Machines)
                {
                    Machine machineClone = stepClone.Machines.FirstOrDefault(m => m.Name == machine.Name);
                    MachineUpdated(machineClone, machine, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);
                }
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part.
        /// All machines from the current part will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest10()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            part.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2010;
            machine1.DepreciationPeriod = 10;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2010;
            machine2.DepreciationPeriod = 10;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(1000);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(1000);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(1000);
            massUpdateMachinesViewModel.CurrentObject = part;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current part
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are expressed in percentages.
        /// All machines from the current process will be updated whether the ApplyUpdatesToSubObjects flag is true or false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest11()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            part.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2010;
            machine1.DepreciationPeriod = 10;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2010;
            machine2.DepreciationPeriod = 10;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = part;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current process
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest12()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Process = new Process();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2007;
            machine1.DepreciationPeriod = 1;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step1.Guid.ToString();
            subassembly1.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2006;
            machine2.DepreciationPeriod = 5;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.Process = new Process();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            subassembly2.Process.Steps.Add(step3);

            Machine machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();
            machine3.ManufacturingYear = 2001;
            machine3.DepreciationPeriod = 12;
            machine3.DepreciationRate = 30;
            machine3.CalculateWithKValue = true;
            machine3.KValue = 230;
            machine3.ManualConsumableCostCalc = true;
            machine3.ManualConsumableCostPercentage = 1;
            step3.Machines.Add(machine3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.Process = new Process();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            part1.Process.Steps.Add(step4);

            Machine machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            machine4.ManufacturingYear = 1998;
            machine4.DepreciationPeriod = 123;
            machine4.DepreciationRate = 70;
            machine4.CalculateWithKValue = true;
            machine4.KValue = 235;
            machine4.ManualConsumableCostCalc = true;
            machine4.ManualConsumableCostPercentage = 1;
            step4.Machines.Add(machine4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Process = new Process();
            assembly.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            part2.Process.Steps.Add(step5);

            Machine machine5 = new Machine();
            machine5.Name = machine5.Guid.ToString();
            machine5.ManufacturingYear = 2008;
            machine5.DepreciationPeriod = 7;
            machine5.DepreciationRate = 100;
            machine5.CalculateWithKValue = true;
            machine5.KValue = 230;
            machine5.ManualConsumableCostCalc = true;
            machine5.ManualConsumableCostPercentage = 1;
            step5.Machines.Add(machine5);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(100);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = assembly;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current assembly 
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, ApplyUpdatesToSubObjects = true and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest13()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Process = new Process();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2007;
            machine1.DepreciationPeriod = 1;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step1.Guid.ToString();
            subassembly1.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2006;
            machine2.DepreciationPeriod = 5;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.Process = new Process();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            subassembly2.Process.Steps.Add(step3);

            Machine machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();
            machine3.ManufacturingYear = 2001;
            machine3.DepreciationPeriod = 12;
            machine3.DepreciationRate = 30;
            machine3.CalculateWithKValue = true;
            machine3.KValue = 230;
            machine3.ManualConsumableCostCalc = true;
            machine3.ManualConsumableCostPercentage = 1;
            step3.Machines.Add(machine3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.Process = new Process();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            part1.Process.Steps.Add(step4);

            Machine machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            machine4.ManufacturingYear = 1998;
            machine4.DepreciationPeriod = 123;
            machine4.DepreciationRate = 70;
            machine4.CalculateWithKValue = true;
            machine4.KValue = 235;
            machine4.ManualConsumableCostCalc = true;
            machine4.ManualConsumableCostPercentage = 1;
            step4.Machines.Add(machine4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Process = new Process();
            assembly.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            part2.Process.Steps.Add(step5);

            Machine machine5 = new Machine();
            machine5.Name = machine5.Guid.ToString();
            machine5.ManufacturingYear = 2008;
            machine5.DepreciationPeriod = 7;
            machine5.DepreciationRate = 100;
            machine5.CalculateWithKValue = true;
            machine5.KValue = 230;
            machine5.ManualConsumableCostCalc = true;
            machine5.ManualConsumableCostPercentage = 1;
            step5.Machines.Add(machine5);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = assembly;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current assembly 
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Project and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest14()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Process = new Process();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2007;
            machine1.DepreciationPeriod = 1;
            machine1.DepreciationRate = 100;
            machine1.CalculateWithKValue = true;
            machine1.KValue = 230;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 1;
            step1.Machines.Add(machine1);

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.Process = new Process();
            //assembly.Subassemblies.Add(subassembly);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            subassembly.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.ManufacturingYear = 2006;
            machine2.DepreciationPeriod = 5;
            machine2.DepreciationRate = 100;
            machine2.CalculateWithKValue = true;
            machine2.KValue = 230;
            machine2.ManualConsumableCostCalc = true;
            machine2.ManualConsumableCostPercentage = 1;
            step2.Machines.Add(machine2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.Process = new Process();
            //assembly.Parts.Add(part1);

            ProcessStep step3 = new PartProcessStep();
            step3.Name = step3.Guid.ToString();
            part1.Process.Steps.Add(step3);

            Machine machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();
            machine3.ManufacturingYear = 2008;
            machine3.DepreciationPeriod = 7;
            machine3.DepreciationRate = 100;
            machine3.CalculateWithKValue = true;
            machine3.KValue = 230;
            machine3.ManualConsumableCostCalc = true;
            machine3.ManualConsumableCostPercentage = 1;
            step3.Machines.Add(machine3);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Process = new Process();
            project.Parts.Add(part2);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            part2.Process.Steps.Add(step4);

            Machine machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            machine4.ManufacturingYear = 2008;
            machine4.DepreciationPeriod = 7;
            machine4.DepreciationRate = 100;
            machine4.CalculateWithKValue = true;
            machine4.KValue = 230;
            machine4.ManualConsumableCostCalc = true;
            machine4.ManualConsumableCostPercentage = 1;
            step4.Machines.Add(machine4);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(100);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = project;
            massUpdateMachinesViewModel.DataContext = dataContext;
            massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = false;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the update was applied to the machines of the current assembly 
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project, ApplyUpdatesToSubObjects = true and the new values are expressed in percentages.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMachineTest15()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.Process = new Process();
        //    project.Assemblies.Add(assembly);

        //    ProcessStep step1 = new AssemblyProcessStep();
        //    step1.Name = step1.Guid.ToString();
        //    assembly.Process.Steps.Add(step1);

        //    Machine machine1 = new Machine();
        //    machine1.Name = machine1.Guid.ToString();
        //    machine1.ManufacturingYear = 2007;
        //    machine1.DepreciationPeriod = 1;
        //    machine1.DepreciationRate = 100;
        //    machine1.CalculateWithKValue = true;
        //    machine1.KValue = 230;
        //    machine1.ManualConsumableCostCalc = true;
        //    machine1.ManualConsumableCostPercentage = 1;
        //    step1.Machines.Add(machine1);

        //    Assembly subassembly = new Assembly();
        //    subassembly.Name = subassembly.Guid.ToString();
        //    subassembly.OverheadSettings = new OverheadSetting();
        //    subassembly.CountrySettings = new CountrySetting();
        //    subassembly.Process = new Process();
        //    assembly.Subassemblies.Add(subassembly);

        //    ProcessStep step2 = new AssemblyProcessStep();
        //    step2.Name = step1.Guid.ToString();
        //    subassembly.Process.Steps.Add(step2);

        //    Machine machine2 = new Machine();
        //    machine2.Name = machine2.Guid.ToString();
        //    machine2.ManufacturingYear = 2006;
        //    machine2.DepreciationPeriod = 5;
        //    machine2.DepreciationRate = 100;
        //    machine2.CalculateWithKValue = true;
        //    machine2.KValue = 230;
        //    machine2.ManualConsumableCostCalc = true;
        //    machine2.ManualConsumableCostPercentage = 1;
        //    step2.Machines.Add(machine2);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    part1.Process = new Process();
        //    assembly.Parts.Add(part1);

        //    ProcessStep step3 = new PartProcessStep();
        //    step3.Name = step3.Guid.ToString();
        //    part1.Process.Steps.Add(step3);

        //    Machine machine3 = new Machine();
        //    machine3.Name = machine3.Guid.ToString();
        //    machine3.ManufacturingYear = 2008;
        //    machine3.DepreciationPeriod = 7;
        //    machine3.DepreciationRate = 100;
        //    machine3.CalculateWithKValue = true;
        //    machine3.KValue = 230;
        //    machine3.ManualConsumableCostCalc = true;
        //    machine3.ManualConsumableCostPercentage = 1;
        //    step3.Machines.Add(machine3);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.OverheadSettings = new OverheadSetting();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.Process = new Process();

        //    ProcessStep step4 = new PartProcessStep();
        //    step4.Name = step4.Guid.ToString();
        //    part2.Process.Steps.Add(step4);

        //    Machine machine4 = new Machine();
        //    machine4.Name = machine4.Guid.ToString();
        //    machine4.ManufacturingYear = 2008;
        //    machine4.DepreciationPeriod = 7;
        //    machine4.DepreciationRate = 100;
        //    machine4.CalculateWithKValue = true;
        //    machine4.KValue = 230;
        //    machine4.ManualConsumableCostCalc = true;
        //    machine4.ManualConsumableCostPercentage = 1;
        //    step4.Machines.Add(machine4);

        //    DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
        //    dataContext.AssemblyRepository.Add(assembly);
        //    dataContext.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model 
        //    Random random = new Random();
        //    massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
        //    massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
        //    massUpdateMachinesViewModel.DepreciationRateIncreasePercentage = random.Next(100);
        //    massUpdateMachinesViewModel.KValueIncreasePercentage = random.Next(100);
        //    massUpdateMachinesViewModel.ConsumablesCostIncreasePercentage = random.Next(100);
        //    massUpdateMachinesViewModel.CurrentObject = project;
        //    massUpdateMachinesViewModel.DataContext = dataContext;
        //    massUpdateMachinesViewModel.ApplyUpdatesToSubObjects = true;

        //    // Set up the MessageBoxResult to "Yes" in order to perform the update operation
        //    windowService.MessageBoxResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the update was applied to the machines of the current assembly 
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateMachinesViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed after the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects was displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Commodity.
        /// </summary>
        [TestMethod]
        public void UpdateMachineTest16()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateMachinesViewModel massUpdateMachinesViewModel = new MassUpdateMachinesViewModel(messenger, windowService);

            // Set up the current object
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            // Set up the view model 
            Random random = new Random();
            massUpdateMachinesViewModel.ManufacturingYearNewValue = "2009";
            massUpdateMachinesViewModel.DepreciationPeriodNewValue = "90";
            massUpdateMachinesViewModel.DepreciationRateNewValue = random.Next(100);
            massUpdateMachinesViewModel.KValueNewValue = random.Next(100);
            massUpdateMachinesViewModel.ConsumablesCostNewValue = random.Next(100);
            massUpdateMachinesViewModel.CurrentObject = commodity;
            massUpdateMachinesViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateMachinesViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects was displayed.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verify if the project was updated according to the values from MassUpdateMachinesViewModel.
        /// </summary>
        /// <param name="oldProject">The project before update.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The MassUpdateMachinesViewModel in which contains the new values.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateMachinesViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MachinesAreEqual(part, oldPart);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    MachinesAreEqual(assembly, oldAssembly);
                }
            }
        }

        /// <summary>
        /// Verify if the assembly was updated according to the values from MassUpdateMachinesViewModel.
        /// </summary>
        /// <param name="oldAssembly">The assembly before update.</param>
        /// <param name="newAssembly">The updated assembly.</param>
        /// <param name="viewModel">The MassUpdateMachinesViewModel in which contains the new values.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateMachinesViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (ProcessStep step in updatedAssembly.Process.Steps)
            {
                ProcessStep oldStep = oldAssembly.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                foreach (Machine machine in step.Machines)
                {
                    Machine oldMachine = oldStep.Machines.FirstOrDefault(m => m.Name == machine.Name);
                    MachineUpdated(oldMachine, machine, viewModel, ref numberOfUpdatedObjects);
                }
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MachinesAreEqual(part, oldPart);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    MachinesAreEqual(subassembly, oldSubassembly);
                }
            }
        }

        /// <summary>
        /// Verify if the part was updated according to the values from MassUpdateMachinesViewModel.
        /// </summary>
        /// <param name="oldPart">The part before update.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The MassUpdateMachinesViewModel in which contains the new values.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateMachinesViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (ProcessStep step in updatedPart.Process.Steps)
            {
                ProcessStep oldStep = oldPart.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                foreach (Machine machine in step.Machines)
                {
                    Machine oldMachine = oldStep.Machines.FirstOrDefault(m => m.Name == machine.Name);
                    MachineUpdated(oldMachine, machine, viewModel, ref numberOfUpdatedObjects);
                }
            }
        }

        /// <summary>
        /// Verify if machine's properties were updated.
        /// </summary>
        /// <param name="oldMachine"> A clone of machine before being updated.</param>
        /// <param name="updatedMachine">The updated  machine.</param>
        /// <param name="viewModel">The MassUpdateMachinesViewModel in which contains the new values.</param>
        /// <return>True if the machine was updated, false otherwise.</return>
        private void MachineUpdated(Machine oldMachine, Machine updatedMachine, MassUpdateMachinesViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(viewModel.ManufacturingYearNewValue))
            {
                Assert.AreEqual<short?>((short?)Converter.StringToNullableInt(viewModel.ManufacturingYearNewValue), updatedMachine.ManufacturingYear, "Failed to update the ManufacturingYear.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<short>(oldMachine.ManufacturingYear.GetValueOrDefault(), updatedMachine.ManufacturingYear.GetValueOrDefault(), "The ManufacturingYear should not have been updated if the new value was null.");
            }

            if (!string.IsNullOrEmpty(viewModel.DepreciationPeriodNewValue))
            {
                Assert.AreEqual<int>(Converter.StringToNullableInt(viewModel.DepreciationPeriodNewValue).GetValueOrDefault(), updatedMachine.DepreciationPeriod.GetValueOrDefault(), "Failed to update the DepreciationPeriod.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldMachine.DepreciationPeriod.GetValueOrDefault(), updatedMachine.DepreciationPeriod.GetValueOrDefault(), "The DepreciationPeriod should not have been updated if the new value was null.");
            }

            if (viewModel.DepreciationRateNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.DepreciationRateNewValue.GetValueOrDefault(), updatedMachine.DepreciationRate.GetValueOrDefault(), "Failed to update the DepreciationRate with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.DepreciationRateIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.DepreciationRateIncreasePercentage.GetValueOrDefault() * oldMachine.DepreciationRate.GetValueOrDefault(), updatedMachine.DepreciationRate.GetValueOrDefault(), "Failed to update the DepreciationRate with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldMachine.DepreciationRate.GetValueOrDefault(), updatedMachine.DepreciationRate.GetValueOrDefault(), "The DepreciationRate should not have been updated if the new value was null.");
            }

            if (oldMachine.CalculateWithKValue == true)
            {
                if (viewModel.KValueNewValue != null)
                {
                    Assert.AreEqual<decimal>(viewModel.KValueNewValue.GetValueOrDefault(), updatedMachine.KValue.GetValueOrDefault(), "Failed to update the KValue with the new value.");
                    objectUpdated = true;
                }
                else if (viewModel.KValueIncreasePercentage != null)
                {
                    Assert.AreEqual<decimal>(viewModel.KValueIncreasePercentage.GetValueOrDefault() * oldMachine.KValue.GetValueOrDefault(), updatedMachine.KValue.GetValueOrDefault(), "Failed to update the KValue with the increase percentage.");
                    objectUpdated = true;
                }
                else
                {
                    Assert.AreEqual<decimal>(oldMachine.KValue.GetValueOrDefault(), updatedMachine.KValue.GetValueOrDefault(), "The KValue should not have been updated if the new value was null.");
                }
            }
            else
            {
                Assert.AreEqual<bool>(oldMachine.CalculateWithKValue.GetValueOrDefault(), updatedMachine.CalculateWithKValue.GetValueOrDefault(), "The CalculateWithKValue flag should not have been changed.");
            }

            if (oldMachine.ManualConsumableCostCalc == true)
            {
                if (viewModel.ConsumablesCostNewValue != null)
                {
                    Assert.AreEqual<decimal>(viewModel.ConsumablesCostNewValue.GetValueOrDefault(), updatedMachine.ManualConsumableCostPercentage.GetValueOrDefault(), "Failed to update the ManualConsumableCostPercentage with the new value.");
                    objectUpdated = true;
                }
                else if (viewModel.ConsumablesCostIncreasePercentage != null)
                {
                    Assert.AreEqual<decimal>(viewModel.ConsumablesCostIncreasePercentage.GetValueOrDefault() * oldMachine.ManualConsumableCostPercentage.GetValueOrDefault(), updatedMachine.ManualConsumableCostPercentage.GetValueOrDefault(), "Failed to update the ManualConsumableCostPercentage with the increase percentage.");
                    objectUpdated = true;
                }
                else
                {
                    Assert.AreEqual<decimal>(oldMachine.ManualConsumableCostPercentage.GetValueOrDefault(), updatedMachine.ManualConsumableCostPercentage.GetValueOrDefault(), "The ManualConsumableCostPercentage should not have been updated if the new value was null.");
                }
            }
            else
            {
                Assert.AreEqual<bool>(oldMachine.ManualConsumableCostCalc, updatedMachine.ManualConsumableCostCalc, "The ManualConsumableCostCalc flag should not have been changed.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verify if the machines belonging to two parts are equal.
        /// </summary>
        /// <param name="comparedPart"> The compared part.</param>
        /// <param name="partToCompareWith">The part to compare with.</param>
        private void MachinesAreEqual(Part comparedPart, Part partToCompareWith)
        {
            foreach (ProcessStep step in partToCompareWith.Process.Steps)
            {
                ProcessStep oldStep = comparedPart.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                foreach (Machine machine in step.Machines)
                {
                    Machine oldMachine = oldStep.Machines.FirstOrDefault(m => m.Name == machine.Name);
                    MachinesAreEqual(oldMachine, machine);
                }
            }
        }

        /// <summary>
        /// Verify if the machines belonging to two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void MachinesAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            foreach (ProcessStep comparedStep in comparedAssembly.Process.Steps)
            {
                ProcessStep stepToCompareWith = assemblyToCompareWith.Process.Steps.FirstOrDefault(s => s.Name == comparedStep.Name);
                foreach (Machine comparedMachine in comparedStep.Machines)
                {
                    Machine machineToCompareWith = stepToCompareWith.Machines.FirstOrDefault(m => m.Name == comparedMachine.Name);
                    MachinesAreEqual(comparedMachine, machineToCompareWith);
                }
            }

            foreach (Part comparedPart in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == comparedPart.Name);
                MachinesAreEqual(comparedPart, partToCompareWith);
            }

            foreach (Assembly comparedSubassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == comparedSubassembly.Name);
                MachinesAreEqual(comparedSubassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verify if two machines have the same properties' values. Only the following properties are compared:
        /// ManufacturingYear,DepreciationPeriod, DepreciationRate, KValue and ManualConsumableCostPercentage.
        /// </summary>
        /// <param name="comparedMachine">The compared machine.</param>
        /// <param name="machineToCompareWith">The machine to compare with.</param>
        /// <return>True if the machines are equal, false otherwise.  </return>
        private void MachinesAreEqual(Machine comparedMachine, Machine machineToCompareWith)
        {
            bool result = false;
            result = comparedMachine.ManufacturingYear == machineToCompareWith.ManufacturingYear &&
                        comparedMachine.DepreciationPeriod == machineToCompareWith.DepreciationPeriod &&
                        comparedMachine.DepreciationRate == machineToCompareWith.DepreciationRate &&
                        comparedMachine.CalculateWithKValue == machineToCompareWith.CalculateWithKValue &&
                        comparedMachine.ManualConsumableCostCalc == machineToCompareWith.ManualConsumableCostCalc;

            if (comparedMachine.CalculateWithKValue.GetValueOrDefault())
            {
                result = result && comparedMachine.KValue == machineToCompareWith.KValue;
            }

            if (comparedMachine.ManualConsumableCostCalc)
            {
                result = result && comparedMachine.ConsumablesCost == machineToCompareWith.ConsumablesCost;
            }

            Assert.IsTrue(result, "The machines are not equal.");
        }

        #endregion Helpers
    }
}
