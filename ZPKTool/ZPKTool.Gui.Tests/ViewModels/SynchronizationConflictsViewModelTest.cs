﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The unit tests for the Synchronization Conflicts View-Model class.
    /// </summary>
    [TestClass]
    public class SynchronizationConflictsViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The synchronization service mock.
        /// </summary>
        private Mock<ISyncService> syncServiceMock;

        /// <summary>
        /// The synchronization manager mock instance.
        /// </summary>
        private SynchronizationManagerMock syncManagerMock;

        #endregion

        #region Tests setup

        /// <summary>
        /// This code will run before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            var messageDialogService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)messageDialogService, (DispatcherServiceMock)dispatcherService);
            this.syncServiceMock = new Mock<ISyncService>();
            this.syncManagerMock = new SynchronizationManagerMock();
            this.syncServiceMock.SetupGet(x => x.SyncManager).Returns(syncManagerMock);

            this.CreateConflictsInSyncManager();
        }

        #endregion

        #region Test methods

        /// <summary>
        /// Tests the initialization of conflict items created to be displayed in the data grid, based on the conflicts from sync manager.
        /// </summary>
        [TestMethod]
        public void TestInitializeConflictGridItems()
        {
            var syncConflictsVM = new SynchronizationConflictsViewModel(this.syncServiceMock.Object, this.windowService, this.messenger);

            Assert.IsTrue(syncConflictsVM.IsResolveButtonEnabled.Value);
            Assert.AreEqual(syncConflictsVM.ConflictItems.Value.Count, this.syncManagerMock.Conflicts.Count);

            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[0].Type, LocalizedResources.General_Project);
            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[0].SyncTask, LocalizedResources.General_MyProjects);
            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[0].Name, "Local Project / Central Project");

            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[1].Type, LocalizedResources.General_BasicSettings);
            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[1].SyncTask, LocalizedResources.Synchronization_SDAndSettings);
            Assert.AreEqual(syncConflictsVM.ConflictItems.Value[1].Name, "Local Setting / Central Setting");
        }

        /// <summary>
        /// Tests that the selected resolution applies to all conflicts from sync manager.
        /// </summary>
        [TestMethod]
        public void TestSelectedResolutionChanged()
        {
            var syncConflictsVM = new SynchronizationConflictsViewModel(this.syncServiceMock.Object, this.windowService, this.messenger);
            var selectedResolution = SynchronizationConflictResolution.CentralVersionWins;
            syncConflictsVM.SelectedConflictResolution.Value = selectedResolution;

            foreach (var conflict in this.syncManagerMock.Conflicts)
            {
                Assert.AreEqual(conflict.Resolution, selectedResolution);
            }
        }

        /// <summary>
        /// Tests that the popup to show details of a conflict is opened and the right values are displayed.
        /// </summary>
        [TestMethod]
        public void TestOpenConflictDetailsPopup()
        {
            var syncConflictsVM = new SynchronizationConflictsViewModel(this.syncServiceMock.Object, this.windowService, this.messenger);

            var itemToOpen = syncConflictsVM.ConflictItems.Value[0];
            var itemToOpenLocalVal = (itemToOpen.Conflict.LocalEntityVersion as DataRow)["Name"].ToString();
            var itemToOpenCentralVal = (itemToOpen.Conflict.CentralEntityVersion as DataRow)["Name"].ToString();

            syncConflictsVM.OpenConflictDetailsPopupCommand.Execute(itemToOpen);

            var itemDetails = syncConflictsVM.ConflictDetails.Value;

            Assert.IsTrue(syncConflictsVM.IsConflictDetailsPopupOpen.Value);
            Assert.AreEqual(itemDetails[0].ColumnName, "Name");
            Assert.AreEqual(itemDetails[0].LocalColumnValue, itemToOpenLocalVal);
            Assert.AreEqual(itemDetails[0].CentralColumnValue, itemToOpenCentralVal);
        }

        /// <summary>
        /// Tests that executing the resolve conflicts command, the IsResolutionSetByUser flag is set for all conflicts and resolution is CentralVersionWins.
        /// </summary>
        [TestMethod]
        public void TestResolveConflictsCentralVersionWinsCommand()
        {
            var syncConflictsVM = new SynchronizationConflictsViewModel(this.syncServiceMock.Object, this.windowService, this.messenger);
            this.CreateUserToLogin();

            syncConflictsVM.SelectedConflictResolution.Value = SynchronizationConflictResolution.CentralVersionWins;
            syncConflictsVM.ResolveConflictsCommand.Execute(null);

            foreach (var conflict in this.syncManagerMock.Conflicts)
            {
                Assert.IsTrue(conflict.IsResolutionSetByUser);
                Assert.AreEqual(conflict.Resolution, syncConflictsVM.SelectedConflictResolution.Value);
            }
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Creates two conflicts in synchronize manager.
        /// </summary>
        private void CreateConflictsInSyncManager()
        {
            var dataTable1 = new DataTable("Projects");
            dataTable1.Columns.Add("Name");

            DataRow row1 = dataTable1.NewRow();
            row1["Name"] = "Local Project";

            DataRow row2 = dataTable1.NewRow();
            row2["Name"] = "Central Project";

            var dataTable2 = new DataTable("BasicSettings");
            dataTable2.Columns.Add("Name");

            DataRow row3 = dataTable2.NewRow();
            row3["Name"] = "Local Setting";

            DataRow row4 = dataTable2.NewRow();
            row4["Name"] = "Central Setting";

            this.syncManagerMock.Conflicts = new List<SynchronizationConflict>();
            this.syncManagerMock.Conflicts.Add(new SynchronizationConflict()
            {
                EntityType = "Projects",
                SyncTask = SyncType.MyProjects,
                LocalEntityVersion = row1,
                CentralEntityVersion = row2
            });

            this.syncManagerMock.Conflicts.Add(new SynchronizationConflict()
            {
                EntityType = "BasicSettings",
                SyncTask = SyncType.StaticDataAndSettings,
                LocalEntityVersion = row3,
                CentralEntityVersion = row4
            });
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateUserToLogin()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");

            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion
    }
}
