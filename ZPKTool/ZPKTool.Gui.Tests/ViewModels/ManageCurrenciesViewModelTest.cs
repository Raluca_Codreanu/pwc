﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for ManageCurrenciesViewModelTest and is intended
    /// to contain all ManageCurrenciesViewModelTest Unit Tests
    /// </summary>
    [TestClass]
    public class ManageCurrenciesViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// You can use the following additional attributes as you write your tests:
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion

        #region Test Methods

        /// <summary>
        /// A test for KeyDownCommand on data grid when the selected item is Null
        /// </summary>
        [TestMethod]
        public void KeyDownCommandWithSelectedItemNullTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);

            // new currency symbol
            var symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            // change the model
            currency.Symbol = symbol;

            manageCurrenciesViewModel.SelectedCurrency = currency;

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            manageCurrenciesViewModel.CurrencyViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand keyDownCommand = manageCurrenciesViewModel.DataGridKeyDownCommand;
            keyDownCommand.Execute(null);

            // The file is was not saved
            // Verifies if currency  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotSaved = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsFalse(currencyNotSaved, "The currency was saved");
        }

        /// <summary>
        /// A test for MouseDownCommand on data grid when the selected item is Null
        /// </summary>
        [TestMethod]
        public void MouseDownCommandWithSelectedItemNullTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);

            // new currency symbol
            var symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            // change the model
            currency.Symbol = symbol;

            manageCurrenciesViewModel.SelectedCurrency = currency;

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            manageCurrenciesViewModel.CurrencyViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand mouseDownCommand = manageCurrenciesViewModel.DataGridMouseDownCommand;
            mouseDownCommand.Execute(null);

            // The file is was not saved
            // Verifies if currency  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotSaved = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsFalse(currencyNotSaved, "The currency was saved");
        }

        /// <summary>
        /// A test for MouseDownCommand on data grid when the selected item is changed
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void MouseDownCommandVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // new currency symbol
            var symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            // change the model
            currency.Symbol = symbol;

            manageCurrenciesViewModel.SelectedCurrency = currency;

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            manageCurrenciesViewModel.CurrencyViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand mouseDownCommand = manageCurrenciesViewModel.DataGridMouseDownCommand;
            mouseDownCommand.Execute(currency);

            // The file is vas changed
            // Verifies if currency  was changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was not modified");
        }

        /// <summary>
        /// A test for MouseDownCommand on data grid when the selected item is changed
        /// and the user do not save the model differences
        /// </summary>
        [TestMethod]
        public void AbortedMouseDownCommandVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            manageCurrenciesViewModel.CurrencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            manageCurrenciesViewModel.CurrencyViewModel.DataSourceManager = dataContext;
            ICommand mouseDownCommand = manageCurrenciesViewModel.DataGridMouseDownCommand;
            mouseDownCommand.Execute(currency);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
            Assert.AreEqual(symbol, manageCurrenciesViewModel.CurrencyViewModel.CurrencySymbol.Value);
        }

        /// <summary>
        /// A test for KeyDownCommand on data grid when the selected item is changed
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void KeyDownCommandIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // new currency symbol
            var symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            // change the model
            currency.Symbol = symbol;

            manageCurrenciesViewModel.SelectedCurrency = currency;

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            manageCurrenciesViewModel.CurrencyViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand keyDownCommand = manageCurrenciesViewModel.DataGridKeyDownCommand;
            keyDownCommand.Execute(currency);

            // The file is vas changed
            // Verifies if currency  was changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was not modified");
        }

        /// <summary>
        /// A test for KeyDownCommand on data grid when the selected item is changed
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void AbortedKeyDownCommandIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            manageCurrenciesViewModel.CurrencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand keyDownCommand = manageCurrenciesViewModel.DataGridKeyDownCommand;
            keyDownCommand.Execute(currency);

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
            Assert.AreEqual(symbol, manageCurrenciesViewModel.CurrencyViewModel.CurrencySymbol.Value, "The view model changes were not reverted.");
        }

        /// <summary>
        /// A test for delete command from database when a currency is assigned to a country
        /// and the user do not confirms the delete action when confirmation message appear
        /// </summary>
        [TestMethod]
        public void DeleteAbortedCurrencyAssignedToCountryTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;

            dataContext.CurrencyRepository.Add(currency);
            this.AddNewCountryToDB(currency, dataContext);
            dataContext.SaveChanges();

            this.CreateUserToLogin(dataContext);

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand deleteCommand = manageCurrenciesViewModel.DeleteCommand;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            deleteCommand.Execute(null);

            // The file is not deleted
            // Verifies if currency  was not deleted.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotDeleted = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotDeleted, "The currency was deleted");
        }

        /// <summary>
        /// A test for delete command from database when a currency is assigned to a country
        /// and the user confirms the delete action when confirmation message appear
        /// </summary>
        [TestMethod]
        public void DeleteCurrencyAssignedToCountryTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;

            dataContext.CurrencyRepository.Add(currency);
            this.AddNewCountryToDB(currency, dataContext);
            dataContext.SaveChanges();

            this.CreateUserToLogin(dataContext);

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand deleteCommand = manageCurrenciesViewModel.DeleteCommand;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            deleteCommand.Execute(null);

            // The file is not deleted
            // Verifies if currency  was not deleted.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotDeleted = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotDeleted, "The currency was deleted");
        }

        /// <summary>
        /// A test for delete command from database when a currency is not assigned to a country
        /// and the user do not confirms the delete action when confirmation message appear
        /// </summary>
        [TestMethod]
        public void DeleteAbortedCurrencyTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            this.CreateUserToLogin(dataContext);

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand deleteCommand = manageCurrenciesViewModel.DeleteCommand;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            deleteCommand.Execute(null);

            // The file is not deleted 
            // Verifies if currency  was not deleted.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotDeleted = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotDeleted, "The currency was deleted");
        }

        /// <summary>
        /// A test for delete command from database when a currency is not assigned to a country
        /// and the user confirms the delete action when confirmation message appear
        /// </summary>
        [TestMethod]
        public void DeleteCurrencyTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            this.CreateUserToLogin(dataContext);

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand deleteCommand = manageCurrenciesViewModel.DeleteCommand;
            manageCurrenciesViewModel.SelectedCurrency = currency;
            deleteCommand.Execute(null);

            // The file is deleted 
            // Verifies if currency  was deleted.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotDeleted = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsFalse(currencyNotDeleted, "The currency was not deleted");
        }

        /// <summary>
        /// A test for delete command from database when the currency is null
        /// </summary>
        [TestMethod]
        public void DeleteCurrencyWithNullSelectedItemTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            this.CreateUserToLogin(dataContext);

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand deleteCommand = manageCurrenciesViewModel.DeleteCommand;
            manageCurrenciesViewModel.SelectedCurrency = null;
            deleteCommand.Execute(null);

            // The file is not deleted 
            // Verifies if currency  was deleted.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyNotDeleted = newContext.CurrencyRepository.CheckIfExists(currency.Guid);
            Assert.IsTrue(currencyNotDeleted, "The currency was deleted");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The manageCurrenciesViewModel is changed and its input is valid
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void OnUnloadingVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // new currency symbol
            var symbol = EncryptionManager.Instance.GenerateRandomString(2, true);

            // change the model
            currency.Symbol = symbol;

            manageCurrenciesViewModel.SelectedCurrency = currency;

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;

            manageCurrenciesViewModel.CurrencyViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            manageCurrenciesViewModel.OnUnloading();

            // The file is vas changed
            // Verifies if currency  was changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was not modified");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The manageCurrenciesViewModel is not changed and its input is valid
        ///  and the user do not save the model differences
        /// </summary>
        [TestMethod]
        public void AbortedOnUnloadingVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowService);
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);

            // Create a currency
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            var symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = symbol;
            currency.ExchangeRate = 1;
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            // Set up the manageCurrenciesViewModel                                    
            manageCurrenciesViewModel.DataSourceManager = dataContext;
            manageCurrenciesViewModel.SelectedCurrency = currency;

            manageCurrenciesViewModel.CurrencyViewModel.CurrencySymbol.Value = EncryptionManager.Instance.GenerateRandomString(2, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            manageCurrenciesViewModel.CurrencyViewModel.DataSourceManager = dataContext;
            manageCurrenciesViewModel.OnUnloading();

            // The file is vas not changed
            // Verifies if currency  was not changed.
            Assert.AreEqual(symbol, currency.Symbol, "The currency was modified");
        }

        /// <summary>
        /// A test for add new currency using a new instance of currencyViewModel into a window
        /// and the add currency is with success
        /// </summary>
        [TestMethod]
        public void AddCurrencyWithSuccessTest()
        {
            IMessenger messenger = new Messenger();
            var messageDialogService = new MessageDialogServiceMock();
            var currencyViewModel = new CurrencyViewModel(new WindowServiceMock(messageDialogService, new DispatcherServiceMock()));

            // Create a currency
            var currency = new Currency
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(3, true),
                IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true),
                ExchangeRate = 1
            };

            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(currencyViewModel, "CreateCurrencyViewTemplate", true, false))
                .Callback(() =>
                {
                    currencyViewModel.Model = currency;
                    currencyViewModel.SaveCommand.Execute(null);
                });

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, currencyViewModel);

            var manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowServiceMock.Object);
            manageCurrenciesViewModel.AddCommand.Execute(null);

            Assert.IsTrue(manageCurrenciesViewModel.Currencies.Contains(currency));
        }

        /// <summary>
        /// A test for add new currency using a new instance of currencyViewModel into a window
        /// and the add currency is with cancel
        /// </summary>
        [TestMethod]
        public void AddCurrencyWithCancelTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            CurrencyViewModel currencyViewModel = new CurrencyViewModel(new WindowServiceMock(messageDialogService, new DispatcherServiceMock()));
            var testCurrency = new Currency() { Name = "Test Currency" };
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(currencyViewModel, "CreateCurrencyViewTemplate", true, false))
                .Callback(() =>
                {
                    // Trigger the release of the Model property value.
                    currencyViewModel.OnUnloading();
                });

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, currencyViewModel);

            ManageCurrenciesViewModel manageCurrenciesViewModel = new ManageCurrenciesViewModel(compositionContainer, messenger, windowServiceMock.Object);
            manageCurrenciesViewModel.AddCommand.Execute(null);

            Assert.IsTrue(!manageCurrenciesViewModel.Currencies.Contains(testCurrency));
        }

        #endregion Test Methods

        /// <summary>
        /// Create a composition container using some parameters
        /// </summary>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The windows service.</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(out MessageDialogServiceMock messageDialogService, out WindowServiceMock windowService)
        {
            messageDialogService = new MessageDialogServiceMock();
            windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CurrencyViewModel currencyViewModel = new CurrencyViewModel(windowService);

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, currencyViewModel);

            return compositionContainer;
        }

        /// <summary>
        /// Create a user to login
        /// </summary>
        /// <param name="dataManager">The data context</param>
        public void CreateUserToLogin(IDataSourceManager dataManager)
        {
            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
        }

        /// <summary>
        /// Add a new country to database
        /// </summary>
        /// <param name="currency">The currency that is assigned to</param>
        /// <param name="dataManager">The data context</param>
        public void AddNewCountryToDB(Currency currency, IDataSourceManager dataManager)
        {
            // Create a country with the current currency
            Country country = new Country();
            country.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country.CountrySetting = new CountrySetting();
            country.IsReleased = false;
            country.Currency = currency;
            MeasurementUnit weightUnit = dataManager.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataManager.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataManager.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataManager.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataManager.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;
            dataManager.CountryRepository.Add(country);
            dataManager.SaveChanges();
        }
    }
}
