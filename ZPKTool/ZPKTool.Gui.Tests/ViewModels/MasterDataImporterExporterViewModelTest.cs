﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The Master Data Importer view model test.
    /// </summary>
    [TestClass]
    public class MasterDataImporterExporterViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        #endregion

        /// <summary> 
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.fileDialogService = new FileDialogServiceMock();
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
        }

        #region Test methods

        /// <summary>
        /// Imports an edited master data with success test.
        /// </summary>
        [TestMethod]
        public void ImportAnEitedMasterDataWithSuccessTest()
        {
            var masterDataImporterExporterViewModel = new MasterDataImporterExporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            masterDataImporterExporterViewModel.DataManager = this.dataManager;
            masterDataImporterExporterViewModel.Operation = MasterDataOperation.Import;
            masterDataImporterExporterViewModel.EntityType = ImportedEntityType.Currency;

            string path = Path.Combine(Environment.CurrentDirectory, "EditedCurrency.xls");
            masterDataImporterExporterViewModel.FilePath.Value = path;
            masterDataImporterExporterViewModel.FileType = MasterDataImportExportFileType.Xls;

            ICommand importEntity = masterDataImporterExporterViewModel.ImportExportCommand;
            importEntity.Execute(null);

            Thread.Sleep(500);
            while (masterDataImporterExporterViewModel.IsImportOrExportInProgress.Value)
            {
                Thread.Sleep(200);
            }

            // The exchange rate of the "Algerian Dinar" base currency was edited and needs to be "90.815".
            var currencyEdited = this.dataManager.CurrencyRepository.GetByIsoCode("DZD");
            Assert.AreEqual(currencyEdited.ExchangeRate, 90.815m);
        }

        /// <summary>
        /// A test for the import master data after added a new entity with success test.
        /// </summary>
        [TestMethod]
        public void ImportANewMasterDataWithSuccessTest()
        {
            var masterDataImporterExporterViewModel = new MasterDataImporterExporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            masterDataImporterExporterViewModel.DataManager = this.dataManager;
            masterDataImporterExporterViewModel.Operation = MasterDataOperation.Import;
            masterDataImporterExporterViewModel.EntityType = ImportedEntityType.Currency;

            string path = Path.Combine(Environment.CurrentDirectory, "AddedCurrency.xls");
            masterDataImporterExporterViewModel.FilePath.Value = path;
            masterDataImporterExporterViewModel.FileType = MasterDataImportExportFileType.Xls;

            ICommand importEntity = masterDataImporterExporterViewModel.ImportExportCommand;
            importEntity.Execute(null);

            Thread.Sleep(500);
            while (masterDataImporterExporterViewModel.IsImportOrExportInProgress.Value)
            {
                Thread.Sleep(200);
            }

            var currency = this.dataManager.CurrencyRepository.GetByIsoCode("NEW");
            Assert.IsNotNull(currency.IsoCode);
        }

        /// <summary>
        /// A test for the import master data after deleted an entity with success test.
        /// </summary>
        [TestMethod]
        public void ImportMasterdataAfterDeletedAnEntityWithSuccessTest()
        {
            var masterDataImporterExporterViewModel = new MasterDataImporterExporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            masterDataImporterExporterViewModel.Operation = MasterDataOperation.Import;
            masterDataImporterExporterViewModel.DataManager = this.dataManager;
            masterDataImporterExporterViewModel.EntityType = ImportedEntityType.Currency;

            string path = Path.Combine(Environment.CurrentDirectory, "DeletedCurrency.xls");
            masterDataImporterExporterViewModel.FilePath.Value = path;
            masterDataImporterExporterViewModel.FileType = MasterDataImportExportFileType.Xls;

            ICommand importEntity = masterDataImporterExporterViewModel.ImportExportCommand;
            importEntity.Execute(null);

            Thread.Sleep(500);
            while (masterDataImporterExporterViewModel.IsImportOrExportInProgress.Value)
            {
                Thread.Sleep(200);
            }

            // The currency should not be deleted after the import.
            var currency = this.dataManager.CurrencyRepository.GetByIsoCode("VND");
            Assert.IsNotNull(currency.IsoCode);
        }

        /// <summary>
        /// Exports the master data with success.
        /// </summary>
        [TestMethod]
        public void ExportMasterDataWithSuccessTest()
        {
            var masterDataImporterExporterViewModel = new MasterDataImporterExporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            masterDataImporterExporterViewModel.Operation = MasterDataOperation.Export;
            masterDataImporterExporterViewModel.DataManager = this.dataManager;
            masterDataImporterExporterViewModel.EntityType = ImportedEntityType.Currency;

            string path = Path.Combine(Path.GetTempPath() + "Currency.xls");
            masterDataImporterExporterViewModel.FilePath.Value = path;

            ICommand exportEntity = masterDataImporterExporterViewModel.ImportExportCommand;
            exportEntity.Execute(null);

            Thread.Sleep(500);
            while (masterDataImporterExporterViewModel.IsImportOrExportInProgress.Value)
            {
                Thread.Sleep(200);
            }

            Assert.IsTrue(File.Exists(masterDataImporterExporterViewModel.FilePath.Value), "The file does not exist.");
        }

        #endregion
    }
}