﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for EntityDocumentsViewModel and is intended 
    /// to contain all EntityDocumentsViewModel Unit Tests
    /// </summary>
    [TestClass]
    public class EntityDocumentsViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDocumentsViewModelTest"/> class.
        /// </summary>
        public EntityDocumentsViewModelTest()
        {
            Utils.ConfigureDbAccess();
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests DeleteDocument command using valid input parameters
        /// </summary>
        [TestMethod]
        public void DeleteDocumentTest()
        {
            IWindowService wndService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a document and add it to documents list of view model
            ZPKTool.Gui.ViewModels.EntityDocumentsViewModel.DocumentItem document1 = new EntityDocumentsViewModel.DocumentItem();
            Media media = new Media();
            document1.Name = media.Guid.ToString();
            document1.Source = media;
            entityDocumentsViewModel.Documents.Add(document1);

            // Delete the added document
            ICommand deleteDocumentCommand = entityDocumentsViewModel.DeleteDocumentCommand;
            deleteDocumentCommand.Execute(document1);

            // Verifies that the document was deleted from documents list
            Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 0, "Failed to delete a document");
        }

        /// <summary>
        /// Tests AddDocument command using valid input parameters
        /// </summary>
        [TestMethod]
        public void AddValidDocumentTest()
        {
            IWindowService wndService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a temp file on the disk
            string path = Path.GetTempFileName();
            fileDialogService.FileName = path;

            try
            {
                ICommand addCommand = entityDocumentsViewModel.AddDocumentCommand;
                addCommand.Execute(null);

                // Verifies that the document was added 
                Assert.AreEqual(1, entityDocumentsViewModel.Documents.Count(), "Failed to add a document");
            }
            finally
            {
                // Delete the created temp file
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests Add Document command using invalid input parameters - null document
        /// </summary>
        [TestMethod]
        public void AddNullDocumentTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Set File path to null
            fileDialogService.FileName = null;
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            messageBoxService.ShowCallCount = 0;

            ICommand addCommand = entityDocumentsViewModel.AddDocumentCommand;
            addCommand.Execute(null);

            // Verifies if show message box method was called and the invalid document was not added to documents list
            Assert.AreEqual<int>(1, messageBoxService.ShowCallCount, "Invalid number of calls of show message box method");
            Assert.AreEqual<int>(0, entityDocumentsViewModel.Documents.Count(), "An invalid document was added to document list");
        }

        /// <summary>
        /// Tests Add Document command using invalid input parameters - not existing file
        /// </summary>
        [TestMethod]
        public void AddNotExistingFileTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Set File path to a path which does not exist;
            fileDialogService.FileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            messageBoxService.ShowCallCount = 0;

            ICommand addCommand = entityDocumentsViewModel.AddDocumentCommand;
            addCommand.Execute(null);

            // Verifies if show message box method was called and the invalid document was not added to documents list
            Assert.IsTrue(messageBoxService.ShowCallCount == 1, "Invalid number of calls of show message box method");
            Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 0, "An invalid document was added to document list");
        }

        /// <summary>
        /// A test for 'Add Document' command using more documents than the max allowed number of documents as input data.
        /// </summary>
        [TestMethod]
        public void AddMoreDocumentsThanMaxAllowedNumberOfDocuments()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Add 5 existing documents (dummy)
            entityDocumentsViewModel.Documents.Add(new EntityDocumentsViewModel.DocumentItem());
            entityDocumentsViewModel.Documents.Add(new EntityDocumentsViewModel.DocumentItem());
            entityDocumentsViewModel.Documents.Add(new EntityDocumentsViewModel.DocumentItem());
            entityDocumentsViewModel.Documents.Add(new EntityDocumentsViewModel.DocumentItem());
            entityDocumentsViewModel.Documents.Add(new EntityDocumentsViewModel.DocumentItem());

            // Add 6 dummy files to the file dialog's list of selected files.
            for (int i = 0; i < 6; i++)
            {
                fileDialogService.FileNames.Add(i.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }

            ICommand addCommand = entityDocumentsViewModel.AddDocumentCommand;
            addCommand.Execute(null);

            // Verifies that the error message was displayed and no document was added
            Assert.IsTrue(messageBoxService.ShowCallCount == 1, "Invalid number of calls of ShowMessageBox method.");
            Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 5, "If the number of documents to be added is greater than the max allowed number, the selected documents shouldn't be added.");
        }

        /// <summary>
        /// Tests Add Document command using invalid input parameters - a file bigger than 50 MB
        /// </summary>
        [TestMethod]
        public void AddDocumentWithSizeGreaterThanMaxAllowedSize()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a temp file on the disk
            string path = Path.GetTempFileName();
            FileStream fileStream = new FileStream(path, FileMode.Open);
            byte[] info = new byte[ZPKTool.Common.Constants.MaxDocumentSize + 1];
            fileStream.Write(info, 0, info.Length);
            fileDialogService.FileName = path;
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            messageBoxService.ShowCallCount = 0;

            try
            {
                ICommand addCommand = entityDocumentsViewModel.AddDocumentCommand;
                addCommand.Execute(null);

                // Verifies if show message box method was called and the invalid document was not added to documents list
                Assert.AreEqual<int>(1, messageBoxService.ShowCallCount, "Invalid number of calls of show message box method");
                Assert.AreEqual<int>(0, entityDocumentsViewModel.Documents.Count(), "An invalid document was added to documents list");
            }
            finally
            {
                fileStream.Close();
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests SaveToDisk command using valid input parameters
        /// </summary>
        [TestMethod]
        public void SaveToDiskValidDocumentTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a file on the disk and set the location were the document will be saved 
            string path = Path.GetTempFileName();

            // Creates a text document
            EntityDocumentsViewModel.DocumentItem document1 = new EntityDocumentsViewModel.DocumentItem();
            document1.Name = path;
            document1.Source = path;
            entityDocumentsViewModel.Documents.Add(document1);

            // Creates a media document
            Media media = new Media();
            EntityDocumentsViewModel.DocumentItem document2 = new EntityDocumentsViewModel.DocumentItem();
            document2.Name = media.Guid.ToString();
            document2.Source = media;
            entityDocumentsViewModel.Documents.Add(document2);

            try
            {
                // Execute save to disk command for the text document
                ICommand saveToDiskCommand = entityDocumentsViewModel.SaveToDiskCommand;
                saveToDiskCommand.Execute(document1);

                // Verifies that the document was saved to disk
                bool fileExistsAtDestination1 = File.Exists(fileDialogService.FileName);
                Assert.IsTrue(fileExistsAtDestination1, "Failed to save to disk a text file");

                // Execute save to disk command for the media document and verifies that document was saved to disk
                saveToDiskCommand.Execute(document2);
                bool fileExistsAtDestination2 = File.Exists(Path.GetFullPath(document2.Name));
                Assert.IsTrue(fileExistsAtDestination2, "Failed to save to disk a media");
            }
            finally
            {
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests SaveCommand for added entities.
        /// </summary>
        [TestMethod]
        public void SaveAddOperationTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Add a document to a part entity
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.SetIsMasterData(true);

            User user = new User();
            user.Name = user.Guid.ToString();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            part.SetOwner(user);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            entityDocumentsViewModel.ModelDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            entityDocumentsViewModel.Model = part;
            this.DelayUntilVmIsLoaded(entityDocumentsViewModel);
            string path = Path.GetTempFileName();

            // Creates a text document
            EntityDocumentsViewModel.DocumentItem document1 = new EntityDocumentsViewModel.DocumentItem();
            document1.Name = path;
            document1.Source = path;
            document1.IsNew = true;
            entityDocumentsViewModel.Documents.Add(document1);

            try
            {
                ICommand saveCommand = entityDocumentsViewModel.SaveCommand;
                saveCommand.Execute(null);

                // Verifies if the document collection of view model was cleared after save
                Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 0, "Failed to save the changes - the documents collection of ViewModel are not cleared.");

                // Verifies if the newly added document was attached to the entity and "IsMasterData" property and "Owner" property were set
                Assert.IsTrue(part.Media.Count == 1, "Failed to save the changes - add a document to the model.");
                foreach (Media media in part.Media)
                {
                    Assert.IsTrue(media.Owner != null && media.Owner.Guid == part.Owner.Guid, "Failed to set the owner of the added document.");
                    Assert.AreEqual(part.IsMasterData, media.IsMasterData, "Failed to set IsMasterData flag of the added document.");
                }
            }
            finally
            {
                // Delete the created temp file
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests SaveCommand for deleted entities.
        /// </summary>
        [TestMethod]
        public void SaveDeleteOperationTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a project entity which will be the model of entityDocumentsViewModel
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Media media = new Media();
            media.Type = MediaType.Document;
            media.Content = new byte[] { 1, 1, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Set the properties of view model
            entityDocumentsViewModel.ModelDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            entityDocumentsViewModel.Model = project;

            // Creates a document item that corresponds to the project's media
            EntityDocumentsViewModel.DocumentItem document = new EntityDocumentsViewModel.DocumentItem();
            document.Name = media.Guid.ToString();
            document.Source = media;
            entityDocumentsViewModel.Documents.Add(document);

            // Delete the added document 
            ICommand deleteCommand = entityDocumentsViewModel.DeleteDocumentCommand;
            deleteCommand.Execute(entityDocumentsViewModel.Documents.ElementAt(0));

            // Save the changes
            ICommand saveCommand = entityDocumentsViewModel.SaveCommand;
            saveCommand.Execute(null);

            // Retrieve the entity's documents
            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<Media> projectMedia = new Collection<Media>(mediaManager.GetAllMedia(project).Where(m => m.Type == MediaType.Document).ToList());

            // Verifies if document was deleted from entity and from view model' documents list
            Assert.IsTrue(projectMedia.Count == 0, "Failed to save the changes - delete a document from an entity");
            Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 0, "Fails to save the changes - delete a document from view model's documents list");
        }

        /// <summary>
        /// Tests Cancel command for added entities.
        /// </summary>
        [TestMethod]
        public void CancelAddOperationTest()
        {
            var messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            var entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a project entity which will be the model of this view model
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Set the properties of view model
            entityDocumentsViewModel.ModelDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            entityDocumentsViewModel.Model = project;
            this.DelayUntilVmIsLoaded(entityDocumentsViewModel);

            // Creates a text document
            var path = Path.GetTempFileName();
            var document = new EntityDocumentsViewModel.DocumentItem
            {
                Name = path,
                Source = path,
                IsNew = true
            };

            entityDocumentsViewModel.Documents.Add(document);

            try
            {
                // Cancel the changes
                ICommand cancelCommand = entityDocumentsViewModel.CancelCommand;
                cancelCommand.Execute(null);

                Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 0, "Failed to cancel the changes - add a document to view model's documents list.");
                Assert.IsTrue(project.Media.Count == 0, "Failed to cancel the changes - add a document to an entity.");
            }
            finally
            {
                // Delete the created temp file 
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests Cancel command for delete operation
        /// </summary>
        [TestMethod]
        public void CancelDeleteOperationTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            IWindowService wndService = new WindowServiceMock(messageBoxService, new DispatcherServiceMock());
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            IModelBrowserHelperService modelBrowserHelperService = new ModelBrowserHelperService();
            EntityDocumentsViewModel entityDocumentsViewModel = new EntityDocumentsViewModel(wndService, fileDialogService, modelBrowserHelperService);

            // Creates a project entity which will be the model of entityDocumentsViewModel
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Media media = new Media();
            media.Type = MediaType.Document;
            media.Content = new byte[] { 1, 1, 0, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            project.Media.Add(media);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Set the properties of view model
            entityDocumentsViewModel.ModelDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            entityDocumentsViewModel.Model = project;
            this.DelayUntilVmIsLoaded(entityDocumentsViewModel);

            // Creates a document item that corresponds to the project's document
            EntityDocumentsViewModel.DocumentItem documentItem = new EntityDocumentsViewModel.DocumentItem();
            documentItem.Name = media.OriginalFileName;
            documentItem.Source = media;
            entityDocumentsViewModel.Documents.Add(documentItem);

            // Deletes the added document item
            ICommand deleteCommand = entityDocumentsViewModel.DeleteDocumentCommand;
            deleteCommand.Execute(entityDocumentsViewModel.Documents.ElementAt(0));

            // Cancel the changes
            ICommand cancelCommand = entityDocumentsViewModel.CancelCommand;
            cancelCommand.Execute(null);

            // Verify that the document was not removed from the vie model 
            Assert.IsTrue(entityDocumentsViewModel.Documents.Count == 1, "Failed to cancel the changes - delete a document from view model's documents list");

            // Verify that the document was not removed from the project
            MediaManager mediaManager = new MediaManager(dataContext);
            MediaMetaData projectDocument = mediaManager.GetDocumentsMetaData(project).FirstOrDefault();
            Assert.IsNotNull(projectDocument, "Failed to cancel the changes - delete a document from an entity");
        }

        /// <summary>
        /// Delay the operations until the view-models passed as parameter is loaded.
        /// </summary>
        /// <param name="viewModel">The view-model.</param>
        private void DelayUntilVmIsLoaded(ViewModel viewModel)
        {
            while (!viewModel.IsLoaded)
            {
                Thread.Sleep(100);
            }
        }
    }
}
