﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Data;
using ZPKTool.Business;
using ZPKTool.DataAccess;

namespace PROimpens.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MassUpdateAdditionalCostViewModel and is intended 
    /// to contain unit tests for all public methods from MassUpdateAdditionalCostViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateAdditionalCostViewModelTest
    {
        /// <summary>
        /// The UI composition framework bootstrapper.
        /// </summary>
        private Bootstrapper bootstrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateAdditionalCostViewModelTest"/> class.
        /// </summary>
        public MassUpdateAdditionalCostViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.DevelopmentCost = 10;
            part.PackagingCost = 20;
            part.ProjectInvest = 30;
            part.LogisticCost = 40;
            part.OtherCost = 50;
            part.PaymentTerms = 60;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Create a clone of the part in order to verify that the part was not modified
            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = part;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "No" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.No;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            AdditionalCostsAreEqual(dbPart, partClone);
        }

        ///// <summary>
        ///// A test for UpdateCommand with the following test data : Current Entity - Project and ApplyUpdatesToSubObjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateAdditionalCostTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

        //    // Set up the current object 
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly1 = new Assembly();
        //    assembly1.Name = assembly1.Guid.ToString();
        //    assembly1.CountrySettings = new CountrySetting();
        //    assembly1.OverheadSettings = new OverheadSetting();
        //    assembly1.DevelopmentCost = 10;
        //    assembly1.PackagingCost = 20;
        //    assembly1.ProjectInvest = 30;
        //    assembly1.LogisticCost = 40;
        //    assembly1.OtherCost = 50;
        //    assembly1.PaymentTerms = 60;
        //    project.Assemblies.Add(assembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.DevelopmentCost = 100;
        //    part1.PackagingCost = 200;
        //    part1.ProjectInvest = 300;
        //    part1.LogisticCost = 400;
        //    part1.OtherCost = 500;
        //    part1.PaymentTerms = 600;
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part1);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
        //    massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
        //    massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
        //    massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
        //    massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
        //    massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
        //    massUpdateAdditionalCostViewModel.CurrentObject = project;

        //    bool updateFinished = false;
        //    messenger.Register<NotificationMessage>((msg) =>
        //        {
        //            if (string.Equals(msg.Notification, "UpdateMassData"))
        //            {
        //                updateFinished = true;
        //            }
        //        }
        //        );

        //    windowService.MessageBoxResult = MessageBoxResult.Yes;
        //    massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
        //    ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    while (!updateFinished)
        //    {
        //        Task.WaitAll();
        //    }

        //    // Verify that the updates were applied to sub-objects too
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    ProjectUpdated(projectClone, dbProject, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Project, ApplyUpdatesToSubObjects = false and 
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.CountrySettings = new CountrySetting();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.DevelopmentCost = 10;
            assembly1.PackagingCost = 20;
            assembly1.ProjectInvest = 30;
            assembly1.LogisticCost = 40;
            assembly1.OtherCost = 50;
            assembly1.PaymentTerms = 60;
            project.Assemblies.Add(assembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.DevelopmentCost = 100;
            part1.PackagingCost = 200;
            part1.ProjectInvest = 300;
            part1.LogisticCost = 400;
            part1.OtherCost = 500;
            part1.PaymentTerms = 600;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project.Parts.Add(part1);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Create a of the project in order to check that the project's sub objects where not updated
            CloneManager cloneManager = new CloneManager(dataContext);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = project;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no sub object of the project was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Project and ApplyUpdatesToSubObjects = false.
        /// Additional Cost Values expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.CountrySettings = new CountrySetting();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.DevelopmentCost = 10;
            assembly1.PackagingCost = 20;
            assembly1.ProjectInvest = 30;
            assembly1.LogisticCost = 40;
            assembly1.OtherCost = 50;
            assembly1.PaymentTerms = 60;
            project.Assemblies.Add(assembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.DevelopmentCost = 100;
            part1.PackagingCost = 200;
            part1.ProjectInvest = 300;
            part1.LogisticCost = 400;
            part1.OtherCost = 500;
            part1.PaymentTerms = 600;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project.Parts.Add(part1);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Create a of the project in order to check that the project's sub objects where not updated
            CloneManager cloneManager = new CloneManager(dataContext);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateAdditionalCostViewModel.DevelopmentCostIncreasePercentage = 11;
            massUpdateAdditionalCostViewModel.PackagingCostIncreasePercentage = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestIncreasePercentage = 13;
            massUpdateAdditionalCostViewModel.OtherCostIncreasePercentage = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.AutoCalculateLogisticCost = true;
            massUpdateAdditionalCostViewModel.CurrentObject = project;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no sub object of the project was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Part and ApplyUpdatesToSubObjects = true.
        /// In this case the ApplyUpdatesToSubObjects flag is not important because the part doesn't have sub objects which have Additional Cost properties.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.DevelopmentCost = 10;
            part.PackagingCost = 20;
            part.ProjectInvest = 30;
            part.LogisticCost = 40;
            part.OtherCost = 50;
            part.PaymentTerms = 60;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Create a clone of the part in order to verify that the part was not modified
            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = part;
            massUpdateAdditionalCostViewModel.DevelopmentCostIncreasePercentage = 200;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the part's additional cost were updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));

            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Part, ApplyUpdatesToSubObjects = true and 
        /// Additional Cost Values expressed in percentages.
        /// In this case the ApplyUpdatesToSubObjects flag is not important because the part doesn't have sub objects which have Additional Cost properties.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.DevelopmentCost = 10;
            part.PackagingCost = 20;
            part.ProjectInvest = 30;
            part.LogisticCost = 40;
            part.OtherCost = 50;
            part.PaymentTerms = 60;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Create a clone of the part in order to verify that the part was not modified
            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DataContext = dataContext;
            massUpdateAdditionalCostViewModel.DevelopmentCostIncreasePercentage = 11;
            massUpdateAdditionalCostViewModel.PackagingCostIncreasePercentage = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestIncreasePercentage = 13;
            massUpdateAdditionalCostViewModel.OtherCostIncreasePercentage = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.AutoCalculateLogisticCost = true;
            massUpdateAdditionalCostViewModel.CurrentObject = part;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the part's additional cost were updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Assembly and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.DevelopmentCost = 10;
            assembly.PackagingCost = 20;
            assembly.ProjectInvest = 30;
            assembly.LogisticCost = 40;
            assembly.OtherCost = 50;
            assembly.PaymentTerms = 60;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.DevelopmentCost = 10;
            subassembly1.PackagingCost = 20;
            subassembly1.ProjectInvest = 30;
            subassembly1.LogisticCost = 40;
            subassembly1.OtherCost = 50;
            subassembly1.PaymentTerms = 60;
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.DevelopmentCost = 10;
            subassembly2.PackagingCost = 20;
            subassembly2.ProjectInvest = 30;
            subassembly2.LogisticCost = 40;
            subassembly2.OtherCost = 50;
            subassembly2.PaymentTerms = 60;
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.DevelopmentCost = 10;
            part1.PackagingCost = 20;
            part1.ProjectInvest = 30;
            part1.LogisticCost = 40;
            part1.OtherCost = 50;
            part1.PaymentTerms = 60;
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.DevelopmentCost = 10;
            part2.PackagingCost = 20;
            part2.ProjectInvest = 30;
            part2.LogisticCost = 40;
            part2.OtherCost = 50;
            part2.PaymentTerms = 60;
            assembly.Parts.Add(part2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Create a clone of the assembly1 in order to check that its sub objects were not updated
            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = assembly;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message

            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Assembly and ApplyUpdatesToSubObjects = false.
        /// Additional Cost Values expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.DevelopmentCost = 10;
            assembly.PackagingCost = 20;
            assembly.ProjectInvest = 30;
            assembly.LogisticCost = 40;
            assembly.OtherCost = 50;
            assembly.PaymentTerms = 60;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.DevelopmentCost = 10;
            subassembly1.PackagingCost = 20;
            subassembly1.ProjectInvest = 30;
            subassembly1.LogisticCost = 40;
            subassembly1.OtherCost = 50;
            subassembly1.PaymentTerms = 60;
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.DevelopmentCost = 10;
            subassembly2.PackagingCost = 20;
            subassembly2.ProjectInvest = 30;
            subassembly2.LogisticCost = 40;
            subassembly2.OtherCost = 50;
            subassembly2.PaymentTerms = 60;
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.DevelopmentCost = 10;
            part1.PackagingCost = 20;
            part1.ProjectInvest = 30;
            part1.LogisticCost = 40;
            part1.OtherCost = 50;
            part1.PaymentTerms = 60;
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.DevelopmentCost = 10;
            part2.PackagingCost = 20;
            part2.ProjectInvest = 30;
            part2.LogisticCost = 40;
            part2.OtherCost = 50;
            part2.PaymentTerms = 60;
            assembly.Parts.Add(part2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Create a clone of the assembly1 in order to check that its sub objects were not updated
            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DataContext = dataContext;
            massUpdateAdditionalCostViewModel.DevelopmentCostIncreasePercentage = 11;
            massUpdateAdditionalCostViewModel.PackagingCostIncreasePercentage = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestIncreasePercentage = 13;
            massUpdateAdditionalCostViewModel.LogisticCostIncreasePercentage = 14;
            massUpdateAdditionalCostViewModel.OtherCostIncreasePercentage = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = assembly;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Assembly and ApplyUpdatesToSubObjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest9()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.DevelopmentCost = 10;
            assembly.PackagingCost = 20;
            assembly.ProjectInvest = 30;
            assembly.LogisticCost = 40;
            assembly.OtherCost = 50;
            assembly.PaymentTerms = 60;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.DevelopmentCost = 10;
            subassembly1.PackagingCost = 20;
            subassembly1.ProjectInvest = 30;
            subassembly1.LogisticCost = 40;
            subassembly1.OtherCost = 50;
            subassembly1.PaymentTerms = 60;
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.DevelopmentCost = 10;
            subassembly2.PackagingCost = 20;
            subassembly2.ProjectInvest = 30;
            subassembly2.LogisticCost = 40;
            subassembly2.OtherCost = 50;
            subassembly2.PaymentTerms = 60;
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.DevelopmentCost = 10;
            part1.PackagingCost = 20;
            part1.ProjectInvest = 30;
            part1.LogisticCost = 40;
            part1.OtherCost = 50;
            part1.PaymentTerms = 60;
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.DevelopmentCost = 10;
            part2.PackagingCost = 20;
            part2.ProjectInvest = 30;
            part2.LogisticCost = 40;
            part2.OtherCost = 50;
            part2.PaymentTerms = 60;
            assembly.Parts.Add(part2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DataContext = dataContext;
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = assembly;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Assembly and ApplyUpdatesToSubObjects = true.
        /// Additional Cost Values expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest10()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.DevelopmentCost = 10;
            assembly.PackagingCost = 20;
            assembly.ProjectInvest = 30;
            assembly.LogisticCost = 40;
            assembly.OtherCost = 50;
            assembly.PaymentTerms = 60;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.DevelopmentCost = 10;
            subassembly1.PackagingCost = 20;
            subassembly1.ProjectInvest = 30;
            subassembly1.LogisticCost = 40;
            subassembly1.OtherCost = 50;
            subassembly1.PaymentTerms = 60;
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.DevelopmentCost = 10;
            subassembly2.PackagingCost = 20;
            subassembly2.ProjectInvest = 30;
            subassembly2.LogisticCost = 40;
            subassembly2.OtherCost = 50;
            subassembly2.PaymentTerms = 60;
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            part1.DevelopmentCost = 10;
            part1.PackagingCost = 20;
            part1.ProjectInvest = 30;
            part1.LogisticCost = 40;
            part1.OtherCost = 50;
            part1.PaymentTerms = 60;
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.DevelopmentCost = 10;
            part2.PackagingCost = 20;
            part2.ProjectInvest = 30;
            part2.LogisticCost = 40;
            part2.OtherCost = 50;
            part2.PaymentTerms = 60;
            assembly.Parts.Add(part2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DevelopmentCostIncreasePercentage = 11;
            massUpdateAdditionalCostViewModel.PackagingCostIncreasePercentage = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestIncreasePercentage = 13;
            massUpdateAdditionalCostViewModel.LogisticCostIncreasePercentage = 14;
            massUpdateAdditionalCostViewModel.OtherCostIncreasePercentage = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = assembly;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateAdditionalCostViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand with the following test data: Current Object -> Commodity and ApplyUpdatesToSubObjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateAdditionalCostTest11()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateAdditionalCostViewModel massUpdateAdditionalCostViewModel = new MassUpdateAdditionalCostViewModel(messenger, windowService);

            // Set up the current object 
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            // Set up the view model 
            massUpdateAdditionalCostViewModel.DevelopmentCostNewValue = 11;
            massUpdateAdditionalCostViewModel.PackagingCostNewValue = 12;
            massUpdateAdditionalCostViewModel.ProjectInvestNewValue = 13;
            massUpdateAdditionalCostViewModel.LogisticCostNewValue = 14;
            massUpdateAdditionalCostViewModel.OtherCostNewValue = 15;
            massUpdateAdditionalCostViewModel.PaymentTermsNewValue = 16;
            massUpdateAdditionalCostViewModel.CurrentObject = commodity;
            massUpdateAdditionalCostViewModel.DataContext = dataContext;

            // Set up the MessageBoxResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateAdditionalCostViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateAdditionalCostViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "No update should be performed if the current object doesn't have 'Additional Cost' properties.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verify if a project was updated.
        /// </summary>
        /// <param name="oldProject">The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateAdditionalCostViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    AdditionalCostsAreEqual(oldPart, part);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AdditionalCostsAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verifies if an assembly was updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateAdditionalCostViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (viewModel.DevelopmentCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.DevelopmentCostNewValue.GetValueOrDefault(), updatedAssembly.DevelopmentCost.GetValueOrDefault(), "Failed to update the DevelopmentCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.DevelopmentCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.DevelopmentCostIncreasePercentage.GetValueOrDefault() * oldAssembly.DevelopmentCost.GetValueOrDefault(), updatedAssembly.DevelopmentCost.GetValueOrDefault(), "Failed to update the DevelopmentCost with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldAssembly.DevelopmentCost.GetValueOrDefault(), updatedAssembly.DevelopmentCost.GetValueOrDefault(), "The DevelopmentCost should not have been updated if the new value was null.");
            }

            if (viewModel.PackagingCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingCostNewValue.GetValueOrDefault(), updatedAssembly.PackagingCost.GetValueOrDefault(), "Failed to update the PackagingCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.PackagingCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingCostIncreasePercentage.GetValueOrDefault() * oldAssembly.PackagingCost.GetValueOrDefault(), updatedAssembly.PackagingCost.GetValueOrDefault(), "Failed to update the PackagingCost with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldAssembly.PackagingCost.GetValueOrDefault(), updatedAssembly.PackagingCost.GetValueOrDefault(), "The PackagingCost should not have been updated if the new value was null.");
            }

            if (viewModel.ProjectInvestNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProjectInvestNewValue.GetValueOrDefault(), updatedAssembly.ProjectInvest.GetValueOrDefault(), "Failed to update the ProjectInvest with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ProjectInvestIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProjectInvestIncreasePercentage.GetValueOrDefault() * oldAssembly.ProjectInvest.GetValueOrDefault(), updatedAssembly.ProjectInvest.GetValueOrDefault(), "Failed to update the ProjectInvest with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldAssembly.ProjectInvest.GetValueOrDefault(), updatedAssembly.ProjectInvest.GetValueOrDefault(), "The ProjectInvest should not have been updated if the new value was null.");
            }

            if (viewModel.AutoCalculateLogisticCost)
            {
                Assert.IsTrue(updatedAssembly.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost.");
                objectUpdated = true;
            }
            else
            {
                if (viewModel.LogisticCostNewValue != null)
                {
                    Assert.AreEqual<decimal>(viewModel.LogisticCostNewValue.GetValueOrDefault(), updatedAssembly.LogisticCost.GetValueOrDefault(), "Failed to update the LogisticCost with the new value.");
                    Assert.IsFalse(updatedAssembly.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost flag.");
                    objectUpdated = true;
                }
                else if (viewModel.LogisticCostIncreasePercentage != null)
                {
                    Assert.AreEqual<decimal>(viewModel.LogisticCostIncreasePercentage.GetValueOrDefault() * oldAssembly.LogisticCost.GetValueOrDefault(), updatedAssembly.LogisticCost.GetValueOrDefault(), "Failed to update the LogisticCost with the increase percentage.");
                    Assert.IsFalse(updatedAssembly.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost flag.");
                    objectUpdated = true;
                }
                else
                {
                    Assert.AreEqual<decimal>(oldAssembly.LogisticCost.GetValueOrDefault(), updatedAssembly.LogisticCost.GetValueOrDefault(), "The ProjectInvest should not have been updated if the new value was null.");
                    Assert.AreEqual<bool>(oldAssembly.CalculateLogisticCost.GetValueOrDefault(), updatedAssembly.CalculateLogisticCost.GetValueOrDefault(), "The CalculateLogisticCost should not have been updated if the new value was null.");
                }
            }

            if (viewModel.OtherCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostNewValue.GetValueOrDefault(), updatedAssembly.OtherCost.GetValueOrDefault(), "Failed to update the OtherCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.OtherCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostIncreasePercentage.GetValueOrDefault() * oldAssembly.OtherCost.GetValueOrDefault(), updatedAssembly.OtherCost.GetValueOrDefault(), "Failed to update the OtherCost value with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldAssembly.OtherCost.GetValueOrDefault(), updatedAssembly.OtherCost.GetValueOrDefault(), "The OtherCost should not have been updated if the new value was null.");
            }

            if (viewModel.PaymentTermsNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PaymentTermsNewValue.GetValueOrDefault(), updatedAssembly.PaymentTerms, "Failed to update the PaymentTerms value with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldAssembly.PaymentTerms, updatedAssembly.PaymentTerms, "The PaymentTerms should not have been updated if the new value was null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    AdditionalCostsAreEqual(oldPart, part);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AdditionalCostsAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verifies if a part was updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateAdditionalCostViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (viewModel.DevelopmentCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.DevelopmentCostNewValue.GetValueOrDefault(), updatedPart.DevelopmentCost.GetValueOrDefault(), "Failed to update the DevelopmentCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.DevelopmentCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.DevelopmentCostIncreasePercentage.GetValueOrDefault() * oldPart.DevelopmentCost.GetValueOrDefault(), updatedPart.DevelopmentCost.GetValueOrDefault(), "Failed to update the DevelopmentCost with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldPart.DevelopmentCost.GetValueOrDefault(), updatedPart.DevelopmentCost.GetValueOrDefault(), "The DevelopmentCost should not have been updated if the new value was null.");
            }

            if (viewModel.PackagingCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingCostNewValue.GetValueOrDefault(), updatedPart.PackagingCost.GetValueOrDefault(), "Failed to update the PackagingCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.PackagingCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingCostIncreasePercentage.GetValueOrDefault() * oldPart.PackagingCost.GetValueOrDefault(), updatedPart.PackagingCost.GetValueOrDefault(), "Failed to update the PackagingCost with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldPart.PackagingCost.GetValueOrDefault(), updatedPart.PackagingCost.GetValueOrDefault(), "The PackagingCost should not have been updated if the new value was null.");
            }

            if (viewModel.ProjectInvestNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProjectInvestNewValue.GetValueOrDefault(), updatedPart.ProjectInvest.GetValueOrDefault(), "Failed to update the ProjectInvest with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ProjectInvestIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProjectInvestIncreasePercentage.GetValueOrDefault() * oldPart.ProjectInvest.GetValueOrDefault(), updatedPart.ProjectInvest.GetValueOrDefault(), "Failed to update the ProjectInvest with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldPart.ProjectInvest.GetValueOrDefault(), updatedPart.ProjectInvest.GetValueOrDefault(), "The ProjectInvest should not have been updated if the new value was null.");
            }

            if (viewModel.AutoCalculateLogisticCost)
            {
                Assert.IsTrue(updatedPart.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost.");
                objectUpdated = true;
            }
            else
            {
                if (viewModel.LogisticCostNewValue != null)
                {
                    Assert.AreEqual<decimal>(viewModel.LogisticCostNewValue.GetValueOrDefault(), updatedPart.LogisticCost.GetValueOrDefault(), "Failed to update the LogisticCost with the new value.");
                    Assert.IsFalse(updatedPart.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost flag.");
                    objectUpdated = true;
                }
                else if (viewModel.LogisticCostIncreasePercentage != null)
                {
                    Assert.AreEqual<decimal>(viewModel.LogisticCostIncreasePercentage.GetValueOrDefault() * oldPart.LogisticCost.GetValueOrDefault(), updatedPart.LogisticCost.GetValueOrDefault(), "Failed to update the LogisticCost with the increase percentage.");
                    Assert.IsFalse(updatedPart.CalculateLogisticCost.GetValueOrDefault(), "Failed to update the CalculateLogisticCost flag.");
                    objectUpdated = true;
                }
                else
                {
                    Assert.AreEqual<decimal>(oldPart.LogisticCost.GetValueOrDefault(), updatedPart.LogisticCost.GetValueOrDefault(), "The ProjectInvest should not have been updated if the new value was null.");
                    Assert.AreEqual<bool>(oldPart.CalculateLogisticCost.GetValueOrDefault(), updatedPart.CalculateLogisticCost.GetValueOrDefault(), "The CalculateLogisticCost should not have been updated if the new value was null.");
                }
            }

            if (viewModel.OtherCostNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostNewValue.GetValueOrDefault(), updatedPart.OtherCost.GetValueOrDefault(), "Failed to update the OtherCost with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.OtherCostIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostIncreasePercentage.GetValueOrDefault() * oldPart.OtherCost.GetValueOrDefault(), updatedPart.OtherCost.GetValueOrDefault(), "Failed to update the OtherCost value with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldPart.OtherCost.GetValueOrDefault(), updatedPart.OtherCost.GetValueOrDefault(), "The OtherCost should not have been updated if the new value was null.");
            }

            if (viewModel.PaymentTermsNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PaymentTermsNewValue.GetValueOrDefault(), updatedPart.PaymentTerms, "Failed to update the PaymentTerms value with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldPart.PaymentTerms, updatedPart.PaymentTerms, "The PaymentTerms should not have been updated if the new value was null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verify if the Additional Costs of two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        public void AdditionalCostsAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            bool result = false;
            result = comparedAssembly.DevelopmentCost == assemblyToCompareWith.DevelopmentCost &&
                         comparedAssembly.PackagingCost == assemblyToCompareWith.PackagingCost &&
                         comparedAssembly.ProjectInvest == assemblyToCompareWith.ProjectInvest &&
                         comparedAssembly.OtherCost == assemblyToCompareWith.OtherCost &&
                         comparedAssembly.PaymentTerms == assemblyToCompareWith.PaymentTerms &&
                         comparedAssembly.CalculateLogisticCost == assemblyToCompareWith.CalculateLogisticCost;
            if (!comparedAssembly.CalculateLogisticCost.GetValueOrDefault())
            {
                result = result && comparedAssembly.LogisticCost == assemblyToCompareWith.LogisticCost;
            }

            Assert.IsTrue(result, "The Additional Settings of the assemblies are not equal.");
            foreach (Part comparedPart in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == comparedPart.Name);
                AdditionalCostsAreEqual(comparedPart, partToCompareWith);
            }

            foreach (Assembly comparedSubassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == comparedSubassembly.Name);
                AdditionalCostsAreEqual(comparedSubassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verify if the AdditionalCosts of two entities(Assemblies or Parts) are equal.
        /// </summary>
        /// <param name="comparedEntity">The compared entity.</param>
        /// <param name="entityToCompareWith">The entity to compare with.</param>
        /// <returns>True if the Additional Costs of entities are equal, false otherwise.</returns>
        private void AdditionalCostsAreEqual(Part comparedPart, Part partToCompareWith)
        {
            bool result = false;
            result = comparedPart.DevelopmentCost == partToCompareWith.DevelopmentCost &&
                     comparedPart.PackagingCost == partToCompareWith.PackagingCost &&
                     comparedPart.ProjectInvest == partToCompareWith.ProjectInvest &&
                     comparedPart.OtherCost == partToCompareWith.OtherCost &&
                     comparedPart.PaymentTerms == partToCompareWith.PaymentTerms &&
                     comparedPart.CalculateLogisticCost == partToCompareWith.CalculateLogisticCost;
            if (!comparedPart.CalculateLogisticCost.GetValueOrDefault())
            {
                result = result && comparedPart.LogisticCost == partToCompareWith.LogisticCost;
            }

            Assert.IsTrue(result, "The Additional Costs of the parts are not equal.");
        }

        #endregion Helpers
    }
}
