﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for SendErrorReportViewModel and is intended 
    /// to contain unit tests for all SendErrorReportViewModel commands
    /// </summary>
    [TestClass]
    public class SendErrorReportViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "SendErrorReportViewModelTest"/> class.
        /// </summary>
        public SendErrorReportViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests SubmitReport command
        ///// </summary>
        //[TestMethod]
        //public void SubmitReportTest()
        //{
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    Exception e = new Exception();
        //    SendErrorReportViewModel sendErrorReportViewModel = new SendErrorReportViewModel(windowService, e);

        //    ICommand submitReportCommand = sendErrorReportViewModel.SubmitReport;
        //    submitReportCommand.Execute(null);
        //}

        /// <summary>
        /// A test for CloseScreen command.
        /// </summary>
        [TestMethod]
        public void CloseScreenTest()
        {
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            Exception ex = new Exception();
            SendErrorReportViewModel sendErrorReportViewModel = new SendErrorReportViewModel(windowService, ex);

            windowService.IsOpen = true;
            ICommand closeScreenCommand = sendErrorReportViewModel.CloseScreen;
            closeScreenCommand.Execute(null);

            // Verify that the screen was closed
            Assert.IsFalse(windowService.IsOpen, "Failed to close the SendErrorReport view.");
        }
    }
}