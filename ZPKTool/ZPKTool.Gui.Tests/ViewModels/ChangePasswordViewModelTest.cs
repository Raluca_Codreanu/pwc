﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ChangePasswordViewModel and is intended 
    /// to contain unit tests for all public methods from ChangePasswordViewModel class.
    /// </summary>
    [TestClass]
    public class ChangePasswordViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for the change password command, with valid current password and matching new and confirm passwords.
        /// </summary>
        [TestMethod]
        public void ChangePasswordWithSuccesTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            changePasswordViewModel.OldPassword = "Password.";
            changePasswordViewModel.NewPassword = "Newpassword.";
            changePasswordViewModel.IsPasswordValid = true;
            changePasswordViewModel.ConfirmNewPassword = "Newpassword.";

            changePasswordViewModel.ChangePasswordCommand.CanExecute(null);
            changePasswordViewModel.ChangePasswordCommand.Execute(null);

            string encryptedNewPassword = EncryptionManager.Instance.HashSHA256("Newpassword.", currentUser.Salt, currentUser.Guid.ToString());

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetByKey(currentUser.Guid);
            Assert.AreEqual(encryptedNewPassword, user.Password, "The password was not changed");
        }

        /// <summary>
        /// A test for the change password command, with invalid current password.
        /// It also checks after each password box is filled, if the change password command can be executed.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ChangePasswordWithInvalidCurrentPasswordTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            changePasswordViewModel.OldPassword = "Wrong";
            changePasswordViewModel.ChangePasswordCommand.CanExecute(null);
            changePasswordViewModel.NewPassword = "Newpassword.";
            changePasswordViewModel.IsPasswordValid = true;
            changePasswordViewModel.ChangePasswordCommand.CanExecute(null);
            changePasswordViewModel.ConfirmNewPassword = "Newpassword.";
            changePasswordViewModel.ChangePasswordCommand.CanExecute(null);

            changePasswordViewModel.ChangePasswordCommand.Execute(null);

            string encryptedNewPassword = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetByKey(currentUser.Guid);
            Assert.AreNotEqual(encryptedNewPassword, user.Password, "The password was changed");
        }

        /// <summary>
        /// A test for the change password command, with valid current password, but different new and confirm passwords.
        /// </summary>
        [TestMethod]
        public void ChangePasswordWithInvalidConfirmedNewPasswordTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            changePasswordViewModel.OldPassword = "Password.";
            changePasswordViewModel.NewPassword = "Newpassword.";
            changePasswordViewModel.IsPasswordValid = true;
            changePasswordViewModel.ConfirmNewPassword = "Wrongnewpassword.";

            changePasswordViewModel.ChangePasswordCommand.Execute(null);

            string encryptedNewPassword = EncryptionManager.Instance.HashSHA256("Newpassword.", currentUser.Salt, currentUser.Guid.ToString());

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetByKey(currentUser.Guid);
            Assert.AreNotEqual(encryptedNewPassword, user.Password, "The password was changed");
        }

        /// <summary>
        /// A test for the change password command, with valid current password, invalid new password, but matching new and confirm passwords.
        /// </summary>
        [TestMethod]
        public void ChangePasswordWithInvalidNewPasswordTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            changePasswordViewModel.OldPassword = "Password.";
            changePasswordViewModel.NewPassword = "Test1234!";
            changePasswordViewModel.IsPasswordValid = false;
            changePasswordViewModel.ConfirmNewPassword = "Test1234!";

            var canChangePassword = changePasswordViewModel.ChangePasswordCommand.CanExecute(null);

            Assert.IsFalse(canChangePassword);
        }

        /// <summary>
        /// A test for the cancel command, when the user confirms it.
        /// It also tests the OnUnloading method, where handleOnUnloading is set to false (to avoid handling OnUnloading).
        /// The test checks the event when user completes only one, two or all three password boxes.
        /// </summary>
        [TestMethod]
        public void CancelCommandConfirmedAndUnloadingNotHandledTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;
            centralUser.Roles = localUser.Roles;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            changePasswordViewModel.OldPassword = "Password.";
            changePasswordViewModel.CancelCommand.Execute(null);

            changePasswordViewModel.NewPassword = "Newpassword.";
            changePasswordViewModel.IsPasswordValid = true;
            changePasswordViewModel.CancelCommand.Execute(null);

            changePasswordViewModel.ConfirmNewPassword = "Newpassword.";
            changePasswordViewModel.CancelCommand.Execute(null);

            string encryptedNewPassword = EncryptionManager.Instance.HashSHA256("Newpassword.", localUser.Salt, localUser.Guid.ToString());

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetByKey(currentUser.Guid);

            var unloading = changePasswordViewModel.OnUnloading();
            Assert.AreNotEqual(encryptedNewPassword, user.Password, "The password was changed");
            Assert.IsTrue(unloading);
        }

        /// <summary>
        /// A test for the cancel command, when the user does not confirm it.
        /// </summary>
        [TestMethod]
        public void CancelCommandNotConfirmedTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            // The Login method uses local db, and the ChangePassword method uses central db; the user will be added in both of them.
            IDataSourceManager localDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IDataSourceManager centralDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User localUser = new User();
            localUser.Name = "User";
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.Admin;

            localDataContext.UserRepository.Add(localUser);
            localDataContext.SaveChanges();

            User centralUser = new User();
            localUser.CopyValuesTo(centralUser);
            centralUser.Guid = localUser.Guid;

            centralDataContext.UserRepository.Add(centralUser);
            centralDataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(centralUser.Username, "Password.");

            changePasswordViewModel.OldPassword = "Password.";
            changePasswordViewModel.NewPassword = "Newpassword.";
            changePasswordViewModel.IsPasswordValid = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            changePasswordViewModel.CancelCommand.Execute(null);

            string encryptedNewPassword = EncryptionManager.Instance.HashSHA256("Newpassword.", currentUser.Salt, currentUser.Guid.ToString());

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User user = newDataContext.UserRepository.GetByKey(currentUser.Guid);
            Assert.AreNotEqual(encryptedNewPassword, user.Password, "The password was changed");
        }

        /// <summary>
        /// A test for the cancel command, when the user does not complete any of the password boxes.
        /// </summary>
        [TestMethod]
        public void CancelCommandWithEmptyBoxesTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            changePasswordViewModel.CancelCommand.Execute(null);
        }

        /// <summary>
        /// A test for the OnUnloading event, handled successfully and user confirms quitting.
        /// The test checks the event when user completes only one, two or all three password boxes.
        /// </summary>
        [TestMethod]
        public void OnUnloadingHandledWithConfirmedQuitTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            changePasswordViewModel.OldPassword = "Password.";
            var unloading1 = changePasswordViewModel.OnUnloading();

            changePasswordViewModel.NewPassword = "Newpassword.";
            var unloading2 = changePasswordViewModel.OnUnloading();

            changePasswordViewModel.ConfirmNewPassword = "Newpassword.";
            var unloading3 = changePasswordViewModel.OnUnloading();

            Assert.IsTrue(unloading1, "The OnUnloading event was not handled.");
            Assert.IsTrue(unloading2, "The OnUnloading event was not handled.");
            Assert.IsTrue(unloading3, "The OnUnloading event was not handled.");
        }

        /// <summary>
        /// A test for the OnUnloading event when user does not confirm quitting.
        /// </summary>
        [TestMethod]
        public void OnUnloadingHandledWithNotConfirmedQuitTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            changePasswordViewModel.OldPassword = "passwd";
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var unloading = changePasswordViewModel.OnUnloading();
            Assert.IsFalse(unloading, "The OnUnloading event was handled and the window closed.");
        }

        /// <summary>
        /// A test for the OnUnloading event handled successfully when the user does not complete any of the password boxes.
        /// </summary>
        [TestMethod]
        public void OnUnloadingHandledWithEmptyBoxesTest()
        {
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            ChangePasswordViewModel changePasswordViewModel = new ChangePasswordViewModel(windowService);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var unloading = changePasswordViewModel.OnUnloading();
            Assert.IsTrue(unloading, "The OnUnloading event was not handled.");
        }

        #endregion Test Methods
    }
}
