﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.LicenseGenerator;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for RegisterViewModel and is intended 
    /// to contain all RegisterViewModel Unit Test
    /// </summary>
    [TestClass]
    public class RegisterViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterViewModelTest"/> class.
        /// </summary>
        public RegisterViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        /// <summary>
        /// Tests Select Path Command using valid input parameters
        /// </summary>
        [TestMethod]
        public void SelectValidPathTest()
        {
            CompositionContainer container = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock wndService = new WindowServiceMock(messageBoxService, dispatcherService);
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            RegisterViewModel registerViewModel = new RegisterViewModel(container, wndService, fileDialogService);

            // Creates a file on the disk
            string path = Path.GetTempFileName();
            fileDialogService.FileName = path;

            try
            {
                // Execute SelectPath command 
                ICommand selectPathCommand = registerViewModel.SelectPathCommand;
                selectPathCommand.Execute(null);

                Assert.AreEqual<string>(fileDialogService.FileName, registerViewModel.FilePath, "Failed to select the path.");
            }
            finally
            {
                // Delete the created temp file
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// Tests Select Path command using invalid input parameters - a inexistent path.
        /// </summary>
        [TestMethod]
        public void SelectInvalidPathTest()
        {
            CompositionContainer container = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock wndService = new WindowServiceMock(messageBoxService, dispatcherService);
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            RegisterViewModel registerViewModel = new RegisterViewModel(container, wndService, fileDialogService);

            fileDialogService.FileName = "MockPath";
            messageBoxService.MessageDialogResult = MessageDialogResult.OK;
            messageBoxService.ShowCallCount = 0;

            // Execute SelectPath command 
            ICommand selectPathCommand = registerViewModel.SelectPathCommand;
            selectPathCommand.Execute(null);

            Assert.AreEqual<int>(1, messageBoxService.ShowCallCount, "Invalid number of calls of show message box method");
        }

        /// <summary>
        /// Tests Activate License Command using valid input parameters
        /// </summary>
        //[TestMethod]
        //        public void ActivateValidLicenseTest()
        //        {
        //            CompositionContainer container = bootstrapper.CompositionContainer;
        //            MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //            IFileDialogService fileDialogService = new FileDialogServiceMock();
        //            RegisterViewModel registerViewModel = new RegisterViewModel(container, windowService, fileDialogService);
        //            string privateKey = @"<RSAKeyValue>
        //                                  <Modulus>wo57J1sP+0D2HnWNhvDtyIjcVFUUii7XlwWpebCRFKaiBhWd2w1ublkGRxHWDvlvVakOc47RRV8Od4XIz+c1qw+1hEq47JhRJ/S7UO3ps2REz70OkeRLfj7Iq7f89z79UKnB8iVAN+9UfhG3rWTVRKaH/svxTjnKgx0c5kD2otbi1LAGNiWhzggPDafcCn0Rm+sIWq74ddj1Dvllt8DX2zvMHteXeOl4t36weprKxT5PkVDJ+06WPlDjE5qv8Q3N2mxM6shKorIZMZs5BAdyZr8gXV72bGEBnwbdHZGv04mbBNBtuZVvwv/FkNWakyjW+SAI0Kf+GG2DMJC1+pivkQ==</Modulus>
        //                                  <Exponent>AQAB</Exponent>
        //                                  <P>6YADD3OH4kms6A+dEq0pXFmNC5ucm8OClRAoMZRFYrGziLlw8h6dWGXem44Sg1PBvxGcPGpRkTV0VrqVAWI8ZEpDHGeEcp6OFMp/mxUus5GAAc9Vbs5sd2HYX0VfLna2o/JRZuTCde8rgsN44gqt7AoA9f+IStCNaoNwTL8owIM=</P>
        //                                  <Q>1U3PU2ZNfgfgEnZ7DlC6EjsFvVdwbegsIgkhgrS0QZhcJhlmgfphTD8GE3JxueD751qjQcaGtnD3/pA0SdM6iOLCwvDc+ftY6zIx1GvMhGFjCCnUKPMQfHkxuSLit1MN7ItMFrdvKS/U5RyYvPt6p5eejSE9LVEVUDSrexLM61s=</Q>
        //                                  <DP>h0HjA/FNdlRtN2PL/gSWn+sz29SBTG1dWBGq5sVXyArdz8Zz2ZzIUmt4Hq2WpcI/yfleglSn+TB4usCMDegkyYeHBo4kV+zmqr5+WoxTqCsRdnrupygmBqWEG/PoZdbYh3GXm73Js10e3Dl07aN4CfHlippCDexu7pNC2wIkjhc=</DP>
        //                                  <DQ>Y53uOKXrhPOKmzTILZxX+hGkkZPXhwzKJ68Z/mAEWyobxxNPw146uJAqd0SYhlXjb13uJtbfrS6/Mx0Hvn6utKUqjj5NBVZ0ZYtQqEMcBasxw+s9EU7LIUa8iRkYxsM8Gn/HrkhG2vZiuAmN75yG4OQ3gPPIDXTzvjyK5XSXTUc=</DQ>
        //                                  <InverseQ>Vn/DWXyonXU6cj3G+2X6dspQuXzlRlz441p4m2GbjPz9zK2TfDpfN3daxGinTsaqQYPXKt2FHRn4/0NbH4bJHQRd6KsIr4MMkDY81q1RbEf9UOrNpRMZj+F170gq2D4C/mVfQmVmiXA/mQhQn85ODUYyOtOKW+e6FgUmydo87ws=</InverseQ>
        //                                  <D>bzuAw653g6HPVPjGcpj6icVdmsWN2bcQQWLWUyGUwObICYyAVMXoFiTFxwo2hRctBR+DiKPll7DSF5mghuTctTFlCP6jnXk0JRDxq0Jqc8rm4JJpaxXH56TDnBG0CjO0JlL39JZBkfk+KFm+1t6oY/iGFjHJg3zdAHs7k5UerVgIofhDtrqXxjCkOyD8wyZvo/pbPcfCO0lNZVMOsZQoMl6zXUlS8DrVBtGP+Jo+U5iLnAq4d0eOhZ8BRQgafHm1eM3v5Vz0orhjb/Id6NOr86OoFaDBwBhkmmHAoRy+GTnTXBrOVy9DERAmI7LY9fNpjPSI+lcbWk7zsJLBuvAAZQ==</D>
        //                                  </RSAKeyValue>";

        //            // If exist a license file move it into a temp folder
        //            string actualLicensePath = Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, "License.lic");
        //            string tempLocation = Path.Combine(Path.GetTempPath(), "License.lic");
        //            if (File.Exists(actualLicensePath))
        //            {
        //                if (File.Exists(tempLocation))
        //                {
        //                    Utils.DeleteFile(tempLocation);
        //                }

        //                File.Move(actualLicensePath, tempLocation);
        //            }

        //            // Generate a new license 
        //            string newLicensePath = Path.GetTempFileName();
        //            LicenseManagerBackend.License license = new LicenseManagerBackend.License();
        //            license.FileName = newLicensePath;
        //            license.ApplicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        //            license.Distributor = new Distributor();
        //            license.Distributor.Name = "Distributor";
        //            license.StartingDate = DateTime.Now;
        //            license.ExpirationDate = license.StartingDate.AddMonths(2);
        //            license.Level = 1;
        //            license.SerialNumber = SerialNumberManager.GenerateSerialNumber().ToUpperInvariant();
        //            license.Type = (short)PROimpens.LicenseGenerator.LicenseType.Nodelocked;
        //            license.User = new PROimpens.LicenseManagerBackend.User();
        //            var generator = new LicensingGenerator(privateKey);
        //            var key = generator.Generate(license);

        //            // Save the generated license into a file 
        //            string EncryptionRgbIV = "ruojvlzmdalyglrj";
        //            string EncryptionKey = "hcxilkqbbhczfeultgbskdmaunivmfuo";
        //            string encryptedData = string.Empty;
        //            encryptedData = LicenseGenerator.Encrypt.EncryptString(key, EncryptionRgbIV, EncryptionKey);
        //            if (newLicensePath != null && encryptedData != null)
        //            {
        //                File.WriteAllText(newLicensePath, encryptedData);
        //            }

        //            AbstractLicenseValidator.InitializeNISTDate();
        //            registerViewModel.FilePath = newLicensePath;

        //            try
        //            {
        //                messageBoxService.ShowCallCount = 0;
        //                windowService.IsOpen = true;
        //                ICommand activateLicenseCommand = registerViewModel.ActivateLicenseCommand;
        //                activateLicenseCommand.Execute(null);

        //                Assert.IsTrue(messageBoxService.ShowCallCount == 1, "Activate successfully message was not displayed.");
        //                Assert.IsFalse(windowService.IsOpen, "Failed to close the window after activation.");
        //            }
        //            finally
        //            {
        //                // Remove the created license files - from temp file and from AppData folder 
        //                Utils.DeleteFile(newLicensePath);
        //                Utils.DeleteFile(actualLicensePath);

        //                // Restore the previous license file
        //                if (File.Exists(tempLocation))
        //                {
        //                    File.Move(tempLocation, actualLicensePath);
        //                }
        //            }
        //        }

        /// <summary>
        /// Tests Copy To Clipboard Command using valid input parameters
        /// </summary>
        [TestMethod]
        public void CopyToClipboardCommandTest()
        {
            CompositionContainer container = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock wndService = new WindowServiceMock(messageBoxService, dispatcherService);
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            RegisterViewModel registerViewModel = new RegisterViewModel(container, wndService, fileDialogService);

            // Generate the serial number for the license
            registerViewModel.SerialNumber = SerialNumberManager.GenerateSerialNumber();

            ICommand copyToClipboardCommand = registerViewModel.CopyToClipboardCommand;
            copyToClipboardCommand.Execute(null);

            Assert.AreEqual<string>(registerViewModel.SerialNumber, Clipboard.GetText(), "Failed to copy to clipboard the serial number.");
        }

        /// <summary>
        /// A test for CloseCommand.
        /// </summary>
        [TestMethod]
        public void CloseTest()
        {
            CompositionContainer container = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            RegisterViewModel registerViewModel = new RegisterViewModel(container, windowService, fileDialogService);

            windowService.IsOpen = true;
            ICommand closingCommand = registerViewModel.CloseCommand;
            closingCommand.Execute(null);

            // Verify if the window was closed
            Assert.IsFalse(windowService.IsOpen, "Failed to close the RegisterView.");
        }

        /// <summary>
        /// Tests RequestLicenseByEmail command using valid input parameters
        /// </summary>
        [TestMethod]
        public void RequestLicenseByEmailView()
        {
            CompositionContainer container = new CompositionContainer();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            IFileDialogService fileDialogService = new FileDialogServiceMock();
            RegisterViewModel registerViewModel = new RegisterViewModel(container, windowService, fileDialogService);

            windowService.IsOpen = false;
            ICommand requestLicenseByEmail = registerViewModel.RequestLicenseByEmailCommand;
            requestLicenseByEmail.Execute(null);

            // Verify that the LicenseRequest view is displayed
            Assert.IsTrue(windowService.IsOpen, "Failed to display the LicenseRequest view.");
            Assert.IsTrue(windowService.DisplayedView.GetType() == typeof(LicenseRequestViewModel), "The displayed view is not the LicenseRequest view.");
        }
    }
}
