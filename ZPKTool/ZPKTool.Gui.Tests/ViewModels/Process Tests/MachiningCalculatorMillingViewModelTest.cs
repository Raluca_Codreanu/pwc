﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorMillingViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorMillingViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorMillingViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);

            millingViewModel.EditMode = ViewModelEditMode.Create;
            millingViewModel.IsExpertMode = true;
            millingViewModel.SelectedMaterial = millingViewModel.MillingMaterials[5];
            millingViewModel.ProcessDescription = "description";
            millingViewModel.SelectedMillingToolType = MillingToolType.MillingSaw;
            millingViewModel.FeedSpeed = 200;
            millingViewModel.SelectedMillingType = MillingType.FineMachining;
            millingViewModel.CutDepth = 20;
            millingViewModel.TeethNumber = 20;
            millingViewModel.MillingToolDiameter = 20;
            millingViewModel.MillingToolWidth = 20;
            millingViewModel.StartStopTime = 2;
            millingViewModel.FeedLength = 20;
            millingViewModel.MillingLength = 20;
            millingViewModel.MillingWidth = 200;
            millingViewModel.MillingDepth = 200;
            millingViewModel.ToolChangeTime = 200;
            millingViewModel.TurningSpeed = 200;
            millingViewModel.FeedRate = 200;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, millingViewModel.MessengerTokenForMachining);

            millingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(millingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual("Milling - " + MillingToolType.MillingSaw, message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(800000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.Milling, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual(200m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(0.16m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(200, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual("description", message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, millingViewModel.MessengerTokenForMachining);

            string data = "10;5;200;2;20;20;20;20;2;20;20;200;200;True;200;200;200";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.Milling;

            millingViewModel.EditMode = ViewModelEditMode.Edit;
            millingViewModel.Model = calculationItemToEdit;
            millingViewModel.MillingToolDiameter = 500;
            millingViewModel.MillingDepth = 10;
            millingViewModel.SaveCommand.Execute(null);

            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(millingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(40000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(20m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(0.02m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(200, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the resulting cycle and milling time are updated after some values are changed.
        /// It also checks if the tool width text box enabled property changes according to the selected milling tool type.
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);

            var millingTime1 = millingViewModel.ResultingMillingTime;
            var cycleTime1 = millingViewModel.ResultingCycleTime;

            millingViewModel.CutDepth = 9;

            var millingTime2 = millingViewModel.ResultingMillingTime;
            var cycleTime2 = millingViewModel.ResultingCycleTime;

            millingViewModel.SelectedMillingToolType = MillingToolType.ShellEndMill;

            var millingTime3 = millingViewModel.ResultingMillingTime;
            var cycleTime3 = millingViewModel.ResultingCycleTime;

            Assert.AreNotEqual(millingTime1, millingTime2, "The milling time value was not updated.");
            Assert.AreNotEqual(cycleTime1, cycleTime2, "The cycle time value was not updated.");

            Assert.AreNotEqual(millingTime2, millingTime3, "The milling time value was not updated.");
            Assert.AreNotEqual(cycleTime2, cycleTime3, "The cycle time value was not updated.");
            Assert.IsTrue(millingViewModel.IsToolWidthTextBoxEnabled, "The tool width text box for ShellEndMill is disabled.");

            millingViewModel.SelectedMillingToolType = MillingToolType.SideMillCutter;
            Assert.IsFalse(millingViewModel.IsToolWidthTextBoxEnabled, "The tool width text box for SideMillCutter is enabled.");
        }

        /// <summary>
        /// A test for the SaveMillingResultDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveMillingResultDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);

            millingViewModel.EditMode = ViewModelEditMode.Create;
            millingViewModel.IsExpertMode = false;
            millingViewModel.SelectedMaterial = millingViewModel.MillingMaterials[1];
            millingViewModel.SelectedMillingToolType = MillingToolType.MillingHead;
            millingViewModel.FeedSpeed = 10;
            millingViewModel.SelectedMillingType = MillingType.FineMachining;
            millingViewModel.CutDepth = 20;
            millingViewModel.TeethNumber = 3;
            millingViewModel.MillingToolDiameter = 40;
            millingViewModel.MillingToolWidth = 50;
            millingViewModel.StartStopTime = 6;
            millingViewModel.FeedLength = 70;
            millingViewModel.MillingLength = 80;
            millingViewModel.MillingWidth = 90;
            millingViewModel.MillingDepth = 100;
            millingViewModel.ToolChangeTime = 110;

            millingViewModel.SaveCommand.Execute(null);

            string expectedData = "18;2;10;2;20;3;40;50;6;70;80;90;100;False;0.0856;110;1592";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = millingViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}
