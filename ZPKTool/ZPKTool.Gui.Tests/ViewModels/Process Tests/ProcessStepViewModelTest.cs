﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// this class is used to test the ProcessStepViewModel
    /// </summary>
    [TestClass]
    public class ProcessStepViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The measurement Unit.
        /// </summary>
        private Collection<MeasurementUnit> priceUnitBaseItems;

        /// <summary>
        /// The Test Context 
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The Composition Container
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The Please Wait service.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The Units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The model browser helper service
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The media view model
        /// </summary>
        private MediaViewModel mediaViewModel;

        /// <summary>
        /// The type selector view model.
        /// </summary>
        private ClassificationSelectorViewModel typeSelectorViewModel;

        /// <summary>
        /// The cost recalculation clone manage
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate text context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            this.container = new CompositionContainer();
            this.messenger = new Messenger();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.pleaseWaitService = new PleaseWaitService(new DispatcherService());
            this.unitsService = new UnitsService();
            this.fileDialogService = new FileDialogServiceMock();
            var videoViewModel = new VideoViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            this.mediaViewModel = new MediaViewModel(videoViewModel, picturesViewModel, this.messenger, this.windowService, this.fileDialogService, this.modelBrowserHelperService);
            this.typeSelectorViewModel = new ClassificationSelectorViewModel();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();
            this.priceUnitBaseItems = new Collection<MeasurementUnit>();
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region Test Methods
        /// <summary>
        /// This method tests the information Tab 
        /// </summary>
        [TestMethod]
        public void ProcessStepInformationTabTest()
        {
            var processStepVm = new ProcessStepViewModel(
            this.windowService,
            this.messenger,
            this.container,
            this.pleaseWaitService,
            this.modelBrowserHelperService,
            this.costRecalculationCloneManager,
            this.mediaViewModel,
            this.typeSelectorViewModel,
            this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var processStep = this.CreateNewProcessStep(localDataManager);

            var newUser = this.CreateNewUser(localDataManager);

            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            processStepVm.DataSourceManager = localDataManager;
            processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
            processStepVm.Model = processStep;
            processStepVm.InitializeForCreation();

            processStepVm.Accuracy.Value = ProcessCalculationAccuracy.Estimated;
            processStepVm.ProcessTime.Value = 10;
            processStepVm.ProcessTimeUnit.Value = measurementAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Time);
            processStepVm.CycleTime.Value = 10;
            processStepVm.CycleTimeUnit.Value = measurementAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Time);
            processStepVm.PartsPerCycle.Value = 10;
            processStepVm.ScrapAmount.Value = 10;
            processStepVm.SetupsPerBatch.Value = 100;
            processStepVm.MaxDownTime.Value = 1000000000;
            processStepVm.MaxDownTimeUnit.Value = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Time).FirstOrDefault(u => u.ScaleFactor == 1m);
            processStepVm.SetupTime.Value = 1000000000;
            processStepVm.SetupTimeUnit.Value = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Time).FirstOrDefault(u => u.ScaleFactor == 1m);
            processStepVm.BatchSize.Value = 100;
            processStepVm.ManufacturingOverhead.Value = 100;
            processStepVm.ShiftsPerWeek.Value = 10;
            processStepVm.ProductionDaysPerWeek.Value = 5;
            processStepVm.ExceedShiftCost.Value = true;
            processStepVm.ExtraShiftsNumber.Value = 10;
            processStepVm.ShiftCostExceedRatio.Value = 10;
            processStepVm.HoursPerShift.Value = 100;
            processStepVm.ProductionWeeksPerYear.Value = 100;
            processStepVm.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var saveCommand = processStepVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(processStep.Name, processStepVm.Name.Value);
            Assert.AreEqual(processStep.Accuracy, processStepVm.Accuracy.Value);
            Assert.AreEqual(processStep.ProcessTime, processStepVm.ProcessTime.Value);
            Assert.AreEqual(processStep.ProcessTimeUnit, processStepVm.ProcessTimeUnit.Value);
            Assert.AreEqual(processStep.CycleTime, processStepVm.CycleTime.Value);
            Assert.AreEqual(processStep.CycleTimeUnit, processStepVm.CycleTimeUnit.Value);
            Assert.AreEqual(processStep.PartsPerCycle, processStepVm.PartsPerCycle.Value);
            Assert.AreEqual(processStep.ScrapAmount, processStepVm.ScrapAmount.Value);
            Assert.AreEqual(processStep.SetupsPerBatch, processStepVm.SetupsPerBatch.Value);
            Assert.AreEqual(processStep.MaxDownTime, processStepVm.MaxDownTime.Value);
            Assert.AreEqual(processStep.MaxDownTimeUnit, processStepVm.MaxDownTimeUnit.Value);
            Assert.AreEqual(processStep.SetupTime, processStepVm.SetupTime.Value);
            Assert.AreEqual(processStep.SetupTimeUnit, processStepVm.SetupTimeUnit.Value);
            Assert.AreEqual(processStep.BatchSize, processStepVm.BatchSize.Value);
            Assert.AreEqual(processStep.ManufacturingOverhead, processStepVm.ManufacturingOverhead.Value);
            Assert.AreEqual(processStep.ShiftsPerWeek, processStepVm.ShiftsPerWeek.Value);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, processStepVm.ProductionDaysPerWeek.Value);
            Assert.AreEqual(processStep.ExceedShiftCost, processStepVm.ExceedShiftCost.Value);
            Assert.AreEqual(processStep.ExtraShiftsNumber, processStepVm.ExtraShiftsNumber.Value);
            Assert.AreEqual(processStep.ShiftCostExceedRatio, processStepVm.ShiftCostExceedRatio.Value);
            Assert.AreEqual(processStep.HoursPerShift, processStepVm.HoursPerShift.Value);
            Assert.AreEqual(processStep.ProductionWeeksPerYear, processStepVm.ProductionWeeksPerYear.Value);
            Assert.AreEqual(processStep.Description, processStepVm.Description.Value);
        }

        /// <summary>
        /// This method tests the labourSettings Tab 
        /// </summary>
        [TestMethod]
        public void ProcessStepTest_labourSettingsTab()
        {
            var processStepVm = new ProcessStepViewModel(
            this.windowService,
            this.messenger,
            this.container,
            this.pleaseWaitService,
            this.modelBrowserHelperService,
            this.costRecalculationCloneManager,
            this.mediaViewModel,
            this.typeSelectorViewModel,
            this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var processStep = this.CreateNewProcessStep(localDataManager);
            var newUser = this.CreateNewUser(localDataManager);
            processStepVm.DataSourceManager = localDataManager;
            processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
            processStepVm.Model = processStep;
            processStepVm.InitializeForCreation();

            processStepVm.ProductionUnskilledLabour.Value = 10;
            processStepVm.ProductionSkilledLabour.Value = 10;
            processStepVm.ProductionForeman.Value = 10;
            processStepVm.ProductionTechnicians.Value = (decimal)999999.99;
            processStepVm.ProductionEngineers.Value = 10;
            processStepVm.SetupUnskilledLabour.Value = 100;
            processStepVm.SetupSkilledLabour.Value = 100;
            processStepVm.SetupForeman.Value = (decimal)999999.99;
            processStepVm.SetupTechnicians.Value = 100;
            processStepVm.SetupEngineers.Value = 100;

            var saveCommand = processStepVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(processStep.ProductionUnskilledLabour, processStepVm.ProductionUnskilledLabour.Value);
            Assert.AreEqual(processStep.ProductionSkilledLabour, processStepVm.ProductionSkilledLabour.Value);
            Assert.AreEqual(processStep.ProductionForeman, processStepVm.ProductionForeman.Value);
            Assert.AreEqual(processStep.ProductionTechnicians, processStepVm.ProductionTechnicians.Value);
            Assert.AreEqual(processStep.ProductionEngineers, processStepVm.ProductionEngineers.Value);
            Assert.AreEqual(processStep.SetupUnskilledLabour, processStepVm.SetupUnskilledLabour.Value);
            Assert.AreEqual(processStep.SetupSkilledLabour, processStepVm.SetupSkilledLabour.Value);
            Assert.AreEqual(processStep.SetupForeman, processStepVm.SetupForeman.Value);
            Assert.AreEqual(processStep.SetupTechnicians, processStepVm.SetupTechnicians.Value);
            Assert.AreEqual(processStep.SetupEngineers, processStepVm.SetupEngineers.Value);
        }

        /// <summary>
        /// This method tests the additionalSettings Tab 
        /// </summary>
        [TestMethod]
        public void ProcessStepAdditionalSettingsTabTest()
        {
            var processStepVm = new ProcessStepViewModel(
            this.windowService,
            this.messenger,
            this.container,
            this.pleaseWaitService,
            this.modelBrowserHelperService,
            this.costRecalculationCloneManager,
            this.mediaViewModel,
            this.typeSelectorViewModel,
            this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var processStep = this.CreateNewProcessStep(localDataManager);
            var newUser = this.CreateNewUser(localDataManager);
            processStepVm.DataSourceManager = localDataManager;
            processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
            processStepVm.Model = processStep;
            processStepVm.InitializeForCreation();

            processStepVm.IsExternal.Value = false;
            processStepVm.IsTransportPerQtySelected.Value = false;
            processStepVm.TransportCost.Value = 222;
            processStepVm.TransportCostType.Value = ProcessTransportCostType.PerQty;
            processStepVm.TransportCostQty.Value = 10;

            var saveCommand = processStepVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(processStep.TransportCost, processStepVm.TransportCost.Value);
            Assert.AreEqual(processStep.IsExternal, processStepVm.IsExternal.Value);
            Assert.AreEqual(processStep.TransportCostType, processStepVm.TransportCostType.Value);
            Assert.AreEqual(processStep.TransportCostQty, processStepVm.TransportCostQty.Value);
        }

        /// <summary>
        /// This method tests the undo button 
        /// </summary>
        [TestMethod]
        public void ProcessStepUndoCommandTest()
        {
            var processStepVm = new ProcessStepViewModel(
            this.windowService,
            this.messenger,
            this.container,
            this.pleaseWaitService,
            this.modelBrowserHelperService,
            this.costRecalculationCloneManager,
            this.mediaViewModel,
            this.typeSelectorViewModel,
            this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var processStep = this.CreateNewProcessStep(localDataManager);
            var newUser = this.CreateNewUser(localDataManager);

            processStepVm.DataSourceManager = localDataManager;
            processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
            processStepVm.Model = processStep;
            processStepVm.InitializeForCreation();

            const int SpW = 10;
            const int PdpW = 5;
            processStepVm.ShiftsPerWeek.Value = SpW;
            processStepVm.ProductionDaysPerWeek.Value = PdpW;

            var saveCommand = processStepVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(processStep.ShiftsPerWeek, SpW);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, PdpW);

            processStepVm.ShiftsPerWeek.Value = 12;
            processStepVm.ProductionDaysPerWeek.Value = 3;

            var undoCommand = processStepVm.UndoCommand;
            undoCommand.Execute(null);

            saveCommand.Execute(null);
            Assert.AreEqual(processStep.ShiftsPerWeek, 12);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, PdpW);

            undoCommand.Execute(null);

            saveCommand.Execute(null);
            Assert.AreEqual(processStep.ShiftsPerWeek, SpW);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, PdpW);
        }

        /// <summary>
        /// This method tests the cancel button 
        /// </summary>
        [TestMethod]
        public void ProcessStepCancelCommandTest()
        {
            var processStepVm = new ProcessStepViewModel(
            this.windowService,
            this.messenger,
            this.container,
            this.pleaseWaitService,
            this.modelBrowserHelperService,
            this.costRecalculationCloneManager,
            this.mediaViewModel,
            this.typeSelectorViewModel,
            this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var processStep = this.CreateNewProcessStep(localDataManager);
            var newUser = this.CreateNewUser(localDataManager);
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(
                service =>
                service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);

            processStepVm.DataSourceManager = localDataManager;
            processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
            processStepVm.Model = processStep;
            processStepVm.InitializeForCreation();

            const int SpW = 10;
            const int PdpW = 5;
            processStepVm.ShiftsPerWeek.Value = SpW;
            processStepVm.ProductionDaysPerWeek.Value = PdpW;

            var saveCommand = processStepVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(processStep.ShiftsPerWeek, SpW);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, PdpW);

            processStepVm.ShiftsPerWeek.Value = 12;
            processStepVm.ProductionDaysPerWeek.Value = 3;

            var cancelCommand = processStepVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(processStep.ShiftsPerWeek, SpW);
            Assert.AreEqual(processStep.ProductionDaysPerWeek, PdpW);
        }

        ///// <summary>
        ///// This method tests the machines Tab 
        ///// </summary>
        //[TestMethod]
        //public void ProcessStepMachinesTabTest()
        //{
        //    var processStepVm = new ProcessStepViewModel(
        //    this.windowService,
        //    this.messenger,
        //    this.container,
        //    this.pleaseWaitService,
        //    this.modelBrowserHelperService,
        //    this.costRecalculationCloneManager,
        //    this.mediaViewModel,
        //    this.typeSelectorViewModel,
        //    this.unitsService);

        //    var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        //    var assembly = CreateNewAssembly(localDataManager);
        //    var processStep = CreateNewProcessStep(assembly, localDataManager);
        //    var machine = this.CreateNewMachine(processStep, localDataManager);
        //    var machine2 = this.CreateNewMachine(processStep, localDataManager);
        //    var newUser = this.CreateNewUser(localDataManager);

        //    var actWinMock = new Mock<IWindowService>();
        //    windowService.MessageDialogService.Show(LocalizedResources.Question_Delete_Item, MessageDialogType.YesNo);
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
        //    processStepVm.DataSourceManager = localDataManager;
        //    processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
        //    processStepVm.ModelClone = cloneManager.Clone(processStep, processStep.GetType(), true);
        //    processStepVm.InitializeForCreation();
        //    processStepVm.Model = processStep;

        //    var saveCommand = processStepVm.SaveCommand;
        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepMachines.Count(), 2);

        //    var copyMachineCommand = processStepVm.CopyCommand;
        //    copyMachineCommand.Execute(machine);

        //    var pasteMachineCommand = processStepVm.PasteCommand;
        //    pasteMachineCommand.Execute(machine);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepMachines.Count(), 3);

        //    processStepVm.SelectedMachineItem.Value = processStepVm.StepMachines.FirstOrDefault(item => item.Machine.Guid == machine2.Guid);
        //    var deleteMachineCommand = processStepVm.DeleteCommand;
        //    deleteMachineCommand.Execute(machine2);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepMachines.Count(), 2);
        //}

        ///// <summary>
        ///// This method tests the tooling Tab 
        ///// </summary>
        //[TestMethod]
        //public void ProcessStepToolingTabTest()
        //{
        //    var processStepVm = new ProcessStepViewModel(
        //    this.windowService,
        //    this.messenger,
        //    this.container,
        //    this.pleaseWaitService,
        //    this.modelBrowserHelperService,
        //    this.costRecalculationCloneManager,
        //    this.mediaViewModel,
        //    this.typeSelectorViewModel,
        //    this.unitsService);

        //    var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        //    var assembly = CreateNewAssembly(localDataManager);
        //    var processStep = CreateNewProcessStep(assembly, localDataManager);
        //    var die = this.CreateNewDie(processStep, localDataManager);
        //    var die2 = this.CreateNewDie(processStep, localDataManager);
        //    var newUser = this.CreateNewUser(localDataManager);

        //    var actWinMock = new Mock<IWindowService>();

        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
        //    processStepVm.DataSourceManager = localDataManager;
        //    processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
        //    processStepVm.ModelClone = cloneManager.Clone(processStep, processStep.GetType(), true);
        //    processStepVm.InitializeForCreation();
        //    processStepVm.Model = processStep;

        //    var saveCommand = processStepVm.SaveCommand;
        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepDieItems.Count(), 2);

        //    var copyDieCommand = processStepVm.CopyCommand;
        //    copyDieCommand.Execute(die);

        //    var pasteDieCommand = processStepVm.PasteCommand;
        //    pasteDieCommand.Execute(die);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepDieItems.Count(), 3);

        //    processStepVm.SelectedDieItem.Value = processStepVm.StepDieItems.FirstOrDefault(item => item.Die.Guid == die2.Guid);
        //    var deleteDieCommand = processStepVm.DeleteCommand;
        //    deleteDieCommand.Execute(die2);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepDieItems.Count(), 2);
        //}

        ///// <summary>
        ///// This method tests the Consumable Tab 
        ///// </summary>
        //[TestMethod]
        //public void ProcessStepConsumableTabTest()
        //{
        //    var processStepVm = new ProcessStepViewModel(
        //    this.windowService,
        //    this.messenger,
        //    this.container,
        //    this.pleaseWaitService,
        //    this.modelBrowserHelperService,
        //    this.costRecalculationCloneManager,
        //    this.mediaViewModel,
        //    this.typeSelectorViewModel,
        //    this.unitsService);

        //    var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        //    var assembly = CreateNewAssembly(localDataManager);
        //    var processStep = CreateNewProcessStep(assembly, localDataManager);
        //    var consumable = this.CreateNewConsumable(processStep, localDataManager);
        //    var consumable2 = this.CreateNewConsumable(processStep, localDataManager);
        //    var newUser = this.CreateNewUser(localDataManager);

        //    var actWinMock = new Mock<IWindowService>();
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
        //    processStepVm.DataSourceManager = localDataManager;
        //    processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
        //    processStepVm.ModelClone = cloneManager.Clone(processStep, processStep.GetType(), true);
        //    processStepVm.InitializeForCreation();
        //    processStepVm.Model = processStep;

        //    var saveCommand = processStepVm.SaveCommand;
        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepConsumables.Count(), 2);

        //    var copyConsumableCommand = processStepVm.CopyCommand;
        //    copyConsumableCommand.Execute(consumable);

        //    var pasteConsumableCommand = processStepVm.PasteCommand;
        //    pasteConsumableCommand.Execute(consumable);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepConsumables.Count(), 3);

        //    processStepVm.SelectedConsumable.Value = processStepVm.StepConsumables.FirstOrDefault(item => item.Consumable.Guid == consumable2.Guid);
        //    var deleteConsumableCommand = processStepVm.DeleteCommand;
        //    deleteConsumableCommand.Execute(consumable2);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepConsumables.Count(), 2);
        //}

        ///// <summary>
        ///// This method tests the Commodities Tab 
        ///// </summary>
        //[TestMethod]
        //public void ProcessStepCommoditiesTabTest()
        //{
        //    var processStepVm = new ProcessStepViewModel(
        //    this.windowService,
        //    this.messenger,
        //    this.container,
        //    this.pleaseWaitService,
        //    this.modelBrowserHelperService,
        //    this.costRecalculationCloneManager,
        //    this.mediaViewModel,
        //    this.typeSelectorViewModel,
        //    this.unitsService);

        //    var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        //    var assembly = CreateNewAssembly(localDataManager);
        //    var processStep = CreateNewProcessStep(assembly, localDataManager);
        //    var commodity = this.CreateNewCommodity(processStep, localDataManager);
        //    var commodity2 = this.CreateNewCommodity(processStep, localDataManager);
        //    var newUser = this.CreateNewUser(localDataManager);

        //    var actWinMock = new Mock<IWindowService>();
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
        //    processStepVm.DataSourceManager = localDataManager;
        //    processStepVm.ProcessParent = processStep.Process.Assemblies.FirstOrDefault();
        //    processStepVm.ModelClone = cloneManager.Clone(processStep, processStep.GetType(), true);
        //    processStepVm.InitializeForCreation();
        //    processStepVm.Model = processStep;

        //    var saveCommand = processStepVm.SaveCommand;
        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepCommodities.Count(), 2);

        //    var copyCommodityCommand = processStepVm.CopyCommand;
        //    copyCommodityCommand.Execute(commodity);

        //    var pasteConsumableCommand = processStepVm.PasteCommand;
        //    pasteConsumableCommand.Execute(commodity);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepCommodities.Count(), 3);

        //    processStepVm.SelectedCommodity.Value = processStepVm.StepCommodities.FirstOrDefault(item => item.Commodity.Guid == commodity2.Guid);
        //    var deleteCommodityCommand = processStepVm.DeleteCommand;
        //    deleteCommodityCommand.Execute(commodity2);

        //    saveCommand.Execute(null);

        //    Assert.AreEqual(processStepVm.StepCommodities.Count(), 2);
        //}
        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// This method creates a new assembly
        /// </summary>
        /// <returns>Returns assembly</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private Assembly CreateNewAssembly(IDataSourceManager dataManager)
        {
            var project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var supplier = new Customer();
            supplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = supplier;
            var overheadSettings = new OverheadSetting();
            project.OverheadSettings = overheadSettings;

            var assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;

            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            assembly.Manufacturer = manufacturer;
            var country = dataManager.CountryRepository.GetByName("Belgium");
            assembly.AssemblingCountry = country.Name;
            assembly.AssemblingCountryId = country.Guid;
            var process = new Process();
            assembly.Process = process;
            assembly.Project = project;

            dataManager.SupplierRepository.Save(supplier);
            dataManager.ProjectRepository.Save(project);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.AssemblyRepository.Save(assembly);
            dataManager.ProcessRepository.Save(process);
            dataManager.SaveChanges();

            return assembly;
        }

        /// <summary>
        /// This method creates a new processStep 
        /// </summary>
        /// <returns>Returns processStep</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private ProcessStep CreateNewProcessStep(IDataSourceManager dataManager)
        {
            var assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;

            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            assembly.Manufacturer = manufacturer;
            var country = dataManager.CountryRepository.GetByName("Belgium");
            assembly.AssemblingCountry = country.Name;
            assembly.AssemblingCountryId = country.Guid;
            var process = new Process();
            var processStep = new AssemblyProcessStep()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true)
            };

            assembly.Process = process;
            assembly.Process.Steps.Add(processStep);

            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.AssemblyRepository.Save(assembly);
            dataManager.ProcessRepository.Save(process);
            dataManager.ProcessStepRepository.Save(processStep);
            dataManager.SaveChanges();

            return processStep;
        }

        /// <summary>
        /// This method creates a new processStep
        /// </summary>
        /// <returns>Returns ProcessStep</returns>
        /// <param name="assembly"> Used for dataManager </param>
        /// <param name="dataManager"> Parameter used for dataManager </param>
        private ProcessStep CreateNewProcessStep(Assembly assembly, IDataSourceManager dataManager)
        {
            var processStep = new AssemblyProcessStep()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true)
            };

            assembly.Process.Steps.Add(processStep);

            dataManager.AssemblyRepository.Save(assembly);
            dataManager.ProcessStepRepository.Save(processStep);
            dataManager.SaveChanges();

            return processStep;
        }

        /// <summary>
        /// This method creates a new machine
        /// </summary>
        /// <returns>Returns machine</returns>
        /// <param name="processStep"> Used for assembly </param>
        /// <param name="dataManager"> Parameter used for dataManager </param>
        private Machine CreateNewMachine(ProcessStep processStep, IDataSourceManager dataManager)
        {
            var machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            machine.Manufacturer = manufacturer;
            machine.Guid = Guid.NewGuid();
            processStep.Machines.Add(machine);

            dataManager.ProcessStepRepository.Save(processStep);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// This method creates a new die
        /// </summary>
        /// <returns>Returns die</returns>
        /// <param name="processStep"> Used for assembly </param>
        /// <param name="dataManager"> Parameter used for dataManager </param>
        private Die CreateNewDie(ProcessStep processStep, IDataSourceManager dataManager)
        {
            var die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            die.Manufacturer = manufacturer;
            processStep.Dies.Add(die);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.DieRepository.Save(die);
            dataManager.SaveChanges();

            return die;
        }

        /// <summary>
        /// This method creates a new consumable
        /// </summary>
        /// <returns>Returns consumable</returns>
        /// <param name="processStep"> Used for assembly </param>
        /// <param name="dataManager"> Parameter used for dataManager </param>
        private Consumable CreateNewConsumable(ProcessStep processStep, IDataSourceManager dataManager)
        {
            var consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumable.Amount = 100;
            consumable.PriceUnitBase = priceUnitBaseItems.FirstOrDefault(u => u.Symbol == "kg");

            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            consumable.Manufacturer = manufacturer;
            processStep.Consumables.Add(consumable);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.ConsumableRepository.Save(consumable);
            dataManager.SaveChanges();

            return consumable;
        }

        /// <summary>
        /// This method creates a new commodity
        /// </summary>
        /// <returns>Returns commodity</returns>
        /// <param name="processStep"> Used for assembly </param>
        /// <param name="dataManager"> Parameter used for dataManager </param>
        private Commodity CreateNewCommodity(ProcessStep processStep, IDataSourceManager dataManager)
        {
            var commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            commodity.Amount = 100;

            var manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            commodity.Manufacturer = manufacturer;
            processStep.Commodities.Add(commodity);

            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.CommodityRepository.Save(commodity);
            dataManager.SaveChanges();

            return commodity;
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }
        #endregion Private Methods
    }
}
