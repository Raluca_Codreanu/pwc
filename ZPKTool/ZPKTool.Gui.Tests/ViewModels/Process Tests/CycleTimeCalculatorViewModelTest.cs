﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CycleTimeCalculatorViewModel and is intended 
    /// to contain unit tests for all public methods from CycleTimeCalculatorViewModel class.
    /// </summary>
    [TestClass]
    public class CycleTimeCalculatorViewModelTest
    {
        private List<MeasurementUnit> timeUnits;

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculatorViewModelTest"/> class.
        /// </summary>
        public CycleTimeCalculatorViewModelTest()
        {
            Utils.ConfigureDbAccess();

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.timeUnits = dataManager.MeasurementUnitRepository.GetAll(MeasurementUnitType.Time).ToList();
        }

        #region Test Methods

        /// <summary>
        /// A test for InitializeProperties method, with a process step without cycle time calculations, user in viewer mode and no current time unit set.
        /// </summary>
        [TestMethod]
        public void InitializePropertiesWithoutCalculationObjectsTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = true;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.ProcessStep = processStep;

            Assert.IsFalse(calculatorViewModel.IsChanged, "InitializeProperties() was not implemented.");
        }

        /// <summary>
        /// A test for InitializeProperties method, with a process step that contains a cycle time calculation.
        /// The list of items should include this calculation.
        /// </summary>
        [TestMethod]
        public void InitializePropertiesWithCalculationObjectsTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            processStep.CycleTimeCalculations.Add(calculation);

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.ProcessStep = processStep;

            Assert.AreEqual(1, calculatorViewModel.CalculationViewModelItems.Count, "The items collection does not contain the calculation.");
        }

        /// <summary>
        /// A test for the Add command, when 2 calculations are added to the process step.
        /// </summary>
        [TestMethod]
        public void AddCalculationItemTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);

            var itemsNo = calculatorViewModel.CalculationViewModelItems.Count;
            Assert.AreEqual(2, itemsNo, "The items were not added.");
        }

        /// <summary>
        /// A test for the Delete command, when one of the calculations is deleted.
        /// </summary>
        [TestMethod]
        public void DeleteCalculationItemTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);

            // Delete the first calculation object from the list.
            var addedCalculation = calculatorViewModel.CalculationViewModelItems[0];
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            calculatorViewModel.DeleteCommand.Execute(addedCalculation);

            var itemsNoAfterDelete = calculatorViewModel.CalculationViewModelItems.Count;

            Assert.AreEqual(1, itemsNoAfterDelete, "The item was not deleted.");
        }

        /// <summary>
        /// A test for the Delete command, when the item to delete sent as parameter is not a cycle time calculation object.
        /// The item is not deleted and the process step contains it.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void DeleteCalculationWithWrongParameterTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            calculatorViewModel.DeleteCommand.Execute(processStep);
        }

        /// <summary>
        /// A test for the Copy and Paste commands, when a new item is copied and pasted in the same list.
        /// </summary>
        [TestMethod]
        public void CopyPasteCommandsTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.DataGridSelectedCalculation = calculatorViewModel.CalculationViewModelItems[0];

            calculatorViewModel.CopyCommand.CanExecute(calculatorViewModel.DataGridSelectedCalculation);
            calculatorViewModel.CopyCommand.Execute(calculatorViewModel.DataGridSelectedCalculation);

            calculatorViewModel.PasteCommand.CanExecute(calculatorViewModel.DataGridSelectedCalculation);
            calculatorViewModel.PasteCommand.Execute(calculatorViewModel.DataGridSelectedCalculation);

            var itemsNo = calculatorViewModel.CalculationViewModelItems.Count;
            Assert.AreEqual(2, itemsNo, "The item was not copied.");
        }

        /// <summary>
        /// A test for CanCopy and CanPaste, when a user in viewer mode tries to copy - paste an item.
        /// </summary>
        [TestMethod]
        public void CanCopyAndCanPasteInViewerModeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = true;
            calculatorViewModel.IsInViewerMode = true;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.DataGridSelectedCalculation = calculatorViewModel.CalculationViewModelItems[0];

            bool canCopy = calculatorViewModel.CopyCommand.CanExecute(calculatorViewModel.DataGridSelectedCalculation);
            bool canPaste = calculatorViewModel.PasteCommand.CanExecute(calculatorViewModel.DataGridSelectedCalculation);

            Assert.IsFalse(canCopy, "The item can be copied.");
            Assert.IsFalse(canPaste, "The item can be pasted.");
        }

        /// <summary>
        /// A test for the UpdateTotalTimeValue method, when the values for some time related properties are changed, and then are compared
        /// the initial and updated cycle time values.
        /// </summary>
        [TestMethod]
        public void UpdateTotalTimeValueTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            var item = calculatorViewModel.CalculationViewModelItems[0];
            var initialTime = calculatorViewModel.CycleTime;

            item.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            item.MachiningTime.Value = 30;
            item.ToolChangeTime.Value = 50;

            var updatedTime = calculatorViewModel.CycleTime;
            Assert.AreNotEqual(initialTime, updatedTime, "The cycle time was not updated correctly.");
        }

        /// <summary>
        /// A test for the MoveUp and MoveDown commands, when a selected item from the grid is moved up and and another one, down.
        /// The index of the moved item and the index of the item which is replaced are inverted.
        /// </summary>
        [TestMethod]
        public void MoveUpAndDownCommandsTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);

            var item1 = calculatorViewModel.CalculationViewModelItems[0];
            var item2 = calculatorViewModel.CalculationViewModelItems[1];
            var item3 = calculatorViewModel.CalculationViewModelItems[2];
            var item4 = calculatorViewModel.CalculationViewModelItems[3];

            var index1 = item1.Index.Value;
            var index2 = item2.Index.Value;
            var index3 = item3.Index.Value;
            var index4 = item4.Index.Value;

            // Move up the second item, with index = 2.
            calculatorViewModel.DataGridSelectedCalculation = item2;
            calculatorViewModel.MoveUpCommand.Execute(calculatorViewModel.DataGridSelectedCalculation);

            // Move down the third item, with index = 3.
            calculatorViewModel.DataGridSelectedCalculation = item3;
            calculatorViewModel.MoveDownCommand.Execute(calculatorViewModel.DataGridSelectedCalculation);

            // The indexes for the second and first item should be inversed.
            Assert.AreEqual(index1, item2.Index.Value, "The item was not moved.");

            // The indexes for the third and fourth item should be inversed.
            Assert.AreEqual(index4, item3.Index.Value, "The item was not moved.");
        }

        /// <summary>
        /// A test for the cycle time value, that is changed according to the selected time unit.
        /// </summary>
        [TestMethod]
        public void CycleTimeChangedWhenTheSelectedTimeUnitIsChangedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            var item = calculatorViewModel.CalculationViewModelItems[0];
            var initialTime = calculatorViewModel.CycleTime;

            item.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            item.MachiningTime.Value = 80;
            item.ToolChangeTime.Value = 150;

            //calculatorViewModel.SelectedTimeUnit = timeUnits[1];

            var updatedTime = calculatorViewModel.CycleTime;
            Assert.AreNotEqual(initialTime, updatedTime, "The cycle time was not updated correctly.");
        }

        /// <summary>
        /// A test for the Save command, when 3 calculation items are created for a process step.
        /// One item is deleted, and the other 2 are saved into the process step.
        /// </summary>
        [TestMethod]
        public void SaveCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            dataManager.ProcessStepRepository.Add(processStep);
            dataManager.SaveChanges();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.DataManager = dataManager;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);
            calculatorViewModel.AddCommand.Execute(null);

            var item1 = calculatorViewModel.CalculationViewModelItems[0].Model;
            calculatorViewModel.CalculationViewModelItems[0].Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var item2 = calculatorViewModel.CalculationViewModelItems[1].Model;
            calculatorViewModel.CalculationViewModelItems[1].Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var item3 = calculatorViewModel.CalculationViewModelItems[2].Model;
            calculatorViewModel.CalculationViewModelItems[2].Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            calculatorViewModel.DeleteCommand.Execute(calculatorViewModel.CalculationViewModelItems[1]);
            bool canSave = calculatorViewModel.SaveCommand.CanExecute(null);
            calculatorViewModel.SaveCommand.Execute(null);

            bool item1Exists = processStep.CycleTimeCalculations.Contains(item1);
            bool item2Exists = processStep.CycleTimeCalculations.Contains(item2);
            bool item3Exists = processStep.CycleTimeCalculations.Contains(item3);

            var itemsNo = calculatorViewModel.CalculationViewModelItems.Count;

            Assert.IsTrue(canSave, "The save command can not be executed.");
            Assert.IsTrue(item1Exists, "The item was not saved into the process step.");
            Assert.IsFalse(item2Exists, "The item was saved into the process step.");
            Assert.IsTrue(item3Exists, "The item was not saved into the process step.");
            Assert.AreEqual(2, itemsNo, "The calculation view model list does not contain 2 items.");
        }

        /// <summary>
        /// A test for the Cancel command, when a new calculation object is added to a process step, but the user cancels the action.
        /// The new calculation is not attached to the process step.
        /// </summary>
        [TestMethod]
        public void CancelCommandWithCalculationChangedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = this.timeUnits[0];
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);
            var item = calculatorViewModel.CalculationViewModelItems[0];
            var calculation = item.Model;
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            calculatorViewModel.CancelCommand.Execute(null);

            bool calculationExists = processStep.CycleTimeCalculations.Contains(calculation);
            Assert.IsFalse(calculationExists, "The calculation was added to the process step.");
        }

        /// <summary>
        /// A test for the Cancel command, when the selected time unit is changed, but the users cancels the action.
        /// The process step cycle time unit will not change.
        /// </summary>
        [TestMethod]
        public void CancelCommandWithTimeUnitChangedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.CycleTimeUnit = this.timeUnits[0];
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = processStep.CycleTimeUnit;
            calculatorViewModel.ProcessStep = processStep;

            var currentTimeUnit = processStep.CycleTimeUnit;
            calculatorViewModel.SelectedTimeUnit = this.timeUnits[1];

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            calculatorViewModel.CancelCommand.Execute(null);

            Assert.AreEqual(processStep.CycleTimeUnit, currentTimeUnit, "The time unit was changed.");
        }

        /// <summary>
        /// A test for the Onunloading event, when the selected time unit is changed, and the user confirms quitting without saving changes.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithTimeUnitChangedAndQuitConfirmedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.CycleTimeUnit = this.timeUnits[0];
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = processStep.CycleTimeUnit;
            calculatorViewModel.ProcessStep = processStep;

            var currentTimeUnit = processStep.CycleTimeUnit;
            calculatorViewModel.SelectedTimeUnit = this.timeUnits[1];

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var unloadingStatus = calculatorViewModel.OnUnloading();

            Assert.AreEqual(processStep.CycleTimeUnit, currentTimeUnit, "The time unit was changed.");
            Assert.IsTrue(unloadingStatus);
        }

        /// <summary>
        /// A test for the Onunloading event, when a new calculation is added, and the user does not confirm quitting without saving changes.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithCalculationChangedAndQuitNotConfirmedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.CycleTimeUnit = this.timeUnits[0];
            processStep.Process = new Process();

            calculatorViewModel.IsReadOnly = false;
            calculatorViewModel.IsInViewerMode = false;
            calculatorViewModel.TimeUnits = this.timeUnits;
            calculatorViewModel.CrtTimeUnit = processStep.CycleTimeUnit;
            calculatorViewModel.ProcessStep = processStep;

            calculatorViewModel.AddCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var unloadingStatus = calculatorViewModel.OnUnloading();

            Assert.IsFalse(unloadingStatus);
        }

        /// <summary>
        /// A test for the Onunloading event, when the message box for confirm quitting should not appear, because no changes are made.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithoutMessageBoxShownTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var windowServiceMock = new Mock<IWindowService>();
            var container = new CompositionContainer();

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);

            bool serviceCallHappened = false;
            windowServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo))
                .Callback(() =>
                {
                    serviceCallHappened = true;
                });

            var unloadingStatus = calculatorViewModel.OnUnloading();

            Assert.IsFalse(serviceCallHappened, "The message box was displayed.");
            Assert.IsTrue(unloadingStatus);
        }

        /// <summary>
        /// A test for machining calculator command that opens a new window; the test verifies if a window for MachiningCalculatorViewModel is opened after executing the command.
        /// </summary>
        [TestMethod]
        public void OpenMachiningCalculatorWindowTest()
        {
            var typeCatalog = new TypeCatalog(
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(WindowServiceMock),
                typeof(MachiningCalculatorSetupTimeViewModel),
                typeof(MachiningCalculatorMillingViewModel),
                typeof(MachiningCalculatorDrillingViewModel),
                typeof(MachiningCalculatorTurningViewModel),
                typeof(MachiningCalculatorGrindingViewModel),
                typeof(MachiningCalculatorGearHobbingViewModel),
                typeof(MachiningCalculatorViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CompositionContainer container = bootstrapper.CompositionContainer;

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            calculatorViewModel.MachiningCalculatorCommand.Execute(null);

            Assert.IsTrue(windowService.IsOpen);
            Assert.AreEqual(windowService.DisplayedView.GetType().Name, "MachiningCalculatorViewModel", "The displayed view's type is not MachiningCalculatorViewModel");
        }

        /// <summary>
        /// A test for edit calculation command; the test verifies if a window for MachiningCalculatorViewModel is opened after executing the command.
        /// </summary>
        [TestMethod]
        public void EditCalculationItemWithSupportedCalculationTest()
        {
            var typeCatalog = new TypeCatalog(
                typeof(CompositionContainer),
                typeof(DispatcherServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(Messenger),
                typeof(ModelBrowserHelperServiceMock),
                typeof(WindowServiceMock),
                typeof(MachiningCalculatorSetupTimeViewModel),
                typeof(MachiningCalculatorMillingViewModel),
                typeof(MachiningCalculatorDrillingViewModel),
                typeof(MachiningCalculatorTurningViewModel),
                typeof(MachiningCalculatorGrindingViewModel),
                typeof(MachiningCalculatorGearHobbingViewModel),
                typeof(MachiningCalculatorViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CompositionContainer container = bootstrapper.CompositionContainer;

            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.PartPositioning;

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, container);
            calculatorViewModel.EditCommand.Execute(itemToEdit);

            Assert.IsTrue(windowService.IsOpen, "The window did not open.");
            Assert.AreEqual(windowService.DisplayedView.GetType().Name, "MachiningCalculatorViewModel", "The displayed view's type is not MachiningCalculatorViewModel");
        }

        /// <summary>
        /// A test for the edit calculation command, when a null parameter is sent and a NullReferenceException is expected to be thrown.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void EditCalculationItemWithNullParameterTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, new CompositionContainer());
            calculatorViewModel.EditCommand.Execute(null);
        }

        /// <summary>
        /// A test for the edit calculation command, when the calculation sent as parameter has a machining type that is not supported for machining calculator.
        /// </summary>
        [TestMethod]
        public void EditCalculationItemWithCalculationParameterNotSupportedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculatorViewModel calculatorViewModel = new CycleTimeCalculatorViewModel(windowService, messenger, new CompositionContainer());

            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.Taping;

            calculatorViewModel.EditCommand.Execute(itemToEdit);
            Assert.IsFalse(windowService.IsOpen, "The window has opened.");
        }

        #endregion
    }
}
