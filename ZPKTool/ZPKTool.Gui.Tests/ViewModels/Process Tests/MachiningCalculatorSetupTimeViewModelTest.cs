﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorSetupTimeViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorSetupTimeViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorSetupTimeViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            setupTimeViewModel.EditMode = ViewModelEditMode.Create;
            setupTimeViewModel.SelectedDescription = SetupType.DieSetUp;
            setupTimeViewModel.Remarks = "description";
            setupTimeViewModel.SetupTimeInMachine = 890;
            setupTimeViewModel.SetupTimeOutsideMachine = 1000;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, setupTimeViewModel.MessengerTokenForMachining);

            setupTimeViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(setupTimeViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(SetupType.DieSetUp + " - description", message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(0, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.PartPositioning, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual("0", Formatter.FormatNumber(message.Content.TransportClampingTime.Value, 0), "The transport clamping time is incorrect.");
            Assert.AreEqual("0", Formatter.FormatNumber(message.Content.MachiningTime.Value, 0), "The machining time is incorrect.");
            Assert.AreEqual(1890, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual(null, message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, setupTimeViewModel.MessengerTokenForMachining);

            string data = "6;description;890;1000";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.PartPositioning;

            setupTimeViewModel.EditMode = ViewModelEditMode.Edit;
            setupTimeViewModel.Model = calculationItemToEdit;
            setupTimeViewModel.SetupTimeOutsideMachine = 555;
            setupTimeViewModel.SaveCommand.Execute(null);

            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(setupTimeViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(0, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual("0", Formatter.FormatNumber(message.Content.TransportClampingTime.Value, 0), "The transport clamping time is incorrect.");
            Assert.AreEqual("0", Formatter.FormatNumber(message.Content.MachiningTime.Value, 0), "The machining time is incorrect.");
            Assert.AreEqual(1445, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the net and gross setup time are updated when a setup time value is changed.
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            var netSetupTime1 = setupTimeViewModel.NetSetupTime;
            var grossSetupTime1 = setupTimeViewModel.GrossSetupTime;

            setupTimeViewModel.SetupTimeInMachine = 400;

            var netSetupTime2 = setupTimeViewModel.NetSetupTime;
            var grossSetupTime2 = setupTimeViewModel.GrossSetupTime;

            Assert.AreNotEqual(netSetupTime1, netSetupTime2, "The net setup time value was not updated.");
            Assert.AreNotEqual(grossSetupTime1, grossSetupTime2, "The gross setup time value was not updated.");
        }

        /// <summary>
        /// A test for the SaveSetupTimeDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveSetupTimeDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            setupTimeViewModel.EditMode = ViewModelEditMode.Create;
            setupTimeViewModel.SelectedDescription = SetupType.MachinePreparation;
            setupTimeViewModel.SetupTimeInMachine = 400;
            setupTimeViewModel.SetupTimeOutsideMachine = 500;

            setupTimeViewModel.SaveCommand.Execute(null);

            string expectedData = "4;;400;500";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = setupTimeViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}