﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorTurningViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorTurningViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorTurningViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);

            turningViewModel.EditMode = ViewModelEditMode.Create;
            turningViewModel.IsExpertMode = true;
            turningViewModel.SelectedMachiningType = TurningMachiningType.Chipping;
            turningViewModel.SelectedMaterial = turningViewModel.TurningMaterials[2];
            turningViewModel.ProcessDescription = "description";
            turningViewModel.SelectedTurningType = TurningType.Radial;
            turningViewModel.FeedSpeed = 30;
            turningViewModel.SelectedTurningSituation = TurningSituation.ModerateIntersections;
            turningViewModel.CutDepth = 20;
            turningViewModel.StartStopTime = 1;
            turningViewModel.FeedLength = 10;
            turningViewModel.TurningDiameter = 20;
            turningViewModel.TurningLength = 30;
            turningViewModel.TurningDepth = 40;
            turningViewModel.ToolChangeTime = 50;
            turningViewModel.TurningSpeed = 60;
            turningViewModel.FeedRate = 70;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, turningViewModel.MessengerTokenForMachining);

            turningViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(turningViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual("Turning - " + TurningType.Radial, message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(0, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.Turning, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual(2.02m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 2), "The transport clamping time is incorrect.");
            Assert.AreEqual(3.01m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(50, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual("description", message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, turningViewModel.MessengerTokenForMachining);

            string data = "23;2;30;2;20;1;10;20;30;40;True;70;50;60;3";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.Turning;

            turningViewModel.EditMode = ViewModelEditMode.Edit;
            turningViewModel.Model = calculationItemToEdit;
            turningViewModel.SelectedTurningSituation = TurningSituation.UnfavourableIntersections;
            turningViewModel.ToolChangeTime = 555;
            turningViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(turningViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(0, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(2.02m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 2), "The transport clamping time is incorrect.");
            Assert.AreEqual(3.77m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(555, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the resulting cycle and turning time are updated after some values are changed.
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);

            var turningTime1 = turningViewModel.ResultingTurningTime;
            var cycleTime1 = turningViewModel.ResultingCycleTime;

            turningViewModel.TurningDiameter = 350;

            var turningTime2 = turningViewModel.ResultingTurningTime;
            var cycleTime2 = turningViewModel.ResultingCycleTime;

            turningViewModel.SelectedMachiningType = TurningMachiningType.Chipping;

            var turningTime3 = turningViewModel.ResultingTurningTime;
            var cycleTime3 = turningViewModel.ResultingCycleTime;

            Assert.AreNotEqual(turningTime1, turningTime2, "The turning time value was not updated.");
            Assert.AreNotEqual(cycleTime1, cycleTime2, "The cycle time value was not updated.");

            Assert.AreNotEqual(turningTime2, turningTime3, "The turning time value was not updated.");
            Assert.AreNotEqual(cycleTime2, cycleTime3, "The cycle time value was not updated.");
        }

        /// <summary>
        /// A test for the SaveTurningResultDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveTurningResultDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);

            turningViewModel.EditMode = ViewModelEditMode.Create;
            turningViewModel.IsExpertMode = false;
            turningViewModel.SelectedMachiningType = TurningMachiningType.FinestFeed;
            turningViewModel.SelectedMaterial = turningViewModel.TurningMaterials[1];
            turningViewModel.SelectedTurningType = TurningType.Radial;
            turningViewModel.FeedSpeed = 30;
            turningViewModel.SelectedTurningSituation = TurningSituation.ModerateIntersections;
            turningViewModel.CutDepth = 20;
            turningViewModel.StartStopTime = 1;
            turningViewModel.FeedLength = 10;
            turningViewModel.TurningDiameter = 20;
            turningViewModel.TurningLength = 30;
            turningViewModel.TurningDepth = 40;
            turningViewModel.ToolChangeTime = 50;

            turningViewModel.SaveCommand.Execute(null);

            string expectedData = "22;2;30;2;20;1;10;20;30;40;False;0.1710;50;4379;1";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = turningViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}
