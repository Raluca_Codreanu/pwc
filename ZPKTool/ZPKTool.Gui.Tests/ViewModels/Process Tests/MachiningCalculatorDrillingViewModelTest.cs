﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorDrillingViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorDrillingViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorDrillingViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);

            drillingViewModel.EditMode = ViewModelEditMode.Create;
            drillingViewModel.IsExpertMode = true;
            drillingViewModel.SelectedMaterial = drillingViewModel.DrillingMaterials[2];
            drillingViewModel.ProcessDescription = "description";
            drillingViewModel.SelectedDrillingType = DrillingType.BlindHole;
            drillingViewModel.IsBoringChecked = true;
            drillingViewModel.FeedSpeed = 200;
            drillingViewModel.StartStopTime = 5;
            drillingViewModel.DrillingDiameter = 15;
            drillingViewModel.DrillingDepth = 100;
            drillingViewModel.HolesNumber = 33;
            drillingViewModel.AvgHoleDistance = 55;
            drillingViewModel.ToolChangeTime = 300;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, drillingViewModel.MessengerTokenForMachining);

            drillingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(drillingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual("Drilling - " + DrillingType.BlindHole, message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(785000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.Drilling, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual(1088.9m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 1), "The transport clamping time is incorrect.");
            Assert.AreEqual(23.7m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 1), "The machining time is incorrect.");
            Assert.AreEqual(300, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual("description", message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, drillingViewModel.MessengerTokenForMachining);

            string data = "27;2;200;5;15;100;33;55;300;True;True;1115";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.Drilling;

            drillingViewModel.EditMode = ViewModelEditMode.Edit;
            drillingViewModel.Model = calculationItemToEdit;
            drillingViewModel.DrillingDiameter = 95;
            drillingViewModel.AvgHoleDistance = 2300;
            drillingViewModel.SaveCommand.Execute(null);

            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(drillingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(785000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(909.9m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 1), "The transport clamping time is incorrect.");
            Assert.AreEqual(17.41m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(300, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the resulting cycle and drilling time are updated after some values are changed.
        /// It also checks if the boring check box visibility changes according to the selected drilling type. 
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);

            var drillingTime1 = drillingViewModel.ResultingDrillingTime;
            var cycleTime1 = drillingViewModel.ResultingCycleTime;

            drillingViewModel.DrillingDepth = 4500;

            var drillingTime2 = drillingViewModel.ResultingDrillingTime;
            var cycleTime2 = drillingViewModel.ResultingCycleTime;

            drillingViewModel.SelectedMaterial = drillingViewModel.DrillingMaterials[3];

            var drillingTime3 = drillingViewModel.ResultingDrillingTime;
            var cycleTime3 = drillingViewModel.ResultingCycleTime;

            drillingViewModel.SelectedDrillingType = DrillingType.Counterbore;

            Assert.AreNotEqual(drillingTime1, drillingTime2, "The drilling time value was not updated.");
            Assert.AreNotEqual(cycleTime1, cycleTime2, "The cycle time value was not updated.");

            Assert.AreNotEqual(drillingTime2, drillingTime3, "The drilling time value was not updated.");
            Assert.AreNotEqual(cycleTime2, cycleTime3, "The cycle time value was not updated.");

            Assert.AreEqual(drillingViewModel.BoringVisibility, Visibility.Collapsed, "The boring check box is still visible.");
        }

        /// <summary>
        /// A test for the SaveDrillingResultDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveDrillingResultDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);

            drillingViewModel.EditMode = ViewModelEditMode.Create;
            drillingViewModel.IsExpertMode = true;
            drillingViewModel.SelectedMaterial = drillingViewModel.DrillingMaterials[4];
            drillingViewModel.SelectedDrillingType = DrillingType.Reaming;
            drillingViewModel.FeedSpeed = 250;
            drillingViewModel.StartStopTime = 1;
            drillingViewModel.DrillingDiameter = 10;
            drillingViewModel.DrillingDepth = 10;
            drillingViewModel.HolesNumber = 30;
            drillingViewModel.AvgHoleDistance = 30;
            drillingViewModel.ToolChangeTime = 100;

            drillingViewModel.SaveCommand.Execute(null);

            string expectedData = "17;5;250;1;10;10;30;30;100;False;True;1115";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = drillingViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}
