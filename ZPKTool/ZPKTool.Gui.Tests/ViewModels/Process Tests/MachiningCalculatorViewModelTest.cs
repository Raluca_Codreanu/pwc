﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CycleTimeCalculatorViewModel and is intended 
    /// to contain unit tests for all public methods from CycleTimeCalculatorViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the EditCycleTimeCalculation and OnModelChanged methods, were a calculation is being edited.
        /// According to the machining type, the appropriate tab is selected (here, SetupTime tab for a PartPositioning).
        /// </summary>
        [TestMethod]
        public void EditCalculationForSelectedTabTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            calculationItemToEdit.MachiningType.Value = MachiningType.PartPositioning;
            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = calculationItemToEdit;

            Assert.AreEqual((int)SelectedMachiningCalculationTab.SetupTimeTab, machiningCalculatorViewModel.SelectedTabIndex.Value, "The setup time tab is not selected.");
        }

        /// <summary>
        /// A test for the expert mode changed event, when the expert mode for all children view models are changed.
        /// </summary>
        [TestMethod]
        public void OnExpertModeChangedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.IsExpertMode = true;

            Assert.IsTrue(machiningCalculatorViewModel.DrillingViewModel.IsExpertMode, "The expert mode is not changed.");
            Assert.IsTrue(machiningCalculatorViewModel.TurningViewModel.IsExpertMode, "The expert mode is not changed.");
            Assert.IsTrue(machiningCalculatorViewModel.MillingViewModel.IsExpertMode, "The expert mode is not changed.");
            Assert.IsTrue(machiningCalculatorViewModel.GearHobbingViewModel.IsExpertMode, "The expert mode is not changed.");
            Assert.IsTrue(machiningCalculatorViewModel.GrindingViewModel.IsExpertMode, "The expert mode is not changed.");
        }

        /// <summary>
        /// A test for the part name in edit mode, when the Turning tab is selected.
        /// The machining part name should be equal to the item to edit's raw part description.
        /// </summary>
        [TestMethod]
        public void PartNameFromItemToEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            calculationItemToEdit.MachiningType.Value = MachiningType.Turning;
            calculationItemToEdit.RawPartDescription.Value = "Test";
            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;

            machiningCalculatorViewModel.Model = calculationItemToEdit;
            machiningCalculatorViewModel.PartName = calculationItemToEdit.RawPartDescription;

            Assert.AreEqual(machiningCalculatorViewModel.PartName.Value, "Test", "The part name is not correctly set.");
            Assert.AreEqual((int)SelectedMachiningCalculationTab.TurningTab, machiningCalculatorViewModel.SelectedTabIndex.Value, "The turning tab is not selected.");
        }

        /// <summary>
        /// A test for handling the notification message received from setup time view model, that adds a new calculation item.
        /// </summary>
        [TestMethod]
        public void HandleNotificationMessageForAddTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);

            MachiningCalculatorSetupTimeViewModel setupTimeVM = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.SetupTimeViewModel = setupTimeVM;
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);
            
            var modelBeforeHandlingNotification = machiningCalculatorViewModel.Model;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();
            setupTimeVM.EditMode = ViewModelEditMode.Create;
            setupTimeVM.SaveCommand.Execute(null);

            var modelAfterHandlingNotification = machiningCalculatorViewModel.Model;

            Assert.AreNotEqual(modelBeforeHandlingNotification, modelAfterHandlingNotification, "The notification was not handled correctly.");
        }

        /// <summary>
        /// A test for handling the notification message received from drilling view model, that edits a calculation item.
        /// </summary>
        [TestMethod]
        public void HandleNotificationMessageForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            var drillingVM = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = drillingVM;

            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            calculationItemToEdit.MachiningType.Value = MachiningType.Drilling;
            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;

            var oldDescription = EncryptionManager.Instance.GenerateRandomString(5, true);
            calculationItemToEdit.RawPartDescription.Value = oldDescription;

            machiningCalculatorViewModel.Model = calculationItemToEdit;
            var newDescription = EncryptionManager.Instance.GenerateRandomString(5, true);
            machiningCalculatorViewModel.PartName.Value = newDescription;

            machiningCalculatorViewModel.OnLoaded();
            drillingVM.SaveCommand.Execute(null);

            Assert.AreNotEqual(oldDescription, newDescription, "The notification was not handled correctly.");
            Assert.AreEqual((int)SelectedMachiningCalculationTab.DrillingTab, machiningCalculatorViewModel.SelectedTabIndex.Value, "The drilling tab is not selected.");
        }

        #endregion
    }
}