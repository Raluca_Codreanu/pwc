﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The test for ProcessViewModelTest
    /// </summary>
    [TestClass]
    public class ProcessViewModelTest
    {
        /// <summary>
        /// The window service
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The messenger
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The composition container
        /// </summary>
        private CompositionContainer compositionContainer;

        #region Additional attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup()
        //// {
        //// }

        /// <summary>
        ///  Use TestInitialize to run code before running each test
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }

            this.compositionContainer = this.SetupCompositionContainer(out windowService, out messenger);
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional attributes

        #region Test Methods

        /// <summary>
        /// A test for loading new model that have the parent an assembly
        /// </summary>
        [TestMethod]
        public void LoadModelWithAssemblyParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();

            var assyStep = new AssemblyProcessStep();
            var machine = new Machine();
            assyStep.Machines.Add(machine);
            assy.Process.Steps.Add(assyStep);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error doesn't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsFalse(processViewModel.CanShowWizards, "Moulding and casting are not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawMaterialsCost, "Raw material cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowPartsCost, "Part cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a part
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a subpart from an assembly
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParentFromSubassembly()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            var part = CreateNewPart();
            part.Assembly = assy;
            assy.Parts.Add(part);

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a raw part
        /// </summary>
        [TestMethod]
        public void LoadModelWithRawPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent an assembly and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithAssemblyParentInViewMode()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            assy.Project = null;

            var assyStep = new AssemblyProcessStep();
            var machine = new Machine();
            assyStep.Machines.Add(machine);
            assy.Process.Steps.Add(assyStep);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist  one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsFalse(processViewModel.CanShowWizards, "Moulding and casting are not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawMaterialsCost, "Raw material cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowPartsCost, "Part cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a part and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParentInViewMode()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();
            part.Project = null;

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a subpart from an assembly and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParentFromSubassemblyInViewMode()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            
            var assy = CreateNewAssembly();
            var part = CreateNewPart();
            var project = part.Project;
            part.Project = null;
            part.Assembly = assy;
            assy.Parts.Add(part);

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a raw part and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithRawPartParentInViewMode()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            part.Project = null;

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsTrue(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widget");
            Assert.IsNotNull(processViewModel.SelectedProcessStepWidget, "No widget is selected, first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.IsNotNull(processViewModel.SelectedProcessStepViewModel, "No process step view model is selected, one view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent an assembly that has no steps
        /// </summary>
        [TestMethod]
        public void LoadModelWithAssemblyParentNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error doesn't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widgets");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap items");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid items");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil items for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must not exist co2 renewable items for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsFalse(processViewModel.CanShowWizards, "Moulding and casting are not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawMaterialsCost, "Raw material cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowPartsCost, "Part cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a part that have no steps
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParentNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widgets");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap items");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid items");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil items for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must not exist co2 renewable items for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a raw part that has no steps
        /// </summary>
        [TestMethod]
        public void LoadModelWithRawPartParentNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widgets");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap items");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid items");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil items for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must not exist co2 renewable items for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent an assembly that has no steps and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithAssemblyParentInViewModeNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            assy.Project = null;

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widget");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap items");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid items");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil items for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must exist co2 renewable items for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsFalse(processViewModel.CanShowWizards, "Moulding and casting are not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawMaterialsCost, "Raw material cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowPartsCost, "Part cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost is not available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a part that have no steps and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithPartParentInViewModeNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();
            part.Project = null;

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widgets");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap items");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid items");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil items for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must not exist renewable items for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for loading new model that have the parent a raw part that has no steps and the view model is in view mode
        /// </summary>
        [TestMethod]
        public void LoadModelWithRawPartParentInViewModeNoSteps()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            part.Project = null;

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = true;
            processViewModel.IsReadOnly = true;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            Assert.IsFalse(processViewModel.ExistsCalculationErrors, "The data check error don't work fine");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 0, "Process must have no widgets");
            Assert.IsNull(processViewModel.SelectedProcessStepWidget, "One widget is selected, no widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 0, "Must not exist scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 0, "Must not exist co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 0, "Must not exist co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 0, "Must not exist co2 renewable item for co2 chart");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 0, "Process must have no process step view models");
            Assert.IsNull(processViewModel.SelectedProcessStepViewModel, "One process step view model is selected, no view model must be selected");
            Assert.IsTrue(processViewModel.CanShowWizards, "Moulding and casting must be available if the parent is an assembly");
            Assert.IsTrue(processViewModel.CanShowRawMaterialsCost, "Raw material cost must be available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowPartsCost, "Part cost is not available if the parent is an assembly");
            Assert.IsFalse(processViewModel.CanShowRawPartsCost, "Raw part cost must be available if the parent is an assembly");
        }

        /// <summary>
        /// A test for adding a new step when the parent an assembly
        /// </summary>
        [TestMethod]
        public void AddStepAssemblyParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var assy = CreateNewAssembly();

            var assyStep = new AssemblyProcessStep();
            var machine = new Machine();
            assyStep.Machines.Add(machine);
            assy.Process.Steps.Add(assyStep);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.AddStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 3, "Must exist three co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for adding a new step when the parent a part
        /// </summary>
        [TestMethod]
        public void AddStepPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var part = CreateNewPart();

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.AddStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 3, "Must exist three co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for adding a new step when the parent a raw part
        /// </summary>
        [TestMethod]
        public void AddStepRawPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var part = CreateNewRawPart();

            var partStep = new PartProcessStep();
            var machine = new Machine();
            partStep.Machines.Add(machine);
            part.Process.Steps.Add(partStep);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.IsInViewerMode = false;
            processViewModel.IsReadOnly = false;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.AddStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 3, "Must exist three co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent an assembly
        /// </summary>
        [TestMethod]
        public void RemoveStepAssemblyParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            assyStep1.Machines.Add(machine1);
            assy.Process.Steps.Add(assyStep1);
            var assyStep2 = new AssemblyProcessStep();
            var machine2 = new Machine();
            assyStep2.Machines.Add(machine2);
            assy.Process.Steps.Add(assyStep2);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent a part
        /// </summary>
        [TestMethod]
        public void RemoveStepPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent a raw part
        /// </summary>
        [TestMethod]
        public void RemoveStepRawPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have one widgets");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 1, "Process must have one process step view models");
            Assert.AreNotEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist one scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 2, "Must exist two co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 1, "Must exist one co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 1, "Must exist one co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent an assembly and select to not remove step when asked
        /// </summary>
        [TestMethod]
        public void AbortRemoveStepAssemblyParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            assyStep1.Machines.Add(machine1);
            assy.Process.Steps.Add(assyStep1);
            var assyStep2 = new AssemblyProcessStep();
            var machine2 = new Machine();
            assyStep2.Machines.Add(machine2);
            assy.Process.Steps.Add(assyStep2);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have two widgets");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 4, "Must exist four co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent a part and select to not remove step when asked
        /// </summary>
        [TestMethod]
        public void AbortRemoveStepPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have two widgets");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 4, "Must exist four co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for remove a step when the parent a raw part and select to not remove step when asked
        /// </summary>
        [TestMethod]
        public void AbortRemoveStepRawPartParent()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;
            var currentSelectedProcessStepViewModel = processViewModel.SelectedProcessStepViewModel;

            processViewModel.RemoveStepCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 2, "Process must have two widgets");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, currentSelectedWidget, "The selected widget is wrong");
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.Count, 2, "Process must have two process step view models");
            Assert.AreEqual(processViewModel.SelectedProcessStepViewModel, currentSelectedProcessStepViewModel, "The selected process step view model is wrong");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 2, "Must exist two scrap item");
            Assert.AreEqual(processViewModel.ProcessCo2EmissionItems.Count, 4, "Must exist four co2 data grid item");
            Assert.AreEqual(processViewModel.CO2ChartFossilItems.Count, 2, "Must exist two co2 fossil item for co2 chart");
            Assert.AreEqual(processViewModel.CO2ChartRenewableItems.Count, 2, "Must exist two co2 renewable item for co2 chart");
        }

        /// <summary>
        /// A test for dropping a process step widget before first element
        /// </summary>
        [TestMethod]
        public void DropStepBeforeFirstElement()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);
            var partStep3 = new PartProcessStep();
            var machine3 = new Machine();
            partStep3.Machines.Add(machine3);
            part.Process.Steps.Add(partStep3);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var firtWidget = processViewModel.ProcessStepWidgetItems.FirstOrDefault();
            var secondWidget = processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1);

            processViewModel.DropBeforeProcessStepCommand.Execute(new Tuple<object, object>(secondWidget, firtWidget));

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.FirstOrDefault(), secondWidget, "The dropped widget didn't changed its position");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), firtWidget, "The target widget didn't changed its position");
        }

        /// <summary>
        /// A test for dropping a process step widget after its target
        /// </summary>
        [TestMethod]
        public void DropStepAfterTarget()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);
            var partStep3 = new PartProcessStep();
            var machine3 = new Machine();
            partStep3.Machines.Add(machine3);
            part.Process.Steps.Add(partStep3);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var firtWidget = processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1);
            var secondWidget = processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(2);

            processViewModel.DropProcessStepCommand.Execute(new Tuple<object, object>(firtWidget, secondWidget));

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), secondWidget, "The target widget didn't changed its position");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(2), firtWidget, "The dropped widget didn't changed its position");
        }

        /// <summary>
        /// A test for dropping a process step widget before its target
        /// </summary>
        [TestMethod]
        public void DropStepBeforeTarget()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);
            var partStep3 = new PartProcessStep();
            var machine3 = new Machine();
            partStep3.Machines.Add(machine3);
            part.Process.Steps.Add(partStep3);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var firtWidget = processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1);
            var secondWidget = processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(2);

            processViewModel.DropProcessStepCommand.Execute(new Tuple<object, object>(secondWidget, firtWidget));

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(2), secondWidget, "The dropped widget didn't changed its position");
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), firtWidget, "The target widget didn't changed its position");
        }

        /// <summary>
        /// A test for dropping an entity to the a process step
        /// </summary>
        [TestMethod]
        public void DropEntityOnProcessStep()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            var machine2 = new Machine();
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            var machine3 = new Machine();

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var lastWidget = processViewModel.ProcessStepWidgetItems.LastOrDefault();
            var currentSelectedWidget = processViewModel.SelectedProcessStepWidget;

            processViewModel.DropEntityOnProcessStepCommand.Execute(new Tuple<object, object>(lastWidget, machine3));

            Assert.AreNotEqual(currentSelectedWidget, processViewModel.SelectedProcessStepWidget, "The current selected widget didn't changed");
        }

        /// <summary>
        /// Test for the save command using valid data after removing a process step
        /// </summary>
        [TestMethod]
        public void ValidSaveCommandTestAfterRemoveStep()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var selectedProcessStep = processViewModel.SelectedProcessStepViewModel.Model;
            processViewModel.RemoveStepCommand.Execute(null);

            processViewModel.SaveCommand.Execute(null);

            // Verifies if process step from process  was not saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool processStepExist = newContext.ProcessStepRepository.CheckIfExists(selectedProcessStep.Guid);
            Assert.IsFalse(processStepExist, "The process step still exist saved");
        }

        /// <summary>
        /// Test for the save command using valid data after adding a process step
        /// </summary>
        [TestMethod]
        public void ValidSaveCommandTestAfterAddingStep()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.CreateUserToLogin(dataContext);
            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            processViewModel.AddStepCommand.Execute(null);
            var selectedProcessStep = processViewModel.ProcessStepViewModelItems.LastOrDefault();
            selectedProcessStep.Name.Value = EncryptionManager.Instance.GenerateRandomString(15, true);

            processViewModel.SaveCommand.Execute(null);

            // Verifies if process step from process  was saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool processStepExist = newContext.ProcessStepRepository.CheckIfExists(selectedProcessStep.Model.Guid);
            Assert.IsTrue(processStepExist, "The process step added is not saved");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelVMIsChangedTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.SaveCommand.Execute(null);

            var numberOfStepsBefore = processViewModel.Model.Steps.Count;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.RemoveStepCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.CancelCommand.Execute(null);

            var numberOfStepsAfter = processViewModel.Model.Steps.Count;

            // Verifies if process step from process  was saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assert.AreEqual(numberOfStepsBefore, numberOfStepsAfter, "The process removed is not recovered");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed and the answer to the message dialog is no
        /// </summary>
        [TestMethod]
        public void AbortedCancelVMIsChangedTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            // Delete 1st process step
            var numberOfStepsBeforeDelete = processViewModel.ProcessStepViewModelItems.Count;
            var selectedProcessStep = processViewModel.SelectedProcessStepViewModel.Model;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.RemoveStepCommand.Execute(null);

            // Click Cancel and choose not to cancel the changes -> the deleted step should remain deleted
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            processViewModel.CancelCommand.Execute(null);

            var numberOfStepsAfterDelete = processViewModel.ProcessStepViewModelItems.Count;

            // Verifies if process step from process  was saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool processStepExist = newContext.ProcessStepRepository.CheckIfExists(selectedProcessStep.Guid);
            Assert.AreEqual(numberOfStepsAfterDelete, numberOfStepsBeforeDelete - 1, "The process removed is recovered, it must remain deleted");
        }

        /// <summary>
        /// A test for when leaving the screen when view model is changed and the answer to the message dialog is yes, to save the data
        /// </summary>
        [TestMethod]
        public void OnUnloadingVMIsChangedTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.SaveCommand.Execute(null);

            var numberOfStepsBefore = processViewModel.ProcessStepViewModelItems.Count;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.RemoveStepCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.OnUnloading();

            var numberOfStepsAfter = processViewModel.ProcessStepViewModelItems.Count;

            // Verifies if process step from process  was saved.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assert.AreEqual(numberOfStepsBefore, numberOfStepsAfter + 1, "The process removed was recovered, it had to be deleted");
        }

        /// <summary>
        /// A test for when leaving the screen when view model is changed and the answer to the message dialog is yes, to save the data
        /// </summary>
        [TestMethod]
        public void AbortedOnUnloadingVMIsChangedTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = CreateNewRawPart();
            var partStep1 = new PartProcessStep();
            partStep1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep1.Machines.Add(machine1);
            part.Process.Steps.Add(partStep1);
            var partStep2 = new PartProcessStep();
            partStep2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            var machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            partStep2.Machines.Add(machine2);
            part.Process.Steps.Add(partStep2);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            var numberOfStepsBefore = processViewModel.Model.Steps.Count;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            processViewModel.RemoveStepCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            processViewModel.OnUnloading();

            var numberOfStepsAfter = processViewModel.Model.Steps.Count;

            // Verifies if process step from process  was saved.            
            Assert.AreEqual(numberOfStepsBefore, numberOfStepsAfter, "The process removed is not recovered");
        }

        /// <summary>
        /// A test for navigate to a process step or to an entity from a process step view model
        /// </summary>
        [TestMethod]
        public void NavigateToEntityInProcessTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            assyStep1.Machines.Add(machine1);
            assyStep1.Consumables.Add(consumable1);
            assyStep1.Commodities.Add(commodity1);
            assyStep1.Dies.Add(die1);
            assy.Process.Steps.Add(assyStep1);

            var assyStep2 = new AssemblyProcessStep();
            var machine2 = new Machine();
            var consumable2 = new Consumable();
            var commodity2 = new Commodity();
            var die2 = new Die();
            assyStep2.Machines.Add(machine2);
            assyStep2.Consumables.Add(consumable2);
            assyStep2.Commodities.Add(commodity2);
            assyStep2.Dies.Add(die2);
            assy.Process.Steps.Add(assyStep2);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            // Navigate to a process step widget
            var navMessage = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, assyStep2);
            messenger.Send(navMessage, GlobalMessengerTokens.MainViewTargetToken);
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), "The navigation to the process step failed");

            // Navigate to a machine
            var navMessage1 = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, machine2);
            messenger.Send(navMessage1, GlobalMessengerTokens.MainViewTargetToken);
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), "The navigation to the machine failed");

            // Navigate to a commodity
            var navMessage2 = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, commodity1);
            messenger.Send(navMessage2, GlobalMessengerTokens.MainViewTargetToken);
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(0), "The navigation to the commodity failed");

            // Navigate to a consumable
            var navMessage3 = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, consumable2);
            messenger.Send(navMessage3, GlobalMessengerTokens.MainViewTargetToken);
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), "The navigation to the consumable failed");

            // Navigate to a die
            var navMessage4 = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, die1);
            messenger.Send(navMessage4, GlobalMessengerTokens.MainViewTargetToken);
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(0), "The navigation to the die failed");
        }

        /// <summary>
        /// A test for synchronization between co2 emission data grid and the co2 emission chart by process
        /// </summary>
        [TestMethod]
        public void SynchronizeCO2DataGridAndChartTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assy = CreateNewAssembly();
            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            assyStep1.Machines.Add(machine1);
            assyStep1.Consumables.Add(consumable1);
            assyStep1.Commodities.Add(commodity1);
            assyStep1.Dies.Add(die1);
            assy.Process.Steps.Add(assyStep1);

            var assyStep2 = new AssemblyProcessStep();
            var machine2 = new Machine();
            var consumable2 = new Consumable();
            var commodity2 = new Commodity();
            var die2 = new Die();
            assyStep2.Machines.Add(machine2);
            assyStep2.Consumables.Add(consumable2);
            assyStep2.Commodities.Add(commodity2);
            assyStep2.Dies.Add(die2);
            assy.Process.Steps.Add(assyStep2);

            processViewModel.ParentProject = assy.Project;
            processViewModel.Parent = assy;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assy.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            // Synchronize the chart when data grid selection changes
            processViewModel.SelectedProcessCo2EmissionItem = processViewModel.ProcessCo2EmissionItems.ElementAtOrDefault(0);
            Assert.AreEqual(processViewModel.SelectedProcessCo2EmissionItem.ProcessStepId, processViewModel.SeletedCO2EmissionsByProcessChartItem.Id, "The synchronization failed");

            // Synchronize the data grid when chart selection changes
            processViewModel.SeletedCO2EmissionsByProcessChartItem = processViewModel.CO2ChartFossilItems.ElementAtOrDefault(1);
            Assert.AreEqual(processViewModel.SelectedProcessCo2EmissionItem.ProcessStepId, processViewModel.SeletedCO2EmissionsByProcessChartItem.Id, "The synchronization failed");
        }

        /// <summary>
        /// A test for when the process step parts or assembly amounts changes
        /// </summary>
        [TestMethod]
        public void ProcessStepPartsAndAssemblyAmountChangedTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assembly = this.CreateNewAssembly();

            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            assyStep1.Machines.Add(machine1);
            assyStep1.Consumables.Add(consumable1);
            assyStep1.Commodities.Add(commodity1);
            assyStep1.Dies.Add(die1);
            assembly.Process.Steps.Add(assyStep1);

            var assyStep2 = new AssemblyProcessStep();
            var machine2 = new Machine();
            var consumable2 = new Consumable();
            var commodity2 = new Commodity();
            var die2 = new Die();
            assyStep2.Machines.Add(machine2);
            assyStep2.Consumables.Add(consumable2);
            assyStep2.Commodities.Add(commodity2);
            assyStep2.Dies.Add(die2);
            assembly.Process.Steps.Add(assyStep2);

            var subAssy1 = this.CreateNewAssembly();
            subAssy1.ParentAssembly = assembly;
            assembly.Subassemblies.Add(subAssy1);

            var assy2 = this.CreateNewAssembly();
            assy2.ParentAssembly = assembly;
            assembly.Subassemblies.Add(assy2);

            var part1 = this.CreateNewPart();
            part1.Assembly = assembly;
            assembly.Parts.Add(part1);

            var part2 = this.CreateNewPart();
            part2.Assembly = assembly;
            assembly.Parts.Add(part2);

            processViewModel.ParentProject = assembly.Project;
            processViewModel.Parent = assembly;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assembly.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            // Check is the amount can be found in the rest of the view models when a part amount in changed
            processViewModel.ProcessStepViewModelItems.ElementAtOrDefault(0).StepParts.FirstOrDefault().Amount = 100;
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.ElementAtOrDefault(1).StepParts.FirstOrDefault().AmountSum, 100, "The amount sum didn't changed");

            // Check is the amount can be found in the rest of the view models when a assembly amount in changed
            processViewModel.ProcessStepViewModelItems.ElementAtOrDefault(0).StepParts.LastOrDefault().Amount = 100;
            Assert.AreEqual(processViewModel.ProcessStepViewModelItems.ElementAtOrDefault(1).StepParts.LastOrDefault().AmountSum, 100, "The amount sum didn't changed");
        }

        /// <summary>
        /// A test for add casting command when the process parent is an part
        /// </summary>
        [TestMethod]
        public void AddCastingWhenParentIsPartTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = this.CreateNewPart();

            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            partStep1.Machines.Add(machine1);
            partStep1.Consumables.Add(consumable1);
            partStep1.Commodities.Add(commodity1);
            partStep1.Dies.Add(die1);
            part.Process.Steps.Add(partStep1);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.AddCastingCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 10, "Process must have ten widget");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), "Wrong selection,the second widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 10, "Must exist ten scrap item");
        }

        /// <summary>
        /// A test for add moulding command when the process parent is an part and don't exist a raw material of Polymer type
        /// </summary>
        [TestMethod]
        public void AddMouldingWhenParentIsPartWithInvalidDataTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = this.CreateNewPart();

            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            partStep1.Machines.Add(machine1);
            partStep1.Consumables.Add(consumable1);
            partStep1.Commodities.Add(commodity1);
            partStep1.Dies.Add(die1);
            part.Process.Steps.Add(partStep1);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.AddMouldingCommand.Execute(null);

            // Check if are any modification in process screen
            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 1, "Process must have ten widget");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(0), "Wrong selection,the first widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 1, "Must exist ten scrap item");
        }

        /// <summary>
        /// A test for add moulding command when the process parent is an part and exist a raw material of Polymer type
        /// </summary>
        [TestMethod]
        public void AddMouldingWhenParentIsPartWithValidDataTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = this.CreateNewPart();

            var partStep1 = new PartProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            partStep1.Machines.Add(machine1);
            partStep1.Consumables.Add(consumable1);
            partStep1.Commodities.Add(commodity1);
            partStep1.Dies.Add(die1);
            part.Process.Steps.Add(partStep1);

            var material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.ParentWeight = 100;

            // Add the polymer classification to the raw material.
            var polymerClassification = new MaterialsClassification();
            polymerClassification.Guid = new Guid("f32882ee-c299-4991-9d6c-27449367aae6");

            material.MaterialsClassificationL1 = polymerClassification;

            part.RawMaterials.Add(material);

            processViewModel.ParentProject = part.Project;
            processViewModel.Parent = part;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = part.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.AddMouldingCommand.Execute(null);

            Assert.AreEqual(processViewModel.ProcessStepWidgetItems.Count, 3, "Process must have three widget");
            Assert.AreEqual(processViewModel.SelectedProcessStepWidget, processViewModel.ProcessStepWidgetItems.ElementAtOrDefault(1), "Wrong selection,the second widget must be selected");
            Assert.AreEqual(processViewModel.ScrapViewItems.Count, 3, "Must exist three scrap item");
        }

        /// <summary>
        /// A test for add casting command when the process parent is an assembly
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddCastingWhenParentIsAssemblyTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assembly = this.CreateNewAssembly();

            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            assyStep1.Machines.Add(machine1);
            assyStep1.Consumables.Add(consumable1);
            assyStep1.Commodities.Add(commodity1);
            assyStep1.Dies.Add(die1);
            assembly.Process.Steps.Add(assyStep1);

            processViewModel.ParentProject = assembly.Project;
            processViewModel.Parent = assembly;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assembly.Process;

            // Wait while the process data is loaded.
            while (!processViewModel.IsDataLoaded)
            {
                Thread.Sleep(100);
            }

            processViewModel.AddCastingCommand.Execute(null);
        }

        /// <summary>
        /// A test for add moulding command when the process parent is an assembly
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddMouldingWhenParentIsAssemblyTest()
        {
            var processViewModel = compositionContainer.GetExportedValue<ProcessViewModel>();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var assembly = this.CreateNewAssembly();

            var assyStep1 = new AssemblyProcessStep();
            var machine1 = new Machine();
            var consumable1 = new Consumable();
            var commodity1 = new Commodity();
            var die1 = new Die();
            assyStep1.Machines.Add(machine1);
            assyStep1.Consumables.Add(consumable1);
            assyStep1.Commodities.Add(commodity1);
            assyStep1.Dies.Add(die1);
            assembly.Process.Steps.Add(assyStep1);

            processViewModel.ParentProject = assembly.Project;
            processViewModel.Parent = assembly;
            processViewModel.DataSourceManager = dataContext;
            processViewModel.Model = assembly.Process;

            processViewModel.AddMouldingCommand.Execute(null);
        }

        #endregion Test Methods

        #region Other Methods

        /// <summary>
        /// Create a composition container using some parameters
        /// </summary>
        /// <param name="windowService">The windows service.</param>
        /// <param name="messenger">The messenger</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(
            out WindowServiceMock windowService,
            out IMessenger messenger)
        {
            var typeCatalog = new TypeCatalog(
               typeof(UnitsServiceMock),
               typeof(CostRecalculationCloneManager),
               typeof(CompositionContainer),
               typeof(DispatcherServiceMock),
               typeof(MessageDialogServiceMock),
               typeof(ModelBrowserHelperServiceMock),
               typeof(ProcessStepWidgetViewModel),
               typeof(FileDialogServiceMock),
               typeof(OnlineCheckServiceMock),
               typeof(CastingModuleNavigationServiceMock),
               typeof(MouldingModuleNavigationServiceMock),
               typeof(VideoViewModel),
               typeof(PicturesViewModel),
               typeof(ManufacturerViewModel),
               typeof(MediaViewModel),
               typeof(ClassificationSelectorViewModel),
               typeof(ProcessViewModel),
               typeof(ProcessStepViewModel),
               typeof(PleaseWaitService),
               typeof(CastingModuleViewModel),
               typeof(MouldingModuleViewModel));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            messenger = new Messenger();

            bootstrapper.CompositionContainer.ComposeParts(windowService, messenger);

            return bootstrapper.CompositionContainer;
        }

        /// <summary>
        /// Add a new assembly to database
        /// </summary>
        /// <returns>A new assembly object</returns>
        public Assembly CreateNewAssembly()
        {
            var assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CalculationVariant = "1.4";
            assembly.BatchSizePerYear = 1000;
            assembly.YearlyProductionQuantity = 2000;
            assembly.LifeTime = 1;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(5, true) };
            assembly.Process = new Process();
            assembly.Process.Assemblies.Add(assembly);
            assembly.Project = new Project();
            assembly.Project.OverheadSettings = new OverheadSetting();
            assembly.Project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            return assembly;
        }

        /// <summary>
        /// Add a new part to database
        /// </summary>
        /// <returns>A new part object</returns>
        public Part CreateNewPart()
        {
            var part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CalculationVariant = "1.4";
            part.BatchSizePerYear = 1000;
            part.YearlyProductionQuantity = 2000;
            part.LifeTime = 1;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(5, true) };
            part.Process = new Process();
            part.Process.Parts.Add(part);
            part.Project = new Project();
            part.Project.OverheadSettings = new OverheadSetting();
            part.Project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            return part;
        }

        /// <summary>
        /// Add a new part to database
        /// </summary>
        /// <returns>A new part object</returns>
        public Part CreateNewRawPart()
        {
            var part = new RawPart();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CalculationVariant = "1.4";
            part.BatchSizePerYear = 1000;
            part.YearlyProductionQuantity = 2000;
            part.LifeTime = 1;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(5, true) };
            part.Process = new Process();
            part.Process.Parts.Add(part);
            part.Project = new Project();
            part.Project.OverheadSettings = new OverheadSetting();
            part.Project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            return part;
        }

        /// <summary>
        /// Create a user to login
        /// </summary>
        /// <param name="dataManager">The data context</param>
        public void CreateUserToLogin(IDataSourceManager dataManager)
        {
            // Login into the application in order to set the current user
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);
        }

        #endregion Other Methods
    }
}
