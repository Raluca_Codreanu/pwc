﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for integration between MachiningCalculatorViewModel and its nested view models.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorIntegrationTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for integration with SetupTimeViewModel for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithSetupTimeForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = setupTimeViewModel;
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.PartName.Value = "part name";
            machiningCalculatorViewModel.OnLoaded();
            setupTimeViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with SetupTimeViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithSetupTimeForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorSetupTimeViewModel setupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = setupTimeViewModel;
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            // Create a new item to edit.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.PartPositioning;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;
            machiningCalculatorViewModel.OnLoaded();

            var oldToolChangeTime = machiningCalculatorViewModel.Model.ToolChangeTime.Value;

            setupTimeViewModel.SetupTimeOutsideMachine = 450;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            setupTimeViewModel.SaveCommand.Execute(null);

            var newToolChangeTime = machiningCalculatorViewModel.Model.ToolChangeTime.Value;

            Assert.AreNotEqual(oldToolChangeTime, newToolChangeTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        /// <summary>
        /// A test for integration with DrillingViewModel for the save command, when an item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithDrillingForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = drillingViewModel;
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();
            machiningCalculatorViewModel.PartName.Value = "part name";
            drillingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with DrillingViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithDrillingForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorDrillingViewModel drillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = drillingViewModel;
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            // Create a new calculation item.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.Drilling;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;
            machiningCalculatorViewModel.OnLoaded();

            var oldTransportClampingTime = machiningCalculatorViewModel.Model.TransportClampingTime.Value;

            drillingViewModel.DrillingDiameter = 95;
            drillingViewModel.AvgHoleDistance = 2300;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            drillingViewModel.SaveCommand.Execute(null);

            var newTransportClampingTime = machiningCalculatorViewModel.Model.TransportClampingTime.Value;

            Assert.AreNotEqual(oldTransportClampingTime, newTransportClampingTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        /// <summary>
        /// A test for integration with TurningViewModel for the save command, when an item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithTurningForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = turningViewModel;
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();
            machiningCalculatorViewModel.PartName.Value = "part name";
            turningViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with TurningViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithTurningForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorTurningViewModel turningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = turningViewModel;
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            // Create a new calculation item.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.Turning;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;

            var oldMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            turningViewModel.TurningLength = 555;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            machiningCalculatorViewModel.OnLoaded();
            turningViewModel.SaveCommand.Execute(null);

            var newMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            Assert.AreNotEqual(oldMachiningTime, newMachiningTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        /// <summary>
        /// A test for integration with MillingViewModel for the save command, when an item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithMillingForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = millingViewModel;
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();

            machiningCalculatorViewModel.PartName.Value = "part name";
            millingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with MillingViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithMillingForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorMillingViewModel millingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = millingViewModel;
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            // Create a new calculation item.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.Milling;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;

            var oldMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            millingViewModel.MillingDepth = 10;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            machiningCalculatorViewModel.OnLoaded();
            millingViewModel.SaveCommand.Execute(null);

            var newMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            Assert.AreNotEqual(oldMachiningTime, newMachiningTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        /// <summary>
        /// A test for integration with GearHobbingViewModel for the save command, when an item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithGearHobbingForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = gearHobbingViewModel;
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();

            machiningCalculatorViewModel.PartName.Value = "part name";
            gearHobbingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with GearHobbingViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithGearHobbingForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = gearHobbingViewModel;
            machiningCalculatorViewModel.GrindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            // Create a new calculation item.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.GearHobbing;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;
            machiningCalculatorViewModel.OnLoaded();

            var oldMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            gearHobbingViewModel.MillingToolDiameter = 2;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            gearHobbingViewModel.SaveCommand.Execute(null);

            var newMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            Assert.AreNotEqual(oldMachiningTime, newMachiningTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        /// <summary>
        /// A test for integration with GrindingViewModel for the save command, when an item is created.
        /// </summary>
        [TestMethod]
        public void IntegrationWithGrindingForCreateTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = grindingViewModel;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Create;
            machiningCalculatorViewModel.OnLoaded();

            machiningCalculatorViewModel.PartName.Value = "part name";
            grindingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(machiningCalculatorViewModel.Model, "The new calculation was not saved");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "part name", "The part name for calculation was not saved.");
        }

        /// <summary>
        /// A test for integration with GrindingViewModel for the save command, when an item is edited.
        /// </summary>
        [TestMethod]
        public void IntegrationWithGrindingForEditTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            MachiningCalculatorViewModel machiningCalculatorViewModel = new MachiningCalculatorViewModel(windowService, messenger);
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            machiningCalculatorViewModel.SetupTimeViewModel = new MachiningCalculatorSetupTimeViewModel(windowService, messenger);
            machiningCalculatorViewModel.DrillingViewModel = new MachiningCalculatorDrillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.TurningViewModel = new MachiningCalculatorTurningViewModel(windowService, messenger);
            machiningCalculatorViewModel.MillingViewModel = new MachiningCalculatorMillingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            machiningCalculatorViewModel.GrindingViewModel = grindingViewModel;

            // Create a new calculation item.
            var itemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);
            itemToEdit.MachiningType.Value = MachiningType.Grinding;
            itemToEdit.MachiningTime.Value = 5;

            machiningCalculatorViewModel.EditMode = ViewModelEditMode.Edit;
            machiningCalculatorViewModel.Model = itemToEdit;
            machiningCalculatorViewModel.OnLoaded();

            var oldMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            grindingViewModel.FeedSpeed = 110;
            machiningCalculatorViewModel.PartName.Value = "new part name";
            grindingViewModel.SaveCommand.Execute(null);

            var newMachiningTime = machiningCalculatorViewModel.Model.MachiningTime.Value;

            Assert.AreNotEqual(oldMachiningTime, newMachiningTime, "The calculation item was not updated.");
            Assert.AreEqual(machiningCalculatorViewModel.Model.RawPartDescription.Value, "new part name", "The raw part description was not updated.");
        }

        #endregion
    }
}
