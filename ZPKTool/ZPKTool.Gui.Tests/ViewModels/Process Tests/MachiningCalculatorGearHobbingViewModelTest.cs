﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorGearHobbingViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorGearHobbingViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorGearHobbingViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);

            gearHobbingViewModel.EditMode = ViewModelEditMode.Create;
            gearHobbingViewModel.IsExpertMode = false;
            gearHobbingViewModel.SelectedMaterial = gearHobbingViewModel.GearHobbingMaterials[2];
            gearHobbingViewModel.ProcessDescription = "description";
            gearHobbingViewModel.SelectedMillingToolType = GearHobbingMillingToolType.Hob;
            gearHobbingViewModel.FeedSpeed = 55;
            gearHobbingViewModel.SelectedMillingType = MillingType.FineMachining;
            gearHobbingViewModel.CutDepth = 20;
            gearHobbingViewModel.TeethNumber = 20;
            gearHobbingViewModel.MillingToolDiameter = 800;
            gearHobbingViewModel.StartStopTime = 5;
            gearHobbingViewModel.FeedLength = 200;
            gearHobbingViewModel.GearDiameter = 510;
            gearHobbingViewModel.GearWidth = 100;
            gearHobbingViewModel.GearModule = 50;
            gearHobbingViewModel.ToolChangeTime = 30;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, gearHobbingViewModel.MessengerTokenForMachining);

            gearHobbingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(gearHobbingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual("Gear Hobbing - " + GearHobbingMillingToolType.Hob, message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(255000000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.GearHobbing, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual(15m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(29932.71m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(30, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual("description", message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, gearHobbingViewModel.MessengerTokenForMachining);

            string data = "27;1;55;2;20;20;800;;5;200;510;100;50;False;0.214;30;12";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.GearHobbing;

            gearHobbingViewModel.EditMode = ViewModelEditMode.Edit;
            gearHobbingViewModel.Model = calculationItemToEdit;
            gearHobbingViewModel.MillingToolDiameter = 2;
            gearHobbingViewModel.GearModule = 500;
            gearHobbingViewModel.SaveCommand.Execute(null);

            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(gearHobbingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(2550000000, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(6250m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(74.83m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(30, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the resulting cycle and milling time are updated after some values are changed.
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);

            var millingTime1 = gearHobbingViewModel.ResultingMillingTime;
            var cycleTime1 = gearHobbingViewModel.ResultingCycleTime;

            gearHobbingViewModel.TeethNumber = 1;

            var millingTime2 = gearHobbingViewModel.ResultingMillingTime;
            var cycleTime2 = gearHobbingViewModel.ResultingCycleTime;

            gearHobbingViewModel.SelectedMaterial = gearHobbingViewModel.GearHobbingMaterials[4];

            var millingTime3 = gearHobbingViewModel.ResultingMillingTime;
            var cycleTime3 = gearHobbingViewModel.ResultingCycleTime;

            Assert.AreNotEqual(millingTime1, millingTime2, "The milling time value was not updated.");
            Assert.AreNotEqual(cycleTime1, cycleTime2, "The cycle time value was not updated.");

            Assert.AreNotEqual(millingTime2, millingTime3, "The milling time value was not updated.");
            Assert.AreNotEqual(cycleTime2, cycleTime3, "The cycle time value was not updated.");
        }

        /// <summary>
        /// A test for the SaveGearHobbingResultDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveGearHobbingResultDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGearHobbingViewModel gearHobbingViewModel = new MachiningCalculatorGearHobbingViewModel(windowService, messenger);

            gearHobbingViewModel.EditMode = ViewModelEditMode.Create;
            gearHobbingViewModel.IsExpertMode = true;
            gearHobbingViewModel.SelectedMaterial = gearHobbingViewModel.GearHobbingMaterials[2];
            gearHobbingViewModel.SelectedMillingToolType = GearHobbingMillingToolType.Hob;
            gearHobbingViewModel.FeedSpeed = 55;
            gearHobbingViewModel.SelectedMillingType = MillingType.RoughMachining;
            gearHobbingViewModel.CutDepth = 20;
            gearHobbingViewModel.TeethNumber = 20;
            gearHobbingViewModel.MillingToolDiameter = 800;
            gearHobbingViewModel.StartStopTime = 5;
            gearHobbingViewModel.FeedLength = 200;
            gearHobbingViewModel.GearDiameter = 510;
            gearHobbingViewModel.GearWidth = 100;
            gearHobbingViewModel.GearModule = 50;
            gearHobbingViewModel.ToolChangeTime = 30;

            gearHobbingViewModel.SaveCommand.Execute(null);

            string expectedData = "27;1;55;1;20;20;800;;5;200;510;100;50;True;0.214;30;139";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = gearHobbingViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}
