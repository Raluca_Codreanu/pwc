﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MachiningCalculatorGrindingViewModel and is intended 
    /// to contain unit tests for all public methods from MachiningCalculatorGrindingViewModel class.
    /// </summary>
    [TestClass]
    public class MachiningCalculatorGrindingViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command, when a new item is created.
        /// </summary>
        [TestMethod]
        public void SaveNewItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            grindingViewModel.EditMode = ViewModelEditMode.Create;
            grindingViewModel.IsExpertMode = true;
            grindingViewModel.FeedSpeed = 50;
            grindingViewModel.SelectedMaterial = grindingViewModel.GrindingMaterials[3];
            grindingViewModel.ProcessDescription = "description";
            grindingViewModel.SelectedGrindingOperationType = GrindingOperationType.CylindricalGrindingAxialInner;
            grindingViewModel.SelectedGrindingGrainSize = grindingViewModel.GrindingGrainSizes[0];
            grindingViewModel.SelectedCuttingDepth = grindingViewModel.GrindingCuttingDepths[0];
            grindingViewModel.GrindingToolDiameter = 20;
            grindingViewModel.GrindingToolWidth = 20;
            grindingViewModel.StartStopTime = 2;
            grindingViewModel.FeedLength = 40;
            grindingViewModel.GrindingPartDiameter = 40;
            grindingViewModel.GrindingLength = 50;
            grindingViewModel.GrindingWidth = 5;
            grindingViewModel.GrindingDepth = 5;
            grindingViewModel.ToolChangeTime = 322;
            grindingViewModel.TurningSpeed = 250;
            grindingViewModel.FeedRate = 250;

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, grindingViewModel.MessengerTokenForMachining);

            grindingViewModel.SaveCommand.Execute(null);

            Assert.IsNotNull(message, "The message sent is null");
            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(grindingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual("Grinding - " + GrindingOperationType.CylindricalGrindingAxialInner, message.Content.Description.Value, "The description is incorrect.");
            Assert.AreEqual(6250, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(MachiningType.Grinding, message.Content.MachiningType.Value, "The machining type is incorrect.");
            Assert.AreEqual(3334m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(298.06m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(322, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
            Assert.AreEqual("description", message.Content.ToleranceType.Value, "The tolerance type is incorrect.");
        }

        /// <summary>
        /// A test for the save command, when the values for an item are updated.
        /// </summary>
        [TestMethod]
        public void SaveUpdatedItemCommandTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);
            CycleTimeCalculationItemViewModel calculationItemToEdit = new CycleTimeCalculationItemViewModel(windowService, messenger);

            NotificationMessage<CycleTimeCalculationItemViewModel> message = null;
            messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>((msg) => message = msg, grindingViewModel.MessengerTokenForMachining);

            string data = "50;3;5;0;0;20;20;2;40;40;50;5;5;True;250;322;250";
            UnicodeEncoding encoding = new UnicodeEncoding();
            calculationItemToEdit.MachiningCalculationData.Value = encoding.GetBytes(data);
            calculationItemToEdit.MachiningType.Value = MachiningType.Grinding;

            grindingViewModel.EditMode = ViewModelEditMode.Edit;
            grindingViewModel.Model = calculationItemToEdit;
            grindingViewModel.SelectedGrindingOperationType = GrindingOperationType.SurfaceGrindingColinear;
            grindingViewModel.StartStopTime = 9;
            grindingViewModel.SaveCommand.Execute(null);

            Assert.AreEqual(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, message.Notification, "The notification sent is incorrect.");
            Assert.AreEqual(grindingViewModel.Model, message.Content, "The content sent is incorrect.");
            Assert.AreEqual(6250, message.Content.MachiningVolume.Value, "The machining volume is incorrect.");
            Assert.AreEqual(15003m, Math.Round(message.Content.TransportClampingTime.Value.GetValueOrDefault(), 0), "The transport clamping time is incorrect.");
            Assert.AreEqual(210.04m, Math.Round(message.Content.MachiningTime.Value.GetValueOrDefault(), 2), "The machining time is incorrect.");
            Assert.AreEqual(322, message.Content.ToolChangeTime.Value, "The tool change time is incorrect.");
        }

        /// <summary>
        /// A test for the UpdateSetupTime method, to check if the resulting cycle time, grinding time and h(m) are updated after some values are changed.
        /// </summary>
        [TestMethod]
        public void UpdateSetupTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            var grindingTime1 = grindingViewModel.ResultingGrindingTime;
            var cycleTime1 = grindingViewModel.ResultingCycleTime;

            grindingViewModel.GrindingToolDiameter = 55;

            var grindingTime2 = grindingViewModel.ResultingGrindingTime;
            var cycleTime2 = grindingViewModel.ResultingCycleTime;
            var resultingHM1 = grindingViewModel.ResultingHM;

            grindingViewModel.SelectedMaterial = grindingViewModel.GrindingMaterials[7];

            var grindingTime3 = grindingViewModel.ResultingGrindingTime;
            var cycleTime3 = grindingViewModel.ResultingCycleTime;
            var resultingHM2 = grindingViewModel.ResultingHM;

            Assert.AreNotEqual(grindingTime1, grindingTime2, "The grinding time value was not updated.");
            Assert.AreNotEqual(cycleTime1, cycleTime2, "The cycle time value was not updated.");

            Assert.AreNotEqual(grindingTime2, grindingTime3, "The grinding time value was not updated.");
            Assert.AreNotEqual(cycleTime2, cycleTime3, "The cycle time value was not updated.");
            Assert.AreNotEqual(resultingHM1, resultingHM2, "The resulting h(m) was not updated");
        }

        /// <summary>
        /// A test for the SaveGrindingResultDataToBinary method, to check if the machining calculation data is correctly calculated and saved to binary.
        /// </summary>
        [TestMethod]
        public void SaveGrindingResultDataToBinaryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            MachiningCalculatorGrindingViewModel grindingViewModel = new MachiningCalculatorGrindingViewModel(windowService, messenger);

            grindingViewModel.EditMode = ViewModelEditMode.Create;
            grindingViewModel.IsExpertMode = false;
            grindingViewModel.FeedSpeed = 10;
            grindingViewModel.SelectedMaterial = grindingViewModel.GrindingMaterials[7];
            grindingViewModel.SelectedGrindingOperationType = GrindingOperationType.CylindricalGrindingRadialOuter;
            grindingViewModel.SelectedGrindingGrainSize = grindingViewModel.GrindingGrainSizes[1];
            grindingViewModel.SelectedCuttingDepth = grindingViewModel.GrindingCuttingDepths[1];
            grindingViewModel.GrindingToolDiameter = 30;
            grindingViewModel.GrindingToolWidth = 30;
            grindingViewModel.StartStopTime = 5;
            grindingViewModel.FeedLength = 70;
            grindingViewModel.GrindingPartDiameter = 90;
            grindingViewModel.GrindingLength = 90;
            grindingViewModel.GrindingWidth = 9;
            grindingViewModel.GrindingDepth = 9;
            grindingViewModel.ToolChangeTime = 90;
            grindingViewModel.TurningSpeed = 400;
            grindingViewModel.FeedRate = 400;

            grindingViewModel.SaveCommand.Execute(null);

            string expectedData = "10;7;4;1;1;30;30;5;70;90;90;9;9;False;36;90;191";

            UnicodeEncoding encoder = new UnicodeEncoding();
            var model = grindingViewModel.Model;
            string data = encoder.GetString(model.MachiningCalculationData.Value);

            Assert.AreEqual(expectedData, data, "The machining calculation data is incorrect.");
        }

        #endregion
    }
}
