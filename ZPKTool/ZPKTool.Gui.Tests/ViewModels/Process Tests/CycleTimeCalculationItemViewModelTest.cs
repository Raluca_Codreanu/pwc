﻿using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CycleTimeCalculationItemViewModel and is intended 
    /// to contain unit tests for all public methods from CycleTimeCalculationItemViewModel class.
    /// </summary>
    [TestClass]
    public class CycleTimeCalculationItemViewModelTest
    {
        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #region Test Methods

        /// <summary>
        /// A test for creating a new calculation that added to the process step and saved.
        /// </summary>
        [TestMethod]
        public void CreateNewCalculationAndSaveItToProcessStepTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            CycleTimeCalculationItemViewModel calculationItemViewModel = new CycleTimeCalculationItemViewModel(windowService, messenger);
            calculationItemViewModel.DataSourceManager = dataManager;
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            ProcessStep processStep = new AssemblyProcessStep();

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            dataManager.ProcessStepRepository.Add(processStep);
            dataManager.SaveChanges();

            calculationItemViewModel.Model = calculation;
            calculationItemViewModel.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            processStep.CycleTimeCalculations.Add(calculation);
            calculationItemViewModel.SaveCommand.Execute(null);

            bool calculationAdded = processStep.CycleTimeCalculations.Contains(calculation);
            Assert.IsTrue(calculationAdded, "The new calculation was not added to the process step.");
        }

        /// <summary>
        /// A test for SetEditButtonVisibility method, when is set a machining type that supports calculation, and the edit button becomes visible.
        /// </summary>
        [TestMethod]
        public void EditButtonVisibilityForMachiningTypeWithCalculationSupportedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemViewModel = new CycleTimeCalculationItemViewModel(windowService, messenger);
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            ProcessStep processStep = new AssemblyProcessStep();
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            dataManager.ProcessStepRepository.Add(processStep);
            dataManager.SaveChanges();

            calculationItemViewModel.Model = calculation;
            calculationItemViewModel.MachiningType.Value = MachiningType.Drilling;

            var visibility = calculationItemViewModel.EditButtonVisibility;
            Assert.AreEqual(Visibility.Visible, visibility, "The edit button is not visible.");
        }

        /// <summary>
        /// A test for SetEditButtonVisibility method, when is set a machining type that does not support calculation,
        /// and the edit button becomes invisible.
        /// </summary>
        [TestMethod]
        public void EditButtonVisibilityForMachiningTypeWithCalculationNotSupportedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemViewModel = new CycleTimeCalculationItemViewModel(windowService, messenger);
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            ProcessStep processStep = new AssemblyProcessStep();
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            dataManager.ProcessStepRepository.Add(processStep);
            dataManager.SaveChanges();

            calculationItemViewModel.Model = calculation;
            calculationItemViewModel.MachiningType.Value = MachiningType.Boring;

            var visibility = calculationItemViewModel.EditButtonVisibility;
            Assert.AreEqual(Visibility.Collapsed, visibility, "The edit button is visible.");
        }

        /// <summary>
        /// A test for UpdateTotalTime method, when all the calculation object's properties are filled, and the total time is updated.
        /// </summary>
        [TestMethod]
        public void UpdateTotalTimeTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());

            CycleTimeCalculationItemViewModel calculationItemViewModel = new CycleTimeCalculationItemViewModel(windowService, messenger);
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            ProcessStep processStep = new AssemblyProcessStep();
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            processStep.Name = "Test Calculation" + DateTime.Now.Ticks;
            processStep.Accuracy = ProcessCalculationAccuracy.Estimated;
            processStep.CycleTime = 100;
            processStep.ProcessTime = 20;
            processStep.PartsPerCycle = 5;
            processStep.ScrapAmount = 20;
            processStep.Process = new Process();

            dataManager.ProcessStepRepository.Add(processStep);
            dataManager.SaveChanges();

            calculationItemViewModel.Model = calculation;

            var initialTotalTime = calculationItemViewModel.TotalTime;

            calculationItemViewModel.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationItemViewModel.MachiningTime.Value = 70;
            calculationItemViewModel.MachiningType.Value = MachiningType.Boring;
            calculationItemViewModel.MachiningVolume.Value = 40;
            calculationItemViewModel.RawPartDescription.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationItemViewModel.SurfaceType.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationItemViewModel.TakeOffTime.Value = 50;
            calculationItemViewModel.ToleranceType.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculationItemViewModel.ToolChangeTime.Value = 120;
            calculationItemViewModel.TransportClampingTime.Value = 20;
            calculationItemViewModel.TransportTimeFromBuffer.Value = 15;

            var updatedTotalTime = calculationItemViewModel.TotalTime;
            Assert.AreNotEqual(initialTotalTime, updatedTotalTime, "The total time value was not changed.");
        }

        #endregion
    }
}
