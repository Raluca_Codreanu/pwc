﻿using System;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Business;
using ZPKTool.Data;

namespace PROimpens.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  MassUpdateCountrySettingsViewModel and is intended 
    /// to contain unit tests for all public methods from  MassUpdateCountrySettingsViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateCountrySettingsViewModelTet
    {
        /// <summary>
        /// The UI composition framework bootstrapper.
        /// </summary>
        private Bootstrapper bootstrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateCountrySettingsViewModelTet"/> class.
        /// </summary>
        public MassUpdateCountrySettingsViewModelTet()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// A test for UpdateCommand -> Cancelling the update. 
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(11, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Create a clone of the part in order to verify that the part was not modified
            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext;
            massUpdateCountrySettingsViewModel.CurrentObject = part;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.No;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the updates were not applied
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            CountrySettingsAreEqual(dbPart, partClone);
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project and ApplyUpdatesToSubObjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateCountrySettingsTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

        //    // Set up the project for test
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly1 = new Assembly();
        //    assembly1.Name = assembly1.Guid.ToString();
        //    assembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(15, true);
        //    assembly1.AssemblingState = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    assembly1.OverheadSettings = new OverheadSetting();
        //    assembly1.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    part1.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(25, true);
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    project.Parts.Add(part1);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateCountrySettingsViewModel.CurrentObject = project;
        //    massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

        //    // Sets the new country settings to random values
        //    Random random = new Random();
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    windowService.MessageDialogResult = MessageDialogResult.Yes;
        //    massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = true;
        //    ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were applied to project's sub objects
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    ProjectUpdated(projectClone, dbProject, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Project and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the project for test
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly1.AssemblingState = EncryptionManager.Instance.GenerateRandomString(20, true);
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(20, true);
            part1.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(25, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            project.Parts.Add(part1);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Clone the project in order to check that the sub objects of project were not updated 
            CloneManager cloneManager = new CloneManager(dataContext);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext;
            massUpdateCountrySettingsViewModel.CurrentObject = project;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no sub object of the project were not updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly and ApplyUpdatesToSubObjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly1.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly2.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext1;
            massUpdateCountrySettingsViewModel.CurrentObject = assembly;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly1.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly2.AssemblingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Clone the parent assembly in order to check that its sub objects were not modified.
            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext;
            massUpdateCountrySettingsViewModel.CurrentObject = assembly;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated and the updates were saved in db
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and ApplyUpdatesToSubObjects = true. 
        /// In this case the ApplyUpdatesToSubObjects flag is not important 
        /// because the part doesn't have sub objects which have CountrySettings property.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext1;
            massUpdateCountrySettingsViewModel.CurrentObject = part;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated and the updates were saved in db
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object: Commodity.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataContext = dataContext;
            massUpdateCountrySettingsViewModel.CurrentObject = commodity;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        ///// <summary>
        ///// A test for BrowseCountryCommand.
        ///// </summary>
        //[TestMethod]
        //public void BrowseCountryTest()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

        //    windowService.IsOpen = false;
        //    ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseCountryCommand;
        //    browseCountryCommand.Execute(null);

        //    // Verify that the window was open
        //    Assert.IsTrue(windowService.IsOpen, "Failed to open the window.");
        //}

        ///// <summary>
        ///// A test for BrowseCountryCommand.
        ///// </summary>
        //[TestMethod]
        //public void BrowseSupplierTest()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

        //    windowService.IsOpen = false;
        //    massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(10, true);
        //    ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseSupplierCommand;
        //    browseCountryCommand.Execute(null);

        //    // Verify that the window was open
        //    Assert.IsTrue(windowService.IsOpen, "Failed to open the window.");
        //}

        #endregion Test Method

        #region Helpers

        /// <summary>
        /// Verify if the project was updated.
        /// </summary>
        /// <param name="oldProject"> The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    CountrySettingsAreEqual(oldPart, part);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    CountrySettingsAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the assembly was updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;

            if (viewModel.CountryNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.CountryNewValue, updatedAssembly.AssemblingCountry, "Failed to update the AssemblingCountry.");
                if (viewModel.SupplierNewValue != null)
                {
                    Assert.AreEqual<string>(viewModel.SupplierNewValue, updatedAssembly.AssemblingState, "Failed to update the AssemblingState.");
                }

                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldAssembly.AssemblingCountry, updatedAssembly.AssemblingCountry, "The AssemblingCountry should not have been updated if the new value was null.");
                Assert.AreEqual<string>(oldAssembly.AssemblingState, updatedAssembly.AssemblingState, "The AssemblingState should not have been updated if the new value was null.");
            }

            if (viewModel.NewCountrySettings != null)
            {
                Assert.IsTrue(CountrySettingsAreEqual(updatedAssembly.CountrySettings, viewModel.NewCountrySettings), "Failed to update the CountrySettings with the new values.");
            }
            else
            {
                Assert.IsTrue(CountrySettingsAreEqual(oldAssembly.CountrySettings, updatedAssembly.CountrySettings), "The CountrySettings should not have been updated if the new values are null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    CountrySettingsAreEqual(oldPart, part);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    CountrySettingsAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the part was updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;

            if (viewModel.CountryNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.CountryNewValue, updatedPart.ManufacturingCountry, "Failed to update the ManufacturingCountry.");
                if (viewModel.SupplierNewValue != null)
                {
                    Assert.AreEqual<string>(viewModel.SupplierNewValue, updatedPart.ManufacturingState, "Failed to update the ManufacturingState.");
                }

                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldPart.ManufacturingCountry, updatedPart.ManufacturingCountry, "The ManufacturingCountry should not have been updated if the new value was null.");
                Assert.AreEqual<string>(oldPart.ManufacturingState, updatedPart.ManufacturingState, "The ManufacturingCountry should not have been updated if the new value was null.");
            }

            if (viewModel.NewCountrySettings != null)
            {
                Assert.IsTrue(CountrySettingsAreEqual(updatedPart.CountrySettings, viewModel.NewCountrySettings), "Failed to update the country settings with the new values.");
            }
            else
            {
                Assert.IsTrue(CountrySettingsAreEqual(oldPart.CountrySettings, updatedPart.CountrySettings), "The CountrySettings should not have been updated if the new values are null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verify if the country settings(Country, State and CountrySettings) of two entities Parts are equal.
        /// </summary>
        /// <param name="comparedPart">The compared entity.</param>
        /// <param name="partToCompareWith">The entity to compare with.</param>
        /// <returns>True if the country settings are equal, false otherwise.</returns>
        private void CountrySettingsAreEqual(Part comparedPart, Part partToCompareWith)
        {
            bool result = false;
            result = comparedPart.ManufacturingCountry == partToCompareWith.ManufacturingCountry &&
                     comparedPart.ManufacturingState == partToCompareWith.ManufacturingState &&
                     CountrySettingsAreEqual(comparedPart.CountrySettings, partToCompareWith.CountrySettings);

            Assert.IsTrue(result, "The Country Settings of parts are not equal.");
        }

        /// <summary>
        /// Verify if the country settings(Country, State and CountrySettings) of two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void CountrySettingsAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            bool result = false;
            result = comparedAssembly.AssemblingCountry == assemblyToCompareWith.AssemblingCountry &&
                     comparedAssembly.AssemblingState == assemblyToCompareWith.AssemblingState &&
                     CountrySettingsAreEqual(comparedAssembly.CountrySettings, assemblyToCompareWith.CountrySettings);

            Assert.IsTrue(result, "The Country Settings of assemblies are not equal.");
            foreach (Part comparePart in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == comparePart.Name);
                CountrySettingsAreEqual(comparePart, partToCompareWith);
            }

            foreach (Assembly comparedSubassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == comparedSubassembly.Name);
                CountrySettingsAreEqual(comparedSubassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the values of two country settings are equal.
        /// </summary>
        /// <param name="compareCountrySetting">The compared CountrySetting.</param>
        /// <param name="countrySettingToCompareWith">The CountrySetting to compare with.</param>
        private bool CountrySettingsAreEqual(CountrySetting comparedCountrySetting, CountrySetting countrySettingToCompareWith)
        {
            bool result = comparedCountrySetting.AirCost == countrySettingToCompareWith.AirCost &&
                         comparedCountrySetting.EnergyCost == countrySettingToCompareWith.EnergyCost &&
                         comparedCountrySetting.EngineerCost == countrySettingToCompareWith.EngineerCost &&
                         comparedCountrySetting.ForemanCost == countrySettingToCompareWith.ForemanCost &&
                         comparedCountrySetting.InterestRate == countrySettingToCompareWith.InterestRate &&
                         comparedCountrySetting.LaborAvailability == countrySettingToCompareWith.LaborAvailability &&
                         comparedCountrySetting.OfficeAreaRentalCost == countrySettingToCompareWith.OfficeAreaRentalCost &&
                         comparedCountrySetting.ProductionAreaRentalCost == countrySettingToCompareWith.ProductionAreaRentalCost &&
                         comparedCountrySetting.ShiftCharge1ShiftModel == countrySettingToCompareWith.ShiftCharge1ShiftModel &&
                         comparedCountrySetting.ShiftCharge2ShiftModel == countrySettingToCompareWith.ShiftCharge2ShiftModel &&
                         comparedCountrySetting.ShiftCharge3ShiftModel == countrySettingToCompareWith.ShiftCharge3ShiftModel &&
                         comparedCountrySetting.SkilledLaborCost == countrySettingToCompareWith.SkilledLaborCost &&
                         comparedCountrySetting.TechnicianCost == countrySettingToCompareWith.TechnicianCost &&
                         comparedCountrySetting.UnskilledLaborCost == countrySettingToCompareWith.UnskilledLaborCost &&
                         comparedCountrySetting.WaterCost == countrySettingToCompareWith.WaterCost;

            return result;
        }

        #endregion Helpers
    }
}
