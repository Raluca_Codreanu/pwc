﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ShellViewModel and is intended 
    /// to contain unit tests for all commands from ShellViewModel
    /// </summary>
    [TestClass]
    public class ShellViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModelTest"/> class.
        /// </summary>
        public ShellViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests Logout command
        ///// </summary>
        //[TestMethod]
        //public void LogoutCommandTest()
        //{
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    ShellViewMock view = new ShellViewMock();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();
        //    ShellViewModel shellViewModel = new ShellViewModel(container, view, messageBoxService, messenger);
        //    shellViewModel.ContentView = new LoginViewMock();

        //    // Register an action which will handle message of type NotificationMessage
        //    bool loggedOutNotificationSend = false;
        //    messenger.Register<NotificationMessage>((msg) =>
        //    {
        //        if (msg.Notification == Notifications.Notification.LoggedOut)
        //        {
        //            loggedOutNotificationSend = true;
        //        }
        //    });

        //    // Executes the Logout command
        //    ICommand logoutCommand = shellViewModel.LogoutCommand;
        //    logoutCommand.Execute(null);

        //    User currentUser = SecurityManager.Instance.CurrentUser;
        //    Assert.IsNull(currentUser, "Failed to logout from the application");
        //    Assert.IsTrue(loggedOutNotificationSend, "LoggedOut notification was not sent");
        //}        
    }
}
