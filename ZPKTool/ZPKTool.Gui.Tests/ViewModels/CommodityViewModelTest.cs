﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// Commodity View Model Test
    /// </summary>
    [TestClass]
    public class CommodityViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The Test Content.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The cost recalculation Clone Manager.
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The manufacturer view-model.
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The MasterDataBrowser view-model.
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The media view-model.
        /// </summary>
        private MediaViewModel media;

        /// <summary>
        /// The units service
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

         /// <summary>
         /// Use ClassInitialize to run code before running the first test in the class.
         /// </summary>
         /// <param name="testContext"> Used to indicate text context</param>
         [ClassInitialize]
         public static void MyClassInitialize(TestContext testContext) 
         {
             Utils.ConfigureDbAccess();
             LocalDataCache.Initialize();
         }
        
         /// <summary>
         /// Use ClassCleanup to run code after all tests in a class have run.
         /// </summary>
         [ClassCleanup]
         public static void MyClassCleanup() 
         {
         }
        
         /// <summary>
         /// Use TestInitialize to run code before running each test.
         /// </summary>
         [TestInitialize]
         public void MyTestInitialize() 
         {
             this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
             this.messenger = new Messenger();
             this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
             this.unitsService = new UnitsServiceMock();
             this.costRecalculationCloneManager = new CostRecalculationCloneManager();
             this.manufacturer = new ManufacturerViewModel(this.windowService, this.messenger, new CompositionContainer());
             var videoViewModel = new VideoViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
             var picturesViewModel = new PicturesViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
             this.media = new MediaViewModel(
                 videoViewModel,
                 picturesViewModel,
                 this.messenger,
                 this.windowService,
                 new FileDialogServiceMock(),
                 new ModelBrowserHelperServiceMock());
             this.masterDataBrowser = new MasterDataBrowserViewModel(
                 new CompositionContainer(),
                 this.messenger,
                 this.windowService,
                 new OnlineCheckService(this.messenger),
                 this.unitsService);
         }
        
         /// <summary>
         /// Use TestCleanup to run code after each test has run
         /// </summary>
         [TestCleanup]
         public void MyTestCleanup() 
         { 
         }
        
        #endregion

        #region Test Methods

         /// <summary>
         /// This method tests the undo button.
         /// </summary>
         [TestMethod]
         public void UndoCommandTest()
         {
             var commodityVm = new CommodityViewModel(
                 this.windowService,
                 this.messenger,
                 this.modelBrowserHelperService,
                 this.costRecalculationCloneManager,
                 this.manufacturer,
                 this.masterDataBrowser,
                 this.media,
                 this.unitsService);

             var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
             commodityVm.DataSourceManager = localDataManager;

             var commodity = this.CreateNewCommodity(localDataManager);
             commodityVm.Model = commodity;
             commodityVm.Price.Value = 200;
             var undoCommand = commodityVm.UndoCommand;
             undoCommand.Execute(null);

             Assert.AreEqual(commodityVm.CommodityCost, 0);
         }

         /// <summary>
         /// This method tests the editable fields from General and manufacturer Tabs, Save and Cancel commands.
         /// </summary>
         [TestMethod]
         public void EditableFieldsSaveAndCancelTest()
         {
             var wndServiceMock = new Mock<IWindowService>();
             wndServiceMock.Setup(
                 service =>
                 service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                 .Returns(MessageDialogResult.Yes);
             var commodityVm = new CommodityViewModel(
                 wndServiceMock.Object,
                 this.messenger,
                 this.modelBrowserHelperService,
                 this.costRecalculationCloneManager,
                 this.manufacturer,
                 this.masterDataBrowser,
                 this.media,
                 this.unitsService);

             var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
             var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
             var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);
             
             commodityVm.DataSourceManager = localDataManager;
             var commodity = this.CreateNewCommodity(localDataManager);
             commodityVm.Model = commodity;

             var name = EncryptionManager.Instance.GenerateRandomString(10, true);
             const decimal Price = 200;
             const string PartNumber = "200";
             const decimal Ratio = 33;
             const decimal WeightValue = 500;
             var remarks = EncryptionManager.Instance.GenerateRandomString(15, true);
             var description = EncryptionManager.Instance.GenerateRandomString(10, true);
             var weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");
             const int Amount = 100;
             var nameManufacturer = EncryptionManager.Instance.GenerateRandomString(10, true);

             commodityVm.Name.Value = name;
             commodityVm.Price.Value = Price;
             commodityVm.PartNumber.Value = PartNumber;
             commodityVm.Amount.Value = Amount;
             commodityVm.RejectRatio.Value = Ratio;
             commodityVm.Weight.Value = WeightValue;
             commodityVm.WeightUnit.Value = weightUnit;
             commodityVm.Remarks.Value = remarks;
             this.manufacturer.Name.Value = nameManufacturer;
             this.manufacturer.Description.Value = description; 

             var saveCommand = commodityVm.SaveCommand;
             saveCommand.Execute(null);

             Assert.AreEqual(commodity.Price, commodityVm.Price.Value);
             Assert.AreEqual(commodity.PartNumber, commodityVm.PartNumber.Value);
             Assert.AreEqual(commodity.RejectRatio, commodityVm.RejectRatio.Value);
             Assert.AreEqual(commodity.Weight, commodityVm.Weight.Value);
             Assert.AreEqual(commodity.WeightUnitBase, commodityVm.WeightUnit.Value);
             Assert.AreEqual(commodity.Remarks, commodityVm.Remarks.Value);
             Assert.AreEqual(commodity.Name, commodityVm.Name.Value);
             Assert.AreEqual(commodity.Amount, commodityVm.Amount.Value);
             Assert.AreEqual(commodity.Manufacturer.Name, this.manufacturer.Name.Value);
             Assert.AreEqual(commodity.Manufacturer.Description, this.manufacturer.Description.Value);
             
             commodityVm.Price.Value = 400;
             commodityVm.PartNumber.Value = "50";
             commodityVm.RejectRatio.Value = 5;
             commodityVm.Weight.Value = 100;
             commodityVm.WeightUnit.Value =
                 weightUnits.First(u => string.Equals(u.Symbol, "Kg", StringComparison.InvariantCultureIgnoreCase));
             commodityVm.Remarks.Value = EncryptionManager.Instance.GenerateRandomString(15, true);
             this.manufacturer.Description.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

             var cancelCommand = commodityVm.CancelCommand;
             cancelCommand.Execute(null);

             Assert.AreEqual(commodity.Price, Price);
             Assert.AreEqual(commodity.PartNumber, PartNumber);
             Assert.AreEqual(commodity.RejectRatio, Ratio);
             Assert.AreEqual(commodity.Weight, WeightValue);
             Assert.AreEqual(commodity.WeightUnitBase, weightUnit);
             Assert.AreEqual(commodity.Remarks, remarks);
             Assert.AreEqual(commodity.Manufacturer.Description, description);
         }

         #endregion Test Methods

        #region Private Methods

         /// <summary>
         /// This method creates a new commodity 
         /// </summary>
         /// <returns>Returns commodity</returns>
         /// <param name="dataManager"> Used for dataManager </param>
         private Commodity CreateNewCommodity(IDataSourceManager dataManager)
        {
            var commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            commodity.Amount = 100;

             var newManufacturer = new Manufacturer { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            commodity.Manufacturer = newManufacturer;

            dataManager.ManufacturerRepository.Save(newManufacturer);
            dataManager.CommodityRepository.Save(commodity);
            dataManager.SaveChanges();

            return commodity;
        }

        #endregion Private Methods
    }
}
