﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CountrySupplierViewModel and is intended 
    /// to contain Unit Tests for all the public methods from CountrySupplierViewModel class.
    /// </summary>
    [TestClass]
    public class CountrySupplierViewModelTest
    {
        /// <summary>
        /// The test context
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        /// <summary>
        /// Configure database access before running the first test in the class
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #endregion

        /// <summary>
        /// Create a composition container using parameters
        /// </summary>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The windows service.</param>
        /// <param name="unitsService">The units service</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(out MessageDialogServiceMock messageDialogService, out WindowServiceMock windowService, out IUnitsService unitsService)
        {
            messageDialogService = new MessageDialogServiceMock();
            windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            unitsService = new UnitsServiceMock();

            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel, unitsService);

            return compositionContainer;
        }

        /// <summary>
        /// Create a new country
        /// </summary>
        /// <returns>The new created country</returns>
        public Country NewCountry()
        {
            Country newCountry = new Country();
            newCountry.CountrySetting = new CountrySetting();
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            var c = new Currency()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true),
                IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true)
            };
            newCountry.Currency = c;

            var mu = new MeasurementUnit()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true)
            };
            newCountry.FloorMeasurementUnit = mu;
            newCountry.LengthMeasurementUnit = mu;
            newCountry.TimeMeasurementUnit = mu;
            newCountry.VolumeMeasurementUnit = mu;
            newCountry.WeightMeasurementUnit = mu;

            return newCountry;
        }

        #region Test Methods

        /// <summary>
        /// A test for the save command in Create Mode with a valid model
        /// </summary>
        [TestMethod]
        public void SaveCreateModeTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            countrySupplierViewModel.SaveCommand.Execute(null);

            IDataSourceManager newManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool supplierSaved = newManager.CountrySupplierRepository.CheckIfExists(newSupplier.Guid);
            Assert.IsTrue(supplierSaved, "The supplier was not saved");
        }

        /// <summary>
        /// A test for the save command in Edit Mode with a valid model
        /// </summary>
        [TestMethod]
        public void SaveEditModeTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            countrySupplierViewModel.EditMode = ViewModelEditMode.Edit;
            newSupplier.CountrySettings.EnergyCost = 2;
            countrySupplierViewModel.SaveCommand.Execute(null);

            IDataSourceManager newManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool supplierSaved = newManager.CountrySupplierRepository.CheckIfExists(newSupplier.Guid);
            Assert.IsTrue(supplierSaved, "The supplier was not saved");
        }

        /// <summary>
        /// A test for the save command with a valid model, in Edit Mode, with an error message
        /// because of already existing supplier name
        /// </summary>
        [TestMethod]
        public void NotSavedSupplierExistsTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier1 = new CountryState();
            newSupplier1.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier1);
            var name1 = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier1.Name = name1;
            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier1;

            CountryState newSupplier2 = new CountryState();
            newSupplier2.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier2);

            var name2 = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier2.Name = name2;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier2;

            dataManager.CountrySupplierRepository.Add(newSupplier1);
            dataManager.CountrySupplierRepository.Add(newSupplier2);
            dataManager.SaveChanges();

            newSupplier2.Name = name1;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            countrySupplierViewModel.SaveCommand.Execute(null);

            Assert.AreNotEqual(name2, newSupplier2.Name, "The supplier was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is yes
        /// </summary>
        [TestMethod]
        public void CancelShowCancelMessageYesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.Name = name;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            countrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.CancelCommand.Execute(null);

            Assert.AreEqual(name, newSupplier.Name, "The supplier was modified");
        }

        /// <summary>
        /// A test for the cancel command when view model is changed, the flag for the cancel dialog box is true
        /// and the answer to the message dialog is no
        /// </summary>
        [TestMethod]
        public void CancelShowCancelMessageNoTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.Name = name;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.ShowCancelMessageFlag = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            countrySupplierViewModel.CancelCommand.Execute(null);

            Assert.AreNotEqual(name, newSupplier.Name, "The supplier was not modified");
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is changed and it's in edit mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingForCreateWithConfirmedChangesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = countrySupplierViewModel.OnUnloading();

            Assert.IsTrue(status && !countrySupplierViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is changed and it's in create mode
        /// and the user do not confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingForCreateWithAbortedChangesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.IsInViewerMode = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            var status = countrySupplierViewModel.OnUnloading();

            Assert.IsFalse(status && !countrySupplierViewModel.IsChanged);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is changed and it's in edit mode
        /// and the user confirms cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingForEditWithConfirmedChangesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.Name = name;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.EditMode = ViewModelEditMode.Edit;
            countrySupplierViewModel.IsInViewerMode = false;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = countrySupplierViewModel.OnUnloading();

            Assert.AreEqual(newSupplier.Name, newName);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is changed and it's in edit mode
        /// and the user does not confirm cancel action
        /// </summary>
        [TestMethod]
        public void OnUnloadingForEditWithAbortedChangesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.Name = name;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.EditMode = ViewModelEditMode.Edit;
            countrySupplierViewModel.IsInViewerMode = false;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            countrySupplierViewModel.OnUnloading();

            Assert.AreNotEqual(newSupplier.Name, newName);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is changed but there are missing mandatory data in required fields.
        /// </summary>
        [TestMethod]
        public void OnUnloadingWithMissingMandatoryDataTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            countryViewModel.DataSourceManager = dataManager;
            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            countrySupplierViewModel.DataSourceManager = dataManager;
            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = new CountrySetting();
            newCountry.States.Add(newSupplier);
            var name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.Name = name;

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            countrySupplierViewModel.IsChanged = true;
            countrySupplierViewModel.EditMode = ViewModelEditMode.Edit;
            countrySupplierViewModel.IsInViewerMode = false;

            countrySupplierViewModel.Name.Value = string.Empty;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var status = countrySupplierViewModel.OnUnloading();

            Assert.IsFalse(status);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The CountrySupplierViewModel is in create mode, with changed values and the model is in view mode
        /// The message box should not appear
        /// </summary>
        [TestMethod]
        public void OnUnloadingInViewModeTest()
        {
            var windowServiceMock = new Mock<IWindowService>();
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(new UnitsServiceMock()), windowServiceMock.Object);
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            CountryState newSupplier = new CountryState();
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            newSupplier.CountrySettings = new CountrySetting();

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.IsInViewerMode = true;
            countrySupplierViewModel.DataSourceManager = dataManager;
            countrySupplierViewModel.Model = newSupplier;

            countrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            bool serviceCallHappened = false;
            windowServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo))
                .Callback(() =>
                    {
                        serviceCallHappened = true;
                    });

            var unloadingStatus = countrySupplierViewModel.OnUnloading();

            Assert.IsFalse(serviceCallHappened);
            Assert.IsTrue(unloadingStatus);
        }

        /// <summary>
        /// A test for the supplier`s initialization with a null parent country as argument.
        /// The code should throw an ArgumentNullException
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InitializeForCreationWithNullParentCountryTest()
        {
            var windowServiceMock = new Mock<IWindowService>();
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(null), windowServiceMock.Object);
            countrySupplierViewModel.InitializeForCreation(null);
        }

        /// <summary>
        /// A test for the supplier`s initialization with a valid parent country as argument
        /// </summary>
        [TestMethod]
        public void InitializeForCreationWithValidParentCountryTest()
        {
            var windowServiceMock = new Mock<IWindowService>();
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(new UnitsServiceMock()), windowServiceMock.Object);
            countrySupplierViewModel.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country parentCountry = new Country();
            parentCountry.Name = "Test Country";
            parentCountry.CountrySetting = new CountrySetting()
            {
                AirCost = 1,
                EnergyCost = 2,
                EngineerCost = 3,
                ShiftCharge1ShiftModel = 0.5m
            };

            countrySupplierViewModel.InitializeForCreation(parentCountry);
            var countrySupplier = countrySupplierViewModel.Model;

            Assert.AreEqual(countrySupplierViewModel.EditMode, ViewModelEditMode.Create);
            Assert.IsNotNull(countrySupplier);
            Assert.AreEqual(countrySupplier.Country, parentCountry);
            Assert.IsNotNull(countrySupplier.CountrySettings);
            Assert.IsTrue(countrySupplier.CountrySettings.EqualsValues(parentCountry.CountrySetting));
        }

        /// <summary>
        /// A test for verifying that the program will not throw an exception if the model is null when CancelCommand is executed
        /// </summary>
        [TestMethod]
        public void CancelWithNullModelTest()
        {
            var windowServiceMock = new Mock<IWindowService>();
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(new UnitsServiceMock()), windowServiceMock.Object);
            countrySupplierViewModel.CancelCommand.Execute(null);
        }
        #endregion
    }
}
