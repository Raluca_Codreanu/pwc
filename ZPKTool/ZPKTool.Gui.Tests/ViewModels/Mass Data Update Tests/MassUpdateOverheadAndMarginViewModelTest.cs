﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  MassUpdateOverheadAndMarginViewModel and is intended 
    /// to contain unit tests for all public methods from  MassUpdateOverheadAndMarginViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateOverheadAndMarginViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateOverheadAndMarginViewModelTest"/> class.
        /// </summary>
        public MassUpdateOverheadAndMarginViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand -> cancelling the update operation. 
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            part.OverheadSettings = overheadSettings;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = part;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.OtherCostOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.PackagingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.LogisticOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.MaterialMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginNewValue = random.Next(1000);

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            OverheadSettingsAreEqual(partClone.OverheadSettings, dbPart.OverheadSettings);
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            part.OverheadSettings = overheadSettings;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = part;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.OtherCostOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.PackagingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.LogisticOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.MaterialMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginNewValue = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            OverheadSettingsUpdated(partClone.OverheadSettings, dbPart.OverheadSettings, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            part.OverheadSettings = overheadSettings;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = part;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ConsumableOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.CommodityOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.OtherCostOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.PackagingOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.LogisticOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.MaterialMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.CommodityMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginPercent = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            OverheadSettingsUpdated(partClone.OverheadSettings, dbPart.OverheadSettings, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            part.OverheadSettings = overheadSettings;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateOverheadAndMarginViewModel.Model = part;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were not updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            OverheadSettingsUpdated(partClone.OverheadSettings, dbPart.OverheadSettings, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are set to valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            assembly.OverheadSettings = overheadSettings;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = overheadSettings.Copy();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = overheadSettings.Copy();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = overheadSettings.Copy();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = overheadSettings.Copy();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = assembly;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.OtherCostOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.PackagingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.LogisticOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.MaterialMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginNewValue = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are expressed in percentages and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            assembly.OverheadSettings = overheadSettings;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = overheadSettings.Copy();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = overheadSettings.Copy();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = overheadSettings.Copy();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = overheadSettings.Copy();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateOverheadAndMarginViewModel.Model = assembly;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ConsumableOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.CommodityOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.OtherCostOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.PackagingOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.LogisticOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.MaterialMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.CommodityMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginPercent = random.Next(100);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginPercent = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are set to null and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            assembly.OverheadSettings = overheadSettings;

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = overheadSettings.Copy();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = overheadSettings.Copy();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = overheadSettings.Copy();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = overheadSettings.Copy();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateOverheadAndMarginViewModel.Model = assembly;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Project, the new values are set to valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();

            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            project.OverheadSettings = overheadSettings;

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = overheadSettings.Copy();
            project.Assemblies.Add(assembly);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = overheadSettings.Copy();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = overheadSettings.Copy();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = overheadSettings.Copy();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = overheadSettings.Copy();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = project;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.OtherCostOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.PackagingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.LogisticOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.MaterialMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginNewValue = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the OHSettings of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project, the new values are expressed in percentages and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateOHSettingsTest9()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();

        //    Random random = new Random();
        //    OverheadSetting overheadSettings = new OverheadSetting();
        //    overheadSettings.MaterialOverhead = random.Next(1000);
        //    overheadSettings.ConsumableOverhead = random.Next(1000);
        //    overheadSettings.CommodityOverhead = random.Next(1000);
        //    overheadSettings.ExternalWorkOverhead = random.Next(1000);
        //    overheadSettings.ManufacturingOverhead = random.Next(1000);
        //    overheadSettings.OtherCostOHValue = random.Next(1000);
        //    overheadSettings.PackagingOHValue = random.Next(1000);
        //    overheadSettings.LogisticOHValue = random.Next(1000);
        //    overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
        //    overheadSettings.MaterialMargin = random.Next(1000);
        //    overheadSettings.ConsumableMargin = random.Next(1000);
        //    overheadSettings.CommodityMargin = random.Next(1000);
        //    overheadSettings.ExternalWorkMargin = random.Next(1000);
        //    overheadSettings.ManufacturingMargin = random.Next(1000);
        //    project.OverheadSettings = overheadSettings;

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.OverheadSettings = overheadSettings;
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.OverheadSettings = overheadSettings.Copy();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly2.OverheadSettings = overheadSettings.Copy();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = overheadSettings.Copy();
        //    subassembly1.Parts.Add(part1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = overheadSettings.Copy();
        //    project.Parts.Add(part2);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateOverheadAndMarginViewModel.CurrentObject = project;
        //    massUpdateOverheadAndMarginViewModel.IDataSourceManager = dataContext1;
        //    massUpdateOverheadAndMarginViewModel.MaterialOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ConsumableOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.CommodityOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ExternalWorkOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ManufacturingOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.OtherCostOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.PackagingOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.LogisticOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.SalesAndAdminOHPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.MaterialMarginPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ConsumableMarginPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.CommodityMarginPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ExternalWorkMarginPercent = random.Next(100);
        //    massUpdateOverheadAndMarginViewModel.ManufacturingMarginPercent = random.Next(100);

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the OHSettings of the current part were updated
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project, the new values are set to null and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateOHSettingsTest10()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();

        //    Random random = new Random();
        //    OverheadSetting overheadSettings = new OverheadSetting();
        //    overheadSettings.MaterialOverhead = random.Next(1000);
        //    overheadSettings.ConsumableOverhead = random.Next(1000);
        //    overheadSettings.CommodityOverhead = random.Next(1000);
        //    overheadSettings.ExternalWorkOverhead = random.Next(1000);
        //    overheadSettings.ManufacturingOverhead = random.Next(1000);
        //    overheadSettings.OtherCostOHValue = random.Next(1000);
        //    overheadSettings.PackagingOHValue = random.Next(1000);
        //    overheadSettings.LogisticOHValue = random.Next(1000);
        //    overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
        //    overheadSettings.MaterialMargin = random.Next(1000);
        //    overheadSettings.ConsumableMargin = random.Next(1000);
        //    overheadSettings.CommodityMargin = random.Next(1000);
        //    overheadSettings.ExternalWorkMargin = random.Next(1000);
        //    overheadSettings.ManufacturingMargin = random.Next(1000);
        //    project.OverheadSettings = overheadSettings;

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.OverheadSettings = overheadSettings;
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.OverheadSettings = overheadSettings.Copy();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly2.OverheadSettings = overheadSettings.Copy();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = overheadSettings.Copy();
        //    subassembly1.Parts.Add(part1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = overheadSettings.Copy();
        //    project.Parts.Add(part2);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateOverheadAndMarginViewModel.CurrentObject = project;
        //    massUpdateOverheadAndMarginViewModel.IDataSourceManager = dataContext1;
        //    ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the OHSettings of the current part were updated
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateOverheadAndMarginViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        ///  A test for UpdateCommand. The following input data were used:
        /// Current object -> RawMaterial.
        /// </summary>
        [TestMethod]
        public void UpdateOHSettingsTest11()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateOverheadAndMarginViewModel massUpdateOverheadAndMarginViewModel = new MassUpdateOverheadAndMarginViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material);
            dataContext1.SaveChanges();

            // Set up the view model
            Random random = new Random();
            massUpdateOverheadAndMarginViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateOverheadAndMarginViewModel.Model = material;
            massUpdateOverheadAndMarginViewModel.DataSourceManager = dataContext1;
            massUpdateOverheadAndMarginViewModel.MaterialOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.OtherCostOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.PackagingOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.LogisticOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.SalesAndAdminOHNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.MaterialMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ConsumableMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.CommodityMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ExternalWorkMarginNewValue = random.Next(1000);
            massUpdateOverheadAndMarginViewModel.ManufacturingMarginNewValue = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateOverheadAndMarginViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verify if an OverheadSettings entity was updated according to specification.
        /// </summary>
        /// <param name="oldOHSettings">The old OHSettings.</param>
        /// <param name="updatedOHSettings">The updated OHSettings.</param>
        /// <param name="viewModel">The MassUpdateOverheadAndMarginViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void OverheadSettingsUpdated(OverheadSetting oldOHSettings, OverheadSetting updatedOHSettings, MassUpdateOverheadAndMarginViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;

            if (viewModel.MaterialOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.MaterialOHNewValue.Value, updatedOHSettings.MaterialOverhead, "Failed to update the OHSettings -> MaterialOverhead with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.MaterialOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.MaterialOHPercent.Value * oldOHSettings.MaterialOverhead, updatedOHSettings.MaterialOverhead, "Failed to update the OHSettings -> MaterialOverhead with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.MaterialOverhead, updatedOHSettings.MaterialOverhead, "The OHSettings -> MaterialOverhead value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ConsumableOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ConsumableOHNewValue.Value, updatedOHSettings.ConsumableOverhead, "Failed to update the OHSettings -> ConsumableOverhead with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ConsumableOHPercent != null)
            {
                Assert.AreEqual<decimal>(oldOHSettings.ConsumableOverhead * viewModel.ConsumableOHPercent.Value, updatedOHSettings.ConsumableOverhead, "Failed to update the OHSettings -> ConsumableOverhead with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ConsumableOverhead, updatedOHSettings.ConsumableOverhead, "The OHSettings -> ConsumableOverhead value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.CommodityOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.CommodityOHNewValue.Value, updatedOHSettings.CommodityOverhead, "Failed to update the OHSettings -> CommodityOverhead with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.CommodityOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.CommodityOHPercent.Value * oldOHSettings.CommodityOverhead, updatedOHSettings.CommodityOverhead, "Failed to update the OHSettings -> CommodityOverhead with increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.CommodityOverhead, updatedOHSettings.CommodityOverhead, "The OHSettings -> CommodityOverhead value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ExternalWorkOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ExternalWorkOHNewValue.Value, updatedOHSettings.ExternalWorkOverhead, "Failed to update the OHSettings -> ExternalWorkOverhead with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ExternalWorkOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.ExternalWorkOHPercent.Value * oldOHSettings.ExternalWorkOverhead, updatedOHSettings.ExternalWorkOverhead, "Failed to update the OHSettings -> ExternalWorkOverhead with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ExternalWorkOverhead, updatedOHSettings.ExternalWorkOverhead, "The OHSettings -> ExternalWorkOverhead value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ManufacturingOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ManufacturingOHNewValue.Value, updatedOHSettings.ManufacturingOverhead, "Failed to update the OHSettings -> ManufacturingOverhead with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ManufacturingOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.ManufacturingOHPercent.Value * oldOHSettings.ManufacturingOverhead, updatedOHSettings.ManufacturingOverhead, "Failed to update the OHSettings -> ManufacturingOverhead with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ManufacturingOverhead, updatedOHSettings.ManufacturingOverhead, "The OHSettings -> ManufacturingOverhead value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.OtherCostOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostOHNewValue.Value, updatedOHSettings.OtherCostOHValue, "Failed to update the OHSettings -> OtherCostOHValue with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.OtherCostOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.OtherCostOHPercent.Value * oldOHSettings.OtherCostOHValue, updatedOHSettings.OtherCostOHValue, "Failed to update the OHSettings -> OtherCostOHValue with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.OtherCostOHValue, updatedOHSettings.OtherCostOHValue, "The OHSettings -> OtherCostOHValue value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.PackagingOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingOHNewValue.Value, updatedOHSettings.PackagingOHValue, "Failed to update the OHSettings -> PackagingOHValue with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.PackagingOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.PackagingOHPercent.Value * oldOHSettings.PackagingOHValue, updatedOHSettings.PackagingOHValue, "Failed to update the OHSettings -> PackagingOHValue with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.PackagingOHValue, updatedOHSettings.PackagingOHValue, "The OHSettings -> PackagingOHValue value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.LogisticOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.LogisticOHNewValue.Value, updatedOHSettings.LogisticOHValue, "Failed to update the OHSettings -> PackagingOHValue with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.LogisticOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.LogisticOHPercent.Value * oldOHSettings.LogisticOHValue, updatedOHSettings.LogisticOHValue, "Failed to update the OHSettings -> LogisticOHValue with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.LogisticOHValue, updatedOHSettings.LogisticOHValue, "The OHSettings -> LogisticOHValue value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.SalesAndAdminOHNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.SalesAndAdminOHNewValue.Value, updatedOHSettings.SalesAndAdministrationOHValue, "Failed to update the OHSettings -> SalesAndAdministrationOHValue with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.SalesAndAdminOHPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.SalesAndAdminOHPercent.Value * oldOHSettings.SalesAndAdministrationOHValue, updatedOHSettings.SalesAndAdministrationOHValue, "Failed to update the OHSettings -> SalesAndAdministrationOHValue with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.SalesAndAdministrationOHValue, updatedOHSettings.SalesAndAdministrationOHValue, "The OHSettings -> SalesAndAdministrationOHValue value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.MaterialMarginNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.MaterialMarginNewValue.Value, updatedOHSettings.MaterialMargin, "Failed to update the OHSettings -> MaterialMargin with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.MaterialMarginPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.MaterialMarginPercent.Value * oldOHSettings.MaterialMargin, updatedOHSettings.MaterialMargin, "Failed to update the OHSettings -> MaterialMargin with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.MaterialMargin, updatedOHSettings.MaterialMargin, "The OHSettings -> MaterialMargin value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ConsumableMarginNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ConsumableMarginNewValue.Value, updatedOHSettings.ConsumableMargin, "Failed to update the OHSettings -> ConsumableMargin with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ConsumableMarginPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.ConsumableMarginPercent.Value * oldOHSettings.ConsumableMargin, updatedOHSettings.ConsumableMargin, "Failed to update the OHSettings -> ConsumableMargin with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ConsumableMargin, updatedOHSettings.ConsumableMargin, "The OHSettings -> ConsumableMargin value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.CommodityMarginNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.CommodityMarginNewValue.Value, updatedOHSettings.CommodityMargin, "Failed to update the OHSettings -> CommodityMargin with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.CommodityMarginPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.CommodityMarginPercent.Value * oldOHSettings.CommodityMargin, updatedOHSettings.CommodityMargin, "Failed to update the OHSettings -> CommodityMargin with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.CommodityMargin, updatedOHSettings.CommodityMargin, "The OHSettings -> CommodityMargin value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ExternalWorkMarginNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ExternalWorkMarginNewValue.Value, updatedOHSettings.ExternalWorkMargin, "Failed to update the OHSettings -> ExternalWorkMargin with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ExternalWorkMarginPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.ExternalWorkMarginPercent.Value * oldOHSettings.ExternalWorkMargin, updatedOHSettings.ExternalWorkMargin, "Failed to update the OHSettings -> ExternalWorkMargin with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ExternalWorkMargin, updatedOHSettings.ExternalWorkMargin, "The OHSettings -> ExternalWorkMargin value should not have been updated(the new value and increased percentage are null).");
            }

            if (viewModel.ManufacturingMarginNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.ManufacturingMarginNewValue.Value, updatedOHSettings.ManufacturingMargin, "Failed to update the OHSettings -> ManufacturingMargin with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ManufacturingMarginPercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.ManufacturingMarginPercent.Value * oldOHSettings.ManufacturingMargin, updatedOHSettings.ManufacturingMargin, "Failed to update the OHSettings -> ManufacturingMargin with the increased percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldOHSettings.ManufacturingMargin, updatedOHSettings.ManufacturingMargin, "The OHSettings -> ManufacturingMargin value should not have been updated(the new value and increased percentage are null).");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verify if the OvearheadSettings belonging to an assembly has been updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The MassUpdateOverheadAndMarginViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateOverheadAndMarginViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            OverheadSettingsUpdated(oldAssembly.OverheadSettings, updatedAssembly.OverheadSettings, viewModel, ref numberOfUpdatedObjects);
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    OverheadSettingsUpdated(oldPart.OverheadSettings, part.OverheadSettings, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    OverheadSettingsAreEqual(oldPart.OverheadSettings, part.OverheadSettings);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    OverheadSettingsAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verify if the OverheadSettings belonging to a project has been updated.
        /// </summary>
        /// <param name="oldProject"> The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model containing the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateOverheadAndMarginViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            OverheadSettingsUpdated(oldProject.OverheadSettings, updatedProject.OverheadSettings, viewModel, ref numberOfUpdatedObjects);
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    OverheadSettingsUpdated(oldPart.OverheadSettings, part.OverheadSettings, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    OverheadSettingsAreEqual(oldPart.OverheadSettings, part.OverheadSettings);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    OverheadSettingsAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verify if the OverheadSettings of two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void OverheadSettingsAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            OverheadSettingsAreEqual(comparedAssembly.OverheadSettings, assemblyToCompareWith.OverheadSettings);

            foreach (Part comparedPart in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == comparedPart.Name);
                OverheadSettingsAreEqual(comparedPart.OverheadSettings, partToCompareWith.OverheadSettings);
            }

            foreach (Assembly subassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                OverheadSettingsAreEqual(subassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verify if two OverheadSettings entities are equal.
        /// </summary>
        /// <param name="comparedOverheadSettings">The compared OverheadSettings.</param>
        /// <param name="overheadSettingsToCompareWith">The OverheadSettings to compare with.</param>
        private void OverheadSettingsAreEqual(OverheadSetting comparedOverheadSettings, OverheadSetting overheadSettingsToCompareWith)
        {
            bool result = comparedOverheadSettings.MaterialOverhead == overheadSettingsToCompareWith.MaterialOverhead &&
                          comparedOverheadSettings.ConsumableOverhead == overheadSettingsToCompareWith.ConsumableOverhead &&
                          comparedOverheadSettings.CommodityOverhead == overheadSettingsToCompareWith.CommodityOverhead &&
                          comparedOverheadSettings.ExternalWorkOverhead == overheadSettingsToCompareWith.ExternalWorkOverhead &&
                          comparedOverheadSettings.ManufacturingOverhead == overheadSettingsToCompareWith.ManufacturingOverhead &&
                          comparedOverheadSettings.OtherCostOHValue == overheadSettingsToCompareWith.OtherCostOHValue &&
                          comparedOverheadSettings.PackagingOHValue == overheadSettingsToCompareWith.PackagingOHValue &&
                          comparedOverheadSettings.LogisticOHValue == overheadSettingsToCompareWith.LogisticOHValue &&
                          comparedOverheadSettings.SalesAndAdministrationOHValue == overheadSettingsToCompareWith.SalesAndAdministrationOHValue &&
                          comparedOverheadSettings.MaterialMargin == overheadSettingsToCompareWith.MaterialMargin &&
                          comparedOverheadSettings.ConsumableMargin == overheadSettingsToCompareWith.ConsumableMargin &&
                          comparedOverheadSettings.CommodityMargin == overheadSettingsToCompareWith.CommodityMargin &&
                          comparedOverheadSettings.ExternalWorkMargin == overheadSettingsToCompareWith.ExternalWorkMargin &&
                          comparedOverheadSettings.ManufacturingMargin == overheadSettingsToCompareWith.ManufacturingMargin;

            Assert.IsTrue(result, "The OverheadSettings entities are not equal.");
        }

        #endregion Helpers
    }
}
