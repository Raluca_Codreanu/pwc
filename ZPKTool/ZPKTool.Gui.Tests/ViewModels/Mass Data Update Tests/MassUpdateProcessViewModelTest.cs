﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels.Mass_Data_Update_Tests
{
    /// <summary>
    /// The mass update process view model test.
    /// </summary>
    [TestClass]
    public class MassUpdateProcessViewModelTest
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateProcessViewModelTest"/> class.
        /// </summary>
        public MassUpdateProcessViewModelTest()
        {
            Utils.ConfigureDbAccess();
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        #endregion Constructor

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand -> Update process step with success.
        /// </summary>
        [TestMethod]
        public void SuccesfullyUpdateProcessStepTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = step;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "Yes" in order to update process step data.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, newValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Cancel process step update.
        /// </summary>
        [TestMethod]
        public void CancelProcessStepUpdateTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            // Set up the view model 
            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = step;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Update process with success.
        /// </summary>
        [TestMethod]
        public void SuccesfullyUpdateProcessTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = part.Process;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "Yes" in order to update the process step data.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, newValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Cancelled process update.
        /// </summary>
        [TestMethod]
        public void CancelledUpdateProcessTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = part.Process;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Update assembly with success.
        /// </summary>
        [TestMethod]
        public void SuccesfullyUpdateAssemblyTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewAssemblyProcessStep(oldValue);
            var assembly = CreateNewAssembly(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.AssemblyRepository.Add(assembly);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = assembly;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "Yes" in order to update the process step data.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, newValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Cancelled assembly update.
        /// </summary>
        [TestMethod]
        public void CancelledUpdateAssemblyTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewAssemblyProcessStep(oldValue);
            var assembly = CreateNewAssembly(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.AssemblyRepository.Add(assembly);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = assembly;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Update part with success.
        /// </summary>
        [TestMethod]
        public void SuccesfullyUpdatePartTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 3;
            var setUpConfig = 2;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = part;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "Yes" in order to update the process step data.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue * newValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Cancelled part update.
        /// </summary>
        [TestMethod]
        public void CancelledUpdatePartTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var part = CreateNewPart(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.PartRepository.Add(part);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = part;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = false;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Update project with success.
        /// </summary>
        [TestMethod]
        public void SuccesfullyUpdateProjectTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewPartProcessStep(oldValue);
            var project = CreateNewProject(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.ProjectRepository.Add(project);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = project;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "Yes" in order to update the process step data.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = true;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            while (massUpdateProcessViewModel.IsProjectUpdating)
            {
                Thread.Sleep(100);
            }

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, newValue, setUpConfig);
        }

        /// <summary>
        /// A test for UpdateCommand -> Cancelled project update.
        /// </summary>
        [TestMethod]
        public void CancelledUpdateProjectTest()
        {
            var messenger = new Messenger();
            var messageBoxService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            var windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var massUpdateProcessViewModel = new MassUpdateProcessViewModel(messenger, windowService, new UnitsServiceMock());

            var oldValue = 1;
            var newValue = 2;
            var setUpConfig = 1;

            var step = CreateNewAssemblyProcessStep(oldValue);
            var project = CreateNewProject(step);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.ProjectRepository.Add(project);
            dataManager.ProcessStepRepository.Add(step);
            dataManager.SaveChanges();

            SetMassUpdateProcessVmProperties(massUpdateProcessViewModel, newValue, setUpConfig);
            massUpdateProcessViewModel.Model = project;
            massUpdateProcessViewModel.DataSourceManager = dataManager;

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateProcessViewModel.ApplyUpdatesToSubObjects = true;
            var updateCommand = massUpdateProcessViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied to the current part
            var dataManager2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var dbStep = dataManager2.ProcessStepRepository.GetByKey(step.Guid);

            ValidationChecks(dbStep, oldValue, setUpConfig);
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Set MassDataUpdate process view-model properties.
        /// </summary>
        /// <param name="massUpdateProcessViewModel">The MassUPdate process view-model.</param>
        /// <param name="value">THe value with which to update the process view-model properties.</param>
        /// <param name="configuration">The set-up configuration.</param>
        private static void SetMassUpdateProcessVmProperties(MassUpdateProcessViewModel massUpdateProcessViewModel, int value, int configuration)
        {
            switch (configuration)
            {
                case 1:
                    {
                        massUpdateProcessViewModel.ProcessTimeNewValue = value;
                        massUpdateProcessViewModel.CycleTimeNewValue = value;
                        massUpdateProcessViewModel.PartsPerCycleNewValue = value;
                        massUpdateProcessViewModel.ScrapAmountNewValue = value;
                        massUpdateProcessViewModel.SetupsPerBatchNewValue = value;
                        massUpdateProcessViewModel.MaxDowntimeNewValue = value;
                        massUpdateProcessViewModel.SetupTimeNewValue = value;
                        massUpdateProcessViewModel.BatchSizeNewValue = value;
                        massUpdateProcessViewModel.ManufacturingOverheadNewValue = value;
                        break;
                    }

                case 2:
                    {
                        massUpdateProcessViewModel.ProcessTimeIncreasePercentage = value;
                        massUpdateProcessViewModel.CycleTimeIncreasePercentage = value;
                        massUpdateProcessViewModel.ScrapAmountIncreasePercentage = value;
                        massUpdateProcessViewModel.MaxDowntimeIncreasePercentage = value;
                        massUpdateProcessViewModel.SetupTimeIncreasePercentage = value;
                        massUpdateProcessViewModel.ManufacturingOverheadIncreasePercentage = value;
                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Validation checks for the process passed as parameter, based on the set-up configuration used at the creation of the step.
        /// </summary>
        /// <param name="step">THe process step.</param>
        /// <param name="value">The properties required value.</param>
        /// <param name="configuration">The set-up configuration.</param>
        private static void ValidationChecks(ProcessStep step, int value, int configuration)
        {
            switch (configuration)
            {
                case 1:
                    {
                        Assert.AreEqual(step.ProcessTime, value);
                        Assert.AreEqual(step.CycleTime, value);
                        Assert.AreEqual(step.PartsPerCycle, value);
                        Assert.AreEqual(step.ScrapAmount, value);
                        Assert.AreEqual(step.SetupsPerBatch, value);
                        Assert.AreEqual(step.MaxDownTime, value);
                        Assert.AreEqual(step.SetupTime, value);
                        Assert.AreEqual(step.BatchSize, value);
                        Assert.AreEqual(step.ManufacturingOverhead, value);
                        break;
                    }

                case 2:
                    {
                        Assert.AreEqual(step.ProcessTime, value);
                        Assert.AreEqual(step.CycleTime, value);
                        Assert.AreEqual(step.ScrapAmount, value);
                        Assert.AreEqual(step.MaxDownTime, value);
                        Assert.AreEqual(step.SetupTime, value);
                        Assert.AreEqual(step.ManufacturingOverhead, value);
                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Create a new Project that contains the process step passed as parameter.
        /// </summary>
        /// <param name="step">The process step.</param>
        /// <returns>The created project.</returns>
        private static Project CreateNewProject(ProcessStep step)
        {
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            var assyProcessStep = step as AssemblyProcessStep;
            if (assyProcessStep != null)
            {
                var assembly = new Assembly();
                assembly.Name = assembly.Guid.ToString();
                assembly.OverheadSettings = new OverheadSetting();
                assembly.CountrySettings = new CountrySetting();
                assembly.Process = new Process();

                project.Assemblies.Add(assembly);
                assembly.Process.Steps.Add(assyProcessStep);
            }

            var partProcessStep = step as PartProcessStep;
            if (partProcessStep != null)
            {
                var part = new Part();
                part.Name = part.Guid.ToString();
                part.OverheadSettings = new OverheadSetting();
                part.CountrySettings = new CountrySetting();
                part.Process = new Process();

                project.Parts.Add(part);
                part.Process.Steps.Add(partProcessStep);
            }

            return project;
        }

        /// <summary>
        /// Create a new Part that contains the process step passed as parameter.
        /// </summary>
        /// <param name="step">The process step.</param>
        /// <returns>The created part.</returns>
        private static Part CreateNewPart(PartProcessStep step)
        {
            var part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();
            part.Process.Steps.Add(step);
            return part;
        }

        /// <summary>
        /// Create a new Assembly that contains the process step passed as parameter.
        /// </summary>
        /// <param name="step">The process step.</param>
        /// <returns>The created assembly.</returns>
        private static Assembly CreateNewAssembly(AssemblyProcessStep step)
        {
            var assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);
            return assembly;
        }

        /// <summary>
        /// Create a new Part Process Step.
        /// </summary>
        /// <param name="value">The value with which to update the step properties.</param>
        /// <returns>The created process step</returns>
        private static PartProcessStep CreateNewPartProcessStep(int value)
        {
            var step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.ProcessTime = value;
            step.CycleTime = value;
            step.PartsPerCycle = value;
            step.ScrapAmount = value;
            step.SetupsPerBatch = value;
            step.MaxDownTime = value;
            step.SetupTime = value;
            step.BatchSize = value;
            step.ManufacturingOverhead = value;
            return step;
        }

        /// <summary>
        /// Create a new Assembly Process Step.
        /// </summary>
        /// <param name="value">The value with which to update the step properties.</param>
        /// <returns>The created process step</returns>
        private static AssemblyProcessStep CreateNewAssemblyProcessStep(int value)
        {
            var step = new AssemblyProcessStep();
            step.Name = step.Guid.ToString();
            step.ProcessTime = value;
            step.CycleTime = value;
            step.PartsPerCycle = value;
            step.ScrapAmount = value;
            step.SetupsPerBatch = value;
            step.MaxDownTime = value;
            step.SetupTime = value;
            step.BatchSize = value;
            step.ManufacturingOverhead = value;
            return step;
        }

        #endregion Helpers
    }
}
