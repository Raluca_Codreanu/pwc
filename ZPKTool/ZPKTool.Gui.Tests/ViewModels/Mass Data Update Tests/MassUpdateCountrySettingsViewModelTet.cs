﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  MassUpdateCountrySettingsViewModel and is intended 
    /// to contain unit tests for all public methods from  MassUpdateCountrySettingsViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateCountrySettingsViewModelTet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateCountrySettingsViewModelTet"/> class.
        /// </summary>
        public MassUpdateCountrySettingsViewModelTet()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize(){}
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// A test for UpdateCommand -> Cancelling the update. 
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(11, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Create a clone of the part in order to verify that the part was not modified
            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext;
            massUpdateCountrySettingsViewModel.Model = part;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the updates were not applied
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            CountrySettingsAreEqual(dbPart, partClone);
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project and ApplyUpdatesToSubObjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateCountrySettingsTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

        //    // Set up the project for test
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly1 = new Assembly();
        //    assembly1.Name = assembly1.Guid.ToString();
        //    assembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(15, true);
        //    assembly1.AssemblingState = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    assembly1.OverheadSettings = new OverheadSetting();
        //    assembly1.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    part1.ManufacturingState = EncryptionManager.Instance.GenerateRandomString(25, true);
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    project.Parts.Add(part1);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateCountrySettingsViewModel.CurrentObject = project;
        //    massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
        //    massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

        //    // Sets the new country settings to random values
        //    Random random = new Random();
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
        //    massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = true;
        //    ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were applied to project's sub objects
        //    int expectedNumberOfUpdatedObjects = 0;
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    ProjectUpdated(projectClone, dbProject, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Project and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the project for test
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly1.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(20, true);
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(20, true);
            part1.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(25, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            project.Parts.Add(part1);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Clone the project in order to check that the sub objects of project were not updated 
            CloneManager cloneManager = new CloneManager(dataContext);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext;
            massUpdateCountrySettingsViewModel.Model = project;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no sub object of the project were not updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly and ApplyUpdatesToSubObjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly1.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly2.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            project.Assemblies.Add(assembly);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext1;
            massUpdateCountrySettingsViewModel.Model = assembly;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = true;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly and ApplyUpdatesToSubObjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly1.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.AssemblingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly2.AssemblingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            project.Assemblies.Add(assembly);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Clone the parent assembly in order to check that its sub objects were not modified.
            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext;
            massUpdateCountrySettingsViewModel.Model = assembly;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated and the updates were saved in db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and ApplyUpdatesToSubObjects = true. 
        /// In this case the ApplyUpdatesToSubObjects flag is not important 
        /// because the part doesn't have sub objects which have CountrySettings property.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object 
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            project.Parts.Add(part);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext1;
            massUpdateCountrySettingsViewModel.Model = part;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the current object was updated and the updates were saved in db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateCountrySettingsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object: Commodity.
        /// </summary>
        [TestMethod]
        public void UpdateCountrySettingsTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            // Set up the current object
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.ManufacturingSupplier = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            var process = new Process();
            part.Process = process;

            var step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            process.Steps.Add(step);

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            step.Commodities.Add(commodity);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            project.Parts.Add(part);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Set up the view model
            massUpdateCountrySettingsViewModel.DataSourceManager = dataContext;
            massUpdateCountrySettingsViewModel.Model = commodity;
            massUpdateCountrySettingsViewModel.CountryNewValue = EncryptionManager.Instance.GenerateRandomString(20, true);
            massUpdateCountrySettingsViewModel.SupplierNewValue = EncryptionManager.Instance.GenerateRandomString(23, true);
            massUpdateCountrySettingsViewModel.NewCountrySettings = new CountrySetting();

            // Sets the new country settings to random values
            Random random = new Random();
            massUpdateCountrySettingsViewModel.NewCountrySettings.AirCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EnergyCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.EngineerCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ForemanCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.InterestRate = random.Next(100);
            massUpdateCountrySettingsViewModel.NewCountrySettings.LaborAvailability = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.OfficeAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ProductionAreaRentalCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge1ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge2ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.ShiftCharge3ShiftModel = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.SkilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.TechnicianCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.UnskilledLaborCost = random.Next(1000);
            massUpdateCountrySettingsViewModel.NewCountrySettings.WaterCost = random.Next(1000);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            massUpdateCountrySettingsViewModel.ApplyUpdatesToSubObjects = false;
            ICommand updateCommand = massUpdateCountrySettingsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for BrowseCountryCommand.
        /// </summary>
        [TestMethod]
        public void BrowseCountryTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            windowService.IsOpen = false;
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser = new CountryAndSupplierBrowserViewModel(windowService);
            ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseCountryCommand;
            browseCountryCommand.Execute(null);

            // Verify that CountryAndSupplierBrowser window was open 
            Assert.IsTrue(windowService.IsOpen, "Failed to open the CountryAndSupplierBrowser window.");

            // Load the Countries 
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int countriesCount = dataContext.CountryRepository.GetAll().Count;
            while (massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems == null || massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.Count < countriesCount)
            {
                Thread.Sleep(2000);
            }

            // Select a country in CountryAndSupplierBrowser
            TreeViewDataItem treeViewDataItem = massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.FirstOrDefault();
            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, treeViewDataItem);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectedTreeItemChangedCommand.Execute(args);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectItem.Execute(null);

            // Verify that the selected country is displayed in MassUpdateCountrySettingsView
            Country expectedCountry = treeViewDataItem.DataObject as Country;
            Assert.AreEqual(expectedCountry.Name, massUpdateCountrySettingsViewModel.CountryNewValue, "Failed to display the selected country's name.");
            CountrySettingsAreEqual(expectedCountry.CountrySetting, massUpdateCountrySettingsViewModel.NewCountrySettings);
        }

        /// <summary>
        /// A test for BrowseCountryCommand. 
        /// The following input data were used: CountryNewValue = null and CountryCurrentValue = null.
        /// Expected results: an info message should be displayed informing the user that a country should be selected first.
        /// </summary>
        [TestMethod]
        public void BrowseSupplierTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            windowService.IsOpen = false;
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser = new CountryAndSupplierBrowserViewModel(windowService);
            ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseSupplierCommand;
            browseCountryCommand.Execute(null);

            Assert.IsFalse(windowService.IsOpen, "CountryAndSupplierBrowser should not be opened until a country was selected.");
            Assert.AreEqual(LocalizedResources.General_SelectCountryFirst, messageBoxService.Message, "An info message should be displayed if no country was selected.");
            Assert.IsNull(massUpdateCountrySettingsViewModel.SupplierNewValue, "SupplierNewValue should be null if CountryNewValue and CountryCurrentValue are null.");
        }

        /// <summary>
        /// A test for BrowseCountryCommand. 
        /// The following input data were used: CountryNewValue = null and CountryCurrentValue != null.
        /// Expected results: CountryAndSupplierBrowser should be open and the suppliers of the current Country should be displayed.
        /// </summary>
        [TestMethod]
        public void BrowseSupplierTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country currentCountry = dataContext.CountryRepository.GetAll().FirstOrDefault();
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser = new CountryAndSupplierBrowserViewModel(windowService);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.CountryName = currentCountry.Name;
            massUpdateCountrySettingsViewModel.CountryCurrentValue = currentCountry.Name;

            // Add some states to the current country
            CountryState state1 = new CountryState();
            state1.Name = state1.Guid.ToString();
            state1.CountrySettings = new CountrySetting();
            currentCountry.States.Add(state1);

            CountryState state2 = new CountryState();
            state2.Name = state2.Guid.ToString();
            state2.CountrySettings = new CountrySetting();
            currentCountry.States.Add(state2);

            CountryState state3 = new CountryState();
            state3.Name = state3.Guid.ToString();
            state3.CountrySettings = new CountrySetting();
            currentCountry.States.Add(state3);
            dataContext.SaveChanges();

            windowService.IsOpen = false;
            ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseSupplierCommand;
            browseCountryCommand.Execute(null);

            // Verify that CountryAndSupplierBrowser window was open 
            Assert.IsTrue(windowService.IsOpen, "Failed to open the CountryAndSupplierBrowser window.");

            // Load the Countries 
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            int suppliersCount = currentCountry.States.Count;
            while (massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems == null || massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.Count < suppliersCount)
            {
                Thread.Sleep(2000);
            }

            // Select a supplier in CountryAndSupplierBrowser
            TreeViewDataItem treeViewDataItem = massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.FirstOrDefault();
            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, treeViewDataItem);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectedTreeItemChangedCommand.Execute(args);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectItem.Execute(null);

            // Verify that the selected supplier and its country are displayed in MassUpdateCountrySettingsView
            CountryState expectedSupplier = treeViewDataItem.DataObject as CountryState;
            Assert.AreEqual(expectedSupplier.Name, massUpdateCountrySettingsViewModel.SupplierNewValue, "Failed to display the selected supplier's name.");
            Assert.AreEqual(expectedSupplier.Country.Name, massUpdateCountrySettingsViewModel.CountryNewValue, "Failed to display the country of the selected supplier.");
            CountrySettingsAreEqual(expectedSupplier.CountrySettings, massUpdateCountrySettingsViewModel.NewCountrySettings);
        }

        /// <summary>
        /// A test for BrowseCountryCommand. 
        /// The following input data were used: CountryNewValue != null and CountryCurrentValue = null.
        /// Expected results: CountryAndSupplierBrowser should be open and the suppliers of the new Country should be displayed.
        /// </summary>
        [TestMethod]
        public void BrowseSupplierTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateCountrySettingsViewModel massUpdateCountrySettingsViewModel = new MassUpdateCountrySettingsViewModel(messenger, windowService);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country newCountry = dataContext.CountryRepository.GetAll().FirstOrDefault();
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser = new CountryAndSupplierBrowserViewModel(windowService);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.CountryName = newCountry.Name;
            massUpdateCountrySettingsViewModel.CountryNewValue = newCountry.Name;

            // Add some states to the current country
            CountryState state1 = new CountryState();
            state1.Name = state1.Guid.ToString();
            state1.CountrySettings = new CountrySetting();
            newCountry.States.Add(state1);

            CountryState state2 = new CountryState();
            state2.Name = state2.Guid.ToString();
            state2.CountrySettings = new CountrySetting();
            newCountry.States.Add(state2);

            CountryState state3 = new CountryState();
            state3.Name = state3.Guid.ToString();
            state3.CountrySettings = new CountrySetting();
            newCountry.States.Add(state3);
            dataContext.SaveChanges();

            windowService.IsOpen = false;
            ICommand browseCountryCommand = massUpdateCountrySettingsViewModel.BrowseSupplierCommand;
            browseCountryCommand.Execute(null);

            // Verify that CountryAndSupplierBrowser window was open 
            Assert.IsTrue(windowService.IsOpen, "Failed to open the CountryAndSupplierBrowser window.");

            // Load the Countries 
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.OnLoaded();

            // Wait until all MD countries/suppliers were loaded
            int suppliersCount = newCountry.States.Count;
            while (massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems == null || massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.Count < suppliersCount)
            {
                Thread.Sleep(2000);
            }

            // Select a supplier in CountryAndSupplierBrowser
            TreeViewDataItem treeViewDataItem = massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.TreeItems.FirstOrDefault();
            System.Windows.RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(null, treeViewDataItem);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectedTreeItemChangedCommand.Execute(args);
            massUpdateCountrySettingsViewModel.CountryAndSupplierBrowser.SelectItem.Execute(null);

            // Verify that the selected supplier and its country are displayed in MassUpdateCountrySettingsView
            CountryState expectedSupplier = treeViewDataItem.DataObject as CountryState;
            Assert.AreEqual(expectedSupplier.Name, massUpdateCountrySettingsViewModel.SupplierNewValue, "Failed to display the selected supplier's name.");
            Assert.AreEqual(expectedSupplier.Country.Name, massUpdateCountrySettingsViewModel.CountryNewValue, "Failed to display the country of the selected supplier.");
            CountrySettingsAreEqual(expectedSupplier.CountrySettings, massUpdateCountrySettingsViewModel.NewCountrySettings);
        }

        #endregion Test Method

        #region Helpers

        /// <summary>
        /// Verify if the project was updated.
        /// </summary>
        /// <param name="oldProject"> The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    CountrySettingsAreEqual(oldPart, part);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    CountrySettingsAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the assembly was updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;

            if (viewModel.CountryNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.CountryNewValue, updatedAssembly.AssemblingCountry, "Failed to update the AssemblingCountry.");
                if (viewModel.SupplierNewValue != null)
                {
                    Assert.AreEqual<string>(viewModel.SupplierNewValue, updatedAssembly.AssemblingSupplier, "Failed to update the AssemblingSupplier.");
                }

                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldAssembly.AssemblingCountry, updatedAssembly.AssemblingCountry, "The AssemblingCountry should not have been updated if the new value was null.");
                Assert.AreEqual<string>(oldAssembly.AssemblingSupplier, updatedAssembly.AssemblingSupplier, "The AssemblingSupplier should not have been updated if the new value was null.");
            }

            if (viewModel.NewCountrySettings != null)
            {
                Assert.IsTrue(CountrySettingsAreEqual(updatedAssembly.CountrySettings, viewModel.NewCountrySettings), "Failed to update the CountrySettings with the new values.");
            }
            else
            {
                Assert.IsTrue(CountrySettingsAreEqual(oldAssembly.CountrySettings, updatedAssembly.CountrySettings), "The CountrySettings should not have been updated if the new values are null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    CountrySettingsAreEqual(oldPart, part);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    CountrySettingsAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the part was updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateCountrySettingsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;

            if (viewModel.CountryNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.CountryNewValue, updatedPart.ManufacturingCountry, "Failed to update the ManufacturingCountry.");
                if (viewModel.SupplierNewValue != null)
                {
                    Assert.AreEqual<string>(viewModel.SupplierNewValue, updatedPart.ManufacturingSupplier, "Failed to update the ManufacturingState.");
                }

                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldPart.ManufacturingCountry, updatedPart.ManufacturingCountry, "The ManufacturingCountry should not have been updated if the new value was null.");
                Assert.AreEqual<string>(oldPart.ManufacturingSupplier, updatedPart.ManufacturingSupplier, "The ManufacturingCountry should not have been updated if the new value was null.");
            }

            if (viewModel.NewCountrySettings != null)
            {
                Assert.IsTrue(CountrySettingsAreEqual(updatedPart.CountrySettings, viewModel.NewCountrySettings), "Failed to update the country settings with the new values.");
            }
            else
            {
                Assert.IsTrue(CountrySettingsAreEqual(oldPart.CountrySettings, updatedPart.CountrySettings), "The CountrySettings should not have been updated if the new values are null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verify if the country settings(Country, State and CountrySettings) of two entities Parts are equal.
        /// </summary>
        /// <param name="comparedPart">The compared entity.</param>
        /// <param name="partToCompareWith">The entity to compare with.</param>
        private void CountrySettingsAreEqual(Part comparedPart, Part partToCompareWith)
        {
            bool result = false;
            result = comparedPart.ManufacturingCountry == partToCompareWith.ManufacturingCountry &&
                     comparedPart.ManufacturingSupplier == partToCompareWith.ManufacturingSupplier &&
                     CountrySettingsAreEqual(comparedPart.CountrySettings, partToCompareWith.CountrySettings);

            Assert.IsTrue(result, "The Country Settings of parts are not equal.");
        }

        /// <summary>
        /// Verify if the country settings(Country, State and CountrySettings) of two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void CountrySettingsAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            bool result = false;
            result = comparedAssembly.AssemblingCountry == assemblyToCompareWith.AssemblingCountry &&
                     comparedAssembly.AssemblingSupplier == assemblyToCompareWith.AssemblingSupplier &&
                     CountrySettingsAreEqual(comparedAssembly.CountrySettings, assemblyToCompareWith.CountrySettings);

            Assert.IsTrue(result, "The Country Settings of assemblies are not equal.");
            foreach (Part comparePart in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == comparePart.Name);
                CountrySettingsAreEqual(comparePart, partToCompareWith);
            }

            foreach (Assembly comparedSubassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == comparedSubassembly.Name);
                CountrySettingsAreEqual(comparedSubassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the values of two country settings are equal.
        /// </summary>
        /// <param name="comparedCountrySetting">The compared CountrySetting.</param>
        /// <param name="countrySettingToCompareWith">The CountrySetting to compare with.</param>
        /// <returns>True if equal , false otherwise</returns>
        private bool CountrySettingsAreEqual(CountrySetting comparedCountrySetting, CountrySetting countrySettingToCompareWith)
        {
            bool result = comparedCountrySetting.AirCost == countrySettingToCompareWith.AirCost &&
                         comparedCountrySetting.EnergyCost == countrySettingToCompareWith.EnergyCost &&
                         comparedCountrySetting.EngineerCost == countrySettingToCompareWith.EngineerCost &&
                         comparedCountrySetting.ForemanCost == countrySettingToCompareWith.ForemanCost &&
                         comparedCountrySetting.InterestRate == countrySettingToCompareWith.InterestRate &&
                         comparedCountrySetting.LaborAvailability == countrySettingToCompareWith.LaborAvailability &&
                         comparedCountrySetting.OfficeAreaRentalCost == countrySettingToCompareWith.OfficeAreaRentalCost &&
                         comparedCountrySetting.ProductionAreaRentalCost == countrySettingToCompareWith.ProductionAreaRentalCost &&
                         comparedCountrySetting.ShiftCharge1ShiftModel == countrySettingToCompareWith.ShiftCharge1ShiftModel &&
                         comparedCountrySetting.ShiftCharge2ShiftModel == countrySettingToCompareWith.ShiftCharge2ShiftModel &&
                         comparedCountrySetting.ShiftCharge3ShiftModel == countrySettingToCompareWith.ShiftCharge3ShiftModel &&
                         comparedCountrySetting.SkilledLaborCost == countrySettingToCompareWith.SkilledLaborCost &&
                         comparedCountrySetting.TechnicianCost == countrySettingToCompareWith.TechnicianCost &&
                         comparedCountrySetting.UnskilledLaborCost == countrySettingToCompareWith.UnskilledLaborCost &&
                         comparedCountrySetting.WaterCost == countrySettingToCompareWith.WaterCost;

            return result;
        }

        #endregion Helpers
    }
}
