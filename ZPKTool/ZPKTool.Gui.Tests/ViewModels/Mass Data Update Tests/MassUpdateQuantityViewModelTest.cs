﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  MassUpdateQuantityViewModel and is intended 
    /// to contain unit tests for all public methods from  MassUpdateQuantityViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateQuantityViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateQuantityViewModelTest"/> class.
        /// </summary>
        public MassUpdateQuantityViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand - cancelling the update.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data.
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 20;
            part.LifeTime = 15;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.BatchSize = 9;
            part.Process = new Process();
            part.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = part;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "No" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            QuantitiesAreEqual(partClone, dbPart);
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> ProcessStep and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.BatchSize = 100;
            process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = step;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetByKey(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> ProcessStep and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.BatchSize = 100;
            process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = step;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYIncreasePercentage = 2;
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetByKey(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> ProcessStep and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.BatchSize = 100;
            process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = step;

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep dbStep = dataContext2.ProcessStepRepository.GetByKey(step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Process and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = process;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;

            // Verify that the quantities of process steps were updated with the new values.
            foreach (ProcessStep step in dbPart.Process.Steps)
            {
                ProcessStep oldStep = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Process and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = process;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYIncreasePercentage = 2;
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;

            // Verify that the quantities of process steps were updated with the new values.
            foreach (ProcessStep step in dbPart.Process.Steps)
            {
                ProcessStep oldStep = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Process and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = process;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;

            // Verify that the quantities of process steps were not updated with null values.
            foreach (ProcessStep step in dbPart.Process.Steps)
            {
                ProcessStep oldStep = partClone.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part and the new values are set valid values.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 100;
            part.LifeTime = 20;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = part;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest9()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 100;
            part.LifeTime = 20;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = part;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYIncreasePercentage = 1;
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest10()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 100;
            part.LifeTime = 20;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 100;
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 200;
            process.Steps.Add(step2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = part;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Assembly, the new values are set valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest11()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 90;
            assembly.LifeTime = 100;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 10;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.BatchSizePerYear = 10;
            part1.YearlyProductionQuantity = 100;
            part1.LifeTime = 20;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 100;
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.BatchSizePerYear = 10;
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.BatchSize = 90;
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step3);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.BatchSizePerYear = 80;
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.Process = new Process();
            subassembly1.Subassemblies.Add(subassembly2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = assembly;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Assembly, the new values are expressed in percentages and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest12()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 90;
            assembly.LifeTime = 100;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 10;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.BatchSizePerYear = 10;
            part1.YearlyProductionQuantity = 100;
            part1.LifeTime = 20;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 100;
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.BatchSizePerYear = 10;
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.BatchSize = 90;
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step3);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.BatchSizePerYear = 80;
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.Process = new Process();
            subassembly1.Subassemblies.Add(subassembly2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = assembly;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYIncreasePercentage = 2;
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Assembly, the new values are set to null and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest13()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 90;
            assembly.LifeTime = 100;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 10;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.BatchSizePerYear = 10;
            part1.YearlyProductionQuantity = 100;
            part1.LifeTime = 20;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 100;
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.BatchSizePerYear = 10;
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.BatchSize = 90;
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step3);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.BatchSizePerYear = 80;
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.Process = new Process();
            subassembly1.Subassemblies.Add(subassembly2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = assembly;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Project, the new values are set to valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest14()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 90;
            assembly.LifeTime = 100;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.BatchSize = 10;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.BatchSizePerYear = 10;
            part1.YearlyProductionQuantity = 100;
            part1.LifeTime = 20;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.BatchSize = 100;
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.BatchSizePerYear = 10;
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.Process = new Process();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.BatchSize = 90;
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step3);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.BatchSizePerYear = 100;
            part2.YearlyProductionQuantity = 90;
            part2.LifeTime = 100;
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            step1.BatchSize = 19;
            part2.Process = new Process();
            part2.Process.Steps.Add(step4);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = project;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 1;
            ProjectUpdated(projectClone, dbProject, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Project, the new values are set to valid values and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateQuantityTest15() 
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService);

        //    // Set up the test data
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.BatchSizePerYear = 120;
        //    assembly.YearlyProductionQuantity = 90;
        //    assembly.LifeTime = 100;
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    ProcessStep step1 = new AssemblyProcessStep();
        //    step1.Name = step1.Guid.ToString();
        //    step1.BatchSize = 10;
        //    assembly.Process = new Process();
        //    assembly.Process.Steps.Add(step1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.BatchSizePerYear = 10;
        //    part1.YearlyProductionQuantity = 100;
        //    part1.LifeTime = 20;
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    assembly.Parts.Add(part1);

        //    ProcessStep step2 = new PartProcessStep();
        //    step2.Name = step2.Guid.ToString();
        //    step2.BatchSize = 100;
        //    part1.Process = new Process();
        //    part1.Process.Steps.Add(step2);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.BatchSizePerYear = 10;
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.Process = new Process();
        //    assembly.Subassemblies.Add(subassembly1);

        //    ProcessStep step3 = new AssemblyProcessStep();
        //    step3.Name = step3.Guid.ToString();
        //    step3.BatchSize = 90;
        //    subassembly1.Process = new Process();
        //    subassembly1.Process.Steps.Add(step3);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.BatchSizePerYear = 100;
        //    part2.YearlyProductionQuantity = 90;
        //    part2.LifeTime = 100;
        //    part2.OverheadSettings = new OverheadSetting();
        //    part2.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part2);

        //    ProcessStep step4 = new PartProcessStep();
        //    step4.Name = step4.Guid.ToString();
        //    step1.BatchSize = 19;
        //    part2.Process = new Process();
        //    part2.Process.Steps.Add(step4);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateQuantityViewModel.IDataSourceManager = dataContext1;
        //    massUpdateQuantityViewModel.CurrentObject = project;
        //    massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateQuantityViewModel.BatchSizeNewValue = "200";
        //    massUpdateQuantityViewModel.YearlyProdQTYIncreasePercentage = 4;
        //    massUpdateQuantityViewModel.LifeTimeNewValue = "100";

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed after the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Project, the new values are set to null values and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateQuantityTest16() 
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService);

        //    // Set up the test data
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.BatchSizePerYear = 120;
        //    assembly.YearlyProductionQuantity = 90;
        //    assembly.LifeTime = 100;
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    ProcessStep step1 = new AssemblyProcessStep();
        //    step1.Name = step1.Guid.ToString();
        //    step1.BatchSize = 10;
        //    assembly.Process = new Process();
        //    assembly.Process.Steps.Add(step1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.BatchSizePerYear = 10;
        //    part1.YearlyProductionQuantity = 100;
        //    part1.LifeTime = 20;
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    assembly.Parts.Add(part1);

        //    ProcessStep step2 = new PartProcessStep();
        //    step2.Name = step2.Guid.ToString();
        //    step2.BatchSize = 100;
        //    part1.Process = new Process();
        //    part1.Process.Steps.Add(step2);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.BatchSizePerYear = 10;
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.Process = new Process();
        //    assembly.Subassemblies.Add(subassembly1);

        //    ProcessStep step3 = new AssemblyProcessStep();
        //    step3.Name = step3.Guid.ToString();
        //    step3.BatchSize = 90;
        //    subassembly1.Process = new Process();
        //    subassembly1.Process.Steps.Add(step3);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.BatchSizePerYear = 100;
        //    part2.YearlyProductionQuantity = 90;
        //    part2.LifeTime = 100;
        //    part2.OverheadSettings = new OverheadSetting();
        //    part2.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part2);

        //    ProcessStep step4 = new PartProcessStep();
        //    step4.Name = step4.Guid.ToString();
        //    step1.BatchSize = 19;
        //    part2.Process = new Process();
        //    part2.Process.Steps.Add(step4);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateQuantityViewModel.IDataSourceManager = dataContext1;
        //    massUpdateQuantityViewModel.CurrentObject = project;
        //    massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateQuantityViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed after the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Raw Material.
        /// </summary>
        [TestMethod]
        public void UpdateQuantityTest17()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateQuantityViewModel massUpdateQuantityViewModel = new MassUpdateQuantityViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the test data
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateQuantityViewModel.DataSourceManager = dataContext1;
            massUpdateQuantityViewModel.Model = material;
            massUpdateQuantityViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateQuantityViewModel.BatchSizeNewValue = "200";
            massUpdateQuantityViewModel.YearlyProdQTYNewValue = "150";
            massUpdateQuantityViewModel.LifeTimeNewValue = "100";

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateQuantityViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        #endregion Test Methods

        #region Helpers
        /// <summary>
        /// Verify if a project was updated.
        /// </summary>
        /// <param name="oldProject">The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateQuantityViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    QuantitiesAreEqual(part, oldPart);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    QuantitiesAreEqual(assembly, oldAssembly);
                }
            }
        }

        /// <summary>
        /// Verify if an assembly was updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateQuantityViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(viewModel.BatchSizeNewValue))
            {
                Assert.AreEqual<int>(
                    Converter.StringToNullableInt(viewModel.BatchSizeNewValue).GetValueOrDefault(),
                    updatedAssembly.BatchSizePerYear.GetValueOrDefault(),
                    "Failed to update the Batch Size of assembly");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldAssembly.BatchSizePerYear.GetValueOrDefault(), updatedAssembly.BatchSizePerYear.GetValueOrDefault(), "Batch Size should not have been updated if the new value is null.");
            }

            if (!string.IsNullOrEmpty(viewModel.YearlyProdQTYNewValue))
            {
                Assert.AreEqual<int>(
                    Converter.StringToNullableInt(viewModel.YearlyProdQTYNewValue).GetValueOrDefault(),
                    updatedAssembly.YearlyProductionQuantity.GetValueOrDefault(),
                    "Failed to update the YearlyProductionQuantity of an assembly with the new value");
                objectUpdated = true;
            }
            else if (viewModel.YearlyProdQTYIncreasePercentage != null)
            {
                Assert.AreEqual<int>(Convert.ToInt32(viewModel.YearlyProdQTYIncreasePercentage * oldAssembly.YearlyProductionQuantity), updatedAssembly.YearlyProductionQuantity.GetValueOrDefault(), "Failed to update the YearlyProductionQuantity with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldAssembly.YearlyProductionQuantity.GetValueOrDefault(), updatedAssembly.YearlyProductionQuantity.GetValueOrDefault(), "YearlyProductionQuantity should not have been updated if the new value is null.");
            }

            if (!string.IsNullOrEmpty(viewModel.LifeTimeNewValue))
            {
                Assert.AreEqual<int>(Converter.StringToNullableInt(viewModel.LifeTimeNewValue).GetValueOrDefault(), updatedAssembly.LifeTime.GetValueOrDefault(), "Failed to update the LifeTime with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldAssembly.LifeTime.GetValueOrDefault(), updatedAssembly.LifeTime.GetValueOrDefault(), "LifeTime should not have been updated if the new value is null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            foreach (ProcessStep step in updatedAssembly.Process.Steps)
            {
                ProcessStep oldStep = oldAssembly.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, viewModel, ref numberOfUpdatedObjects);
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    QuantitiesAreEqual(part, oldPart);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    QuantitiesAreEqual(subassembly, oldSubassembly);
                }
            }
        }

        /// <summary>
        /// Verify if a part was updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateQuantityViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(viewModel.BatchSizeNewValue))
            {
                Assert.AreEqual<int>(
                    Converter.StringToNullableInt(viewModel.BatchSizeNewValue).GetValueOrDefault(),
                    updatedPart.BatchSizePerYear.GetValueOrDefault(),
                    "Failed to update the Batch Size of part.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldPart.BatchSizePerYear.GetValueOrDefault(), updatedPart.BatchSizePerYear.GetValueOrDefault(), "Batch Size should not have been updated if the new value is null.");
            }

            if (!string.IsNullOrEmpty(viewModel.YearlyProdQTYNewValue))
            {
                Assert.AreEqual<int>(
                    Converter.StringToNullableInt(viewModel.YearlyProdQTYNewValue).GetValueOrDefault(),
                    updatedPart.YearlyProductionQuantity.GetValueOrDefault(),
                    "Failed to update the YearlyProductionQuantity of a part with the new value");
                objectUpdated = true;
            }
            else if (viewModel.YearlyProdQTYIncreasePercentage != null)
            {
                Assert.AreEqual<int>(Convert.ToInt32(viewModel.YearlyProdQTYIncreasePercentage * oldPart.YearlyProductionQuantity), updatedPart.YearlyProductionQuantity.GetValueOrDefault(), "Failed to update the YearlyProductionQuantity with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldPart.YearlyProductionQuantity.GetValueOrDefault(), updatedPart.YearlyProductionQuantity.GetValueOrDefault(), "YearlyProductionQuantity should not have been updated if the new value is null.");
            }

            if (!string.IsNullOrEmpty(viewModel.LifeTimeNewValue))
            {
                Assert.AreEqual<int>(Converter.StringToNullableInt(viewModel.LifeTimeNewValue).GetValueOrDefault(), updatedPart.LifeTime.GetValueOrDefault(), "Failed to update the LifeTime with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldPart.LifeTime.GetValueOrDefault(), updatedPart.LifeTime.GetValueOrDefault(), "LifeTime should not have been updated if the new value is null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            foreach (ProcessStep step in updatedPart.Process.Steps)
            {
                ProcessStep oldStep = oldPart.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, viewModel, ref numberOfUpdatedObjects);
            }
        }

        /// <summary>
        /// Verify if a process step was updated.
        /// </summary>
        /// <param name="oldStep">The old step.</param>
        /// <param name="updatedStep">The updated step.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProcessStepUpdated(ProcessStep oldStep, ProcessStep updatedStep, MassUpdateQuantityViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(viewModel.CalculatedBatchSizeNewValue))
            {
                Assert.AreEqual<int>(Converter.StringToNullableInt(viewModel.CalculatedBatchSizeNewValue).GetValueOrDefault(), updatedStep.BatchSize.GetValueOrDefault(), "Failed to update the BatchSize value of a process step with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.CalculatedBatchSizeIncreasePercentage != null)
            {
                Assert.AreEqual<int>(Convert.ToInt32(viewModel.CalculatedBatchSizeIncreasePercentage * oldStep.BatchSize), updatedStep.BatchSize.GetValueOrDefault(), "Failed to update the BatchSize value of a process step with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<int>(oldStep.BatchSize.GetValueOrDefault(), updatedStep.BatchSize.GetValueOrDefault(), "The BatchSize value of a process step should not have been updated if the new value is null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verifies if the quantities of two assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void QuantitiesAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            bool result = false;
            result = comparedAssembly.BatchSizePerYear.GetValueOrDefault() == assemblyToCompareWith.BatchSizePerYear.GetValueOrDefault() &&
                     comparedAssembly.YearlyProductionQuantity.GetValueOrDefault() == assemblyToCompareWith.YearlyProductionQuantity.GetValueOrDefault() &&
                     comparedAssembly.LifeTime.GetValueOrDefault() == assemblyToCompareWith.LifeTime.GetValueOrDefault();

            Assert.IsTrue(result, "The quantities of assemblies are not equal");
            foreach (ProcessStep step in comparedAssembly.Process.Steps)
            {
                ProcessStep stepToCompareWith = assemblyToCompareWith.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                Assert.IsTrue(step.BatchSize.GetValueOrDefault() == stepToCompareWith.BatchSize.GetValueOrDefault(), "The quantities of process steps are not equal.");
            }

            foreach (Part part in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == part.Name);
                QuantitiesAreEqual(part, partToCompareWith);
            }

            foreach (Assembly subassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                QuantitiesAreEqual(subassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the quantities of two parts are equal.
        /// </summary>
        /// <param name="comparedPart">The compared part.</param>
        /// <param name="partToCompareWith">The part to compare with.</param>
        private void QuantitiesAreEqual(Part comparedPart, Part partToCompareWith)
        {
            bool result = false;
            result = comparedPart.BatchSizePerYear.GetValueOrDefault() == partToCompareWith.BatchSizePerYear.GetValueOrDefault() &&
                     comparedPart.YearlyProductionQuantity.GetValueOrDefault() == partToCompareWith.YearlyProductionQuantity.GetValueOrDefault() &&
                     comparedPart.LifeTime.GetValueOrDefault() == partToCompareWith.LifeTime.GetValueOrDefault();

            Assert.IsTrue(result, "The quantities of parts are not equal.");
            foreach (ProcessStep step in comparedPart.Process.Steps)
            {
                ProcessStep stepToCompareWith = partToCompareWith.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                Assert.IsTrue(step.BatchSize.GetValueOrDefault() == stepToCompareWith.BatchSize.GetValueOrDefault(), "The quantities of process steps are not equal.");
            }
        }

        #endregion Helpers
    }
}
