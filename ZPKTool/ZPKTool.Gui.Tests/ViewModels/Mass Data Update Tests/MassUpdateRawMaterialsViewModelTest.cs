﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  UpdateRawMaterialsViewModel and is intended 
    /// to contain unit tests for all public methods from UpdateRawMaterialsViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateRawMaterialsViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateRawMaterialsViewModelTest"/> class.
        /// </summary>
        public MassUpdateRawMaterialsViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand -> cancelling the update. 
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.Price = random.Next(501, 1000);
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(1000);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterialsList = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the  materials from db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            GetPartMaterials(dbPart, ref dbMaterials);

            // Verify that the materials were not updated
            foreach (RawMaterial oldMaterial in cloneOfMaterialsList)
            {
                RawMaterial updatedMaterial = dbMaterials.FirstOrDefault(m => m.Guid == oldMaterial.Guid);
                MaterialsAreEqual(oldMaterial, updatedMaterial);
            }
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Project and the new values are set to valid values.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object 
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part);

        //    Random random = new Random();
        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    material1.Price = random.Next(0, 500);
        //    material1.YieldStrength = random.Next(1000).ToString();
        //    material1.RuptureStrength = random.Next(1000).ToString();
        //    material1.Density = random.Next(1000).ToString();
        //    material1.MaxElongation = random.Next(1000).ToString();
        //    material1.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material1.Rx = random.Next(1000).ToString();
        //    material1.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    material2.Price = random.Next(501, 1000);
        //    material2.YieldStrength = random.Next(1000).ToString();
        //    material2.RuptureStrength = random.Next(1000).ToString();
        //    material2.Density = random.Next(1000).ToString();
        //    material2.MaxElongation = random.Next(1000).ToString();
        //    material2.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material2.Rx = random.Next(1000).ToString();
        //    material2.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material2);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.IDataSourceManager = dataContext1;
        //    massUpdateRawMaterialsViewModel.CurrentObject = project;
        //    massUpdateRawMaterialsViewModel.IsTabSelected = true;
        //    massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(1000);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials.Add(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault());

        //    bool projectNotified = false;
        //    messenger.Register<EntityChangedMessage>(msg =>
        //    {
        //        if (msg.ChangeType == NotificationType.RefreshData)
        //        {
        //            projectNotified = true;
        //        }
        //    });

        //    // Retrieve the raw materials from RawMaterialsCollection
        //    List<RawMaterial> materialsList = new List<RawMaterial>();
        //    foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
        //    {
        //        foreach (RawMaterial material in materialDataItem.RawMaterials)
        //        {
        //            materialsList.Add(material);
        //        }
        //    }

        //    List<RawMaterial> cloneOfMaterialsList = CloneMaterialsList(materialsList, dataContext1);
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Retrieve the project from db in order to check that the materials were updated
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    List<RawMaterial> dbMaterials = new List<RawMaterial>();
        //    GetProjectMaterials(dbProject, ref dbMaterials);

        //    // Verify that the materials were updated
        //    int expectedNumberOfUpdatedObjects = 0;
        //    MaterialsUpdated(cloneOfMaterialsList, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");

        //    // Verify that a notification has been send 
        //    Assert.IsTrue(projectNotified, "Failed to send a notification that the project has been changed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part, the new values are set to valid values and all raw materials 
        /// from the list are selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.Price = random.Next(501, 800);
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.Price = random.Next(801, 1000);
            material3.YieldStrength = random.Next(1000).ToString();
            material3.RuptureStrength = random.Next(1000).ToString();
            material3.Density = random.Next(1000).ToString();
            material3.MaxElongation = random.Next(1000).ToString();
            material3.GlassTransitionTemperature = random.Next(1000).ToString();
            material3.Rx = random.Next(1000).ToString();
            material3.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

            // Retrieve the raw materials from RawMaterialsCollection
            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterialsList = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the part from db in order to verify that the raw materials were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            GetPartMaterials(dbPart, ref dbMaterials);

            // Verify that the selected raw materials were updated
            int expectedNumberOfUpdatedObjects = 0;
            MaterialsUpdated(cloneOfMaterialsList, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part, the new values are set to valid values and no raw material 
        /// from the list is selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.Price = random.Next(501, 800);
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.Price = random.Next(801, 1000);
            material3.YieldStrength = random.Next(1000).ToString();
            material3.RuptureStrength = random.Next(1000).ToString();
            material3.Density = random.Next(1000).ToString();
            material3.MaxElongation = random.Next(1000).ToString();
            material3.GlassTransitionTemperature = random.Next(1000).ToString();
            material3.Rx = random.Next(1000).ToString();
            material3.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = new List<RawMaterialDataItem>();

            // Retrieve the raw materials from RawMaterialsCollection
            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterialsList = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the part's raw materials from db in order to check that the materials were not updated 
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            GetPartMaterials(dbPart, ref dbMaterials);

            // Verify that the updates were not applied
            int expectedNumberOfUpdatedObjects = 0;
            MaterialsUpdated(cloneOfMaterialsList, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Part, the new values are set to null and all raw materials 
        /// from the list are selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.Price = random.Next(501, 800);
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.Price = random.Next(801, 1000);
            material3.YieldStrength = random.Next(1000).ToString();
            material3.RuptureStrength = random.Next(1000).ToString();
            material3.Density = random.Next(1000).ToString();
            material3.MaxElongation = random.Next(1000).ToString();
            material3.GlassTransitionTemperature = random.Next(1000).ToString();
            material3.Rx = random.Next(1000).ToString();
            material3.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

            // Retrieve the raw materials from RawMaterialsCollection
            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterials = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the part's raw materials from db in order to check that the materials were not updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            GetPartMaterials(dbPart, ref dbMaterials);

            // Verify that the updates were not applied
            int expectedNumberOfUpdatedObjects = 0;
            MaterialsUpdated(cloneOfMaterials, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> RawMaterial, the new values are set to valid values and the raw material 
        /// from the list is selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            RawMaterial materialClone = cloneManager.Clone(material1);

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = material1;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

            // Retrieve the raw materials from RawMaterialsCollection
            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterials = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the materials from db in order to check that the material was updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material1.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            dbMaterials.Add(dbMaterial);

            // Verify that the updates were applied
            int expectedNumberOfUpdatedObjects = 0;
            MaterialsUpdated(cloneOfMaterials, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> RawMaterial, the new values are set to valid values and the raw material 
        /// from the list is not selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Random random = new Random();
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.Price = random.Next(0, 500);
            material.YieldStrength = random.Next(1000).ToString();
            material.RuptureStrength = random.Next(1000).ToString();
            material.Density = random.Next(1000).ToString();
            material.MaxElongation = random.Next(1000).ToString();
            material.GlassTransitionTemperature = random.Next(1000).ToString();
            material.Rx = random.Next(1000).ToString();
            material.Rm = random.Next(1000).ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            RawMaterial materialClone = cloneManager.Clone(material);

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = material;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = new List<RawMaterialDataItem>();

            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the updates were not applied
            int expectedNumberOfUpdatedObjects = 0;
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material.Guid);
            MaterialsAreEqual(materialClone, dbMaterial);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> RawMaterial, the new values are set to null and the raw material 
        /// from the list is selected for update.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            RawMaterial materialClone = cloneManager.Clone(material1);

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = material1;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

            // Retrieve the raw materials from RawMaterialsCollection
            List<RawMaterial> materialsList = new List<RawMaterial>();
            foreach (RawMaterialDataItem materialDataItem in massUpdateRawMaterialsViewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    materialsList.Add(material);
                }
            }

            List<RawMaterial> cloneOfMaterials = CloneMaterialsList(materialsList, dataContext1);
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Retrieve the materials from db in order to check that the material was not updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material1.Guid);
            List<RawMaterial> dbMaterials = new List<RawMaterial>();
            dbMaterials.Add(dbMaterial);

            // Verify that the updates were applied
            int expectedNumberOfUpdatedObjects = 0;
            MaterialsUpdated(cloneOfMaterials, dbMaterials, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for ChangeMaterialsCollection method. The following input data were used:
        /// Current object: Project and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            var unitsService = new UnitsServiceMock();
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, unitsService);

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = rawMaterial1.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = rawMaterial1.Name;
            part2.RawMaterials.Add(rawMaterial2);

            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            project.Parts.Add(part3);

            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = rawMaterial3.Guid.ToString();
            part3.RawMaterials.Add(rawMaterial3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the current view model
            massUpdateRawMaterialsViewModel.Model = project;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            // Verify that the collection is empty
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 0, "The RawMaterialsCollection shouldn't be populated if the current object is a project and ApplyUpdatesToSubobjects = false.");
        }

        ///// <summary>
        ///// A test for ChangeMaterialsCollection method. The following input data were used:
        ///// Current object: Project and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void ChangeMaterialsCollectionTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    subassembly1.Parts.Add(part1);

        //    RawMaterial rawMaterial1 = new RawMaterial();
        //    rawMaterial1.Name = rawMaterial1.Guid.ToString();
        //    part1.RawMaterials.Add(rawMaterial1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = new OverheadSetting();
        //    assembly.Parts.Add(part2);

        //    RawMaterial rawMaterial2 = new RawMaterial();
        //    rawMaterial2.Name = rawMaterial1.Name;
        //    part2.RawMaterials.Add(rawMaterial2);

        //    Part part3 = new Part();
        //    part3.Name = part3.Guid.ToString();
        //    part3.OverheadSettings = new OverheadSetting();
        //    part3.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part3);

        //    RawMaterial rawMaterial3 = new RawMaterial();
        //    rawMaterial3.Name = rawMaterial3.Guid.ToString();
        //    part3.RawMaterials.Add(rawMaterial3);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    // Set up the current view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = project;
        //    massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateRawMaterialsViewModel.IDataSourceManager = dataContext1;
        //    massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

        //    // Verify that the collection contains all raw materials from the project and the materials with the same name are listed once
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 2, "The number of displayed raw materials is different than the expected one.");
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i => i.Name == rawMaterial1.Name).RawMaterials.Count == 2, "Only one raw material item should be listed for the materials with the same name.");
        //}

        /// <summary>
        /// A test for ChangeMaterialsCollection. The following input data were used:
        /// Current object: Assembly and the ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = rawMaterial1.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            subassembly2.Parts.Add(part2);

            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = rawMaterial2.Guid.ToString();
            part2.RawMaterials.Add(rawMaterial2);

            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part3);

            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = rawMaterial1.Name;
            part3.RawMaterials.Add(rawMaterial3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = assembly;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            // Verify that the RawMaterials collection is empty
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 0, "The RawMaterialsCollection should be empty if the current object is Assembly and ApplyUpdatesToSubobjects = false.");
        }

        /// <summary>
        /// A test for ChangeMaterialsCollection. The following input data were used:
        /// Current object: Assembly and the ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = rawMaterial1.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            subassembly2.Parts.Add(part2);

            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = rawMaterial2.Guid.ToString();
            part2.RawMaterials.Add(rawMaterial2);

            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part3);

            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = rawMaterial1.Name;
            part3.RawMaterials.Add(rawMaterial3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = assembly;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext1;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            // Verify that the RawMaterials collection is contains all raw materials from the assembly and the materials with the same name are listed once
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 2, "Failed to change the RawMaterialsCollection.");
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i => i.Name == rawMaterial1.Name).RawMaterials.Count == 2, "The raw materials with the same name should be listed once.");
        }

        /// <summary>
        /// A test for ChangeMaterialsCollection method using the following data:
        /// Current object: part.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material1.Name;
            part.RawMaterials.Add(material3);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 2, "Failed to change the RawMaterialsCollection.");
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i => i.Name == material1.Name).RawMaterials.Count == 2, "The raw materials with the same name should be listed once.");
        }

        /// <summary>
        /// A test for SelectionChangedCommand. 
        /// </summary>
        [TestMethod]
        public void ListBoxSelectionChangedTest()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService, new UnitsServiceMock());

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(2000, 3000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material1.Guid.ToString();
            material2.Price = material1.Price;
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.Price = material1.Price;
            material3.YieldStrength = material1.YieldStrength;
            material3.RuptureStrength = material2.RuptureStrength;
            material3.Density = material1.Density;
            material3.MaxElongation = random.Next(1000).ToString();
            material3.GlassTransitionTemperature = random.Next(1000).ToString();
            material3.Rx = random.Next(1000).ToString();
            material3.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material3);

            RawMaterial material4 = new RawMaterial();
            material4.Name = material4.Guid.ToString();
            material4.Price = material1.Price;
            material4.YieldStrength = material1.YieldStrength;
            material4.RuptureStrength = material2.RuptureStrength;
            material4.Density = random.Next(1000).ToString();
            material4.MaxElongation = random.Next(1000).ToString();
            material4.GlassTransitionTemperature = random.Next(1000).ToString();
            material4.Rx = material3.Rx;
            material4.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material4);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.Model = part;
            massUpdateRawMaterialsViewModel.DataSourceManager = dataContext;
            massUpdateRawMaterialsViewModel.IsTabSelected = true;
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.Where(m => m.Name == material1.Name || m.Name == material3.Name).ToList();

            List<RawMaterialDataItem> removedItems = new List<RawMaterialDataItem>();
            removedItems.Add(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(m => m.Name == material3.Name));

            List<RawMaterialDataItem> addedItems = new List<RawMaterialDataItem>();
            addedItems.Add(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(m => m.Name == material4.Name));

            SelectionChangedEventArgs args = new SelectionChangedEventArgs(Selector.SelectedEvent, removedItems, addedItems);
            ICommand selectionChangedCommand = massUpdateRawMaterialsViewModel.SelectionChangedCommand;
            selectionChangedCommand.Execute(args);

            // Verifies that an item that was deselected was removed from SelectedRawMaterials list
            foreach (RawMaterialDataItem removedItem in removedItems)
            {
                Assert.IsNull(massUpdateRawMaterialsViewModel.SelectedRawMaterials.FirstOrDefault(m => m.Name == removedItem.Name), "Failed to remove an item from selected materials collection.");
            }

            // Verifies that a selected item was added into  SelectedRawMaterials list
            foreach (RawMaterialDataItem addedItem in addedItems)
            {
                Assert.IsNotNull(massUpdateRawMaterialsViewModel.SelectedRawMaterials.FirstOrDefault(m => m.Name == addedItem.Name), "Failed to add an item to selected materials collection.");
            }

            // Verifies that the prices of selected materials are equal 
            Assert.AreEqual(massUpdateRawMaterialsViewModel.PriceCurrentValue, material1.Price, "Failed to display the price of selected materials.");
            
            // Verifies if the warning message is displayed
            Assert.IsTrue(massUpdateRawMaterialsViewModel.WarningMessageVisibility == System.Windows.Visibility.Visible, "Failed to display the warning message for the current values.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verifies if two materials have the same property values.
        /// The following properties will be compared: Price, YieldStrength, RuptureStrength,
        /// Density, MaxElongation, GlassTransitionTemperature, Rx and Rm.
        /// </summary>
        /// <param name="comparedMaterial">The compared raw material.</param>
        /// <param name="materialToCompareWith">The raw material to compare with.</param>
        private void MaterialsAreEqual(RawMaterial comparedMaterial, RawMaterial materialToCompareWith)
        {
            bool result = comparedMaterial.Price == materialToCompareWith.Price &&
                          string.Equals(comparedMaterial.YieldStrength, materialToCompareWith.YieldStrength) &&
                          string.Equals(comparedMaterial.RuptureStrength, materialToCompareWith.RuptureStrength) &&
                          string.Equals(comparedMaterial.Density, materialToCompareWith.Density) &&
                          string.Equals(comparedMaterial.MaxElongation, materialToCompareWith.MaxElongation) &&
                          string.Equals(comparedMaterial.GlassTransitionTemperature, materialToCompareWith.GlassTransitionTemperature) &&
                          string.Equals(comparedMaterial.Rx, materialToCompareWith.Rx) &&
                          string.Equals(comparedMaterial.Rm, materialToCompareWith.Rm);

            Assert.IsTrue(result, "The raw materials are not equal.");
        }

        /// <summary>
        /// Verifies if the selected raw materials were updated with the new values and that the others were not updated.
        /// </summary>
        /// <param name="oldMaterialsList">The list of old materials.</param>
        /// <param name="updatedMaterialsList">The list of the updated materials.</param>
        /// <param name="viewModel">The MassUpdateRawMaterialsViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The count of updated materials </param>
        private void MaterialsUpdated(List<RawMaterial> oldMaterialsList, List<RawMaterial> updatedMaterialsList, MassUpdateRawMaterialsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (RawMaterialDataItem materialDataItem in viewModel.RawMaterialsCollection)
            {
                foreach (RawMaterial material in materialDataItem.RawMaterials)
                {
                    RawMaterial oldMaterial = oldMaterialsList.FirstOrDefault(m => m.Guid == material.Guid);
                    RawMaterial updatedMaterial = updatedMaterialsList.FirstOrDefault(m => m.Guid == material.Guid);
                    bool objectUpdated = false;

                    if (viewModel.SelectedRawMaterials.Contains(materialDataItem))
                    {
                        if (viewModel.PriceNewValue != null)
                        {
                            Assert.AreEqual<decimal>(viewModel.PriceNewValue.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "Failed to update the Price property with the new value.");
                            objectUpdated = true;
                        }
                        else if (viewModel.PricePercent != null)
                        {
                            Assert.AreEqual<decimal>(viewModel.PricePercent.GetValueOrDefault() * oldMaterial.Price.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "Failed to update the Price property with the increase percentage.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<decimal>(oldMaterial.Price.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "The Price should not have been updated if the new value was null.");
                        }

                        if (viewModel.YieldStrengthNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.YieldStrengthNewValue, updatedMaterial.YieldStrength, "Failed to update the YieldStrength property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.YieldStrength, updatedMaterial.YieldStrength, "The YieldStrength should not have been updated if the new value was null.");
                        }

                        if (viewModel.RuptureStrengthNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.RuptureStrengthNewValue, updatedMaterial.RuptureStrength, "Failed to update the RuptureStrength property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.RuptureStrength, updatedMaterial.RuptureStrength, "The RuptureStrength should not have been updated if the new value was null.");
                        }

                        if (viewModel.DensityNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.DensityNewValue, updatedMaterial.Density, "Failed to update the Density property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.Density, updatedMaterial.Density, "The Density property should not have been updated if the new value was null.");
                        }

                        if (viewModel.MaxElongationNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.MaxElongationNewValue, updatedMaterial.MaxElongation, "Failed to update the MaxElongation property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.MaxElongation, updatedMaterial.MaxElongation, "The MaxElongation property should not have been updated if the new value was null.");
                        }

                        if (viewModel.GlassTTNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.GlassTTNewValue, updatedMaterial.GlassTransitionTemperature, "Failed to update the GlassTransitionTemperature property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.GlassTransitionTemperature, updatedMaterial.GlassTransitionTemperature, "The GlassTransitionTemperature property should not have been updated if the new value was null.");
                        }

                        if (viewModel.RxNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.RxNewValue, updatedMaterial.Rx, "Failed to update the Rx property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.Rx, updatedMaterial.Rx, "The Rx property should not have been updated if the new value was null.");
                        }

                        if (viewModel.RmNewValue != null)
                        {
                            Assert.AreEqual<string>(viewModel.RmNewValue, updatedMaterial.Rm, "Failed to update the Rm property with the new value.");
                            objectUpdated = true;
                        }
                        else
                        {
                            Assert.AreEqual<string>(oldMaterial.Rm, updatedMaterial.Rm, "The Rm property should not have been updated if the new value was null.");
                        }
                    }
                    else
                    {
                        MaterialsAreEqual(oldMaterial, updatedMaterial);
                    }

                    if (objectUpdated)
                    {
                        numberOfUpdatedObjects++;
                    }
                }
            }
        }

        /// <summary>
        /// Clones a list of raw materials including Guids.
        /// </summary>
        /// <param name="materialsList">The list of materials to be cloned.</param>
        /// <param name="dataContext">IDataSourceManager of CloneManager.</param>
        /// <returns>The list of materials clones.</returns>
        private List<RawMaterial> CloneMaterialsList(List<RawMaterial> materialsList, IDataSourceManager dataContext)
        {
            CloneManager cloneManager = new CloneManager(dataContext);
            List<RawMaterial> clonedList = new List<RawMaterial>();
            foreach (RawMaterial material in materialsList)
            {
                RawMaterial clone = cloneManager.Clone(material);
                clone.Guid = material.Guid;
                clonedList.Add(clone);
            }

            return clonedList;
        }

        /// <summary>
        /// Gets the list of all raw materials from the project.
        /// </summary>
        /// <param name="project">The parent project(fully loaded) of raw materials.</param>
        /// <param name="materialsList">The list of all raw materials belonging to the project.</param>
        private void GetProjectMaterials(Project project, ref List<RawMaterial> materialsList)
        {
            foreach (Part part in project.Parts)
            {
                GetPartMaterials(part, ref materialsList);
            }

            foreach (Assembly assembly in project.Assemblies)
            {
                GetAssemblyMaterials(assembly, ref materialsList);
            }
        }

        /// <summary>
        /// Gets the list of all raw materials from the assembly.
        /// </summary>
        /// <param name="assembly">The parent assembly(fully loaded) of raw materials.</param>
        /// <param name="materialsList">The list of all raw materials belonging to the assembly.</param>
        private void GetAssemblyMaterials(Assembly assembly, ref List<RawMaterial> materialsList)
        {
            foreach (Part part in assembly.Parts)
            {
                GetPartMaterials(part, ref materialsList);
            }

            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                GetAssemblyMaterials(subassembly, ref materialsList);
            }
        }

        /// <summary>
        ///  Gets the list of all raw materials from the part.
        /// </summary>
        /// <param name="part">The parent part(fully loaded) of raw materials.</param>
        /// <param name="materialsList">The raw materials</param>
        private void GetPartMaterials(Part part, ref List<RawMaterial> materialsList)
        {
            materialsList.AddRange(part.RawMaterials);
        }

        #endregion Helpers
    }
}
