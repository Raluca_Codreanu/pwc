﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The test class for the MassUpdateGeneralViewModel
    /// </summary>
    [TestClass]
    public class MassUpdateGeneralViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateGeneralViewModelTest"/> class.
        /// </summary>
        public MassUpdateGeneralViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand -> cancelling the update.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.SBMActive = false;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            part.Version = random.Next(100);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateGeneralViewModel.Model = part;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult(confirmation message) to "No" in order to cancel the update operation            
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that no update was applied for the current part
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPart(part.Guid);
            Assert.AreEqual(partClone.Version, dbPart.Version, "Failed to cancel the update operation.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.SBMActive = false;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            part.Version = random.Next(100);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateGeneralViewModel.Model = part;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPart(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to null values.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.SBMActive = false;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            part.Version = random.Next(100);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateGeneralViewModel.Model = part;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = null;

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current part were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPart(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.SBMActive = false;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            assembly.Version = random.Next(100);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.SBMActive = false;
            subassembly1.Version = random.Next(100);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.SBMActive = false;
            subassembly2.Version = random.Next(100);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.SBMActive = false;
            part1.Version = random.Next(100);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.SBMActive = false;
            part2.Version = random.Next(100);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateGeneralViewModel.Model = assembly;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current assembly were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assemblyDb = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, assemblyDb, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.SBMActive = false;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            assembly.Version = random.Next(100);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.SBMActive = false;
            subassembly1.Version = random.Next(100);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.SBMActive = false;
            subassembly2.Version = random.Next(100);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.SBMActive = false;
            part1.Version = random.Next(100);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.SBMActive = false;
            part2.Version = random.Next(100);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateGeneralViewModel.Model = assembly;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = true;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current assembly were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assemblyDb = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, assemblyDb, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, ApplyUpdatesToSubobjects = true and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.SBMActive = false;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            assembly.Version = random.Next(100);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.Version = random.Next(100);
            subassembly1.SBMActive = false;
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.Version = random.Next(100);
            subassembly2.SBMActive = false;
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.Version = random.Next(100);
            part1.SBMActive = false;
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.Version = random.Next(100);
            part1.SBMActive = false;
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateGeneralViewModel.Model = assembly;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = true;
            massUpdateGeneralViewModel.VersionNumberNewValue = null;

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current assembly were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assemblyDb = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, assemblyDb, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current Object -> Project, ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();
            var currency = new Currency();
            currency.IsoCode = Constants.DefaultCurrencyIsoCode;
            currency.Name = "Euro";
            currency.Symbol = "€";
            project.Currencies.Add(currency);
            project.BaseCurrency = currency;
            project.OverheadSettings = new OverheadSetting();

            Random random = new Random();
            project.Version = random.Next(100);

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.SBMActive = false;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.SBMActive = false;
            subassembly1.Version = random.Next(100);
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.SBMActive = false;
            subassembly2.Version = random.Next(100);
            subassembly2.CountrySettings = new CountrySetting();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.SBMActive = false;
            part1.Version = random.Next(100);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.SBMActive = false;
            part2.Version = random.Next(100);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model
            massUpdateGeneralViewModel.Model = project;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current project were updated
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current Object -> Project, ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateGeneralDataTest8() 
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Random random = new Random();
        //    project.Version = random.Next(100);

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.Version = random.Next(100);
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.Version = random.Next(100);
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly2.OverheadSettings = new OverheadSetting();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.Version = random.Next(100);
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    subassembly1.Parts.Add(part1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.Version = random.Next(100);
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = new OverheadSetting();
        //    project.Parts.Add(part2);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateGeneralViewModel.CurrentObject = project;
        //    massUpdateGeneralViewModel.IDataSourceManager = dataContext1;
        //    massUpdateGeneralViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the General properties of the current project were updated
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone,dbProject, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed after the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current Object -> Project, ApplyUpdatesToSubobjects = true and the new values are set to null.
        ///// </summary>
        //[TestMethod]
        //public void UpdateGeneralDataTest9() 
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Random random = new Random();
        //    project.Version = random.Next(100);

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.Version = random.Next(100);
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.Version = random.Next(100);
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly2.OverheadSettings = new OverheadSetting();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.Version = random.Next(100);
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    subassembly1.Parts.Add(part1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.Version = random.Next(100);
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = new OverheadSetting();
        //    project.Parts.Add(part2);

        //    IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model
        //    massUpdateGeneralViewModel.CurrentObject = project;
        //    massUpdateGeneralViewModel.IDataSourceManager = dataContext1;
        //    massUpdateGeneralViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateGeneralViewModel.VersionNumberNewValue = null;

        //    // Set up the MessageDialogResult to "Yes" in order to perform the update operation
        //    ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the General properties of the current project were updated
        //    IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone,dbProject, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed after the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current Object -> Raw Material.
        /// </summary>
        [TestMethod]
        public void UpdateGeneralDataTest10()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material);
            dataContext1.SaveChanges();

            // Set up the view model
            Random random = new Random();
            massUpdateGeneralViewModel.Model = material;
            massUpdateGeneralViewModel.DataSourceManager = dataContext1;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.VersionNumberNewValue = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateTheCalculationVariant()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            messageBoxService.MessageDialogResult = MessageDialogResult.Yes;
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.SBMActive = false;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model
            massUpdateGeneralViewModel.Model = assembly;
            massUpdateGeneralViewModel.DataSourceManager = dataContext;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.CalculationVariantNewValue.Value = EncryptionManager.Instance.GenerateRandomString(7, true);

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current part were updated
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly dbAssembly = dataContext1.AssemblyRepository.GetAssembly(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part, ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateTheCalculationVariant1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            messageBoxService.MessageDialogResult = MessageDialogResult.Yes;
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateGeneralViewModel massUpdateGeneralViewModel = new MassUpdateGeneralViewModel(messenger, windowService);

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.SBMActive = false;
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model
            massUpdateGeneralViewModel.Model = part;
            massUpdateGeneralViewModel.DataSourceManager = dataContext;
            massUpdateGeneralViewModel.ApplyUpdatesToSubObjects.Value = false;
            massUpdateGeneralViewModel.CalculationVariantNewValue.Value = EncryptionManager.Instance.GenerateRandomString(7, true);

            // Set up the MessageDialogResult(confirmation message) to "Yes" in order to perform the update operation
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateGeneralViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the General properties of the current part were updated
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part dbPart = dataContext1.PartRepository.GetPart(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateGeneralViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed after the update operation was performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation was performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verify if the General information of a project were updated.
        /// </summary>
        /// <param name="oldProject"> The old project.</param>
        /// <param name="updatedProject"> The updated project.</param>
        /// <param name="viewModel">The MassUpdateGeneralViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateGeneralViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.VersionNumberNewValue != null)
            {
                Assert.AreEqual(viewModel.VersionNumberNewValue, updatedProject.Version, "Failed to update project's version with the new value.");
                numberOfUpdatedObjects++;
            }
            else
            {
                Assert.AreEqual(oldProject.Version, updatedProject.Version, "The Version should not have been updated if the new value was null.");
            }

            if (viewModel.ApplyUpdatesToSubObjects.Value)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
        }

        /// <summary>
        /// Verify if the General information of an assembly were updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The MassUpdateGeneralViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">the number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateGeneralViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.VersionNumberNewValue != null)
            {
                Assert.AreEqual(viewModel.VersionNumberNewValue, updatedAssembly.Version, "Failed to update assembly's version with the new value");
            }
            else
            {
                Assert.AreEqual(oldAssembly.Version, updatedAssembly.Version, "The Version should not have been updated if the new value was null.");
            }

            if (oldAssembly.Version != updatedAssembly.Version)
            {
                numberOfUpdatedObjects++;
            }

            if (oldAssembly.CalculationVariant != updatedAssembly.CalculationVariant)
            {
                numberOfUpdatedObjects++;
            }

            if (viewModel.ApplyUpdatesToSubObjects.Value)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
        }

        /// <summary>
        /// Verify if the General information of a part were updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The MassUpdateGeneralViewModel which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateGeneralViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.VersionNumberNewValue != null)
            {
                Assert.AreEqual(viewModel.VersionNumberNewValue, updatedPart.Version, "Failed to update part's version with the new value.");
            }
            else
            {
                Assert.AreEqual(oldPart.Version, updatedPart.Version, "The Version should not have been updated if the new value was null.");
            }

            if (oldPart.Version != updatedPart.Version)
            {
                numberOfUpdatedObjects++;
            }

            if (oldPart.CalculationVariant != updatedPart.CalculationVariant)
            {
                numberOfUpdatedObjects++;
            }

            if (oldPart.RawPart != null &&
                updatedPart.RawPart != null &&
                !oldPart.RawPart.IsDeleted &&
                !updatedPart.RawPart.IsDeleted)
            {
                PartUpdated(oldPart.RawPart, updatedPart.RawPart, viewModel, ref numberOfUpdatedObjects);
            }
        }

        #endregion Helpers
    }
}
