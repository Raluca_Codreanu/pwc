﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// The Manage Users view model test.
    /// </summary>
    [TestClass]
    public class ManageUsersViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The Manage Users View Model.
        /// </summary>
        private ManageUsersViewModel manageUsersViewModel;

        /// <summary>
        /// The container.
        /// </summary>
        private CompositionContainer container;

        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageUsersViewModelTest"/> class.
        /// </summary>
        public ManageUsersViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var typeCatalog = new TypeCatalog(
                typeof(OnlineCheckServiceMock),
                typeof(UserViewModel),
                typeof(WindowServiceMock),
                typeof(MessageDialogServiceMock),
                typeof(DispatcherServiceMock));

            var bootstrapper = new Bootstrapper();
            bootstrapper.AddCatalog(typeCatalog);

            this.messenger = new Messenger();
            this.messageDialogService = new MessageDialogServiceMock();
            this.container = bootstrapper.CompositionContainer;
            this.windowService = new WindowServiceMock(this.messageDialogService, new DispatcherServiceMock());
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            this.manageUsersViewModel = new ManageUsersViewModel(this.windowService, this.container);
        }

        #region Test methods

        /// <summary>
        /// Deletes an user without projects with success test.
        /// </summary>
        [TestMethod]
        public void DeleteAnUserWithoutProjectsTest()
        {
            // Add a new user.
            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Name = newUser.Guid.ToString();
            newUser.Roles = Role.User;

            dataManager.UserRepository.Add(newUser);
            dataManager.SaveChanges();

            this.manageUsersViewModel = new ManageUsersViewModel(this.windowService, this.container);

            // Delete the new added user.
            manageUsersViewModel.SelectedUser = manageUsersViewModel.Users.FirstOrDefault(u => u.Guid == newUser.Guid);
            ((MessageDialogServiceMock)this.messageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand deleteUserCommand = manageUsersViewModel.DeleteUserCommand;
            deleteUserCommand.Execute(null);

            var allUsers = dataManager.UserRepository.GetAll(true).ToList();
            var isUser = allUsers.Any(u => u == newUser);
            Assert.IsFalse(isUser);
        }

        /// <summary>
        /// Deletes an user with projects with success test.
        /// </summary>
        [TestMethod]
        public void DeleteAnUserWithProjectsTest()
        {
            // Add a new user.
            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Name = newUser.Guid.ToString();
            newUser.Roles = Role.User;

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.StartDate = DateTime.Now;
            project.EndDate = DateTime.Now;
            project.Status = ProjectStatus.ValidatedInternal;
            project.Partner = "Partner" + DateTime.Now.Ticks;
            project.ProjectLeader = newUser;
            project.ResponsibleCalculator = newUser;
            project.Version = 1;
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(newUser);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 3, 3, 9 };
            document.OriginalFileName = document.Guid.ToString();
            document.Size = document.Content.Length;
            project.Media.Add(document);

            dataManager.UserRepository.Add(newUser);
            dataManager.ProjectRepository.Add(project);
            dataManager.SaveChanges();

            this.manageUsersViewModel = new ManageUsersViewModel(this.windowService, this.container);

            // Delete the new added user.
            manageUsersViewModel.SelectedUser = manageUsersViewModel.Users.FirstOrDefault(u => u.Guid == newUser.Guid);
            ((MessageDialogServiceMock)this.messageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            ICommand deleteUserCommand = manageUsersViewModel.DeleteUserCommand;
            deleteUserCommand.Execute(null);

            var allUsers = dataManager.UserRepository.GetAll(true).ToList();
            var isUser = allUsers.Any(u => u == newUser);
            Assert.IsFalse(isUser);
        }

        /// <summary>
        /// Rejects to delete an user with projects test.
        /// </summary>
        [TestMethod]
        public void RejectToDeleteAnUserWithProjectsTest()
        {
            // Add a new user.
            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Name = newUser.Guid.ToString();
            newUser.Roles = Role.User;

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.StartDate = DateTime.Now;
            project.EndDate = DateTime.Now;
            project.Status = ProjectStatus.ValidatedInternal;
            project.Partner = "Partner" + DateTime.Now.Ticks;
            project.ProjectLeader = newUser;
            project.ResponsibleCalculator = newUser;
            project.Version = 1;
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(newUser);

            dataManager.UserRepository.Add(newUser);
            dataManager.ProjectRepository.Add(project);
            dataManager.SaveChanges();

            this.manageUsersViewModel = new ManageUsersViewModel(this.windowService, this.container);

            // Delete the new added user.
            manageUsersViewModel.SelectedUser = manageUsersViewModel.Users.FirstOrDefault(u => u.Guid == newUser.Guid);
            ((MessageDialogServiceMock)this.messageDialogService).MessageDialogResult = MessageDialogResult.No;
            ICommand deleteUserCommand = manageUsersViewModel.DeleteUserCommand;
            deleteUserCommand.Execute(null);

            var allUsers = dataManager.UserRepository.GetAll(true).ToList();
            var isUser = allUsers.Any(u => u == newUser);
            Assert.IsTrue(isUser);
        }

        /// <summary>
        /// Shows the disabled users with success test.
        /// </summary>
        [TestMethod]
        public void ShowDisabledUsersTest()
        {
            // Add a new user.
            User newUser = new User();
            newUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser.Name = newUser.Guid.ToString();
            newUser.Roles = Role.User;
            newUser.Disabled = true;

            dataManager.UserRepository.Add(newUser);
            dataManager.SaveChanges();

            // Add a new user.
            User newUser1 = new User();
            newUser1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            newUser1.Password = EncryptionManager.Instance.HashSHA256("Password.", newUser.Salt, newUser.Guid.ToString());
            newUser1.Name = newUser.Guid.ToString();
            newUser1.Roles = Role.User;
            newUser1.Disabled = false;

            dataManager.UserRepository.Add(newUser1);
            dataManager.SaveChanges();

            this.manageUsersViewModel = new ManageUsersViewModel(this.windowService, this.container);
            var allUsers = dataManager.UserRepository.GetAll(true).ToList();

            // Don't show the disabled users.
            manageUsersViewModel.ShowDisabledUsers = false;
            Assert.AreEqual(allUsers.Where(a => a.Disabled == false).ToList().Count, manageUsersViewModel.Users.Count);

            // Show the disabled users.
            manageUsersViewModel.ShowDisabledUsers = true;
            Assert.AreEqual(allUsers.Count, manageUsersViewModel.Users.Count);
        }

        #endregion
    }
}
