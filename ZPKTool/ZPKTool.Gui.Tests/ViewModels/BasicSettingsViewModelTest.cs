﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This class is used to test BasicSettingsViewModel
    /// </summary>
    [TestClass]
    public class BasicSettingsViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The Test Context service.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate the test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.unitsService = new UnitsServiceMock();
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region TestMethods

        /// <summary>
        /// This method tests the editable fields of Basic Settings. Save button is also tested 
        /// </summary>
        [TestMethod]
        public void BasicSettingsEditableFields()
        {
            var basicSettingsVm = new BasicSettingsViewModel(
                this.messenger,
                this.windowService,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            basicSettingsVm.DataSourceManager = localDataManager;
            var basicSettings = this.CreateNewBasicSettings(localDataManager);
            basicSettingsVm.Model = basicSettings;

            basicSettingsVm.DeprPeriod.Value = 200;
            basicSettingsVm.PartBatch.Value = 300;
            basicSettingsVm.HoursPerShift.Value = 400;
            basicSettingsVm.ProdWeeks.Value = 20;
            basicSettingsVm.LogisticCostRatio.Value = 500;
            basicSettingsVm.DeprRate.Value = 600;
            basicSettingsVm.ShiftsPerWeek.Value = 700;
            basicSettingsVm.ProdDays.Value = 1;
            basicSettingsVm.AssetRate.Value = 800;

            var saveCommand = basicSettingsVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(basicSettings.DeprPeriod, basicSettingsVm.DeprPeriod.Value);
            Assert.AreEqual(basicSettings.PartBatch, basicSettingsVm.PartBatch.Value);
            Assert.AreEqual(basicSettings.HoursPerShift, basicSettingsVm.HoursPerShift.Value);
            Assert.AreEqual(basicSettings.ProdWeeks, basicSettingsVm.ProdWeeks.Value);
            Assert.AreEqual(basicSettings.LogisticCostRatio, basicSettingsVm.LogisticCostRatio.Value);
            Assert.AreEqual(basicSettings.DeprRate, basicSettingsVm.DeprRate.Value);
            Assert.AreEqual(basicSettings.ShiftsPerWeek, basicSettingsVm.ShiftsPerWeek.Value);
            Assert.AreEqual(basicSettings.ProdDays, basicSettingsVm.ProdDays.Value);
            Assert.AreEqual(basicSettings.AssetRate, basicSettingsVm.AssetRate.Value);
        }

        /// <summary>
        /// This method tests the undo button. 
        /// </summary>
        [TestMethod]
        public void UndoButton()
        {
            var basicSettingsVm = new BasicSettingsViewModel(
                this.messenger,
                this.windowService,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            basicSettingsVm.DataSourceManager = localDataManager;
            var basicSettings = this.CreateNewBasicSettings(localDataManager);
            basicSettingsVm.Model = basicSettings;

            const int ValueDeprPeriod = 2000;
            basicSettingsVm.DeprPeriod.Value = ValueDeprPeriod;
            basicSettingsVm.HoursPerShift.Value = 20;

            basicSettingsVm.DeprPeriod.Value = 500;
            basicSettingsVm.HoursPerShift.Value = 30;

            var undoCommnad = basicSettingsVm.UndoCommand;
            undoCommnad.Execute(null);

            Assert.AreEqual(basicSettingsVm.DeprPeriod.Value, 500);
            Assert.AreEqual(basicSettingsVm.HoursPerShift.Value, 20);

            undoCommnad.Execute(null);

            Assert.AreEqual(basicSettingsVm.DeprPeriod.Value, ValueDeprPeriod);
        }

        /// <summary>
        /// This method tests the cancel button. 
        /// </summary>
        [TestMethod]
        public void CancelButton()
        {
            var basicSettingsVm = new BasicSettingsViewModel(
                this.messenger,
                this.windowService,
                this.unitsService);

            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(
                service =>
                service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            basicSettingsVm.DataSourceManager = localDataManager;
            var basicSettings = this.CreateNewBasicSettings(localDataManager);
            basicSettingsVm.Model = basicSettings;

            const int ValueDeprPeriod = 3000;
            basicSettingsVm.DeprPeriod.Value = ValueDeprPeriod;

            var saveCommand = basicSettingsVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(basicSettings.DeprPeriod, ValueDeprPeriod);

            basicSettingsVm.DeprPeriod.Value = 30000;

            var cancelCommnad = basicSettingsVm.CancelCommand;
            cancelCommnad.Execute(null);

            Assert.AreEqual(basicSettings.DeprPeriod, ValueDeprPeriod);
        }

        #endregion TestMethods

        #region Private Methods
        /// <summary>
        /// This method creates a new basicSettings 
        /// </summary>
        /// <returns>Returns basicSettings</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private BasicSetting CreateNewBasicSettings(IDataSourceManager dataManager)
        {
            var basicSettings = new BasicSetting() { Guid = Guid.NewGuid() };

            dataManager.BasicSettingsRepository.Save(basicSettings);
            dataManager.SaveChanges();

            return basicSettings;
        }

        #endregion Private Methods
    }
}
