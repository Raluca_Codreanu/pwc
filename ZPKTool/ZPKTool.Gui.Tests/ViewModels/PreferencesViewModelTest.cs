﻿using System;
using System.IO;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// The Preferences view model test.
    /// </summary>
    [TestClass]
    public class PreferencesViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The preferences view model.
        /// </summary>
        private PreferencesViewModel preferencesViewModel;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private MessageDialogServiceMock messageDialogService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferencesViewModelTest"/> class.
        /// </summary>
        public PreferencesViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.messageDialogService = new MessageDialogServiceMock();
            this.windowService = new WindowServiceMock(this.messageDialogService, new DispatcherServiceMock());
            this.fileDialogService = new FileDialogServiceMock();
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            UserSettingsManager.Instance.SettingsFilePath = Path.Combine(Environment.CurrentDirectory, "settings.xml");
            GlobalSettingsManager.Instance.SettingsFilePath = Path.Combine(Environment.CurrentDirectory, "GlobalSettings.xml");
        }

        #region Test methods

        /// <summary>
        /// Saves the preferences view model display changes test.
        /// </summary>
        [TestMethod]
        public void SaveDisplayChangesTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "en-US";
            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Metric;
            this.preferencesViewModel.SelectedMaterialLocalization = RawMaterialLocalisedName.GermanName;
            this.preferencesViewModel.SelectedDecimalPlacesNumber = 2;
            this.preferencesViewModel.SelectedBackgroundID = 0;
            this.preferencesViewModel.SelectedMasterDataBrowserDataSource = DbLocation.Central;

            // Result details settings.
            this.preferencesViewModel.SelectedChartViewMode = ChartViewMode.WatefallView;
            this.preferencesViewModel.ShowPercentagesInWaterfallChart = true;

            // Header settings.
            this.preferencesViewModel.HeaderDisplaysCalculationResult = true;
            this.preferencesViewModel.HeaderDisplaysWeight = true;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.SelectedDecimalPlacesNumber = 3;
            this.preferencesViewModel.SelectedChartViewMode = ChartViewMode.PieView;
            this.preferencesViewModel.HeaderDisplaysCalculationResult = false;

            ICommand saveCommand1 = this.preferencesViewModel.SaveCommand;
            saveCommand1.Execute(null);

            Assert.AreEqual(this.preferencesViewModel.SelectedDecimalPlacesNumber, UserSettingsManager.Instance.DecimalPlacesNumber);
            Assert.AreEqual(this.preferencesViewModel.SelectedChartViewMode.GetHashCode(), UserSettingsManager.Instance.DefaultChartViewMode);
            Assert.AreEqual(this.preferencesViewModel.HeaderDisplaysCalculationResult, UserSettingsManager.Instance.HeaderDisplayCalculationResult);
        }

        /// <summary>
        /// Saves the preferences view model database changes test.
        /// </summary>
        [TestMethod]
        public void SaveDatabaseChangesTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "en-US";
            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Metric;
            this.preferencesViewModel.SelectedMaterialLocalization = RawMaterialLocalisedName.GermanName;
            this.preferencesViewModel.SelectedDecimalPlacesNumber = 2;
            this.preferencesViewModel.SelectedBackgroundID = 0;
            this.preferencesViewModel.SelectedMasterDataBrowserDataSource = DbLocation.Central;

            // Local Database configuration.
            this.preferencesViewModel.LocalDbServer = @"localhost\SQLSERVER2012";
            this.preferencesViewModel.LocalDbUseWindowsAuth = true;

            // Central Database configuration.
            this.preferencesViewModel.CentralDbServer = @"localhost\SQLSERVER2012";
            this.preferencesViewModel.CentralDbUseWindowsAuth = true;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.LocalDbUseWindowsAuth = false;
            this.preferencesViewModel.LocalDbUser = "PROimpensUser";
            this.preferencesViewModel.LocalDbPassword = "Password";

            ICommand saveCommand1 = this.preferencesViewModel.SaveCommand;
            saveCommand1.Execute(null);

            var passwordGlobaSettingsDecrypted = EncryptionManager.Instance.DecryptBlowfish(GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted, Constants.PasswordEncryptionKey);

            Assert.IsFalse(this.preferencesViewModel.LocalDbUseWindowsAuth);
            Assert.AreEqual(this.preferencesViewModel.LocalDbPassword, passwordGlobaSettingsDecrypted);
        }

        /// <summary>
        /// Saves the preferences view custom logo changes test.
        /// </summary>
        [TestMethod]
        public void SaveCustomLogoChangesTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "en-US";
            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Metric;
            this.preferencesViewModel.CustomLogoPath = string.Empty;
            this.preferencesViewModel.SelectedChartViewMode = ChartViewMode.WatefallView;

            // Custom logo.
            this.preferencesViewModel.CustomLogoPath = string.Empty;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.CustomLogoPath = Path.Combine(Environment.CurrentDirectory, "PictureSample2.png");
            this.fileDialogService.FileName = Path.Combine(Environment.CurrentDirectory, "PictureSample2.png");
            ICommand browseLogoCommand = this.preferencesViewModel.BrowseForLogoCommand;
            browseLogoCommand.Execute(null);

            ICommand saveCommand1 = this.preferencesViewModel.SaveCommand;
            saveCommand1.Execute(null);

            Assert.AreNotEqual(string.Empty, UserSettingsManager.Instance.CustomLogoPath);
        }

        /// <summary>
        /// This method tests restart application button.
        /// </summary>
        [TestMethod]
        public void RestartApplicationButtonTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "de-DE";
            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Metric;
            this.preferencesViewModel.SelectedDecimalPlacesNumber = 2;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.SelectedUILanguage = "en-US";
            Assert.IsTrue(this.preferencesViewModel.ShowRestart);

            ICommand restartApplication = this.preferencesViewModel.RestartApplicationCommand;
            restartApplication.Execute(null);

            Assert.AreEqual(this.preferencesViewModel.SelectedUILanguage, UserSettingsManager.Instance.UILanguage);
        }

        /// <summary>
        /// This method tests undo button.
        /// </summary>
        [TestMethod]
        public void UndoButtonTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "en-US";
            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Metric;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.SelectedUnitsSystem.Value = UnitsSystem.Imperial;
            ICommand undoCommand = this.preferencesViewModel.UndoCommand;
            undoCommand.Execute(null);

            Assert.AreEqual(this.preferencesViewModel.SelectedUnitsSystem.Value, UnitsSystem.Metric);
        }

        /// <summary>
        /// This method tests cancel button.
        /// </summary>
        [TestMethod]
        public void CancelButtonTest()
        {
            this.preferencesViewModel = new PreferencesViewModel(this.messenger, this.windowService, this.fileDialogService);

            // General settings.
            this.preferencesViewModel.SelectedUILanguage = "en-US";
            this.preferencesViewModel.SelectedChartViewMode = ChartViewMode.WatefallView;
            this.preferencesViewModel.HeaderDisplaysCalculationResult = true;
            this.preferencesViewModel.LocalDbUseWindowsAuth = true;

            ICommand saveCommand = this.preferencesViewModel.SaveCommand;
            saveCommand.Execute(null);

            this.preferencesViewModel.SelectedUILanguage = "de-DE";
            this.preferencesViewModel.HeaderDisplaysCalculationResult = false;
            this.preferencesViewModel.LocalDbUseWindowsAuth = false;

            ICommand cancelCommand = this.preferencesViewModel.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreNotEqual(this.preferencesViewModel.SelectedUILanguage, UserSettingsManager.Instance.UILanguage);
            Assert.AreNotEqual(this.preferencesViewModel.HeaderDisplaysCalculationResult, UserSettingsManager.Instance.HeaderDisplayCalculationResult);
            Assert.AreNotEqual(this.preferencesViewModel.LocalDbUseWindowsAuth, GlobalSettingsManager.Instance.UseWindowsAuthForLocalDb);
        }

        #endregion
    }
}
