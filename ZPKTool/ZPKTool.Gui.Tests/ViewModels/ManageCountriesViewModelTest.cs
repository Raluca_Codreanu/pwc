﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ManageCountriesViewModel and is intended 
    /// to contain Unit Tests for all the public methods from ManageCountriesViewModel class
    /// </summary>
    [TestClass]
    public class ManageCountriesViewModelTest
    {
        /// <summary>
        /// The test context
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        /// <summary>
        /// Configure database access before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        #endregion

        /// <summary>
        /// Create a composition container using parameters
        /// </summary>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The windows service.</param>
        /// <param name="unitsService">The units service</param>
        /// <returns>The custom composition container</returns>
        private CompositionContainer SetupCompositionContainer(out MessageDialogServiceMock messageDialogService, out WindowServiceMock windowService, out IUnitsService unitsService)
        {
            messageDialogService = new MessageDialogServiceMock();
            windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            unitsService = new UnitsServiceMock();

            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel, unitsService);

            return compositionContainer;
        }

        /// <summary>
        /// Create a new country
        /// </summary>
        /// <returns>The new created country</returns>
        public Country NewCountry()
        {
            Country newCountry = new Country();
            newCountry.CountrySetting = new CountrySetting();
            newCountry.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            
            var currency = new Currency()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true),
                IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true)
            };
            newCountry.Currency = currency;

            var mu = new MeasurementUnit()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                Symbol = EncryptionManager.Instance.GenerateRandomString(2, true)
            };
            newCountry.FloorMeasurementUnit = mu;
            newCountry.LengthMeasurementUnit = mu;
            newCountry.TimeMeasurementUnit = mu;
            newCountry.VolumeMeasurementUnit = mu;
            newCountry.WeightMeasurementUnit = mu;

            return newCountry;
        }

        #region Test Methods for Country objects

        /// <summary>
        /// A test for MouseDownCommand on CountriesDataGrid when the selected item is changed
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void MouseDownCommandCountryViewModelIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = manageCountriesViewModel.Countries.FirstOrDefault();
            countryViewModel.Name.Value = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ICommand mouseDownCommand = manageCountriesViewModel.CountriesDataGridMouseDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            mouseDownCommand.Execute(manageCountriesViewModel.SelectedCountry);

            Assert.AreEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country was not modified.");
        }

        /// <summary>
        /// A test for MouseDownCommand on CountriesDataGrid when the selected item is changed
        /// and the user does not save the model differences
        /// </summary>
        [TestMethod]
        public void MouseDownCommandCountryViewModelIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = manageCountriesViewModel.Countries.FirstOrDefault();
            countryViewModel.Name.Value = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ICommand mouseDownCommand = manageCountriesViewModel.CountriesDataGridMouseDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            mouseDownCommand.Execute(manageCountriesViewModel.SelectedCountry);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country was modified.");
        }

        /// <summary>
        /// A test for MouseDownCommand on CountriesDataGrid when the selected item is null
        /// </summary>
        [TestMethod]
        public void MouseDownCommandCountryNullParameterTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = manageCountriesViewModel.Countries.FirstOrDefault();
            countryViewModel.Name.Value = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ICommand mouseDownCommand = manageCountriesViewModel.CountriesDataGridMouseDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            mouseDownCommand.Execute(null);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country name was saved.");
        }

        /// <summary>
        /// A test for KeyDownCommand on CountriesDataGrid when the selected item is changed
        /// and the user does not save the model differences
        /// </summary>
        [TestMethod]
        public void KeyDownCommandCountryViewModelIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = manageCountriesViewModel.Countries.FirstOrDefault();
            countryViewModel.Name.Value = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ICommand keyDownCommand = manageCountriesViewModel.CountriesDataGridKeyDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            keyDownCommand.Execute(manageCountriesViewModel.SelectedCountry);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country name was saved.");
        }

        /// <summary>
        /// A test for KeyDownCommand on CountriesDataGrid when the selected item is null
        /// </summary>
        [TestMethod]
        public void KeyDownCommandCountryNullParameterTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            var countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            var countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.DataSourceManager = dataManager;
            countryViewModel.Name.Value = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ICommand keyDownCommand = manageCountriesViewModel.CountriesDataGridKeyDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            keyDownCommand.Execute(null);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country name was saved");
        }

        /// <summary>
        /// A test for add new country using a new instance of CountryViewModel into a window
        /// and the add country is with success
        /// </summary>
        [TestMethod]
        public void AddCountryWithSuccessTest()
        {
            var unitsService = new UnitsServiceMock();
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country newCountry = NewCountry();

            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(countryViewModel, "CreateCountryViewTemplate", true, false))
                .Callback(() =>
                {
                    countryViewModel.Model = newCountry;
                    countryViewModel.SaveCommand.Execute(null);
                });

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel);

            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowServiceMock.Object, compositionContainer) { DataSourceManager = dataManager };
            manageCountriesViewModel.AddCountryCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.Countries.Contains(newCountry));
        }

        /// <summary>
        /// A test for add new country using a new instance of CountryViewModel into a window
        /// and the add country is canceled
        /// </summary>
        [TestMethod]
        public void AddCountryWithCancelTest()
        {
            var unitsService = new UnitsServiceMock();
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country newCountry = NewCountry();
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(countryViewModel, "CreateCountryViewTemplate", true, false))
                .Callback(() =>
                {
                    // Trigger the release of the Model property value.
                    countryViewModel.OnUnloading();
                });
            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel);

            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowServiceMock.Object, compositionContainer) { DataSourceManager = dataManager };
            manageCountriesViewModel.AddCountryCommand.Execute(null);

            Assert.IsFalse(manageCountriesViewModel.Countries.Contains(newCountry));
        }

        /// <summary>
        /// A test for delete command on a country from database
        /// and the user confirms the delete action when confirmation message appears.
        /// </summary>
        [TestMethod]
        public void DeleteCountryWithSuccesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;

            ICommand deleteCommand = manageCountriesViewModel.DeleteCountryCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            deleteCommand.Execute(null);

            Assert.IsFalse(manageCountriesViewModel.Countries.Contains(newCountry));
        }

        /// <summary>
        /// A test for delete command on a country from database
        /// and the user does not confirm the delete action when confirmation message appears
        /// </summary>
        [TestMethod]
        public void DeleteCountryAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;

            ICommand deleteCommand = manageCountriesViewModel.DeleteCountryCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            deleteCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.Countries.Contains(newCountry));
        }

        /// <summary>
        /// A test for delete command on a country from database
        /// and the selected country to delete is null
        /// </summary>
        [TestMethod]
        public void DeleteCountryWithNullSelectedCountryTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country c1 = new Country();
            Country c2 = new Country();
            Country c3 = new Country();

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(c1);
            manageCountriesViewModel.Countries.Add(c2);
            manageCountriesViewModel.Countries.Add(c3);
            manageCountriesViewModel.SelectedCountry = null;

            ICommand deleteCommand = manageCountriesViewModel.DeleteCountryCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            deleteCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.Countries.Contains(c1));
            Assert.IsTrue(manageCountriesViewModel.Countries.Contains(c2));
            Assert.IsTrue(manageCountriesViewModel.Countries.Contains(c3));
        }

        /// <summary>
        /// A test for control moved from the countries main area
        /// The manageCountriesViewModel is changed, its input is valid
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountryViewModelIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country newCountry = NewCountry();

            manageCountriesViewModel.DataSourceManager = dataManager;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            newCountry.Name = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            manageCountriesViewModel.OnUnloading();

            Assert.AreEqual(newName, newCountry.Name, "The country was not modified");
        }

        /// <summary>
        /// A test for control moved from the countries main area
        /// The manageCountriesViewModel is changed, its input is valid
        /// and the user does not save the model differences
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountryViewModelIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country newCountry = NewCountry();

            manageCountriesViewModel.DataSourceManager = dataManager;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            newCountry.Name = newName;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            manageCountriesViewModel.OnUnloading();

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountry.Name, "The country was modified");
        }

        /// <summary>
        /// A test for control moved from the countries main area
        /// The manageCountriesViewModel is changed and its input is not valid
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountriesIsInputValidFalseTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.Name.Value = string.Empty;

            manageCountriesViewModel.CountryViewModel.IsChanged = true;
            manageCountriesViewModel.CountryViewModel.IsInputValid = false;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var unloadingStatus = manageCountriesViewModel.OnUnloading();
            Assert.IsFalse(unloadingStatus);
        }

        #endregion

        #region Test Methods for CountrySupplier objects

        /// <summary>
        /// A test for KeyDownCommand on StatesDataGrid when the selected item is changed
        /// and the user saves the model differences.
        /// </summary>
        [TestMethod]
        public void KeyDownCommandSupplierVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;
            countryViewModel.SaveCommand.Execute(null);

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.SaveCommand.Execute(null);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ICommand keyDownCommand = manageCountriesViewModel.StatesDataGridKeyDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            keyDownCommand.Execute(manageCountriesViewModel.SelectedCountrySupplier);

            Assert.AreEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was not changed.");
        }

        /// <summary>
        /// A test for KeyDownCommand on StatesDataGrid when the selected item is changed
        /// and the user does not save the model differences.
        /// </summary>
        [TestMethod]
        public void KeyDownCommandSupplierVMIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;
            countryViewModel.SaveCommand.Execute(null);

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.SaveCommand.Execute(null);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ICommand keyDownCommand = manageCountriesViewModel.StatesDataGridKeyDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            keyDownCommand.Execute(manageCountriesViewModel.SelectedCountrySupplier);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was changed.");
        }

        /// <summary>
        /// A test for KeyDownCommand on StatesDataGrid when the selected item is null.
        /// </summary>
        [TestMethod]
        public void KeyDownCommandSupplierNullParameterTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;
            countryViewModel.SaveCommand.Execute(null);

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.SaveCommand.Execute(null);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ICommand keyDownCommand = manageCountriesViewModel.StatesDataGridKeyDownCommand;
            keyDownCommand.Execute(null);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was changed.");
        }

        /// <summary>
        /// A test for MouseDownCommand on StatesDataGrid when the selected item is changed
        /// and the user does not save the model differences.
        /// </summary>
        [TestMethod]
        public void MouseDownCommandSupplierVMIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;
            countryViewModel.SaveCommand.Execute(null);

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.SaveCommand.Execute(null);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ICommand mouseDownCommand = manageCountriesViewModel.StatesDataGridMouseDownCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            mouseDownCommand.Execute(manageCountriesViewModel.SelectedCountrySupplier);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was changed.");
        }

        /// <summary>
        /// A test for MouseDownCommand on StatesDataGrid when the selected item is null.
        /// </summary>
        [TestMethod]
        public void MouseDownCommandSupplierNullParameterTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel =
                new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            countryViewModel.EditMode = ViewModelEditMode.Create;
            countryViewModel.Model = newCountry;
            countryViewModel.SaveCommand.Execute(null);

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            countrySupplierViewModel.EditMode = ViewModelEditMode.Create;
            countrySupplierViewModel.Model = newSupplier;
            countrySupplierViewModel.SaveCommand.Execute(null);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ICommand mouseDownCommand = manageCountriesViewModel.StatesDataGridMouseDownCommand;
            mouseDownCommand.Execute(null);

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was changed.");
        }

        /// <summary>
        /// A test for add new country supplier using a new instance of CountrySupplierViewModel into a window
        /// and the add country is with success.
        /// </summary>
        [TestMethod]
        public void AddSupplierWithSuccessTest()
        {
            var unitsService = new UnitsServiceMock();
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = null;
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(countrySupplierViewModel, "CreateCountrySupplierViewTemplate", true, false))
                .Callback(() =>
                {
                    countrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
                    countrySupplierViewModel.SaveCommand.Execute(null);
                    newSupplier = countrySupplierViewModel.Model;
                });

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel);

            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowServiceMock.Object, compositionContainer) { DataSourceManager = dataManager };
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.AddSupplierCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.CountryStates.Contains(newSupplier));
            Assert.AreEqual(newSupplier.Country, newCountry);
        }

        /// <summary>
        /// A test for add new country supplier using a new instance of CountrySupplierViewModel into a window
        /// and the add country is canceled.
        /// </summary>
        [TestMethod]
        public void AddSupplierWithCancelTest()
        {
            var unitsService = new UnitsServiceMock();
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();

            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(countryViewModel, "CreateCountryViewTemplate", true, false))
                .Callback(() =>
                {
                    // Trigger the release of the Model property value.
                    countryViewModel.OnUnloading();
                });
            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel);

            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowServiceMock.Object, compositionContainer) { DataSourceManager = dataManager };
            manageCountriesViewModel.AddCountryCommand.Execute(null);

            Assert.IsFalse(manageCountriesViewModel.Countries.Contains(newCountry));
        }

        /// <summary>
        /// A test for add new country supplier using a new instance of CountrySupplierViewModel into a window
        /// and the add country is failed because the country is null.
        /// </summary>
        [TestMethod]
        public void AddSupplierWithNullCountryTest()
        {
            var unitsService = new UnitsServiceMock();
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageDialogService = new MessageDialogServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();

            countrySupplierViewModel.DataSourceManager = dataManager;

            CountryState newSupplier = null;
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock.Setup(service => service.ShowViewInDialog(countrySupplierViewModel, "CreateCountrySupplierViewTemplate", true, false))
                .Callback(() =>
                {
                    countrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
                    newSupplier = countrySupplierViewModel.Model;
                });

            var compositionContainer = new CompositionContainer();
            compositionContainer.ComposeParts(messageDialogService, countryViewModel, countrySupplierViewModel);

            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowServiceMock.Object, compositionContainer) { DataSourceManager = dataManager };
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = null;
            manageCountriesViewModel.AddSupplierCommand.Execute(null);

            Assert.IsFalse(manageCountriesViewModel.CountryStates.Contains(newSupplier));
        }

        /// <summary>
        /// A test for delete command on a supplier from database
        /// and the user confirms the delete action when confirmation message appears
        /// </summary>
        [TestMethod]
        public void DeleteSupplierWithSuccesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            manageCountriesViewModel.CountryStates.Add(newSupplier);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            ICommand deleteCommand = manageCountriesViewModel.DeleteSupplierCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            deleteCommand.Execute(null);

            Assert.IsFalse(manageCountriesViewModel.CountryStates.Contains(newSupplier));
        }

        /// <summary>
        /// A test for delete command on a supplier from database
        /// and the user does not confirm the delete action when confirmation message appears
        /// </summary>
        [TestMethod]
        public void DeleteSupplierAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            manageCountriesViewModel.CountryStates.Add(newSupplier);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            ICommand deleteCommand = manageCountriesViewModel.DeleteSupplierCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            deleteCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.CountryStates.Contains(newSupplier));
        }

        /// <summary>
        /// A test for delete command on a supplier from database
        /// and the selected supplier to delete is null
        /// </summary>
        [TestMethod]
        public void DeleteSupplierWithNullParameterTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.Countries.Add(newCountry);
            manageCountriesViewModel.SelectedCountry = newCountry;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            manageCountriesViewModel.CountryStates.Add(newSupplier);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = null;

            ICommand deleteCommand = manageCountriesViewModel.DeleteSupplierCommand;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            deleteCommand.Execute(null);

            Assert.IsTrue(manageCountriesViewModel.CountryStates.Contains(newSupplier));
        }

        /// <summary>
        /// A test for control moved from the suppliers main area
        /// The manageCountriesViewModel is changed, its input is valid
        /// and the user saves the model differences
        /// </summary>
        [TestMethod]
        public void OnUnloadingSupplierVMIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            manageCountriesViewModel.Countries.Add(newCountry);

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.CountryStates.Add(newSupplier);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            manageCountriesViewModel.OnUnloading();

            Assert.AreEqual(newName, newSupplier.Name, "The country was not modified");
        }

        /// <summary>
        /// A test for control moved from the suppliers main area
        /// The manageCountriesViewModel is changed, its input is valid
        /// and the user does not save the model differences
        /// </summary>
        [TestMethod]
        public void OnUnloadingSupplierVMIsChangedAbortedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            manageCountriesViewModel.DataSourceManager = dataManager;

            Country newCountry = NewCountry();
            manageCountriesViewModel.Countries.Add(newCountry);

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.CountryStates.Add(newSupplier);

            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.SelectedCountrySupplier = newSupplier;

            var newName = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = newName;

            manageCountriesViewModel.CountrySupplierViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;
            manageCountriesViewModel.OnUnloading();

            Assert.AreNotEqual(newName, manageCountriesViewModel.SelectedCountrySupplier.Name, "The supplier was modified");
        }

        #endregion

        /// <summary>
        /// A test for control moved from the suppliers main area
        /// The manageCountriesViewModel is changed and its input is not valid
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountryAndSupplierIsChangedTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();
            countryViewModel.DataSourceManager = dataManager;
            countryViewModel.Model = newCountry;

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();
            countrySupplierViewModel.DataSourceManager = dataManager;
            countrySupplierViewModel.Model = newSupplier;

            countryViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            countrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            manageCountriesViewModel.IsChanged = true;
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var unloadingStatus = manageCountriesViewModel.OnUnloading();
            Assert.IsTrue(unloadingStatus);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The manageCountriesViewModel is changed, the country name is valid, the supplier name is null,
        /// the response in MessageDialog is Yes, but no data is saved
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountryAndSupplierIsChangedNullSupplierResultYesTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.CountryViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            manageCountriesViewModel.CountrySupplierViewModel.Name.Value = string.Empty;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;

            var unloadingStatus = manageCountriesViewModel.OnUnloading();
            Assert.IsFalse(unloadingStatus);
        }

        /// <summary>
        /// A test for control moved from the main area
        /// The manageCountriesViewModel is changed, the country name is null, the supplier name is valid,
        /// the response in MessageDialog is No, so no data is saved
        /// </summary>
        [TestMethod]
        public void OnUnloadingCountryAndSupplierIsChangedNullCountryResultNoTest()
        {
            MessageDialogServiceMock messageBoxService;
            WindowServiceMock windowService;
            IUnitsService unitsService;
            IMessenger messenger = new Messenger();
            var compositionContainer = this.SetupCompositionContainer(out messageBoxService, out windowService, out unitsService);
            CountryViewModel countryViewModel = new CountryViewModel(new CountrySettingsViewModel(unitsService), windowService);
            CountrySupplierViewModel countrySupplierViewModel = new CountrySupplierViewModel(new CountrySettingsViewModel(unitsService), windowService);
            ManageCountriesViewModel manageCountriesViewModel = new ManageCountriesViewModel(countryViewModel, countrySupplierViewModel, messenger, windowService, compositionContainer);

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            Country newCountry = NewCountry();
            dataManager.CountryRepository.Add(newCountry);
            dataManager.SaveChanges();

            CountryState newSupplier = new CountryState();
            newSupplier.CountrySettings = newCountry.CountrySetting.Copy();
            newCountry.States.Add(newSupplier);
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            dataManager.CountrySupplierRepository.Add(newSupplier);
            dataManager.SaveChanges();

            manageCountriesViewModel.DataSourceManager = dataManager;
            manageCountriesViewModel.SelectedCountry = newCountry;
            manageCountriesViewModel.CountryViewModel.Name.Value = string.Empty;
            manageCountriesViewModel.CountrySupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.No;

            var unloadingStatus = manageCountriesViewModel.OnUnloading();
            Assert.IsTrue(unloadingStatus);
        }
    }
}
