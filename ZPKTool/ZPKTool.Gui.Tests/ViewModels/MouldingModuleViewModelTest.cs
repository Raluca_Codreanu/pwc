﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MouldingModuleViewModel and is intended to contain unit tests for all commands from MouldingModuleViewModel.
    /// </summary>
    [TestClass]
    public class MouldingModuleViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// You can use the following additional attributes as you write your tests:
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run.
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion

        #region Test methods

        /// <summary>
        /// A test for validating the Inertial Mould Pressure value depending on the selected Main Material.
        /// </summary>
        [TestMethod]
        public void ValidateInertialMouldPressure()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.YearlyProductionQuantity = 1000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.ParentWeight = 100;

            // Add the polymer classification to the raw material.
            var polymerClassification = new MaterialsClassification();
            polymerClassification.Guid = new Guid("f32882ee-c299-4991-9d6c-27449367aae6");

            material.MaterialsClassificationL1 = polymerClassification;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            // Select all the materials and validate that the inertial mould pressure has the same value with the selected main material's default pressure.
            foreach (var rawMaterial in mouldingVM.MatchingMaterials)
            {
                mouldingVM.SelectedMaterial.Value = rawMaterial;
                Assert.AreEqual<decimal>(mouldingVM.SelectedMaterial.Value.DefaultPressure, mouldingVM.ProposedInertialMouldPressure.Value.GetValueOrDefault(), "Wrong 'ProposedInertialMouldPressure' value.");
            }

            mouldingVM.SelectedMaterial.Value = null;
            Assert.AreEqual<decimal>(0, mouldingVM.ProposedInertialMouldPressure.Value.GetValueOrDefault(), "Wrong 'ProposedInertialMouldPressure' value.");
        }

        /// <summary>
        /// A test for validating the Resulting Cycle Time value based on the proposed and inserted depending values.
        /// </summary>
        [TestMethod]
        public void ValidateResultingCycleTimeValue()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            // Validate that the Resulting Cycle Time value is the sum of the inputted/proposed depending values.
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedPlaceInsertPartTime.Value = mouldingVM.ProposedPlaceInsertPartTime.Value.HasValue ? mouldingVM.ProposedPlaceInsertPartTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedCloseMouldingDieTime.Value = mouldingVM.ProposedCloseMouldingDieTime.Value.HasValue ? mouldingVM.ProposedCloseMouldingDieTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedCloseSliderTime.Value = mouldingVM.ProposedCloseSliderTime.Value.HasValue ? mouldingVM.ProposedCloseSliderTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedInjectMouldTime.Value = mouldingVM.ProposedInjectMouldTime.Value.HasValue ? mouldingVM.ProposedInjectMouldTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedCoolingTime.Value = mouldingVM.ProposedCoolingTime.Value.HasValue ? mouldingVM.ProposedCoolingTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedPullSliderTime.Value = mouldingVM.ProposedPullSliderTime.Value.HasValue ? mouldingVM.ProposedPullSliderTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedOpenMouldingDieTime.Value = mouldingVM.ProposedOpenMouldingDieTime.Value.HasValue ? mouldingVM.ProposedOpenMouldingDieTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);

            mouldingVM.InputtedPickPartsTime.Value = mouldingVM.ProposedPickPartsTime.Value.HasValue ? mouldingVM.ProposedPickPartsTime.Value + 1m : 1m;
            this.ValidateResultingCycleTime(mouldingVM);
        }

        /// <summary>
        /// A test for validating the selection made on the Moulding Type Window.
        /// </summary>
        [TestMethod]
        public void ValidateMouldingTypeSelections()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            bool propertyChangedRaised = false;

            mouldingVM.MaterialType.PropertyChanged += (s, e) => { propertyChangedRaised = true; };

            var materialType = MouldingMaterialType.ThermoplasticElastomers;
            var materialTypeSelectedCommand = mouldingVM.MaterialTypeSelectedCommand;
            materialTypeSelectedCommand.Execute(materialType);

            Assert.IsTrue(mouldingVM.MaterialType.Value == materialType, "Failed to select the 'Material Type'.");
            Assert.IsTrue(propertyChangedRaised, "Failed to raise a PropertyChanged event.");

            propertyChangedRaised = false;

            mouldingVM.QualityDemand.PropertyChanged += (s, e) => { propertyChangedRaised = true; };

            var qualityDemand = MouldingQualityDemand.Mid;
            var qualityDemandSelectedCommand = mouldingVM.QualityDemandSelectedCommand;
            qualityDemandSelectedCommand.Execute(qualityDemand);

            Assert.IsTrue(mouldingVM.QualityDemand.Value == qualityDemand, "Failed to select the 'Quality Demand'.");
            Assert.IsTrue(propertyChangedRaised, "Failed to raise a PropertyChanged event.");

            propertyChangedRaised = false;

            mouldingVM.Technology.PropertyChanged += (s, e) => { propertyChangedRaised = true; };

            var technology = MouldingTechnology.InsertedParts;
            var technologySelectedCommand = mouldingVM.TechnologySelectedCommand;
            technologySelectedCommand.Execute(technology);

            Assert.IsTrue(mouldingVM.Technology.Value == technology, "Failed to select the 'Technology'.");
            Assert.IsTrue(propertyChangedRaised, "Failed to raise a PropertyChanged event.");

            propertyChangedRaised = false;

            mouldingVM.Slider.PropertyChanged += (s, e) => { propertyChangedRaised = true; };

            var slider = MouldingSlider.MechanicalSlider;
            var sliderSelectedCommand = mouldingVM.SliderSelectedCommand;
            sliderSelectedCommand.Execute(slider);

            Assert.IsTrue(mouldingVM.Slider.Value == slider, "Failed to select the 'Slider'.");
            Assert.IsTrue(propertyChangedRaised, "Failed to raise a PropertyChanged event.");
        }

        /// <summary>
        /// A test for validating the Proposed Close Moulding Die Time value depending on the material type, technology and necessary machine force.
        /// </summary>
        [TestMethod]
        public void ValidateProposedCloseMouldingDieTime()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            mouldingVM.MaterialType.Value = MouldingMaterialType.Thermoplastics;
            mouldingVM.Technology.Value = MouldingTechnology.Hotrunner;

            mouldingVM.InputtedNecessaryMachineForce.Value = 200m;
            Assert.AreEqual<decimal>(0.4m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 400m;
            Assert.AreEqual<decimal>(0.6m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 700m;
            Assert.AreEqual<decimal>(0.75m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 2000m;
            Assert.AreEqual<decimal>(1.5m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 8000m;
            Assert.AreEqual<decimal>(2.5m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 15000m;
            Assert.AreEqual<decimal>(4.5m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.Technology.Value = MouldingTechnology.InsertedParts;

            mouldingVM.InputtedNecessaryMachineForce.Value = 200m;
            Assert.AreEqual<decimal>(0.75m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 400m;
            Assert.AreEqual<decimal>(0.85m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 700m;
            Assert.AreEqual<decimal>(1.25m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 2000m;
            Assert.AreEqual<decimal>(2m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 8000m;
            Assert.AreEqual<decimal>(3.5m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 15000m;
            Assert.AreEqual<decimal>(6m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.MaterialType.Value = MouldingMaterialType.Duroplastics;

            mouldingVM.InputtedNecessaryMachineForce.Value = 200m;
            Assert.AreEqual<decimal>(1.05m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 400m;
            Assert.AreEqual<decimal>(1.25m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 700m;
            Assert.AreEqual<decimal>(1.7m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 2000m;
            Assert.AreEqual<decimal>(4m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 8000m;
            Assert.AreEqual<decimal>(5.5m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");

            mouldingVM.InputtedNecessaryMachineForce.Value = 15000m;
            Assert.AreEqual<decimal>(7m, mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseMouldingDieTime' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Close Slider Time value, depending on the moulding slider type.
        /// </summary>
        [TestMethod]
        public void ValidateProposedCloseSliderTime()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            mouldingVM.Slider.Value = MouldingSlider.HydraulicSlider;
            Assert.AreEqual<decimal>(2m, mouldingVM.ProposedCloseSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseSliderTime' value.");

            mouldingVM.Slider.Value = MouldingSlider.MechanicalSlider;
            Assert.AreEqual<decimal>(0.5m, mouldingVM.ProposedCloseSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseSliderTime' value.");

            mouldingVM.Slider.Value = MouldingSlider.None;
            Assert.AreEqual<decimal>(0m, mouldingVM.ProposedCloseSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedCloseSliderTime' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Pull Slider Time value, depending on the moulding slider type.
        /// </summary>
        [TestMethod]
        public void ValidateProposedPullSliderTime()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            mouldingVM.Slider.Value = MouldingSlider.HydraulicSlider;
            Assert.AreEqual<decimal>(2m, mouldingVM.ProposedPullSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedPullSliderTime' value.");

            mouldingVM.Slider.Value = MouldingSlider.MechanicalSlider;
            Assert.AreEqual<decimal>(0.5m, mouldingVM.ProposedPullSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedPullSliderTime' value.");

            mouldingVM.Slider.Value = MouldingSlider.None;
            Assert.AreEqual<decimal>(0m, mouldingVM.ProposedPullSliderTime.Value.GetValueOrDefault(), "Wrong 'ProposedPullSliderTime' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities0()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(0, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities1()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.1m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(1, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities2()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.05m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(2, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities4()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.02m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(4, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities9()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.01m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(9, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities16()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.006m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(16, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities25()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.003m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(25, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities36()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.0025m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(36, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities49()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.0017m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(49, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities64()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.0015m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(64, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities81()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.0012m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(81, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for validating the Proposed Number of Cavities value, depending on the parent part and part's raw materials.
        /// </summary>
        [TestMethod]
        public void ValidateProposedNumberOfCavities100()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.YearlyProductionQuantity = 100000;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var material = new RawMaterial();
            material.ParentWeight = 0.0005m;

            part.RawMaterials.Add(material);

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            Assert.AreEqual<int>(100, mouldingVM.ProposedNumberOfCavities.Value.GetValueOrDefault(), "Wrong 'ProposedNumberOfCavities' value.");
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 350.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo350()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 350m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 450.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo450()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 450m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 650.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo650()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 650m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 900.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo900()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 900m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 1100.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo1100()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 1100m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 1500.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo1500()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 1500m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 1800.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo1800()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 1800m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 2400.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo2400()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 2400m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 3000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo3000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 3000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 4000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo4000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 4000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 5000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo5000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 5000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 6500.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo6500()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 6500m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 8000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo8000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 8000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 10000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo10000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 10000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 11000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo11000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 11000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 13000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo13000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 13000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 15000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo15000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 15000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 17000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo17000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 17000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 20000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo20000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 20000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 23000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo23000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 23000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 25000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo25000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 25000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 27000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo27000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 27000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 30000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo30000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 30000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 32000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo32000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 32000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 36000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo36000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 36000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 40000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo40000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 40000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 50000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo50000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 50000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force largen then 50000.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceLargenThen50000()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;
            mouldingVM.InputtedNecessaryMachineForce.Value = 55000m;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);

            // Validate the created steps.
            this.ValidateMaterialConditioningStep(mouldingVM);
            this.ValidateInjectionMouldingStep(mouldingVM);
        }

        /// <summary>
        /// A test for moulding creation with a machine force up to 350.
        /// </summary>
        [TestMethod]
        public void CreateMouldingWithMachineForceUpTo()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var windowService = new WindowServiceMock(messageDialogService, new DispatcherServiceMock());
            var onlineChecker = new OnlineCheckServiceMock();
            var navigationService = new MouldingModuleNavigationService();
            var compositionContainer = new CompositionContainer();

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var part = new Part();
            part.Name = part.Guid.ToString();
            part.IsMasterData = true;
            part.Process = new Process();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            var mouldingVM = new MouldingModuleViewModel(compositionContainer, windowService, onlineChecker, new UnitsServiceMock());
            mouldingVM.NavigationService = navigationService;
            mouldingVM.ParentPart = part;
            mouldingVM.PartDataSourceManager = dsm;

            var finishCommand = mouldingVM.FinishCommand;
            finishCommand.Execute(null);
        }

        #endregion Test methods

        #region Helpers

        /// <summary>
        /// A method that checks if the Material Conditioning step was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        private void ValidateMaterialConditioningStep(MouldingModuleViewModel mouldingVM)
        {
            var step = mouldingVM.CreatedSteps.FirstOrDefault(s => s.Name == LocalizedResources.Moulding_MaterialConditioning);
            Assert.IsNotNull(step, "Failed to generate the " + LocalizedResources.Moulding_MaterialConditioning + " step.");

            var resultingCycleTime = mouldingVM.InputtedResultingCycleTime.Value.HasValue ? mouldingVM.InputtedResultingCycleTime.Value.Value : mouldingVM.ProposedResultingCycleTime.Value.GetValueOrDefault();

            Assert.AreEqual<decimal>(resultingCycleTime, step.CycleTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'CycleTime' value.");
            Assert.AreEqual<decimal>(resultingCycleTime, step.ProcessTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'ProcessTime' value.");
            Assert.AreEqual<int>(1, step.PartsPerCycle.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'PartsPerCycle' value.");
            Assert.AreEqual(ProcessCalculationAccuracy.Calculated, step.Accuracy.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'Accuracy' value.");
            Assert.AreEqual<decimal>(0.5m, step.ProductionSkilledLabour.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'ProductionSkilledLabour' value.");

            // Validate the created step machines.
            this.ValidateBlenderMixerMachine(mouldingVM, step);
            this.ValidateCoolerMachine(mouldingVM, step);
            this.ValidateDryerMachine(mouldingVM, step);
        }

        /// <summary>
        /// A method that checks if the Injection Moulding step was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        private void ValidateInjectionMouldingStep(MouldingModuleViewModel mouldingVM)
        {
            var step = mouldingVM.CreatedSteps.FirstOrDefault(s => s.Name == LocalizedResources.Moulding_InjectionMoulding);
            Assert.IsNotNull(step, "Failed to generate the " + LocalizedResources.Moulding_InjectionMoulding + " step.");

            var resultingCycleTime = mouldingVM.InputtedResultingCycleTime.Value.HasValue ? mouldingVM.InputtedResultingCycleTime.Value.Value : mouldingVM.ProposedResultingCycleTime.Value.GetValueOrDefault();

            Assert.AreEqual<decimal>(resultingCycleTime, step.CycleTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'CycleTime' value.");
            Assert.AreEqual<decimal>(resultingCycleTime, step.ProcessTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'ProcessTime' value.");
            Assert.AreEqual<int>(1, step.PartsPerCycle.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'PartsPerCycle' value.");
            Assert.AreEqual(ProcessCalculationAccuracy.Calculated, step.Accuracy.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'Accuracy' value.");
            Assert.AreEqual<decimal>(3600m, step.SetupTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'SetupTime' value.");
            Assert.AreEqual<decimal>(3600m, step.MaxDownTime.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'MaxDownTime' value.");
            Assert.AreEqual<decimal>(1m, step.SetupSkilledLabour.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'SetupSkilledLabour' value.");
            Assert.AreEqual<decimal>(0.5m, step.ProductionSkilledLabour.GetValueOrDefault(), "'" + LocalizedResources.Moulding_MaterialConditioning + "' step -> Wrong 'ProductionSkilledLabour' value.");

            // Validate the created step machines.
            this.ValidateInjectionMouldingMachine(mouldingVM, step);
            this.ValidateHandlingRobotMachine(mouldingVM, step);
        }

        /// <summary>
        /// A method that checks if the Blender mixer machine was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        /// <param name="step">The process step that contains the created machines.</param>
        private void ValidateBlenderMixerMachine(MouldingModuleViewModel mouldingVM, ProcessStep step)
        {
            var machineForce = mouldingVM.InputtedNecessaryMachineForce.Value.HasValue ? mouldingVM.InputtedNecessaryMachineForce.Value.Value : mouldingVM.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            // Depending on the resulting machine force check if the right machine was created.
            if (machineForce <= 900)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Gravimax GMX 14V");
                Assert.IsNotNull(machine, "Failed to create the 'Gravimax GMX 14V' machine.");
            }
            else if (machineForce <= 5000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Gravimax GMX 94");
                Assert.IsNotNull(machine, "Failed to create the 'Gravimax GMX 94' machine.");
            }
            else if (machineForce <= 10000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Gravimax GMX 184");
                Assert.IsNotNull(machine, "Failed to create the 'Gravimax GMX 184' machine.");
            }
            else
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Gravimax GMX 274");
                Assert.IsNotNull(machine, "Failed to create the 'Gravimax GMX 274' machine.");
            }
        }

        /// <summary>
        /// A method that checks if the Cooler machine was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        /// <param name="step">The process step that contains the created machines.</param>
        private void ValidateCoolerMachine(MouldingModuleViewModel mouldingVM, ProcessStep step)
        {
            var machineForce = mouldingVM.InputtedNecessaryMachineForce.Value.HasValue ? mouldingVM.InputtedNecessaryMachineForce.Value.Value : mouldingVM.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            // Depending on the resulting machine force check if the right machine was created.
            if (machineForce <= 900)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Coolmax  30");
                Assert.IsNotNull(machine, "Failed to create the 'Coolmax  30' machine.");
            }
            else if (machineForce <= 5000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Coolmax  70");
                Assert.IsNotNull(machine, "Failed to create the 'Coolmax  70' machine.");
            }
            else if (machineForce <= 10000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Coolmax  100");
                Assert.IsNotNull(machine, "Failed to create the 'Coolmax  100' machine.");
            }
            else
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Coolmax  250");
                Assert.IsNotNull(machine, "Failed to create the 'Coolmax  250' machine.");
            }
        }

        /// <summary>
        /// A method that checks if the Dryer machine was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        /// <param name="step">The process step that contains the created machines.</param>
        private void ValidateDryerMachine(MouldingModuleViewModel mouldingVM, ProcessStep step)
        {
            var machineForce = mouldingVM.InputtedNecessaryMachineForce.Value.HasValue ? mouldingVM.InputtedNecessaryMachineForce.Value.Value : mouldingVM.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            // Depending on the resulting machine force check if the right machine was created.
            if (machineForce <= 900)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Drymax ES40/50");
                Assert.IsNotNull(machine, "Failed to create the 'Drymax ES40/50' machine.");
            }
            else if (machineForce <= 5000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Drymax D60/150");
                Assert.IsNotNull(machine, "Failed to create the 'Drymax D60/150' machine.");
            }
            else if (machineForce <= 10000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Drymax D100/200");
                Assert.IsNotNull(machine, "Failed to create the 'Drymax D100/200' machine.");
            }
            else
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Drymax D100/400");
                Assert.IsNotNull(machine, "Failed to create the 'Drymax D100/400' machine.");
            }
        }

        /// <summary>
        /// A method that checks if the Injection Moulding machine was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        /// <param name="step">The process step that contains the created machines.</param>
        private void ValidateInjectionMouldingMachine(MouldingModuleViewModel mouldingVM, ProcessStep step)
        {
            var machineForce = mouldingVM.InputtedNecessaryMachineForce.Value.HasValue ? mouldingVM.InputtedNecessaryMachineForce.Value.Value : mouldingVM.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            // Depending on the resulting machine force check if the right machine was created.
            if (machineForce <= 350)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Plus 35");
                Assert.IsNotNull(machine, "Failed to create the 'Plus 35' machine.");
            }
            else if (machineForce <= 450)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM45 / 210");
                Assert.IsNotNull(machine, "Failed to create the 'HM45 / 210' machine.");
            }
            else if (machineForce <= 650)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM65 / 350");
                Assert.IsNotNull(machine, "Failed to create the 'HM65 / 350' machine.");
            }
            else if (machineForce <= 900)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM90 / 525");
                Assert.IsNotNull(machine, "Failed to create the 'HM90 / 525' machine.");
            }
            else if (machineForce <= 1100)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM110 / 750");
                Assert.IsNotNull(machine, "Failed to create the 'HM110 / 750' machine.");
            }
            else if (machineForce <= 1500)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM150 / 1000");
                Assert.IsNotNull(machine, "Failed to create the 'HM150 / 1000' machine.");
            }
            else if (machineForce <= 1800)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM180 / 1330");
                Assert.IsNotNull(machine, "Failed to create the 'HM180 / 1330' machine.");
            }
            else if (machineForce <= 2400)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM240 / 1330");
                Assert.IsNotNull(machine, "Failed to create the 'HM240 / 1330' machine.");
            }
            else if (machineForce <= 3000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM300 / 2250");
                Assert.IsNotNull(machine, "Failed to create the 'HM300 / 2250' machine.");
            }
            else if (machineForce <= 4000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM400 / 5100");
                Assert.IsNotNull(machine, "Failed to create the 'HM400 / 5100' machine.");
            }
            else if (machineForce <= 5000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM500 / 5100");
                Assert.IsNotNull(machine, "Failed to create the 'HM500 / 5100' machine.");
            }
            else if (machineForce <= 6500)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "HM600 / 8800");
                Assert.IsNotNull(machine, "Failed to create the 'HM600 / 8800' machine.");
            }
            else if (machineForce <= 8000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Macro Power 800 / 8800");
                Assert.IsNotNull(machine, "Failed to create the 'Macro Power 800 / 8800' machine.");
            }
            else if (machineForce <= 10000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Macro Power 1000 / 13800");
                Assert.IsNotNull(machine, "Failed to create the 'Macro Power 1000 / 13800' machine.");
            }
            else if (machineForce <= 11000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 7050/1100");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 7050/1100' machine.");
            }
            else if (machineForce <= 13000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 11050/1300");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 11050/1300' machine.");
            }
            else if (machineForce <= 15000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 11050/1500");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 11050/1500' machine.");
            }
            else if (machineForce <= 17000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 11050/1700");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 11050/1700' machine.");
            }
            else if (machineForce <= 20000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 16050/2000");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 16050/2000' machine.");
            }
            else if (machineForce <= 23000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 16050/2300");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 16050/2300' machine.");
            }
            else if (machineForce <= 25000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 23050/2500");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 23050/2500' machine.");
            }
            else if (machineForce <= 27000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 23050/2700");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 23050/2700' machine.");
            }
            else if (machineForce <= 30000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 23050/3000");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 23050/3000' machine.");
            }
            else if (machineForce <= 32000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 23050/3200");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 23050/3200' machine.");
            }
            else if (machineForce <= 36000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 35050/3600");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 35050/3600' machine.");
            }
            else if (machineForce <= 40000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 35050/4000");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 35050/4000' machine.");
            }
            else if (machineForce <= 50000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 45050/5000");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 45050/5000' machine.");
            }
            else
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "Duo 45050/5500");
                Assert.IsNotNull(machine, "Failed to create the 'Duo 45050/5500' machine.");
            }
        }

        /// <summary>
        /// A method that checks if the Handling Robot machine was created properly.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        /// <param name="step">The process step that contains the created machines.</param>
        private void ValidateHandlingRobotMachine(MouldingModuleViewModel mouldingVM, ProcessStep step)
        {
            var machineForce = mouldingVM.InputtedNecessaryMachineForce.Value.HasValue ? mouldingVM.InputtedNecessaryMachineForce.Value.Value : mouldingVM.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            // Depending on the resulting machine force check if the right machine was created.
            if (machineForce <= 900)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "W801");
                Assert.IsNotNull(machine, "Failed to create the 'W801' machine.");
            }
            else if (machineForce <= 5000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "W811");
                Assert.IsNotNull(machine, "Failed to create the 'W811' machine.");
            }
            else if (machineForce <= 10000)
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "W832");
                Assert.IsNotNull(machine, "Failed to create the 'W832' machine.");
            }
            else
            {
                var machine = step.Machines.FirstOrDefault(m => m.Name == "W873");
                Assert.IsNotNull(machine, "Failed to create the 'W873' machine.");
            }
        }

        /// <summary>
        /// A method that checks the Resulting Cycle Time result.
        /// </summary>
        /// <param name="mouldingVM">The moulding view-model.</param>
        private void ValidateResultingCycleTime(MouldingModuleViewModel mouldingVM)
        {
            var placeInsertPartTime = mouldingVM.InputtedPlaceInsertPartTime.Value.HasValue ? mouldingVM.InputtedPlaceInsertPartTime.Value.Value : mouldingVM.ProposedPlaceInsertPartTime.Value.GetValueOrDefault();
            var closeMouldingDieTime = mouldingVM.InputtedCloseMouldingDieTime.Value.HasValue ? mouldingVM.InputtedCloseMouldingDieTime.Value.Value : mouldingVM.ProposedCloseMouldingDieTime.Value.GetValueOrDefault();
            var closeSliderTime = mouldingVM.InputtedCloseSliderTime.Value.HasValue ? mouldingVM.InputtedCloseSliderTime.Value.Value : mouldingVM.ProposedCloseSliderTime.Value.GetValueOrDefault();
            var injectMouldTime = mouldingVM.InputtedInjectMouldTime.Value.HasValue ? mouldingVM.InputtedInjectMouldTime.Value.Value : mouldingVM.ProposedInjectMouldTime.Value.GetValueOrDefault();
            var coolingTime = mouldingVM.InputtedCoolingTime.Value.HasValue ? mouldingVM.InputtedCoolingTime.Value.Value : mouldingVM.ProposedCoolingTime.Value.GetValueOrDefault();
            var pullSliderTime = mouldingVM.InputtedPullSliderTime.Value.HasValue ? mouldingVM.InputtedPullSliderTime.Value.Value : mouldingVM.ProposedPullSliderTime.Value.GetValueOrDefault();
            var openMouldingDieTime = mouldingVM.InputtedOpenMouldingDieTime.Value.HasValue ? mouldingVM.InputtedOpenMouldingDieTime.Value.Value : mouldingVM.ProposedOpenMouldingDieTime.Value.GetValueOrDefault();
            var pickPartsTime = mouldingVM.InputtedPickPartsTime.Value.HasValue ? mouldingVM.InputtedPickPartsTime.Value.Value : mouldingVM.ProposedPickPartsTime.Value.GetValueOrDefault();

            var proposedResultingCycleTime = placeInsertPartTime + closeMouldingDieTime + closeSliderTime + injectMouldTime + coolingTime + pullSliderTime + openMouldingDieTime + pickPartsTime;

            Assert.AreEqual<decimal>(proposedResultingCycleTime, mouldingVM.ProposedResultingCycleTime.Value.GetValueOrDefault(), "Wrong 'ProposedResultingCycleTime' value.");
        }

        #endregion Helpers
    }

    #region Inner Classes

    /// <summary>
    /// The service that navigates between the Moulding Wizard steps.
    /// </summary>
    [Export(typeof(IMouldingModuleNavigationService))]
    public class MouldingModuleNavigationServiceMock : IMouldingModuleNavigationService
    {
        /// <summary>
        /// Starts the moulding wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The moulding wizard's view model (necessary to be set as the data context of the moulding wizard views).</param>
        public void StartWizard(MouldingModuleViewModel viewModel)
        {
            viewModel.FinishCommand.Execute(null);
        }

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        public void CloseWizard()
        {
        }

        /// <summary>
        /// Navigates to the next moulding step.
        /// </summary>
        public void GoToNextStep()
        {
        }

        /// <summary>
        /// Navigates to the previous moulding step.
        /// </summary>
        public void GoToPreviousStep()
        {
        }
    }

    #endregion InnerClasses
}