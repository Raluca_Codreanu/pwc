﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.DataAccess;
using ZPKTool.Business;
using System.Windows.Input;

namespace PROimpens.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  MassUpdateShiftInformationViewModel and is intended 
    /// to contain unit tests for all public methods from  MassUpdateShiftInformationViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateShiftInformationViewModelTest
    {
        /// <summary>
        /// The UI composition framework bootstrapper.
        /// </summary>
        private Bootstrapper bootstrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateShiftInformationViewModelTest"/> class.
        /// </summary>
        public MassUpdateShiftInformationViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for UpdateCommand - cancelling the update.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            Random random = new Random();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.ShiftsPerWeek = random.Next(1000);
            step.HoursPerShift = random.Next(1000);
            step.ProductionWeeksPerYear = random.Next(1000);
            step.ProductionDaysPerWeek = random.Next(1000);
            part.Process.Steps.Add(step);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = part;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "No" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.No;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the updates were not applied
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            ShiftInformationAreEqual(dbPart, partClone);
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> ProcessStep and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Random random = new Random();
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.ShiftsPerWeek = random.Next(1000);
            step.HoursPerShift = random.Next(1000);
            step.ProductionWeeksPerYear = random.Next(1000);
            step.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = step;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.Context.ProcessStepSet.FirstOrDefault(s => s.Guid == step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> ProcessStep and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Random random = new Random();
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.ShiftsPerWeek = random.Next(1000);
            step.HoursPerShift = random.Next(1000);
            step.ProductionWeeksPerYear = random.Next(1000);
            step.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = step;
            massUpdateShiftInformationViewModel.ShiftsPerWeekIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.HoursPerShiftIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekIncreasePercentage = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.Context.ProcessStepSet.FirstOrDefault(s => s.Guid == step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> ProcessStep and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest4()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Random random = new Random();
            Process process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.ShiftsPerWeek = random.Next(1000);
            step.HoursPerShift = random.Next(1000);
            step.ProductionWeeksPerYear = random.Next(1000);
            step.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            ProcessStep stepClone = cloneManager.Clone(step, null);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = step;

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            ProcessStep dbStep = dataContext2.Context.ProcessStepSet.FirstOrDefault(s => s.Guid == step.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> Process and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest5()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = process;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (ProcessStep dbStep in dbPart.Process.Steps)
            {
                ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == dbStep.Name);
                ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> Process and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest6()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = process;
            massUpdateShiftInformationViewModel.ShiftsPerWeekIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.HoursPerShiftIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekIncreasePercentage = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (ProcessStep dbStep in dbPart.Process.Steps)
            {
                ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == dbStep.Name);
                ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following test data were used:
        /// Current object -> Process and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateInformationTest7()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = process;

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            foreach (ProcessStep dbStep in dbPart.Process.Steps)
            {
                ProcessStep stepClone = partClone.Process.Steps.FirstOrDefault(s => s.Name == dbStep.Name);
                ProcessStepUpdated(stepClone, dbStep, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);
            }

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateInformationTest8()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = part;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are expressed in percentages.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest9()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = part;
            massUpdateShiftInformationViewModel.ShiftsPerWeekIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.HoursPerShiftIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekIncreasePercentage = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Part and the new values are set to null.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest10()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            Process process = new Process();
            part.Process = process;

            // Set up the current object
            Random random = new Random();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            process.Steps.Add(step2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Part partClone = cloneManager.Clone(part);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = part;

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            PartUpdated(partClone, dbPart, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are set to valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest11()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.ShiftsPerWeek = random.Next(1000);
            step3.HoursPerShift = random.Next(1000);
            step3.ProductionWeeksPerYear = random.Next(1000);
            step3.ProductionDaysPerWeek = random.Next(1000);
            subassembly2.Process = new Process();
            subassembly2.Process.Steps.Add(step3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            step4.ShiftsPerWeek = random.Next(1000);
            step4.HoursPerShift = random.Next(1000);
            step4.ProductionWeeksPerYear = random.Next(1000);
            step4.ProductionDaysPerWeek = random.Next(1000);
            part1.Process = new Process();
            part1.Process.Steps.Add(step4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            step5.ShiftsPerWeek = random.Next(1000);
            step5.HoursPerShift = random.Next(1000);
            step5.ProductionWeeksPerYear = random.Next(1000);
            step5.ProductionDaysPerWeek = random.Next(1000);
            part2.Process = new Process();
            part2.Process.Steps.Add(step5);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateShiftInformationViewModel.CurrentObject = assembly;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are expressed in percentages and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest12()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.ShiftsPerWeek = random.Next(1000);
            step3.HoursPerShift = random.Next(1000);
            step3.ProductionWeeksPerYear = random.Next(1000);
            step3.ProductionDaysPerWeek = random.Next(1000);
            subassembly2.Process = new Process();
            subassembly2.Process.Steps.Add(step3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            step4.ShiftsPerWeek = random.Next(1000);
            step4.HoursPerShift = random.Next(1000);
            step4.ProductionWeeksPerYear = random.Next(1000);
            step4.ProductionDaysPerWeek = random.Next(1000);
            part1.Process = new Process();
            part1.Process.Steps.Add(step4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            step5.ShiftsPerWeek = random.Next(1000);
            step5.HoursPerShift = random.Next(1000);
            step5.ProductionWeeksPerYear = random.Next(1000);
            step5.ProductionDaysPerWeek = random.Next(1000);
            part2.Process = new Process();
            part2.Process.Steps.Add(step5);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = assembly;
            massUpdateShiftInformationViewModel.ShiftsPerWeekIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.HoursPerShiftIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearIncreasePercentage = random.Next(100);
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekIncreasePercentage = random.Next(100);

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Assembly, the new values are set to null and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest13()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Random random = new Random();
            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.ShiftsPerWeek = random.Next(1000);
            step3.HoursPerShift = random.Next(1000);
            step3.ProductionWeeksPerYear = random.Next(1000);
            step3.ProductionDaysPerWeek = random.Next(1000);
            subassembly2.Process = new Process();
            subassembly2.Process.Steps.Add(step3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            step4.ShiftsPerWeek = random.Next(1000);
            step4.HoursPerShift = random.Next(1000);
            step4.ProductionWeeksPerYear = random.Next(1000);
            step4.ProductionDaysPerWeek = random.Next(1000);
            part1.Process = new Process();
            part1.Process.Steps.Add(step4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            step5.ShiftsPerWeek = random.Next(1000);
            step5.HoursPerShift = random.Next(1000);
            step5.ProductionWeeksPerYear = random.Next(1000);
            step5.ProductionDaysPerWeek = random.Next(1000);
            part2.Process = new Process();
            part2.Process.Steps.Add(step5);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Assembly assemblyClone = cloneManager.Clone(assembly);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateShiftInformationViewModel.CurrentObject = assembly;

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Assembly dbAssembly = dataContext2.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            AssemblyUpdated(assemblyClone, dbAssembly, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Project, the new values are set to valid values and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest14()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Random random = new Random();
            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.ShiftsPerWeek = random.Next(1000);
            step1.HoursPerShift = random.Next(1000);
            step1.ProductionWeeksPerYear = random.Next(1000);
            step1.ProductionDaysPerWeek = random.Next(1000);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.ShiftsPerWeek = random.Next(1000);
            step2.HoursPerShift = random.Next(1000);
            step2.ProductionWeeksPerYear = random.Next(1000);
            step2.ProductionDaysPerWeek = random.Next(1000);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step2);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = step3.Guid.ToString();
            step3.ShiftsPerWeek = random.Next(1000);
            step3.HoursPerShift = random.Next(1000);
            step3.ProductionWeeksPerYear = random.Next(1000);
            step3.ProductionDaysPerWeek = random.Next(1000);
            subassembly2.Process = new Process();
            subassembly2.Process.Steps.Add(step3);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            ProcessStep step4 = new PartProcessStep();
            step4.Name = step4.Guid.ToString();
            step4.ShiftsPerWeek = random.Next(1000);
            step4.HoursPerShift = random.Next(1000);
            step4.ProductionWeeksPerYear = random.Next(1000);
            step4.ProductionDaysPerWeek = random.Next(1000);
            part1.Process = new Process();
            part1.Process.Steps.Add(step4);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            ProcessStep step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            step5.ShiftsPerWeek = random.Next(1000);
            step5.HoursPerShift = random.Next(1000);
            step5.ProductionWeeksPerYear = random.Next(1000);
            step5.ProductionDaysPerWeek = random.Next(1000);
            part2.Process = new Process();
            part2.Process.Steps.Add(step5);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Project projectClone = cloneManager.Clone(project);

            // Set up the view model 
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateShiftInformationViewModel.CurrentObject = project;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            int expectedNumberOfUpdatedObjects = 0;
            ProjectUpdated(projectClone, dbProject, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project, the new values are expressed in percentages and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateShiftInformationTest15()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    Random random = new Random();
        //    ProcessStep step1 = new AssemblyProcessStep();
        //    step1.Name = step1.Guid.ToString();
        //    step1.ShiftsPerWeek = random.Next(1000);
        //    step1.HoursPerShift = random.Next(1000);
        //    step1.ProductionWeeksPerYear = random.Next(1000);
        //    step1.ProductionDaysPerWeek = random.Next(1000);
        //    assembly.Process = new Process();
        //    assembly.Process.Steps.Add(step1);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    ProcessStep step2 = new AssemblyProcessStep();
        //    step2.Name = step2.Guid.ToString();
        //    step2.ShiftsPerWeek = random.Next(1000);
        //    step2.HoursPerShift = random.Next(1000);
        //    step2.ProductionWeeksPerYear = random.Next(1000);
        //    step2.ProductionDaysPerWeek = random.Next(1000);
        //    subassembly1.Process = new Process();
        //    subassembly1.Process.Steps.Add(step2);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.OverheadSettings = new OverheadSetting();
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    ProcessStep step3 = new AssemblyProcessStep();
        //    step3.Name = step3.Guid.ToString();
        //    step3.ShiftsPerWeek = random.Next(1000);
        //    step3.HoursPerShift = random.Next(1000);
        //    step3.ProductionWeeksPerYear = random.Next(1000);
        //    step3.ProductionDaysPerWeek = random.Next(1000);
        //    subassembly2.Process = new Process();
        //    subassembly2.Process.Steps.Add(step3);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    subassembly1.Parts.Add(part1);

        //    ProcessStep step4 = new PartProcessStep();
        //    step4.Name = step4.Guid.ToString();
        //    step4.ShiftsPerWeek = random.Next(1000);
        //    step4.HoursPerShift = random.Next(1000);
        //    step4.ProductionWeeksPerYear = random.Next(1000);
        //    step4.ProductionDaysPerWeek = random.Next(1000);
        //    part1.Process = new Process();
        //    part1.Process.Steps.Add(step4);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.OverheadSettings = new OverheadSetting();
        //    part2.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part2);

        //    ProcessStep step5 = new PartProcessStep();
        //    step5.Name = step5.Guid.ToString();
        //    step5.ShiftsPerWeek = random.Next(1000);
        //    step5.HoursPerShift = random.Next(1000);
        //    step5.ProductionWeeksPerYear = random.Next(1000);
        //    step5.ProductionDaysPerWeek = random.Next(1000);
        //    part2.Process = new Process();
        //    part2.Process.Steps.Add(step5);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model 
        //    massUpdateShiftInformationViewModel.DataContext = dataContext1;
        //    massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateShiftInformationViewModel.CurrentObject = project;
        //    massUpdateShiftInformationViewModel.ShiftsPerWeekIncreasePercentage = random.Next(100);
        //    massUpdateShiftInformationViewModel.HoursPerShiftIncreasePercentage = random.Next(100);
        //    massUpdateShiftInformationViewModel.ProductionWeeksPerYearIncreasePercentage = random.Next(100);
        //    massUpdateShiftInformationViewModel.ProductionDaysPerWeekIncreasePercentage = random.Next(100);

        //    // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
        //    windowService.MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object -> Project, the new values are set to null and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void UpdateShiftInformationTest16()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    Random random = new Random();
        //    ProcessStep step1 = new AssemblyProcessStep();
        //    step1.Name = step1.Guid.ToString();
        //    step1.ShiftsPerWeek = random.Next(1000);
        //    step1.HoursPerShift = random.Next(1000);
        //    step1.ProductionWeeksPerYear = random.Next(1000);
        //    step1.ProductionDaysPerWeek = random.Next(1000);
        //    assembly.Process = new Process();
        //    assembly.Process.Steps.Add(step1);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    ProcessStep step2 = new AssemblyProcessStep();
        //    step2.Name = step2.Guid.ToString();
        //    step2.ShiftsPerWeek = random.Next(1000);
        //    step2.HoursPerShift = random.Next(1000);
        //    step2.ProductionWeeksPerYear = random.Next(1000);
        //    step2.ProductionDaysPerWeek = random.Next(1000);
        //    subassembly1.Process = new Process();
        //    subassembly1.Process.Steps.Add(step2);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.OverheadSettings = new OverheadSetting();
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    ProcessStep step3 = new AssemblyProcessStep();
        //    step3.Name = step3.Guid.ToString();
        //    step3.ShiftsPerWeek = random.Next(1000);
        //    step3.HoursPerShift = random.Next(1000);
        //    step3.ProductionWeeksPerYear = random.Next(1000);
        //    step3.ProductionDaysPerWeek = random.Next(1000);
        //    subassembly2.Process = new Process();
        //    subassembly2.Process.Steps.Add(step3);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    subassembly1.Parts.Add(part1);

        //    ProcessStep step4 = new PartProcessStep();
        //    step4.Name = step4.Guid.ToString();
        //    step4.ShiftsPerWeek = random.Next(1000);
        //    step4.HoursPerShift = random.Next(1000);
        //    step4.ProductionWeeksPerYear = random.Next(1000);
        //    step4.ProductionDaysPerWeek = random.Next(1000);
        //    part1.Process = new Process();
        //    part1.Process.Steps.Add(step4);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.OverheadSettings = new OverheadSetting();
        //    part2.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part2);

        //    ProcessStep step5 = new PartProcessStep();
        //    step5.Name = step5.Guid.ToString();
        //    step5.ShiftsPerWeek = random.Next(1000);
        //    step5.HoursPerShift = random.Next(1000);
        //    step5.ProductionWeeksPerYear = random.Next(1000);
        //    step5.ProductionDaysPerWeek = random.Next(1000);
        //    part2.Process = new Process();
        //    part2.Process.Steps.Add(step5);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Project projectClone = cloneManager.Clone(project);

        //    // Set up the view model 
        //    massUpdateShiftInformationViewModel.DataContext = dataContext1;
        //    massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateShiftInformationViewModel.CurrentObject = project;

        //    // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
        //    windowService.MessageDialogResult = MessageDialogResult.Yes;
        //    ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
        //    int expectedNumberOfUpdatedObjects = 0;
        //    ProjectUpdated(projectClone, dbProject, massUpdateShiftInformationViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object -> Commodity, the new values are set to valid values and ApplyUpdatesToSubobjects = true.
        /// </summary>
        [TestMethod]
        public void UpdateShiftInformationTest17()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateShiftInformationViewModel massUpdateShiftInformationViewModel = new MassUpdateShiftInformationViewModel(messenger, windowService);

            // Set up the current object
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.CommodityRepository.Add(commodity);
            dataContext1.SaveChanges();

            // Set up the view model 
            Random random = new Random();
            massUpdateShiftInformationViewModel.DataContext = dataContext1;
            massUpdateShiftInformationViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateShiftInformationViewModel.CurrentObject = commodity;
            massUpdateShiftInformationViewModel.ShiftsPerWeekNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.HoursPerShiftNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionWeeksPerYearNewValue = random.Next(1000).ToString();
            massUpdateShiftInformationViewModel.ProductionDaysPerWeekNewValue = random.Next(1000).ToString();

            // Set up the MessageDialogResult to "Yes" in order to cancel the update operation
            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateShiftInformationViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.IsTrue(numberOfUpdatedObjects == 0, "Wrong number of updated objects displayed.");
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verifies if a process step was updated.
        /// </summary>
        /// <param name="oldStep">The old step.</param>
        /// <param name="updatedStep">The updated step.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProcessStepUpdated(ProcessStep oldStep, ProcessStep updatedStep, MassUpdateShiftInformationViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(viewModel.ShiftsPerWeekNewValue))
            {
                Assert.AreEqual<decimal>(Converter.StringToNullableDecimal(viewModel.ShiftsPerWeekNewValue).GetValueOrDefault(), updatedStep.ShiftsPerWeek.GetValueOrDefault(), "Failed to update the ShiftsPerWeek with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ShiftsPerWeekIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.ShiftsPerWeekIncreasePercentage.GetValueOrDefault() * oldStep.ShiftsPerWeek.GetValueOrDefault(), updatedStep.ShiftsPerWeek.GetValueOrDefault(), "Failed to update the ShiftsPerWeek with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                ShiftInformationAreEqual(oldStep, updatedStep);
            }

            if (!string.IsNullOrEmpty(viewModel.HoursPerShiftNewValue))
            {
                Assert.AreEqual<decimal>(Converter.StringToNullableDecimal(viewModel.HoursPerShiftNewValue).GetValueOrDefault(), updatedStep.HoursPerShift.GetValueOrDefault(), "Failed to update HoursPerShift with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.HoursPerShiftIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.HoursPerShiftIncreasePercentage.GetValueOrDefault() * oldStep.HoursPerShift.GetValueOrDefault(), updatedStep.HoursPerShift.GetValueOrDefault(), "Failed to update the HoursPerShift with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                ShiftInformationAreEqual(oldStep, updatedStep);
            }

            if (!string.IsNullOrEmpty(viewModel.ProductionWeeksPerYearNewValue))
            {
                Assert.AreEqual<decimal>(Converter.StringToNullableDecimal(viewModel.ProductionWeeksPerYearNewValue).GetValueOrDefault(), updatedStep.ProductionWeeksPerYear.GetValueOrDefault(), "Failed to update the ProductionWeeksPerYear with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ProductionWeeksPerYearIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProductionWeeksPerYearIncreasePercentage.GetValueOrDefault() * oldStep.ProductionWeeksPerYear.GetValueOrDefault(), updatedStep.ProductionWeeksPerYear.GetValueOrDefault(), "Failed to update the ProductionWeeksPerYear with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                ShiftInformationAreEqual(oldStep, updatedStep);
            }

            if (!string.IsNullOrEmpty(viewModel.ProductionDaysPerWeekNewValue))
            {
                Assert.AreEqual<decimal>(Converter.StringToNullableDecimal(viewModel.ProductionDaysPerWeekNewValue).GetValueOrDefault(), updatedStep.ProductionDaysPerWeek.GetValueOrDefault(), "Failed to update the ProductionDaysPerWeek with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.ProductionDaysPerWeekIncreasePercentage != null)
            {
                Assert.AreEqual<decimal>(viewModel.ProductionDaysPerWeekIncreasePercentage.GetValueOrDefault() * oldStep.ProductionDaysPerWeek.GetValueOrDefault(), updatedStep.ProductionDaysPerWeek.GetValueOrDefault(), "Failed to update the ProductionDaysPerWeek with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                ShiftInformationAreEqual(oldStep, updatedStep);
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verifies if a project was updated.
        /// </summary>
        /// <param name="oldProject">The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void ProjectUpdated(Project oldProject, Project updatedProject, MassUpdateShiftInformationViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    AssemblyUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    ShiftInformationAreEqual(oldPart, part);
                }

                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    ShiftInformationAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verifies if an assembly was updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void AssemblyUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateShiftInformationViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (ProcessStep step in updatedAssembly.Process.Steps)
            {
                ProcessStep oldStep = oldAssembly.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, viewModel, ref numberOfUpdatedObjects);
            }

            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    PartUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    AssemblyUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    ShiftInformationAreEqual(oldPart, part);
                }

                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    ShiftInformationAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verifies if a part was updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated projects.</param>
        private void PartUpdated(Part oldPart, Part updatedPart, MassUpdateShiftInformationViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (ProcessStep step in updatedPart.Process.Steps)
            {
                ProcessStep oldStep = oldPart.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ProcessStepUpdated(oldStep, step, viewModel, ref numberOfUpdatedObjects);
            }
        }

        /// <summary>
        /// Verifies if the shift information of assemblies are equal.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void ShiftInformationAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            foreach (ProcessStep step in comparedAssembly.Process.Steps)
            {
                ProcessStep stepToCompareWith = assemblyToCompareWith.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ShiftInformationAreEqual(step, stepToCompareWith);
            }

            foreach (Part part in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == part.Name);
                ShiftInformationAreEqual(part, partToCompareWith);
            }

            foreach (Assembly subassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                ShiftInformationAreEqual(subassembly, subassemblyToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the shift information of two parts are equal.
        /// </summary>
        /// <param name="comparedPart">The compared part.</param>
        /// <param name="partToCompareWith">The part to compare with.</param>
        private void ShiftInformationAreEqual(Part comparedPart, Part partToCompareWith)
        {
            foreach (ProcessStep step in comparedPart.Process.Steps)
            {
                ProcessStep stepToCompareWith = partToCompareWith.Process.Steps.FirstOrDefault(s => s.Name == step.Name);
                ShiftInformationAreEqual(step, stepToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the shift information of two process steps are equal.
        /// </summary>
        /// <param name="comparedStep">The compared step.</param>
        /// <param name="stepToCompareWith">The step to compare with.</param>
        private void ShiftInformationAreEqual(ProcessStep comparedStep, ProcessStep stepToCompareWith)
        {
            bool result = comparedStep.ShiftsPerWeek.GetValueOrDefault() == stepToCompareWith.ShiftsPerWeek.GetValueOrDefault() &&
                          comparedStep.HoursPerShift.GetValueOrDefault() == stepToCompareWith.HoursPerShift.GetValueOrDefault() &&
                          comparedStep.ProductionWeeksPerYear.GetValueOrDefault() == stepToCompareWith.ProductionWeeksPerYear.GetValueOrDefault() &&
                          comparedStep.ProductionDaysPerWeek.GetValueOrDefault() == stepToCompareWith.ProductionDaysPerWeek.GetValueOrDefault();

            Assert.IsTrue(result, "The ShiftInformation are not equal.");
        }

        #endregion Helpers
    }
}
