﻿namespace ZPKTool.Gui.Tests.ViewModels
{
    using System.ComponentModel.Composition.Hosting;
    using System.Linq;
    using System.Threading;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using ZPKTool.Business;
    using ZPKTool.Common;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Notifications;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;
    using ZPKTool.Gui.Tests.Services;
    using ZPKTool.Gui.ViewModels;
    using ZPKTool.MvvmCore.Services;

    /// <summary>
    /// The unit tests for the Synchronization View-Model class.
    /// </summary>
    [TestClass]
    public class SynchronizationViewModelTest
    {
        #region Attributes

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The synchronization service mock.
        /// </summary>
        private Mock<ISyncService> syncServiceMock;

        /// <summary>
        /// The synchronization manager mock instance.
        /// </summary>
        private SynchronizationManagerMock syncManagerMock;

        /// <summary>
        /// The test context which provides information 
        /// about and functionality for the current test run.
        /// </summary>
        private TestContext testContextInstance;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="SynchronizationViewModelTest"/> class.
        /// </summary>
        public SynchronizationViewModelTest()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// This code will run before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            ZPKTool.Gui.Tests.Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock(
                (MessageDialogServiceMock)messageDialogService,
                (DispatcherServiceMock)dispatcherService);
            this.messenger = new Messenger();
            this.container = new CompositionContainer();
            this.pleaseWaitService = new PleaseWaitService(dispatcherService);
            this.syncServiceMock = new Mock<ISyncService>();
            this.syncManagerMock = new SynchronizationManagerMock();
            this.syncServiceMock.SetupGet(x => x.SyncManager)
                .Returns(syncManagerMock);
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion

        #region Test methods

        /// <summary>
        /// Tests the synchronization command if it was not chosen anything to synchronize.
        /// </summary>
        [TestMethod]
        public void TestSyncCommandWithoutAnyOptionSelected()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            var syncVm = new SynchronizationViewModel(
                this.windowService,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            var syncStarted = false;
            this.messenger.Register<SynchronizationStartedMessage>(
                (msg) =>
                {
                    syncStarted = true;
                });

            syncVm.SynchronizationCommand.Execute(null);

            Assert.IsFalse(syncStarted);
        }

        /// <summary>
        /// Tests the synchronization command.
        /// </summary>
        [TestMethod]
        public void TestSyncCommandWithSuccess()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            var syncVm = new SynchronizationViewModel(
                this.windowService,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            // choose the option to synchronize my projects.
            syncVm.SynchronizeMyProjects.Value = true;

            var syncStarted = false;
            this.messenger.Register<SynchronizationStartedMessage>(
                (msg) =>
                {
                    syncStarted = true;
                });

            syncVm.SynchronizationCommand.Execute(null);

            while (!syncManagerMock.SyncWasCalled)
            {
                Thread.Sleep(50);
            }

            Assert.IsTrue(syncManagerMock.SyncWasCalled);
            Assert.IsTrue(syncStarted);
        }

        /// <summary>
        /// Tests the abort command, when the user confirm the abort operation.
        /// </summary>
        [TestMethod]
        public void TestAbortCommandWithSuccess()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            /* 
             * Setup the MessageDialogService to return 'Yes' 
             * when the user is asked to confirm the abort command.
             */
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock
                .Setup(x => x.MessageDialogService
                    .Show(
                    LocalizedResources.Synchronization_AbortConfirmationMessage,
                    MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);

            var syncVm = new SynchronizationViewModel(
                windowServiceMock.Object,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            /*
             * Synchronize my projects.
             */
            syncVm.SynchronizeMyProjects.Value = true;
            syncVm.SynchronizationCommand.Execute(null);

            /*
             * Do not execute the abort command as long as 
             * the synchronization has not started.
             */
            while (!syncManagerMock.SyncWasCalled)
            {
                Thread.Sleep(50);
            }

            syncVm.AbortCommand.Execute(null);

            Assert.IsTrue(syncManagerMock.AbortWasCalled);
        }

        /// <summary>
        /// Tests the abort command, when the user does not confirm the abort operation.
        /// </summary>
        [TestMethod]
        public void TestCancelAbortCommand()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            /* 
             * Setup the MessageDialogService to return 'No' 
             * when the user is asked to confirm the abort command.
             */
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock
                .Setup(x => x.MessageDialogService
                    .Show(
                    LocalizedResources.Synchronization_AbortConfirmationMessage,
                    MessageDialogType.YesNo))
                .Returns(MessageDialogResult.No);

            var syncVm = new SynchronizationViewModel(
                windowServiceMock.Object,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            /*
             * Synchronize my projects.
             */
            syncVm.SynchronizeMyProjects.Value = true;
            syncVm.SynchronizationCommand.Execute(null);

            /*
             * Do not execute the abort command as long as 
             * the synchronization has not started.
             */
            while (!syncManagerMock.SyncWasCalled)
            {
                Thread.Sleep(50);
            }

            syncVm.AbortCommand.Execute(null);

            Assert.IsFalse(syncManagerMock.AbortWasCalled);
        }

        /// <summary>
        /// Tests the OnUnloading while synchronize is running 
        /// and the user confirm the run in background operation.
        /// </summary>
        [TestMethod]
        public void TestOnUnloadingWhileSyncIsRunningWithSuccess()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            /* 
             * Setup the MessageDialogService to return 'Yes' 
             * when the user is asked to confirm synchronization run in background.
             */
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock
                .Setup(x => x.MessageDialogService
                    .Show(
                    LocalizedResources.Synchronization_RunInBackgroundConfirmationMessage,
                    MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);

            var syncVm = new SynchronizationViewModel(
                windowServiceMock.Object,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            /*
             * Synchronize my projects.
             */
            syncVm.SynchronizeMyProjects.Value = true;
            syncVm.SynchronizationCommand.Execute(null);

            /*
             * Do not execute the unloading as long as 
             * the synchronization has not started.
             */
            while (!syncManagerMock.SyncWasCalled)
            {
                Thread.Sleep(50);
            }

            var wasUnloaded = syncVm.OnUnloading();

            Assert.IsTrue(wasUnloaded);
        }

        /// <summary>
        /// Tests the OnUnloading while synchronize is running and 
        /// the user does not confirm the run in background operation.
        /// </summary>
        [TestMethod]
        public void TestOnUnloadingAbortedWhileSyncIsRunning()
        {
            var localDataManager =
                DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currentUser = this.CreateNewUser(localDataManager);

            /* 
             * Setup the MessageDialogService to return 'No' 
             * when the user is asked to confirm synchronization run in background.
             */
            var windowServiceMock = new Mock<IWindowService>();
            windowServiceMock
                .Setup(x => x.MessageDialogService
                    .Show(
                    LocalizedResources.Synchronization_RunInBackgroundConfirmationMessage,
                    MessageDialogType.YesNo))
                .Returns(MessageDialogResult.No);

            var syncVm = new SynchronizationViewModel(
                windowServiceMock.Object,
                this.container,
                this.pleaseWaitService,
                this.syncServiceMock.Object,
                this.messenger);

            /*
             * Synchronize my projects.
             */
            syncVm.SynchronizeMyProjects.Value = true;
            syncVm.SynchronizationCommand.Execute(null);

            /*
             * Do not execute the unloading as long as 
             * the synchronization has not started.
             */
            while (!syncManagerMock.SyncWasCalled)
            {
                Thread.Sleep(50);
            }

            var wasUnloaded = syncVm.OnUnloading();

            Assert.IsFalse(wasUnloaded);
        }

        #endregion Test methods

        #region Private methods

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(ZPKTool.DataAccess.IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("password", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "password");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion Private methods
    }
}
