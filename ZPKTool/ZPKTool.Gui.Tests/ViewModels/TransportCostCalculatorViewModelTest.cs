﻿using System;
using System.ComponentModel.Composition.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for TransportCostCalculatorViewModel and is intended 
    /// to contain unit tests for all public methods from TransportCostCalculatorViewModel class.
    /// </summary>
    [TestClass]
    public class TransportCostCalculatorViewModelTest
    {
        #region Attributes

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The please wait service.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculatorViewModelTest"/> class.
        /// </summary>
        public TransportCostCalculatorViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        }

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var messageDialogService = new MessageDialogServiceMock();
            var dispatcherService = new DispatcherServiceMock();

            this.windowService = new WindowServiceMock(messageDialogService, dispatcherService);
            this.messenger = new Messenger();
            this.unitsService = new UnitsServiceMock();
            this.compositionContainer = new CompositionContainer();
            this.pleaseWaitService = new PleaseWaitService(dispatcherService);
        }

        #endregion

        #region Test methods

        /// <summary>
        /// A test for handling the parent entity changed, when a new instance of transport cost calculator is opened and the parent entity set is an assembly.
        /// The assembly has a transport cost calculation object, and the test checks if the calculation is added with the correct values.
        /// </summary>
        [TestMethod]
        public void HandleParentAssemblyChangedTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            // Create the parent assembly.
            Assembly assembly = new Assembly();
            assembly.Name = "Assembly";
            assembly.Number = "22";
            assembly.AssemblingSupplier = "Supplier";
            assembly.AssemblingCountry = "Country";

            Guid countryId = new Guid("0426A877-D986-4E26-B8F9-4A95F2B2FF6B");
            assembly.AssemblingCountryId = countryId;

            TransportCostCalculation transportCostCalculation = new TransportCostCalculation();
            transportCostCalculation.TransportCostCalculationSetting = new TransportCostCalculationSetting();

            assembly.TransportCostCalculations.Add(transportCostCalculation);
            tccViewModel.ParentEntity = assembly;

            Assert.AreEqual(1, tccViewModel.CalculationItems.Count, "The transport cost calculation was not added into calculations list.");
            Assert.AreEqual("Assembly", tccViewModel.CalculationItems[0].PartName.Value, "The calculation's parent name is incorrect.");
            Assert.AreEqual("22", tccViewModel.CalculationItems[0].PartNumber.Value, "The calculation's parent number is incorrect.");
            Assert.AreEqual("Supplier", tccViewModel.CalculationItems[0].Supplier.Value, "The calculation's supplier is incorrect.");
            Assert.AreEqual(null, tccViewModel.CalculationItems[0].PartWeight.Value, "The calculation's parent weight is incorrect.");
            Assert.AreEqual("Country", tccViewModel.CalculationItems[0].PartCountry.Value, "The calculation's country is incorrect.");
            Assert.AreEqual(countryId, tccViewModel.CalculationItems[0].PartCountryId.Value, "The calculation's country id is incorrect.");
        }

        /// <summary>
        /// A test for handling the parent entity changed, when a new instance of transport cost calculator is opened and the parent entity set is a part.
        /// The part has a transport cost calculation object, and the test checks  if the calculation is added with the correct values.
        /// </summary>
        [TestMethod]
        public void HandleParentPartChangedTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            // Create the parent part.
            Part part = new Part();
            part.Name = "Part";
            part.Number = "22";

            // Add values for the raw material weight to get the part weight.
            RawMaterial rawmaterial1 = new RawMaterial();
            rawmaterial1.ParentWeight = 250;

            RawMaterial rawmaterial2 = new RawMaterial();
            rawmaterial2.ParentWeight = 100;

            part.RawMaterials.Add(rawmaterial1);
            part.RawMaterials.Add(rawmaterial2);

            part.ManufacturingSupplier = "Supplier";
            part.ManufacturingCountry = "Country";

            Guid countryId = new Guid("55C6ECB4-BEFD-4533-9C75-13D98B98CE4F");
            part.ManufacturingCountryId = countryId;

            TransportCostCalculation transportCostCalculation = new TransportCostCalculation();
            transportCostCalculation.TransportCostCalculationSetting = new TransportCostCalculationSetting();

            part.TransportCostCalculations.Add(transportCostCalculation);
            tccViewModel.ParentEntity = part;

            Assert.AreEqual(1, tccViewModel.CalculationItems.Count, "The transport cost calculation was not added into calculations list.");
            Assert.AreEqual("Part", tccViewModel.CalculationItems[0].PartName.Value, "The calculation's parent name is incorrect.");
            Assert.AreEqual("22", tccViewModel.CalculationItems[0].PartNumber.Value, "The calculation's parent number is incorrect.");
            Assert.AreEqual("Supplier", tccViewModel.CalculationItems[0].Supplier.Value, "The calculation's supplier is incorrect.");
            Assert.AreEqual(350, tccViewModel.CalculationItems[0].PartWeight.Value, "The calculation's parent weight is incorrect.");
            Assert.AreEqual("Country", tccViewModel.CalculationItems[0].PartCountry.Value, "The calculation's country is incorrect.");
            Assert.AreEqual(countryId, tccViewModel.CalculationItems[0].PartCountryId.Value, "The calculation's country id is incorrect.");
        }

        /// <summary>
        /// This is a test for the AddNewCalculation command, which tests if 2 new calculations are added into calculations list.
        /// There are no transport cost calculation settings into database for the current logged in user, so the calculations' settings must be the default ones.
        /// </summary>
        [TestMethod]
        public void AddNewCalculationsWithoutSettingsForUserIntoDatabaseTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            Assert.AreEqual(2, tccViewModel.CalculationItems.Count, "The number of added calculations is incorrect.");
            Assert.AreEqual(245, tccViewModel.CalculationItems[0].Model.TransportCostCalculationSetting.DaysPerYear, "The days per year value from settings is incorrect.");
            Assert.AreEqual(0.5m, tccViewModel.CalculationItems[1].Model.TransportCostCalculationSetting.TruckTrailerCost, "The truck trailer cost from settings is incorrect.");
        }

        /// <summary>
        /// This is a test for the AddNewCalculation command, which tests if 2 new calculations are added into calculations list.
        /// There is a transport cost calculation setting into the database, so the calculations' settings must have its values.
        /// </summary>
        [TestMethod]
        public void AddNewCalculationsWithExistingSettingsIntoDatabaseTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            var user = this.Login();

            // Create the parent assembly with calculation and calculation settings.
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculation calculation = new TransportCostCalculation();
            calculation.Assembly = assembly;
            calculation.SourceCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculationSetting settings = new TransportCostCalculationSetting();
            settings.AverageLoadTarget = 1.5m;
            settings.DaysPerYear = 200;
            settings.MegaTrailerCost = 30m;
            settings.Truck3_5tCost = 45m;
            settings.Truck7tCost = 50m;
            settings.TruckTrailerCost = 40m;
            settings.VanTypeCost = 25m;
            settings.TransportCostCalculation = calculation;
            settings.Owner = user;

            this.dataManager.TransportCostCalculationSettingRepository.Add(settings);
            this.dataManager.SaveChanges();

            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            Assert.AreEqual(2, tccViewModel.CalculationItems.Count, "The number of added calculations is incorrect.");
            Assert.AreEqual(1.5m, tccViewModel.CalculationItems[0].Model.TransportCostCalculationSetting.AverageLoadTarget, "The average load target value from settings is incorrect.");
            Assert.AreEqual(25m, tccViewModel.CalculationItems[1].Model.TransportCostCalculationSetting.VanTypeCost, "The van type cost from settings is incorrect.");
        }

        /// <summary>
        /// This is a test for the UpdateTotalCosts method, when 3 items are added with values for packaging, packaging for pcs, transport and transport for pcs
        /// costs, and then the total costs from summary view model are calculated.
        /// </summary>
        [TestMethod]
        public void UpdateTotalCostsTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            var user = this.Login();

            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var firstItem = tccViewModel.CalculationItems[0];
            var secondItem = tccViewModel.CalculationItems[1];
            var thirdItem = tccViewModel.CalculationItems[2];

            // Add some cost values for the added items.
            firstItem.PackagingCost.Value = 1000m;
            firstItem.PackagingCostPerPcs.Value = 100m;
            firstItem.TransportCost.Value = 2000m;
            firstItem.TransportCostPerPcs.Value = 200m;

            secondItem.PackagingCost.Value = 3000m;
            secondItem.PackagingCostPerPcs.Value = 300m;
            secondItem.TransportCost.Value = 4000m;
            secondItem.TransportCostPerPcs.Value = 400m;

            thirdItem.PackagingCost.Value = 5000m;
            thirdItem.PackagingCostPerPcs.Value = 500m;
            thirdItem.TransportCost.Value = 6000m;
            thirdItem.TransportCostPerPcs.Value = 600m;

            this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateTotalCosts"));

            Assert.AreEqual(9000m, tccViewModel.SummaryViewModel.TotalPackagingCost.Value, "The total packaging cost is incorrect.");
            Assert.AreEqual(900m, tccViewModel.SummaryViewModel.TotalPackagingCostPerPcs, "The total packaging cost per pieces is incorrect.");
            Assert.AreEqual(12000m, tccViewModel.SummaryViewModel.TotalTransportCost.Value, "The total transport cost is incorrect.");
            Assert.AreEqual(1200m, tccViewModel.SummaryViewModel.TotalTransportCostPerPcs, "The total transport cost per pierces is incorrect.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if it can be executed although there are errors in the SummaryViewModel.
        /// </summary>
        [TestMethod]
        public void CanSaveWithErrorsInSummaryVMTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Set an out of range value for a field.
            tccViewModel.SummaryViewModel.TotalPackagingCostPerPcs = 10000000000000m;

            Assert.IsFalse(tccViewModel.SaveCommand.CanExecute(null), "The save command can be executed although there are errors.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if it can be executed although there are errors in one of the calculation view model items.
        /// </summary>
        [TestMethod]
        public void CanSaveWithErrorsInCalculationVMTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Add a new item that has, by default, some empty required fields.
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            Assert.IsFalse(tccViewModel.SaveCommand.CanExecute(null), "The save command can be executed although there are errors.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if a calculation is attached to its parent part and saved into database.
        /// </summary>
        [TestMethod]
        public void SaveCalculationIntoPartTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            var user = this.Login();

            // Create a part with calculation and settings into db.
            Part part = new Part();
            part.Name = "Part";
            part.Number = "22";

            CountrySetting countrySetting = new CountrySetting();
            part.CountrySettings = countrySetting;

            OverheadSetting overheadSetting = new OverheadSetting();
            part.OverheadSettings = overheadSetting;

            TransportCostCalculation calculation = new TransportCostCalculation();
            calculation.Part = part;
            calculation.SourceCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculationSetting settings = new TransportCostCalculationSetting();
            settings.TransportCostCalculation = calculation;
            settings.Owner = user;

            this.dataManager.TransportCostCalculationSettingRepository.Add(settings);
            this.dataManager.SaveChanges();

            // Add a new calculation attached to the parent part.
            tccViewModel.ParentEntity = part;
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var addedItem = tccViewModel.CalculationItems[1];
            addedItem.SourceCountry.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.SourceLocation.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.SourceZipCode.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationCountry.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationLocation.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationZipCode.Value = EncryptionManager.Instance.GenerateRandomString(14, true);

            tccViewModel.SaveCommand.Execute(null);

            Assert.IsTrue(part.TransportCostCalculations.Contains(addedItem.Model), "The calculation was not attached to the part.");
            Assert.IsNotNull(this.dataManager.TransportCostCalculationRepository.GetByKey(addedItem.Model.Guid), "The calculation was not saved into db.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if a calculation is attached to its parent assembly and saved into database.
        /// </summary>
        [TestMethod]
        public void SaveCalculationIntoAssemblyTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            var user = this.Login();

            // Create an assembly with calculation and settings into db.
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculation calculation = new TransportCostCalculation();
            calculation.Assembly = assembly;
            calculation.SourceCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculationSetting settings = new TransportCostCalculationSetting();
            settings.TransportCostCalculation = calculation;
            settings.Owner = user;

            this.dataManager.TransportCostCalculationSettingRepository.Add(settings);
            this.dataManager.SaveChanges();

            // Add a new calculation attached to the parent assembly.
            tccViewModel.ParentEntity = assembly;
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var addedItem = tccViewModel.CalculationItems[1];
            addedItem.SourceCountry.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.SourceLocation.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.SourceZipCode.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationCountry.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationLocation.Value = EncryptionManager.Instance.GenerateRandomString(14, true);
            addedItem.DestinationZipCode.Value = EncryptionManager.Instance.GenerateRandomString(14, true);

            tccViewModel.SaveCommand.Execute(null);

            Assert.IsTrue(assembly.TransportCostCalculations.Contains(addedItem.Model), "The calculation was not attached to the assembly.");
            Assert.IsNotNull(this.dataManager.TransportCostCalculationRepository.GetByKey(addedItem.Model.Guid), "The calculation was not saved into db.");
        }

        /// <summary>
        /// This is a test for the SaveCommand, to check if a calculation contained by an assembly and deleted meanwhile, it is removed from the database.
        /// </summary>
        [TestMethod]
        public void SaveCommandWithRemoveDeleteCalculationTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            var user = this.Login();

            // Create an assembly with calculation and settings into db.
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculation calculation = new TransportCostCalculation();
            calculation.Assembly = assembly;
            calculation.SourceCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.SourceZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationCountry = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationLocation = EncryptionManager.Instance.GenerateRandomString(14, true);
            calculation.DestinationZipCode = EncryptionManager.Instance.GenerateRandomString(14, true);

            TransportCostCalculationSetting settings = new TransportCostCalculationSetting();
            settings.TransportCostCalculation = calculation;
            settings.Owner = user;

            this.dataManager.TransportCostCalculationSettingRepository.Add(settings);
            this.dataManager.SaveChanges();

            tccViewModel.ParentEntity = assembly;

            // Delete the calculation.
            var itemToDelete = tccViewModel.CalculationItems[0];
            tccViewModel.DeleteCalculationCommand.Execute(itemToDelete);
            tccViewModel.SaveCommand.Execute(null);

            Assert.IsFalse(assembly.TransportCostCalculations.Contains(itemToDelete.Model), "The calculation was not detached from the assembly.");
            Assert.IsNull(this.dataManager.TransportCostCalculationRepository.GetByKey(itemToDelete.Model.Guid), "The calculation was not removed from db.");
        }

        /// <summary>
        /// This is a test for the DeleteCalculation command, to check if the summary view model is deleted from the view models list (only calculation can be deleted).
        /// </summary>
        [TestMethod]
        public void DeleteSummaryViewModelTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            Assert.IsFalse(tccViewModel.DeleteCalculationCommand.CanExecute(tccViewModel.SummaryViewModel), "The SummaryViewModel can be deleted.");

            var viewModelsNoBeforeDelete = tccViewModel.ViewModelItems.Count;

            // Try to delete the summary view model.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.DeleteCalculationCommand.Execute(tccViewModel.SummaryViewModel);

            Assert.AreEqual(viewModelsNoBeforeDelete, tccViewModel.ViewModelItems.Count, "The SummaryViewModel was deleted from the view models list.");
        }

        /// <summary>
        /// This is a test for the DeleteCalculation command, to check if a calculation is deleted from the calculation items list and view models list.
        /// </summary>
        [TestMethod]
        public void DeleteCalculationViewModelTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Add 3 calculation items.
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var calculationsNoBeforeDelete = tccViewModel.CalculationItems.Count;
            var viewModelsNoBeforeDelete = tccViewModel.ViewModelItems.Count;

            // Delete the first item.
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.DeleteCalculationCommand.Execute(tccViewModel.CalculationItems[0]);

            Assert.AreEqual(calculationsNoBeforeDelete - 1, tccViewModel.CalculationItems.Count, "The calculation item was not deleted from the calculation items list.");
            Assert.AreEqual(viewModelsNoBeforeDelete - 1, tccViewModel.ViewModelItems.Count, "The calculation item was not deleted from the view models list.");
        }

        /// <summary>
        /// This is a test for the SelectCalculation method, to check that after the current selected item is deleted, the next calculation is selected.
        /// It also checks that the calculations indexes are adjusted after one calculation is removed.
        /// </summary>
        [TestMethod]
        public void SelectedItemAfterSelectedCalculationIsDeletedTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Add 3 calculation items.
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var firstItem = tccViewModel.CalculationItems[0];
            var secondItem = tccViewModel.CalculationItems[1];
            var thirdItem = tccViewModel.CalculationItems[2];

            // Get the ids that uniquely identifies the items.
            var firstItemId = firstItem.Model.Guid;
            var secondItemId = secondItem.Model.Guid;
            var thirdItemId = thirdItem.Model.Guid;

            tccViewModel.SelectedViewModelItem = firstItem;

            // Delete the first item (the selected view model item).
            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.DeleteCalculationCommand.Execute(firstItem);

            Assert.AreEqual(1, secondItem.Index.Value, "The index is incorrect.");
            Assert.AreEqual(2, thirdItem.Index.Value, "The index is incorrect.");

            var selectedItem = tccViewModel.SelectedViewModelItem as TransportCostCalculationViewModel;
            Assert.IsNotNull(selectedItem, "The selected view model is not a calculation view model.");

            // After the first item was deleted, the second one must be selected.
            Assert.AreEqual(secondItemId, tccViewModel.SummaryViewModel.SelectedCalculationItem.Model.Guid, "The selected calculation is not the expected one.");
        }

        /// <summary>
        /// This is a test for the SelectCalculation method, to check that after the only existing item is deleted, no other calculation is selected.
        /// </summary>
        [TestMethod]
        public void SelectedItemAfterLastCalculationIsDeletedTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Add a calculation that will be deleted.
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var firstItem = tccViewModel.CalculationItems[0];

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.DeleteCalculationCommand.Execute(firstItem);

            // After the item was deleted, no item has to be selected.
            Assert.IsNull(tccViewModel.SummaryViewModel.SelectedCalculationItem, "The selected calculation is not null.");
        }

        /// <summary>
        /// This is a test for the SelectCalculation method, to check that after the second item is deleted, the first calculation is selected.
        /// </summary>
        [TestMethod]
        public void SelectedItemAfterSecondCalculationIsDeletedTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            // Add 2 calculation items.
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);
            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            var firstItem = tccViewModel.CalculationItems[0];
            var secondItem = tccViewModel.CalculationItems[1];

            // Get the id that uniquely identifies the first item.
            var firstItemId = firstItem.Model.Guid;

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.DeleteCalculationCommand.Execute(secondItem);

            var selectedItem = tccViewModel.SelectedViewModelItem as TransportCostCalculationViewModel;
            Assert.IsNotNull(selectedItem, "The selected view model is not a calculation view model.");

            // After the first item was deleted, the second one must be selected.
            Assert.AreEqual(firstItemId, tccViewModel.SummaryViewModel.SelectedCalculationItem.Model.Guid, "The selected calculation is not the expected one.");
        }

        /// <summary>
        /// This is a test for the CancelCommand, to check if an added calculation is removed from the calculations list after the user cancels the changes.
        /// </summary>
        [TestMethod]
        public void CancelChangesTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            tccViewModel.CancelCommand.Execute(null);

            Assert.AreEqual(0, tccViewModel.CalculationItems.Count, "The calculation was not removed from the list after cancelling changes.");
        }

        /// <summary>
        /// This is a test for the CancelCommand, to check if an added calculation is removed from the calculations list after the user cancels the changes.
        /// </summary>
        [TestMethod]
        public void OnUnloadingTest()
        {
            TransportCostCalculatorViewModel tccViewModel = new TransportCostCalculatorViewModel(
                this.windowService,
                this.messenger,
                this.unitsService,
                this.compositionContainer,
                this.pleaseWaitService);

            tccViewModel.DataSourceManager = this.dataManager;

            this.Login();

            tccViewModel.SummaryViewModel.AddCalculationCommand.Execute(null);

            ((MessageDialogServiceMock)windowService.MessageDialogService).MessageDialogResult = MessageDialogResult.Yes;
            var onUnloading = tccViewModel.OnUnloading();

            Assert.AreEqual(0, tccViewModel.CalculationItems.Count, "The calculation was not removed from the list after cancelling changes.");
            Assert.IsTrue(onUnloading, "The OnUnloading event was not handled.");
        }

        #endregion

        /// <summary>
        /// Creates a new user and logins with its username and password.
        /// </summary>
        /// <returns>The user logged in.</returns>
        private User Login()
        {
            User user = new User();
            user.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User - " + DateTime.Now.Ticks;

            user.Roles = Role.Admin;

            this.dataManager.UserRepository.Add(user);
            this.dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");

            return user;
        }
    }
}
