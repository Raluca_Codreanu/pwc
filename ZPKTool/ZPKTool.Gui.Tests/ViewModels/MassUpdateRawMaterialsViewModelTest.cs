﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.DataAccess;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Gui.Notifications;

namespace PROimpens.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for  UpdateRawMaterialsViewModel and is intended 
    /// to contain unit tests for all public methods from UpdateRawMaterialsViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateRawMaterialsViewModelTest
    {
        /// <summary>
        /// The UI composition framework bootstrapper.
        /// </summary>
        private Bootstrapper bootstrapper;

        public MassUpdateRawMaterialsViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        ///// <summary>
        ///// A test for UpdateCommand -> cancelling the update. 
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest1()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();

        //    Random random = new Random();
        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    material1.Price = random.Next(0, 500);
        //    material1.YieldStrength = random.Next(1000).ToString();
        //    material1.RuptureStrength = random.Next(1000).ToString();
        //    material1.Density = random.Next(1000).ToString();
        //    material1.MaxElongation = random.Next(1000).ToString();
        //    material1.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material1.Rx = random.Next(1000).ToString();
        //    material1.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    material2.Price = random.Next(501, 1000);
        //    material2.YieldStrength = random.Next(1000).ToString();
        //    material2.RuptureStrength = random.Next(1000).ToString();
        //    material2.Density = random.Next(1000).ToString();
        //    material2.MaxElongation = random.Next(1000).ToString();
        //    material2.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material2.Rx = random.Next(1000).ToString();
        //    material2.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material2);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.PartRepository.Add(part);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Part partClone = cloneManager.Clone(part);
        //    CloneMaterialsGuids(part, partClone);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.CurrentObject = part;
        //    massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(1000);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

        //    windowService.MessageDialogResult = MessageDialogResult.No;
        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    MaterialsAreEqual(part, partClone);
        //}

        /// <summary>
        /// A test for UpdateCommand. The following input data were used:
        /// Current object-> Project and the new values are set to valid values.
        /// </summary>
        [TestMethod]
        public void UpdateMaterialsTest2()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

            // Set up the current object 
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Random random = new Random();
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.Price = random.Next(0, 500);
            material1.YieldStrength = random.Next(1000).ToString();
            material1.RuptureStrength = random.Next(1000).ToString();
            material1.Density = random.Next(1000).ToString();
            material1.MaxElongation = random.Next(1000).ToString();
            material1.GlassTransitionTemperature = random.Next(1000).ToString();
            material1.Rx = random.Next(1000).ToString();
            material1.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.Price = random.Next(501, 1000);
            material2.YieldStrength = random.Next(1000).ToString();
            material2.RuptureStrength = random.Next(1000).ToString();
            material2.Density = random.Next(1000).ToString();
            material2.MaxElongation = random.Next(1000).ToString();
            material2.GlassTransitionTemperature = random.Next(1000).ToString();
            material2.Rx = random.Next(1000).ToString();
            material2.Rm = random.Next(1000).ToString();
            part.RawMaterials.Add(material2);

            List<RawMaterialDataItem> materials = new List<RawMaterialDataItem>();
            RawMaterialDataItem item1 = new RawMaterialDataItem();
            item1.Name = material1.Name;
            item1.RawMaterials.Add(material1);
            materials.Add(item1);

            RawMaterialDataItem item2 = new RawMaterialDataItem();
            item2.Name = material2.Name;
            item2.RawMaterials.Add(material2);
            materials.Add(item2);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1);
            Project projectClone = cloneManager.Clone(project);
            CloneMaterialsGuids(project, projectClone);

            // Set up the view model
            massUpdateRawMaterialsViewModel.DataContext = dataContext1;
            massUpdateRawMaterialsViewModel.CurrentObject = project;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
            massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(1000);
            massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
            massUpdateRawMaterialsViewModel.SelectedRawMaterials = materials;

            bool projectNotified = false;
            messenger.Register<EntityChangedMessage>(msg =>
            {
                if (msg.ChangeType == NotificationType.RefreshData)
                {
                    projectNotified = true;
                }
            });

            windowService.MessageDialogResult = MessageDialogResult.Yes;
            ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
            updateCommand.Execute(null);

            // Verify that the updates were not applied
            int expectedNumberOfUpdatedObjects = 0;
            DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
            Project dbProject = dataContext2.ProjectRepository.GetProjectFull(project.Guid);
            MaterialsUpdated(projectClone, dbProject, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

            // Verify that an info message was displayed when the update operation has been performed
            Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

            // Retrieve the number of updated objects from the displayed message
            int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
            Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");

            // Verify that a notification has been send 
            Assert.IsTrue(projectNotified, "Failed to send a notification that the project has been changed.");
        }

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Part, the new values are set to valid values and all raw materials 
        ///// from the list are selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest3()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();

        //    Random random = new Random();
        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    material1.Price = random.Next(0, 500);
        //    material1.YieldStrength = random.Next(1000).ToString();
        //    material1.RuptureStrength = random.Next(1000).ToString();
        //    material1.Density = random.Next(1000).ToString();
        //    material1.MaxElongation = random.Next(1000).ToString();
        //    material1.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material1.Rx = random.Next(1000).ToString();
        //    material1.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    material2.Price = random.Next(501, 800);
        //    material2.YieldStrength = random.Next(1000).ToString();
        //    material2.RuptureStrength = random.Next(1000).ToString();
        //    material2.Density = random.Next(1000).ToString();
        //    material2.MaxElongation = random.Next(1000).ToString();
        //    material2.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material2.Rx = random.Next(1000).ToString();
        //    material2.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material2);

        //    RawMaterial material3 = new RawMaterial();
        //    material3.Name = material3.Guid.ToString();
        //    material3.Price = random.Next(801, 1000);
        //    material3.YieldStrength = random.Next(1000).ToString();
        //    material3.RuptureStrength = random.Next(1000).ToString();
        //    material3.Density = random.Next(1000).ToString();
        //    material3.MaxElongation = random.Next(1000).ToString();
        //    material3.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material3.Rx = random.Next(1000).ToString();
        //    material3.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material3);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.PartRepository.Add(part);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Part partClone = cloneManager.Clone(part);
        //    CloneMaterialsGuids(part, partClone);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = part;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
        //    MaterialsUpdated(partClone, dbPart, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Part, the new values are set to valid values and no raw material 
        ///// from the list is selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest4()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();

        //    Random random = new Random();
        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    material1.Price = random.Next(0, 500);
        //    material1.YieldStrength = random.Next(1000).ToString();
        //    material1.RuptureStrength = random.Next(1000).ToString();
        //    material1.Density = random.Next(1000).ToString();
        //    material1.MaxElongation = random.Next(1000).ToString();
        //    material1.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material1.Rx = random.Next(1000).ToString();
        //    material1.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    material2.Price = random.Next(501, 800);
        //    material2.YieldStrength = random.Next(1000).ToString();
        //    material2.RuptureStrength = random.Next(1000).ToString();
        //    material2.Density = random.Next(1000).ToString();
        //    material2.MaxElongation = random.Next(1000).ToString();
        //    material2.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material2.Rx = random.Next(1000).ToString();
        //    material2.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material2);

        //    RawMaterial material3 = new RawMaterial();
        //    material3.Name = material3.Guid.ToString();
        //    material3.Price = random.Next(801, 1000);
        //    material3.YieldStrength = random.Next(1000).ToString();
        //    material3.RuptureStrength = random.Next(1000).ToString();
        //    material3.Density = random.Next(1000).ToString();
        //    material3.MaxElongation = random.Next(1000).ToString();
        //    material3.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material3.Rx = random.Next(1000).ToString();
        //    material3.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material3);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.PartRepository.Add(part);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Part partClone = cloneManager.Clone(part);
        //    CloneMaterialsGuids(part, partClone);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = part;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = new List<RawMaterialDataItem>();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
        //    MaterialsAreEqual(partClone, dbPart);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> Part, the new values are set to null and all raw materials 
        ///// from the list are selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest5()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();

        //    Random random = new Random();
        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    material1.Price = random.Next(0, 500);
        //    material1.YieldStrength = random.Next(1000).ToString();
        //    material1.RuptureStrength = random.Next(1000).ToString();
        //    material1.Density = random.Next(1000).ToString();
        //    material1.MaxElongation = random.Next(1000).ToString();
        //    material1.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material1.Rx = random.Next(1000).ToString();
        //    material1.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    material2.Price = random.Next(501, 800);
        //    material2.YieldStrength = random.Next(1000).ToString();
        //    material2.RuptureStrength = random.Next(1000).ToString();
        //    material2.Density = random.Next(1000).ToString();
        //    material2.MaxElongation = random.Next(1000).ToString();
        //    material2.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material2.Rx = random.Next(1000).ToString();
        //    material2.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material2);

        //    RawMaterial material3 = new RawMaterial();
        //    material3.Name = material3.Guid.ToString();
        //    material3.Price = random.Next(801, 1000);
        //    material3.YieldStrength = random.Next(1000).ToString();
        //    material3.RuptureStrength = random.Next(1000).ToString();
        //    material3.Density = random.Next(1000).ToString();
        //    material3.MaxElongation = random.Next(1000).ToString();
        //    material3.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material3.Rx = random.Next(1000).ToString();
        //    material3.Rm = random.Next(1000).ToString();
        //    part.RawMaterials.Add(material3);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.PartRepository.Add(part);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    Part partClone = cloneManager.Clone(part);
        //    CloneMaterialsGuids(part, partClone);
        
        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = part;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    Part dbPart = dataContext2.PartRepository.GetPartFull(part.Guid);
        //    MaterialsUpdated(partClone, dbPart, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> RawMaterial, the new values are set to valid values and the raw material 
        ///// from the list is selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest6()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Random random = new Random();
        //    RawMaterial material = new RawMaterial();
        //    material.Name = material.Guid.ToString();
        //    material.Price = random.Next(0, 500);
        //    material.YieldStrength = random.Next(1000).ToString();
        //    material.RuptureStrength = random.Next(1000).ToString();
        //    material.Density = random.Next(1000).ToString();
        //    material.MaxElongation = random.Next(1000).ToString();
        //    material.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material.Rx = random.Next(1000).ToString();
        //    material.Rm = random.Next(1000).ToString();

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.RawMaterialRepository.Add(material);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    RawMaterial materialClone = cloneManager.Clone(material);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = material;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material.Guid);
        //    MaterialUpdated(materialClone, dbMaterial, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> RawMaterial, the new values are set to valid values and the raw material 
        ///// from the list is not selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest7()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Random random = new Random();
        //    RawMaterial material = new RawMaterial();
        //    material.Name = material.Guid.ToString();
        //    material.Price = random.Next(0, 500);
        //    material.YieldStrength = random.Next(1000).ToString();
        //    material.RuptureStrength = random.Next(1000).ToString();
        //    material.Density = random.Next(1000).ToString();
        //    material.MaxElongation = random.Next(1000).ToString();
        //    material.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material.Rx = random.Next(1000).ToString();
        //    material.Rm = random.Next(1000).ToString();

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.RawMaterialRepository.Add(material);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    RawMaterial materialClone = cloneManager.Clone(material);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = material;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.PriceNewValue = random.Next(100);
        //    massUpdateRawMaterialsViewModel.YieldStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RuptureStrengthNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.DensityNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.MaxElongationNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.GlassTTNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.RxNewValue = random.Next(1000).ToString();
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = new List<RawMaterialDataItem>();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material.Guid);
        //    MaterialsAreEqual(materialClone, dbMaterial);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        ///// <summary>
        ///// A test for UpdateCommand. The following input data were used:
        ///// Current object-> RawMaterial, the new values are set to null and the raw material 
        ///// from the list is selected for update.
        ///// </summary>
        //[TestMethod]
        //public void UpdateMaterialsTest8()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Random random = new Random();
        //    RawMaterial material = new RawMaterial();
        //    material.Name = material.Guid.ToString();
        //    material.Price = random.Next(0, 500);
        //    material.YieldStrength = random.Next(1000).ToString();
        //    material.RuptureStrength = random.Next(1000).ToString();
        //    material.Density = random.Next(1000).ToString();
        //    material.MaxElongation = random.Next(1000).ToString();
        //    material.GlassTransitionTemperature = random.Next(1000).ToString();
        //    material.Rx = random.Next(1000).ToString();
        //    material.Rm = random.Next(1000).ToString();

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.RawMaterialRepository.Add(material);
        //    dataContext1.SaveChanges();

        //    CloneManager cloneManager = new CloneManager(dataContext1);
        //    RawMaterial materialClone = cloneManager.Clone(material);

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = material;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.SelectedRawMaterials = massUpdateRawMaterialsViewModel.RawMaterialsCollection.ToList();

        //    ICommand updateCommand = massUpdateRawMaterialsViewModel.UpdateCommand;
        //    updateCommand.Execute(null);

        //    // Verify that the updates were not applied
        //    int expectedNumberOfUpdatedObjects = 0;
        //    DataContext dataContext2 = new DataContext(EntityContext.LocalDatabase);
        //    RawMaterial dbMaterial = dataContext2.RawMaterialRepository.GetById(material.Guid);
        //    MaterialUpdated(materialClone, dbMaterial, massUpdateRawMaterialsViewModel, ref expectedNumberOfUpdatedObjects);

        //    // Verify that an info message was displayed when the update operation has been performed
        //    Assert.IsFalse(string.IsNullOrEmpty(messageBoxService.Message), "Failed to display an info message after the update operation has been performed.");

        //    // Retrieve the number of updated objects from the displayed message
        //    int numberOfUpdatedObjects = int.Parse(string.Join(null, System.Text.RegularExpressions.Regex.Split(messageBoxService.Message, "[^\\d]")));
        //    Assert.AreEqual<int>(expectedNumberOfUpdatedObjects, numberOfUpdatedObjects, "Wrong number of updated objects displayed.");
        //}

        /// <summary>
        /// A test for ChangeMaterialsCollection method. The following input data were used:
        /// Current object: Project and ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest1()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

            // Set up the current object
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            subassembly1.Parts.Add(part1);

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = rawMaterial1.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial1);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part2);

            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = rawMaterial1.Name;
            part2.RawMaterials.Add(rawMaterial2);

            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            project.Parts.Add(part3);

            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = rawMaterial3.Guid.ToString();
            part3.RawMaterials.Add(rawMaterial3);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Set up the current view model
            massUpdateRawMaterialsViewModel.CurrentObject = project;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateRawMaterialsViewModel.DataContext = dataContext1;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            // Verify that the collection is empty
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 0, "The RawMaterialsCollection shouldn't be populated if the current object is a project and ApplyUpdatesToSubobjects = false.");
        }

        ///// <summary>
        ///// A test for ChangeMaterialsCollection method. The following input data were used:
        ///// Current object: Project and ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void ChangeMaterialsCollectionTest2()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Project project = new Project();
        //    project.Name = project.Guid.ToString();
        //    project.OverheadSettings = new OverheadSetting();

        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.OverheadSettings = new OverheadSetting();
        //    assembly.CountrySettings = new CountrySetting();
        //    project.Assemblies.Add(assembly);

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.CountrySettings = new CountrySetting();
        //    part1.OverheadSettings = new OverheadSetting();
        //    subassembly1.Parts.Add(part1);

        //    RawMaterial rawMaterial1 = new RawMaterial();
        //    rawMaterial1.Name = rawMaterial1.Guid.ToString();
        //    part1.RawMaterials.Add(rawMaterial1);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = new OverheadSetting();
        //    assembly.Parts.Add(part2);

        //    RawMaterial rawMaterial2 = new RawMaterial();
        //    rawMaterial2.Name = rawMaterial1.Name;
        //    part2.RawMaterials.Add(rawMaterial2);

        //    Part part3 = new Part();
        //    part3.Name = part3.Guid.ToString();
        //    part3.OverheadSettings = new OverheadSetting();
        //    part3.CountrySettings = new CountrySetting();
        //    project.Parts.Add(part3);

        //    RawMaterial rawMaterial3 = new RawMaterial();
        //    rawMaterial3.Name = rawMaterial3.Guid.ToString();
        //    part3.RawMaterials.Add(rawMaterial3);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.ProjectRepository.Add(project);
        //    dataContext1.SaveChanges();

        //    // Set up the current view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = project;
        //    massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

        //    // Verify that the collection contains all raw materials from the project and the materials with the same name are listed once
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 2, "The RawMaterialsCollection shouldn't be populated if the current object is a project and ApplyUpdatesToSubobjects = false.");
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i => i.Name == rawMaterial1.Name).RawMaterials.Count == 2, "Only one raw material item should be listed for the materials with the same name.");
        //}

        /// <summary>
        /// A test for ChangeMaterialsCollection. The following input data were used:
        /// Current object: Assembly and the ApplyUpdatesToSubobjects = false.
        /// </summary>
        [TestMethod]
        public void ChangeMaterialsCollectionTest3()
        {
            IMessenger messenger = new Messenger();
            MessageDialogServiceMock messageBoxService = new MessageDialogServiceMock();
            DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
            WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
            MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

            // Set up the current object
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = subassembly1.Guid.ToString();
            subassembly1.CountrySettings = new CountrySetting();
            subassembly1.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part1);

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = rawMaterial1.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = subassembly2.Guid.ToString();
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            subassembly2.Parts.Add(part2);

            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = rawMaterial2.Guid.ToString();
            part2.RawMaterials.Add(rawMaterial2);

            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part3);

            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = rawMaterial1.Name;
            part3.RawMaterials.Add(rawMaterial3);

            DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Set up the view model
            massUpdateRawMaterialsViewModel.CurrentObject = assembly;
            massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = false;
            massUpdateRawMaterialsViewModel.DataContext = dataContext1;
            massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

            // Verify that the RawMaterials collection is empty
            Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 0, "The RawMaterialsCollection should be empty if the current object is Assembly and ApplyUpdatesToSubobjects = false.");
        }

        ///// <summary>
        ///// A test for ChangeMaterialsCollection. The following input data were used:
        ///// Current object: Assembly and the ApplyUpdatesToSubobjects = true.
        ///// </summary>
        //[TestMethod]
        //public void ChangeMaterialsCollectionTest4()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Assembly assembly = new Assembly();
        //    assembly.Name = assembly.Guid.ToString();
        //    assembly.CountrySettings = new CountrySetting();
        //    assembly.OverheadSettings = new OverheadSetting();

        //    Assembly subassembly1 = new Assembly();
        //    subassembly1.Name = subassembly1.Guid.ToString();
        //    subassembly1.CountrySettings = new CountrySetting();
        //    subassembly1.OverheadSettings = new OverheadSetting();
        //    assembly.Subassemblies.Add(subassembly1);

        //    Part part1 = new Part();
        //    part1.Name = part1.Guid.ToString();
        //    part1.OverheadSettings = new OverheadSetting();
        //    part1.CountrySettings = new CountrySetting();
        //    subassembly1.Parts.Add(part1);

        //    RawMaterial rawMaterial1 = new RawMaterial();
        //    rawMaterial1.Name = rawMaterial1.Guid.ToString();
        //    part1.RawMaterials.Add(rawMaterial1);

        //    Assembly subassembly2 = new Assembly();
        //    subassembly2.Name = subassembly2.Guid.ToString();
        //    subassembly2.OverheadSettings = new OverheadSetting();
        //    subassembly2.CountrySettings = new CountrySetting();
        //    subassembly1.Subassemblies.Add(subassembly2);

        //    Part part2 = new Part();
        //    part2.Name = part2.Guid.ToString();
        //    part2.CountrySettings = new CountrySetting();
        //    part2.OverheadSettings = new OverheadSetting();
        //    subassembly2.Parts.Add(part2);

        //    RawMaterial rawMaterial2 = new RawMaterial();
        //    rawMaterial2.Name = rawMaterial2.Guid.ToString();
        //    part2.RawMaterials.Add(rawMaterial2);

        //    Part part3 = new Part();
        //    part3.Name = part3.Guid.ToString();
        //    part3.OverheadSettings = new OverheadSetting();
        //    part3.CountrySettings = new CountrySetting();
        //    assembly.Parts.Add(part3);

        //    RawMaterial rawMaterial3 = new RawMaterial();
        //    rawMaterial3.Name = rawMaterial1.Name;
        //    part3.RawMaterials.Add(rawMaterial3);

        //    DataContext dataContext1 = new DataContext(EntityContext.LocalDatabase);
        //    dataContext1.AssemblyRepository.Add(assembly);
        //    dataContext1.SaveChanges();

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = assembly;
        //    massUpdateRawMaterialsViewModel.ApplyUpdatesToSubObjects = true;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext1;
        //    massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

        //    // Verify that the RawMaterials collection is contains all raw materials from the assembly and the materials with the same name are listed once
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 2, "Failed to change the RawMaterialsCollection.");
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i => i.Name == rawMaterial1.Name).RawMaterials.Count == 2, "The raw materials with the same name should be listed once.");
        //}

        ///// <summary>
        ///// A test for ChangeMaterialsCollection method using the following data:
        ///// Current object: part.
        ///// </summary>
        //[TestMethod]
        //public void ChangeMaterialsCollectionTest5()
        //{
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MassUpdateRawMaterialsViewModel massUpdateRawMaterialsViewModel = new MassUpdateRawMaterialsViewModel(messenger, windowService);

        //    // Set up the current object
        //    Part part = new Part();
        //    part.Name = part.Guid.ToString();
        //    part.OverheadSettings = new OverheadSetting();
        //    part.CountrySettings = new CountrySetting();

        //    RawMaterial material1 = new RawMaterial();
        //    material1.Name = material1.Guid.ToString();
        //    part.RawMaterials.Add(material1);

        //    RawMaterial material2 = new RawMaterial();
        //    material2.Name = material2.Guid.ToString();
        //    part.RawMaterials.Add(material2);

        //    RawMaterial material3 = new RawMaterial();
        //    material3.Name = material1.Name;
        //    part.RawMaterials.Add(material3);

        //    DataContext dataContext = new DataContext(EntityContext.LocalDatabase);
        //    dataContext.PartRepository.Add(part);
        //    dataContext.SaveChanges();

        //    // Set up the view model
        //    massUpdateRawMaterialsViewModel.CurrentObject = part;
        //    massUpdateRawMaterialsViewModel.DataContext = dataContext;
        //    massUpdateRawMaterialsViewModel.ChangeMaterialsCollection();

        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.Count == 3, "");
        //    Assert.IsTrue(massUpdateRawMaterialsViewModel.RawMaterialsCollection.FirstOrDefault(i=>i.Name == material1.Name).RawMaterials.Count == 2,"");
        //}

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Verifies if a raw material was updated.
        /// </summary>
        /// <param name="oldMaterial">The old material.</param>
        /// <param name="updatedMaterial">The updated material.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void MaterialUpdated(RawMaterial oldMaterial, RawMaterial updatedMaterial, MassUpdateRawMaterialsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            bool objectUpdated = false;
            if (viewModel.PriceNewValue != null)
            {
                Assert.AreEqual<decimal>(viewModel.PriceNewValue.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "Failed to update the Price property with the new value.");
                objectUpdated = true;
            }
            else if (viewModel.PricePercent != null)
            {
                Assert.AreEqual<decimal>(viewModel.PricePercent.GetValueOrDefault() * oldMaterial.Price.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "Failed to update the Price property with the increase percentage.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<decimal>(oldMaterial.Price.GetValueOrDefault(), updatedMaterial.Price.GetValueOrDefault(), "The Price should not have been updated if the new value was null.");
            }

            if (viewModel.YieldStrengthNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.YieldStrengthNewValue, updatedMaterial.YieldStrength, "Failed to update the YieldStrength property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.YieldStrength, updatedMaterial.YieldStrength, "The YieldStrength should not have been updated if the new value was null.");
            }

            if (viewModel.RuptureStrengthNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.RuptureStrengthNewValue, updatedMaterial.RuptureStrength, "Failed to update the RuptureStrength property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.RuptureStrength, updatedMaterial.RuptureStrength, "The RuptureStrength should not have been updated if the new value was null.");
            }

            if (viewModel.DensityNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.DensityNewValue, updatedMaterial.Density, "Failed to update the Density property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.Density, updatedMaterial.Density, "The Density property should not have been updated if the new value was null.");
            }

            if (viewModel.MaxElongationNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.MaxElongationNewValue, updatedMaterial.MaxElongation, "Failed to update the MaxElongation property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.MaxElongation, updatedMaterial.MaxElongation, "The MaxElongation property should not have been updated if the new value was null.");
            }

            if (viewModel.GlassTTNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.GlassTTNewValue, updatedMaterial.GlassTransitionTemperature, "Failed to update the GlassTransitionTemperature property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.GlassTransitionTemperature, updatedMaterial.GlassTransitionTemperature, "The GlassTransitionTemperature property should not have been updated if the new value was null.");
            }

            if (viewModel.RxNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.RxNewValue, updatedMaterial.Rx, "Failed to update the Rx property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.Rx, updatedMaterial.Rx, "The Rx property should not have been updated if the new value was null.");
            }

            if (viewModel.RmNewValue != null)
            {
                Assert.AreEqual<string>(viewModel.RmNewValue, updatedMaterial.Rm, "Failed to update the Rm property with the new value.");
                objectUpdated = true;
            }
            else
            {
                Assert.AreEqual<string>(oldMaterial.Rm, updatedMaterial.Rm, "The Rm property should not have been updated if the new value was null.");
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Verifies if the raw materials from a project were updated.
        /// </summary>
        /// <param name="oldProject">The old project.</param>
        /// <param name="updatedProject">The updated project.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void MaterialsUpdated(Project oldProject, Project updatedProject, MassUpdateRawMaterialsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MaterialsUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }
                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    MaterialsUpdated(oldAssembly, assembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedProject.Parts)
                {
                    Part oldPart = oldProject.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MaterialsAreEqual(oldPart, part);
                }
                foreach (Assembly assembly in updatedProject.Assemblies)
                {
                    Assembly oldAssembly = oldProject.Assemblies.FirstOrDefault(a => a.Name == assembly.Name);
                    MaterialsAreEqual(oldAssembly, assembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the raw materials belonging to an assembly were updated.
        /// </summary>
        /// <param name="oldAssembly">The old assembly.</param>
        /// <param name="updatedAssembly">The updated assembly.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void MaterialsUpdated(Assembly oldAssembly, Assembly updatedAssembly, MassUpdateRawMaterialsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            if (viewModel.ApplyUpdatesToSubObjects)
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MaterialsUpdated(oldPart, part, viewModel, ref numberOfUpdatedObjects);
                }
                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    MaterialsUpdated(oldSubassembly, subassembly, viewModel, ref numberOfUpdatedObjects);
                }
            }
            else
            {
                foreach (Part part in updatedAssembly.Parts)
                {
                    Part oldPart = oldAssembly.Parts.FirstOrDefault(p => p.Name == part.Name);
                    MaterialsAreEqual(oldPart, part);
                }
                foreach (Assembly subassembly in updatedAssembly.Subassemblies)
                {
                    Assembly oldSubassembly = oldAssembly.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                    MaterialsAreEqual(oldSubassembly, subassembly);
                }
            }
        }

        /// <summary>
        /// Verifies if the raw materials belonging to a part were updated.
        /// </summary>
        /// <param name="oldPart">The old part.</param>
        /// <param name="updatedPart">The updated part.</param>
        /// <param name="viewModel">The view model which contains the new values.</param>
        /// <param name="numberOfUpdatedObjects">The number of updated objects.</param>
        private void MaterialsUpdated(Part oldPart, Part updatedPart, MassUpdateRawMaterialsViewModel viewModel, ref int numberOfUpdatedObjects)
        {
            foreach (RawMaterial material in updatedPart.RawMaterials)
            {
                RawMaterial oldMaterial = oldPart.RawMaterials.FirstOrDefault(m => m.Guid == material.Guid);
                MaterialUpdated(oldMaterial, material, viewModel, ref numberOfUpdatedObjects);
            }
        }

        /// <summary>
        /// Verifies if two materials have the same property values.
        /// The following properties will be compared: Price, YieldStrength, RuptureStrength,
        /// Density, MaxElongation, GlassTransitionTemperature, Rx and Rm.
        /// </summary>
        /// <param name="comparedMaterial">The compared raw material.</param>
        /// <param name="materialToCompareWith">The raw material to compare with.</param>
        private void MaterialsAreEqual(RawMaterial comparedMaterial, RawMaterial materialToCompareWith)
        {
            bool result = comparedMaterial.Price == materialToCompareWith.Price &&
                          string.Equals(comparedMaterial.YieldStrength, materialToCompareWith.YieldStrength) &&
                          string.Equals(comparedMaterial.RuptureStrength, materialToCompareWith.RuptureStrength) &&
                          string.Equals(comparedMaterial.Density, materialToCompareWith.Density) &&
                          string.Equals(comparedMaterial.MaxElongation, materialToCompareWith.MaxElongation) &&
                          string.Equals(comparedMaterial.GlassTransitionTemperature, materialToCompareWith.GlassTransitionTemperature) &&
                          string.Equals(comparedMaterial.Rx, materialToCompareWith.Rx) &&
                          string.Equals(comparedMaterial.Rm, materialToCompareWith.Rm);

            Assert.IsTrue(result, "The raw materials are not equal.");
        }

        /// <summary>
        /// Verifies if the materials of two parts have the same properties values.
        /// </summary>
        /// <param name="comparedPart">The compared part.</param>
        /// <param name="partToCompareWith">The part to compare with.</param>
        private void MaterialsAreEqual(Part comparedPart, Part partToCompareWith)
        {
            foreach (RawMaterial material in comparedPart.RawMaterials)
            {
                RawMaterial materialToCompareWith = partToCompareWith.RawMaterials.FirstOrDefault(m => m.Guid == material.Guid);
                MaterialsAreEqual(material, materialToCompareWith);
            }
        }

        /// <summary>
        /// Verifies if the materials of two assemblies have the same properties values.
        /// </summary>
        /// <param name="comparedAssembly">The compared assembly.</param>
        /// <param name="assemblyToCompareWith">The assembly to compare with.</param>
        private void MaterialsAreEqual(Assembly comparedAssembly, Assembly assemblyToCompareWith)
        {
            foreach (Part part in comparedAssembly.Parts)
            {
                Part partToCompareWith = assemblyToCompareWith.Parts.FirstOrDefault(p => p.Name == part.Name);
                MaterialsAreEqual(partToCompareWith, part);
            }
            foreach (Assembly subassembly in comparedAssembly.Subassemblies)
            {
                Assembly subassemblyToCompareWith = assemblyToCompareWith.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                MaterialsAreEqual(subassemblyToCompareWith, subassembly);
            }
        }

        /// <summary>
        /// Clones the Guids of raw materials belonging to the project. 
        /// The materials from the project needs to have a unique price.
        /// </summary>
        /// <param name="project">The cloned project.</param>
        /// <param name="projectClone">The clone of the project.</param>
        private void CloneMaterialsGuids(Project project, Project projectClone)
        {
            foreach (Part part in project.Parts)
            {
                Part partClone = projectClone.Parts.FirstOrDefault(p => p.Name == part.Name);
                CloneMaterialsGuids(part, partClone);
            }

            foreach (Assembly subassembly in project.Assemblies)
            {
                Assembly subassemblyClone = projectClone.Assemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                CloneMaterialsGuids(subassembly, subassemblyClone);
            }
        }

        /// <summary>
        /// Clones the Guids of raw materials belonging to the assembly. 
        /// The materials from the assembly needs to have a unique price.
        /// </summary>
        /// <param name="assembly">The cloned assembly.</param>
        /// <param name="assemblyClone">The clone of assembly.</param>
        private void CloneMaterialsGuids(Assembly assembly, Assembly assemblyClone)
        {
            foreach (Part part in assembly.Parts)
            {
                Part partClone = assemblyClone.Parts.FirstOrDefault(p => p.Name == part.Name);
                CloneMaterialsGuids(part, partClone);
            }

            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                Assembly subassemblyClone = assemblyClone.Subassemblies.FirstOrDefault(a => a.Name == subassembly.Name);
                CloneMaterialsGuids(subassembly, subassemblyClone);
            }
        }

        /// <summary>
        /// Clones the Guids of raw materials belonging to the part. 
        /// The materials from the part needs to have a unique price.
        /// </summary>
        /// <param name="part">The cloned part.</param>
        /// <param name="partClone">The clone of part.</param>
        private void CloneMaterialsGuids(Part part, Part partClone)
        {
            foreach (RawMaterial material in part.RawMaterials)
            {
                RawMaterial materialClone = partClone.RawMaterials.FirstOrDefault(m => m.Name == material.Name && m.Price == material.Price);
                materialClone.Guid = material.Guid;
            }
        }

        #endregion Helpers
    }
}
