﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// PicturesViewModelTest class.
    /// </summary>
    [TestClass]
    public class PicturesViewModelTest
    {
        #region Fields

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PicturesViewModelTest"/> class.
        /// </summary>
        public PicturesViewModelTest()
        {
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        /// <value>
        /// The test context.
        /// </value>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
            this.messenger = new Messenger();
            this.modelBrowserService = new ModelBrowserHelperServiceMock();

            var compositionContainer = new CompositionContainer();
        }

        //// Use TestCleanup to run code after each test has run
        ////[TestCleanup]
        ////public void MyTestCleanup()
        ////{            
        ////}

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// PicturesViewModel delete single picture test.
        /// </summary>
        [TestMethod]
        public void DeleteSinglePictureTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_DeletePicture, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);
            var picturesVM = new PicturesViewModel(messenger, wndServiceMock.Object, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            picturesVM.MediaSource.Media.Add(mediaList[0]);

            bool mediaDeleted = false;

            messenger.Register<NotificationMessage>(
            (msg) =>
            {
                if (msg.Notification == Notification.MediaDeleted)
                {
                    mediaDeleted = true;
                }
            });

            ICommand deleteMediaCommand = picturesVM.DeleteCurrentMediaCommand;
            deleteMediaCommand.Execute(null);

            Assert.IsTrue(mediaDeleted);
        }

        /// <summary>
        /// PicturesViewModel delete multiple pictures.
        /// </summary>
        [TestMethod]
        public void DeleteMultiplePicturesTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_DeleteAllPictures, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);
            var picturesVM = new PicturesViewModel(messenger, wndServiceMock.Object, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Image);

            foreach (var mediaItem in mediaList)
            {
                picturesVM.MediaSource.Media.Add(mediaItem);
            }

            bool mediaDeleted = false;
            messenger.Register<NotificationMessage>(
            (msg) =>
            {
                if (msg.Notification == Notification.MediaDeleted)
                {
                    mediaDeleted = true;
                }
            });

            picturesVM.TogglePreviewMode(true);
            ICommand deleteMediaCommand = picturesVM.DeleteCurrentMediaCommand;
            deleteMediaCommand.Execute(null);

            Assert.IsTrue(mediaDeleted);
        }

        /// <summary>
        /// PicturesViewModel save as command test.
        /// </summary>
        [TestMethod]
        public void SaveAsPicturesTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var picturesVM = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            picturesVM.MediaSource.Media.Add(mediaList[0]);

            ICommand saveAsCommand = picturesVM.SaveMediaAsCommand;
            saveAsCommand.Execute(null);
        }

        /// <summary>
        /// PicturesViewModel Display media at a specific index test.
        /// </summary>
        [TestMethod]
        public void DisplayMediaTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var picturesVM = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            picturesVM.MediaSource.Media.Add(mediaList[0]);

            picturesVM.DisplayMediaAtIndex(0);
        }

        /// <summary>
        /// PicturesViewModel GetMedia Bytes test.
        /// </summary>
        [TestMethod]
        public void GetMediaBytesTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var picturesVM = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            byte[] pictureBytes = null;
            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);
            if (pictureExists)
            {
                pictureBytes = picturesVM.GetMediaBytes(picturePath);
            }

            Assert.IsNotNull(pictureBytes);
        }

        /// <summary>
        /// PicturesViewModel GetPicturesFromMetaData test.
        /// </summary>
        [TestMethod]
        public void GetPicturesFromMetaDataTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var picturesVM = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            picturesVM.DataContext = localDataManager;

            var machine = this.CreateNewMachine(localDataManager);
            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            if (mediaList.Count > 0)
            {
                machine.Media = mediaList[0];
            }

            var media = picturesVM.GetPictures(machine, DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase));
            Assert.AreEqual(media.Count, 0);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// Create and save to DB a new machine.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created machine.</returns>
        private Machine CreateNewMachine(IDataSourceManager dataManager)
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;

            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// Create a new Media and save it to DB.
        /// </summary>
        /// <param name="dataManager">Data Manager source.</param>
        /// <param name="mediaMode">Media Mode.</param>
        /// <param name="mediaType">Media Type.</param>
        /// <returns>A list of media.</returns>
        private List<Media> CreateNewMedia(IDataSourceManager dataManager, MediaControlMode mediaMode, MediaType mediaType)
        {
            var medialist = new List<Media>();
            var media = new Media();

            switch (mediaMode)
            {
                case MediaControlMode.SingleImage:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                case MediaControlMode.MultipleImagesOrVideo:
                    {
                        if (mediaType == MediaType.Image)
                        {
                            medialist.Add(this.GetMedia(MediaType.Image));
                            medialist.Add(this.GetMedia(MediaType.Image));
                        }

                        if (mediaType == MediaType.Video)
                        {
                            medialist.Add(this.GetMedia(MediaType.Video));
                        }

                        break;
                    }

                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                default:
                    break;
            }

            foreach (var mediaItem in medialist)
            {
                dataManager.MediaRepository.Add(mediaItem);
            }

            dataManager.SaveChanges();
            return medialist;
        }

        /// <summary>
        /// Get a media object.
        /// </summary>
        /// <param name="type">Media Type.</param>
        /// <returns>The created media.</returns>
        private Media GetMedia(MediaType type)
        {
            var media = new Media();
            media.Type = type;
            media.Content = new byte[] { 1, 1, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();

            return media;
        }

        #endregion Private Methods
    }
}