﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business.Export;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// MediaViewModelTest class.
    /// </summary>
    [TestClass]
    public class MediaViewModelTest
    {
        #region Fields

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The video view-model.
        /// </summary>
        private VideoViewModel videoVM;

        /// <summary>
        /// The pictures view-model.
        /// </summary>
        private PicturesViewModel picturesVM;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaViewModelTest"/> class.
        /// </summary>
        public MediaViewModelTest()
        {
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        /// <value>
        /// The test context.
        /// </value>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
            this.messenger = new Messenger();
            this.modelBrowserService = new ModelBrowserHelperServiceMock();

            var compositionContainer = new CompositionContainer();
            this.videoVM = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            this.picturesVM = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
        }

        //// Use TestCleanup to run code after each test has run
        ////[TestCleanup]
        ////public void MyTestCleanup()
        ////{            
        ////}

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// MediaViewModel load media test.
        /// </summary>
        [TestMethod]
        public void LoadMediaTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;

            mediaVM.Mode = MediaControlMode.SingleImage;
            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;
            mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            for (int i = 0; i < mediaList.Count; i++)
            {
                Assert.AreEqual(mediaList[i].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[i].OriginalFileName);
                Assert.AreEqual(mediaList[i].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[i] as Media).OriginalFileName);
            }

            mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.Mode = MediaControlMode.MultipleImagesNoVideo;
            mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesNoVideo, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            for (int i = 0; i < mediaList.Count; i++)
            {
                Assert.AreEqual(mediaList[i].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[i].OriginalFileName);
                Assert.AreEqual(mediaList[i].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[i] as Media).OriginalFileName);
            }
        }

        /// <summary>
        /// Load and save media test.
        /// </summary>
        [TestMethod]
        public void LoadAndSaveMediaScenarioTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;
            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.SaveToModelCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);

            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.SaveToModelCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with a single picture in Media test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithSinglePictureTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            var machine = this.CreateNewMachine(localDataManager);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            if (mediaList.Count > 0)
            {
                machine.Media = mediaList[0];
            }

            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with multiple pictures in Media test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithMultiplePicturesTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;

            var assembly = this.CreateNewAssembly(localDataManager);
            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Image);
            if (mediaList.Count > 0)
            {
                foreach (var mediaItem in mediaList)
                {
                    assembly.Media.Add(mediaItem);
                }
            }

            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
            for (int i = 0; i < mediaList.Count; i++)
            {
                Assert.AreEqual(mediaList[i].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[i].OriginalFileName);
            }
        }

        /// <summary>
        /// Load model with video media test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithVideoTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;

            var assembly = this.CreateNewAssembly(localDataManager);
            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            assembly.Media.Add(mediaList[0]);
            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with a single picture in Media from DB test.
        /// </summary>
        [TestMethod]
        public void LoadModelSinglePictureMediaFromDBTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), this.modelBrowserService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            var media = this.GetMedia(MediaType.Image);
            machine.Media = media;

            localDataManager.MediaRepository.Add(machine.Media);
            localDataManager.MachineRepository.Save(machine);
            localDataManager.SaveChanges();

            var machineFromDB = localDataManager.MachineRepository.GetById(machine.Guid);
            mediaVM.Model = machineFromDB;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(media.OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with multiple pictures in Media from DB test.
        /// </summary>
        [TestMethod]
        public void LoadModelMultiplePicturesMediaFromDBTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), this.modelBrowserService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            Media img1 = this.GetMedia(MediaType.Image);
            Media img2 = this.GetMedia(MediaType.Image);
            assembly.Media.Add(img1);
            assembly.Media.Add(img2);

            localDataManager.MediaRepository.Add(img1);
            localDataManager.MediaRepository.Add(img2);
            localDataManager.AssemblyRepository.Save(assembly);
            localDataManager.SaveChanges();

            var assemblyFromDB = localDataManager.AssemblyRepository.GetAssembly(assembly.Guid);
            mediaVM.Model = assemblyFromDB;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with video media from DB test.
        /// </summary>
        [TestMethod]
        public void LoadModelVideoMediaFromDBTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), this.modelBrowserService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            var videoMedia = this.GetMedia(MediaType.Video);
            assembly.Media.Add(videoMedia);

            localDataManager.MediaRepository.Add(videoMedia);
            localDataManager.AssemblyRepository.Save(assembly);
            localDataManager.SaveChanges();

            var assemblyFromDB = localDataManager.AssemblyRepository.GetAssembly(assembly.Guid);
            mediaVM.Model = assemblyFromDB;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaMetaData.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoMedia.OriginalFileName, mediaVM.MediaHandler.MediaSource.MediaMetaData[0].OriginalFileName);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with a single picture in Media in view-mode test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithSinglePictureInViewModeTest()
        {
            var modelBrowserHelperMock = new Mock<IModelBrowserHelperService>();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), modelBrowserHelperMock.Object);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.Mode = MediaControlMode.SingleImage;
            mediaVM.IsInViewerMode = true;
            mediaVM.IsReadOnly = true;

            var machine = new Machine();

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);
            if (!pictureExists)
            {
                return;
            }

            ImportedMedia importedMedia = new ImportedMedia(new Media() { Type = (short)MediaType.Image, OriginalFileName = "Test" }, picturePath);
            modelBrowserHelperMock.Setup(service => service.GetMediaForEntity(machine)).Returns(new List<ImportedMedia>() { importedMedia });
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with multiple pictures in Media in view-mode test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithMultiplePicturesInViewModeTest()
        {
            var modelBrowserHelperMock = new Mock<IModelBrowserHelperService>();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), modelBrowserHelperMock.Object);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;
            mediaVM.IsInViewerMode = true;
            mediaVM.IsReadOnly = true;

            var assembly = new Assembly();

            var picture1Path = Environment.CurrentDirectory + "\\PictureSample.png";
            var picture2Path = Environment.CurrentDirectory + "\\PictureSample2.png";
            bool picturesExists = File.Exists(picture1Path) && File.Exists(picture2Path);
            if (!picturesExists)
            {
                return;
            }

            ImportedMedia importedMedia1 = new ImportedMedia(new Media() { Type = (short)MediaType.Image, OriginalFileName = "Test" }, picture1Path);
            ImportedMedia importedMedia2 = new ImportedMedia(new Media() { Type = (short)MediaType.Image, OriginalFileName = "Test" }, picture2Path);
            modelBrowserHelperMock.Setup(service => service.GetMediaForEntity(assembly)).Returns(new List<ImportedMedia>() { importedMedia1, importedMedia2 });
            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 2);
            Assert.IsTrue(mediaVM.MediaHandler.MediaSource.MediaPaths.Contains(picture1Path));
            Assert.IsTrue(mediaVM.MediaHandler.MediaSource.MediaPaths.Contains(picture2Path));
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Load model with video media in view-mode test.
        /// </summary>
        [TestMethod]
        public void LoadModelWithVideoInViewModeTest()
        {
            var modelBrowserHelperMock = new Mock<IModelBrowserHelperService>();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), modelBrowserHelperMock.Object);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;
            mediaVM.IsInViewerMode = true;
            mediaVM.IsReadOnly = true;

            var assembly = new Assembly();

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);
            if (!videoExists)
            {
                return;
            }

            ImportedMedia importedMedia = new ImportedMedia(new Media() { Type = MediaType.Video, OriginalFileName = "Test" }, videoPath);
            modelBrowserHelperMock.Setup(service => service.GetMediaForEntity(assembly)).Returns(new List<ImportedMedia>() { importedMedia });
            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// SaveToModel a media with single picture.
        /// </summary>
        [TestMethod]
        public void SaveSinglePictureToModelTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.SaveToModelCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// SaveToModel a media with multiple pictures.
        /// </summary>
        [TestMethod]
        public void SaveMultiplePicturesToModelTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, mediaList.Count);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, mediaList.Count);
            for (int i = 0; i < mediaList.Count; i++)
            {
                Assert.AreEqual(mediaList[i].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[i].OriginalFileName);
                Assert.AreEqual(mediaList[i].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[i] as Media).OriginalFileName);
            }

            mediaVM.SaveToModelCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// SaveToModel a media with video.
        /// </summary>
        [TestMethod]
        public void SaveVideoToModelTest()
        {
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            mediaVM.SaveToModelCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Add a single picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void AddPictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                fileDialogService.FileName = picturePath;
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Add an unsupported picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void AddUnsupporterPictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\UnsupportedPicture.bmp";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                fileDialogService.FileName = picturePath;
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Add a video to a ViewModelMedia in SinglePicture mode.
        /// </summary>
        [TestMethod]
        public void AddVideoInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                fileDialogService.FileName = picturePath;
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Add an image to MediaViewModel in MultipleImagesOrVideo mode.
        /// </summary>
        [TestMethod]
        public void AddPictureInMultipleImagesOrVideoModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                fileDialogService.FileNames.Add(picturePath);
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Add a video to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void AddVideoTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);
            if (videoExists)
            {
                fileDialogService.FileNames.Add(videoPath);
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Add and SaveToModel a media.
        /// </summary>
        [TestMethod]
        public void AddAndSaveMediaScenarioTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                fileDialogService.FileName = picturePath;
                ICommand addCommand = mediaVM.AddMediaCommand;
                addCommand.Execute(null);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);

            mediaVM.SaveCommand.Execute(null);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeAdded.Count, 0);
            Assert.AreEqual(mediaVM.MediaHandler.MediaToBeDeleted.Count, 0);
        }

        /// <summary>
        /// Cancel changes in a MediaVideModel with pictures.
        /// </summary>
        [TestMethod]
        public void CancelPictureMediaTest()
        {
            var fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.SingleImage, MediaType.Image);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            var cancelCommand = mediaVM.CancelCommand;
            cancelCommand.Execute(null);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.IsNull(mediaVM.MediaHandler);
        }

        /// <summary>
        /// Cancel changes in a MediaVideModel with video.
        /// </summary>
        [TestMethod]
        public void CancelVideoMediaTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;
            mediaVM.ViewLoadedCommand.Execute(true);
            this.DelayUntilVMIsLoaded(mediaVM);

            var mediaList = this.CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            mediaVM.LoadMedia(mediaList);

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.Media.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(mediaList[0].OriginalFileName, mediaVM.MediaHandler.MediaSource.Media[0].OriginalFileName);
            Assert.AreEqual(mediaList[0].OriginalFileName, (mediaVM.MediaHandler.MediaToBeAdded[0] as Media).OriginalFileName);

            ICommand cancelCommand = mediaVM.CancelCommand;
            cancelCommand.Execute(null);
            this.DelayUntilVMIsLoaded(mediaVM);

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// OnUnloaded test.
        /// </summary>
        [TestMethod]
        public void OnUnloadedTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            mediaVM.OnUnloaded();
        }

        /// <summary>
        /// Paste a single picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void PastePictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var listPath = new StringCollection();
                listPath.Add(picturePath);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Paste an image to MediaViewModel in MultipleImagesOrVideo mode.
        /// </summary>
        [TestMethod]
        public void PastePictureInMultipleImagesOrVideoModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var listPath = new StringCollection();
                listPath.Add(picturePath);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Paste multiple images to MediaViewModel in MultipleImagesOrVideo mode.
        /// </summary>
        [TestMethod]
        public void PasteMultiplePicturesInMultipleImagesOrVideoModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath1 = Environment.CurrentDirectory + "\\PictureSample.png";
            var picturePath2 = Environment.CurrentDirectory + "\\PictureSample2.png";

            if (File.Exists(picturePath1) && File.Exists(picturePath2))
            {
                var listPath = new StringCollection();
                listPath.Add(picturePath1);
                listPath.Add(picturePath2);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 2);
            Assert.AreEqual(picturePath1, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath1, mediaVM.MediaHandler.MediaSource.AllSources[0]);
            Assert.AreEqual(picturePath2, mediaVM.MediaHandler.MediaSource.MediaPaths[1]);
            Assert.AreEqual(picturePath2, mediaVM.MediaHandler.MediaSource.AllSources[1]);
        }

        /// <summary>
        /// Paste an unsupported picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void PasteUnsupporterPictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\UnsupportedPicture.bmp";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var listPath = new StringCollection();
                listPath.Add(picturePath);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Paste a video to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void PasteVideoTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);
            if (videoExists)
            {
                var listPath = new StringCollection();
                listPath.Add(videoPath);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Paste a video to a ViewModelMedia in SinglePicture mode.
        /// </summary>
        [TestMethod]
        public void PasteVideoInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool pictureExists = File.Exists(videoPath);

            if (pictureExists)
            {
                var listPath = new StringCollection();
                listPath.Add(videoPath);
                Clipboard.SetFileDropList(listPath);
                if (mediaVM.CanPaste())
                {
                    mediaVM.PasteCommand.Execute(null);
                }
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Drop a video to a ViewModelMedia in SinglePicture mode.
        /// </summary>
        [TestMethod]
        public void DropVideoInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);

            if (videoExists)
            {
                var fileList = new string[] { videoPath };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Drop a single picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void DropPictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var fileList = new string[] { picturePath };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Drop an image to MediaViewModel in MultipleImagesOrVideo mode.
        /// </summary>
        [TestMethod]
        public void DropPictureInMultipleImagesOrVideoModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var fileList = new string[] { picturePath };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Drop multiple images to MediaViewModel in MultipleImagesOrVideo mode.
        /// </summary>
        [TestMethod]
        public void DropMultiplePicturesInMultipleImagesOrVideoModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var picturePath1 = Environment.CurrentDirectory + "\\PictureSample.png";
            var picturePath2 = Environment.CurrentDirectory + "\\PictureSample2.png";

            if (File.Exists(picturePath1) && File.Exists(picturePath2))
            {
                var fileList = new string[] { picturePath1, picturePath2 };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 2);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 2);
            Assert.AreEqual(picturePath1, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath1, mediaVM.MediaHandler.MediaSource.AllSources[0]);
            Assert.AreEqual(picturePath2, mediaVM.MediaHandler.MediaSource.MediaPaths[1]);
            Assert.AreEqual(picturePath2, mediaVM.MediaHandler.MediaSource.AllSources[1]);
        }

        /// <summary>
        /// Drop an unsupported picture to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void DropUnsupporterPictureInSinglePictureModeTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\UnsupportedPicture.bmp";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var fileList = new string[] { picturePath };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.IsNull(mediaVM.MediaHandler, null);
        }

        /// <summary>
        /// Drop a video to MediaViewModel.
        /// </summary>
        [TestMethod]
        public void DropVideoTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);

            if (videoExists)
            {
                var fileList = new string[] { videoPath };

                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(fileList);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Drop a single picture to MediaViewModel when the file list source is not a string[] but a string
        /// </summary>
        [TestMethod]
        public void DropPictureWithStringFileListTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.SingleImage;

            var machine = this.CreateNewMachine(localDataManager);
            mediaVM.Model = machine;

            var picturePath = Environment.CurrentDirectory + "\\PictureSample.png";
            bool pictureExists = File.Exists(picturePath);

            if (pictureExists)
            {
                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(picturePath);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(picturePath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        /// <summary>
        /// Drop a video to MediaViewModel when the file list source is not a string[] but a string
        /// </summary>
        [TestMethod]
        public void DropVideoWithStringFileListTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var mediaVM = new MediaViewModel(videoVM, picturesVM, messenger, windowService, fileDialogService, new ModelBrowserHelperServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            mediaVM.DataSourceManager = localDataManager;
            mediaVM.Mode = MediaControlMode.MultipleImagesOrVideo;

            var assembly = this.CreateNewAssembly(localDataManager);
            mediaVM.Model = assembly;

            var videoPath = Environment.CurrentDirectory + "\\VideoSample.wmv";
            bool videoExists = File.Exists(videoPath);

            if (videoExists)
            {
                var dataobjmock = new Mock<DataObjectForMock>();
                dataobjmock.Setup(m => m.GetData(DataFormats.FileDrop)).Returns(videoPath);

                var mockEventArgs = RoutedEventUtils.CreateDragEventArgsInstance(dataobjmock.Object);
                mediaVM.CheckDropTargetCommand.Execute(mockEventArgs);
                mediaVM.DropCommand.Execute(mockEventArgs);
            }

            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.MediaPaths.Count, 1);
            Assert.AreEqual(mediaVM.MediaHandler.MediaSource.AllSources.Count, 1);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.MediaPaths[0]);
            Assert.AreEqual(videoPath, mediaVM.MediaHandler.MediaSource.AllSources[0]);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// Create a new Media and save it to DB.
        /// </summary>
        /// <param name="dataManager">Data Manager source.</param>
        /// <param name="mediaMode">Media Mode.</param>
        /// <param name="mediaType">Media Type.</param>
        /// <returns>A list of media.</returns>
        private List<Media> CreateNewMedia(IDataSourceManager dataManager, MediaControlMode mediaMode, MediaType mediaType)
        {
            var medialist = new List<Media>();
            var media = new Media();

            switch (mediaMode)
            {
                case MediaControlMode.SingleImage:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                case MediaControlMode.MultipleImagesOrVideo:
                    {
                        if (mediaType == MediaType.Image)
                        {
                            medialist.Add(this.GetMedia(MediaType.Image));
                            medialist.Add(this.GetMedia(MediaType.Image));
                        }

                        if (mediaType == MediaType.Video)
                        {
                            medialist.Add(this.GetMedia(MediaType.Video));
                        }

                        break;
                    }

                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                default:
                    break;
            }

            foreach (var mediaItem in medialist)
            {
                dataManager.MediaRepository.Add(mediaItem);
            }

            dataManager.SaveChanges();
            return medialist;
        }

        /// <summary>
        /// Create and save to DB a new machine.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created machine.</returns>
        private Machine CreateNewMachine(IDataSourceManager dataManager)
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;

            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// Create and save to DB a new assembly.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created assembly.</returns>
        private Assembly CreateNewAssembly(IDataSourceManager dataManager)
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            dataManager.AssemblyRepository.Save(assembly);
            dataManager.SaveChanges();

            return assembly;
        }

        /// <summary>
        /// Get a media object.
        /// </summary>
        /// <param name="type">Media Type.</param>
        /// <returns>The created media.</returns>
        private Media GetMedia(MediaType type)
        {
            var media = new Media();
            media.Type = type;
            media.Content = new byte[] { 1, 1, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();

            return media;
        }

        /// <summary>
        /// Delay the operations until the view-models passed as parameter is loaded.
        /// </summary>
        /// <param name="viewModel">The view-model.</param>
        private void DelayUntilVMIsLoaded(ViewModel viewModel)
        {
            while (!viewModel.IsLoaded)
            {
                Thread.Sleep(100);
            }
        }

        #endregion Private Methods
    }
}