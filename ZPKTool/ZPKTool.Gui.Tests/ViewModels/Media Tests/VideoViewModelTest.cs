﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// VideoViewModelTest class.
    /// </summary>
    [TestClass]
    public class VideoViewModelTest
    {
        #region Fields

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VideoViewModelTest"/> class.
        /// </summary>
        public VideoViewModelTest()
        {
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        /// <value>
        /// The test context.
        /// </value>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
            this.messenger = new Messenger();
            this.modelBrowserService = new ModelBrowserHelperServiceMock();

            var compositionContainer = new CompositionContainer();
        }

        //// Use TestCleanup to run code after each test has run
        ////[TestCleanup]
        ////public void MyTestCleanup()
        ////{            
        ////}

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// VideoPlayer Clicked command test.
        /// </summary>
        [TestMethod]
        public void VideoPlayerClickedTest()
        {
            var videoVM = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            videoVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            videoVM.MediaSource.Media.Add(mediaList[0]);

            bool mediaAdded = false;

            messenger.Register<NotificationMessage>(
            (msg) =>
            {
                if (msg.Notification == Notification.AddMedia)
                {
                    mediaAdded = true;
                }
            });

            ICommand playerClickCommand = videoVM.VideoPlayerClickedCommand;
            playerClickCommand.Execute(null);

            Assert.IsTrue(mediaAdded);
            Assert.AreEqual(videoVM.MediaToBeAdded.Count, 0);
        }

        /// <summary>
        /// VideoViewModel delete current media command test.
        /// </summary>
        [TestMethod]
        public void DeleteCurrentMediaTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_DeletePicture, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);
            var videoVM = new VideoViewModel(messenger, wndServiceMock.Object, new FileDialogServiceMock());
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            videoVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            videoVM.MediaSource.Media.Add(mediaList[0]);

            bool mediaDeleted = false;

            messenger.Register<NotificationMessage>(
            (msg) =>
            {
                if (msg.Notification == Notification.MediaDeleted)
                {
                    mediaDeleted = true;
                }
            });

            ICommand deleteMediaCommand = videoVM.DeleteCurrentMediaCommand;
            deleteMediaCommand.Execute(null);

            Assert.IsTrue(mediaDeleted);
        }

        /// <summary>
        /// VideoViewModel save as command test.
        /// </summary>
        [TestMethod]
        public void SaveAsVideoTest()
        {
            FileDialogServiceMock fileDialogService = new FileDialogServiceMock();
            var videoVM = new VideoViewModel(messenger, windowService, fileDialogService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            videoVM.DataContext = localDataManager;

            var mediaList = CreateNewMedia(localDataManager, MediaControlMode.MultipleImagesOrVideo, MediaType.Video);
            videoVM.MediaSource.Media.Add(mediaList[0]);

            ICommand saveAsCommand = videoVM.SaveMediaAsCommand;
            saveAsCommand.Execute(null);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// Create a new Media and save it to DB.
        /// </summary>
        /// <param name="dataManager">Data Manager source.</param>
        /// <param name="mediaMode">Media Mode.</param>
        /// <param name="mediaType">Media Type.</param>
        /// <returns>A list of media.</returns>
        private List<Media> CreateNewMedia(IDataSourceManager dataManager, MediaControlMode mediaMode, MediaType mediaType)
        {
            var medialist = new List<Media>();
            var media = new Media();

            switch (mediaMode)
            {
                case MediaControlMode.SingleImage:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                case MediaControlMode.MultipleImagesOrVideo:
                    {
                        if (mediaType == MediaType.Image)
                        {
                            medialist.Add(this.GetMedia(MediaType.Image));
                            medialist.Add(this.GetMedia(MediaType.Image));
                        }

                        if (mediaType == MediaType.Video)
                        {
                            medialist.Add(this.GetMedia(MediaType.Video));
                        }

                        break;
                    }

                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        medialist.Add(this.GetMedia(MediaType.Image));
                        break;
                    }

                default:
                    break;
            }

            foreach (var mediaItem in medialist)
            {
                dataManager.MediaRepository.Add(mediaItem);
            }

            dataManager.SaveChanges();
            return medialist;
        }

        /// <summary>
        /// Get a media object.
        /// </summary>
        /// <param name="type">Media Type.</param>
        /// <returns>The created media.</returns>
        private Media GetMedia(MediaType type)
        {
            var media = new Media();
            media.Type = type;
            media.Content = new byte[] { 1, 1, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();

            return media;
        }

        #endregion Private Methods
    }
}