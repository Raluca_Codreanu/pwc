﻿using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// MachineViewModelTest class.
    /// </summary>
    [TestClass]
    public class MachineViewModelTest
    {
        #region Fields

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserService;

        /// <summary>
        /// The cost recalculation Clone Manager.
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The manufacturer view-model.
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The MasterDataBrowser view-model.
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The media view-model.
        /// </summary>
        private MediaViewModel media;

        /// <summary>
        /// The Machine classifications view-model.
        /// </summary>
        private ClassificationSelectorViewModel machineClassifications;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The units service
        /// </summary>
        private IUnitsService unitsService;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineViewModelTest"/> class.
        /// </summary>
        public MachineViewModelTest()
        {
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        /// <value>
        /// The test context.
        /// </value>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            CostCalculatorFactory.SetBasicSettings(new BasicSetting());
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
            this.messenger = new Messenger();
            this.modelBrowserService = new ModelBrowserHelperServiceMock();
            this.machineClassifications = new ClassificationSelectorViewModel();
            this.unitsService = new UnitsServiceMock();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();

            var compositionContainer = new CompositionContainer();
            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            this.masterDataBrowser = new MasterDataBrowserViewModel(compositionContainer, messenger, windowService, new OnlineCheckService(messenger), this.unitsService);
            this.media = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            this.manufacturer = new ManufacturerViewModel(windowService, messenger, compositionContainer);
        }

        //// Use TestCleanup to run code after each test has run
        ////[TestCleanup]
        ////public void MyTestCleanup()
        ////{            
        ////}

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests a MachineViewModel edit scenario.
        /// </summary>
        [TestMethod]
        public void EditMachineTest()
        {
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            machineVM.DataSourceManager = localDataManager;

            var user = this.CreateNewUser(localDataManager);
            var machine = this.CreateNewMachine(localDataManager);

            var machineCalcParams = new MachineCostCalculationParameters();
            machineCalcParams.CountrySettingsAirCost = 1m;
            machineCalcParams.CountrySettingsEnergyCost = 1m;
            machineCalcParams.CountrySettingsWaterCost = 1m;
            machineCalcParams.CountrySettingsRentalCostProductionArea = 1m;
            machineCalcParams.ProjectDepreciationPeriod = 5;
            machineCalcParams.ProjectDepreciationRate = 5;
            machineCalcParams.ProcessStepHoursPerShift = 1m;
            machineCalcParams.ProcessStepProductionWeeksPerYear = 1m;
            machineCalcParams.ProcessStepShiftsPerWeek = 1m;
            machineCalcParams.ProcessStepExtraShiftsPerWeek = 1m;

            machine.SetOwner(user);
            machineVM.Model = new Machine();
            machineVM.Model = machine;
            machineVM.MachineCalculationParams = machineCalcParams;
            machineVM.CostCalculationVersion = "1.3";
            machineVM.MachineClassificationsView.ClassificationLevels = new System.Collections.ObjectModel.ObservableCollection<object>();
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.ManufacturingYear.Value = 2000;
            machineVM.DepreciationPeriod.Value = 10;
            machineVM.DepreciationRate.Value = 0.4m;
            machineVM.RegistrationNumber.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.DescriptionOfFunction.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.FloorSize.Value = 1m;
            machineVM.WorkspaceArea.Value = 2m;
            machineVM.AirConsumption.Value = 3m;
            machineVM.WaterConsumption.Value = 4m;
            machineVM.FullLoadRate.Value = 5m;
            machineVM.FossilEnergyConsumptionRatio.Value = 6m;
            machineVM.MaxCapacity.Value = 7;
            machineVM.OEE.Value = 8m;
            machineVM.Availability.Value = 9m;
            machineVM.MachineInvestment.Value = 1m;
            machineVM.SetupInvestment.Value = 2m;
            machineVM.AdditionalEquipmentInvestment.Value = 3m;
            machineVM.FundamentalSetupInvestment.Value = 4m;
            machineVM.InvestmentRemarks.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.ExternalWorkCost.Value = 5m;
            machineVM.MaterialCost.Value = 6m;
            machineVM.RepairsCost.Value = 7m;
            machineVM.KValue.Value = 5m;
            machineVM.CalculateWithKValue.Value = true;
            machineVM.MountingCubeLength.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MountingCubeWidth.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MountingCubeHeight.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaxDiameterRodPerChuckMaxThickness.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaxPartsWeightPerCapacity.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.LockingForce.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.PressCapacity.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaximumSpeed.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.RapidFeedPerCuttingSpeed.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.StrokeRate.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.Other.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.CalculateWithKValue.Value = false;
            machineVM.ManualConsumableCostCalc.Value = true;
            machineVM.RenewableEnergyConsumptionRatio.Value = 0.2m;

            var browseMDCommand = machineVM.BrowseMasterDataCommand;
            browseMDCommand.Execute(null);
            var cloaseMDCommand = machineVM.MasterDataBrowser.CloseCommand;
            cloaseMDCommand.Execute(null);

            var saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(machine.Name, machineVM.Name.Value);
            Assert.AreEqual(machine.ManufacturingYear, machineVM.ManufacturingYear.Value);
            Assert.AreEqual(machine.DepreciationPeriod, machineVM.DepreciationPeriod.Value);
            Assert.AreEqual(machine.DepreciationRate, machineVM.DepreciationRate.Value);
            Assert.AreEqual(machine.RegistrationNumber, machineVM.RegistrationNumber.Value);
            Assert.AreEqual(machine.DescriptionOfFunction, machineVM.DescriptionOfFunction.Value);
            Assert.AreEqual(machine.FloorSize, machineVM.FloorSize.Value);
            Assert.AreEqual(machine.WorkspaceArea, machineVM.WorkspaceArea.Value);
            Assert.AreEqual(machine.AirConsumption, machineVM.AirConsumption.Value);
            Assert.AreEqual(machine.WaterConsumption, machineVM.WaterConsumption.Value);
            Assert.AreEqual(machine.FullLoadRate, machineVM.FullLoadRate.Value);
            Assert.AreEqual(machine.FossilEnergyConsumptionRatio, machineVM.FossilEnergyConsumptionRatio.Value);
            Assert.AreEqual(machine.MaxCapacity, machineVM.MaxCapacity.Value);
            Assert.AreEqual(machine.OEE, machineVM.OEE.Value);
            Assert.AreEqual(machine.Availability, machineVM.Availability.Value);
            Assert.AreEqual(machine.MachineInvestment, machineVM.MachineInvestment.Value);
            Assert.AreEqual(machine.SetupInvestment, machineVM.SetupInvestment.Value);
            Assert.AreEqual(machine.AdditionalEquipmentInvestment, machineVM.AdditionalEquipmentInvestment.Value);
            Assert.AreEqual(machine.FundamentalSetupInvestment, machineVM.FundamentalSetupInvestment.Value);
            Assert.AreEqual(machine.InvestmentRemarks, machineVM.InvestmentRemarks.Value);
            Assert.AreEqual(machine.ExternalWorkCost, machineVM.ExternalWorkCost.Value);
            Assert.AreEqual(machine.MaterialCost, machineVM.MaterialCost.Value);
            Assert.AreEqual(machine.ConsumablesCost, machineVM.ConsumablesCost.Value);
            Assert.AreEqual(machine.RepairsCost, machineVM.RepairsCost.Value);
            Assert.AreEqual(machine.KValue, machineVM.KValue.Value);
            Assert.AreEqual(machine.CalculateWithKValue, machineVM.CalculateWithKValue.Value);
            Assert.AreEqual(machine.MountingCubeLength, machineVM.MountingCubeLength.Value);
            Assert.AreEqual(machine.MountingCubeWidth, machineVM.MountingCubeWidth.Value);
            Assert.AreEqual(machine.MountingCubeHeight, machineVM.MountingCubeHeight.Value);
            Assert.AreEqual(machine.MaxDiameterRodPerChuckMaxThickness, machineVM.MaxDiameterRodPerChuckMaxThickness.Value);
            Assert.AreEqual(machine.MaxPartsWeightPerCapacity, machineVM.MaxPartsWeightPerCapacity.Value);
            Assert.AreEqual(machine.LockingForce, machineVM.LockingForce.Value);
            Assert.AreEqual(machine.PressCapacity, machineVM.PressCapacity.Value);
            Assert.AreEqual(machine.MaximumSpeed, machineVM.MaximumSpeed.Value);
            Assert.AreEqual(machine.RapidFeedPerCuttingSpeed, machineVM.RapidFeedPerCuttingSpeed.Value);
            Assert.AreEqual(machine.StrokeRate, machineVM.StrokeRate.Value);
            Assert.AreEqual(machine.Other, machineVM.Other.Value);
        }

        /// <summary>
        /// Tests MachineViewModel load model in ViewMode.
        /// </summary>
        [TestMethod]
        public void LoadMachineInViewModelTest()
        {
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var machine = this.CreateNewMachine(localDataManager);

            machineVM.DataSourceManager = localDataManager;
            machineVM.IsInViewerMode = true;
            machineVM.IsReadOnly = true;
            machineVM.Model = machine;

            var closeCommand = machineVM.CloseCommand;
            closeCommand.Execute(null);

            Assert.IsTrue(machineVM.ManufacturerViewModel.IsReadOnly);
            Assert.IsTrue(machineVM.MediaViewModel.IsReadOnly);
            Assert.IsTrue(machineVM.MachineClassificationsView.IsReadOnly);
            Assert.IsNotNull(machineVM.MachineClassificationsView.ClassificationLevels);
        }

        /// <summary>
        /// Tests MachineViewModel SaveCommand.
        /// </summary>
        [TestMethod]
        public void SaveMachineTest()
        {
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);
            var machine = this.CreateNewMachine(localDataManager);

            machineVM.Model = machine;
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.ManufacturingYear.Value = 2000;
            machineVM.DepreciationPeriod.Value = 10;
            machineVM.DepreciationRate.Value = 0.4m;
            machineVM.RegistrationNumber.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.DescriptionOfFunction.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.FloorSize.Value = 1m;
            machineVM.WorkspaceArea.Value = 2m;
            machineVM.AirConsumption.Value = 3m;
            machineVM.WaterConsumption.Value = 4m;
            machineVM.FullLoadRate.Value = 5m;
            machineVM.FossilEnergyConsumptionRatio.Value = 6m;
            machineVM.MaxCapacity.Value = 7;
            machineVM.OEE.Value = 8m;
            machineVM.Availability.Value = 9m;
            machineVM.MachineInvestment.Value = 1m;
            machineVM.SetupInvestment.Value = 2m;
            machineVM.AdditionalEquipmentInvestment.Value = 3m;
            machineVM.FundamentalSetupInvestment.Value = 4m;
            machineVM.InvestmentRemarks.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.ExternalWorkCost.Value = 5m;
            machineVM.MaterialCost.Value = 6m;
            machineVM.RepairsCost.Value = 7m;
            machineVM.KValue.Value = 5m;
            machineVM.CalculateWithKValue.Value = true;
            machineVM.MountingCubeLength.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MountingCubeWidth.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MountingCubeHeight.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaxDiameterRodPerChuckMaxThickness.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaxPartsWeightPerCapacity.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.LockingForce.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.PressCapacity.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.MaximumSpeed.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.RapidFeedPerCuttingSpeed.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.StrokeRate.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.Other.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            ICommand saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(machine.Name, machineVM.Name.Value);
            Assert.AreEqual(machine.ManufacturingYear, machineVM.ManufacturingYear.Value);
            Assert.AreEqual(machine.DepreciationPeriod, machineVM.DepreciationPeriod.Value);
            Assert.AreEqual(machine.DepreciationRate, machineVM.DepreciationRate.Value);
            Assert.AreEqual(machine.RegistrationNumber, machineVM.RegistrationNumber.Value);
            Assert.AreEqual(machine.DescriptionOfFunction, machineVM.DescriptionOfFunction.Value);
            Assert.AreEqual(machine.FloorSize, machineVM.FloorSize.Value);
            Assert.AreEqual(machine.WorkspaceArea, machineVM.WorkspaceArea.Value);
            Assert.AreEqual(machine.AirConsumption, machineVM.AirConsumption.Value);
            Assert.AreEqual(machine.WaterConsumption, machineVM.WaterConsumption.Value);
            Assert.AreEqual(machine.FullLoadRate, machineVM.FullLoadRate.Value);
            Assert.AreEqual(machine.FossilEnergyConsumptionRatio, machineVM.FossilEnergyConsumptionRatio.Value);
            Assert.AreEqual(machine.MaxCapacity, machineVM.MaxCapacity.Value);
            Assert.AreEqual(machine.OEE, machineVM.OEE.Value);
            Assert.AreEqual(machine.Availability, machineVM.Availability.Value);
            Assert.AreEqual(machine.MachineInvestment, machineVM.MachineInvestment.Value);
            Assert.AreEqual(machine.SetupInvestment, machineVM.SetupInvestment.Value);
            Assert.AreEqual(machine.AdditionalEquipmentInvestment, machineVM.AdditionalEquipmentInvestment.Value);
            Assert.AreEqual(machine.FundamentalSetupInvestment, machineVM.FundamentalSetupInvestment.Value);
            Assert.AreEqual(machine.InvestmentRemarks, machineVM.InvestmentRemarks.Value);
            Assert.AreEqual(machine.ExternalWorkCost, machineVM.ExternalWorkCost.Value);
            Assert.AreEqual(machine.MaterialCost, machineVM.MaterialCost.Value);
            Assert.AreEqual(machine.ConsumablesCost, machineVM.ConsumablesCost.Value);
            Assert.AreEqual(machine.RepairsCost, machineVM.RepairsCost.Value);
            Assert.AreEqual(machine.KValue, machineVM.KValue.Value);
            Assert.AreEqual(machine.CalculateWithKValue, machineVM.CalculateWithKValue.Value);
            Assert.AreEqual(machine.MountingCubeLength, machineVM.MountingCubeLength.Value);
            Assert.AreEqual(machine.MountingCubeWidth, machineVM.MountingCubeWidth.Value);
            Assert.AreEqual(machine.MountingCubeHeight, machineVM.MountingCubeHeight.Value);
            Assert.AreEqual(machine.MaxDiameterRodPerChuckMaxThickness, machineVM.MaxDiameterRodPerChuckMaxThickness.Value);
            Assert.AreEqual(machine.MaxPartsWeightPerCapacity, machineVM.MaxPartsWeightPerCapacity.Value);
            Assert.AreEqual(machine.LockingForce, machineVM.LockingForce.Value);
            Assert.AreEqual(machine.PressCapacity, machineVM.PressCapacity.Value);
            Assert.AreEqual(machine.MaximumSpeed, machineVM.MaximumSpeed.Value);
            Assert.AreEqual(machine.RapidFeedPerCuttingSpeed, machineVM.RapidFeedPerCuttingSpeed.Value);
            Assert.AreEqual(machine.StrokeRate, machineVM.StrokeRate.Value);
            Assert.AreEqual(machine.Other, machineVM.Other.Value);
        }

        /// <summary>
        /// Tests MachineViewModel aborted CancelCommand.
        /// </summary>
        [TestMethod]
        public void CancelCommandAbortedTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo)).Returns(MessageDialogResult.No);

            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);
            var machine = this.CreateNewMachine(localDataManager);
            var machineName = machine.Name;
            machineVM.Model = machine;
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var cancelCommand = machineVM.CancelCommand;
            cancelCommand.Execute(null);

            Assert.IsTrue(machineVM.IsChanged);
            Assert.AreNotEqual(machineVM.Name.Value, machineName);
        }

        /// <summary>
        /// Tests MachineViewModel CancelCommand.
        /// </summary>
        [TestMethod]
        public void CancelCommandSuccessfullyTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);
            var machine = this.CreateNewMachine(localDataManager);
            var machineName = machine.Name;
            machineVM.Model = machine;
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var cancelCommand = machineVM.CancelCommand;
            cancelCommand.Execute(null);

            Assert.IsFalse(machineVM.IsChanged);
            Assert.AreEqual(machineVM.Name.Value, machineName);
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with a new unmodified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingUnmodifiedNewMachineTest()
        {
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            this.CreateNewUser(localDataManager);

            machineVM.InitializeForCreation(false, new Project());
            machineVM.OnUnloading();

            Assert.IsFalse(machineVM.Saved);
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with a new modified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedNewMachineSuccessfullyTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo)).Returns(MessageDialogResult.No);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);

            machineVM.InitializeForCreation(false, new Project());
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.OnUnloading();

            Assert.IsNotNull(machineVM.Model, "Failed to return to view model.");
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with a new modified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedNewMachineCanceledTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);

            machineVM.InitializeForCreation(false, new Project());
            var machine = machineVM.Model;
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.OnUnloading();

            Assert.IsFalse(machineVM.Saved);
            Assert.IsNull(localDataManager.MachineRepository.GetByKey(machine.Guid), "Failed to remove the machine from context.");
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with an existing unmodified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingUnmodifiedExistingMachineTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var machine = this.CreateNewMachine(localDataManager);
            this.CreateNewUser(localDataManager);

            machineVM.Model = machine;
            machineVM.OnUnloading();

            Assert.IsFalse(machineVM.IsChanged);
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with an existing modified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedExistingMachineSuccessfullyTest()
        {
            var machineName = EncryptionManager.Instance.GenerateRandomString(10, true);

            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var machine = this.CreateNewMachine(localDataManager);
            this.CreateNewUser(localDataManager);

            machineVM.Model = machine;
            machine.Name = machineName;
            machineVM.IsChanged = true;
            machineVM.OnUnloading();

            Assert.IsFalse(machineVM.IsChanged);
            Assert.AreNotEqual(machine.Name, machineName);
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with an existing modified machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedExistingMachineCanceledTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel)).Returns(MessageDialogResult.No);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var machine = this.CreateNewMachine(localDataManager);
            this.CreateNewUser(localDataManager);

            machineVM.Model = machine;
            machineVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            machineVM.DescriptionOfFunction.Value = "test";
            machineVM.OnUnloading();

            Assert.IsFalse(machineVM.IsChanged);
        }

        /// <summary>
        /// Tests MachineViewModel OnUnloading with an invalid machine.
        /// </summary>
        [TestMethod]
        public void OnUnloadingInvalidMachineTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var machineVM = new MachineViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            machineVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);
            var machine = this.CreateNewMachine(localDataManager);

            machineVM.Model = machine;
            machineVM.Name.Value = string.Empty;
            machineVM.OnUnloading();

            Assert.IsTrue(machineVM.IsChanged);
            Assert.AreEqual(machineVM.Name.Value, string.Empty);
        }

        /// <summary>
        /// Tests MachineViewModel RefreshParentCost.
        /// </summary>
        [TestMethod]
        public void RefreshParentCostTest()
        {
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var machine = this.CreateNewAssemblyMachine(localDataManager);

            machineVM.DataSourceManager = localDataManager;
            machineVM.Model = machine;
            machineVM.MachineParent = new Part() { OverheadSettings = new OverheadSetting(), CountrySettings = new CountrySetting() };
            machineVM.ParentProject = new Project();

            var projectNotified = false;
            messenger.Register<CurrentComponentCostChangedMessage>(
            (msg) =>
            {
                projectNotified = true;
            },
            GlobalMessengerTokens.MainViewTargetToken);

            var saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
            projectNotified = false;

            Project machineParentProject;
            Part machineParentPart;
            this.CreateNewPartMachine(localDataManager, out machineParentProject, out machineParentPart);
            machineVM.ParentProject = machineParentProject;
            machineVM.MachineParent = machineParentPart;
            machineVM.Model = machine;

            saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
        }

        /// <summary>
        /// Tests MachineViewModel RefreshParentCost for a machine in view-mode.
        /// </summary>
        [TestMethod]
        public void RefreshParentCostInViewModeTest()
        {
            var machineVM = new MachineViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                machineClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var machine = this.CreateNewAssemblyMachine(localDataManager);

            machineVM.DataSourceManager = localDataManager;
            machineVM.IsInViewerMode = true;
            machineVM.Model = machine;
            machineVM.MachineParent = new Part() { OverheadSettings = new OverheadSetting(), CountrySettings = new CountrySetting() };
            machineVM.ParentProject = new Project();

            var projectNotified = false;
            messenger.Register<CurrentComponentCostChangedMessage>(
            (msg) =>
            {
                projectNotified = true;
            },
            GlobalMessengerTokens.ModelBrowserTargetToken);

            var saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
            projectNotified = false;

            Project machineParentProject;
            Part machineParentPart;
            this.CreateNewPartMachine(localDataManager, out machineParentProject, out machineParentPart);
            machineVM.ParentProject = machineParentProject;
            machineVM.MachineParent = machineParentPart;
            machineVM.Model = machine;

            saveCommand = machineVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// Create and save to DB a new machine.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created machine.</returns>
        private Machine CreateNewMachine(IDataSourceManager dataManager)
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;

            var mainClassification = new MachinesClassification() { Name = "Main Classification" };
            var typeClassification = new MachinesClassification() { Name = "Type Classification" };
            var subClassification = new MachinesClassification() { Name = "Sub Classification" };
            var classificationLevel4 = new MachinesClassification() { Name = "Level 4 Classification" };

            machine.MainClassification = mainClassification;
            machine.TypeClassification = typeClassification;
            machine.SubClassification = subClassification;
            machine.ClassificationLevel4 = classificationLevel4;

            dataManager.MachinesClassificationRepository.Save(mainClassification);
            dataManager.MachinesClassificationRepository.Save(typeClassification);
            dataManager.MachinesClassificationRepository.Save(subClassification);
            dataManager.MachinesClassificationRepository.Save(classificationLevel4);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// Create and save to DB a new assembly machine.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created machine.</returns>
        private Machine CreateNewAssemblyMachine(IDataSourceManager dataManager)
        {
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Process process = new Process();
            assembly.Process = process;

            ProcessStep processStep = new AssemblyProcessStep();
            processStep.Name = processStep.Guid.ToString();
            process.Steps.Add(processStep);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;
            processStep.Machines.Add(machine);

            dataManager.ProjectRepository.Save(project);
            dataManager.AssemblyRepository.Save(assembly);
            dataManager.ProcessRepository.Save(process);
            dataManager.ProcessStepRepository.Save(processStep);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// Create and save to DB a new part machine.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <param name="projectParent">The machine project parent</param>
        /// <param name="parentPart">The machine part parent</param>
        /// <returns>The created machine.</returns>
        private Machine CreateNewPartMachine(IDataSourceManager dataManager, out Project projectParent, out Part parentPart)
        {
            projectParent = new Project();
            projectParent.Name = projectParent.Guid.ToString();
            projectParent.OverheadSettings = new OverheadSetting();

            parentPart = new Part();
            parentPart.Name = parentPart.Guid.ToString();
            parentPart.CountrySettings = new CountrySetting();
            parentPart.OverheadSettings = new OverheadSetting();
            projectParent.Parts.Add(parentPart);

            Process process = new Process();
            parentPart.Process = process;

            ProcessStep processStep = new PartProcessStep();
            processStep.Name = processStep.Guid.ToString();
            process.Steps.Add(processStep);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;
            processStep.Machines.Add(machine);

            dataManager.ProjectRepository.Save(projectParent);
            dataManager.PartRepository.Save(parentPart);
            dataManager.ProcessRepository.Save(process);
            dataManager.ProcessStepRepository.Save(processStep);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.MachineRepository.Save(machine);
            dataManager.SaveChanges();

            return machine;
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion Private Methods
    }
}