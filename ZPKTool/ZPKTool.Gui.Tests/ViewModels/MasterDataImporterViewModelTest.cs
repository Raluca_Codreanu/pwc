﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// The Master Data Importer view model test.
    /// </summary>
    [TestClass]
    public class MasterDataImporterViewModelTest
    {
        #region Additional test attributes

        /// <summary>
        /// A value indicating whether the import is finished.
        /// </summary>
        private bool isImportFinished;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The Master Data Importer View Model.
        /// </summary>
        private MasterDataImporterViewModel masterDataImporterViewModel;

        #endregion

        /// <summary> 
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messenger = new Messenger();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.fileDialogService = new FileDialogServiceMock();
            this.dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            this.masterDataImporterViewModel = new MasterDataImporterViewModel(this.fileDialogService, this.messenger, this.windowService);
        }

        #region Test methods

        /// <summary>
        /// Imports an editing master data with success test.
        /// </summary>
        [TestMethod]
        public void ImportAnEditingMasterDataWithSuccessTest()
        {
            this.masterDataImporterViewModel = new MasterDataImporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            this.isImportFinished = false;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);

            this.masterDataImporterViewModel.DataManager = this.dataManager;
            this.masterDataImporterViewModel.EntityType = ImportedEntityType.Currency;
            this.masterDataImporterViewModel.ImportMasterData = true;

            string path = Path.Combine(Environment.CurrentDirectory, "EditedCurrency.xls");
            this.masterDataImporterViewModel.FilePath.Value = path;
            this.masterDataImporterViewModel.FileType = MasterDataImporterFileType.Xls;

            ICommand importEntity = this.masterDataImporterViewModel.ExportImportCommand;
            importEntity.Execute(null);

            while (!this.isImportFinished)
            {
                Thread.Sleep(100);
            }

            // The exchange rate of the "Algerian Dinar" base currency was edited and needs to be "90.815".
            var currencyEdited = this.dataManager.CurrencyRepository.GetByIsoCode("DZD");
            Assert.AreEqual(currencyEdited.ExchangeRate, 90.815m);
        }

        /// <summary>
        /// A test for the import master data after added a new entity with success test.
        /// </summary>
        [TestMethod]
        public void ImportANewMasterDataWithSuccessTest()
        {
            this.masterDataImporterViewModel = new MasterDataImporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            this.isImportFinished = false;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);

            this.masterDataImporterViewModel.DataManager = this.dataManager;
            this.masterDataImporterViewModel.EntityType = ImportedEntityType.Currency;
            this.masterDataImporterViewModel.ImportMasterData = true;

            string path = Path.Combine(Environment.CurrentDirectory, "AddedCurrency.xls");
            this.masterDataImporterViewModel.FilePath.Value = path;
            this.masterDataImporterViewModel.FileType = MasterDataImporterFileType.Xls;

            ICommand importEntity = this.masterDataImporterViewModel.ExportImportCommand;
            importEntity.Execute(null);

            while (!this.isImportFinished)
            {
                Thread.Sleep(100);
            }

            var currency = this.dataManager.CurrencyRepository.GetByIsoCode("NEW");
            Assert.IsNotNull(currency.IsoCode);
        }

        /// <summary>
        /// A test for the import master data after deleted an entity with success test.
        /// </summary>
        [TestMethod]
        public void ImportMasterdataAfterDeletedAnEntityWithSuccessTest()
        {
            this.masterDataImporterViewModel = new MasterDataImporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            this.isImportFinished = false;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);

            this.masterDataImporterViewModel.DataManager = this.dataManager;
            this.masterDataImporterViewModel.EntityType = ImportedEntityType.Currency;
            this.masterDataImporterViewModel.ImportMasterData = true;

            string path = Path.Combine(Environment.CurrentDirectory, "DeletedCurrency.xls");
            this.masterDataImporterViewModel.FilePath.Value = path;
            this.masterDataImporterViewModel.FileType = MasterDataImporterFileType.Xls;

            ICommand importEntity = this.masterDataImporterViewModel.ExportImportCommand;
            importEntity.Execute(null);

            while (!this.isImportFinished)
            {
                Thread.Sleep(100);
            }

            // The currency should not be deleted after the import.
            var currency = this.dataManager.CurrencyRepository.GetByIsoCode("VND");
            Assert.IsNotNull(currency.IsoCode);
        }

        /// <summary>
        /// Exports the master data with success.
        /// </summary>
        [TestMethod]
        public void ExportMasterDataWithSuccessTest()
        {
            this.masterDataImporterViewModel = new MasterDataImporterViewModel(this.fileDialogService, this.messenger, this.windowService);
            this.masterDataImporterViewModel.DataManager = this.dataManager;
            this.masterDataImporterViewModel.EntityType = ImportedEntityType.Currency;
            this.masterDataImporterViewModel.ImportMasterData = false;
            this.masterDataImporterViewModel.ImportMasterData = false;

            string path = Path.Combine(Path.GetTempPath() + "Currency.xls");
            this.masterDataImporterViewModel.FilePath.Value = path;

            ICommand exportEntity = this.masterDataImporterViewModel.ExportImportCommand;
            exportEntity.Execute(null);

            Assert.IsTrue(File.Exists(this.masterDataImporterViewModel.FilePath.Value), "The file does not exist.");
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.ImportMasterDataFinished)
            {
                this.isImportFinished = true;
            }
        }

        #endregion
    }
}
