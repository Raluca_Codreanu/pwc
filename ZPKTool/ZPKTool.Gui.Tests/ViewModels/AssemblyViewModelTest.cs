﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// Assembly View Model Test
    /// </summary>
    [TestClass]
    public class AssemblyViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The Manufacturer View Model
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The Test Context 
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The Cost Recalculation Clone Manager
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The Entity Document View Model
        /// </summary>
        private EntityDocumentsViewModel documentsViewModel;

        /// <summary>
        /// The Overhead Settings View Model
        /// </summary>
        private OverheadSettingsViewModel ohsettingsViewModel;

        /// <summary>
        /// The Country Settings View Model 
        /// </summary>
        private CountrySettingsViewModel countrySettingsViewModel;

        /// <summary>
        /// The Country and Supplier Browser
        /// </summary>
        private CountryAndSupplierBrowserViewModel countryBrowser;

        /// <summary>
        /// The Master Data Browser View Model 
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The media view-model.
        /// </summary>
        private MediaViewModel media;

        #endregion Atributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext"> Used to indicate text context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup() 
        { 
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            var dialog = new FileDialogServiceMock();
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.messenger = new Messenger();
            this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            this.unitsService = new UnitsServiceMock();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();
            var container = new CompositionContainer();
            this.manufacturer = new ManufacturerViewModel(this.windowService, this.messenger, container);
            var videoViewModel = new VideoViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            this.media = new MediaViewModel(
                videoViewModel,
                picturesViewModel,
                this.messenger,
                this.windowService,
                new FileDialogServiceMock(),
                new ModelBrowserHelperServiceMock());
            this.masterDataBrowser = new MasterDataBrowserViewModel(
                new CompositionContainer(),
                this.messenger,
                this.windowService,
                new OnlineCheckService(this.messenger),
                this.unitsService);
            this.documentsViewModel = new EntityDocumentsViewModel(this.windowService, dialog, this.modelBrowserHelperService);
            this.countrySettingsViewModel = new CountrySettingsViewModel(this.unitsService);
            this.ohsettingsViewModel = new OverheadSettingsViewModel(this.messenger, this.windowService, this.unitsService);
            this.countryBrowser = new CountryAndSupplierBrowserViewModel(this.windowService);
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup() 
        { 
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// This method tests the editable fields of Assembly General tab for Fine Calculation Accuracy. Save button is also tested 
        /// </summary>
        [TestMethod]
        public void GeneralTabFineCalculationTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            DateTime dt = new DateTime(2004, 09, 30);
            var description = EncryptionManager.Instance.GenerateRandomString(10, true);

            assemblyVm.AssyAssemblingCountry.Value = "Austria";
            assemblyVm.AssyNumber.Value = "321";
            assemblyVm.AssyVersion.Value = 555;
            assemblyVm.AssyVersionDate.Value = dt;
            assemblyVm.AssyDescription.Value = description;
            assemblyVm.AssyCalculationAccuracy.Value = PartCalculationAccuracy.FineCalculation;
            assemblyVm.AssyCalculationApproach.Value = PartCalculationApproach.Greenfield;
            assemblyVm.AssyCalculationVariant.Value = "1.4";
            assemblyVm.UpdateToLatestCalculationVariantCommand.Execute(null);
            assemblyVm.AssyBatchSize.Value = 222;
            assemblyVm.AssyYearlyProductionQuantity.Value = 100;
            assemblyVm.AssyLifeTime.Value = 200;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.AssemblingCountry, assemblyVm.AssyAssemblingCountry.Value);
            Assert.AreEqual(assembly.Number, assemblyVm.AssyNumber.Value);
            Assert.AreEqual(assembly.Version, assemblyVm.AssyVersion.Value);
            Assert.AreEqual(assembly.VersionDate, assemblyVm.AssyVersionDate.Value);
            Assert.AreEqual(assembly.Description, assemblyVm.AssyDescription.Value);
            Assert.AreEqual(assembly.CalculationAccuracy, assemblyVm.AssyCalculationAccuracy.Value.Value);
            Assert.AreEqual(assembly.CalculationApproach, assemblyVm.AssyCalculationApproach.Value);
            Assert.AreEqual(assembly.CalculationVariant, assemblyVm.AssyCalculationVariant.Value);
            Assert.AreEqual(assembly.BatchSizePerYear, assemblyVm.AssyBatchSize.Value);
            Assert.AreEqual(assembly.YearlyProductionQuantity, assemblyVm.AssyYearlyProductionQuantity.Value);
            Assert.AreEqual(assembly.LifeTime, assemblyVm.AssyLifeTime.Value);          
        }

        /// <summary>
        /// This method tests the editable fields of Assembly General tab for Rough Calculation Accuracy.
        /// </summary>
        [TestMethod]
        public void GeneralTabRoughCalculationTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;
            var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);

            MeasurementUnit weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");

            assemblyVm.AssyCalculationVariant.Value = "1.4";
            assemblyVm.AssyName.Value = "33";
            assemblyVm.ManufacturerViewModel.Name.Value = "400";
            assemblyVm.AssyCalculationAccuracy.Value = PartCalculationAccuracy.RoughCalculation;
            assemblyVm.AssyEstimatedCost.Value = 100;
            assemblyVm.AssyWeight.Value = 50;
            assemblyVm.WeightUnit = weightUnit;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.EstimatedCost, assemblyVm.AssyEstimatedCost.Value);
            Assert.AreEqual(assembly.CalculationAccuracy, assemblyVm.AssyCalculationAccuracy.Value);
            Assert.AreEqual(assembly.Weight, assemblyVm.AssyWeight.Value);
            Assert.AreEqual(assembly.MeasurementUnit, assemblyVm.WeightUnit);
        }

        /// <summary>
        /// This method tests the editable fields of Assembly General tab for Estimation Accuracy.
        /// </summary>
        [TestMethod]
        public void GeneralTabEstimationTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            var weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");

            assemblyVm.AssyCalculationAccuracy.Value = PartCalculationAccuracy.OfferOrExternalCalculation;
            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            assemblyVm.AssyAssemblingCountry.Value = "Austria";
            assemblyVm.AssyCalculationAccuracy.Value = PartCalculationAccuracy.Estimation;
            assemblyVm.AssyCalculationVariant.Value = "1.4";
            assemblyVm.AssyEstimatedCost.Value = 100;
            assemblyVm.AssyWeight.Value = 50;
            assemblyVm.WeightUnit = weightUnit;

            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationAccuracy, assemblyVm.AssyCalculationAccuracy.Value);
            Assert.AreEqual(assembly.EstimatedCost, assemblyVm.AssyEstimatedCost.Value);
            Assert.AreEqual(assembly.Weight, assemblyVm.AssyWeight.Value);
            Assert.AreEqual(assembly.MeasurementUnit, assemblyVm.WeightUnit);
        }

        /// <summary>
        /// This method tests the editable fields of Assembly General tab for OfferOrExternalCalculation Accuracy.
        /// </summary>
        [TestMethod]
        public void GeneralTabOfferOrExternalCalculationTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var measurementAdapter = this.unitsService.GetUnitsAdapter(localDataManager);
            var weightUnits = measurementAdapter.GetMeasurementUnits(MeasurementUnitType.Weight);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            var weightUnit = weightUnits.FirstOrDefault(u => u.Symbol == "g");

            assemblyVm.AssyAssemblingCountry.Value = "Austria";
            assemblyVm.AssyCalculationAccuracy.Value = PartCalculationAccuracy.OfferOrExternalCalculation;
            assemblyVm.AssyCalculationVariant.Value = "1.4";
            assemblyVm.AssyEstimatedCost.Value = 121;
            assemblyVm.AssyWeight.Value = 30;
            
            assemblyVm.WeightUnit = weightUnit;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationAccuracy, assemblyVm.AssyCalculationAccuracy.Value);
            Assert.AreEqual(assembly.EstimatedCost, assemblyVm.AssyEstimatedCost.Value);
            Assert.AreEqual(assembly.Weight, assemblyVm.AssyWeight.Value);
            Assert.AreEqual(assembly.MeasurementUnit, assemblyVm.WeightUnit);
        }

        /// <summary>
        /// This method tests undo button. 
        /// </summary>
        [TestMethod]
        public void UndoButtonTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            const PartCalculationApproach Approach = PartCalculationApproach.Greenfield;
            assemblyVm.AssyCalculationApproach.Value = Approach;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationApproach, assemblyVm.AssyCalculationApproach.Value);

            assemblyVm.AssyCalculationApproach.Value = PartCalculationApproach.Brownfield;

            var undoCommand = assemblyVm.UndoCommand;
            undoCommand.Execute(null);
            Assert.AreEqual(assemblyVm.AssyCalculationApproach.Value, Approach);
        }

        /// <summary>
        /// This method tests undo button. 
        /// </summary>
        [TestMethod]
        public void CancelButtonTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(
                service =>
                service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            const PartCalculationApproach Approach = PartCalculationApproach.Greenfield;
            const int ValueLifetime = 5;
            assemblyVm.AssyCalculationApproach.Value = Approach;
            assemblyVm.AssyLifeTime.Value = ValueLifetime;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationApproach, assemblyVm.AssyCalculationApproach.Value);
            Assert.AreEqual(assembly.LifeTime, assemblyVm.AssyLifeTime.Value);

            assemblyVm.AssyCalculationApproach.Value = PartCalculationApproach.Brownfield;
            assemblyVm.AssyLifeTime.Value = 20;

            var cancelCommand = assemblyVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationApproach, Approach);
            Assert.AreEqual(assembly.LifeTime, ValueLifetime);
        }

        /// <summary>
        /// This method tests Update calculation Variant Button
        /// </summary>
        [TestMethod]
        public void UpdateCalculationVariantTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            assemblyVm.AssyCalculationVariant.Value = "1.3";

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationVariant, assemblyVm.AssyCalculationVariant.Value);

            var updateCalculationVariantCommand = assemblyVm.UpdateToLatestCalculationVariantCommand;
            updateCalculationVariantCommand.Execute(null);
         
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.CalculationVariant, CostCalculatorFactory.LatestVersion);
        }

        /// <summary>
        /// This method tests editable fields of Assembly Manufacturer Tab . 
        /// </summary>
        [TestMethod]
        public void ManufacturerTabTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            var manufacturerDescription = EncryptionManager.Instance.GenerateRandomString(10, true);
            var manufacturerName = EncryptionManager.Instance.GenerateRandomString(10, true);

            this.manufacturer.Description.Value = manufacturerDescription;
            this.manufacturer.Name.Value = manufacturerName;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.Manufacturer.Description, this.manufacturer.Description.Value);
            Assert.AreEqual(assembly.Manufacturer.Name, this.manufacturer.Name.Value);
        }

        /// <summary>
        /// This method tests editable fields of Assembly Settings Tab . 
        /// </summary>
        [TestMethod]
        public void SettingsTabTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            assemblyVm.AssyPurchasePrice.Value = 200;
            assemblyVm.AssyTargetPrice.Value = 300;
            assemblyVm.AssyDeliveryType.Value = PartDeliveryType.CPT;
            assemblyVm.AssyAssetRate.Value = 10;
            assemblyVm.AssyToolingActive.Value = true;
            assemblyVm.AssyIsExternal.Value = true;
            assemblyVm.AssyExternalSGA.Value = true;
             
            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.PurchasePrice, assemblyVm.AssyPurchasePrice.Value);
            Assert.AreEqual(assembly.TargetPrice, assemblyVm.AssyTargetPrice.Value);
            Assert.AreEqual(assembly.DeliveryType, assemblyVm.AssyDeliveryType.Value);
            Assert.AreEqual(assembly.AssetRate, assemblyVm.AssyAssetRate.Value);
            Assert.AreEqual(assembly.SBMActive, assemblyVm.AssyExternalSGA.Value);
            Assert.AreEqual(assembly.IsExternal, assemblyVm.AssyIsExternal.Value);
            Assert.AreEqual(assembly.ExternalSGA, assemblyVm.AssyExternalSGA.Value);
        }

        /// <summary>
        /// This method tests editable fields of Assembly Overhead Settings Tab . 
        /// </summary>
        [TestMethod]
        public void OverheadSettingsTabTest()
        {
            var assemblyVm = new AssemblyViewModel(
                this.messenger,
                this.windowService,
                this.modelBrowserHelperService,
                this.costRecalculationCloneManager,
                this.manufacturer,
                this.documentsViewModel,
                this.ohsettingsViewModel,
                this.countrySettingsViewModel,
                this.countryBrowser,
                this.masterDataBrowser,
                this.media,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyVm.DataSourceManager = localDataManager;
            var assembly = this.CreateNewAssembly(localDataManager);
            assemblyVm.Model = assembly;

            this.ohsettingsViewModel.MaterialOverhead.Value = 200;
            this.ohsettingsViewModel.CommodityOverhead.Value = 30;
            this.ohsettingsViewModel.ManufacturingOverhead.Value = 23;
            this.ohsettingsViewModel.PackagingOverhead.Value = 20;
            this.ohsettingsViewModel.SalesAndAdministrationOverhead.Value = 20;
            this.ohsettingsViewModel.ConsumableOverhead.Value = 30;
            this.ohsettingsViewModel.ExternalWorkOverhead.Value = 44;
            this.ohsettingsViewModel.OtherCostOverhead.Value = 1000;
            this.ohsettingsViewModel.LogisticOverhead.Value = 99;
            this.ohsettingsViewModel.CompanySurchargeOverhead.Value = 10;
            this.ohsettingsViewModel.MaterialMargin.Value = 20;
            this.ohsettingsViewModel.CommodityMargin.Value = 30;
            this.ohsettingsViewModel.ManufacturingMargin.Value = 40;
            this.ohsettingsViewModel.ConsumableMargin.Value = 50;
            this.ohsettingsViewModel.ExternalWorkMargin.Value = 60;

            var saveCommand = assemblyVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(assembly.OverheadSettings.MaterialOverhead, this.ohsettingsViewModel.MaterialOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.CommodityOverhead, this.ohsettingsViewModel.CommodityOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.ManufacturingOverhead, this.ohsettingsViewModel.ManufacturingOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.PackagingOHValue, this.ohsettingsViewModel.PackagingOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.SalesAndAdministrationOHValue, this.ohsettingsViewModel.SalesAndAdministrationOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.ConsumableOverhead, this.ohsettingsViewModel.ConsumableOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.ExternalWorkOverhead, this.ohsettingsViewModel.ExternalWorkOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.OtherCostOHValue, this.ohsettingsViewModel.OtherCostOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.LogisticOHValue, this.ohsettingsViewModel.LogisticOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.CompanySurchargeOverhead, this.ohsettingsViewModel.CompanySurchargeOverhead.Value);
            Assert.AreEqual(assembly.OverheadSettings.MaterialMargin, this.ohsettingsViewModel.MaterialMargin.Value);
            Assert.AreEqual(assembly.OverheadSettings.CommodityMargin, this.ohsettingsViewModel.CommodityMargin.Value);
            Assert.AreEqual(assembly.OverheadSettings.ManufacturingMargin, this.ohsettingsViewModel.ManufacturingMargin.Value);
            Assert.AreEqual(assembly.OverheadSettings.ConsumableMargin, this.ohsettingsViewModel.ConsumableMargin.Value);
            Assert.AreEqual(assembly.OverheadSettings.ExternalWorkMargin, this.ohsettingsViewModel.ExternalWorkMargin.Value);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// This method creates a new assembly
        /// </summary>
        /// <returns>Returns assembly</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private Assembly CreateNewAssembly(IDataSourceManager dataManager)
        {
            var assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;
     
            var newManufacturer = new Manufacturer() { Name = EncryptionManager.Instance.GenerateRandomString(10, true) };
            assembly.Manufacturer = newManufacturer;
            var country = dataManager.CountryRepository.GetByName("Belgium");
            assembly.AssemblingCountry = country.Name;
            assembly.AssemblingCountryId = country.Guid;

            dataManager.ManufacturerRepository.Save(newManufacturer);
            dataManager.AssemblyRepository.Save(assembly);
            dataManager.SaveChanges();
           
            return assembly;
        }

        #endregion Private Methods
    }
}