﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests
{
    /// <summary>
    /// This is a test class for WelcomeViewModel and is intended 
    /// to contain all WelcomeViewModel Unit Test
    /// </summary>
    [TestClass]
    public class WelcomeViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WelcomeViewModelTest"/> class.
        /// </summary>
        public WelcomeViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests New Project Command using valid input parameters
        ///// </summary>
        //[TestMethod]
        //public void NewProjectCommandTest()
        //{
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    IMessenger messenger = new Messenger();
        //    WelcomeViewModel welcomeViewModel = new WelcomeViewModel(this.bootstrapper.CompositionContainer, windowService, messenger);

        //    ICommand newProjectCommand = welcomeViewModel.NewProjectCommand;
        //    newProjectCommand.Execute(null);
        //}

        ///// <summary>
        ///// Tests Key Down Command using valid input parameters
        ///// </summary>
        //[TestMethod]
        //public void CarouselKeyDownCommandTest()
        //{
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    IWelcomeView view = new WelcomeViewMock();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();

        //    WelcomeViewModel welcomeViewModel = new WelcomeViewModel(container, view, messageBoxService, messenger);
        //    Project project = new Project();
        //    project.Name = "Test Project" + DateTime.Now.Ticks;
        //    project.OverheadSettings = new OverheadSetting();
        //    EntityManager.Instance.AddEntity(project, EntityContext.LocalDatabase);

        //    //var key = Key.Enter;                    // Key to send
        //    //var target = Keyboard.FocusedElement;    // Target element
        //    //var routedEvent = Keyboard.KeyDownEvent; // Event to send

        //    //target.RaiseEvent(
        //    //  new KeyEventArgs(
        //    //    Keyboard.PrimaryDevice,
        //    //    PresentationSource.FromVisual(target),
        //    //    0,
        //    //    key) { RoutedEvent = routedEvent }
        //    //);

        //}

        ///// <summary>
        ///// Tests CarouselDoubleClick command using valid input parameters 
        ///// </summary>
        //[TestMethod]
        //public void CarouselDoubleClickCommandTest()
        //{
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    IWelcomeView view = new WelcomeViewMock();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    IMessenger messenger = new Messenger();
        //    WelcomeViewModel welcomeViewModel = new WelcomeViewModel(container, view, messageBoxService, messenger);

        //    CarouselProjectItem carouselItem = new CarouselProjectItem();
        //    CarouselDoubleClickEventArgs arg = new CarouselDoubleClickEventArgs(carouselItem);
        //    ICommand carouselDoubleClickCommand = welcomeViewModel.CarouselDoubleClickCommand;
        //    carouselDoubleClickCommand.Execute(arg);
        //}

        /// <summary>
        /// This is a moca class which is used to initialize an instance of the WelcomeViewModel
        /// </summary>
        public class WelcomeViewMock
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="WelcomeViewMock"/> class.
            /// </summary>
            public WelcomeViewMock()
            {
                this.DataSourceManager = DbIdentifier.LocalDatabase;
                this.IsEnabled = false;
            }

            /// <summary>
            /// Gets or sets the data context of the view.
            /// </summary>
            public object DataSourceManager { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this view is enabled in the UI.
            /// </summary>
            /// <value>
            /// true if the view is enabled; otherwise, false.
            /// </value>
            public bool IsEnabled { get; set; }
        }
    }
}
