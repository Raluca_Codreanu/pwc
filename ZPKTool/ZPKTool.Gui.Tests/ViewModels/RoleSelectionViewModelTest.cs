﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for RoleSelectionViewModel and is intended 
    /// to contain unit tests for all RoleSelectionViewModel commands
    /// </summary>
    [TestClass]
    public class RoleSelectionViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "RoleSelectionViewModelTest"/> class.
        /// </summary>
        public RoleSelectionViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Additional test attributes

        ///// <summary>
        ///// Tests SaveSelectedRole command 
        ///// </summary>
        //[TestMethod]
        //public void SaveSelectedRoleTest()
        //{
        //    CompositionContainer container = this.bootstrapper.CompositionContainer;
        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    RoleSelectionViewModel roleSelectionViewModel = new RoleSelectionViewModel(container, messageBoxService, messenger);
        //    ICommand okCommand = roleSelectionViewModel.OkCommand;

        //    // Create a user with admin role
        //    User user = new User();
        //    user.Name = user.Guid.ToString();
        //    user.Username = user.Guid.ToString();
        //    user.Password = EncryptionManager.Instance.EncodeMD5("password");

        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Role adminRole = dataContext.RoleRepository.GetAll().FirstOrDefault(r => r.Equals(UserRole.Admin));
        //    user.Roles.Add(adminRole);
        //    dataContext.UserRepository.Add(user);
        //    dataContext.SaveChanges();

        //    // Login in the application and execute OK command without selecting a role
        //    SecurityManager.Instance.Login(user.Username, "password");
        //    messageBoxService.ShowCallCount = 0;
        //    messageBoxService.ShowReturnValue = MessageBoxResult.OK;
        //    okCommand.Execute(null);

        //    // Selected Role was null => a message box should be displayed which request the user to select a role
        //    Assert.AreEqual<int>(1, messageBoxService.ShowCallCount, "Invalid number of calls of show message box method");

        //    // Select a role and execute OK command
        //    roleSelectionViewModel.SelectedRole = user.Roles.FirstOrDefault();
        //    okCommand.Execute(null);

        //    // Verify that the Current Role was set 
        //    Assert.IsTrue(SecurityManager.Instance.CurrentUserRole != null && SecurityManager.Instance.CurrentUserRole.Guid == adminRole.Guid, "Failed to change the current role of the current user.");
        //}
    }
}
