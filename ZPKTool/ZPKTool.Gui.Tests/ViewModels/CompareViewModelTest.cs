﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for CompareViewModel and is intended 
    /// to contain unit tests for all public methods from CompareViewModel class.
    /// </summary>
    [TestClass]
    public class CompareViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompareViewModelTest"/> class.
        /// </summary>
        public CompareViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }
    }
}
