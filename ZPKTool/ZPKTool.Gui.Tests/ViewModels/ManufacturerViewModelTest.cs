﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for ManufacturerViewModel and is intended 
    /// to contain unit tests for all ManufacturerViewModel commands
    /// </summary>
    [TestClass]
    public class ManufacturerViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref = "ManufacturerViewModelTest"/> class.
        /// </summary>
        public ManufacturerViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        ///// <summary>
        ///// Tests BrowseMasterData command.
        ///// </summary>
        //[TestMethod]
        //public void BrowseMasterDataCommandTest()
        //{
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    IMessenger messenger = new Messenger();
        //    CompositionContainer compositionContainer = this.bootstrapper.CompositionContainer;
        //    ManufacturerViewModel manufacturerViewModel = new ManufacturerViewModel(windowService, messenger, compositionContainer);

        //    windowService.IsOpen = false;
        //    ICommand browseMasterDataCommand = manufacturerViewModel.BrowseMasterDataCommand;
        //    browseMasterDataCommand.Execute(null);

        //    // TO DO -> Simulate the selection of a MD Manufacturer and verify that the view model contains the selected manufacturer
        //    //Verify that the view was displayed in the Dialog
        //    Assert.IsTrue(windowService.IsOpen, "Failed to open the MasterDataBrowser.");
        //}
    }
}
