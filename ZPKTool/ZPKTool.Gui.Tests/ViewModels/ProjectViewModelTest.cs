﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// Assembly View Model Test
    /// </summary>
    [TestClass]
    public class ProjectViewModelTest
    {
        #region Attributes

        /// <summary>
        /// The media view model
        /// </summary>
        private MediaViewModel mediaViewModel;

        /// <summary>
        /// The overhead settings view model
        /// </summary>
        private OverheadSettingsViewModel overheadSettingsViewModel;

        /// <summary>
        /// The model browser helper service
        /// </summary>
        private ModelBrowserHelperServiceMock modelBrowserHelperService;

        /// <summary>
        /// The documents view model
        /// </summary>
        private EntityDocumentsViewModel documentsViewModel;

        /// <summary>
        /// The supplier
        /// </summary>
        private SupplierViewModel supplier;

        /// <summary>
        /// The test context instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate the test context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.unitsService = new UnitsServiceMock();
            this.pleaseWaitService = new PleaseWaitService(new DispatcherService());
            this.windowService = new WindowServiceMock(new MessageDialogServiceMock(), new DispatcherServiceMock());
            this.messenger = new Messenger();
            var container = new CompositionContainer();
            this.supplier = new SupplierViewModel(this.windowService, container);
            var dialog = new FileDialogServiceMock();
            this.modelBrowserHelperService = new ModelBrowserHelperServiceMock();
            this.documentsViewModel = new EntityDocumentsViewModel(this.windowService, dialog, this.modelBrowserHelperService);
            this.overheadSettingsViewModel = new OverheadSettingsViewModel(this.messenger, this.windowService, this.unitsService);
            var videoViewModel = new VideoViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(this.messenger, this.windowService, new FileDialogServiceMock());
            this.mediaViewModel = new MediaViewModel(
                videoViewModel,
                picturesViewModel,
                this.messenger,
                this.windowService,
                new FileDialogServiceMock(),
                new ModelBrowserHelperServiceMock());
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region TestMethods

        /// <summary>
        /// This method tests editable fields of Project General Tab and Save button
        /// </summary>
        [TestMethod]
        public void GeneralTabTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            projectVm.DataSourceManager = localDataManager;
            projectVm.InitializeForProjectCreation(null);
            var project = projectVm.Model;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.SupplierViewModel.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.Number.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.StartDate.Value = DateTime.Now;
            projectVm.EndDate.Value = DateTime.Now;
            projectVm.Status.Value = ProjectStatus.Working;
            projectVm.Partner.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.ProjectLeader.Value = projectVm.Users.First(u => u.Guid == newUser.Guid);
            projectVm.ResponsibleCalculator.Value = projectVm.Users.First(u => u.Guid == newUser.Guid);
            projectVm.Version.Value = 10;
            projectVm.VersionDate.Value = DateTime.Now;

            var currencies = localDataManager.CurrencyRepository.GetBaseCurrencies();
            var basec = currencies.First(c => c.IsoCode == "EUR");
            projectVm.BaseCurrency.Value = basec;
            projectVm.ProductName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.ProductNumber.Value = "100";
            projectVm.YearlyProductionQuantity.Value = 10;
            var descriptionProduct = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.ProductDescription.Value = descriptionProduct;
            var supplierDescription = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.SupplierDescription.Value = supplierDescription;
            var remarks = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.Remarks.Value = remarks;
            projectVm.LifeTime.Value = 100;

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.Name, projectVm.Name.Value);
            Assert.AreEqual(project.Number, projectVm.Number.Value);
            Assert.AreEqual(project.StartDate, projectVm.StartDate.Value);
            Assert.AreEqual(project.EndDate, projectVm.EndDate.Value);
            Assert.AreEqual(project.Status, projectVm.Status.Value);
            Assert.AreEqual(project.Partner, projectVm.Partner.Value);
            Assert.AreEqual(project.ProjectLeader, projectVm.ProjectLeader.Value);
            Assert.AreEqual(project.ResponsibleCalculator, projectVm.ResponsibleCalculator.Value);
            Assert.AreEqual(project.Version, projectVm.Version.Value);
            Assert.AreEqual(project.VersionDate, projectVm.VersionDate.Value);
            Assert.AreEqual(project.BaseCurrency.IsoCode, projectVm.BaseCurrency.Value.IsoCode);
            Assert.AreEqual(project.ProductName, projectVm.ProductName.Value);
            Assert.AreEqual(project.ProductNumber, projectVm.ProductNumber.Value);
            Assert.AreEqual(project.YearlyProductionQuantity, projectVm.YearlyProductionQuantity.Value);
            Assert.AreEqual(project.ProductDescription, projectVm.ProductDescription.Value);
            Assert.AreEqual(project.Remarks, projectVm.Remarks.Value);
            Assert.AreEqual(project.LifeTime, projectVm.LifeTime.Value);
        }

        /// <summary>
        /// This method tests editable fields of Project Supplier Tab
        /// </summary>
        [TestMethod]
        public void SupplierTabTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            projectVm.DataSourceManager = localDataManager;
            var project = this.CreateNewProject(localDataManager);
            projectVm.InitializeForProjectCreation(null);
            projectVm.Model = project;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            var basec = new Currency();
            basec.Name = "US Dollar";
            basec.Symbol = "S/.";
            basec.IsoCode = "USD";
            projectVm.BaseCurrency.Value = basec;
            this.supplier.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.Type.Value = SupplierType.MidSize;
            this.supplier.Number.Value = "121";
            this.supplier.Description.Value = EncryptionManager.Instance.GenerateRandomString(20, true);
            this.supplier.StreetAddress.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.ZipCode.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.CountrySupplier.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.Country.Value = "Austria";
            this.supplier.City.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactMobileNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactEmail.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactPhoneNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactFaxNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.FirstContactWebPage.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactMobileNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactEmail.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactPhoneNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactFaxNo.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            this.supplier.SecondContactWebPage.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.Name, projectVm.Name.Value);
            Assert.AreEqual(project.Customer.Name, this.supplier.Name.Value);
            Assert.AreEqual(project.Customer.Type, this.supplier.Type.Value);
            Assert.AreEqual(project.Customer.Description, this.supplier.Description.Value);
            Assert.AreEqual(project.Customer.StreetAddress, this.supplier.StreetAddress.Value);
            Assert.AreEqual(project.Customer.ZipCode, this.supplier.ZipCode.Value);
            Assert.AreEqual(project.Customer.Country, this.supplier.Country.Value);
            Assert.AreEqual(project.Customer.City, this.supplier.City.Value);
            Assert.AreEqual(project.Customer.FirstContactName, this.supplier.FirstContactName.Value);
            Assert.AreEqual(project.Customer.FirstContactMobile, this.supplier.FirstContactMobileNo.Value);
            Assert.AreEqual(project.Customer.FirstContactEmail, this.supplier.FirstContactEmail.Value);
            Assert.AreEqual(project.Customer.FirstContactPhone, this.supplier.FirstContactPhoneNo.Value);
            Assert.AreEqual(project.Customer.FirstContactFax, this.supplier.FirstContactFaxNo.Value);
            Assert.AreEqual(project.Customer.FirstContactWebAddress, this.supplier.FirstContactWebPage.Value);
            Assert.AreEqual(project.Customer.SecondContactEmail, this.supplier.SecondContactEmail.Value);
            Assert.AreEqual(project.Customer.SecondContactFax, this.supplier.SecondContactFaxNo.Value);
            Assert.AreEqual(project.Customer.SecondContactMobile, this.supplier.SecondContactMobileNo.Value);
            Assert.AreEqual(project.Customer.SecondContactName, this.supplier.SecondContactName.Value);
            Assert.AreEqual(project.Customer.SecondContactPhone, this.supplier.SecondContactPhoneNo.Value);
            Assert.AreEqual(project.Customer.SecondContactWebAddress, this.supplier.SecondContactWebPage.Value);
        }

        /// <summary>
        /// This method tests editable fields of Project Settings Tab
        /// </summary>
        [TestMethod]
        public void SettingsTabTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            projectVm.DataSourceManager = localDataManager;
            var project = this.CreateNewProject(localDataManager);
            projectVm.InitializeForProjectCreation(null);
            projectVm.Model = project;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            var basec = new Currency();
            basec.Name = "US Dollar";
            basec.Symbol = "S/.";
            basec.IsoCode = "USD";
            projectVm.BaseCurrency.Value = basec;
            this.supplier.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.DepreciationPeriod.Value = 10;
            projectVm.DepreciationRate.Value = 10;
            projectVm.LogisticCostRatio.Value = 10;
            projectVm.ShiftsPerWeek.Value = 5;
            projectVm.ProductionDaysPerWeek.Value = 10;
            projectVm.HoursPerShift.Value = 10;
            projectVm.ProductionWeeksPerYear.Value = 50;

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.Name, projectVm.Name.Value);
            Assert.AreEqual(project.DepreciationPeriod, projectVm.DepreciationPeriod.Value);
            Assert.AreEqual(project.DepreciationRate, projectVm.DepreciationRate.Value);
            Assert.AreEqual(project.LogisticCostRatio, projectVm.LogisticCostRatio.Value);
            Assert.AreEqual(project.ShiftsPerWeek, projectVm.ShiftsPerWeek.Value);
            Assert.AreEqual(project.ProductionDaysPerWeek, projectVm.ProductionDaysPerWeek.Value);
            Assert.AreEqual(project.HoursPerShift, projectVm.HoursPerShift.Value);
            Assert.AreEqual(project.ProductionWeeksPerYear, projectVm.ProductionWeeksPerYear.Value);
        }

        /// <summary>
        /// This method tests editable fields of Project OverheadSettings Tab
        /// </summary>
        [TestMethod]
        public void OverHeadSettingsTabTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.CreateNewUser(localDataManager);
            projectVm.DataSourceManager = localDataManager;
            var project = this.CreateNewProject(localDataManager);
            projectVm.InitializeForProjectCreation(null);
            projectVm.Model = project;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            var basec = new Currency();
            basec.Name = "US Dollar";
            basec.Symbol = "S/.";
            basec.IsoCode = "USD";
            projectVm.BaseCurrency.Value = basec;
            this.supplier.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            projectVm.OverheadSettingsViewModel.MaterialOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.MaterialMargin.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.CommodityOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.ManufacturingOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.PackagingOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.SalesAndAdministrationOverhead.Value = 20;
            projectVm.OverheadSettingsViewModel.ConsumableOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.ExternalWorkOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.OtherCostOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.LogisticOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.CompanySurchargeOverhead.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.MaterialMargin.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.CommodityMargin.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.ManufacturingMargin.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.ConsumableMargin.Value = (decimal)0.1;
            projectVm.OverheadSettingsViewModel.ExternalWorkMargin.Value = (decimal)0.1;

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.OverheadSettings.MaterialOverhead, projectVm.OverheadSettingsViewModel.MaterialOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.CommodityOverhead, projectVm.OverheadSettingsViewModel.CommodityOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.ManufacturingOverhead, projectVm.OverheadSettingsViewModel.ManufacturingOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.PackagingOHValue, projectVm.OverheadSettingsViewModel.PackagingOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.SalesAndAdministrationOHValue, projectVm.OverheadSettingsViewModel.SalesAndAdministrationOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.ConsumableOverhead, projectVm.OverheadSettingsViewModel.ConsumableOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.ExternalWorkOverhead, projectVm.OverheadSettingsViewModel.ExternalWorkOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.OtherCostOHValue, projectVm.OverheadSettingsViewModel.OtherCostOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.LogisticOHValue, projectVm.OverheadSettingsViewModel.LogisticOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.CompanySurchargeOverhead, projectVm.OverheadSettingsViewModel.CompanySurchargeOverhead.Value);
            Assert.AreEqual(project.OverheadSettings.MaterialMargin, projectVm.OverheadSettingsViewModel.MaterialMargin.Value);
            Assert.AreEqual(project.OverheadSettings.CommodityMargin, projectVm.OverheadSettingsViewModel.CommodityMargin.Value);
            Assert.AreEqual(project.OverheadSettings.ManufacturingMargin, projectVm.OverheadSettingsViewModel.ManufacturingMargin.Value);
            Assert.AreEqual(project.OverheadSettings.ConsumableMargin, projectVm.OverheadSettingsViewModel.ConsumableMargin.Value);
            Assert.AreEqual(project.OverheadSettings.ExternalWorkMargin, projectVm.OverheadSettingsViewModel.ExternalWorkMargin.Value);
        }

        /// <summary>
        /// This method tests cancel command.
        /// </summary>
        [TestMethod]
        public void CancelCommandTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.CreateNewUser(localDataManager);
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(
                service =>
                service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);
            projectVm.DataSourceManager = localDataManager;
            var project = this.CreateNewProject(localDataManager);
            projectVm.InitializeForProjectCreation(null);
            projectVm.Model = project;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            var basec = new Currency();
            basec.Name = "US Dollar";
            basec.Symbol = "S/.";
            basec.IsoCode = "USD";
            projectVm.BaseCurrency.Value = basec;
            projectVm.ProductName.Value = "aaa";
            projectVm.ProductNumber.Value = "100";

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.ProductName, projectVm.ProductName.Value);
            Assert.AreEqual(project.ProductNumber, projectVm.ProductNumber.Value);

            projectVm.ProductName.Value = "bbb";

            var cancelCommand = projectVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(project.ProductName, "aaa");
        }

        /// <summary>
        /// This method tests undo command.
        /// </summary>
        [TestMethod]
        public void UndoCommandTest()
        {
            var projectVm = new ProjectViewModel(
                this.windowService,
                this.pleaseWaitService,
                this.messenger,
                this.supplier,
                this.documentsViewModel,
                this.overheadSettingsViewModel,
                this.mediaViewModel,
                this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var newUser = this.CreateNewUser(localDataManager);
            projectVm.DataSourceManager = localDataManager;
            var project = this.CreateNewProject(localDataManager);
            projectVm.InitializeForProjectCreation(null);
            projectVm.Model = project;

            projectVm.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            var basec = new Currency();
            basec.Name = "US Dollar";
            basec.Symbol = "S/.";
            basec.IsoCode = "USD";
            projectVm.BaseCurrency.Value = basec;
            projectVm.ProductName.Value = "aaa";
            projectVm.ProductNumber.Value = "100";
            projectVm.ProductName.Value = "bbb";
            projectVm.ProductNumber.Value = "200";

            var saveCommand = projectVm.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(project.ProductName, projectVm.ProductName.Value);
            Assert.AreEqual(project.ProductNumber, projectVm.ProductNumber.Value);

            var undoCommand = projectVm.UndoCommand;
            undoCommand.Execute(null);

            saveCommand.Execute(null);
            Assert.AreEqual(project.ProductName, "bbb");
            Assert.AreEqual(project.ProductNumber, "100");

            undoCommand.Execute(null);

            saveCommand.Execute(null);
            Assert.AreEqual(project.ProductName, "aaa");
            Assert.AreEqual(project.ProductNumber, "100");
        }

        #endregion TestMethods

        #region Private Methods

        /// <summary>
        /// This method creates a new project 
        /// </summary>
        /// <returns>Returns project</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private Project CreateNewProject(IDataSourceManager dataManager)
        {
            var project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            var newSupplier = new Customer();
            newSupplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = newSupplier;
            var overheadSettings = new OverheadSetting();
            project.OverheadSettings = overheadSettings;

            dataManager.SupplierRepository.Save(newSupplier);
            dataManager.ProjectRepository.Save(project);
            dataManager.SaveChanges();

            return project;
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion Private Methods
    }
}
