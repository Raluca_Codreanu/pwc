﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MainViewModel and is intended 
    /// to contain unit tests for all commands of MainViewModel
    /// </summary>
    [TestClass]
    public class MainViewModelTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModelTest"/> class.
        /// </summary>
        public MainViewModelTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }

        /// <summary>
        /// Use TestCleanup to run code after each test has run.
        /// If a user is logged in the application, it should be logged out after the test was done otherwise will influence other tests.
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional test attributes

        #region Test Methods

        ///// <summary>
        ///// A test for RenameCommand.
        ///// </summary>
        //[TestMethod]
        //public void RenameCommandTest()
        //{
        //    User user = new User();
        //    user.Username = user.Guid.ToString();
        //    user.Name = user.Guid.ToString();
        //    user.Password = EncryptionManager.Instance.EncodeMD5("password");

        //    ProjectFolder folder = new ProjectFolder();
        //    folder.Name = folder.Guid.ToString();
        //    folder.Owner = user;

        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    Role adminRole = dataContext.RoleRepository.GetAll().FirstOrDefault(r => r.Equals(UserRole.Admin));
        //    user.Roles.Add(adminRole);
        //    dataContext.ProjectFolderRepository.Add(folder);
        //    dataContext.SaveChanges();

        //    SecurityManager.Instance.Login(user.Username, "password");

        //    IMessenger messenger = new Messenger();
        //    MessageBoxServiceMock messageBoxService = new MessageBoxServiceMock();
        //    DispatcherServiceMock dispatcherService = new DispatcherServiceMock();
        //    WindowServiceMock windowService = new WindowServiceMock(messageBoxService, dispatcherService);
        //    MainViewModel mainViewModel = new MainViewModel(this.bootstrapper.CompositionContainer, messenger, windowService);

        //    TreeViewDataItem treeViewDataItem = new TreeViewDataItem();
        //    treeViewDataItem.DataObject = folder;
        //    treeViewDataItem.IsSelected = true;
        //    mainViewModel.ProjectsTreeViewModel.Items.Add(treeViewDataItem);

        //    // Simulate the selection of a Tree View Item
        //    RoutedPropertyChangedEventArgs<object> e = new RoutedPropertyChangedEventArgs<object>(null, treeViewDataItem);
        //    ICommand selectedTreeItemChangedCommand = mainViewModel.ProjectsTreeViewModel.ProjectsTreeSelectedItemChangedCommand;
        //    selectedTreeItemChangedCommand.Execute(e);

        //    ICommand renameCommand = mainViewModel.RenameCommand;
        //    renameCommand.Execute(folder);
        //}

        #endregion Test Methods

        /// <summary>
        /// A mock implementation of IMainView interface
        /// </summary>
        public class MainViewMock
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="MainViewMock"/> class.
            /// </summary>
            public MainViewMock()
            {
                this.DataSourceManager = DbIdentifier.LocalDatabase;
                this.IsEnabled = false;
            }

            /// <summary>
            /// Gets or sets the data context of the view.
            /// </summary>
            public object DataSourceManager { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this view is enabled in the UI.
            /// </summary>
            /// <value>
            /// true if the view is enabled; otherwise, false.
            /// </value>
            public bool IsEnabled { get; set; }
        }
    }
}
