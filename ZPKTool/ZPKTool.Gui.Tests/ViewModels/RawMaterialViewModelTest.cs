﻿using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// RawMaterialViewModelTest class.
    /// </summary>
    [TestClass]
    public class RawMaterialViewModelTest
    {
        #region Fields

        /// <summary>
        /// THe window service.
        /// </summary>
        private WindowServiceMock windowService;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private IDispatcherService dispatcherService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserService;

        /// <summary>
        /// The cost recalculation Clone Manager.
        /// </summary>
        private ICostRecalculationCloneManager costRecalculationCloneManager;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The manufacturer view-model.
        /// </summary>
        private ManufacturerViewModel manufacturer;

        /// <summary>
        /// The MasterDataBrowser view-model.
        /// </summary>
        private MasterDataBrowserViewModel masterDataBrowser;

        /// <summary>
        /// The media view-model.
        /// </summary>
        private MediaViewModel media;

        /// <summary>
        /// The RawMaterial classifications view-model.
        /// </summary>
        private ClassificationSelectorViewModel rawMaterialClassifications;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The units service
        /// </summary>
        private IUnitsService unitsService;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialViewModelTest"/> class.
        /// </summary>
        public RawMaterialViewModelTest()
        {
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        /// <value>
        /// The test context.
        /// </value>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class
        /// </summary>
        /// <param name="testContext">Test Context.</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////

        /// <summary>
        /// Use TestInitialize to run code before running each test 
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.messageDialogService = new MessageDialogServiceMock();
            this.dispatcherService = new DispatcherServiceMock();
            this.windowService = new WindowServiceMock((MessageDialogServiceMock)this.messageDialogService, (DispatcherServiceMock)this.dispatcherService);
            this.messenger = new Messenger();
            this.modelBrowserService = new ModelBrowserHelperServiceMock();
            this.unitsService = new UnitsServiceMock();
            this.rawMaterialClassifications = new ClassificationSelectorViewModel();
            this.costRecalculationCloneManager = new CostRecalculationCloneManager();

            var compositionContainer = new CompositionContainer();
            var videoViewModel = new VideoViewModel(messenger, windowService, new FileDialogServiceMock());
            var picturesViewModel = new PicturesViewModel(messenger, windowService, new FileDialogServiceMock());
            this.masterDataBrowser = new MasterDataBrowserViewModel(compositionContainer, messenger, windowService, new OnlineCheckService(messenger), this.unitsService);
            this.media = new MediaViewModel(videoViewModel, picturesViewModel, messenger, windowService, new FileDialogServiceMock(), new ModelBrowserHelperServiceMock());
            this.manufacturer = new ManufacturerViewModel(windowService, messenger, compositionContainer);
        }

        //// Use TestCleanup to run code after each test has run
        ////[TestCleanup]
        ////public void MyTestCleanup()
        ////{            
        ////}

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests a RawMaterialViewModel edit scenario.
        /// </summary>
        [TestMethod]
        public void EditRawMaterialTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            this.CreateNewUser(localDataManager);
            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterial.Sprue = 0.4m;
            rawMaterial.ParentWeight = 2m;
            rawMaterialVM.CostCalculationVersion = "1.0";
            rawMaterialVM.Model = new RawMaterial();
            rawMaterialVM.Model = rawMaterial;

            rawMaterialVM.StockKeepingState = true;
            rawMaterialVM.StockKeepingState = false;
            rawMaterialVM.StockKeepingState = true;
            rawMaterialVM.StockKeeping.Value = 1m;
            rawMaterialVM.PartWeight.Value = 3m;
            rawMaterialVM.Quantity.Value = 0;
            rawMaterialVM.MaterialClassificationsView.ClassificationLevels = new System.Collections.ObjectModel.ObservableCollection<object>();

            var browseMDCommand = rawMaterialVM.BrowseMasterDataCommand;
            browseMDCommand.Execute(null);
            var cloaseMDCommand = rawMaterialVM.MasterDataBrowser.CloseCommand;
            cloaseMDCommand.Execute(null);

            var saveCommand = rawMaterialVM.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(rawMaterial.StockKeeping, rawMaterialVM.StockKeeping.Value);
            Assert.AreEqual(rawMaterial.ParentWeight, rawMaterialVM.PartWeight.Value);
            Assert.AreEqual(rawMaterial.Name, rawMaterialVM.Name.Value);
        }

        /// <summary>
        /// Tests a RawMaterialViewModel process material based on parent weight, quantity, loss and sprue.
        /// </summary>
        [TestMethod]
        public void ProcessMaterialsTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = new RawMaterial();
            rawMaterialVM.Model = rawMaterial;

            rawMaterialVM.PartWeight.Value = 20m;
            rawMaterialVM.Sprue.Value = 0.5m;
            rawMaterialVM.Loss.Value = 0.5m;
            Assert.AreEqual(rawMaterialVM.ProcessMaterial, 45m);

            rawMaterialVM.Loss.Value = 0;
            Assert.AreEqual(rawMaterialVM.ProcessMaterial, 30m);

            rawMaterialVM.Loss.Value = null;
            Assert.AreEqual(rawMaterialVM.ProcessMaterial, 30m);
        }

        /// <summary>
        /// Tests RawMaterialViewModel load model in ViewMode.
        /// </summary>
        [TestMethod]
        public void LoadRawMaterialInViewModelTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            var deliveryTypes = localDataManager.RawMaterialDeliveryTypeRepository.FindAll();
            if (deliveryTypes.Count > 0)
            {
                rawMaterial.DeliveryType = deliveryTypes.ElementAt(0);
            }

            rawMaterialVM.DataSourceManager = localDataManager;
            rawMaterialVM.IsInViewerMode = true;
            rawMaterialVM.IsReadOnly = true;
            rawMaterialVM.Model = rawMaterial;

            var closeCommand = rawMaterialVM.CloseCommand;
            closeCommand.Execute(null);

            Assert.IsTrue(rawMaterialVM.ManufacturerViewModel.IsReadOnly);
            Assert.IsTrue(rawMaterialVM.MediaViewModel.IsReadOnly);
            Assert.IsTrue(rawMaterialVM.MaterialClassificationsView.IsReadOnly);
            Assert.IsNotNull(rawMaterialVM.MaterialClassificationsView.ClassificationLevels);
        }

        /// <summary>
        /// Tests RawMaterialViewModel SaveCommand.
        /// </summary>
        [TestMethod]
        public void SaveRawMaterialTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.NameUK.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.NameUS.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.NormName.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.Price.Value = 1m;
            rawMaterialVM.PartWeight.Value = 1m;
            rawMaterialVM.Quantity.Value = 1m;
            rawMaterialVM.Sprue.Value = 0.5m;
            rawMaterialVM.Loss.Value = 0.5m;
            rawMaterialVM.ScrapRefundRatio.Value = 1m;
            rawMaterialVM.RejectRatio.Value = 1m;
            rawMaterialVM.RecyclingRatio.Value = 1m;
            rawMaterialVM.StockKeeping.Value = 1m;
            rawMaterialVM.YieldStrength.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.RuptureStrength.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.Density.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.MaxElongation.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.GlassTransitionTemperature.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.Rx.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.Rm.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var saveCommand = rawMaterialVM.SaveCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(rawMaterial.Name, rawMaterialVM.Name.Value);
            Assert.AreEqual(rawMaterial.NameUK, rawMaterialVM.NameUK.Value);
            Assert.AreEqual(rawMaterial.NameUS, rawMaterialVM.NameUS.Value);
            Assert.AreEqual(rawMaterial.NormName, rawMaterialVM.NormName.Value);
            Assert.AreEqual(rawMaterial.Price, rawMaterialVM.Price.Value);
            Assert.AreEqual(rawMaterial.ParentWeight, rawMaterialVM.PartWeight.Value);
            Assert.AreEqual(rawMaterial.Quantity, rawMaterialVM.Quantity.Value);
            Assert.AreEqual(rawMaterial.Sprue, rawMaterialVM.Sprue.Value);
            Assert.AreEqual(rawMaterial.Loss, rawMaterialVM.Loss.Value);
            Assert.AreEqual(rawMaterial.ScrapRefundRatio, rawMaterialVM.ScrapRefundRatio.Value);
            Assert.AreEqual(rawMaterial.RecyclingRatio, rawMaterialVM.RecyclingRatio.Value);
            Assert.AreEqual(rawMaterial.RejectRatio, rawMaterialVM.RejectRatio.Value);
            Assert.AreEqual(rawMaterial.StockKeeping, rawMaterialVM.StockKeeping.Value);
            Assert.AreEqual(rawMaterial.YieldStrength, rawMaterialVM.YieldStrength.Value);
            Assert.AreEqual(rawMaterial.RuptureStrength, rawMaterialVM.RuptureStrength.Value);
            Assert.AreEqual(rawMaterial.Density, rawMaterialVM.Density.Value);
            Assert.AreEqual(rawMaterial.MaxElongation, rawMaterialVM.MaxElongation.Value);
            Assert.AreEqual(rawMaterial.GlassTransitionTemperature, rawMaterialVM.GlassTransitionTemperature.Value);
            Assert.AreEqual(rawMaterial.Rx, rawMaterialVM.Rx.Value);
            Assert.AreEqual(rawMaterial.Rm, rawMaterialVM.Rm.Value);
        }

        /// <summary>
        /// Tests RawMaterialViewModel aborted CancelCommand.
        /// </summary>
        [TestMethod]
        public void CancelCommandAbortedTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo)).Returns(MessageDialogResult.No);

            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            var rawMaterialName = rawMaterial.Name;
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var cancelCommand = rawMaterialVM.CancelCommand;
            cancelCommand.Execute(null);

            Assert.IsTrue(rawMaterialVM.IsChanged);
            Assert.AreNotEqual(rawMaterialVM.Name.Value, rawMaterialName);
        }

        /// <summary>
        /// Tests RawMaterialViewModel CancelCommand.
        /// </summary>
        [TestMethod]
        public void CancelCommandSuccessfullyTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            var rawMaterialName = rawMaterial.Name;
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);

            var cancelCommand = rawMaterialVM.CancelCommand;
            cancelCommand.Execute(null);

            Assert.IsFalse(rawMaterialVM.IsChanged);
            Assert.AreEqual(rawMaterialVM.Name.Value, rawMaterialName);
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with a new unmodified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingUnmodifiedNewRawMaterialTest()
        {
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            this.CreateNewUser(localDataManager);

            rawMaterialVM.InitializeForCreation(false, new Part(), "1.3");
            rawMaterialVM.OnUnloading();

            Assert.IsFalse(rawMaterialVM.Saved);
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with a new modified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedNewRawMaterialSuccessfullyTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo)).Returns(MessageDialogResult.No);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            this.CreateNewUser(localDataManager);

            rawMaterialVM.InitializeForCreation(false, new Part(), "1.3");
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.OnUnloading();

            Assert.IsNotNull(rawMaterialVM.Model, "Failed to return to view model.");
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with a new modified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedNewRawMaterialCanceledTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            this.CreateNewUser(localDataManager);

            rawMaterialVM.InitializeForCreation(false, new Part(), "1.3");
            var rawMaterial = rawMaterialVM.Model;
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.OnUnloading();

            Assert.IsFalse(rawMaterialVM.Saved);
            Assert.IsNull(localDataManager.RawMaterialRepository.GetByKey(rawMaterial.Guid), "Failed to remove the raw material from context.");
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with an existing unmodified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingUnmodifiedExistingRawMaterialTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.OnUnloading();

            Assert.IsFalse(rawMaterialVM.IsChanged);
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with an existing modified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedExistingRawMaterialSuccessfullyTest()
        {
            string rawMaterialName = EncryptionManager.Instance.GenerateRandomString(10, true);

            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel)).Returns(MessageDialogResult.Yes);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = rawMaterial;
            rawMaterial.Name = rawMaterialName;
            rawMaterialVM.IsChanged = true;
            rawMaterialVM.OnUnloading();

            Assert.IsFalse(rawMaterialVM.IsChanged);
            Assert.AreNotEqual(rawMaterial.Name, rawMaterialName);
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with an existing modified raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingModifiedExistingRawMaterialCanceledTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel)).Returns(MessageDialogResult.No);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService) { DataSourceManager = localDataManager };

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.Name.Value = EncryptionManager.Instance.GenerateRandomString(10, true);
            rawMaterialVM.Remarks.Value = "test";
            rawMaterialVM.OnUnloading();

            Assert.IsFalse(rawMaterialVM.IsChanged);
        }

        /// <summary>
        /// Tests RawMaterialViewModel OnUnloading with an invalid raw material.
        /// </summary>
        [TestMethod]
        public void OnUnloadingInvalidRawMaterialTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(service => service.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNo)).Returns(MessageDialogResult.Yes);

            var rawMaterialVM = new RawMaterialViewModel(
                wndServiceMock.Object,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            rawMaterialVM.DataSourceManager = localDataManager;

            var rawMaterial = this.CreateNewRawMaterial(localDataManager);
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.Name.Value = string.Empty;
            rawMaterialVM.OnUnloading();

            Assert.IsTrue(rawMaterialVM.IsChanged);
            Assert.AreEqual(rawMaterialVM.Name.Value, string.Empty);
        }

        /// <summary>
        /// Tests RawMaterialViewModel RefreshParentCost.
        /// </summary>
        [TestMethod]
        public void RefreshParentCostTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var rawMaterial = this.CreateNewPartRawMaterial(localDataManager, true);
            rawMaterial.Part = new Part()
            {
                Name = EncryptionManager.Instance.GenerateRandomString(10, true),
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = new Process()
            };

            rawMaterialVM.DataSourceManager = localDataManager;
            rawMaterialVM.Model = rawMaterial;
            rawMaterialVM.ParentProject = new Project();

            bool projectNotified = false;
            messenger.Register<CurrentComponentCostChangedMessage>(
            (msg) =>
            {
                projectNotified = true;
            },
            GlobalMessengerTokens.MainViewTargetToken);

            var saveCommand = rawMaterialVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
        }

        /// <summary>
        /// Tests RawMaterialViewModel RefreshParentCost for a raw material in view-mode.
        /// </summary>
        [TestMethod]
        public void RefreshParentCostInViewModeTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var rawMaterial = this.CreateNewPartRawMaterial(localDataManager, true);
            rawMaterialVM.DataSourceManager = localDataManager;
            rawMaterialVM.IsInViewerMode = true;
            rawMaterialVM.Model = rawMaterial;

            bool projectNotified = false;
            messenger.Register<CurrentComponentCostChangedMessage>(
            (msg) =>
            {
                projectNotified = true;
            },
            GlobalMessengerTokens.ModelBrowserTargetToken);

            ICommand saveCommand = rawMaterialVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
        }

        /// <summary>
        /// Tests RawMaterialViewModel RefreshParentCost for a raw material in view-mode.
        /// </summary>
        [TestMethod]
        public void RefreshParentCostInViewModeWithParentProjectNULLTest()
        {
            var rawMaterialVM = new RawMaterialViewModel(
                windowService,
                messenger,
                modelBrowserService,
                costRecalculationCloneManager,
                manufacturer,
                masterDataBrowser,
                media,
                rawMaterialClassifications,
                this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var rawMaterial = this.CreateNewPartRawMaterial(localDataManager, false);
            rawMaterialVM.DataSourceManager = localDataManager;
            rawMaterialVM.IsInViewerMode = true;
            rawMaterialVM.Model = rawMaterial;

            var projectNotified = false;
            messenger.Register<CurrentComponentCostChangedMessage>(
            (msg) =>
            {
                projectNotified = true;
            },
            GlobalMessengerTokens.ModelBrowserTargetToken);

            var saveCommand = rawMaterialVM.SaveCommand;
            saveCommand.Execute(null);

            // Verify that the CurrentComponentCostChangedMessage notification was send.
            Assert.IsTrue(projectNotified);
        }

        #endregion Test Methods

        #region Private Methods

        /// <summary>
        /// Create and save to DB a new raw material.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created raw material.</returns>
        private RawMaterial CreateNewRawMaterial(IDataSourceManager dataManager)
        {
            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            rawMaterial.Manufacturer = manufacturer;

            var materialsClassificationL1 = new MaterialsClassification() { Name = "Level 1 Classification" };
            var materialsClassificationL2 = new MaterialsClassification() { Name = "Level 2 Classification" };
            var materialsClassificationL3 = new MaterialsClassification() { Name = "Level 3 Classification" };
            var materialsClassificationL4 = new MaterialsClassification() { Name = "Level 4 Classification" };

            rawMaterial.MaterialsClassificationL1 = materialsClassificationL1;
            rawMaterial.MaterialsClassificationL2 = materialsClassificationL2;
            rawMaterial.MaterialsClassificationL3 = materialsClassificationL3;
            rawMaterial.MaterialsClassificationL4 = materialsClassificationL4;

            dataManager.MaterialsClassificationRepository.Save(materialsClassificationL1);
            dataManager.MaterialsClassificationRepository.Save(materialsClassificationL2);
            dataManager.MaterialsClassificationRepository.Save(materialsClassificationL3);
            dataManager.MaterialsClassificationRepository.Save(materialsClassificationL4);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.RawMaterialRepository.Save(rawMaterial);
            dataManager.SaveChanges();

            return rawMaterial;
        }

        /// <summary>
        /// Create and save to DB a new part raw material.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <param name="hasParentProject">A bool param indicating if the new created raw material has a project parent.</param>
        /// <returns>The created raw material.</returns>
        private RawMaterial CreateNewPartRawMaterial(IDataSourceManager dataManager, bool hasParentProject)
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            Process process = new Process();
            part.Process = process;

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            rawMaterial.Manufacturer = manufacturer;
            part.RawMaterials.Add(rawMaterial);

            if (hasParentProject)
            {
                Project project = new Project();
                project.Name = project.Guid.ToString();
                project.OverheadSettings = new OverheadSetting();
                project.Parts.Add(part);
                dataManager.ProjectRepository.Save(project);
            }

            dataManager.PartRepository.Save(part);
            dataManager.ManufacturerRepository.Save(manufacturer);
            dataManager.RawMaterialRepository.Save(rawMaterial);
            dataManager.SaveChanges();

            return rawMaterial;
        }

        /// <summary>
        /// Create a new user and set it as current user.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <returns>The created user.</returns>
        private User CreateNewUser(IDataSourceManager dataManager)
        {
            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            dataManager.UserRepository.Add(user);
            dataManager.SaveChanges();

            SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Admin);

            return user;
        }

        #endregion Private Methods
    }
}