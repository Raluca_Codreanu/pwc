﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// This is a test class for MassUpdateAdditionalCostViewModel and is intended 
    /// to contain unit tests for all public methods from MassUpdateAdditionalCostViewModel class.
    /// </summary>
    [TestClass]
    public class MassUpdateCountrySettingsViewModelTest
    {
        /// <summary>
        /// This is a test class for MasUpdateAdditionalCostViewModel and is intended 
        /// to contain unit tests for all commands from MasUpdateAdditionalCostViewModel.
        /// </summary>
        private Bootstrapper bootstrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateCountrySettingsViewModelTest"/> class.
        /// </summary>
        public MassUpdateCountrySettingsViewModelTest()
        {
            Utils.ConfigureDbAccess();
            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods
        
        [TestMethod]
        public void TestMethod1()
        {

        }

        #endregion Test Method
    }
}
