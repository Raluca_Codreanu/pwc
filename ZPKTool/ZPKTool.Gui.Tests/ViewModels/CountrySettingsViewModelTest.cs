﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Tests.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.ViewModels
{
    /// <summary>
    /// this class is used to test the CountrySettingsViewModel
    /// </summary>
    [TestClass]
    public class CountrySettingsViewModelTest
    {
        #region Attributes
        /// <summary>
        /// The Test Content.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The units service
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Additional test attributes

        /// <summary>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </summary>
        /// <param name="testContext">Used to indicate text context</param>
        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
            LocalDataCache.Initialize();
        }

        /// <summary>
        /// Use ClassCleanup to run code after all tests in a class have run.
        /// </summary>
        [ClassCleanup]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// Use TestInitialize to run code before running each test.
        /// </summary>
        [TestInitialize]
        public void MyTestInitialize()
        {
            this.unitsService = new UnitsServiceMock();
        }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// This method tests the editable fields of Country settings view model. Save button is also tested 
        /// </summary>
        [TestMethod]
        public void CountrySettingsEditAndSaveTest()
        {
            var countrySettingsVm = new CountrySettingsViewModel(this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var countrySettings = this.CreateNewCountrySettings(localDataManager);
            countrySettingsVm.DataSourceManager = localDataManager;
            countrySettingsVm.Model = countrySettings;
            countrySettingsVm.UnskilledLaborCost.Value = 5;
            countrySettingsVm.SkilledLaborCost.Value = 10;
            countrySettingsVm.ForemanCost.Value = 5;
            countrySettingsVm.TechnicianCost.Value = 10;
            countrySettingsVm.EngineerCost.Value = 100;
            countrySettingsVm.LaborAvailability.Value = 70;
            countrySettingsVm.EnergyCost.Value = (decimal)0.01;
            countrySettingsVm.AirCost.Value = (decimal)0.01;
            countrySettingsVm.WaterCost.Value = (decimal)0.5;
            countrySettingsVm.ProductionAreaRentalCost.Value = 2;
            countrySettingsVm.OfficeAreaRentalCost.Value = 4;
            countrySettingsVm.InterestRate.Value = 15;
            countrySettingsVm.OneShiftModelCostRatio.Value = 50;
            countrySettingsVm.TwoShiftsModelCostRatio.Value = 40;
            countrySettingsVm.ThreeShiftsModelCostRatio.Value = 50;

            var saveCommand = countrySettingsVm.SaveToModelCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(countrySettings.UnskilledLaborCost, countrySettingsVm.UnskilledLaborCost.Value);
            Assert.AreEqual(countrySettings.SkilledLaborCost, countrySettingsVm.SkilledLaborCost.Value);
            Assert.AreEqual(countrySettings.ForemanCost, countrySettingsVm.ForemanCost.Value);
            Assert.AreEqual(countrySettings.TechnicianCost, countrySettingsVm.TechnicianCost.Value);
            Assert.AreEqual(countrySettings.EngineerCost, countrySettingsVm.EngineerCost.Value);
            Assert.AreEqual(countrySettings.AirCost, countrySettingsVm.AirCost.Value);
            Assert.AreEqual(countrySettings.LaborAvailability, countrySettingsVm.LaborAvailability.Value);
            Assert.AreEqual(countrySettings.EnergyCost, countrySettingsVm.EnergyCost.Value);
            Assert.AreEqual(countrySettings.WaterCost, countrySettingsVm.WaterCost.Value);
            Assert.AreEqual(countrySettings.ProductionAreaRentalCost, countrySettingsVm.ProductionAreaRentalCost.Value);
            Assert.AreEqual(countrySettings.OfficeAreaRentalCost, countrySettingsVm.OfficeAreaRentalCost.Value);
            Assert.AreEqual(countrySettings.InterestRate, countrySettingsVm.InterestRate.Value);
            Assert.AreEqual(countrySettings.ShiftCharge1ShiftModel, countrySettingsVm.OneShiftModelCostRatio.Value);
            Assert.AreEqual(countrySettings.ShiftCharge2ShiftModel, countrySettingsVm.TwoShiftsModelCostRatio.Value);
            Assert.AreEqual(countrySettings.ShiftCharge3ShiftModel, countrySettingsVm.ThreeShiftsModelCostRatio.Value);
        }

        /// <summary>
        /// This method tests the undo button 
        /// </summary>
        [TestMethod]
        public void UndoCommandTest()
        {
            var countrySettingsVm = new CountrySettingsViewModel(this.unitsService);

            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countrySettingsVm.DataSourceManager = localDataManager;
            var countrySettings = this.CreateNewCountrySettings(localDataManager);
            countrySettingsVm.Model = countrySettings;

            countrySettingsVm.UnskilledLaborCost.Value = 5;
            countrySettingsVm.SkilledLaborCost.Value = 10;
            countrySettingsVm.UnskilledLaborCost.Value = 50;
            countrySettingsVm.SkilledLaborCost.Value = 100;

            var saveCommand = countrySettingsVm.SaveToModelCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(countrySettings.UnskilledLaborCost, 50);
            Assert.AreEqual(countrySettings.SkilledLaborCost, 100);

            var undoCommand = countrySettingsVm.UndoCommand;
            undoCommand.Execute(null);

            Assert.AreEqual(countrySettings.UnskilledLaborCost, 50);
            Assert.AreEqual(countrySettingsVm.SkilledLaborCost.Value, 10);

            undoCommand.Execute(null);

            Assert.AreEqual(countrySettingsVm.UnskilledLaborCost.Value, 5);
            Assert.AreEqual(countrySettingsVm.SkilledLaborCost.Value, 10);
        }

        /// <summary>
        /// This method tests the cancel command. 
        /// </summary>
        [TestMethod]
        public void CancelCommandTest()
        {
            var wndServiceMock = new Mock<IWindowService>();
            wndServiceMock.Setup(
                service =>
                service.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo))
                .Returns(MessageDialogResult.Yes);
            var countrySettingsVm = new CountrySettingsViewModel(this.unitsService);
            var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            countrySettingsVm.DataSourceManager = localDataManager;
            var countrySettings = this.CreateNewCountrySettings(localDataManager);
            countrySettingsVm.Model = countrySettings;

            countrySettingsVm.UnskilledLaborCost.Value = 5;
            countrySettingsVm.SkilledLaborCost.Value = 10;

            var saveCommand = countrySettingsVm.SaveToModelCommand;
            saveCommand.Execute(null);

            Assert.AreEqual(countrySettings.UnskilledLaborCost, 5);
            Assert.AreEqual(countrySettings.SkilledLaborCost, 10);

            countrySettingsVm.UnskilledLaborCost.Value = 50;
            countrySettingsVm.SkilledLaborCost.Value = 100;

            var cancelCommand = countrySettingsVm.CancelCommand;
            cancelCommand.Execute(null);

            Assert.AreEqual(countrySettings.UnskilledLaborCost, 5);
            Assert.AreEqual(countrySettings.SkilledLaborCost, 10);
        }

        #endregion Test Methods

        #region Private Methods
        /// <summary>
        /// This method creates a new countrySettings 
        /// </summary>
        /// <returns>Returns countrySettings</returns>
        /// <param name="dataManager"> Used for dataManager </param>
        private CountrySetting CreateNewCountrySettings(IDataSourceManager dataManager)
        {
            var countrySetting = new CountrySetting();
            countrySetting.UnskilledLaborCost = 10;
            dataManager.CountrySettingRepository.Save(countrySetting);
            dataManager.SaveChanges();

            return countrySetting;
        }

        #endregion Private Methods
    }
}
