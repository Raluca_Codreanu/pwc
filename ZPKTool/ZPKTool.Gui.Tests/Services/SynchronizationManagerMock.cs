﻿namespace ZPKTool.Gui.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using ZPKTool.Synchronization;

    /// <summary>
    ///  A mock service that handles synchronization logic.
    /// </summary>
    public class SynchronizationManagerMock : ISynchronizationManager
    {
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="SynchronizationManagerMock"/> class.
        /// </summary>
        public SynchronizationManagerMock()
        {
            this.CurrentStatus = new SyncStatus();
            if (this.Progress != null)
            {      
            }
        }

        /// <summary>
        /// Gets a value indicating whether
        /// the synchronization was called or not.
        /// </summary>
        public bool SyncWasCalled
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether 
        /// the abort method was called or not.
        /// </summary>
        public bool AbortWasCalled
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the synchronization current status.
        /// </summary>
        public SyncStatus CurrentStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the synchronization conflicts.
        /// </summary>
        public ICollection<SynchronizationConflict> Conflicts
        {
            get;
            set;
        }

        /// <summary>
        /// Occurs during the synchronization process progress.
        /// </summary>
        public event SynchronizationProgressHandler Progress;

        /// <summary>
        /// Initializes the scopes.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="keepTrackOfSuccessfullyMaintainedUser">if set to <c>true</c> 
        /// keeps track of successfully maintained user.</param>
        public void InitializeScopesAndConvert(
            Guid userGuid, bool keepTrackOfSuccessfullyMaintainedUser)
        {
        }

        /// <summary>
        /// Determines whether a synchronization scope(s) update is necessary.
        /// </summary>
        /// <param name="userGuid">The user ID for which to perform the check.</param>
        /// <returns>true if a scope update is necessary; otherwise, false.</returns>
        public bool IsSyncScopeUpdateNecessary(Guid userGuid)
        {
            return true;
        }

        /// <summary>
        /// Starts a synchronization session.
        /// </summary>
        /// <param name="syncStaticDataAndSettings">if set to true the static data 
        /// and settings should be synced.</param>
        /// <param name="syncReleasedProjects">if set to true the released projects should be synced.</param>
        /// <param name="syncMasterData">if set to true the master data should be synced.</param>
        /// <param name="syncMyProjects">if set to true my projects should be synced.</param>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve any conflicts 
        /// that appeared in previous runs and stored in the Conflicts property.</param>
        public void Synchronize(
            bool syncStaticDataAndSettings,
            bool syncReleasedProjects,
            bool syncMasterData,
            bool syncMyProjects,
            Guid userGuid,
            bool resolveConflicts)
        {
            this.CurrentStatus.State = SyncState.Running;
            this.SyncWasCalled = true;
        }
        
        /// <summary>
        /// Starts the synchronization process with the last settings for data to be sync and resolves the conflicts; the method actually restarts the last sync, to resolve it's conflicts.
        /// </summary>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        public void ResynchronizeAndResolveConflicts(Guid userGuid)
        {
        }

        /// <summary>
        /// Starts  the Synchronization process.
        /// </summary>
        /// <param name="syncType">Type of the synchronization.</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="direction">The synchronization direction.</param>
        /// <param name="notifyProgress">A value indicating whether to notify
        /// the progress when sync is finished, or not.</param>
        public void Synchronize(
            SyncType syncType,
            Guid userGuid,
            SyncDirection direction,
            bool notifyProgress)
        {
        }

        /// <summary>
        /// Aborts the synchronization.
        /// </summary>
        public void AbortSynchronization()
        {
            this.AbortWasCalled = true;
        }

        /// <summary>
        /// Resets the current status.
        /// </summary>
        public void ResetCurrentStatus()
        {
        }
    }
}
