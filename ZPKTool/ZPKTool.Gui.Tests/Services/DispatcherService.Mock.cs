﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock implementation of DispatcherService.
    /// </summary>
    [Export(typeof(IDispatcherService))]
    public class DispatcherServiceMock : IDispatcherService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherServiceMock"/> class.
        /// </summary>
        public DispatcherServiceMock()
        {
        }

        /// <summary>
        /// Determines whether the calling thread is the thread associated with the UI dispatcher.
        /// </summary>
        /// <returns>
        /// true if the calling thread is associated with the UI dispatcher or the UI dispatcher is null; otherwise, false.
        /// </returns>
        public bool CheckAccess()
        {
            return false;
        }

        /// <summary>
        /// Executes the specified action asynchronously on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void BeginInvoke(Action action)
        {            
            action();
        }

        /// <summary>
        /// Executes the specified action asynchronously at the specified priority on the thread the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is execute.</param>
        public void BeginInvoke(Action action, DispatcherPriority priority)
        {            
            action();
        }

        /// <summary>
        /// Executes the specified action on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void Invoke(Action action)
        {
            action();
        }

        /// <summary>
        /// Executes the specified action synchronously at the specified priority on the thread on which the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is executed.</param>
        public void Invoke(Action action, DispatcherPriority priority)
        {
            action();
        }
    }
}
