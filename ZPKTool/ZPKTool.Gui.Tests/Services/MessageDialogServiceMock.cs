﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock implementation of IMessageService
    /// </summary>
    [Export(typeof(IMessageDialogService))]
    public class MessageDialogServiceMock : IMessageDialogService
    {
        /// <summary>
        /// Gets or sets the message box result.
        /// </summary>
        public MessageDialogResult MessageDialogResult { get; set; }

        /// <summary>
        /// Gets or sets count of calling a method.
        /// </summary>
        public int ShowCallCount { get; set; }

        /// <summary>
        /// Gets or sets the displayed message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// A mock implementation of Show(Exception e, object owner) method. 
        /// Increments 'ShowCallCount' property whenever it is called.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <param name="owner">The owner of the message box.</param>
        /// <returns> ShowReturnValue property </returns>
        public MessageDialogResult Show(Exception e, object owner)
        {
            ++this.ShowCallCount;
            this.Message = e.InnerException.Message;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// A mock implementation of Show(string message, MessageBoxType type, object owner) method. 
        /// Increments 'ShowCallCount' property whenever it is called.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type of the message.</param>
        /// <param name="owner">The owner of the message box.</param>
        /// <returns> ShowReturnValue property </returns>
        public MessageDialogResult Show(string message, MessageDialogType type, object owner)
        {
            ++this.ShowCallCount;
            this.Message = message;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// A mock implementation of Show(Exception e) method. 
        /// Increments 'ShowCallCount' property whenever it is called. 
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns> ShowReturnValue property </returns>
        public MessageDialogResult Show(Exception e)
        {
            ++this.ShowCallCount;
            this.Message = e.InnerException.Message;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// A mock implementation of Show(string message, MessageBoxType type) method. 
        /// Increments 'ShowCallCount' property whenever it is called. 
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type of the message.</param>
        /// <returns> ShowReturnValue property </returns>
        public MessageDialogResult Show(string message, MessageDialogType type)
        {
            ++this.ShowCallCount;
            this.Message = message;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// A mock implementation of Show(MessageDialogInfo dialogInfo) method. 
        /// </summary>
        /// <param name="dialogInfo">The message dialog information used to compute the windows content</param>
        /// <returns>A value that signifies how the dialog window was closed by the user.</returns>
        public MessageDialogResult Show(MessageDialogInfo dialogInfo)
        {
            this.ShowCallCount++;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// A mock implementation of Show(string message, MessageBoxType type, string note) method. 
        /// Increments 'ShowCallCount' property whenever it is called. 
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="additionalMessage">The additional message</param>
        /// <param name="type">The type of the message.</param>
        /// <returns> ShowReturnValue property </returns>
        public MessageDialogResult Show(string message, string additionalMessage, MessageDialogType type)
        {
            ++this.ShowCallCount;
            this.Message = message;
            return this.MessageDialogResult;
        }

        /// <summary>
        /// Gets the error message containing some information about the specified exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>
        /// A string containing the error message.
        /// </returns>
        public string GetErrorMessage(Exception e)
        {
            return this.GetErrorMessage(e);
        }
    }
}
