﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock implementation of OnlineCheckService.
    /// </summary>
    [Export(typeof(IOnlineCheckService))]
    public class OnlineCheckServiceMock : IOnlineCheckService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineCheckServiceMock"/> class.
        /// </summary>
        public OnlineCheckServiceMock()
        {
            this.IsOnline = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the central server is online.
        /// </summary>
        public bool IsOnline { get; set; }

        public DbOnlineCheckResult LastOnlineCheck { get; set; }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="interval">The interval at which the online check is performed, in milliseconds.
        /// Should be greater than 0 and less then int.MaxValue; if is not it defaults to 60000 (1 minute).</param>
        /// <exception cref="System.NotImplementedException">The exception thrown</exception>
        public void Start(double interval)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks the online state of the central server on the calling thread.
        /// </summary>
        /// <returns>
        /// True if the central server is online, false if is offline.
        /// </returns>
        /// <exception cref="System.NotImplementedException">The exception thrown</exception>
        public bool CheckNow()
        {
            return this.IsOnline;
        }

        /// <summary>
        /// Checks asynchronously the online state of the central server.
        /// </summary>
        /// <exception cref="System.NotImplementedException">The exception thrown</exception>
        public void CheckAsync()
        {
            throw new NotImplementedException();
        }
    }
}