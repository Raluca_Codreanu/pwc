﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock service that handles the units adapters and holds the list of currencies and base currency for the selected object.
    /// </summary>
    [Export(typeof(IUnitsService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UnitsServiceMock : IUnitsService
    {
        /// <summary>
        /// The unit adapter that contains the default units
        /// </summary>
        private UnitsAdapter unitsAdapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsServiceMock"/> class.
        /// </summary>
        [ImportingConstructor]
        public UnitsServiceMock()
        {
            this.unitsAdapter = new UnitsAdapter(null);

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;

            this.Currencies = new Collection<Currency>();
            this.Currencies.Add(defaultCurrency);
            this.BaseCurrency = defaultCurrency;

            this.unitsAdapter.Currencies = this.Currencies;
            this.unitsAdapter.BaseCurrency = this.BaseCurrency;
        }

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        public ICollection<Currency> Currencies { get; set; }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public Currency BaseCurrency { get; set; }

        /// <summary>
        /// Gets a units adapter with the specified data manager.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        /// <returns>The default units adapter</returns>
        public UnitsAdapter GetUnitsAdapter(IDataSourceManager dataManager)
        {
            return this.unitsAdapter;
        }
    }
}
