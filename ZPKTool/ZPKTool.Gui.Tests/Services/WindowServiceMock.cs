﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock service used to simulate the action of displaying message boxes and views inside windows.
    /// </summary>
    [Export(typeof(IWindowService))]
    public class WindowServiceMock : IWindowService
    {
        /// <summary>
        /// The UI Dispatcher.
        /// </summary>
        private IDispatcherService dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowServiceMock"/> class.
        /// </summary>
        /// <param name="messageBox">The message box service used to display the message boxes.</param>
        /// <param name="dispatcher">The UI dispatcher.</param>
        [ImportingConstructor]
        public WindowServiceMock(IMessageDialogService messageBox, IDispatcherService dispatcher)
        {
            this.MessageDialogService = messageBox;
            this.dispatcher = dispatcher;
            this.IsOpen = false;
            this.DisplayedView = null;
        }

        /// <summary>
        /// Gets or sets the source of an interaction (usually a view), when this instance is used with InteractionTrigger.
        /// </summary>
        public DependencyObject TriggerSource { get; set; }

        /// <summary>
        /// Gets a service that can be used to display message dialogs.
        /// </summary>
        public IMessageDialogService MessageDialogService { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the window is closed or open.
        /// </summary>
        public bool IsOpen { get; set; }

        /// <summary>
        /// Gets the view model displayed in the dialog or in the window.
        /// </summary>
        public object DisplayedView { get; private set; }

        /// <summary>
        /// Shows the view associated with the specified view-model in a window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model template that defines the desired view. Should be set when you have in the same scope multiple templates
        /// for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <param name="windowClosedAction">Represents the action performed when the window is closed. Defaults to null.</param>
        public void ShowViewInWindow(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false, Action windowClosedAction = null)
        {
            this.DisplayedView = viewModel;
            this.IsOpen = true;
        }

        /// <summary>
        /// Shows the view associated with the specified view-model in a dialog window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model template that defines the desired view. Should be set when you have in the same scope multiple templates
        /// for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <returns>
        /// A value that signifies how the dialog window was closed by the user.
        /// </returns>
        /// ///
        public bool? ShowViewInDialog(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false)
        {
            this.DisplayedView = viewModel;
            this.IsOpen = true;
            return this.IsOpen;
        }

        /// <summary>
        /// Closes the window containing the view of a specified view-model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>
        /// true if a window was found for the view and was closed; otherwise false.
        /// </returns>
        public bool CloseViewWindow(object viewModel)
        {
            this.DisplayedView = null;
            this.IsOpen = false;
            return this.IsOpen;
        }

        /// <summary>
        /// Shows the specified window.
        /// </summary>
        /// <param name="window">The window to show.</param>
        public void ShowWindow(Window window)
        {
        }

        /// <summary>
        /// Shows the specified window as a modal dialog.
        /// </summary>
        /// <param name="window">The window to show.</param>
        /// <returns>
        /// A value that signifies how the dialog window was closed by the user.
        /// </returns>
        public bool? ShowDialog(Window window)
        {
            return true;
        }

        /// <summary>
        /// Closes the window containing the view of a specified view-model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>
        /// true if a window was found for the view and was closed; otherwise false.
        /// </returns>
        public bool HideViewWindow(object viewModel)
        {
            return true;
        }

        /// <summary>
        /// Gets the view-model window.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>
        /// The window, coresponding to the view-model instance passed as parameter.
        /// </returns>
        public Window GetViewModelWindow(object viewModel)
        {
            return new Window();
        }
    }
}
