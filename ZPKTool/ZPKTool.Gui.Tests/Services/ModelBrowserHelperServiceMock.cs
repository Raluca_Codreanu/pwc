﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using ZPKTool.Business.Export;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// This service is used to provide various information (e.g. parent project, media) for the ModelBrowser.
    /// </summary>
    [Export(typeof(IModelBrowserHelperService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ModelBrowserHelperServiceMock : IModelBrowserHelperService
    {
        /// <summary>
        /// Gets the media for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of imported media objects representing video or image which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        public IEnumerable<ImportedMedia> GetMediaForEntity(object entity)
        {
            return new List<ImportedMedia>();
        }

        /// <summary>
        /// Gets the documents for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of imported media objects representing documents which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        public IEnumerable<ImportedMedia> GetDocumentsForEntity(object entity)
        {
            return new List<ImportedMedia>();
        }

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="PartCostCalculationParameters" />,
        /// which is necessary for calculating the cost of a Part or Raw Part.
        /// </summary>
        /// <returns>
        /// The cost calculation parameters or null if the cost calculation data is not available (pre v1.3).
        /// </returns>
        public PartCostCalculationParameters GetPartCostCalculationParameters()
        {
            return new PartCostCalculationParameters();
        }

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="AssemblyCostCalculationParameters" />,
        /// which is necessary for calculating the cost of an Assembly.
        /// </summary>
        /// <returns>
        /// The cost calculation parameters, or null if the cost calculation data is not available (pre v1.3).
        /// </returns>
        public AssemblyCostCalculationParameters GetAssemblyCostCalculationParameters()
        {
            return new AssemblyCostCalculationParameters();
        }

        /// <summary>
        /// Sets the data for the currently opened model into this instance.
        /// </summary>
        /// <param name="modelData">The model data.</param>
        public void SetCurrentModelData(ImportedData<object> modelData)
        {
        }
    }
}
