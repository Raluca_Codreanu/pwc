﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// A mock implementation of IFolderBrowserService.
    /// </summary>
    [Export(typeof(IFolderBrowserService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FolderBrowserServiceMock : IFolderBrowserService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FolderBrowserServiceMock"/> class.
        /// </summary>
        public FolderBrowserServiceMock()
        {
        }

        #region IFolderBrowserService Members

        /// <summary>
        /// Gets or sets the path selected by the user.
        /// </summary>
        public string SelectedPath { get; set; }

        /// <summary>
        /// Gets or sets the descriptive text displayed above the folders tree view in the dialog box.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has selected a folder or not.
        /// </summary>
        public bool FolderIsSelected { get; set; }

        /// <summary>
        /// Shows the folder browser dialog that allows the user to select a folder.
        /// </summary>
        /// <returns>
        /// True if the user has selected a folder (clicked the OK button); otherwise false.
        /// </returns>
        public bool ShowFolderBrowserDialog()
        {
            return this.FolderIsSelected;
        }

        #endregion IFolderBrowserService Members
    }
}
