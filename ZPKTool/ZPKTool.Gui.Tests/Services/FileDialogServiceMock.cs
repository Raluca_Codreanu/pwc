﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Win32;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Tests.Services
{
    /// <summary>
    /// WPF implementation of the service that allows to open or save files
    /// </summary>
    [Export(typeof(IFileDialogService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FileDialogServiceMock : IFileDialogService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileDialogServiceMock"/> class.
        /// </summary>
        public FileDialogServiceMock()
        {
            this.FileNames = new List<string>();
        }

        /// <summary>
        /// String containing the full path of the file selected in a file dialog.
        /// </summary>
        private string fileName;

        /// <summary>
        ///  Gets or sets a string containing the full path of the file selected in a file dialog.
        /// </summary>
        public string FileName
        {
            get
            {
                return this.fileName;
            }

            set
            {
                this.fileName = value;

                this.FileNames.Clear();
                this.FileNames.Add(value);
            }
        }

        /// <summary>
        /// Gets or sets an array that contains one file name for each selected file.
        /// </summary>
        public ICollection<string> FileNames { get; private set; }

        /// <summary>
        /// Gets or sets the filter string that determines what types of files are displayed in a file dialog.
        /// </summary>
        /// <value>
        /// A System.String that contains the filter.
        /// The default is System.String.Empty, which means that no filter is applied and all file types are displayed.
        /// </value>
        /// <exception cref="System.ArgumentException">The filter string is invalid.</exception>
        public string Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index of the filter currently selected in a file dialog.
        /// </summary>
        public int FilterIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the initial directory that is displayed by a file dialog.
        /// </summary>
        public string InitialDirectory
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether an OpenFileDialog allows users to select multiple files.
        /// </summary>
        /// <value>
        /// true if multiple selections are allowed; otherwise, false. The default is false.
        /// </value>
        public bool Multiselect
        {
            get;
            set;
        }

        /// <summary>
        /// A mock implementation of ShowOpenFileDialog() method
        /// </summary>
        /// <returns>Returns always true</returns>
        public bool? ShowOpenFileDialog()
        {
            return true;
        }

        /// <summary>
        /// A mock implementation of ShowOpenFileDialog(Window owner) method
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>Returns always true</returns>
        public bool? ShowOpenFileDialog(Window owner)
        {
            return true;
        }

        /// <summary>
        /// A mock implementation of ShowSaveFileDialog() method
        /// </summary>
        /// <returns>Returns always true</returns>
        public bool? ShowSaveFileDialog()
        {
            return true;
        }

        /// <summary>
        /// A mock implementation of ShowSaveFileDialog(Window owner) method
        /// </summary>
        /// <param name="owner">The owner of the dialog.></param>
        /// <returns>Returns always true</returns>
        public bool? ShowSaveFileDialog(Window owner)
        {
            return true;
        }

        /// <summary>
        ///  A mock implementation of Reset() method
        /// </summary>
        public void Reset()
        {
        }
    }
}
