//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RawMaterial : System.ComponentModel.INotifyPropertyChanged
    {
        public RawMaterial()
        {
            this.priceHistory = new HashSet<RawMaterialsPriceHistory>();
            this.OnItitialized();
        }
    

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        private string normName;

        public string NormName
        {
            get
            {
                return this.normName;
            }

            set
            {
                if (this.normName != value)
                {
                    this.normName = value;
                    this.OnPropertyChanged("NormName");
                }
            }
        }

        private Nullable<decimal> scrambledPrice;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the Price field
        /// </summary>
        public Nullable<decimal> ScrambledPrice
        {
            get
            {
                return this.scrambledPrice;
            }

            set
            {
                if (this.scrambledPrice != value)
                {
                    this.scrambledPrice = value;
                    this.OnPropertyChanged("ScrambledPrice");
                }
            }
        }

        private Nullable<decimal> rejectRatio;

        /// <summary>
        /// A value between 0 and 1, with 4 decimal places, representing a percentage.
        /// </summary>
        public Nullable<decimal> RejectRatio
        {
            get
            {
                return this.rejectRatio;
            }

            set
            {
                if (this.rejectRatio != value)
                {
                    this.rejectRatio = value;
                    this.OnPropertyChanged("RejectRatio");
                }
            }
        }

        private string remarks;

        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                if (this.remarks != value)
                {
                    this.remarks = value;
                    this.OnPropertyChanged("Remarks");
                }
            }
        }

        private Nullable<decimal> parentWeight;

        public Nullable<decimal> ParentWeight
        {
            get
            {
                return this.parentWeight;
            }

            set
            {
                if (this.parentWeight != value)
                {
                    this.parentWeight = value;
                    this.OnPropertyChanged("ParentWeight");
                }
            }
        }

        private Nullable<decimal> quantity;

        public Nullable<decimal> Quantity
        {
            get
            {
                return this.quantity;
            }

            set
            {
                if (this.quantity != value)
                {
                    this.quantity = value;
                    this.OnPropertyChanged("Quantity");
                }
            }
        }

        private Nullable<decimal> scrapRefundRatio;

        /// <summary>
        /// A value between 0 and 1, with 4 decimal places, representing a percentage.
        /// </summary>
        public Nullable<decimal> ScrapRefundRatio
        {
            get
            {
                return this.scrapRefundRatio;
            }

            set
            {
                if (this.scrapRefundRatio != value)
                {
                    this.scrapRefundRatio = value;
                    this.OnPropertyChanged("ScrapRefundRatio");
                }
            }
        }

        private Nullable<decimal> loss;

        /// <summary>
        /// A value between 0 and 1, with 4 decimal places, representing a percentage.
        /// </summary>
        public Nullable<decimal> Loss
        {
            get
            {
                return this.loss;
            }

            set
            {
                if (this.loss != value)
                {
                    this.loss = value;
                    this.OnPropertyChanged("Loss");
                }
            }
        }

        private bool isMasterData;

        public bool IsMasterData
        {
            get
            {
                return this.isMasterData;
            }

            set
            {
                if (this.isMasterData != value)
                {
                    this.isMasterData = value;
                    this.OnPropertyChanged("IsMasterData");
                }
            }
        }

        private bool isDeleted;

        public bool IsDeleted
        {
            get
            {
                return this.isDeleted;
            }

            set
            {
                if (this.isDeleted != value)
                {
                    this.isDeleted = value;
                    this.OnPropertyChanged("IsDeleted");
                }
            }
        }

        private string nameUK;

        public string NameUK
        {
            get
            {
                return this.nameUK;
            }

            set
            {
                if (this.nameUK != value)
                {
                    this.nameUK = value;
                    this.OnPropertyChanged("NameUK");
                }
            }
        }

        private string nameUS;

        public string NameUS
        {
            get
            {
                return this.nameUS;
            }

            set
            {
                if (this.nameUS != value)
                {
                    this.nameUS = value;
                    this.OnPropertyChanged("NameUS");
                }
            }
        }

        private Nullable<decimal> sprue;

        public Nullable<decimal> Sprue
        {
            get
            {
                return this.sprue;
            }

            set
            {
                if (this.sprue != value)
                {
                    this.sprue = value;
                    this.OnPropertyChanged("Sprue");
                }
            }
        }

        private ScrapCalculationType scrapCalculationType;

        /// <summary>
        /// The type of scrap calculation. This value is a member of the ScrapCalculationType enumeration.
        /// </summary>
        public ScrapCalculationType ScrapCalculationType
        {
            get
            {
                return this.scrapCalculationType;
            }

            set
            {
                if (this.scrapCalculationType != value)
                {
                    this.scrapCalculationType = value;
                    this.OnPropertyChanged("ScrapCalculationType");
                }
            }
        }

        private string scrambledYieldStrength;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the YieldStrength field
        /// </summary>
        public string ScrambledYieldStrength
        {
            get
            {
                return this.scrambledYieldStrength;
            }

            set
            {
                if (this.scrambledYieldStrength != value)
                {
                    this.scrambledYieldStrength = value;
                    this.OnPropertyChanged("ScrambledYieldStrength");
                }
            }
        }

        private string scrambledRuptureStrength;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the RuptureStrength field
        /// </summary>
        public string ScrambledRuptureStrength
        {
            get
            {
                return this.scrambledRuptureStrength;
            }

            set
            {
                if (this.scrambledRuptureStrength != value)
                {
                    this.scrambledRuptureStrength = value;
                    this.OnPropertyChanged("ScrambledRuptureStrength");
                }
            }
        }

        private string scrambledDensity;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the Density field
        /// </summary>
        public string ScrambledDensity
        {
            get
            {
                return this.scrambledDensity;
            }

            set
            {
                if (this.scrambledDensity != value)
                {
                    this.scrambledDensity = value;
                    this.OnPropertyChanged("ScrambledDensity");
                }
            }
        }

        private string scrambledMaxElongation;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the MaxElongation field
        /// </summary>
        public string ScrambledMaxElongation
        {
            get
            {
                return this.scrambledMaxElongation;
            }

            set
            {
                if (this.scrambledMaxElongation != value)
                {
                    this.scrambledMaxElongation = value;
                    this.OnPropertyChanged("ScrambledMaxElongation");
                }
            }
        }

        private string scrambledGlassTransitionTemperature;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the GlassTransitionTemperature field
        /// </summary>
        public string ScrambledGlassTransitionTemperature
        {
            get
            {
                return this.scrambledGlassTransitionTemperature;
            }

            set
            {
                if (this.scrambledGlassTransitionTemperature != value)
                {
                    this.scrambledGlassTransitionTemperature = value;
                    this.OnPropertyChanged("ScrambledGlassTransitionTemperature");
                }
            }
        }

        private string scrambledRx;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the Rx field
        /// </summary>
        public string ScrambledRx
        {
            get
            {
                return this.scrambledRx;
            }

            set
            {
                if (this.scrambledRx != value)
                {
                    this.scrambledRx = value;
                    this.OnPropertyChanged("ScrambledRx");
                }
            }
        }

        private string scrambledRm;

        /// <summary>
        /// Scrambled Field. Do not use this field in the application. The true value of the field is in the Rm field
        /// </summary>
        public string ScrambledRm
        {
            get
            {
                return this.scrambledRm;
            }

            set
            {
                if (this.scrambledRm != value)
                {
                    this.scrambledRm = value;
                    this.OnPropertyChanged("ScrambledRm");
                }
            }
        }

        private bool isScrambled;

        public bool IsScrambled
        {
            get
            {
                return this.isScrambled;
            }

            set
            {
                if (this.isScrambled != value)
                {
                    this.isScrambled = value;
                    this.OnPropertyChanged("IsScrambled");
                }
            }
        }

        private System.Guid guid;

        public System.Guid Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                if (this.guid != value)
                {
                    this.guid = value;
                    this.OnPropertyChanged("Guid");
                }
            }
        }

        private Nullable<decimal> stockKeeping;

        /// <summary>
        /// Represents an integral number of days.
        /// </summary>
        public Nullable<decimal> StockKeeping
        {
            get
            {
                return this.stockKeeping;
            }

            set
            {
                if (this.stockKeeping != value)
                {
                    this.stockKeeping = value;
                    this.OnPropertyChanged("StockKeeping");
                }
            }
        }

        private Nullable<decimal> recyclingRatio;

        public Nullable<decimal> RecyclingRatio
        {
            get
            {
                return this.recyclingRatio;
            }

            set
            {
                if (this.recyclingRatio != value)
                {
                    this.recyclingRatio = value;
                    this.OnPropertyChanged("RecyclingRatio");
                }
            }
        }

        private bool isStockMasterData;

        public bool IsStockMasterData
        {
            get
            {
                return this.isStockMasterData;
            }

            set
            {
                if (this.isStockMasterData != value)
                {
                    this.isStockMasterData = value;
                    this.OnPropertyChanged("IsStockMasterData");
                }
            }
        }

        private Nullable<System.DateTime> createDate;

        public Nullable<System.DateTime> CreateDate
        {
            get
            {
                return this.createDate;
            }

            set
            {
                if (this.createDate != value)
                {
                    this.createDate = value;
                    this.OnPropertyChanged("CreateDate");
                }
            }
        }

        private Nullable<System.DateTime> calculationCreateDate;

        public Nullable<System.DateTime> CalculationCreateDate
        {
            get
            {
                return this.calculationCreateDate;
            }

            set
            {
                if (this.calculationCreateDate != value)
                {
                    this.calculationCreateDate = value;
                    this.OnPropertyChanged("CalculationCreateDate");
                }
            }
        }

        private Manufacturer manufacturer;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Manufacturer Manufacturer
        {
            get
            {
                return this.manufacturer;
            }

            set
            {
                if (this.manufacturer != value)
                {
                    this.manufacturer = value;
                    this.OnPropertyChanged("Manufacturer");
                }
            }
        }

        private MaterialsClassification materialsClassificationL1;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MaterialsClassification MaterialsClassificationL1
        {
            get
            {
                return this.materialsClassificationL1;
            }

            set
            {
                if (this.materialsClassificationL1 != value)
                {
                    this.materialsClassificationL1 = value;
                    this.OnPropertyChanged("MaterialsClassificationL1");
                }
            }
        }

        private MaterialsClassification materialsClassificationL2;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MaterialsClassification MaterialsClassificationL2
        {
            get
            {
                return this.materialsClassificationL2;
            }

            set
            {
                if (this.materialsClassificationL2 != value)
                {
                    this.materialsClassificationL2 = value;
                    this.OnPropertyChanged("MaterialsClassificationL2");
                }
            }
        }

        private MaterialsClassification materialsClassificationL3;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MaterialsClassification MaterialsClassificationL3
        {
            get
            {
                return this.materialsClassificationL3;
            }

            set
            {
                if (this.materialsClassificationL3 != value)
                {
                    this.materialsClassificationL3 = value;
                    this.OnPropertyChanged("MaterialsClassificationL3");
                }
            }
        }

        private MaterialsClassification materialsClassificationL4;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MaterialsClassification MaterialsClassificationL4
        {
            get
            {
                return this.materialsClassificationL4;
            }

            set
            {
                if (this.materialsClassificationL4 != value)
                {
                    this.materialsClassificationL4 = value;
                    this.OnPropertyChanged("MaterialsClassificationL4");
                }
            }
        }

        private MeasurementUnit priceUnitBase;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MeasurementUnit PriceUnitBase
        {
            get
            {
                return this.priceUnitBase;
            }

            set
            {
                if (this.priceUnitBase != value)
                {
                    this.priceUnitBase = value;
                    this.OnPropertyChanged("PriceUnitBase");
                }
            }
        }

        private MeasurementUnit parentWeightUnitBase;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MeasurementUnit ParentWeightUnitBase
        {
            get
            {
                return this.parentWeightUnitBase;
            }

            set
            {
                if (this.parentWeightUnitBase != value)
                {
                    this.parentWeightUnitBase = value;
                    this.OnPropertyChanged("ParentWeightUnitBase");
                }
            }
        }

        private MeasurementUnit quantityUnitBase;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MeasurementUnit QuantityUnitBase
        {
            get
            {
                return this.quantityUnitBase;
            }

            set
            {
                if (this.quantityUnitBase != value)
                {
                    this.quantityUnitBase = value;
                    this.OnPropertyChanged("QuantityUnitBase");
                }
            }
        }

        private Media media;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Media Media
        {
            get
            {
                return this.media;
            }

            set
            {
                if (this.media != value)
                {
                    this.media = value;
                    this.OnPropertyChanged("Media");
                }
            }
        }

        private Part part;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Part Part
        {
            get
            {
                return this.part;
            }

            set
            {
                if (this.part != value)
                {
                    this.part = value;
                    this.OnPropertyChanged("Part");
                }
            }
        }

        private RawMaterialDeliveryType deliveryType;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual RawMaterialDeliveryType DeliveryType
        {
            get
            {
                return this.deliveryType;
            }

            set
            {
                if (this.deliveryType != value)
                {
                    this.deliveryType = value;
                    this.OnPropertyChanged("DeliveryType");
                }
            }
        }

        private User owner;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual User Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                if (this.owner != value)
                {
                    this.owner = value;
                    this.OnPropertyChanged("Owner");
                }
            }
        }

        private ICollection<RawMaterialsPriceHistory> priceHistory;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<RawMaterialsPriceHistory> PriceHistory
        {
            get
            {
                return this.priceHistory;
            }

            set
            {
                if (this.priceHistory != value)
                {
                    this.priceHistory = value;
                    this.OnPropertyChanged("PriceHistory");
                }
            }
        }
    
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when the value of a property has changed, in order to raise the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
