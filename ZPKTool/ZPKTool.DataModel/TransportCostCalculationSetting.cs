//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransportCostCalculationSetting : System.ComponentModel.INotifyPropertyChanged
    {
        public TransportCostCalculationSetting()
        {
            this.OnItitialized();
        }
    

        private System.Guid guid;

        public System.Guid Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                if (this.guid != value)
                {
                    this.guid = value;
                    this.OnPropertyChanged("Guid");
                }
            }
        }

        private int daysPerYear;

        public int DaysPerYear
        {
            get
            {
                return this.daysPerYear;
            }

            set
            {
                if (this.daysPerYear != value)
                {
                    this.daysPerYear = value;
                    this.OnPropertyChanged("DaysPerYear");
                }
            }
        }

        private Nullable<decimal> averageLoadTarget;

        public Nullable<decimal> AverageLoadTarget
        {
            get
            {
                return this.averageLoadTarget;
            }

            set
            {
                if (this.averageLoadTarget != value)
                {
                    this.averageLoadTarget = value;
                    this.OnPropertyChanged("AverageLoadTarget");
                }
            }
        }

        private decimal vanTypeCost;

        public decimal VanTypeCost
        {
            get
            {
                return this.vanTypeCost;
            }

            set
            {
                if (this.vanTypeCost != value)
                {
                    this.vanTypeCost = value;
                    this.OnPropertyChanged("VanTypeCost");
                }
            }
        }

        private decimal truckTrailerCost;

        public decimal TruckTrailerCost
        {
            get
            {
                return this.truckTrailerCost;
            }

            set
            {
                if (this.truckTrailerCost != value)
                {
                    this.truckTrailerCost = value;
                    this.OnPropertyChanged("TruckTrailerCost");
                }
            }
        }

        private decimal megaTrailerCost;

        public decimal MegaTrailerCost
        {
            get
            {
                return this.megaTrailerCost;
            }

            set
            {
                if (this.megaTrailerCost != value)
                {
                    this.megaTrailerCost = value;
                    this.OnPropertyChanged("MegaTrailerCost");
                }
            }
        }

        private decimal truck7tCost;

        public decimal Truck7tCost
        {
            get
            {
                return this.truck7tCost;
            }

            set
            {
                if (this.truck7tCost != value)
                {
                    this.truck7tCost = value;
                    this.OnPropertyChanged("Truck7tCost");
                }
            }
        }

        private decimal truck3_5tCost;

        public decimal Truck3_5tCost
        {
            get
            {
                return this.truck3_5tCost;
            }

            set
            {
                if (this.truck3_5tCost != value)
                {
                    this.truck3_5tCost = value;
                    this.OnPropertyChanged("Truck3_5tCost");
                }
            }
        }

        private System.DateTime timestamp;

        public System.DateTime Timestamp
        {
            get
            {
                return this.timestamp;
            }

            set
            {
                if (this.timestamp != value)
                {
                    this.timestamp = value;
                    this.OnPropertyChanged("Timestamp");
                }
            }
        }

        private TransportCostCalculation transportCostCalculation;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual TransportCostCalculation TransportCostCalculation
        {
            get
            {
                return this.transportCostCalculation;
            }

            set
            {
                if (this.transportCostCalculation != value)
                {
                    this.transportCostCalculation = value;
                    this.OnPropertyChanged("TransportCostCalculation");
                }
            }
        }

        private User owner;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual User Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                if (this.owner != value)
                {
                    this.owner = value;
                    this.OnPropertyChanged("Owner");
                }
            }
        }
    
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when the value of a property has changed, in order to raise the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
