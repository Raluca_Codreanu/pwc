//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class MachinesClassification : System.ComponentModel.INotifyPropertyChanged
    {
        public MachinesClassification()
        {
            this.machines = new HashSet<Machine>();
            this.typeClassifications = new HashSet<Machine>();
            this.subClassifications = new HashSet<Machine>();
            this.classificationsLevel4 = new HashSet<Machine>();
            this.children = new HashSet<MachinesClassification>();
            this.OnItitialized();
        }
    

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        private bool isReleased;

        public bool IsReleased
        {
            get
            {
                return this.isReleased;
            }

            set
            {
                if (this.isReleased != value)
                {
                    this.isReleased = value;
                    this.OnPropertyChanged("IsReleased");
                }
            }
        }

        private System.Guid guid;

        public System.Guid Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                if (this.guid != value)
                {
                    this.guid = value;
                    this.OnPropertyChanged("Guid");
                }
            }
        }

        private Nullable<System.DateTime> createDate;

        public Nullable<System.DateTime> CreateDate
        {
            get
            {
                return this.createDate;
            }

            set
            {
                if (this.createDate != value)
                {
                    this.createDate = value;
                    this.OnPropertyChanged("CreateDate");
                }
            }
        }

        private ICollection<Machine> machines;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<Machine> Machines
        {
            get
            {
                return this.machines;
            }

            set
            {
                if (this.machines != value)
                {
                    this.machines = value;
                    this.OnPropertyChanged("Machines");
                }
            }
        }

        private ICollection<Machine> typeClassifications;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<Machine> TypeClassifications
        {
            get
            {
                return this.typeClassifications;
            }

            set
            {
                if (this.typeClassifications != value)
                {
                    this.typeClassifications = value;
                    this.OnPropertyChanged("TypeClassifications");
                }
            }
        }

        private ICollection<Machine> subClassifications;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<Machine> SubClassifications
        {
            get
            {
                return this.subClassifications;
            }

            set
            {
                if (this.subClassifications != value)
                {
                    this.subClassifications = value;
                    this.OnPropertyChanged("SubClassifications");
                }
            }
        }

        private ICollection<Machine> classificationsLevel4;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<Machine> ClassificationsLevel4
        {
            get
            {
                return this.classificationsLevel4;
            }

            set
            {
                if (this.classificationsLevel4 != value)
                {
                    this.classificationsLevel4 = value;
                    this.OnPropertyChanged("ClassificationsLevel4");
                }
            }
        }

        private ICollection<MachinesClassification> children;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<MachinesClassification> Children
        {
            get
            {
                return this.children;
            }

            set
            {
                if (this.children != value)
                {
                    this.children = value;
                    this.OnPropertyChanged("Children");
                }
            }
        }

        private MachinesClassification parent;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MachinesClassification Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (this.parent != value)
                {
                    this.parent = value;
                    this.OnPropertyChanged("Parent");
                }
            }
        }
    
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when the value of a property has changed, in order to raise the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
