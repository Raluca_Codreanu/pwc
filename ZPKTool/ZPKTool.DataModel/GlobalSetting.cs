//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class GlobalSetting : System.ComponentModel.INotifyPropertyChanged
    {
        public GlobalSetting()
        {
            this.OnItitialized();
        }
    

        private System.Guid guid;

        public System.Guid Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                if (this.guid != value)
                {
                    this.guid = value;
                    this.OnPropertyChanged("Guid");
                }
            }
        }

        private string encryptedKey;

        public string EncryptedKey
        {
            get
            {
                return this.encryptedKey;
            }

            set
            {
                if (this.encryptedKey != value)
                {
                    this.encryptedKey = value;
                    this.OnPropertyChanged("EncryptedKey");
                }
            }
        }

        private string encryptedValue;

        public string EncryptedValue
        {
            get
            {
                return this.encryptedValue;
            }

            set
            {
                if (this.encryptedValue != value)
                {
                    this.encryptedValue = value;
                    this.OnPropertyChanged("EncryptedValue");
                }
            }
        }
    
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when the value of a property has changed, in order to raise the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
