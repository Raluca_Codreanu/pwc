﻿using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace System.Data.Entity
{
    /// <summary>
    /// Useful extension methods for use with the Entity Framework LINQ queries.
    /// </summary>
    public static class QueryableExtensions
    {
        /// <summary>
        /// Sets the merge option of the specified query.
        /// </summary>
        /// <typeparam name="T">The type of data on which the query works</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="mergeOption">The new merge option value.</param>
        /// <exception cref="System.ArgumentNullException">The query was null.</exception>
        /// <exception cref="System.InvalidOperationException">The type of the specified query is not supported.</exception>
        public static void SetMergeOption<T>(this IQueryable<T> query, MergeOption mergeOption) where T : class
        {
            if (query == null)
            {
                throw new ArgumentNullException("query", "The query was null.");
            }

            var objectQuery = GetUnderlyingObjectQuery(query);
            objectQuery.MergeOption = mergeOption;
        }

        /// <summary>
        /// Creates a copy of the specified query and applies the PreserveChanges merge option to it.
        /// </summary>
        /// <typeparam name="T">The type of data on which the query works</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>A new query having the PreserveChanges merge option.</returns>
        /// <exception cref="System.ArgumentNullException">The source query was null.</exception>
        /// <exception cref="System.InvalidOperationException">The type of the specified query is not supported.</exception>
        public static IQueryable<T> AsPreserveChanges<T>(this IQueryable<T> source) where T : class
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "The source query was null.");
            }

            var objectQuery = GetUnderlyingObjectQuery(source);

            var asIQueryable = (IQueryable)objectQuery;
            var newQuery = (ObjectQuery<T>)asIQueryable.Provider.CreateQuery(asIQueryable.Expression);
            newQuery.MergeOption = MergeOption.PreserveChanges;
            return newQuery;
        }

        /// <summary>
        /// Creates a copy of the specified query and applies the OverwriteChanges merge option to it.
        /// </summary>
        /// <typeparam name="T">The type of data on which the query works</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>A new query having the OverwriteChanges merge option.</returns>
        /// <exception cref="System.ArgumentNullException">The source query was null.</exception>
        /// <exception cref="System.InvalidOperationException">The type of the specified query is not supported.</exception>
        public static IQueryable<T> AsOverwriteChanges<T>(this IQueryable<T> source) where T : class
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "The source query was null.");
            }

            var objectQuery = GetUnderlyingObjectQuery(source);

            var asIQueryable = (IQueryable)objectQuery;
            var newQuery = (ObjectQuery<T>)asIQueryable.Provider.CreateQuery(asIQueryable.Expression);
            newQuery.MergeOption = MergeOption.OverwriteChanges;
            return newQuery;
        }

        /// <summary>
        /// Gets the underlying ObjectQuery{T} of the specified query.
        /// <para />
        /// If the query's type is ObjectQuery{T} it just casts it.
        /// If its type is DbQuery{T}, it extracts its internal ObjectQuery{T} instance.
        /// </summary>
        /// <typeparam name="T">The type of the data on which the query is performed.</typeparam>
        /// <param name="query">The query.</param>
        /// <returns>
        /// The underlying ObjectQuery{T} instance or null if the query is neither QbjectQuery{T} nor DbQuery{T}.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// Failed to get the internal ObjectQuery{T} instance of a DbQuery{T}
        /// or
        /// The type of the specified query is not supported.
        /// </exception>
        private static ObjectQuery<T> GetUnderlyingObjectQuery<T>(IQueryable<T> query)
        {
            var objectQuery = query as ObjectQuery<T>;
            if (objectQuery == null)
            {
                var dbQuery = query as DbQuery<T>;
                if (dbQuery != null)
                {
                    var internalQuery = dbQuery
                            .GetType()
                            .GetProperty("InternalQuery", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance)
                            .GetValue(dbQuery, null);

                    if (internalQuery == null)
                    {
                        throw new InvalidOperationException("DbQuery<T>.InternalQuery property was null.");
                    }

                    objectQuery = (ObjectQuery<T>)internalQuery
                        .GetType()
                        .GetProperty("ObjectQuery", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance)
                        .GetValue(internalQuery, null);

                    if (objectQuery == null)
                    {
                        throw new InvalidOperationException("IInternalQuery<T>.ObjectQuery property was null.");
                    }
                }
                else
                {
                    string message = string.Format("The query type '{0}' is not supported.", query.GetType().Name);
                    throw new InvalidOperationException(message);
                }
            }

            return objectQuery;
        }
    }
}
