﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial class for the Entity Framework generated Entity Context.
    /// </summary>
    public partial class DataEntities
    {
        #region Wrappers for Stored Procedures, Table Valued Functions and Functions

        /// <summary>
        /// Gets the hierarchy of folders to which a project belongs.        
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>
        /// A collection containing the IDs of the folders in the hierarchy. The 1st item in the collection is the direct parent of the project
        /// while the last one is the root of the hierarchy.
        /// </returns>
        public Collection<Guid> GetProjectParentFolderHierarchy(Guid projectId)
        {
            System.Data.SqlClient.SqlParameter projectIdParam = new System.Data.SqlClient.SqlParameter("ProjectId", projectId);
            var result = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Guid>(
                "SELECT [Guid] FROM dbo.GetProjectParentFolderHierarchy(@ProjectId)",
                projectIdParam);

            return new Collection<Guid>(result.ToList());
        }

        /// <summary>
        /// Finds the Id of the top-most parent of a specified child.
        /// </summary>
        /// <remarks>It is mainly used for finding the root of a Project hierarchy for navigation purposes.</remarks>
        /// <param name="childId">The Id of the child whose parent to find.</param>
        /// <param name="childTableName">The name of the table containing the child whose Id is specified by <paramref name="childId"/>.</param>
        /// <param name="parentType">A value indicating what type of top parent to find. Accepted values: 1 - Project, 2 - Assembly.</param>
        /// <returns>The Id of the top parent if found or Guid.Empty if not found.</returns>
        public Guid FindTopParentId(Guid childId, string childTableName, byte parentType)
        {
            ObjectParameter parentId = new ObjectParameter("parentId", typeof(Guid));
            this.FindTopParentId(childId, childTableName, parentType, parentId);

            return parentId.Value is Guid ? (Guid)parentId.Value : Guid.Empty;
        }

        /// <summary>
        /// Gets the list of ids of all sub-assemblies of an assembly. Includes all assemblies in its graph, not only its direct children assemblies.
        /// The id of the assembly is included in the result.
        /// </summary>
        /// <param name="assemblyId">The id of the assembly whose sub-assembly ids to get.</param>
        /// <returns>A collection containing the id of the sub-assemblies, including the id of the assembly.</returns>
        public Collection<Guid> GetSubAssembliesIds(Guid assemblyId)
        {
            System.Data.SqlClient.SqlParameter projectIdParam = new System.Data.SqlClient.SqlParameter("AssemblyId", assemblyId);
            var result = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Guid>(
                "SELECT [Guid] FROM dbo.GetAssemblyHierarchyFromAssembly(@AssemblyId)",
                projectIdParam);

            return new Collection<Guid>(result.ToList());
        }

        #endregion Wrappers for Stored Procedures, Table Valued Functions and Functions

        #region Navigation Properties related

        /// <summary>
        /// Gets the names of all navigation properties of a specified entity (both reference and collection navigation properties, excluding simple/scalar properties).        
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="referenceProperties">Will contain the reference properties.</param>
        /// <param name="collectionProperties">Will contain the collection properties.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="System.InvalidOperationException">The entity is not part of the data model.</exception>
        public void GetNavigationProperties(
            object entity,
            out IEnumerable<string> referenceProperties,
            out IEnumerable<string> collectionProperties)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            this.GetNavigationProperties(entity.GetType(), out referenceProperties, out collectionProperties);
        }

        /// <summary>
        /// Gets the names of all navigation properties of a specified entity type (both reference and collection navigation properties, excluding simple/scalar properties).        
        /// </summary>
        /// <param name="entityType">The entity type.</param>
        /// <param name="referenceProperties">Will contain the reference properties.</param>
        /// <param name="collectionProperties">Will contain the collection properties.</param>
        /// <exception cref="System.ArgumentNullException">The entity type was null.</exception>
        /// <exception cref="System.InvalidOperationException">The entity type is not part of the data model.</exception>
        public void GetNavigationProperties(
            Type entityType,
            out IEnumerable<string> referenceProperties,
            out IEnumerable<string> collectionProperties)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType", "The entity type was null.");
            }

            // Get the type information for the entity from the object context's metadata store. The type information contains the name of the navigation properties.
            string entityTypeName = ObjectContext.GetObjectType(entityType).Name;
            var edmEntityType = this.objectContext.MetadataWorkspace.GetItems<EntityType>(DataSpace.CSpace).FirstOrDefault(item => item.Name == entityTypeName);
            if (edmEntityType == null)
            {
                string message = string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "The type metadata of the entity type '{0}' could not be found in the object context's metadata store. Usually it this means that the type is not part of the data model.",
                    entityTypeName);
                throw new InvalidOperationException(message);
            }

            // Get the name of the navigation properties from the metadata.
            referenceProperties = edmEntityType.NavigationProperties
                .Where(p => p.ToEndMember.RelationshipMultiplicity == RelationshipMultiplicity.ZeroOrOne
                    || p.ToEndMember.RelationshipMultiplicity == RelationshipMultiplicity.One)
                .Select(p => p.Name)
                .ToList();

            collectionProperties = edmEntityType.NavigationProperties
                .Where(p => p.ToEndMember.RelationshipMultiplicity == RelationshipMultiplicity.Many)
                .Select(p => p.Name)
                .ToList();
        }

        /// <summary>
        /// Refreshes the specified collection navigation property from the database.
        /// All property changes are overwritten with the data from database, deleted entities are removed from the collection and new entities are added.
        /// <para />
        /// To refresh a reference navigation property just refresh the entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <typeparam name="TElement">The type of entities contained in the navigation property.</typeparam>
        /// <param name="entity">The entity whose navigation property to refresh.</param>
        /// <param name="navigationProperty">The collection navigation property to refresh.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null
        /// or
        /// The navigation property was null</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The syntax accepts lambda expressions, which are easy to understand.")]
        public void RefreshNavigationProperty<TEntity, TElement>(
            TEntity entity,
            Expression<Func<TEntity, ICollection<TElement>>> navigationProperty)
            where TEntity : class
            where TElement : class
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null");
            }

            if (navigationProperty == null)
            {
                throw new ArgumentNullException("navigationProperty", "The navigation property was null");
            }

            var collectionEntry = this.Entry<TEntity>(entity).Collection(navigationProperty);

            // Refreshes existing entities with values from database and removes from context the objects corresponding to database entries that no longer exist.
            var collection = collectionEntry.CurrentValue as IEnumerable;
            if (collection != null && collection.GetEnumerator().MoveNext())
            {
                this.objectContext.Refresh(RefreshMode.StoreWins, collection);
            }

            // Loads into context the entities that exist in the database but not in the context. It does not refresh in any way the entities that exist in the collection.
            collectionEntry.Load();
        }

        /// <summary>
        /// Refreshes the specified collection navigation property from the database.
        /// All property changes are overwritten with the data from database, deleted entities are removed from the collection and new entities are added.
        /// <para />
        /// To refresh a reference navigation property just refresh the entity.
        /// </summary>
        /// <param name="entity">The entity whose navigation property to refresh.</param>
        /// <param name="navigationProperty">The collection navigation property to refresh.</param>
        public void RefreshCollectionNavigationProperty(
            object entity,
            string navigationProperty)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null");
            }

            if (string.IsNullOrWhiteSpace(navigationProperty))
            {
                throw new ArgumentNullException("navigationProperty", "The navigation property was null or empty");
            }

            var collectionEntry = this.Entry(entity).Collection(navigationProperty);

            // Refreshes existing entities with values from database and removes from context the objects corresponding to database entries that no longer exist.
            var collection = collectionEntry.CurrentValue as IEnumerable;
            if (collection != null && collection.GetEnumerator().MoveNext())
            {
                this.objectContext.Refresh(RefreshMode.StoreWins, collection);
            }

            // Loads into context the entities that exist in the database but not in the context. It does not refresh in any way the entities that exist in the collection.
            collectionEntry.Load();
        }

        /// <summary>
        /// Checks if the provided navigation property is a reference navigation property.
        /// </summary>
        /// <param name="entity">The entity whose navigation property to check.</param>
        /// <param name="navigationProperty">The navigation property to check.</param>
        /// <returns>True if the navigation property is a reference navigation property or false otherwise.</returns>
        public bool IsNavigationPropertyReference(object entity, string navigationProperty)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null");
            }

            if (string.IsNullOrWhiteSpace(navigationProperty))
            {
                throw new ArgumentNullException("navigationProperty", "The navigation property was null or empty");
            }

            return this.Entry(entity).Member(navigationProperty) is DbReferenceEntry;
        }

        /// <summary>
        /// Checks if the provided navigation property is a collection navigation property.
        /// </summary>
        /// <param name="entity">The entity whose navigation property to check.</param>
        /// <param name="navigationProperty">The navigation property to check.</param>
        /// <returns>True if the navigation property is a collection navigation property or false otherwise.</returns>
        public bool IsCollectionNavigationProperty(object entity, string navigationProperty)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null");
            }

            if (string.IsNullOrWhiteSpace(navigationProperty))
            {
                throw new ArgumentNullException("navigationProperty", "The navigation property was null or empty");
            }

            return this.Entry(entity).Member(navigationProperty) is DbCollectionEntry;
        }

        #endregion Navigation Properties related

        /// <summary>
        /// Creates an object query for a specified entity type.
        /// This method should only be used when you can't write a complicated query using the DbContext API.
        /// DO NOT USE IT for simple queries.
        /// </summary>
        /// <typeparam name="T">The entity type.</typeparam>
        /// <returns>An ObjectQuery instance</returns>
        public ObjectQuery<T> CreateObjectQuery<T>() where T : class
        {
            return this.objectContext.CreateObjectSet<T>();
        }

        /// <summary>
        /// Reverts all entity related changes tracked by the context.
        /// All added entities are deleted and all modified or deleted entities are restored to their original state.        
        /// </summary>
        public void RevertChanges()
        {
            // Delete all added entities and mark as not changed the modified and deleted entities.
            this.ChangeTracker.DetectChanges();
            var changedEntities = this.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged).ToList();
            foreach (var entry in changedEntities)
            {
                if (entry.State == EntityState.Added)
                {
                    this.Detach(entry.Entity);
                }
                else if (entry.State == EntityState.Deleted)
                {
                    // Deleted entities must be move to the Modified state first, otherwise their original values are lost.
                    entry.State = EntityState.Modified;
                    entry.State = EntityState.Unchanged;
                }
                else
                {
                    entry.State = EntityState.Unchanged;
                }
            }

            // Detect changes to update the state of relationships.
            this.ChangeTracker.DetectChanges();

            // Delete all added relationships.
            var addedRelationships = this.objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added).Where(e => e.IsRelationship).ToList();
            foreach (var relationship in addedRelationships)
            {
                relationship.ChangeState(EntityState.Detached);
            }

            // Restore all deleted relationships.
            var deletedRelationships = this.objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).Where(e => e.IsRelationship).ToList();
            foreach (var relationship in deletedRelationships)
            {
                relationship.ChangeState(EntityState.Unchanged);
            }
        }
    }
}
