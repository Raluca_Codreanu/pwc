//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Commodity : System.ComponentModel.INotifyPropertyChanged
    {
        public Commodity()
        {
            this.priceHistory = new HashSet<CommoditiesPriceHistory>();
            this.OnItitialized();
        }
    

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        private Nullable<decimal> price;

        public Nullable<decimal> Price
        {
            get
            {
                return this.price;
            }

            set
            {
                if (this.price != value)
                {
                    this.price = value;
                    this.OnPropertyChanged("Price");
                }
            }
        }

        private Nullable<decimal> weight;

        public Nullable<decimal> Weight
        {
            get
            {
                return this.weight;
            }

            set
            {
                if (this.weight != value)
                {
                    this.weight = value;
                    this.OnPropertyChanged("Weight");
                }
            }
        }

        private Nullable<decimal> rejectRatio;

        /// <summary>
        /// A value between 0 and 1, with 4 decimal places, representing a percentage.
        /// </summary>
        public Nullable<decimal> RejectRatio
        {
            get
            {
                return this.rejectRatio;
            }

            set
            {
                if (this.rejectRatio != value)
                {
                    this.rejectRatio = value;
                    this.OnPropertyChanged("RejectRatio");
                }
            }
        }

        private string remarks;

        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                if (this.remarks != value)
                {
                    this.remarks = value;
                    this.OnPropertyChanged("Remarks");
                }
            }
        }

        private Nullable<int> amount;

        public Nullable<int> Amount
        {
            get
            {
                return this.amount;
            }

            set
            {
                if (this.amount != value)
                {
                    this.amount = value;
                    this.OnPropertyChanged("Amount");
                }
            }
        }

        private bool isMasterData;

        public bool IsMasterData
        {
            get
            {
                return this.isMasterData;
            }

            set
            {
                if (this.isMasterData != value)
                {
                    this.isMasterData = value;
                    this.OnPropertyChanged("IsMasterData");
                }
            }
        }

        private bool isDeleted;

        public bool IsDeleted
        {
            get
            {
                return this.isDeleted;
            }

            set
            {
                if (this.isDeleted != value)
                {
                    this.isDeleted = value;
                    this.OnPropertyChanged("IsDeleted");
                }
            }
        }

        private string partNumber;

        public string PartNumber
        {
            get
            {
                return this.partNumber;
            }

            set
            {
                if (this.partNumber != value)
                {
                    this.partNumber = value;
                    this.OnPropertyChanged("PartNumber");
                }
            }
        }

        private System.Guid guid;

        public System.Guid Guid
        {
            get
            {
                return this.guid;
            }

            set
            {
                if (this.guid != value)
                {
                    this.guid = value;
                    this.OnPropertyChanged("Guid");
                }
            }
        }

        private bool isStockMasterData;

        public bool IsStockMasterData
        {
            get
            {
                return this.isStockMasterData;
            }

            set
            {
                if (this.isStockMasterData != value)
                {
                    this.isStockMasterData = value;
                    this.OnPropertyChanged("IsStockMasterData");
                }
            }
        }

        private Nullable<System.DateTime> createDate;

        public Nullable<System.DateTime> CreateDate
        {
            get
            {
                return this.createDate;
            }

            set
            {
                if (this.createDate != value)
                {
                    this.createDate = value;
                    this.OnPropertyChanged("CreateDate");
                }
            }
        }

        private Nullable<System.DateTime> calculationCreateDate;

        public Nullable<System.DateTime> CalculationCreateDate
        {
            get
            {
                return this.calculationCreateDate;
            }

            set
            {
                if (this.calculationCreateDate != value)
                {
                    this.calculationCreateDate = value;
                    this.OnPropertyChanged("CalculationCreateDate");
                }
            }
        }

        private Manufacturer manufacturer;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Manufacturer Manufacturer
        {
            get
            {
                return this.manufacturer;
            }

            set
            {
                if (this.manufacturer != value)
                {
                    this.manufacturer = value;
                    this.OnPropertyChanged("Manufacturer");
                }
            }
        }

        private MeasurementUnit weightUnitBase;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual MeasurementUnit WeightUnitBase
        {
            get
            {
                return this.weightUnitBase;
            }

            set
            {
                if (this.weightUnitBase != value)
                {
                    this.weightUnitBase = value;
                    this.OnPropertyChanged("WeightUnitBase");
                }
            }
        }

        private Media media;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Media Media
        {
            get
            {
                return this.media;
            }

            set
            {
                if (this.media != value)
                {
                    this.media = value;
                    this.OnPropertyChanged("Media");
                }
            }
        }

        private Part part;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual Part Part
        {
            get
            {
                return this.part;
            }

            set
            {
                if (this.part != value)
                {
                    this.part = value;
                    this.OnPropertyChanged("Part");
                }
            }
        }

        private ProcessStep processStep;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ProcessStep ProcessStep
        {
            get
            {
                return this.processStep;
            }

            set
            {
                if (this.processStep != value)
                {
                    this.processStep = value;
                    this.OnPropertyChanged("ProcessStep");
                }
            }
        }

        private User owner;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual User Owner
        {
            get
            {
                return this.owner;
            }

            set
            {
                if (this.owner != value)
                {
                    this.owner = value;
                    this.OnPropertyChanged("Owner");
                }
            }
        }

        private ICollection<CommoditiesPriceHistory> priceHistory;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Having a public setter on collection properties is a requirement for Entity Framework proxies.")]
        public virtual ICollection<CommoditiesPriceHistory> PriceHistory
        {
            get
            {
                return this.priceHistory;
            }

            set
            {
                if (this.priceHistory != value)
                {
                    this.priceHistory = value;
                    this.OnPropertyChanged("PriceHistory");
                }
            }
        }
    
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when the value of a property has changed, in order to raise the <see cref="INotifyPropertyChanged.PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
