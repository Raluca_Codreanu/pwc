﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for PartRepository and is intended to contain
    /// unit tests for all public methods from PartRepository class.
    /// </summary>
    [TestClass]
    public class PartRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartRepositoryTest"/> class.
        /// </summary>
        public PartRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddPart method. Create a part and adds the created part into
        /// local context, commits the changes, then checks if part was added.
        /// </summary>
        [TestMethod]
        public void AddPartTest()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            part.Description = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.BatchSizePerYear = 10;
            part.IsExternal = true;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            // Add media to part.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Media.Add(media);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Verifies if part was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part.Guid);

            Assert.IsNotNull(result, "Failed to create a part.");
        }

        /// <summary>
        /// Creates a part, updates it and checks if part was updated.
        /// </summary>
        [TestMethod]
        public void UpdatePartTest()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            part.Description = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.BatchSizePerYear = 10;
            part.IsExternal = true;           
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            // Add media to part.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Media.Add(media);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            part.Name += " - Updated";
            dataContext1.SaveChanges();

            // Create a new context in order to get the updated part from db.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part partFromDB = dataContext2.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part.Guid);

            // Verify if part was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(part.Name, partFromDB.Name, "Failed to update a part.");
        }

        /// <summary>
        /// Creates a part, deletes it and checks if part was deleted.
        /// </summary>
        [TestMethod]
        public void DeletePartTest()
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Manufacturer = new Manufacturer();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            // Add media to part.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            part.Media.Add(media);

            // Add a raw material to part.
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.Manufacturer = new Manufacturer();
            material.Manufacturer.Name = material.Manufacturer.Guid.ToString();
            material.Media = media.Copy();
            part.RawMaterials.Add(material);

            // Add a commodity to part.
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.Media = media.Copy();
            part.Commodities.Add(commodity);

            // Add process and a process step to part.
            part.Process = new Process();
            ProcessStep step = new PartProcessStep();
            step.Name = step.Guid.ToString();
            step.Media = media.Copy();
            part.Process.Steps.Add(step);

            // Add a machine to process step.
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = media.Copy();
            step.Machines.Add(machine);

            // Add a consumable to process step.
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step.Consumables.Add(consumable);

            // Add a die to process step.
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = media.Copy();
            step.Dies.Add(die);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation.TransportTimeFromBuffer = 10;
            calculation.ToolChangeTime = 10;
            calculation.TransportClampingTime = 10;
            calculation.MachiningTime = 10;
            calculation.ToolChangeTime = 10;
            step.CycleTimeCalculations.Add(calculation);

            // Get the entities that belongs to the part unless the shared ones in order to check that the part was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Delete the created part
            dataContext1.PartRepository.RemoveAll(part);
            dataContext1.SaveChanges();

            // Verify that the part was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid part.
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfValidPart()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.PartRepository.GetParentProjectId(part);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project of a part");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a part that doesn't have a parent project.
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfPartWithoutParentProject()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.IsMasterData = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.PartRepository.GetParentProjectId(part);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a part that doesn't have a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullPart()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext.PartRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid part
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfValidPart()
        {
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            parentAssembly.Subassemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);
            assembly.SetIsMasterData(true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(parentAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.PartRepository.GetTopParentAssemblyId(part);

            Assert.AreEqual<Guid>(parentAssembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of a part");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a part that doesn't have a parent assembly
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfPartWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.PartRepository.GetTopParentAssemblyId(part);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a part that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyGuid method using a null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyGuidOfNullPart()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.PartRepository.GetTopParentAssemblyId(null);
        }

        /// <summary>
        /// Tests GetPartForTrashBinView method using valid data.
        /// </summary>
        [TestMethod]
        public void GetPartForViewTest()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            // Add a Calculator to the part
            User calculator = new User();
            calculator.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            calculator.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            calculator.Password = EncryptionManager.Instance.HashSHA256("Calculator.", calculator.Salt, calculator.Guid.ToString());
            part.CalculatorUser = calculator;
            calculator.Roles = Role.Admin;

            // Add a Manufacturer to the part
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Call GetPartForView method for the created part
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part partForTrashBinView = dataContext2.PartRepository.GetPartForView(part.Guid);

            // Verifies that the searched part was return
            Assert.IsTrue(
                partForTrashBinView != null && part.Guid == partForTrashBinView.Guid,
                "Failed to return the part for view");

            // Verifies that the Calculator of part was loaded
            Assert.IsTrue(
                partForTrashBinView.CalculatorUser != null && calculator.Guid == partForTrashBinView.CalculatorUser.Guid,
                "The Calculator of the part was not included");

            // Verifies that the Manufacturer of the part was loaded
            Assert.IsTrue(
                partForTrashBinView.Manufacturer != null && manufacturer.Guid == partForTrashBinView.Manufacturer.Guid,
                "The Manufacturer of the part was not included");

            // Verifies that the OverheadSettings of the part were loaded
            Assert.IsTrue(
                partForTrashBinView.OverheadSettings != null && part.OverheadSettings.Guid == partForTrashBinView.OverheadSettings.Guid,
                "The OverheadSettings of the part were not included");

            // Verifies that the CountrySettings of the part were loaded
            Assert.IsTrue(
                partForTrashBinView.CountrySettings != null && part.CountrySettings.Guid == partForTrashBinView.CountrySettings.Guid,
                "The CountrySettings of the part were not included");
        }

        /// <summary>
        /// A test for GetAllMasterData() method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataTest()
        {
            Part part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.IsMasterData = true;

            Part part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part1);
            dataContext1.PartRepository.Add(part2);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Part> actualResult = dataContext2.PartRepository.GetAllMasterDataPart();
            Collection<Part> expectedResult = new Collection<Part>(dataContext2.EntityContext.PartSet.Where(p => p.IsMasterData == true).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data parts");
            foreach (Part part in expectedResult)
            {
                Part correspondingPart = actualResult.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Wrong parts were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetPart(Guid partId) method using a valid part as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidPartTest()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetPart(part.Guid);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get a part by id");
        }

        /// <summary>
        /// A test for GetPart(Guid partId) method using a null part as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullPartTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part part = null;
            dataContext.PartRepository.GetPart(part.Guid);
        }

        /// <summary>
        /// A test for GetPartWithOwner(Guid partGuid) method.
        /// </summary>
        [TestMethod]
        public void GetPartWithOwnerTest()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            part.Owner = user;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetPartWithOwner(part.Guid);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the right part");
            Assert.IsTrue(result.Owner != null && result.Owner.Guid == user.Guid, "Failed to get a part with owner");
        }

        /// <summary>
        /// A test for GetPartWithOwner(Guid partGuid) method using a null part as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNullPartWithOwnerTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part part = null;
            dataContext.PartRepository.GetPartWithOwner(part.Guid);
        }

        /// <summary>
        /// A test for GetProjectParts(Guid projectGuid) method.
        /// </summary>
        [TestMethod]
        public void GetProjectPartsTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.CountrySettings = new CountrySetting();
            part1.OverheadSettings = new OverheadSetting();
            project.Parts.Add(part1);

            Part part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Part> actualResult = dataContext2.PartRepository.GetProjectParts(project.Guid);
            Collection<Part> expectedResult = new Collection<Part>(dataContext2.EntityContext.PartSet.Where(p => p.Project.Guid != null && p.Project.Guid == project.Guid).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all parts of the project");
            foreach (Part part in expectedResult)
            {
                Part correspondingPart = actualResult.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Wrong parts were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidMachine()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(machine);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of machine");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid raw material as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidRawMaterial()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.RawMaterials.Add(rawMaterial);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(rawMaterial);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of raw material");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid consumable as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidConsumable()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(consumable);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of a consumable");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidCommodity()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Commodities.Add(commodity);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(commodity);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of a commodity");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid die as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidDie()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(die);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of a die");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid process step as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidProcessStep()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(step);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of a process step");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a valid process as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfValidProcess()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext2.PartRepository.GetParentPart(process);

            Assert.AreEqual<Guid>(part.Guid, result.Guid, "Failed to get the parent part of a process");
        }

        /// <summary>
        /// A test for GetParentPart(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentPartOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part result = dataContext.PartRepository.GetParentPart(null);

            Assert.IsNull(result, "A parent part was returned for a null entity");
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid partId, OverheadSetting overheadSettings) method using valid data as input data.
        /// </summary>
        [TestMethod]
        public void UpdateOverheadSettingsOfValidPart()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = 1;
            overheadSettings.ConsumableOverhead = 2;
            overheadSettings.CommodityOverhead = 3;
            overheadSettings.ExternalWorkOverhead = 4;
            overheadSettings.ManufacturingOverhead = 5;
            overheadSettings.MaterialMargin = 6;
            overheadSettings.ConsumableMargin = 7;
            overheadSettings.CommodityMargin = 8;
            overheadSettings.ExternalWorkMargin = 9;
            overheadSettings.ManufacturingMargin = 10;
            overheadSettings.OtherCostOHValue = 11;
            overheadSettings.PackagingOHValue = 12;
            overheadSettings.SalesAndAdministrationOHValue = 13;
            overheadSettings.LastUpdateTime = DateTime.Now;

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.PartRepository.UpdateOverheadSettings(part.Guid, overheadSettings);
            dataContext2.SaveChanges();

            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part updatedPart = dataContext3.EntityContext.PartSet.Include(p => p.OverheadSettings).FirstOrDefault(p => p.Guid == part.Guid);
            bool overheadSettingsUpdated = updatedPart.OverheadSettings.MaterialOverhead == overheadSettings.MaterialOverhead &&
                                           updatedPart.OverheadSettings.ConsumableOverhead == overheadSettings.ConsumableOverhead &&
                                           updatedPart.OverheadSettings.CommodityOverhead == overheadSettings.CommodityOverhead &&
                                           updatedPart.OverheadSettings.ExternalWorkOverhead == overheadSettings.ExternalWorkOverhead &&
                                           updatedPart.OverheadSettings.ManufacturingOverhead == overheadSettings.ManufacturingOverhead &&
                                           updatedPart.OverheadSettings.MaterialMargin == overheadSettings.MaterialMargin &&
                                           updatedPart.OverheadSettings.ConsumableMargin == overheadSettings.ConsumableMargin &&
                                           updatedPart.OverheadSettings.CommodityMargin == overheadSettings.CommodityMargin &&
                                           updatedPart.OverheadSettings.ExternalWorkMargin == overheadSettings.ExternalWorkMargin &&
                                           updatedPart.OverheadSettings.ManufacturingMargin == overheadSettings.ManufacturingMargin &&
                                           updatedPart.OverheadSettings.OtherCostOHValue == overheadSettings.OtherCostOHValue &&
                                           updatedPart.OverheadSettings.PackagingOHValue == overheadSettings.PackagingOHValue &&
                                           updatedPart.OverheadSettings.SalesAndAdministrationOHValue == overheadSettings.SalesAndAdministrationOHValue &&
                                           string.Equals(updatedPart.OverheadSettings.LastUpdateTime.GetValueOrDefault().ToShortDateString(), overheadSettings.LastUpdateTime.GetValueOrDefault().ToShortDateString());

            Assert.IsTrue(overheadSettingsUpdated, "Failed to update the overhead settings of a part");
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid partId, OverheadSetting overheadSettings) method using 
        /// null part as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateOverheadSettingsOfNullPart()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            OverheadSetting overheadSettings = new OverheadSetting();
            Part part = null;
            dataContext.PartRepository.UpdateOverheadSettings(part.Guid, overheadSettings);
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid partId, OverheadSetting overheadSettings) method using null 
        /// overhead settings as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateOverheadSettingsWithNullOHSettings()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.PartRepository.UpdateOverheadSettings(part.Guid, null);
        }

        #endregion Test Methods
    }
}
