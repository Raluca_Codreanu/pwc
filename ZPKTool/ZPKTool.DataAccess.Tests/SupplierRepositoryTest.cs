﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for SupplierRepository and is intended to contain
    /// unit tests for all public methods from SupplierRepository class.
    /// </summary>
    [TestClass]
    public class SupplierRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierRepositoryTest"/> class.
        /// </summary>
        public SupplierRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddSupplier method. Create a supplier and adds the created  supplier into
        /// local context, commits the changes, then checks if the supplier was added.
        /// </summary>
        [TestMethod()]
        public void AddSupplierTest()
        {
            Customer supplier = new Customer();
            supplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            supplier.Description = EncryptionManager.Instance.GenerateRandomString(20, true);

            // Create a user which will be the owner of the supplier.
            User owner = new User();
            owner.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            owner.Password = EncryptionManager.Instance.HashSHA256("Password.", owner.Salt, owner.Guid.ToString());
            owner.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            supplier.Owner = owner;
            owner.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.SupplierRepository.Add(supplier);
            dataContext.SaveChanges();

            // Verifies if the supplier was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool supplierAdded = newContext.SupplierRepository.CheckIfExists(supplier.Guid);

            Assert.IsTrue(supplierAdded, "Failed to create a supplier.");
        }

        /// <summary>
        /// Creates a supplier, updates it and checks if the supplier was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateSupplierTest()
        {
            Customer supplier = new Customer();
            supplier.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            supplier.Description = EncryptionManager.Instance.GenerateRandomString(20, true);

            // Create a user which will be the owner of customer.
            User owner = new User();
            owner.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            owner.Password = EncryptionManager.Instance.HashSHA256("Password.", owner.Salt, owner.Guid.ToString());
            owner.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            owner.Roles = Role.Admin;
            supplier.Owner = owner;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.SupplierRepository.Add(supplier);
            dataContext.SaveChanges();

            supplier.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated customer from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Customer result = newContext.EntityContext.CustomerSet.FirstOrDefault(c => c.Guid == supplier.Guid);

            // Verifies if customer was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(supplier.Name, result.Name, "Failed to update a customer.");
        }

        /// <summary>
        /// Creates a customer, deletes it and checks if customer was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteSupplierTest()
        {
            Customer customer = new Customer();
            customer.Name = customer.Guid.ToString();
            customer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.StreetAddress = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.ZipCode = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.City = EncryptionManager.Instance.GenerateRandomString(7, true);
            customer.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactEmail = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.FirstContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactWebAddress = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactEmail = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactWebAddress = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.Type = (short)SupplierType.CorporateGroup;
            customer.Country = EncryptionManager.Instance.GenerateRandomString(7, true);
            customer.State = EncryptionManager.Instance.GenerateRandomString(10, true);

            // Get the entities that belongs to the customer unless the shared ones in order to check that the customer was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(customer);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.SupplierRepository.Add(customer);
            dataContext1.SaveChanges();

            // Delete the customer and save the changes
            dataContext1.SupplierRepository.RemoveAll(customer);
            dataContext1.SaveChanges();

            // Verify that the customer was permanently deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetById(Guid supplierGuid, bool noTracking = false) method using a valid supplier as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidSupplier()
        {
            Customer supplier = new Customer();
            supplier.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            supplier.Description = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.SupplierRepository.Add(supplier);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Customer result = newContext.SupplierRepository.FindById(supplier.Guid);

            Assert.AreEqual<Guid>(supplier.Guid, result.Guid, "Failed to get a supplier by id");
        }

        /// <summary>
        /// A test for GetById(Guid supplierGuid, bool noTracking = false) method using a null supplier as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullSupplier()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Customer supplier = null;
            dataContext.SupplierRepository.FindById(supplier.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData() method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataSuppliersTest()
        {
            Customer supplier1 = new Customer();
            supplier1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            supplier1.Description = EncryptionManager.Instance.GenerateRandomString(20, true);
            supplier1.IsMasterData = true;

            Customer supplier2 = new Customer();
            supplier2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            supplier2.Description = EncryptionManager.Instance.GenerateRandomString(19, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.SupplierRepository.Add(supplier1);
            dataContext.SupplierRepository.Add(supplier2);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Customer> actualResult = newContext.SupplierRepository.FindAllMasterData();
            Collection<Customer> expectedResult = new Collection<Customer>(newContext.EntityContext.CustomerSet.Where(c => c.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data suppliers.");
            foreach (Customer supplier in expectedResult)
            {
                Customer correspondingSupplier = actualResult.FirstOrDefault(c => c.Guid == supplier.Guid);
                if (correspondingSupplier == null)
                {
                    Assert.Fail("Wrong suppliers were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
