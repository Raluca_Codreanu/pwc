﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for TrashBinRepository and is intended to contain
    /// unit tests for all public methods from TrashBinRepository class.
    /// </summary>
    [TestClass]
    public class TrashBinRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinRepositoryTest"/> class.
        /// </summary>
        public TrashBinRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test Add TrashBinItem method. Create a trash bin item, add the created trash bin item to 
        /// local context, commit the changes then checks if trash bin item was added. 
        /// </summary>
        [TestMethod]
        public void AddTrashBinItemTest()
        {
            TrashBinItem trashBinItem = new TrashBinItem();
            trashBinItem.EntityName = EncryptionManager.Instance.GenerateRandomString(15, true);
            trashBinItem.EntityType = TrashBinItemType.Assembly;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.TrashBinRepository.Add(trashBinItem);
            dataContext1.SaveChanges();

            // Verifies if trash bin item was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool itemAdded = dataContext2.TrashBinRepository.CheckIfExists(trashBinItem.Guid);

            Assert.IsTrue(itemAdded, "Failed to create a trash bin item.");
        }

        /// <summary>
        /// Creates a trash bin item, deletes it and checks if the trash bin item was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteTrashBinItemTest()
        {
            TrashBinItem trashBinItem = new TrashBinItem();
            trashBinItem.EntityName = "Test trash bin item " + DateTime.Now.Ticks;
            trashBinItem.EntityType = TrashBinItemType.Assembly;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.TrashBinRepository.Add(trashBinItem);
            dataContext.SaveChanges();

            dataContext.TrashBinRepository.RemoveAll(trashBinItem);
            dataContext.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem result = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);

            Assert.IsNull(result, "Failed to delete a trash bin item");
        }

        /// <summary>
        /// A test for GetItemCount() method.
        /// </summary>
        [TestMethod]
        public void GetItemCountTest()
        {
            User u1 = new User();
            u1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            u1.Password = EncryptionManager.Instance.HashSHA256("Password.", u1.Salt, u1.Guid.ToString());
            u1.Name = EncryptionManager.Instance.GenerateRandomString(20, true);
            u1.Roles = Role.Admin;

            TrashBinItem item = new TrashBinItem();
            item.EntityGuid = Guid.NewGuid();
            item.EntityType = TrashBinItemType.Part;
            item.Owner = u1;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.TrashBinRepository.Add(item);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int actualResult = dataContext2.TrashBinRepository.GetItemCount(u1.Guid);
            int expectedResult = dataContext2.EntityContext.TrashBinItemSet.Count(i => i.Owner.Guid == u1.Guid);

            Assert.AreEqual<int>(expectedResult, actualResult, "Failed to get the trash bin item count.");
        }

        /// <summary>
        /// A test for GetAll(Guid ownerId, bool noTracking = false) method.
        /// </summary>
        [TestMethod]
        public void GetAllTrashBinItemsTest()
        {
            TrashBinItem trashBinItem1 = new TrashBinItem();
            trashBinItem1.EntityGuid = Guid.NewGuid();
            trashBinItem1.EntityType = TrashBinItemType.Die;

            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            trashBinItem1.SetOwner(user1);
            user1.Roles = Role.Admin;

            TrashBinItem trashBinItem2 = new TrashBinItem();
            trashBinItem2.EntityGuid = Guid.NewGuid();
            trashBinItem2.EntityType = TrashBinItemType.Machine;

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            trashBinItem2.SetOwner(user2);
            user2.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.TrashBinRepository.Add(trashBinItem1);
            dataContext1.TrashBinRepository.Add(trashBinItem2);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<TrashBinItem> actualResult = dataContext2.TrashBinRepository.GetAll(user1.Guid);
            Collection<TrashBinItem> expectedResult = new Collection<TrashBinItem>(dataContext2.EntityContext.TrashBinItemSet.Where(t => t.Owner != null && t.Owner.Guid == user1.Guid).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all trash bin items of a specified user");
            foreach (TrashBinItem trashBinItem in expectedResult)
            {
                TrashBinItem correspondingTrashBinItem = actualResult.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
                if (correspondingTrashBinItem == null)
                {
                    Assert.Fail("Wrong trash bin items were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetItemOfObject(Guid objectId) method using a valid object as input data.
        /// </summary>
        [TestMethod]
        public void GetItemOfValidObjectTest()
        {
            TrashBinItem trashBinItem1 = new TrashBinItem();
            trashBinItem1.EntityGuid = Guid.NewGuid();
            trashBinItem1.EntityType = TrashBinItemType.Part;

            TrashBinItem trashBinItem2 = new TrashBinItem();
            trashBinItem2.EntityGuid = Guid.NewGuid();
            trashBinItem2.EntityType = TrashBinItemType.RawMaterial;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.TrashBinRepository.Add(trashBinItem1);
            dataContext1.TrashBinRepository.Add(trashBinItem2);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem result = dataContext2.TrashBinRepository.GetItemOfObject(trashBinItem1.EntityGuid);

            Assert.AreEqual<Guid>(trashBinItem1.Guid, result.Guid, "Failed to get the trash bin item of the specified object");
        }

        /// <summary>
        ///  A test for GetItemOfObject(Guid objectId) method using a null object as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetItemOfNullObjectTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part part = null;
            dataContext.TrashBinRepository.GetItemOfObject(part.Guid);
        }

        /// <summary>
        /// A test for AddItemForObject(ITrashable obj) method using valid object as input data.
        /// </summary>
        [TestMethod]
        public void AddItemForValidObjectTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.TrashBinRepository.AddItemForObject(assembly);
            dataContext2.SaveChanges();

            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem result = dataContext3.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == assembly.Guid);

            Assert.IsNotNull(result, "Failed to add a trash bin item for a specified object.");
        }

        /// <summary>
        /// A test for AddItemForObject(ITrashable obj) method using a null object as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddItemForNullObjectTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.TrashBinRepository.AddItemForObject(null);
        }

        /// <summary>
        /// A test for DeleteItemOfObject(ITrashable obj) method using a valid object as input data.
        /// </summary>
        [TestMethod]
        public void DeleteItemOfValidObjectTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.SetIsDeleted(true);

            TrashBinItem trashBinItem = new TrashBinItem();
            trashBinItem.EntityGuid = assembly.Guid;
            trashBinItem.EntityType = TrashBinItemType.Machine;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.TrashBinRepository.Add(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.TrashBinRepository.DeleteItemOfObject(assembly);
            dataContext2.SaveChanges();

            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem result = dataContext3.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);

            Assert.IsNull(result, "Failed to delete the trash bin item of a given entity");
        }

        /// <summary>
        /// A test for DeleteItemOfObject(ITrashable obj) method using a null object as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteItemOfNullObjectTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.TrashBinRepository.DeleteItemOfObject(null);
        }

        /// <summary>
        /// A test for DeleteToTrashBin(ITrashable obj) method using a valid entity as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinValidItemTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            ProjectFolder subfolder = new ProjectFolder();
            subfolder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            folder.ChildrenProjectFolders.Add(subfolder);

            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();
            folder.Projects.Add(project);

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Machines.Add(machine1);

            Commodity commodity1 = new Commodity();
            commodity1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Commodities.Add(commodity1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Consumables.Add(consumable1);

            Die die1 = new Die();
            die1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Dies.Add(die1);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.Process = new Process();
            assembly.Subassemblies.Add(subassembly);

            Part part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.RawMaterials.Add(rawMaterial);

            Commodity commodity2 = new Commodity();
            commodity2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.Commodities.Add(commodity2);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Consumables.Add(consumable2);

            Die die2 = new Die();
            die2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Dies.Add(die2);

            Part part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Process = new Process();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.TrashBinRepository.DeleteToTrashBin(folder);
            dataContext2.SaveChanges();

            Assert.IsTrue(folder.IsDeleted, "Failed to delete to trash bin a project folder");
            Assert.IsTrue(subfolder.IsDeleted, "Failed to delete to trash bin a child folder");
            Assert.IsTrue(project.IsDeleted, "Failed to delete to trash bin a project");
            Assert.IsTrue(assembly.IsDeleted, "Failed to delete to trash bin an assembly");
            Assert.IsTrue(machine1.IsDeleted, "Failed to delete to trash bin a machine of assembly");
            Assert.IsTrue(commodity1.IsDeleted, "Failed to delete to trash bin a commodity of assembly");
            Assert.IsTrue(die1.IsDeleted, "Failed to delete to trash bin a die of assembly");
            Assert.IsTrue(consumable1.IsDeleted, "Failed to delete to trash bin a consumable of assembly");
            Assert.IsTrue(subassembly.IsDeleted, "Failed to delete to trash bin a subassembly");
            Assert.IsTrue(part1.IsDeleted, "Failed to delete to trash bin a part of assembly");
            Assert.IsTrue(rawMaterial.IsDeleted, "Failed to delete to trash bin a raw material");
            Assert.IsTrue(commodity2.IsDeleted, "Failed to delete to trash bin a commodity of part");
            Assert.IsTrue(machine2.IsDeleted, "Failed to delete to trash bin a machine of part");
            Assert.IsTrue(die2.IsDeleted, "Failed to delete to trash bin a die of part");
            Assert.IsTrue(consumable2.IsDeleted, "Failed to delete to trash bin a consumable of part");
            Assert.IsTrue(part2.IsDeleted, "Failed to delete to trash bin a part of project");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(ITrashable obj) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteToTrashBinNullItemTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.TrashBinRepository.DeleteToTrashBin(null);
        }

        /// <summary>
        /// A test for RestoreFromTrashBin(ITrashable obj) method using a valid entity as input data.
        /// </summary>
        [TestMethod]
        public void RestoreFromTrashBinValidEntityTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            ProjectFolder subfolder = new ProjectFolder();
            subfolder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            folder.ChildrenProjectFolders.Add(subfolder);

            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();
            folder.Projects.Add(project);

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Machines.Add(machine1);

            Commodity commodity1 = new Commodity();
            commodity1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Commodities.Add(commodity1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Consumables.Add(consumable1);

            Die die1 = new Die();
            die1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step1.Dies.Add(die1);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.Process = new Process();
            assembly.Subassemblies.Add(subassembly);

            Part part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part1);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.RawMaterials.Add(rawMaterial);

            Commodity commodity2 = new Commodity();
            commodity2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.Commodities.Add(commodity2);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Consumables.Add(consumable2);

            Die die2 = new Die();
            die2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step2.Dies.Add(die2);

            Part part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Process = new Process();
            project.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            dataContext1.TrashBinRepository.DeleteToTrashBin(folder);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.TrashBinRepository.RestoreFromTrashBin(folder);
            dataContext2.SaveChanges();

            // Retrieve the trash bin item of deleted folder
            TrashBinItem folderItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == folder.Guid);

            Assert.IsFalse(folder.IsDeleted, "Failed to restore from the trash bin a project folder");
            Assert.IsNull(folderItem, "The trash bin item of the restored folder was not deleted");
            Assert.IsFalse(subfolder.IsDeleted, "Failed to restore from the trash bin a child folder");
            Assert.IsFalse(project.IsDeleted, "Failed to restore from the trash bin a project");
            Assert.IsFalse(assembly.IsDeleted, "Failed to restore from the trash bin  an assembly");
            Assert.IsFalse(machine1.IsDeleted, "Failed to restore from the trash bin a machine of assembly");
            Assert.IsFalse(commodity1.IsDeleted, "Failed to restore from the trash bin a commodity of assembly");
            Assert.IsFalse(die1.IsDeleted, "Failed to restore from the trash bin a die of assembly");
            Assert.IsFalse(consumable1.IsDeleted, "Failed to restore from the trash bin a consumable of assembly");
            Assert.IsFalse(subassembly.IsDeleted, "Failed to restore from the trash bin a subassembly");
            Assert.IsFalse(part1.IsDeleted, "Failed to restore from the trash bin a part of assembly");
            Assert.IsFalse(rawMaterial.IsDeleted, "Failed to restore from the trash bin a raw material");
            Assert.IsFalse(commodity2.IsDeleted, "Failed to restore from the trash bin a commodity of part");
            Assert.IsFalse(machine2.IsDeleted, "Failed to restore from the trash bin a machine of part");
            Assert.IsFalse(die2.IsDeleted, "Failed to restore from the trash bin a die of part");
            Assert.IsFalse(consumable2.IsDeleted, "Failed to restore from the trash bin a consumable of part");
            Assert.IsFalse(part2.IsDeleted, "Failed to restore from the trash bin a part of project");
        }

        /// <summary>
        /// A test for RestoreFromTrashBin(ITrashable obj) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RestoreFromTrashVinNullEntityTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.TrashBinRepository.RestoreFromTrashBin(null);
        }

        #endregion Test Methods
    }
}
