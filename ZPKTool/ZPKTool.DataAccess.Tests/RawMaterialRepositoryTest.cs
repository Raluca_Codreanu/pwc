﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for RawMaterialRepository and is intended to contain
    /// unit tests for all public methods from RawMaterialRepository class
    /// </summary>
    [TestClass]
    public class RawMaterialRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialRepositoryTest"/> class.
        /// </summary>
        public RawMaterialRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddRawMaterial method. Create a raw material and adds the created raw material into 
        /// local context, commits the changes, then checks if raw material was added. 
        /// </summary>
        [TestMethod()]
        public void AddRawMaterialTest()
        {
            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.Media = new Media();
            material.Media.Type = (short)MediaType.Image;
            material.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            material.Media.Size = material.Media.Content.Length;
            material.Media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            // Verifies if material was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool materialAdded = newContext.RawMaterialRepository.CheckIfExists(material.Guid);

            Assert.IsTrue(materialAdded, "Failed to create a raw material.");
        }

        /// <summary>
        /// Creates a raw material, updates it and checks if raw material was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateRawMaterialTest()
        {
            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            material.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated raw material from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial materialFromDB = newContext.EntityContext.RawMaterialSet.FirstOrDefault(r => r.Guid == material.Guid);

            // Verifies if material was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(material.Name, materialFromDB.Name, "Failed to update a raw material");
        }

        /// <summary>
        /// Creates a raw material, deletes it and checks if raw material was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteRawMaterialTest()
        {
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();

            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            material.Media = media;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            material.Manufacturer = manufacturer;

            // Get the entities that belong to the material unless the shared ones in order to check that the material was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(material);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material);
            dataContext1.SaveChanges();

            // Delete the created raw material and save the changes
            dataContext1.RawMaterialRepository.RemoveAll(material);
            dataContext1.SaveChanges();

            // Verify that the raw material was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid raw material.
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfValidRawMaterial()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.RawMaterials.Add(material);

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.ProjectRepository.Add(project);
            contextManager.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.RawMaterialRepository.GetParentProjectId(material);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project id of a raw material");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a raw material that doesn't have a parent.
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfRawMaterialWithoutParent()
        {
            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.IsMasterData = true;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.RawMaterialRepository.Add(material);
            contextManager.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.RawMaterialRepository.GetParentProjectId(material);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a raw material that doesn't have a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null raw material.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullRawMaterial()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectId = contextManager.RawMaterialRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid raw material.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfValidRawMaterial()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.RawMaterials.Add(material);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.RawMaterialRepository.GetParentAssemblyId(material);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of a raw material.");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a raw material that doesn't have a parent assembly.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfRawMaterialWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.RawMaterials.Add(material);

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.ProjectRepository.Add(project);
            contextManager.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.RawMaterialRepository.GetParentAssemblyId(material);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a raw material that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null raw material.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullRawMaterial()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.RawMaterialRepository.GetParentAssemblyId(null);
        }

        /// <summary>
        /// A test for GetById method using a valid raw material as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidRawMaterial()
        {
            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial result = newContext.RawMaterialRepository.GetById(material.Guid);

            Assert.AreEqual<Guid>(material.Guid, result.Guid, "Failed to get by id a valid raw material");
        }

        /// <summary>
        /// A test for GetById method using a null raw material as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullRawMaterial()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial material = null;
            dataContext.RawMaterialRepository.GetById(material.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataRawMaterials()
        {
            RawMaterial material1 = new RawMaterial();
            material1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material1.IsMasterData = true;

            RawMaterial material2 = new RawMaterial();
            material2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material1);
            dataContext.RawMaterialRepository.Add(material2);
            dataContext.SaveChanges();

            Collection<RawMaterial> actualResult = dataContext.RawMaterialRepository.GetAllMasterData();
            Collection<RawMaterial> expectedResult = new Collection<RawMaterial>(dataContext.EntityContext.RawMaterialSet.Where(m => m.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data raw materials.");
            foreach (RawMaterial material in expectedResult)
            {
                RawMaterial correspondingMaterial = actualResult.FirstOrDefault(m => m.Guid == material.Guid);
                if (correspondingMaterial == null)
                {
                    Assert.Fail("Wrong raw materials were returned.");
                }
            }
        }

        #endregion Test Methods
    }
}
