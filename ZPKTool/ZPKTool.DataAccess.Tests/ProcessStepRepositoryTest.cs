﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ProcessStepRepository and is intended to contain
    /// unit tests for all public methods from ProcessStepRepository class.
    /// </summary>
    [TestClass]
    public class ProcessStepRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepRepositoryTest"/> class.
        /// </summary>
        public ProcessStepRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddProcessStep method. Create a  process step  and adds the created  step into
        /// local context, commits the changes, then checks if step was added.
        /// </summary>
        [TestMethod()]
        public void AddProcessStepTest()
        {
            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Accuracy = ProcessCalculationAccuracy.Estimated;
            step.CycleTime = 100;
            step.ProcessTime = 20;
            step.PartsPerCycle = 5;
            step.ScrapAmount = 20;
            step.Process = new Process();

            // Add media to the process step.
            step.Media = new Media();
            step.Media.Type = (short)MediaType.Image;
            step.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            step.Media.Size = step.Media.Content.Length;
            step.Media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessStepRepository.Add(step);
            dataContext1.SaveChanges();

            // Verifies if the step was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext2.EntityContext.ProcessStepSet.FirstOrDefault(s => s.Guid == step.Guid);

            Assert.IsNotNull(result, "Failed to create a process step.");
        }

        /// <summary>
        /// Creates a process step, updates it and checks if process step was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateProcessStepTest()
        {
            ProcessStep step = new AssemblyProcessStep();
            step.Name = "Test ProcessStep" + DateTime.Now.Ticks;
            step.Accuracy = ProcessCalculationAccuracy.Estimated;
            step.CycleTime = 100;
            step.ProcessTime = 20;
            step.PartsPerCycle = 5;
            step.ScrapAmount = 20;
            step.Process = new Process();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessStepRepository.Add(step);
            dataContext1.SaveChanges();

            step.Name += " - Updated";
            dataContext1.SaveChanges();

            // Create a new context in order to get the updated process step from db.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep stepFromDB = dataContext2.EntityContext.ProcessStepSet.FirstOrDefault(s => s.Guid == step.Guid);

            // Verify if process step was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(step.Name, stepFromDB.Name, "Failed to update a process step.");
        }

        /// <summary>
        /// Creates a process step, deletes it and checks if process step was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteProcessStepTest()
        {
            ProcessStep step = new AssemblyProcessStep();
            step.Name = step.Guid.ToString();
            step.Accuracy = ProcessCalculationAccuracy.Estimated;
            step.CycleTime = 100;
            step.ProcessTime = 20;
            step.PartsPerCycle = 5;
            step.ScrapAmount = 20;

            // Add media to process step.
            Media media1 = new Media();
            media1.Type = (short)MediaType.Image;
            media1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media1.Size = media1.Content.Length;
            media1.OriginalFileName = media1.Guid.ToString();
            step.Media = media1;

            // Add a machine to process step.
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.Media = media1.Copy();
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            step.Machines.Add(machine);

            // Add a consumable to process step.
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step.Consumables.Add(consumable);

            // Add a die to process step.
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Media = media1.Copy();
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            step.Dies.Add(die);

            // Add a commodity to process step
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Media = media1.Copy();
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            step.Commodities.Add(commodity);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation.TransportTimeFromBuffer = 10;
            calculation.ToolChangeTime = 10;
            calculation.TransportClampingTime = 10;
            calculation.MachiningTime = 10;
            calculation.ToolChangeTime = 10;
            step.CycleTimeCalculations.Add(calculation);

            // Attach the step to a process.
            step.Process = new Process();

            // Get the entities that belongs to the step unless the shared ones in order to check that the step was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessStepRepository.Add(step);
            dataContext1.SaveChanges();

            // Delete the created step 
            dataContext1.ProcessStepRepository.RemoveAll(step);
            dataContext1.SaveChanges();

            // Verify that the process step was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid process step
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfValidProcessStep()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.ProcessStepRepository.GetParentProjectId(step);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project id of a step entity");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a process step that doesn't have a parent project
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfProcessStepWithoutParentProject()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);
            assembly.SetIsMasterData(true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.ProcessStepRepository.GetParentProjectId(step);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a step that doesn't have a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null process step.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullProcessStep()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = contextManager.ProcessStepRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid process step
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfValidProcessStep()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.ProcessStepRepository.GetParentAssemblyId(step);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "The returned assembly is not the parent assembly of step");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a step that doesn't have a parent assembly
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfProcessStepWithoutParentAssembly()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();

            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);
            part.SetIsMasterData(true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.ProcessStepRepository.GetParentAssemblyId(step);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a step that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null step.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullProcessStep()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.ProcessStepRepository.GetParentAssemblyId(null);
        }
                
        /// <summary> 
        /// A test for GetParentProcessStep(object entity) method using a valid machine as input data.
        /// </summary>
        [TestMethod()]
        public void GetParentProcessStepOfValidMachine()
        {
            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Process process = new Process();
            process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext2.ProcessStepRepository.GetParentProcessStep(machine);

            Assert.AreEqual<Guid>(step.Guid, result.Guid, "Failed to get the parent process step of a machine");
        }

        /// <summary>
        /// A test for GetParentProcessStep(object entity) method using a valid consumable as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProcessStepOfValidConsumable()
        {
            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Process process = new Process();
            process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext2.ProcessStepRepository.GetParentProcessStep(consumable);

            Assert.AreEqual<Guid>(step.Guid, result.Guid, "Failed to get the parent process step of a consumable");
        }

        /// <summary>
        /// A test for GetParentProcessStep(object entity) method using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProcessStepOfValidCommodity()
        {
            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Process process = new Process();
            process.Steps.Add(step);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Commodities.Add(commodity);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext2.ProcessStepRepository.GetParentProcessStep(commodity);

            Assert.AreEqual<Guid>(step.Guid, result.Guid, "Failed to get the parent process step of commodity");
        }

        /// <summary>
        /// A test for GetParentProcessStep(object entity) method using a valid die as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProcessStepOfValidDie()
        {
            ProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Process process = new Process();
            process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext2.ProcessStepRepository.GetParentProcessStep(die);

            Assert.AreEqual<Guid>(step.Guid, result.Guid, "Failed to get the parent process step of a die");
        }

        /// <summary>
        /// A test for GetParentProcessStep(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProcessStepOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep result = dataContext.ProcessStepRepository.GetParentProcessStep(null);

            Assert.IsNull(result, "A parent process step was returned for a null entity");
        }

        /// <summary>
        /// A test for GetProcessStepFull(Guid stepId) method using a valid process step as input data.
        /// </summary>
        [TestMethod]
        public void GetProcessStepFull()
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(subassembly);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = step.Guid.ToString();

            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = calculation.Guid.ToString();
            calculation.TransportTimeFromBuffer = 10;
            calculation.ToolChangeTime = 10;
            calculation.TransportClampingTime = 10;
            calculation.MachiningTime = 10;
            calculation.TakeOffTime = 10;
            step.CycleTimeCalculations.Add(calculation);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Type == MeasurementUnitType.Time);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Type == MeasurementUnitType.Weight);

            step.CycleTimeUnit = timeUnit;
            step.MaxDownTimeUnit = timeUnit;
            step.ProcessTimeUnit = timeUnit;
            step.SetupTimeUnit = timeUnit;

            ProcessStepsClassification classification1 = new ProcessStepsClassification();
            classification1.Name = classification1.Guid.ToString();
            step.Type = classification1;

            ProcessStepsClassification classification2 = new ProcessStepsClassification();
            classification2.Name = classification2.Guid.ToString();
            step.SubType = classification2;

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.WeightUnitBase = weightUnit;
            step.Commodities.Add(commodity);

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            consumable.AmountUnitBase = weightUnit;
            consumable.PriceUnitBase = weightUnit;
            step.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            step.Dies.Add(die);

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.MainClassification = new MachinesClassification();
            machine.MainClassification.Name = machine.MainClassification.Guid.ToString();
            machine.SubClassification = new MachinesClassification();
            machine.SubClassification.Name = machine.SubClassification.Guid.ToString();
            machine.TypeClassification = new MachinesClassification();
            machine.TypeClassification.Name = machine.TypeClassification.Guid.ToString();
            machine.ClassificationLevel4 = new MachinesClassification();
            machine.ClassificationLevel4.Name = machine.ClassificationLevel4.Guid.ToString();
            step.Machines.Add(machine);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = subassembly;
            assemblyAmount.Amount = 10;
            step.AssemblyAmounts.Add(assemblyAmount);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part;
            partAmount.Amount = 10;
            step.PartAmounts.Add(partAmount);

            User user = new User();
            user.Name = user.Guid.ToString();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;

            Process process = new Process();
            process.Steps.Add(step);
            assembly.Process = process;
            assembly.SetOwner(user);

            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStep fullStep = dataContext2.ProcessStepRepository.GetProcessStepFull(step.Guid);

            // Verify that the process step was fully loaded
            Assert.IsNotNull(fullStep, "Failed to load a process step full.");
            Assert.IsNotNull(fullStep.Owner, "Failed to load the owner of the process step.");
            Assert.IsNotNull(fullStep.CycleTimeCalculations.FirstOrDefault(), "Failed to load the cycle time calculations of step.");
            Assert.IsNotNull(fullStep.CycleTimeCalculations.FirstOrDefault().Owner, "Failed to load the owner of the cycle time calculations");
            Assert.IsNotNull(fullStep.CycleTimeUnit, "Failed to load the CycleTimeUnit.");
            Assert.IsNotNull(fullStep.MaxDownTimeUnit, "Failed to load the MaxDownTimeUnit.");
            Assert.IsNotNull(fullStep.ProcessTimeUnit, "Failed to load the ProcessTimeUnit.");
            Assert.IsNotNull(fullStep.SetupTimeUnit, "Failed to load the SetupTimeUnit.");
            Assert.IsNotNull(fullStep.Type, "Failed to load the step's type.");
            Assert.IsNotNull(fullStep.SubType, "Failed to load the step's subtype.");
            Assert.IsNotNull(fullStep.AssemblyAmounts.FirstOrDefault(), "Failed to load AssemblyAmounts.");
            Assert.IsNotNull(fullStep.AssemblyAmounts.FirstOrDefault().Owner, "Failed to load the Owner of the assembly amounts.");
            Assert.IsNotNull(fullStep.PartAmounts.FirstOrDefault(), "Failed to load PartAmounts.");
            Assert.IsNotNull(fullStep.PartAmounts.FirstOrDefault().Owner, "Failed to load the Owner of part amounts.");
            Assert.IsNotNull(fullStep.Commodities.FirstOrDefault(), "Failed to load the commodities.");
            Assert.IsNotNull(fullStep.Commodities.FirstOrDefault().Owner, "Failed to load the owner of commodities.");
            Assert.IsNotNull(fullStep.Commodities.FirstOrDefault().Manufacturer, "Failed to load the commodities' manufacturers.");
            Assert.IsNotNull(fullStep.Commodities.FirstOrDefault().Manufacturer.Owner, "Failed to load the owner of commodity's manufacturer.");
            Assert.IsNotNull(fullStep.Commodities.FirstOrDefault().WeightUnitBase, "Failed to load the WeightUnitBase of commodity.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault(), "Failed to load the consumables.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault().AmountUnitBase, "Failed to load the AmountUnitBase of consumables.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault().PriceUnitBase, "Failed to load the PriceUnitBase of consumables.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault().Owner, "Failed to load the Owner of consumables.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault().Manufacturer, "Failed to load the consumable's Manufacturer.");
            Assert.IsNotNull(fullStep.Consumables.FirstOrDefault().Manufacturer.Owner, "Failed to load the owner of consumable's manufacturer.");
            Assert.IsNotNull(fullStep.Dies.FirstOrDefault(), "Failed to load the dies");
            Assert.IsNotNull(fullStep.Dies.FirstOrDefault().Owner, "Failed to load the die's Owner.");
            Assert.IsNotNull(fullStep.Dies.FirstOrDefault().Manufacturer, "Failed to load the die's Manufacturer.");
            Assert.IsNotNull(fullStep.Dies.FirstOrDefault().Manufacturer.Owner, "Failed to load the Owner of die's manufacturer.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault(), "Failed to load the machines.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().Owner, "Failed to load the machine's Owner.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().Manufacturer, "Failed to load the machine's Manufacturer.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().Manufacturer.Owner, "Failed to load the Owner of machine's manufacturer.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().MainClassification, "Failed to load the MainClassification of machine.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().SubClassification, "Failed to load the SubClassification of machine.");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().TypeClassification, "Failed to load the TypeClassification of machine");
            Assert.IsNotNull(fullStep.Machines.FirstOrDefault().ClassificationLevel4, "Failed to load the ClassificationLevel4 of machine.");
        }

        #endregion Test Methods
    }
}
