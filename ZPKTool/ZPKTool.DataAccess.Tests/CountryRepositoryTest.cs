﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for CountryRepository and is intended to contain
    /// unit tests for all public methods from CountryRepository
    /// </summary>
    [TestClass]
    public class CountryRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryRepositoryTest"/> class.
        /// </summary>
        public CountryRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddCountry method. Create a country  and adds the created country  into
        /// local context, commits the changes, then checks if country  was added.
        /// </summary>
        [TestMethod]
        public void AddCountryTest()
        {
            Country country = new Country();
            country.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country.CountrySetting = new CountrySetting();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = "C";
            currency.ExchangeRate = 1;
            country.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();

            // Verifies if country  was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool countryAdded = newContext.CountryRepository.CheckIfExists(country.Guid);

            Assert.IsTrue(countryAdded, "Fails to add a country.");
        }

        /// <summary>
        /// Creates a country, updates it and checks if country was updated.
        /// </summary>
        [TestMethod]
        public void UpdateCountryTest()
        {
            Country country = new Country();
            country.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country.CountrySetting = new CountrySetting();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.Symbol = "C";
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            country.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();

            country.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated country from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country countryFromDB = newContext.EntityContext.CountrySet.FirstOrDefault(c => c.Guid == country.Guid);

            // Verifies if country was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(country.Name, countryFromDB.Name, "Failed to update a country.");
        }

        /// <summary>
        /// Creates a country, deletes it and checks if country was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteCountryTest()
        {
            Country country = new Country();
            country.Name = country.Guid.ToString();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = currency.Guid.ToString();
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(5, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            country.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;

            // Creates country setting which will be used by country.
            CountrySetting countrySetting = new CountrySetting();
            countrySetting.AirCost = 1;
            countrySetting.EnergyCost = 1;
            countrySetting.EngineerCost = 1;
            countrySetting.ForemanCost = 1;
            countrySetting.InterestRate = 1;
            countrySetting.OfficeAreaRentalCost = 1;
            countrySetting.ProductionAreaRentalCost = 1;
            countrySetting.SkilledLaborCost = 1;
            countrySetting.TechnicianCost = 1;
            countrySetting.UnskilledLaborCost = 1;
            countrySetting.WaterCost = 1;
            country.CountrySetting = countrySetting;

            // Add a state to the country
            CountryState state = new CountryState();
            state.Name = state.Guid.ToString();
            state.CountrySettings = countrySetting.Copy();
            country.States.Add(state);

            // Get the entities that belongs to country unless the shared ones in order to check that the country was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(country);

            dataContext1.CountryRepository.Add(country);
            dataContext1.SaveChanges();

            // Delete the created country and save the changes
            dataContext1.CountryRepository.RemoveAll(country);
            dataContext1.SaveChanges();

            // Verify that the country was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetAll() method.
        /// </summary>
        [TestMethod]
        public void GetAllCountriesTest()
        {
            Country country1 = new Country();
            country1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country1.CountrySetting = new CountrySetting();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = "C";
            currency.ExchangeRate = 1;
            country1.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country1.WeightMeasurementUnit = weightUnit;
            country1.LengthMeasurementUnit = lengthUnit;
            country1.VolumeMeasurementUnit = volumeUnit;
            country1.TimeMeasurementUnit = timeUnit;
            country1.FloorMeasurementUnit = floorUnit;

            dataContext.CountryRepository.Add(country1);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Country> actualResult = newContext.CountryRepository.GetAll();
            Collection<Country> expectedResult = new Collection<Country>(newContext.EntityContext.CountrySet.Where(c => !c.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all countries");
            foreach (Country country in expectedResult)
            {
                Country correspondingCountry = actualResult.FirstOrDefault(c => c.Guid == country.Guid);
                if (correspondingCountry == null)
                {
                    Assert.Fail("Wrong countries were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetById method using a valid country as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidCountry()
        {
            Country country = new Country();
            country.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country.CountrySetting = new CountrySetting();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.Symbol = "C";
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            country.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country result = newContext.CountryRepository.GetById(country.Guid);

            Assert.AreEqual<Guid>(country.Guid, result.Guid, "Failed to get by id a country entity.");
        }

        /// <summary>
        /// A test for GetById method using a null country as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullCountry()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country country = null;
            dataContext.CountryRepository.GetById(country.Guid);
        }

        /// <summary>
        /// A test for GetByName method using a valid country as input data.
        /// </summary>
        [TestMethod]
        public void GetByNameValidCountry()
        {
            Country country = new Country();
            country.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            country.CountrySetting = new CountrySetting();

            // Creates a currency which will be used by country.
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.Symbol = "C";
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1;
            country.Currency = currency;

            // Gets measurement units which will be used by country.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            MeasurementUnit lengthUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Meter");
            MeasurementUnit volumeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Liter");
            MeasurementUnit timeUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Second");
            MeasurementUnit floorUnit = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Square Meter");
            country.WeightMeasurementUnit = weightUnit;
            country.LengthMeasurementUnit = lengthUnit;
            country.VolumeMeasurementUnit = volumeUnit;
            country.TimeMeasurementUnit = timeUnit;
            country.FloorMeasurementUnit = floorUnit;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country result = newContext.CountryRepository.GetByName(country.Name);

            Assert.AreEqual<Guid>(country.Guid, result.Guid, "Failed to get by name a country entity");
        }

        /// <summary>
        /// A test for GetByName method using a null country as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByNameNullCountry()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Country country = null;
            dataContext.CountryRepository.GetByName(country.Name);
        }

        #endregion Test Methods
    }
}
