﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for DieRepository and is intended
    /// to contain unit tests for all public methods from DieRepository class
    /// </summary>
    [TestClass]
    public class DieRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DieRepositoryTest"/> class.
        /// </summary>
        public DieRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddDie method. Create a die and adds the created die into 
        /// local context, commits the changes, then checks if die was added. 
        /// </summary>
        [TestMethod]
        public void AddDieTest()
        {
            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;
            die.Media = new Media();
            die.Media.Type = (short)MediaType.Image;
            die.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            die.Media.Size = die.Media.Content.Length;
            die.Media.OriginalFileName = "test.txt";

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            // Verifies if die was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool dieAdded = newContext.DieRepository.CheckIfExists(die.Guid);

            Assert.IsTrue(dieAdded, "Failed to create a die.");
        }

        /// <summary>
        /// Creates a die, updates it and checks if die was updated.
        /// </summary>
        [TestMethod]
        public void UpdateDieTest()
        {
            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;
            die.Media = new Media();
            die.Media.Type = (short)MediaType.Image;
            die.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            die.Media.Size = die.Media.Content.Length;
            die.Media.OriginalFileName = "test.txt";

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            die.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated die from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Die dieFromDB = newContext.EntityContext.DieSet.FirstOrDefault(d => d.Guid == die.Guid);

            // Verify if die was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(die.Name, dieFromDB.Name, "Fails to update a die.");
        }

        /// <summary>
        /// Creates a die, deletes it and checks if die was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteDieTest()
        {
            Die die = new Die();
            die.Name = die.Guid.ToString();

            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            die.Media = media;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            die.Manufacturer = manufacturer;

            // Get the entities that belongs to the die unless the shared ones in order to check that the die was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(die);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die);
            dataContext1.SaveChanges();

            // Delete the created die
            dataContext1.DieRepository.RemoveAll(die);
            dataContext1.SaveChanges();

            // Verify that the die was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid die
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfValidDie()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.DieRepository.GetParentProjectId(die);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project guid of a die entity");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a die which hasn't a parent
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfDieWithoutParent()
        {
            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.DieRepository.GetParentProjectId(die);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a die which hasn't a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null die.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullDie()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext.DieRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid die
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfValidDie()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.DieRepository.GetParentAssemblyId(die);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly of a die entity");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a die which hasn't a parent assembly
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfDieWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.DieRepository.GetParentAssemblyId(die);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a die which hasn't a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null die.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyGuidOfNullDie()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.AssemblyRepository.GetTopParentAssemblyId(Guid.Empty);
        }

        /// <summary>
        /// A test for GetAllMasterData method
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataTest()
        {
            // Create a die in master data
            Die die1 = new Die();
            die1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            die1.IsMasterData = true;

            // Create a die that is not master data
            Die die2 = new Die();
            die2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die1);
            dataContext.DieRepository.Add(die2);
            dataContext.SaveChanges();

            Collection<Die> actualResults = dataContext.DieRepository.GetAllMasterData();
            Collection<Die> expectedResult = new Collection<Die>(dataContext.EntityContext.DieSet.Where(d => d.IsMasterData).ToList());

            // Verifies if the actual results are the expected ones
            Assert.AreEqual<int>(actualResults.Count, expectedResult.Count, "Failed to get all master data dies");
            foreach (Die die in expectedResult)
            {
                Die correspondingDie = actualResults.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Wrong dies returned");
                }
            }
        }

        /// <summary>
        /// A test for GetById method using a valid die as input parameter
        /// </summary>
        [TestMethod]
        public void GetByIdValidDieTest()
        {
            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Die result = newContext.DieRepository.GetById(die.Guid);

            Assert.AreEqual<Guid>(die.Guid, result.Guid, "Failed to get by id a die entity");
        }

        /// <summary>
        /// A test for GetById method using a null die as input parameter
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullDieTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Die die = null;
            dataContext.DieRepository.GetById(die.Guid);
        }

        #endregion Test Methods
    }
}
