﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for MachineRepository and is intended to contain
    /// unit tests for all public methods from MachineRepository class.
    /// </summary>
    [TestClass]
    public class MachineRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MachineRepositoryTest"/> class.
        /// </summary>
        public MachineRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region TestMethods

        /// <summary>
        /// Test AddMachine method. Create a machine and adds the created machine into 
        /// local context, commits the changes, then checks if machine was added. 
        /// </summary>
        [TestMethod()]
        public void AddMachineTest()
        {
            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.Media = new Media();
            machine.Media.Type = (short)MediaType.Image;
            machine.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            machine.Media.Size = machine.Media.Content.Length;
            machine.Media.OriginalFileName = "test.txt";
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            // Verifies if machine was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool machineAdded = newContext.MachineRepository.CheckIfExists(machine.Guid);

            Assert.IsTrue(machineAdded, "Fails to create a machine.");
        }

        /// <summary>
        /// Creates a machine, updates it and checks if the machine was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateMachineTest()
        {
            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            machine.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated machine from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Machine machineFromDB = newContext.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);

            // Verify if machine was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(machine.Name, machineFromDB.Name, "Fails to update a machine.");
        }

        /// <summary>
        /// Creates a machine, deletes it and checks if machine was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteMachineTest()
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();

            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            machine.Media = media;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            machine.Manufacturer = manufacturer;

            // Get the entities that belongs to the machine unless the shared ones in order to check that the machine was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(machine);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MachineRepository.Add(machine);
            dataContext1.SaveChanges();

            // Delete the machine and save the changes
            dataContext1.MachineRepository.RemoveAll(machine);
            dataContext1.SaveChanges();

            // Verify that the machine was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetParentProjectId method using a valid machine as input data.
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfValidMachine()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid result = newContext.MachineRepository.GetParentProjectId(machine);

            Assert.AreEqual<Guid>(project.Guid, result, "Failed to get the parent project id of a machine entity");
        }

        /// <summary>
        /// A test for GetParentProjectId method using a machine that doesn't have a parent.
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfMachineWithoutParent()
        {
            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid result = newContext.MachineRepository.GetParentProjectId(machine);

            Assert.AreEqual<Guid>(Guid.Empty, result, "A parent project id was returned for a machine that doesn't have a parent");
        }

        /// <summary>
        /// A test for GetParentProjectId method using a null machine as input data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullMachine()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// A test for GetParentAssemblyId method using a valid machine entity as input data.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfValidMachine()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            AssemblyProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid result = newContext.MachineRepository.GetParentAssemblyId(machine);

            Assert.AreEqual<Guid>(assembly.Guid, result, "Failed to get the parent assembly id of a machine entity");
        }

        /// <summary>
        /// A test for GetParentAssemblyId method using a machine that doesn't have a parent assembly.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfMachineWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid result = newContext.MachineRepository.GetParentAssemblyId(machine);

            Assert.AreEqual<Guid>(Guid.Empty, result, "A parent assembly id was returned for a machine that doesn't have a parent assembly");
        }

        /// <summary>
        /// A test method for GetParentAssemblyId method using a null machine as input data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullMachine()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.GetParentAssemblyId(null);
        }

        /// <summary>
        /// A test for GetById method using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidMachine()
        {
            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Machine result = newContext.MachineRepository.GetById(machine.Guid);

            Assert.AreEqual<Guid>(machine.Guid, result.Guid, "Failed to get by id a machine entity");
        }

        /// <summary>
        /// A test for GetById method using a null machine as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullMachine()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Machine machine = null;
            dataContext.MachineRepository.GetById(machine.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData() method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataMachinesTest()
        {
            Machine machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine1.IsMasterData = true;

            Machine machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine1);
            dataContext.MachineRepository.Add(machine2);
            dataContext.SaveChanges();

            Collection<Machine> actualResult = dataContext.MachineRepository.GetAllMasterData();
            Collection<Machine> expectedResult = new Collection<Machine>(dataContext.EntityContext.MachineSet.Where(m => m.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data machines");
            foreach (Machine machine in expectedResult)
            {
                Machine correspondingMachine = actualResult.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Wrong machines were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetMasterDataMachines method.
        /// </summary>
        [TestMethod]
        public void GetMasterDataMachinesTest()
        {
            Machine machine1 = new Machine();
            machine1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine1.IsMasterData = true;

            Machine machine2 = new Machine();
            machine2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine1);
            dataContext.MachineRepository.Add(machine2);
            dataContext.SaveChanges();

            Collection<Guid> guidList1 = new Collection<Guid>();
            guidList1.Add(machine1.Guid);
            guidList1.Add(machine2.Guid);

            Collection<Machine> result1 = dataContext.MachineRepository.GetMasterDataMachines(guidList1);
            Collection<Machine> expectedResult1 = new Collection<Machine>(dataContext.EntityContext.MachineSet.Where(m => m.IsMasterData && guidList1.Contains(m.Guid)).ToList());

            Assert.AreEqual<int>(expectedResult1.Count, result1.Count, "Failed to get master data machines");
            foreach (Machine machine in expectedResult1)
            {
                Machine correspondingMachine = result1.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("A wrong machine was returned");
                }
            }

            // Tests the method using a null list as input data
            Collection<Guid> guidList2 = null;
            Collection<Machine> result2 = dataContext.MachineRepository.GetMasterDataMachines(guidList2);

            // Verify that no machine was returned for an empty list of guids
            Assert.IsTrue(result2.Count == 0, "No machine should be returned for an empty list of guids");
        }

        #endregion TestMethods
    }
}
