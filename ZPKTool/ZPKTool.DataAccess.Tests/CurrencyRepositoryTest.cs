﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for CurrencyRepository and is intended to contain
    /// unit tests for all public methods from CurrencyRepository class.
    /// </summary>
    [TestClass]
    public class CurrencyRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyRepositoryTest"/> class.
        /// </summary>
        public CurrencyRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddCurrency method. Create a currency and adds the created currency into 
        /// local context, commits the changes, then checks if currency was added. 
        /// </summary>
        [TestMethod]
        public void AddCurrencyTest()
        {
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.Symbol = "$";
            currency.ExchangeRate = 100;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.CurrencyRepository.Add(currency);
            contextManager.SaveChanges();

            // Verifies if currency was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool currencyAdded = newContext.CurrencyRepository.CheckIfExists(currency.Guid);

            Assert.IsTrue(currencyAdded, "Failed to create a currency.");
        }

        /// <summary>
        /// Creates a currency, updates it and checks if currency was updated.
        /// </summary>
        [TestMethod]
        public void UpdateCurrencyTest()
        {
            Currency currency = new Currency();
            currency.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency.Symbol = "$";
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 100;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();

            currency.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated currency from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Currency currencyFromDB = newContext.EntityContext.CurrencySet.FirstOrDefault(c => c.Guid == currency.Guid);

            // Verify if currency was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(currency.Name, currencyFromDB.Name, "Failed to update a currency.");
        }

        /// <summary>
        /// Creates a currency, deletes it and checks if currency was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteCurrencyTest()
        {
            Currency currency = new Currency();
            currency.Name = currency.Guid.ToString();
            currency.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency.ExchangeRate = 1000;

            // Get the entities that belongs to currency unless the shared ones in order to check that the currency was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(currency);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.CurrencyRepository.Add(currency);
            dataContext1.SaveChanges();

            // Delete the currency and save the changes
            dataContext1.CurrencyRepository.RemoveAll(currency);
            dataContext1.SaveChanges();

            // Verify that the currency was permanently deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetBaseCurrencies() method.
        /// </summary>
        [TestMethod]
        public void GetAllCurrenciesTest()
        {
            // Add a currency that is not base currency
            Currency currency1 = new Currency();
            currency1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            currency1.Symbol = "S";
            currency1.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            currency1.ExchangeRate = 9;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CurrencyRepository.Add(currency1);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Currency> actualResult = newContext.CurrencyRepository.GetBaseCurrencies();
            Collection<Currency> expectedResult = new Collection<Currency>(newContext.EntityContext.CurrencySet.Where(c => !c.IsReleased && c.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all base currencies");
            foreach (Currency currency in expectedResult)
            {
                Currency correspondingCurrency = actualResult.FirstOrDefault(c => c.Guid == currency.Guid);
                if (correspondingCurrency == null)
                {
                    Assert.Fail("Wrong currencies were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
