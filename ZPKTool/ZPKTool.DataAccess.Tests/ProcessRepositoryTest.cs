﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ProcessRepository and is intended to contain
    /// unit tests for all public methods from ProcessRepository class.
    /// </summary>
    [TestClass]
    public class ProcessRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessRepositoryTest"/> class.
        /// </summary>
        public ProcessRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddProcess method. Create a process and adds the created process into 
        /// local context,commits the changes, then checks if process was added. 
        /// </summary>
        [TestMethod]
        public void AddProcessTest()
        {
            Process process = new Process();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessRepository.Add(process);
            dataContext.SaveChanges();

            // Verifies if process was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool processAdded = newContext.ProcessRepository.CheckIfExists(process.Guid);

            Assert.IsTrue(processAdded, "Failed to create a process.");
        }

        /// <summary>
        /// Creates a process, updates it and checks if process was updated.
        /// </summary>
        [TestMethod]
        public void UpdateProcessTest()
        {
            Process process = new Process();
            process.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessRepository.Add(process);
            dataContext.SaveChanges();

            process.IsMasterData = false;
            dataContext.SaveChanges();

            // Create a new context in order to get the updated process from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Process processFromDB = newContext.EntityContext.ProcessSet.FirstOrDefault(p => p.Guid == process.Guid);

            // Verify if process was updated in database too, by comparing the updated entity with the entity stored in data base.
            Assert.AreEqual<bool>(process.IsMasterData, processFromDB.IsMasterData, "Failed to update a process entity.");
        }

        /// <summary>
        /// Creates a process, deletes it and checks if process was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteProcessTest()
        {
            Process process = new Process();
            ProcessStep step = new AssemblyProcessStep();
            step.Name = step.Guid.ToString();
            step.Accuracy = ProcessCalculationAccuracy.Estimated;
            step.CycleTime = 100;
            step.ProcessTime = 20;
            step.PartsPerCycle = 5;
            step.ScrapAmount = 20;
            process.Steps.Add(step);

            // Add media to process step.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            step.Media = media;

            // Add a machine to process step.
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.Media = media.Copy();
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            step.Machines.Add(machine);

            // Add a consumable to process step.
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step.Consumables.Add(consumable);

            // Add a die to process step.
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Media = media.Copy();
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            step.Dies.Add(die);

            // Add a commodity to process step
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Media = media.Copy();
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            step.Commodities.Add(commodity);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation.TransportTimeFromBuffer = 10;
            calculation.ToolChangeTime = 10;
            calculation.TransportClampingTime = 10;
            calculation.MachiningTime = 10;
            calculation.ToolChangeTime = 10;
            step.CycleTimeCalculations.Add(calculation);

            // Get the entities that belongs to the process unless the shared ones in order to check that the process was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(process);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            // Delete the process and save the changes
            dataContext1.ProcessRepository.RemoveAll(process);
            dataContext1.SaveChanges();

            // Verify that the process was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetParentProjectId method using a valid process.
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfValidProcess()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            Process process = new Process();
            process.Steps.Add(step);
            part.Process = process;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.ProcessRepository.GetParentProjectId(process);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project id of a process entity.");
        }

        /// <summary>
        /// A test for GetParentProjectId method using a process which hasn't a parent project
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfProcessWithoutParentProject()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;
            part.SetIsMasterData(true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.ProcessRepository.GetParentProjectId(process);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a process that doesn't have a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null process.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullProcess()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = contextManager.ProcessRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid process.
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfValidProcess()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            Process process = new Process();
            assembly.Process = process;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.ProcessRepository.GetParentAssemblyId(process);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of a process entity.");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a process that doesn't have a parent assembly.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfProcessWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Process process = new Process();
            part.Process = process;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.ProcessRepository.GetParentAssemblyId(process);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a process that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null process.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullProcess()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = contextManager.ProcessRepository.GetParentAssemblyId(null);
        }

        #endregion Test Methods
    }
}
