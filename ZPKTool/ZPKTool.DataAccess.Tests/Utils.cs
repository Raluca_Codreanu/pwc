﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;
using ZPKTool.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// Helper class containing useful methods used in DataAccess unit tests.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Configures the database access for the data access layer.
        /// Due to implementation details, this method should be called before attempting any call to data access methods.
        /// </summary>
        public static void ConfigureDbAccess()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();            
            localConnParams.InitialCatalog = "ZPKTool";
            localConnParams.DataSource = @"localhost\SQLEXPRESS";
            localConnParams.UserId = "PROimpensUser";
            localConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            localConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;

            DbConnectionInfo centralConnParams = new DbConnectionInfo();            
            centralConnParams.InitialCatalog = "ZPKToolCentral";
            centralConnParams.DataSource = @"localhost\SQLEXPRESS";
            centralConnParams.UserId = "PROimpensUser";
            centralConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            centralConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;
            
            ConnectionConfiguration.Initialize(localConnParams, centralConnParams);
        }

        /// <summary>
        /// Verifies if a list of entities were permanently deleted.
        /// </summary>
        /// <param name="deletedEntities">The list of entities to be checked.</param>
        /// <param name="dataManager">The change set associated with the entities.</param>
        public static void VerifyDeletedEntities(List<IIdentifiable> deletedEntities, IDataSourceManager dataManager)
        {
            foreach (IIdentifiable entity in deletedEntities)
            {
                if (entity is ProjectFolder)
                {
                    Assert.IsNull(dataManager.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == entity.Guid), "Failed to delete a project folder.");
                }
                else if (entity is Project)
                {
                    Assert.IsNull(dataManager.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == entity.Guid), "Failed to delete a project.");
                }
                else if (entity is Assembly)
                {
                    Assert.IsNull(dataManager.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == entity.Guid), "Failed to delete an assembly.");
                }
                else if (entity is Part)
                {
                    Assert.IsNull(dataManager.EntityContext.PartSet.FirstOrDefault(p => p.Guid == entity.Guid), "Failed to delete a part.");
                }
                else if (entity is RawMaterial)
                {
                    Assert.IsNull(dataManager.EntityContext.RawMaterialSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a raw material.");
                }
                else if (entity is Commodity)
                {
                    Assert.IsNull(dataManager.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a commodity.");
                }
                else if (entity is Consumable)
                {
                    Assert.IsNull(dataManager.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a consumable.");
                }
                else if (entity is Machine)
                {
                    Assert.IsNull(dataManager.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a machine.");
                }
                else if (entity is Die)
                {
                    Assert.IsNull(dataManager.EntityContext.DieSet.FirstOrDefault(d => d.Guid == entity.Guid), "Failed to delete a die.");
                }
                else if (entity is Currency)
                {
                    Assert.IsNull(dataManager.EntityContext.CurrencySet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a currency.");
                }
                else if (entity is Media)
                {
                    Assert.IsNull(dataManager.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a media.");
                }
                else if (entity is ProcessStep)
                {
                    Assert.IsNull(dataManager.EntityContext.ProcessStepSet.FirstOrDefault(s => s.Guid == entity.Guid), "Failed to delete a process step.");
                }
                else if (entity is Process)
                {
                    Assert.IsNull(dataManager.EntityContext.ProcessSet.FirstOrDefault(p => p.Guid == entity.Guid), "Failed to delete a process.");
                }
                else if (entity is MeasurementUnit)
                {
                    Assert.IsNull(dataManager.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a measurement unit.");
                }
                else if (entity is User)
                {
                    Assert.IsNull(dataManager.EntityContext.UserSet.FirstOrDefault(u => u.Guid == entity.Guid), "Failed to delete a user.");
                }
                else if (entity is OverheadSetting)
                {
                    Assert.IsNull(dataManager.EntityContext.OverheadSettingSet.FirstOrDefault(s => s.Guid == entity.Guid), "Failed to delete the overhead settings.");
                }
                else if (entity is Customer)
                {
                    Assert.IsNull(dataManager.EntityContext.CustomerSet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a customer.");
                }
                else if (entity is CountrySetting)
                {
                    Assert.IsNull(dataManager.EntityContext.CountrySettingSet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a country setting.");
                }
                else if (entity is Country)
                {
                    Assert.IsNull(dataManager.EntityContext.CountrySet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a country.");
                }
                else if (entity is Manufacturer)
                {
                    Assert.IsNull(dataManager.EntityContext.ManufacturerSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a manufacturer.");
                }
                else if (entity is CountryState)
                {
                    Assert.IsNull(dataManager.EntityContext.CountryStateSet.FirstOrDefault(s => s.Guid == entity.Guid), "Failed to delete a country state.");
                }
                else if (entity is ProcessStepsClassification)
                {
                    Assert.IsNull(dataManager.EntityContext.ProcessStepsClassificationSet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a process step classification.");
                }
                else if (entity is MaterialsClassification)
                {
                    Assert.IsNull(dataManager.EntityContext.MaterialsClassificationSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a material classification.");
                }
                else if (entity is MachinesClassification)
                {
                    Assert.IsNull(dataManager.EntityContext.MachinesClassificationSet.FirstOrDefault(m => m.Guid == entity.Guid), "Failed to delete a machine classification.");
                }
                else if (entity is ProcessStepAssemblyAmount)
                {
                    Assert.IsNull(dataManager.EntityContext.ProcessStepAssemblyAmountSet.FirstOrDefault(a => a.Guid == entity.Guid), "Failed to delete an assembly amount.");
                }
                else if (entity is ProcessStepPartAmount)
                {
                    Assert.IsNull(dataManager.EntityContext.ProcessStepPartAmountSet.FirstOrDefault(a => a.Guid == entity.Guid), "Failed to delete a part amount.");
                }
                else if (entity is CycleTimeCalculation)
                {
                    Assert.IsNull(dataManager.EntityContext.CycleTimeCalculationSet.FirstOrDefault(c => c.Guid == entity.Guid), "Failed to delete a cycle time calculation.");
                }
            }
        }

        /// <summary>
        /// Create a list that contains the entities belonging to the entity's graph unless the shared entities.
        /// </summary>
        /// <param name="entity">The parent entity.</param>
        /// <returns>A list of entities from the entity's graph unless the shared entities.</returns>
        public static List<IIdentifiable> GetEntityGraph(IIdentifiable entity)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();

            if (entity is ProjectFolder)
            {
                ProjectFolder folder = entity as ProjectFolder;
                return GetProjectFolderGraph(folder);
            }

            if (entity is Project)
            {
                Project project = entity as Project;
                return GetProjectGraph(project);
            }

            if (entity is Assembly)
            {
                Assembly assembly = entity as Assembly;
                return GetAssemblyGraph(assembly);
            }

            if (entity is Part)
            {
                Part part = entity as Part;
                return GetPartGraph(part);
            }

            if (entity is RawMaterial)
            {
                RawMaterial material = entity as RawMaterial;
                return GetRawMaterialGraph(material);
            }

            if (entity is Commodity)
            {
                Commodity commodity = entity as Commodity;
                return GetCommodityGraph(commodity);
            }

            if (entity is Machine)
            {
                Machine machine = entity as Machine;
                return GetMachineGraph(machine);
            }

            if (entity is Consumable)
            {
                Consumable consumable = entity as Consumable;
                return GetConsumableGraph(consumable);
            }

            if (entity is Die)
            {
                Die die = entity as Die;
                return GetDieGraph(die);
            }

            if (entity is Process)
            {
                Process process = entity as Process;
                return GetProcessGraph(process);
            }

            if (entity is ProcessStep)
            {
                ProcessStep step = entity as ProcessStep;
                return GetProcessStepGraph(step);
            }

            if (entity is Manufacturer)
            {
                Manufacturer manufacturer = entity as Manufacturer;
                return GetManufacturerGraph(manufacturer);
            }

            if (entity is Currency)
            {
                Currency currency = entity as Currency;
                return GetCurrencyGraph(currency);
            }

            if (entity is Media)
            {
                Media media = entity as Media;
                return GetMediaGraph(media);
            }

            if (entity is MeasurementUnit)
            {
                MeasurementUnit measurementUnit = entity as MeasurementUnit;
                return GetMeasurementUnitGraph(measurementUnit);
            }

            if (entity is OverheadSetting)
            {
                OverheadSetting overheadSettings = entity as OverheadSetting;
                return GetOverheadSettingsGraph(overheadSettings);
            }

            if (entity is CountrySetting)
            {
                CountrySetting countrySettings = entity as CountrySetting;
                return GetCountrySettingsGraph(countrySettings);
            }

            if (entity is CountryState)
            {
                CountryState state = entity as CountryState;
                return GetCountryStateGraph(state);
            }

            if (entity is Country)
            {
                Country country = entity as Country;
                return GetCountryGraph(country);
            }

            if (entity is Customer)
            {
                Customer supplier = entity as Customer;
                return GetSupplierGraph(supplier);
            }

            if (entity is User)
            {
                User user = entity as User;
                return GetUserGraph(user);
            }

            if (entity is ProcessStepsClassification)
            {
                ProcessStepsClassification classification = entity as ProcessStepsClassification;
                return GetProcessStepClassificationGraph(classification);
            }

            if (entity is MachinesClassification)
            {
                MachinesClassification classification = entity as MachinesClassification;
                return GetMachinesClassificationGraph(classification);
            }

            if (entity is MaterialsClassification)
            {
                MaterialsClassification classification = entity as MaterialsClassification;
                return GetMaterialsClassificationGraph(classification);
            }

            if (entity is ProcessStepAssemblyAmount)
            {
                ProcessStepAssemblyAmount assemblyAmount = entity as ProcessStepAssemblyAmount;
                return GetProcessStepAssemblyAmountGraph(assemblyAmount);
            }

            if (entity is ProcessStepPartAmount)
            {
                ProcessStepPartAmount partAmount = entity as ProcessStepPartAmount;
                return GetProcessStepPartAmountGraph(partAmount);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the folder's graph unless the shared entities.
        /// </summary>
        /// <param name="folder">The parent folder.</param>
        /// <returns>A list of entities from the folder's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProjectFolderGraph(ProjectFolder folder)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (folder == null)
            {
                return result;
            }

            result.Add(folder);
            foreach (Project project in folder.Projects)
            {
                List<IIdentifiable> projectGraph = GetProjectGraph(project);
                result.AddRange(projectGraph);
            }

            foreach (ProjectFolder subFolder in folder.ChildrenProjectFolders)
            {
                List<IIdentifiable> folderGraph = GetProjectFolderGraph(subFolder);
                result.AddRange(folderGraph);
            }

            return result;
        }

        /// <summary>
        ///  Create a list that contains the entities of the project's graph unless the shared entities.
        /// </summary>
        /// <param name="project">The parent project.</param>
        /// <returns>A list of entities from the project's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProjectGraph(Project project)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (project == null)
            {
                return result;
            }

            result.Add(project);
            if (project.Customer != null)
            {
                List<IIdentifiable> supplierGraph = GetSupplierGraph(project.Customer);
                result.AddRange(supplierGraph);
            }

            if (project.OverheadSettings != null)
            {
                List<IIdentifiable> overheadSettingsGraph = GetOverheadSettingsGraph(project.OverheadSettings);
                result.AddRange(overheadSettingsGraph);
            }

            foreach (Media media in project.Media)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(media);
                result.AddRange(mediaGraph);
            }

            foreach (Part part in project.Parts)
            {
                List<IIdentifiable> partGraph = GetPartGraph(part);
                result.AddRange(partGraph);
            }

            foreach (Assembly assembly in project.Assemblies)
            {
                List<IIdentifiable> assemblyGraph = GetAssemblyGraph(assembly);
                result.AddRange(assemblyGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the assembly's graph unless the shared entities.
        /// </summary>
        /// <param name="assembly">The parent assembly.</param>
        /// <returns>A list of entities from the assembly's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetAssemblyGraph(Assembly assembly)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (assembly == null)
            {
                return result;
            }

            result.Add(assembly);
            if (assembly.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(assembly.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (assembly.OverheadSettings != null)
            {
                List<IIdentifiable> overheadSettingsGraph = GetOverheadSettingsGraph(assembly.OverheadSettings);
                result.AddRange(overheadSettingsGraph);
            }

            if (assembly.CountrySettings != null)
            {
                List<IIdentifiable> countrySettingsGraph = GetCountrySettingsGraph(assembly.CountrySettings);
                result.AddRange(countrySettingsGraph);
            }

            foreach (Media media in assembly.Media)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(media);
                result.AddRange(mediaGraph);
            }

            if (assembly.Process != null)
            {
                List<IIdentifiable> processGraph = GetProcessGraph(assembly.Process);
                result.AddRange(processGraph);
            }

            foreach (Part part in assembly.Parts)
            {
                List<IIdentifiable> partGraph = GetPartGraph(part);
                result.AddRange(partGraph);
            }

            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                List<IIdentifiable> subassemblyGraph = GetAssemblyGraph(subassembly);
                result.AddRange(subassemblyGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the part's graph unless the shared entities.
        /// </summary>
        /// <param name="part">The parent part.</param>
        /// <returns>A list of entities from the part's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetPartGraph(Part part)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (part == null)
            {
                return result;
            }

            result.Add(part);
            if (part.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(part.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (part.OverheadSettings != null)
            {
                List<IIdentifiable> overheadSettingsGraph = GetOverheadSettingsGraph(part.OverheadSettings);
                result.AddRange(overheadSettingsGraph);
            }

            if (part.CountrySettings != null)
            {
                List<IIdentifiable> countrySettingsGraph = GetCountrySettingsGraph(part.CountrySettings);
                result.AddRange(countrySettingsGraph);
            }

            foreach (Media media in part.Media)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(media);
                result.AddRange(mediaGraph);
            }

            foreach (RawMaterial material in part.RawMaterials)
            {
                List<IIdentifiable> materialGraph = GetRawMaterialGraph(material);
                result.AddRange(materialGraph);
            }

            foreach (Commodity commodity in part.Commodities)
            {
                List<IIdentifiable> commodityGraph = GetCommodityGraph(commodity);
                result.AddRange(commodityGraph);
            }

            if (part.Process != null)
            {
                List<IIdentifiable> processGraph = GetProcessGraph(part.Process);
                result.AddRange(processGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the manufacturer's graph unless the shared entities.
        /// </summary>
        /// <param name="manufacturer">The parent manufacturer.</param>
        /// <returns>A list of entities from the manufacturer's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetManufacturerGraph(Manufacturer manufacturer)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (manufacturer == null)
            {
                return result;
            }

            result.Add(manufacturer);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the currency's graph unless the shared entities.
        /// </summary>
        /// <param name="currency">The parent currency.</param>
        /// <returns>A list of entities from the currency's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCurrencyGraph(Currency currency)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (currency == null)
            {
                return result;
            }

            result.Add(currency);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the media's graph unless the shared entities.
        /// </summary>
        /// <param name="media">The parent media.</param>
        /// <returns>A list of entities from the media's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetMediaGraph(Media media)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (media == null)
            {
                return result;
            }

            result.Add(media);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the measurement unit's graph unless the shared entities.
        /// </summary>
        /// <param name="measurementUnit">The parent measurement unit.</param>
        /// <returns>A list of entities from the measurement unit's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetMeasurementUnitGraph(MeasurementUnit measurementUnit)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (measurementUnit == null)
            {
                return result;
            }

            result.Add(measurementUnit);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the overheadSettings' graph unless the shared entities.
        /// </summary>
        /// <param name="overheadSettings">The parent overhead settings.</param>
        /// <returns>A list of entities from the overheadSettings' graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetOverheadSettingsGraph(OverheadSetting overheadSettings)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (overheadSettings == null)
            {
                return result;
            }

            result.Add(overheadSettings);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the overheadSettings' graph unless the shared entities.
        /// </summary>
        /// <param name="countrySetting">The parent countrySetting.</param>
        /// <returns>A list of entities from the countrySetting' graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCountrySettingsGraph(CountrySetting countrySetting)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (countrySetting == null)
            {
                return result;
            }

            result.Add(countrySetting);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the state' graph unless the shared entities.
        /// </summary>
        /// <param name="state">The parent state.</param>
        /// <returns>A list of entities from the state' graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCountryStateGraph(CountryState state)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (state == null)
            {
                return result;
            }

            result.Add(state);
            //TO DO: Uncomment when the defect #1219 will be fixed
            //if (state.CountrySettings != null)
            //{
            //    List<IIdentifiable> countrySettingsGraph = GetCountrySettingsGraph(state.CountrySettings);
            //    result.AddRange(countrySettingsGraph);
            //}

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the customer's graph unless the shared entities.
        /// </summary>
        /// <param name="customer">The parent customer.</param>
        /// <returns>A list of entities from the customer's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetSupplierGraph(Customer customer)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (customer == null)
            {
                return result;
            }

            result.Add(customer);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the commodity's graph unless the shared entities.
        /// </summary>
        /// <param name="commodity">The parent commodity.</param>
        /// <returns>A list of entities from the commodity's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCommodityGraph(Commodity commodity)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (commodity == null)
            {
                return result;
            }

            result.Add(commodity);
            if (commodity.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(commodity.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (commodity.Media != null)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(commodity.Media);
                result.AddRange(mediaGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the die's graph unless the shared entities.
        /// </summary>
        /// <param name="die">The parent die.</param>
        /// <returns>A list of entities from the die's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetDieGraph(Die die)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (die == null)
            {
                return result;
            }

            result.Add(die);
            if (die.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(die.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (die.Media != null)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(die.Media);
                result.AddRange(mediaGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the machine's graph unless the shared entities.
        /// </summary>
        /// <param name="machine">The parent machine.</param>
        /// <returns>A list of entities from the machine's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetMachineGraph(Machine machine)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (machine == null)
            {
                return result;
            }

            result.Add(machine);
            if (machine.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(machine.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (machine.Media != null)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(machine.Media);
                result.AddRange(mediaGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the consumable's graph unless the shared entities.
        /// </summary>
        /// <param name="consumable">The parent consumable.</param>
        /// <returns>A list of entities from the consumable's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetConsumableGraph(Consumable consumable)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (consumable == null)
            {
                return result;
            }

            result.Add(consumable);
            if (consumable.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(consumable.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the material's graph unless the shared entities.
        /// </summary>
        /// <param name="material">The parent material.</param>
        /// <returns>A list of entities from the material's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetRawMaterialGraph(RawMaterial material)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (material == null)
            {
                return result;
            }

            result.Add(material);
            if (material.Manufacturer != null)
            {
                List<IIdentifiable> manufacturerGraph = GetManufacturerGraph(material.Manufacturer);
                result.AddRange(manufacturerGraph);
            }

            if (material.Media != null)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(material.Media);
                result.AddRange(mediaGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the process's graph unless the shared entities.
        /// </summary>
        /// <param name="process">The parent process.</param>
        /// <returns>A list of entities from the process's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProcessGraph(Process process)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (process == null)
            {
                return result;
            }

            result.Add(process);
            foreach (ProcessStep step in process.Steps)
            {
                List<IIdentifiable> stepGraph = GetProcessStepGraph(step);
                result.AddRange(stepGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the step's graph unless the shared entities.
        /// </summary>
        /// <param name="step">The parent process step.</param>
        /// <returns>A list of entities from the step's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProcessStepGraph(ProcessStep step)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (step == null)
            {
                return result;
            }

            result.Add(step);
            if (step.Media != null)
            {
                List<IIdentifiable> mediaGraph = GetMediaGraph(step.Media);
                result.AddRange(mediaGraph);
            }

            foreach (CycleTimeCalculation calculation in step.CycleTimeCalculations)
            {
                List<IIdentifiable> calculationGraph = GetCycleTimeCalculationGraph(calculation);
                result.AddRange(calculationGraph);
            }

            foreach (Machine machine in step.Machines)
            {
                List<IIdentifiable> machineGraph = GetMachineGraph(machine);
                result.AddRange(machineGraph);
            }

            foreach (Consumable consumable in step.Consumables)
            {
                List<IIdentifiable> consumableGraph = GetConsumableGraph(consumable);
                result.AddRange(consumableGraph);
            }

            foreach (Die die in step.Dies)
            {
                List<IIdentifiable> dieGraph = GetDieGraph(die);
                result.AddRange(dieGraph);
            }

            foreach (Commodity commodity in step.Commodities)
            {
                List<IIdentifiable> commodityGraph = GetCommodityGraph(commodity);
                result.AddRange(commodityGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the user's graph unless the shared entities.
        /// </summary>
        /// <param name="user">The parent user.</param>
        /// <returns>A list of entities from the user's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetUserGraph(User user)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (user == null)
            {
                return result;
            }

            result.Add(user);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the country's graph unless the shared entities.
        /// </summary>
        /// <param name="country">The parent country.</param>
        /// <returns>A list of entities from the country's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCountryGraph(Country country)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (country == null)
            {
                return result;
            }

            result.Add(country);
            if (country.CountrySetting != null)
            {
                List<IIdentifiable> countrySettingsGraph = GetCountrySettingsGraph(country.CountrySetting);
                result.AddRange(countrySettingsGraph);
            }

            foreach (CountryState state in country.States)
            {
                List<IIdentifiable> stateGraph = GetCountryStateGraph(state);
                result.AddRange(stateGraph);
            }

            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the classification's graph unless the shared entities.
        /// </summary>
        /// <param name="classification">The parent classification.</param>
        /// <returns>A list of entities from the classification's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProcessStepClassificationGraph(ProcessStepsClassification classification)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (classification == null)
            {
                return result;
            }

            result.Add(classification);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the classification's graph unless the shared entities.
        /// </summary>
        /// <param name="classification">The parent classification.</param>
        /// <returns>A list of entities from the classification's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetMaterialsClassificationGraph(MaterialsClassification classification)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (classification == null)
            {
                return result;
            }

            result.Add(classification);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the classification's graph unless the shared entities.
        /// </summary>
        /// <param name="classification">The parent classification.</param>
        /// <returns>A list of entities from the classification's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetMachinesClassificationGraph(MachinesClassification classification)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (classification == null)
            {
                return result;
            }

            result.Add(classification);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the assemblyAmount's graph unless the shared entities.
        /// </summary>
        /// <param name="assemblyAmount">The parent assemblyAmount.</param>
        /// <returns>A list of entities from the assemblyAmount's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProcessStepAssemblyAmountGraph(ProcessStepAssemblyAmount assemblyAmount)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (assemblyAmount == null)
            {
                return result;
            }

            result.Add(assemblyAmount);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the partAmount's graph unless the shared entities.
        /// </summary>
        /// <param name="partAmount">The parent partAmount.</param>
        /// <returns>A list of entities from the partAmount's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetProcessStepPartAmountGraph(ProcessStepPartAmount partAmount)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (partAmount == null)
            {
                return result;
            }

            result.Add(partAmount);
            return result;
        }

        /// <summary>
        /// Create a list that contains the entities of the calculation's graph unless the shared entities.
        /// </summary>
        /// <param name="calculation">The parent calculation.</param>
        /// <returns>A list of entities from the calculation's graph unless the shared entities.</returns>
        private static List<IIdentifiable> GetCycleTimeCalculationGraph(CycleTimeCalculation calculation)
        {
            List<IIdentifiable> result = new List<IIdentifiable>();
            if (calculation == null)
            {
                return result;
            }

            result.Add(calculation);
            return result;
        }
    }
}
