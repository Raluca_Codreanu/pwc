﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ProcessStepsClassificationRepository and is intended to contain
    /// unit tests for all public methods from ProcessStepsClassificationRepository class.
    /// </summary>
    [TestClass]
    public class ProcessStepsClassificationRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepsClassificationRepositoryTest"/> class.
        /// </summary>
        public ProcessStepsClassificationRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// Test AddProcessStepClassifications method. Create a  process step classification and adds the created  classification into
        /// local context, commits the changes, then checks if classification was added.
        /// </summary>
        [TestMethod]
        public void AddProcessStepsClassificationTest()
        {
            ProcessStepsClassification classification = new ProcessStepsClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            // Add a subtype to main classification.
            ProcessStepsClassification subType = new ProcessStepsClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            classification.Children.Add(subType);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessStepsClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            // Verifies if classification was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool classificationAdded = newContext.ProcessStepsClassificationRepository.CheckIfExists(classification.Guid);

            Assert.IsTrue(classificationAdded, "Failed to create a  ProcessStepsClassification.");
        }

        /// <summary>
        /// Creates a  process step classification, updates it and checks if classification was updated.
        /// </summary>
        [TestMethod]
        public void UpdateProcessStepsClassificationTest()
        {
            ProcessStepsClassification classification = new ProcessStepsClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            // Add a subtype to main classification.
            ProcessStepsClassification subType = new ProcessStepsClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            classification.Children.Add(subType);

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.ProcessStepsClassificationRepository.Add(classification);
            contextManager.SaveChanges();

            classification.Name += " - Updated";
            contextManager.SaveChanges();

            // Create a new context in order to get the updated process step classification from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStepsClassification classificationFromDB = newContext.EntityContext.ProcessStepsClassificationSet.FirstOrDefault(c => c.Guid == classification.Guid);

            // Verifies if classification was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(classification.Name, classificationFromDB.Name, "Failed to update a  process step classification.");
        }

        /// <summary>
        /// Creates a process step classification, deletes it and checks if classification was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteProcessStepsClassificationTest()
        {
            ProcessStepsClassification classification = new ProcessStepsClassification();
            classification.Name = classification.Guid.ToString();

            // Get the entities that belongs to the classification unless the shared ones in order to check that the classification was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(classification);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessStepsClassificationRepository.Add(classification);
            dataContext1.SaveChanges();

            // Delete the created classification and save the changes
            dataContext1.ProcessStepsClassificationRepository.RemoveAll(classification);
            dataContext1.SaveChanges();

            // Verify that the classification was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetClassificationTree() method.
        /// </summary>
        [TestMethod]
        public void GetClassificationTreeTest()
        {
            ProcessStepsClassification mainClassification1 = new ProcessStepsClassification();
            mainClassification1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            // Add a sub-classification to the main classification.
            ProcessStepsClassification subClassification = new ProcessStepsClassification();
            subClassification.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            mainClassification1.Children.Add(subClassification);

            ProcessStepsClassification mainClassification2 = new ProcessStepsClassification();
            mainClassification2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            mainClassification2.IsReleased = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessStepsClassificationRepository.Add(mainClassification1);
            dataContext.ProcessStepsClassificationRepository.Add(mainClassification2);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<ProcessStepsClassification> actualResult = newContext.ProcessStepsClassificationRepository.LoadClassificationTree();
            Collection<ProcessStepsClassification> expectedResult = new Collection<ProcessStepsClassification>(newContext.EntityContext.ProcessStepsClassificationSet.Where(c => !c.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get the process steps classification tree");
            foreach (ProcessStepsClassification classification in expectedResult)
            {
                ProcessStepsClassification correspondingClassification = actualResult.FirstOrDefault(c => c.Guid == classification.Guid);
                if (correspondingClassification == null)
                {
                    Assert.Fail("Wrong classifications were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetByName(string classificationName) method using a valid classification as input data.
        /// </summary>
        [TestMethod]
        public void GetByNameValidProcessStepsClassification()
        {
            ProcessStepsClassification classification1 = new ProcessStepsClassification();
            classification1.Name = EncryptionManager.Instance.GenerateRandomString(10, true) + DateTime.Now.Ticks;

            ProcessStepsClassification classification2 = new ProcessStepsClassification();
            classification2.Name = EncryptionManager.Instance.GenerateRandomString(15, true) + DateTime.Now.Ticks;
            classification2.IsReleased = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessStepsClassificationRepository.Add(classification1);
            dataContext.ProcessStepsClassificationRepository.Add(classification2);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStepsClassification result1 = newContext.ProcessStepsClassificationRepository.GetByName(classification1.Name);

            // Check if the right classification was returned
            Assert.AreEqual<Guid>(classification1.Guid, result1.Guid, "Failed to get a process step classification by name");

            // Retrieve a released classification
            ProcessStepsClassification result2 = newContext.ProcessStepsClassificationRepository.GetByName(classification2.Name);

            // Check if the classification was not returned because is released
            Assert.IsNull(result2, "A released classification was returned + Guid: ");
        }

        /// <summary>
        /// A test for GetByName(string classificationName) method using a null classification as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByNameNullProcessStepsClassification()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProcessStepsClassification classification = null;
            dataContext.ProcessStepsClassificationRepository.GetByName(classification.Name);
        }

        #endregion Test Method
    }
}
