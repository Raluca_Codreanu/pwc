﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;
using System.Collections.ObjectModel;

namespace ZPKTool.DataAccess.Tests
{
    [TestClass]
    public class RawMaterialDeliveryTypeRepositoryTest
    {
        [ClassInitialize]
        public static void InitializeTests(TestContext testContext)
        {
            Utils.ConfigureDbAccess();
        }

        /// <summary>
        /// A test for GetById method using a valid RawMaterialDeliveryType as input data.
        /// </summary>
        [TestMethod]
        public void GetValidRawMaterialDeliveryTypeById()
        {
            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.DeliveryType = deliveryType;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterialDeliveryType result = newContext.RawMaterialDeliveryTypeRepository.FindById(deliveryType.Guid);

            Assert.AreEqual<Guid>(deliveryType.Guid, result.Guid, "Failed to get by id a raw material delivery type");
        }

        /// <summary>
        /// A test for GetById method using a null RawMaterialDeliveryType as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNullRawMaterialDeliveryTypeById()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterialDeliveryType deliveryType = null;
            dataContext.RawMaterialDeliveryTypeRepository.FindById(deliveryType.Guid);
        }

        /// <summary>
        /// A test for GetByName method using a valid RawMaterialDeliveryType as input data.
        /// </summary>
        [TestMethod]
        public void GetValidRawMaterialDeliveryTypeByName()
        {
            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.DeliveryType = deliveryType;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterialDeliveryType result = newContext.RawMaterialDeliveryTypeRepository.FindByName(deliveryType.Name);

            Assert.AreEqual<Guid>(deliveryType.Guid, result.Guid, "Failed to get by id a raw material delivery type");
        }

        /// <summary>
        /// A test for GetAll method.
        /// </summary>
        [TestMethod]
        public void GetAllDeliveryTypesTest()
        {
            RawMaterialDeliveryType deliveryType1 = new RawMaterialDeliveryType();
            deliveryType1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            RawMaterial material = new RawMaterial();
            material.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            material.DeliveryType = deliveryType1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var actualResult = newContext.RawMaterialDeliveryTypeRepository.FindAll();
            var expectedResult = newContext.EntityContext.RawMaterialDeliveryTypeSet.Where(dt => !dt.IsReleased).ToList();

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all raw material delivery types");
            foreach (RawMaterialDeliveryType deliveryType in expectedResult)
            {
                RawMaterialDeliveryType correspondingDeliveryType = actualResult.FirstOrDefault(dt => dt.Guid == deliveryType.Guid);
                if (correspondingDeliveryType == null)
                {
                    Assert.Fail("Wrong raw material delivery types were returned");
                }
            }
        }
    }
}
