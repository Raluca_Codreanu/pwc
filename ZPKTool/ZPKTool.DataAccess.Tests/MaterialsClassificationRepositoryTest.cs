﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for MaterialsClassificationRepository and is intended to contain
    /// unit tests for all public methods from MaterialsClassificationRepository class.
    /// </summary>
    [TestClass]
    public class MaterialsClassificationRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialsClassificationRepositoryTest"/> class.
        /// </summary>
        public MaterialsClassificationRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddMaterialsClassification method. Create a  materials classification and adds the created  classification into
        /// local context, commits the changes, then checks if materials classification was added.
        /// </summary>
        [TestMethod()]
        public void AddMaterialsClassificationTest()
        {
            MaterialsClassification classification = new MaterialsClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a subtype to main classification.
            MaterialsClassification subType = new MaterialsClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            classification.Children.Add(subType);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MaterialsClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            // Verifies if materials classification was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool classificationAdded = newContext.MaterialsClassificationRepository.CheckIfExists(classification.Guid);

            Assert.IsTrue(classificationAdded, "Failed to create a  materials classification.");
        }

        /// <summary>
        /// Creates a  materials classification, updates it and checks if  materials classification was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateMaterialsClassificationTest()
        {
            MaterialsClassification classification = new MaterialsClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a subtype to main classification.
            MaterialsClassification subType = new MaterialsClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(11, true);
            classification.Children.Add(subType);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MaterialsClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            classification.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated  materials classification from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MaterialsClassification classificationFromDB = newContext.EntityContext.MaterialsClassificationSet.FirstOrDefault(c => c.Guid == classification.Guid);

            // Verifies if materials classification was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(classification.Name, classificationFromDB.Name, "Failed to update a  material classification.");
        }

        /// <summary>
        /// Creates a materials classification, deletes it and checks if classification was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteMaterialsClassificationTest()
        {
            MaterialsClassification classification = new MaterialsClassification();
            classification.Name = classification.Guid.ToString();

            // Get the entities that belongs to the classification unless the shared ones in order to check that the classification was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(classification);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MaterialsClassificationRepository.Add(classification);
            dataContext1.SaveChanges();

            // Delete the created classification and save the changes
            dataContext1.MaterialsClassificationRepository.RemoveAll(classification);
            dataContext1.SaveChanges();

            // Verify that the classification was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetByName method using a valid material classification.
        /// </summary>
        [TestMethod]
        public void GetByNameValidMaterialClassification()
        {
            MaterialsClassification classification = new MaterialsClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            MaterialsClassification subclassification = new MaterialsClassification();
            subclassification.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            classification.Children.Add(subclassification);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MaterialsClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MaterialsClassification result = newContext.MaterialsClassificationRepository.GetByName(subclassification.Name);

            Assert.AreEqual<Guid>(subclassification.Guid, result.Guid, "Failed to get by name a material classification");
        }

        /// <summary>
        /// A test for GetByName method using a null material classification. 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByNameNullMaterialClassification()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MaterialsClassification classification = null;
            dataContext.MaterialsClassificationRepository.GetByName(classification.Name);
        }

        /// <summary>
        /// A test for GetClassificationTree() method.
        /// </summary>
        [TestMethod]
        public void GetClassificationTreeTest()
        {
            MaterialsClassification mainClassification = new MaterialsClassification();
            mainClassification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            MaterialsClassification subClassification = new MaterialsClassification();
            subClassification.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            mainClassification.Children.Add(subClassification);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MaterialsClassificationRepository.Add(mainClassification);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MaterialsClassification> actualResult = newContext.MaterialsClassificationRepository.LoadClassificationTree();
            Collection<MaterialsClassification> expectedResult = new Collection<MaterialsClassification>(newContext.EntityContext.MaterialsClassificationSet.Where(c => !c.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get materials classification tree");
            foreach (MaterialsClassification classification in expectedResult)
            {
                MaterialsClassification correspondingClassification = actualResult.FirstOrDefault(c => c.Guid == classification.Guid);
                if (correspondingClassification == null)
                {
                    Assert.Fail("Wrong materials classifications were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
