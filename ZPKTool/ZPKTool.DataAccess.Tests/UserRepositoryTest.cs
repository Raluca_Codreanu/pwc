﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for UserRepository and is intended to contain
    /// unit tests for all public methods from UserRepository class.
    /// </summary>
    [TestClass]
    public class UserRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepositoryTest"/> class.
        /// </summary>
        public UserRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test Add User method. Create a user and adds the created user into 
        /// local context, commits the changes, then checks if user was added. 
        /// </summary>
        [TestMethod]
        public void AddUserTest()
        {
            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Description = EncryptionManager.Instance.GenerateRandomString(20, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Verifies if user was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool userAdded = newContext.UserRepository.CheckIfExists(user.Guid);

            Assert.IsTrue(userAdded, "Failed to create a user.");
        }

        /// <summary>
        /// Creates a  user, updates it and checks if user was updated.
        /// </summary>
        [TestMethod]
        public void UpdateUserTest()
        {
            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Description = EncryptionManager.Instance.GenerateRandomString(20, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.UserRepository.Add(user);
            contextManager.SaveChanges();

            user.Name += " - Updated";
            contextManager.SaveChanges();

            // Create a new context in order to get the updated user from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User userFromDB = newContext.EntityContext.UserSet.FirstOrDefault(u => u.Guid == user.Guid);

            // Verifies if user was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(user.Name, userFromDB.Name, "Failed to update a user.");
        }

        /// <summary>
        /// A test for Save(User user) method. 
        /// Input data: null user.
        /// Expected result: throw ArgumentNullException.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveNullUserTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.UserRepository.Save(null);
        }

        /// <summary>
        /// A test for Save(User user) method. 
        /// Input data: a user that doesn't exist in db and a user that exist in db.
        /// Expected result: Adds or updates the specified user.
        /// </summary>
        [TestMethod]
        public void SaveValidUserTest()
        {
            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = user1.Guid.ToString();
            user1.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user2);
            dataContext1.SaveChanges();

            user2.Name += "_Updated";
            dataContext1.UserRepository.Save(user1);
            dataContext1.UserRepository.Save(user2);
            dataContext1.SaveChanges();

            // Retrieve the updated users from db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User user1Updated = dataContext2.EntityContext.UserSet.FirstOrDefault(u => u.Guid == user1.Guid);
            User user2Updated = dataContext2.EntityContext.UserSet.FirstOrDefault(u => u.Guid == user2.Guid);

            Assert.AreEqual<string>(user2.Name, user2Updated.Name, "Failed to save the updates for a user that exist in db");
            Assert.IsNotNull(user1Updated, "Failed to add a valid user in db");
        }

        /// <summary>
        /// A test for Save(User user) method. Create a user with a duplicate username.
        /// </summary>
        [ExpectedException(typeof(DataAccessException))]
        [TestMethod]
        public void SaveInvalidUser1()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Password = EncryptionManager.Instance.EncodeMD5("password");

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user1);
            dataContext1.SaveChanges();

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = user1.Username;
            user2.Password = EncryptionManager.Instance.EncodeMD5("password");
            dataContext1.UserRepository.Save(user2);
        }

        /// <summary>
        /// A test for Save(User user) method. Update a user with a duplicate username.
        /// </summary>
        [ExpectedException(typeof(DataAccessException))]
        [TestMethod]
        public void SaveInvalidUser2()
        {
            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.EncodeMD5("password");

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.EncodeMD5("password");

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user1);
            dataContext1.UserRepository.Add(user2);
            dataContext1.SaveChanges();

            user2.Username = user1.Username;
            dataContext1.UserRepository.Save(user2);
        }

        /// <summary>
        /// Creates a user, deletes it and checks if user was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteUserTest()
        {
            User user = new User();
            user.Name = user.Guid.ToString();
            user.Description = EncryptionManager.Instance.GenerateRandomString(15, true);
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;

            // Get the entities that belongs to user unless the shared ones in order to check that the user was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(user);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user);
            dataContext1.SaveChanges();

            // Delete the created user and save the changes
            dataContext1.UserRepository.RemoveAll(user);
            dataContext1.SaveChanges();

            // Verify that the user was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetAll(bool includeDisabled, bool noTracking = false) method.
        /// </summary>
        [TestMethod]
        public void GetAllUsersTest()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Disabled = true;
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            User user3 = new User();
            user3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user3.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user3.Password = EncryptionManager.Instance.HashSHA256("Password.", user3.Salt, user3.Guid.ToString());
            user3.IsReleased = true;
            user3.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.UserRepository.Add(user1);
            dataContext.UserRepository.Add(user2);
            dataContext.UserRepository.Add(user3);
            dataContext.SaveChanges();

            // Retrieve all users including disabled users
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<User> actualResult1 = newContext.UserRepository.GetAll(true);
            Collection<User> expectedResult1 = new Collection<User>(newContext.EntityContext.UserSet.Where(u => !u.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult1.Count, actualResult1.Count, "Failed to get all users including disabled users");
            foreach (User user in expectedResult1)
            {
                User correspondingUser = actualResult1.FirstOrDefault(u => u.Guid == user.Guid);
                if (correspondingUser == null)
                {
                    Assert.Fail("Wrong users were returned");
                }
            }

            // Retrieve all users without including the disabled ones
            Collection<User> actualResult2 = newContext.UserRepository.GetAll(false);
            Collection<User> expectedResult2 = new Collection<User>(newContext.EntityContext.UserSet.Where(u => !u.IsReleased && !u.Disabled).ToList());

            Assert.AreEqual<int>(expectedResult2.Count, actualResult2.Count, "Failed to get all users without including disabled");
            foreach (User user in expectedResult2)
            {
                User correspondingUser = actualResult2.FirstOrDefault(u => u.Guid == user.Guid);
                if (correspondingUser == null)
                {
                    Assert.Fail("Wrong users were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetById(Guid id, bool includeDisabled) method using a valid user as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidUserTest()
        {
            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Disabled = true;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Retrieve a user including disabled users
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User result1 = newContext.UserRepository.GetById(user.Guid, true);

            // Check if the disabled user was returned
            Assert.AreEqual<Guid>(user.Guid, result1.Guid, "Failed to get by id a user including disabled");

            // Retrieve a disabled user without including disabled users
            User result2 = newContext.UserRepository.GetById(user.Guid, false);

            // Check if the disabled user was not returned
            Assert.IsNull(result2, "A disabled user was returned for GetAllUsers without including disabled users");
        }

        /// <summary>
        /// A test for GetById(Guid id, bool includeDisabled) method using a null user as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullUserTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User user = null;
            dataContext.UserRepository.GetById(user.Guid, true);
        }

        /// <summary>
        /// A test for GetByUsername(string username, bool includeDisabled, bool noTracking = false) method
        /// using a valid user as input data.
        /// </summary>
        [TestMethod]
        public void GetByUsernameValidUserTest()
        {
            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Disabled = true;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Retrieve a disabled user including disabled users
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User result1 = newContext.UserRepository.GetByUsername(user.Username, true);

            // Check if the disabled user was returned
            Assert.AreEqual<Guid>(user.Guid, result1.Guid, "Failed to get by username a disabled user including disabled users");

            // Retrieve a disabled user without including disabled users
            User result2 = newContext.UserRepository.GetByUsername(user.Username, false);

            // Check that the disabled user was not returned
            Assert.IsNull(result2, "A disabled user was returned for GetAllUsers without including disabled users");
        }

        /// <summary>
        /// A test for GetByUsername(string username, bool includeDisabled, bool noTracking = false) method
        /// using a null user as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByUsernameNullUserTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User user = null;
            dataContext.UserRepository.GetByUsername(user.Username, true);
        }

        #endregion Test Methods
    }
}
