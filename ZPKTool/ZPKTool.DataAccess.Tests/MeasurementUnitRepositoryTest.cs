﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for MeasurementUnitRepository and is intended to contain
    /// unit tests for all public methods from MeasurementUnitRepository class.
    /// </summary>
    [TestClass]
    public class MeasurementUnitRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MeasurementUnitRepositoryTest"/> class.
        /// </summary>
        public MeasurementUnitRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// Test AddMeasurementUnit method. Create a  measurement unit and adds the created  measurement unit into
        /// local context, commits the changes, then checks if measurement unit was added.
        /// </summary>
        [TestMethod]
        public void AddMeasurementUnitTest()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            measurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            measurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            measurementUnit.ConversionRate = 2;
            measurementUnit.Type = MeasurementUnitType.Weight;
            measurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            measurementUnit.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(measurementUnit);
            dataContext.SaveChanges();

            // Verifies if measurement unit was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool measurementUnitAdded = newContext.MeasurementUnitRepository.CheckIfExists(measurementUnit.Guid);

            Assert.IsTrue(measurementUnitAdded, "Failed to create a measurement unit.");
        }

        /// <summary>
        /// Creates a  measurement unit, updates it and checks if measurement unit was updated.
        /// </summary>
        [TestMethod]
        public void UpdateMeasurementUnitTest()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            measurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            measurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            measurementUnit.ConversionRate = 2;
            measurementUnit.Type = MeasurementUnitType.Weight;
            measurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            measurementUnit.ScaleFactor = 1;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.MeasurementUnitRepository.Add(measurementUnit);
            contextManager.SaveChanges();

            measurementUnit.Name += " - Updated";
            contextManager.SaveChanges();

            // Create a new context in order to get the updated measurement unit from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit measurementUnitFromDB = newContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Guid == measurementUnit.Guid);

            // Verifies if measurement unit was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(measurementUnit.Name, measurementUnitFromDB.Name, "Failed to update a  measurement unit.");
        }

        /// <summary>
        /// Creates a measurement unit, deletes it and checks if measurement unit was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteMeasurementUnitTest()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            measurementUnit.Name = measurementUnit.Guid.ToString();
            measurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(3, true);
            measurementUnit.ConversionRate = 2;
            measurementUnit.Type = MeasurementUnitType.Weight;
            measurementUnit.ScaleID = MeasurementUnitScale.MetricWeightScale;
            measurementUnit.ScaleFactor = 1;

            // Get the entities that belongs to the measurement unit unless the shared ones in order to check that the measurement unit was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(measurementUnit);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MeasurementUnitRepository.Add(measurementUnit);
            dataContext1.SaveChanges();

            // Delete the measurement unit
            dataContext1.MeasurementUnitRepository.RemoveAll(measurementUnit);
            dataContext1.SaveChanges();

            // Verify that the measurement unit was permanently deleted 
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetAll() method.
        /// </summary>
        [TestMethod]
        public void GetAllMeasurementUnitsTest()
        {
            MeasurementUnit measurementUnit1 = new MeasurementUnit();
            measurementUnit1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            measurementUnit1.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            measurementUnit1.ConversionRate = 2;
            measurementUnit1.Type = MeasurementUnitType.Weight;
            measurementUnit1.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            measurementUnit1.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(measurementUnit1);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MeasurementUnit> actualResult = newContext.MeasurementUnitRepository.GetAll();
            Collection<MeasurementUnit> expectedResult = new Collection<MeasurementUnit>(newContext.EntityContext.MeasurementUnitSet.Where(m => !m.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all measurement units");
            foreach (MeasurementUnit measurementUnit in expectedResult)
            {
                MeasurementUnit correspondingMeasurementUnit = actualResult.FirstOrDefault(m => m.Guid == measurementUnit.Guid);
                if (correspondingMeasurementUnit == null)
                {
                    Assert.Fail("Wrong measurement units were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetAll(MeasurementUnitType unitsType) method.
        /// </summary>
        [TestMethod]
        public void GetAllMeasurementUnitsByType()
        {
            MeasurementUnit metricWeightMeasurementUnit = new MeasurementUnit();
            metricWeightMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            metricWeightMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            metricWeightMeasurementUnit.ConversionRate = 2;
            metricWeightMeasurementUnit.Type = MeasurementUnitType.Weight;
            metricWeightMeasurementUnit.ScaleID = MeasurementUnitScale.MetricWeightScale;
            metricWeightMeasurementUnit.ScaleFactor = 1;

            MeasurementUnit imperialWeightMeasurementUnit = new MeasurementUnit();
            imperialWeightMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            imperialWeightMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            imperialWeightMeasurementUnit.ConversionRate = 2;
            imperialWeightMeasurementUnit.Type = MeasurementUnitType.Weight;
            imperialWeightMeasurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            imperialWeightMeasurementUnit.ScaleFactor = 1;

            MeasurementUnit volumeMeasurementUnit = new MeasurementUnit();
            volumeMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            volumeMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            volumeMeasurementUnit.ConversionRate = 2;
            volumeMeasurementUnit.Type = MeasurementUnitType.Volume;
            volumeMeasurementUnit.ScaleID = MeasurementUnitScale.MetricVolumeScale;
            volumeMeasurementUnit.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(metricWeightMeasurementUnit);
            dataContext.MeasurementUnitRepository.Add(volumeMeasurementUnit);
            dataContext.MeasurementUnitRepository.Add(imperialWeightMeasurementUnit);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MeasurementUnit> actualResult = newContext.MeasurementUnitRepository.GetAll(MeasurementUnitType.Weight);
            Collection<MeasurementUnit> expectedResult = new Collection<MeasurementUnit>(newContext.EntityContext.MeasurementUnitSet.Where(m => m.Type == MeasurementUnitType.Weight && !m.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all measurement units by type");
            foreach (MeasurementUnit measurementUnit in expectedResult)
            {
                MeasurementUnit correspondingMeasurementUnit = actualResult.FirstOrDefault(m => m.Guid == measurementUnit.Guid);
                if (correspondingMeasurementUnit == null)
                {
                    Assert.Fail("Wrong measurement units were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetAll(MeasurementUnitScale unitsScale) method.
        /// </summary>
        [TestMethod]
        public void GetAllMeasurementUnitsByScale()
        {
            MeasurementUnit metricWeightScaleMeasurementUnit = new MeasurementUnit();
            metricWeightScaleMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            metricWeightScaleMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            metricWeightScaleMeasurementUnit.ConversionRate = 2;
            metricWeightScaleMeasurementUnit.Type = MeasurementUnitType.Weight;
            metricWeightScaleMeasurementUnit.ScaleID = MeasurementUnitScale.MetricWeightScale;
            metricWeightScaleMeasurementUnit.ScaleFactor = 1;

            MeasurementUnit imperialWeightScaleMeasurementUnit = new MeasurementUnit();
            imperialWeightScaleMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            imperialWeightScaleMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            imperialWeightScaleMeasurementUnit.ConversionRate = 2;
            imperialWeightScaleMeasurementUnit.Type = MeasurementUnitType.Weight;
            imperialWeightScaleMeasurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            imperialWeightScaleMeasurementUnit.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(metricWeightScaleMeasurementUnit);
            dataContext.MeasurementUnitRepository.Add(imperialWeightScaleMeasurementUnit);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MeasurementUnit> actualResult = newContext.MeasurementUnitRepository.GetAll(MeasurementUnitScale.MetricWeightScale);
            Collection<MeasurementUnit> expectedResult = new Collection<MeasurementUnit>(newContext.EntityContext.MeasurementUnitSet.Where(m => !m.IsReleased && m.ScaleID == MeasurementUnitScale.MetricWeightScale).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all measurement units by scale");
            foreach (MeasurementUnit measurementUnit in expectedResult)
            {
                MeasurementUnit correspondingMeasurementUnit = actualResult.FirstOrDefault(m => m.Guid == measurementUnit.Guid);
                if (correspondingMeasurementUnit == null)
                {
                    Assert.Fail("Wrong measurement units were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetBaseMeasurementUnits() method.
        /// </summary>
        [TestMethod]
        public void GetBaseMeasurementUnitsTest()
        {
            MeasurementUnit metricWeightMeasurementUnit1 = new MeasurementUnit();
            metricWeightMeasurementUnit1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            metricWeightMeasurementUnit1.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            metricWeightMeasurementUnit1.ConversionRate = 2;
            metricWeightMeasurementUnit1.Type = MeasurementUnitType.Weight;
            metricWeightMeasurementUnit1.ScaleID = MeasurementUnitScale.MetricWeightScale;
            metricWeightMeasurementUnit1.ScaleFactor = 1;

            MeasurementUnit metricWeightMeasurementUnit2 = new MeasurementUnit();
            metricWeightMeasurementUnit2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            metricWeightMeasurementUnit2.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            metricWeightMeasurementUnit2.ConversionRate = 2;
            metricWeightMeasurementUnit2.Type = MeasurementUnitType.Weight;
            metricWeightMeasurementUnit2.ScaleID = MeasurementUnitScale.MetricWeightScale;
            metricWeightMeasurementUnit2.ScaleFactor = 2;

            MeasurementUnit imperialWeightMeasurementUnit = new MeasurementUnit();
            imperialWeightMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            imperialWeightMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            imperialWeightMeasurementUnit.ConversionRate = 2;
            imperialWeightMeasurementUnit.Type = MeasurementUnitType.Weight;
            imperialWeightMeasurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            imperialWeightMeasurementUnit.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(metricWeightMeasurementUnit1);
            dataContext.MeasurementUnitRepository.Add(metricWeightMeasurementUnit2);
            dataContext.MeasurementUnitRepository.Add(imperialWeightMeasurementUnit);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MeasurementUnit> actualResult = newContext.MeasurementUnitRepository.GetBaseMeasurementUnits();
            Collection<MeasurementUnit> expectedResult = new Collection<MeasurementUnit>(newContext.EntityContext.MeasurementUnitSet.Where(m => m.ScaleFactor == 1 && !m.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all base measurement units");
            foreach (MeasurementUnit measurementUnit in expectedResult)
            {
                MeasurementUnit correspondingMeasurementUnit = actualResult.FirstOrDefault(m => m.Guid == measurementUnit.Guid);
                if (correspondingMeasurementUnit == null)
                {
                    Assert.Fail("Wrong measurement units were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetBaseMeasurementUnit(MeasurementUnitScale scale) method.
        /// </summary>
        [TestMethod]
        public void GetBaseMeasurementUnitTest()
        {
            MeasurementUnit imperialWeightMeasurementUnit = new MeasurementUnit();
            imperialWeightMeasurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            imperialWeightMeasurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            imperialWeightMeasurementUnit.ConversionRate = 2;
            imperialWeightMeasurementUnit.Type = MeasurementUnitType.Weight;
            imperialWeightMeasurementUnit.ScaleID = MeasurementUnitScale.ImperialWeightScale;
            imperialWeightMeasurementUnit.ScaleFactor = 1;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(imperialWeightMeasurementUnit);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit actualResult = newContext.MeasurementUnitRepository.GetBaseMeasurementUnit(MeasurementUnitScale.TimeScale);
            MeasurementUnit expectedResult = newContext.EntityContext.MeasurementUnitSet.
                FirstOrDefault(m => m.ScaleID == MeasurementUnitScale.TimeScale && m.ScaleFactor == 1 && !m.IsReleased);

            Assert.AreEqual<Guid>(expectedResult.Guid, actualResult.Guid, "Failed to get the base measurement unit from a given scale");
        }

        /// <summary>
        /// A test for GetByName(string unitName) method using a valid measurement unit as input data.
        /// </summary>
        [TestMethod]
        public void GetByNameValidMeasurementUnit()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            measurementUnit.Name = EncryptionManager.Instance.GenerateRandomString(9, true);
            measurementUnit.Symbol = EncryptionManager.Instance.GenerateRandomString(2, true);
            measurementUnit.ConversionRate = 2;
            measurementUnit.Type = MeasurementUnitType.Volume;
            measurementUnit.ScaleID = MeasurementUnitScale.MetricVolumeScale;
            measurementUnit.ScaleFactor = 2;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MeasurementUnitRepository.Add(measurementUnit);
            dataContext.SaveChanges();

            IDataSourceManager newDataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit result = newDataContext.MeasurementUnitRepository.GetByName(measurementUnit.Name);

            Assert.AreEqual<Guid>(measurementUnit.Guid, result.Guid, "Failed to get a measurement unit by name");
        }

        /// <summary>
        /// A test for GetByName(string unitName) using a null measurement unit as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByNameNullMeasurementUnit()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit measurementUnit = null;
            dataContext.MeasurementUnitRepository.GetByName(measurementUnit.Name);
        }

        #endregion Test Method
    }
}
