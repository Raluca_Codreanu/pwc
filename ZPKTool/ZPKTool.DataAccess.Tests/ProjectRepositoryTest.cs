﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ProjectRepository and is intended to contain
    /// unit tests for all public methods from ProjectRepository class.
    /// </summary>
    [TestClass]
    public class ProjectRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectRepositoryTest"/> class.
        /// </summary>
        public ProjectRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddProject method. Create a project and adds the created project into
        /// local context, commits the changes, then checks if the project was added.
        /// </summary>
        [TestMethod()]
        public void AddProjectTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.Number = EncryptionManager.Instance.GenerateRandomString(2, true);
            project.OverheadSettings = new OverheadSetting();

            // Add a media to project.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.Media.Add(media);

            // Add a supplier to project
            project.Customer = new Customer();
            project.Customer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Verifies if the project was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool projectAdded = dataContext2.ProjectRepository.CheckIfExists(project.Guid);

            Assert.IsTrue(projectAdded, "Failed to create a project.");
        }

        /// <summary>
        /// Creates a project, updates it and checks if project was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateProjectTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.Number = EncryptionManager.Instance.GenerateRandomString(2, true);
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            project.Name += " - Updated";
            dataContext1.SaveChanges();

            // Create a new context in order to get the updated project from db.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project projectFromDB = dataContext2.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == project.Guid);

            // Verify if the project was updated in database too, by comparing the updated entity with the entity stored in data base.
            Assert.AreEqual<string>(project.Name, projectFromDB.Name, "Failed to update a project.");
        }

        /// <summary>
        /// Creates a project, deletes it and checks if project was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteProjectTest()
        {
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = project.Customer.Guid.ToString();

            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 1, 1, 1, 1 };
            media.Size = media.Content.Length;
            project.Media.Add(media);

            // Add an assembly to the project
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(media.Copy());
            project.Assemblies.Add(assembly);

            ProcessStep stepOfAssembly = new AssemblyProcessStep();
            stepOfAssembly.Name = stepOfAssembly.Guid.ToString();
            stepOfAssembly.Accuracy = ProcessCalculationAccuracy.Estimated;
            stepOfAssembly.CycleTime = 100;
            stepOfAssembly.ProcessTime = 20;
            stepOfAssembly.PartsPerCycle = 5;
            stepOfAssembly.ScrapAmount = 20;
            stepOfAssembly.Media = media.Copy();
            assembly.Process = new Process();
            assembly.Process.Steps.Add(stepOfAssembly);

            // Add a machine to process step.
            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.Media = media.Copy();
            machine1.Manufacturer = new Manufacturer();
            machine1.Manufacturer.Name = machine1.Manufacturer.Guid.ToString();
            stepOfAssembly.Machines.Add(machine1);

            // Add a consumable to process step.
            Consumable consumable1 = new Consumable();
            consumable1.Name = consumable1.Guid.ToString();
            consumable1.Manufacturer = new Manufacturer();
            consumable1.Manufacturer.Name = consumable1.Manufacturer.Guid.ToString();
            stepOfAssembly.Consumables.Add(consumable1);

            // Add a die to process step.
            Die die1 = new Die();
            die1.Name = die1.Guid.ToString();
            die1.Media = media.Copy();
            die1.Manufacturer = new Manufacturer();
            die1.Manufacturer.Name = die1.Manufacturer.Guid.ToString();
            stepOfAssembly.Dies.Add(die1);

            // Add a commodity to process step
            Commodity commodity1 = new Commodity();
            commodity1.Name = commodity1.Guid.ToString();
            commodity1.Media = media.Copy();
            commodity1.Manufacturer = new Manufacturer();
            commodity1.Manufacturer.Name = commodity1.Manufacturer.Guid.ToString();
            stepOfAssembly.Commodities.Add(commodity1);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation1 = new CycleTimeCalculation();
            calculation1.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation1.TransportTimeFromBuffer = 10;
            calculation1.ToolChangeTime = 10;
            calculation1.TransportClampingTime = 10;
            calculation1.MachiningTime = 10;
            calculation1.ToolChangeTime = 10;
            stepOfAssembly.CycleTimeCalculations.Add(calculation1);

            // Add a part to the Project
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Media.Add(media.Copy());
            project.Parts.Add(part);

            // Add a raw material to part.
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.Manufacturer = new Manufacturer();
            material.Manufacturer.Name = material.Manufacturer.Guid.ToString();
            material.Media = media.Copy();
            part.RawMaterials.Add(material);

            // Add a commodity to part.
            Commodity commodity2 = new Commodity();
            commodity2.Name = commodity2.Guid.ToString();
            commodity2.Manufacturer = new Manufacturer();
            commodity2.Manufacturer.Name = commodity2.Manufacturer.Guid.ToString();
            commodity2.Media = media.Copy();
            part.Commodities.Add(commodity2);

            // Add process and a process step to part.
            part.Process = new Process();
            ProcessStep stepOfPart = new PartProcessStep();
            stepOfPart.Name = stepOfPart.Guid.ToString();
            stepOfPart.Media = media.Copy();
            part.Process.Steps.Add(stepOfPart);

            // Add a machine to process step.
            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = new Manufacturer();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            machine2.Media = media.Copy();
            stepOfPart.Machines.Add(machine2);

            // Add a consumable to process step.
            Consumable consumable2 = new Consumable();
            consumable2.Name = consumable2.Guid.ToString();
            consumable2.Manufacturer = new Manufacturer();
            consumable2.Manufacturer.Name = consumable2.Manufacturer.Guid.ToString();
            stepOfPart.Consumables.Add(consumable2);

            // Add a die to process step.
            Die die2 = new Die();
            die2.Name = die2.Guid.ToString();
            die2.Manufacturer = new Manufacturer();
            die2.Manufacturer.Name = die2.Manufacturer.Guid.ToString();
            die2.Media = media.Copy();
            stepOfPart.Dies.Add(die2);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation2 = new CycleTimeCalculation();
            calculation2.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation2.TransportTimeFromBuffer = 10;
            calculation2.ToolChangeTime = 10;
            calculation2.TransportClampingTime = 10;
            calculation2.MachiningTime = 10;
            calculation2.ToolChangeTime = 10;
            stepOfPart.CycleTimeCalculations.Add(calculation2);

            // Get the entities that belong to project unless the shared ones in order to check that the project was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(project);
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Delete the project
            dataContext1.ProjectRepository.RemoveAll(project);
            dataContext1.SaveChanges();

            // Verify that the project was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetProjectForEdit(Guid projectGuid) method.
        /// </summary>
        [TestMethod()]
        public void GetProjectForEditTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Customer customer = new Customer();
            customer.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = customer;

            OverheadSetting overheadSettings = new OverheadSetting();
            project.OverheadSettings = overheadSettings;

            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            project.SetOwner(user1);
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(13, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(13, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            project.ResponsibleCalculator = user2;
            user2.Roles = Role.Admin;

            User user3 = new User();
            user3.Name = EncryptionManager.Instance.GenerateRandomString(12, true);
            user3.Username = EncryptionManager.Instance.GenerateRandomString(12, true);
            user3.Password = EncryptionManager.Instance.HashSHA256("Password.", user3.Salt, user3.Guid.ToString());
            project.ProjectLeader = user3;
            user3.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetProjectForEdit(project.Guid);

            Assert.IsNotNull(result, "Failed to get a project for edit");

            Assert.IsTrue(
                result.Customer != null && result.Customer.Guid == customer.Guid,
                "The customer of the project was not included");

            Assert.IsTrue(
                result.OverheadSettings != null && result.OverheadSettings.Guid == overheadSettings.Guid,
                "The overhead settings of the project was not included");

            Assert.IsTrue(
                result.Owner != null && result.Owner.Guid == user1.Guid,
                "The owner of the project was not included");

            Assert.IsTrue(
                result.ResponsibleCalculator != null && result.ResponsibleCalculator.Guid == user2.Guid,
                "The responsible calculator of the project was not included");

            Assert.IsTrue(
                result.ProjectLeader != null && result.ProjectLeader.Guid == user3.Guid,
                "The leader of the project was not included");
        }

        /// <summary>
        /// Tests GetProjectForTrashBinView(Guid projectId) method using valid data.
        /// </summary>
        [TestMethod]
        public void GetProjectForViewTest()
        {
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            // Add a Supplier to the project
            Customer customer = new Customer();
            customer.Name = "Customer of Project" + DateTime.Now.Ticks;
            project.Customer = customer;

            // Add a ProjectLeader to the project
            User projectLeader = new User();
            projectLeader.Name = "Project Leader" + DateTime.Now.Ticks;
            projectLeader.Username = "Project Leader" + DateTime.Now.Ticks;
            projectLeader.Password = EncryptionManager.Instance.HashSHA256("ProjectLeader.", projectLeader.Salt, projectLeader.Guid.ToString());
            project.ProjectLeader = projectLeader;
            projectLeader.Roles = Role.Admin;

            // Add a ResponsibleCalculator to the project
            User responsibleCalculator = new User();
            responsibleCalculator.Name = "Responsible Calculator" + DateTime.Now.Ticks;
            responsibleCalculator.Username = "Responsible Calculator" + DateTime.Now.Ticks;
            responsibleCalculator.Password = EncryptionManager.Instance.HashSHA256("ResponsibleCalculator.", responsibleCalculator.Salt, responsibleCalculator.Guid.ToString());
            project.ResponsibleCalculator = responsibleCalculator;
            responsibleCalculator.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project projectForView = dataContext2.ProjectRepository.GetProjectForView(project.Guid);

            // Verifies that the project was returned and it is the searched one
            Assert.IsTrue(
                projectForView != null && project.Guid == projectForView.Guid,
                "Failed to return the project for view");

            // Verifies that the customer of the project was loaded
            Assert.IsTrue(
                projectForView.Customer != null && customer.Guid == projectForView.Customer.Guid,
                "Project's customer was not included");

            // Verifies that the Leader of the project was loaded
            Assert.IsTrue(
                projectForView.ProjectLeader != null && projectLeader.Guid == projectForView.ProjectLeader.Guid,
                "The Leader of the project was not included");

            // Verifies that the ResponsibleCalculator of the project was loaded 
            Assert.IsTrue(
                projectForView.ResponsibleCalculator != null && responsibleCalculator.Guid == projectForView.ResponsibleCalculator.Guid,
                "The responsible calculator of the project was not included");

            // Verifies that the OverheadSettings of the project were loaded
            Assert.IsTrue(
                projectForView.OverheadSettings != null && project.OverheadSettings.Guid == projectForView.OverheadSettings.Guid,
                "The OverheadSettings of the project was not included");
        }

        /// <summary>
        /// A test for GetProjectForProjectsTree(Guid projectId) method.
        /// </summary>
        [TestMethod()]
        public void GetProjectForProjectsTreeTest()
        {
            Project project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.IsDeleted = true;

            Project project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            OverheadSetting overheadSettings = new OverheadSetting();
            project2.OverheadSettings = overheadSettings;

            Customer customer = new Customer();
            customer.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            project2.Customer = customer;

            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder.Projects.Add(project2);

            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(12, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(12, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            folder.SetOwner(user);
            user.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.ProjectRepository.Add(project1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Retrieve a deleted project for projects tree
            Project result1 = dataContext2.ProjectRepository.GetProjectForProjectsTree(project1.Guid);

            // Verifies that the deleted project was not returned
            Assert.IsNull(result1, "A deleted project was returned for projects tree");

            // Get a valid project for projects tree
            Project result2 = dataContext2.ProjectRepository.GetProjectForProjectsTree(project2.Guid);

            Assert.IsTrue(
                result2 != null && result2.Guid == project2.Guid,
                "Failed to return a project for ProjectsTree");

            Assert.IsTrue(
                result2.Customer != null && result2.Customer.Guid == customer.Guid,
                "Project's customer was not included");

            Assert.IsTrue(
                result2.OverheadSettings != null && result2.OverheadSettings.Guid == overheadSettings.Guid,
                "The project's overhead settings were not included");

            Assert.IsTrue(
                result2.ProjectFolder != null && result2.ProjectFolder.Guid == folder.Guid,
                "The parent folder was not included");

            Assert.IsTrue(
                result2.Owner != null && result2.Owner.Guid == user.Guid,
                "The project's owner was not included");
        }

        /// <summary>
        /// A test for GetProjectsForWelcomeView(Guid userGuid) method.
        /// </summary>
        [TestMethod()]
        public void GetProjectsForWelcomeViewTest()
        {
            User user1 = new User();
            user1.Username = EncryptionManager.Instance.GenerateRandomString(11, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = EncryptionManager.Instance.GenerateRandomString(11, true);
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(12, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Name = EncryptionManager.Instance.GenerateRandomString(12, true);
            user2.Roles = Role.Admin;

            Project project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(user1);

            Project project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            project2.OverheadSettings = new OverheadSetting();
            project2.IsDeleted = true;
            project2.SetOwner(user1);

            Project project3 = new Project();
            project3.Name = EncryptionManager.Instance.GenerateRandomString(16, true);
            project3.OverheadSettings = new OverheadSetting();
            project3.SetOwner(user2);

            Project project4 = new Project();
            project4.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            project4.OverheadSettings = new OverheadSetting();
            project4.SetOwner(user1);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project1);
            dataContext1.ProjectRepository.Add(project2);
            dataContext1.ProjectRepository.Add(project3);
            dataContext1.ProjectRepository.Add(project4);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Project> actualResult = dataContext2.ProjectRepository.GetProjectsForWelcomeView(user1.Guid, Guid.Empty,40);
            Collection<Project> allResults = new Collection<Project>(dataContext2.EntityContext.ProjectSet.Where(p => p.Owner != null && p.Owner.Guid == user1.Guid && p.IsDeleted == false).ToList());

            Assert.IsTrue(actualResult.Count <= allResults.Count, "Failed to get maximum number of projects of a specified user for welcome view");
            foreach (Project project in actualResult)
            {
                if (!allResults.Any(p => p.Guid == project.Guid))
                {
                    Assert.Fail("Wrong projects were returned for welcome view");
                    break;
                }
            }
        }

        /// <summary>
        /// A test for GetProjectsForProjectsTree(Guid ownerId, Guid folderId) method.
        /// </summary>
        [TestMethod()]
        public void GetProjectsForProjectsTree()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(11, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(11, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a project to 'folder'
            Project project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.Customer = new Customer();
            project1.Customer.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            folder.Projects.Add(project1);

            // Add a deleted project to 'folder'
            Project project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(13, true);
            project2.OverheadSettings = new OverheadSetting();
            project2.Customer = new Customer();
            project2.Customer.Name = EncryptionManager.Instance.GenerateRandomString(13, true);
            folder.Projects.Add(project2);
            folder.SetOwner(user1);

            // Create a project which belongs to 'user1'
            Project project3 = new Project();
            project3.Name = EncryptionManager.Instance.GenerateRandomString(16, true);
            project3.OverheadSettings = new OverheadSetting();
            project3.Customer = new Customer();
            project3.Customer.Name = EncryptionManager.Instance.GenerateRandomString(16, true);
            project3.SetOwner(user1);

            // Create a project which is deleted and belongs to 'user1'
            Project project4 = new Project();
            project4.Name = EncryptionManager.Instance.GenerateRandomString(12, true);
            project4.OverheadSettings = new OverheadSetting();
            project4.Customer = new Customer();
            project4.Customer.Name = EncryptionManager.Instance.GenerateRandomString(12, true);
            project4.SetOwner(user1);

            // Create a project which belongs to 'user2'
            Project project5 = new Project();
            project5.Name = EncryptionManager.Instance.GenerateRandomString(9, true);
            project5.OverheadSettings = new OverheadSetting();
            project5.Customer = new Customer();
            project5.Customer.Name = EncryptionManager.Instance.GenerateRandomString(9, true);
            project5.SetOwner(user2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.ProjectRepository.Add(project3);
            dataContext1.ProjectRepository.Add(project4);
            dataContext1.ProjectRepository.Add(project5);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            // Retrieve the projects from 'folder' belonging to 'user1'
            Collection<Project> actualResult1 = dataContext2.ProjectRepository.GetProjectsForProjectsTree(user1.Guid, folder.Guid);
            var query1 = from project in dataContext2.EntityContext.ProjectSet
                              .Include(p => p.Customer)
                              .Include(p => p.OverheadSettings)
                              .Include(p => p.Owner)
                         where project.Owner.Guid == user1.Guid && project.ProjectFolder.Guid == folder.Guid && !project.IsDeleted
                         select project;

            List<Project> expectedResult1 = query1.ToList();

            Assert.AreEqual<int>(expectedResult1.Count, actualResult1.Count, "Failed to get all projects for Projects Tree of a corresponding user from a corresponding folder");
            foreach (Project project in expectedResult1)
            {
                Project correspondingProject = actualResult1.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("The returned project is wrong");
                }

                Assert.IsTrue(project.Customer.Guid == correspondingProject.Customer.Guid, "The project's customer was not included");
                Assert.IsTrue(project.OverheadSettings.Guid == correspondingProject.OverheadSettings.Guid, "The project's overhead settings were not included");
                Assert.IsTrue(project.Owner.Guid == correspondingProject.Owner.Guid, "The project's owner was to included");
            }

            // Retrieve the projects belonging to user1 and don't have a parent folder
            Collection<Project> actualResult2 = dataContext2.ProjectRepository.GetProjectsForProjectsTree(user1.Guid, Guid.Empty);
            var query2 = from project in dataContext2.EntityContext.ProjectSet
                              .Include(p => p.Customer)
                              .Include(p => p.OverheadSettings)
                              .Include(p => p.Owner)
                         where project.Owner.Guid == user1.Guid && project.ProjectFolder == null && !project.IsDeleted
                         select project;

            List<Project> expectedResult2 = query2.ToList();

            Assert.AreEqual<int>(expectedResult2.Count, actualResult2.Count, "Failed to get all projects for Projects Tree of a corresponding user");
            foreach (Project project in expectedResult2)
            {
                Project correspondingProject = actualResult2.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("The returned project is wrong");
                }

                Assert.IsTrue(project.Customer.Guid == correspondingProject.Customer.Guid, "The project's customer was not included");
                Assert.IsTrue(project.OverheadSettings.Guid == correspondingProject.OverheadSettings.Guid, "The project's overhead settings were not included");
                Assert.IsTrue(project.Owner.Guid == correspondingProject.Owner.Guid, "The project's owner was not included");
            }
        }

        /// <summary>
        /// Tests GetReleasedProjectsForProjectsTree() method.
        /// </summary>
        [TestMethod()]
        public void GetReleasedProjectsForProjectsTreeTest()
        {
            Project releasedProject = new Project();
            releasedProject.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            releasedProject.IsReleased = true;
            releasedProject.OverheadSettings = new OverheadSetting();

            Project unreleasedProject = new Project();
            unreleasedProject.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            unreleasedProject.OverheadSettings = new OverheadSetting();

            Project deletedProject = new Project();
            deletedProject.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            deletedProject.OverheadSettings = new OverheadSetting();
            deletedProject.IsDeleted = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(releasedProject);
            dataContext1.ProjectRepository.Add(unreleasedProject);
            dataContext1.ProjectRepository.Add(deletedProject);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Project> actualResult = dataContext2.ProjectRepository.GetReleasedProjectsForProjectsTree(Guid.Empty);
            Collection<Project> expectedResult = new Collection<Project>(dataContext2.EntityContext.ProjectSet.Where(p => p.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all released projects");
            foreach (Project project in expectedResult)
            {
                if (actualResult.FirstOrDefault(p => p.Guid == project.Guid) == null)
                {
                    Assert.Fail("A wrong project was returned");
                }
            }
        }

        /// <summary>
        /// A test for GetOtherUserProjects(Guid userId, Guid leaderOrCalculatorId) method.
        /// </summary>
        [TestMethod]
        public void GetOtherUserProjectsTest()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user2.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(13, true);

            // Add a project to the created folder. The project's leader is "user1".
            Project project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.ProjectLeader = user1;
            project1.OverheadSettings = new OverheadSetting();
            project1.Customer = new Customer();
            project1.Customer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            folder.Projects.Add(project1);

            // Add a project to the created folder. The project's calculator is "user1".
            Project project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(11, true);
            project2.ResponsibleCalculator = user1;
            project2.OverheadSettings = new OverheadSetting();
            project2.Customer = new Customer();
            project2.Customer.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder.Projects.Add(project2);

            // Create a deleted project into the folder. "user1" is the leader and the calculator of created project.
            Project project3 = new Project();
            project3.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            project3.OverheadSettings = new OverheadSetting();
            project3.ProjectLeader = user1;
            project3.ResponsibleCalculator = user1;
            project3.IsDeleted = true;

            // Create a project into the folder that has no leader and calculator.
            Project project4 = new Project();
            project4.Name = EncryptionManager.Instance.GenerateRandomString(7, true);
            project4.OverheadSettings = new OverheadSetting();
            project4.Customer = new Customer();
            project4.Customer.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder.Projects.Add(project4);
            folder.SetOwner(user2);

            // Create a project and set the owner to "user1" and set the leader and the calculator to "user2".
            Project project5 = new Project();
            project5.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project5.OverheadSettings = new OverheadSetting();
            project5.ProjectLeader = user1;
            project5.ResponsibleCalculator = user1;
            project5.SetOwner(user1);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.ProjectRepository.Add(project5);
            dataContext1.SaveChanges();

            // Retrieve the projects of "user2" having the leader or calculator set to "user1"
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Project> actualResult1 = dataContext2.ProjectRepository.GetOtherUserProjects(user2.Guid, user1.Guid);
            Collection<Project> expectedResult1 = new Collection<Project>(
                dataContext2.EntityContext.ProjectSet.Include(p => p.Customer)
                                                .Include(p => p.OverheadSettings)
                                                .Include(p => p.ProjectFolder)
                                                .Where(p => p.Owner.Guid == user2.Guid &&
                                                      !p.IsDeleted &&
                                                      (p.ProjectLeader.Guid == user1.Guid ||
                                                       p.ResponsibleCalculator.Guid == user1.Guid)).ToList());

            Assert.AreEqual<int>(expectedResult1.Count, actualResult1.Count, "Failed to get all projects of a specified user having a specified leader or calculator");
            foreach (Project project in expectedResult1)
            {
                Project correspondingProject = actualResult1.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Wrong project was returned");
                }

                Assert.IsTrue(project.Customer.Guid == correspondingProject.Customer.Guid, "The project's customer was not included");
                Assert.IsTrue(project.OverheadSettings.Guid == correspondingProject.OverheadSettings.Guid, "The project's overhead settings were not included");
                Assert.IsTrue(project.ProjectFolder.Guid == correspondingProject.ProjectFolder.Guid, "The project's folder was not included");
            }

            // Retrieve the projects of "user2" having any leader or calculator
            Collection<Project> actualResult2 = dataContext2.ProjectRepository.GetOtherUserProjects(user2.Guid, Guid.Empty);
            Collection<Project> expectedResult2 = new Collection<Project>(
                dataContext2.EntityContext.ProjectSet.Include(p => p.Customer)
                                                .Include(p => p.OverheadSettings)
                                                .Include(p => p.ProjectFolder)
                                                .Where(p => p.Owner.Guid == user2.Guid &&
                                                       !p.IsDeleted).ToList());

            Assert.AreEqual<int>(expectedResult2.Count, actualResult2.Count, "Failed to get all projects of a specified user having any leader or calculator");
            foreach (Project project in expectedResult1)
            {
                Project correspondingProject = actualResult1.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Wrong project was returned");
                }

                Assert.IsTrue(project.Customer.Guid == correspondingProject.Customer.Guid, "The project's customer was not included");
                Assert.IsTrue(project.OverheadSettings.Guid == correspondingProject.OverheadSettings.Guid, "The project's overhead settings were not included");
                Assert.IsTrue(project.ProjectFolder.Guid == correspondingProject.ProjectFolder.Guid, "The project's folder was not included");
            }
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method using a valid project as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidProject()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(project);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a project entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(subassembly);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of an assembly entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method using a valid part as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidPart()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(part);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a part entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidMachine()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(17, true);
            subassembly.Process = new Process();
            subassembly.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(machine);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a valid machine");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid raw material as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidRawMaterial()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.RawMaterials.Add(rawMaterial);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(rawMaterial);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a raw material entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid consumable as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidConsumable()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(17, true);
            subassembly.Process = new Process();
            subassembly.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(9, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(consumable);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a consumable entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidCommodity()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(17, true);
            subassembly.Process = new Process();
            subassembly.Process.Steps.Add(step);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Commodities.Add(commodity);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(commodity);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a commodity entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid die as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidDie()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(17, true);
            subassembly.Process = new Process();
            subassembly.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(die);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a die entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid process step as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidProcessStep()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(17, true);
            subassembly.Process = new Process();
            subassembly.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(step);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a process step entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a valid process as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfValidProcess()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Process process = new Process();
            subassembly.Process = process;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetParentProject(process);

            Assert.AreEqual<Guid>(project.Guid, result.Guid, "Failed to get the parent project of a process entity");
        }

        /// <summary>
        /// A test for GetParentProject(object entity, bool noTracking = false) method 
        /// using a null entity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentProjectOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext.ProjectRepository.GetParentProject(null);

            Assert.IsNull(result, "A parent project was returned for a null entity");
        }

        /// <summary>
        /// A test for GetProjectTopLevelChildren(Guid projectId) method 
        /// </summary>
        [TestMethod]
        public void GetProjectTopLevelChildren()
        {
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = project.Customer.Guid.ToString();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            User user = new User();
            user.Name = user.Guid.ToString();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            project.SetOwner(user);
            project.ProjectLeader = user;
            project.ResponsibleCalculator = user;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project result = dataContext2.ProjectRepository.GetProjectIncludingTopLevelChildren(project.Guid);

            // Verify that the top level children of project were loaded
            Assert.IsNotNull(result, "Failed to get the project.");
            Assert.IsNotNull(result.Customer, "Failed to load the Customer.");
            Assert.IsNotNull(result.OverheadSettings, "Failed to load the OverheadSettings.");
            Assert.IsNotNull(result.Owner, "Failed to load the project's Owner.");
            Assert.IsNotNull(result.ProjectLeader, "Failed to load the ProjectLeader.");
            Assert.IsNotNull(result.ResponsibleCalculator, "Failed to load the ResponsibleCalculator.");
            Assert.IsNotNull(result.Assemblies.FirstOrDefault(), "Failed to load the Assemblies.");
            Assert.IsNotNull(result.Parts.FirstOrDefault(), "Failed to load the Parts.");
        }

        #endregion Test Methods
    }
}
