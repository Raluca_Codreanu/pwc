﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for AssemblyRepository and is intended to contain
    /// unit tests for all public methods from AssemblyRepository class.
    /// </summary>
    [TestClass]
    public class AssemblyRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyRepositoryTest"/> class.
        /// </summary>
        public AssemblyRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddAssembly method. Create an assembly and adds the created assembly into
        /// local context, commits the changes, then checks if assembly was added.
        /// </summary>
        [TestMethod]
        public void AddAssemblyTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            assembly.BatchSizePerYear = 10;
            assembly.IsExternal = true;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            // Add a Manufacturer to assembly
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Manufacturer = manufacturer;

            // Add media to assembly.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Media.Add(media);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Verifies if assembly was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);            
            Assembly result = dataContext2.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == assembly.Guid);

            Assert.IsNotNull(result, "Failed to create an assembly.");
        }

        /// <summary>
        /// Creates an assembly, updates it and checks if the assembly was updated.
        /// </summary>
        [TestMethod]
        public void UpdateAssemblyTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            assembly.BatchSizePerYear = 10;
            assembly.IsExternal = true;          
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            // Add a Manufacturer to assembly
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Manufacturer = manufacturer;

            // Add media to assembly.
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Media.Add(media);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            assembly.Name += " - Updated";
            dataContext1.SaveChanges();

            // Create a new context in order to get the updated assembly from db.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assemblyFromDB = dataContext2.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == assembly.Guid);

            // Verify if assembly was updated in database too, by comparing the updated entity with the entity stored in the data base.
            Assert.AreEqual<string>(assembly.Name, assemblyFromDB.Name, "Failed to update an assembly.");
        }

        /// <summary>
        /// Creates an assembly, deletes it and checks if assembly was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteAssemblyTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();

            Media assemblyMedia = new Media();
            assemblyMedia.Type = (short)MediaType.Image;
            assemblyMedia.Content = new byte[] { 1, 1, 1, 1, 1 };
            assemblyMedia.Size = assemblyMedia.Content.Length;
            assembly.Media.Add(assemblyMedia);

            assembly.Process = new Process();
            ProcessStep step = new AssemblyProcessStep();
            step.Name = step.Guid.ToString();
            step.Accuracy = ProcessCalculationAccuracy.Estimated;
            step.CycleTime = 100;
            step.ProcessTime = 20;
            step.PartsPerCycle = 5;
            step.ScrapAmount = 20;
            step.Media = assemblyMedia.Copy();
            assembly.Process.Steps.Add(step);

            // Add a machine to process step.
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.Media = assemblyMedia.Copy();
            machine.Manufacturer = new Manufacturer();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            step.Machines.Add(machine);

            // Add a consumable to process step.
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step.Consumables.Add(consumable);

            // Add a die to process step.
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Media = assemblyMedia.Copy();
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            step.Dies.Add(die);

            // Add a commodity to process step
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Media = assemblyMedia.Copy();
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            step.Commodities.Add(commodity);

            // Add a cycle time calculation to process step
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            calculation.TransportTimeFromBuffer = 10;
            calculation.ToolChangeTime = 10;
            calculation.TransportClampingTime = 10;
            calculation.MachiningTime = 10;
            calculation.ToolChangeTime = 10;
            step.CycleTimeCalculations.Add(calculation);

            // Add a Part
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = new Manufacturer();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part);

            // Add a media to the part
            Media partMedia = assemblyMedia.Copy();
            part.Media.Add(partMedia);

            // Add a Subassembly
            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.Manufacturer = new Manufacturer();
            subassembly.Manufacturer.Name = subassembly.Manufacturer.Guid.ToString();
            assembly.Subassemblies.Add(subassembly);

            // Add a media to Subassembly
            Media subassemblyMedia = assemblyMedia.Copy();
            subassembly.Media.Add(subassemblyMedia);

            // Add an AssemblyAmount
            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Amount = 10;
            assemblyAmount.Assembly = subassembly;
            step.AssemblyAmounts.Add(assemblyAmount);

            // Add a PartAmount
            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Amount = 9;
            partAmount.Part = part;
            step.PartAmounts.Add(partAmount);

            // Get the entities that belong to assembly unless the shared ones in order to check that the assembly was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(assembly);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Delete the created assembly
            dataContext1.AssemblyRepository.RemoveAll(assembly);
            dataContext1.SaveChanges();

            // Verify that the assembly was permanently deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid assembly
        /// </summary>
        [TestMethod]
        public void GetParentProjectIdOfValidAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.AssemblyRepository.GetParentProjectId(assembly);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project id of an assembly entity");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using an assembly that doesn't have a parent project
        /// </summary>
        [TestMethod]
        public void GetParentProjectGuidOfAssemblyWithoutParentProject()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.IsMasterData = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext2.AssemblyRepository.GetParentProjectId(assembly);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for an assembly that doesn't have a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null assembly.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext.AssemblyRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid assembly
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyIdOfValidAssembly()
        {
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            parentAssembly.Subassemblies.Add(assembly);
            parentAssembly.SetIsMasterData(true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(parentAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.AssemblyRepository.GetTopParentAssemblyId(assembly.Guid);

            Assert.AreEqual<Guid>(parentAssembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of an assembly entity");
        }

        /// <summary>
        /// Tests the GetParentAssemblyGuid method using an assembly that doesn't have a parent assembly
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyGuidOfAssemblyWithoutParentAssembly()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.IsMasterData = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext2.AssemblyRepository.GetTopParentAssemblyId(assembly.Guid);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "A parent assembly was returned for an assembly that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null assembly as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.AssemblyRepository.GetTopParentAssemblyId(Guid.Empty);
        }

        /// <summary>
        /// A test for GetAssembly(Guid assemblyId) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidAssembly()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetAssembly(assembly.Guid);

            Assert.AreEqual<Guid>(assembly.Guid, result.Guid, "Failed to get by id an assembly entity");
        }

        /// <summary>
        /// A test for GetAssembly(Guid assemblyId) method using a null assembly as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assembly = null;
            dataContext.AssemblyRepository.GetAssembly(assembly.Guid);
        }

        /// <summary>
        /// A test for GetAssembly(Guid assemblyId) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void GetAssemblyWithOwnerTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            assembly.Owner = user;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetAssemblyWithOwner(assembly.Guid);

            Assert.AreEqual<Guid>(assembly.Guid, result.Guid, "Failed to get the right assembly");
            Assert.IsTrue(result.Owner != null && result.Owner.Guid == user.Guid, "Failed to get an assembly with owner");
        }

        /// <summary>
        /// A test for GetAssembly(Guid assemblyId) method using a null assembly as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNullAssemblyWithOwnerTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assembly = null;
            dataContext.AssemblyRepository.GetAssemblyWithOwner(assembly.Guid);
        }

        /// <summary>
        /// Tests GetAssemblyForView(Guid assemblyId) method using valid data.
        /// </summary>
        [TestMethod]
        public void GetAssemblyForViewTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            // Add a Manufacturer to the assembly 
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Manufacturer = manufacturer;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Call GetAssemblyForView method for the created assembly
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly assemblyForView = dataContext2.AssemblyRepository.GetAssemblyForView(assembly.Guid);

            // Verifies that the search assembly was returned
            Assert.IsTrue(
                assemblyForView != null && assembly.Guid == assemblyForView.Guid,
                "Failed to return the assembly for view");

            // Verifies if the Manufacturer of assembly was loaded
            Assert.IsTrue(
                assemblyForView.Manufacturer != null && manufacturer.Guid == assemblyForView.Manufacturer.Guid,
                "The Manufacturer of the assembly was not included");

            // Verifies if the OverheadSettings of assembly were loaded
            Assert.IsTrue(
                assemblyForView.OverheadSettings != null && assembly.OverheadSettings.Guid == assemblyForView.OverheadSettings.Guid,
                "The OverheadSettings of the assembly were not included");

            // Verifies if the CountrySettings of assembly were loaded
            Assert.IsTrue(
                assemblyForView.CountrySettings != null && assembly.CountrySettings.Guid == assemblyForView.CountrySettings.Guid,
                "The CountrySettings of the assembly were not included");
        }

        /// <summary>
        /// A test for GetParentAssembly(object entity) method using a valid process as input data.
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyOfValidProcess()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Process process = new Process();
            assembly.Process = process;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetParentAssembly(process);

            Assert.AreEqual<Guid>(assembly.Guid, result.Guid, "Failed to get the parent assembly of a process");
        }

        /// <summary>
        /// A test for GetParentAssembly(object entity) method using a valid part as input data.
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyOfValidPart()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetParentAssembly(part);

            Assert.AreEqual<Guid>(assembly.Guid, result.Guid, "Failed to get the parent assembly of a part entity");
        }

        /// <summary>
        /// A test for GetParentAssembly(object entity) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyOfValidAssembly()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Assembly subassembly = new Assembly();
            subassembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetParentAssembly(subassembly);

            Assert.AreEqual<Guid>(assembly.Guid, result.Guid, "Failed to get the parent assembly of an assembly entity");
        }

        /// <summary>
        /// A test for GetParentAssembly(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        public void GetParentAssemblyOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext.AssemblyRepository.GetParentAssembly(null);

            Assert.IsNull(result, "A parent assembly was returned for a null entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidAssembly()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            Assembly subassembly2 = new Assembly();
            subassembly2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly2.OverheadSettings = new OverheadSetting();
            subassembly2.CountrySettings = new CountrySetting();
            subassembly1.Subassemblies.Add(subassembly2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(subassembly2);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of an assembly entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid part as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidPart()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(part);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a part entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidMachine()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step);

            Machine machine = new Machine();
            machine.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Machines.Add(machine);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(machine);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a machine entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid raw material as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidRawMaterial()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            subassembly1.Parts.Add(part);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.RawMaterials.Add(rawMaterial);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(rawMaterial);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a raw material entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid consumable as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidConsumable()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(consumable);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a consumable entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidCommodity()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Commodities.Add(commodity);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext1.AssemblyRepository.GetTopParentAssembly(commodity);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a commodity entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid die as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidDie()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step);

            Die die = new Die();
            die.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Dies.Add(die);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(die);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a die entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid process step as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidProcessStep()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.Process = new Process();
            subassembly1.Process.Steps.Add(step);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(step);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a process step entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a valid process as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfValidProcess()
        {
            Assembly topAssembly = new Assembly();
            topAssembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            topAssembly.OverheadSettings = new OverheadSetting();
            topAssembly.CountrySettings = new CountrySetting();

            Assembly subassembly1 = new Assembly();
            subassembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            subassembly1.OverheadSettings = new OverheadSetting();
            subassembly1.CountrySettings = new CountrySetting();
            topAssembly.Subassemblies.Add(subassembly1);

            Process process = new Process();
            subassembly1.Process = process;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(topAssembly);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext2.AssemblyRepository.GetTopParentAssembly(process);

            Assert.AreEqual<Guid>(topAssembly.Guid, result.Guid, "Failed to get the top parent assembly of a process entity");
        }

        /// <summary>
        /// A test for GetTopParentAssembly(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        public void GetTopParentAssemblyOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly result = dataContext.AssemblyRepository.GetTopParentAssembly(null);

            Assert.IsNull(result, "A top parent assembly was returned for a null entity");
        }

        /// <summary>
        /// A test for GetProjectAssemblies(Guid projectGuid) method.
        /// </summary>
        [TestMethod]
        public void GetProjectAssembliesTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly1);

            Assembly assembly2 = new Assembly();
            assembly2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Assembly> actualResult = dataContext2.AssemblyRepository.GetProjectAssemblies(project.Guid);
            Collection<Assembly> expectedResult = new Collection<Assembly>(dataContext2.EntityContext.AssemblySet.Where(a => a.Project != null && a.Project.Guid == project.Guid).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get the assemblies of a project");
            foreach (Assembly assembly in expectedResult)
            {
                Assembly correspondingAssembly = actualResult.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Wrong assemblies were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetAllMasterData() method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataAssembliesTest()
        {
            Assembly assembly1 = new Assembly();
            assembly1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            assembly1.IsMasterData = true;

            Assembly assembly2 = new Assembly();
            assembly2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly1);
            dataContext1.AssemblyRepository.Add(assembly2);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Assembly> actualResult = dataContext2.AssemblyRepository.GetAllMasterData();
            Collection<Assembly> expectedResult = new Collection<Assembly>(dataContext2.EntityContext.AssemblySet.Where(a => a.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data assemblies");
            foreach (Assembly assembly in expectedResult)
            {
                Assembly correspondingAssembly = actualResult.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Wrong assemblies were returned");
                }
            }
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid assemblyId, OverheadSetting overheadSettings) method using valid input data.
        /// </summary>
        [TestMethod]
        public void UpdateOverheadSettingsOfValidAssembly()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Assembly subassembly = new Assembly();
            subassembly.Name = subassembly.Guid.ToString();
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            assembly.Subassemblies.Add(subassembly);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            assembly.Parts.Add(part);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = 1;
            overheadSettings.ConsumableOverhead = 2;
            overheadSettings.CommodityOverhead = 3;
            overheadSettings.ExternalWorkOverhead = 4;
            overheadSettings.ManufacturingOverhead = 5;
            overheadSettings.MaterialMargin = 6;
            overheadSettings.ConsumableMargin = 7;
            overheadSettings.CommodityMargin = 8;
            overheadSettings.ExternalWorkMargin = 9;
            overheadSettings.ManufacturingMargin = 10;
            overheadSettings.OtherCostOHValue = 11;
            overheadSettings.PackagingOHValue = 12;
            overheadSettings.SalesAndAdministrationOHValue = 13;
            overheadSettings.LastUpdateTime = DateTime.Now;

            dataContext1.AssemblyRepository.UpdateOverheadSettings(assembly.Guid, overheadSettings);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly updatedAssembly = dataContext2.EntityContext.AssemblySet.Include(a => a.OverheadSettings).FirstOrDefault(a => a.Guid == assembly.Guid);
            Assembly updatedSubassembly = dataContext2.EntityContext.AssemblySet.Include(a => a.OverheadSettings).FirstOrDefault(a => a.Guid == subassembly.Guid);
            Part updatedPart = dataContext2.EntityContext.PartSet.Include(p => p.OverheadSettings).FirstOrDefault(p => p.Guid == part.Guid);
            bool assemblyOHSettingsUpdated = AreEqual(overheadSettings, updatedAssembly.OverheadSettings);
            bool subassemblyOHSettingsUpdated = AreEqual(overheadSettings, updatedSubassembly.OverheadSettings);
            bool partOHSettingsUpdated = AreEqual(overheadSettings, updatedPart.OverheadSettings);

            Assert.IsTrue(assemblyOHSettingsUpdated, "Failed to update the overhead settings of an assembly");
            Assert.IsTrue(subassemblyOHSettingsUpdated, "Failed to update the overhead settings of a subassembly");
            Assert.IsTrue(partOHSettingsUpdated, "Failed to update the overhead settings of assembly's parts");
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid assemblyId, OverheadSetting overheadSettings) method 
        /// using a null assembly as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateOverheadSettingsOfNullAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            OverheadSetting overheadSettings = new OverheadSetting();
            Assembly assembly = null;
            dataContext.AssemblyRepository.UpdateOverheadSettings(assembly.Guid, overheadSettings);
        }

        /// <summary>
        /// A test for UpdateOverheadSettings(Guid assemblyId, OverheadSetting overheadSettings) method 
        /// using null overhead settings as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateOverheadSettingsWithNullOHSettings()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            dataContext.AssemblyRepository.UpdateOverheadSettings(assembly.Guid, null);
        }

        #endregion Test Methods

        #region Helpers

        /// <summary>
        /// Compares two overhead settings
        /// </summary>
        /// <param name="setting">The compared overhead setting</param>
        /// <param name="settingToCompareWith">The overhead setting to compare with</param>
        /// <returns>True if overhead settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(OverheadSetting setting, OverheadSetting settingToCompareWith)
        {
            if (setting == null && settingToCompareWith == null)
            {
                return true;
            }

            if ((setting == null && settingToCompareWith != null) || (setting != null && settingToCompareWith == null))
            {
                return false;
            }

            bool result = setting.MaterialOverhead == settingToCompareWith.MaterialOverhead &&
                          setting.ConsumableOverhead == settingToCompareWith.ConsumableOverhead &&
                          setting.CommodityOverhead == settingToCompareWith.CommodityOverhead &&
                          setting.ExternalWorkOverhead == settingToCompareWith.ExternalWorkOverhead &&
                          setting.ManufacturingOverhead == settingToCompareWith.ManufacturingOverhead &&
                          setting.MaterialMargin == settingToCompareWith.MaterialMargin &&
                          setting.ConsumableMargin == settingToCompareWith.ConsumableMargin &&
                          setting.CommodityMargin == settingToCompareWith.CommodityMargin &&
                          setting.ExternalWorkMargin == settingToCompareWith.ExternalWorkMargin &&
                          setting.ManufacturingMargin == settingToCompareWith.ManufacturingMargin &&
                          setting.OtherCostOHValue == settingToCompareWith.OtherCostOHValue &&
                          setting.PackagingOHValue == settingToCompareWith.PackagingOHValue &&
                          setting.LogisticOHValue == settingToCompareWith.LogisticOHValue &&
                          setting.SalesAndAdministrationOHValue == settingToCompareWith.SalesAndAdministrationOHValue &&
                          string.Equals(
                          setting.LastUpdateTime.GetValueOrDefault().ToShortDateString(),
                          settingToCompareWith.LastUpdateTime.GetValueOrDefault().ToShortDateString());

            return result;
        }

        #endregion Helpers
    }
}
