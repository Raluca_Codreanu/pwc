﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for CommodityRepository and is intended
    /// to contain unit tests for all public methods from CommodityRepository class
    /// </summary>
    [TestClass]
    public class CommodityRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommodityRepositoryTest"/> class.
        /// </summary>
        public CommodityRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddCommodity method. Create a commodity and adds the created commodity into 
        /// local context, then checks if commodity was added. 
        /// </summary>
        [TestMethod()]
        public void AddCommodityTest()
        {
            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            commodity.Media = new Media();
            commodity.Media.Type = (short)MediaType.Image;
            commodity.Media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            commodity.Media.Size = commodity.Media.Content.Length;
            commodity.Media.OriginalFileName = "test.txt";
            commodity.Manufacturer = new Manufacturer();
            commodity.Manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            // Verifies if commodity was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool commodityAdded = newContext.CommodityRepository.CheckIfExists(commodity.Guid);

            Assert.IsTrue(commodityAdded, "Fails to create a commodity.");
        }

        /// <summary>
        /// Creates a commodity, updates it and checks if commodity was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateCommodityTest()
        {
            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            // Update the commodity
            commodity.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated commodity from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Commodity commodityFromDB = newContext.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == commodity.Guid);

            // Verify if commodity was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(commodity.Name, commodityFromDB.Name, "Fails to update a commodity.");
        }

        /// <summary>
        /// Creates a commodity, deletes it and checks if commodity was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteCommodityTest()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            commodity.Manufacturer = manufacturer;

            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 1, 1, 1, 1, 1 };
            media.Size = media.Content.Length;
            media.OriginalFileName = media.Guid.ToString();
            commodity.Media = media;

            // Get the entities that belongs to commodity unless the shared ones in order to check that the commodity was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(commodity);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.CommodityRepository.Add(commodity);
            dataContext1.SaveChanges();

            // Delete the commodity
            dataContext1.CommodityRepository.RemoveAll(commodity);
            dataContext1.SaveChanges();

            // Verify that the commodity was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectGuid method using a valid commodity.
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfValidCommodity()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Commodities.Add(commodity);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Verify if parent project of commodity was found
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.CommodityRepository.GetParentProjectId(commodity);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project of commodity");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a commodity that doesn't have a parent.
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfCommodityWithoutParent()
        {
            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            commodity.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.CommodityRepository.GetParentProjectId(commodity);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a commodity that doesn't have a parent");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null commodity.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullCommodity()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = contextManager.CommodityRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid commodity
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfValidCommodity()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            assembly.Parts.Add(part);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Commodities.Add(commodity);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.CommodityRepository.GetParentAssemblyId(commodity);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of a commodity");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a commodity that doesn't have a parent assembly
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfCommodityWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Commodities.Add(commodity);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.CommodityRepository.GetParentAssemblyId(commodity);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a commodity that doesn't have a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null commodity
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullCommodity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.CommodityRepository.GetParentAssemblyId(null);
        }

        /// <summary>
        /// A test for GetById method using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidCommodityTest()
        {
            Commodity commodity = new Commodity();
            commodity.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Commodity result = newContext.CommodityRepository.GetById(commodity.Guid);

            Assert.AreEqual<Guid>(commodity.Guid, result.Guid, "Failed to get by id a commodity entity");
        }

        /// <summary>
        /// A test for GetById method using a null commodity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullCommodityTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Commodity commodity = null;
            dataContext.CommodityRepository.GetById(commodity.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataCommodities()
        {
            Commodity commodity1 = new Commodity();
            commodity1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            commodity1.IsMasterData = true;

            Commodity commodity2 = new Commodity();
            commodity2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.CommodityRepository.Add(commodity1);
            dataContext.CommodityRepository.Add(commodity2);
            dataContext.SaveChanges();

            Collection<Commodity> actualResult = dataContext.CommodityRepository.GetAllMasterData();
            Collection<Commodity> expectedResult = new Collection<Commodity>(dataContext.EntityContext.CommoditySet.Where(c => c.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data commodities");
            foreach (Commodity commodity in expectedResult)
            {
                Commodity correspondingCommodity = actualResult.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Wrong commodities were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
