﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for DataBaseInfoRepository and is intended to contain
    /// unit tests for all public methods from DataBaseInfoRepository class.
    /// </summary>
    [TestClass]
    public class DataBaseInfoRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataBaseInfoRepositoryTest"/> class.
        /// </summary>
        public DataBaseInfoRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for GetDataBaseVersion() method.
        /// </summary>
        //[TestMethod]
        //public void GetDataBaseVersionTest()
        //{
        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(EntityContext.LocalDatabase);
        //    decimal? actualResult = dataContext.DataBaseInfoRepository.GetDataBaseVersion();
        //    decimal? expectedResult = dataContext.Context.DataBaseInfoSet.FirstOrDefault();

        //    Assert.AreEqual<Guid>(expectedResult.Guid, actualResult.Guid, "Wrong data base info was returned");
        //    Assert.AreEqual<decimal>(expectedResult.Version, actualResult.Version, "Failed to get data base version");
        //}

        #endregion Test Methods
    }
}
