﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ManufacturerRepository and is intended to contain
    /// unit tests for all public methods from ManufacturerRepository class.
    /// </summary>
    [TestClass]
    public class ManufacturerRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManufacturerRepositoryTest"/> class.
        /// </summary>
        public ManufacturerRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddManufacturer method. Create a manufacturer  and adds the created manufacturer into
        /// local context, commits the changes, then checks if manufacturer was added.
        /// </summary>
        [TestMethod()]
        public void AddManufacturerTest()
        {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Create a user which will be the owner of manufacturer
            User owner = new User();
            owner.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            owner.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            owner.Password = EncryptionManager.Instance.HashSHA256("Password.", owner.Salt, owner.Guid.ToString());
            manufacturer.Owner = owner;
            owner.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ManufacturerRepository.Add(manufacturer);
            dataContext.SaveChanges();

            // Verifies if manufacturer  was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool manufacturerAdded = newContext.ManufacturerRepository.CheckIfExists(manufacturer.Guid);

            Assert.IsTrue(manufacturerAdded, "Failed to create a manufacturer.");
        }

        /// <summary>
        /// Creates a manufacturer, updates it and checks if manufacturer was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateManufacturerTest()
        {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Create a user which will be the owner of manufacturer
            User owner = new User();
            owner.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            owner.Username = EncryptionManager.Instance.GenerateRandomString(15, true);
            owner.Password = EncryptionManager.Instance.HashSHA256("Password.", owner.Salt, owner.Guid.ToString());
            owner.Roles = Role.Admin;
            manufacturer.Owner = owner;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ManufacturerRepository.Add(manufacturer);
            dataContext.SaveChanges();

            manufacturer.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated manufacturer from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Manufacturer manufacturerFromDB = newContext.EntityContext.ManufacturerSet.FirstOrDefault(m => m.Guid == manufacturer.Guid);

            // Verifies if manufacturer was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(manufacturer.Name, manufacturerFromDB.Name, "Fails to update a manufacturer.");
        }

        /// <summary>
        /// Creates a manufacturer, deletes it and checks if manufacturer was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteManufacturerTest()
        {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(20, true);

            // Get the entities that belongs to the manufacturer unless the shared ones in order to check that the manufacturer was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(manufacturer);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ManufacturerRepository.Add(manufacturer);
            dataContext1.SaveChanges();

            // Delete the manufacturer 
            dataContext1.ManufacturerRepository.RemoveAll(manufacturer);
            dataContext1.SaveChanges();

            // Verify that the manufacturer was permanently deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetById method using a valid manufacturer as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidManufacturer()
        {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ManufacturerRepository.Add(manufacturer);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Manufacturer result = newContext.ManufacturerRepository.GetById(manufacturer.Guid);

            Assert.AreEqual<Guid>(manufacturer.Guid, result.Guid, "Failed to get by id a manufacturer");
        }

        /// <summary>
        /// A test for GetById method using a null manufacturer as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullManufacturer()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Manufacturer manufacturer = null;
            dataContext.ManufacturerRepository.GetById(manufacturer.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData method.
        /// </summary>
        [TestMethod]
        public void GetAllMasterDataManufacturers()
        {
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer1.Description = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer1.IsMasterData = true;

            Manufacturer manufacturer2 = new Manufacturer();
            manufacturer2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            manufacturer2.Description = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ManufacturerRepository.Add(manufacturer1);
            dataContext.ManufacturerRepository.Add(manufacturer2);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<Manufacturer> actualResult = newContext.ManufacturerRepository.GetAllMasterData();
            Collection<Manufacturer> expectedResult = new Collection<Manufacturer>(newContext.EntityContext.ManufacturerSet.Where(m => m.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data manufacturers");
            foreach (Manufacturer manufacturer in expectedResult)
            {
                Manufacturer correspondingManufacturer = actualResult.FirstOrDefault(m => m.Guid == manufacturer.Guid);
                if (correspondingManufacturer == null)
                {
                    Assert.Fail("Wrong manufacturers were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
