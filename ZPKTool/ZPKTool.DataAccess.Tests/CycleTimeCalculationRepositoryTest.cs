﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for CycleTimeCalculationRepository and is intended to contain
    /// unit tests for all public methods from CycleTimeCalculationRepository class.
    /// </summary>
    [TestClass]
    public class CycleTimeCalculationRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculationRepositoryTest"/> class.
        /// </summary>
        public CycleTimeCalculationRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        // 
        #endregion Additional test attributes

        #region Test Method

        #endregion Test Method
    }
}
