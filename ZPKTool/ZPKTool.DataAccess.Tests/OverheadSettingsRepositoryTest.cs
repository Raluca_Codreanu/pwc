﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for OverheadSettingsRepository and is intended to contain
    /// unit tests for all public methods from OverheadSettingsRepository class
    /// </summary>
    [TestClass]
    public class OverheadSettingsRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OverheadSettingsRepositoryTest"/> class.
        /// </summary>
        public OverheadSettingsRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region TestMethods

        /// <summary>
        /// Test AddOverheadSetting method. Create an overhead setting and adds the created OH setting into
        /// local context, commits the changes, then checks if OH setting was added.
        /// </summary>
        [TestMethod()]
        public void AddOverheadSettingTest()
        {
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            overheadSetting.ConsumableOverhead = 1;
            overheadSetting.CommodityOverhead = 1;
            overheadSetting.ExternalWorkOverhead = 1;
            overheadSetting.ManufacturingOverhead = 1;
            overheadSetting.MaterialMargin = 1;
            overheadSetting.ConsumableMargin = 1;
            overheadSetting.CommodityMargin = 1;
            overheadSetting.ExternalWorkMargin = 1;
            overheadSetting.ManufacturingMargin = 1;
            overheadSetting.OtherCostOHValue = 1;
            overheadSetting.PackagingOHValue = 1;
            overheadSetting.LogisticOHValue = 1;
            overheadSetting.SalesAndAdministrationOHValue = 1;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.OverheadSettingsRepository.Add(overheadSetting);
            contextManager.SaveChanges();

            // Verifies if overhead setting was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool overheadSettingAdded = newContext.OverheadSettingsRepository.CheckIfExists(overheadSetting.Guid);

            Assert.IsTrue(overheadSettingAdded, "Failed to create an overhead setting.");
        }

        /// <summary>
        /// Creates an  overhead setting, updates it and checks if OH setting was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateOverheadSettingTest()
        {
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            overheadSetting.ConsumableOverhead = 1;
            overheadSetting.CommodityOverhead = 1;
            overheadSetting.ExternalWorkOverhead = 1;
            overheadSetting.ManufacturingOverhead = 1;
            overheadSetting.MaterialMargin = 1;
            overheadSetting.ConsumableMargin = 1;
            overheadSetting.CommodityMargin = 1;
            overheadSetting.ExternalWorkMargin = 1;
            overheadSetting.ManufacturingMargin = 1;
            overheadSetting.OtherCostOHValue = 1;
            overheadSetting.PackagingOHValue = 1;
            overheadSetting.LogisticOHValue = 1;
            overheadSetting.SalesAndAdministrationOHValue = 1;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.OverheadSettingsRepository.Add(overheadSetting);
            contextManager.SaveChanges();

            overheadSetting.LogisticOHValue = 9;
            contextManager.SaveChanges();

            // Create a new context in order to get the updated OH setting from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            OverheadSetting overheadSettingFromDB = newContext.EntityContext.OverheadSettingSet.FirstOrDefault(o => o.Guid == overheadSetting.Guid);

            // Verifies if overhead setting was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<decimal>(overheadSetting.LogisticOHValue, overheadSettingFromDB.LogisticOHValue, "Fails to update an  overhead setting.");
        }

        /// <summary>
        /// Creates an overhead setting, deletes it and checks if OH setting was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteOverheadSettingTest()
        {
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            overheadSetting.ConsumableOverhead = 1;
            overheadSetting.CommodityOverhead = 1;
            overheadSetting.ExternalWorkOverhead = 1;
            overheadSetting.ManufacturingOverhead = 1;
            overheadSetting.MaterialMargin = 1;
            overheadSetting.ConsumableMargin = 1;
            overheadSetting.CommodityMargin = 1;
            overheadSetting.ExternalWorkMargin = 1;
            overheadSetting.ManufacturingMargin = 1;
            overheadSetting.OtherCostOHValue = 1;
            overheadSetting.PackagingOHValue = 1;
            overheadSetting.LogisticOHValue = 1;
            overheadSetting.SalesAndAdministrationOHValue = 1;

            // Get the entities that belongs to the overheadSetting unless the shared ones in order to check that the overheadSetting was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(overheadSetting);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.OverheadSettingsRepository.Add(overheadSetting);
            dataContext1.SaveChanges();

            // Delete the overhead setting
            dataContext1.OverheadSettingsRepository.RemoveAll(overheadSetting);
            dataContext1.SaveChanges();

            // Verify that the overhead setting was permanently deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetMasterData method.
        /// </summary>
        [TestMethod]
        public void GetMasterDataOverheadSettingTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            OverheadSetting actualResult = dataContext.OverheadSettingsRepository.GetMasterData();
            OverheadSetting expectedResult = dataContext.EntityContext.OverheadSettingSet.FirstOrDefault(o => o.IsMasterData);

            Assert.AreEqual<Guid>(expectedResult.Guid, actualResult.Guid, "Failed to get master data overhead setting");
        }

        #endregion TestMethods
    }
}
