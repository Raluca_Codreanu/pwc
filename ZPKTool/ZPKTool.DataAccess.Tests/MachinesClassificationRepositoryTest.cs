﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for MachinesClassificationRepository and is intended to contain
    /// unit tests for all public methods from MachinesClassificationRepository class.
    /// </summary>
    [TestClass]
    public class MachinesClassificationRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MachinesClassificationRepositoryTest"/> class.
        /// </summary>
        public MachinesClassificationRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddMachinesClassification method. Create a  machines classification and adds the created  classification into
        /// local context, commits the changes, then checks if machines classification was added.
        /// </summary>
        [TestMethod]
        public void AddMachinesClassificationTest()
        {
            MachinesClassification classification = new MachinesClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a subtype to main classification.
            MachinesClassification subType = new MachinesClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            classification.Children.Add(subType);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachinesClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            // Verifies if machines classification was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool classificationAdded = newContext.MachinesClassificationRepository.CheckIfExists(classification.Guid);

            Assert.IsTrue(classificationAdded, "Failed to create a  machines classification.");
        }

        /// <summary>
        /// Creates a  machines classification, updates it and checks if  machines classification was updated.
        /// </summary>
        [TestMethod]
        public void UpdateMachinesClassificationTest()
        {
            MachinesClassification classification = new MachinesClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a subtype to main classification.
            MachinesClassification subType = new MachinesClassification();
            subType.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            classification.Children.Add(subType);

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.MachinesClassificationRepository.Add(classification);
            contextManager.SaveChanges();

            classification.Name += " - Updated";
            contextManager.SaveChanges();

            // Create a new context in order to get the updated  machines classification from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MachinesClassification classificationFromDB = newContext.EntityContext.MachinesClassificationSet.FirstOrDefault(c => c.Guid == classification.Guid);

            // Verifies if machines classification was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(classification.Name, classificationFromDB.Name, "Failed to update a  machines classification.");
        }

        /// <summary>
        /// Creates a machines classification, deletes it and checks if classification was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteMachinesClassificationTest()
        {
            MachinesClassification classification = new MachinesClassification();
            classification.Name = classification.Guid.ToString();

            // Get the entities that belongs to the classification unless the shared ones in order to check that the classification was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(classification);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MachinesClassificationRepository.Add(classification);
            dataContext1.SaveChanges();

            // Delete the created classification
            dataContext1.MachinesClassificationRepository.RemoveAll(classification);
            dataContext1.SaveChanges();

            // Verify that the classification was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// A test for GetByName method using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void GetByNameValidMachineClassification()
        {
            MachinesClassification classification = new MachinesClassification();
            classification.Name = EncryptionManager.Instance.GenerateRandomString(10, true);

            MachinesClassification subClassification = new MachinesClassification();
            subClassification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            classification.Children.Add(subClassification);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachinesClassificationRepository.Add(classification);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MachinesClassification result = newContext.MachinesClassificationRepository.GetByName(subClassification.Name);

            Assert.AreEqual<Guid>(subClassification.Guid, result.Guid, "Failed to get a machine classification by name");
        }

        /// <summary>
        /// A test for GetByName method using a null machine as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByNameNullMachineClassification()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MachinesClassification classification = null;
            dataContext.MachinesClassificationRepository.GetByName(classification.Name);
        }

        /// <summary>
        /// A test for GetClassificationTree method.
        /// </summary>
        [TestMethod]
        public void GetClassificationTreeTest()
        {
            MachinesClassification mainClassification = new MachinesClassification();
            mainClassification.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            MachinesClassification subClassification = new MachinesClassification();
            subClassification.Name = EncryptionManager.Instance.GenerateRandomString(11, true);
            mainClassification.Children.Add(subClassification);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachinesClassificationRepository.Add(mainClassification);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<MachinesClassification> actualResult = newContext.MachinesClassificationRepository.LoadClassificationTree();
            Collection<MachinesClassification> expectedResult = new Collection<MachinesClassification>(newContext.EntityContext.MachinesClassificationSet.Where(c => !c.IsReleased).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get the classification tree");
            foreach (MachinesClassification classification in expectedResult)
            {
                MachinesClassification correspondingClassification = actualResult.FirstOrDefault(c => c.Guid == classification.Guid);
                if (correspondingClassification == null)
                {
                    Assert.Fail("Wrong classifications were returned");
                }
            }
        }

        #endregion Test Methods
    }
}
