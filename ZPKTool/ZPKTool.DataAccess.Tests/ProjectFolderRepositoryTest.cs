﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ProjectFolderRepository and is intended to contain
    /// unit tests for all public methods from ProjectFolderRepository class.
    /// </summary>
    [TestClass]
    public class ProjectFolderRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectFolderRepositoryTest"/> class.
        /// </summary>
        public ProjectFolderRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// Test AddProjectFolder method. Create a folder and adds the created folder into
        /// local context, commits the changes, then checks if folder was added.
        /// </summary>
        [TestMethod()]
        public void AddProjectFolderTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            // Verifies if the project folder was added.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool folderAdded = dataContext2.ProjectFolderRepository.CheckIfExists(folder.Guid);

            Assert.IsTrue(folderAdded, "Failed to create a project folder.");
        }

        /// <summary>
        /// Creates a project folder, updates it and checks if folder was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateProjectFolderTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            folder.Name += " - Updated";
            dataContext1.SaveChanges();

            // Create a new context in order to get the updated project folder from db.
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProjectFolder folderFromDB = dataContext2.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == folder.Guid);

            // Verify if the project folder was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(folder.Name, folderFromDB.Name, "Failed to update a project folder.");
        }

        /// <summary>
        /// Creates a project folder, deletes it and checks if folder was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteProjectFolderTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = folder.Guid.ToString();

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = project.Customer.Guid.ToString();

            Media media = new Media();
            media.Content = new byte[] { 1, 1, 1, 1 };
            media.Size = media.Content.Length;
            media.Type = (short)MediaType.Image;
            project.Media.Add(media);

            // Add an assembly to the project
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(media.Copy());
            project.Assemblies.Add(assembly);

            // Add a part to the project 
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Manufacturer = new Manufacturer();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            part.Media.Add(media.Copy());
            project.Parts.Add(part);

            ProjectFolder subfolder = new ProjectFolder();
            subfolder.Name = subfolder.Guid.ToString();
            folder.ChildrenProjectFolders.Add(subfolder);

            // Get the entities that belong to the folder unless the shared ones in order to check that the folder was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(project);
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            // Delete the created folder
            dataContext1.ProjectFolderRepository.RemoveAll(folder);
            dataContext1.SaveChanges();

            // Verify that the folder was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetRootFolders(Guid ownerId, bool noTracking) method.
        /// </summary>
        [TestMethod()]
        public void GetRootFoldersTest()
        {
            // Create a user - 'user1' which is the owner of two folders : a top level folder and a subLevel folder
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;

            ProjectFolder parentFolder = new ProjectFolder();
            parentFolder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            ProjectFolder subFolder = new ProjectFolder();
            subFolder.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            parentFolder.ChildrenProjectFolders.Add(subFolder);
            parentFolder.SetOwner(user1);

            // Create a user - 'user2' which is the owner of one folder: a top level folder
            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(11, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            ProjectFolder folder = new ProjectFolder();
            folder.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder.SetOwner(user2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(parentFolder);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<ProjectFolder> actualResult = dataContext2.ProjectFolderRepository.GetRootFolders(user1.Guid);
            Collection<ProjectFolder> expectedResults = new Collection<ProjectFolder>(
                dataContext2.EntityContext.ProjectFolderSet.Where(f => f.Owner != null && f.Owner.Guid == user1.Guid &&
                                                            f.ParentProjectFolder == null && !f.IsDeleted).ToList());

            Assert.AreEqual<int>(expectedResults.Count, actualResult.Count, "Failed to get all root folders of a specified user");
            foreach (ProjectFolder f in expectedResults)
            {
                if (actualResult.FirstOrDefault(fo => fo.Guid == f.Guid) == null)
                {
                    Assert.Fail("Wrong folders were returned");
                }
            }
        }

        /// <summary>
        /// Tests the GetSubFolders(Guid parentFolderId, Guid ownerId) method.
        /// </summary>
        [TestMethod()]
        public void GetSubFoldersTest()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(13, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;

            // Create a folder belonging to the "user1" 
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Add a folder to the "folder1". Set "IsDeleted" flag to true
            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            folder2.IsDeleted = true;
            folder1.ChildrenProjectFolders.Add(folder2);

            // Add a folder to the "folder2". Set the owner of "folder1" to "user1"
            ProjectFolder folder3 = new ProjectFolder();
            folder3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder1.ChildrenProjectFolders.Add(folder3);
            folder1.SetOwner(user1);

            User user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(14, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            ProjectFolder folder4 = new ProjectFolder();
            folder4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder4.SetOwner(user2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.ProjectFolderRepository.Add(folder4);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Collection<ProjectFolder> actualResult = dataContext2.ProjectFolderRepository.GetSubFolders(folder1.Guid, user1.Guid);
            Collection<ProjectFolder> expectedResult = new Collection<ProjectFolder>(
                dataContext2.EntityContext.ProjectFolderSet.Where(f => !f.IsDeleted &&
                                                            f.Owner != null &&
                                                            f.Owner.Guid == user1.Guid &&
                                                            f.ParentProjectFolder != null &&
                                                            f.ParentProjectFolder.Guid == folder1.Guid).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all subfolders of a specified folder");
            foreach (ProjectFolder folder in expectedResult)
            {
                if (actualResult.FirstOrDefault(f => f.Guid == folder.Guid) == null)
                {
                    Assert.Fail("Wrong folders were returned");
                }
            }
        }

        /// <summary>
        /// A test for GetFolderWithParent(Guid folderId, bool noTracking = false) method
        /// using a valid folder that has a parent folder as input data.
        /// </summary>
        [TestMethod]
        public void GetFolderWithParentTest()
        {
            User user1 = new User();
            user1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            user1.Username = EncryptionManager.Instance.GenerateRandomString(13, true);
            user1.Password = EncryptionManager.Instance.EncodeMD5("password");

            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder1.ChildrenProjectFolders.Add(folder2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProjectFolder result = dataContext2.ProjectFolderRepository.GetFolderWithParent(folder2.Guid);

            Assert.AreEqual<Guid>(folder2.Guid, result.Guid, "Wrong folder was returned");
            Assert.AreEqual<Guid>(folder2.ParentProjectFolder.Guid, result.ParentProjectFolder.Guid, "Failed to get a folder with parent");
        }

        /// <summary>
        /// A test for GetFolderWithParent(Guid folderId, bool noTracking = false) method.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNullFolderWithParentTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProjectFolder folder = null;
            dataContext.ProjectFolderRepository.GetFolderWithParent(folder.Guid);
        }

        /// <summary>
        /// A test for CheckIfParentOf(ProjectFolder parent, ProjectFolder folder) method.
        /// </summary>
        [TestMethod]
        public void CheckIfParentOfTest()
        {
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder1.ChildrenProjectFolders.Add(folder2);

            ProjectFolder folder3 = new ProjectFolder();
            folder3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            folder2.ChildrenProjectFolders.Add(folder3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool result1 = dataContext2.ProjectFolderRepository.CheckIfParentOf(folder1, folder3);
            bool result2 = dataContext2.ProjectFolderRepository.CheckIfParentOf(folder3, folder1);
            bool result3 = dataContext2.ProjectFolderRepository.CheckIfParentOf(null, folder3);
            bool result4 = dataContext2.ProjectFolderRepository.CheckIfParentOf(folder1, null);

            Assert.IsTrue(result1, "Failed to check if a folder is the parent of a specified folder");
            Assert.IsFalse(result2, "Failed to check if a folder is the parent of a specified folder");
            Assert.IsFalse(result3, "Failed to check if a null folder is the parent of a specified folder");
            Assert.IsFalse(result4, "Failed to check if a folder is the parent of a null folder");
        }

        #endregion Test Method
    }
}
