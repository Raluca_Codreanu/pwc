﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for BasicSettingsRepository and is intended
    /// to contain unit tests for all public methods from BasicSettingsRepository.
    /// </summary>
    [TestClass]
    public class BasicSettingsRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasicSettingsRepositoryTest"/> class.
        /// </summary>
        public BasicSettingsRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for GetBasicSettings(bool noTracking = false) method.
        /// </summary>
        [TestMethod]
        public void GetBasicSettingsTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            BasicSetting result = dataContext1.BasicSettingsRepository.GetBasicSettings();
            BasicSetting expectedResult = dataContext1.EntityContext.BasicSettingSet.FirstOrDefault();

            Assert.AreEqual<Guid>(result.Guid, expectedResult.Guid, "Wrong BasicSettings were returned");
        }

        #endregion Test Methods
    }
}
