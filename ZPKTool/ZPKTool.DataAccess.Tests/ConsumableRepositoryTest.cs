﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for ConsumableRepository and is intended to contain
    /// unit tests for all public methods from ConsumableRepository class
    /// </summary>
    [TestClass]
    public class ConsumableRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumableRepositoryTest"/> class.
        /// </summary>
        public ConsumableRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Test AddConsumable method. Create a consumable and adds the created consumable into the
        /// local context,commits the changes, then checks if consumable was added. 
        /// </summary>
        [TestMethod()]
        public void AddConsumableTest()
        {
            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            consumable.Manufacturer = new Manufacturer();
            consumable.Manufacturer.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();

            // Verifies if consumable was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool consumableAdded = newContext.ConsumableRepository.CheckIfExists(consumable.Guid);

            Assert.IsTrue(consumableAdded, "Fails to create a consumable.");
        }

        /// <summary>
        /// Creates a consumable, updates it and checks if consumable was updated.
        /// </summary>
        [TestMethod()]
        public void UpdateConsumableTest()
        {
            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();

            consumable.Name += " - Updated";
            dataContext.SaveChanges();

            // Create a new context in order to get the updated consumable from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Consumable consumableFromDB = newContext.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == consumable.Guid);

            // Verify if consumable was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<string>(consumable.Name, consumableFromDB.Name, "Fails to update a consumable.");
        }

        /// <summary>
        /// Creates a consumable, deletes it and checks if consumable was deleted.
        /// </summary>
        [TestMethod()]
        public void DeleteConsumableTest()
        {
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            consumable.Manufacturer = manufacturer;

            // Get the entities that belongs to consumable unless the shared ones in order to check that the consumable was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(consumable);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ConsumableRepository.Add(consumable);
            dataContext1.SaveChanges();

            // Delete the consumable
            dataContext1.ConsumableRepository.RemoveAll(consumable);
            dataContext1.SaveChanges();

            // Verify that the consumable was fully deleted
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a valid consumable as input data
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfValidConsumable()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.ConsumableRepository.GetParentProjectId(consumable);

            Assert.AreEqual<Guid>(project.Guid, parentProjectGuid, "Failed to get the parent project id of a consumable entity");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a consumable which hasn't a parent 
        /// </summary>
        [TestMethod()]
        public void GetParentProjectIdOfConsumableWithoutParent()
        {
            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            consumable.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = newContext.ConsumableRepository.GetParentProjectId(consumable);

            Assert.AreEqual<Guid>(Guid.Empty, parentProjectGuid, "A parent project was returned for a die without a parent project");
        }

        /// <summary>
        /// Tests the GetParentProjectId method using a null consumable as input data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentProjectIdOfNullConsumable()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentProjectGuid = dataContext.ConsumableRepository.GetParentProjectId(null);
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a valid consumable as input data.
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfValidConsumable()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            AssemblyProcessStep step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.ConsumableRepository.GetParentAssemblyId(consumable);

            Assert.AreEqual<Guid>(assembly.Guid, parentAssemblyGuid, "Failed to get the parent assembly id of a consumable entity");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a consumable that hasn't a parent assembly
        /// </summary>
        [TestMethod()]
        public void GetParentAssemblyIdOfConsumableWithoutParentAssembly()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            PartProcessStep step = new PartProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Process = new Process();
            part.Process.Steps.Add(step);

            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            step.Consumables.Add(consumable);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = newContext.ConsumableRepository.GetParentAssemblyId(consumable);

            Assert.AreEqual<Guid>(Guid.Empty, parentAssemblyGuid, "A parent assembly was returned for a consumable that hasn't a parent assembly");
        }

        /// <summary>
        /// Tests the GetParentAssemblyId method using a null consumable as input data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetParentAssemblyIdOfNullConsumable()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Guid parentAssemblyGuid = dataContext.ConsumableRepository.GetParentAssemblyId(null);
        }

        /// <summary>
        /// A test for GetById method using a valid consumable as input data.
        /// </summary>
        [TestMethod()]
        public void GetByIdValidConsumableTest()
        {
            Consumable consumable = new Consumable();
            consumable.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();

            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Consumable result = newContext.ConsumableRepository.GetById(consumable.Guid);

            Assert.AreEqual<Guid>(consumable.Guid, result.Guid, "Failed to get by id a consumable entity");
        }

        /// <summary>
        /// A test for GetById method using a null consumable as input data
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullConsumableTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Consumable consumable = null;
            dataContext.ConsumableRepository.GetById(consumable.Guid);
        }

        /// <summary>
        /// A test for GetAllMasterData method
        /// </summary>
        [TestMethod()]
        public void GetAllMasterDataConsumablesTest()
        {
            Consumable consumable1 = new Consumable();
            consumable1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            consumable1.IsMasterData = true;

            Consumable consumable2 = new Consumable();
            consumable2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ConsumableRepository.Add(consumable1);
            dataContext.ConsumableRepository.Add(consumable2);
            dataContext.SaveChanges();

            Collection<Consumable> actualResult = dataContext.ConsumableRepository.GetAllMasterData();
            Collection<Consumable> expectedResult = new Collection<Consumable>(dataContext.EntityContext.ConsumableSet.Where(c => c.IsMasterData).ToList());

            Assert.AreEqual<int>(expectedResult.Count, actualResult.Count, "Failed to get all master data consumables");
            foreach (Consumable consumable in expectedResult)
            {
                Consumable correspondingConsumable = actualResult.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Wrong consumable was returned");
                }
            }
        }

        #endregion Test Methods
    }
}
