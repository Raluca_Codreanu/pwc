﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.DataAccess.Tests
{
    /// <summary>
    /// This is a test class for MediaRepository and is intended to contain
    /// unit tests for all public methods from MediaRepository class.
    /// </summary>
    [TestClass]
    public class MediaRepositoryTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaRepositoryTest"/> class.
        /// </summary>
        public MediaRepositoryTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Method

        /// <summary>
        /// Test AddMedia method. Create a media and adds the created media into 
        /// local context, commits the changes, then checks if media was added. 
        /// </summary>
        [TestMethod]
        public void AddMediaTest()
        {
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            media.IsMasterData = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MediaRepository.Add(media);
            dataContext.SaveChanges();

            // Verifies if media was added.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            bool mediaIsAdded = newContext.MediaRepository.CheckIfExists(media.Guid);

            Assert.IsTrue(mediaIsAdded, "Failed to create a media.");
        }

        /// <summary>
        /// Creates a media, updates it and checks if media was updated.
        /// </summary>
        [TestMethod]
        public void UpdateMediaTest()
        {
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            media.IsMasterData = true;

            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.MediaRepository.Add(media);
            contextManager.SaveChanges();

            // Attach the media to the context. 
            contextManager.EntityContext.MediaSet.Attach(media);
            media.IsMasterData = false;
            contextManager.SaveChanges();

            // Create a new context in order to get the updated media from db.
            IDataSourceManager newContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media mediaFromDB = newContext.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == media.Guid);

            // Verify if media was updated in database too, by comparing the updated entity with entity stored in data base.
            Assert.AreEqual<bool>(media.IsMasterData, mediaFromDB.IsMasterData, "Failed to update a media.");
        }

        /// <summary>
        /// Creates a media, deletes it and checks if media was deleted.
        /// </summary>
        [TestMethod]
        public void DeleteMediaTest1()
        {
            Media media = new Media();
            media.Type = (short)MediaType.Image;
            media.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media.Size = media.Content.Length;
            media.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            media.IsMasterData = true;

            // Get the entities that belongs to the media unless the shared ones in order to check that the media was fully deleted
            List<IIdentifiable> deletedEntities = Utils.GetEntityGraph(media);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(media);
            dataContext1.SaveChanges();

            dataContext1.EntityContext.MediaSet.Attach(media);
            dataContext1.MediaRepository.RemoveAll(media);
            dataContext1.SaveChanges();

            // Verify that media was permanently deleted 
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Utils.VerifyDeletedEntities(deletedEntities, dataContext2);
        }

        /// <summary>
        /// Tests DeleteMedia(MediaMetaData metaData) method.
        /// </summary>
        [TestMethod]
        public void DeleteMediaTest2()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            image.IsMasterData = true;

            // Create a metadata for the created media
            MediaMetaData mediaMetadata = new MediaMetaData();
            mediaMetadata.Guid = image.Guid;
            mediaMetadata.Type = image.Type;
            mediaMetadata.Size = image.Size;
            mediaMetadata.IsMasterData = image.IsMasterData;
            mediaMetadata.OriginalFileName = image.OriginalFileName;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.MediaRepository.DeleteMedia(mediaMetadata);
            dataContext2.SaveChanges();

            // Gets the medias of assembly of all types
            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media result = dataContext3.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == image.Guid);

            Assert.IsNull(result, "Failed to delete a media");
        }

        /// <summary>
        /// Tests DeleteMedia(MediaMetaData metaData) method using a null metadata.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteMediaUsingNullMetadata()
        {
            IDataSourceManager contextManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            contextManager.MediaRepository.DeleteMedia(null);
        }

        /// <summary>
        /// Tests DeleteAllMedia(object entity) method.
        /// </summary>
        [TestMethod]
        public void DeleteAllMediaTest1()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            image.IsMasterData = true;

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Media.Add(image);
            assembly.Media.Add(document);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Deletes all medias of assembly
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.DeleteAllMedia(assembly);
            dataContext1.SaveChanges();

            // Gets the medias of assembly of all types
            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<Media> assemblyMedia = dataContext3.EntityContext.MediaSet.Include(m => m.Assembly).
                Where(m => m.Assembly != null && m.Assembly.Guid == assembly.Guid).ToList();

            Assert.IsTrue(assemblyMedia.Count == 0, "Failed to delete all medias of an assembly entity");
        }

        /// <summary>
        /// Tests DeleteAllMedia(object entity, IEnumerable[MediaType] types) method.
        /// </summary>
        [TestMethod]
        public void DeleteAllMediaTest2()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            image.IsMasterData = true;

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            document.IsMasterData = true;

            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Media.Add(image);
            assembly.Media.Add(document);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Creates a list of media types to be deleted
            List<MediaType> types = new List<MediaType>();
            types.Add(MediaType.Image);
            types.Add(MediaType.Video);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext2.MediaRepository.DeleteAllMedia(assembly, types);
            dataContext2.SaveChanges();

            IDataSourceManager dataContext3 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<Media> assemblyMedia = dataContext3.EntityContext.MediaSet.Include(m => m.Assembly).
                Where(m => m.Assembly != null && m.Assembly.Guid == assembly.Guid).ToList();

            Assert.IsNull(
                assemblyMedia.FirstOrDefault(m => m.Type == (short)MediaType.Image || m.Type == MediaType.Video),
                "Not all medias of the given type were deleted");

            Assert.IsNotNull(
                assemblyMedia.FirstOrDefault(m => m.Type == MediaType.Document),
                "A media of a different type was deleted");
        }

        /// <summary>
        /// Tests GetMedia(MediaMetaData metaData) method using a valid data.
        /// </summary>
        [TestMethod]
        public void GetMediaTest1()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);

            // Create a metadata for the created image
            MediaMetaData imageMetaData = new MediaMetaData();
            imageMetaData.Guid = image.Guid;
            imageMetaData.Type = image.Type;
            imageMetaData.Size = image.Size;
            imageMetaData.IsMasterData = image.IsMasterData;
            imageMetaData.OriginalFileName = image.OriginalFileName;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media result = dataContext2.MediaRepository.GetMedia(imageMetaData);

            Assert.AreEqual<Guid>(image.Guid, result.Guid, "Failed to get a media entity by metadata");
        }

        /// <summary>
        /// Tests GetMedia(MediaMetaData metaData) method using a null metadata.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMediaOfNullMetadata()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media result = dataContext.MediaRepository.GetMedia((MediaMetaData)null);
        }

        /// <summary>
        ///  A test GetMedia(object entity, IEnumerable[MediaType] types) method using valid data. 
        /// </summary>
        [TestMethod]
        public void GetMediaTest2()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Media.Add(image);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Media.Add(document);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 1, 1, 1, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = new List<MediaType>();
            types.Add(MediaType.Image);
            types.Add(MediaType.Video);
            Collection<Media> result = dataContext2.MediaRepository.GetMedia(assembly, types);

            Assert.IsTrue(result.Count() > 0, "Not all media were returned");
            Assert.IsNull(result.FirstOrDefault(m => m.Type == MediaType.Document), "A media of a different type was returned");
            Assert.IsNull(result.FirstOrDefault(m => m.Guid != image.Guid), "A media belonging to other entity was returned");
        }

        /// <summary>
        /// Tests GetMedia(object entity, IEnumerable[MediaType] types) method using a null entity.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMediaOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = new List<MediaType>();
            types.Add(MediaType.Image);
            Collection<Media> result = dataContext.MediaRepository.GetMedia(null, types);
        }

        /// <summary>
        /// Tests GetMediaMetaData method using a null type.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMediaOfNullType()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.Media.Add(image);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = null;
            Collection<Media> result = dataContext2.MediaRepository.GetMedia(part, types);
        }

        /// <summary>
        /// A test for GetById(Guid mediaId) method using a valid media as input data.
        /// </summary>
        [TestMethod]
        public void GetByIdValidMediaTest()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media result = dataContext2.MediaRepository.GetById(image.Guid);

            Assert.AreEqual<Guid>(image.Guid, result.Guid, "Failed to get by id a media entity");
        }

        /// <summary>
        /// A test for GetById(Guid mediaId) method using a null media as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetByIdNullMediaTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media media = null;
            Media result = dataContext.MediaRepository.GetById(media.Guid);
        }

        /// <summary>
        /// Tests the GetMediaForProcessSteps method
        /// </summary>
        [TestMethod()]
        public void GetMediaForProcessStepsTest()
        {
            ProcessStep step1 = new PartProcessStep();
            step1.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(10, true);
            step1.Media = image;

            Process process = new Process();
            process.Steps.Add(step1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = EncryptionManager.Instance.GenerateRandomString(15, true);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(12, true);
            step2.Media = document;
            process.Steps.Add(step2);

            ProcessStep step3 = new PartProcessStep();
            step3.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            process.Steps.Add(step3);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProcessRepository.Add(process);
            dataContext1.SaveChanges();

            List<ProcessStep> steps = new List<ProcessStep>();
            steps.Add(step1);
            steps.Add(step2);
            steps.Add(step3);
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var result = dataContext2.MediaRepository.GetMediaForProcessSteps(steps);

            Assert.IsTrue(result.Count == 2, "Failed to return all media - process steps pairs");

            Media step3Media;
            bool stepMediaFound = result.TryGetValue(step3.Guid, out step3Media);
            Assert.IsFalse(stepMediaFound, "A step without media was returned");
        }

        #endregion Test Method

        #region GetMediaMetaData tests

        /// <summary>
        /// Tests GetMediaMetaData(object entity, IEnumerable[MediaType] types) method using a valid data.
        /// </summary>
        [TestMethod]
        public void GetMediaMetaDataTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Media.Add(image);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = EncryptionManager.Instance.GenerateRandomString(15, true);
            assembly.Media.Add(document);

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 1, 1, 1, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";
            video.IsMasterData = false;
            part.Media.Add(video);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = new List<MediaType>();
            types.Add(MediaType.Image);
            types.Add(MediaType.Video);
            Collection<MediaMetaData> result = dataContext2.MediaRepository.GetMediaMetaData(assembly, types);

            Assert.IsTrue(result.Count() > 0, "Not all medias metadata were returned");
            Assert.IsNull(result.FirstOrDefault(m => m.Type == MediaType.Document), "A media metadata of a different type was returned");
            Assert.IsNull(result.FirstOrDefault(m => m.Guid != image.Guid), "A media metadata belonging to other entity was returned");
        }

        /// <summary>
        /// Tests GetMediaMetaData(object entity, IEnumerable[MediaType] types) method using a null entity.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMediaMetaDataOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = new List<MediaType>();
            types.Add(MediaType.Image);
            Collection<MediaMetaData> result = dataContext.MediaRepository.GetMediaMetaData(null, types);
        }

        /// <summary>
        /// Tests GetMediaMetaData(object entity, IEnumerable[MediaType] types) method using a null type.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMediaMetaDataOfNullType()
        {
            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(15, true);
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            part.Media.Add(image);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<MediaType> types = null;
            Collection<MediaMetaData> result = dataContext2.MediaRepository.GetMediaMetaData(part, types);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetMediaMetaDataForMultipleEntitiesWithUnsupportedEntityType()
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataManager.MediaRepository.GetMediaMetaData(new List<object>() { new ArgumentException() });
        }

        #endregion GetMediaMetaData tests
    }
}
