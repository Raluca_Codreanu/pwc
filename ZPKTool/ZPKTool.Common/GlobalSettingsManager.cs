﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Manages the global application settings.
    /// </summary>
    public sealed class GlobalSettingsManager : GenericSettingsManager
    {
        #region Attributes

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly GlobalSettingsManager instance = new GlobalSettingsManager();

        #endregion Attributes

        /// <summary>
        /// Prevents a default instance of the <see cref="GlobalSettingsManager"/> class from being created.
        /// </summary>
        private GlobalSettingsManager()
        {
            // The global settings file is placed next to the app exe. In unit tests the entry assembly is null and we shouldn't automatically initialize the settings file path.            
            if (System.Reflection.Assembly.GetEntryAssembly() != null)
            {
                var settingsFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                this.SettingsFilePath = Path.Combine(settingsFolder, "GlobalSettings.xml");
            }
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>
        public static GlobalSettingsManager Instance
        {
            get { return instance; }
        }

        #region Setting properties

        /// <summary>
        /// Gets or sets the address of the local SQL Server that stores the application's local database.
        /// </summary>        
        [Setting]
        [DefaultSettingValue("localhost\\SQLEXPRESS")]
        public string LocalConnectionDataSource
        {
            get
            {
                return (string)this["LocalConnectionDataSource"];
            }

            set
            {
                this["LocalConnectionDataSource"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for connecting to the local database server.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool UseWindowsAuthForLocalDb
        {
            get
            {
                return (bool)this["UseWindowsAuthForLocalDb"];
            }

            set
            {
                this["UseWindowsAuthForLocalDb"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for local database synchronization.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool UseWindowsAuthForLocalDbSync
        {
            get
            {
                return (bool)this["UseWindowsAuthForLocalDbSync"];
            }

            set
            {
                this["UseWindowsAuthForLocalDbSync"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the local database used by the application.
        /// </summary>        
        [Setting]
        [DefaultSettingValue("ZPKTool")]
        public string LocalConnectionInitialCatalog
        {
            get
            {
                return (string)this["LocalConnectionInitialCatalog"];
            }

            set
            {
                this["LocalConnectionInitialCatalog"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name used for connecting to the local SQL Server.
        /// </summary>
        [Setting]
        [DefaultSettingValue("PROimpensUser")]
        public string LocalConnectionUser
        {
            get
            {
                return (string)this["LocalConnectionUser"];
            }

            set
            {
                this["LocalConnectionUser"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name used by the synchronization for connecting to the local SQL Server.
        /// </summary>
        [Setting]
        [DefaultSettingValue("PROimpensSyncUser")]
        public string LocalSyncConnectionUser
        {
            get
            {
                return (string)this["LocalSyncConnectionUser"];
            }

            set
            {
                this["LocalSyncConnectionUser"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the encrypted password used for connecting to the local database.
        /// </summary>
        [Setting]
        [DefaultSettingValue("iRl9VE3o1n1GPJexKN6RjA==")]
        public string LocalConnectionPasswordEncrypted
        {
            get
            {
                var password = (string)this["LocalConnectionPasswordEncrypted"];

                // If it's the old password return the default one.
                if (string.Equals(password, "iRl9VE3o1n1OlxzcTaOA0g==", StringComparison.Ordinal))
                {
                    return "iRl9VE3o1n1GPJexKN6RjA==";
                }

                return password;
            }

            set
            {
                this["LocalConnectionPasswordEncrypted"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the address of the SLQ Server that hosts the central database.
        /// </summary>
        [Setting]
        [DefaultSettingValue("localhost\\SQLEXPRESS")]
        public string CentralConnectionDataSource
        {
            get
            {
                return (string)this["CentralConnectionDataSource"];
            }

            set
            {
                this["CentralConnectionDataSource"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for connecting to the central database server.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool UseWindowsAuthForCentralDb
        {
            get
            {
                return (bool)this["UseWindowsAuthForCentralDb"];
            }

            set
            {
                this["UseWindowsAuthForCentralDb"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for central database synchronization.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool UseWindowsAuthForCentralDbSync
        {
            get
            {
                return (bool)this["UseWindowsAuthForCentralDbSync"];
            }

            set
            {
                this["UseWindowsAuthForCentralDbSync"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use SSL support for connecting to the central database server.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool UseSSLSupportForCentralDb
        {
            get
            {
                return (bool)this["UseSSLSupportForCentralDb"];
            }

            set
            {
                this["UseSSLSupportForCentralDb"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the central database.
        /// </summary>
        [Setting]
        [DefaultSettingValue("ZPKToolCentral")]
        public string CentralConnectionInitialCatalog
        {
            get
            {
                return (string)this["CentralConnectionInitialCatalog"];
            }

            set
            {
                this["CentralConnectionInitialCatalog"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name used for connecting to the central server
        /// </summary>
        [Setting]
        [DefaultSettingValue("PROimpensUser")]
        public string CentralConnectionUser
        {
            get
            {
                return (string)this["CentralConnectionUser"];
            }

            set
            {
                this["CentralConnectionUser"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the user name used by the synchronization for connecting to the central server
        /// </summary>
        [Setting]
        [DefaultSettingValue("PROimpensSyncUser")]
        public string CentralSyncConnectionUser
        {
            get
            {
                return (string)this["CentralSyncConnectionUser"];
            }

            set
            {
                this["CentralSyncConnectionUser"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the encrypted password used for connecting to the central database
        /// </summary>
        [Setting]
        [DefaultSettingValue("iRl9VE3o1n1GPJexKN6RjA==")]
        public string CentralConnectionPasswordEncrypted
        {
            get
            {
                var password = (string)this["CentralConnectionPasswordEncrypted"];

                // If it's the old password return the default one.
                if (string.Equals(password, "iRl9VE3o1n1OlxzcTaOA0g==", StringComparison.Ordinal))
                {
                    return "iRl9VE3o1n1GPJexKN6RjA==";
                }

                return password;
            }

            set
            {
                this["CentralConnectionPasswordEncrypted"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the location where to check for updates (URL or UNC path).
        /// </summary>
        [Setting]
        [DefaultSettingValue("")]
        public string AutomaticUpdatePath
        {
            get
            {
                return (string)this["AutomaticUpdatePath"];
            }

            set
            {
                this["AutomaticUpdatePath"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to start the application normally or in viewer mode.
        /// Default is false.
        /// </summary>        
        [Setting]
        [DefaultSettingValue("False")]
        public bool StartInViewerMode
        {
            get
            {
                return (bool)this["StartInViewerMode"];
            }

            set
            {
                this["StartInViewerMode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PCM Viewer was installed or is run from an executable.
        /// </summary>        
        [Setting]
        [DefaultSettingValue("False")]
        public bool PCMViewerIsInstalled
        {
            get
            {
                return (bool)this["PCMViewerIsInstalled"];
            }

            set
            {
                this["PCMViewerIsInstalled"] = value;
            }
        }

        #endregion Setting properties
    }
}
