﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// This class contains the error codes.
    /// An error code should be created only if the error must be reported to the user (and makes sense to report it).
    /// </summary>
    public static class ErrorCodes
    {
        #region Generic error codes

        /// <summary>
        /// The error was not due to happen at anytime and is probably the result of a bug.
        /// </summary>
        public const string InternalError = "Error_Internal";

        /// <summary>
        /// The target part already contains a raw part.
        /// </summary>
        public const string PartAlreadyContainsRawPart = "Error_PartAlreadyContainsRawPart";

        /// <summary>
        /// The target part already contains a raw part but it's deleted to the trash bin.
        /// </summary>
        public const string PartAlreadyContainsDeletedRawPart = "Error_PartAlreadyContainsDeletedRawPart";

        /// <summary>
        /// Provided AccessRights do not match the ones required for
        /// the operation to be performed.
        /// </summary>
        public const string InvalidAccessRights = "Error_InvalidAccessRight";

        /// <summary>
        /// Possible project states : {Released and !Released}
        /// </summary>
        public const string InvalidProjectState = "Error_InvalidProjectState";

        /// <summary>
        /// Required user was not found in the database.
        /// Users should be created only by the administrator.
        /// </summary>
        public const string UserDoesNotExist = "Error_NoSuchUser";

        /// <summary>
        /// Generic conversion error.
        /// </summary>
        public const string ConversionError = "Error_ConversionError";

        /// <summary>
        /// Item not found error.
        /// </summary>
        public const string ItemNotFound = "General_ItemNotFoundInDb";

        /// <summary>
        /// Error triggered when a user is assigned a username that already exists in the db.
        /// </summary>
        public const string UsernameAlreadyExists = "User_UsernameAlreadyExists";

        /// <summary>
        /// Error raised when the current password provided by an user is not correct.
        /// </summary>
        public const string WrongCurrentPassword = "General_CurrentPasswordNotCorrect";

        /// <summary>
        /// Raised when the current password provided by an user is not valid.
        /// </summary>
        public const string InvalidPasssword = "Password_PolicyRequirements";

        /// <summary>
        /// Raised if the user hasn't change the password.
        /// </summary>
        public const string PasswordChangeMandatory = "Password_ChangeMandatory";

        /// <summary>
        /// Error occurs when the new password is the same with the old password changed.
        /// </summary>
        public const string NewAndOldPasswordsAreIdentical = "General_NewAndOldPasswordsAreIdentical";

        /// <summary>
        /// Error occurs when the new password is the same with one that has been used for the last 5 times.  
        /// </summary>
        public const string PasswordHasBeenUsedBefore = "General_PasswordHasBeenUsedBefore";

        /// <summary>
        /// Raised when an unexpected error occurs during the creation of an excel report.
        /// </summary>
        public const string ExcelReportGenerationUnknownError = "Reports_UnknownError";

        /// <summary>
        /// An IOException has been thrown when creating a report.
        /// </summary>
        public const string ReportGenerationIOError = "Reports_IOError";

        /// <summary>
        /// Raised when an error occurs during the creation of an PDF report.
        /// </summary>
        public const string PdfReportGenerationUnknownError = "Reports_UnknownError";

        /// <summary>
        /// Error occurs when the recipient of a paste operation does not support the pasted object.
        /// </summary>
        public const string PastedObjectNotSupported = "Paste_ObjectNotSupported";

        /// <summary>
        /// Error occurs when the object that must be pasted no longer exists in the database.
        /// </summary>
        public const string PastedObjectNotAvailable = "Paste_PasteObjectNotAvailable";

        /// <summary>
        /// Error occurs when the recipient of a paste operations is does not support the paste operation.
        /// </summary>
        public const string PasteRecipientNotSupported = "Paste_RecipientNotSupported";

        /// <summary>
        /// Error occurs when the object from the clipboard was deleted.
        /// </summary>
        public const string PasteClipboardObjectDeleted = "Paste_ClipboardObjectDeleted";

        /// <summary>
        /// Error occurs when reading a media file from the disk fails.
        /// </summary>
        public const string MediaFileReadFailed = "Media_FailedToReadFromDisk";

        /// <summary>
        /// Error occurs when a (document)file can't be read from the disk.
        /// </summary>
        public const string FileReadFail = "Error_FileReadFailed";

        /// <summary>
        /// Error occurs when a (document)file can't be written to the disk.
        /// </summary>
        public const string FileWriteFail = "Error_FileWriteFailed";

        /// <summary>
        /// Error occurs when creating a BitmapImage from an existing file fails.
        /// </summary>
        public const string ErrorCreatingImage = "Error_CantCreateImageFromFile";

        /// <summary>
        /// Error that occurs when trying to move a project folder in one of its children.
        /// </summary>
        public const string ProjectFolderMoveInChildError = "Error_ProjectFolderMoveInChild";

        /// <summary>
        /// Error that occurs when trying to move an assembly in one of its children.
        /// </summary>
        public const string AssemblyMoveInChildError = "Error_AssemblyMoveInChild";

        /// <summary>
        /// The local database was offline during the login of a user.
        /// </summary>
        public const string UserAuthenticationLocalDatabaseOffline = "UserAuthentication_LocalDatabaseOffline";

        #endregion Generic error codes

        #region Import Export

        /// <summary>
        /// Failed to create the export file.
        /// </summary>
        public const string ExportFileError = "ExportImport_ExportFileCreateFail";

        /// <summary>
        /// The file to be imported had invalid content (corrupted or not a file exported with the application);
        /// </summary>
        public const string ImportFileInvalidContent = "ExportImport_ImportInvalidFileContent";

        /// <summary>
        /// The imported file was created by a (newer) app version which uses a new file format and import/export algorithm.
        /// </summary>
        public const string ImportFileCreatedByNewerVersion = "ExportImport_ImportUnsupportedVersion";

        /// <summary>
        /// The imported file did not contain a commodity.
        /// </summary>
        public const string ImportCommodityInvalidFileContent = "ExportImport_NoCommodityContent";

        /// <summary>
        /// The imported file did not contain a consumable.
        /// </summary>
        public const string ImportConsumableInvalidFileContent = "ExportImport_NoConsumableContent";

        /// <summary>
        /// The imported file did not contain a die.
        /// </summary>
        public const string ImportDieInvalidFileContent = "ExportImport_NoDieContent";

        /// <summary>
        /// The imported file did not contain a machine.
        /// </summary>
        public const string ImportMachineInvalidFileContent = "ExportImport_NoMachineContent";

        /// <summary>
        /// The imported file did not contain a raw material.
        /// </summary>
        public const string ImportRawMaterialInvalidFileContent = "ExportImport_NoRawMaterialContent";

        /// <summary>
        /// The imported file did not contain a part.
        /// </summary>
        public const string ImportPartInvalidFileContent = "ExportImport_NoPartContent";

        /// <summary>
        /// The imported file did not contain a project.
        /// </summary>
        public const string ImportProjectInvalidFileContent = "ExportImport_NoProjectContent";

        /// <summary>
        /// The imported file did not contain a project folder.
        /// </summary>
        public const string ImportProjectFolderInvalidFileContent = "ExportImport_NoProjectFolderContent";

        /// <summary>
        /// The imported file did not contain an assembly.
        /// </summary>
        public const string ImportAssemblyInvalidFileContent = "ExportImport_NoAssemblyContent";

        /// <summary>
        /// There was an error while importing the media of the imported object/model.
        /// </summary>
        public const string ImportMediaError = "ExportImport_MediaImportError";

        /// <summary>
        /// The imported entity is not supported in the view mode.
        /// </summary>
        public const string ImportedEntityNotSupportedInView = "ExportImport_EntityNotSupportedInViewMode";

        #endregion Import Export

        #region Security

        /// <summary>
        /// The user is disabled.
        /// </summary>
        public const string UserDisabled = "Security_UserDisabled";

        /// <summary>
        /// The user is deleted.
        /// </summary>
        public const string UserDeleted = "Security_UserDeleted";

        #endregion Security

        #region Calculator

        /// <summary>
        /// An unknown error occurred while reading the Machining Calculator KB data from the file.
        /// </summary>
        public const string MachiningCalculatorKBLoadUnknownError = "MachiningCalculator_Knowledgebase_LoadUnknownError";

        #endregion Calculator
    }
}