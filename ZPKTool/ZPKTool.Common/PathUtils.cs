﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Text;

    /// <summary>
    /// Contains utility methods related to files and folder manipulation and validation.
    /// </summary>
    public static class PathUtils
    {
        /// <summary>
        /// Fixes the invalid characters in a file name by replacing them with the underscore character ('_').
        /// </summary>
        /// <param name="fileName">The file name to fix.</param>
        /// <returns>
        /// The fixed file name.
        /// </returns>
        public static string SanitizeFileName(string fileName)
        {
            return SanitizeFileName(fileName, '_');
        }

        /// <summary>
        /// Fixes the invalid characters in a file name by replacing them with a given character (that must be valid).
        /// </summary>
        /// <param name="fileName">The file name to fix.</param>
        /// <param name="replacementChar">The character that will replace the invalid ones.</param>
        /// <returns>The fixed file name.</returns>
        public static string SanitizeFileName(string fileName, char replacementChar)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("The path to fix is null or empty.");
            }

            List<char> invalidChars = new List<char>(System.IO.Path.GetInvalidFileNameChars());
            if (invalidChars.Contains(replacementChar))
            {
                throw new ArgumentException("The replacement character (" + replacementChar + ") is invalid.");
            }

            string fixedFileName = fileName;
            foreach (char c in invalidChars)
            {
                if (fixedFileName.Contains(c))
                {
                    fixedFileName = fixedFileName.Replace(c, replacementChar);
                }
            }

            return fixedFileName;
        }

        /// <summary>
        /// Sanitizes the folder path by replacing all invalid characters with the underscore character ('_').
        /// </summary>
        /// <param name="path">The folder path.</param>
        /// <returns>
        /// The sanitized path.
        /// </returns>
        /// <exception cref="System.ArgumentException">The folder path to sanitize is null or empty.</exception>
        public static string SanitizeFolderPath(string path)
        {
            return SanitizeFolderPath(path, '_');
        }

        /// <summary>
        /// Sanitizes the folder path by replacing all invalid characters with a specified character.
        /// </summary>
        /// <param name="path">The folder path.</param>
        /// <param name="replacement">The replacement character.</param>
        /// <returns>
        /// The sanitized path.
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// The folder path to sanitize is null or empty.
        /// or
        /// The replacement character is invalid.
        /// </exception>
        public static string SanitizeFolderPath(string path, char replacement)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("The folder path to sanitize is null or empty.", "path");
            }

            var invalidChars = System.IO.Path.GetInvalidPathChars().ToList();

            // Add the file name specific invalid chars \ / : * ? " < > |
            var invalidFileNameChars = System.IO.Path.GetInvalidFileNameChars().ToList();
            invalidChars.AddRange(invalidFileNameChars.Except(invalidChars));

            if (invalidChars.Contains(replacement))
            {
                throw new ArgumentException("The replacement character (" + replacement + ") is invalid.", "replacement");
            }

            string sanitizedPath = path;
            foreach (char ch in invalidChars)
            {
                if (sanitizedPath.Contains(ch))
                {
                    sanitizedPath = sanitizedPath.Replace(ch, replacement);
                }
            }

            return sanitizedPath;
        }

        /// <summary>
        /// Checks if the specified path is a well-formed path, containing only valid characters.
        /// <para/>
        /// Does not check if the path exists.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        /// <returns>
        /// True if the path is valid, false otherwise.
        /// </returns>
        /// <exception cref="ArgumentException">the path is null, empty or whitespace or contains illegal characters.</exception>
        /// <exception cref="PathTooLongException">the path length exceeds the operating system maximum length for paths</exception>
        public static bool ValidatePath(string path)
        {
            return ValidatePath(path, true);
        }

        /// <summary>
        /// Checks if the specified path is a well-formed path, containing only valid characters.
        /// <para />
        /// Does not check if the path exists.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        /// <param name="throwOnError">if set to true it throws an exception when a validation error occurs; otherwise it returns false.</param>
        /// <returns>True if the path is valid, false otherwise.</returns>
        /// <exception cref="ArgumentException">the path is null, empty or whitespace or contains illegal characters.</exception>
        /// <exception cref="PathTooLongException">the path length exceeds the operating system maximum length for paths</exception>
        public static bool ValidatePath(string path, bool throwOnError)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                if (throwOnError)
                {
                    throw new ArgumentException("The path can't be null, empty or whitespace.", path);
                }
                else
                {
                    return false;
                }
            }

            try
            {
                // Used GetFullPath only for absolute paths to avoid errors related to retrieving the full path corresponding to a relative path.
                if (Path.IsPathRooted(path))
                {
                    // This method throws an exception for most of the invalid chars, but not all.
                    Path.GetFullPath(path);
                }

                // Path.GetInvalidPathChars() does not contain all invalid folder chars so we use Path.GetInvalidFileNameChars().
                // The only problem with Path.GetInvalidFileNameChars() is that it contains the ':' character which is contained in the path root,
                // that is why we remove the root from the path before validating its characters.
                var pathRoot = Path.GetPathRoot(path);
                path = path.Remove(0, pathRoot.Length);

                bool valid = path.IndexOfAny(Path.GetInvalidFileNameChars()) == -1;
                if (throwOnError && !valid)
                {
                    throw new ArgumentException("The path contains invalid characters.", path);
                }
                else
                {
                    return valid;
                }
            }
            catch
            {
                if (throwOnError)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Parse the given exception to give it the appropriate error code and message.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns>
        /// A persistence exception containing appropriate error code and message.
        /// </returns>
        public static Exception ParseFileRelatedException(Exception ex)
        {
            FileRelatedException fileErrorException = null;

            if (ex is DirectoryNotFoundException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.DirectoryNotFound, ex);
            }
            else if (ex is DriveNotFoundException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.DriveNotFound, ex);
            }
            else if (ex is FileNotFoundException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.FileNotFound, ex);
            }
            else if (ex is SecurityException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.Security, ex);
            }
            else if (ex is PathTooLongException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.PathTooLong, ex);
            }
            else if (ex is UnauthorizedAccessException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.UnauthorizedAccess, ex);
            }
            else if (ex is IOException)
            {
                fileErrorException = new FileRelatedException(FileErrorCode.IOError, ex);
            }

            return fileErrorException ?? ex;
        }

        /// <summary>
        /// Creates a temporary folder with a random unique name, in the current user's TEMP directory.        
        /// </summary>
        /// <returns>
        /// The path to the created directory.
        /// </returns>
        public static string CreateTemporaryFolder()
        {
            string tempDirPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirPath);
            return tempDirPath;
        }

        /// <summary>
        /// Returns the input if the file path does not exist, otherwise it returns the first file path which can be used, 
        /// in order to avoid the file name collisions (input file path + (1), (2), etc.).
        /// </summary>
        /// <param name="filePath">The input file path</param>
        /// <returns>
        /// The new file path, based on the input, avoiding the file name collisions .
        /// </returns>
        public static string FixFilePathForCollisions(string filePath)
        {
            var folderPath = Path.GetDirectoryName(filePath);
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);

            string validFilePath = filePath;
            int index = 0;
            while (File.Exists(validFilePath))
            {
                index++;
                validFilePath = Path.Combine(folderPath, fileName + " (" + index + ")" + extension);
            }

            return validFilePath;
        }
    }
}
