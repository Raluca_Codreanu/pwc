﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// The exception that is thrown when an error occurs during MAPI operations.
    /// </summary>
    [Serializable]
    public class MapiException : ZPKException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapiException"/> class.
        /// </summary>
        public MapiException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapiException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public MapiException(string message)
            : base(string.Empty, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapiException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public MapiException(string errorCode, Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapiException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected MapiException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
