﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// The recipient types of MAPI mail messages.
    /// </summary>
    public enum MapiRecipientType
    {
        /// <summary>
        /// Original recipient.
        /// </summary>
        Orig = 0,

        /// <summary>
        /// To recipient.
        /// </summary>
        To = 1,

        /// <summary>
        /// CC recipient.
        /// </summary>
        Cc = 2,

        /// <summary>
        /// BCC recipient.
        /// </summary>
        Bcc = 3
    }
}
