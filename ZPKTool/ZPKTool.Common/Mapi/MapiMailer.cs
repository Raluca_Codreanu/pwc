﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// MAPI (Messaging Application Program Interface) enables you to send e-mail from within a Windows 
    /// application and attach the document you are working on to the e-mail.
    /// </summary>    
    public class MapiMailer
    {
        #region Members

        /// <summary>
        /// Maximum allowed attachments.
        /// </summary>
        private const int MaxAttachments = 20;

        /// <summary>
        /// Contains the descriptions messages for the code returned by the SendMail call. The description for code x is at index x in the array.
        /// </summary>
        private static readonly string[] mapiReturnCodeDescriptions = new string[] 
        {
            "OK [0]",
            "User abort [1]",
            "General MAPI failure [2]",
            "MAPI login failure [3]",
            "Disk full [4]",
            "Insufficient memory [5]",
            "Access denied [6]",
            "-unknown- [7]",
            "Too many sessions [8]",
            "Too many files were specified [9]",
            "Too many recipients were specified [10]",
            "A specified attachment was not found [11]",
            "Attachment open failure [12]",
            "Attachment write failure [13]",
            "Unknown recipient [14]",
            "Bad recipient type [15]",
            "No messages [16]",
            "Invalid message [17]",
            "Text too large [18]",
            "Invalid session [19]",
            "Type not supported [20]",
            "A recipient was specified ambiguously [21]",
            "Message in use [22]",
            "Network failure [23]",
            "Invalid edit fields [24]",
            "Invalid recipients [25]",
            "Not supported [26]" 
        };
                
        #endregion

        #region Public Methods

        /// <summary>
        /// Sends the specified mail.
        /// </summary>
        /// <param name="mail">The mail.</param>
        /// <exception cref="ZPKTool.Common.Mapi.MapiException">
        /// The number of mail attachments exceeded the maximum supported amount 
        /// or The MAPI SendMail call failed (see the exception message for details on the reason).
        /// </exception>
        public void SendMail(MapiMail mail)
        {
            if (mail.Attachments.Count > MaxAttachments)
            {
                throw new MapiException("The number of mail attachments exceeded the maximum supported amount.");
            }

            var message = new NativeMethods.MapiMessage();
            try
            {
                message.Subject = mail.Subject;
                message.NoteText = mail.Body;

                this.MarshalRecipients(mail.Recipients, out message.Recipients, out message.RecipientsCount);
                this.MarshalAttachments(mail.Attachments, out message.Files, out message.FileCount);

                int returnCode = NativeMethods.MAPISendMail(IntPtr.Zero, IntPtr.Zero, ref message, (int)MapiSendMailFlags.Dialog | (int)MapiSendMailFlags.LogonUI, 0);
                if (returnCode > 1)
                {
                    string errorDescription = null;
                    if (returnCode < mapiReturnCodeDescriptions.Length)
                    {
                        errorDescription = mapiReturnCodeDescriptions[returnCode];
                    }
                    else
                    {
                        errorDescription = string.Format(CultureInfo.InvariantCulture, "MAPI SendMail failed with the unknown error code '{0}'.", returnCode);
                    }

                    throw new MapiException(errorDescription);
                }
            }
            finally
            {
                this.ReleaseMemory(message);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Marshals the specified recipient information into an unmanaged block of memory that will be used by MAPISendMail.
        /// </summary>
        /// <param name="recipients">The recipients.</param>
        /// <param name="marshaledRecipients">The marshaled recipients.</param>
        /// <param name="marshaledRecipientsCount">The marshaled recipients count.</param>
        private void MarshalRecipients(
            ICollection<MapiRecipient> recipients,
            out IntPtr marshaledRecipients,
            out int marshaledRecipientsCount)
        {
            marshaledRecipientsCount = 0;
            marshaledRecipients = IntPtr.Zero;

            if (recipients.Count == 0)
            {
                return;
            }

            int size = Marshal.SizeOf(typeof(NativeMethods.MapiRecipientDescription));
            marshaledRecipients = Marshal.AllocHGlobal(recipients.Count * size);
            IntPtr ptr = marshaledRecipients;

            foreach (var recipient in recipients)
            {
                var mapiRecipient = new NativeMethods.MapiRecipientDescription();
                mapiRecipient.RecipientClass = (int)recipient.Type;
                mapiRecipient.Name = recipient.Address;

                Marshal.StructureToPtr(mapiRecipient, ptr, false);
                ptr += size;
            }

            marshaledRecipientsCount = recipients.Count;
        }

        /// <summary>
        /// Marshals the specified attachment information into an unmanaged block of memory that will be used by MAPISendMail.
        /// </summary>
        /// <param name="attachments">The attachments.</param>
        /// <param name="marshaledAttachments">The marshaled attachments.</param>
        /// <param name="marshaledAttachmentsCount">The marshaled attachments count.</param>
        private void MarshalAttachments(
            ICollection<MapiAttachment> attachments,
            out IntPtr marshaledAttachments,
            out int marshaledAttachmentsCount)
        {
            marshaledAttachmentsCount = 0;
            marshaledAttachments = IntPtr.Zero;

            if (attachments.Count == 0)
            {
                return;
            }

            if (attachments.Count > MaxAttachments)
            {
                return;
            }

            int size = Marshal.SizeOf(typeof(NativeMethods.MapiFileDescription));
            marshaledAttachments = Marshal.AllocHGlobal(attachments.Count * size);
            IntPtr ptr = marshaledAttachments;

            foreach (var attachment in attachments)
            {
                var mapiAttachment = new NativeMethods.MapiFileDescription();
                mapiAttachment.Position = -1;
                mapiAttachment.Path = attachment.Path;
                mapiAttachment.Name = attachment.Name;

                Marshal.StructureToPtr(mapiAttachment, ptr, false);
                ptr += size;
            }

            marshaledAttachmentsCount = attachments.Count;
        }

        /// <summary>
        /// Releases the unmanaged memory allocated for the specified message.
        /// </summary>
        /// <param name="mapiMessage">The message.</param>
        private void ReleaseMemory(NativeMethods.MapiMessage mapiMessage)
        {
            int size = Marshal.SizeOf(typeof(NativeMethods.MapiRecipientDescription));
            
            if (mapiMessage.Recipients != IntPtr.Zero)
            {
                IntPtr ptr = mapiMessage.Recipients;
                for (int i = 0; i < mapiMessage.RecipientsCount; i++)
                {
                    Marshal.DestroyStructure(ptr, typeof(NativeMethods.MapiRecipientDescription));
                    ptr += size;
                }

                Marshal.FreeHGlobal(mapiMessage.Recipients);
            }

            if (mapiMessage.Files != IntPtr.Zero)
            {
                size = Marshal.SizeOf(typeof(NativeMethods.MapiFileDescription));

                IntPtr ptr = mapiMessage.Files;
                for (int i = 0; i < mapiMessage.FileCount; i++)
                {
                    Marshal.DestroyStructure((IntPtr)ptr, typeof(NativeMethods.MapiFileDescription));
                    ptr += size;
                }

                Marshal.FreeHGlobal(mapiMessage.Files);
            }
        }

        #endregion
    }
}
