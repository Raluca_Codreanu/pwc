﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// Represents a mail that can be sent using MAPI.
    /// </summary>
    public class MapiMail
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapiMail"/> class.
        /// </summary>
        public MapiMail()
        {
            this.Recipients = new List<MapiRecipient>();
            this.Attachments = new List<MapiAttachment>();
        }

        /// <summary>
        /// Gets the recipient(s) of the mail.
        /// </summary>
        public ICollection<MapiRecipient> Recipients { get; private set; }

        /// <summary>
        /// Gets or sets the subject of the mail.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body of the mail.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets the attachments of the mail.
        /// </summary>
        public ICollection<MapiAttachment> Attachments { get; private set; }
    }
}
