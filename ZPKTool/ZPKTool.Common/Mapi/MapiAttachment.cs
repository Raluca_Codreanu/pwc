﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// Represents an attachment of a MAPI Mail.
    /// </summary>
    public class MapiAttachment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapiAttachment" /> class.
        /// </summary>
        /// <param name="attachmentPath">The the fully qualified path of the attached file. This path should include the disk drive letter and directory name.</param>
        /// <exception cref="System.ArgumentException">The attachment path was null or empty.</exception>
        public MapiAttachment(string attachmentPath)
        {
            if (string.IsNullOrWhiteSpace(attachmentPath))
            {
                throw new ArgumentException("The attachment path was null or empty.", "attachmentPath");
            }

            this.Path = attachmentPath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MapiAttachment" /> class.
        /// </summary>
        /// <param name="attachmentPath">The the fully qualified path of the attached file. This path should include the disk drive letter and directory name.</param>
        /// <param name="attachmentName">The attachment filename seen by the recipient, which may differ from the filename in the <paramref name="attachmentPath" />
        /// parameter if temporary files are being used. If the value is empty or null, the filename from the <paramref name="attachmentPath" /> parameter is used.</param>
        /// <exception cref="System.ArgumentException">The attachment path was null or empty.</exception>
        public MapiAttachment(string attachmentPath, string attachmentName)
            : this(attachmentPath)
        {
            this.Name = attachmentName;
        }

        /// <summary>
        /// Gets the the fully qualified path of the attached file. This path should include the disk drive letter and directory name.
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the attachment filename seen by the recipient, which may differ from the filename in the Path member if temporary files are being used.
        /// If the value is empty or null, the filename from the Path member is used.
        /// </summary>
        public string Name { get; private set; }
    }
}
