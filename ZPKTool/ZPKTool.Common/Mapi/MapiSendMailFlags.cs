﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// The flags that can be set in the flags parameter of the MAPISendMail function. 
    /// </summary>
    [Flags]
    public enum MapiSendMailFlags
    {
        /// <summary>
        /// A dialog box should be displayed to prompt the user to log on if required.
        /// If this flag is not set, the client application does not display a logon dialog box and returns an error value if the user is not logged on.
        /// If the MessageID parameter is empty, this flag is ignored.
        /// </summary>
        LogonUI = 0x00000001,

        /// <summary>
        /// An attempt is made to create a new session rather than acquire the environment's shared session. If this flag is not set, the function uses an existing,
        /// shared session. If you set the flag (preventing the use of a shared session) and the profile requires a password, you must also set the LogonUI flag
        /// or the function will fail. Your client application can avoid this failure by using the default profile without a password or by using an explicit profile without a password.
        /// </summary>
        NewSession = 0x00000002,

        /// <summary>
        /// A application modal dialog box should be displayed to prompt the user for recipients and other sending options.
        /// If MapiDialog is not set, at least one recipient must be specified.
        /// </summary>
        Dialog = 0x00000008,

        /// <summary>
        /// Do not convert the message to ANSI if the provider does not support Unicode.
        /// This flag is available for MAPISendMailW only.
        /// </summary>
        ForceUnicode = 0x00040000
    }
}
