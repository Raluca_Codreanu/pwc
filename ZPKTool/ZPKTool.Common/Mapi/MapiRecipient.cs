﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common.Mapi
{
    /// <summary>
    /// Represents a recipient of a MAPI Mail.
    /// </summary>
    public class MapiRecipient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapiRecipient" /> class.
        /// </summary>
        /// <param name="recipientAddress">The address of the recipient.</param>
        /// <param name="recipientType">Type of the recipient.</param>
        /// <exception cref="System.ArgumentException">The recipient address was null or empty.</exception>
        public MapiRecipient(string recipientAddress, MapiRecipientType recipientType)
        {
            if (string.IsNullOrWhiteSpace(recipientAddress))
            {
                throw new ArgumentException("The recipient address was null or empty.", "recipientAddress");
            }

            this.Address = recipientAddress;
            this.Type = recipientType;
        }

        /// <summary>
        /// Gets the address of the recipient.
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        /// Gets the type of the recipient.
        /// </summary>
        public MapiRecipientType Type { get; private set; }
    }
}
