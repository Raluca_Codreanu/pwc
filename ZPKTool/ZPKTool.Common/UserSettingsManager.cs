﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace ZPKTool.Common
{
    /// <summary>
    /// Keeps track of the application settings
    /// The properties of the application are kept as properties and retrieved using reflection
    /// Other fields are simple member and have specific get/set methods in order to avoid having to filter the app settings from the properties of the class
    /// </summary>
    public sealed class UserSettingsManager : GenericSettingsManager
    {
        #region Attributes

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly UserSettingsManager instance = new UserSettingsManager();

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="UserSettingsManager"/> class from being created.
        /// </summary>
        private UserSettingsManager()
        {
            // Initialize the settings file path.
            this.SettingsFilePath = Path.Combine(Constants.ApplicationDataFolderPath, "settings.xml");
        }

        #endregion Constructor

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>
        public static UserSettingsManager Instance
        {
            get { return instance; }
        }

        #region Setting properties

        /// <summary>
        /// Gets or sets the time interval between two consecutive checks for the connection to the central server. Time expressed in milliseconds
        /// </summary>
        [Setting]
        [DefaultSettingValue("60000")]
        public int CentralServerOnlineCheckInterval
        {
            get
            {
                return (int)this["CentralServerOnlineCheckInterval"];
            }

            set
            {
                this["CentralServerOnlineCheckInterval"] = value;
            }
        }

        /// <summary>
        /// Gets or sets an integer specifying the language in which the name of the raw materials is displayed
        /// </summary>
        [Setting]
        [DefaultSettingValue("0")]
        public int SearchRawMaterialLocalisedName
        {
            get
            {
                return (int)this["SearchRawMaterialLocalisedName"];
            }

            set
            {
                this["SearchRawMaterialLocalisedName"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the string in which the Boolean values for each option (true/false) in the search tool is concatenated
        /// </summary>
        [Setting]
        [DefaultSettingValue("")]
        public string SearchFiltersLocation
        {
            get
            {
                return (string)this["SearchFiltersLocation"];
            }

            set
            {
                this["SearchFiltersLocation"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the last user name which logged in the application
        /// </summary>
        [Setting]
        [DefaultSettingValue("")]
        public string LastLoginUserName
        {
            get
            {
                return (string)this["LastLoginUserName"];
            }

            set
            {
                this["LastLoginUserName"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the version and time stamp labels in certain screens.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool DisplayVersionAndTimestamp
        {
            get
            {
                return (bool)this["DisplayVersionAndTimestamp"];
            }

            set
            {
                this["DisplayVersionAndTimestamp"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the weight label on the header.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool HeaderDisplayWeight
        {
            get
            {
                return (bool)this["HeaderDisplayWeight"];
            }

            set
            {
                this["HeaderDisplayWeight"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the investment label on the header
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool HeaderDisplayInvestment
        {
            get
            {
                return (bool)this["HeaderDisplayInvestment"];
            }

            set
            {
                this["HeaderDisplayInvestment"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the pruchase price label on the header
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool HeaderDisplaysPurchasePrice
        {
            get
            {
                return (bool)this["HeaderDisplaysPurchasePrice"];
            }

            set
            {
                this["HeaderDisplaysPurchasePrice"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the target price label on the header
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool HeaderDisplaysTargetPrice
        {
            get
            {
                return (bool)this["HeaderDisplaysTargetPrice"];
            }

            set
            {
                this["HeaderDisplaysTargetPrice"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the calculation results.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool HeaderDisplayCalculationResult
        {
            get
            {
                return (bool)this["HeaderDisplayCalculationResult"];
            }

            set
            {
                this["HeaderDisplayCalculationResult"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the language of the user interface
        /// </summary>
        [Setting]
        [DefaultSettingValue("en-US")]
        public string UILanguage
        {
            get
            {
                return (string)this["UILanguage"];
            }

            set
            {
                this["UILanguage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the database location to load master data on the user preference
        /// </summary>
        [Setting]
        [DefaultSettingValue("1")]
        public int MasterDataBrowserDataSource
        {
            get
            {
                return (int)this["MasterDataBrowserDataSource"];
            }

            set
            {
                this["MasterDataBrowserDataSource"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the automatic update is on
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool AutomaticUpdatesOn
        {
            get
            {
                return (bool)this["AutomaticUpdatesOn"];
            }

            set
            {
                this["AutomaticUpdatesOn"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the difference between two update checks in days
        /// </summary>
        [Setting]
        [DefaultSettingValue("1")]
        public int AutomaticUpdateCheckPeriod
        {
            get
            {
                return (int)this["AutomaticUpdateCheckPeriod"];
            }

            set
            {
                this["AutomaticUpdateCheckPeriod"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the time of the last update check performed
        /// </summary>
        [Setting]
        [DefaultSettingValue("01/01/2012")]
        public DateTime AutomaticUpdateLastCheck
        {
            get
            {
                return (DateTime)this["AutomaticUpdateLastCheck"];
            }

            set
            {
                this["AutomaticUpdateLastCheck"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of decimal places used to display decimal number in the application
        /// </summary>
        [Setting]
        [DefaultSettingValue("2")]
        public int DecimalPlacesNumber
        {
            get
            {
                return (int)this["DecimalPlacesNumber"];
            }

            set
            {
                this["DecimalPlacesNumber"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the zoom level for every application window.
        /// </summary>
        [Setting]
        [DefaultSettingValue("1")]
        public double ZoomLevel
        {
            get
            {
                return (double)this["ZoomLevel"];
            }

            set
            {
                this["ZoomLevel"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the Capacity Utilization for machines.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool DisplayCapacityUtilization
        {
            get
            {
                return (bool)this["DisplayCapacityUtilization"];
            }

            set
            {
                this["DisplayCapacityUtilization"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the start screen.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool ShowStartScreen
        {
            get
            {
                return (bool)this["ShowStartScreen"];
            }

            set
            {
                this["ShowStartScreen"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the import confirmation screen.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool DontShowImportConfirmation
        {
            get
            {
                return (bool)this["DontShowImportConfirmation"];
            }

            set
            {
                this["DontShowImportConfirmation"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the empty subassemblies
        /// and empty parts in the project tree.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool HideEmptySubassembliesAndParts
        {
            get
            {
                return (bool)this["HideEmptySubassembliesAndParts"];
            }

            set
            {
                this["HideEmptySubassembliesAndParts"] = value;
            }
        }

        /// <summary>
        /// Gets or sets an integer specifying the default chart view mode
        /// </summary>
        [Setting]
        [DefaultSettingValue("1")]
        public int DefaultChartViewMode
        {
            get
            {
                return (int)this["DefaultChartViewMode"];
            }

            set
            {
                this["DefaultChartViewMode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the date when the license expiration warning was last shown.
        /// </summary>
        [Setting]
        [DefaultSettingValue("01/01/2012")]
        public DateTime LastLicenseExpirationWarningDate
        {
            get
            {
                return (DateTime)this["LastLicenseExpirationWarningDate"];
            }

            set
            {
                this["LastLicenseExpirationWarningDate"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the ID of the selected background color setting.
        /// </summary>
        [Setting]
        [DefaultSettingValue("0")]
        public int BackgroundColorID
        {
            get
            {
                return (int)this["BackgroundColorID"];
            }

            set
            {
                this["BackgroundColorID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the units system.
        /// </summary>
        [Setting]
        [DefaultSettingValue("0")]
        public UnitsSystem UnitsSystem
        {
            get
            {
                return (UnitsSystem)this["UnitsSystem"];
            }

            set
            {
                this["UnitsSystem"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the percentages on the waterfall chart columns.
        /// </summary>
        [Setting]
        [DefaultSettingValue("True")]
        public bool ShowPercentagesInWaterfallChart
        {
            get
            {
                return (bool)this["ShowPercentagesInWaterfallChart"];
            }

            set
            {
                this["ShowPercentagesInWaterfallChart"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the warnings in result details or not.
        /// </summary>
        [Setting]
        [DefaultSettingValue("False")]
        public bool ShowWarningsInResultDetails
        {
            get
            {
                return (bool)this["ShowWarningsInResultDetails"];
            }

            set
            {
                this["ShowWarningsInResultDetails"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the path to the custom logo image path.
        /// </summary>
        [Setting]
        [DefaultSettingValue("")]
        public string CustomLogoPath
        {
            get
            {
                return (string)this["CustomLogoPath"];
            }

            set
            {
                this["CustomLogoPath"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the symbol of the UI Currency selected in the Model Viewer.
        /// </summary>
        [Setting]
        [DefaultSettingValue("EUR")]
        public string ViewerUICurrencyIsoCode
        {
            get
            {
                return (string)this["ViewerUICurrencyIsoCode"];
            }

            set
            {
                this["ViewerUICurrencyIsoCode"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the map when calculating distance in transport cost calculator or not.
        /// </summary>
        [Setting]
        [DefaultSettingValue("true")]
        public bool ShowMapWindow
        {
            get
            {
                return (bool)this["ShowMapWindow"];
            }

            set
            {
                this["ShowMapWindow"] = value;
            }
        }

        #endregion Setting properties

        /// <summary>
        /// Loads the settings from the file specified by the <see cref="SettingsFilePath"/> property.
        /// If the file does not exist or is not a valid xml file, it is (re)created using the default values of the setting properties.
        /// </summary>
        public override void Load()
        {
            base.Load();

            // Correct some setting values that were broken by bugs (by bringing their values to the default value or in normal range).
            if (this.CorrectSettingValues())
            {
                this.Save();
            }
        }

        /// <summary>
        /// Corrects some setting values that were broken by bugs.
        /// </summary>
        /// <returns>true if at least one setting had to be corrected; otherwise returns false.</returns>
        private bool CorrectSettingValues()
        {
            bool settingsCorrected = false;

            // Correct the values of the AutomaticUpdateLastCheck and ZoomLevel settings that may have been incorrectly altered after the SettingManager
            // started to manage conversions in a culture-invariant way. For example if ZoomLevel was stored as 1,5 now will be converted to 15.
            if (this.ZoomLevel >= 5d)
            {
                // The number 5 is chosen above because the smallest value that can be miss-converted is 0,5 => 5
                this.ZoomLevel = 1d;
                settingsCorrected = true;
            }

            if (this.AutomaticUpdateLastCheck > DateTime.Now)
            {
                // The last update check date is in the future due to a culture related conversion issue that switched the places of month and day values (2012-01-05 became 2012-05-01).
                // What we do is set the last check date back so the check will be performed tomorrow.
                int days = this.AutomaticUpdateCheckPeriod >= 0 ? this.AutomaticUpdateCheckPeriod + 1 : 1;
                this.AutomaticUpdateLastCheck = DateTime.Now - TimeSpan.FromDays(days);
                settingsCorrected = true;
            }

            return settingsCorrected;
        }
    }
}