﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// Contains useful extension methods for the generic collection classes and interfaces (from the System.Collections.Generic namespace).
    /// </summary>    
    public static class GenericCollectionsExtensions
    {
        /// <summary>
        ///  Sorts the elements in a list in ascending order according to the specified key.
        /// </summary>
        /// <typeparam name="TSource">The type of elements in the list.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector"/>.</typeparam>
        /// <param name="source">The list to order.</param>
        /// <param name="keySelector">A function to extract a key from an element.</param>
        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "This is an extension method for List<T>.")]
        public static void Sort<TSource, TKey>(this List<TSource> source, Func<TSource, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;
            source.Sort((x, y) => comparer.Compare(keySelector(x), keySelector(y)));
        }

        /// <summary>
        ///  Sorts the elements in a list in descending order according to the specified key.
        /// </summary>
        /// <typeparam name="TSource">The type of elements in the list.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector"/>.</typeparam>
        /// <param name="source">The list to order.</param>
        /// <param name="keySelector">A function to extract a key from an element.</param>
        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "This is an extension method for List<T>.")]
        public static void SortDescending<TSource, TKey>(this List<TSource> source, Func<TSource, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;
            source.Sort((x, y) => comparer.Compare(keySelector(y), keySelector(x)));
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of the specified <see cref="ICollection{T}" /> instance.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="source">The collection in which to add elements.</param>
        /// <param name="collection">The collection from which to add elements.</param>
        public static void AddRange<T>(this ICollection<T> source, IEnumerable<T> collection)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "The source collection was null.");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "The collection was null.");
            }

            foreach (var item in collection)
            {
                source.Add(item);
            }
        }
    }
}
