﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// Extensions for the System.Collections.Generic.Queue class.
    /// </summary>
    public static class QueueExtensions
    {
        /// <summary>
        /// Adds a list of objects to the end of the System.Collections.Generic.Queue&lt;T&gt;. The objects are added in the order they are in the list.
        /// </summary>
        /// <typeparam name="T">The type of objects in the queue.</typeparam>
        /// <param name="queue">The queue in which to add.</param>
        /// <param name="items">The objects to add to the System.Collections.Generic.Queue&lt;T&gt;. The objects can be null for reference types.</param>
        public static void Enqueue<T>(this Queue<T> queue, IEnumerable<T> items)
        {
            if (queue == null || items == null)
            {
                return;
            }

            foreach (T item in items)
            {
                queue.Enqueue(item);
            }
        }
    }
}
