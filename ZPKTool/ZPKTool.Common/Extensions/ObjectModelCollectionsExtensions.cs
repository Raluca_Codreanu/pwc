﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Collections.ObjectModel
{
    /// <summary>
    /// Extensions for the Collection classes from the <see cref="System.Collections.ObjectModel"/> namespace.
    /// </summary>
    public static class ObjectModelCollectionsExtensions
    {
        /// <summary>
        /// Adds the elements of the specified collection to the end of the ObservableCollection.
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">The collection whose elements should be added to the end of the ObservableCollection. 
        /// The collection itself cannot be null, but it can contain elements that are null, if type T is a reference type.</param>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">Items is null.</exception>        
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection", "The collection was null.");
            }

            if (items == null)
            {
                throw new ArgumentNullException("items", "The items list was null.");
            }

            foreach (T item in items)
            {
                collection.Add(item);
            }
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of the Collection.
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection</typeparam>
        /// <param name="collection">The collection whose elements should be added to the end of the Collection. 
        /// The collection itself cannot be null, but it can contain elements that are null, if type T is a reference type.</param>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">Items is null.</exception>        
        public static void AddRange<T>(this Collection<T> collection, IEnumerable<T> items)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection", "The collection was null.");
            }

            if (items == null)
            {
                throw new ArgumentNullException("items", "The items list was null.");
            }

            foreach (T item in items)
            {
                collection.Add(item);
            }
        }
    }
}
