﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// Extension methods for the <see cref="Exception"/> class.
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Finds the 1st inner exception of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of the inner exception to be found.</typeparam>
        /// <param name="ex">The exception.</param>
        /// <returns>The inner exception of type <typeparamref name="T"/> if found, null otherwise.</returns>
        public static T FindInnerException<T>(this Exception ex) where T : Exception
        {
            T found = null;
            var parser = ex;
            while (parser != null)
            {
                var val = parser as T;
                if (val != null)
                {
                    found = val;
                    break;
                }

                parser = parser.InnerException;
            }

            return found;
        }
    }
}
