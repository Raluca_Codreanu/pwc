﻿namespace ZPKTool.Common
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Implements <see cref="System.ComponentModel.INotifyPropertyChanged"/> and has helper methods for raising the PropertyChanged event.
    /// </summary> 
    public class ObservableObject : INotifyPropertyChanged, INotifyPropertyChanging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableObject"/> class.
        /// </summary>
        public ObservableObject()
        {
        }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region INotifyPropertyChanging Members

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        #endregion

        /// <summary>
        /// Sets the backing field of a property to a specified value and raises the INotifyPropertyChanged.PropertyChanged event
        /// to indicate that the property's value has changed.
        /// </summary>
        /// <typeparam name="T">The type of the backing field and the property.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="property">The property.</param>        
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The member value is a lambda expression.")]
        protected void SetProperty<T>(ref T field, T value, Expression<Func<object>> property)
        {
            bool sameValue = (field == null && value == null)
                || (field != null && value != null && field.Equals(value));

            if (!sameValue)
            {
                this.OnPropertyChanging(property);
                field = value;
                this.OnPropertyChanged(property);
            }
        }

        /// <summary>
        /// Sets the backing field of a property to a specified value and raises the INotifyPropertyChanged.PropertyChanged event
        /// to indicate that the property's value has changed.
        /// </summary>
        /// <typeparam name="T">The type of the backing field and the property.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="property">The property.</param>
        /// <param name="afterPropertyChanged">The action executed after the property value has changed and the PropertyChanged event has been raised.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The member value is a lambda expression.")]
        protected void SetProperty<T>(ref T field, T value, Expression<Func<object>> property, Action afterPropertyChanged)
        {
            bool sameValue = (field == null && value == null)
                || (field != null && value != null && field.Equals(value));

            if (!sameValue)
            {
                this.OnPropertyChanging(property);
                field = value;
                this.OnPropertyChanged(property);
                if (afterPropertyChanged != null)
                {
                    afterPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Called when the value of a property has changed in order to raise the INotifyPropertyChanged.PropertyChanged event.
        /// </summary>
        /// <param name="property">The property that changed.</param>
        /// <remarks>
        /// This method is around 5 times slower that the version that takes the property name as a string (100.000 calls take ~2000ms vs. ~400ms for the other version).
        /// </remarks>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The type is required in order to pass a delegate as parameter."),]
        protected void OnPropertyChanged(Expression<Func<object>> property)
        {
            this.OnPropertyChanged(ReflectionUtils.GetPropertyName(property));
        }

        /// <summary>
        /// Called when the value of a property has changed in order to raise the INotifyPropertyChanged.PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>        
        protected void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Called when the value of a property is changing in order to raise the INotifyPropertyChanging.PropertyChanging event.
        /// </summary>
        /// <param name="property">The property that is changing.</param>
        /// <remarks>
        /// This method is around 5 times slower that the version that takes the property name as a string (100.000 calls take ~2000ms vs. ~400ms for the other version).
        /// </remarks>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The type is required in order to pass a delegate as parameter."),]
        protected void OnPropertyChanging(Expression<Func<object>> property)
        {
            this.OnPropertyChanging(ReflectionUtils.GetPropertyName(property));
        }

        /// <summary>
        /// Called when the value of a property is changing in order to raise the INotifyPropertyChanging.PropertyChanging event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>        
        protected void OnPropertyChanging(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            var handler = this.PropertyChanging;
            if (handler != null)
            {
                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Warns the developer if this object does not have a public property with the specified name. This method does not exist in a Release build.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [Conditional("DEBUG")]
        private void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real, public, instance property on this object.
            if (this.GetType().GetProperties().FirstOrDefault(prop => prop.Name == propertyName) == null)
            {
                string msg = "Invalid property name: " + propertyName;
                throw new ZPKException(string.Empty, msg);
            }
        }
    }
}
