﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ZPKTool.Common
{
    /// <summary>
    /// Helper methods for validations.
    /// </summary>
    public static class ValidationUtils
    {
        /// <summary>
        /// Determines whether the specified string represets a valid email address.
        /// </summary>
        /// <param name="input">The input test.</param>
        /// <returns>
        /// <c>true</c> if the specified input is an email address; otherwise, <c>false</c>.
        /// </returns>      
        public static bool IsEmail(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            var regex = new Regex(Constants.EmailValidationRegex);
            return regex.IsMatch(input);
        }
    }
}
