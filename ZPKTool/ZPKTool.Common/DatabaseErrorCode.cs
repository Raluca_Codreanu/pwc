﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// This class contains the codes of database related errors.
    /// </summary>
    //// TODO: this class should be placed in DataAccess and should probably be internal (there's no real reason for other layers to access these error codes).
    public static class DatabaseErrorCode
    {
        /// <summary>
        /// Entity Framework error that is not handled by us.
        /// </summary>
        public const string Unknown = "Error_Internal";

        /// <summary>
        /// The connection to the db server is not possible (server name is incorrect).
        /// The error is identified by the SQL exception number.
        /// </summary>
        public const string NoConnection = "Database_NoConnection";

        /// <summary>
        /// Timeout expired. The timeout period elapsed prior to completion of the operation or the server is not responding.
        /// The error is identified by the SQL exception number.
        /// </summary>
        public const string TimeoutExpired = "Database_TimeoutExpired";

        /// <summary>
        /// SQL server Deadlock occurred.
        /// </summary>
        public const string Deadlock = "Database_Deadlock";

        /// <summary>
        /// SQL server optimistic concurrency.
        /// </summary>
        public const string OptimisticConcurrency = "Database_OptimisticConcurrency";

        /// <summary>
        /// SQL server arithmetic overflow occurred.
        /// </summary>
        public const string ArithmeticOverflow = "Database_Overflow";

        /// <summary>
        /// An Optimistic concurrency exception was auto-handled.
        /// </summary>
        public const string AutoHandledOptimisticConcurency = "Database_AutohandledOptimisticConcurency";

        /// <summary>
        /// The fix concurrency failed.
        /// </summary>
        public const string FixConcurrencyError = "Database_FixConcurrencyError";

        /// <summary>
        /// Error occurs when an entity can not be deleted because it is used in a foreign key relation.
        /// </summary>
        public const string ForeignKeyViolation = "Error_CantDeleteEntityIsUsed";

        /// <summary>
        /// Database Version Mismatch - need to Update the Local application.
        /// </summary>
        public const string LocalDatabaseVersionMismatch = "AppDatabaseVersionMismatch";

        /// <summary>
        ///  Database Version Mismatch - need to Update the Central database.
        /// </summary>
        public const string CentralDatabaseVersionMismatch = "CentralDatabaseVersionMismatch";

        /// <summary>
        /// Invalid database schema.
        /// </summary>
        public const string InvalidSchema = "DatabaseSchemaInvalid";

        /// <summary>
        /// The SQL server account used by the application for db access is disabled.
        /// </summary>
        public const string UserAccountDisabled = "DatabaseServerAccountDisabled";

        /// <summary>
        /// Logging into the SQL server has failed.
        /// </summary>
        public const string LoginFailed = "DatabaseLoginFailed";

        /// <summary>
        /// Error occurs when trying to insert or update the ISO code of a master data currency and the value to insert or update already exists.
        /// </summary>
        public const string DuplicateCurrencyIsoCode = "Currency_IsoCode_AlreadyExists";
    }
}
