﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Contains utility methods that help with performing operations by using reflection.
    /// </summary>
    public static class ReflectionUtils
    {
        /// <summary>
        /// Gets the property that is accessed using a lambda expression.
        /// <para />
        /// This method should replace the need for code like
        /// <code>
        /// myObject.GetType().GetProperty("PropertyName");
        /// </code>        
        /// </summary>
        /// <param name="propertyAccessExpr">A lambda expression that accesses the desired property.</param>
        /// <returns>The <see cref="PropertyInfo"/> instance for the accessed property.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="propertyAccessExpr"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyAccessExpr"/> was empty or did not contain a property</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The type is required in order to pass a delegate as parameter."),]
        public static PropertyInfo GetProperty(Expression<Func<object>> propertyAccessExpr)
        {
            var propInfo = GetAccessedMember(propertyAccessExpr) as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException("The lambda expression did not contain a property definition.", "propertyAccessExpr");
            }

            return propInfo;
        }

        /// <summary>
        /// Gets the property that is accessed using a lambda expression.
        /// <para />
        /// This method should replace the need for code like
        /// <code>
        /// myObject.GetType().GetProperty("PropertyName");
        /// </code>
        /// </summary>
        /// <typeparam name="T">The type that defines the property accessed by the lambda expression.</typeparam>
        /// <param name="propertyAccessExpr">A lambda expression that accesses the desired property.</param>
        /// <returns>
        /// The <see cref="PropertyInfo" /> instance for the accessed property.
        /// </returns>
        /// <exception cref="System.ArgumentException">The lambda expression did not contain a property definition.;propertyExpression</exception>
        /// <exception cref="ArgumentNullException"><paramref name="propertyAccessExpr" /> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyAccessExpr" /> was empty or did not contain a property</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The type is required in order to pass a delegate as parameter."),]
        public static PropertyInfo GetProperty<T>(Expression<Func<T, object>> propertyAccessExpr)
        {
            var propInfo = GetAccessedMember(propertyAccessExpr) as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException("The lambda expression did not contain a property definition.", "propertyAccessExpr");
            }

            return propInfo;
        }

        /// <summary>
        /// Gets the name of the property specified by the lambda expression.
        /// <para />
        /// Use this method to avoid hard-coding property names.
        /// </summary>
        /// <param name="propertyExpression">The lambda expression accessing the desired property.</param>
        /// <returns>The name of the property.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="propertyExpression"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyExpression"/> was empty or did not contain a property</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        public static string GetPropertyName(Expression<Func<object>> propertyExpression)
        {
            return GetProperty(propertyExpression).Name;
        }

        /// <summary>
        /// Gets the name of the property specified by the lambda expression.
        /// <para />
        /// Use this method to avoid hard-coding property names.
        /// </summary>
        /// <typeparam name="T">The type that defines the desired property.</typeparam>
        /// <param name="propertyExpression">The lambda expression accessing the desired property.</param>
        /// <returns>
        /// The name of the property.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="propertyExpression" /> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyExpression" /> was empty or did not contain a property</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        public static string GetPropertyName<T>(Expression<Func<T, object>> propertyExpression)
        {
            return GetProperty(propertyExpression).Name;
        }

        /// <summary>
        /// Gets the value of a specified property from a specified object.
        /// </summary>
        /// <typeparam name="T">The type of the object from which to get the property value.</typeparam>
        /// <param name="obj">The object from which to get the property value.</param>
        /// <param name="propertyExpression">The lambda expression that accesses the property whose value to get.</param>
        /// <returns>
        /// The value of the property.
        /// </returns>
        /// <exception cref="System.ArgumentException">The property expression contained a property that does not belong to the specified object.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="propertyExpression" /> was null.</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyExpression" /> was empty or did not contain a property.</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        public static object GetPropertyValue<T>(T obj, Expression<Func<T, object>> propertyExpression)
        {
            var propInfo = GetProperty(propertyExpression);
            return propInfo.GetValue(obj, null);
        }

        /// <summary>
        /// Gets the value of a simple property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="propertyName">The name  of the property.</param>
        /// <returns>The value of the property</returns>
        public static object GetSimplePropertyValue(object obj, string propertyName)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The input object was null.");
            }

            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("Property name was null or empty.", "propertyName");
            }

            Type type = obj.GetType();
            System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
            if (pi != null && pi.CanRead)
            {
                return pi.GetValue(obj, null);
            }

            return null;
        }

        /// <summary>
        /// Returns the value of a complex property.
        /// A complex property is a property of a sub-object belonging to the graph of the input object.
        /// "Project.Assembly.AssemblingCountry.Name" is a complex property and requests the value of the "Name" property.
        /// </summary>
        /// <param name="obj">The object where to search for the property.</param>
        /// <param name="complexProperty">The property to get its value.</param>
        /// <returns>The value of the property.</returns>
        public static object GetComplexPropertyValue(object obj, string complexProperty)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The input object was null.");
            }

            if (string.IsNullOrEmpty(complexProperty))
            {
                throw new ArgumentException("The complex property was null or empty.", "complexProperty");
            }

            object result = null;
            List<string> properties = null;

            // check if we have a complex property
            if (complexProperty.Contains("."))
            {
                properties = complexProperty.Split('.').ToList();
            }
            else
            {
                properties = new List<string>() { complexProperty };
            }

            object crtValue = obj;
            for (int i = 0; i < properties.Count; i++)
            {
                string propName = properties[i];
                crtValue = GetSimplePropertyValue(crtValue, propName);
                if (i == properties.Count - 1)
                {
                    result = crtValue;
                }
                else if (crtValue == null)
                {
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the member of a type accessed by the specified lambda expression.
        /// </summary>
        /// <param name="expression">The lambda expression representing the member access.</param>
        /// <returns>A <see cref="MemberInfo"/> instance representing the accessed member or null if the expression did not access a member of a type.</returns>
        /// <exception cref="System.ArgumentNullException">The lambda expression was null.</exception>
        /// <exception cref="System.ArgumentException">The lambda expression's body was empty.</exception>
        public static MemberInfo GetAccessedMember(LambdaExpression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression", "The lambda expression was null.");
            }

            if (expression.Body == null)
            {
                throw new ArgumentException("The lambda expression's body was empty.", "expression");
            }

            // Extract the property name from the linq expression
            MemberInfo memberInfo = null;
            Expression body = expression.Body;
            if (body.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression mexp = body as MemberExpression;
                if (mexp != null)
                {
                    memberInfo = mexp.Member;
                }
            }
            else if (body.NodeType == ExpressionType.Convert)
            {
                UnaryExpression exp = body as UnaryExpression;
                if (exp != null
                    && exp.Operand != null
                    && exp.Operand.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression mexp = exp.Operand as MemberExpression;
                    if (mexp != null)
                    {
                        memberInfo = mexp.Member;
                    }
                }
            }

            return memberInfo;
        }
    }
}
