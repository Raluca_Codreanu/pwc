﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// This class is the base class for the exception types used in the application
    /// </summary>
    [Serializable]
    public class ZPKException : System.Exception
    {
        /// <summary>
        /// The parameters to format the error message.
        /// </summary>
        private ICollection<object> errorMessageFormatArgs = new List<object>();

        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        public ZPKException()
            : base(string.Empty)
        {
            ErrorCode = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        public ZPKException(string errorCode)
            : base(string.Empty)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        /// <param name="errorMessage">The error message</param>
        public ZPKException(string errorCode, string errorMessage)
            : base(errorMessage)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The base exception</param>
        public ZPKException(string errorCode, string errorMessage, Exception ex)
            : base(errorMessage, ex)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="exception">The base ZPKException</param>
        public ZPKException(ZPKException exception)
            : base(exception.Message, exception)
        {
            ErrorCode = exception.ErrorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public ZPKException(string errorCode, Exception ex)
            : base(string.Empty, ex)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZPKException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected ZPKException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.ErrorCode = info.GetString("ErrorCode");
            this.ErrorMessageFormatArgs = (ICollection<object>)info.GetValue("ErrorMessageFormatArgs", typeof(ICollection<object>));
        }

        #endregion Initialization

        #region Properties

        /// <summary>
        /// Gets or sets the error code of the exception.
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets the parameters to format the error message.
        /// </summary>
        public ICollection<object> ErrorMessageFormatArgs
        {
            get
            {
                return this.errorMessageFormatArgs;
            }

            private set
            {
                if (this.errorMessageFormatArgs != value)
                {
                    this.errorMessageFormatArgs = value;
                }
            }
        }

        #endregion Properties

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("ErrorCode", this.ErrorCode);
            info.AddValue("ErrorMessageFormatArgs", this.ErrorMessageFormatArgs);
        }
    }
}
