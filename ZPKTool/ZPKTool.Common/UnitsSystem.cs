﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Represents the units system.
    /// </summary>
    public enum UnitsSystem
    {
        /// <summary>
        /// This item represents the metric units system.
        /// </summary>
        Metric = 0,

        /// <summary>
        /// This item represents the imperial units system.
        /// </summary>
        Imperial = 1
    }
}