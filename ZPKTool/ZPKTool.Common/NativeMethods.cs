﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Contains imports of native Win32 methods.
    /// </summary>
    internal static class NativeMethods
    {
        /// <summary>
        /// Sends a message using the MAPI windows API.
        /// </summary>
        /// <param name="sessionHandle">Handle to a Simple MAPI session or zero. If is zero it creates a session that exists only for the duration of the call.</param>
        /// <param name="windowHandle">Parent window handle or zero. If is non-zero and MAPI displays a dialog box, the window identified by this handle will be its parent.</param>
        /// <param name="message">The structure containing the message.</param>
        /// <param name="flags">Bitmask of option flags.</param>
        /// <param name="reserved">Reserved; must be zero.</param>
        /// <returns>
        /// A value indicating whether the call has succeeded. If the call has failed, the value indicates the error.
        /// The member <see cref="mapiReturnCodeDescriptions"/> contains the description of every returned value.
        /// </returns>
        [DllImport("MAPI32.DLL", CharSet = CharSet.Ansi)]
        internal static extern int MAPISendMail(IntPtr sessionHandle, IntPtr windowHandle, ref MapiMessage message, int flags, int reserved);

        /// <summary>
        /// Contains information about a message to send using MAPISendMail.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        internal struct MapiMessage
        {
            /// <summary>
            /// Reserved; must be zero.
            /// </summary>
            public int Reserved;

            /// <summary>
            /// The text describing the message subject, typically limited to 256 characters or less.
            /// </summary>
            public string Subject;

            /// <summary>
            /// The message text.
            /// </summary>
            public string NoteText;

            /// <summary>
            /// A string indicating a non-IPM type of message.
            /// Client applications can select message types for their non-IPM messages. Clients that only support IPM messages can ignore
            /// this member when reading messages and set it to empty or null when sending messages.
            /// </summary>
            public string MessageType;

            /// <summary>
            /// A string indicating the date when the message was received. The format is YYYY/MM/DD HH:MM, using a 24-hour clock.
            /// </summary>
            public string DateReceived;

            /// <summary>
            /// A string identifying the conversation thread to which the message belongs. Some messaging systems can ignore and not return this member.
            /// </summary>
            public string ConversationID;

            /// <summary>
            /// Bitmask of message status flags. The following flags can be set.
            ///     - MAPI_RECEIPT_REQUESTED (0x00000002): A receipt notification is requested. Client applications set this flag when sending a message.
            ///     - MAPI_SENT (0x00000004): The message has been sent.
            ///     - MAPI_UNREAD (0x00000001): The message has not been read.
            /// </summary>
            public int Flags;

            /// <summary>
            /// Pointer to a <see cref="MapiRecipientDescription"/> structure containing information about the sender of the message. 
            /// </summary>
            public IntPtr Originator;

            /// <summary>
            /// The number of message recipient structures in the array pointed to by the Recipients member.
            /// A value of zero indicates no recipients are included.
            /// </summary>
            public int RecipientsCount;

            /// <summary>
            /// Pointer to an array of <see cref="MapiRecipientDescription"/> structures, each containing information about a message recipient.
            /// </summary>
            public IntPtr Recipients;

            /// <summary>
            /// The number of structures describing file attachments in the array pointed to by the Files member.
            /// A value of zero indicates no file attachments are included.
            /// </summary>
            public int FileCount;

            /// <summary>
            /// Pointer to an array of <see cref="MapiFileDescription"/> structures, each containing information about a file attachment.
            /// </summary>
            public IntPtr Files;
        }

        /// <summary>
        /// Contains information about a message sender or recipient.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        internal struct MapiRecipientDescription
        {
            /// <summary>
            /// Reserved; must be zero.
            /// </summary>
            public int Reserved;

            /// <summary>
            /// Contains a numeric value that indicates the type of recipient. See the <see cref="MapiRecipientType"/> enumeration for possible values.
            /// </summary>
            public int RecipientClass;

            /// <summary>
            /// The display name of the message recipient or sender.
            /// </summary>
            public string Name;

            /// <summary>
            /// Optionally, specifies the recipient or sender's address; this address is provider-specific message delivery data.
            /// Generally, the messaging system provides such addresses for inbound messages. For outbound messages, this member can point to an address entered by the user for a recipient not in an address book (that is, a custom recipient).
            /// The format of the address is address type:email address. Examples of valid addresses are FAX:206-555-1212 and SMTP:M@X.COM.
            /// </summary>
            public string Address;

            /// <summary>
            /// The size, in bytes, of the entry identifier pointed to by the EntryID member.
            /// </summary>
            public int EntryIDSize;

            /// <summary>
            /// Pointer to an opaque entry identifier used by a messaging system service provider to identify the message recipient.
            /// Entry identifiers have meaning only for the service provider; client applications will not be able to decipher them.
            /// The messaging system uses this member to return valid entry identifiers for all recipients or senders listed in the address book.
            /// </summary>
            public IntPtr EntryID;
        }

        /// <summary>
        /// Contains information about a file containing a message attachment stored as a temporary file.
        /// That file can contain a static OLE object, an embedded OLE object, an embedded message, and other types of files.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        internal struct MapiFileDescription
        {
            /// <summary>
            /// Reserved; must be zero.
            /// </summary>
            public int Reserved;

            /// <summary>
            /// Bitmask of attachment flags. The following flags can be set.
            ///     - MAPI_OLE (0x00000001): The attachment is an OLE object. If MAPI_OLE_STATIC is also set, the attachment is a static OLE object. If MAPI_OLE_STATIC is not set, the attachment is an embedded OLE object.
            ///     - MAPI_OLE_STATIC (0x00000002): The attachment is a static OLE object.
            /// </summary>
            public int Flags;

            /// <summary>
            /// An integer used to indicate where in the message text to render the attachment. Attachments replace the character found at a certain position
            /// in the message text. That is, attachments replace the character in the MapiMessage structure field NoteText[nPosition].
            /// A value of -1 (0xFFFFFFFF) means the attachment position is not indicated; the client application will have to provide a way for the user to access the attachment.
            /// </summary>
            public int Position;

            /// <summary>
            /// The fully qualified path of the attached file. This path should include the disk drive letter and directory name.
            /// </summary>
            public string Path;

            /// <summary>
            /// The attachment filename seen by the recipient, which may differ from the filename in the Path member if temporary files are being used.
            /// If the value is empty or null, the filename from the Path member is used.
            /// </summary>
            public string Name;

            /// <summary>
            /// Pointer to the attachment file type, which can be represented with a MapiFileTagExt structure.
            /// A value of null indicates an unknown file type or a file type determined by the operating system.
            /// </summary>
            public IntPtr Type;
        }
    }
}
