﻿namespace ZPKTool.Common
{
    using System.ComponentModel;

    /// <summary>
    /// WeakEventManager implementation for the PropertyChanging event of the INotifyPropertyChanging.
    /// </summary>
    public class PropertyChangingEventManager : WeakEventManagerBase<PropertyChangingEventManager, INotifyPropertyChanging>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(INotifyPropertyChanging source)
        {
            source.PropertyChanging += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(INotifyPropertyChanging source)
        {
            source.PropertyChanging -= DeliverEvent;
        }
    }
}
