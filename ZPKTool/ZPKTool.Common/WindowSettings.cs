﻿namespace ZPKTool.Common
{
    using System.Windows;

    /// <summary>
    /// A class that keeps some settings fow a ZPK window.
    /// </summary>
    public class WindowSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the window identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the window top property.
        /// </summary>
        public double? Top { get; set; }

        /// <summary>
        /// Gets or sets the window left property.
        /// </summary>
        public double? Left { get; set; }

        /// <summary>
        /// Gets or sets the window width property.
        /// </summary>
        public double? Width { get; set; }

        /// <summary>
        /// Gets or sets the window height property.
        /// </summary>
        public double? Height { get; set; }

        /// <summary>
        /// Gets or sets the state of the window.
        /// </summary>
        public WindowState WindowState { get; set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowSettings"/> class.
        /// </summary>
        /// <param name="id">The window id.</param>
        /// <param name="width">The window width.</param>
        /// <param name="height">The window height.</param>
        /// <param name="windowState">State of the window.</param>
        public WindowSettings(string id, double? width, double? height, WindowState windowState)
        {
            this.Id = id;
            this.WindowState = windowState;
            this.Width = width;
            this.Height = height;
        }

        #endregion Constructor
    }
}
