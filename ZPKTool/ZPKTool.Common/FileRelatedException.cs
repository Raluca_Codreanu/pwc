﻿using System;
using System.Runtime.Serialization;

namespace ZPKTool.Common
{
    /// <summary>
    /// Exception thrown by the DataAccess layer
    /// </summary>
    [Serializable]
    public class FileRelatedException : ZPKException
    {
        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        public FileRelatedException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public FileRelatedException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        public FileRelatedException(string errorCode, string errorMessage)
            : base(errorCode, errorMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The exception object.</param>
        public FileRelatedException(string errorCode, string errorMessage, Exception ex)
            : base(errorCode, errorMessage, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception object.</param>
        public FileRelatedException(string errorCode, Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRelatedException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected FileRelatedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion Initialization
    }
}