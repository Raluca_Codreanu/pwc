﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.Common
{
    /// <summary>
    /// WeakEventManager with AddListener/RemoveListener and CurrentManager implementation.
    /// Helps implementing the WeakEventManager pattern with less code.
    /// </summary>
    /// <typeparam name="TManager">The type of the manager.</typeparam>
    /// <typeparam name="TEventSource">The type of the event source.</typeparam>
    public abstract class WeakEventManagerBase<TManager, TEventSource> : WeakEventManager
        where TManager : WeakEventManagerBase<TManager, TEventSource>, new()
        where TEventSource : class
    {
        /// <summary>
        /// Gets the current manager.
        /// </summary>
        protected static TManager CurrentManager
        {
            get
            {
                Type managerType = typeof(TManager);
                TManager manager = (TManager)GetCurrentManager(managerType);
                if (manager == null)
                {
                    manager = new TManager();
                    SetCurrentManager(managerType, manager);
                }

                return manager;
            }
        }

        /// <summary>
        /// Adds a weak event listener.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="listener">The listener.</param>
        [SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "This syntax is easier for registering a listener.")]
        public static void AddListener(TEventSource source, IWeakEventListener listener)
        {
            CurrentManager.ProtectedAddListener(source, listener);
        }

        /// <summary>
        /// Removes a weak event listener.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="listener">The listener.</param>
        [SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "This syntax is easier for removing a listener.")]
        public static void RemoveListener(TEventSource source, IWeakEventListener listener)
        {
            CurrentManager.ProtectedRemoveListener(source, listener);
        }

        /// <summary>
        /// When overridden in a derived class, starts listening for the event being managed. After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)"/>  is first called, the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)"/> or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)"/> whenever the relevant event from the provided source is handled.
        /// </summary>
        /// <param name="source">The source to begin listening on.</param>
        protected sealed override void StartListening(object source)
        {
            StartListening((TEventSource)source);
        }

        /// <summary>
        /// When overridden in a derived class, stops listening on the provided source for the event being managed.
        /// </summary>
        /// <param name="source">The source to stop listening on.</param>
        protected sealed override void StopListening(object source)
        {
            StopListening((TEventSource)source);
        }

        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected abstract void StartListening(TEventSource source);

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected abstract void StopListening(TEventSource source);
    }
}
