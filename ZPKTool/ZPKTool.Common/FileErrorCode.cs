﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Errors related to operations on files and directories.
    /// </summary>
    public static class FileErrorCode
    {
        /// <summary>
        /// The directory was not found.
        /// </summary>
        public const string DirectoryNotFound = "Error_DirectoryNotFound";

        /// <summary>
        /// The drive was not found.
        /// </summary>
        public const string DriveNotFound = "Error_DriveNotFound";

        /// <summary>
        /// The file was not found.
        /// </summary>
        public const string FileNotFound = "Error_FileNotFound";

        /// <summary>
        /// The was a security error when a file/directory operation was attempted.
        /// </summary>
        public const string Security = "Error_Security";

        /// <summary>
        /// The path was too long (exceeded the maximum supported by the OS).
        /// </summary>
        public const string PathTooLong = "Error_PathTooLong";

        /// <summary>
        /// The access to the file/directory was not allowed.
        /// </summary>
        public const string UnauthorizedAccess = "General_FileAccessError";

        /// <summary>
        /// Represents an error when attempting an operation on files/directories, other than the errors enumerated in this class.
        /// </summary>
        public const string IOError = "General_FileIOError";
    }
}
