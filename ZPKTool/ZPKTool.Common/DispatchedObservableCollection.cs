﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Threading;

    /// <summary>
    /// This is an ObservableCollection that fires its CollectionChanged event on the UI thread.
    /// This class should be used when an ObservableCollection bound to an ItemsControl is changed from a background thread.
    /// </summary>
    /// <typeparam name="T">The type of elements in the collection</typeparam>
    public class DispatchedObservableCollection<T> : ObservableCollection<T>
    {
        /// <summary>
        /// A flag used to suppress the OnCollectionChanged event notification while adding a batch of items to the collection.
        /// </summary>
        private bool suppressOnCollectionChangedNotification;

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatchedObservableCollection&lt;T&gt;"/> class.
        /// </summary>
        public DispatchedObservableCollection()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatchedObservableCollection&lt;T&gt;"/> class that contains elements copied from the specified collection.
        /// </summary>
        /// <param name="collection">The collection from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception>
        public DispatchedObservableCollection(IEnumerable<T> collection)
            : base(collection)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatchedObservableCollection&lt;T&gt;"/> class that contains elements copied from the specified list.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        /// <exception cref="System.ArgumentNullException">The list parameter cannot be null.</exception>
        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "The construct or based on that of the base class.")]
        public DispatchedObservableCollection(List<T> list)
            : base(list)
        {
        }

        /// <summary>
        /// Occurs when an item is added, removed, changed, moved, or the entire list is refreshed.
        /// </summary>
        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Raises the <see cref="E:CollectionChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            using (this.BlockReentrancy())
            {
                // Don't raise the event event if suppression is enabled.
                if (this.suppressOnCollectionChangedNotification)
                {
                    return;
                }

                var evt = this.CollectionChanged;
                if (evt != null)
                {
                    var dispatcher = Application.Current != null ? Application.Current.Dispatcher : null;

                    var handlers = evt.GetInvocationList();
                    foreach (NotifyCollectionChangedEventHandler handler in handlers)
                    {
                        NotifyCollectionChangedEventArgs eventArgs = null;
                        var collectionViewHandler = handler.Target as System.ComponentModel.ICollectionView;
                        if (collectionViewHandler != null
                            && ((e.NewItems != null && e.NewItems.Count > 1) || (e.OldItems != null && e.OldItems.Count > 1)))
                        {
                            // ICollectionView implementations do not support multiple items provided by the event (they throw exception) so send them the Reset event 
                            // instead of the Add or Remove event with multiple NewItems or OldItems. Sending them the Reset event causes them to recreate their view,
                            // which is usually the normal behavior when Clearing the collection or adding multiple items to it.
                            eventArgs = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
                        }
                        else
                        {
                            // To other listeners, send the original event.
                            eventArgs = e;
                        }

                        // Dispatch calls from other threads to the UI thread to avoid an exception thrown
                        // by the CollectionView associated with this collection on the UI.                    
                        if (dispatcher != null && !dispatcher.CheckAccess())
                        {
                            dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, eventArgs);
                        }
                        else
                        {
                            handler(this, eventArgs);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clears the items.
        /// </summary>
        protected override void ClearItems()
        {
            var oldItems = this.Items.ToList().AsReadOnly();

            try
            {
                this.suppressOnCollectionChangedNotification = true;
                base.ClearItems();
            }
            finally
            {
                this.suppressOnCollectionChangedNotification = false;
            }

            if (oldItems.Count > 0)
            {
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, oldItems, 0));
            }
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of the collection.
        /// <para />
        /// The OnCollectionChanged event is triggered only once, after all items have been added to the collection.
        /// </summary>                
        /// <param name="items">The items to add.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="items"/> is null.</exception>        
        public void AddRange(IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "The items list can not be null.");
            }

            this.CheckReentrancy();

            this.suppressOnCollectionChangedNotification = true;
            var tempItems = items.ToList();
            var insertionIndex = this.Count;

            try
            {
                foreach (T item in tempItems)
                {
                    this.Add(item);
                }
            }
            finally
            {
                this.suppressOnCollectionChangedNotification = false;
            }

            if (tempItems.Count > 0)
            {
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, tempItems.AsReadOnly(), insertionIndex));
            }
        }
    }
}
