﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// The data necessary to connect to a database.
    /// </summary>
    public class DbConnectionInfo
    {
        /// <summary>
        /// Gets or sets the address of the SQL Server to connect to.
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// Gets or sets the name of the database to work with.
        /// </summary>
        public string InitialCatalog { get; set; }

        /// <summary>
        /// Gets or sets the user name to use for connecting to the SQL Server.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the password to use for connecting to the SQL Server.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the encryption key used to decrypt the password. It should be set to null or Empty if the password is not encrypted.
        /// </summary>
        public string PasswordEncryptionKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication instead of username/password authentication when connecting to the SQL Server.
        /// </summary>
        public bool UseWindowsAuthentication { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use SSL support.
        /// </summary>
        public bool UseSSLSupport { get; set; }
  
        /// <summary>
        /// Gets or sets a value indicating whether the connection should enable pooling.
        /// </summary>
        public bool Pooling { get; set; }

        /// <summary>
        /// Gets or sets the connection timeout (in seconds). A null value indicate to use the default timeout.
        /// </summary>        
        public int? ConnectionTimeout { get; set; }
    }
}
