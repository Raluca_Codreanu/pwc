﻿namespace ZPKTool.Common
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// A class that handles loading, saving and storing of the application windows settings.
    /// </summary>
    public class WindowsSettingsManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// An object used to lock the file while reading / writing.
        /// </summary>
        private static readonly object ThisLock = new object();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly WindowsSettingsManager instance = new WindowsSettingsManager();

        /// <summary>
        /// The path to the xml file which holds the windows settings.
        /// </summary>
        private string windowsSettingsPath;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsSettingsManager"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The application should not throw errors if for some reason an error occurs when processing windows settings file.")]
        public WindowsSettingsManager()
        {
            try
            {
                var settingsFolder = Constants.ApplicationDataFolderPath;
                if (settingsFolder != null)
                {
                    this.windowsSettingsPath = Path.Combine(settingsFolder, "WindowsSettings.xml");
                    if (!File.Exists(this.windowsSettingsPath))
                    {
                        Log.Info(
                            "The application windows settings file '{0}' does not exist. The file will be created with default setting values.",
                            this.windowsSettingsPath);
                        this.CreateSettingsFile();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorException("An error occurred during WindowsSettingsManager initialization.", ex);
            }
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>
        public static WindowsSettingsManager Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets or sets the path to the xml file which holds the settings.
        /// <para />
        /// Setting this property does not trigger the loading of the settings. The Load method must be explicitly called for that.
        /// </summary>
        /// <exception cref="ArgumentException">The path was null, empty or whitespace.</exception>
        public string WindowsSettingsPath
        {
            get
            {
                return this.windowsSettingsPath;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("The windows settings file path can't be null, empty or whitespace.");
                }

                this.windowsSettingsPath = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets the settings for the window with the name passed as parameter.
        /// </summary>
        /// <param name="windowId">The Uid of the window.</param>
        /// <returns>The founded window settings. 
        /// Null if the window with the specified name does not have some settings saved yet.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The application should not throw errors if for some reason an error occurs when processing windows settings file.")]
        public WindowSettings GetWindowSettings(string windowId)
        {
            try
            {
                lock (ThisLock)
                {
                    // Load the settings file content.
                    var doc = XDocument.Load(this.windowsSettingsPath);

                    var settingsElement =
                        doc.Descendants("WindowsSettings")
                            .Elements("WindowSettings")
                            .FirstOrDefault(x => x.Attribute("Id") != null && x.Attribute("Id").Value == windowId);

                    var windowSettings = this.ReadSettings(settingsElement);
                    return this.SizeToFit(windowSettings);
                }
            }
            catch (XmlException ex)
            {
                Log.ErrorException("Windows settings file had to be recreated due to invalid xml format.", ex);
            }
            catch (Exception ex)
            {
                Log.ErrorException(
                    "Windows settings file could not be processed due to an error that occurred.",
                    ex);
            }

            return null;
        }

        /// <summary>
        /// Saves the settings for the window with the name passed as parameter.
        /// </summary>
        /// <param name="windowSettings">The window settings.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The application should not throw errors if for some reason an error occurs when processing windows settings file.")]
        public void Save(WindowSettings windowSettings)
        {
            try
            {
                if (windowSettings != null && windowSettings.WindowState != WindowState.Minimized)
                {
                    lock (ThisLock)
                    {
                        // Load the settings file content.
                        var doc = XDocument.Load(this.windowsSettingsPath);
                        var settingsElement =
                            doc.Descendants("WindowsSettings")
                                .Elements("WindowSettings")
                                .FirstOrDefault(
                                    x => x.Attribute("Id") != null && x.Attribute("Id").Value == windowSettings.Id);

                        // Tf the element exists, update the
                        if (settingsElement != null)
                        {
                            // Do not save the window width and height if the window is maximized.
                            if (windowSettings.WindowState == WindowState.Normal)
                            {
                                if (windowSettings.Width.HasValue)
                                {
                                    var widthElement = settingsElement.Attribute("Width");
                                    var newWidth = windowSettings.Width.Value.ToString(CultureInfo.InvariantCulture);
                                    if (widthElement != null)
                                    {
                                        widthElement.Value = newWidth;
                                    }
                                    else
                                    {
                                        settingsElement.SetAttributeValue("Width", newWidth);
                                    }
                                }

                                if (windowSettings.Height.HasValue)
                                {
                                    var heightElement = settingsElement.Attribute("Height");
                                    var newHeight = windowSettings.Height.Value.ToString(CultureInfo.InvariantCulture);
                                    if (heightElement != null)
                                    {
                                        heightElement.Value = newHeight;
                                    }
                                    else
                                    {
                                        settingsElement.SetAttributeValue("Height", newHeight);
                                    }
                                }
                            }

                            var stateElement = settingsElement.Attribute("State");
                            var newState = windowSettings.WindowState.ToString();
                            if (stateElement != null && newState != stateElement.Value)
                            {
                                stateElement.Value = newState;
                            }
                        }
                        else
                        {
                            var root = doc.Element("WindowsSettings");
                            if (root != null)
                            {
                                var windowElement = new XElement("WindowSettings");
                                windowElement.SetAttributeValue("Id", windowSettings.Id);
                                windowElement.SetAttributeValue("State", windowSettings.WindowState.ToString());

                                // If the window state is maximized do not set the window width and height.
                                var width = windowSettings.WindowState == WindowState.Normal
                                            && windowSettings.Width.HasValue
                                                ? windowSettings.Width.Value.ToString(CultureInfo.InvariantCulture)
                                                : string.Empty;

                                if (!string.IsNullOrWhiteSpace(width))
                                {
                                    windowElement.SetAttributeValue("Width", width);
                                }

                                var height = windowSettings.WindowState == WindowState.Normal
                                             && windowSettings.Height.HasValue
                                                 ? windowSettings.Height.Value.ToString(CultureInfo.InvariantCulture)
                                                 : string.Empty;

                                if (!string.IsNullOrWhiteSpace(height))
                                {
                                    windowElement.SetAttributeValue("Height", height);
                                }

                                root.Add(windowElement);
                            }
                        }

                        doc.Save(windowsSettingsPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorException(
                    "An error occurred during the process of saving the window settings.",
                    ex);
            }
        }

        /// <summary>
        /// Reads the settings element and get the windows settings.
        /// </summary>
        /// <param name="settingsElement">The settings element.</param>
        /// <returns>The window settings.</returns>
        private WindowSettings ReadSettings(XElement settingsElement)
        {
            if (settingsElement == null || settingsElement.Attribute("Id") == null)
            {
                return null;
            }

            var windowId = settingsElement.Attribute("Id").Value;

            double? width = null;
            if (settingsElement.Attribute("Width") != null)
            {
                double widthValue;
                var convSucceeded = double.TryParse(
                    settingsElement.Attribute("Width").Value,
                    NumberStyles.Any,
                    CultureInfo.InvariantCulture,
                    out widthValue);
                if (convSucceeded)
                {
                    width = widthValue;
                }
            }

            double? height = null;
            if (settingsElement.Attribute("Height") != null)
            {
                double heightValue;
                var convSucceeded = double.TryParse(
                    settingsElement.Attribute("Height").Value,
                    NumberStyles.Any,
                    CultureInfo.InvariantCulture,
                    out heightValue);
                if (convSucceeded)
                {
                    height = heightValue;
                }
            }

            var winState = WindowState.Normal;
            if (settingsElement.Attribute("State") != null)
            {
                WindowState state;
                var convSucceeded = Enum.TryParse(settingsElement.Attribute("State").Value, out state);
                if (convSucceeded)
                {
                    winState = state;
                }
            }

            return new WindowSettings(windowId, width, height, winState);
        }

        /// <summary>
        /// If the saved window dimensions are larger than the current screen ignore the window sizes.
        /// </summary>
        /// <param name="windowSettings">Window settings.</param>
        /// <returns>The adjusted window settings.</returns>
        private WindowSettings SizeToFit(WindowSettings windowSettings)
        {
            if (windowSettings == null)
            {
                return null;
            }

            var primaryScreenHeight = SystemParameters.VirtualScreenHeight;
            var primaryScreenWidth = SystemParameters.VirtualScreenWidth;
            var firstOrDefault = System.Windows.Forms.Screen.AllScreens.FirstOrDefault(s => s.Primary);
            if (firstOrDefault != null)
            {
                primaryScreenWidth = firstOrDefault.WorkingArea.Width;
                primaryScreenHeight = firstOrDefault.WorkingArea.Height;
            }

            var top = (primaryScreenHeight - windowSettings.Height) / 2;
            var left = (primaryScreenWidth - windowSettings.Width) / 2;

            if (windowSettings.Height > primaryScreenHeight || windowSettings.Width > primaryScreenWidth)
            {
                windowSettings.Height = null;
                windowSettings.Width = null;
                return windowSettings;
            }

            if (top > 0)
            {
                windowSettings.Top = top;
            }

            if (left > 0)
            {
                windowSettings.Left = left;
            }

            return windowSettings;
        }

        /// <summary>
        /// Creates the windows settings file.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The application should not throw errors if for some reason an error occurs when processing windows settings file.")]
        private void CreateSettingsFile()
        {
            if (File.Exists(this.windowsSettingsPath))
            {
                return;
            }

            lock (ThisLock)
            {
                try
                {
                    var settingsFolderPath = Path.GetDirectoryName(this.windowsSettingsPath);
                    if (settingsFolderPath != null && !Directory.Exists(settingsFolderPath))
                    {
                        Directory.CreateDirectory(settingsFolderPath);
                    }

                    var writerSettings = new XmlWriterSettings { Indent = true };
                    using (var writer = XmlWriter.Create(this.windowsSettingsPath, writerSettings))
                    {
                        // Write root (WindowsSettings)
                        writer.WriteStartElement("WindowsSettings");
                        writer.WriteEndElement();
                    }
                }
                catch (XmlException ex)
                {
                    var msg = string.Format(
                        CultureInfo.InvariantCulture,
                        "An xml-related error occurred while creating the '{0}' settings file.",
                        this.windowsSettingsPath);
                    Log.ErrorException(msg, ex);
                }
                catch (Exception ex)
                {
                    Log.ErrorException(
                        "An error occurred while creating the '{0}' settings file.",
                        ex);
                }
            }
        }

        #endregion Methods
    }
}
