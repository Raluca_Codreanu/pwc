﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace ZPKTool.Common
{
    /// <summary>
    /// Defines global constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The Current Database Version used by the application.
        /// </summary>
        public const decimal DataBaseVersion = 1.46m;

        /// <summary>
        /// The max value of the textboxes if they contain numbers.
        /// </summary>
        public const decimal MaxNumberValue = 1000000000m;

        /// <summary>
        /// The maximum value which can be entered into the largest decimal fields in the db. (i.e. decimal(29, 16));
        /// </summary>
        /// <remarks>This is also defined in <see cref="ZPKTool.Data.EntityUtils" />.</remarks>
        public const decimal MaxNumberValueAllowedInDb = 1000000000000m;

        /// <summary>
        /// The max value for the smaller text boxes.
        /// </summary>
        public const decimal MaxSmallValue = 999999.99m;

        /// <summary>
        /// Font size for titles across the application.
        /// </summary>
        public const double TitleSize = 28;

        /// <summary>
        /// Font size for subtitles across the application.
        /// </summary>
        public const double SubtitleSize = 16;

        /// <summary>
        /// Font size for normal text across the application.
        /// </summary>
        public const double LabelFontSize = 12;

        /// <summary>
        /// Font size for text in input boxes across the application.
        /// </summary>
        public const double InputTextSize = 12;

        /// <summary>
        /// Font size for group box titles across the application.
        /// </summary>
        public const double GroupBoxTitleSize = 12;

        /// <summary>
        /// Font size for tab tiles across the application.
        /// </summary>
        public const double TabLabelSize = 12;

        /// <summary>
        /// The maximum size a video can have in the application, in bytes.
        /// </summary>
        public const int MaxVideoSize = 52428800; // 50MB

        /// <summary>
        /// The maximum size a document can have in the application, in bytes.
        /// </summary>
        public const int MaxDocumentSize = 52428800; // 50MB

        /// <summary>
        /// The maximum number of pictures allowed to be added for an entity.
        /// </summary>
        public const int NumberOfMaxAllowedPictures = 10;

        /// <summary>
        /// The name of the folder on the disk where the video files will be cached.
        /// </summary>
        public const string VideoCacheFolderName = "VideoCache";

        /// <summary>
        /// The name of the folder on the disk where the viewer mode files will be cached.
        /// </summary>
        public const string ViewerModeCacheFolderName = "ViewerModeCache";

        /// <summary>
        /// The font used by the itextsharp library; the library knows a few fonts and is difficult to use a different one.
        /// </summary>
        public const string PdfReportsFont = "Helvetica";

        /// <summary>
        /// Encryption key used for decrypting the password
        /// </summary>
        public const string PasswordEncryptionKey = "BVf4jSKZ762fo1FyKd79WWeGITZMLRW7H0At2ES11otN1VZueoifQBD6";

        /// <summary>
        /// Regular expression for password validation.
        /// The password should contain minimum 8 characters, 1 capital letter and at least 1 number or special character.
        /// </summary>
        public const string PasswordValidationExpression = @"^(?=.*[A-Z])((?=.*\d)|(?=.*[^a-zA-Z\d]))(.{8,})$";

        /// <summary>
        /// Regular expression for email validation.        
        /// </summary>
        public const string EmailValidationRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// The ISO code of the default currency.
        /// </summary>
        public const string DefaultCurrencyIsoCode = "EUR";
                
        #region ShiftInfomation Constants

        /// <summary>
        /// The max value for production days per week.
        /// </summary>
        public const decimal MaxProductionDaysPerWeek = 7; // 7 days per week

        /// <summary>
        /// The max value for production weeks per year.
        /// </summary>
        public const decimal MaxProductionWeeksPerYear = 53; // 53 weeks per year

        /// <summary>
        /// The max value for Hours per shift * shifts per week
        /// </summary>
        public const decimal MaxTotalHoursAndShifts = 168; // 24h * 7 days

        /// <summary>
        /// The min value for all shift information parameters.
        /// </summary>
        public const decimal MinShiftInfoValue = 0;

        /// <summary>
        /// The max value for Hours per shift and Shifts per week parameters.
        /// </summary>
        public const decimal MaxShiftInfoValue = 9999999.99m;

        #endregion ShiftInfomation Constants

        /// <summary>
        /// The max value for number of days for payment.
        /// </summary>
        public const decimal MaxPaymentTerms = 100000m;

        /// <summary>
        /// The path to the folder where the application stores data.
        /// </summary>
        public static readonly string ApplicationDataFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PCMData";

        /// <summary>
        /// The full folder path of the video cache folder.
        /// </summary>
        public static readonly string VideoCacheFolderPath = Path.Combine(ApplicationDataFolderPath, VideoCacheFolderName);

        /// <summary>
        /// The full folder path of the viewer mode cache folder.
        /// </summary>
        public static readonly string ViewerModeCacheFolderPath = Path.Combine(ApplicationDataFolderPath, ViewerModeCacheFolderName);
    }
}