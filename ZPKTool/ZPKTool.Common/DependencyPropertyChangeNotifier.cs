﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Common
{
    /// <summary>
    /// Mechanism for getting notifications when a dependency property value has changed.
    /// Useful when controls don't expose a "changed" event for a property you need.
    /// This is an alternative to using "DependencyPropertyDescriptor" that will not leak.
    /// (when using DependencyPropertyDescriptor if you call AddValueChanged without calling RemoveValueChanged when done will cause a memory leak
    /// because the event to which AddValueChanged subscribes is, in essence, static)
    /// </summary>
    public sealed class DependencyPropertyChangeNotifier : DependencyObject
    {
        #region Member Variables

        /// <summary>
        /// Weak reference to the object whose property is being watched for changes by this instance.
        /// </summary>
        private WeakReference propertySource;

        #endregion Member Variables

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyPropertyChangeNotifier"/> class.
        /// </summary>
        /// <param name="propertySource">The object whose property will be watched.</param>
        /// <param name="property">The property to watch for changes.</param>
        public DependencyPropertyChangeNotifier(DependencyObject propertySource, string property)
            : this(propertySource, new PropertyPath(property))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyPropertyChangeNotifier"/> class.
        /// </summary>
        /// <param name="propertySource">The object whose property will be watched.</param>
        /// <param name="property">The property to watch for changes.</param>
        public DependencyPropertyChangeNotifier(DependencyObject propertySource, DependencyProperty property)
            : this(propertySource, new PropertyPath(property))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyPropertyChangeNotifier"/> class.
        /// </summary>
        /// <param name="propertySource">The object whose property will be watched.</param>
        /// <param name="property">The property to watch for changes.</param>
        public DependencyPropertyChangeNotifier(DependencyObject propertySource, PropertyPath property)
        {
            if (propertySource == null)
            {
                throw new ArgumentNullException("propertySource", "The source object for the property was null");
            }

            if (property == null)
            {
                throw new ArgumentNullException("property", "The property was null.");
            }

            this.propertySource = new WeakReference(propertySource);
            Binding binding = new Binding();
            binding.Path = property;
            binding.Mode = BindingMode.OneWay;
            binding.Source = propertySource;
            BindingOperations.SetBinding(this, ValueProperty, binding);
        }

        #endregion Constructor

        #region Events

        /// <summary>
        /// Occurs when the value of the property changes.
        /// </summary>
        public event EventHandler ValueChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the object whose property is being watched for changes by this instance.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Accessing the property should not throw exceptions.")]
        public DependencyObject PropertySource
        {
            get
            {
                try
                {
                    // It is possible that accessing the target property will result in an exception so this check is wrapped in a try catch
                    return this.propertySource.IsAlive ? (DependencyObject)this.propertySource.Target : null;
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Identifies the <see cref="Value"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(object),
            typeof(DependencyPropertyChangeNotifier),
            new PropertyMetadata(null, new PropertyChangedCallback(OnPropertyChanged)));

        /// <summary>
        /// Gets or sets the value of the property.
        /// </summary>        
        /// <seealso cref="ValueProperty"/>
        [Category("Behavior")]
        [Bindable(true)]
        public object Value
        {
            get
            {
                return (object)this.GetValue(DependencyPropertyChangeNotifier.ValueProperty);
            }

            set
            {
                this.SetValue(DependencyPropertyChangeNotifier.ValueProperty, value);
            }
        }

        #endregion Properties

        /// <summary>
        /// Called when ValueProperty has changed.
        /// </summary>
        /// <param name="d">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DependencyPropertyChangeNotifier notifier = (DependencyPropertyChangeNotifier)d;
            if (notifier.ValueChanged != null)
            {
                notifier.ValueChanged(notifier, EventArgs.Empty);
            }
        }
    }
}
