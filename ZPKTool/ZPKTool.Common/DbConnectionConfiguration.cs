﻿using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Stores the information necessary to connect to a database and provides ways to create connection strings.
    /// </summary>
    public class DbConnectionConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionConfiguration"/> class.
        /// </summary>
        public DbConnectionConfiguration()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionConfiguration"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        public DbConnectionConfiguration(DbConnectionInfo connectionInfo)
        {
            this.ConnectionInfo = connectionInfo;
        }

        /// <summary>
        /// Gets or sets the connection information based on which the connection strings will be generated.
        /// </summary>        
        public DbConnectionInfo ConnectionInfo { get; set; }

        /// <summary>
        /// Gets the connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        public string SQLConnectionString
        {
            get { return CreateSQLConnectionString(this.ConnectionInfo); }
        }

        /// <summary>
        /// Gets the connection string to be used by the <see cref="System.Data.Objects.ObjectContext"/> class.
        /// </summary>
        public string EntityConnectionString
        {
            get { return CreateEntityConnectionString(this.ConnectionInfo); }
        }

        /// <summary>
        /// Creates a connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <returns>The connection string</returns>
        public static string CreateSQLConnectionString(DbConnectionInfo connectionInfo)
        {
            string decryptedPassword = connectionInfo.Password;
            if (!string.IsNullOrEmpty(connectionInfo.PasswordEncryptionKey))
            {
                decryptedPassword = EncryptionManager.Instance.DecryptBlowfish(connectionInfo.Password, connectionInfo.PasswordEncryptionKey);
            }

            SqlConnectionStringBuilder connStrgBuilder = new SqlConnectionStringBuilder();
            connStrgBuilder.DataSource = connectionInfo.DataSource;
            connStrgBuilder.InitialCatalog = connectionInfo.InitialCatalog;

            if (connectionInfo.UseWindowsAuthentication)
            {
                connStrgBuilder.IntegratedSecurity = true;
            }
            else
            {
                connStrgBuilder.IntegratedSecurity = false;
                connStrgBuilder.UserID = connectionInfo.UserId;
                connStrgBuilder.Password = decryptedPassword;
            }

            connStrgBuilder.Pooling = connectionInfo.Pooling;
            if (connectionInfo.ConnectionTimeout.HasValue)
            {
                connStrgBuilder.ConnectTimeout = connectionInfo.ConnectionTimeout.Value;
            }

            return connStrgBuilder.ConnectionString;
        }

        /// <summary>
        /// Creates a connection string to be used by the <see cref="System.Data.Objects.ObjectContext"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <returns>The created entity connection string.</returns>
        public static string CreateEntityConnectionString(DbConnectionInfo connectionInfo)
        {
            EntityConnectionStringBuilder entityConnBuilder = new EntityConnectionStringBuilder();
            entityConnBuilder.Metadata = "res://*";
            entityConnBuilder.Provider = "System.Data.SqlClient";

            entityConnBuilder.ProviderConnectionString = CreateSQLConnectionString(connectionInfo);
            return entityConnBuilder.ConnectionString;
        }
    }
}
