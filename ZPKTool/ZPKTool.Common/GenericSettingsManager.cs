﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// A class that handles loading, saving and storing settings in a similar fashion to the .NET way of managing configuration,
    /// except it is customizable and extensible.
    /// </summary>
    public class GenericSettingsManager : ObservableObject
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The path to the xml file which holds the settings.
        /// </summary>
        private string settingsFilePath;

        /// <summary>
        /// This dictionary stores the settings. It is accessed through the indexer property of this class.
        /// It should not be used directly, not even in private code.
        /// </summary>
        private Dictionary<string, object> settings = new Dictionary<string, object>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericSettingsManager"/> class.
        /// </summary>
        public GenericSettingsManager()
        {
            this.LoadDefaultSettingValues();
        }

        /// <summary>
        /// Gets or sets the path to the xml file which holds the settings.
        /// <para />
        /// Setting this property does not trigger the loading of the settings. The Load method must be explicitly called for that.
        /// </summary>
        /// <exception cref="ArgumentException">The path was null, empty or whitespace.</exception>
        public string SettingsFilePath
        {
            get
            {
                return settingsFilePath;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("The settings file path can't be null, empty or whitespace.");
                }

                // The path content is not validated because to avoid the invalidation of valid paths due to bugs in the validation code.
                // If the path is invalid the Load and Save methods will handle the resulting IO errors.
                settingsFilePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of the setting with the specified setting name.
        /// </summary>
        /// <param name="settingName">The name of the setting whose value to get.</param>
        /// <returns>
        /// The value of the setting with the specified name.
        /// </returns>
        [System.Runtime.CompilerServices.IndexerName("Setting")]
        protected object this[string settingName]
        {
            get
            {
                object value;
                this.settings.TryGetValue(settingName, out value);
                return value;
            }

            set
            {
                object crtValue;
                this.settings.TryGetValue(settingName, out crtValue);
                if (crtValue == null || value == null || !crtValue.Equals(value))
                {
                    this.settings[settingName] = value;
                    this.OnPropertyChanged(settingName);
                }
            }
        }

        /// <summary>
        /// Determines whether the process can write into the settings file.
        /// <para />
        /// Note that the method returns false also if the <see cref="SettingsFilePath"/> property is not initialized or if the file does not exist.
        /// </summary>
        /// <returns>
        /// true if the settings file can be written; otherwise, false.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Intentional because any exception means the settings file can not be written.")]
        public bool CanWriteSettingsFile()
        {
            try
            {
                FileInfo fi = new FileInfo(this.SettingsFilePath);
                using (var stream = fi.OpenWrite())
                {
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Loads the settings from the file specified by the <see cref="SettingsFilePath"/> property.
        /// If the file does not exist or is not a valid xml file, it is (re)created using the default values of the setting properties.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "If exceptions occur during settings load the default values must be used.")]
        public virtual void Load()
        {
            if (!File.Exists(this.SettingsFilePath))
            {
                log.Info("The settings file '{0}' does not exist. The file will be created with default setting values.", this.SettingsFilePath);
                this.CreateSettingsFile();
                return;
            }

            try
            {
                // Load the settings file content.
                XmlDocument doc = new XmlDocument();
                doc.Load(this.SettingsFilePath);

                XmlNode root = doc.SelectSingleNode("SettingsFile");
                if (root == null)
                {
                    throw new XmlException("The document's root element is missing.");
                }

                XmlNode settingsNode = root.SelectSingleNode("Settings");
                if (settingsNode != null)
                {
                    bool settingValueConversionFailed = false;

                    // Get all properties representing settings.
                    var settingProperties = this.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(pi => pi.IsDefined(typeof(SettingAttribute), true));

                    // Load each setting property's value from the settings file. If the file does not contain a value for the property use the default value defined
                    // by the DefaultSettingValue attribute.
                    foreach (var settingProperty in settingProperties)
                    {
                        string query = string.Format("Setting[@Name='{0}']", settingProperty.Name);
                        var settingNode = settingsNode.SelectSingleNode(query);

                        if (settingNode != null)
                        {
                            // Get the settings's value.                       
                            var settingValueNode = settingNode.SelectSingleNode("Value");
                            if (settingValueNode != null)
                            {
                                // TODO: empty values may be valid, for example for strings; handle this case after v1.3 release
                                string settingValue = settingValueNode.InnerText;
                                if (!string.IsNullOrWhiteSpace(settingValue))
                                {
                                    // Get the setting's CLR type.
                                    if (settingNode.Attributes["Type"] != null)
                                    {
                                        var settingTypeName = settingNode.Attributes["Type"].Value;
                                        Type settingType = null;
                                        try
                                        {
                                            settingType = Type.GetType(settingTypeName);
                                        }
                                        catch (Exception ex)
                                        {
                                            string msg = string.Format("The Type attribute value ('{0}') of the Setting '{1}' was not a valid System Type.", settingTypeName, settingProperty.Name);
                                            log.WarnException(msg, ex);
                                        }

                                        if (settingType != null)
                                        {
                                            try
                                            {
                                                // Convert the setting's value to the type determined above and set the value into the setting property.
                                                var value = this.ConvertSettingFromStoreValue(settingValue, settingType);
                                                this[settingProperty.Name] = value;
                                            }
                                            catch (Exception ex)
                                            {
                                                settingValueConversionFailed = true;

                                                string msg = string.Format(
                                                    CultureInfo.InvariantCulture,
                                                    "The conversion of the '{0}' setting's value ('{1}') to the '{2}' type has failed. The setting will be reverted to the default value.",
                                                    settingProperty.Name,
                                                    settingValue,
                                                    settingType.FullName);
                                                log.WarnException(msg, ex);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        log.Warn("The Setting '{0}' is missing the Type attribute.", settingProperty.Name);
                                    }
                                }
                            }
                        }
                    }

                    // If a setting's value from the settings file could not be converted to the appropriate type (is corrupted or has an invalid format),
                    // re-create the settings file using the valid loaded settings (and default values for the invalid settings), to avoid the error(s) next time the file is loaded.
                    if (settingValueConversionFailed)
                    {
                        this.CreateSettingsFile();
                    }
                }
            }
            catch (XmlException ex)
            {
                log.ErrorException("Settings file had to be recreated due to invalid xml format.", ex);

                // Recreate file using the values which are already loaded into the setting properties (most likely, the default values, at this stage).
                this.CreateSettingsFile();
            }
            catch (UnauthorizedAccessException ex)
            {
                log.WarnException("The settings file could not be read due to authorization issues. The default setting values will be used.", ex);
            }
        }

        /// <summary>
        /// Saves all changed settings into the settings file.
        /// </summary>
        public virtual void Save()
        {
            if (!File.Exists(this.SettingsFilePath))
            {
                // If the file does not exist for some reason, recreate it with the current setting values.
                log.Error("The settings file does not exist. It will be created now.");
                this.CreateSettingsFile();
                return;
            }

            try
            {
                // load the file to save the new values
                XmlDocument doc = new XmlDocument();
                doc.Load(this.SettingsFilePath);

                bool saveSettingsDoc = false;

                XmlNode root = doc.DocumentElement;
                if (root == null)
                {
                    // Throw XmlException so the xml document is re-created in the catch at the end.
                    throw new XmlException("The document's root element is missing.");
                }

                XmlNode settingsNode = root.SelectSingleNode("Settings");
                if (settingsNode == null)
                {
                    // If Settings element doesn't exist, create it.
                    settingsNode = doc.CreateElement("Settings");
                    root.AppendChild(settingsNode);
                    saveSettingsDoc = true;
                }

                // Get all properties representing settings.
                var settingProperties = this.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(pi => pi.IsDefined(typeof(SettingAttribute), true));

                foreach (var settingProperty in settingProperties)
                {
                    string query = string.Format("Setting[@Name='{0}']", settingProperty.Name);
                    var settingNode = settingsNode.SelectSingleNode(query);

                    if (settingNode == null)
                    {
                        // Create the <Setting /> element.
                        settingNode = doc.CreateElement("Setting");

                        var nameAttr = doc.CreateAttribute("Name");
                        nameAttr.Value = settingProperty.Name;
                        settingNode.Attributes.Append(nameAttr);

                        var typeAttr = doc.CreateAttribute("Type");
                        typeAttr.Value = settingProperty.PropertyType.FullName;
                        settingNode.Attributes.Append(typeAttr);

                        settingsNode.AppendChild(settingNode);
                        saveSettingsDoc = true;
                    }

                    var settingValueNode = settingNode.SelectSingleNode("Value");
                    if (settingValueNode == null)
                    {
                        // Create the Value element
                        settingValueNode = doc.CreateElement("Value");
                        settingNode.AppendChild(settingValueNode);
                        saveSettingsDoc = true;
                    }

                    // Store the setting value if it has changed
                    string newValue = this.ConvertSettingToStoreValue(settingProperty.GetValue(this, null));
                    if (newValue != settingValueNode.InnerText)
                    {
                        settingValueNode.InnerText = newValue;
                        saveSettingsDoc = true;
                    }
                }

                // Save the document only it was changed by adding elements or changing setting values.
                if (saveSettingsDoc)
                {
                    doc.Save(this.SettingsFilePath);
                }
            }
            catch (XmlException ex)
            {
                var msg = string.Format(CultureInfo.InvariantCulture, "An xml-related error occurred while creating the '{0}' settings file.", this.SettingsFilePath);
                log.ErrorException(msg, ex);

                // If any xml-related errors occurs while saving, recreate the settings file using the current setting values.
                this.CreateSettingsFile();
            }
            catch (UnauthorizedAccessException ex)
            {
                log.WarnException("The settings file could not be saved due to authorization issues.", ex);
            }
        }

        /// <summary>
        /// Creates a new settings file and writes the current settings in it. If the settings file exists, it is overwritten.
        /// </summary>
        private void CreateSettingsFile()
        {
            try
            {
                string settingsFolderPath = Path.GetDirectoryName(this.SettingsFilePath);
                if (!Directory.Exists(settingsFolderPath))
                {
                    Directory.CreateDirectory(settingsFolderPath);
                }

                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(this.SettingsFilePath, writerSettings))
                {
                    var settingProperties = this.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(pi => pi.IsDefined(typeof(SettingAttribute), true));

                    // Write root (SettingsFile) and Settings elements
                    writer.WriteStartElement("SettingsFile");
                    writer.WriteStartElement("Settings");

                    // Write a Setting element for reach property that represents a setting. Write the default value, if it has one defined.
                    foreach (var propInfo in settingProperties)
                    {
                        // write a new setting element
                        writer.WriteStartElement("Setting");
                        writer.WriteAttributeString("Name", propInfo.Name);
                        writer.WriteAttributeString("Type", propInfo.PropertyType.FullName);
                        writer.WriteStartElement("Value");

                        var propValue = propInfo.GetValue(this, null);
                        if (propValue != null)
                        {
                            writer.WriteValue(this.ConvertSettingToStoreValue(propValue));
                        }

                        writer.WriteEndElement(); // </Value>
                        writer.WriteEndElement(); // </Setting>
                    }

                    writer.WriteEndElement(); // </Settings>
                    writer.WriteEndElement(); // </SettingsFile>
                }
            }
            catch (XmlException ex)
            {
                string msg = string.Format(CultureInfo.InvariantCulture, "An xml-related error occurred while creating the '{0}' settings file.", this.SettingsFilePath);
                log.ErrorException(msg, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                log.WarnException("The settings file could not be created due to authorization issues.", ex);
            }
        }

        /// <summary>
        /// Converts the specified setting value to a value that can be stored in the settings file.
        /// </summary>
        /// <param name="settingValue">The setting value.</param>
        /// <returns>
        /// The corresponding value to be stored in the settings file (the store value corresponding to the specified setting value).
        /// </returns>
        private string ConvertSettingToStoreValue(object settingValue)
        {
            return Convert.ToString(settingValue, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the specified store value (obtained from the settings file) to a typed value that can be set as the value of a setting property.
        /// </summary>
        /// <param name="storeValue">The store value.</param>
        /// <param name="storeValueType">The type to which the store value must be converted.</param>
        /// <returns>
        /// The setting value.
        /// </returns>
        private object ConvertSettingFromStoreValue(string storeValue, Type storeValueType)
        {
            if (storeValueType.IsEnum)
            {
                return Enum.Parse(storeValueType, storeValue);
            }
            else if (storeValueType == typeof(Uri))
            {
                if (string.IsNullOrWhiteSpace(storeValue))
                {
                    return null;
                }

                try
                {
                    return new Uri(storeValue);
                }
                catch (UriFormatException)
                {
                    return null;
                }
            }
            else
            {
                return Convert.ChangeType(storeValue, storeValueType, CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Loads the default setting values from the DefaultSettingValue of each Setting property.
        /// </summary>
        private void LoadDefaultSettingValues()
        {
            var settingProperties = this.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(pi => pi.IsDefined(typeof(SettingAttribute), true));

            foreach (var settingProperty in settingProperties)
            {
                var defaultValAttr = (DefaultSettingValueAttribute)settingProperty.GetCustomAttributes(typeof(DefaultSettingValueAttribute), true).FirstOrDefault();
                if (defaultValAttr != null && defaultValAttr.Value != null)
                {
                    try
                    {
                        var value = this.ConvertSettingFromStoreValue(defaultValAttr.Value, settingProperty.PropertyType);
                        this[settingProperty.Name] = value;
                    }
                    catch
                    {
                        log.Error("An error occurred while applying the default value '{0}' to the Setting '{1}'.", defaultValAttr.Value, settingProperty.Name);
                        throw;
                    }
                }
            }
        }
    }
}
