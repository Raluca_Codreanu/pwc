﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.Common
{
    /// <summary>
    /// A common weak event listener which can be used for different kinds of events.
    /// </summary>
    /// <typeparam name="TEventArgs">The EventArgs type for the event.</typeparam>
    public class WeakEventListener<TEventArgs> : IWeakEventListener where TEventArgs : EventArgs
    {
        /// <summary>
        /// The weak event's handler.
        /// </summary>
        private EventHandler<TEventArgs> realHander;
        
        /// <summary>
        /// Initializes a new instance of the WeakEventListener class.
        /// </summary>
        /// <param name="handler">The handler for the event.</param>
        public WeakEventListener(EventHandler<TEventArgs> handler)
        {
            if (handler == null)
            {
                throw new ArgumentNullException("handler");
            }
            
            this.realHander = handler;
        }

        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (e is TEventArgs)
            {
                TEventArgs realArgs = (TEventArgs)e;
                this.realHander(sender, realArgs);
                return true;
            }

            return false;
        }
    }
}
