﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Contains various utility methods.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Checks if the input contains the same character more than 3 times or if it contains a sequence
        /// of characters, for ex: "abc", "567".
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A value indicating whether are to many characters of
        /// the same type or the sequence length. </returns>
        public static bool CheckForDuplicateAndSequenceOfCharacters(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return true;
            }

            List<char> listElement = input.ToList();
            if (listElement.GroupBy(a => a).Any(a => a.Count() > 3))
            {
                return false;
            }

            byte[] asciiValues = Encoding.ASCII.GetBytes(input);
            for (int i = 1; i < input.Length - 1; i++)
            {
                if (asciiValues[i] == (asciiValues[i - 1] + 1)
                    && asciiValues[i] == (asciiValues[i + 1] - 1))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
