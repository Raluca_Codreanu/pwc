﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// Encryption class
    /// Contains string encryption and randomization methods
    /// </summary>
    //// TODO: make the class static instead of singleton.
    public sealed class EncryptionManager
    {
        #region Attributes

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly EncryptionManager instance = new EncryptionManager();

        #endregion Attributes

        /// <summary>
        /// Prevents a default instance of the <see cref="EncryptionManager" /> class from being created.
        /// </summary>
        private EncryptionManager()
        {
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static EncryptionManager Instance
        {
            get { return instance; }
        }

        #region Public Methods
        /// <summary>
        /// Decrypts a string with a given key using the blowfish algorithm
        /// using CBC (cipher-block chaining) block cipher mode
        /// </summary>
        /// <param name="textToDecrypt">The text to decrypt.</param>
        /// <param name="key">The key used to encrypt the text.</param>
        /// <returns>The decrypted string.</returns>
        public string DecryptBlowfish(string textToDecrypt, string key)
        {
            if (string.IsNullOrEmpty(key) || key.Length > 56)
            {
                throw new SettingsPropertyNotFoundException("Specified key is too long. Max length is 56 characters.");
            }

            if (string.IsNullOrEmpty(textToDecrypt))
            {
                throw new SettingsPropertyNotFoundException("Specified text to decrypt is invalid.");
            }

            // the password is stored in base64 therefore it needs decoding 
            // to an 8-bit unsigned integer array.
            byte[] encryptedText = Convert.FromBase64String(textToDecrypt);

            int dataLen = encryptedText.Length;
            int offset = encryptedText.Length % BlowfishECB.BlockSize;
            if (offset != 0)
            {
                dataLen += BlowfishECB.BlockSize - offset;
            }

            // create a mask array with a dimension multiple of BLOCK_SIZE
            byte[] encrypted = new byte[dataLen];
            encryptedText.CopyTo(encrypted, 0);

            // converting encrypted text to an array of bytes - required for decryption
            byte[] keyBytes = new byte[key.Length];
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            keyBytes = encoding.GetBytes(key);

            BlowfishECB ecb = new BlowfishECB(keyBytes, 0, keyBytes.Length);
            byte[] decrypted = new byte[dataLen];
            ecb.Decrypt(encrypted, 0, decrypted, 0, dataLen);

            // remove added extra \0 characters in order to comply with the initial plain text 
            string decryptedText = encoding.GetString(decrypted);
            decryptedText = decryptedText.Replace("\0", string.Empty);
            return decryptedText;
        }

        /// <summary>
        /// Encrypts a string using the blowfish encryption.
        /// </summary>
        /// <param name="text">The string to encrypt.</param>
        /// <param name="encryptionKey">The encryption key.</param>
        /// <returns>The encrypted result converted to a base64 string.</returns>
        /// <exception cref="ArgumentException">A parameter was invalid.</exception>
        public string EncryptBlowfish(string text, string encryptionKey)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException("The text to encrypt is invalid.");
            }

            if (string.IsNullOrEmpty(encryptionKey) || encryptionKey.Length > 56)
            {
                throw new ArgumentException("Blowfish encryption key is too long. Max length is 56 characters.");
            }

            ASCIIEncoding encoding = new ASCIIEncoding();

            byte[] bytesToEncrypt = encoding.GetBytes(text);
            byte[] keyBytes = encoding.GetBytes(encryptionKey);

            // The encryption input array length must be a multiple of BlowfishECB.BLOCK_SIZE
            int encryptInputLen = bytesToEncrypt.Length;
            int offset = encryptInputLen % BlowfishECB.BlockSize;
            if (offset != 0)
            {
                encryptInputLen += BlowfishECB.BlockSize - offset;
            }

            byte[] encryptInput = new byte[encryptInputLen];
            bytesToEncrypt.CopyTo(encryptInput, 0);

            byte[] encrypted = new byte[encryptInputLen];
            BlowfishECB ecb = new BlowfishECB(keyBytes, 0, keyBytes.Length);
            ecb.Encrypt(encryptInput, 0, encrypted, 0, encryptInputLen);

            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Hashes a string
        /// </summary>
        /// <param name="textToEncode">The text to encode.</param>
        /// <returns>Hashed string</returns>
        public string EncodeMD5(string textToEncode)
        {
            if (textToEncode == null)
            {
                textToEncode = string.Empty;
            }

            // Create a new instance of the MD5CryptoServiceProvider object.
            using (MD5 md5Hasher = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(textToEncode));

                StringBuilder builder = new StringBuilder();

                // Format each byte one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    builder.Append(data[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }

        /// <summary>
        /// Hashes with SHA256 the specified text combined with the salt and the extra text.
        /// </summary>
        /// <param name="text">The text to hash.</param>
        /// <param name="salt">The salt attached.</param>
        /// <param name="extraText">The extra text used to secure the hash.</param>
        /// <returns>The hashed value.</returns>
        public string HashSHA256(string text, string salt, string extraText)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException("The text to hash is invalid.");
            }

            if (string.IsNullOrEmpty(salt))
            {
                throw new ArgumentException("The salt used to hash the text is invalid.");
            }

            if (string.IsNullOrEmpty(extraText))
            {
                throw new ArgumentException("The extra text used to hash the text is invalid.");
            }

            var textBytes = Encoding.UTF8.GetBytes(text);
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            var extraTextBytes = Encoding.UTF8.GetBytes(extraText);
            var textWithSaltAndExtraBytes = new byte[textBytes.Length + saltBytes.Length + extraTextBytes.Length];

            // Copy text bytes, salt and extra text into resulting array.
            for (int i = 0; i < textBytes.Length; i++)
            {
                textWithSaltAndExtraBytes[i] = textBytes[i];
            }

            for (int i = 0; i < saltBytes.Length; i++)
            {
                textWithSaltAndExtraBytes[textBytes.Length + i] = saltBytes[i];
            }

            for (int i = 0; i < extraTextBytes.Length; i++)
            {
                textWithSaltAndExtraBytes[textBytes.Length + saltBytes.Length + i] = extraTextBytes[i];
            }

            var hash = new SHA256Managed();
            var hashBytes = hash.ComputeHash(textWithSaltAndExtraBytes);
            var hashWithSaltAndExtraTextBytes = new byte[hashBytes.Length + saltBytes.Length + extraTextBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashWithSaltAndExtraTextBytes[i] = hashBytes[i];
            }

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
            {
                hashWithSaltAndExtraTextBytes[hashBytes.Length + i] = saltBytes[i];
            }

            // Append extra text bytes to the result.
            for (int i = 0; i < extraTextBytes.Length; i++)
            {
                hashWithSaltAndExtraTextBytes[hashBytes.Length + saltBytes.Length + i] = extraTextBytes[i];
            }

            return Convert.ToBase64String(hashWithSaltAndExtraTextBytes);
        }

        /// <summary>
        /// Generates a random string that has len length and consists of smallcaps letters
        /// </summary>
        /// <param name="len">Length of the generated string</param>
        /// <param name="readable">Indicates if the generated string should be easily readable and memorizable</param>
        /// <param name="avoidChars">Array of chars that should not be included in the generated string</param>
        /// <returns>A randomly generated string</returns>
        public string GenerateRandomString(int len, bool readable, char[] avoidChars)
        {
            if (!readable)
            {
                if (avoidChars == null)
                {
                    avoidChars = new char[] { };
                }

                // ToDo: implementation needed for readable password
                StringBuilder randomString = new StringBuilder();
                Random rnd = new Random(Guid.NewGuid().GetHashCode());

                char newChar;
                for (int i = 0; i < len; i++)
                {
                    do
                    {
                        newChar = (char)rnd.Next(97, 122);
                    }
                    while (avoidChars.Contains<char>(newChar));

                    randomString.Append(newChar);
                }

                return randomString.ToString();
            }
            else
            {
                return this.GenerateRandomString(len, !readable, avoidChars);
            }
        }

        /// <summary>
        /// Generates a random string from all available characters
        /// </summary>
        /// <param name="len">The length of the generated string.</param>
        /// <param name="readable">if set to <c>true</c> [readable].</param>
        /// <returns>A random string</returns>
        public string GenerateRandomString(int len, bool readable)
        {
            return this.GenerateRandomString(len, readable, null);
        }

        /// <summary>
        /// Generates a random string from all available characters which is not readable
        /// </summary>
        /// <param name="len">The length of the string to generate.</param>
        /// <returns>A random string.</returns>
        public string GenerateRandomString(int len)
        {
            return this.GenerateRandomString(len, false, null);
        }

        #endregion Public Methods
    }
}
