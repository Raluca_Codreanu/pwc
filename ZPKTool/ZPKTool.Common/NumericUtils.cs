﻿namespace ZPKTool.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains utility methods related to numeric types and operations with numbers.
    /// </summary>
    public static class NumericUtils
    {
        /// <summary>
        /// Determines whether the specified value is a number, that is, a numeric type and is not null.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// true if the specified value is number; otherwise, false.
        /// </returns>
        public static bool IsNumber(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is sbyte)
            {
                return true;
            }

            if (value is byte)
            {
                return true;
            }

            if (value is short)
            {
                return true;
            }

            if (value is ushort)
            {
                return true;
            }

            if (value is int)
            {
                return true;
            }

            if (value is uint)
            {
                return true;
            }

            if (value is long)
            {
                return true;
            }

            if (value is ulong)
            {
                return true;
            }

            if (value is float)
            {
                return true;
            }

            if (value is double)
            {
                return true;
            }

            if (value is decimal)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified value is an integral number, that is a numeric, type containing an integer and is not null.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// true if the specified value is number; otherwise, false.
        /// </returns>
        public static bool IsIntegralNumber(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is sbyte)
            {
                return true;
            }

            if (value is byte)
            {
                return true;
            }

            if (value is short)
            {
                return true;
            }

            if (value is ushort)
            {
                return true;
            }

            if (value is int)
            {
                return true;
            }

            if (value is uint)
            {
                return true;
            }

            if (value is long)
            {
                return true;
            }

            if (value is ulong)
            {
                return true;
            }

            return false;
        }
    }
}
