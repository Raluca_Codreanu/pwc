﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// This class contains methods for formatting numbers and strings.
    /// </summary>
    public static class Formatter
    {
        /// <summary>
        /// Formats a given number according to the current culture (for display on the UI).
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <param name="maxDecimalPlaces">The maximum number of decimal places to be included.
        /// This parameter is ignored if <paramref name="value"/> is an integer.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not 
        /// a number or is null.
        /// </returns>
        public static string FormatNumber(object value, int maxDecimalPlaces)
        {
            if (!NumericUtils.IsNumber(value))
            {
                return string.Empty;
            }

            // This check is needed because in the German culture it would result in ,0 which would be trimmed.
            if (value.ToString() == "0")
            {
                return "0";
            }

            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            
            string numberFormat = "{0:#,0";
            if (!NumericUtils.IsIntegralNumber(value) && maxDecimalPlaces > 0)
            {
                numberFormat += ".";
                for (int i = 0; i < maxDecimalPlaces; i++)
                {
                    numberFormat += "#";
                }
            }

            numberFormat += "}";

            string result = string.Format(CultureInfo.CurrentCulture, numberFormat, value);
            if (result.StartsWith(decimalSeparator))
            {
                result = "0" + result;
            }

            if (result.EndsWith(decimalSeparator + "0"))
            {
                return result.Substring(0, result.Length - 2);
            }

            return result.Trim();
        }

        /// <summary>
        /// Formats an integer number (short, int, long, decimal) according to the current culture, for display on the UI.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not
        /// an integer or is null.
        /// </returns>
        public static string FormatInteger(short? value)
        {
            return FormatNumber(value, 0);
        }

        /// <summary>
        /// Formats an integer number (short, int, long, decimal) according to the current culture, for display on the UI.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not
        /// an integer or is null.
        /// </returns>
        public static string FormatInteger(int? value)
        {
            return FormatNumber(value, 0);
        }

        /// <summary>
        /// Formats an integer number (short, int, long, decimal) according to the current culture, for display on the UI.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not
        /// an integer or is null.
        /// </returns>
        public static string FormatInteger(long? value)
        {
            return FormatNumber(value, 0);
        }

        /// <summary>
        /// Formats an integer number (short, int, long, decimal) according to the current culture, for display on the UI.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not
        /// an integer or is null.
        /// </returns>
        public static string FormatInteger(decimal? value)
        {
            return FormatNumber(value, 0);
        }

        /// <summary>
        /// Formats a number representing a sum of money according to the current culture, for display on the UI.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if<paramref name="value"/> is not
        /// an integer or is null.
        /// </returns>
        public static string FormatMoney(decimal? value)
        {
            return FormatNumber(value, UserSettingsManager.Instance.DecimalPlacesNumber);
        }

        /// <summary>
        /// Formats the specified number for display on the UI, using the number of decimal places specified in the app settings.
        /// </summary>
        /// <param name="value">The number to format.</param>
        /// <returns>
        /// A string containing the formatted number or an empty string if <paramref name="value"/> is null.
        /// </returns>
        public static string FormatNumber(decimal? value)
        {
            return FormatNumber(value, UserSettingsManager.Instance.DecimalPlacesNumber);
        }
    }
}
