﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Common
{
    /// <summary>
    /// A basic class that is used to store two related objects. 
    /// </summary>
    /// <typeparam name="T">The type of the 1st object.</typeparam>
    /// <typeparam name="U">The type of the 2nd object.</typeparam>    
    public class Pair<T, U>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pair&lt;T, U&gt;"/> class.
        /// </summary>
        public Pair()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pair&lt;T, U&gt;"/> class.
        /// </summary>
        /// <param name="first">The 1st element of the pair.</param>
        /// <param name="second">The 2nd element of the pair.</param>
        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        /// <summary>
        /// Gets or sets the 1st object of the pair.
        /// </summary>
        public T First { get; set; }

        /// <summary>
        /// Gets or sets the 2nd object of the pair.
        /// </summary>
        public U Second { get; set; }
    }
}
