﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CO2Emissions;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CO2Emissions
{
    /// <summary>
    /// Units test for the <see cref="CO2EmissionsCalculator"/> class.
    /// </summary>
    [TestClass]
    public class CO2EmissionsCalculatorTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CO2EmissionsCalculatorTest" /> class.
        /// </summary>
        public CO2EmissionsCalculatorTest()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }

        #endregion

        /// <summary>
        /// Calculates the emissions using a null value for the "process" parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateEmissionsWithNullProcess()
        {
            var calculator = new CO2EmissionsCalculator();
            calculator.CalculateEmissions(null);
        }

        /// <summary>
        /// Calculates the emissions using the input data provided by the client.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithReferenceData()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { Name = "Process A", Index = 1, ProcessTime = 25m, PartsPerCycle = 1 };
            stepA.Machines.Add(new Machine() { Name = "Machine A", Index = 1, PowerConsumption = 2.4m, FossilEnergyConsumptionRatio = 1m });
            stepA.Machines.Add(new Machine() { Name = "Machine B", Index = 2, PowerConsumption = 4.6m, FossilEnergyConsumptionRatio = 0.5m });
            process.Steps.Add(stepA);

            var stepB = new PartProcessStep() { Name = "Process B", Index = 2, ProcessTime = 40m, PartsPerCycle = 4 };
            stepB.Machines.Add(new Machine() { Name = "Machine C", Index = 1, PowerConsumption = 45m, FossilEnergyConsumptionRatio = 0.8m });
            process.Steps.Add(stepB);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var stepAEmissions = processEmissions.FirstOrDefault(em => em.StepId == stepA.Guid);
            Assert.AreEqual(stepAEmissions.FossilSourcesEmissionsPerHour, 2585m);
            Assert.AreEqual(stepAEmissions.FossilSourcesEmissionsPerPart, 17.95138888888888888888888889m);
            Assert.AreEqual(stepAEmissions.RenewableSourcesEmissionsPerHour, 57.5m);
            Assert.AreEqual(stepAEmissions.RenewableSourcesEmissionsPerPart, 0.399305555555555555555555555m);
            Assert.AreEqual(stepAEmissions.TotalEmissionsPerHour, 2642.5m);
            Assert.AreEqual(stepAEmissions.TotalEmissionsPerPart, 18.350694444444444444444444445m);

            var stepBEmissions = processEmissions.FirstOrDefault(em => em.StepId == stepB.Guid);
            Assert.AreEqual(stepBEmissions.FossilSourcesEmissionsPerHour, 19800m);
            Assert.AreEqual(stepBEmissions.FossilSourcesEmissionsPerPart, 55m);
            Assert.AreEqual(stepBEmissions.RenewableSourcesEmissionsPerHour, 225m);
            Assert.AreEqual(stepBEmissions.RenewableSourcesEmissionsPerPart, 0.625m);
            Assert.AreEqual(stepBEmissions.TotalEmissionsPerHour, 20025m);
            Assert.AreEqual(stepBEmissions.TotalEmissionsPerPart, 55.625m);

            var machineAEmissions =
                stepAEmissions.MachineEmissions.FirstOrDefault(em => em.MachineId == stepA.Machines.FirstOrDefault(m => m.Name == "Machine A").Guid);
            Assert.AreEqual(machineAEmissions.FossilSourcesEmissionsPerHour, 1320m);
            Assert.AreEqual(machineAEmissions.FossilSourcesEmissionsPerPart, 9.166666666666666666666666668m);
            Assert.AreEqual(machineAEmissions.RenewableSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineAEmissions.RenewableSourcesEmissionsPerPart, 0m);
            Assert.AreEqual(machineAEmissions.TotalEmissionsPerHour, 1320m);
            Assert.AreEqual(machineAEmissions.TotalEmissionsPerPart, 9.166666666666666666666666668m);

            var machineBEmissions =
                stepAEmissions.MachineEmissions.FirstOrDefault(em => em.MachineId == stepA.Machines.FirstOrDefault(m => m.Name == "Machine B").Guid);
            Assert.AreEqual(machineBEmissions.FossilSourcesEmissionsPerHour, 1265m);
            Assert.AreEqual(machineBEmissions.FossilSourcesEmissionsPerPart, 8.784722222222222222222222222m);
            Assert.AreEqual(machineBEmissions.RenewableSourcesEmissionsPerHour, 57.5m);
            Assert.AreEqual(machineBEmissions.RenewableSourcesEmissionsPerPart, 0.399305555555555555555555555m);
            Assert.AreEqual(machineBEmissions.TotalEmissionsPerHour, 1322.5m);
            Assert.AreEqual(machineBEmissions.TotalEmissionsPerPart, 9.184027777777777777777777777m);

            var machineCEmissions =
                stepBEmissions.MachineEmissions.FirstOrDefault(em => em.MachineId == stepB.Machines.FirstOrDefault(m => m.Name == "Machine C").Guid);
            Assert.AreEqual(machineCEmissions.FossilSourcesEmissionsPerHour, 19800m);
            Assert.AreEqual(machineCEmissions.FossilSourcesEmissionsPerPart, 55m);
            Assert.AreEqual(machineCEmissions.RenewableSourcesEmissionsPerHour, 225m);
            Assert.AreEqual(machineCEmissions.RenewableSourcesEmissionsPerPart, 0.625m);
            Assert.AreEqual(machineCEmissions.TotalEmissionsPerHour, 20025m);
            Assert.AreEqual(machineCEmissions.TotalEmissionsPerPart, 55.625m);

            var totalFossilEmissionsPerPart = processEmissions.Sum(em => em.FossilSourcesEmissionsPerPart);
            var totalRenewableEmissionsPerPart = processEmissions.Sum(em => em.RenewableSourcesEmissionsPerPart);
            Assert.AreEqual(totalFossilEmissionsPerPart, 72.95138888888888888888888889m);
            Assert.AreEqual(totalRenewableEmissionsPerPart, 1.024305555555555555555555555m);
        }

        /// <summary>
        /// Calculates the emissions using a machine with null value for the fossil energy consumption ratio.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithNullFossilEnergyConsumptionRatio()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = null };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 7177.5m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 23.925m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 0m);
        }

        /// <summary>
        /// Calculates the emissions using a machine with null value for the power consumption.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithNullPowerConsumption()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { PowerConsumption = null, FossilEnergyConsumptionRatio = 0.6m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 0m);
        }

        /// <summary>
        /// Calculates the emissions using a process step with null value for the process time.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithNullProcessTime()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = null, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = 0.5m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 3588.75m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 163.125m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 0m);
        }

        /// <summary>
        /// Calculates the emissions using a process step with null value for the parts per cycle variable.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithNullPartsPerCycle()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = null };
            process.Steps.Add(stepA);

            var machineA = new Machine() { PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = 0.5m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 3588.75m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 163.125m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 0m);
        }

        /// <summary>
        /// Calculates the emissions using a machine with an amount equal to 0.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithZeroMachineAmount()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { Amount = 0, PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = 0.5m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 0m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 0m);
        }

        /// <summary>
        /// Calculates the emissions using a machine with an amount larger than 1.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithMultipleMachineAmount()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { Amount = 3, PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = 0.5m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            var machineEmissions = processEmissions[0].MachineEmissions[0];
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerHour, 10766.25m);
            Assert.AreEqual(machineEmissions.FossilSourcesEmissionsPerPart, 35.8875m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerHour, 489.375m);
            Assert.AreEqual(machineEmissions.RenewableSourcesEmissionsPerPart, 1.63125m);
        }

        // TODO: with empty process and steps without machines and steps

        /// <summary>
        /// Calculates the emissions using a step that contains a machine that was deleted to the trash bin.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithDeletedMachine()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var machineA = new Machine() { IsDeleted = true, PowerConsumption = 13.05m, FossilEnergyConsumptionRatio = 0.5m };
            stepA.Machines.Add(machineA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            Assert.IsTrue(processEmissions[0].MachineEmissions.Count == 0, "CO2 emissions were calculated for deleted machines.");
        }

        /// <summary>
        /// Calculates the emissions using a step that does not contain any machine.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithoutMachines()
        {
            Process process = new Process();

            var stepA = new PartProcessStep() { ProcessTime = 24m, PartsPerCycle = 2 };
            process.Steps.Add(stepA);

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            Assert.IsTrue(processEmissions[0].MachineEmissions.Count == 0, "CO2 emissions were calculated for inexistent machines.");
        }

        /// <summary>
        /// Calculates the emissions using a process without steps.
        /// </summary>
        [TestMethod]
        public void CalculateEmissionsWithEmptyProcess()
        {
            Process process = new Process();

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(process);

            Assert.IsTrue(processEmissions.Count == 0, "CO2 emissions were calculated for inexistent process steps.");
        }
    }
}
