﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the fucntionality related to AssemblyCost.
    /// </summary>
    [TestClass]
    public class AssemblyCostTest
    {
        /// <summary>
        /// Tests the Clone method.
        /// </summary>
        [TestMethod]
        public void CloneTest()
        {
            var cost = new AssemblyCost()
            {
                AmountPerAssembly = 1,
                AssemblyId = Guid.NewGuid(),
                AssemblyIndex = 2,
                Description = EncryptionManager.Instance.GenerateRandomString(80, true),
                ExternalMargin = 0.56m,
                ExternalOverhead = 1.88m,
                ExternalSGA = 0.99m,
                Name = EncryptionManager.Instance.GenerateRandomString(25, true),
                Number = EncryptionManager.Instance.GenerateRandomString(8, true),
                TargetCost = 17,
                TargetPrice = 16
            };
            cost.FullCalculationResult = new CalculationResult() { PackagingCost = 17 };

            var clone = cost.Clone();

            AssertExtensions.AreEqualValueProperties(cost, clone);
        }

        /// <summary>
        /// Applies the unit conversion test.
        /// </summary>
        [TestMethod]
        public void ApplyUnitConversionTest()
        {
            var cost = new AssemblyCost()
            {
                ExternalMargin = 2m,
                ExternalOverhead = 1m,
                ExternalSGA = 3m,
                TargetCost = 4,
                TargetPrice = 5
            };
            cost.FullCalculationResult = new CalculationResult() { PaymentCost = 7 };

            var factor = new UnitConversionFactors() { CurrencyFactor = 2m };

            cost.ApplyUnitConversion(factor);

            Assert.AreEqual(4, cost.ExternalMargin);
            Assert.AreEqual(2, cost.ExternalOverhead);
            Assert.AreEqual(6, cost.ExternalSGA);
            Assert.AreEqual(8, cost.TargetCost);
            Assert.AreEqual(10, cost.TargetPrice);
            Assert.AreEqual(14, cost.FullCalculationResult.PaymentCost);

            // Verify the conversion with null TargetPrice
            cost.TargetPrice = null;
            cost.ApplyUnitConversion(factor);

            Assert.IsNull(cost.TargetPrice);
        }

        /// <summary>
        /// Applies the unit conversion test using invalid parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApplyUnitConversionTestUsingInvalidParameters()
        {
            var cost = new AssemblyCost();
            cost.ApplyUnitConversion(null);
        }
    }
}
