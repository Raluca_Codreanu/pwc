﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the fucntionality related to AssembliesCost.
    /// </summary>
    [TestClass]
    public class AssembliesCostTest
    {
        /// <summary>
        /// Adds a valid assembly cost test.
        /// </summary>
        [TestMethod]
        public void AddValidAssemblyCost()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 5;
            cost.AssemblyCostsSum = 10;

            var costToAdd = new AssemblyCost();
            costToAdd.AmountPerAssembly = 3;
            costToAdd.TargetCost = 6;

            cost.AddCost(costToAdd);

            Assert.AreEqual(8, cost.AssemblyAmountsSum);
            Assert.AreEqual(28, cost.AssemblyCostsSum);
            Assert.AreEqual(1, cost.AssemblyCosts.Count);
            Assert.IsTrue(cost.AssemblyCosts.Contains(costToAdd));
        }

        /// <summary>
        /// Adds a null assembly cost test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullAssemblyCost()
        {
            var cost = new AssembliesCost();
            cost.AddCost(null);
        }

        /// <summary>
        /// Adds a valid assembly costs test.
        /// </summary>
        [TestMethod]
        public void AddValidAssemblyCosts()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 2;
            cost.AssemblyCostsSum = 5;

            var costsToAdd = new List<AssemblyCost>()
            { 
                new AssemblyCost() { AmountPerAssembly = 1, TargetCost = 1 },
                new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 1 },
                new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 1.5m }
            };

            cost.AddCosts(costsToAdd);

            Assert.AreEqual(7, cost.AssemblyAmountsSum);
            Assert.AreEqual(11, cost.AssemblyCostsSum);

            Assert.AreEqual(3, cost.AssemblyCosts.Count);
            foreach (var addedCost in costsToAdd)
            {
                Assert.IsTrue(cost.AssemblyCosts.Contains(addedCost));
            }
        }

        /// <summary>
        /// Adds a zero assembly costs test.
        /// </summary>
        [TestMethod]
        public void AddZeroAssemblyCosts()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 5;
            cost.AssemblyCostsSum = 10;
            cost.AssemblyCosts.Add(new AssemblyCost());

            var costsToAdd = new List<AssemblyCost>();
            cost.AddCosts(costsToAdd);

            Assert.AreEqual(5, cost.AssemblyAmountsSum);
            Assert.AreEqual(10, cost.AssemblyCostsSum);
            Assert.AreEqual(1, cost.AssemblyCosts.Count);
        }

        /// <summary>
        /// Adds a null assembly costs test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullAssemblyCosts()
        {
            var cost = new AssembliesCost();
            cost.AddCosts(null);
        }

        /// <summary>
        /// Sums valid assemblies cost test.
        /// </summary>
        [TestMethod]
        public void SumWithValidAssembliesCost()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 5;
            cost.AssemblyCostsSum = 10;
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 3, TargetCost = 2 });
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2 });

            var costToAdd = new AssembliesCost();
            costToAdd.AssemblyAmountsSum = 5;
            costToAdd.AssemblyCostsSum = 10.9m;
            costToAdd.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 1, TargetCost = 1.5m });
            costToAdd.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2 });
            costToAdd.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2.7m });

            cost.SumWith(costToAdd);

            Assert.AreEqual(10, cost.AssemblyAmountsSum);
            Assert.AreEqual(20.9m, cost.AssemblyCostsSum);

            Assert.AreEqual(5, cost.AssemblyCosts.Count);
            foreach (var addedCost in costToAdd.AssemblyCosts)
            {
                Assert.IsTrue(cost.AssemblyCosts.Contains(addedCost));
            }
        }

        /// <summary>
        /// Sums null assemblies cost test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SumWithNullAssembliesCost()
        {
            var cost = new AssembliesCost();
            cost.SumWith(null);
        }

        /// <summary>
        /// Clone test.
        /// </summary>
        [TestMethod]
        public void CloneTest()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 5;
            cost.AssemblyCostsSum = 10.9m;
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 1, TargetCost = 1.5m, ExternalSGA = 0.98m });
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2, Description = "Test description" });
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2.7m, Number = "xcv-100" });

            var clone = cost.Clone();

            AssertExtensions.AreEqualValueProperties(cost, clone, deepCheck: true);
        }

        /// <summary>
        /// Applies the unit conversion test.
        /// </summary>
        [TestMethod]
        public void ApplyUnitConversionTest()
        {
            var cost = new AssembliesCost();
            cost.AssemblyAmountsSum = 5;
            cost.AssemblyCostsSum = 10.9m;
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 1, TargetCost = 1.5m });
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2 });
            cost.AssemblyCosts.Add(new AssemblyCost() { AmountPerAssembly = 2, TargetCost = 2.7m });

            var factor = new UnitConversionFactors() { CurrencyFactor = 2m };

            cost.ApplyUnitConversion(factor);

            Assert.AreEqual(21.8m, cost.AssemblyCostsSum);
            Assert.AreEqual(3, cost.AssemblyCosts[0].TargetCost);
            Assert.AreEqual(4, cost.AssemblyCosts[1].TargetCost);
            Assert.AreEqual(5.4m, cost.AssemblyCosts[2].TargetCost);
        }

        /// <summary>
        /// Applies the unit conversion test using invalid parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApplyUnitConversionTestUsingInvalidParameters()
        {
            var cost = new AssembliesCost();
            cost.ApplyUnitConversion(null);
        }
    }
}
