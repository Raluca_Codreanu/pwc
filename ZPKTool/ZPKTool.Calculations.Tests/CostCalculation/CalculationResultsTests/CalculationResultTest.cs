﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Unit tests for CalculationResult.
    /// </summary>
    [TestClass]
    public class CalculationResultTest
    {
        /// <summary>
        /// Clones test.
        /// </summary>
        [TestMethod]
        public void CloneTest()
        {
            var result = new CalculationResult()
            {
                AssembliesCost = new AssembliesCost() { AssemblyAmountsSum = 1, AssemblyCostsSum = 1 },
                CalculationAccuracy = Data.PartCalculationAccuracy.FineCalculation,
                CalculatorVersion = "1.4",
                CommoditiesCost = new CommoditiesCost() { CostsSum = 7, MarginSum = 2, RejectCostSum = 1 },
                ConsumablesCost = new ConsumablesCost() { CostsSum = 1, OverheadSum = 0.2m, TotalCostSum = 1.2m },
                DevelopmentCost = 2,
                External = true,
                InvestmentCostCumulated = 3000,
                LogisticCost = 1.3m,
                Name = "Test Calculation",
                OtherCost = 2,
                OverheadCost = new OverheadCost() { CommodityMargin = 0.1m, LogisticOverhead = 0.2m, SalesAndAdministrationOverhead = 0.1m },
                PackagingCost = 2.4m,
                PartNumber = "x-1000-ddf",
                PartsCost = new PartsCost() { PartAmountsSum = 2, PartCostsSum = 1 },
                PaymentCost = 2,
                ProcessCost = new ProcessCost() { DiesMaintenanceCostSum = 7, EstimatedManufacturingCostSum = 5, RejectCostSum = 1 },
                ProductionCost = 5,
                ProjectInvest = 10,
                PurchasePrice = 2,
                RawMaterialsCost = new RawMaterialsCost() { NetCostSum = 1, OverheadSum = 0.1m, WIPCostSum = 0.2m },
                Remark = "abcdefgh",
                SubpartsAndSubassembliesCost = 10,
                Summary = new CalculationResultSummary() { CommoditiesCost = 1, ManufacturingRejectCost = 1, PaymentCost = 2 },
                TargetPrice = 33,
                TotalCost = 22,
                TransportCost = 9,
                Weight = 1
            };
            result.InvestmentCost = new InvestmentCost();
            result.InvestmentCost.AddDieInvestment(new InvestmentCostSingle() { Amount = 1, Investment = 1000 });
            result.InvestmentCost.AddMachineInvestment(new InvestmentCostSingle() { Amount = 2, Investment = 1000 });

            var clone = result.Clone();

            AssertExtensions.AreEqualValueProperties(result, clone, true, true);
        }

        /// <summary>
        /// Applies the unit conversion using valid conversion parameters.
        /// </summary>
        [TestMethod]
        public void ApplyUnitConversionUsingValidConversionParameters()
        {
            var result = new CalculationResult()
            {
                AssembliesCost = new AssembliesCost() { AssemblyAmountsSum = 1, AssemblyCostsSum = 1 },
                CalculationAccuracy = Data.PartCalculationAccuracy.FineCalculation,
                CalculatorVersion = "1.4",
                CommoditiesCost = new CommoditiesCost() { CostsSum = 7, MarginSum = 2, RejectCostSum = 1 },
                ConsumablesCost = new ConsumablesCost() { CostsSum = 1, OverheadSum = 0.2m, TotalCostSum = 1.2m },
                DevelopmentCost = 2,
                External = true,
                InvestmentCostCumulated = 3000,
                LogisticCost = 1.3m,
                Name = "Test Calculation",
                OtherCost = 2,
                OverheadCost = new OverheadCost() { CommodityMargin = 0.1m, LogisticOverhead = 0.2m, SalesAndAdministrationOverhead = 0.1m },
                PackagingCost = 2.4m,
                PartNumber = "x-1000-ddf",
                PartsCost = new PartsCost() { PartAmountsSum = 2, PartCostsSum = 1 },
                PaymentCost = 2,
                ProcessCost = new ProcessCost() { DiesMaintenanceCostSum = 7, EstimatedManufacturingCostSum = 5, RejectCostSum = 1 },
                ProductionCost = 5,
                ProjectInvest = 10,                
                RawMaterialsCost = new RawMaterialsCost() { NetCostSum = 1, OverheadSum = 0.1m, WIPCostSum = 0.2m },
                Remark = "abcdefgh",
                SubpartsAndSubassembliesCost = 10,
                Summary = new CalculationResultSummary() { CommoditiesCost = 1, ManufacturingRejectCost = 1, PaymentCost = 2 },                
                TotalCost = 22,
                TransportCost = 9,
                Weight = 1
            };
            result.InvestmentCost = new InvestmentCost();
            result.InvestmentCost.AddDieInvestment(new InvestmentCostSingle() { Amount = 1, Investment = 1000 });
            result.InvestmentCost.AddMachineInvestment(new InvestmentCostSingle() { Amount = 2, Investment = 1000 });

            var conversionFactors = new UnitConversionFactors() { CurrencyFactor = 0.8m, WeightFactor = 1.6m };
            result.ApplyUnitConversion(conversionFactors);

            Assert.AreEqual(0.8m, result.AssembliesCost.AssemblyCostsSum);
            Assert.AreEqual(1.6m, result.DevelopmentCost);
            Assert.AreEqual(2400, result.InvestmentCostCumulated);
            Assert.AreEqual(1.04m, result.LogisticCost);
            Assert.AreEqual(1.6m, result.OtherCost);
            Assert.AreEqual(1.92m, result.PackagingCost);
            Assert.AreEqual(1.6m, result.PaymentCost);
            Assert.AreEqual(4, result.ProductionCost);
            Assert.AreEqual(8, result.ProjectInvest);            
            Assert.AreEqual(8, result.SubpartsAndSubassembliesCost);            
            Assert.AreEqual(17.6m, result.TotalCost);
            Assert.AreEqual(7.2m, result.TransportCost);
            Assert.AreEqual(0.625m, result.Weight);            
            Assert.AreEqual(5.6m, result.CommoditiesCost.CostsSum);
            Assert.AreEqual(1.6m, result.CommoditiesCost.MarginSum);
            Assert.AreEqual(0.8m, result.CommoditiesCost.RejectCostSum);
            Assert.AreEqual(0.8m, result.ConsumablesCost.CostsSum);
            Assert.AreEqual(0.08m, result.OverheadCost.CommodityMargin);
            Assert.AreEqual(0.8m, result.PartsCost.PartCostsSum);
            Assert.AreEqual(5.6m, result.ProcessCost.DiesMaintenanceCostSum);
            Assert.AreEqual(0.8m, result.RawMaterialsCost.NetCostSum);
            Assert.AreEqual(0.8m, result.Summary.CommoditiesCost);
            Assert.AreEqual(800, result.InvestmentCost.DieInvestments[0].Investment);            
        }

        /// <summary>
        /// Applies the unit conversion test using null parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApplyUnitConversionTestUsingNullParameters()
        {
            var result = new CalculationResult();
            result.ApplyUnitConversion(null);
        }
    }
}
