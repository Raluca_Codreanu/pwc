﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Calculations.Tests.CostCalculation.CalculationResultsTests
{
    /// <summary>
    /// Unit tests for the CalculationResultSummary.
    /// </summary>
    [TestClass]
    public class CalculationResultSummaryTest
    {
        /// <summary>
        /// Adds a valid summary test.
        /// </summary>
        [TestMethod]
        public void AddValidSummary()
        {
            var summary = new CalculationResultSummary()
            {
                CommoditiesCost = 1,
                ConsumableCost = 1,
                DevelopmentCost = 1,
                LogisticCost = 1,
                ManufacturingRejectCost = 1,
                ManufacturingTransportCost = 2,
                OfferExternalCalcPartsCost = 2,
                OtherCost = 3,
                OverheadAndMarginCost = 4,
                PackagingCost = 2,
                PaymentCost = 2,
                ProductionCost = 3,
                ProjectInvest = 3,
                RawMaterialCost = 5,
                SubpartsAndSubassembliesCost = 9,
                TargetCost = 2,
                TotalManufacturingCost = 1,
                TransportCost = 2,
                WIPCost = 2
            };

            var summaryToAdd = new CalculationResultSummary()
            {
                CommoditiesCost = 1,
                ConsumableCost = 1,
                DevelopmentCost = 1,
                LogisticCost = 1,
                ManufacturingRejectCost = 1,
                ManufacturingTransportCost = 2,
                OfferExternalCalcPartsCost = 2,
                OtherCost = 3,
                OverheadAndMarginCost = 4,
                PackagingCost = 2,
                PaymentCost = 2,
                ProductionCost = 3,
                ProjectInvest = 3,
                RawMaterialCost = 5,
                SubpartsAndSubassembliesCost = 9,
                TargetCost = 2,
                TotalManufacturingCost = 1,
                TransportCost = 2,
                WIPCost = 2
            };

            summary.Add(summaryToAdd);

            Assert.AreEqual(2, summary.CommoditiesCost);
            Assert.AreEqual(2, summary.ConsumableCost);
            Assert.AreEqual(2, summary.DevelopmentCost);
            Assert.AreEqual(2, summary.LogisticCost);
            Assert.AreEqual(2, summary.ManufacturingRejectCost);
            Assert.AreEqual(4, summary.ManufacturingTransportCost);
            Assert.AreEqual(4, summary.OfferExternalCalcPartsCost);
            Assert.AreEqual(6, summary.OtherCost);
            Assert.AreEqual(8, summary.OverheadAndMarginCost);
            Assert.AreEqual(4, summary.PackagingCost);
            Assert.AreEqual(4, summary.PaymentCost);
            Assert.AreEqual(6, summary.ProductionCost);
            Assert.AreEqual(6, summary.ProjectInvest);
            Assert.AreEqual(10, summary.RawMaterialCost);
            Assert.AreEqual(18, summary.SubpartsAndSubassembliesCost);
            Assert.AreEqual(4, summary.TargetCost);
            Assert.AreEqual(2, summary.TotalManufacturingCost);
            Assert.AreEqual(4, summary.TransportCost);
            Assert.AreEqual(4, summary.WIPCost);
        }

        /// <summary>
        /// Adds a null summary test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullSummary()
        {
            var summary = new CalculationResultSummary();
            summary.Add(null);
        }

        /// <summary>
        /// Clones a valid summary test.
        /// </summary>
        [TestMethod]
        public void CloneValidSummary()
        {
            var summary = new CalculationResultSummary()
            {
                CommoditiesCost = 1,
                ConsumableCost = 1,
                DevelopmentCost = 1,
                LogisticCost = 1,
                ManufacturingRejectCost = 1,
                ManufacturingTransportCost = 2,
                OfferExternalCalcPartsCost = 2,
                OtherCost = 3,
                OverheadAndMarginCost = 4,
                PackagingCost = 2,
                PaymentCost = 2,
                ProductionCost = 3,
                ProjectInvest = 3,
                RawMaterialCost = 5,
                SubpartsAndSubassembliesCost = 9,
                TargetCost = 2,
                TotalManufacturingCost = 1,
                TransportCost = 2,
                WIPCost = 2
            };

            var clone = summary.Clone();

            AssertExtensions.AreEqualValueProperties(summary, clone);
        }

        /// <summary>
        /// Applies the unit conversion using valid conversion parameters.
        /// </summary>
        [TestMethod]
        public void ApplyUnitConversionUsingValidConversionParameters()
        {
            var summary = new CalculationResultSummary()
            {
                CommoditiesCost = 1,
                ConsumableCost = 1,
                DevelopmentCost = 1,
                LogisticCost = 1,
                ManufacturingRejectCost = 1,
                ManufacturingTransportCost = 2,
                OfferExternalCalcPartsCost = 2,
                OtherCost = 3,
                OverheadAndMarginCost = 4,
                PackagingCost = 2,
                PaymentCost = 2,
                ProductionCost = 3,
                ProjectInvest = 3,
                RawMaterialCost = 5,
                SubpartsAndSubassembliesCost = 9,
                TargetCost = 2,
                TotalManufacturingCost = 1,
                TransportCost = 2,
                WIPCost = 2
            };

            var conversionFactors = new UnitConversionFactors() { CurrencyFactor = 2 };
            summary.ApplyUnitConversion(conversionFactors);

            Assert.AreEqual(2, summary.CommoditiesCost);
            Assert.AreEqual(2, summary.ConsumableCost);
            Assert.AreEqual(2, summary.DevelopmentCost);
            Assert.AreEqual(2, summary.LogisticCost);
            Assert.AreEqual(2, summary.ManufacturingRejectCost);
            Assert.AreEqual(4, summary.ManufacturingTransportCost);
            Assert.AreEqual(4, summary.OfferExternalCalcPartsCost);
            Assert.AreEqual(6, summary.OtherCost);
            Assert.AreEqual(8, summary.OverheadAndMarginCost);
            Assert.AreEqual(4, summary.PackagingCost);
            Assert.AreEqual(4, summary.PaymentCost);
            Assert.AreEqual(6, summary.ProductionCost);
            Assert.AreEqual(6, summary.ProjectInvest);
            Assert.AreEqual(10, summary.RawMaterialCost);
            Assert.AreEqual(18, summary.SubpartsAndSubassembliesCost);
            Assert.AreEqual(4, summary.TargetCost);
            Assert.AreEqual(2, summary.TotalManufacturingCost);
            Assert.AreEqual(4, summary.TransportCost);
            Assert.AreEqual(4, summary.WIPCost);
        }

        /// <summary>
        /// Applies the unit conversion test using null parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApplyUnitConversionTestUsingNullParameters()
        {
            var summary = new CalculationResultSummary();
            summary.ApplyUnitConversion(null);
        }
    }
}
