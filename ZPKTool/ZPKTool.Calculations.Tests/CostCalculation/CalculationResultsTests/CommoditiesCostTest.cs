﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Calculations.Tests.CostCalculation.CalculationResultsTests
{
    /// <summary>
    /// Unit tests for CommoditiesCost.
    /// </summary>
    [TestClass]
    public class CommoditiesCostTest
    {
        /// <summary>
        /// Adds cost using valid commodity cost test.
        /// </summary>
        [TestMethod]
        public void AddCostUsingValidCommodityCost()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 2,
                TotalCostSum = 5
            };

            var costToAdd = new CommodityCost()
            {
                Cost = 1,
                Overhead = 1,
                Margin = 1,
                RejectCost = 1,
                TotalCost = 4
            };

            cost.AddCost(costToAdd);

            Assert.AreEqual(2, cost.CostsSum);
            Assert.AreEqual(2, cost.MarginSum);
            Assert.AreEqual(2, cost.OverheadSum);
            Assert.AreEqual(3, cost.RejectCostSum);
            Assert.AreEqual(9, cost.TotalCostSum);
            Assert.AreEqual(1, cost.CommodityCosts.Count);
            Assert.IsTrue(cost.CommodityCosts.Contains(costToAdd));
        }

        /// <summary>
        /// Adds cost using null commodity cost test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddCostUsingNullCommodityCost()
        {
            var cost = new CommoditiesCost();
            cost.AddCost(null);
        }

        /// <summary>
        /// Adds costs using valid commodity costs test.
        /// </summary>
        [TestMethod]
        public void AddCostsUsingValidCommodityCosts()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 2,
                TotalCostSum = 5
            };

            var costsToAdd = new List<CommodityCost>()
            {
                new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 },
                new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 }
            };

            cost.AddCosts(costsToAdd);

            Assert.AreEqual(3, cost.CostsSum);
            Assert.AreEqual(3, cost.MarginSum);
            Assert.AreEqual(3, cost.OverheadSum);
            Assert.AreEqual(4, cost.RejectCostSum);
            Assert.AreEqual(13, cost.TotalCostSum);

            Assert.AreEqual(2, cost.CommodityCosts.Count);
            foreach (var addedCost in costsToAdd)
            {
                Assert.IsTrue(cost.CommodityCosts.Contains(addedCost));
            }
        }

        /// <summary>
        /// Adds costs using null commodity costs test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddCostsUsingNullCommodityCosts()
        {
            var cost = new CommoditiesCost();
            cost.AddCosts(null);
        }

        /// <summary>
        /// Adds valid commodities cost test.
        /// </summary>
        [TestMethod]
        public void AddUsingValidCommoditiesCost()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 2,
                TotalCostSum = 5
            };

            var costToAdd = new CommoditiesCost()
            {
                CostsSum = 2,
                MarginSum = 2,
                OverheadSum = 2,
                RejectCostSum = 2,
                TotalCostSum = 8,
                CommodityCosts = 
                { 
                    new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 },
                    new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 }
                }
            };

            cost.Add(costToAdd);

            Assert.AreEqual(3, cost.CostsSum);
            Assert.AreEqual(3, cost.MarginSum);
            Assert.AreEqual(3, cost.OverheadSum);
            Assert.AreEqual(4, cost.RejectCostSum);
            Assert.AreEqual(13, cost.TotalCostSum);

            Assert.AreEqual(2, cost.CommodityCosts.Count);
            foreach (var addedCost in costToAdd.CommodityCosts)
            {
                Assert.IsTrue(cost.CommodityCosts.Contains(addedCost));
            }
        }

        /// <summary>
        /// Adds commodities cost without commodity costs test.
        /// </summary>
        [TestMethod]
        public void AddUsingCommoditiesCostWithoutCommodityCosts()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 2,
                TotalCostSum = 5
            };

            var costToAdd = new CommoditiesCost()
            {
                CostsSum = 2,
                MarginSum = 2,
                OverheadSum = 2,
                RejectCostSum = 2,
                TotalCostSum = 8
            };

            cost.Add(costToAdd);

            Assert.AreEqual(1, cost.CostsSum);
            Assert.AreEqual(1, cost.MarginSum);
            Assert.AreEqual(1, cost.OverheadSum);
            Assert.AreEqual(2, cost.RejectCostSum);
            Assert.AreEqual(5, cost.TotalCostSum);
            Assert.AreEqual(0, cost.CommodityCosts.Count);
        }

        /// <summary>
        /// Adds null commodities cost test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddUsingNullCommoditiesCost()
        {
            var cost = new CommoditiesCost();
            cost.Add(null);
        }

        /// <summary>
        /// Recalculates the sums for valid cost test.
        /// </summary>
        [TestMethod]
        public void RecalculateSumsForValidCost()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 1,
                TotalCostSum = 4,
                CommodityCosts =
                {
                    new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 }
                }
            };

            // Edit an existing commodity cost
            cost.CommodityCosts[0].Cost = 2;
            cost.CommodityCosts[0].Overhead = 2;
            cost.CommodityCosts[0].TotalCost = 6;

            // Add a new commodity cost without using the AddCost(s) method.
            var newCommodityCost = new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 };
            cost.CommodityCosts.Add(newCommodityCost);

            cost.RecalculateSums();

            Assert.AreEqual(3, cost.CostsSum);
            Assert.AreEqual(2, cost.MarginSum);
            Assert.AreEqual(3, cost.OverheadSum);
            Assert.AreEqual(2, cost.RejectCostSum);
            Assert.AreEqual(10, cost.TotalCostSum);
        }

        /// <summary>
        /// Clones valid cost test.
        /// </summary>
        [TestMethod]
        public void CloneValidCost()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 1,
                TotalCostSum = 4,
                CommodityCosts =
                {
                    new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 }
                }
            };

            var clone = cost.Clone();

            AssertExtensions.AreEqualValueProperties(cost, clone, deepCheck: true);
        }

        /// <summary>
        /// Applies the unit conversion using valid conversion parameters.
        /// </summary>
        [TestMethod]
        public void ApplyUnitConversionUsingValidConversionParameters()
        {
            var cost = new CommoditiesCost()
            {
                CostsSum = 1,
                MarginSum = 1,
                OverheadSum = 1,
                RejectCostSum = 1,
                TotalCostSum = 4,
                CommodityCosts =
                {
                    new CommodityCost() { Cost = 1, Overhead = 1, Margin = 1, RejectCost = 1, TotalCost = 4 }
                }
            };

            var conversionFactors = new UnitConversionFactors() { CurrencyFactor = 2 };
            cost.ApplyUnitConversion(conversionFactors);

            Assert.AreEqual(2, cost.CostsSum);
            Assert.AreEqual(2, cost.MarginSum);
            Assert.AreEqual(2, cost.OverheadSum);
            Assert.AreEqual(2, cost.RejectCostSum);
            Assert.AreEqual(8, cost.TotalCostSum);
            Assert.AreEqual(2, cost.CommodityCosts[0].Cost);
        }

        /// <summary>
        /// Applies the unit conversion test using null parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApplyUnitConversionTestUsingNullParameters()
        {
            var cost = new CommoditiesCost();
            cost.ApplyUnitConversion(null);
        }
    }
}
