﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_4.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_4_Test
    {
        #region Fields

        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_4_Test"/> class.
        /// </summary>
        public CostCalculator_1_4_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Initialize the tests.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_v_1_4);
            customTestAssembly.SetCalculationVariant("1.4");
        }

        #endregion

        #region Tests Using Reference Calculation Models

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_4_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };

            var assembly = customTestAssembly;
            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);

            decimal expectedCost = 233.07525730659338073800146051M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_4_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var assembly = customTestAssembly;

            var calculator = new CostCalculator_1_4(basicSettings, imperialUnits);

            decimal expectedCost = 233.07525730659338073800146051M;
            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_4_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.4");

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(27.718659277036115170117442000M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_4_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.4");

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(93.10386567032668972308960322M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_4_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.4");

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(220.35587926696609287761674397M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        #endregion Tests Using Reference Calculation Models

        /// <summary>
        /// Machines the investment calculation test.
        /// </summary>
        [TestMethod]
        public void MachineInvestmentCalculationTest()
        {
            var machine = new Machine()
            {
                ManufacturingYear = 2013,
                DepreciationPeriod = 8,
                DepreciationRate = 0.06m,
                FloorSize = 30,
                WorkspaceArea = 15,
                PowerConsumption = 10,
                FullLoadRate = 0.98m,
                Availability = 0.95m,
                MachineLeaseCosts = 150,
                SetupInvestment = 1000,
                AdditionalEquipmentInvestment = 1500,
                FundamentalSetupInvestment = 800,
                CalculateWithKValue = true,
                KValue = 0.04m
            };

            var costCalcParams = new MachineCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProcessStepShiftsPerWeek = 15,
                ProcessStepExtraShiftsPerWeek = 0,
                ProcessStepHoursPerShift = 7.5m,
                ProcessStepProductionWeeksPerYear = 48,
                CountrySettingsEnergyCost = 0.09078195m,
                CountrySettingsAirCost = 0.0158019m,
                CountrySettingsWaterCost = 0.9876192m,
                CountrySettingsRentalCostProductionArea = 6.32076291m
            };

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);

            var machineCost = calculator.CalculateMachineCost(machine, costCalcParams);

            Assert.AreEqual(0.6383040935672514619883040936M, machineCost.DepreciationPerHour, "The machine depreciation cost was incorrect.");
            Assert.AreEqual(2.2667101612339181286549707603M, machineCost.FullCostPerHour, "The machine total cost was incorrect.");
        }

        /// <summary>
        /// Calculates the ExternalsSga test.
        /// </summary>
        [TestMethod]
        public void ExternalSGACalculationTest()
        {
            // Setup: have an assembly with 2 sub-assemblies and 2 sub-parts, each having an estimated cost of 1 euro and the Offer / External Calculation accuracy.
            // One sub-assembly is external and has external SGA and the other is external without SGA; same setup applies to parts.
            var rootAssembly = new Assembly()
            {
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };

            var subAssy1 = new Assembly()
            {
                Name = "A1",
                CalculationVariant = "1.4",
                CalculationAccuracy = PartCalculationAccuracy.OfferOrExternalCalculation,
                EstimatedCost = 1,
                IsExternal = true,
                ExternalSGA = true,
                Index = 0,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };

            var subPart1 = new Part()
            {
                Name = "P1",
                CalculationVariant = "1.4",
                CalculationAccuracy = PartCalculationAccuracy.OfferOrExternalCalculation,
                EstimatedCost = 1,
                IsExternal = true,
                ExternalSGA = true,
                Index = 0,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };

            var subAssy2 = new Assembly();
            subAssy1.CopyValuesTo(subAssy2);
            subAssy2.ExternalSGA = false;
            subAssy2.Index = 1;
            subAssy2.OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings();
            subAssy2.CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings();

            var subPart2 = new Part();
            subPart1.CopyValuesTo(subPart2);
            subPart2.ExternalSGA = false;
            subPart2.Index = 1;
            subPart2.OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings();
            subPart2.CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings();

            rootAssembly.Subassemblies.Add(subAssy1);
            rootAssembly.Subassemblies.Add(subAssy2);
            rootAssembly.Parts.Add(subPart1);
            rootAssembly.Parts.Add(subPart2);

            rootAssembly.Process.Steps.Add(new AssemblyProcessStep()
            {
                AssemblyAmounts = 
                { 
                    new ProcessStepAssemblyAmount() { Amount = 1, Assembly = subAssy1 },
                    new ProcessStepAssemblyAmount() { Amount = 1, Assembly = subAssy2 }
                },
                PartAmounts =
                { 
                    new ProcessStepPartAmount() { Amount = 1, Part = subPart1 },
                    new ProcessStepPartAmount() { Amount = 1, Part = subPart2 }
                }
            });

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);

            var cost = calculator.CalculateAssemblyCost(rootAssembly, new AssemblyCostCalculationParameters());

            Assert.AreEqual(0.1272M, cost.AssembliesCost.AssemblyCosts[0].ExternalSGA, "The external SGA cost of sub-assembly 1 was incorrect.");
            Assert.AreEqual(0M, cost.AssembliesCost.AssemblyCosts[1].ExternalSGA, "The external SGA cost of sub-assembly 2 was incorrect.");

            Assert.AreEqual(0.1272M, cost.PartsCost.PartCosts[0].ExternalSGA, "The external SGA cost of sub-part 1 was incorrect.");
            Assert.AreEqual(0M, cost.PartsCost.PartCosts[1].ExternalSGA, "The external SGA cost of sub-part 2 was incorrect.");

            Assert.AreEqual(0.2544m, cost.OverheadCost.SalesAndAdministrationOverhead, "The total external SGA cost was incorrect.");
        }

        /// <summary>
        /// Calculates the net lifetime production qunatity for part.
        /// </summary>
        [TestMethod]
        public void CalculateNetLifetimeProductionQunatityForPart()
        {
            var part = new Part()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 5
            };

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var qty = calculator.CalculateNetLifetimeProductionQuantity(part);

            Assert.AreEqual(62520, qty, "The calculated net lifetime production quantity for part was incorrect.");
        }

        /// <summary>
        /// Calculates the net lifetime production qunatity for assembly.
        /// </summary>
        [TestMethod]
        public void CalculateNetLifetimeProductionQunatityForAssembly()
        {
            var assy = new Assembly()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 5
            };

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var qty = calculator.CalculateNetLifetimeProductionQuantity(assy);

            Assert.AreEqual(62520, qty, "The calculated net lifetime production quantity for part was incorrect.");
        }

        /// <summary>
        /// Calculates the cost of assembly with null process.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfAssemblyWithNullProcess()
        {
            var assy = new Assembly()
            {
                CalculationVariant = "1.4",
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                DevelopmentCost = 1
            };
            assy.Parts.Add(new Part() { EstimatedCost = 1, CalculationAccuracy = PartCalculationAccuracy.OfferOrExternalCalculation });

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());
            Assert.AreEqual(0, result.ProcessCost.TotalManufacturingCostSum);
            Assert.AreEqual(1, result.TotalCost);
        }

        /// <summary>
        /// Calculates the yearly production quantity using null assembly.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateYearlyProductionQuantityUsingNullAssembly()
        {
            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            calculator.CalculateYearlyProductionQuantity((Assembly)null);
        }

        /// <summary>
        /// Calculates the yearly production quantity using null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateYearlyProductionQuantityUsingNullPart()
        {
            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);
            calculator.CalculateYearlyProductionQuantity((Part)null);
        }

        /// <summary>
        /// Calculates the yearly production quantity for part with raw part.
        /// </summary>
        [TestMethod]
        public void CalculateYearlyProductionQuantityForPartWithRawPart()
        {
            var part = new Part()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                Process = new Process()
                {
                    Steps = { new PartProcessStep() { Index = 0, ScrapAmount = 0.02m }, new PartProcessStep() { Index = 1, ScrapAmount = 0.03m } }
                }
            };

            var rawPart = new RawPart()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                Process = new Process()
            };

            part.RawPart = rawPart;
            rawPart.ParentOfRawPart.Add(part);

            var calculator = new CostCalculator_1_4(basicSettings, metricUnits);

            var calculatedProdQty = calculator.CalculateYearlyProductionQuantity(rawPart);

            Assert.AreEqual(13152M, calculatedProdQty, "The calculated yearly production quantity for a Part->RawPart hierarchy was incorrect.");
        }

        #region Misc Tests

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            var consumable = new Consumable();
            consumable.Amount = 1;
            consumable.Price = 1m;

            var calculator = new CostCalculator_1_4(basicSettings);
            var consumableCost = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());

            Assert.AreEqual(1, consumableCost.Cost, "The consumable cost was incorrect (calculator was not provided with measurement units).");
        }

        #endregion Misc Tests
    }
}
