using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_0.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_0_Test
    {
        /// <summary>
        /// The custom assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_0_Test"/> class.
        /// </summary>
        public CostCalculator_1_0_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Initializes the tests in CostCalculator_1_0Test.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_Old);
            customTestAssembly.SetCalculationVariant("1.0");
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }

        /////// <summary>
        /////// This method is executed before running each test.
        /////// The methods set "calculationVersions" variable.
        /////// </summary>
        ////[TestInitialize()]
        ////public void MyTestInitialize()
        ////{
        ////}

        #endregion Additional test attributes

        #region Assembly Tests

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_0_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };
            var assembly = customTestAssembly;
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            decimal expectedCost = 224.43817318736686879211491906M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_0_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };
            var assembly = customTestAssembly;
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, imperialUnits);

            decimal expectedCost = 224.43817318736686879211491906M;

            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_0_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.0");

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(24.320285348332441904109844153M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_0_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.0");

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(89.5196699863493146511904962M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_0_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.0");

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(215.10418015383640150035324111M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        /// <summary>
        /// Calculates the assy cost with null assembly.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateAssyCostWithNullAssy()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateAssemblyCost(null, null);
        }

        /// <summary>
        /// Calculates the assembly cost with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateAssyCostWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateAssemblyCost(CostCalculator_1_0_Test.customTestAssembly, null);
        }

        /// <summary>
        /// Calculates the assembly cost with null overhead settings.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CalculateAssemblyCostWithNullOverheadSettings()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Assembly assy = new Assembly();
            assy.CountrySettings = new CountrySetting();

            calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the assembly with null country settings.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CalculateAssemblyWithNullCountrySettings()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Assembly assy = new Assembly();
            assy.OverheadSettings = new OverheadSetting();

            calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());
        }

        /// <summary>
        /// Test the cost calculation for an assembly when its additional costs are spread over the total number of pieces produced in its lifetime.
        /// </summary>
        [TestMethod]
        public void CalculateAssemblyCost_AdditionalCostsArePerPartsTotal()
        {
            Assembly assy = new Assembly();
            assy.OverheadSettings = new OverheadSetting();
            assy.CountrySettings = new CountrySetting();
            assy.BatchSizePerYear = 12;
            assy.YearlyProductionQuantity = 10000;
            assy.LifeTime = 8;
            assy.AreAdditionalCostsPerPartsTotal = true;

            assy.CalculateLogisticCost = false;
            assy.DevelopmentCost = 50000;
            assy.ProjectInvest = 125000;
            assy.OtherCost = 3000;
            assy.PackagingCost = 5500;
            assy.TransportCost = 7000;
            assy.LogisticCost = 6000;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());

            Assert.AreEqual(result.DevelopmentCost, 0.625M);
            Assert.AreEqual(result.ProjectInvest, 1.5625M);
            Assert.AreEqual(result.OtherCost, 0.0375M);
            Assert.AreEqual(result.PackagingCost, 0.06875M);
            Assert.AreEqual(result.TransportCost, 0.0875M);
            Assert.AreEqual(result.LogisticCost, 0.075M);

            // If the logistic cost is calculated
            assy.CalculateLogisticCost = true;
            var result2 = calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());

            Assert.AreEqual(result2.LogisticCost, 0M); // The calculated logistic cost is per part, not per parts total
        }

        /// <summary>
        /// Tests if the cost of sub-parts and sub-assemblies deleted to trash bin is ignored.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfAssemblyWithDeletedSubPartsAndSubAssembly()
        {
            var subAssy = new Assembly();
            subAssy.CalculationAccuracy = PartCalculationAccuracy.OfferOrExternalCalculation;
            subAssy.EstimatedCost = 1;
            subAssy.OverheadSettings = new OverheadSetting();
            subAssy.CountrySettings = new CountrySetting();
            subAssy.IsDeleted = true;

            var subPart = new Part();
            subPart.CalculationAccuracy = PartCalculationAccuracy.OfferOrExternalCalculation;
            subPart.EstimatedCost = 1;
            subPart.OverheadSettings = new OverheadSetting();
            subPart.CountrySettings = new CountrySetting();
            subPart.IsDeleted = true;

            var assembly = new Assembly();
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Process = new Process();

            assembly.Parts.Add(subPart);
            assembly.Subassemblies.Add(subAssy);

            var processStep = new AssemblyProcessStep();
            assembly.Process.Steps.Add(processStep);

            processStep.PartAmounts.Add(new ProcessStepPartAmount() { Amount = 1, Part = subPart });
            processStep.AssemblyAmounts.Add(new ProcessStepAssemblyAmount() { Amount = 1, Assembly = subAssy });

            var costCalculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var assemblyCost = costCalculator.CalculateAssemblyCost(assembly, new AssemblyCostCalculationParameters());

            Assert.AreEqual(0m, assemblyCost.TotalCost);
        }

        #endregion Assembly Tests

        #region Commodity Tests

        /// <summary>
        /// Calculates the commodity cost test with null commodity.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateCommodityCostTestWithNullCommodity()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateCommodityCost(null, new CommodityCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the commodity cost test with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateCommodityCostTestWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateCommodityCost(new Commodity(), null);
        }

        #endregion Commodity Tests

        #region Consumable Tests

        /// <summary>
        /// Calculates the consumable cost test with null consumable.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateConsumableCostTestWithNullConsumable()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateConsumableCost(null, new ConsumableCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the consumable cost with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateConsumableCostWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateConsumableCost(new Consumable(), null);
        }

        #endregion Consumable Tests

        #region Die Tests

        /// <summary>
        /// Calculates the die cost test with null die.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateDieCostTestWithNullDie()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateDieCost(null, new DieCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the die cost test with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateDieCostTestWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateDieCost(new Die(), null);
        }

        #endregion Die Tests

        #region Machine Tests

        /// <summary>
        /// Calculates the machine cost test with null machine.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateMachineCostTestWithNullMachine()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateMachineCost(null, new MachineCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the machine cost test with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateMachineCostTestWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculateMachineCost(new Machine(), null);
        }

        #endregion Machine Tests

        #region Part Tests

        /// <summary>
        /// Tests the cost calculation for a part when its additional costs are spread over the total number of pieces produced in its lifetime.
        /// </summary>
        [TestMethod]
        public void CalculatePartCost_AdditionalCostsArePerPartsTotal()
        {
            Part part = new Part();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.BatchSizePerYear = 12;
            part.YearlyProductionQuantity = 10000;
            part.LifeTime = 6;
            part.AreAdditionalCostsPerPartsTotal = true;

            RawMaterial abs = new RawMaterial()
            {
                Name = "ABS",
                Price = 2.28M,
                ParentWeight = 1,
                Sprue = 0,
                Quantity = 1,
                Loss = 0,
                ScrapRefundRatio = 0,
                RejectRatio = 0,
                RecyclingRatio = 0
            };
            part.RawMaterials.Add(abs);

            part.CalculateLogisticCost = false;
            part.DevelopmentCost = 100000;
            part.ProjectInvest = 250000;
            part.OtherCost = 3000;
            part.PackagingCost = 5000;
            part.TransportCost = 6000;
            part.LogisticCost = 15000;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(result.DevelopmentCost, 1.6666666666666666666666666667M);
            Assert.AreEqual(result.ProjectInvest, 4.1666666666666666666666666667M);
            Assert.AreEqual(result.OtherCost, 0.05M);
            Assert.AreEqual(result.PackagingCost, 0.0833333333333333333333333333M);
            Assert.AreEqual(result.TransportCost, 0.1M);
            Assert.AreEqual(result.LogisticCost, 0.25M);

            // If the logistic cost is calculated
            part.CalculateLogisticCost = true;
            var result2 = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(result2.LogisticCost, 0.0912M); // The calculated logistic cost is per part, not per parts total
        }

        /// <summary>
        /// Calculates the part cost with null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculatePartCostWithNullPart()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            calculator.CalculatePartCost(null, new PartCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the part cost with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculatePartCostWithNullCalculationParameters()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var part = new Part() { OverheadSettings = new OverheadSetting(), CountrySettings = new CountrySetting() };
            calculator.CalculatePartCost(part, null);
        }

        /// <summary>
        /// Calculates the part cost with null overhead settings.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CalculatePartCostWithNullOverheadSettings()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Part part = new Part();
            part.CountrySettings = new CountrySetting();

            calculator.CalculatePartCost(part, new PartCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the part cost with null country settings.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CalculatePartCostWithNullCountrySettings()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Part part = new Part();
            part.OverheadSettings = new OverheadSetting();

            calculator.CalculatePartCost(part, new PartCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the part weight test with null parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculatePartWeightTestWithNullParam()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            Part part = null;
            calculator.CalculatePartWeight(part);
        }

        /// <summary>
        /// Calculates the part weight test.
        /// </summary>
        [TestMethod]
        public void CalculatePartWeightTest()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Part part = customTestAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part");
            var result = calculator.CalculatePartWeight(part);
            Assert.AreEqual(2.25m, result, "The calculated part weight was wrong.");
        }

        #endregion Part Tests

        #region Process Tests

        /// <summary>
        /// Calculates the process material test with null parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateProcessMaterialTestWithNullParam()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            Part part = null;
            calculator.CalculateProcessMaterial(part);
        }

        /// <summary>
        /// Calculates the process material test.
        /// </summary>
        [TestMethod]
        public void CalculateProcessMaterialTest()
        {
            Part part = customTestAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part");

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateProcessMaterial(part);
            Assert.AreEqual(2.7021m, result, "The calculated process material weight was wrong.");
        }

        #endregion Process Tests

        #region Raw Material Tests

        /// <summary>
        /// Calculates the raw material cost with null material.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateRawMaterialCostWithNullMaterial()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result1 = calculator.CalculateRawMaterialCost(null, null);
        }

        // TODO: CalculateRawMaterialCostWithNullCalculationParameters and valid material -> check that the OH is not calculated but Net cost is.
        #endregion Raw Material Tests

        #region Consumable Tests

        /// <summary>
        /// The the consumable cost calculation using tonne as its amount unit.
        /// </summary>
        [TestMethod]
        public void CalculateConsumableCostWithAmountInTonnes()
        {
            // Create and add into the test assembly a consumable that has it amount in tonnes.            
            var tonne = new MeasurementUnit() { Name = "Tonne", ConversionRate = 1000, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 1000 };
            var consumable = new Consumable()
            {
                Price = 0.5m,
                Amount = 0.001m,
                AmountUnitBase = tonne
            };

            var costCalculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var consumableCost = costCalculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());
            Assert.AreEqual(0.5M, consumableCost.Cost);
        }

        /// <summary>
        /// The the consumable cost calculation without using an amount unit (it should default to kg).
        /// </summary>
        [TestMethod]
        public void CalculateConsumableCostWithoutAmountUnit()
        {
            // Create and add into the test assembly a consumable that has it amount in tonnes.                        
            var consumable = new Consumable()
            {
                Price = 0.5m,
                Amount = 2m,
            };

            var costCalculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var consumableCost = costCalculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());
            Assert.AreEqual(1, consumableCost.Cost);
        }

        #endregion

        #region Other Tests

        /// <summary>
        /// Calculates the net lifetime production quantity with valid assembly.
        /// </summary>
        [TestMethod]
        public void CalculateNetLifetimeProductionQuantityWithValidAssembly()
        {
            Assembly assy = new Assembly();
            assy.YearlyProductionQuantity = 10000;
            assy.LifeTime = 5;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            var result = calculator.CalculateNetLifetimeProductionQuantity(assy);
            Assert.AreEqual(50000m, result, "The calculated lifetime production quantity for an Assembly was wrong.");
        }

        /// <summary>
        /// Calculates the net lifetime production quantity with null assembly.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateNetLifetimeProductionQuantityWithNullAssembly()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateNetLifetimeProductionQuantity((Assembly)null);
        }

        /// <summary>
        /// Calculates the net lifetime production quantity with valid part.
        /// </summary>
        [TestMethod]
        public void CalculateNetLifetimeProductionQuantityWithValidPart()
        {
            Part part = new Part();
            part.YearlyProductionQuantity = 10000;
            part.LifeTime = 5;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            var result = calculator.CalculateNetLifetimeProductionQuantity(part);
            Assert.AreEqual(50000m, result, "The calculated lifetime production quantity for a Part was wrong.");
        }

        /// <summary>
        /// Calculates the net lifetime production quantity with null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateNetLifetimeProductionQuantityWithNullPart()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);
            var result = calculator.CalculateNetLifetimeProductionQuantity((Part)null);
        }

        /// <summary>
        /// Calculates the gross lifetime production qunatity for assembly test.
        /// </summary>
        [TestMethod]
        public void CalculateGrossLifetimeProductionQunatityForAssemblyTest()
        {
            Assembly assy = new Assembly();
            assy.YearlyProductionQuantity = 10000;
            assy.LifeTime = 5;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            var result = calculator.CalculateGrossLifetimeProductionQuantity(assy, new AssemblyCostCalculationParameters());
            Assert.AreEqual(50000m, result, "The calculated number of parts needed for an Assembly was wrong.");
        }

        /// <summary>
        /// Calculates the gross lifetime production qunatity for part test.
        /// </summary>
        [TestMethod]
        public void CalculateGrossLifetimeProductionQunatityForPartTest()
        {
            Part part = new Part();
            part.YearlyProductionQuantity = 10000;
            part.LifeTime = 5;

            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            var result = calculator.CalculateGrossLifetimeProductionQuantity(part, new PartCostCalculationParameters());
            Assert.AreEqual(50000m, result, "The calculated number of parts needed for a Part was wrong.");
        }

        /// <summary>
        /// Calculates the cost of assemblies and parts when the calculator's basic settings are not set.        
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateWithNullBasicSettings()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(null, metricUnits);

            calculator.CalculateAssemblyCost(new Assembly(), new AssemblyCostCalculationParameters());
            calculator.CalculatePartCost(new Part(), new PartCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the yearly production quantity test.
        /// </summary>
        [TestMethod]
        public void CalculateYearlyProductionQuantityTest()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, metricUnits);

            Part part = new Part();
            var result1 = calculator.CalculateYearlyProductionQuantity(part);
            Assert.AreEqual(result1, -1);

            Assembly assy = new Assembly();
            var result2 = calculator.CalculateYearlyProductionQuantity(assy);
            Assert.AreEqual(result2, -1);
        }

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            CostCalculator_1_0 calculator = new CostCalculator_1_0(basicSettings, new List<MeasurementUnit>());

            var weightUnitProp = calculator.GetType().GetProperty("WeightUnit", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var weightUnit = weightUnitProp.GetValue(calculator, null) as MeasurementUnit;

            Assert.IsNotNull(weightUnit);
            Assert.AreEqual(1M, weightUnit.ConversionRate);
        }

        #endregion Other Tests
    }
}
