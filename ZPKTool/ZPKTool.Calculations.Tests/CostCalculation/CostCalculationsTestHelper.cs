﻿namespace ZPKTool.Calculations.Tests.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using Ionic.Zip;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using ZPKTool.Business.Export;
    using ZPKTool.Calculations.CostCalculation;
    using ZPKTool.Data;

    /// <summary>
    /// Utility methods for CostCalculation unit tests.
    /// </summary>
    public static class CostCalculationsTestHelper
    {
        /// <summary>
        /// Imports a calculation model from a specified file.
        /// </summary>
        /// <param name="fileBytes">The file content.</param>
        /// <returns>
        /// The entity imported from the file.
        /// </returns>
        public static object ImportModel(byte[] fileBytes)
        {
            using (var fileStream = new MemoryStream(fileBytes))
            {
                MemoryStream modelStream = null;
                try
                {
                    if (ZipFile.IsZipFile(fileStream, false))
                    {
                        fileStream.Seek(0, SeekOrigin.Begin);
                        using (ZipFile zip = ZipFile.Read(fileStream))
                        {
                            zip.AlternateEncoding = Encoding.UTF8;
                            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                            ZipEntry modelFileEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "ModelData.bin", StringComparison.OrdinalIgnoreCase) == 0);
                            if (modelFileEntry == null)
                            {
                                modelFileEntry = zip.First();
                            }

                            modelStream = new MemoryStream();
                            modelFileEntry.Extract(modelStream);
                        }
                    }
                    else
                    {
                        modelStream = new MemoryStream(fileBytes);
                    }

                    modelStream.Seek(0, SeekOrigin.Begin);
                    var serializer = new ZPKTool.Business.Export.Serializer();
                    ExportedObject model = (ExportedObject)serializer.Deserialize(modelStream);

                    ExportMapper mapper = new ExportMapper();
                    return mapper.MapExportedObject(model);
                }
                finally
                {
                    if (modelStream != null)
                    {
                        modelStream.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Imports a calculation model from a specified file.
        /// </summary>
        /// <typeparam name="T">The type of the model.</typeparam>
        /// <param name="fileBytes">The file content.</param>
        /// <returns>
        /// The entity imported from the file.
        /// </returns>
        public static T ImportModel<T>(byte[] fileBytes)
        {
            T model = (T)CostCalculationsTestHelper.ImportModel(fileBytes);

            // Fix-up relationships workaround for assemblies and parts (explained in the fix-up method's documentation).
            var assembly = model as Assembly;
            if (assembly != null)
            {
                FixupAssemblyHierarchyRelationships(assembly);
            }

            return model;
        }

        /// <summary>
        /// Gets the reference basic settings used by all calculation tests.
        /// </summary>
        /// <returns>A proxy object for the basic settings.</returns>
        public static BasicSetting GetReferenceBasicSettings()
        {
            return new BasicSetting()
            {
                DeprPeriod = 8,
                DeprRate = 0.06m,
                PartBatch = 12,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProdDays = 5,
                ProdWeeks = 48,
                AssetRate = 0.055m,
                LogisticCostRatio = 0.04m
            };
        }

        /// <summary>
        /// Gets the metric units used for cost calculation tests.
        /// </summary>
        /// <returns>The units.</returns>
        public static List<MeasurementUnit> GetMetricUnits()
        {
            var kg = new MeasurementUnit() { Name = "Kilogram", ConversionRate = 1, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 1 };
            var meter = new MeasurementUnit() { Name = "Meter", ConversionRate = 1, Type = MeasurementUnitType.Length, ScaleID = MeasurementUnitScale.MetricLengthScale, ScaleFactor = 1 };
            var squareMeter = new MeasurementUnit() { Name = "Square Meter", ConversionRate = 1, Type = MeasurementUnitType.Area, ScaleID = MeasurementUnitScale.MetricAreaScale, ScaleFactor = 1 };
            var cubicMeter = new MeasurementUnit() { Name = "Cubic Meter", ConversionRate = 1, Type = MeasurementUnitType.Volume, ScaleID = MeasurementUnitScale.MetricVolumeScale, ScaleFactor = 1 };

            return new List<MeasurementUnit>() { kg, meter, squareMeter, cubicMeter };
        }

        /// <summary>
        /// Gets the imperial units used for cost calculation tests.
        /// </summary>
        /// <returns>The units.</returns>
        public static List<MeasurementUnit> GetImperialUnits()
        {
            var pound = new MeasurementUnit() { Name = "Pound", ConversionRate = 2.20462m, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.ImperialWeightScale, ScaleFactor = 1m };
            var foot = new MeasurementUnit() { Name = "Foot", ConversionRate = 3.28083m, Type = MeasurementUnitType.Length, ScaleID = MeasurementUnitScale.ImperialLengthScale, ScaleFactor = 1 };
            var squareFoot = new MeasurementUnit() { Name = "Square Foot", ConversionRate = 10.76390m, Type = MeasurementUnitType.Area, ScaleID = MeasurementUnitScale.ImperialAreaScale, ScaleFactor = 1 };
            var cubicFoot = new MeasurementUnit() { Name = "Cubic Foot", ConversionRate = 35.31466m, Type = MeasurementUnitType.Volume, ScaleID = MeasurementUnitScale.ImperialVolumeScale, ScaleFactor = 1 };

            return new List<MeasurementUnit>() { pound, foot, squareFoot, cubicFoot };
        }

        /// <summary>
        /// Gets the reference country settings to use in tests (they are the settings of Germany country).
        /// </summary>
        /// <returns>Country settings.</returns>
        public static CountrySetting GetReferenceCountrySettings()
        {
            return new CountrySetting()
            {
                UnskilledLaborCost = 15.47175026m,
                SkilledLaborCost = 17.56833907m,
                ForemanCost = 21.46046805m,
                TechnicianCost = 26.12672212m,
                EngineerCost = 36.84620441m,
                ShiftCharge1ShiftModel = 0.51m,
                ShiftCharge2ShiftModel = 0.51m,
                ShiftCharge3ShiftModel = 0.59m,
                LaborAvailability = 0.85m,
                EnergyCost = 0.09078195m,
                AirCost = 0.0158019m,
                WaterCost = 0.9876192m,
                ProductionAreaRentalCost = 6.32076291m,
                OfficeAreaRentalCost = 10.11322066m,
                InterestRate = 0.06m
            };
        }

        /// <summary>
        /// Gets the reference overhead settings to use in tests.
        /// </summary>
        /// <returns>Overhead settings.</returns>
        public static OverheadSetting GetReferenceOverheadSettings()
        {
            return new OverheadSetting()
            {
                MaterialOverhead = 0.04m,
                CommodityOverhead = 0.03m,
                ManufacturingOverhead = 0.25m,
                PackagingOHValue = 0.04m,
                SalesAndAdministrationOHValue = 0.12m,
                ConsumableOverhead = 0.04m,
                ExternalWorkOverhead = 0.04m,
                OtherCostOHValue = 0.04m,
                LogisticOHValue = 0.04m,
                CompanySurchargeOverhead = 0m,
                MaterialMargin = 0.03m,
                CommodityMargin = 0.03m,
                ManufacturingMargin = 0.08m,
                ConsumableMargin = 0.03m,
                ExternalWorkMargin = 0.03m
            };
        }

        /// <summary>
        /// Fixes the relationships between the parts and assemblies of the specified assembly to be both way. Currently, a child (assy or part)
        /// is added into the parent but its reference to the parent is not set during import; its reference is set only when it is saved in the db.
        /// This is due to the POCO usage in Entity Framework 5.
        /// <para/>
        /// The cost calculation needs the child to parent reference to be set in order to calculate the YearlyProductionQuantity, which takes into consideration
        /// the reject rate from the parent hierarchy.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private static void FixupAssemblyHierarchyRelationships(Assembly assembly)
        {
            foreach (var part in assembly.Parts)
            {
                part.Assembly = assembly;
                FixupAssemblyHierarchyRelationships(part);
            }

            foreach (var subAssy in assembly.Subassemblies)
            {
                subAssy.ParentAssembly = assembly;
                FixupAssemblyHierarchyRelationships(subAssy);
            }
        }

        /// <summary>
        /// Same logic as for Assembly.
        /// </summary>
        /// <param name="part">The part.</param>
        private static void FixupAssemblyHierarchyRelationships(Part part)
        {
            if (part.RawPart != null)
            {
                part.RawPart.ParentOfRawPart.Add(part);
            }
        }
    }
}
