﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_1_1.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_1_1_Test
    {
        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1_1_Test"/> class.
        /// </summary>
        public CostCalculator_1_1_1_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// The unit tests initlizations.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_Old);
            customTestAssembly.SetCalculationVariant("1.1.1");
        }

        #endregion

        #region Tests Using Reference Calculation Models

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_1_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };

            var assembly = customTestAssembly;
            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);

            decimal expectedCost = 232.60132896529718179972520307M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_1_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var assembly = customTestAssembly;

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, imperialUnits);

            decimal expectedCost = 232.60132896529718179972520307M;
            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_1_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.1.1");

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(31.157744445281464434166109632M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_1_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.1.1");

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(93.36882216279616876839516936M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_1_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.1.1");

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(222.37350657152579183170057961M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        #endregion Tests Using Reference Calculation Models

        #region Process tests

        /// <summary>
        /// Calculates the process cost for estimated part.
        /// </summary>
        [TestMethod]
        public void CalculateProcessCostForEstimatedPart()
        {
            var process = new Process();
            var step = new PartProcessStep()
            {
                Accuracy = ProcessCalculationAccuracy.Estimated,
                Price = 1m
            };
            process.Steps.Add(step);

            var step2 = new PartProcessStep()
            {
                Accuracy = (short)ProcessCalculationAccuracy.Calculated,
                ProcessTime = 600,
                CycleTime = 600,
                PartsPerCycle = 1,
                ScrapAmount = 0.01M,
                SetupsPerBatch = 1,
                SetupTime = 300,
                BatchSize = 834,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48,
                ProductionUnskilledLabour = 1,
                SetupUnskilledLabour = 1,
                IsExternal = true,
                TransportCost = 0.5m,
                TransportCostType = (short)ProcessTransportCostType.PerPart
            };
            process.Steps.Add(step2);

            step2.Machines.Add(new Machine()
            {
                ManufacturingYear = 2010,
                DepreciationPeriod = 8,
                DepreciationRate = 0.06m,
                FloorSize = 30,
                PowerConsumption = 10,
                Availability = 0.96m,
                MachineInvestment = 115000,
                CalculateWithKValue = true,
                KValue = 0.04m
            });
            step2.Dies.Add(new Die()
            {
                Investment = 10000,
                IsMaintenanceInPercentage = false,
                Maintenance = 0.02m,
                LifeTime = 100000,
                AllocationRatio = 1,
                DiesetsNumberPaidByCustomer = 0,
                CostAllocationBasedOnPartsPerLifeTime = true
            });
            step2.Consumables.Add(new Consumable()
            {
                Amount = 1,
                Price = 0.1m
            });

            var part = new Part()
            {
                CalculationAccuracy = PartCalculationAccuracy.RoughCalculation,
                CalculationVariant = "1.1.1",
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 10000,
                LifeTime = 4,
                AssetRate = 0.055m,
                SBMActive = true,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = process
            };

            var calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);

            // Invoke the tested method using reflection
            var calcProcessMethod = calculator.GetType().GetMethod(
                "CalculateProcessCost",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                Type.DefaultBinder,
                new Type[] { typeof(Part), typeof(PartProcessCostCalculationParameters) },
                null);

            var processCost = (ProcessCost)calcProcessMethod.Invoke(calculator, new object[] { part, new PartProcessCostCalculationParameters() });

            Assert.AreEqual(0M, processCost.CommoditiesCost.TotalCostSum);
            Assert.AreEqual(0M, processCost.ConsumablesCost.TotalCostSum);
            Assert.AreEqual(0M, processCost.DiesMaintenanceCostSum);
            Assert.AreEqual(0M, processCost.DirectLabourCostSum);
            Assert.AreEqual(0M, processCost.InvestmentCost.TotalInvestment);
            Assert.AreEqual(0M, processCost.MachineCostSum);
            Assert.AreEqual(0M, processCost.ManufacturingCostSum);
            Assert.AreEqual(0M, processCost.ManufacturingOverheadSum);
            Assert.AreEqual(0M, processCost.ManufacturingTransportCost);
            Assert.AreEqual(0M, processCost.RejectCostSum);
            Assert.AreEqual(0M, processCost.RejectOverview.TotalRejectCost);
            Assert.AreEqual(0M, processCost.SetupCostSum);
            Assert.AreEqual(0M, processCost.ToolAndDieCostSum);
            Assert.AreEqual(0M, processCost.TotalManufacturingCostSum);

            Assert.AreEqual(0, processCost.StepCosts.Count, "The process cost contained step cost(s) even though the process had no steps.");
        }

        /// <summary>
        /// Calculates the process cost for estimated assembly.
        /// </summary>
        [TestMethod]
        public void CalculateProcessCostForEstimatedAssembly()
        {
            var process = new Process();

            var step = new AssemblyProcessStep()
            {
                Accuracy = ProcessCalculationAccuracy.Estimated,
                Price = 1m
            };
            process.Steps.Add(step);

            var step2 = new AssemblyProcessStep()
            {
                Accuracy = (short)ProcessCalculationAccuracy.Calculated,
                ProcessTime = 600,
                CycleTime = 600,
                PartsPerCycle = 1,
                ScrapAmount = 0.01M,
                SetupsPerBatch = 1,
                SetupTime = 300,
                BatchSize = 834,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48,
                ProductionUnskilledLabour = 1,
                SetupUnskilledLabour = 1,
                IsExternal = true,
                TransportCost = 0.5m,
                TransportCostType = (short)ProcessTransportCostType.PerPart
            };
            process.Steps.Add(step2);

            step2.Machines.Add(new Machine()
            {
                ManufacturingYear = 2010,
                DepreciationPeriod = 8,
                DepreciationRate = 0.06m,
                FloorSize = 30,
                PowerConsumption = 10,
                Availability = 0.96m,
                MachineInvestment = 115000,
                CalculateWithKValue = true,
                KValue = 0.04m
            });
            step2.Dies.Add(new Die()
            {
                Investment = 10000,
                IsMaintenanceInPercentage = false,
                Maintenance = 0.02m,
                LifeTime = 100000,
                AllocationRatio = 1,
                DiesetsNumberPaidByCustomer = 0,
                CostAllocationBasedOnPartsPerLifeTime = true
            });
            step2.Consumables.Add(new Consumable()
            {
                Amount = 1,
                Price = 0.1m
            });
            step2.Commodities.Add(new Commodity()
            {
                Price = 0.2m,
                Amount = 2,
                RejectRatio = 0.015m,
                Weight = 0.01m
            });

            var assembly = new Assembly()
            {
                CalculationAccuracy = PartCalculationAccuracy.Estimation,
                CalculationVariant = "1.1.1",
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 10000,
                LifeTime = 4,
                AssetRate = 0.055m,
                SBMActive = true,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = process
            };

            var calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);

            // Invoke the tested method using reflection
            var calcProcessMethod = calculator.GetType().GetMethod(
                "CalculateProcessCost",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                Type.DefaultBinder,
                new Type[] { typeof(Assembly), typeof(AssemblyProcessCostCalculationParameters) },
                null);

            var processCost = (ProcessCost)calcProcessMethod.Invoke(calculator, new object[] { assembly, new AssemblyProcessCostCalculationParameters() });

            Assert.AreEqual(0M, processCost.CommoditiesCost.TotalCostSum);
            Assert.AreEqual(0M, processCost.ConsumablesCost.TotalCostSum);
            Assert.AreEqual(0M, processCost.DiesMaintenanceCostSum);
            Assert.AreEqual(0M, processCost.DirectLabourCostSum);
            Assert.AreEqual(0M, processCost.InvestmentCost.TotalInvestment);
            Assert.AreEqual(0M, processCost.MachineCostSum);
            Assert.AreEqual(0M, processCost.ManufacturingCostSum);
            Assert.AreEqual(0M, processCost.ManufacturingOverheadSum);
            Assert.AreEqual(0M, processCost.ManufacturingTransportCost);
            Assert.AreEqual(0M, processCost.RejectCostSum);
            Assert.AreEqual(0M, processCost.RejectOverview.TotalRejectCost);
            Assert.AreEqual(0M, processCost.SetupCostSum);
            Assert.AreEqual(0M, processCost.ToolAndDieCostSum);
            Assert.AreEqual(0M, processCost.TotalManufacturingCostSum);

            Assert.AreEqual(0, processCost.StepCosts.Count, "The process cost contained step cost(s) even though the process had no steps.");
        }

        #endregion Process tests

        /// <summary>
        /// Calculates the gross lifetime production quantity for assembly.
        /// </summary>
        [TestMethod]
        public void CalculateGrossLifetimeProductionQuantityForAssembly()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);
            var assy = customTestAssembly;
            decimal totalPartsNeeded = calculator.CalculateGrossLifetimeProductionQuantity(assy, costCalcParams);
            Assert.AreEqual(262164, totalPartsNeeded, "The calculated total number of parts needed by the super assembly was wrong.");
        }

        /// <summary>
        /// Calculates the gross lifetime production qunatity for part.
        /// </summary>
        [TestMethod]
        public void CalculateGrossLifetimeProductionQunatityForPart()
        {
            var costCalcParams = new PartCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Part testPart = customTestAssembly
                .Subassemblies.FirstOrDefault(s => s.Name == "Havac Main Unit - Center Box")
                .Parts.FirstOrDefault(p => p.Name == "Plastic Frame - Heater");

            CostCalculator_1_1_1 calculator = new CostCalculator_1_1_1(basicSettings, metricUnits);

            decimal totalPartsNeeded = calculator.CalculateGrossLifetimeProductionQuantity(testPart, costCalcParams);
            Assert.AreEqual(187260, totalPartsNeeded, "The calculated total number of parts needed by the test part was wrong.");
        }

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            var consumable = new Consumable();
            consumable.Amount = 1;
            consumable.Price = 1m;

            var calculator = new CostCalculator_1_1_1(basicSettings);
            var consumableCost = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());

            Assert.AreEqual(1, consumableCost.Cost, "The consumable cost was incorrect (calculator was not provided with measurement units).");
        }
    }
}
