﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_1.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_1_Test
    {
        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1_Test"/> class.
        /// </summary>
        public CostCalculator_1_1_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Initialize the tests.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_Old);
            customTestAssembly.SetCalculationVariant("1.1");
        }

        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        ////
        //// Use TestCleanup to run code after each test has run
        //// [TestCleanup()]
        //// public void MyTestCleanup() { }

        #endregion

        #region Tests Using Reference Calculation Models

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };

            var assembly = customTestAssembly;
            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);

            decimal expectedCost = 229.33553532607492138401237911M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var assembly = customTestAssembly;

            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, imperialUnits);

            decimal expectedCost = 229.33553532607492138401237911M;
            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.1");

            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(24.549105485119282987439928895M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.1");

            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(92.90361854653614107848223906M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_1_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.1");

            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(220.09347593677324158333279311M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        #endregion Tests Using Reference Calculation Models

        #region Die Tests

        /// <summary>
        /// Calculates the die cost test with null die.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateDieCostTestWithNullDie()
        {
            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            calculator.CalculateDieCost(null, new DieCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the die cost test with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateDieCostTestWithNullCalculationParameters()
        {
            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            calculator.CalculateDieCost(new Die(), null);
        }

        #endregion Die Tests

        #region Machine Tests

        /// <summary>
        /// Calculates the machine cost test with null machine.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateMachineCostTestWithNullMachine()
        {
            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            calculator.CalculateMachineCost(null, new MachineCostCalculationParameters());
        }

        /// <summary>
        /// Calculates the machine cost test with null calculation parameters.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateMachineCostTestWithNullCalculationParameters()
        {
            CostCalculator_1_1 calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            calculator.CalculateMachineCost(new Machine(), null);
        }

        /// <summary>
        /// Calculates the machine investment test.
        /// </summary>
        [TestMethod]
        public void CalculateMachineInvestmentTest()
        {
            var machine = new Machine();
            machine.MachineInvestment = 12345.9987M;
            machine.SetupInvestment = 778M;

            var step = new PartProcessStep();
            step.Machines.Add(machine);
            var process = new Process();
            process.Steps.Add(step);

            var part = new Part()
            {
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = process
            };

            var calculator = new CostCalculator_1_1(basicSettings, metricUnits);
            var cost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            var expectedValue = machine.MachineInvestment + machine.SetupInvestment;
            var actualValue = cost.ProcessCost.StepCosts[0].InvestmentCost.MachineInvestments[0].Investment;
            Assert.AreEqual(expectedValue, actualValue, "The calculated machine investment was");
        }

        #endregion Machine Tests

        /// <summary>
        /// Calculates the raw material without part weight.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialWithoutPartWeight()
        {
            var material = new RawMaterial();
            material.Price = 1M;
            material.ParentWeight = null;
            material.Quantity = null;

            var calculator = new CostCalculator_1_1(basicSettings);
            var materialCost = calculator.CalculateRawMaterialCost(material, new RawMaterialCostCalculationParameters());
            Assert.AreEqual(0M, materialCost.TotalCost);
        }

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            var consumable = new Consumable();
            consumable.Amount = 2;
            consumable.Price = 2.5m;

            var calculator = new CostCalculator_1_1(basicSettings);
            var consumableCost = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());

            Assert.AreEqual(5, consumableCost.Cost, "The consumable cost was incorrect (calculator was not provided with measurement units).");
        }
    }
}
