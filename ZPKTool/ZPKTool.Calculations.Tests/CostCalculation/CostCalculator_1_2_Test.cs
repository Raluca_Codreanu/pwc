﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_2.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_2_Test
    {
        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_2_Test"/> class.
        /// </summary>
        public CostCalculator_1_2_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Initialize the tests.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_Old);
            customTestAssembly.SetCalculationVariant("1.2");
        }

        #endregion

        #region Tests Using Reference Calculation Models

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_2_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };

            var assembly = customTestAssembly;
            var calculator = new CostCalculator_1_2(basicSettings, metricUnits);

            decimal expectedCost = 232.58823344963968113489324017M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_2_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var assembly = customTestAssembly;

            var calculator = new CostCalculator_1_2(basicSettings, imperialUnits);

            decimal expectedCost = 232.58823344963968113489324015M;
            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_2_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.2");

            var calculator = new CostCalculator_1_2(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(31.089007810897110317829726668M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_2_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.2");

            var calculator = new CostCalculator_1_2(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(93.27041084129714603027318982M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_2_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.2");

            var calculator = new CostCalculator_1_2(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(222.36041105586829116686861669M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        #endregion Tests Using Reference Calculation Models

        #region Assembly Tests

        #endregion Assembly Tests

        #region Process Tests

        /// <summary>
        /// Calculates the process cost using process step level manufacturing overhead rate.
        /// </summary>
        [TestMethod]
        public void CalculateProcessCostUsingProcessStepLevelManufacturingOverheadRate()
        {
            var part = new Part();
            part.OverheadSettings = new OverheadSetting() { ManufacturingOverhead = 0.25m };
            part.CountrySettings = new CountrySetting();
            part.Process = new Process();

            var step1 = new PartProcessStep()
            {
                ProcessTime = 1000,
                CycleTime = 1000,
                PartsPerCycle = 1,
                SetupsPerBatch = 1,
                BatchSize = 850,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48
            };
            part.Process.Steps.Add(step1);

            step1.Machines.Add(new Machine()
            {
                ManufacturingYear = 2010,
                DepreciationPeriod = 10,
                DepreciationRate = 0.06m,
                FloorSize = 160,
                WorkspaceArea = 80,
                PowerConsumption = 400,
                FullLoadRate = 0.65m,
                OEE = 0.65m,
                Availability = 0.96m,
                MachineInvestment = 4200000,
                KValue = 0.03m,
                CalculateWithKValue = true
            });

            var step2 = new PartProcessStep()
            {
                ProcessTime = 1000,
                CycleTime = 1000,
                PartsPerCycle = 1,
                SetupsPerBatch = 1,
                BatchSize = 850,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48,
                ManufacturingOverhead = 0.35m
            };
            part.Process.Steps.Add(step2);

            step2.Machines.Add(new Machine()
            {
                ManufacturingYear = 2010,
                DepreciationPeriod = 10,
                DepreciationRate = 0.06m,
                FloorSize = 160,
                WorkspaceArea = 80,
                PowerConsumption = 400,
                FullLoadRate = 0.65m,
                OEE = 0.65m,
                Availability = 0.96m,
                MachineInvestment = 4200000,
                KValue = 0.03m,
                CalculateWithKValue = true
            });

            var costCalculator = new CostCalculator_1_2(basicSettings, metricUnits);
            var partCost = costCalculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(0.3M, partCost.OverheadCost.ManufacturingOverheadRateAverage, "The calculated average manufacturing overhead rate was incorrect.");
        }

        /// <summary>
        /// Calculates the cost of part with null process.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfPartWithNullProcess()
        {
            var part = new Part()
            {
                CalculationVariant = "1.3",
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting()
            };

            part.RawMaterials.Add(new RawMaterial() { Price = 1, Quantity = 1, ParentWeight = 1 });

            var calculator = new CostCalculator_1_2(basicSettings, metricUnits);
            var result = calculator.CalculatePartCost(part, new PartCostCalculationParameters());
            Assert.AreEqual(0, result.ProcessCost.TotalManufacturingCostSum);
            Assert.AreNotEqual(0, result.TotalCost);
        }

        #endregion Process Tests

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            var consumable = new Consumable();
            consumable.Amount = 1;
            consumable.Price = 1m;

            var calculator = new CostCalculator_1_2(basicSettings);
            var consumableCost = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());

            Assert.AreEqual(1, consumableCost.Cost, "The consumable cost was incorrect (calculator was not provided with measurement units).");
        }
    }
}
