﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Calculations.Tests.Resources;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Tests the ICostCalculator methods implemented by CostCalculator_1_3.
    /// </summary>
    [TestClass]
    public class CostCalculator_1_3_Test
    {
        #region Fields

        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly customTestAssembly;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> metricUnits;

        /// <summary>
        /// The list of imperial measurement units.
        /// </summary>
        private static List<MeasurementUnit> imperialUnits;

        /// <summary>
        /// The basic settings used for tests.
        /// </summary>
        private static BasicSetting basicSettings;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_3_Test"/> class.
        /// </summary>
        public CostCalculator_1_3_Test()
        {
        }

        #region Additional test attributes

        /// <summary>
        /// Initialize the tests.
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize]
        public static void TestsInitialize(TestContext testContext)
        {
            basicSettings = CostCalculationsTestHelper.GetReferenceBasicSettings();
            metricUnits = CostCalculationsTestHelper.GetMetricUnits();
            imperialUnits = CostCalculationsTestHelper.GetImperialUnits();

            // Load the test assembly
            customTestAssembly = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Custom_Assembly_v_1_3);
            customTestAssembly.SetCalculationVariant("1.3");
        }

        #endregion

        #region Tests Using Reference Calculation Models

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.        
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_3_TestUsingCustomAssemblyAndMetricUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m
            };

            var assembly = customTestAssembly;
            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            decimal expectedCost = 231.13335741327657045146005723M;

            // Test with project's LogisticCostRatio set to null; the LogisticCostRatio is taken from Basic Settings.
            costCalcParams.ProjectLogisticCostRatio = null;
            var result0 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result0.TotalCost, "Custom assembly cost calculation result was wrong (LogisticCostRatio = null).");

            // Test with project's LogisticCostRatio set to a value.
            costCalcParams.ProjectLogisticCostRatio = 0.04m;
            var result1 = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result1.TotalCost, "Custom assembly cost calculation result was wrong.");
        }

        /// <summary>
        /// Performs cost calculation tests using a large assembly that drives the calculation through all possible code paths.                
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_3_TestUsingCustomAssemblyAndImperialUnits()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var assembly = customTestAssembly;

            var calculator = new CostCalculator_1_3(basicSettings, imperialUnits);

            decimal expectedCost = 231.13335741327657045146005722M;
            var result = calculator.CalculateAssemblyCost(assembly, costCalcParams);
            Assert.AreEqual(expectedCost, result.TotalCost, "Custom assembly cost calculation result with imperial units was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "30-2 NEXTEER - Motor Iskra" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_3_TestUsingNexteerMotorIskraAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Nexteer_Motor_Iskra_Assy);
            referenceAssy.SetCalculationVariant("1.3");

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);

            Assert.AreEqual(27.718659277036115170117442000M, result.TotalCost, "Nexteer Motor Iskra cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Steering Column L320 Manual ROW" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_3_TestUsingSteeringColumnAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Steering_Column_Assy);
            referenceAssy.SetCalculationVariant("1.3");

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(93.10386567032668972308960322M, result.TotalCost, "Steering Column cost calculation result was wrong.");
        }

        /// <summary>
        /// A cost calculation test using the "Havac Main Unit - 50-4 GBP" reference assembly.
        /// </summary>
        [TestMethod]
        public void CostCalculator_1_3_TestUsingHavacMainUnitAssy()
        {
            var costCalcParams = new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            Assembly referenceAssy = CostCalculationsTestHelper.ImportModel<Assembly>(TestResources.Havac_Main_Unit_Assy);
            referenceAssy.SetCalculationVariant("1.3");

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(referenceAssy, costCalcParams);
            Assert.AreEqual(220.35587926696609287761674397M, result.TotalCost, "Havac Main Unit cost calculation result was wrong.");
        }

        #endregion Tests Using Reference Calculation Models

        #region Assembly Tests

        /// <summary>
        /// Calculates the cumulated weight and investment cost for assembly.
        /// </summary>
        [TestMethod]
        public void CalculateCumulatedWeightAndInvestmentCostForAssembly()
        {
            var assy = new Assembly()
            {
                CalculationVariant = "1.3",
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                Process = new Process()
            };

            var assyStep1 = new AssemblyProcessStep()
            {
                Commodities = { new Commodity() { Amount = 1, Weight = 1 } },
                Machines = { new Machine() { FundamentalSetupInvestment = 1000, AdditionalEquipmentInvestment = 500 } },
                Dies = { new Die { Investment = 1000, ReusableInvest = 1000, LifeTime = 24000 } }
            };
            assy.Process.Steps.Add(assyStep1);

            assy.Process.Steps.Add(new AssemblyProcessStep()
            {
                Commodities = { new Commodity() { Amount = 2, Weight = 0.5m } },
                Dies = { new Die { Investment = 1000, ReusableInvest = 1000, LifeTime = 12000 } }
            });

            // Make sure the weights of sub-assemblies and sub-parts are included, including the ones with estimated cost.
            var subPart1 = new Part()
            {
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                RawMaterials = { new RawMaterial() { ParentWeight = 1 } },
                Process = new Process()
                {
                    Steps =
                    {
                        new PartProcessStep()
                        {
                            Machines = { new Machine() { FundamentalSetupInvestment = 1000 } }
                        }
                    }
                }
            };
            assy.Parts.Add(subPart1);
            assyStep1.PartAmounts.Add(new ProcessStepPartAmount() { Amount = 1, Part = subPart1 });

            var subPart2 = new Part()
            {
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                CalculationAccuracy = PartCalculationAccuracy.Estimation,
                Weight = 0.5m
            };
            assy.Parts.Add(subPart2);
            assyStep1.PartAmounts.Add(new ProcessStepPartAmount() { Amount = 2, Part = subPart2 });

            // Parts not used in the process are ignored during this calculation.
            var subPart3 = new Part()
            {
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                CalculationAccuracy = PartCalculationAccuracy.Estimation,
                Weight = 0.5m
            };
            assy.Parts.Add(subPart3);

            var subAssy1 = new Assembly()
            {
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                CalculationAccuracy = PartCalculationAccuracy.Estimation,
                Weight = 0.5m
            };
            assy.Subassemblies.Add(subAssy1);
            assyStep1.AssemblyAmounts.Add(new ProcessStepAssemblyAmount() { Amount = 2, Assembly = subAssy1 });

            // Sub-assemblies not used in the process are ignored during this calculation.
            var subAssy2 = new Assembly()
            {
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                CalculationAccuracy = PartCalculationAccuracy.Estimation,
                Weight = 0.5m
            };
            assy.Subassemblies.Add(subAssy2);

            var subAssy3 = new Assembly()
            {
                Name = "aha3",
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                Process = new Process()
            };
            assy.Subassemblies.Add(subAssy3);
            assyStep1.AssemblyAmounts.Add(new ProcessStepAssemblyAmount() { Amount = 1, Assembly = subAssy3 });

            subAssy3.Process.Steps.Add(new AssemblyProcessStep()
            {
                Commodities = { new Commodity() { Amount = 1, Weight = 1 } },
                Machines = { new Machine() { MachineInvestment = 500 } },
            });

            var subAssy4 = new Assembly()
            {
                Name = "aha",
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                Process = new Process()
                {
                    Steps =
                    {
                        new AssemblyProcessStep()
                        {
                            Commodities = { new Commodity() { Amount = 2, Weight = 0.5m } },
                            Machines = { new Machine() { MachineInvestment = 500 } },
                        }
                    }
                }
            };
            subAssy3.Subassemblies.Add(subAssy4);
            subAssy3.Process.Steps.ElementAt(0).AssemblyAmounts.Add(new ProcessStepAssemblyAmount() { Amount = 2, Assembly = subAssy4 });

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var assyCost = calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());

            Assert.AreEqual(8, assyCost.Weight, "The cumulated weight calculated for an assembly was incorrect.");
            Assert.AreEqual(6500, assyCost.InvestmentCostCumulated, "The cumulated weight calculated for an assembly was incorrect.");
        }

        /// <summary>
        /// Calculates the cost of assembly with null process.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfAssemblyWithNullProcess()
        {
            var assy = new Assembly()
            {
                CalculationVariant = "1.3",
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting()
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var result = calculator.CalculateAssemblyCost(assy, new AssemblyCostCalculationParameters());
            Assert.AreEqual(0, result.ProcessCost.TotalManufacturingCostSum);
        }

        #endregion Assembly Tests

        #region Part Tests

        /// <summary>
        /// Calculates the cost of part with raw part.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfPartWithRawPart()
        {
            var part = new Part()
            {
                CalculationAccuracy = PartCalculationAccuracy.FineCalculation,
                CalculationVariant = "1.3",
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                SBMActive = true,
                AssetRate = 0.055m,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };

            var rawPart = new RawPart()
            {
                CalculationAccuracy = PartCalculationAccuracy.FineCalculation,
                CalculationVariant = "1.3",
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                SBMActive = true,
                AssetRate = 0.055m,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };
            part.RawPart = rawPart;
            rawPart.ParentOfRawPart.Add(part);

            rawPart.Process.Steps.Add(new PartProcessStep()
            {
                ScrapAmount = 0.01m
            });

            rawPart.RawMaterials.Add(new RawMaterial()
            {
                Price = 1.4m,
                ParentWeight = 0.5m,
                Sprue = 0,
            });

            var costCalcParams = new PartCostCalculationParameters()
            {
                ProjectDepreciationPeriod = 8,
                ProjectDepreciationRate = 0.06m,
                ProjectLogisticCostRatio = 0.04m
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, costCalcParams);

            // Check if the raw part cost was calculated
            Assert.IsTrue(
                partCost.PartsCost.PartCosts.Count == 1 && partCost.PartsCost.PartCosts[0].PartId == rawPart.Guid,
                "The cost of the raw part was not included in the cost of its parent part.");

            // Check the raw part cost
            var rawPartCost = partCost.PartsCost.PartCosts[0];
            Assert.IsTrue(rawPartCost.IsRawPart, "The 'IsRawPart' flag was not set for the raw part cost.");
            Assert.AreEqual(1, rawPartCost.AmountPerAssembly, "Only 1 raw part can exist per parent part.");
            Assert.AreEqual(0.8707038003838771593090211132M, rawPartCost.TargetCost, "The raw part's calculated cost was wrong.");

            // Check if the raw part cost is also reported as raw material cost.
            var rawPartCostAsMaterial = partCost.RawMaterialsCost.RawMaterialCosts.FirstOrDefault(c => c.RawMaterialId == rawPart.Guid);
            Assert.AreEqual(rawPartCostAsMaterial.PartWeight, rawPartCost.FullCalculationResult.Weight, "The raw part's weight was not included as material weight.");
            Assert.AreEqual(rawPartCostAsMaterial.BaseCost, rawPartCost.TargetCost, "The raw part's total cost was not included as material base cost.");
            Assert.AreEqual(rawPartCostAsMaterial.NetCost, rawPartCost.TargetCost, "The raw part's total cost was not included as material net cost.");
            Assert.AreEqual(rawPartCostAsMaterial.Overhead, rawPartCostAsMaterial.NetCost * part.OverheadSettings.MaterialOverhead, "Raw part as raw material: material overhead was not correct.");
            Assert.AreEqual(rawPartCostAsMaterial.TotalCost, rawPartCostAsMaterial.NetCost + rawPartCostAsMaterial.Overhead, "Raw part as raw material: material total cost was not correct.");
            Assert.IsTrue(rawPartCostAsMaterial.IsRawPart, "Raw part as raw material: the IsRawPart flag was not set.");

            // Check if the raw part cost was used in the process reject cost calculation
            var rawPartReject = partCost.ProcessCost.RejectOverview.RawPartsTotal;
            Assert.AreEqual(0.7073896353166986564299424184M, rawPartReject.MaterialCostPerPiece, "Raw part reject cost: material cost per piece was incorrectly calculated.");
            Assert.AreEqual(0.0104463437796771130104463437M, rawPartReject.RatioOfRejectCostInTotalCost, "Raw part reject cost: ratio of reject cost in total cost was incorrectly calculated.");
            Assert.AreEqual(0.0073896353166986564299424184M, rawPartReject.RejectMaterialCostPerPiece, "Raw part reject cost: reject material cost per piece was incorrectly calculated.");
            Assert.AreEqual(7.6999999999999999999999999728M, rawPartReject.TotalRejectMaterialCost, "Raw part reject cost: total reject material cost was incorrectly calculated.");
        }

        /// <summary>
        /// Calculates the cumulated weight and investment cost for part.
        /// </summary>
        [TestMethod]
        public void CalculateCumulatedWeightAndInvestmentCostForPart()
        {
            var part = new Part()
            {
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                Process = new Process()
            };

            // The weight is influenced by raw materials and commodities.
            var material = new RawMaterial()
            {
                Name = "Does not matter",
                ParentWeight = 0.5m,
                Sprue = 0.2m,
                Quantity = 0.6m,
                Loss = 0.02m,
                ScrapRefundRatio = 0.4m,
                RejectRatio = 0.01m,
                RecyclingRatio = 0.05m,
                StockKeeping = 30
            };
            part.RawMaterials.Add(material);

            var commodity = new Commodity()
            {
                Name = "Does not matter",
                Amount = 2,
                Weight = 0.25m
            };
            part.Commodities.Add(commodity);

            var rawPart = new RawPart()
            {
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CalculationVariant = "1.3",
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting(),
                Process = new Process()
            };
            part.RawPart = rawPart;

            var materialClone = new RawMaterial();
            material.CopyValuesTo(materialClone);
            rawPart.RawMaterials.Add(materialClone);
            var commodityClone = new Commodity();
            commodity.CopyValuesTo(commodityClone);
            rawPart.Commodities.Add(commodityClone);

            // The investment is influenced by machines and dies
            part.Process.Steps.Add(new PartProcessStep() { Machines = { new Machine() { MachineInvestment = 1000, SetupInvestment = 500 } } });
            part.Process.Steps.Add(new PartProcessStep()
            {
                Machines = { new Machine() { FundamentalSetupInvestment = 1000, AdditionalEquipmentInvestment = 500 } },
                Dies = 
                { 
                    new Die { Investment = 1000, ReusableInvest = 1000, LifeTime = 12000 },
                    new Die { Investment = 1000, ReusableInvest = 1000, LifeTime = 0 } // this will be ignored because its necessary amount per lifetime is zero
                }
            });

            rawPart.Process.Steps.Add(new PartProcessStep() { Machines = { new Machine() { MachineInvestment = 1000, SetupInvestment = 500 } } });
            rawPart.Process.Steps.Add(new PartProcessStep()
            {
                Machines = { new Machine() { FundamentalSetupInvestment = 1000, AdditionalEquipmentInvestment = 500 } },
                Dies = { new Die { Investment = 1000, ReusableInvest = 1000, LifeTime = 24000 } }
            });

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(2, partCost.Weight, "The cumulated weight calculated for a part was incorrect.");
            Assert.AreEqual(9000, partCost.InvestmentCostCumulated, "The cumulated weight calculated for a part was incorrect.");

            // Check the Raw Part
            Assert.AreEqual(1, partCost.PartsCost.PartCosts[0].FullCalculationResult.Weight, "The cumulated weight calculated for a raw part was incorrect.");
            Assert.AreEqual(4000, partCost.PartsCost.PartCosts[0].FullCalculationResult.InvestmentCostCumulated, "The cumulated weight calculated for a raw part was incorrect.");
        }

        /// <summary>
        /// Calculates the cost of part with null process.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfPartWithNullProcess()
        {
            var part = new Part()
            {
                CalculationVariant = "1.3",
                YearlyProductionQuantity = 12000,
                LifeTime = 2,
                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting()
            };

            part.RawMaterials.Add(new RawMaterial() { Price = 1, Quantity = 1, ParentWeight = 1 });

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var result = calculator.CalculatePartCost(part, new PartCostCalculationParameters());
            Assert.AreEqual(0, result.ProcessCost.TotalManufacturingCostSum);
            Assert.AreNotEqual(0, result.TotalCost);
        }

        #endregion Part Tests

        #region Process Tests

        /// <summary>
        /// Calculates the n tier transport cost per quantity with valid qty.
        /// </summary>
        [TestMethod]
        public void CalculateNTierTransportCostPerQuantityWithValidQty()
        {
            var part = new Part()
            {
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = new Process()
            };
            part.Process.Steps.Add(new PartProcessStep()
            {
                IsExternal = true,
                TransportCost = 100m,
                TransportCostType = ProcessTransportCostType.PerQty,
                TransportCostQty = 10000
            });

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(0.01M, partCost.ProcessCost.StepCosts[0].TransportCost, "The process step n-tier transport cost per Qty was incorrect.");
        }

        /// <summary>
        /// Calculates the n tier transport cost per quantity with zero qty.
        /// </summary>
        [TestMethod]
        public void CalculateNTierTransportCostPerQuantityWithZeroQty()
        {
            var part = new Part()
            {
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = new Process()
            };
            part.Process.Steps.Add(new PartProcessStep()
            {
                IsExternal = true,
                TransportCost = 100m,
                TransportCostType = ProcessTransportCostType.PerQty,
                TransportCostQty = 0
            });

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(0, partCost.ProcessCost.StepCosts[0].TransportCost, "The process step n-tier transport cost was not zero when the transport quantity was zero.");
        }

        /// <summary>
        /// Calculates the n tier transport cost per part.
        /// </summary>
        [TestMethod]
        public void CalculateNTierTransportCostPerPart()
        {
            var part = new Part()
            {
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = new Process()
            };
            var step = new PartProcessStep()
            {
                IsExternal = true,
                TransportCost = 1.2m,
                TransportCostType = (short)ProcessTransportCostType.PerPart,
                TransportCostQty = 0
            };
            part.Process.Steps.Add(step);

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(step.TransportCost, partCost.ProcessCost.StepCosts[0].TransportCost, "The process step n-tier transport cost per part was incorrect.");
        }

        /// <summary>
        /// Calculates the n tier transport cost when process step is not external.
        /// </summary>
        [TestMethod]
        public void CalculateNTierTransportCostWhenProcessStepIsNotExternal()
        {
            var part = new Part()
            {
                OverheadSettings = new OverheadSetting(),
                CountrySettings = new CountrySetting(),
                Process = new Process()
            };
            var step = new PartProcessStep()
            {
                IsExternal = false,
                TransportCost = 1.2m,
                TransportCostType = (short)ProcessTransportCostType.PerPart,
                TransportCostQty = 0
            };
            part.Process.Steps.Add(step);

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(0M, partCost.ProcessCost.StepCosts[0].TransportCost, "The process step n-tier transport cost was calculated for a step that was not external.");
        }

        #endregion Process Tests

        #region Raw Material Tests

        /// <summary>
        /// Tests the raw material WIP cost calculation.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialWIPCostUsingValidData()
        {
            var material = new RawMaterial()
            {
                Price = 2,
                ParentWeight = 1,
                Sprue = 0,
                StockKeeping = 60
            };

            var costCalcParams = new RawMaterialCostCalculationParameters()
            {
                AnnualProductionQty = 12500,
                BatchSize = 12,
                AssetRate = 0.055m
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            var materialCost = calculator.CalculateRawMaterialCost(material, costCalcParams);

            Assert.AreEqual(0.0183392M, materialCost.WIPCost, "The material WIP cost calculation result is incorrect.");
        }

        /// <summary>
        /// Tests the raw material WIP cost calculation.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialWIPCostUsingZeroParentBatchSizeAndYearlyProductionQty()
        {
            var material = new RawMaterial()
            {
                Price = 2,
                ParentWeight = 1,
                Sprue = 0,
                StockKeeping = 60
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var costCalcParams = new RawMaterialCostCalculationParameters()
            {
                AnnualProductionQty = 0,
                BatchSize = 0,
                AssetRate = 0.055m
            };

            var materialCost = calculator.CalculateRawMaterialCost(material, costCalcParams);

            Assert.AreEqual(0M, materialCost.WIPCost, "The material WIP cost calculation result is incorrect.");
        }

        /// <summary>
        /// Calculates the raw material cost without part and material weights.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialCostWithoutPartAndMaterialWeights()
        {
            var material = new RawMaterial()
            {
                Price = 2,
                ParentWeight = null,
                Sprue = null,
                Quantity = null
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var materialCost = calculator.CalculateRawMaterialCost(material, new RawMaterialCostCalculationParameters());

            Assert.AreEqual(0, materialCost.TotalCost, "The total material cost calculated was incorrect.");
        }

        /// <summary>
        /// Tests the inclusion of recycling ration in raw material cost calculation.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialCostUsingRecyclingRate()
        {
            var material = new RawMaterial()
            {
                Price = 1,
                ParentWeight = 1,
                Sprue = 0.5m,
                RecyclingRatio = 0.5m,

                ScrapRefundRatio = 0.4m,
                RejectRatio = 0.01m,
                Loss = 0.02m,
                StockKeeping = 90
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var materialCost = calculator.CalculateRawMaterialCost(material, new RawMaterialCostCalculationParameters());

            Assert.AreEqual(1.1918m, materialCost.NetCost, "The net material cost calculated was incorrect.");
        }

        /// <summary>
        /// Tests the CalculateRawMaterialCost method by providing null cost calculation parameters.
        /// In this case the overhead cost and WIP cost are not calculated.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialCostWithNullCalculationParameters()
        {
            var material = new RawMaterial()
            {
                Price = 1,
                ParentWeight = 1,
                Sprue = 0.5m,
                RecyclingRatio = 0.5m,
                ScrapRefundRatio = 0.4m,
                RejectRatio = 0.01m,
                Loss = 0.02m,
                StockKeeping = 90
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var materialCost = calculator.CalculateRawMaterialCost(material, null);

            Assert.AreEqual(1.1918m, materialCost.NetCost, "The net material cost calculated was incorrect.");
            Assert.AreEqual(0, materialCost.Overhead);
            Assert.AreEqual(0, materialCost.WIPCost);
        }

        #endregion Raw Material Tests

        #region Tests for CalculateYearlyProductionQuantity

        /// <summary>
        /// Calculates the yearly production quantity for a model with one hierarchical level (part or assembly without raw parts or sub-assemblies/sub-parts).
        /// </summary>
        [TestMethod]
        public void CalculateYearlyProductionQuantityForModelWithSingleLevelHierarchy()
        {
            var part = new Part()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                Process = new Process()
                {
                    Steps = { new PartProcessStep() { Index = 0, ScrapAmount = 0.02m }, new PartProcessStep() { Index = 1, ScrapAmount = 0.03m } }
                }
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var calculatedProdQty = calculator.CalculateYearlyProductionQuantity(part);

            Assert.AreEqual(12500, calculatedProdQty, "The calculated yearly production quantity for a single level hierarchical model was incorrect.");
        }

        /// <summary>
        /// Calculates the yearly production quantity for part with raw part.
        /// </summary>
        [TestMethod]
        public void CalculateYearlyProductionQuantityForPartWithRawPart()
        {
            var part = new Part()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                Process = new Process()
                {
                    Steps = { new PartProcessStep() { Index = 0, ScrapAmount = 0.02m }, new PartProcessStep() { Index = 1, ScrapAmount = 0.03m } }
                }
            };

            var rawPart = new RawPart()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                Process = new Process()
            };

            part.RawPart = rawPart;
            rawPart.ParentOfRawPart.Add(part);

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var calculatedProdQty = calculator.CalculateYearlyProductionQuantity(rawPart);

            Assert.AreEqual(13152M, calculatedProdQty, "The calculated yearly production quantity for a Part->RawPart hierarchy was incorrect.");
        }

        /// <summary>
        /// Calculates the yearly production quantity for an assembly hierarchy:
        /// RootAssy -> SubAssy1 -> SubAssy2 -> SubPart.
        /// </summary>
        [TestMethod]
        public void CalculateYearlyProductionQuantityForAssemblyHierarchy()
        {
            var assembly = new Assembly()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 10300,
                LifeTime = 7,
                Process = new Process()
                {
                    Steps = 
                    { 
                        new AssemblyProcessStep() { Index = 0, ScrapAmount = 0m },
                        new AssemblyProcessStep() { Index = 1, ScrapAmount = 0.03m },
                        new AssemblyProcessStep() { Index = 2, ScrapAmount = 0.04m }
                    }
                }
            };

            var subAssy1 = new Assembly()
            {
                BatchSizePerYear = 10,
                YearlyProductionQuantity = 16000,
                LifeTime = 5,
                Process = new Process()
                {
                    Steps = { new AssemblyProcessStep() { Index = 0, ScrapAmount = 0.1m }, new AssemblyProcessStep() { Index = 1, ScrapAmount = 0.015m } }
                }
            };

            var subAssy2 = new Assembly()
            {
                BatchSizePerYear = 16,
                YearlyProductionQuantity = 12000,
                LifeTime = 6,
                Process = new Process()
            };

            var subPart = new Part()
            {
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12000,
                LifeTime = 4,
                Process = new Process()
            };

            assembly.Subassemblies.Add(subAssy1);
            subAssy1.ParentAssembly = assembly;

            subAssy1.Subassemblies.Add(subAssy2);
            subAssy2.ParentAssembly = subAssy1;

            subAssy2.Parts.Add(subPart);
            subPart.Assembly = subAssy2;

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var calculatedProdQtyForPart = calculator.CalculateYearlyProductionQuantity(subPart);
            var calculatedProdQtyForSubAssy2 = calculator.CalculateYearlyProductionQuantity(subAssy2);
            var calculatedProdQtyForSubAssy1 = calculator.CalculateYearlyProductionQuantity(subAssy1);
            var calculatedProdQtyForRootAssy = calculator.CalculateYearlyProductionQuantity(assembly);

            Assert.AreEqual(12368M, calculatedProdQtyForPart);
            Assert.AreEqual(12360M, calculatedProdQtyForSubAssy2);
            Assert.AreEqual(11052M, calculatedProdQtyForSubAssy1);
            Assert.AreEqual(10300M, calculatedProdQtyForRootAssy);
        }

        /// <summary>
        /// Calculates the yearly production quantity using null assembly.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateYearlyProductionQuantityUsingNullAssembly()
        {
            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            calculator.CalculateYearlyProductionQuantity((Assembly)null);
        }

        /// <summary>
        /// Calculates the yearly production quantity using null part.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateYearlyProductionQuantityUsingNullPart()
        {
            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);
            calculator.CalculateYearlyProductionQuantity((Part)null);
        }

        #endregion Tests for CalculateYearlyProductionQuantity

        /// <summary>
        /// Calculates the external work overhead and margin test.
        /// </summary>
        [TestMethod]
        public void CalculateExternalWorkOverheadAndMarginTest()
        {
            var part = new Part()
            {
                CalculationVariant = "1.3",
                BatchSizePerYear = 12,
                YearlyProductionQuantity = 12500,
                LifeTime = 7,
                AssetRate = 0.055m,
                SBMActive = true,
                OverheadSettings = CostCalculationsTestHelper.GetReferenceOverheadSettings(),
                CountrySettings = CostCalculationsTestHelper.GetReferenceCountrySettings(),
                Process = new Process()
            };

            var step1 = new PartProcessStep()
            {
                Name = "test",
                ProcessTime = 1000,
                CycleTime = 1000,
                PartsPerCycle = 1,
                ScrapAmount = 0.01m,
                BatchSize = 1042,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48,
                IsExternal = true,
                Machines =
                {
                    new Machine()
                    {
                        ManufacturingYear = 2013,
                        DepreciationPeriod = 8,
                        DepreciationRate = 0.06m,
                        FloorSize = 20,
                        PowerConsumption = 10,
                        FullLoadRate = 1m,
                        OEE = 0.65m,
                        Availability = 0.85m,
                        MachineInvestment = 10000,
                        CalculateWithKValue = true,
                        KValue = 0.04m
                    }
                }
            };
            part.Process.Steps.Add(step1);

            // Steps that are not external should not affect the external margin and overhead
            var step2 = new PartProcessStep();
            part.Process.Steps.Add(step2);
            step1.CopyValuesTo(step2);
            step2.IsExternal = false;

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var partCost = calculator.CalculatePartCost(part, new PartCostCalculationParameters());

            Assert.AreEqual(0.0233324335697919062414188671M, partCost.OverheadCost.ExternalWorkOverhead, "The calculated external work overhead was incorrect.");
            Assert.AreEqual(0.0181992981844376868683067164M, partCost.OverheadCost.ExternalWorkMargin, "The calculated external work margin was incorrect.");
        }

        /// <summary>
        /// Calculates the company surcharge overhead test.
        /// </summary>
        [TestMethod]
        public void CalculateCompanySurchargeOverheadTest()
        {
            // The company surcharge OH will applied to the production cost, which in this case is the estimated cost.
            var assembly = new Assembly()
            {
                CalculationAccuracy = PartCalculationAccuracy.RoughCalculation,
                EstimatedCost = 2,

                CountrySettings = new CountrySetting(),
                OverheadSettings = new OverheadSetting() { CompanySurchargeOverhead = 0.1m }
            };

            var calculator = new CostCalculator_1_3(basicSettings, metricUnits);

            var assyCost = calculator.CalculateAssemblyCost(assembly, new AssemblyCostCalculationParameters());

            Assert.AreEqual(0.2m, assyCost.OverheadCost.CompanySurchargeOverhead, "The calculated company surcharge overhead was incorrect.");
        }

        /// <summary>
        /// Initializes the calculator without measurement units.
        /// </summary>
        [TestMethod]
        public void InitializeCalculatorWithoutMeasurementUnits()
        {
            var consumable = new Consumable();
            consumable.Amount = 1;
            consumable.Price = 1m;

            var calculator = new CostCalculator_1_3(basicSettings);
            var consumableCost = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());

            Assert.AreEqual(1, consumableCost.Cost, "The consumable cost was incorrect (calculator was not provided with measurement units).");
        }
    }
}
