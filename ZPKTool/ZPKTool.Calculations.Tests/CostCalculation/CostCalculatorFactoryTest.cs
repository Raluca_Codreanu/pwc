﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;

namespace ZPKTool.Calculations.Tests.CostCalculation
{
    /// <summary>
    /// Unit Tests for CostCalculatorFactory.
    /// </summary>
    [TestClass]
    public class CostCalculatorFactoryTest
    {
        /// <summary>
        /// Calculations versions test.
        /// </summary>
        [TestMethod]
        public void CalculationVersionsTest()
        {
            var versions = CostCalculatorFactory.CalculationVersions;
            Assert.IsNotNull(versions, "The versions collection was null.");
            Assert.IsTrue(versions.Count > 0, "The versions collection was empty.");
        }

        /// <summary>
        /// Gets the calculator test.
        /// </summary>
        [TestMethod]
        public void GetCalculatorTest()
        {
            var basicSettings = new BasicSetting();
            CostCalculatorFactory.SetBasicSettings(basicSettings);

            // Test with an existing version
            var calculator = CostCalculatorFactory.GetCalculator("1.1");
            Assert.AreEqual("1.1", calculator.Version);

            // Test with null or empty version
            calculator = CostCalculatorFactory.GetCalculator(null);
            Assert.AreEqual(CostCalculatorFactory.OldestVersion, calculator.Version);

            calculator = CostCalculatorFactory.GetCalculator(string.Empty);
            Assert.AreEqual(CostCalculatorFactory.OldestVersion, calculator.Version);

            // Test with an inexistent version
            calculator = CostCalculatorFactory.GetCalculator("cj.HHaw4");
            Assert.AreEqual(CostCalculatorFactory.LatestVersion, calculator.Version);
        }

        /// <summary>
        /// Tests the GetCalculator method in the following scenario: an inexistent version is provided as parameter and the
        /// internal mapping between a ICostCalculator implementation and the latest version is missing.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetCalculator_InvalidMappingForLatestVersion()
        {
            // Get the mapping between ICostCalculator implementations and versions (it's stored in a private list)
            var factoryType = typeof(CostCalculatorFactory);
            var implMapField = factoryType.GetField("calculatorImplementationsMap", BindingFlags.NonPublic | BindingFlags.Static);
            var implMap = (List<Tuple<string, Type>>)implMapField.GetValue(null);

            // Remove the mapping for the latest version. Add it back at the end because due to the static nature of the factory the mapping will be permanently removed.
            var mappingIndex = implMap.FindIndex(it => it.Item1 == CostCalculatorFactory.LatestVersion);
            var mapping = implMap[mappingIndex];
            try
            {
                implMap.RemoveAt(mappingIndex);

                // Now try to get a calculator using an inexistent version => exception
                CostCalculatorFactory.GetCalculator("a.c22.jsdgf", new BasicSetting(), new List<MeasurementUnit>());
            }
            finally
            {
                implMap.Insert(mappingIndex, mapping);
            }
        }

        /// <summary>
        /// Tests the GetCalculator method in the following scenario: a null or empty version is provided as parameter and the
        /// internal mapping between a ICostCalculator implementation and the oldest version is missing.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetCalculator_InvalidMappingForOldestVersion()
        {
            // Get the mapping between ICostCalculator implementations and versions (it's stored in a private list)
            var factoryType = typeof(CostCalculatorFactory);
            var implMapField = factoryType.GetField("calculatorImplementationsMap", BindingFlags.NonPublic | BindingFlags.Static);
            var implMap = (List<Tuple<string, Type>>)implMapField.GetValue(null);

            // Remove the mapping for the latest version. Add it back at the end because due to the static nature of the factory the mapping will be permanently removed.
            var mappingIndex = implMap.FindIndex(it => it.Item1 == CostCalculatorFactory.OldestVersion);
            var mapping = implMap[mappingIndex];
            try
            {
                implMap.RemoveAt(mappingIndex);

                // Now try to get a calculator using a null or empty value for version => exception
                CostCalculatorFactory.GetCalculator(null, new BasicSetting(), new List<MeasurementUnit>());
                CostCalculatorFactory.GetCalculator(string.Empty, new BasicSetting(), new List<MeasurementUnit>());
            }
            finally
            {
                implMap.Insert(mappingIndex, mapping);
            }
        }

        /// <summary>
        /// Gets the calculator with null basic settings.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetCalculatorWithNullBasicSettings()
        {
            CostCalculatorFactory.GetCalculator("1.0", null, new List<MeasurementUnit>());
        }

        /// <summary>
        /// Gets the calculator with null measurement units.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetCalculatorWithNullMeasurementUnits()
        {
            CostCalculatorFactory.GetCalculator("1.0", new BasicSetting(), null);
        }

        /// <summary>
        /// Determines whether the version is valid test.
        /// </summary>
        [TestMethod]
        public void IsValidVersionTest()
        {
            var result = CostCalculatorFactory.IsValidVersion(null);
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsValidVersion(string.Empty);
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsValidVersion("8732864gdf");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsValidVersion("1.1.1");
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Determines whether is latest version test.
        /// </summary>
        [TestMethod]
        public void IsLatestVersionTest()
        {
            var result = CostCalculatorFactory.IsLatestVersion(null);
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsLatestVersion("324");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsLatestVersion("1.1");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsLatestVersion(CostCalculatorFactory.LatestVersion);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Determines whether is newer test.
        /// </summary>
        [TestMethod]
        public void IsNewerTest()
        {
            var result = CostCalculatorFactory.IsNewer(null, null);
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsNewer("asd6t55", "dd8");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsNewer("1.0", "1.1");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsNewer("1.1.1", "1.0");
            Assert.IsTrue(result);

            result = CostCalculatorFactory.IsNewer(CostCalculatorFactory.LatestVersion, "1.1");
            Assert.IsTrue(result);

            result = CostCalculatorFactory.IsNewer("1.1", "1.1");
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Determines whether is older test.
        /// </summary>
        [TestMethod]
        public void IsOlderTest()
        {
            var result = CostCalculatorFactory.IsOlder(null, null);
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsOlder("asd6t55", "dd8");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsOlder("1.0", "1.0");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsOlder("1.1", "1.0");
            Assert.IsFalse(result);

            result = CostCalculatorFactory.IsOlder("1.1", CostCalculatorFactory.LatestVersion);
            Assert.IsTrue(result);

            result = CostCalculatorFactory.IsOlder(CostCalculatorFactory.OldestVersion, CostCalculatorFactory.LatestVersion);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Sets the measurement units.
        /// </summary>
        [TestMethod]
        public void SetMeasurementUnits()
        {
            // This test succeeds if no exception is thrown.
            CostCalculatorFactory.SetMeasurementUnits(new List<MeasurementUnit>());
        }

        /// <summary>
        /// Sets the measurement units with null value.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetMeasurementUnitsWithNullValue()
        {
            CostCalculatorFactory.SetMeasurementUnits(null);
        }

        /// <summary>
        /// Sets the basic settings with null value.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetBasicSettingsWithNullValue()
        {
            CostCalculatorFactory.SetBasicSettings(null);
        }

        /// <summary>
        /// Tests the <see cref="CostCalculatorFactory.IsSame "/> method using combinations of two valid calculation versions.
        /// </summary>
        [TestMethod]
        public void AreVersionsTheSameUsingValidValues()
        {
            bool test1 = CostCalculatorFactory.IsSame("1.0", "1.1");
            Assert.IsFalse(test1);

            bool test2 = CostCalculatorFactory.IsSame("1.3", "1.1");
            Assert.IsFalse(test2);

            bool test3 = CostCalculatorFactory.IsSame("1.1.1", "1.1");
            Assert.IsFalse(test3);

            bool test4 = CostCalculatorFactory.IsSame("1.0", "1.0");
            Assert.IsTrue(test4);

            bool test5 = CostCalculatorFactory.IsSame("1.2", "1.2");
            Assert.IsTrue(test5);
        }

        /// <summary>
        /// Tests the <see cref="CostCalculatorFactory.IsSame "/> method using combinations of invalid calculation versions.
        /// </summary>
        [TestMethod]
        public void AreVersionsTheSameUsingInvalidValues()
        {
            bool test1 = CostCalculatorFactory.IsSame("asd", "1.1");
            Assert.IsFalse(test1);

            bool test2 = CostCalculatorFactory.IsSame("1.1", "asd");
            Assert.IsFalse(test2);

            bool test3 = CostCalculatorFactory.IsSame("99fk.", "asd");
            Assert.IsFalse(test3);
        }
    }
}
