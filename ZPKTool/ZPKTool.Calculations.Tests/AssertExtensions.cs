﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Calculations.Tests
{
    /// <summary>
    /// Extensions for Assert, used in calculation unit tests.
    /// </summary>
    public sealed class AssertExtensions
    {
        /// <summary>
        /// Checks if the value properties of the specified objects are equal.
        /// Value properties are considered to be the properties whose type is value type (numbers, enums, structs) and strings.
        /// <para />
        /// Optionally reference properties can be checked for equality too.
        /// </summary>
        /// <typeparam name="T">The type of the objects to be checked.</typeparam>
        /// <param name="expected">The objects whose values are expected to be correct (the reference object for comparison purpose).</param>
        /// <param name="actual">The objects whose values are compared against the expected object's values.</param>
        /// <param name="referencesShouldNotBeEqual">
        /// If set to true when a reference property has the same reference vale for both objects it generates an error; otherwise it continues with the check.
        /// </param>
        /// <param name="deepCheck">
        /// If set to true this check is performed for all sub-objects of the objects specified for compare.
        /// </param>
        /// <exception cref="System.InvalidOperationException">Not all properties were examined by the algorithm. Please review and fix this algorithm.</exception>
        public static void AreEqualValueProperties<T>(T expected, T actual, bool referencesShouldNotBeEqual = true, bool deepCheck = false)
            where T : class
        {
            if (expected == null && actual == null)
            {
                Assert.Fail("The compared objects were null.");
            }
            else if (expected == null)
            {
                Assert.Fail("The expected object was null.");
            }
            else if (actual == null)
            {
                Assert.Fail("The actual object was null.");
            }

            var type = expected.GetType();
            var stringType = typeof(string);
            var allProperties = type.GetProperties();

            var valueProperties = allProperties.Where(p => p.PropertyType.IsValueType || p.PropertyType == stringType);

            foreach (var property in valueProperties)
            {
                var expectedVal = property.GetValue(expected, null);
                var actualVal = property.GetValue(actual, null);

                string message = string.Format(
                    "The '{0}.{1}' value property did not have equal values for the compared objects. Expected: {2}, Actual: {3}",
                    type.Name,
                    property.Name,
                    expectedVal,
                    actualVal);
                Assert.AreEqual(expectedVal, actualVal, message);
            }

            var referenceProperties = allProperties.Where(p => (p.PropertyType.IsClass || p.PropertyType.IsInterface) && p.PropertyType != stringType);

            // Make sure all properties are examined. This should be removed when the method is considered fully tested.
            var examinedProperties = new List<System.Reflection.PropertyInfo>(valueProperties);
            examinedProperties.AddRange(referenceProperties);
            if (examinedProperties.Count != allProperties.Length)
            {
                throw new InvalidOperationException("Not all properties were examined by the algorithm. Please review and fix this algorithm.");
            }

            List<Tuple<object, object>> subObjects = new List<Tuple<object, object>>();
            foreach (var property in referenceProperties)
            {
                var expectedVal = property.GetValue(expected, null);
                var actualVal = property.GetValue(actual, null);

                if (referencesShouldNotBeEqual && expectedVal != null && actualVal != null && expectedVal.Equals(actualVal))
                {
                    string message = string.Format("The '{0}.{1}' reference property had equal values for the compared objects.", type.Name, property.Name);
                    Assert.Fail(message);
                }

                if ((expectedVal != null && actualVal == null) || (expectedVal == null && actualVal != null))
                {
                    string message = string.Format("The '{0}.{1}' reference property had null value for one of the compared objects and not null for the other.", type.Name, property.Name);
                    Assert.Fail(message);
                }
                else if (expectedVal != null && actualVal != null && deepCheck)
                {
                    var enumerableExpectedVal = expectedVal as IEnumerable;
                    if (enumerableExpectedVal != null)
                    {
                        // The reference is enumerable so we compare the objects in it.
                        var expectedValEnumerator = enumerableExpectedVal.GetEnumerator();
                        var actualValEnumerator = ((IEnumerable)actualVal).GetEnumerator();

                        bool elementsExist = true;
                        do
                        {
                            bool enum1Moved = expectedValEnumerator.MoveNext();
                            bool enum2Moved = actualValEnumerator.MoveNext();
                            elementsExist = enum1Moved && enum2Moved;
                            if (enum1Moved != enum2Moved)
                            {
                                // The enumerable values do not have the same number of elements.
                                string message = string.Format("The '{0}.{1}' enumerable property had different number of elements in the compared objects.", type.Name, property.Name);
                                Assert.Fail(message);
                            }

                            if (elementsExist)
                            {
                                subObjects.Add(new Tuple<object, object>(expectedValEnumerator.Current, actualValEnumerator.Current));
                            }
                        }
                        while (elementsExist);
                    }
                    else
                    {
                        // The reference property
                        subObjects.Add(new Tuple<object, object>(expectedVal, actualVal));
                    }
                }

                // Otherwise both are null => nothing to do
            }

            if (deepCheck)
            {
                foreach (var subObject in subObjects)
                {
                    AreEqualValueProperties(subObject.Item1, subObject.Item2, referencesShouldNotBeEqual, deepCheck);
                }
            }
        }
    }
}
