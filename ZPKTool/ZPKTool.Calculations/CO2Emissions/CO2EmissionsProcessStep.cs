﻿namespace ZPKTool.Calculations.CO2Emissions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains the CO2 emissions data for a process step.
    /// </summary>
    public class CO2EmissionsProcessStep : CO2Emissions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CO2EmissionsProcessStep" /> class.
        /// </summary>
        public CO2EmissionsProcessStep()
        {
            this.MachineEmissions = new Collection<CO2EmissionsMachine>();
        }

        /// <summary>
        /// Gets or sets the id of the process step.
        /// </summary>        
        public Guid StepId { get; set; }

        /// <summary>
        /// Gets the CO2 emissions data for all machines in the process step.
        /// </summary>        
        public Collection<CO2EmissionsMachine> MachineEmissions { get; private set; }
    }
}
