﻿namespace ZPKTool.Calculations.CO2Emissions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// Calculates the carbon dioxide emissions for a manufacturing process.
    /// </summary>
    public class CO2EmissionsCalculator
    {
        /// <summary>
        /// The number of CO2 grams emitted to produce 1 kilowatt hour of electricity using fossil energy sources.
        /// </summary>
        private const decimal FossilSourceEmissionsPerKWh = 550m;

        /// <summary>
        /// The number of CO2 grams emitted to produce 1 kilowatt hour of electricity using renewable energy sources.
        /// </summary>
        private const decimal RenewableSourceEmissionsPerKWh = 25m;

        /// <summary>
        /// Initializes a new instance of the <see cref="CO2EmissionsCalculator" /> class.
        /// </summary>
        public CO2EmissionsCalculator()
        {
        }

        /// <summary>
        /// Calculates the CO2 emissions for the specified process.
        /// </summary>
        /// <param name="process">The process data.</param>
        /// <returns>The list of emissions for each process step.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="process"/> was null</exception>
        public Collection<CO2EmissionsProcessStep> CalculateEmissions(Process process)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process", "The process can not be null.");
            }

            var processEmissions = new Collection<CO2EmissionsProcessStep>();

            foreach (var processStep in process.Steps.OrderBy(s => s.Index))
            {
                var stepEmisions = new CO2EmissionsProcessStep();
                stepEmisions.StepId = processStep.Guid;

                // Calculate the emissions for each machine and add the result in the step emissions result object.
                foreach (var machine in processStep.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index))
                {
                    decimal powerConsumption = machine.PowerConsumption.GetValueOrDefault();
                    decimal fossilSourcesRatio = machine.FossilEnergyConsumptionRatio.GetValueOrDefault(1m);
                    decimal renewableSourcesRatio = 1m - fossilSourcesRatio;
                    decimal processTime = processStep.ProcessTime.GetValueOrDefault();
                    decimal partsPerCycle = processStep.PartsPerCycle.GetValueOrDefault();

                    decimal fossilEmissionsPerHour = fossilSourcesRatio * powerConsumption * FossilSourceEmissionsPerKWh;
                    decimal fossilEmissionsPerPart = 0m;
                    if (partsPerCycle != 0m)
                    {
                        fossilEmissionsPerPart = fossilEmissionsPerHour / 3600m * processTime / partsPerCycle;
                    }

                    decimal renewableEmissionsPerHour = renewableSourcesRatio * powerConsumption * RenewableSourceEmissionsPerKWh;
                    decimal renewableEmissionsPerPart = 0m;
                    if (partsPerCycle != 0m)
                    {
                        renewableEmissionsPerPart = renewableEmissionsPerHour / 3600m * processTime / partsPerCycle;
                    }

                    var machineEmissions = new CO2EmissionsMachine();
                    machineEmissions.MachineId = machine.Guid;

                    // Multiply all emissions by the amount of the machine used in the step.
                    machineEmissions.FossilSourcesEmissionsPerHour = fossilEmissionsPerHour * machine.Amount;
                    machineEmissions.FossilSourcesEmissionsPerPart = fossilEmissionsPerPart * machine.Amount;
                    machineEmissions.RenewableSourcesEmissionsPerHour = renewableEmissionsPerHour * machine.Amount;
                    machineEmissions.RenewableSourcesEmissionsPerPart = renewableEmissionsPerPart * machine.Amount;

                    stepEmisions.MachineEmissions.Add(machineEmissions);
                }

                stepEmisions.FossilSourcesEmissionsPerHour = stepEmisions.MachineEmissions.Sum(em => em.FossilSourcesEmissionsPerHour);
                stepEmisions.FossilSourcesEmissionsPerPart = stepEmisions.MachineEmissions.Sum(em => em.FossilSourcesEmissionsPerPart);
                stepEmisions.RenewableSourcesEmissionsPerHour = stepEmisions.MachineEmissions.Sum(em => em.RenewableSourcesEmissionsPerHour);
                stepEmisions.RenewableSourcesEmissionsPerPart = stepEmisions.MachineEmissions.Sum(em => em.RenewableSourcesEmissionsPerPart);

                processEmissions.Add(stepEmisions);
            }

            return processEmissions;
        }
    }
}
