﻿namespace ZPKTool.Calculations.CO2Emissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains the common CO2 emissions data for all entities.
    /// </summary>
    public class CO2Emissions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CO2Emissions" /> class.
        /// </summary>
        public CO2Emissions()
        {
        }

        /// <summary>
        /// Gets or sets the emissions from fossil sources per hour (in grams).
        /// </summary>        
        public decimal FossilSourcesEmissionsPerHour { get; set; }

        /// <summary>
        /// Gets or sets the emissions from fossil sources per part (in grams).
        /// </summary>        
        public decimal FossilSourcesEmissionsPerPart { get; set; }

        /// <summary>
        /// Gets or sets the emissions from renewable sources per hour (in grams).
        /// </summary>        
        public decimal RenewableSourcesEmissionsPerHour { get; set; }

        /// <summary>
        /// Gets or sets the emissions from renewable sources per part (in grams).
        /// </summary>        
        public decimal RenewableSourcesEmissionsPerPart { get; set; }

        /// <summary>
        /// Gets the total emissions per hour (in grams).
        /// </summary>        
        public decimal TotalEmissionsPerHour
        {
            get { return this.FossilSourcesEmissionsPerHour + this.RenewableSourcesEmissionsPerHour; }
        }

        /// <summary>
        /// Gets the total emissions per part (in grams).
        /// </summary>        
        public decimal TotalEmissionsPerPart
        {
            get { return this.FossilSourcesEmissionsPerPart + this.RenewableSourcesEmissionsPerPart; }
        }
    }
}
