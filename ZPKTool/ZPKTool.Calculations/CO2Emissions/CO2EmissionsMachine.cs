﻿namespace ZPKTool.Calculations.CO2Emissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains the CO2 emissions data for a Machine.
    /// </summary>
    public class CO2EmissionsMachine : CO2Emissions
    {
        /// <summary>
        /// Gets or sets the id of the machine.
        /// </summary>        
        public Guid MachineId { get; set; }
    }  
}
