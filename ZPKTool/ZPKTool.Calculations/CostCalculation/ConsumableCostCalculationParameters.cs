﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The input parameters for the cost calculation of a consumable.
    /// </summary>
    public class ConsumableCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the percentage used to calculate the consumable's overhead cost.
        /// </summary>
        public decimal ConsumableOverheadRate { get; set; }
    }
}
