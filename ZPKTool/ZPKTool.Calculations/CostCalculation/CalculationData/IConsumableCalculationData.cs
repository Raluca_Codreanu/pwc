﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Consumable.
    /// </summary>
    public interface IConsumableCalculationData
    {
        /// <summary>
        /// Gets the consumable's id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the consumable's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the consumable's description.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the name of the consumable's manufacturer.
        /// </summary>
        string ManufacturerName { get; }

        /// <summary>
        /// Gets the amount of consumable units used.
        /// </summary>
        decimal Amount { get; }

        /// <summary>
        /// Gets the unit in which the consumable amount is expressed.
        /// </summary>
        IMeasurementUnitData AmountUnit { get; }

        /// <summary>
        /// Gets the base unit of the scale to which the amount unit belongs.
        /// </summary>
        IMeasurementUnitData AmountUnitBase { get; }

        /// <summary>
        /// Gets the consumable's price per one <see cref="AmountUnitBase"/> unit.
        /// </summary>
        decimal Price { get; }

        #region External data

        /// <summary>
        /// Gets or sets the percentage used to calculate the consumable's overhead cost.
        /// </summary>
        decimal ConsumableOverheadRate { get; set; }

        #endregion External data
    }
}
