﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the common data that is necessary to calculate the cost of a Process Step, for both assembling and manufacturing processes.
    /// </summary>
    public interface IProcessStepCalculationData
    {
        /// <summary>
        /// Gets the step's id.
        /// </summary>        
        Guid Id { get; }

        /// <summary>
        /// Gets the step's name.
        /// </summary>        
        string Name { get; }

        /// <summary>
        /// Gets a value indicating whether the step is external.
        /// </summary>
        bool IsExternal { get; }

        /// <summary>
        /// Gets the step's index, which indicates its position in the process.
        /// </summary>        
        int Index { get; }

        /// <summary>
        /// Gets the step's calculation accuracy.
        /// <para />
        /// The default value should be <see cref="ProcessCalculationAccuracy.FineCalculation"/>.
        /// </summary>   
        ProcessCalculationAccuracy CalculationAccuracy { get; }

        /// <summary>
        /// Gets the step's estimated cost.
        /// </summary>
        decimal EstimatedCost { get; }

        /// <summary>
        /// Gets the step's process time.
        /// </summary>        
        decimal ProcessTime { get; }

        /// <summary>
        /// Gets the step's cycle time.
        /// </summary>
        decimal CycleTime { get; }

        /// <summary>
        /// Gets the step's parts per cycle value.
        /// </summary>
        int PartsPerCycle { get; }

        /// <summary>
        /// Gets the step's batch size.
        /// </summary>
        int BatchSize { get; }

        /// <summary>
        /// Gets the number of set up operations necessary for a producing a batch of parts.
        /// </summary>
        int SetupsPerBatch { get; }

        /// <summary>
        /// Gets the step's setup time.
        /// </summary>
        decimal SetupTime { get; }

        /// <summary>
        /// Gets the step's max down time.
        /// </summary>
        decimal MaxDownTime { get; }

        /// <summary>
        /// Gets the number of hours per shift.
        /// </summary>
        decimal HoursPerShift { get; }

        /// <summary>
        /// Gets the number of shifts per week.
        /// </summary>
        decimal ShiftsPerWeek { get; }

        /// <summary>
        /// Gets the number of production days per week.
        /// </summary>
        decimal ProductionDaysPerWeek { get; }

        /// <summary>
        /// Gets the number of production weeks per year.
        /// </summary>
        decimal ProductionWeeksPerYear { get; }

        /// <summary>
        /// Gets a value indicating whether extra shifts are allowed (or defined).
        /// </summary>
        bool ExceedShiftCost { get; }

        /// <summary>
        /// Gets the percentage by which the cost of an extra shift is increased compared to a normal shift.
        /// </summary>
        decimal ShiftCostExceedRatio { get; }

        /// <summary>
        /// Gets the number of extra shifts.
        /// </summary>
        decimal ExtraShiftsNumber { get; }

        /// <summary>
        /// Gets the number of unskilled laborers necessary for the setup operation(s).
        /// </summary>
        decimal SetupUnskilledLabour { get; }

        /// <summary>
        /// Gets the number of skilled laborers necessary for the setup operation(s).
        /// </summary>
        decimal SetupSkilledLabour { get; }

        /// <summary>
        /// Gets the number of foremen necessary for the setup operation(s).
        /// </summary>
        decimal SetupForeman { get; }

        /// <summary>
        /// Gets the number of technicians necessary for the setup operation(s).
        /// </summary>
        decimal SetupTechnicians { get; }

        /// <summary>
        /// Gets the number of engineers necessary for the setup operation(s).
        /// </summary>
        decimal SetupEngineers { get; }

        /// <summary>
        /// Gets the number of unskilled laborers necessary during production.
        /// </summary>
        decimal ProductionUnskilledLabour { get; }

        /// <summary>
        /// Gets the number of skilled laborers necessary during production.
        /// </summary>
        decimal ProductionSkilledLabour { get; }

        /// <summary>
        /// Gets the number of foremen necessary during production.
        /// </summary>
        decimal ProductionForeman { get; }

        /// <summary>
        /// Gets the number of technicians necessary during production.
        /// </summary>
        decimal ProductionTechnicians { get; }

        /// <summary>
        /// Gets the number of engineers necessary during production.
        /// </summary>
        decimal ProductionEngineers { get; }

        /// <summary>
        /// Gets the reject ratio for the step.
        /// </summary>
        decimal RejectRatio { get; }

        /// <summary>
        /// Gets the manufacturing overhead rate defined at step level (which override the manufacturing oh rate from part level).
        /// </summary>
        decimal? ManufacturingOverheadRate { get; }

        /// <summary>
        /// Gets the mode of allocating the the transport cost for the step.
        /// </summary>
        ProcessTransportCostType TransportCostType { get; }

        /// <summary>
        /// Gets the step's transport cost.
        /// </summary>
        decimal TransportCost { get; }

        /// <summary>
        /// Gets the quantity for which the transport cost is expressed, when the <see cref="TransportCostType"/> is <see cref="ProcessTransportCostType.PerQty"/>.
        /// </summary>
        decimal TransportCostQty { get; }

        /// <summary>
        /// Gets the data for the step's consumables.
        /// </summary>
        IEnumerable<IConsumableCalculationData> Consumables { get; }

        /// <summary>
        /// Gets the die data for the step's dies.
        /// </summary>
        IEnumerable<IDieCalculationData> Dies { get; }

        /// <summary>
        /// Gets the machine data for the step's machines.
        /// </summary>
        IEnumerable<IMachineCalculationData> Machines { get; }
    }
}
