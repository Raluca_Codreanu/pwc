﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Raw Part.
    /// </summary>
    public interface IRawPartCalculationData : IPartCalculationData
    {
    }
}
