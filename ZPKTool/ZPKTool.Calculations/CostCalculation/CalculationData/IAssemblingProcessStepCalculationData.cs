﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of an Assembling Process Step (Assembly's Process Step).
    /// </summary>
    public interface IAssemblingProcessStepCalculationData : IProcessStepCalculationData
    {        
        /// <summary>
        /// Gets the data regarding the amounts of sub-assemblies (of the parent assembly) used in this step.
        /// </summary>        
        IEnumerable<IProcessStepPartAmountCalculationData> AssemblyAmounts { get; }

        /// <summary>
        /// Gets the data regarding the amounts of sub-parts (of the parent assembly) used in this step.
        /// </summary>
        IEnumerable<IProcessStepPartAmountCalculationData> PartAmounts { get; }

        /// <summary>
        /// Gets the data for the step's commodities.
        /// </summary>
        IEnumerable<ICommodityCalculationData> Commodities { get; }        
    }
}
