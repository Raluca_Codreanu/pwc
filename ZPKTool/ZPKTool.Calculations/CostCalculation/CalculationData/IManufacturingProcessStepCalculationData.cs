﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Manufacturing Process Step (Part's Process Step).
    /// </summary>
    public interface IManufacturingProcessStepCalculationData : IProcessStepCalculationData
    {
    }
}
