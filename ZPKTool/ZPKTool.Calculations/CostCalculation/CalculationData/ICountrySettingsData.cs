﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the country settings data needed for cost calculations.
    /// </summary>
    public interface ICountrySettingsData
    {
        /// <summary>
        /// Gets the interest rate.
        /// </summary>
        decimal InterestRate { get; }

        /// <summary>
        /// Gets the air cost (per cubic meter).
        /// </summary>
        decimal AirCost { get; }

        /// <summary>
        /// Gets the water cost (per cubic meter).
        /// </summary>
        decimal WaterCost { get; }

        /// <summary>
        /// Gets the energy cost (per kilowatt hour).
        /// </summary>
        decimal EnergyCost { get; }

        /// <summary>
        /// Gets the cost for renting production area (per square meter per month).
        /// </summary>
        decimal ProductionAreaRentalCost { get; }

        /// <summary>
        /// Gets the unskilled labor cost (per hour).
        /// </summary>
        decimal UnskilledLaborCost { get; }

        /// <summary>
        /// Gets the skilled labor cost (per hour).
        /// </summary>
        decimal SkilledLaborCost { get; }

        /// <summary>
        /// Gets the foreman cost (per hour).
        /// </summary>
        decimal ForemanCost { get; }

        /// <summary>
        /// Gets the technician cost (per hour).
        /// </summary>
        decimal TechnicianCost { get; }

        /// <summary>
        /// Gets the engineer cost (per hour).
        /// </summary>
        decimal EngineerCost { get; }

        /// <summary>
        /// Gets the labor availability.
        /// </summary>
        decimal LaborAvailability { get; }

        /// <summary>
        /// Gets the increase rate for labor costs for when there is 1 extra shift.
        /// </summary>
        decimal ShiftCharge1ShiftModel { get; }

        /// <summary>
        /// Gets the increase rate for labor costs for when there are 2 extra shifts.
        /// </summary>
        decimal ShiftCharge2ShiftModel { get; }

        /// <summary>
        /// Gets the increase rate for labor costs for when there are 3 extra shifts.
        /// </summary>
        decimal ShiftCharge3ShiftModel { get; }
    }
}
