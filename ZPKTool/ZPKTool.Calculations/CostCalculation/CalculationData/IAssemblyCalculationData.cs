﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of an Assembly.
    /// </summary>
    public interface IAssemblyCalculationData
    {
        #region Assembly data

        /// <summary>
        /// Gets the assembly's id.
        /// </summary>        
        Guid Id { get; }

        /// <summary>
        /// Gets the assembly's name.
        /// </summary>        
        string Name { get; }

        /// <summary>
        /// Gets the assembly's number.
        /// </summary>
        string Number { get; }

        /// <summary>
        /// Gets a value indicating whether the assembly is external.
        /// </summary>
        bool IsExternal { get; }

        /// <summary>
        /// Gets a value indicating whether sales and administration overhead (SG&amp;A) is enabled when the assembly is external.
        /// </summary>
        bool HasExternalSGA { get; }

        /// <summary>
        /// Gets the assembly's calculation accuracy.
        /// <para />
        /// The default value should be <see cref="PartCalculationAccuracy.FineCalculation"/>.
        /// </summary>        
        PartCalculationAccuracy CalculationAccuracy { get; }

        /// <summary>
        /// Gets the assembly's target price.
        /// </summary>
        decimal? TargetPrice { get; }

        /// <summary>
        /// Gets the assembly's purchase price.
        /// </summary>
        decimal? PurchasePrice { get; }

        /// <summary>
        /// Gets the assembly's estimated cost.
        /// </summary>
        decimal EstimatedCost { get; }

        /// <summary>
        /// Gets the assembly's payment terms.
        /// </summary>
        decimal PaymentTerms { get; }
                
        /// <summary>
        /// Gets a value indicating whether tooling is active for the assembly.
        /// </summary>
        bool ToolingActive { get; }

        /// <summary>
        /// Gets the assembly's description.
        /// </summary>        
        string Description { get; }

        /// <summary>
        /// Gets the assembly's index, indicating its position in the parent's list of sub-assemblies.
        /// </summary>        
        int? Index { get; }

        /// <summary>
        /// Gets the calculation variant (or version) to be used when calculating the assembly's cost.
        /// </summary>        
        string CalculationVariant { get; }

        /// <summary>
        /// Gets the development cost of the assembly.
        /// </summary>        
        decimal DevelopmentCost { get; }

        /// <summary>
        /// Gets the project invest for the assembly.
        /// </summary>        
        decimal ProjectInvest { get; }

        /// <summary>
        /// Gets the assembly's other cost value.
        /// </summary>        
        decimal OtherCost { get; }

        /// <summary>
        /// Gets the packaging cost of the assembly.
        /// </summary>        
        decimal PackagingCost { get; }

        /// <summary>
        /// Gets the transport cost of the assembly.
        /// </summary>        
        decimal TransportCost { get; }

        /// <summary>
        /// Gets the logistic cost of the assembly.
        /// </summary>
        decimal LogisticCost { get; }

        /// <summary>
        /// Gets a value indicating whether to calculate the logistic cost or use the value in the <see cref="LogisticCost"/> property.
        /// </summary>
        bool CalculateLogisticCost { get; }

        /// <summary>
        /// Gets a value indicating whether the additional costs are per total amount of pieces produced in the assembly's lifetime (value of true)
        /// or per one piece of the assembly (value of false).
        /// </summary>        
        bool AreAdditionalCostsPerPartsTotal { get; }

        /// <summary>
        /// Gets the assembly's weight.
        /// </summary>
        decimal Weight { get; }

        /// <summary>
        /// Gets the assembly's batch size per year (number of batches produced every year).
        /// </summary>
        int BatchSizePerYear { get; }

        /// <summary>
        /// Gets the assembly's life time.
        /// </summary>
        int LifeTime { get; }

        /// <summary>
        /// Gets the assembly's yearly production quantity.
        /// </summary>
        int YearlyProductionQuantity { get; }

        /// <summary>
        /// Gets a value indicating whether the assembly is reusable after being rejected during the process.
        /// </summary>
        bool Reusable { get; }

        /// <summary>
        /// Gets the data necessary for calculating the costs of the assembly's sub-assemblies.
        /// </summary>
        IEnumerable<IAssemblyCalculationData> SubAssemblies { get; }

        /// <summary>
        /// Gets the data necessary for calculating the costs of the assembly's sub-parts.
        /// </summary>
        IEnumerable<IPartCalculationData> SubParts { get; }

        /// <summary>
        /// Gets the data for the assembly's process steps.
        /// </summary>        
        IEnumerable<IAssemblingProcessStepCalculationData> ProcessSteps { get; }

        /// <summary>
        /// Gets the overhead settings data to be used when calculating the assembly's cost.
        /// </summary>
        IOverheadSettingsData OverheadSettings { get; }

        /// <summary>
        /// Gets the country settings data to be used when calculating the assembly's cost.
        /// </summary>
        ICountrySettingsData CountrySettings { get; }

        /// <summary>
        /// Gets the data necessary for calculating the assembly's yearly production quantity.        
        /// </summary>
        IYearlyProductionQuantityData YearlyProductionQuantityCalculationData { get; }

        #endregion Assembly data

        #region External data

        /// <summary>
        /// Gets or sets the depreciation period of the project to which the assembly belongs.
        /// </summary>        
        int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the project to which the assembly belongs.
        /// </summary>        
        decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost ratio of the project to which the assembly belongs.
        /// </summary>        
        decimal? ProjectLogisticCostRatio { get; set; }
                
        #endregion External data
    }
}