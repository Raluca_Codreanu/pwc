﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the basic settings data needed for cost calculations.
    /// </summary>
    public interface IBasicSettingsData
    {
        /// <summary>
        /// Gets the asset rate.
        /// </summary>
        decimal AssetRate { get; }

        /// <summary>
        /// Gets the depreciation period, in years.
        /// </summary>
        int DepreciationPeriod { get; }

        /// <summary>
        /// Gets the depreciation rate.
        /// </summary>
        decimal DepreciationRate { get; }

        /// <summary>
        /// Gets the logistic cost ratio.
        /// </summary>
        decimal LogisticCostRatio { get; }

        /// <summary>
        /// Gets the hours per shift.
        /// </summary>
        decimal HoursPerShift { get; }

        /// <summary>
        /// Gets the production days per week.
        /// </summary>
        decimal ProductionDays { get; }

        /// <summary>
        /// Gets the production weeks per year.
        /// </summary>
        decimal ProductionWeeks { get; }

        /// <summary>
        /// Gets the shifts per week.
        /// </summary>
        decimal ShiftsPerWeek { get; }

        /// <summary>
        /// Gets the part batches per year.
        /// </summary>
        int PartBatch { get; }
    }
}
