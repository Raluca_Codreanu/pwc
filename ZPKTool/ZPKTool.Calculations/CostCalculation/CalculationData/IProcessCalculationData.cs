﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the process data necessary for calculating the cost of both assembling and manufacturing processes.
    /// </summary>
    public interface IProcessCalculationData
    {
        /// <summary>
        /// Gets the data for the process' steps.
        /// </summary>
        IEnumerable<IProcessStepCalculationData> Steps { get; }
    }
}
