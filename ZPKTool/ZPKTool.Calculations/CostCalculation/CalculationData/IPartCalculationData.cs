﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Part.
    /// </summary>
    public interface IPartCalculationData
    {
        /// <summary>
        /// Gets the part's id.
        /// </summary>        
        Guid Id { get; }

        /// <summary>
        /// Gets the part's name.
        /// </summary>        
        string Name { get; }

        /// <summary>
        /// Gets the part's number.
        /// </summary>
        string Number { get; }

        /// <summary>
        /// Gets a value indicating whether the part is external.
        /// </summary>
        bool IsExternal { get; }

        /// <summary>
        /// Gets a value indicating whether sales and administration overhead (SG&amp;A) is enabled when the part is external.
        /// </summary>
        bool HasExternalSGA { get; }

        /// <summary>
        /// Gets the part's calculation accuracy.
        /// <para />
        /// The default value should be <see cref="PartCalculationAccuracy.FineCalculation"/>.
        /// </summary>        
        PartCalculationAccuracy CalculationAccuracy { get; }

        /// <summary>
        /// Gets the part's target price.
        /// </summary>
        decimal? TargetPrice { get; }

        /// <summary>
        /// Gets the part's purchase price.
        /// </summary>
        decimal? PurchasePrice { get; }

        /// <summary>
        /// Gets the part's remarks.
        /// </summary>        
        string Remarks { get; }

        /// <summary>
        /// Gets the part's estimated cost.
        /// </summary>
        decimal EstimatedCost { get; }

        /// <summary>
        /// Gets the part's payment terms.
        /// </summary>
        decimal PaymentTerms { get; }

        /// <summary>
        /// Gets a value indicating whether tooling is active for the part.
        /// </summary>
        bool ToolingActive { get; }

        /// <summary>
        /// Gets the part's description.
        /// </summary>        
        string Description { get; }

        /// <summary>
        /// Gets the part's index, indicating its position in the parent's list of sub-parts.
        /// </summary>        
        int? Index { get; }

        /// <summary>
        /// Gets the calculation variant (or version) to be used when calculating the part's cost.
        /// </summary>        
        string CalculationVariant { get; }

        /// <summary>
        /// Gets the development cost of the part.
        /// </summary>        
        decimal DevelopmentCost { get; }

        /// <summary>
        /// Gets the project invest for the part.
        /// </summary>        
        decimal ProjectInvest { get; }

        /// <summary>
        /// Gets the part's other cost value.
        /// </summary>        
        decimal OtherCost { get; }

        /// <summary>
        /// Gets the packaging cost of the part.
        /// </summary>        
        decimal PackagingCost { get; }

        /// <summary>
        /// Gets the transport cost of the part.
        /// </summary>        
        decimal TransportCost { get; }

        /// <summary>
        /// Gets the logistic cost of the part.
        /// </summary>
        decimal LogisticCost { get; }

        /// <summary>
        /// Gets a value indicating whether to calculate the logistic cost or use the value in the <see cref="LogisticCost"/> property.
        /// </summary>
        bool CalculateLogisticCost { get; }

        /// <summary>
        /// Gets a value indicating whether the additional costs are per total amount of pieces produced in the part's lifetime (value of true)
        /// or per one piece of the part (value of false).
        /// </summary>        
        bool AreAdditionalCostsPerPartsTotal { get; }

        /// <summary>
        /// Gets the part's weight.
        /// </summary>        
        decimal Weight { get; }

        /// <summary>
        /// Gets the part's manufacturing ratio.
        /// </summary>
        decimal ManufacturingRatio { get; }

        /// <summary>
        /// Gets the part's batch size per year (number of batches produced every year).
        /// </summary>
        int BatchSizePerYear { get; }

        /// <summary>
        /// Gets the part's life time.
        /// </summary>
        int LifeTime { get; }

        /// <summary>
        /// Gets the part's yearly production quantity.
        /// </summary>
        int YearlyProductionQuantity { get; }

        /// <summary>
        /// Gets a value indicating whether the part is reusable after being rejected during the process.
        /// </summary>
        bool Reusable { get; }

        /// <summary>
        /// Gets the part's asset rate.
        /// </summary>
        decimal AssetRate { get; }

        /// <summary>
        /// Gets the overhead settings data to be used for calculating the part's cost.
        /// </summary>
        IOverheadSettingsData OverheadSettings { get; }

        /// <summary>
        /// Gets the country settings data to be used for calculating the part's cost..
        /// </summary>
        ICountrySettingsData CountrySettings { get; }

        /// <summary>
        /// Gets the part's raw part data.
        /// </summary>
        IRawPartCalculationData RawPart { get; }

        /// <summary>
        /// Gets the data for the part's process steps.
        /// </summary>
        IEnumerable<IManufacturingProcessStepCalculationData> ProcessSteps { get; }

        /// <summary>
        /// Gets the data for the part's raw materials.
        /// </summary>
        IEnumerable<IRawMaterialCalculationData> RawMaterials { get; }

        /// <summary>
        /// Gets the data for the part's commodities.
        /// </summary>
        IEnumerable<ICommodityCalculationData> Commodities { get; }

        /// <summary>
        /// Gets the data necessary for calculating the part's yearly production quantity.        
        /// </summary>
        IYearlyProductionQuantityData YearlyProductionQuantityCalculationData { get; }

        #region External data

        /// <summary>
        /// Gets or sets the depreciation period of the project to which the assembly belongs.
        /// </summary>        
        int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the project to which the assembly belongs.
        /// </summary>        
        decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost ratio of the project to which the assembly belongs.
        /// </summary>        
        decimal? ProjectLogisticCostRatio { get; set; }

        #endregion External data
    }
}
