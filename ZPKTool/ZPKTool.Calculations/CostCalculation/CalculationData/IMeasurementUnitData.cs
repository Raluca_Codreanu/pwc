﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data of a measurement unit needed for cost calculations.
    /// </summary>
    public interface IMeasurementUnitData
    {
        /// <summary>
        /// Gets the unit's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the unit's symbol.
        /// </summary>
        string Symbol { get; }

        /// <summary>
        /// Gets the unit's type.
        /// </summary>
        MeasurementUnitType Type { get; }

        /// <summary>
        /// Gets the scale to which the unit belongs.
        /// </summary>
        MeasurementUnitScale Scale { get; }

        /// <summary>
        /// Gets the conversion rate with respect to the unit in which all values are stored in the database.
        /// </summary>
        decimal ConversionRate { get; }

        /// <summary>
        /// Gets the conversion factor with respect to the base unit of the scale to which this unit belongs.
        /// </summary>
        decimal ScaleFactor { get; }
    }
}
