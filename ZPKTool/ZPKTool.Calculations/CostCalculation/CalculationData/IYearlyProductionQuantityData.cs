﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the part/assembly data necessary to calculate the yearly production quantity.
    /// </summary>
    public interface IYearlyProductionQuantityData
    {
        /// <summary>
        /// Gets the yearly production quantity of the part/assembly.
        /// </summary>
        decimal YearlyProductionQuantity { get; }

        /// <summary>
        /// Gets the batch size per year of the part/assembly.
        /// </summary>
        decimal BatchSizePerYear { get; }

        /// <summary>
        /// Gets the necessary data from the part/assembly process.
        /// </summary>
        IEnumerable<IProcessStepDataForYearlyProductionQuantity> ProcessData { get; }

        /// <summary>
        /// Gets the yearly production quantity data of the part's/assembly's parent.
        /// </summary>
        IYearlyProductionQuantityData ParentData { get; }
    }
}
