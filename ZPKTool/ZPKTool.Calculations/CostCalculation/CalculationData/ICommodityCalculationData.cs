﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Commodity.
    /// </summary>
    public interface ICommodityCalculationData
    {
        /// <summary>
        /// Gets the commodity's id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the commodity's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the number of the commodity pieces used.
        /// </summary>
        int Amount { get; }

        /// <summary>
        /// Gets the commodity's remarks.
        /// </summary>
        string Remarks { get; }

        /// <summary>
        /// Gets the name of the commodity's manufacturer.
        /// </summary>
        string ManufacturerName { get; }

        /// <summary>
        /// Gets the commodity's weight.
        /// </summary>
        decimal? Weight { get; }

        /// <summary>
        /// Gets the commodity's reject ratio (the percentage of the commodity pieces that are rejected during the process).
        /// </summary>
        decimal RejectRatio { get; }

        /// <summary>
        /// Gets the commodity's price per piece (or price per unit).
        /// </summary>
        decimal Price { get; }

        #region External data

        /// <summary>
        /// Gets or sets the percentage used to calculate the commodity's overhead cost.
        /// </summary>
        decimal CommodityOverheadRate { get; set; }

        /// <summary>
        /// Gets or sets the percentage used to calculate the commodity's margin cost.
        /// </summary>
        decimal CommodityMarginRate { get; set; }

        #endregion External data
    }
}
