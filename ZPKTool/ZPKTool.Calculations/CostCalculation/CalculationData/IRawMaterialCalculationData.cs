﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Raw Material.
    /// </summary>
    public interface IRawMaterialCalculationData
    {
        /// <summary>
        /// Gets the raw material's id.
        /// </summary>        
        Guid Id { get; }

        /// <summary>
        /// Gets the raw material's name.
        /// </summary>        
        string Name { get; }

        /// <summary>
        /// Gets the raw material's price (per base weight unit).
        /// </summary>
        decimal Price { get; }

        /// <summary>
        /// Gets the part weight.
        /// </summary>
        decimal? PartWeight { get; }

        /// <summary>
        /// Gets the material weight.
        /// </summary>
        decimal? MaterialWeight { get; }

        /// <summary>
        /// Gets the material's sprue value.
        /// </summary>
        decimal? Sprue { get; }

        /// <summary>
        /// Gets the scrap refund ratio for the material.
        /// </summary>
        decimal ScrapRefundRatio { get; }

        /// <summary>
        /// Gets the type of the calculation to use for calculating the scrap cost.
        /// </summary>        
        ScrapCalculationType ScrapCalculationType { get; }

        /// <summary>
        /// Gets the loss for the material.
        /// </summary>
        decimal Loss { get; }

        /// <summary>
        /// Gets the (weight) measurement unit for which the price is expressed.
        /// </summary>
        IMeasurementUnitData PriceUnit { get; }

        /// <summary>
        /// Gets the material's reject ratio.
        /// </summary>
        decimal RejectRatio { get; }

        /// <summary>
        /// Gets the raw material's stock keeping period, in days.
        /// </summary>
        decimal StockKeeping { get; }

        /// <summary>
        /// Gets the material's recycling ratio.
        /// </summary>
        decimal RecyclingRatio { get; }

        #region External data

        /// <summary>
        /// Gets or sets the percentage that is used when calculating the material overhead cost.
        /// </summary>        
        decimal MaterialOverheadRate { get; set; }

        /// <summary>
        /// Gets or sets the asset rate needed to calculate the WIP cost.
        /// </summary>        
        decimal AssetRate { get; set; }

        /// <summary>
        /// Gets or sets the batch size from part level.
        /// </summary>        
        int BatchSize { get; set; }

        /// <summary>
        /// Gets or sets the annual production quantity from part level.
        /// </summary>        
        int AnnualProductionQty { get; set; }

        #endregion External data
    }
}
