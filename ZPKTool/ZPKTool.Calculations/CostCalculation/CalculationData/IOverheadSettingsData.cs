﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the overhead settings data needed for cost calculations.
    /// </summary>
    public interface IOverheadSettingsData
    {
        /// <summary>
        /// Gets the material overhead rate.
        /// </summary>
        decimal MaterialOverhead { get; }

        /// <summary>
        /// Gets the commodity overhead rate.
        /// </summary>
        decimal CommodityOverhead { get; }

        /// <summary>
        /// Gets the consumable overhead rate.
        /// </summary>
        decimal ConsumableOverhead { get; }

        /// <summary>
        /// Gets the manufacturing overhead rate.
        /// </summary>
        decimal ManufacturingOverhead { get; }

        /// <summary>
        /// Gets the other cost overhead rate.
        /// </summary>
        decimal OtherCostOverhead { get; }

        /// <summary>
        /// Gets the packaging overhead rate.
        /// </summary>
        decimal PackagingOverhead { get; }

        /// <summary>
        /// Gets the logistic overhead rate.
        /// </summary>
        decimal LogisticOverhead { get; }

        /// <summary>
        /// Gets the sales and administration overhead rate.
        /// </summary>
        decimal SalesAndAdministrationOverhead { get; }

        /// <summary>
        /// Gets the company surcharge overhead rate.
        /// </summary>
        decimal CompanySurchargeOverhead { get; }

        /// <summary>
        /// Gets the external work overhead rate.
        /// </summary>
        decimal ExternalWorkOverhead { get; }

        /// <summary>
        /// Gets the material margin rate.
        /// </summary>
        decimal MaterialMargin { get; }

        /// <summary>
        /// Gets the consumable margin rate.
        /// </summary>
        decimal ConsumableMargin { get; }

        /// <summary>
        /// Gets the commodity margin rate.
        /// </summary>
        decimal CommodityMargin { get; }

        /// <summary>
        /// Gets the manufacturing margin rate.
        /// </summary>
        decimal ManufacturingMargin { get; }

        /// <summary>
        /// Gets the external work margin rate.
        /// </summary>
        decimal ExternalWorkMargin { get; }

        /// <summary>
        /// Creates a copy of this instance.
        /// </summary>
        /// <returns>The copy.</returns>
        IOverheadSettingsData Copy();
    }
}
