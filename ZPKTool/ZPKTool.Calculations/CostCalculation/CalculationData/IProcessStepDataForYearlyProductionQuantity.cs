﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data.Common;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data of a process step that is necessary to calculate the yearly production quantity.
    /// </summary>
    public interface IProcessStepDataForYearlyProductionQuantity
    {
        /// <summary>
        /// Gets the id of the process step.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the index of the process step.
        /// </summary>
        int Index { get; }

        /// <summary>
        /// Gets the reject ratio of the process step.
        /// </summary>
        decimal RejectRatio { get; }

        /// <summary>
        /// Gets the step's calculation accuracy.
        /// <para />
        /// The default value should be <see cref="ProcessCalculationAccuracy.FineCalculation"/>.
        /// </summary>   
        ProcessCalculationAccuracy CalculationAccuracy { get; }
    }
}
