﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Die.
    /// </summary>
    public interface IDieCalculationData
    {
        /// <summary>
        /// Gets the die's id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the die's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the die's investment.
        /// </summary>
        decimal Investment { get; }

        /// <summary>
        /// Gets the die's life time.
        /// </summary>
        decimal LifeTime { get; }

        /// <summary>
        /// Gets the number of dies paid by customer.
        /// </summary>
        int DiesetsNumberPaidByCustomer { get; }

        /// <summary>
        /// Gets a value indicating whether the die's cost allocation should be calculated based on the number of parent parts produced per life time
        /// or based on the number of parts defined by the value of the <see cref="CostAllocationNumberOfParts"/> property.
        /// </summary>
        bool CostAllocationBasedOnPartsPerLifeTime { get; }

        /// <summary>
        /// Gets the number of parts to use when calculating the die's cost allocation.
        /// </summary>
        int CostAllocationNumberOfParts { get; }

        /// <summary>
        /// Gets the die's maintenance cost.
        /// </summary>
        decimal Maintenance { get; }

        /// <summary>
        /// Gets a value indicating whether the value of the <see cref="Maintenance"/> property represents the percentage to be used to calculate
        /// the maintenance cost (value of true) or the actual maintenance cost (value of false).
        /// </summary>
        bool IsMaintenanceInPercentage { get; }

        /// <summary>
        /// Gets the die's wear cost.
        /// </summary>
        decimal Wear { get; }

        /// <summary>
        /// Gets a value indicating whether the value of the <see cref="Wear"/> property represents the percentage to be used to calculate
        /// the wear cost (value of true) or the actual wear cost (value of false).
        /// </summary>
        bool IsWearInPercentage { get; }

        /// <summary>
        /// Gets the die's allocation ratio.
        /// </summary>
        decimal AllocationRatio { get; }

        #region External data

        /// <summary>
        /// Gets or sets the lifecycle production quantity of the die's parent.
        /// </summary>
        decimal ParentLifecycleProductionQuantity { get; set; }

        #endregion External data
    }
}
