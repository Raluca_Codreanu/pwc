﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the calculation data needed for the association of a sub-assembly or sub-part with an assembling process step.
    /// </summary>
    public interface IProcessStepPartAmountCalculationData
    {
        /// <summary>
        /// Gets the amount of the Assembly or Part used in the assembling process step.
        /// </summary>        
        int Amount { get; }

        /// <summary>
        /// Gets the id of the Assembly or Part.
        /// </summary>        
        Guid PartId { get; }
    }
}
