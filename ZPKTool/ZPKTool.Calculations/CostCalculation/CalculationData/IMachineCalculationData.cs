﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the data that is necessary to calculate the cost of a Machine.
    /// </summary>
    public interface IMachineCalculationData
    {
        /// <summary>
        /// Gets the machine's id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the machine's name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the machine's index number in the parent's machines list.
        /// </summary>
        int? Index { get; }

        /// <summary>
        /// Gets the machine's floor size.
        /// </summary>        
        decimal FloorSize { get; }

        /// <summary>
        /// Gets the machine's workspace area.
        /// </summary>
        decimal WorkspaceArea { get; }

        /// <summary>
        /// Gets the machine's depreciation period.
        /// </summary>
        int? DepreciationPeriod { get; }

        /// <summary>
        /// Gets the machine's depreciation rate.
        /// </summary>
        decimal? DepreciationRate { get; }

        /// <summary>
        /// Gets the machine's manufacturing year.
        /// </summary>
        short? ManufacturingYear { get; }

        /// <summary>
        /// Gets the machine investment.
        /// </summary>
        decimal MachineInvestment { get; }

        /// <summary>
        /// Gets a value indicating whether the machine is leased.
        /// </summary>
        bool IsMachineLeased { get; }

        /// <summary>
        /// Gets the lease costs for the machine, if it is leased.
        /// </summary>
        decimal MachineLeaseCosts { get; }
                
        /// <summary>
        /// Gets the setup investment for the machine.
        /// </summary>
        decimal SetupInvestment { get; }

        /// <summary>
        /// Gets the investment for additional machine equipment.
        /// </summary>
        decimal AdditionalEquipmentInvestment { get; }

        /// <summary>
        /// Gets the fundamental setup investment for the machine.
        /// </summary>
        decimal FundamentalSetupInvestment { get; }

        /// <summary>
        /// Gets a value indicating whether the maintenance cost should be calculated using the K-value.
        /// </summary>
        bool CalculateWithKValue { get; }

        /// <summary>
        /// Gets the machine's K-value.
        /// </summary>
        decimal KValue { get; }

        /// <summary>
        /// Gets a value indicating whether the machine's consumables cost is calculated (value of true) or inputted as a fixed value (value of false).
        /// </summary>
        bool IsConsumablesCostCalculated { get; }

        /// <summary>
        /// Gets the rate that will be used to calculate the machine's consumables cost when the cost is calculated, not inputted.
        /// </summary>
        decimal ConsumablesCostCalculationRate { get; }
                
        /// <summary>
        /// Gets the machine's consumables cost value (the inputted value, not the calculated one).
        /// </summary>
        decimal ConsumablesCost { get; }

        /// <summary>
        /// Gets the external work cost for the machine.
        /// </summary>
        decimal ExternalWorkCost { get; }

        /// <summary>
        /// Gets the material cost for the machine.
        /// </summary>
        decimal MaterialCost { get; }

        /// <summary>
        /// Gets the repairs cost for the machine.
        /// </summary>
        decimal RepairsCost { get; }

        /// <summary>
        /// Gets the machine's power consumption.
        /// </summary>
        decimal PowerConsumption { get; }

        /// <summary>
        /// Gets the machine's air consumption.
        /// </summary>
        decimal AirConsumption { get; }

        /// <summary>
        /// Gets the machine's water consumption.
        /// </summary>
        decimal WaterConsumption { get; }

        /// <summary>
        /// Gets the machine's  full load rate.
        /// </summary>
        decimal FullLoadRate { get; }

        /// <summary>
        /// Gets a value indicating whether the machine is project specific.
        /// </summary>
        bool IsProjectSpecific { get; }

        /// <summary>
        /// Gets the machine's availability.
        /// </summary>
        decimal Availability { get; }

        /// <summary>
        /// Gets the amount of this machine used in the process step where the machine is defined.
        /// </summary>
        int Amount { get; }

        /// <summary>
        /// Gets the machine's fossil energy consumption ratio. The renewable energy consumption ratio is 100% minus this value.
        /// </summary>
        decimal FossilEnergyConsumptionRatio { get; }

        #region External data

        /// <summary>
        /// Gets or sets the depreciation period of the project to which the machine belongs.
        /// </summary>        
        int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the project to which the machine belongs.
        /// </summary>        
        decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the shifts per week value of the process step to which the machine belongs.
        /// </summary>        
        decimal ProcessStepShiftsPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the extra shifts per week value of the process step to which the machine belongs.
        /// </summary>
        decimal ProcessStepExtraShiftsPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the hours per shift value of the process step to which the machine belongs.
        /// </summary>        
        decimal ProcessStepHoursPerShift { get; set; }

        /// <summary>
        /// Gets or sets the production weeks per year value of the process step to which the machine belongs.
        /// </summary>        
        decimal ProcessStepProductionWeeksPerYear { get; set; }

        /// <summary>
        /// Gets or sets the energy cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>        
        decimal CountrySettingsEnergyCost { get; set; }

        /// <summary>
        /// Gets or sets the air cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>        
        decimal CountrySettingsAirCost { get; set; }

        /// <summary>
        /// Gets or sets the water cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>   
        decimal CountrySettingsWaterCost { get; set; }

        /// <summary>
        /// Gets or sets the rental cost for production area from the country settings of the entity to whose process the machine belongs.
        /// </summary>
        decimal CountrySettingsRentalCostProductionArea { get; set; }

        #endregion External data
    }
}
