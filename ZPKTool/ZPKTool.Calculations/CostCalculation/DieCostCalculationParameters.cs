﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The input parameters for the cost calculation of a die.
    /// </summary>
    public class DieCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the lifecycle production quantity of the die's parent.
        /// </summary>
        public decimal ParentLifecycleProductionQuantity { get; set; }
    }
}
