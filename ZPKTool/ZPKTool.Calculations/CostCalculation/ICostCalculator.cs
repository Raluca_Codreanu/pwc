﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Defines the cost calculation for all types of data objects.
    /// </summary>
    public interface ICostCalculator
    {
        /// <summary>
        /// Gets or sets the version of the ICostCalculator implementation.
        /// </summary>        
        string Version { get; set; }

        /// <summary>
        /// Gets the basic settings to be used during calculations.
        /// </summary>
        BasicSetting BasicSettings { get; }

        /// <summary>
        /// Gets the measurement units to be used during calculations.
        /// </summary>
        IEnumerable<MeasurementUnit> MeasurementUnits { get; }

        /// <summary>
        /// Calculates the cost of an Assembly.
        /// </summary>
        /// <param name="assembly">The assembly whose cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the cost calculation.</param>
        /// <returns>
        /// An instance of <see cref="CalculationResult" /> that contains the cost breakdown of the assembly.
        /// </returns>
        CalculationResult CalculateAssemblyCost(Assembly assembly, AssemblyCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of a Part or Raw Part.
        /// </summary>        
        /// <param name="part">The part whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown of the given part.
        /// </returns>
        CalculationResult CalculatePartCost(Part part, PartCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of a Commodity.
        /// </summary>
        /// <param name="commodity">The commodity whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the given commodity.
        /// </returns>
        CommodityCost CalculateCommodityCost(Commodity commodity, CommodityCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of the specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the given consumable.
        /// </returns>
        ConsumableCost CalculateConsumableCost(Consumable consumable, ConsumableCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of a Die.
        /// </summary>
        /// <param name="die">The die to calculate the cost for.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>The cost breakdown for the die.</returns>
        DieCost CalculateDieCost(Die die, DieCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of a Machine.
        /// </summary>        
        /// <param name="machine">The machine whose cost to calculate.</param>
        /// <param name="parameters">The input parameters needed for calculation.</param>
        /// <returns>An object containing the cost breakdown for the machine.</returns>
        MachineCost CalculateMachineCost(Machine machine, MachineCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the cost of a Raw Material.
        /// </summary>
        /// <param name="material">The raw material whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation. Can be null if you need to calculate only the net cost.</param>
        /// <returns>
        /// An object containing the cost breakdown for the raw material.
        /// </returns>
        RawMaterialCost CalculateRawMaterialCost(RawMaterial material, RawMaterialCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the weight of a part, in kilograms.
        /// </summary>
        /// <param name="part">The part whose whose weight to calculate.</param>
        /// <returns>
        /// The part's weight.
        /// </returns>
        decimal CalculatePartWeight(Part part);

        /// <summary>
        /// Calculates the weight of the material going into the process of a part, in kilograms.
        /// </summary>
        /// <param name="part">The part for which to calculate.</param>
        /// <returns>
        /// The material weight.
        /// </returns>
        decimal CalculateProcessMaterial(Part part);

        /// <summary>
        /// Calculates the net number of pieces produced in an assembly's lifetime.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The net number of assemblies.
        /// </returns>
        decimal CalculateNetLifetimeProductionQuantity(Assembly assembly);

        /// <summary>
        /// Calculates the net number of pieces produced in a part's lifetime.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The net number of parts.
        /// </returns>
        decimal CalculateNetLifetimeProductionQuantity(Part part);

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in an assembly's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        decimal CalculateGrossLifetimeProductionQuantity(Assembly assembly, AssemblyCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in a part's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        decimal CalculateGrossLifetimeProductionQuantity(Part part, PartCostCalculationParameters parameters);

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for an Assembly.
        /// <para />
        /// In order to calculate a correct result the assembly's full parent hierarchy must be loaded, that is, the references chain from the assembly
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="assembly" /> was null</exception>        
        decimal CalculateYearlyProductionQuantity(Assembly assembly);

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for a Part or Raw Part.
        /// <para />
        /// In order to calculate a correct result the part's full parent hierarchy must be loaded, that is, the references chain from the part
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="part" /> was null</exception>
        decimal CalculateYearlyProductionQuantity(Part part);
    }
}