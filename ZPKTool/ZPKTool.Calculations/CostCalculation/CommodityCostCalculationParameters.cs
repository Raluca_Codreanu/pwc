﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The input parameters for the cost calculation of a commodity.
    /// </summary>
    public class CommodityCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the percentage used to calculate the commodity's overhead cost.
        /// </summary>
        public decimal CommodityOverheadRate { get; set; }

        /// <summary>
        /// Gets or sets the percentage used to calculate the commodity's margin cost.
        /// </summary>
        public decimal CommodityMarginRate { get; set; }
    }
}
