﻿namespace ZPKTool.Calculations.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// Helper class for working with cost calculators and performing different calculation related tasks.
    /// </summary>
    public static class CostCalculationHelper
    {
        /// <summary>
        /// Creates a new instance of <see cref="AssemblyCostCalculationParameters"/> and populates it with data from the specified project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>An instance of <see cref="AssemblyCostCalculationParameters"/>.</returns>
        public static AssemblyCostCalculationParameters CreateAssemblyParamsFromProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "The project parameter can not be null.");
            }

            return new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = project.DepreciationPeriod.GetValueOrDefault(),
                ProjectDepreciationRate = project.DepreciationRate.GetValueOrDefault(),
                ProjectLogisticCostRatio = project.LogisticCostRatio,
                ProjectCreateDate = GetProjectCreateDate(project)
            };
        }

        /// <summary>
        /// Creates a new instance of <see cref="PartCostCalculationParameters"/> and populates it with data from the specified project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>An instance of <see cref="PartCostCalculationParameters"/>.</returns>
        public static PartCostCalculationParameters CreatePartParamsFromProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "The project parameter can not be null.");
            }

            return new PartCostCalculationParameters()
            {
                ProjectDepreciationPeriod = project.DepreciationPeriod.GetValueOrDefault(),
                ProjectDepreciationRate = project.DepreciationRate.GetValueOrDefault(),
                ProjectLogisticCostRatio = project.LogisticCostRatio,
                ProjectCreateDate = GetProjectCreateDate(project)
            };
        }

        /// <summary>
        /// Adjust the price of a raw material from the measurement unit in which is currently expressed to a new measurement unit in order to maintain
        /// the ratio of price per weight unit.
        /// <para />
        /// This adjustment is necessary to keep constant the cost of the material when the weight unit changes to another scale. The cost is price * quantity,
        /// so when the scale changes the quantity increases or decreases, directly affecting the cost even though you have the same quantity in the database unit.
        /// </summary>
        /// <param name="price">The price of the material.</param>
        /// <param name="priceUnit">The measurement unit in which the price is given. If is null, kilogram is used as default.</param>
        /// <param name="targetUnit">The measurement unit to which to adjust. If is null, kilogram is used as default.</param>
        /// <returns>
        /// The adjusted price.
        /// </returns>
        /// <remarks>
        /// The price is always in euro but the price unit can be kilograms or pounds.
        /// If you have a price of 1 euro/kg and a material quantity of 1 kg the cost is 1 euro (1 euro * 1kg). If you switch to pounds and don't do this
        /// adjustment, you get a cost of 2.20462 (1 euro * 2.20462 lb; 2.20462 is the quantity converted to pounds). With the adjustment you get a price
        /// of 0.4535929 euro/lb; the cost is the same 1 euro (0.4535929 * 2.20462).
        /// </remarks>
        public static decimal AdjustRawMaterialPriceToNewWeightUnit(decimal price, MeasurementUnit priceUnit, MeasurementUnit targetUnit)
        {
            if (price == 0m)
            {
                return 0m;
            }

            // If the price unit is missing we use use kg as default (conversion rate is 1)
            decimal priceUnitConversionRate = 1m;
            if (priceUnit != null && priceUnit.ConversionRate != 0m)
            {
                // For the calculations below, we need the conversion rate of the base unit of the measurement units scale to which the price unit belongs.
                // This conversion rate can be calculated as the scale factor (how many units are equal to 1 base unit of the scale) divided by the
                // conversion rate (how many units equal 1 unit in which the values are stored in the database, kg in this case). If the price unit is the base unit
                // of its scale this calculation's result is 1 which is a base unit's conversion rate anyway.
                var conversionRate = priceUnit.ScaleFactor / priceUnit.ConversionRate;

                // The value obtained above is how many units of the price measurement unit equal to 1 unit of the db storage measurement unit (kg).
                // We actually need how may units of the db storage measurement unit equal to 1 price unit.
                if (conversionRate != 0m)
                {
                    priceUnitConversionRate = 1m / conversionRate;
                }
            }

            decimal targetUnitConversionRate = targetUnit != null ? targetUnit.ConversionRate : 1m;

            // How many units of the target unit are in one unit of the price unit?
            decimal amountOfTargetUnits = 1m;
            if (priceUnitConversionRate != 0m && targetUnitConversionRate != 0m)
            {
                amountOfTargetUnits = priceUnitConversionRate / targetUnitConversionRate;
            }

            // The adjusted price is the old price divided by the mount of target units that corresponds to 1 unit of the price unit
            // ("corresponds to 1 unit" because the price is expressed per unit).
            var adjustedPrice = price / amountOfTargetUnits;

            return adjustedPrice;
        }

        /// <summary>
        /// Gets the project creation date if it can be obtained.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>The project creation date.</returns>
        public static DateTime? GetProjectCreateDate(Project project)
        {
            if (project == null)
            {
                return null;
            }

            if (project.CreateDate != null)
            {
                return project.CreateDate;
            }

            /*
             * If the project BaseCurrency is set, get first ExchangeRate change DateTime.
             */
            if (project.BaseCurrency != null
                && project.BaseCurrency.CurrenciesExchangeRateHistory.Count > 0)
            {
                var currenciesFirstChange =
                    project.BaseCurrency.CurrenciesExchangeRateHistory.OrderBy(c => c.Timestamp).FirstOrDefault();
                if (currenciesFirstChange != null)
                {
                    return currenciesFirstChange.Timestamp;
                }
            }

            /*
             * If none of the CreateDate or BaseCurrency weren't set 
             * returns the project StartDate.
             */
            return project.StartDate;
        }
    }
}
