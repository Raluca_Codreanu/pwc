﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the calculation cost for the process.
    /// </summary>
    public class ProcessCost
    {
        /// <summary>
        /// The cost calculation result of each step in the process.
        /// </summary>
        private List<ProcessStepCost> stepCosts = new List<ProcessStepCost>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessCost"/> class.
        /// </summary>
        public ProcessCost()
        {
            this.InvestmentCost = new InvestmentCost();
            this.CommoditiesCost = new CommoditiesCost();
            this.ConsumablesCost = new ConsumablesCost();
            this.RejectOverview = new ProcessRejectOverview();

            this.StepCosts = new ReadOnlyCollection<ProcessStepCost>(this.stepCosts);
        }

        /// <summary>
        /// Gets or sets the machine cost sum.
        /// </summary>
        /// <value>The machine cost sum.</value>
        public decimal MachineCostSum { get; set; }

        /// <summary>
        /// Gets or sets the setup cost sum.
        /// </summary>
        /// <value>The setup cost sum.</value>
        public decimal SetupCostSum { get; set; }

        /// <summary>
        /// Gets or sets the direct labor cost sum.
        /// </summary>
        /// <value>The direct labor cost sum.</value>
        public decimal DirectLabourCostSum { get; set; }

        /// <summary>
        /// Gets or sets the maintenance cost sum of all dies in the process.
        /// </summary>        
        public decimal DiesMaintenanceCostSum { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing cost sum.
        /// </summary>
        public decimal ManufacturingCostSum { get; set; }

        /// <summary>
        /// Gets or sets the part of the manufacturing cost sum that is estimated.
        /// <para />
        /// This value is already included in <see cref="ManufacturingCostSum" />.
        /// </summary>
        public decimal EstimatedManufacturingCostSum { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing overhead sum.
        /// </summary>
        /// <value>The manufacturing overhead sum.</value>
        public decimal ManufacturingOverheadSum { get; set; }

        /// <summary>
        /// Gets or sets the total reject cost for the process.
        /// </summary>        
        public decimal RejectCostSum { get; set; }

        /// <summary>
        /// Gets or sets the total manufacturing transport cost for the process.
        /// </summary>        
        public decimal ManufacturingTransportCost { get; set; }

        /// <summary>
        /// Gets or sets the total manufacturing cost sum.
        /// </summary>
        /// <value>The total manufacturing cost sum.</value>
        public decimal TotalManufacturingCostSum { get; set; }

        /// <summary>
        /// Gets or sets the die cost sum.
        /// </summary>
        /// <value>The die cost sum.</value>
        public decimal ToolAndDieCostSum { get; set; }

        /// <summary>
        /// Gets a collection containing the cost calculation result of each step in the process.
        /// </summary>                
        public ReadOnlyCollection<ProcessStepCost> StepCosts { get; private set; }

        /// <summary>
        /// Gets or sets the investment cost for the process' machines and dies.
        /// </summary>
        public InvestmentCost InvestmentCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of all commodities used in the process.
        /// </summary>        
        public CommoditiesCost CommoditiesCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of all consumables used in the process.
        /// </summary>        
        public ConsumablesCost ConsumablesCost { get; set; }

        /// <summary>
        /// Gets the reject overview.
        /// </summary>
        public ProcessRejectOverview RejectOverview { get; private set; }

        /// <summary>
        /// Adds the specified step cost to the process cost.        
        /// </summary>
        /// <param name="stepCost">The step cost.</param>
        public void AddStepCost(ProcessStepCost stepCost)
        {
            this.stepCosts.Add(stepCost);

            this.MachineCostSum += stepCost.MachineCost;
            this.SetupCostSum += stepCost.SetupCost;
            this.DirectLabourCostSum += stepCost.DirectLabourCost;
            this.DiesMaintenanceCostSum += stepCost.DiesMaintenanceCost;
            this.ManufacturingCostSum += stepCost.ManufacturingCost;
            this.ManufacturingOverheadSum += stepCost.ManufacturingOverheadCost;
            this.RejectCostSum += stepCost.RejectCost;
            this.ManufacturingTransportCost += stepCost.TransportCost;
            this.TotalManufacturingCostSum += stepCost.TotalManufacturingCost;
            this.ToolAndDieCostSum += stepCost.ToolAndDieCost;
            this.InvestmentCost.Add(stepCost.InvestmentCost);

            if (stepCost.ManufacturingCostIsEstimated)
            {
                this.EstimatedManufacturingCostSum += stepCost.ManufacturingCost;
            }
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ProcessCost Clone()
        {
            // Copy the value members
            ProcessCost clone = (ProcessCost)this.MemberwiseClone();

            // Clone the reference members
            if (this.CommoditiesCost != null)
            {
                clone.CommoditiesCost = this.CommoditiesCost.Clone();
            }

            if (this.ConsumablesCost != null)
            {
                clone.ConsumablesCost = this.ConsumablesCost.Clone();
            }

            if (this.InvestmentCost != null)
            {
                clone.InvestmentCost = this.InvestmentCost.Clone();
            }

            clone.stepCosts = new List<ProcessStepCost>();
            clone.StepCosts = new ReadOnlyCollection<ProcessStepCost>(clone.stepCosts);
            foreach (ProcessStepCost stepCost in this.stepCosts)
            {
                clone.stepCosts.Add(stepCost.Clone());
            }

            clone.RejectOverview = this.RejectOverview.Clone();

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.DiesMaintenanceCostSum *= conversionFactors.CurrencyFactor;
            this.DirectLabourCostSum *= conversionFactors.CurrencyFactor;
            this.MachineCostSum *= conversionFactors.CurrencyFactor;
            this.ManufacturingCostSum *= conversionFactors.CurrencyFactor;
            this.ManufacturingOverheadSum *= conversionFactors.CurrencyFactor;
            this.RejectCostSum *= conversionFactors.CurrencyFactor;
            this.SetupCostSum *= conversionFactors.CurrencyFactor;
            this.ToolAndDieCostSum *= conversionFactors.CurrencyFactor;
            this.TotalManufacturingCostSum *= conversionFactors.CurrencyFactor;
            this.ManufacturingTransportCost *= conversionFactors.CurrencyFactor;
            this.EstimatedManufacturingCostSum *= conversionFactors.CurrencyFactor;

            if (this.CommoditiesCost != null)
            {
                this.CommoditiesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.ConsumablesCost != null)
            {
                this.ConsumablesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.InvestmentCost != null)
            {
                this.InvestmentCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.StepCosts != null)
            {
                foreach (ProcessStepCost stepCost in this.StepCosts)
                {
                    stepCost.ApplyUnitConversion(conversionFactors);
                }
            }

            this.RejectOverview.ApplyUnitConversion(conversionFactors);
        }

        /// <summary>
        /// Sorts the <see cref="StepCosts"/> collection in ascending order according to the specified key.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="keySelector">A function that returns the key to use for sorting.</param>
        public void SortStepCosts<TKey>(Func<ProcessStepCost, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;
            this.stepCosts.Sort((x, y) => comparer.Compare(keySelector(x), keySelector(y)));
        }        
    }
}
