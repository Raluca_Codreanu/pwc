﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    ///  Stores the cost calculation results for multiple RawMaterials.
    /// </summary>
    public class RawMaterialsCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialsCost"/> class.
        /// </summary>
        public RawMaterialsCost()
        {
            this.RawMaterialCosts = new Collection<RawMaterialCost>();
        }

        /// <summary>
        /// Gets or sets the net cost sum (sum of costs without overhead).
        /// </summary>        
        public decimal NetCostSum { get; set; }

        /// <summary>
        /// Gets or sets the reject cost sum.
        /// </summary>        
        public decimal RejectCostSum { get; set; }

        /// <summary>
        /// Gets or sets the overhead costs sum.
        /// </summary>        
        public decimal OverheadSum { get; set; }

        /// <summary>
        /// Gets or sets the WIP (Work in Progress) costs sum.
        /// </summary>        
        public decimal WIPCostSum { get; set; }

        /// <summary>
        /// Gets or sets the total costs sum.
        /// </summary>       
        public decimal TotalCostSum { get; set; }

        /// <summary>
        /// Gets the raw material costs list.
        /// </summary>        
        public Collection<RawMaterialCost> RawMaterialCosts { get; private set; }

        /// <summary>
        /// Adds the specified raw material cost to the raw materials costs list.
        /// </summary>
        /// <param name="materialCost">The raw material cost.</param>
        public void AddCost(RawMaterialCost materialCost)
        {
            if (materialCost == null)
            {
                throw new ArgumentNullException("materialCost", "The material cost to add was null.");
            }

            this.NetCostSum += materialCost.NetCost;
            this.RejectCostSum += materialCost.RejectCost;
            this.OverheadSum += materialCost.Overhead;
            this.WIPCostSum += materialCost.WIPCost;
            this.TotalCostSum += materialCost.TotalCost;

            this.RawMaterialCosts.Add(materialCost);
        }

        /// <summary>
        /// Adds the specified raw material costs to the raw material costs list.
        /// </summary>
        /// <param name="materialCosts">The consumable costs.</param>
        public void AddCosts(IEnumerable<RawMaterialCost> materialCosts)
        {
            if (materialCosts == null)
            {
                throw new ArgumentNullException("materialCosts", "The raw material costs to add was null.");
            }

            foreach (RawMaterialCost cost in materialCosts)
            {
                AddCost(cost);
            }
        }

        /// <summary>
        /// Adds the specified cost to this instance and saves the result in this instance.
        /// Equivalent to this += cost.
        /// </summary>
        /// <param name="cost">The cost to add to his instance.</param>
        public void Add(RawMaterialsCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The raw materials cost to sum with was null.");
            }

            AddCosts(cost.RawMaterialCosts);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public RawMaterialsCost Clone()
        {
            // Copy the value members
            RawMaterialsCost clone = (RawMaterialsCost)this.MemberwiseClone();

            // Clone the reference members
            clone.RawMaterialCosts = new Collection<RawMaterialCost>();
            if (this.RawMaterialCosts != null)
            {
                foreach (RawMaterialCost cost in this.RawMaterialCosts)
                {
                    clone.RawMaterialCosts.Add(cost.Clone());
                }
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.NetCostSum *= conversionFactors.CurrencyFactor;
            this.RejectCostSum *= conversionFactors.CurrencyFactor;
            this.OverheadSum *= conversionFactors.CurrencyFactor;
            this.WIPCostSum *= conversionFactors.CurrencyFactor;
            this.TotalCostSum *= conversionFactors.CurrencyFactor;

            if (this.RawMaterialCosts != null)
            {
                foreach (RawMaterialCost cost in this.RawMaterialCosts)
                {
                    cost.ApplyUnitConversion(conversionFactors);
                }
            }
        }
    }
}
