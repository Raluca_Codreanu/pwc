﻿using System;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the cost calculation results for a Part.
    /// </summary>
    public class PartCost
    {
        /// <summary>
        /// Gets or sets the part's id.
        /// </summary>        
        public Guid PartId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>        
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>        
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the index of the part whose cost this instance represents.
        /// </summary>        
        public int? PartIndex { get; set; }

        /// <summary>
        /// Gets or sets the target price of the part. This value is not calculated, it is taken from the Part object.
        /// </summary>        
        public decimal? TargetPrice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this cost is of a raw part.        
        /// </summary>        
        public bool IsRawPart { get; set; }

        /// <summary>
        /// Gets or sets the amount per assembly.
        /// </summary>        
        public int AmountPerAssembly { get; set; }

        /// <summary>
        /// Gets or sets the total cost of the part.
        /// </summary>        
        public decimal TargetCost { get; set; }

        /// <summary>
        /// Gets or sets the full calculation result for the part.
        /// </summary>
        public CalculationResult FullCalculationResult { get; set; }

        /// <summary>
        /// Gets or sets the external overhead cost.
        /// This cost is set only if the part is external and represents the overhead added to the assembly that uses this part, not to the part itself.
        /// Represents the overhead for 1 part.
        /// </summary>        
        public decimal ExternalOverhead { get; set; }

        /// <summary>
        /// Gets or sets the external margin cost.
        /// This cost is set only if the part is external and represents the margin added to the assembly that uses this part, not to the part itself.
        /// Represents the margin for 1 part.
        /// </summary>        
        public decimal ExternalMargin { get; set; }

        /// <summary>
        /// Gets or sets the sales and administration overhead on external parts.
        /// This cost is set only if the part is external and represents the sales and administration overhead added
        /// to the parent assembly that uses this part, not to the part itself.
        /// Represents the overhead for 1 assembly.
        /// </summary>
        public decimal ExternalSGA { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public PartCost Clone()
        {
            // Copy the value members
            PartCost clone = (PartCost)this.MemberwiseClone();

            // Clone the reference members
            if (this.FullCalculationResult != null)
            {
                clone.FullCalculationResult = this.FullCalculationResult.Clone();
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.ExternalMargin *= conversionFactors.CurrencyFactor;
            this.ExternalOverhead *= conversionFactors.CurrencyFactor;
            this.ExternalSGA *= conversionFactors.CurrencyFactor;
            this.TargetCost *= conversionFactors.CurrencyFactor;
            this.TargetPrice *= conversionFactors.CurrencyFactor;

            if (this.FullCalculationResult != null)
            {
                this.FullCalculationResult.ApplyUnitConversion(conversionFactors);
            }
        }
    }
}
