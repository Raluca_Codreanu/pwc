﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the cost breakdown for a Die.
    /// </summary>
    public class DieCost
    {
        /// <summary>
        /// Gets or sets the id of the die.
        /// </summary>
        public Guid DieId { get; set; }

        /// <summary>
        /// Gets or sets the name of the die.
        /// </summary>        
        public string DieName { get; set; }

        /// <summary>
        /// Gets or sets the calculated number of dies per life time.
        /// </summary>
        public decimal DiesPerLifeTime { get; set; }

        /// <summary>
        /// Gets or sets the cost of the die (without maintenance).
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Gets or sets the die's maintenance cost.
        /// </summary>
        public decimal MaintenanceCost { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public DieCost Clone()
        {
            // Copy the value members
            DieCost clone = (DieCost)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.MaintenanceCost *= conversionFactors.CurrencyFactor;
            this.Cost *= conversionFactors.CurrencyFactor;
        }
    }
}
