﻿using System;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores calculation results for RawMaterial
    /// </summary>
    public class RawMaterialCost
    {
        /// <summary>
        /// Gets or sets the id of the material.
        /// </summary>        
        public Guid RawMaterialId { get; set; }

        /// <summary>
        /// Gets or sets the name of the material.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this cost is of a raw part.        
        /// </summary>        
        public bool IsRawPart { get; set; }

        /// <summary>
        /// Gets or sets the price of the material.
        /// </summary>        
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the material weight.
        /// </summary>        
        public decimal MaterialWeight { get; set; }

        /// <summary>
        /// Gets or sets the part weight.
        /// </summary>        
        public decimal PartWeight { get; set; }

        /// <summary>
        /// Gets or sets the base material cost.
        /// </summary>
        public decimal BaseCost { get; set; }

        /// <summary>
        /// Gets or sets the scrap cost.
        /// </summary>
        public decimal ScrapCost { get; set; }

        /// <summary>
        /// Gets or sets the scrap refund cost for the material. For Yield refund this value is negative, suggesting that it is subtracted from the material cost
        /// and for Dispose is positive because it is is added to the material cost.
        /// </summary>
        public decimal ScrapRefund { get; set; }

        /// <summary>
        /// Gets or sets the loss cost.
        /// </summary>
        public decimal LossCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost.
        /// </summary>
        public decimal RejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost calculated without including the scrap refund. This cost is used during the enhanced scrap calculation.
        /// </summary>
        public decimal RejectCostWithoutScrapRefund { get; set; }

        /// <summary>
        /// Gets or sets the net material cost, which includes the reject cost.
        /// </summary>
        public decimal NetCost { get; set; }

        /// <summary>
        /// Gets or sets the overhead cost.
        /// </summary>        
        public decimal Overhead { get; set; }

        /// <summary>
        /// Gets or sets the cost for "Work in Progress".
        /// </summary>        
        public decimal WIPCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>        
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the measurement unit for the Amount.
        /// </summary>
        public string WeightUnit { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public RawMaterialCost Clone()
        {
            // Copy the value members
            RawMaterialCost clone = (RawMaterialCost)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.NetCost *= conversionFactors.CurrencyFactor;
            this.Overhead *= conversionFactors.CurrencyFactor;
            this.Price *= conversionFactors.CurrencyFactor;
            this.TotalCost *= conversionFactors.CurrencyFactor;
            this.BaseCost *= conversionFactors.CurrencyFactor;
            this.ScrapCost *= conversionFactors.CurrencyFactor;
            this.ScrapRefund *= conversionFactors.CurrencyFactor;
            this.LossCost *= conversionFactors.CurrencyFactor;
            this.RejectCost *= conversionFactors.CurrencyFactor;
            this.RejectCostWithoutScrapRefund *= conversionFactors.CurrencyFactor;
            this.WIPCost *= conversionFactors.CurrencyFactor;

            this.MaterialWeight /= conversionFactors.WeightFactor;
            this.PartWeight /= conversionFactors.WeightFactor;
        }
    }
}
