﻿using System;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the cost calculation result for a Consumable object.
    /// </summary>
    public class ConsumableCost
    {        
        /// <summary>
        /// Gets or sets the consumable's id.
        /// </summary>        
        public Guid ConsumableId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>        
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer.
        /// </summary>        
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>        
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the measurement unit in which the Amount is expressed.
        /// </summary>
        public MeasurementUnit AmountUnit { get; set; }

        /// <summary>
        /// Gets or sets the symbol of the base unit of the scale to which the Amount's unit belongs.
        /// </summary>        
        public string AmountUnitBaseSymbol { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>        
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Gets or sets the overhead cost.
        /// </summary>        
        public decimal Overhead { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>        
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ConsumableCost Clone()
        {
            // Copy the value members
            ConsumableCost clone = (ConsumableCost)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.Cost *= conversionFactors.CurrencyFactor;
            this.Overhead *= conversionFactors.CurrencyFactor;
            this.Price *= conversionFactors.CurrencyFactor;
            this.TotalCost *= conversionFactors.CurrencyFactor;
        }
    }
}
