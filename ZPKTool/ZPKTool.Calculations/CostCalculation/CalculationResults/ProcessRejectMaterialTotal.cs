﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Holds the values for the Material Totals in the Reject Overview (Parts, Raw Materials, Commodities and Raw Parts totals).
    /// </summary>
    public class ProcessRejectMaterialTotal
    {
        /// <summary>
        /// Gets or sets the ratio of the reject cost in the total cost.
        /// </summary>                
        public decimal RatioOfRejectCostInTotalCost { get; set; }

        /// <summary>
        /// Gets or sets the material cost per piece.
        /// </summary>                
        public decimal MaterialCostPerPiece { get; set; }

        /// <summary>
        /// Gets or sets the reject material cost per piece.
        /// </summary>                
        public decimal RejectMaterialCostPerPiece { get; set; }

        /// <summary>
        /// Gets or sets the total reject material cost (per gross batch).
        /// </summary>                
        public decimal TotalRejectMaterialCost { get; set; }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.MaterialCostPerPiece *= conversionFactors.CurrencyFactor;
            this.RejectMaterialCostPerPiece *= conversionFactors.CurrencyFactor;
            this.TotalRejectMaterialCost *= conversionFactors.CurrencyFactor;
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ProcessRejectMaterialTotal Clone()
        {
            // Copy the value members
            ProcessRejectMaterialTotal clone = (ProcessRejectMaterialTotal)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }
    }
}
