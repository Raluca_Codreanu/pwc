﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the costs of all sub-assemblies and sub-parts used in an assembling process.
    /// </summary>
    public class SubPartsAndSubAssembliesCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubPartsAndSubAssembliesCost"/> class.
        /// </summary>
        public SubPartsAndSubAssembliesCost()
        {
            this.SubAssembliesCost = new AssembliesCost();
            this.SubPartsCost = new PartsCost();
            this.InvestmentCost = new InvestmentCost();
        }

        /// <summary>
        /// Gets or sets the sub-assemblies cost.
        /// </summary>        
        public AssembliesCost SubAssembliesCost { get; set; }

        /// <summary>
        /// Gets or sets the sub-parts cost.
        /// </summary>        
        public PartsCost SubPartsCost { get; set; }

        /// <summary>
        /// Gets or sets the investment cost of all sub-parts and sub-assemblies.
        /// </summary>
        public InvestmentCost InvestmentCost { get; set; }

        /// <summary>
        /// Gets or sets the overhead on external sub-parts and sub-assemblies.
        /// </summary>        
        public decimal ExternalOverhead { get; set; }

        /// <summary>
        /// Gets or sets the margin on the external sub-parts and sub-assemblies.
        /// </summary>
        public decimal ExternalMargin { get; set; }

        /// <summary>
        /// Gets or sets the sales and administration overhead on external sub-parts and sub-assemblies.
        /// </summary>
        public decimal ExternalSGA { get; set; }

        /// <summary>
        /// Gets the total cost of all sub-parts and sub-assemblies.
        /// </summary>        
        public decimal TotalCost
        {
            get
            {
                return this.SubAssembliesCost.AssemblyCostsSum + this.SubPartsCost.PartCostsSum;
            }
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public SubPartsAndSubAssembliesCost Clone()
        {
            // Copy the value members
            SubPartsAndSubAssembliesCost clone = (SubPartsAndSubAssembliesCost)this.MemberwiseClone();

            // Clone the reference members
            if (this.SubAssembliesCost != null)
            {
                clone.SubAssembliesCost = this.SubAssembliesCost.Clone();
            }

            if (this.SubPartsCost != null)
            {
                clone.SubPartsCost = this.SubPartsCost.Clone();
            }

            if (this.InvestmentCost != null)
            {
                clone.InvestmentCost = this.InvestmentCost.Clone();
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.ExternalMargin *= conversionFactors.CurrencyFactor;
            this.ExternalOverhead *= conversionFactors.CurrencyFactor;
            this.ExternalSGA *= conversionFactors.CurrencyFactor;

            if (this.InvestmentCost != null)
            {
                this.InvestmentCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.SubAssembliesCost != null)
            {
                this.SubAssembliesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.SubPartsCost != null)
            {
                this.SubPartsCost.ApplyUnitConversion(conversionFactors);
            }
        }
    }
}
