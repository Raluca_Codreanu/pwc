﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the cost calculation results for multiple Parts.
    /// </summary>
    public class PartsCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartsCost"/> class.
        /// </summary>
        public PartsCost()
        {
            this.PartCosts = new Collection<PartCost>();
        }

        /// <summary>
        /// Gets or sets the sum of the cost of the stored parts.
        /// </summary>        
        public decimal PartCostsSum { get; set; }

        /// <summary>
        /// Gets or sets the sum of amounts of the stored parts.
        /// </summary>
        /// <value>The part amounts sum.</value>
        public int PartAmountsSum { get; set; }

        /// <summary>
        /// Gets a collection that stores the detailed cost of the parts.
        /// </summary>        
        public Collection<PartCost> PartCosts { get; private set; }

        /// <summary>
        /// Adds the specified part cost to the parts costs list.
        /// </summary>
        /// <param name="cost">The part cost.</param>
        public void AddCost(PartCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The part cost to add was null.");
            }

            this.PartAmountsSum += cost.AmountPerAssembly;
            this.PartCostsSum += cost.AmountPerAssembly * cost.TargetCost;

            this.PartCosts.Add(cost);
        }

        /// <summary>
        /// Adds the specified part costs to the parts costs list.
        /// </summary>
        /// <param name="costs">The part costs.</param>
        public void AddCosts(IEnumerable<PartCost> costs)
        {
            if (costs == null)
            {
                throw new ArgumentNullException("costs", "The part costs to add was null.");
            }

            foreach (PartCost cost in costs)
            {
                AddCost(cost);
            }
        }

        /// <summary>
        /// Adds the specified cost to this instance and saves the result in this instance.
        /// Equivalent to "this += costToAdd".
        /// </summary>
        /// <param name="costToAdd">The cost to add to his instance.</param>
        public void SumWith(PartsCost costToAdd)
        {
            if (costToAdd == null)
            {
                throw new ArgumentNullException("costToAdd", "The parts cost to sum with was null.");
            }

            AddCosts(costToAdd.PartCosts);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public PartsCost Clone()
        {
            // Copy the value members
            PartsCost clone = (PartsCost)this.MemberwiseClone();

            // Clone the reference members
            clone.PartCosts = new Collection<PartCost>();
            if (this.PartCosts != null)
            {
                foreach (PartCost cost in this.PartCosts)
                {
                    clone.PartCosts.Add(cost.Clone());
                }
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.PartCostsSum *= conversionFactors.CurrencyFactor;

            if (this.PartCosts != null)
            {
                foreach (PartCost cost in this.PartCosts)
                {
                    cost.ApplyUnitConversion(conversionFactors);
                }
            }
        }
    }
}
