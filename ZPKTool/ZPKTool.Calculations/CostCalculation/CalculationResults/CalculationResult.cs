﻿using System;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the calculation result for entities
    /// </summary>
    public class CalculationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationResult"/> class.
        /// </summary>        
        public CalculationResult()
        {
            this.Summary = new CalculationResultSummary();
            this.OverheadCost = new OverheadCost();
            this.ProcessCost = new ProcessCost();
            this.CommoditiesCost = new CommoditiesCost();
            this.ConsumablesCost = new ConsumablesCost();
            this.RawMaterialsCost = new RawMaterialsCost();
            this.PartsCost = new PartsCost();
            this.AssembliesCost = new AssembliesCost();
            this.InvestmentCost = new InvestmentCost();
        }

        #region Information not related to costs

        /// <summary>
        /// Gets or sets the part number of the entity.
        /// </summary>
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the entity.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the remarks associated with the entity.
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets the purchase price associated with the entity.
        /// </summary>
        public decimal? PurchasePrice { get; set; }

        /// <summary>
        /// Gets or sets the target price associated with the entity.
        /// </summary>
        public decimal? TargetPrice { get; set; }

        /// <summary>
        /// Gets a value indicating whether the cost of the entity is estimated.
        /// </summary>
        public bool Estimated
        {
            get { return this.CalculationAccuracy != PartCalculationAccuracy.FineCalculation; }
        }

        /// <summary>
        /// Gets or sets the accuracy used to calculate this result.
        /// </summary>        
        public PartCalculationAccuracy CalculationAccuracy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the the entity is external.
        /// </summary>
        public bool External { get; set; }

        /// <summary>
        /// Gets or sets the version of the cost calculator that performed this calculation.
        /// </summary>        
        public string CalculatorVersion { get; set; }

        /// <summary>
        /// Gets or sets the calculated weight of the entity.
        /// </summary>        
        public decimal Weight { get; set; }

        #endregion Information not related to costs

        #region Cost information

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>        
        public CalculationResultSummary Summary { get; set; }

        /// <summary>
        /// Gets or sets the overhead cost.
        /// </summary>        
        public OverheadCost OverheadCost { get; set; }

        /// <summary>
        /// Gets or sets the process cost.
        /// </summary>
        public ProcessCost ProcessCost { get; set; }

        /// <summary>
        /// Gets or sets the commodities cost.
        /// </summary>
        public CommoditiesCost CommoditiesCost { get; set; }

        /// <summary>
        /// Gets or sets the consumables cost.
        /// </summary>
        public ConsumablesCost ConsumablesCost { get; set; }

        /// <summary>
        /// Gets or sets the raw materials cost.
        /// </summary>
        public RawMaterialsCost RawMaterialsCost { get; set; }

        /// <summary>
        /// Gets or sets the parts cost.
        /// </summary>
        public PartsCost PartsCost { get; set; }

        /// <summary>
        /// Gets or sets the assemblies cost.
        /// </summary>
        public AssembliesCost AssembliesCost { get; set; }

        /// <summary>
        /// Gets or sets the investment cost.
        /// </summary>
        public InvestmentCost InvestmentCost { get; set; }

        /// <summary>
        /// Gets or sets the cumulated investment cost.
        /// This cost represents the total investment cost sum for the entire assembly/part hierarchy, not just for the assembly/part whose cost this is.
        /// </summary>        
        public decimal InvestmentCostCumulated { get; set; }

        /// <summary>
        /// Gets or sets the production cost.
        /// </summary>        
        public decimal ProductionCost { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost.
        /// </summary>
        public decimal LogisticCost { get; set; }

        /// <summary>
        /// Gets or sets the transport cost.
        /// </summary>        
        public decimal TransportCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost of sub-parts and sub-assemblies used in the assembling process of the assembly for which this calculation was done.
        /// </summary>
        public decimal SubpartsAndSubassembliesCost { get; set; }

        /// <summary>
        /// Gets or sets the development cost.
        /// </summary>        
        public decimal DevelopmentCost { get; set; }

        /// <summary>
        /// Gets or sets the project invest.
        /// </summary>        
        public decimal ProjectInvest { get; set; }

        /// <summary>
        /// Gets or sets the other cost.
        /// </summary>
        public decimal OtherCost { get; set; }

        /// <summary>
        /// Gets or sets the packaging cost.
        /// </summary>        
        public decimal PackagingCost { get; set; }

        /// <summary>
        /// Gets or sets the payment cost.
        /// </summary>
        public decimal PaymentCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the additional costs notes.
        /// </summary>
        public string AdditionalCostsNotes { get; set; }

        #endregion Cost information

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public CalculationResult Clone()
        {
            // Copy the value members
            CalculationResult clone = (CalculationResult)this.MemberwiseClone();

            // Clone the reference members
            if (this.Summary != null)
            {
                clone.Summary = this.Summary.Clone();
            }

            if (this.OverheadCost != null)
            {
                clone.OverheadCost = this.OverheadCost.Clone();
            }

            if (this.ProcessCost != null)
            {
                clone.ProcessCost = this.ProcessCost.Clone();
            }

            if (this.CommoditiesCost != null)
            {
                clone.CommoditiesCost = this.CommoditiesCost.Clone();
            }

            if (this.ConsumablesCost != null)
            {
                clone.ConsumablesCost = this.ConsumablesCost.Clone();
            }

            if (this.RawMaterialsCost != null)
            {
                clone.RawMaterialsCost = this.RawMaterialsCost.Clone();
            }

            if (this.PartsCost != null)
            {
                clone.PartsCost = this.PartsCost.Clone();
            }

            if (this.AssembliesCost != null)
            {
                clone.AssembliesCost = this.AssembliesCost.Clone();
            }

            if (this.InvestmentCost != null)
            {
                clone.InvestmentCost = this.InvestmentCost.Clone();
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.DevelopmentCost *= conversionFactors.CurrencyFactor;
            this.LogisticCost *= conversionFactors.CurrencyFactor;
            this.TransportCost *= conversionFactors.CurrencyFactor;
            this.OtherCost *= conversionFactors.CurrencyFactor;
            this.PackagingCost *= conversionFactors.CurrencyFactor;
            this.PaymentCost *= conversionFactors.CurrencyFactor;
            this.ProductionCost *= conversionFactors.CurrencyFactor;
            this.ProjectInvest *= conversionFactors.CurrencyFactor;
            this.SubpartsAndSubassembliesCost *= conversionFactors.CurrencyFactor;
            this.TotalCost *= conversionFactors.CurrencyFactor;
            this.InvestmentCostCumulated *= conversionFactors.CurrencyFactor;

            if (conversionFactors.WeightFactor != 0)
            {
                this.Weight /= conversionFactors.WeightFactor;
            }

            if (this.AssembliesCost != null)
            {
                this.AssembliesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.CommoditiesCost != null)
            {
                this.CommoditiesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.ConsumablesCost != null)
            {
                this.ConsumablesCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.InvestmentCost != null)
            {
                this.InvestmentCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.OverheadCost != null)
            {
                this.OverheadCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.PartsCost != null)
            {
                this.PartsCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.ProcessCost != null)
            {
                this.ProcessCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.RawMaterialsCost != null)
            {
                this.RawMaterialsCost.ApplyUnitConversion(conversionFactors);
            }

            if (this.Summary != null)
            {
                this.Summary.ApplyUnitConversion(conversionFactors);
            }
        }
    }
}
