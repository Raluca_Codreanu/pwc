﻿using System;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the calculation results for the commodity entity
    /// </summary>
    public class CommodityCost
    {
        /// <summary>
        /// Gets or sets the id of the commodity.
        /// </summary>        
        public Guid CommodityId { get; set; }

        /// <summary>
        /// Gets or sets the commodity's name.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the commodity's manufacturer.
        /// </summary>        
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the commodity's remarks.
        /// </summary>        
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the reject ratio of the commodity.
        /// </summary>        
        public decimal RejectRatio { get; set; }

        /// <summary>
        /// Gets or sets the weight of the commodity.
        /// </summary>
        public decimal? Weight { get; set; }

        /// <summary>
        /// Gets or sets the commodity amount.
        /// </summary>        
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the price for 1 unit of the commodity.
        /// </summary>        
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the reject cost of 1 piece of this commodity.
        /// </summary>        
        public decimal RejectCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of the commodity, including reject cost (does not include the overhead).
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Gets or sets the overhead cost.
        /// </summary>        
        public decimal Overhead { get; set; }

        /// <summary>
        /// Gets or sets the margin cost.
        /// </summary>        
        public decimal Margin { get; set; }

        /// <summary>
        /// Gets or sets the total cost of the commodity.
        /// </summary>        
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the overhead ratio used to calculate the overhead cost.
        /// </summary>        
        public decimal OverheadRatio { get; set; }

        /// <summary>
        /// Gets or sets the margin ratio used to calculate the margin cost.
        /// </summary>        
        public decimal MarginRatio { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public CommodityCost Clone()
        {
            // Copy the value members
            CommodityCost clone = (CommodityCost)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.Cost *= conversionFactors.CurrencyFactor;
            this.Margin *= conversionFactors.CurrencyFactor;
            this.Overhead *= conversionFactors.CurrencyFactor;
            this.Price *= conversionFactors.CurrencyFactor;
            this.TotalCost *= conversionFactors.CurrencyFactor;
            this.RejectCost *= conversionFactors.CurrencyFactor;

            this.Weight /= conversionFactors.WeightFactor;
        }
    }
}
