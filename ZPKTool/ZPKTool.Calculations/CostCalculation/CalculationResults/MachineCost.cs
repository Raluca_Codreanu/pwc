﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the cost calculation results for a Machine.
    /// </summary>
    public class MachineCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MachineCost"/> class.
        /// </summary>
        public MachineCost()
        {
        }

        /// <summary>
        /// Gets or sets the id of the machine.
        /// </summary>        
        public Guid MachineId { get; set; }

        /// <summary>
        /// Gets or sets the name of the machine.
        /// </summary>        
        public string MachineName { get; set; }

        /// <summary>
        /// Gets or sets the floor cost.
        /// </summary>
        public decimal FloorCost { get; set; }

        /// <summary>
        /// Gets or sets the depreciation per hour.
        /// </summary>
        /// <value>The depreciation per hour.</value>
        public decimal DepreciationPerHour { get; set; }

        /// <summary>
        /// Gets or sets the maintenance per hour.
        /// </summary>
        /// <value>The maintenance per hour.</value>
        public decimal MaintenancePerHour { get; set; }

        /// <summary>
        /// Gets or sets the energy cost per hour.
        /// </summary>
        /// <value>The energy cost per hour.</value>
        public decimal EnergyCostPerHour { get; set; }

        /// <summary>
        /// Gets or sets the full (total) cost per hour.
        /// </summary>
        /// <value>The full cost per hour.</value>
        public decimal FullCostPerHour { get; set; }

        /// <summary>
        /// Gets or sets the calculated capacity utilization of the machine.
        /// </summary>        
        public decimal CapacityUtilization { get; set; }

        /// <summary>
        /// Gets or sets the machine's calculated consumables cost.
        /// This value is calculated only when the consumables cost is inputted as a percentage; otherwise its value is zero.
        /// </summary>
        public decimal CalculatedConsumablesCost { get; set; }

        /// <summary>
        /// Gets or sets the machine maintenance notes.
        /// </summary>
        public string MaintenanceNotes { get; set; }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public MachineCost Clone()
        {
            // Copy the value members
            MachineCost clone = (MachineCost)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.DepreciationPerHour *= conversionFactors.CurrencyFactor;
            this.EnergyCostPerHour *= conversionFactors.CurrencyFactor;
            this.FloorCost *= conversionFactors.CurrencyFactor;
            this.FullCostPerHour *= conversionFactors.CurrencyFactor;
            this.MaintenancePerHour *= conversionFactors.CurrencyFactor;
        }
    }
}
