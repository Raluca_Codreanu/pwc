﻿using System;
using System.Collections.ObjectModel;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the cost calculation for a process step
    /// </summary>
    public class ProcessStepCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepCost"/> class.
        /// </summary>
        public ProcessStepCost()
        {
            this.InvestmentCost = new InvestmentCost();
            this.MachineCosts = new Collection<MachineCost>();
            this.DieCosts = new Collection<DieCost>();
        }

        #region Parent information properties

        /// <summary>
        /// Gets or sets the id of the process step.
        /// </summary>
        /// <value>The guid of the object.</value>
        public Guid StepId { get; set; }

        /// <summary>
        /// Gets or sets the name of the step.
        /// </summary>
        public string StepName { get; set; }

        /// <summary>
        /// Gets or sets the index of the step (the Index property value of a ProcessStep).
        /// </summary>
        public int StepIndex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the step is external.
        /// </summary>        
        public bool IsExternal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SBM is active in the parent part/assembly.
        /// </summary>        
        public bool SBMActive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the parent part/assembly is external.
        /// </summary>
        /// <value>
        /// true if the parent part/assembly is external; otherwise, false.
        /// </value>
        public bool ParentPartIsExternal { get; set; }

        #endregion Parent information properties

        #region Cost properties

        /// <summary>
        /// Gets or sets the total cost of the step's machines. This value is based on the individual cost of each machine (from the MachineCosts collection)
        /// but it is not the sum of those costs, it is calculated based on process step data.
        /// </summary>
        public decimal MachineCost { get; set; }

        /// <summary>
        /// Gets or sets the setup cost.
        /// </summary>
        public decimal SetupCost { get; set; }

        /// <summary>
        /// Gets or sets the direct labor cost.
        /// </summary>
        public decimal DirectLabourCost { get; set; }

        /// <summary>
        /// Gets or sets the maintenance cost of all dies used in the step.
        /// </summary>
        public decimal DiesMaintenanceCost { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing cost.
        /// </summary>
        public decimal ManufacturingCost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the manufacturing cost is estimated.
        /// </summary>
        public bool ManufacturingCostIsEstimated { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing overhead cost.
        /// </summary>        
        public decimal ManufacturingOverheadCost { get; set; }

        /// <summary>
        /// Gets or sets the overhead rate used to calculate the ManufacturingOverheadCost. This value represents a percentage.
        /// </summary>        
        public decimal ManufacturingOverheadRate { get; set; }

        /// <summary>
        /// Gets or sets the total reject cost for the step (Reject Process Cost Per Piece).
        /// </summary>        
        public decimal RejectCost { get; set; }

        /// <summary>
        /// Gets or sets the transport cost.
        /// </summary>        
        public decimal TransportCost { get; set; }

        /// <summary>
        /// Gets the total manufacturing cost (including OH and scrap).
        /// </summary>        
        public decimal TotalManufacturingCost
        {
            get { return this.ManufacturingCost + this.ManufacturingOverheadCost + this.RejectCost + this.TransportCost; }
        }

        /// <summary>
        /// Gets or sets the die cost.
        /// </summary>
        /// <value>The die cost.</value>
        public decimal ToolAndDieCost { get; set; }

        /// <summary>
        /// Gets the total labor cost (setup labor + direct labor cost).
        /// </summary>
        public decimal TotalLabourCost
        {
            get
            {
                return this.SetupCost + this.DirectLabourCost;
            }
        }

        /// <summary>
        /// Gets or sets the investment cost for the step's machines and dies.
        /// </summary>
        public InvestmentCost InvestmentCost { get; set; }

        /// <summary>
        /// Gets a collection containing the individual cost breakdown of each machine in the step.
        /// </summary>
        public Collection<MachineCost> MachineCosts { get; private set; }

        /// <summary>
        /// Gets a collection containing the individual cost breakdown of each die in the step.
        /// </summary>
        public Collection<DieCost> DieCosts { get; private set; }

        #endregion Cost properties

        #region Enhanced Scrap/Reject related properties

        /// <summary>
        /// Gets or sets the batch size that is necessary to be used in order to produce the target batch size (includes the rejected number of parts).
        /// </summary>
        public decimal GrossBatchSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the batch that must result from the step.
        /// </summary>
        public decimal NetBatchSize { get; set; }

        /// <summary>
        /// Gets or sets the number of rejected parts in this step.
        /// </summary>        
        public decimal RejectedPieces { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing cost for the rejected parts in this step.
        /// </summary>
        public decimal RejectManufacturingCostPerPiece { get; set; }

        /// <summary>
        /// Gets or sets the scrap cost for the rejected parts in this step.
        /// </summary>
        public decimal RejectMaterialCostPerPiece { get; set; }

        /// <summary>
        /// Gets or sets the total reject process cost per part for this step and all previous steps
        /// (for step n is RejectProcessCostPerPieceCumulated[n-1] + RejectProcessCostPerPiece[n]).
        /// </summary>        
        public decimal RejectProcessCostPerPieceCumulated { get; set; }

        /// <summary>
        /// Gets or sets the total reject process cost.
        /// </summary>        
        public decimal RejectProcessCostTotal { get; set; }

        #endregion Enhanced Scrap/Reject related properties

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ProcessStepCost Clone()
        {
            // Copy the value members
            ProcessStepCost clone = (ProcessStepCost)this.MemberwiseClone();

            // Clone the reference members
            clone.InvestmentCost = this.InvestmentCost.Clone();

            clone.MachineCosts = new Collection<MachineCost>();
            foreach (var cost in this.MachineCosts)
            {
                clone.MachineCosts.Add(cost.Clone());
            }

            clone.DieCosts = new Collection<DieCost>();
            foreach (var cost in this.DieCosts)
            {
                clone.DieCosts.Add(cost.Clone());
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.DiesMaintenanceCost *= conversionFactors.CurrencyFactor;
            this.DirectLabourCost *= conversionFactors.CurrencyFactor;
            this.MachineCost *= conversionFactors.CurrencyFactor;
            this.ManufacturingCost *= conversionFactors.CurrencyFactor;
            this.ManufacturingOverheadCost *= conversionFactors.CurrencyFactor;
            this.RejectCost *= conversionFactors.CurrencyFactor;
            this.SetupCost *= conversionFactors.CurrencyFactor;
            this.ToolAndDieCost *= conversionFactors.CurrencyFactor;
            this.RejectMaterialCostPerPiece *= conversionFactors.CurrencyFactor;
            this.RejectManufacturingCostPerPiece *= conversionFactors.CurrencyFactor;
            this.TransportCost *= conversionFactors.CurrencyFactor;
            this.RejectProcessCostPerPieceCumulated *= conversionFactors.CurrencyFactor;
            this.RejectProcessCostTotal *= conversionFactors.CurrencyFactor;

            this.InvestmentCost.ApplyUnitConversion(conversionFactors);

            foreach (var cost in this.MachineCosts)
            {
                cost.ApplyUnitConversion(conversionFactors);
            }

            foreach (var cost in this.DieCosts)
            {
                cost.ApplyUnitConversion(conversionFactors);
            }
        }
    }
}