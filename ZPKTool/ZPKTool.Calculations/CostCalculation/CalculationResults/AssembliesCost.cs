﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that aggregates the cost calculation results for multiple assemblies, usually the sub-assemblies of a specific assembly.
    /// </summary>
    public class AssembliesCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssembliesCost"/> class.
        /// </summary>
        public AssembliesCost()
        {
            this.AssemblyCosts = new Collection<AssemblyCost>();
        }

        /// <summary>
        /// Gets or sets the assembly costs sum.
        /// </summary>        
        public decimal AssemblyCostsSum { get; set; }

        /// <summary>
        /// Gets or sets the assembly amounts sum.
        /// </summary>        
        public int AssemblyAmountsSum { get; set; }

        /// <summary>
        /// Gets a collection that contains the detailed cost of each assembly.
        /// </summary>        
        public Collection<AssemblyCost> AssemblyCosts { get; private set; }

        /// <summary>
        /// Adds the specified assembly cost to the assemblies costs list.
        /// </summary>
        /// <param name="cost">The assembly cost.</param>
        public void AddCost(AssemblyCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The assembly cost to add was null.");
            }

            this.AssemblyAmountsSum += cost.AmountPerAssembly;
            this.AssemblyCostsSum += cost.AmountPerAssembly * cost.TargetCost;

            this.AssemblyCosts.Add(cost);
        }

        /// <summary>
        /// Adds the specified assembly costs to the assembly costs list.
        /// </summary>
        /// <param name="costs">The assembly costs.</param>
        public void AddCosts(IEnumerable<AssemblyCost> costs)
        {
            if (costs == null)
            {
                throw new ArgumentNullException("costs", "The assembly costs to add was null.");
            }

            foreach (AssemblyCost cost in costs)
            {
                AddCost(cost);
            }
        }

        /// <summary>
        /// Adds the specified cost to this instance and saves the result in this instance.
        /// Equivalent to "this += costToAdd".
        /// </summary>
        /// <param name="costToAdd">The cost to add to his instance.</param>
        public void SumWith(AssembliesCost costToAdd)
        {
            if (costToAdd == null)
            {
                throw new ArgumentNullException("costToAdd", "The assemblies cost to sum with was null.");
            }

            AddCosts(costToAdd.AssemblyCosts);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public AssembliesCost Clone()
        {
            // Copy the value members
            AssembliesCost clone = (AssembliesCost)this.MemberwiseClone();

            // Clone the reference members
            clone.AssemblyCosts = new Collection<AssemblyCost>();
            if (this.AssemblyCosts != null)
            {
                foreach (AssemblyCost cost in this.AssemblyCosts)
                {
                    clone.AssemblyCosts.Add(cost.Clone());
                }
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.AssemblyCostsSum *= conversionFactors.CurrencyFactor;

            if (this.AssemblyCosts != null)
            {
                foreach (AssemblyCost cost in this.AssemblyCosts)
                {
                    cost.ApplyUnitConversion(conversionFactors);
                }
            }
        }
    }
}
