﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Holds the data displayed on the Reject Overview section of the Process' Scrap/Reject View.
    /// </summary>
    public class ProcessRejectOverview
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessRejectOverview"/> class.
        /// </summary>
        public ProcessRejectOverview()
        {
            this.RawMaterialsTotal = new ProcessRejectMaterialTotal();
            this.CommoditiesTotal = new ProcessRejectMaterialTotal();
            this.RawPartsTotal = new ProcessRejectMaterialTotal();
            this.PartsTotal = new ProcessRejectMaterialTotal();
        }

        /// <summary>
        /// Gets or sets the number of pieces that must be produced per year (the target number of pieces).
        /// </summary>            
        public decimal IOPiecesPerYear { get; set; }

        /// <summary>
        /// Gets or sets the number of batches produced per year.
        /// </summary>            
        public decimal BatchesPerYear { get; set; }

        /// <summary>
        /// Gets or sets the net batch size.
        /// </summary>            
        public decimal IOPiecesPerBatch { get; set; }

        /// <summary>
        /// Gets or sets the actual batch size that must be produced in order to remain with the net batch size after the rejected pieces are removed.
        /// </summary>        
        public decimal GrossBatchSize { get; set; }

        /// <summary>
        /// Gets or sets the total number of rejected pieces during the process.
        /// </summary>            
        public decimal RejectedPiecesTotal { get; set; }

        /// <summary>
        /// Gets or sets the reject cost generated by the manufacturing/assembling process.
        /// </summary>            
        public decimal ProcessRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost generated by raw materials.
        /// </summary>            
        public decimal RawMaterialsRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost generated by commodities.
        /// </summary>            
        public decimal CommoditiesRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost generated by raw parts.
        /// </summary>            
        public decimal RawPartsRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost generated by sub-parts and sub-assemblies.
        /// </summary>            
        public decimal PartsRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the reject cost for 1 piece.
        /// </summary>
        public decimal RejectCostForIOPiece { get; set; }

        /// <summary>
        /// Gets or sets the total reject cost for 1 piece.
        /// </summary>            
        public decimal TotalRejectCostForIOPiece { get; set; }

        /// <summary>
        /// Gets or sets the total reject cost.
        /// </summary>            
        public decimal TotalRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost of 1 piece.
        /// </summary>
        public decimal TotalCostOfIOPiece { get; set; }

        /// <summary>
        /// Gets or sets the ratio of the reject cost in the total cost.
        /// </summary>
        public decimal RatioOfRejectCostInTotalCost { get; set; }

        /// <summary>
        /// Gets the data for the Raw Materials section of the Reject Overview's Material Total.
        /// </summary>            
        public ProcessRejectMaterialTotal RawMaterialsTotal { get; private set; }

        /// <summary>
        /// Gets the Commodities section of the Reject Overview's Material Total.
        /// </summary>            
        public ProcessRejectMaterialTotal CommoditiesTotal { get; private set; }

        /// <summary>
        /// Gets the Raw Parts section of the Reject Overview's Material Total.
        /// </summary>            
        public ProcessRejectMaterialTotal RawPartsTotal { get; private set; }

        /// <summary>
        /// Gets the Parts section of the Reject Overview's Material Total.
        /// </summary>            
        public ProcessRejectMaterialTotal PartsTotal { get; private set; }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.ProcessRejectCost *= conversionFactors.CurrencyFactor;
            this.RawMaterialsRejectCost *= conversionFactors.CurrencyFactor;
            this.CommoditiesRejectCost *= conversionFactors.CurrencyFactor;
            this.RawPartsRejectCost *= conversionFactors.CurrencyFactor;
            this.PartsRejectCost *= conversionFactors.CurrencyFactor;
            this.RejectCostForIOPiece *= conversionFactors.CurrencyFactor;
            this.TotalRejectCostForIOPiece *= conversionFactors.CurrencyFactor;
            this.TotalRejectCost *= conversionFactors.CurrencyFactor;
            this.TotalCostOfIOPiece *= conversionFactors.CurrencyFactor;

            this.RawMaterialsTotal.ApplyUnitConversion(conversionFactors);
            this.CommoditiesTotal.ApplyUnitConversion(conversionFactors);
            this.RawPartsTotal.ApplyUnitConversion(conversionFactors);
            this.PartsTotal.ApplyUnitConversion(conversionFactors);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ProcessRejectOverview Clone()
        {
            // Copy the value members
            ProcessRejectOverview clone = (ProcessRejectOverview)this.MemberwiseClone();

            // Clone the reference members
            clone.RawMaterialsTotal = this.RawMaterialsTotal.Clone();
            clone.CommoditiesTotal = this.CommoditiesTotal.Clone();
            clone.RawPartsTotal = this.RawPartsTotal.Clone();
            clone.PartsTotal = this.PartsTotal.Clone();

            return clone;
        }        
    }
}
