﻿using System;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores a summary of the cost calculation result.
    /// </summary>
    public class CalculationResultSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationResultSummary"/> class.
        /// </summary>
        public CalculationResultSummary()
        {
        }

        /// <summary>
        /// Gets or sets the raw material cost.
        /// </summary>
        public decimal RawMaterialCost { get; set; }

        /// <summary>
        /// Gets or sets the WIP cost.
        /// </summary>        
        public decimal WIPCost { get; set; }

        /// <summary>
        /// Gets or sets the consumable cost.
        /// </summary>
        public decimal ConsumableCost { get; set; }

        /// <summary>
        /// Gets or sets the commodities cost.
        /// </summary>
        public decimal CommoditiesCost { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing cost (manufacturing cost is called assembling cost for assemblies).
        /// </summary>
        public decimal TotalManufacturingCost { get; set; }

        /// <summary>
        /// Gets or sets the total reject cost for the assembling/manufacturing process.
        /// </summary>
        public decimal ManufacturingRejectCost { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing transport cost.
        /// </summary>        
        public decimal ManufacturingTransportCost { get; set; }

        /// <summary>
        /// Gets or sets the production cost.
        /// </summary>
        public decimal ProductionCost { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost.
        /// </summary>
        public decimal LogisticCost { get; set; }

        /// <summary>
        /// Gets or sets the transport cost.
        /// </summary>        
        public decimal TransportCost { get; set; }

        /// <summary>
        /// Gets or sets the overhead and margin cost.
        /// </summary>
        public decimal OverheadAndMarginCost { get; set; }

        /// <summary>
        /// Gets or sets the payment cost.
        /// </summary>
        public decimal PaymentCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        public decimal TargetCost { get; set; }

        /// <summary>
        /// Gets or sets the development cost.
        /// </summary>
        public decimal DevelopmentCost { get; set; }

        /// <summary>
        /// Gets or sets the project invest.
        /// </summary>
        public decimal ProjectInvest { get; set; }

        /// <summary>
        /// Gets or sets the other cost.
        /// </summary>
        public decimal OtherCost { get; set; }

        /// <summary>
        /// Gets or sets the packaging cost.
        /// </summary>
        public decimal PackagingCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost of sub-parts and sub-assemblies used in the assembling process of the entity
        /// for which this calculation was done.
        /// </summary>
        public decimal SubpartsAndSubassembliesCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of the sub-parts and sub-assemblies that have the "Offer / External Calculation" accuracy.
        /// It is set only for the enhanced breakdown.
        /// </summary>
        public decimal OfferExternalCalcPartsCost { get; set; }

        /// <summary>
        /// Adds the values in the specified instance to this instance (equivalent to +=).
        /// </summary>
        /// <param name="summary">The instance to add.</param>
        public void Add(CalculationResultSummary summary)
        {
            this.Add(summary, 1);
        }

        /// <summary>
        /// Adds the values multiplied by <paramref name="multiplier"/> value in the specified instance to this instance (equivalent to +=).
        /// </summary>
        /// <param name="summary">The instance to add.</param>
        /// <param name="multiplier">The value to multiply the instance to add</param>
        public void Add(CalculationResultSummary summary, int multiplier)
        {
            if (summary == null)
            {
                throw new ArgumentNullException("summary", "The value to add was null.");
            }

            this.CommoditiesCost += summary.CommoditiesCost * multiplier;
            this.ConsumableCost += summary.ConsumableCost * multiplier;
            this.DevelopmentCost += summary.DevelopmentCost * multiplier;
            this.LogisticCost += summary.LogisticCost * multiplier;
            this.OtherCost += summary.OtherCost * multiplier;
            this.OverheadAndMarginCost += summary.OverheadAndMarginCost * multiplier;
            this.PackagingCost += summary.PackagingCost * multiplier;
            this.PaymentCost += summary.PaymentCost * multiplier;
            this.ProductionCost += summary.ProductionCost * multiplier;
            this.ProjectInvest += summary.ProjectInvest * multiplier;
            this.RawMaterialCost += summary.RawMaterialCost * multiplier;
            this.ManufacturingRejectCost += summary.ManufacturingRejectCost * multiplier;
            this.SubpartsAndSubassembliesCost += summary.SubpartsAndSubassembliesCost * multiplier;
            this.TargetCost += summary.TargetCost * multiplier;
            this.TotalManufacturingCost += summary.TotalManufacturingCost * multiplier;
            this.TransportCost += summary.TransportCost * multiplier;
            this.ManufacturingTransportCost += summary.ManufacturingTransportCost * multiplier;
            this.WIPCost += summary.WIPCost * multiplier;
            this.OfferExternalCalcPartsCost += summary.OfferExternalCalcPartsCost * multiplier;
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public CalculationResultSummary Clone()
        {
            // Copy the value members
            CalculationResultSummary clone = (CalculationResultSummary)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.CommoditiesCost *= conversionFactors.CurrencyFactor;
            this.ConsumableCost *= conversionFactors.CurrencyFactor;
            this.DevelopmentCost *= conversionFactors.CurrencyFactor;
            this.LogisticCost *= conversionFactors.CurrencyFactor;
            this.TransportCost *= conversionFactors.CurrencyFactor;
            this.OtherCost *= conversionFactors.CurrencyFactor;
            this.OverheadAndMarginCost *= conversionFactors.CurrencyFactor;
            this.PackagingCost *= conversionFactors.CurrencyFactor;
            this.PaymentCost *= conversionFactors.CurrencyFactor;
            this.ProductionCost *= conversionFactors.CurrencyFactor;
            this.ProjectInvest *= conversionFactors.CurrencyFactor;
            this.RawMaterialCost *= conversionFactors.CurrencyFactor;
            this.ManufacturingRejectCost *= conversionFactors.CurrencyFactor;
            this.SubpartsAndSubassembliesCost *= conversionFactors.CurrencyFactor;
            this.TargetCost *= conversionFactors.CurrencyFactor;
            this.TotalManufacturingCost *= conversionFactors.CurrencyFactor;
            this.ManufacturingTransportCost *= conversionFactors.CurrencyFactor;
            this.WIPCost *= conversionFactors.CurrencyFactor;
            this.OfferExternalCalcPartsCost *= conversionFactors.CurrencyFactor;
        }
    }
}
