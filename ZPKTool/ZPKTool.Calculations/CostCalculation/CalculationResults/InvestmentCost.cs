﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Stores the total investment cost for a Part or Assembly.
    /// </summary>
    public class InvestmentCost
    {
        /// <summary>
        /// The investment costs of all machines.
        /// </summary>
        private List<InvestmentCostSingle> machineInvestments = new List<InvestmentCostSingle>();

        /// <summary>
        /// The investment costs of all dies.
        /// </summary>
        private List<InvestmentCostSingle> dieInvestments = new List<InvestmentCostSingle>();

        /// <summary>
        /// Initializes a new instance of the <see cref="InvestmentCost"/> class.
        /// </summary>
        public InvestmentCost()
        {
        }

        /// <summary>
        /// Gets the investment costs of all machines.
        /// </summary>
        public ReadOnlyCollection<InvestmentCostSingle> MachineInvestments
        {
            get { return this.machineInvestments.AsReadOnly(); }
        }

        /// <summary>
        /// Gets the investment costs of all dies.
        /// </summary>
        public ReadOnlyCollection<InvestmentCostSingle> DieInvestments
        {
            get { return this.dieInvestments.AsReadOnly(); }
        }

        /// <summary>
        /// Gets the total machine investment.
        /// </summary>        
        public decimal TotalMachinesInvestment { get; private set; }

        /// <summary>
        /// Gets the total die investment.
        /// </summary>        
        public decimal TotalDiesInvestment { get; private set; }

        /// <summary>
        /// Gets the total investment cost.
        /// </summary>        
        public decimal TotalInvestment
        {
            get { return this.TotalMachinesInvestment + this.TotalDiesInvestment; }
        }

        /// <summary>
        /// Adds a specified investment cost to this instance (equivalent to +=).
        /// </summary>
        /// <param name="cost">The cost to add.</param>
        /// <exception cref="ArgumentNullException">The cost parameter was null.</exception>
        public void Add(InvestmentCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The cost parameter was null.");
            }

            this.TotalMachinesInvestment += cost.TotalMachinesInvestment;
            this.TotalDiesInvestment += cost.TotalDiesInvestment;

            this.dieInvestments.AddRange(cost.dieInvestments);
            this.machineInvestments.AddRange(cost.machineInvestments);
        }

        /// <summary>
        /// Adds a specified investment cost to the list of machine investments.
        /// </summary>
        /// <param name="cost">The cost to add.</param>
        /// <exception cref="ArgumentNullException">The cost parameter was null.</exception>
        public void AddMachineInvestment(InvestmentCostSingle cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The cost parameter was null.");
            }

            this.TotalMachinesInvestment += cost.TotalInvestment;
            this.machineInvestments.Add(cost);
        }

        /// <summary>
        /// Adds a specified investment cost to the list of die costs.
        /// </summary>
        /// <param name="cost">The cost to add.</param>
        /// <exception cref="ArgumentNullException">The cost parameter was null.</exception>
        public void AddDieInvestment(InvestmentCostSingle cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The cost parameter was null.");
            }

            this.TotalDiesInvestment += cost.TotalInvestment;
            this.dieInvestments.Add(cost);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public InvestmentCost Clone()
        {
            // Copy the value members
            InvestmentCost clone = (InvestmentCost)this.MemberwiseClone();

            // Clone the reference members
            clone.dieInvestments = new List<InvestmentCostSingle>();
            foreach (InvestmentCostSingle singleCost in this.dieInvestments)
            {
                clone.dieInvestments.Add(singleCost.Clone());
            }

            clone.machineInvestments = new List<InvestmentCostSingle>();
            foreach (InvestmentCostSingle singleCost in this.machineInvestments)
            {
                clone.machineInvestments.Add(singleCost.Clone());
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.TotalDiesInvestment *= conversionFactors.CurrencyFactor;
            this.TotalMachinesInvestment *= conversionFactors.CurrencyFactor;

            foreach (InvestmentCostSingle singleCost in this.dieInvestments)
            {
                singleCost.ApplyUnitConversion(conversionFactors);
            }

            foreach (InvestmentCostSingle singleCost in this.machineInvestments)
            {
                singleCost.ApplyUnitConversion(conversionFactors);
            }
        }
    }
}
