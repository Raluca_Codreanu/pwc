﻿using System;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The class stores calculation results of the overhead costs
    /// </summary>
    public class OverheadCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OverheadCost"/> class.
        /// </summary>
        public OverheadCost()
        {
        }

        /// <summary>
        /// Gets or sets the raw material overhead cost.
        /// </summary>        
        public decimal RawMaterialOverhead { get; set; }

        /// <summary>
        /// Gets or sets the raw material margin cost.
        /// </summary>        
        public decimal RawMaterialMargin { get; set; }

        /// <summary>
        /// Gets or sets the consumable overhead cost.
        /// </summary>        
        public decimal ConsumableOverhead { get; set; }

        /// <summary>
        /// Gets or sets the consumable margin cost.
        /// </summary>        
        public decimal ConsumableMargin { get; set; }

        /// <summary>
        /// Gets or sets the commodity overhead cost.
        /// </summary>        
        public decimal CommodityOverhead { get; set; }

        /// <summary>
        /// Gets or sets the commodity margin cost.
        /// </summary>        
        public decimal CommodityMargin { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing overhead cost.
        /// </summary>        
        public decimal ManufacturingOverhead { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing margin cost.
        /// </summary>        
        public decimal ManufacturingMargin { get; set; }

        /// <summary>
        /// Gets or sets the other cost overhead cost.
        /// </summary>        
        public decimal OtherCostOverhead { get; set; }

        /// <summary>
        /// Gets or sets the packaging overhead cost.
        /// </summary>        
        public decimal PackagingOverhead { get; set; }

        /// <summary>
        /// Gets or sets the logistic overhead cost.
        /// </summary>        
        public decimal LogisticOverhead { get; set; }

        /// <summary>
        /// Gets or sets the sales and administration overhead cost.
        /// </summary>        
        public decimal SalesAndAdministrationOverhead { get; set; }

        /// <summary>
        /// Gets or sets the company surcharge overhead cost.
        /// </summary>        
        public decimal CompanySurchargeOverhead { get; set; }

        /// <summary>
        /// Gets or sets the external work margin cost.
        /// </summary>
        public decimal ExternalWorkMargin { get; set; }

        /// <summary>
        /// Gets or sets the external work overhead cost.
        /// </summary>
        public decimal ExternalWorkOverhead { get; set; }

        /// <summary>
        /// Gets or sets the average manufacturing overhead rate, which takes into account the different manufacturing rates used at process step level
        /// (different from the rate of the part/assembly).
        /// If no step uses a different rate then this value is equal to the part/assembly's manufacturing overhead rate.        
        /// </summary>
        public decimal ManufacturingOverheadRateAverage { get; set; }

        /// <summary>
        /// Gets the total overhead cost.
        /// </summary>        
        public decimal TotalSumOverhead
        {
            get
            {
                return this.RawMaterialOverhead + this.ConsumableOverhead + this.CommodityOverhead + this.ManufacturingOverhead + this.OtherCostOverhead
                    + this.PackagingOverhead + this.LogisticOverhead + this.SalesAndAdministrationOverhead + this.CompanySurchargeOverhead
                    + this.ExternalWorkOverhead;
            }
        }

        /// <summary>
        /// Gets the total sum margin cost.
        /// </summary>        
        public decimal TotalSumMargin
        {
            get
            {
                return this.RawMaterialMargin + this.ConsumableMargin + this.CommodityMargin + this.ManufacturingMargin + this.ExternalWorkMargin;
            }
        }

        /// <summary>
        /// Gets the total overhead and margin cost.
        /// </summary>      
        public decimal TotalSumOverheadAndMargin
        {
            get { return this.TotalSumOverhead + this.TotalSumMargin; }
        }

        /// <summary>
        /// Gets or sets the overhead settings used to calculate the overhead and margin costs.
        /// </summary>        
        public OverheadSetting OverheadSettings { get; set; }

        /// <summary>
        /// Adds the values in the specified cost to the values in this instance.
        /// </summary>
        /// <param name="cost">The cost to add.</param>        
        public void Add(OverheadCost cost)
        {
            this.Add(cost, 1);
        }

        /// <summary>
        /// Adds the values multiplied by <paramref name="multiplier"/> value in the specified cost to the values in this instance.
        /// </summary>
        /// <param name="cost">The cost to add.</param>        
        /// <param name="multiplier">The value to multiply the instance to add</param>
        public void Add(OverheadCost cost, int multiplier)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The OverheadCost to add to this instance was null.");
            }

            this.CommodityMargin += cost.CommodityMargin * multiplier;
            this.CommodityOverhead += cost.CommodityOverhead * multiplier;
            this.ConsumableMargin += cost.ConsumableMargin * multiplier;
            this.ConsumableOverhead += cost.ConsumableOverhead * multiplier;
            this.ExternalWorkMargin += cost.ExternalWorkMargin * multiplier;
            this.LogisticOverhead += cost.LogisticOverhead * multiplier;
            this.ManufacturingMargin += cost.ManufacturingMargin * multiplier;
            this.ManufacturingOverhead += cost.ManufacturingOverhead * multiplier;
            this.OtherCostOverhead += cost.OtherCostOverhead * multiplier;
            this.PackagingOverhead += cost.PackagingOverhead * multiplier;
            this.RawMaterialMargin += cost.RawMaterialMargin * multiplier;
            this.RawMaterialOverhead += cost.RawMaterialOverhead * multiplier;
            this.SalesAndAdministrationOverhead += cost.SalesAndAdministrationOverhead * multiplier;
            this.CompanySurchargeOverhead += cost.CompanySurchargeOverhead * multiplier;
            this.ExternalWorkOverhead += cost.ExternalWorkOverhead * multiplier;
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public OverheadCost Clone()
        {
            // Copy the value members
            OverheadCost clone = (OverheadCost)this.MemberwiseClone();

            // Clone the reference members
            if (this.OverheadSettings != null)
            {
                clone.OverheadSettings = this.OverheadSettings.Copy();
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.CommodityMargin *= conversionFactors.CurrencyFactor;
            this.CommodityOverhead *= conversionFactors.CurrencyFactor;
            this.ConsumableMargin *= conversionFactors.CurrencyFactor;
            this.ConsumableOverhead *= conversionFactors.CurrencyFactor;
            this.ExternalWorkMargin *= conversionFactors.CurrencyFactor;
            this.LogisticOverhead *= conversionFactors.CurrencyFactor;
            this.ManufacturingMargin *= conversionFactors.CurrencyFactor;
            this.ManufacturingOverhead *= conversionFactors.CurrencyFactor;
            this.OtherCostOverhead *= conversionFactors.CurrencyFactor;
            this.PackagingOverhead *= conversionFactors.CurrencyFactor;
            this.RawMaterialMargin *= conversionFactors.CurrencyFactor;
            this.RawMaterialOverhead *= conversionFactors.CurrencyFactor;
            this.SalesAndAdministrationOverhead *= conversionFactors.CurrencyFactor;
            this.CompanySurchargeOverhead *= conversionFactors.CurrencyFactor;
            this.ExternalWorkOverhead *= conversionFactors.CurrencyFactor;
        }
    }
}
