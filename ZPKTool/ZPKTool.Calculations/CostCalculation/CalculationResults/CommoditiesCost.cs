﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the calculation results for multiple commodities.
    /// </summary>
    public class CommoditiesCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommoditiesCost"/> class.
        /// </summary>
        public CommoditiesCost()
        {
            this.CommodityCosts = new Collection<CommodityCost>();
        }

        /// <summary>
        /// Gets or sets the sum of the reject cost of all stored commodities.
        /// </summary>
        public decimal RejectCostSum { get; set; }

        /// <summary>
        /// Gets or sets the sum of the cost of all stored commodities.
        /// </summary>
        public decimal CostsSum { get; set; }

        /// <summary>
        /// Gets or sets the sum of the overhead cost of all stored commodities.
        /// </summary>        
        public decimal OverheadSum { get; set; }

        /// <summary>
        /// Gets or sets the sum of the margin cost of all stored commodities.
        /// </summary>
        public decimal MarginSum { get; set; }

        /// <summary>
        /// Gets or sets the sum of the total cost of all stored commodities.
        /// </summary>        
        public decimal TotalCostSum { get; set; }

        /// <summary>
        /// Gets a collection that stores the detailed cost of the commodities.
        /// </summary>        
        public Collection<CommodityCost> CommodityCosts { get; private set; }

        /// <summary>
        /// Adds the specified commodity cost to the commodity costs list.
        /// </summary>
        /// <param name="commodityCost">The consumable cost.</param>
        public void AddCost(CommodityCost commodityCost)
        {
            if (commodityCost == null)
            {
                throw new ArgumentNullException("commodityCost", "The commodity cost to add was null.");
            }

            this.RejectCostSum += commodityCost.RejectCost;
            this.CostsSum += commodityCost.Cost;
            this.OverheadSum += commodityCost.Overhead;
            this.MarginSum += commodityCost.Margin;
            this.TotalCostSum += commodityCost.TotalCost;

            this.CommodityCosts.Add(commodityCost);
        }

        /// <summary>
        /// Adds the specified commodity costs to the commodity costs list.
        /// </summary>
        /// <param name="commodityCosts">The consumable costs.</param>
        public void AddCosts(IEnumerable<CommodityCost> commodityCosts)
        {
            if (commodityCosts == null)
            {
                throw new ArgumentNullException("commodityCosts", "The commodity costs to add were null.");
            }

            foreach (CommodityCost cost in commodityCosts)
            {
                this.AddCost(cost);
            }
        }

        /// <summary>
        /// Recalculates the sum property values.
        /// </summary>
        public void RecalculateSums()
        {
            this.RejectCostSum = this.CommodityCosts.Sum(c => c.RejectCost);
            this.CostsSum = this.CommodityCosts.Sum(c => c.Cost);
            this.OverheadSum = this.CommodityCosts.Sum(c => c.Overhead);
            this.MarginSum = this.CommodityCosts.Sum(c => c.Margin);
            this.TotalCostSum = this.CommodityCosts.Sum(c => c.TotalCost);
        }

        /// <summary>
        /// Adds the specified cost to this instance and saves the result in this instance.
        /// Equivalent to this += cost.
        /// </summary>
        /// <param name="cost">The cost to add to his instance.</param>
        public void Add(CommoditiesCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The commodities cost to sum with was null.");
            }

            this.AddCosts(cost.CommodityCosts);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public CommoditiesCost Clone()
        {
            // Copy the value members
            CommoditiesCost clone = (CommoditiesCost)this.MemberwiseClone();

            // Clone the reference members
            clone.CommodityCosts = new Collection<CommodityCost>();
            if (this.CommodityCosts != null)
            {
                foreach (CommodityCost cost in this.CommodityCosts)
                {
                    clone.CommodityCosts.Add(cost.Clone());
                }
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.RejectCostSum *= conversionFactors.CurrencyFactor;
            this.CostsSum *= conversionFactors.CurrencyFactor;
            this.MarginSum *= conversionFactors.CurrencyFactor;
            this.OverheadSum *= conversionFactors.CurrencyFactor;
            this.TotalCostSum *= conversionFactors.CurrencyFactor;

            if (this.CommodityCosts != null)
            {
                foreach (CommodityCost cost in this.CommodityCosts)
                {
                    cost.ApplyUnitConversion(conversionFactors);
                }
            }
        }
    }
}
