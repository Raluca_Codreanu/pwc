﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Represents the single (non-aggregated) investment cost of an entity (like machine and die).
    /// </summary>
    public class InvestmentCostSingle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvestmentCostSingle"/> class.
        /// </summary>
        public InvestmentCostSingle()
        {
            this.Amount = 1m;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvestmentCostSingle"/> class.
        /// </summary>
        /// <param name="parentId">The id of the entity.</param>
        /// <param name="name">The name of the entity.</param>
        /// <param name="investment">The investment.</param>
        public InvestmentCostSingle(Guid parentId, string name, decimal investment)
            : this()
        {
            this.ParentId = parentId;
            this.Name = name;
            this.Investment = investment;
        }

        /// <summary>
        /// Gets or sets the id of the object whose investment is represented by this instance.
        /// </summary>        
        public Guid ParentId { get; set; }

        /// <summary>
        /// Gets or sets the name of the entity whose investment this is.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the investment for one amount of the entity.
        /// </summary>
        public decimal Investment { get; set; }

        /// <summary>
        /// Gets or sets the amount of the entity. The default value is 1.        
        /// </summary>
        /// <remarks>
        /// The default value of 1 is necessary to support backwards compatibility for the cost calculators older than v1.2
        /// </remarks>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets the total investment (investment x amount).
        /// </summary>
        public decimal TotalInvestment
        {
            get { return this.Investment * this.Amount; }
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public InvestmentCostSingle Clone()
        {
            // Copy the value members
            InvestmentCostSingle clone = (InvestmentCostSingle)this.MemberwiseClone();

            //// Clone the reference members

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.Investment *= conversionFactors.CurrencyFactor;
        }
    }
}
