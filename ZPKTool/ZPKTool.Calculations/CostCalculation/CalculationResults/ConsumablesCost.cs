﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Class that stores the calculation results for multiple Consumables.
    /// </summary>
    public class ConsumablesCost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumablesCost"/> class.
        /// </summary>
        public ConsumablesCost()
        {
            this.ConsumableCosts = new Collection<ConsumableCost>();
        }

        /// <summary>
        /// Gets or sets the costs sum.
        /// </summary>
        public decimal CostsSum { get; set; }

        /// <summary>
        /// Gets or sets the overhead costs sum.
        /// </summary>        
        public decimal OverheadSum { get; set; }

        /// <summary>
        /// Gets or sets the total costs sum.
        /// </summary>        
        public decimal TotalCostSum { get; set; }

        /// <summary>
        /// Gets a collection that stores the detailed cost of the consumables.
        /// </summary>        
        public Collection<ConsumableCost> ConsumableCosts { get; private set; }

        /// <summary>
        /// Adds the specified consumable cost to the consumable costs list.
        /// </summary>
        /// <param name="consumableCost">The consumable cost.</param>
        public void AddCost(ConsumableCost consumableCost)
        {
            if (consumableCost == null)
            {
                throw new ArgumentNullException("consumableCost", "The consumable cost to add was null.");
            }

            this.CostsSum += consumableCost.Cost;
            this.OverheadSum += consumableCost.Overhead;
            this.TotalCostSum += consumableCost.TotalCost;

            this.ConsumableCosts.Add(consumableCost);
        }

        /// <summary>
        /// Adds the specified consumable costs to the consumable costs list.
        /// </summary>
        /// <param name="consumableCosts">The consumable costs.</param>
        public void AddCosts(IEnumerable<ConsumableCost> consumableCosts)
        {
            if (consumableCosts == null)
            {
                throw new ArgumentNullException("consumableCosts", "The consumable costs to add was null.");
            }

            foreach (ConsumableCost cost in consumableCosts)
            {
                AddCost(cost);
            }
        }

        /// <summary>
        /// Adds the specified cost to this instance and saves the result in this instance.
        /// Equivalent to this += cost.
        /// </summary>
        /// <param name="cost">The cost to add to his instance.</param>
        public void Add(ConsumablesCost cost)
        {
            if (cost == null)
            {
                throw new ArgumentNullException("cost", "The consumables cost to sum with was null.");
            }

            AddCosts(cost.ConsumableCosts);
        }

        /// <summary>
        /// Creates a deep copy of this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public ConsumablesCost Clone()
        {
            // Copy the value members
            ConsumablesCost clone = (ConsumablesCost)this.MemberwiseClone();

            // Clone the reference members
            clone.ConsumableCosts = new Collection<ConsumableCost>();
            if (this.ConsumableCosts != null)
            {
                foreach (ConsumableCost cost in this.ConsumableCosts)
                {
                    clone.ConsumableCosts.Add(cost.Clone());
                }
            }

            return clone;
        }

        /// <summary>
        /// Converts all members that represent values in a measurement unit using the appropriate conversion factors from the once specified
        /// in the <paramref name="conversionFactors"/> parameter.
        /// </summary>
        /// <param name="conversionFactors">The conversion factors.</param>
        public void ApplyUnitConversion(UnitConversionFactors conversionFactors)
        {
            if (conversionFactors == null)
            {
                throw new ArgumentNullException("conversionFactors", "The conversion factors were null.");
            }

            this.CostsSum *= conversionFactors.CurrencyFactor;
            this.OverheadSum *= conversionFactors.CurrencyFactor;
            this.TotalCostSum *= conversionFactors.CurrencyFactor;

            if (this.ConsumableCosts != null)
            {
                foreach (ConsumableCost cost in this.ConsumableCosts)
                {
                    cost.ApplyUnitConversion(conversionFactors);
                }
            }
        }
    }
}
