﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Factory for retrieving ICostCalculator implementations based on version number.
    /// </summary>
    public static class CostCalculatorFactory
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The available cost calculation versions.        
        /// </summary>
        /// <remarks>        
        /// If you add a new calculation version, add it at the end of the list and also update the LatestVersion constant to match it.
        /// A version can have at most 10 characters.
        /// </remarks>
        private static readonly List<string> calculationVersions = new List<string>()
        {
            "1.0",
            "1.1",
            "1.1.1",
            "1.2",
            "1.3",
            "1.4"
        };

        /// <summary>
        /// The mapping between calculation versions and ICostCalculator implementations.
        /// </summary>
        /// <remarks>
        /// When implementing a new calculation version, add its mapping here.
        /// </remarks>
        private static readonly List<Tuple<string, Type>> calculatorImplementationsMap = new List<Tuple<string, Type>>()
        {
            new Tuple<string, Type>("1.0", typeof(CostCalculator_1_0)),
            new Tuple<string, Type>("1.1", typeof(CostCalculator_1_1)),
            new Tuple<string, Type>("1.1.1", typeof(CostCalculator_1_1_1)),
            new Tuple<string, Type>("1.2", typeof(CostCalculator_1_2)),
            new Tuple<string, Type>("1.3", typeof(CostCalculator_1_3)),
            new Tuple<string, Type>("1.4", typeof(CostCalculator_1_4))
        };

        /// <summary>
        /// An object used to sync access to the <see cref="measurementUnits"/> and <see cref="basicSettings"/>
        /// </summary>
        private static readonly object dataLock = new object();

        /// <summary>
        /// The measurement units that will be used by the <see cref="ICostCalculator"/> instances created by this factory.
        /// </summary>
        private static List<MeasurementUnit> measurementUnits = new List<MeasurementUnit>();

        /// <summary>
        /// The basic settings that will be used by the <see cref="ICostCalculator"/> instances created by this factory.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// The latest (newest) calculator version.
        /// </summary>
        public const string LatestVersion = "1.4";

        /// <summary>
        /// The oldest calculator version.
        /// </summary>
        public const string OldestVersion = "1.0";

        /// <summary>
        /// Gets the available cost calculation versions.
        /// </summary>        
        public static ReadOnlyCollection<string> CalculationVersions
        {
            get
            {
                return calculationVersions.AsReadOnly();
            }
        }

        /// <summary>
        /// Gets the ICostCalculator implementation for the specified version.
        /// <para />
        /// If the specified version is null or whitespace it returns the oldest version implementation (for backwards compatibility with data).
        /// <para />
        /// If the specified version is not in supported it the newest version implementation.
        /// </summary>
        /// <param name="version">The calculation version.</param>
        /// <returns>The appropriate cost calculator implementation.</returns>        
        /// <exception cref="System.ArgumentNullException">The basic settings or the measurement units were null.</exception>
        public static ICostCalculator GetCalculator(string version)
        {
            lock (dataLock)
            {
                return GetCalculator(version, CostCalculatorFactory.basicSettings, CostCalculatorFactory.measurementUnits);
            }
        }

        /// <summary>
        /// Gets the ICostCalculator implementation for the specified version.
        /// <para />
        /// If the specified version is null or whitespace it returns the oldest version implementation (for backwards compatibility with data).
        /// <para />
        /// If the specified version is not in supported it the newest version implementation.
        /// </summary>
        /// <param name="version">The calculation version.</param>
        /// <param name="basicSettings">The basic settings to be used by the created calculator instance.</param>
        /// <param name="measurementUnits">The measurement units to be used by the created calculator instance.</param>
        /// <returns>
        /// The appropriate cost calculator implementation.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The basic settings or the measurement units were null.</exception>        
        public static ICostCalculator GetCalculator(string version, BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
        {
            if (basicSettings == null)
            {
                throw new ArgumentNullException("basicSettings", "The basic settings can not be null.");
            }

            if (measurementUnits == null)
            {
                throw new ArgumentNullException("measurementUnits", "The measurement units can not be null.");
            }

            // The oldest calculator is returned for null or empty version so that objects created before calculation versioning was implemented
            // are assigned the oldest calculation algorithms.
            if (string.IsNullOrEmpty(version))
            {
                version = OldestVersion;
            }

            if (!calculationVersions.Contains(version))
            {
                version = LatestVersion;
                log.Warn("The calculator version '{0}' does not exist. The newest version ('{1}') will be provided.", version, LatestVersion);
            }

            Tuple<string, Type> calculatorMapping = calculatorImplementationsMap.FirstOrDefault(p => p.Item1 == version);
            if (calculatorMapping != null)
            {
                ICostCalculator calculator =
                    (ICostCalculator)Activator.CreateInstance(calculatorMapping.Item2, basicSettings, measurementUnits);
                calculator.Version = version;

                return calculator;
            }
            else
            {
                throw new InvalidOperationException(string.Format(
                    CultureInfo.InvariantCulture,
                    "A cost calculator implementation was not found for version '{0}'.",
                    version));
            }
        }

        /// <summary>
        /// Determines whether the specified version is valid.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// true if the version is valid; otherwise, false.
        /// </returns>
        public static bool IsValidVersion(string version)
        {
            if (string.IsNullOrEmpty(version))
            {
                return false;
            }

            return calculationVersions.Contains(version);
        }

        /// <summary>
        /// Determines whether the specified version is the latest (newest) version.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// true if the specified version is the latest version; otherwise, false.
        /// </returns>
        public static bool IsLatestVersion(string version)
        {
            if (version == null)
            {
                return false;
            }

            return string.Compare(version, LatestVersion, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Determines whether a calculation version (version1) is newer than another version.
        /// </summary>
        /// <param name="version1">The 1st version.</param>
        /// <param name="version2">The 2nd version.</param>
        /// <returns>
        /// true if version1 is newer; otherwise, false.
        /// </returns>
        public static bool IsNewer(string version1, string version2)
        {
            int index1 = calculationVersions.IndexOf(version1);
            int index2 = calculationVersions.IndexOf(version2);
            return index1 > index2;
        }

        /// <summary>
        /// Determines whether a calculation version (version1) is older than another version.
        /// </summary>
        /// <param name="version1">The 1st version.</param>
        /// <param name="version2">The 2nd version.</param>
        /// <returns>
        /// true if version1 is older; otherwise, false.
        /// </returns>
        public static bool IsOlder(string version1, string version2)
        {
            int index1 = calculationVersions.IndexOf(version1);
            int index2 = calculationVersions.IndexOf(version2);
            return index1 < index2;
        }

        /// <summary>
        /// Determines whether two calculation versions are equal.
        /// </summary>
        /// <param name="version1">The 1st version.</param>
        /// <param name="version2">The 2nd version.</param>
        /// <returns>
        /// true if version1 represents the same calculation version as version2; otherwise, false.
        /// </returns>
        public static bool IsSame(string version1, string version2)
        {
            int index1 = calculationVersions.IndexOf(version1);
            int index2 = calculationVersions.IndexOf(version2);
            return index1 == index2 && index1 != -1;
        }

        /// <summary>
        /// Sets the measurement units that will be used by the <see cref="ICostCalculator" /> instances created by this factory.
        /// </summary>
        /// <param name="measurementUnits">The measurement units.</param>
        /// <exception cref="System.ArgumentNullException">The measurement units can not be null.</exception>
        public static void SetMeasurementUnits(IEnumerable<MeasurementUnit> measurementUnits)
        {
            if (measurementUnits == null)
            {
                throw new ArgumentNullException("measurementUnits", "The measurement units can not be null.");
            }

            lock (dataLock)
            {
                CostCalculatorFactory.measurementUnits.Clear();
                CostCalculatorFactory.measurementUnits.AddRange(measurementUnits);
            }
        }

        /// <summary>
        /// Sets the basic settings that will be used by the cost calculator instances created by this factory.
        /// </summary>
        /// <param name="basicSettings">The basic settings.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings can not be null.</exception>
        public static void SetBasicSettings(BasicSetting basicSettings)
        {
            if (basicSettings == null)
            {
                throw new ArgumentNullException("basicSettings", "The basic settings can not be null.");
            }

            lock (dataLock)
            {
                CostCalculatorFactory.basicSettings = basicSettings;
            }
        }
    }
}
