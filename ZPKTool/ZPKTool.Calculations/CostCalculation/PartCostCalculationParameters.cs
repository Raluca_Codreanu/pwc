﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The input parameters for the cost calculation of a part.
    /// </summary>
    public class PartCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the depreciation period of the project to which the part belongs.
        /// </summary>        
        public int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the project to which the part belongs.
        /// </summary>        
        public decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost ratio of the project to which the part belongs.
        /// </summary>        
        public decimal? ProjectLogisticCostRatio { get; set; }

        /// <summary>
        /// Gets or sets the parent project (if it exists) creation date.
        /// </summary>
        public DateTime? ProjectCreateDate { get; set; }
    }
}
