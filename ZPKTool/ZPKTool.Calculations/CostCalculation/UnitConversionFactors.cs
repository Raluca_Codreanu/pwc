﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The unit conversion factors to be applied to the CalculationResult's object graph.
    /// </summary>
    public class UnitConversionFactors
    {
        /// <summary>
        /// Gets or sets the conversion factor for currency. Multiply with this value when converting.
        /// </summary>
        public decimal CurrencyFactor { get; set; }

        /// <summary>
        /// Gets or sets the conversion factor for weight. Divide by this value when converting.
        /// </summary>
        public decimal WeightFactor { get; set; }

        /// <summary>
        /// Gets or sets the conversion factor for length. Divide by this value when converting.
        /// </summary>
        public decimal LengthFactor { get; set; }

        /// <summary>
        /// Gets or sets the conversion factor for volume. Divide by this value when converting.
        /// </summary>
        public decimal VolumeFactor { get; set; }

        /// <summary>
        /// Gets or sets the conversion factor for area. Divide by this value when converting.
        /// </summary>
        public decimal AreaFactor { get; set; }
    }
}
