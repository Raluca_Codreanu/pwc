﻿namespace ZPKTool.Calculations.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The input parameters for calculating the cost of an assembly's process.
    /// </summary>
    internal class AssemblyProcessCostCalculationParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyProcessCostCalculationParameters"/> class.
        /// </summary>
        public AssemblyProcessCostCalculationParameters()
        {
            this.SubAssembliesCost = new AssembliesCost();
            this.SubPartsCost = new PartsCost();
        }

        /// <summary>
        ///  Gets or sets the depreciation period of the assembly's parent project.
        /// </summary>        
        public int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the assembly's parent project.
        /// </summary>        
        public decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the cost of all sub-assemblies belonging to the assembly (direct sub-assemblies, not all down the hierarchy).
        /// </summary>        
        public AssembliesCost SubAssembliesCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of all sub-parts belonging to the assembly (direct sub-parts, not all down the hierarchy).
        /// </summary>        
        public PartsCost SubPartsCost { get; set; }

        /// <summary>
        /// Gets or sets the parent project (if it exists) creation date.
        /// </summary>
        public DateTime? ProjectCreateDate { get; set; }
    }
}
