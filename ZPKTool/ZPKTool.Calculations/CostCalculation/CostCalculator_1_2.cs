﻿namespace ZPKTool.Calculations.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// The cost calculator implementation v1.2
    /// </summary>
    /// <remarks>
    /// Changes in this version:
    ///     - Fix for Part Enhanced Scrap Calculation (the scrap cost for the rejected parts is now divided by the step's parts per cycle field).
    ///     - Use Manufacturing Overhead Ratio from the Process Step level, if defined
    ///     - Use the Machine Amount property at process step level
    /// </remarks>
    internal class CostCalculator_1_2 : CostCalculator_1_1_1, ICostCalculator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_2" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_2(BasicSetting basicSettings)
            : base(basicSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_2" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_2(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : base(basicSettings, measurementUnits)
        {
        }

        /// <summary>
        /// Calculates the cost of an assembling process (the process of an assembly).
        /// </summary>
        /// <param name="assembly">The assembly whose process cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>
        protected override ProcessCost CalculateProcessCost(Assembly assembly, AssemblyProcessCostCalculationParameters parameters)
        {
            ProcessCost processCost = base.CalculateProcessCost(assembly, parameters);

            if (assembly.Process != null)
            {
                // Calculate the Capacity Utilization for all machines in the process.
                this.CalculateMachinesCapacityUtilization(
                    assembly.Process.Steps,
                    processCost,
                    assembly.BatchSizePerYear.GetValueOrDefault(),
                    assembly.YearlyProductionQuantity.GetValueOrDefault());
            }

            return processCost;
        }

        /// <summary>
        /// Calculates the cost of a manufacturing process (the process of a part).
        /// </summary>
        /// <param name="part">The part whose process cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>
        protected override ProcessCost CalculateProcessCost(Part part, PartProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || part.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();

            // Initialize the calculation parameters for consumables. These parameters are the same for all steps.            
            var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = part.OverheadSettings.ConsumableOverhead };

            decimal batchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            decimal batchSizeTarget = 0m;
            if (batchesPerYear != 0m)
            {
                batchSizeTarget = Math.Ceiling((decimal)part.YearlyProductionQuantity.GetValueOrDefault() / batchesPerYear);
            }

            decimal totalNecessaryBatchSize = batchSizeTarget;
            decimal previousStepScrapRejectCost = 0m;

            // Calculating the step costs in reverse order is a requirement for the enhanced scrap calculation algorithm
            foreach (var step in part.Process.Steps.OrderByDescending(step => step.Index))
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    ConsumablesCost consumablesCost = this.CalculateConsumablesCost(step.Consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var partLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(part);

                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    part.CountrySettings,
                    part.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    partLifetimeProdQty,
                    part.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);

                stepCost.ParentPartIsExternal = part.IsExternal.GetValueOrDefault();

                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    /*** Enhanced scrap calculation ***/

                    // The quantity of parts that must be outputted from this step.
                    // This step's output quantity is the previous step's process gross batch size (for the last step is the batch size target)
                    decimal processOutputQty = totalNecessaryBatchSize;

                    // The amount of parts that have to be produced in order to have an output of processOutputQty parts.
                    decimal processGrossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * processOutputQty);

                    decimal rejectedScrapCost = 0m;
                    if (batchSizeTarget != 0m && step.PartsPerCycle.GetValueOrDefault() != 0m)
                    {
                        // Scrap cost for the rejected parts
                        var materialsCost = parameters.RawMaterialsCost.NetCostSum + parameters.CommoditiesCost.CostsSum;
                        rejectedScrapCost = (((processGrossBatchSize - processOutputQty) * materialsCost / batchSizeTarget)
                            + previousStepScrapRejectCost)
                            / step.PartsPerCycle.GetValueOrDefault();
                    }

                    decimal rejectedManufacturingCost = 0m;
                    if (batchSizeTarget != 0m)
                    {
                        // Manufacturing cost for the rejected parts
                        rejectedManufacturingCost = ((processGrossBatchSize - processOutputQty) * stepCost.ManufacturingCost / batchSizeTarget)
                            * step.PartsPerCycle.GetValueOrDefault();
                    }

                    stepCost.GrossBatchSize = processGrossBatchSize;
                    stepCost.NetBatchSize = processOutputQty;
                    stepCost.RejectedPieces = stepCost.GrossBatchSize - stepCost.NetBatchSize;
                    stepCost.RejectMaterialCostPerPiece = rejectedScrapCost;
                    stepCost.RejectManufacturingCostPerPiece = rejectedManufacturingCost;
                    stepCost.RejectCost = stepCost.RejectMaterialCostPerPiece + stepCost.RejectManufacturingCostPerPiece;

                    totalNecessaryBatchSize = processGrossBatchSize;
                    previousStepScrapRejectCost = rejectedScrapCost;

                    /**********************************/
                }

                processCost.AddStepCost(stepCost);
            }

            processCost.RejectOverview.IOPiecesPerYear = part.YearlyProductionQuantity.GetValueOrDefault();
            processCost.RejectOverview.BatchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            processCost.RejectOverview.IOPiecesPerBatch = batchSizeTarget;
            processCost.RejectOverview.GrossBatchSize = totalNecessaryBatchSize;
            processCost.RejectOverview.RejectedPiecesTotal = processCost.RejectOverview.GrossBatchSize - processCost.RejectOverview.IOPiecesPerBatch;

            // Calculate the Capacity Utilization for all machines in the process.
            this.CalculateMachinesCapacityUtilization(
                part.Process.Steps,
                processCost,
                part.BatchSizePerYear.GetValueOrDefault(),
                part.YearlyProductionQuantity.GetValueOrDefault());

            return processCost;
        }

        /// <summary>
        /// Computes the costs of a process step.
        /// </summary>
        /// <param name="step">The process step to calculate the costs for.</param>
        /// <param name="countrySettings">The country settings to be used during the calculations.</param>
        /// <param name="overheadSettings">The overhead settings to be used during the calculations.</param>
        /// <param name="projectDepreciationPeriod">The project depreciation period.</param>
        /// <param name="projectDepreciationRate">The project depreciation rate.</param>
        /// <param name="partLifeCycleProductionQuantity">The life cycle production quantity of the part/assembly to which the step belongs to.</param>
        /// <param name="toolingActive">a value indicating whether tooling is active for the process</param>
        /// <param name="projectCreateDate">The project create date.</param>
        /// <returns>
        /// An object containing the costs for the given process step.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The complexity of the calculation code can not be easily avoided.")]
        protected override ProcessStepCost CalculateProcessStepCost(
            ProcessStep step,
            CountrySetting countrySettings,
            OverheadSetting overheadSettings,
            int projectDepreciationPeriod,
            decimal projectDepreciationRate,
            decimal partLifeCycleProductionQuantity,
            bool toolingActive,
            DateTime? projectCreateDate)
        {
            ProcessStepCost stepCost = new ProcessStepCost();
            stepCost.StepId = step.Guid;
            stepCost.StepName = step.Name;
            stepCost.StepIndex = step.Index;
            stepCost.IsExternal = step.IsExternal.GetValueOrDefault();
            stepCost.SBMActive = toolingActive;

            // If the step calculation accuracy is estimated then the estimated price is the step's total cost.
            var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
            if (stepAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                stepCost.ManufacturingCostIsEstimated = true;
                stepCost.ManufacturingCost = step.Price.GetValueOrDefault();
                stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
                stepCost.ManufacturingOverheadRate = overheadSettings.ManufacturingOverhead;
                return stepCost;
            }

            // Copy the most used step parameters in local not nullable variables
            decimal stepPartsPerCycle = step.PartsPerCycle.GetValueOrDefault();
            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
            decimal stepCycleTime = step.CycleTime.GetValueOrDefault();
            int stepBatchSize = step.BatchSize.GetValueOrDefault();

            // Calculate machines cost
            if (step.Machines.Count > 0)
            {
                var machineCalcParams = new MachineCostCalculationParameters()
                {
                    CountrySettingsAirCost = countrySettings.AirCost,
                    CountrySettingsEnergyCost = countrySettings.EnergyCost,
                    CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost,
                    CountrySettingsWaterCost = countrySettings.WaterCost,
                    ProcessStepHoursPerShift = step.HoursPerShift.GetValueOrDefault(),
                    ProcessStepProductionWeeksPerYear = step.ProductionWeeksPerYear.GetValueOrDefault(),
                    ProcessStepShiftsPerWeek = step.ShiftsPerWeek.GetValueOrDefault(),
                    ProcessStepExtraShiftsPerWeek = step.ExtraShiftsNumber.GetValueOrDefault(),
                    ProjectDepreciationPeriod = projectDepreciationPeriod,
                    ProjectDepreciationRate = projectDepreciationRate,
                    ProjectCreateDate = projectCreateDate
                };

                decimal machinesCostSum = 0m;
                foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                {
                    MachineCost machineCost = CalculateMachineCost(machine, machineCalcParams);
                    stepCost.MachineCosts.Add(machineCost);

                    decimal machineCostInProcess = 0m;
                    if (machine.IsProjectSpecific)
                    {
                        if (partLifeCycleProductionQuantity != 0m && stepPartsPerCycle != 0m)
                        {
                            decimal totalInvest = (machine.MachineInvestment.GetValueOrDefault()
                                + machine.SetupInvestment.GetValueOrDefault()
                                + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                                + machine.FundamentalSetupInvestment.GetValueOrDefault())
                                * machine.Amount;

                            machineCostInProcess = (totalInvest / partLifeCycleProductionQuantity)
                                + ((stepProcessTime / stepPartsPerCycle) * (machineCost.EnergyCostPerHour / SecondsInHour))
                                + machineCost.FloorCost;
                        }
                    }
                    else if (stepPartsPerCycle != 0m)
                    {
                        machineCostInProcess = (machineCost.FullCostPerHour / SecondsInHour) * (stepProcessTime / stepPartsPerCycle);
                    }

                    machinesCostSum += machineCostInProcess;

                    // Calculate the investment cost for the machine.
                    decimal investment = machine.MachineInvestment.GetValueOrDefault()
                        + machine.SetupInvestment.GetValueOrDefault()
                        + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                        + machine.FundamentalSetupInvestment.GetValueOrDefault();

                    InvestmentCostSingle machineInvestment = new InvestmentCostSingle(machine.Guid, machine.Name, investment);
                    machineInvestment.Amount = machine.Amount;
                    stepCost.InvestmentCost.AddMachineInvestment(machineInvestment);
                }

                stepCost.MachineCost = machinesCostSum;
            }

            // Determine the shift model by dividing the no. of shifts per week by the number of working week days
            decimal shiftModel = 1;
            if (step.ShiftsPerWeek.GetValueOrDefault() != 0m && step.ProductionDaysPerWeek.GetValueOrDefault() != 0)
            {
                shiftModel = Math.Ceiling(step.ShiftsPerWeek.GetValueOrDefault() / step.ProductionDaysPerWeek.GetValueOrDefault());
            }

            // Select the shift charge value based of the shift model
            decimal shiftCharge = 0m;
            decimal laborAvailability = countrySettings.LaborAvailability.GetValueOrDefault(1m);
            if (shiftModel <= 1m)
            {
                shiftCharge = countrySettings.ShiftCharge1ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel == 2m)
            {
                shiftCharge = countrySettings.ShiftCharge2ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel >= 3m)
            {
                shiftCharge = countrySettings.ShiftCharge3ShiftModel.GetValueOrDefault();
            }

            // Calculate the costs for the different labor skill levels
            decimal unskilledLaborCost = 0m;
            decimal skilledLaborCost = 0m;
            decimal foremanCost = 0m;
            decimal technicianCost = 0m;
            decimal engineerCost = 0m;
            if (laborAvailability != 0m)
            {
                unskilledLaborCost = countrySettings.UnskilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                skilledLaborCost = countrySettings.SkilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                foremanCost = countrySettings.ForemanCost * (1m + shiftCharge) * (1m / laborAvailability);
                technicianCost = countrySettings.TechnicianCost * (1m + shiftCharge) * (1m / laborAvailability);
                engineerCost = countrySettings.EngineerCost * (1m + shiftCharge) * (1m / laborAvailability);
            }

            // Calculate the setup cost
            if (stepPartsPerCycle != 0m && stepCycleTime != 0m && stepBatchSize != 0)
            {
                int stepSetupsPerBatch = step.SetupsPerBatch.GetValueOrDefault();
                decimal stepSetupTime = step.SetupTime.GetValueOrDefault();
                decimal stepMaxDownTime = step.MaxDownTime.GetValueOrDefault();

                stepCost.SetupCost = (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                    * ((step.SetupUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.SetupSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.SetupForeman.GetValueOrDefault() * foremanCost)
                    + (step.SetupTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.SetupEngineers.GetValueOrDefault() * engineerCost)))
                    + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                    * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                    / stepBatchSize;

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal setupExtraShiftCostPerPart =
                        (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                        * ((step.SetupUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupForeman.GetValueOrDefault() * (foremanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupTechnicians.GetValueOrDefault() * (technicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupEngineers.GetValueOrDefault() * (engineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))))
                        + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                        * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                        / stepBatchSize;

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (setupExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.SetupCost += extraShiftsCost;
                }
            }

            // Calculate the labor cost
            if (stepPartsPerCycle != 0m && stepCycleTime != 0m)
            {
                stepCost.DirectLabourCost = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                    * ((step.ProductionUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.ProductionSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.ProductionForeman.GetValueOrDefault() * foremanCost)
                    + (step.ProductionTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.ProductionEngineers.GetValueOrDefault() * engineerCost));

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal directLaborExtraShiftCostPerPart =
                        (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                        * ((step.ProductionUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionForeman.GetValueOrDefault() * (foremanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionTechnicians.GetValueOrDefault() * (technicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionEngineers.GetValueOrDefault() * (engineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault()))));

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (directLaborExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.DirectLabourCost += extraShiftsCost;
                }
            }

            // Calculate the dies cost            
            if (step.Dies.Count > 0)
            {
                var dieCalcParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = partLifeCycleProductionQuantity };
                foreach (var die in step.Dies.Where(d => !d.IsDeleted))
                {
                    DieCost dieCost = this.CalculateDieCost(die, dieCalcParams);
                    stepCost.DieCosts.Add(dieCost);

                    stepCost.ToolAndDieCost += dieCost.Cost;
                    stepCost.DiesMaintenanceCost += dieCost.MaintenanceCost;

                    InvestmentCostSingle dieInvestment = new InvestmentCostSingle(die.Guid, die.Name, die.Investment.GetValueOrDefault());

                    decimal dieSetsPaidByCustomer = die.DiesetsNumberPaidByCustomer.GetValueOrDefault();
                    if (dieCost.DiesPerLifeTime >= dieSetsPaidByCustomer)
                    {
                        dieInvestment.Amount = dieCost.DiesPerLifeTime - dieSetsPaidByCustomer;
                    }

                    stepCost.InvestmentCost.AddDieInvestment(dieInvestment);
                }
            }

            // Calculate the manufacturing cost. Add ToolAndDieCost to ManufacturingCost only if the parent part's SBM is active.
            stepCost.ManufacturingCost = stepCost.MachineCost + stepCost.SetupCost + stepCost.DirectLabourCost + stepCost.DiesMaintenanceCost;
            if (toolingActive)
            {
                stepCost.ManufacturingCost += stepCost.ToolAndDieCost;
            }

            // Calculate the scrap and overhead.
            // CHANGE: If the step defines the Manufacturing Overhead Ratio use that to calculate the manufacturing overhead,
            // not the value from the inputted Overhead Settings.
            stepCost.ManufacturingOverheadRate =
                step.ManufacturingOverhead.HasValue ? step.ManufacturingOverhead.Value : overheadSettings.ManufacturingOverhead;
            stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * stepCost.ManufacturingOverheadRate;
            stepCost.RejectCost = stepCost.ManufacturingCost * step.ScrapAmount.GetValueOrDefault();

            return stepCost;
        }

        /// <summary>
        /// Calculates the capacity utilization of every machine in the specified process.
        /// The process's and every machine's cost must be calculated before calling this method.
        /// </summary>
        /// <param name="processSteps">The steps of the process.</param>
        /// <param name="processCost">The process's cost.</param>
        /// <param name="partBatchesPerYear">The batches per year of the parent part/assembly.</param>
        /// <param name="yearlyProductionQuantity">The yearly production quantity of the parent part/assembly.</param>
        protected virtual void CalculateMachinesCapacityUtilization(
            IEnumerable<ProcessStep> processSteps,
            ProcessCost processCost,
            decimal partBatchesPerYear,
            decimal yearlyProductionQuantity)
        {
            // Calculate the capacity utilization of every machine in the process.
            foreach (var step in processSteps)
            {
                ProcessStepCost stepCost = processCost.StepCosts.FirstOrDefault(sc => sc.StepId == step.Guid);
                if (stepCost != null)
                {
                    foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                    {
                        MachineCost machineCost = stepCost.MachineCosts.FirstOrDefault(mc => mc.MachineId == machine.Guid);
                        if (machineCost == null)
                        {
                            continue;
                        }

                        // The production capacity per year in hours            
                        decimal productionCapacityPerYear = step.ShiftsPerWeek.GetValueOrDefault() * step.HoursPerShift.GetValueOrDefault()
                            * step.ProductionWeeksPerYear.GetValueOrDefault();

                        // The number of hours per year that the machine is down (does not produce anything).
                        decimal yearlyMachineDownTime = (step.SetupsPerBatch.GetValueOrDefault() * step.MaxDownTime.GetValueOrDefault() * partBatchesPerYear) / 3600m;

                        // The number of hours per year the machine is available for work
                        decimal machineAvailabilityPerYear =
                            ((productionCapacityPerYear - yearlyMachineDownTime) * machine.Amount) * machine.Availability.GetValueOrDefault();

                        decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
                        decimal stepPartsperCycle = step.PartsPerCycle.GetValueOrDefault();

                        if (stepProcessTime != 0m && stepPartsperCycle != 0m)
                        {
                            decimal theoreticalCapacityPerHour = 3600m / (stepProcessTime / stepPartsperCycle);
                            decimal theoreticalCapacityPerYear = Math.Floor(theoreticalCapacityPerHour * machineAvailabilityPerYear);

                            decimal neededPartsPerYear = processCost.RejectOverview.GrossBatchSize * partBatchesPerYear;
                            if (theoreticalCapacityPerYear != 0m)
                            {
                                decimal capacityUtilization = neededPartsPerYear / theoreticalCapacityPerYear;

                                // If the calculated capacity utilization is over 100% but less than 100.05% the machine overload check determines that
                                // the machine is not overloaded so we adjust the capacity utilization down to 100% to be in sync with the overload check.
                                if (capacityUtilization > 1m && capacityUtilization < 1.0005m)
                                {
                                    capacityUtilization = 1m;
                                }
                                else if (capacityUtilization >= 1.0005m && capacityUtilization < 1.005m)
                                {
                                    // If the utilization is 100.05% or more but less than 100.5% the overload check determines that the machine is overloaded
                                    // so we have to set the utilization to 100.5% so the rounding makes it 101%, which means overloading.
                                    capacityUtilization = 1.01m;
                                }

                                machineCost.CapacityUtilization = Math.Round(capacityUtilization * 100m) / 100m;
                            }
                        }
                    }
                }
            }
        }
    }
}
