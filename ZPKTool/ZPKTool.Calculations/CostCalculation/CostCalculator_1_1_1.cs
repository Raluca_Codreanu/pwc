﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The cost calculator implementation v1.1.1
    /// </summary>
    internal class CostCalculator_1_1_1 : CostCalculator_1_1, ICostCalculator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1_1" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_1_1(BasicSetting basicSettings)
            : base(basicSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1_1" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_1_1(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : base(basicSettings, measurementUnits)
        {
        }

        #region ICostCalculator implementation

        /// <summary>
        /// Computes the cost of an Assembly.
        /// </summary>
        /// <param name="assembly">The assembly whose cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the cost calculation.</param>
        /// <returns>
        /// An instance of <see cref="CalculationResult" /> that contains the cost breakdown of the assembly.
        /// </returns>
        public override CalculationResult CalculateAssemblyCost(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            this.CheckCalculationData(assembly);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = assembly.Number;
            calcResult.Name = assembly.Name;
            calcResult.Remark = string.Empty;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = assembly.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = assembly.TargetPrice;
            calcResult.PurchasePrice = assembly.PurchasePrice;
            calcResult.AdditionalCostsNotes = assembly.AdditionalCostsNotes;

            var overheadSettings = assembly.OverheadSettings;

            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Compute the cost of sub-parts and sub-assemblies. If the assembly's cost is any type of estimation, sub parts are not included.
                // This must be done before calculating the process cost because the sub-parts costs are necessary for the enhanced scrap calculation.
                SubPartsAndSubAssembliesCost cost = this.CalculateSubPartsAndSubAssembliesCost(assembly, parameters);

                calcResult.AssembliesCost.SumWith(cost.SubAssembliesCost);
                calcResult.PartsCost.SumWith(cost.SubPartsCost);

                // The external parts OH and margin are added to the commodity OH and margin.
                calcResult.OverheadCost.CommodityOverhead += cost.ExternalOverhead;
                calcResult.OverheadCost.CommodityMargin += cost.ExternalMargin;
                calcResult.SubpartsAndSubassembliesCost = cost.TotalCost;

                // Calculate the cost of the assembling process and add the results to the calculation result object.
                var processCalcParams = new AssemblyProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    SubAssembliesCost = calcResult.AssembliesCost,
                    SubPartsCost = calcResult.PartsCost,
                    ProjectCreateDate = parameters.ProjectCreateDate
                };

                ProcessCost processCost = this.CalculateProcessCost(assembly, processCalcParams);
                calcResult.ProcessCost = processCost;

                calcResult.CommoditiesCost.Add(processCost.CommoditiesCost);
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on 100% Manufacturing Ratio (the full estimated cost is considered to be manufacturing cost)                
                calcResult.ProcessCost.ManufacturingCostSum = assembly.EstimatedCost.GetValueOrDefault();
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = assembly.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, assembly, productionCost, logisticCostRatio);
            }

            // Calculate all overheads and margins and save them in the result object. If the assembly's cost is calculated externally no overheads are applied.
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * assembly.PaymentTerms * (assembly.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the assembly's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(assembly, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Part or Raw Part.
        /// </summary>
        /// <param name="part">The part whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown of the given part.
        /// </returns>
        public override CalculationResult CalculatePartCost(Part part, PartCostCalculationParameters parameters)
        {
            this.CheckCalculationData(part);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = part.Number;
            calcResult.Name = part.Name;
            calcResult.Remark = part.AdditionalRemarks;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = part.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = part.TargetPrice;
            calcResult.PurchasePrice = part.PurchasePrice;
            calcResult.AdditionalCostsNotes = part.AdditionalCostsNotes;

            var overheadSettings = part.OverheadSettings;

            // Compute the cost the part's raw materials.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var materialParams = new RawMaterialCostCalculationParameters() { MaterialOverheadRate = overheadSettings.MaterialOverhead };
                var materialsCost = this.CalculateRawMaterialsCost(part.RawMaterials, materialParams);
                calcResult.RawMaterialsCost.Add(materialsCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.RoughCalculation || calculationAccuracy == PartCalculationAccuracy.Estimation)
            {
                // Calculate the materials cost using the Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost that is allocated
                // as ManufacturingCost and the difference is allocated as Material Cost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.RawMaterialsCost.NetCostSum = estimatedCost - (estimatedCost * manufacturingRatio);
                calcResult.RawMaterialsCost.OverheadSum = calcResult.RawMaterialsCost.NetCostSum * overheadSettings.MaterialOverhead;
            }

            // Compute the cost of the part's commodities            
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var commodityParams = new CommodityCostCalculationParameters()
                {
                    CommodityOverheadRate = overheadSettings.CommodityOverhead,
                    CommodityMarginRate = overheadSettings.CommodityMargin
                };

                CommoditiesCost commodityCost = this.CalculateCommoditiesCost(part.Commodities, commodityParams);
                calcResult.CommoditiesCost.Add(commodityCost);
            }

            // Compute the cost of the part's manufacturing process and add the cost data in the calculation result object
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var calcParams = new PartProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    RawMaterialsCost = calcResult.RawMaterialsCost,
                    CommoditiesCost = calcResult.CommoditiesCost,
                    ProjectCreateDate = parameters.ProjectCreateDate
                };

                var processCost = this.CalculateProcessCost(part, calcParams);
                calcResult.ProcessCost = processCost;
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on the part's Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost
                // that is allocated as ManufacturingCost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.ProcessCost.ManufacturingCostSum = estimatedCost * manufacturingRatio;
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = part.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, part, productionCost, logisticCostRatio);
            }

            // Calculate the overheads. If the part's cost is calculated externally, the overheads are not applied
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * part.PaymentTerms * (part.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary.
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(part, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in an assembly's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        public override decimal CalculateGrossLifetimeProductionQuantity(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            CalculationResult result = this.CalculateAssemblyCost(assembly, parameters);
            return result.ProcessCost.RejectOverview.GrossBatchSize * assembly.BatchSizePerYear.GetValueOrDefault() * assembly.LifeTime.GetValueOrDefault();
        }

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in a part's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        public override decimal CalculateGrossLifetimeProductionQuantity(Part part, PartCostCalculationParameters parameters)
        {
            CalculationResult result = this.CalculatePartCost(part, parameters);
            return result.ProcessCost.RejectOverview.GrossBatchSize * part.BatchSizePerYear.GetValueOrDefault() * part.LifeTime.GetValueOrDefault();
        }

        #endregion ICostCalculator implementation

        #region Helper methods

        /// <summary>
        /// Calculates the cost of an assembling process (the process of an assembly).
        /// </summary>
        /// <param name="assembly">The assembly whose process cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>        
        protected override ProcessCost CalculateProcessCost(Assembly assembly, AssemblyProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || assembly.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();

            // Calculate for each step the cumulated not reusable material cost (each step's not reusable cost will also contain the not reusable cost
            // of the previous steps)
            Dictionary<Guid, decimal> notReusableCosts =
                this.CalculateCostOfNotReusableSubAssembliesAndParts(assembly, parameters.SubAssembliesCost, parameters.SubPartsCost);

            // Initialize the calculation parameters for commodities and consumables. These parameters are the same for all steps.
            var commodityCalcParams = new CommodityCostCalculationParameters()
            {
                CommodityOverheadRate = assembly.OverheadSettings.CommodityOverhead,
                CommodityMarginRate = assembly.OverheadSettings.CommodityMargin
            };
            var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = assembly.OverheadSettings.ConsumableOverhead };

            // Calculate the batch size target
            decimal partsPerYear = assembly.YearlyProductionQuantity.GetValueOrDefault();
            decimal batchesPerYear = assembly.BatchSizePerYear.GetValueOrDefault();
            decimal batchSizeTarget = 0m;
            if (batchesPerYear != 0m)
            {
                batchSizeTarget = Math.Ceiling(partsPerYear / batchesPerYear);
            }

            decimal totalNecessaryBatchSize = batchSizeTarget;

            // Calculating the step costs in reverse order is a requirement for the enhanced scrap calculation algorithm
            foreach (var step in assembly.Process.Steps.OrderByDescending(step => step.Index))
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    var commoditiesCost = this.CalculateCommoditiesCost(step.Commodities, commodityCalcParams);
                    processCost.CommoditiesCost.Add(commoditiesCost);

                    var consumablesCost = this.CalculateConsumablesCost(step.Consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var assyLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(assembly);

                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    assembly.CountrySettings,
                    assembly.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    assyLifetimeProdQty,
                    assembly.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);

                stepCost.ParentPartIsExternal = assembly.IsExternal.GetValueOrDefault();

                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    /*** Enhanced scrap calculation ***/

                    // The quantity of parts that must be outputted from this step.
                    // This step's output quantity is the previous step's process gross batch size (for the last step is the batch size target)
                    decimal processOutputQty = totalNecessaryBatchSize;

                    // The amount of parts that have to be produced in order to have an output of processOutputQty parts.
                    decimal processGrossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * processOutputQty);

                    // The number of scrapped parts in this step
                    decimal scrapParts = processGrossBatchSize - processOutputQty;

                    // The scrap cost for this step
                    decimal scrapRejectCost = 0m;
                    if (batchSizeTarget != 0m)
                    {
                        scrapRejectCost = scrapParts * notReusableCosts[step.Guid] / batchSizeTarget;
                    }

                    // The manufacturing reject cost per part
                    decimal manufacturingRejectCost = 0m;
                    if (batchSizeTarget != 0m && step.PartsPerCycle.GetValueOrDefault() != 0m)
                    {
                        manufacturingRejectCost = scrapParts * stepCost.ManufacturingCost / batchSizeTarget / step.PartsPerCycle.GetValueOrDefault();
                    }

                    stepCost.GrossBatchSize = processGrossBatchSize;
                    stepCost.NetBatchSize = processOutputQty;
                    stepCost.RejectMaterialCostPerPiece = scrapRejectCost;
                    stepCost.RejectManufacturingCostPerPiece = manufacturingRejectCost;
                    stepCost.RejectCost = stepCost.RejectMaterialCostPerPiece + stepCost.RejectManufacturingCostPerPiece;

                    totalNecessaryBatchSize = processGrossBatchSize;

                    /**********************************/
                }

                processCost.AddStepCost(stepCost);
            }

            processCost.RejectOverview.IOPiecesPerYear = partsPerYear;
            processCost.RejectOverview.BatchesPerYear = batchesPerYear;
            processCost.RejectOverview.IOPiecesPerBatch = batchSizeTarget;
            processCost.RejectOverview.GrossBatchSize = totalNecessaryBatchSize;
            processCost.RejectOverview.RejectedPiecesTotal = processCost.RejectOverview.GrossBatchSize - processCost.RejectOverview.IOPiecesPerBatch;

            return processCost;
        }

        /// <summary>
        /// Calculates the cumulated cost of not reusable sub assemblies and parts of the specified assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="subAssembliesCost">The costs of the assembly's sub-assemblies.</param>
        /// <param name="subPartsCost">The cost of the assembly's sub-parts.</param>
        /// <returns>A dictionary containing the sub-part/sub-assembly ID as key and its not reusable cost as value.</returns>
        private Dictionary<Guid, decimal> CalculateCostOfNotReusableSubAssembliesAndParts(
            Assembly assembly,
            AssembliesCost subAssembliesCost,
            PartsCost subPartsCost)
        {
            Dictionary<Guid, decimal> notReusableCosts = new Dictionary<Guid, decimal>();
            decimal previousStepNotReusableCost = 0m;
            foreach (var step in assembly.Process.Steps.OrderBy(step => step.Index))
            {
                decimal notReusableCost = 0m;
                foreach (var amount in step.AssemblyAmounts)
                {
                    var subAssy = assembly.Subassemblies.FirstOrDefault(a => a.Guid == amount.FindAssemblyId());
                    if (subAssy == null || subAssy.IsDeleted)
                    {
                        // The assembly referred to by the amount does not exist. This is the case when the assembly is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    if (!subAssy.Reusable)
                    {
                        AssemblyCost cost = subAssembliesCost.AssemblyCosts.FirstOrDefault(c => c.AssemblyId == subAssy.Guid);
                        if (cost != null)
                        {
                            notReusableCost += cost.TargetCost * amount.Amount;
                        }
                    }
                }

                foreach (var amount in step.PartAmounts)
                {
                    var part = assembly.Parts.FirstOrDefault(p => p.Guid == amount.FindPartId());
                    if (part == null || part.IsDeleted)
                    {
                        // The part referred to by the amount does not exist. This is the case when the part is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    if (!part.Reusable)
                    {
                        PartCost cost = subPartsCost.PartCosts.FirstOrDefault(c => c.PartId == part.Guid);
                        if (cost != null)
                        {
                            notReusableCost += cost.TargetCost * amount.Amount;
                        }
                    }
                }

                notReusableCost += previousStepNotReusableCost;
                previousStepNotReusableCost = notReusableCost;
                notReusableCosts[step.Guid] = notReusableCost;
            }

            return notReusableCosts;
        }

        /// <summary>
        /// Calculates the cost of a manufacturing process (the process of a part).
        /// </summary>
        /// <param name="part">The part whose process cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>
        protected override ProcessCost CalculateProcessCost(Part part, PartProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || part.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();

            // Initialize the calculation parameters for consumables. These parameters are the same for all steps.            
            var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = part.OverheadSettings.ConsumableOverhead };

            decimal batchSizeTarget = 0m;
            decimal batchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            if (batchesPerYear != 0m)
            {
                batchSizeTarget =
                    Math.Ceiling((decimal)part.YearlyProductionQuantity.GetValueOrDefault() / batchesPerYear);
            }

            decimal totalNecessaryBatchSize = batchSizeTarget;
            decimal previousStepScrapRejectCost = 0m;

            // Calculating the step costs in reverse order is a requirement for the enhanced scrap calculation algorithm
            foreach (var step in part.Process.Steps.OrderByDescending(step => step.Index))
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    ConsumablesCost consumablesCost = this.CalculateConsumablesCost(step.Consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var partLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(part);

                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    part.CountrySettings,
                    part.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    partLifetimeProdQty,
                    part.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);

                stepCost.ParentPartIsExternal = part.IsExternal.GetValueOrDefault();

                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    /*** Enhanced scrap calculation ***/

                    // The quantity of parts that must be outputted from this step.
                    // This step's output quantity is the previous step's process gross batch size (for the last step is the batch size target)
                    decimal processOutputQty = totalNecessaryBatchSize;

                    // The amount of parts that have to be produced in order to have an output of processOutputQty parts.
                    decimal processGrossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * processOutputQty);

                    decimal rejectedScrapCost = 0m;
                    decimal rejectedManufacturingCost = 0m;
                    if (batchSizeTarget != 0m)
                    {
                        // Scrap cost for the rejected parts
                        var materialsCost = parameters.RawMaterialsCost.NetCostSum + parameters.CommoditiesCost.CostsSum;
                        rejectedScrapCost = ((processGrossBatchSize - processOutputQty) * materialsCost / batchSizeTarget) + previousStepScrapRejectCost;

                        // Manufacturing cost for the rejected parts
                        rejectedManufacturingCost = ((processGrossBatchSize - processOutputQty) * stepCost.ManufacturingCost / batchSizeTarget)
                            * step.PartsPerCycle.GetValueOrDefault();
                    }

                    stepCost.GrossBatchSize = processGrossBatchSize;
                    stepCost.NetBatchSize = processOutputQty;
                    stepCost.RejectedPieces = stepCost.GrossBatchSize - stepCost.NetBatchSize;
                    stepCost.RejectMaterialCostPerPiece = rejectedScrapCost;
                    stepCost.RejectManufacturingCostPerPiece = rejectedManufacturingCost;
                    stepCost.RejectCost = stepCost.RejectMaterialCostPerPiece + stepCost.RejectManufacturingCostPerPiece;

                    totalNecessaryBatchSize = processGrossBatchSize;
                    previousStepScrapRejectCost = rejectedScrapCost;

                    /**********************************/
                }

                processCost.AddStepCost(stepCost);
            }

            processCost.RejectOverview.IOPiecesPerYear = part.YearlyProductionQuantity.GetValueOrDefault();
            processCost.RejectOverview.BatchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            processCost.RejectOverview.IOPiecesPerBatch = batchSizeTarget;
            processCost.RejectOverview.GrossBatchSize = totalNecessaryBatchSize;
            processCost.RejectOverview.RejectedPiecesTotal = processCost.RejectOverview.GrossBatchSize - processCost.RejectOverview.IOPiecesPerBatch;

            return processCost;
        }

        /// <summary>
        /// Computes the costs of a process step.
        /// </summary>
        /// <param name="step">The process step to calculate the costs for.</param>
        /// <param name="countrySettings">The country settings to be used during the calculations.</param>
        /// <param name="overheadSettings">The overhead settings to be used during the calculations.</param>
        /// <param name="projectDepreciationPeriod">The project depreciation period.</param>
        /// <param name="projectDepreciationRate">The project depreciation rate.</param>
        /// <param name="partLifeCycleProductionQuantity">The life cycle production quantity of the part/assembly to which the step belongs to.</param>
        /// <param name="toolingActive">a value indicating whether tooling is active for the process</param>
        /// <param name="projectCreateDate">The project create date.</param>
        /// <returns>
        /// An object containing the costs for the given process step.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The complexity of the calculation code can not be easily avoided.")]
        protected override ProcessStepCost CalculateProcessStepCost(
            ProcessStep step,
            CountrySetting countrySettings,
            OverheadSetting overheadSettings,
            int projectDepreciationPeriod,
            decimal projectDepreciationRate,
            decimal partLifeCycleProductionQuantity,
            bool toolingActive,
            DateTime? projectCreateDate)
        {
            ProcessStepCost stepCost = new ProcessStepCost();
            stepCost.StepId = step.Guid;
            stepCost.StepName = step.Name;
            stepCost.StepIndex = step.Index;
            stepCost.IsExternal = step.IsExternal.GetValueOrDefault();
            stepCost.SBMActive = toolingActive;
            stepCost.ManufacturingOverheadRate = overheadSettings.ManufacturingOverhead;

            // If the step calculation accuracy is estimated then the estimated price is the step's total cost.
            var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
            if (stepAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                stepCost.ManufacturingCostIsEstimated = true;
                stepCost.ManufacturingCost = step.Price.GetValueOrDefault();
                stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
                return stepCost;
            }

            // Copy the most used step parameters in local not nullable variables
            decimal stepPartsPerCycle = step.PartsPerCycle.GetValueOrDefault();
            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
            decimal stepCycleTime = step.CycleTime.GetValueOrDefault();
            int stepBatchSize = step.BatchSize.GetValueOrDefault();

            // Calculate machines cost
            if (step.Machines.Count > 0)
            {
                var machineCalcParams = new MachineCostCalculationParameters()
                {
                    CountrySettingsAirCost = countrySettings.AirCost,
                    CountrySettingsEnergyCost = countrySettings.EnergyCost,
                    CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost,
                    CountrySettingsWaterCost = countrySettings.WaterCost,
                    ProcessStepHoursPerShift = step.HoursPerShift.GetValueOrDefault(),
                    ProcessStepProductionWeeksPerYear = step.ProductionWeeksPerYear.GetValueOrDefault(),
                    ProcessStepShiftsPerWeek = step.ShiftsPerWeek.GetValueOrDefault(),
                    ProcessStepExtraShiftsPerWeek = step.ExtraShiftsNumber.GetValueOrDefault(),
                    ProjectDepreciationPeriod = projectDepreciationPeriod,
                    ProjectDepreciationRate = projectDepreciationRate,
                    ProjectCreateDate = projectCreateDate
                };

                decimal machinesCostSum = 0m;
                foreach (var machineData in step.Machines.Where(m => !m.IsDeleted))
                {
                    MachineCost machineCost = this.CalculateMachineCost(machineData, machineCalcParams);
                    stepCost.MachineCosts.Add(machineCost);

                    if (machineData.IsProjectSpecific)
                    {
                        if (partLifeCycleProductionQuantity != 0m && stepPartsPerCycle != 0m)
                        {
                            decimal totalInvest = machineData.MachineInvestment.GetValueOrDefault()
                                + machineData.SetupInvestment.GetValueOrDefault()
                                + machineData.AdditionalEquipmentInvestment.GetValueOrDefault()
                                + machineData.FundamentalSetupInvestment.GetValueOrDefault();

                            machinesCostSum += (totalInvest / partLifeCycleProductionQuantity) +
                                ((stepProcessTime / stepPartsPerCycle) * (machineCost.EnergyCostPerHour / SecondsInHour))
                                + machineCost.FloorCost;
                        }
                    }
                    else if (stepPartsPerCycle != 0m)
                    {
                        machinesCostSum += (machineCost.FullCostPerHour / SecondsInHour) * (stepProcessTime / stepPartsPerCycle);
                    }

                    // Calculate the investment cost for the machine
                    decimal investment = machineData.MachineInvestment.GetValueOrDefault()
                        + machineData.SetupInvestment.GetValueOrDefault()
                        + machineData.AdditionalEquipmentInvestment.GetValueOrDefault()
                        + machineData.FundamentalSetupInvestment.GetValueOrDefault();

                    stepCost.InvestmentCost.AddMachineInvestment(new InvestmentCostSingle(machineData.Guid, machineData.Name, investment));
                }

                stepCost.MachineCost = machinesCostSum;
            }

            // Determine the shift model by dividing the no. of shifts per week by the number of working week days
            decimal shiftModel = 1;
            if (step.ShiftsPerWeek.GetValueOrDefault() != 0m && step.ProductionDaysPerWeek.GetValueOrDefault() != 0)
            {
                shiftModel = Math.Ceiling(step.ShiftsPerWeek.GetValueOrDefault() / step.ProductionDaysPerWeek.GetValueOrDefault());
            }

            // Select the shift charge value based of the shift model
            decimal shiftCharge = 0m;
            decimal laborAvailability = countrySettings.LaborAvailability.GetValueOrDefault(1m);
            if (shiftModel <= 1m)
            {
                shiftCharge = countrySettings.ShiftCharge1ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel == 2m)
            {
                shiftCharge = countrySettings.ShiftCharge2ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel >= 3m)
            {
                shiftCharge = countrySettings.ShiftCharge3ShiftModel.GetValueOrDefault();
            }

            // Calculate the costs for the different labor skill levels
            decimal unskilledLaborCost = 0m;
            decimal skilledLaborCost = 0m;
            decimal foremanCost = 0m;
            decimal technicianCost = 0m;
            decimal engineerCost = 0m;
            if (laborAvailability != 0m)
            {
                unskilledLaborCost = countrySettings.UnskilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                skilledLaborCost = countrySettings.SkilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                foremanCost = countrySettings.ForemanCost * (1m + shiftCharge) * (1m / laborAvailability);
                technicianCost = countrySettings.TechnicianCost * (1m + shiftCharge) * (1m / laborAvailability);
                engineerCost = countrySettings.EngineerCost * (1m + shiftCharge) * (1m / laborAvailability);
            }

            // Calculate the setup cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0 && stepBatchSize != 0)
            {
                int stepSetupsPerBatch = step.SetupsPerBatch.GetValueOrDefault();
                decimal stepSetupTime = step.SetupTime.GetValueOrDefault();
                decimal stepMaxDownTime = step.MaxDownTime.GetValueOrDefault();

                stepCost.SetupCost = (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                    * ((step.SetupUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.SetupSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.SetupForeman.GetValueOrDefault() * foremanCost)
                    + (step.SetupTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.SetupEngineers.GetValueOrDefault() * engineerCost)))
                    + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                    * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                    / stepBatchSize;

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal setupExtraShiftCostPerPart =
                        (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                        * ((step.SetupUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupForeman.GetValueOrDefault() * (foremanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupTechnicians.GetValueOrDefault() * (technicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupEngineers.GetValueOrDefault() * (engineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))))
                        + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                        * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                        / stepBatchSize;

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (setupExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.SetupCost += extraShiftsCost;
                }
            }

            // Calculate the labor cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0)
            {
                stepCost.DirectLabourCost = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                    * ((step.ProductionUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.ProductionSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.ProductionForeman.GetValueOrDefault() * foremanCost)
                    + (step.ProductionTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.ProductionEngineers.GetValueOrDefault() * engineerCost));

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal directLaborExtraShiftCostPerPart = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                        * ((step.ProductionUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionForeman.GetValueOrDefault() * (foremanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionTechnicians.GetValueOrDefault() * (technicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionEngineers.GetValueOrDefault() * (engineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault()))));

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (directLaborExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.DirectLabourCost += extraShiftsCost;
                }
            }

            // Calculate the dies cost
            if (step.Dies.Count > 0)
            {
                var dieCalcParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = partLifeCycleProductionQuantity };
                foreach (var die in step.Dies.Where(d => !d.IsDeleted))
                {
                    DieCost dieCost = this.CalculateDieCost(die, dieCalcParams);
                    stepCost.DieCosts.Add(dieCost);

                    stepCost.ToolAndDieCost += dieCost.Cost;
                    stepCost.DiesMaintenanceCost += dieCost.MaintenanceCost;

                    stepCost.InvestmentCost.AddDieInvestment(new InvestmentCostSingle(die.Guid, die.Name, die.Investment.GetValueOrDefault()));
                }
            }

            // Calculate the manufacturing cost. Add ToolAndDieCost to ManufacturingCost only if the parent part's SBM is active.
            stepCost.ManufacturingCost = stepCost.MachineCost + stepCost.SetupCost + stepCost.DirectLabourCost + stepCost.DiesMaintenanceCost;
            if (toolingActive)
            {
                stepCost.ManufacturingCost += stepCost.ToolAndDieCost;
            }

            // Calculate the scrap and overhead.
            stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
            stepCost.RejectCost = stepCost.ManufacturingCost * step.ScrapAmount.GetValueOrDefault();

            return stepCost;
        }

        #endregion Helper methods
    }
}
