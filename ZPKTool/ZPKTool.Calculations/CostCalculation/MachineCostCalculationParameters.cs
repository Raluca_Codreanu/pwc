﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// Contains the input parameters for calculating the cost of a Machine.
    /// </summary>
    public class MachineCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the depreciation period of the project to which the machine belongs.
        /// </summary>        
        public int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the project to which the machine belongs.
        /// </summary>        
        public decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the shifts per week value of the process step to which the machine belongs.
        /// </summary>        
        public decimal ProcessStepShiftsPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the extra shifts per week value of the process step to which the machine belongs.
        /// </summary>
        public decimal ProcessStepExtraShiftsPerWeek { get; set; }

        /// <summary>
        /// Gets or sets the hours per shift value of the process step to which the machine belongs.
        /// </summary>        
        public decimal ProcessStepHoursPerShift { get; set; }

        /// <summary>
        /// Gets or sets the production weeks per year value of the process step to which the machine belongs.
        /// </summary>        
        public decimal ProcessStepProductionWeeksPerYear { get; set; }

        /// <summary>
        /// Gets or sets the energy cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>        
        public decimal CountrySettingsEnergyCost { get; set; }

        /// <summary>
        /// Gets or sets the air cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>        
        public decimal CountrySettingsAirCost { get; set; }

        /// <summary>
        /// Gets or sets the water cost from the country settings of the entity to whose process the machine belongs.
        /// </summary>   
        public decimal CountrySettingsWaterCost { get; set; }

        /// <summary>
        /// Gets or sets the rental cost for production area from the country settings of the entity to whose process the machine belongs.
        /// </summary>
        public decimal CountrySettingsRentalCostProductionArea { get; set; }

        /// <summary>
        /// Gets or sets the parent project (if it exists) creation date.
        /// </summary>
        public DateTime? ProjectCreateDate { get; set; }
    }
}
