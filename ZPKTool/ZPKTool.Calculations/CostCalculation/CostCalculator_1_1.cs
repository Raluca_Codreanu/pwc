﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The cost calculator implementation v1.1
    /// </summary>
    internal class CostCalculator_1_1 : CostCalculator_1_0, ICostCalculator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_1(BasicSetting basicSettings)
            : base(basicSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_1" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_1(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : base(basicSettings, measurementUnits)
        {
        }

        #region ICostCalculator implementation

        /// <summary>
        /// Computes the cost of an Assembly.
        /// </summary>
        /// <param name="assembly">The assembly whose cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the cost calculation.</param>
        /// <returns>
        /// An instance of <see cref="CalculationResult" /> that contains the cost breakdown of the assembly.
        /// </returns>
        public override CalculationResult CalculateAssemblyCost(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            this.CheckCalculationData(assembly);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = assembly.Number;
            calcResult.Name = assembly.Name;
            calcResult.Remark = string.Empty;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = assembly.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = assembly.TargetPrice;
            calcResult.PurchasePrice = assembly.PurchasePrice;
            calcResult.AdditionalCostsNotes = assembly.AdditionalCostsNotes;

            var overheadSettings = assembly.OverheadSettings;

            // Calculate the cost of the assembling process and add the results to the calculation result object.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var processCalcParams = new AssemblyProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    ProjectCreateDate = parameters.ProjectCreateDate
                };
                ProcessCost processCost = this.CalculateProcessCost(assembly, processCalcParams);

                calcResult.ProcessCost = processCost;
                calcResult.CommoditiesCost.Add(processCost.CommoditiesCost);
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on 100% Manufacturing Ratio (the full estimated cost is considered to be manufacturing cost)
                calcResult.ProcessCost.ManufacturingCostSum = assembly.EstimatedCost.GetValueOrDefault();
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = assembly.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, assembly, productionCost, logisticCostRatio);
            }

            // Calculate all overheads and margins and save them in the result object. If the assembly's cost is calculated externally no overheads are applied.
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Compute the cost of sub-parts and sub-assemblies. If the assembly's cost is any type of estimation, single parts are not included.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                SubPartsAndSubAssembliesCost cost = this.CalculateSubPartsAndSubAssembliesCost(assembly, parameters);

                calcResult.AssembliesCost.SumWith(cost.SubAssembliesCost);
                calcResult.PartsCost.SumWith(cost.SubPartsCost);

                // The external parts OH and margin are added to the commodity OH and margin.
                calcResult.OverheadCost.CommodityOverhead += cost.ExternalOverhead;
                calcResult.OverheadCost.CommodityMargin += cost.ExternalMargin;
                calcResult.SubpartsAndSubassembliesCost = cost.TotalCost;
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * assembly.PaymentTerms * (assembly.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(assembly, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Part or Raw Part.
        /// </summary>
        /// <param name="part">The part whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown of the given part.
        /// </returns>
        public override CalculationResult CalculatePartCost(Part part, PartCostCalculationParameters parameters)
        {
            this.CheckCalculationData(part);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = part.Number;
            calcResult.Name = part.Name;
            calcResult.Remark = part.AdditionalRemarks;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = part.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = part.TargetPrice;
            calcResult.PurchasePrice = part.PurchasePrice;
            calcResult.AdditionalCostsNotes = part.AdditionalCostsNotes;

            var overheadSettings = part.OverheadSettings;

            // Compute the cost the part's raw materials.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var materialParams = new RawMaterialCostCalculationParameters() { MaterialOverheadRate = overheadSettings.MaterialOverhead };
                var materialsCost = this.CalculateRawMaterialsCost(part.RawMaterials, materialParams);
                calcResult.RawMaterialsCost.Add(materialsCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.RoughCalculation || calculationAccuracy == PartCalculationAccuracy.Estimation)
            {
                // Calculate the materials cost using the Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost that is allocated
                // as ManufacturingCost and the difference is allocated as Material Cost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.RawMaterialsCost.NetCostSum = estimatedCost - (estimatedCost * manufacturingRatio);
                calcResult.RawMaterialsCost.OverheadSum = calcResult.RawMaterialsCost.NetCostSum * overheadSettings.MaterialOverhead;
            }

            // Compute the cost of the part's commodities            
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var commodityParams = new CommodityCostCalculationParameters()
                {
                    CommodityOverheadRate = overheadSettings.CommodityOverhead,
                    CommodityMarginRate = overheadSettings.CommodityMargin
                };

                CommoditiesCost commodityCost = this.CalculateCommoditiesCost(part.Commodities, commodityParams);
                calcResult.CommoditiesCost.Add(commodityCost);
            }

            // Compute the cost of the part's manufacturing process and add the cost data in the calculation result object
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var calcParams = new PartProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    ProjectCreateDate = parameters.ProjectCreateDate
                };

                var processCost = this.CalculateProcessCost(part, calcParams);
                calcResult.ProcessCost = processCost;
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on the part's Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost
                // that is allocated as ManufacturingCost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.ProcessCost.ManufacturingCostSum = estimatedCost * manufacturingRatio;
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = part.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, part, productionCost, logisticCostRatio);
            }

            // Calculate the overheads. If the part's cost is calculated externally, the overheads are not applied
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * part.PaymentTerms * (part.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary.
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(part, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Die.
        /// </summary>
        /// <param name="die">The die to calculate the cost for.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The cost breakdown for the die.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The die was null.
        /// or
        /// The die parameters were null.
        /// </exception>
        public override DieCost CalculateDieCost(Die die, DieCostCalculationParameters parameters)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The die parameters were null.");
            }

            DieCost dieCost = new DieCost();
            dieCost.DieId = die.Guid;
            dieCost.DieName = die.Name;

            decimal dieLifeTime = die.LifeTime.GetValueOrDefault();
            decimal dieInvestment = die.Investment.GetValueOrDefault();

            // Compute the number of dies per lifetime
            decimal diesPerLifeTime = 0m;
            if (dieLifeTime != 0m)
            {
                diesPerLifeTime = parameters.ParentLifecycleProductionQuantity / dieLifeTime;

                // Dies per lifetime must be at least 1
                if (diesPerLifeTime >= 1)
                {
                    diesPerLifeTime = Math.Ceiling(diesPerLifeTime);
                }
                else
                {
                    diesPerLifeTime = 1m;
                }
            }

            dieCost.DiesPerLifeTime = diesPerLifeTime;

            decimal partsNo = 0m;
            if (die.CostAllocationBasedOnPartsPerLifeTime.GetValueOrDefault())
            {
                partsNo = parameters.ParentLifecycleProductionQuantity;
            }
            else
            {
                partsNo = die.CostAllocationNumberOfParts.GetValueOrDefault();
            }

            decimal maintenance =
                die.IsMaintenanceInPercentage.GetValueOrDefault() ? die.Maintenance.GetValueOrDefault() * dieInvestment : die.Maintenance.GetValueOrDefault();
            if (dieLifeTime != 0m)
            {
                dieCost.MaintenanceCost = maintenance / dieLifeTime;
            }

            // Compute the die cost
            if (partsNo != 0m)
            {
                decimal dieSetsNumberPaidByCustomer = die.DiesetsNumberPaidByCustomer.GetValueOrDefault();
                if (dieSetsNumberPaidByCustomer < diesPerLifeTime)
                {
                    dieCost.Cost = (diesPerLifeTime - dieSetsNumberPaidByCustomer) * dieInvestment * die.AllocationRatio.GetValueOrDefault() / partsNo;
                }
                else
                {
                    decimal wear = die.IsWearInPercentage.GetValueOrDefault() ? die.Wear.GetValueOrDefault() * dieInvestment : die.Wear.GetValueOrDefault();
                    dieCost.Cost = (maintenance + wear) / partsNo;
                }
            }

            return dieCost;
        }

        /// <summary>
        /// Calculates the cost of a Machine.
        /// </summary>
        /// <param name="machine">The machine whose cost to calculate.</param>
        /// <param name="parameters">The input parameters needed for calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the machine.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// The machine was null.
        /// or
        /// The machine parameters were null.
        /// </exception>
        public override MachineCost CalculateMachineCost(Machine machine, MachineCostCalculationParameters parameters)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The machine parameters were null.");
            }

            MachineCost cost = new MachineCost();
            cost.MachineId = machine.Guid;
            cost.MachineName = machine.Name;
            cost.MaintenanceNotes = machine.MaintenanceNotes;

            decimal yearlyProductionHours =
                (parameters.ProcessStepShiftsPerWeek * parameters.ProcessStepHoursPerShift * parameters.ProcessStepProductionWeeksPerYear)
                + (parameters.ProcessStepExtraShiftsPerWeek * parameters.ProcessStepHoursPerShift * parameters.ProcessStepProductionWeeksPerYear);

            if (yearlyProductionHours != 0m)
            {
                cost.FloorCost = ((machine.FloorSize.GetValueOrDefault() + machine.WorkspaceArea.GetValueOrDefault())
                    * parameters.CountrySettingsRentalCostProductionArea
                    * MonthsInYear)
                    / yearlyProductionHours;
            }

            decimal machineInvestment = this.CalculateMachineInvestment(machine);

            // The linear depreciation is used when the machine's age is larger than the depreciation period.
            int depreciationPeriod = machine.DepreciationPeriod.HasValue ? machine.DepreciationPeriod.Value : parameters.ProjectDepreciationPeriod;
            bool useLinearDepreciation = false;
            if (machine.ManufacturingYear.HasValue)
            {
                /*
                 * Set as accurately as possible the reference DateTime,
                 * relative to which is calculated the machine depreciation.
                 * If the machine CalculationCreationDate has been set, then we use it.
                 * Otherwise, if the Project CreateDate has been set, , then we use it.
                 * Otherwise we use the current date.
                 */
                var firstCalculationDate = machine.CalculationCreateDate.HasValue
                    ? machine.CalculationCreateDate
                    : parameters.ProjectCreateDate.HasValue
                        ? parameters.ProjectCreateDate.Value
                        : DateTime.Now;
                TimeSpan timeDiff = firstCalculationDate.Value - new DateTime((int)machine.ManufacturingYear.Value, 1, 1);
                if (timeDiff.TotalDays > depreciationPeriod * 365)
                {
                    useLinearDepreciation = true;
                }
            }

            if (useLinearDepreciation)
            {
                cost.DepreciationPerHour = 0m;
            }
            else if (depreciationPeriod != 0m && yearlyProductionHours != 0m && machine.Availability.GetValueOrDefault() != 0m)
            {
                decimal depreciationRate = machine.DepreciationRate.HasValue ? machine.DepreciationRate.Value : parameters.ProjectDepreciationRate;
                decimal totalMachineInvestment = machineInvestment
                    + machine.SetupInvestment.GetValueOrDefault()
                    + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                    + machine.FundamentalSetupInvestment.GetValueOrDefault();
                cost.DepreciationPerHour = ((totalMachineInvestment / depreciationPeriod) + (totalMachineInvestment * depreciationRate))
                    / (yearlyProductionHours * machine.Availability.Value);
            }

            if (yearlyProductionHours != 0m)
            {
                // Determine the consumables cost.
                decimal consumablesCost = 0m;
                if (machine.ManualConsumableCostCalc)
                {
                    consumablesCost = machine.ManualConsumableCostPercentage.GetValueOrDefault() * machineInvestment;
                    cost.CalculatedConsumablesCost = consumablesCost;
                }
                else
                {
                    consumablesCost = machine.ConsumablesCost.GetValueOrDefault();
                }

                // Calculate the maintenance cost
                if (machine.CalculateWithKValue.GetValueOrDefault())
                {
                    cost.MaintenancePerHour = ((machine.KValue.GetValueOrDefault() * machineInvestment) + consumablesCost) / yearlyProductionHours;
                }
                else
                {
                    cost.MaintenancePerHour = (machine.ExternalWorkCost.GetValueOrDefault() + machine.MaterialCost.GetValueOrDefault()
                        + consumablesCost + machine.RepairsCost.GetValueOrDefault())
                        / yearlyProductionHours;
                }
            }

            cost.EnergyCostPerHour = ((machine.PowerConsumption.GetValueOrDefault() * parameters.CountrySettingsEnergyCost)
                + (machine.AirConsumption.GetValueOrDefault() * parameters.CountrySettingsAirCost)
                + (machine.WaterConsumption.GetValueOrDefault() * parameters.CountrySettingsWaterCost))
                * machine.FullLoadRate.GetValueOrDefault();

            cost.FullCostPerHour = cost.DepreciationPerHour + cost.MaintenancePerHour + cost.EnergyCostPerHour + cost.FloorCost;

            return cost;
        }

        #endregion ICostCalculator implementation

        #region Helper methods

        /// <summary>
        /// Computes the costs of a process step.
        /// </summary>
        /// <param name="step">The process step to calculate the costs for.</param>
        /// <param name="countrySettings">The country settings to be used during the calculations.</param>
        /// <param name="overheadSettings">The overhead settings to be used during the calculations.</param>
        /// <param name="projectDepreciationPeriod">The project depreciation period.</param>
        /// <param name="projectDepreciationRate">The project depreciation rate.</param>
        /// <param name="partLifeCycleProductionQuantity">The life cycle production quantity of the part/assembly to which the step belongs to.</param>
        /// <param name="toolingActive">a value indicating whether tooling is active for the process</param>
        /// <param name="projectCreateDate">The project create date.</param>
        /// <returns>
        /// An object containing the costs for the given process step.
        /// </returns>
        protected override ProcessStepCost CalculateProcessStepCost(
            ProcessStep step,
            CountrySetting countrySettings,
            OverheadSetting overheadSettings,
            int projectDepreciationPeriod,
            decimal projectDepreciationRate,
            decimal partLifeCycleProductionQuantity,
            bool toolingActive,
            DateTime? projectCreateDate)
        {
            ProcessStepCost stepCost = new ProcessStepCost();
            stepCost.StepId = step.Guid;
            stepCost.StepName = step.Name;
            stepCost.StepIndex = step.Index;
            stepCost.IsExternal = step.IsExternal.GetValueOrDefault();
            stepCost.SBMActive = toolingActive;
            stepCost.ManufacturingOverheadRate = overheadSettings.ManufacturingOverhead;

            // If the step calculation accuracy is estimated then the estimated price is the step's total cost.
            var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
            if (stepAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                stepCost.ManufacturingCostIsEstimated = true;
                stepCost.ManufacturingCost = step.Price.GetValueOrDefault();
                stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
                return stepCost;
            }

            // Copy the most used step parameters in local not nullable variables
            decimal stepPartsPerCycle = step.PartsPerCycle.GetValueOrDefault();
            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
            decimal stepCycleTime = step.CycleTime.GetValueOrDefault();
            int stepBatchSize = step.BatchSize.GetValueOrDefault();

            // Calculate machines cost
            if (step.Machines.Count > 0)
            {
                var machineCalculationParams = new MachineCostCalculationParameters()
                {
                    CountrySettingsAirCost = countrySettings.AirCost,
                    CountrySettingsEnergyCost = countrySettings.EnergyCost,
                    CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost,
                    CountrySettingsWaterCost = countrySettings.WaterCost,
                    ProcessStepHoursPerShift = step.HoursPerShift.GetValueOrDefault(),
                    ProcessStepShiftsPerWeek = step.ShiftsPerWeek.GetValueOrDefault(),
                    ProcessStepProductionWeeksPerYear = step.ProductionWeeksPerYear.GetValueOrDefault(),
                    ProcessStepExtraShiftsPerWeek = step.ExtraShiftsNumber.GetValueOrDefault(),
                    ProjectDepreciationPeriod = projectDepreciationPeriod,
                    ProjectDepreciationRate = projectDepreciationRate,
                    ProjectCreateDate = projectCreateDate
                };

                decimal machinesCostSum = 0m;
                foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                {
                    MachineCost machineCost = this.CalculateMachineCost(machine, machineCalculationParams);
                    stepCost.MachineCosts.Add(machineCost);

                    if (machine.IsProjectSpecific)
                    {
                        if (partLifeCycleProductionQuantity != 0m && stepPartsPerCycle != 0m)
                        {
                            decimal totalInvest = machine.MachineInvestment.GetValueOrDefault()
                                + machine.SetupInvestment.GetValueOrDefault()
                                + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                                + machine.FundamentalSetupInvestment.GetValueOrDefault();

                            machinesCostSum += (totalInvest / partLifeCycleProductionQuantity) +
                                ((stepProcessTime / stepPartsPerCycle) * (machineCost.EnergyCostPerHour / SecondsInHour))
                                + machineCost.FloorCost;
                        }
                    }
                    else if (stepPartsPerCycle != 0m)
                    {
                        machinesCostSum += (machineCost.FullCostPerHour / SecondsInHour) * (stepProcessTime / stepPartsPerCycle);
                    }

                    // Calculate the investment cost for the machine
                    decimal investment = machine.MachineInvestment.GetValueOrDefault()
                        + machine.SetupInvestment.GetValueOrDefault()
                        + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                        + machine.FundamentalSetupInvestment.GetValueOrDefault();

                    stepCost.InvestmentCost.AddMachineInvestment(new InvestmentCostSingle(machine.Guid, machine.Name, investment));
                }

                stepCost.MachineCost = machinesCostSum;
            }

            // Calculate the setup cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0 && stepBatchSize != 0)
            {
                int stepSetupsPerBatch = step.SetupsPerBatch.GetValueOrDefault();
                decimal stepSetupTime = step.SetupTime.GetValueOrDefault();
                decimal stepMaxDownTime = step.MaxDownTime.GetValueOrDefault();

                stepCost.SetupCost = (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                    * ((step.SetupUnskilledLabour.GetValueOrDefault() * countrySettings.UnskilledLaborCost)
                    + (step.SetupSkilledLabour.GetValueOrDefault() * countrySettings.SkilledLaborCost)
                    + (step.SetupForeman.GetValueOrDefault() * countrySettings.ForemanCost)
                    + (step.SetupTechnicians.GetValueOrDefault() * countrySettings.TechnicianCost)
                    + (step.SetupEngineers.GetValueOrDefault() * countrySettings.EngineerCost)))
                    + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                    * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                    / stepBatchSize;

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal setupExtraShiftCostPerPart =
                        (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                        * ((step.SetupUnskilledLabour.GetValueOrDefault() * (countrySettings.UnskilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupSkilledLabour.GetValueOrDefault() * (countrySettings.SkilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupForeman.GetValueOrDefault() * (countrySettings.ForemanCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupTechnicians.GetValueOrDefault() * (countrySettings.TechnicianCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupEngineers.GetValueOrDefault() * (countrySettings.EngineerCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))))
                        + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                        * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                        / stepBatchSize;

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (setupExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.SetupCost += extraShiftsCost;
                }
            }

            // Calculate the labor cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0)
            {
                stepCost.DirectLabourCost = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                    * ((step.ProductionUnskilledLabour.GetValueOrDefault() * countrySettings.UnskilledLaborCost)
                    + (step.ProductionSkilledLabour.GetValueOrDefault() * countrySettings.SkilledLaborCost)
                    + (step.ProductionForeman.GetValueOrDefault() * countrySettings.ForemanCost)
                    + (step.ProductionTechnicians.GetValueOrDefault() * countrySettings.TechnicianCost)
                    + (step.ProductionEngineers.GetValueOrDefault() * countrySettings.EngineerCost));

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal directLaborExtraShiftCostPerPart = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                        * ((step.ProductionUnskilledLabour.GetValueOrDefault() * (countrySettings.UnskilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionSkilledLabour.GetValueOrDefault() * (countrySettings.SkilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionForeman.GetValueOrDefault() * (countrySettings.ForemanCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionTechnicians.GetValueOrDefault() * (countrySettings.TechnicianCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionEngineers.GetValueOrDefault() * (countrySettings.EngineerCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault()))));

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (directLaborExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.DirectLabourCost += extraShiftsCost;
                }
            }

            // Calculate the dies cost
            if (step.Dies.Count > 0)
            {
                var dieCalcParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = partLifeCycleProductionQuantity };
                foreach (var die in step.Dies.Where(d => !d.IsDeleted))
                {
                    DieCost dieCost = this.CalculateDieCost(die, dieCalcParams);
                    stepCost.DieCosts.Add(dieCost);

                    stepCost.ToolAndDieCost += dieCost.Cost;
                    stepCost.DiesMaintenanceCost += dieCost.MaintenanceCost;

                    stepCost.InvestmentCost.AddDieInvestment(new InvestmentCostSingle(die.Guid, die.Name, die.Investment.GetValueOrDefault()));
                }
            }

            // Calculate the manufacturing cost. Add ToolAndDieCost to ManufacturingCost only if the parent part's SBM is active.
            stepCost.ManufacturingCost = stepCost.MachineCost + stepCost.SetupCost + stepCost.DirectLabourCost + stepCost.DiesMaintenanceCost;
            if (toolingActive)
            {
                stepCost.ManufacturingCost += stepCost.ToolAndDieCost;
            }

            // Calculate the scrap and overhead.
            stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
            stepCost.RejectCost = stepCost.ManufacturingCost * step.ScrapAmount.GetValueOrDefault();

            return stepCost;
        }

        /// <summary>
        /// Calculates the net cost of a raw material.
        /// The net cost is the cost without overheads or margins.
        /// </summary>
        /// <param name="material">The raw material for which to calculate the cost.</param>
        /// <param name="materialCost">The cost object where to put the calculated costs for the raw material.</param>
        protected override void CalculateRawMaterialNetCost(RawMaterial material, RawMaterialCost materialCost)
        {
            // Adjust the raw material's price for the current weight unit used by the calculator.            
            var priceUnit = material.QuantityUnitBase != null ? material.QuantityUnitBase : material.ParentWeightUnitBase;
            decimal price = CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(material.Price.GetValueOrDefault(), priceUnit, this.WeightUnit);

            decimal? partWeight = material.ParentWeight / this.WeightUnit.ConversionRate;
            materialCost.BaseCost = partWeight.GetValueOrDefault() * price;

            // If the PartWeight and Sprue fields are set ignore the material.Quantity value because there are cases when it was set to 0,
            // causing calculation errors (normally material.Quantity should be set to the value calculated below). If PartWeight or Sprue is not set
            // we considered that material.Quantity was manually inputted and use it as it is.
            decimal? materialWeight = null;
            if (partWeight.HasValue && material.Sprue.HasValue)
            {
                materialWeight = partWeight.Value * (1M + material.Sprue.Value);
            }
            else
            {
                materialWeight = material.Quantity / this.WeightUnit.ConversionRate;
            }

            // Calculate the scrap cost
            decimal scrapCost = (materialWeight.GetValueOrDefault() - partWeight.GetValueOrDefault()) * price;
            decimal scrapRefund = scrapCost * material.ScrapRefundRatio.GetValueOrDefault();

            var scrapCalculationType = (ScrapCalculationType)material.ScrapCalculationType;
            if (scrapCalculationType == ScrapCalculationType.Yield)
            {
                // For Yield scrap refund mode, the refund is subtracted from the scrap cost
                materialCost.ScrapCost = scrapCost - scrapRefund;
                materialCost.ScrapRefund = scrapRefund * -1m;
            }
            else if (scrapCalculationType == ScrapCalculationType.Dispose)
            {
                // For Dispose scrap refund mode, the refund is added to the scrap cost
                materialCost.ScrapCost = scrapCost + scrapRefund;
                materialCost.ScrapRefund = scrapRefund;
            }

            // Calculate the loss cost
            materialCost.LossCost = materialWeight.GetValueOrDefault() * material.Loss.GetValueOrDefault() * price;

            materialCost.NetCost = materialCost.BaseCost + materialCost.ScrapCost + materialCost.LossCost;

            // Calculate the rejection cost
            materialCost.RejectCost = materialCost.NetCost * material.RejectRatio.GetValueOrDefault();

            materialCost.NetCost += materialCost.RejectCost;
        }

        /// <summary>
        /// Calculates the machine investment for a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The calculated machine investment.</returns>
        protected virtual decimal CalculateMachineInvestment(Machine machine)
        {
            return machine.MachineInvestment.GetValueOrDefault();
        }

        #endregion Helper methods
    }
}
