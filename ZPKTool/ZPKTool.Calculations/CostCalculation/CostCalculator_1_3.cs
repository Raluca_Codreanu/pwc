﻿namespace ZPKTool.Calculations.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// The cost calculator implementation v1.3
    /// </summary>
    /// <remarks>
    /// Changes in this version:
    /// - Transport cost calculation (from process and part/assy level)
    /// - Integrated the Raw Part in cost calculations
    /// - Calculation of cumulated Weight and Investment cost (for the entire calculation model/object graph)
    /// - Calculation of ExternalWorkOverhead and updated the calculation of ExternalWorkMargin
    /// - Raw Material: 
    ///     - Calculation of Work in Progress (WIP) cost
    ///     - Recycling
    /// - Added Company Surcharge Overhead calculation
    /// - Small formula change in Part enhanced scrap calculation
    /// - Updated the enhanced scrap calculation for Part and Assembly processes.
    /// </remarks>
    internal class CostCalculator_1_3 : CostCalculator_1_2, ICostCalculator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_3" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_3(BasicSetting basicSettings)
            : base(basicSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_3" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_3(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : base(basicSettings, measurementUnits)
        {
        }

        #region ICostCalculator implementation

        /// <summary>
        /// Computes the cost of an Assembly.
        /// </summary>
        /// <param name="assembly">The assembly whose cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the cost calculation.</param>
        /// <returns>
        /// An instance of <see cref="CalculationResult" /> that contains the cost breakdown of the assembly.
        /// </returns>
        public override CalculationResult CalculateAssemblyCost(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            this.CheckCalculationData(assembly);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = assembly.Number;
            calcResult.Name = assembly.Name;
            calcResult.Remark = string.Empty;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = assembly.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = assembly.TargetPrice;
            calcResult.PurchasePrice = assembly.PurchasePrice;
            calcResult.AdditionalCostsNotes = assembly.AdditionalCostsNotes;

            var overheadSettings = assembly.OverheadSettings;

            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Compute the cost of sub-parts and sub-assemblies. If the assembly's cost is any type of estimation, sub parts are not included.
                // This must be done before calculating the process cost because the sub-parts costs are necessary for the enhanced scrap calculation.
                SubPartsAndSubAssembliesCost cost = this.CalculateSubPartsAndSubAssembliesCost(assembly, parameters);

                calcResult.AssembliesCost.SumWith(cost.SubAssembliesCost);
                calcResult.PartsCost.SumWith(cost.SubPartsCost);

                // The external parts OH and margin are added to the commodity OH and margin; add the sales and administration overhead on external parts
                // into the corresponding section of the total overhead cost.
                calcResult.OverheadCost.CommodityOverhead += cost.ExternalOverhead;
                calcResult.OverheadCost.CommodityMargin += cost.ExternalMargin;
                calcResult.OverheadCost.SalesAndAdministrationOverhead += cost.ExternalSGA;
                calcResult.SubpartsAndSubassembliesCost = cost.TotalCost;

                // Calculate the cost of the assembling process and add the results to the calculation result object.
                var processCalcParams = new AssemblyProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    SubAssembliesCost = calcResult.AssembliesCost,
                    SubPartsCost = calcResult.PartsCost,
                    ProjectCreateDate = parameters.ProjectCreateDate
                };

                ProcessCost processCost = this.CalculateProcessCost(assembly, processCalcParams);
                calcResult.ProcessCost = processCost;
                calcResult.CommoditiesCost.Add(processCost.CommoditiesCost);
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on 100% Manufacturing Ratio (the full estimated cost is considered to be manufacturing cost)                
                calcResult.ProcessCost.ManufacturingCostSum = assembly.EstimatedCost.GetValueOrDefault();
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = assembly.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.RawMaterialsCost.WIPCostSum
                    + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum + calcResult.ProcessCost.ManufacturingTransportCost;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, assembly, productionCost, logisticCostRatio);
            }

            // Calculate all overheads and margins and save them in the result object. If the assembly's cost is calculated externally no overheads are applied.
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost
                + calcResult.TransportCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * assembly.PaymentTerms * (assembly.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the assembly's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(assembly, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Part or Raw Part.
        /// </summary>
        /// <param name="part">The part whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown of the given part.
        /// </returns>
        public override CalculationResult CalculatePartCost(Part part, PartCostCalculationParameters parameters)
        {
            this.CheckCalculationData(part);

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = part.Number;
            calcResult.Name = part.Name;
            calcResult.Remark = part.AdditionalRemarks;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = part.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = part.TargetPrice;
            calcResult.PurchasePrice = part.PurchasePrice;
            calcResult.AdditionalCostsNotes = part.AdditionalCostsNotes;

            var overheadSettings = part.OverheadSettings;

            // Compute the cost of the part's raw materials and raw part.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var materialParams = new RawMaterialCostCalculationParameters()
                {
                    MaterialOverheadRate = overheadSettings.MaterialOverhead,
                    AnnualProductionQty = part.YearlyProductionQuantity.GetValueOrDefault(),
                    AssetRate = part.AssetRate.GetValueOrDefault(),
                    BatchSize = part.BatchSizePerYear.GetValueOrDefault()
                };
                var materialsCost = this.CalculateRawMaterialsCost(part.RawMaterials, materialParams);
                calcResult.RawMaterialsCost.Add(materialsCost);

                // Compute the cost of the part's raw part.
                var rawPart = part.RawPart as RawPart;
                if (rawPart != null && !rawPart.IsDeleted)
                {
                    PartCost rawPartCost = this.CalculateRawPartCost(rawPart, parameters);
                    calcResult.PartsCost.AddCost(rawPartCost);

                    // The raw part's cost is added as material cost into its parent part's cost calculation.
                    RawMaterialCost materialCost = new RawMaterialCost();
                    materialCost.RawMaterialId = rawPart.Guid;
                    materialCost.Name = rawPart.Name;
                    materialCost.PartWeight = rawPartCost.FullCalculationResult.Weight;
                    materialCost.BaseCost = rawPartCost.TargetCost;
                    materialCost.NetCost = rawPartCost.TargetCost;
                    materialCost.Overhead = materialCost.NetCost * overheadSettings.MaterialOverhead;
                    materialCost.TotalCost = materialCost.NetCost + materialCost.Overhead;
                    materialCost.IsRawPart = true;

                    calcResult.RawMaterialsCost.AddCost(materialCost);
                }
            }
            else if (calculationAccuracy == PartCalculationAccuracy.RoughCalculation || calculationAccuracy == PartCalculationAccuracy.Estimation)
            {
                // Calculate the materials cost using the Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost that is allocated
                // as ManufacturingCost and the difference is allocated as Material Cost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.RawMaterialsCost.NetCostSum = estimatedCost - (estimatedCost * manufacturingRatio);
                calcResult.RawMaterialsCost.OverheadSum = calcResult.RawMaterialsCost.NetCostSum * overheadSettings.MaterialOverhead;
                calcResult.RawMaterialsCost.TotalCostSum = calcResult.RawMaterialsCost.NetCostSum + calcResult.RawMaterialsCost.OverheadSum;
            }

            // Compute the cost of the part's commodities            
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var commodityParams = new CommodityCostCalculationParameters()
                {
                    CommodityOverheadRate = overheadSettings.CommodityOverhead,
                    CommodityMarginRate = overheadSettings.CommodityMargin
                };

                CommoditiesCost commodityCost = this.CalculateCommoditiesCost(part.Commodities, commodityParams);
                calcResult.CommoditiesCost.Add(commodityCost);
            }

            // Compute the cost of the part's manufacturing process and add the cost data in the calculation result object
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                var calcParams = new PartProcessCostCalculationParameters()
                {
                    ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                    ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                    RawMaterialsCost = calcResult.RawMaterialsCost,
                    CommoditiesCost = calcResult.CommoditiesCost,
                    RawPartCost = calcResult.PartsCost.PartCosts.FirstOrDefault(c => c.IsRawPart),
                    ProjectCreateDate = parameters.ProjectCreateDate
                };

                ProcessCost processCost = this.CalculateProcessCost(part, calcParams);
                calcResult.ProcessCost = processCost;
                calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
                calcResult.InvestmentCost.Add(processCost.InvestmentCost);
            }
            else if (calculationAccuracy == PartCalculationAccuracy.Estimation || calculationAccuracy == PartCalculationAccuracy.RoughCalculation)
            {
                // Calculate the manufacturing cost based on the part's Manufacturing Ratio. The Manufacturing Ratio represents the percentage of the estimated cost
                // that is allocated as ManufacturingCost.
                decimal estimatedCost = part.EstimatedCost.GetValueOrDefault();
                decimal manufacturingRatio = part.ManufacturingRatio.GetValueOrDefault(Part.DefaultManufacturingRatio);
                calcResult.ProcessCost.ManufacturingCostSum = estimatedCost * manufacturingRatio;
                calcResult.ProcessCost.ManufacturingOverheadSum = calcResult.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingOverhead;
            }

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = part.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.RawMaterialsCost.WIPCostSum
                    + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum + calcResult.ProcessCost.ManufacturingTransportCost;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, part, productionCost, logisticCostRatio);
            }

            // Calculate the overheads. If the part's cost is calculated externally, the overheads are not applied
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost
                + calcResult.TransportCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * part.PaymentTerms * (part.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary.
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(part, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Raw Material.
        /// </summary>
        /// <param name="material">The raw material whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation. Can be null if you need to calculate only the net cost.</param>
        /// <returns>
        /// An object containing the cost breakdown for the raw material.
        /// </returns>
        public override RawMaterialCost CalculateRawMaterialCost(RawMaterial material, RawMaterialCostCalculationParameters parameters)
        {
            var materialCost = base.CalculateRawMaterialCost(material, parameters);

            // The parameters may be null when only the net cost is needed.
            if (parameters == null)
            {
                return materialCost;
            }

            // Calculate the WIP (Work In Progress) cost based on stock keeping time
            int reach = 0;
            decimal batchSize = 0m;
            if (parameters.BatchSize != 0)
            {
                reach = 365 / parameters.BatchSize; // 365 is the number of days in a year
                batchSize = Math.Ceiling((decimal)parameters.AnnualProductionQty / (decimal)parameters.BatchSize);
            }

            if (reach != 0 && parameters.AnnualProductionQty != 0)
            {
                decimal materialStock = (material.StockKeeping.GetValueOrDefault() / reach) * batchSize * materialCost.NetCost;
                materialCost.WIPCost = (materialStock * parameters.AssetRate) / parameters.AnnualProductionQty;
                materialCost.TotalCost += materialCost.WIPCost;
            }

            return materialCost;
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for an Assembly.
        /// <para />
        /// In order to calculate a correct result the assembly's full parent hierarchy must be loaded, that is, the references chain from the assembly
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        public override decimal CalculateYearlyProductionQuantity(Assembly assembly)
        {
            return this.CalculateYearlyProductionQuantityInternal(assembly);
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for a Part or Raw Part.
        /// <para />
        /// In order to calculate a correct result the part's full parent hierarchy must be loaded, that is, the references chain from the part
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        public override decimal CalculateYearlyProductionQuantity(Part part)
        {
            return this.CalculateYearlyProductionQuantityInternal(part);
        }

        #endregion ICostCalculator implementation

        #region Process calculation

        /// <summary>
        /// Computes the costs of a process step.
        /// </summary>
        /// <param name="step">The process step to calculate the costs for.</param>
        /// <param name="countrySettings">The country settings to be used during the calculations.</param>
        /// <param name="overheadSettings">The overhead settings to be used during the calculations.</param>
        /// <param name="projectDepreciationPeriod">The project depreciation period.</param>
        /// <param name="projectDepreciationRate">The project depreciation rate.</param>
        /// <param name="partLifeCycleProductionQuantity">The life cycle production quantity of the part/assembly to which the step belongs to.</param>
        /// <param name="toolingActive">a value indicating whether tooling is active for the process</param>
        /// <param name="projectCreateDate">The project create date.</param>
        /// <returns>
        /// An object containing the costs for the given process step.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The complexity of the calculation code can not be easily avoided.")]
        protected override ProcessStepCost CalculateProcessStepCost(
            ProcessStep step,
            CountrySetting countrySettings,
            OverheadSetting overheadSettings,
            int projectDepreciationPeriod,
            decimal projectDepreciationRate,
            decimal partLifeCycleProductionQuantity,
            bool toolingActive,
            DateTime? projectCreateDate)
        {
            ProcessStepCost stepCost = new ProcessStepCost();
            stepCost.StepId = step.Guid;
            stepCost.StepName = step.Name;
            stepCost.StepIndex = step.Index;
            stepCost.IsExternal = step.IsExternal.GetValueOrDefault();
            stepCost.SBMActive = toolingActive;

            // If the step calculation accuracy is estimated then the estimated price is the step's total cost.
            var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
            if (stepAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                stepCost.ManufacturingCostIsEstimated = true;
                stepCost.ManufacturingCost = step.Price.GetValueOrDefault();
                stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
                stepCost.ManufacturingOverheadRate = overheadSettings.ManufacturingOverhead;
                return stepCost;
            }

            // Copy the most used step parameters in local not nullable variables
            decimal stepPartsPerCycle = step.PartsPerCycle.GetValueOrDefault();
            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
            decimal stepCycleTime = step.CycleTime.GetValueOrDefault();
            int stepBatchSize = step.BatchSize.GetValueOrDefault();

            // Calculate machines cost
            if (step.Machines.Count > 0)
            {
                var machineParams = new MachineCostCalculationParameters()
                {
                    CountrySettingsAirCost = countrySettings.AirCost,
                    CountrySettingsEnergyCost = countrySettings.EnergyCost,
                    CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost,
                    CountrySettingsWaterCost = countrySettings.WaterCost,
                    ProcessStepHoursPerShift = step.HoursPerShift.GetValueOrDefault(),
                    ProcessStepProductionWeeksPerYear = step.ProductionWeeksPerYear.GetValueOrDefault(),
                    ProcessStepShiftsPerWeek = step.ShiftsPerWeek.GetValueOrDefault(),
                    ProcessStepExtraShiftsPerWeek = step.ExtraShiftsNumber.GetValueOrDefault(),
                    ProjectDepreciationPeriod = projectDepreciationPeriod,
                    ProjectDepreciationRate = projectDepreciationRate,
                    ProjectCreateDate = projectCreateDate
                };

                decimal machinesCostSum = 0m;
                foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                {
                    MachineCost machineCost = this.CalculateMachineCost(machine, machineParams);
                    stepCost.MachineCosts.Add(machineCost);

                    decimal machineInvestment = this.CalculateMachineInvestment(machine);
                    decimal machineCostInProcess = 0m;
                    if (machine.IsProjectSpecific)
                    {
                        if (partLifeCycleProductionQuantity != 0m && stepPartsPerCycle != 0m)
                        {
                            decimal totalInvest = (machineInvestment
                                + machine.SetupInvestment.GetValueOrDefault()
                                + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                                + machine.FundamentalSetupInvestment.GetValueOrDefault())
                                * machine.Amount;

                            machineCostInProcess = (totalInvest / partLifeCycleProductionQuantity)
                                + ((stepProcessTime / stepPartsPerCycle) * (machineCost.EnergyCostPerHour / SecondsInHour))
                                + machineCost.FloorCost;
                        }
                    }
                    else if (stepPartsPerCycle != 0m)
                    {
                        machineCostInProcess = (machineCost.FullCostPerHour / SecondsInHour)
                            * (stepProcessTime / stepPartsPerCycle);
                    }

                    machinesCostSum += machineCostInProcess;

                    // Calculate the investment cost for the machine.
                    decimal investment = machineInvestment
                        + machine.SetupInvestment.GetValueOrDefault()
                        + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                        + machine.FundamentalSetupInvestment.GetValueOrDefault();

                    InvestmentCostSingle machineInvestmentCost = new InvestmentCostSingle(machine.Guid, machine.Name, investment);
                    machineInvestmentCost.Amount = machine.Amount;
                    stepCost.InvestmentCost.AddMachineInvestment(machineInvestmentCost);
                }

                stepCost.MachineCost = machinesCostSum;
            }

            // Determine the shift model by dividing the no. of shifts per week by the number of working week days
            decimal shiftModel = 1;
            if (step.ShiftsPerWeek.GetValueOrDefault() != 0m && step.ProductionDaysPerWeek.GetValueOrDefault() != 0)
            {
                shiftModel = Math.Ceiling(step.ShiftsPerWeek.GetValueOrDefault() / step.ProductionDaysPerWeek.GetValueOrDefault());
            }

            // Select the shift charge value based of the shift model
            decimal shiftCharge = 0m;
            decimal laborAvailability = countrySettings.LaborAvailability.GetValueOrDefault(1m);
            if (shiftModel <= 1m)
            {
                shiftCharge = countrySettings.ShiftCharge1ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel == 2m)
            {
                shiftCharge = countrySettings.ShiftCharge2ShiftModel.GetValueOrDefault();
            }
            else if (shiftModel >= 3m)
            {
                shiftCharge = countrySettings.ShiftCharge3ShiftModel.GetValueOrDefault();
            }

            // Calculate the costs for the different labor skill levels
            decimal unskilledLaborCost = 0m;
            decimal skilledLaborCost = 0m;
            decimal foremanCost = 0m;
            decimal technicianCost = 0m;
            decimal engineerCost = 0m;
            if (laborAvailability != 0m)
            {
                unskilledLaborCost = countrySettings.UnskilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                skilledLaborCost = countrySettings.SkilledLaborCost * (1m + shiftCharge) * (1m / laborAvailability);
                foremanCost = countrySettings.ForemanCost * (1m + shiftCharge) * (1m / laborAvailability);
                technicianCost = countrySettings.TechnicianCost * (1m + shiftCharge) * (1m / laborAvailability);
                engineerCost = countrySettings.EngineerCost * (1m + shiftCharge) * (1m / laborAvailability);
            }

            // Calculate the setup cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0m && stepBatchSize != 0)
            {
                int stepSetupsPerBatch = step.SetupsPerBatch.GetValueOrDefault();
                decimal stepSetupTime = step.SetupTime.GetValueOrDefault();
                decimal stepMaxDownTime = step.MaxDownTime.GetValueOrDefault();

                stepCost.SetupCost = (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                    * ((step.SetupUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.SetupSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.SetupForeman.GetValueOrDefault() * foremanCost)
                    + (step.SetupTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.SetupEngineers.GetValueOrDefault() * engineerCost)))
                    + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                    * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                    / stepBatchSize;

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal setupExtraShiftCostPerPart =
                        (((((stepSetupsPerBatch * stepSetupTime) / SecondsInHour) / stepPartsPerCycle)
                        * ((step.SetupUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupForeman.GetValueOrDefault() * (foremanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupTechnicians.GetValueOrDefault() * (technicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupEngineers.GetValueOrDefault() * (engineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))))
                        + (((stepSetupsPerBatch * stepMaxDownTime) / SecondsInHour)
                        * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                        / stepBatchSize;

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (setupExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.SetupCost += extraShiftsCost;
                }
            }

            // Calculate the labor cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0)
            {
                stepCost.DirectLabourCost = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                    * ((step.ProductionUnskilledLabour.GetValueOrDefault() * unskilledLaborCost)
                    + (step.ProductionSkilledLabour.GetValueOrDefault() * skilledLaborCost)
                    + (step.ProductionForeman.GetValueOrDefault() * foremanCost)
                    + (step.ProductionTechnicians.GetValueOrDefault() * technicianCost)
                    + (step.ProductionEngineers.GetValueOrDefault() * engineerCost));

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal directLaborExtraShiftCostPerPart = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                        * ((step.ProductionUnskilledLabour.GetValueOrDefault() * (unskilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionSkilledLabour.GetValueOrDefault() * (skilledLaborCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionForeman.GetValueOrDefault() * (foremanCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionTechnicians.GetValueOrDefault() * (technicianCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionEngineers.GetValueOrDefault() * (engineerCost * (1 + step.ShiftCostExceedRatio.GetValueOrDefault()))));

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (directLaborExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.DirectLabourCost += extraShiftsCost;
                }
            }

            // Calculate the dies cost.
            if (step.Dies.Count > 0)
            {
                var dieCalcParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = partLifeCycleProductionQuantity };
                foreach (var die in step.Dies.Where(d => !d.IsDeleted))
                {
                    DieCost dieCost = this.CalculateDieCost(die, dieCalcParams);
                    stepCost.DieCosts.Add(dieCost);

                    stepCost.ToolAndDieCost += dieCost.Cost;
                    stepCost.DiesMaintenanceCost += dieCost.MaintenanceCost;

                    InvestmentCostSingle dieInvestment = new InvestmentCostSingle(die.Guid, die.Name, die.Investment.GetValueOrDefault());

                    decimal dieSetsPaidByCustomer = die.DiesetsNumberPaidByCustomer.GetValueOrDefault();
                    if (dieCost.DiesPerLifeTime >= dieSetsPaidByCustomer)
                    {
                        dieInvestment.Amount = dieCost.DiesPerLifeTime - dieSetsPaidByCustomer;
                    }

                    stepCost.InvestmentCost.AddDieInvestment(dieInvestment);
                }
            }

            // Calculate the manufacturing cost. Add ToolAndDieCost to ManufacturingCost only if the parent part's SBM is active.
            stepCost.ManufacturingCost = stepCost.MachineCost + stepCost.SetupCost + stepCost.DirectLabourCost + stepCost.DiesMaintenanceCost;
            if (toolingActive)
            {
                stepCost.ManufacturingCost += stepCost.ToolAndDieCost;
            }

            // Calculate the overhead.
            // CHANGE: If the step defines the Manufacturing Overhead Ratio use that to calculate the manufacturing overhead,
            // not the value from the inputted Overhead Settings.
            stepCost.ManufacturingOverheadRate =
                step.ManufacturingOverhead.HasValue ? step.ManufacturingOverhead.Value : overheadSettings.ManufacturingOverhead;
            stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * stepCost.ManufacturingOverheadRate;

            // Calculate the Transport Cost
            if (step.IsExternal.GetValueOrDefault())
            {
                var stepTransportCostType = EntityUtils.ConvertToEnum(step.TransportCostType, ProcessTransportCostType.PerPart);
                if (stepTransportCostType == ProcessTransportCostType.PerQty)
                {
                    decimal transportCost = 0m;
                    var transportCostQty = step.TransportCostQty.GetValueOrDefault();
                    if (transportCostQty != 0m)
                    {
                        transportCost = step.TransportCost.GetValueOrDefault() / transportCostQty;
                    }
                    else
                    {
                        transportCost = 0m;
                    }

                    stepCost.TransportCost = transportCost;
                }
                else
                {
                    // The transport cost is per part.
                    stepCost.TransportCost = step.TransportCost.GetValueOrDefault();
                }
            }

            return stepCost;
        }

        /// <summary>
        /// Calculates the cost of a manufacturing process (the process of a part).
        /// </summary>
        /// <param name="part">The part whose process cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The complexity of the calculation code can not be easily avoided.")]
        protected override ProcessCost CalculateProcessCost(Part part, PartProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || part.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();
            var rejectOverview = processCost.RejectOverview;
            var rawMaterialsTotal = rejectOverview.RawMaterialsTotal;
            var commoditiesTotal = rejectOverview.CommoditiesTotal;
            var rawPartsTotal = rejectOverview.RawPartsTotal;

            // Calculate the total raw material and commodity cost, which is necessary during scrap calculation.
            var rawMaterialCosts = parameters.RawMaterialsCost.RawMaterialCosts.Where(m => !m.IsRawPart);
            rawMaterialsTotal.MaterialCostPerPiece = rawMaterialCosts.Sum(c => c.NetCost - c.RejectCost);

            // Calculate the Raw Parts Totals. They can be calculated here because they don't depend on the calculations below, instead are compiled from the Raw Part's calculation data.
            if (parameters.RawPartCost != null)
            {
                var rawPartRejectOverview = parameters.RawPartCost.FullCalculationResult.ProcessCost.RejectOverview;

                rawPartsTotal.TotalRejectMaterialCost = rawPartRejectOverview.TotalRejectCostForIOPiece * rawPartRejectOverview.IOPiecesPerBatch;
                rawPartsTotal.RejectMaterialCostPerPiece = rawPartRejectOverview.TotalRejectCostForIOPiece;
                rawPartsTotal.MaterialCostPerPiece = rawPartRejectOverview.TotalCostOfIOPiece;
                rawPartsTotal.RatioOfRejectCostInTotalCost = rawPartRejectOverview.RatioOfRejectCostInTotalCost;
            }

            decimal batchSizeTarget = 0m; // or net batch size => the number of pieces/batch that must be outputted from the process
            decimal yearlyProductionQty = this.CalculateYearlyProductionQuantity(part);
            decimal batchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            if (batchesPerYear != 0)
            {
                batchSizeTarget = Math.Ceiling(yearlyProductionQty / batchesPerYear);
            }

            // Recalculate the cost of commodities to include the reject cost.            
            decimal grossProductionQtyPerYear = this.CalculateGrossBatchSize(part, yearlyProductionQty) * batchesPerYear;
            this.RecalculateCommoditiesCostInProcess(parameters.CommoditiesCost, yearlyProductionQty, grossProductionQtyPerYear);

            // The commodities total MaterialCostPerPiece must be calculated here because is used when calculating the reject cost of the process.
            commoditiesTotal.MaterialCostPerPiece = parameters.CommoditiesCost.CommodityCosts.Sum(c => c.Cost - c.RejectCost);

            // Initialize the calculation parameters for consumables. These parameters are the same for all steps.            
            var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = part.OverheadSettings.ConsumableOverhead };

            // Calculating the step costs in reverse order is a requirement for the enhanced scrap calculation algorithm
            decimal grossBatchSize = batchSizeTarget;
            foreach (var step in part.Process.Steps.OrderByDescending(step => step.Index))
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    var consumablesCost = this.CalculateConsumablesCost(step.Consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var partLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(part);

                ProcessStepCost stepCost = this.CalculateProcessStepCost(
                    step,
                    part.CountrySettings,
                    part.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    partLifetimeProdQty,
                    part.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);

                stepCost.ParentPartIsExternal = part.IsExternal.GetValueOrDefault();

                /*** Enhanced scrap calculation ***/

                // The quantity of parts that must be outputted from this step (net quantity).
                // This step's net batch size is the previous step's gross batch size (for the last step is the batch size target)
                stepCost.NetBatchSize = grossBatchSize;

                // The amount of parts that have to be produced in order to output the above net batch size.
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    stepCost.GrossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * stepCost.NetBatchSize);
                }
                else
                {
                    stepCost.GrossBatchSize = stepCost.NetBatchSize;
                }

                // The number of rejected parts during this step.
                stepCost.RejectedPieces = stepCost.GrossBatchSize - stepCost.NetBatchSize;

                // The additional manufacturing cost caused by the rejected parts for this step.
                stepCost.RejectManufacturingCostPerPiece = 0m;
                if (batchSizeTarget != 0m)
                {
                    // Manufacturing cost for the rejected parts
                    stepCost.RejectManufacturingCostPerPiece = stepCost.RejectedPieces * stepCost.ManufacturingCost / batchSizeTarget;
                }

                // Material cost for the part, excluding material reject cost.
                decimal materialCostPerPiece = rawMaterialsTotal.MaterialCostPerPiece + rawPartsTotal.MaterialCostPerPiece + commoditiesTotal.MaterialCostPerPiece;

                stepCost.RejectMaterialCostPerPiece = 0m;
                if (batchSizeTarget != 0m)
                {
                    // Material cost for the rejected parts
                    stepCost.RejectMaterialCostPerPiece = stepCost.RejectedPieces * materialCostPerPiece / batchSizeTarget;
                }

                stepCost.RejectCost = stepCost.RejectMaterialCostPerPiece + stepCost.RejectManufacturingCostPerPiece;

                stepCost.RejectProcessCostTotal = (stepCost.ManufacturingCost + materialCostPerPiece) * stepCost.RejectedPieces;

                grossBatchSize = stepCost.GrossBatchSize;

                /**********************************/

                processCost.AddStepCost(stepCost);
            }

            rejectOverview.GrossBatchSize = grossBatchSize;

            // Calculate the cumulated rejected cost for each step.
            decimal prevStepRejProcessCostPerPieceCumulated = 0m;
            foreach (var stepCost in processCost.StepCosts.OrderBy(c => c.StepIndex))
            {
                stepCost.RejectProcessCostPerPieceCumulated = stepCost.RejectCost + prevStepRejProcessCostPerPieceCumulated;
                prevStepRejProcessCostPerPieceCumulated = stepCost.RejectProcessCostPerPieceCumulated;
            }

            // Calculate the Raw Materials Totals
            rawMaterialsTotal.TotalRejectMaterialCost = rawMaterialCosts.Sum(c => c.RejectCostWithoutScrapRefund) * rejectOverview.GrossBatchSize;
            rawMaterialsTotal.RejectMaterialCostPerPiece = batchSizeTarget != 0m ? rawMaterialsTotal.TotalRejectMaterialCost / batchSizeTarget : 0m;
            if (rawMaterialsTotal.MaterialCostPerPiece != 0m)
            {
                rawMaterialsTotal.RatioOfRejectCostInTotalCost = 1m / rawMaterialsTotal.MaterialCostPerPiece * rawMaterialsTotal.RejectMaterialCostPerPiece;
            }

            // Calculate the Commodities Totals
            commoditiesTotal.TotalRejectMaterialCost = parameters.CommoditiesCost.CommodityCosts.Sum(c => c.RejectCost) * rejectOverview.GrossBatchSize;
            commoditiesTotal.RejectMaterialCostPerPiece = parameters.CommoditiesCost.CommodityCosts.Sum(c => c.RejectCost);
            if (commoditiesTotal.MaterialCostPerPiece != 0m)
            {
                commoditiesTotal.RatioOfRejectCostInTotalCost = 1m / commoditiesTotal.MaterialCostPerPiece * commoditiesTotal.RejectMaterialCostPerPiece;
            }

            // Calculate/Compile the Totals per Piece
            rejectOverview.ProcessRejectCost = processCost.StepCosts.Sum(c => c.RejectCost);
            rejectOverview.RawMaterialsRejectCost = rawMaterialsTotal.RejectMaterialCostPerPiece;
            rejectOverview.CommoditiesRejectCost = commoditiesTotal.RejectMaterialCostPerPiece;
            rejectOverview.RejectCostForIOPiece = rejectOverview.ProcessRejectCost + rejectOverview.RawMaterialsRejectCost + rejectOverview.CommoditiesRejectCost;
            rejectOverview.RawPartsRejectCost = rawPartsTotal.RejectMaterialCostPerPiece;
            rejectOverview.TotalRejectCostForIOPiece = rejectOverview.RejectCostForIOPiece + rejectOverview.RawPartsRejectCost + rejectOverview.PartsRejectCost;
            rejectOverview.TotalRejectCost = processCost.StepCosts.Sum(c => c.RejectProcessCostTotal)
                + rawMaterialsTotal.TotalRejectMaterialCost
                + commoditiesTotal.TotalRejectMaterialCost
                + rawPartsTotal.TotalRejectMaterialCost;

            if (batchSizeTarget != 0m)
            {
                rejectOverview.TotalCostOfIOPiece = ((rawMaterialsTotal.MaterialCostPerPiece * rejectOverview.GrossBatchSize)
                    + (commoditiesTotal.MaterialCostPerPiece * rejectOverview.GrossBatchSize)
                    + (rawPartsTotal.MaterialCostPerPiece * rejectOverview.GrossBatchSize)
                    + processCost.StepCosts.Sum(c => c.ManufacturingCost * c.GrossBatchSize))
                    / batchSizeTarget;
            }

            if (rejectOverview.TotalCostOfIOPiece != 0m)
            {
                rejectOverview.RatioOfRejectCostInTotalCost = 1m / rejectOverview.TotalCostOfIOPiece * rejectOverview.TotalRejectCostForIOPiece;
            }

            // Compile the data about the number of pieces per year and batch sizes.
            rejectOverview.IOPiecesPerYear = yearlyProductionQty;
            rejectOverview.BatchesPerYear = batchesPerYear;
            rejectOverview.IOPiecesPerBatch = batchSizeTarget;
            rejectOverview.RejectedPiecesTotal = rejectOverview.GrossBatchSize - rejectOverview.IOPiecesPerBatch;

            // Calculate the Capacity Utilization for all machines in the process.
            this.CalculateMachinesCapacityUtilization(
                part.Process.Steps,
                processCost,
                part.BatchSizePerYear.GetValueOrDefault(),
                part.YearlyProductionQuantity.GetValueOrDefault());

            return processCost;
        }

        /// <summary>
        /// Calculates the cost of an assembling process (the process of an assembly).
        /// </summary>
        /// <param name="assembly">The assembly whose process cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The complexity of the calculation code can not be easily avoided.")]
        protected override ProcessCost CalculateProcessCost(Assembly assembly, AssemblyProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || assembly.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();
            var rejectOverview = processCost.RejectOverview;
            var commoditiesTotal = processCost.RejectOverview.CommoditiesTotal;

            // Initialize the calculation parameters for commodities and consumables. These parameters are the same for all steps.
            var commodityCalcParams = new CommodityCostCalculationParameters()
            {
                CommodityOverheadRate = assembly.OverheadSettings.CommodityOverhead,
                CommodityMarginRate = assembly.OverheadSettings.CommodityMargin
            };
            var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = assembly.OverheadSettings.ConsumableOverhead };

            // Calculate the batch size target
            decimal batchSizeTarget = 0m; // or net batch size => the number of pieces/batch that must be outputted from the process   
            decimal yearlyProductionQty = this.CalculateYearlyProductionQuantity(assembly);
            decimal batchesPerYear = assembly.BatchSizePerYear.GetValueOrDefault();
            if (batchesPerYear != 0m)
            {
                batchSizeTarget = Math.Ceiling(yearlyProductionQty / batchesPerYear);
            }

            decimal grossBatchSize = batchSizeTarget;

            // Calculating the step costs in reverse order is a requirement for the enhanced scrap calculation algorithm
            foreach (var step in assembly.Process.Steps.OrderByDescending(step => step.Index))
            {
                CommoditiesCost stepCommoditiesCost = new CommoditiesCost();

                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    stepCommoditiesCost = this.CalculateCommoditiesCost(step.Commodities, commodityCalcParams);

                    ConsumablesCost consumablesCost = this.CalculateConsumablesCost(step.Consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var assyLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(assembly);

                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    assembly.CountrySettings,
                    assembly.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    assyLifetimeProdQty,
                    assembly.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);

                stepCost.ParentPartIsExternal = assembly.IsExternal.GetValueOrDefault();

                /*** Enhanced scrap calculation ***/

                // The quantity of parts that must be outputted from this step (net quantity).
                // This step's net batch size is the previous step's gross batch size (for the last step is the batch size target)
                stepCost.NetBatchSize = grossBatchSize;

                // The amount of parts that have to be produced in order to output the above net batch size.
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    stepCost.GrossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * stepCost.NetBatchSize);
                }
                else
                {
                    stepCost.GrossBatchSize = stepCost.NetBatchSize;
                }

                // The number of rejected parts during this step.
                stepCost.RejectedPieces = stepCost.GrossBatchSize - stepCost.NetBatchSize;

                // The additional manufacturing cost caused by the rejected parts for this step.                   
                if (batchSizeTarget != 0m)
                {
                    stepCost.RejectManufacturingCostPerPiece = stepCost.RejectedPieces * stepCost.ManufacturingCost / batchSizeTarget;
                }

                // Recalculate the cost of commodities to include reject cost                
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    decimal stepGrossProdQtyPerYear = stepCost.GrossBatchSize * batchesPerYear;
                    this.RecalculateCommoditiesCostInProcess(stepCommoditiesCost, yearlyProductionQty, stepGrossProdQtyPerYear);
                    processCost.CommoditiesCost.Add(stepCommoditiesCost);
                }

                // Calculate the Reject Material Cost of the step's commodities and add it to the Total Reject Material Cost of the Commodities Material Total section.                
                commoditiesTotal.TotalRejectMaterialCost += stepCommoditiesCost.CommodityCosts.Sum(c => c.RejectCost * stepCost.GrossBatchSize);

                // Calculate the cost of sub-parts and subassemblies used in this step.
                decimal partsCost = parameters.SubPartsCost.PartCosts
                    .Where(c => step.PartAmounts.Any(a => a.FindPartId() == c.PartId))
                    .Sum(c => c.AmountPerAssembly * c.FullCalculationResult.ProcessCost.RejectOverview.TotalCostOfIOPiece);
                decimal assyCost = parameters.SubAssembliesCost.AssemblyCosts
                    .Where(c => step.AssemblyAmounts.Any(a => a.FindAssemblyId() == c.AssemblyId))
                    .Sum(c => c.AmountPerAssembly * c.FullCalculationResult.ProcessCost.RejectOverview.TotalCostOfIOPiece);

                decimal commoditiesCost = stepCommoditiesCost.CommodityCosts.Sum(c => c.Cost - c.RejectCost);

                // The material cost per piece is the cost of SubParts and SubAssemblies used in this step plus the cost of Commodities used in this step.
                decimal materialCostPerPiece = commoditiesCost + partsCost + assyCost;

                // The material cost for the rejected parts.
                if (batchSizeTarget != 0m)
                {
                    stepCost.RejectMaterialCostPerPiece = materialCostPerPiece * stepCost.RejectedPieces / batchSizeTarget;
                }

                stepCost.RejectCost = stepCost.RejectMaterialCostPerPiece + stepCost.RejectManufacturingCostPerPiece;

                stepCost.RejectProcessCostTotal = (stepCost.ManufacturingCost + materialCostPerPiece) * stepCost.RejectedPieces;

                grossBatchSize = stepCost.GrossBatchSize;

                /**********************************/

                processCost.AddStepCost(stepCost);
            }

            processCost.RejectOverview.GrossBatchSize = grossBatchSize;

            // Calculate the cumulated rejected cost for each step.
            decimal prevStepRejProcessCostPerPieceCumulated = 0m;
            foreach (var stepCost in processCost.StepCosts.OrderBy(c => c.StepIndex))
            {
                stepCost.RejectProcessCostPerPieceCumulated = stepCost.RejectCost + prevStepRejProcessCostPerPieceCumulated;
                prevStepRejProcessCostPerPieceCumulated = stepCost.RejectProcessCostPerPieceCumulated;
            }

            // Calculate the Parts Total
            var partsTotal = processCost.RejectOverview.PartsTotal;

            partsTotal.TotalRejectMaterialCost =
                parameters.SubPartsCost.PartCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalRejectCostForIOPiece * c.FullCalculationResult.ProcessCost.RejectOverview.IOPiecesPerBatch)
                + parameters.SubAssembliesCost.AssemblyCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalRejectCostForIOPiece * c.FullCalculationResult.ProcessCost.RejectOverview.IOPiecesPerBatch);

            partsTotal.RejectMaterialCostPerPiece = parameters.SubPartsCost.PartCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalRejectCostForIOPiece)
                + parameters.SubAssembliesCost.AssemblyCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalRejectCostForIOPiece);

            partsTotal.MaterialCostPerPiece = parameters.SubPartsCost.PartCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalCostOfIOPiece)
                + parameters.SubAssembliesCost.AssemblyCosts.Sum(c => c.FullCalculationResult.ProcessCost.RejectOverview.TotalCostOfIOPiece);

            if (partsTotal.MaterialCostPerPiece != 0m)
            {
                partsTotal.RatioOfRejectCostInTotalCost = 1m / partsTotal.MaterialCostPerPiece * partsTotal.RejectMaterialCostPerPiece;
            }

            // Calculate the Commodities Total            
            commoditiesTotal.RejectMaterialCostPerPiece = processCost.CommoditiesCost.CommodityCosts.Sum(c => c.RejectCost);
            commoditiesTotal.MaterialCostPerPiece = processCost.CommoditiesCost.CommodityCosts.Sum(c => c.Cost - c.RejectCost);
            if (commoditiesTotal.MaterialCostPerPiece != 0m)
            {
                commoditiesTotal.RatioOfRejectCostInTotalCost = 1m / commoditiesTotal.MaterialCostPerPiece * commoditiesTotal.RejectMaterialCostPerPiece;
            }

            // Calculate/Compile the Totals per Piece
            rejectOverview.ProcessRejectCost = processCost.StepCosts.Sum(c => c.RejectCost);
            rejectOverview.CommoditiesRejectCost = commoditiesTotal.RejectMaterialCostPerPiece;
            rejectOverview.RejectCostForIOPiece = rejectOverview.ProcessRejectCost + rejectOverview.CommoditiesRejectCost;
            rejectOverview.PartsRejectCost = partsTotal.RejectMaterialCostPerPiece;
            rejectOverview.TotalRejectCostForIOPiece = rejectOverview.RejectCostForIOPiece + rejectOverview.PartsRejectCost;
            rejectOverview.TotalRejectCost = processCost.StepCosts.Sum(c => c.RejectProcessCostTotal)
                + partsTotal.TotalRejectMaterialCost
                + commoditiesTotal.TotalRejectMaterialCost;

            if (batchSizeTarget != 0m)
            {
                rejectOverview.TotalCostOfIOPiece = ((partsTotal.MaterialCostPerPiece * rejectOverview.GrossBatchSize)
                    + (commoditiesTotal.MaterialCostPerPiece * rejectOverview.GrossBatchSize)
                    + processCost.StepCosts.Sum(c => c.ManufacturingCost * c.GrossBatchSize))
                    / batchSizeTarget;
            }

            if (rejectOverview.TotalCostOfIOPiece != 0m)
            {
                rejectOverview.RatioOfRejectCostInTotalCost = 1m / rejectOverview.TotalCostOfIOPiece * rejectOverview.TotalRejectCostForIOPiece;
            }

            // Compile the data about the number of pieces per year and batch sizes.
            rejectOverview.IOPiecesPerYear = yearlyProductionQty;
            rejectOverview.BatchesPerYear = batchesPerYear;
            rejectOverview.IOPiecesPerBatch = batchSizeTarget;
            rejectOverview.RejectedPiecesTotal = rejectOverview.GrossBatchSize - rejectOverview.IOPiecesPerBatch;

            // Calculate the Capacity Utilization for all machines in the process.
            this.CalculateMachinesCapacityUtilization(
                assembly.Process.Steps,
                processCost,
                assembly.BatchSizePerYear.GetValueOrDefault(),
                assembly.YearlyProductionQuantity.GetValueOrDefault());

            return processCost;
        }

        /// <summary>
        /// Recalculates the cost of commodities during the calculation of the process' cost in order to include the reject cost.
        /// </summary>
        /// <param name="commoditiesCost">The commodities costs.</param>
        /// <param name="netYearlyProductionQty">The net yearly production quantity of the parent entity of the commodities.</param>
        /// <param name="grossYearlyProductionQty">The gross yearly production quantity of the parent entity of the commodities.</param>
        protected void RecalculateCommoditiesCostInProcess(
            CommoditiesCost commoditiesCost,
            decimal netYearlyProductionQty,
            decimal grossYearlyProductionQty)
        {
            foreach (var commodityCost in commoditiesCost.CommodityCosts)
            {
                // The total needed quantity per year.
                decimal totalAmountPerYear = Math.Ceiling(commodityCost.Amount * grossYearlyProductionQty * (1 + commodityCost.RejectRatio));

                // The cost for 1 piece of the commodity, including reject cost.
                decimal costPerPiece = 0m;
                if (commodityCost.Amount != 0m && netYearlyProductionQty != 0m)
                {
                    costPerPiece = totalAmountPerYear * commodityCost.Price / commodityCost.Amount / netYearlyProductionQty;
                }

                commodityCost.RejectCost = (costPerPiece - commodityCost.Price) * commodityCost.Amount;
                commodityCost.Cost = costPerPiece * commodityCost.Amount;

                // Recalculate the overhead, margin and total costs because they depend of the cost recalculated above.
                commodityCost.Overhead = commodityCost.Cost * commodityCost.OverheadRatio;
                commodityCost.Margin = commodityCost.Cost * commodityCost.MarginRatio;
                commodityCost.TotalCost = commodityCost.Cost + commodityCost.Overhead;
            }

            commoditiesCost.RecalculateSums();
        }

        #endregion Process calculation

        #region Helpers

        /// <summary>
        /// Calculates the net cost of a raw material.
        /// The net cost is the cost without overheads or margins.
        /// </summary>
        /// <param name="material">The raw material for which to calculate the cost.</param>
        /// <param name="materialCost">The cost object where to put the calculated costs for the raw material.</param>
        protected override void CalculateRawMaterialNetCost(RawMaterial material, RawMaterialCost materialCost)
        {
            // Adjust the raw material's price for the current weight unit used by the calculator.
            var priceUnit = material.QuantityUnitBase != null ? material.QuantityUnitBase : material.ParentWeightUnitBase;
            decimal price = CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(material.Price.GetValueOrDefault(), priceUnit, this.WeightUnit);

            decimal? partWeight = material.ParentWeight / this.WeightUnit.ConversionRate;
            materialCost.BaseCost = partWeight.GetValueOrDefault() * price;

            // If the PartWeight and Sprue fields are set ignore the material.Quantity value because there are cases when it was set to 0,
            // causing calculation errors (normally material.Quantity should be set to the value calculated below). If PartWeight or Sprue is not set
            // we considered that material.Quantity was manually inputted and use it as it is.
            decimal? materialWeight = null;
            if (partWeight.HasValue && material.Sprue.HasValue)
            {
                materialWeight = partWeight.Value * (1M + material.Sprue.Value);
            }
            else
            {
                materialWeight = material.Quantity / this.WeightUnit.ConversionRate;
            }

            // Calculate the scrap cost & recycling
            decimal sprue = (materialWeight - partWeight).GetValueOrDefault();
            decimal recyclingValue = (sprue * material.RecyclingRatio.GetValueOrDefault()) * price;
            decimal scrapCost = (sprue * price) - recyclingValue;
            decimal scrapRefund = scrapCost * material.ScrapRefundRatio.GetValueOrDefault();

            var scrapCalculationType = (ScrapCalculationType)material.ScrapCalculationType;
            if (scrapCalculationType == ScrapCalculationType.Yield)
            {
                // For Yield scrap refund mode, the refund is subtracted from the scrap cost
                materialCost.ScrapCost = scrapCost - scrapRefund;
                materialCost.ScrapRefund = scrapRefund * -1m;
            }
            else if (scrapCalculationType == ScrapCalculationType.Dispose)
            {
                // For Dispose scrap refund mode, the refund is added to the scrap cost
                materialCost.ScrapCost = scrapCost + scrapRefund;
                materialCost.ScrapRefund = scrapRefund;
            }

            // Calculate the loss cost
            materialCost.LossCost = (materialWeight * material.Loss * price).GetValueOrDefault();

            materialCost.NetCost = materialCost.BaseCost + materialCost.ScrapCost + materialCost.LossCost;

            // Calculate the reject cost
            materialCost.RejectCost = materialCost.NetCost * material.RejectRatio.GetValueOrDefault();

            // Calculate the reject cost without subtracting or adding the scrap refund from/to the scrap cost.
            // This is the same formula used above to calculate the RejectCost but instead of using the materialCost.ScrapCost value we use the scrapCost value (from which the scrapRefund is not subtracted).
            materialCost.RejectCostWithoutScrapRefund = (materialCost.BaseCost + scrapCost + materialCost.LossCost) * material.RejectRatio.GetValueOrDefault();

            materialCost.NetCost += materialCost.RejectCost;
        }

        /// <summary>
        /// Calculates the overhead and margin costs and stores them in the OverheadCost member of the calculation result.
        /// </summary>
        /// <param name="result">The calculation result.</param>
        /// <param name="overheadSettings">The overhead settings to use during the calculations.</param>
        protected override void CalculateOverheadAndMarginCosts(CalculationResult result, OverheadSetting overheadSettings)
        {
            base.CalculateOverheadAndMarginCosts(result, overheadSettings);

            // Calculate the external work overhead for external process steps.
            decimal externalWorkCost = result.ProcessCost.StepCosts
                .Where(s => s.IsExternal)
                .Sum(step => step.ManufacturingCost + step.ManufacturingOverheadCost + step.RejectCost);
            result.OverheadCost.ExternalWorkOverhead = externalWorkCost * overheadSettings.ExternalWorkOverhead;

            // Calculate the external work margin for external process steps.
            result.OverheadCost.ExternalWorkMargin =
                (externalWorkCost + result.OverheadCost.ExternalWorkOverhead) * overheadSettings.ExternalWorkMargin;

            // Calculate the Company Surcharge overhead cost
            result.OverheadCost.CompanySurchargeOverhead = result.ProductionCost * overheadSettings.CompanySurchargeOverhead;
        }

        /// <summary>
        /// Calculates the cost of the specified raw part.
        /// </summary>
        /// <param name="rawPart">The raw part.</param>
        /// <param name="parameters">The input parameters necessary for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the raw part.
        /// </returns>
        protected virtual PartCost CalculateRawPartCost(RawPart rawPart, PartCostCalculationParameters parameters)
        {
            PartCost rawPartCost = new PartCost();
            rawPartCost.PartId = rawPart.Guid;
            rawPartCost.Name = rawPart.Name;
            rawPartCost.Description = rawPart.Description;
            rawPartCost.Number = rawPart.Number;
            rawPartCost.PartIndex = rawPart.Index;
            rawPartCost.TargetPrice = rawPart.TargetPrice;
            rawPartCost.AmountPerAssembly = 1; // an entity has at most one raw part.
            rawPartCost.IsRawPart = true;

            // Calculate the sub-part's cost using the appropriate calculation version
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(rawPart.CalculationVariant, this.BasicSettings, this.MeasurementUnits);
            CalculationResult result = calculator.CalculatePartCost(rawPart, parameters);

            rawPartCost.FullCalculationResult = result;
            rawPartCost.TargetCost = result.Summary.TargetCost;

            return rawPartCost;
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for an Assembly, Part or Raw Part.
        /// <para />
        /// In order to calculate a correct result the entity's full parent hierarchy must be loaded, that is, the references chain from the entity
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// The calculated yearly production quantity/
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity can not be null.</exception>
        /// <exception cref="System.InvalidOperationException">The entity is not supported.</exception>
        protected virtual decimal CalculateYearlyProductionQuantityInternal(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity can not be null.");
            }

            Assembly currentAssy = null;

            // Contains the data for the full parent hierarchy of the entity. A data item contains the entity and some pieces of data common to all entities in the hierarchy.
            var parentHierarchyData = new List<YearlyProductionQuantityData>();

            // If the entity is a Part add it to the hierarchy on the last position. If the part is a raw part, add its parent part to the hierarchy before itself.
            var tempPart = entity as Part;
            if (tempPart != null)
            {
                parentHierarchyData.Add(new YearlyProductionQuantityData(tempPart));
                if (tempPart is RawPart)
                {
                    var parentPart = tempPart.ParentOfRawPart.FirstOrDefault();
                    if (parentPart != null)
                    {
                        parentHierarchyData.Insert(0, new YearlyProductionQuantityData(parentPart));
                        currentAssy = parentPart.Assembly;
                    }
                }
                else
                {
                    currentAssy = tempPart.Assembly;
                }
            }
            else
            {
                var assy = entity as Assembly;
                if (assy != null)
                {
                    currentAssy = assy;
                }
                else
                {
                    throw new InvalidOperationException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "The entity type '{0}' is not supported.",
                        entity.GetType().FullName));
                }
            }

            // Add all assemblies, up to the top-most one, to the hierarchy.            
            while (currentAssy != null)
            {
                parentHierarchyData.Insert(0, new YearlyProductionQuantityData(currentAssy));
                currentAssy = currentAssy.ParentAssembly;
            }

            // Calculate I.O Pieces per Year starting with the 1st assembly/part in the hierarchy. Each subsequent item's I.O Pieces per Year
            // will be calculated based on its parent's Gross batch Size and Batches per Year values.
            decimal parentGrossBatchSize = 0m;
            decimal parentBatchesPerYear = 0m;
            decimal currentIOPiecesPerYear = 0m;

            if (parentHierarchyData.Count > 0)
            {
                // The I.O Pieces per Year of the 1st item is its YearlyProductionQuantity property value.
                var firstItem = parentHierarchyData[0];
                currentIOPiecesPerYear = firstItem.YearlyProductionQuantity;
                parentBatchesPerYear = firstItem.BatchSizePerYear;
                parentGrossBatchSize = this.CalculateGrossBatchSize(firstItem.Entity, currentIOPiecesPerYear);

                for (int i = 1; i < parentHierarchyData.Count; i++)
                {
                    var item = parentHierarchyData[i];
                    currentIOPiecesPerYear = parentGrossBatchSize * parentBatchesPerYear;
                    parentBatchesPerYear = item.BatchSizePerYear;
                    parentGrossBatchSize = this.CalculateGrossBatchSize(item.Entity, currentIOPiecesPerYear);
                }
            }

            return currentIOPiecesPerYear;
        }

        /// <summary>
        /// Calculates the gross batch of an assembly or part's assembling/manufacturing process.
        /// <para />
        /// This method is used to support the enhanced scrap calculation.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="parentYearlyProductionQty">The yearly production quantity of the entity's parent Part or Assembly.</param>
        /// <returns>
        /// The entity process' gross batch size.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity can not be null.</exception>
        /// <exception cref="System.InvalidOperationException">The entity is not supported.</exception>
        /// <remarks>
        /// The Gross Batch Size is the actual batch size that must be used in order to produce the target (or net) batch size.
        /// For example, if the process must output 1000 parts in a batch, due to rejection of some parts it may have to actually produce 1100 parts (100 are rejected/scrapped).
        /// </remarks>        
        protected virtual decimal CalculateGrossBatchSize(object entity, decimal parentYearlyProductionQty)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity can not be null.");
            }

            Process process = null;
            decimal batchesPerYear = 0;

            var part = entity as Part;
            if (part != null)
            {
                process = part.Process;
                batchesPerYear = part.BatchSizePerYear.GetValueOrDefault();
            }
            else
            {
                var assy = entity as Assembly;
                if (assy != null)
                {
                    process = assy.Process;
                    batchesPerYear = assy.BatchSizePerYear.GetValueOrDefault();
                }
                else
                {
                    throw new InvalidOperationException(string.Format("The entity type {0} is not supported.", entity.GetType().FullName));
                }
            }

            decimal netBatchSize = 0m;
            if (batchesPerYear != 0)
            {
                netBatchSize = Math.Ceiling(parentYearlyProductionQty / batchesPerYear);
            }

            decimal grossBatchSize = netBatchSize;
            if (process != null)
            {
                foreach (var step in process.Steps.OrderByDescending(step => step.Index))
                {
                    var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                    if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                    {
                        grossBatchSize = Math.Ceiling((1m + step.ScrapAmount.GetValueOrDefault()) * grossBatchSize);
                    }
                }
            }

            return grossBatchSize;
        }

        #endregion

        #region Inner classes

        /// <summary>
        /// Holds the data of an entity that is necessary during the calculation of its Yearly Production Quantity.
        /// <para />
        /// This class was designed to be used by the <see cref="CalculateYearlyProductionQuantityInternal"/> method.
        /// </summary>        
        protected class YearlyProductionQuantityData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="YearlyProductionQuantityData"/> class.
            /// </summary>
            /// <param name="entity">The entity.</param>
            /// <param name="yearlyProductionQuantity">The entity's yearly production quantity.</param>
            /// <param name="batchSizePerYear">The entity's batch size per year.</param>
            public YearlyProductionQuantityData(object entity, decimal yearlyProductionQuantity, decimal batchSizePerYear)
            {
                this.Entity = entity;
                this.YearlyProductionQuantity = yearlyProductionQuantity;
                this.BatchSizePerYear = batchSizePerYear;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="YearlyProductionQuantityData"/> class.
            /// </summary>
            /// <param name="assembly">The assembly from which to extract the necessary data.</param>
            public YearlyProductionQuantityData(Assembly assembly)
            {
                this.Entity = assembly;
                this.YearlyProductionQuantity = assembly.YearlyProductionQuantity.GetValueOrDefault();
                this.BatchSizePerYear = assembly.BatchSizePerYear.GetValueOrDefault();
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="YearlyProductionQuantityData"/> class.
            /// </summary>
            /// <param name="part">The part from which to extract the necessary data.</param>
            public YearlyProductionQuantityData(Part part)
            {
                this.Entity = part;
                this.YearlyProductionQuantity = part.YearlyProductionQuantity.GetValueOrDefault();
                this.BatchSizePerYear = part.BatchSizePerYear.GetValueOrDefault();
            }

            /// <summary>
            /// Gets the entity whose data is wrapped by this instance.
            /// </summary>
            public object Entity { get; private set; }

            /// <summary>
            /// Gets the entity's yearly production quantity.
            /// </summary>
            public decimal YearlyProductionQuantity { get; private set; }

            /// <summary>
            /// Gets the entity's batch size per year.
            /// </summary>
            public decimal BatchSizePerYear { get; private set; }
        }

        #endregion Inner classes
    }
}