﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The input parameters for the cost calculation of a raw material.
    /// </summary>
    public class RawMaterialCostCalculationParameters
    {
        /// <summary>
        /// Gets or sets the percentage that is used when calculating the material overhead cost.
        /// </summary>        
        public decimal MaterialOverheadRate { get; set; }

        /// <summary>
        /// Gets or sets the asset rate needed to calculate the WIP cost.
        /// </summary>        
        public decimal AssetRate { get; set; }

        /// <summary>
        /// Gets or sets the batch size from part level.
        /// </summary>        
        public int BatchSize { get; set; }

        /// <summary>
        /// Gets or sets the annual production quantity from part level.
        /// </summary>        
        public int AnnualProductionQty { get; set; }
    }
}
