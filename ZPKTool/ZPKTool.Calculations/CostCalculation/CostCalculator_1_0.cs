﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The cost calculator implementation v1.0
    /// </summary>
    internal class CostCalculator_1_0 : ICostCalculator
    {
        /// <summary>
        /// The number of seconds in 1 hour.
        /// </summary>
        protected const int SecondsInHour = 3600;

        /// <summary>
        /// The number of months in 1 year.
        /// </summary>
        protected const int MonthsInYear = 12;

        /// <summary>
        /// The measurement units to be used during calculations.
        /// </summary>
        private IEnumerable<MeasurementUnit> measurementUnits;

        /// <summary>
        /// Prevents a default instance of the <see cref="CostCalculator_1_0" /> class from being created.
        /// </summary>
        private CostCalculator_1_0()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_0" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_0(BasicSetting basicSettings)
            : this()
        {
            if (basicSettings == null)
            {
                throw new ArgumentNullException("basicSettings", "The basic settings were null.");
            }

            this.BasicSettings = basicSettings;
            this.SetWeightUnitToDefault();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_0" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_0(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : this(basicSettings)
        {
            this.MeasurementUnits = measurementUnits;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the version of the ICostCalculator implementation.
        /// </summary>
        public virtual string Version { get; set; }

        /// <summary>
        /// Gets the basic settings data to be used during calculations.
        /// </summary>
        public BasicSetting BasicSettings { get; private set; }

        /// <summary>
        /// Gets the measurement units to be used during calculations.
        /// </summary>
        public IEnumerable<MeasurementUnit> MeasurementUnits
        {
            get
            {
                return this.measurementUnits;
            }

            private set
            {
                if (this.measurementUnits != value)
                {                    
                    this.measurementUnits = value;
                    this.OnMeasurementUnitsChanged(this.measurementUnits);
                }
            }
        }

        /// <summary>
        /// Gets the unit to which to convert quantities that represent weight during cost calculations.
        /// The values of weights are stored in data objects and database in kilograms so this unit is used to convert them
        /// to another weight system.
        /// <para />
        /// The default value is kilogram, which is the unit in which weight values are stored in the database.
        /// </summary>
        protected MeasurementUnit WeightUnit { get; private set; }

        #endregion Properties

        #region ICostCalculator implementation

        /// <summary>
        /// Computes the cost of an Assembly.
        /// </summary>
        /// <param name="assembly">The assembly whose cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the cost calculation.</param>
        /// <returns>
        /// An instance of <see cref="CalculationResult" /> that contains the cost breakdown of the assembly.
        /// </returns>
        public virtual CalculationResult CalculateAssemblyCost(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            this.CheckCalculationData(assembly);

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The parameters were null.");
            }

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = assembly.Number;
            calcResult.Name = assembly.Name;
            calcResult.Remark = string.Empty;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = assembly.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = assembly.TargetPrice;
            calcResult.PurchasePrice = assembly.PurchasePrice;
            calcResult.AdditionalCostsNotes = assembly.AdditionalCostsNotes;

            // Calculate the cost of the assembling process and add the results to the calculation result object.
            var processCalcParams = new AssemblyProcessCostCalculationParameters()
            {
                ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                ProjectCreateDate = parameters.ProjectCreateDate
            };

            ProcessCost processCost = CalculateProcessCost(assembly, processCalcParams);
            calcResult.ProcessCost = processCost;
            calcResult.CommoditiesCost.Add(processCost.CommoditiesCost);
            calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
            calcResult.InvestmentCost.Add(processCost.InvestmentCost);

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = assembly.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, assembly, productionCost, logisticCostRatio);
            }

            // Calculate all overheads and margins and save them in the result object. If the assembly's cost is calculated externally no overheads are applied.
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, assembly.OverheadSettings);
            }

            // Compute the cost of sub-parts and sub-assemblies. If the assembly's cost is any type of estimation, single parts are not included.
            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                SubPartsAndSubAssembliesCost cost = this.CalculateSubPartsAndSubAssembliesCost(assembly, parameters);

                calcResult.AssembliesCost.SumWith(cost.SubAssembliesCost);
                calcResult.PartsCost.SumWith(cost.SubPartsCost);

                // The external parts OH and margin are added to the commodity OH and margin.
                calcResult.OverheadCost.CommodityOverhead += cost.ExternalOverhead;
                calcResult.OverheadCost.CommodityMargin += cost.ExternalMargin;
                calcResult.SubpartsAndSubassembliesCost = cost.TotalCost;
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * assembly.PaymentTerms * (assembly.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(assembly, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Part or Raw Part.
        /// </summary>
        /// <param name="part">The part whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown of the given part.
        /// </returns>
        public virtual CalculationResult CalculatePartCost(Part part, PartCostCalculationParameters parameters)
        {
            this.CheckCalculationData(part);

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The parameters were null.");
            }

            decimal logisticCostRatio = 0;
            if (parameters.ProjectLogisticCostRatio.HasValue)
            {
                logisticCostRatio = parameters.ProjectLogisticCostRatio.Value;
            }
            else if (this.BasicSettings != null)
            {
                logisticCostRatio = this.BasicSettings.LogisticCostRatio;
            }

            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            CalculationResult calcResult = new CalculationResult();
            calcResult.PartNumber = part.Number;
            calcResult.Name = part.Name;
            calcResult.Remark = part.AdditionalRemarks;
            calcResult.CalculationAccuracy = calculationAccuracy;
            calcResult.External = part.IsExternal.GetValueOrDefault();
            calcResult.CalculatorVersion = this.Version;
            calcResult.TargetPrice = part.TargetPrice;
            calcResult.PurchasePrice = part.PurchasePrice;
            calcResult.AdditionalCostsNotes = part.AdditionalCostsNotes;

            var overheadSettings = part.OverheadSettings;

            if (calculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Compute the cost the part's raw materials.
                var materialParams = new RawMaterialCostCalculationParameters() { MaterialOverheadRate = overheadSettings.MaterialOverhead };
                var materialsCost = this.CalculateRawMaterialsCost(part.RawMaterials, materialParams);
                calcResult.RawMaterialsCost.Add(materialsCost);

                // Compute the cost of the part's commodities                        
                var commodityParams = new CommodityCostCalculationParameters()
                {
                    CommodityOverheadRate = overheadSettings.CommodityOverhead,
                    CommodityMarginRate = overheadSettings.CommodityMargin
                };

                CommoditiesCost commodityCost = this.CalculateCommoditiesCost(part.Commodities, commodityParams);
                calcResult.CommoditiesCost.Add(commodityCost);
            }

            // Compute the cost of the part's manufacturing process and add the cost data in the calculation result object
            var calcParams = new PartProcessCostCalculationParameters()
            {
                ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                ProjectCreateDate = parameters.ProjectCreateDate
            };

            ProcessCost processCost = this.CalculateProcessCost(part, calcParams);
            calcResult.ProcessCost = processCost;
            calcResult.ConsumablesCost.Add(processCost.ConsumablesCost);
            calcResult.InvestmentCost.Add(processCost.InvestmentCost);

            // Calculate the production cost. The calculation is based on the raw material, commodity, consumable and process costs.            
            decimal productionCost = 0m;
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                productionCost = part.EstimatedCost.GetValueOrDefault();
            }
            else
            {
                productionCost = calcResult.RawMaterialsCost.NetCostSum + calcResult.ConsumablesCost.CostsSum + calcResult.CommoditiesCost.CostsSum
                    + calcResult.ProcessCost.ManufacturingCostSum + calcResult.ProcessCost.RejectCostSum;
            }

            calcResult.ProductionCost = productionCost;

            // Calculate the logistics cost (the calculation is based only on the production cost).
            // Set the development, project investment, packaging and other costs.
            if (calculationAccuracy != PartCalculationAccuracy.Estimation && calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateAdditionalCosts(calcResult, part, productionCost, logisticCostRatio);
            }

            // Calculate the overheads. If the part's cost is calculated externally, the overheads are not applied
            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.CalculateOverheadAndMarginCosts(calcResult, overheadSettings);
            }

            // Calculate the total cost of the part
            calcResult.TotalCost = calcResult.ProductionCost + calcResult.SubpartsAndSubassembliesCost + calcResult.OverheadCost.TotalSumOverheadAndMargin
                + calcResult.ProjectInvest + calcResult.DevelopmentCost + calcResult.PackagingCost + calcResult.LogisticCost + calcResult.OtherCost;

            if (calculationAccuracy != PartCalculationAccuracy.OfferOrExternalCalculation && calculationAccuracy != PartCalculationAccuracy.Estimation)
            {
                // Calculate the Payment cost and add it to the total cost
                calcResult.PaymentCost = calcResult.TotalCost * part.PaymentTerms * (part.CountrySettings.InterestRate / 365);
                calcResult.TotalCost += calcResult.PaymentCost;
            }

            // Create the calculation result summary.
            this.CreateCalculationResultSummary(calcResult);

            // Calculate the part's weight and total investment.
            this.CalculateWeightAndCumulatedInvestment(part, calcResult);

            return calcResult;
        }

        /// <summary>
        /// Calculates the cost of a Commodity.
        /// </summary>
        /// <param name="commodity">The commodity whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the given commodity.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The commodity data was null.</exception>
        public virtual CommodityCost CalculateCommodityCost(Commodity commodity, CommodityCostCalculationParameters parameters)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "The commodity was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The commodity parameters were null.");
            }

            CommodityCost commodityCost = new CommodityCost();

            commodityCost.CommodityId = commodity.Guid;
            commodityCost.Name = commodity.Name;
            commodityCost.Remarks = commodity.Remarks;
            commodityCost.Manufacturer = commodity.Manufacturer != null ? commodity.Manufacturer.Name : null;
            commodityCost.Weight = commodity.Weight;
            commodityCost.RejectRatio = commodity.RejectRatio.GetValueOrDefault();
            commodityCost.OverheadRatio = parameters.CommodityOverheadRate;
            commodityCost.MarginRatio = parameters.CommodityMarginRate;
            commodityCost.Amount = commodity.Amount.GetValueOrDefault();
            commodityCost.Price = commodity.Price.GetValueOrDefault();

            commodityCost.Cost = commodityCost.Price * commodityCost.Amount;
            commodityCost.Overhead = commodityCost.Cost * commodityCost.OverheadRatio;
            commodityCost.Margin = commodityCost.Cost * commodityCost.MarginRatio;
            commodityCost.TotalCost = commodityCost.Cost + commodityCost.Overhead;

            return commodityCost;
        }

        /// <summary>
        /// Calculates the cost of the specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the given consumable.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The consumable data was null.</exception>
        public virtual ConsumableCost CalculateConsumableCost(Consumable consumable, ConsumableCostCalculationParameters parameters)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The consumable parameters were null.");
            }

            ConsumableCost consumableCost = new ConsumableCost();
            consumableCost.ConsumableId = consumable.Guid;
            consumableCost.Name = consumable.Name;
            consumableCost.Description = consumable.Description;
            consumableCost.Manufacturer = consumable.Manufacturer != null ? consumable.Manufacturer.Name : null;

            consumableCost.Price = consumable.Price.GetValueOrDefault();
            consumableCost.Amount = consumable.Amount.GetValueOrDefault();
            consumableCost.AmountUnit = consumable.AmountUnitBase;
            if (consumable.PriceUnitBase != null)
            {
                consumableCost.AmountUnitBaseSymbol = consumable.PriceUnitBase.Symbol;
            }

            decimal amount = consumableCost.Amount;
            if (consumable.AmountUnitBase != null
                && consumable.AmountUnitBase.ConversionRate > 1)
            {
                amount *= consumable.AmountUnitBase.ConversionRate;
            }

            consumableCost.Cost = amount * consumableCost.Price;
            consumableCost.Overhead = consumableCost.Cost * parameters.ConsumableOverheadRate;
            consumableCost.TotalCost = consumableCost.Cost + consumableCost.Overhead;

            return consumableCost;
        }

        /// <summary>
        /// Calculates the cost of a Die.
        /// </summary>
        /// <param name="die">The die to calculate the cost for.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The cost breakdown for the die.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The die data was null.</exception>
        public virtual DieCost CalculateDieCost(Die die, DieCostCalculationParameters parameters)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The die parameters were null.");
            }

            DieCost dieCost = new DieCost();
            dieCost.DieId = die.Guid;
            dieCost.DieName = die.Name;

            decimal dieLifeTime = die.LifeTime.GetValueOrDefault();
            decimal dieInvestment = die.Investment.GetValueOrDefault();
            decimal dieSetsNumberPaidByCustomer = die.DiesetsNumberPaidByCustomer.GetValueOrDefault();

            // Compute the number of dies per lifetime
            decimal diesPerLifeTime = 0m;
            if (dieLifeTime != 0m)
            {
                diesPerLifeTime = parameters.ParentLifecycleProductionQuantity / dieLifeTime;

                // Dies per lifetime must be at least 1
                if (diesPerLifeTime >= 1)
                {
                    diesPerLifeTime = Math.Round(diesPerLifeTime);
                }
                else
                {
                    diesPerLifeTime = 1m;
                }
            }

            dieCost.DiesPerLifeTime = diesPerLifeTime;

            decimal partsNo = 0m;
            if (die.CostAllocationBasedOnPartsPerLifeTime.GetValueOrDefault())
            {
                partsNo = parameters.ParentLifecycleProductionQuantity;
            }
            else
            {
                partsNo = die.CostAllocationNumberOfParts.GetValueOrDefault();
            }

            decimal maintenance =
                die.IsMaintenanceInPercentage.GetValueOrDefault() ? die.Maintenance.GetValueOrDefault() * dieInvestment : die.Maintenance.GetValueOrDefault();
            if (dieLifeTime != 0m)
            {
                dieCost.MaintenanceCost = maintenance / dieLifeTime;
            }

            // Compute the die cost
            if (partsNo != 0m)
            {
                if (dieSetsNumberPaidByCustomer < diesPerLifeTime)
                {
                    dieCost.Cost = (diesPerLifeTime - dieSetsNumberPaidByCustomer) * dieInvestment * die.AllocationRatio.GetValueOrDefault() / partsNo;
                }
                else
                {
                    decimal wear = die.IsWearInPercentage.GetValueOrDefault() ? die.Wear.GetValueOrDefault() * dieInvestment : die.Wear.GetValueOrDefault();
                    dieCost.Cost = (maintenance + wear) / partsNo;
                }
            }

            return dieCost;
        }

        /// <summary>
        /// Calculates the cost of a Machine.
        /// </summary>
        /// <param name="machine">The machine whose cost to calculate.</param>
        /// <param name="parameters">The input parameters needed for calculation.</param>
        /// <returns>
        /// An object containing the cost breakdown for the machine.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The machine data was null.</exception>
        public virtual MachineCost CalculateMachineCost(Machine machine, MachineCostCalculationParameters parameters)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine was null.");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters", "The machine parameters were null.");
            }

            MachineCost cost = new MachineCost();
            cost.MachineId = machine.Guid;
            cost.MachineName = machine.Name;
            cost.MaintenanceNotes = machine.MaintenanceNotes;

            decimal yearlyProductionHours = parameters.ProcessStepShiftsPerWeek
                * parameters.ProcessStepHoursPerShift
                * parameters.ProcessStepProductionWeeksPerYear;

            if (yearlyProductionHours != 0m)
            {
                cost.FloorCost = (machine.FloorSize.GetValueOrDefault() + machine.WorkspaceArea.GetValueOrDefault())
                    * parameters.CountrySettingsRentalCostProductionArea
                    * MonthsInYear
                    / yearlyProductionHours;
            }

            // The linear depreciation is used when the machine's age is larger than the depreciation period.
            int depreciationPeriod = machine.DepreciationPeriod.HasValue ? machine.DepreciationPeriod.Value : parameters.ProjectDepreciationPeriod;
            bool useLinearDepreciation = false;
            if (machine.ManufacturingYear.HasValue)
            {
                /*
                 * Set as accurately as possible the reference DateTime,
                 * relative to which is calculated the machine depreciation.
                 * If the machine CalculationCreationDate has been set, then we use it.
                 * Otherwise, if the Project CreateDate has been set, , then we use it.
                 * Otherwise we use the current date.
                 */
                var firstCalculationDate = machine.CalculationCreateDate.HasValue
                    ? machine.CalculationCreateDate
                    : parameters.ProjectCreateDate.HasValue
                        ? parameters.ProjectCreateDate.Value
                        : DateTime.Now;
                TimeSpan timeDiff = firstCalculationDate.Value - new DateTime((int)machine.ManufacturingYear.Value, 1, 1);
                if (timeDiff.TotalDays > depreciationPeriod * 365)
                {
                    useLinearDepreciation = true;
                }
            }

            if (useLinearDepreciation)
            {
                cost.DepreciationPerHour = 0m;
            }
            else if (depreciationPeriod != 0 && yearlyProductionHours != 0m)
            {
                decimal depreciationRate = machine.DepreciationRate.HasValue ? machine.DepreciationRate.Value : parameters.ProjectDepreciationRate;
                decimal totalMachineInvestment = machine.MachineInvestment.GetValueOrDefault()
                    + machine.SetupInvestment.GetValueOrDefault()
                    + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                    + machine.FundamentalSetupInvestment.GetValueOrDefault();
                cost.DepreciationPerHour = ((totalMachineInvestment / depreciationPeriod) + (totalMachineInvestment * depreciationRate)) / yearlyProductionHours;
            }

            if (yearlyProductionHours != 0m)
            {
                // Determine the consumables cost.
                decimal consumablesCost = 0m;
                if (machine.ManualConsumableCostCalc)
                {
                    consumablesCost = machine.ManualConsumableCostPercentage.GetValueOrDefault() * machine.MachineInvestment.GetValueOrDefault();
                    cost.CalculatedConsumablesCost = consumablesCost;
                }
                else
                {
                    consumablesCost = machine.ConsumablesCost.GetValueOrDefault();
                }

                // Calculate the maintenance cost
                if (machine.CalculateWithKValue.GetValueOrDefault())
                {
                    cost.MaintenancePerHour = ((machine.KValue.GetValueOrDefault() * machine.MachineInvestment.GetValueOrDefault()) + consumablesCost)
                        / yearlyProductionHours;
                }
                else
                {
                    cost.MaintenancePerHour = (machine.ExternalWorkCost.GetValueOrDefault() + machine.MaterialCost.GetValueOrDefault()
                        + consumablesCost + machine.RepairsCost.GetValueOrDefault())
                        / yearlyProductionHours;
                }
            }

            cost.EnergyCostPerHour = ((machine.PowerConsumption.GetValueOrDefault() * parameters.CountrySettingsEnergyCost)
                + (machine.AirConsumption.GetValueOrDefault() * parameters.CountrySettingsAirCost)
                + (machine.WaterConsumption.GetValueOrDefault() * parameters.CountrySettingsWaterCost))
                * machine.FullLoadRate.GetValueOrDefault();

            cost.FullCostPerHour = cost.DepreciationPerHour + cost.MaintenancePerHour + cost.EnergyCostPerHour + cost.FloorCost;

            return cost;
        }

        /// <summary>
        /// Calculates the cost of a Raw Material.
        /// </summary>
        /// <param name="material">The raw material whose cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation. Can be null if you need to calculate only the net cost.</param>
        /// <returns>
        /// An object containing the cost breakdown for the raw material.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The material was null.</exception>
        public virtual RawMaterialCost CalculateRawMaterialCost(RawMaterial material, RawMaterialCostCalculationParameters parameters)
        {
            if (material == null)
            {
                throw new ArgumentNullException("material", "The material was null.");
            }

            RawMaterialCost materialCost = new RawMaterialCost();
            materialCost.RawMaterialId = material.Guid;
            materialCost.Name = material.Name;
            materialCost.Price = material.Price.GetValueOrDefault();

            materialCost.MaterialWeight = material.Quantity.GetValueOrDefault();
            materialCost.PartWeight = material.ParentWeight.GetValueOrDefault();

            var weightUnit = material.QuantityUnitBase != null ? material.QuantityUnitBase : material.ParentWeightUnitBase;
            materialCost.WeightUnit = weightUnit != null ? weightUnit.Symbol : null;

            this.CalculateRawMaterialNetCost(material, materialCost);

            if (parameters != null)
            {
                materialCost.Overhead = materialCost.NetCost * parameters.MaterialOverheadRate;
            }

            materialCost.TotalCost = materialCost.NetCost + materialCost.Overhead;

            return materialCost;
        }

        /// <summary>
        /// Calculates the weight of a part, in kilograms.
        /// </summary>
        /// <param name="part">The part whose whose weight to calculate.</param>
        /// <returns>
        /// The part's weight.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The part was null.</exception>
        public virtual decimal CalculatePartWeight(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            return part.RawMaterials.Where(m => !m.IsDeleted).Sum(material => material.ParentWeight.GetValueOrDefault());
        }

        /// <summary>
        /// Calculates the weight of the material going into the process of a part, in kilograms.
        /// </summary>
        /// <param name="part">The part for which to calculate.</param>
        /// <returns>
        /// The material weight.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">partData;The part data was null.</exception>
        public virtual decimal CalculateProcessMaterial(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            // Process material = part weight + sprue + loss
            // Material Weight is part weight + sprue
            decimal processMaterialWeight =
                part.RawMaterials.Where(m => !m.IsDeleted).Sum(material => material.Quantity.GetValueOrDefault() * (1m + material.Loss.GetValueOrDefault()));

            return processMaterialWeight;
        }

        /// <summary>
        /// Calculates the net number of pieces produced in an assembly's lifetime.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The net number of assemblies.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The assembly was null.</exception>
        public virtual decimal CalculateNetLifetimeProductionQuantity(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            return (decimal)assembly.YearlyProductionQuantity.GetValueOrDefault() * (decimal)assembly.LifeTime.GetValueOrDefault();
        }

        /// <summary>
        /// Calculates the net number of pieces produced in a part's lifetime.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The net number of parts.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">part;The part was null.</exception>
        public virtual decimal CalculateNetLifetimeProductionQuantity(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            return (decimal)part.YearlyProductionQuantity.GetValueOrDefault() * (decimal)part.LifeTime.GetValueOrDefault();
        }

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in an assembly's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        public virtual decimal CalculateGrossLifetimeProductionQuantity(Assembly assembly, AssemblyCostCalculationParameters parameters)
        {
            return this.CalculateNetLifetimeProductionQuantity(assembly);
        }

        /// <summary>
        /// Calculates the gross number of pieces needed to be produced in a part's lifetime order to obtain the net number of parts
        /// calculated with <see cref="CalculateNetLifetimeProductionQuantity" />.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The gross number of needed parts.
        /// </returns>
        public virtual decimal CalculateGrossLifetimeProductionQuantity(Part part, PartCostCalculationParameters parameters)
        {
            return this.CalculateNetLifetimeProductionQuantity(part);
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for an Assembly.
        /// <para />
        /// In order to calculate a correct result the assembly's full parent hierarchy must be loaded, that is, the references chain from the assembly
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        public virtual decimal CalculateYearlyProductionQuantity(Assembly assembly)
        {
            return -1m;
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for a Part or Raw Part.
        /// <para />
        /// In order to calculate a correct result the part's full parent hierarchy must be loaded, that is, the references chain from the part
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// <para />
        /// This method is implemented starting with calculator version 1.3; the older calculators return -1.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The calculated yearly production quantity or -1 if the cost calculator version is older than 1.3.
        /// </returns>
        public virtual decimal CalculateYearlyProductionQuantity(Part part)
        {
            return -1m;
        }

        #endregion ICostCalculator implementation

        #region Calculation helper methods

        /// <summary>
        /// Calculates the cost of all sub-assemblies and parts used in the assembly's process.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parameters">The input parameters for the assembly cost calculation.</param>
        /// <returns>
        /// The cost of sub-parts and sub-assemblies.
        /// </returns>
        protected virtual SubPartsAndSubAssembliesCost CalculateSubPartsAndSubAssembliesCost(
            Assembly assembly,
            AssemblyCostCalculationParameters parameters)
        {
            SubPartsAndSubAssembliesCost totalCost = new SubPartsAndSubAssembliesCost();
            if (assembly.Process == null)
            {
                return totalCost;
            }

            foreach (var step in assembly.Process.Steps)
            {
                // Compute the cost of sub-assemblies used in the current step
                foreach (var assemblyAmount in step.AssemblyAmounts)
                {
                    var subAssembly = assembly.Subassemblies.FirstOrDefault(a => a.Guid == assemblyAmount.FindAssemblyId());
                    if (subAssembly == null || subAssembly.IsDeleted)
                    {
                        // The assembly referred to by the amount does not exist. This is the case when the assembly is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    AssemblyCost crtAssyCost = totalCost.SubAssembliesCost.AssemblyCosts.FirstOrDefault(a => a.AssemblyId == subAssembly.Guid);
                    if (crtAssyCost != null)
                    {
                        // If the cost was already calculated for the assembly of the current amount then just update it
                        crtAssyCost.AmountPerAssembly += assemblyAmount.Amount;
                        totalCost.ExternalOverhead += assemblyAmount.Amount * crtAssyCost.ExternalOverhead;
                        totalCost.ExternalMargin += assemblyAmount.Amount * crtAssyCost.ExternalMargin;
                        totalCost.SubAssembliesCost.AssemblyAmountsSum += assemblyAmount.Amount;
                        totalCost.SubAssembliesCost.AssemblyCostsSum += assemblyAmount.Amount * crtAssyCost.TargetCost;
                    }
                    else
                    {
                        AssemblyCost assemblyCost = new AssemblyCost();
                        assemblyCost.AssemblyId = subAssembly.Guid;
                        assemblyCost.Name = subAssembly.Name;
                        assemblyCost.Description = subAssembly.Description;
                        assemblyCost.Number = subAssembly.Number;
                        assemblyCost.AssemblyIndex = subAssembly.Index;
                        assemblyCost.TargetPrice = subAssembly.TargetPrice;
                        assemblyCost.AmountPerAssembly = assemblyAmount.Amount;

                        // Calculate the sub-assembly's cost using the appropriate calculation version
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subAssembly.CalculationVariant, this.BasicSettings, this.MeasurementUnits);
                        CalculationResult result = calculator.CalculateAssemblyCost(subAssembly, parameters);

                        assemblyCost.FullCalculationResult = result;
                        assemblyCost.TargetCost = result.Summary.TargetCost;

                        // Apply a commodity overhead and margin for external assemblies.
                        if (subAssembly.IsExternal.GetValueOrDefault())
                        {
                            assemblyCost.ExternalOverhead = result.Summary.TargetCost * assembly.OverheadSettings.CommodityOverhead;
                            assemblyCost.ExternalMargin = result.Summary.TargetCost * assembly.OverheadSettings.CommodityMargin;

                            totalCost.ExternalOverhead += assemblyAmount.Amount * assemblyCost.ExternalOverhead;
                            totalCost.ExternalMargin += assemblyAmount.Amount * assemblyCost.ExternalMargin;
                        }

                        totalCost.SubAssembliesCost.AddCost(assemblyCost);
                        totalCost.InvestmentCost.Add(result.InvestmentCost);
                    }
                }

                // Compute the cost of parts used in the current step
                foreach (var partAmount in step.PartAmounts)
                {
                    var part = assembly.Parts.FirstOrDefault(p => p.Guid == partAmount.FindPartId());
                    if (part == null || part.IsDeleted)
                    {
                        // The part referred to by the amount does not exist. This is the case when the part is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    PartCost crtPartCost = totalCost.SubPartsCost.PartCosts.FirstOrDefault(p => p.PartId == part.Guid);
                    if (crtPartCost != null)
                    {
                        // If the cost was already calculated for the assembly of the current amount then just update it
                        crtPartCost.AmountPerAssembly += partAmount.Amount;
                        totalCost.ExternalOverhead += partAmount.Amount * crtPartCost.ExternalOverhead;
                        totalCost.ExternalMargin += partAmount.Amount * crtPartCost.ExternalMargin;
                        totalCost.SubPartsCost.PartAmountsSum += partAmount.Amount;
                        totalCost.SubPartsCost.PartCostsSum += partAmount.Amount * crtPartCost.TargetCost;
                    }
                    else
                    {
                        PartCost partCost = new PartCost();
                        partCost.PartId = part.Guid;
                        partCost.Name = part.Name;
                        partCost.Description = part.Description;
                        partCost.Number = part.Number;
                        partCost.PartIndex = part.Index;
                        partCost.TargetPrice = part.TargetPrice;
                        partCost.AmountPerAssembly = partAmount.Amount;

                        // Calculate the sub-part's cost using the appropriate calculation version
                        var partCalculationParams = new PartCostCalculationParameters()
                        {
                            ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                            ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                            ProjectLogisticCostRatio = parameters.ProjectLogisticCostRatio,
                            ProjectCreateDate = parameters.ProjectCreateDate
                        };

                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant, this.BasicSettings, this.MeasurementUnits);
                        CalculationResult result = calculator.CalculatePartCost(part, partCalculationParams);

                        partCost.FullCalculationResult = result;
                        partCost.TargetCost = result.Summary.TargetCost;

                        // Apply a commodity overhead and margin for external assemblies
                        if (part.IsExternal.GetValueOrDefault())
                        {
                            partCost.ExternalOverhead = result.Summary.TargetCost * assembly.OverheadSettings.CommodityOverhead;
                            partCost.ExternalMargin = result.Summary.TargetCost * assembly.OverheadSettings.CommodityMargin;

                            totalCost.ExternalOverhead += partAmount.Amount * partCost.ExternalOverhead;
                            totalCost.ExternalMargin += partAmount.Amount * partCost.ExternalMargin;
                        }

                        totalCost.SubPartsCost.AddCost(partCost);
                        totalCost.InvestmentCost.Add(result.InvestmentCost);
                    }
                }
            }

            return totalCost;
        }

        /// <summary>
        /// Computes the cost of multiple raw materials.
        /// </summary>
        /// <param name="materials">The raw materials.</param>
        /// <param name="parameters">The input parameters for the cost calculation of each material.</param>
        /// <returns>
        /// An object containing the cost breakdown for all materials.
        /// </returns>
        protected virtual RawMaterialsCost CalculateRawMaterialsCost(
            IEnumerable<RawMaterial> materials,
            RawMaterialCostCalculationParameters parameters)
        {
            RawMaterialsCost totalCost = new RawMaterialsCost();
            foreach (var material in materials.Where(m => !m.IsDeleted))
            {
                RawMaterialCost materialCost = CalculateRawMaterialCost(material, parameters);
                totalCost.AddCost(materialCost);
            }

            return totalCost;
        }

        /// <summary>
        /// Computes the cost of the specified consumables.
        /// </summary>
        /// <param name="consumables">The consumables.</param>
        /// <param name="parameters">The input parameters for the calculation of each consumable.</param>
        /// <returns>
        /// An object containing the cost breakdown for all consumables.
        /// </returns>
        protected virtual ConsumablesCost CalculateConsumablesCost(
            IEnumerable<Consumable> consumables,
            ConsumableCostCalculationParameters parameters)
        {
            ConsumablesCost totalCost = new ConsumablesCost();
            foreach (var consumable in consumables.Where(c => !c.IsDeleted))
            {
                ConsumableCost consumableCost = this.CalculateConsumableCost(consumable, parameters);
                totalCost.AddCost(consumableCost);
            }

            return totalCost;
        }

        /// <summary>
        /// Computes the cost of the specified commodities.
        /// </summary>
        /// <param name="commodities">The commodities.</param>
        /// <param name="parameters">The input parameters used to calculate the cost of each commodity.</param>
        /// <returns>
        /// An object containing the cost breakdown for all commodities.
        /// </returns>
        protected virtual CommoditiesCost CalculateCommoditiesCost(
            IEnumerable<Commodity> commodities,
            CommodityCostCalculationParameters parameters)
        {
            CommoditiesCost totalCost = new CommoditiesCost();
            foreach (var commodity in commodities.Where(c => !c.IsDeleted))
            {
                CommodityCost commodityCost = this.CalculateCommodityCost(commodity, parameters);
                totalCost.AddCost(commodityCost);
            }

            return totalCost;
        }

        /// <summary>
        /// Calculates the cost of an assembling process (the process of an assembly).
        /// </summary>
        /// <param name="assembly">The assembly whose process cost to calculate.</param>
        /// <param name="parameters">The parameters necessary for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>        
        protected virtual ProcessCost CalculateProcessCost(Assembly assembly, AssemblyProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || assembly.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();
            foreach (AssemblyProcessStep step in assembly.Process.Steps)
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    // Calculate the cost of commodities in the step
                    var commodityCalculationParams = new CommodityCostCalculationParameters()
                    {
                        CommodityOverheadRate = assembly.OverheadSettings.CommodityOverhead,
                        CommodityMarginRate = assembly.OverheadSettings.CommodityMargin
                    };
                    var commodities = step.Commodities.Where(c => !c.IsDeleted);
                    var commoditiesCost = this.CalculateCommoditiesCost(commodities, commodityCalculationParams);
                    processCost.CommoditiesCost.Add(commoditiesCost);

                    // Calculate the cost of consumables in the step
                    var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = assembly.OverheadSettings.ConsumableOverhead };
                    var consumables = step.Consumables.Where(c => !c.IsDeleted);
                    var consumablesCost = this.CalculateConsumablesCost(consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                var assyLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(assembly);

                // Calculate the cost of the step.
                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    assembly.CountrySettings,
                    assembly.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    assyLifetimeProdQty,
                    assembly.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);
                stepCost.ParentPartIsExternal = assembly.IsExternal.GetValueOrDefault();
                processCost.AddStepCost(stepCost);
            }

            return processCost;
        }

        /// <summary>
        /// Calculates the cost of a manufacturing process (the process of a part).
        /// </summary>
        /// <param name="part">The part whose process cost to calculate.</param>
        /// <param name="parameters">The input parameters for the calculation.</param>
        /// <returns>
        /// The process' cost.
        /// </returns>        
        protected virtual ProcessCost CalculateProcessCost(Part part, PartProcessCostCalculationParameters parameters)
        {
            var calculationAccuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            if (calculationAccuracy != PartCalculationAccuracy.FineCalculation
                || part.Process == null)
            {
                return new ProcessCost();
            }

            ProcessCost processCost = new ProcessCost();
            foreach (PartProcessStep step in part.Process.Steps)
            {
                var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                if (stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    // Calculate the cost of consumables in the step
                    var consumableCalcParams = new ConsumableCostCalculationParameters() { ConsumableOverheadRate = part.OverheadSettings.ConsumableOverhead };
                    var consumables = step.Consumables.Where(c => !c.IsDeleted);
                    var consumablesCost = this.CalculateConsumablesCost(consumables, consumableCalcParams);
                    processCost.ConsumablesCost.Add(consumablesCost);
                }

                // Calculate the cost of the step.
                var partLifetimeProdQty = this.CalculateNetLifetimeProductionQuantity(part);
                ProcessStepCost stepCost = CalculateProcessStepCost(
                    step,
                    part.CountrySettings,
                    part.OverheadSettings,
                    parameters.ProjectDepreciationPeriod,
                    parameters.ProjectDepreciationRate,
                    partLifetimeProdQty,
                    part.SBMActive.GetValueOrDefault(),
                    parameters.ProjectCreateDate);
                stepCost.ParentPartIsExternal = part.IsExternal.GetValueOrDefault();
                processCost.AddStepCost(stepCost);
            }

            return processCost;
        }

        /// <summary>
        /// Computes the costs of a process step.
        /// </summary>
        /// <param name="step">The process step to calculate the costs for.</param>
        /// <param name="countrySettings">The country settings to be used during the calculations.</param>
        /// <param name="overheadSettings">The overhead settings to be used during the calculations.</param>
        /// <param name="projectDepreciationPeriod">The project depreciation period.</param>
        /// <param name="projectDepreciationRate">The project depreciation rate.</param>
        /// <param name="partLifeCycleProductionQuantity">The life cycle production quantity of the part/assembly to which the step belongs to.</param>
        /// <param name="toolingActive">a value indicating whether tooling is active for the process</param>
        /// <param name="projectCreateDate">The project create date.</param>
        /// <returns>
        /// An object containing the costs for the given process step.
        /// </returns>
        protected virtual ProcessStepCost CalculateProcessStepCost(
            ProcessStep step,
            CountrySetting countrySettings,
            OverheadSetting overheadSettings,
            int projectDepreciationPeriod,
            decimal projectDepreciationRate,
            decimal partLifeCycleProductionQuantity,
            bool toolingActive,
            DateTime? projectCreateDate)
        {
            ProcessStepCost stepCost = new ProcessStepCost();
            stepCost.StepId = step.Guid;
            stepCost.StepName = step.Name;
            stepCost.StepIndex = step.Index;
            stepCost.IsExternal = step.IsExternal.GetValueOrDefault();
            stepCost.SBMActive = toolingActive;
            stepCost.ManufacturingOverheadRate = overheadSettings.ManufacturingOverhead;

            // If the step calculation accuracy is estimated then the estimated price is the step's total cost.
            var stepAccuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
            if (stepAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                stepCost.ManufacturingCostIsEstimated = true;
                stepCost.ManufacturingCost = step.Price.GetValueOrDefault();
                return stepCost;
            }

            // Copy the most used step parameters in local not nullable variables
            decimal stepPartsPerCycle = step.PartsPerCycle.GetValueOrDefault();
            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
            decimal stepCycleTime = step.CycleTime.GetValueOrDefault();
            int stepBatchSize = step.BatchSize.GetValueOrDefault();

            // Calculate machines cost
            if (step.Machines.Count > 0)
            {
                var machineCalcParams = new MachineCostCalculationParameters()
                {
                    CountrySettingsAirCost = countrySettings.AirCost,
                    CountrySettingsEnergyCost = countrySettings.EnergyCost,
                    CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost,
                    CountrySettingsWaterCost = countrySettings.WaterCost,
                    ProcessStepHoursPerShift = step.HoursPerShift.GetValueOrDefault(),
                    ProcessStepShiftsPerWeek = step.ShiftsPerWeek.GetValueOrDefault(),
                    ProcessStepProductionWeeksPerYear = step.ProductionWeeksPerYear.GetValueOrDefault(),
                    ProjectDepreciationPeriod = projectDepreciationPeriod,
                    ProjectDepreciationRate = projectDepreciationRate,
                    ProjectCreateDate = projectCreateDate
                };

                decimal machinesCostSum = 0m;
                foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                {
                    MachineCost machineCost = CalculateMachineCost(machine, machineCalcParams);
                    stepCost.MachineCosts.Add(machineCost);

                    if (machine.IsProjectSpecific)
                    {
                        if (partLifeCycleProductionQuantity != 0m && stepPartsPerCycle != 0m)
                        {
                            decimal totalInvest = machine.MachineInvestment.GetValueOrDefault()
                                + machine.SetupInvestment.GetValueOrDefault()
                                + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                                + machine.FundamentalSetupInvestment.GetValueOrDefault();

                            machinesCostSum += (totalInvest / partLifeCycleProductionQuantity)
                                + ((stepProcessTime / stepPartsPerCycle) * (machineCost.EnergyCostPerHour / SecondsInHour))
                                + machineCost.FloorCost;
                        }
                    }
                    else if (stepPartsPerCycle != 0m)
                    {
                        machinesCostSum += (machineCost.FullCostPerHour / SecondsInHour) * (stepProcessTime / stepPartsPerCycle);
                    }

                    // Calculate the investment cost for the machine
                    decimal machineInvestment = machine.MachineInvestment.GetValueOrDefault()
                        + machine.SetupInvestment.GetValueOrDefault()
                        + machine.AdditionalEquipmentInvestment.GetValueOrDefault()
                        + machine.FundamentalSetupInvestment.GetValueOrDefault();

                    stepCost.InvestmentCost.AddMachineInvestment(new InvestmentCostSingle(machine.Guid, machine.Name, machineInvestment));
                }

                stepCost.MachineCost = machinesCostSum;
            }

            // Calculate the setup cost
            if (stepPartsPerCycle != 0m && stepCycleTime != 0m && stepBatchSize != 0)
            {
                int stepSetupsPerBatch = step.SetupsPerBatch.GetValueOrDefault();
                decimal stepSetupTime = step.SetupTime.GetValueOrDefault();
                decimal stepMaxDownTime = step.MaxDownTime.GetValueOrDefault();

                stepCost.SetupCost = (((((stepSetupsPerBatch * (stepSetupTime + stepMaxDownTime)) / SecondsInHour) / stepPartsPerCycle)
                    * ((step.SetupUnskilledLabour.GetValueOrDefault() * countrySettings.UnskilledLaborCost)
                    + (step.SetupSkilledLabour.GetValueOrDefault() * countrySettings.SkilledLaborCost)
                    + (step.SetupForeman.GetValueOrDefault() * countrySettings.ForemanCost)
                    + (step.SetupTechnicians.GetValueOrDefault() * countrySettings.TechnicianCost)
                    + (step.SetupEngineers.GetValueOrDefault() * countrySettings.EngineerCost))) +
                    (((stepSetupsPerBatch * (stepSetupTime + stepMaxDownTime)) / SecondsInHour)
                    * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                    / stepBatchSize;

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal setupExtraShiftCostPerPart =
                        (((((stepSetupsPerBatch * (stepSetupTime + stepMaxDownTime)) / SecondsInHour) / stepPartsPerCycle)
                        * ((step.SetupUnskilledLabour.GetValueOrDefault() * (countrySettings.UnskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupSkilledLabour.GetValueOrDefault() * (countrySettings.SkilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupForeman.GetValueOrDefault() * (countrySettings.ForemanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupTechnicians.GetValueOrDefault() * (countrySettings.TechnicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.SetupEngineers.GetValueOrDefault() * (countrySettings.EngineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))))
                        + (((stepSetupsPerBatch * (stepSetupTime + stepMaxDownTime)) / SecondsInHour)
                        * stepCost.MachineCost * (SecondsInHour / stepCycleTime)))
                        / stepBatchSize;

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (setupExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.SetupCost += extraShiftsCost;
                }
            }

            // Calculate the labor cost
            if (stepPartsPerCycle != 0 && stepCycleTime != 0m)
            {
                stepCost.DirectLabourCost = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                    * ((step.ProductionUnskilledLabour.GetValueOrDefault() * countrySettings.UnskilledLaborCost)
                    + (step.ProductionSkilledLabour.GetValueOrDefault() * countrySettings.SkilledLaborCost)
                    + (step.ProductionForeman.GetValueOrDefault() * countrySettings.ForemanCost)
                    + (step.ProductionTechnicians.GetValueOrDefault() * countrySettings.TechnicianCost)
                    + (step.ProductionEngineers.GetValueOrDefault() * countrySettings.EngineerCost));

                // Calculate and add the extra shifts costs
                if (step.ExceedShiftCost)
                {
                    decimal directLaborExtraShiftCostPerPart = (stepCycleTime / SecondsInHour / stepPartsPerCycle)
                        * ((step.ProductionUnskilledLabour.GetValueOrDefault() * (countrySettings.UnskilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionSkilledLabour.GetValueOrDefault() * (countrySettings.SkilledLaborCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionForeman.GetValueOrDefault() * (countrySettings.ForemanCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionTechnicians.GetValueOrDefault() * (countrySettings.TechnicianCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault())))
                        + (step.ProductionEngineers.GetValueOrDefault() * (countrySettings.EngineerCost * (1m + step.ShiftCostExceedRatio.GetValueOrDefault()))));

                    decimal partsPerShift = ((step.HoursPerShift.GetValueOrDefault() / SecondsInHour) / stepCycleTime) * stepPartsPerCycle;
                    decimal extraShiftsCost = (directLaborExtraShiftCostPerPart * partsPerShift) * step.ExtraShiftsNumber.GetValueOrDefault();

                    stepCost.DirectLabourCost += extraShiftsCost;
                }
            }

            // Calculate the dies cost
            if (step.Dies.Count > 0)
            {
                var dieCalcParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = partLifeCycleProductionQuantity };
                foreach (var die in step.Dies.Where(d => !d.IsDeleted))
                {
                    DieCost dieCost = CalculateDieCost(die, dieCalcParams);
                    stepCost.DieCosts.Add(dieCost);

                    stepCost.ToolAndDieCost += dieCost.Cost;
                    stepCost.DiesMaintenanceCost += dieCost.MaintenanceCost;

                    stepCost.InvestmentCost.AddDieInvestment(new InvestmentCostSingle(die.Guid, die.Name, die.Investment.GetValueOrDefault()));
                }
            }

            // Calculate the manufacturing cost. Add ToolAndDieCost to ManufacturingCost only if the parent part's SBM is active.
            stepCost.ManufacturingCost = stepCost.MachineCost + stepCost.SetupCost + stepCost.DirectLabourCost;
            if (toolingActive)
            {
                stepCost.ManufacturingCost += stepCost.ToolAndDieCost;
            }

            // Calculate the scrap and overhead.
            stepCost.ManufacturingOverheadCost = stepCost.ManufacturingCost * overheadSettings.ManufacturingOverhead;
            stepCost.RejectCost = stepCost.ManufacturingCost * step.ScrapAmount.GetValueOrDefault();

            return stepCost;
        }

        /// <summary>
        /// Calculates the net cost of a raw material.
        /// The net cost is the cost without overheads or margins.
        /// </summary>
        /// <param name="material">The raw material for which to calculate the cost.</param>
        /// <param name="materialCost">The cost object where to put the calculated costs for the raw material.</param>
        protected virtual void CalculateRawMaterialNetCost(RawMaterial material, RawMaterialCost materialCost)
        {
            // Adjust the raw material's price for the current weight unit used by the calculator.
            var priceUnit = material.QuantityUnitBase != null ? material.QuantityUnitBase : material.ParentWeightUnitBase;
            decimal price = CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(material.Price.GetValueOrDefault(), priceUnit, this.WeightUnit);

            decimal partWeight = material.ParentWeight.GetValueOrDefault() / this.WeightUnit.ConversionRate;
            decimal materialWeight = material.Quantity.GetValueOrDefault() / this.WeightUnit.ConversionRate;

            materialCost.BaseCost = partWeight * price;

            // In calculation version 1.0 the scrap refund was Yield only, so we subtract it from the scrap cost
            decimal scrapCost = (materialWeight - partWeight) * price;
            decimal scrapRefund = scrapCost * material.ScrapRefundRatio.GetValueOrDefault();
            materialCost.ScrapCost = scrapCost - scrapRefund;
            materialCost.ScrapRefund = scrapRefund;

            materialCost.LossCost = materialWeight * material.Loss.GetValueOrDefault() * price;
            materialCost.RejectCost = 0m;
            materialCost.NetCost = materialCost.BaseCost + materialCost.ScrapCost + materialCost.LossCost;
        }

        /// <summary>
        /// Calculates the overhead and margin costs and stores them in the OverheadCost member of the calculation result.
        /// </summary>
        /// <param name="result">The calculation result.</param>
        /// <param name="overheadSettings">The overhead settings to use during the calculations.</param>
        protected virtual void CalculateOverheadAndMarginCosts(CalculationResult result, OverheadSetting overheadSettings)
        {
            OverheadCost overheadCost = result.OverheadCost;
            overheadCost.OverheadSettings = overheadSettings.Copy();

            // Calculate overheads
            overheadCost.RawMaterialOverhead += result.RawMaterialsCost.OverheadSum;
            overheadCost.ConsumableOverhead += result.ConsumablesCost.OverheadSum;
            overheadCost.CommodityOverhead += result.CommoditiesCost.OverheadSum;
            overheadCost.ManufacturingOverhead += result.ProcessCost.ManufacturingOverheadSum;
            overheadCost.OtherCostOverhead += result.OtherCost * overheadSettings.OtherCostOHValue;
            overheadCost.PackagingOverhead += result.PackagingCost * overheadSettings.PackagingOHValue;
            overheadCost.LogisticOverhead += result.LogisticCost * overheadSettings.LogisticOHValue;
            overheadCost.SalesAndAdministrationOverhead += result.ProductionCost * overheadSettings.SalesAndAdministrationOHValue;

            // Calculate margins
            overheadCost.RawMaterialMargin += result.RawMaterialsCost.NetCostSum * overheadSettings.MaterialMargin;
            overheadCost.ConsumableMargin += result.ConsumablesCost.CostsSum * overheadSettings.ConsumableMargin;
            overheadCost.CommodityMargin += result.CommoditiesCost.CostsSum * overheadSettings.CommodityMargin;
            overheadCost.ManufacturingMargin += result.ProcessCost.ManufacturingCostSum * overheadSettings.ManufacturingMargin;

            // The external work margin is calculated only for external steps            
            var externalSteps = result.ProcessCost.StepCosts.Where(sc => sc.IsExternal);
            decimal labourCost = externalSteps.Sum(s => s.TotalLabourCost);
            overheadCost.ExternalWorkMargin += labourCost * overheadSettings.ExternalWorkMargin;

            // If the Manufacturing Overhead is inputted at process step level then calculate the average Manufacturing
            // Overhead value across the process and store it in the OverheadSettings attached to the result.
            if (result.ProcessCost.StepCosts.Any(cost => cost.ManufacturingOverheadRate != overheadSettings.ManufacturingOverhead)
                && result.ProcessCost.ManufacturingCostSum != 0m)
            {
                overheadCost.ManufacturingOverheadRateAverage = result.ProcessCost.ManufacturingOverheadSum / result.ProcessCost.ManufacturingCostSum;
            }
            else
            {
                // There is no step that specifies a different manufacturing overhead rate -> use the one from the provided overhead settings.
                overheadCost.ManufacturingOverheadRateAverage = overheadCost.OverheadSettings.ManufacturingOverhead;
            }
        }

        /// <summary>
        /// Creates the summary for the specified calculation result.
        /// The summary is stored in the calculation result's "Summary" member.
        /// </summary>
        /// <param name="result">The calculation result.</param>
        protected virtual void CreateCalculationResultSummary(CalculationResult result)
        {
            CalculationResultSummary summary = result.Summary;

            summary.RawMaterialCost = result.RawMaterialsCost.NetCostSum;
            summary.WIPCost = result.RawMaterialsCost.WIPCostSum;
            summary.CommoditiesCost = result.CommoditiesCost.CostsSum;
            summary.ConsumableCost = result.ConsumablesCost.CostsSum;
            summary.TotalManufacturingCost = result.ProcessCost.ManufacturingCostSum;
            summary.ManufacturingRejectCost = result.ProcessCost.RejectCostSum;
            summary.ManufacturingTransportCost = result.ProcessCost.ManufacturingTransportCost;
            summary.ProductionCost = result.ProductionCost;
            summary.SubpartsAndSubassembliesCost = result.SubpartsAndSubassembliesCost;
            summary.DevelopmentCost = result.DevelopmentCost;
            summary.ProjectInvest = result.ProjectInvest;
            summary.OtherCost = result.OtherCost;
            summary.PackagingCost = result.PackagingCost;
            summary.LogisticCost = result.LogisticCost;
            summary.TransportCost = result.TransportCost;
            summary.OverheadAndMarginCost = result.OverheadCost.TotalSumOverheadAndMargin;
            summary.PaymentCost = result.PaymentCost;
            summary.TargetCost = result.TotalCost;
        }

        /// <summary>
        /// Calculates the total weight and the cumulated investment cost of a part or assembly and saves them in <paramref name="result"/>.
        /// </summary>
        /// <param name="entity">The entity for which to calculate.</param>
        /// <param name="result">The calculation result for the entity.</param>
        protected void CalculateWeightAndCumulatedInvestment(object entity, CalculationResult result)
        {
            decimal totalWeight = 0m;
            decimal totalInvestment = 0m;

            if (result.Estimated)
            {
                var assy = entity as Assembly;
                if (assy != null)
                {
                    totalWeight = assy.Weight.GetValueOrDefault();
                }
                else
                {
                    var part = entity as Part;
                    if (part != null)
                    {
                        totalWeight = part.Weight.GetValueOrDefault();
                    }
                }
            }
            else
            {
                totalInvestment = result.InvestmentCost.TotalInvestment;

                foreach (RawMaterialCost rawMaterialCost in result.RawMaterialsCost.RawMaterialCosts)
                {
                    totalWeight += rawMaterialCost.PartWeight;
                }

                foreach (CommodityCost commodityCost in result.CommoditiesCost.CommodityCosts)
                {
                    totalWeight += commodityCost.Weight.GetValueOrDefault() * commodityCost.Amount;
                }

                foreach (AssemblyCost assemblyCost in result.AssembliesCost.AssemblyCosts)
                {
                    totalWeight += assemblyCost.FullCalculationResult.Weight * assemblyCost.AmountPerAssembly;
                    totalInvestment += assemblyCost.FullCalculationResult.InvestmentCostCumulated;
                }

                foreach (PartCost partCost in result.PartsCost.PartCosts)
                {
                    totalInvestment += partCost.FullCalculationResult.InvestmentCostCumulated;

                    // The raw part weight is already added because it appears as raw material in calculations, so we have to ignore it here. 
                    if (!partCost.IsRawPart)
                    {
                        totalWeight += partCost.FullCalculationResult.Weight * partCost.AmountPerAssembly;
                    }
                }
            }

            result.Weight = totalWeight;
            result.InvestmentCostCumulated = totalInvestment;
        }

        /// <summary>
        /// Calculates the additional costs (development, packaging, project invest, logistic, transport and other costs) for the specified entity (part or assembly).
        /// </summary>
        /// <param name="result">The calculation result into which to put the calculated additional costs.</param>
        /// <param name="entity">The entity for which to calculate the additional costs (part or assembly).</param>
        /// <param name="entityProductionCost">The entity's production cost, which should be calculated prior to this.</param>
        /// <param name="logisticCostRatio">
        /// The logistic cost ratio (percentage) to be used i the calculation. Usually comes from the parent Project.
        /// </param>
        protected virtual void CalculateAdditionalCosts(
            CalculationResult result,
            object entity,
            decimal entityProductionCost,
            decimal logisticCostRatio)
        {
            decimal developmentCost = 0m;
            decimal projectInvest = 0m;
            decimal otherCost = 0m;
            decimal packagingCost = 0m;
            decimal transportCost = 0m;
            decimal logisticCost = 0m;
            bool calculateLogisticCost = false;
            bool areCostsPerPartsTotal = true;
            decimal totalNumberOfParts = 0m;

            Assembly assembly = null;
            Part part = null;

            // Extract the additional costs values from the entity.
            if ((assembly = entity as Assembly) != null)
            {
                developmentCost = assembly.DevelopmentCost.GetValueOrDefault();
                projectInvest = assembly.ProjectInvest.GetValueOrDefault();
                otherCost = assembly.OtherCost.GetValueOrDefault();
                packagingCost = assembly.PackagingCost.GetValueOrDefault();
                transportCost = assembly.TransportCost;
                logisticCost = assembly.LogisticCost.GetValueOrDefault();
                calculateLogisticCost = assembly.CalculateLogisticCost.GetValueOrDefault();
                areCostsPerPartsTotal = assembly.AreAdditionalCostsPerPartsTotal;
                totalNumberOfParts = this.CalculateNetLifetimeProductionQuantity(assembly);
            }
            else if ((part = entity as Part) != null)
            {
                developmentCost = part.DevelopmentCost.GetValueOrDefault();
                projectInvest = part.ProjectInvest.GetValueOrDefault();
                otherCost = part.OtherCost.GetValueOrDefault();
                packagingCost = part.PackagingCost.GetValueOrDefault();
                transportCost = part.TransportCost;
                logisticCost = part.LogisticCost.GetValueOrDefault();
                calculateLogisticCost = part.CalculateLogisticCost.GetValueOrDefault();
                areCostsPerPartsTotal = part.AreAdditionalCostsPerPartsTotal;
                totalNumberOfParts = this.CalculateNetLifetimeProductionQuantity(part);
            }

            // Calculate the logistic cost if necessary; if not, the logistic cost is the logistic cost value taken from the entity.
            if (calculateLogisticCost)
            {
                logisticCost = entityProductionCost * logisticCostRatio;
            }

            if (areCostsPerPartsTotal)
            {
                // The additional costs should be spread over the total number of parts/assemblies produced in the part's/assembly's lifetime.
                if (totalNumberOfParts != 0m)
                {
                    result.DevelopmentCost = developmentCost / totalNumberOfParts;
                    result.ProjectInvest = projectInvest / totalNumberOfParts;
                    result.OtherCost = otherCost / totalNumberOfParts;
                    result.PackagingCost = packagingCost / totalNumberOfParts;
                    result.TransportCost = transportCost / totalNumberOfParts;

                    // If the logistic cost is auto-calculated, the calculated value is per part not per parts total 
                    // so it must not be divided by the total number of parts. 
                    result.LogisticCost = calculateLogisticCost ? logisticCost : logisticCost / totalNumberOfParts;
                }
            }
            else
            {
                // The additional costs are applied to each produced part/assembly.
                result.LogisticCost = logisticCost;
                result.DevelopmentCost = developmentCost;
                result.ProjectInvest = projectInvest;
                result.OtherCost = otherCost;
                result.PackagingCost = packagingCost;
                result.TransportCost = transportCost;
            }
        }

        #endregion Calculation helper methods

        #region Misc helpers

        /// <summary>
        /// Called when the value of the <see cref="MeasurementUnits" /> property has changed.
        /// </summary>
        /// <param name="newValue">The new value.</param>
        protected void OnMeasurementUnitsChanged(IEnumerable<MeasurementUnit> newValue)
        {
            MeasurementUnit weightUnit = null;
            if (newValue != null)
            {
                weightUnit = newValue.FirstOrDefault(u => u.Type == MeasurementUnitType.Weight);
            }

            if (weightUnit != null)
            {
                this.WeightUnit = weightUnit;
            }
            else
            {
                this.SetWeightUnitToDefault();
            }
        }

        /// <summary>
        /// Sets the weight unit to its default value.
        /// </summary>
        protected void SetWeightUnitToDefault()
        {
            this.WeightUnit = new MeasurementUnit()
            {
                ConversionRate = 1m,
                Name = "Kilogram",
                ScaleFactor = 1m,
                ScaleID = MeasurementUnitScale.MetricWeightScale,
                Symbol = "kg",
                Type = MeasurementUnitType.Weight
            };
        }

        /// <summary>
        /// Checks whether the provided calculation data is complete.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <exception cref="System.ArgumentNullException">The assembly was null.</exception>
        /// <exception cref="System.ArgumentException">The assembly had null overhead settings or country settings.</exception>
        protected virtual void CheckCalculationData(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            if (assembly.OverheadSettings == null)
            {
                throw new ArgumentException("The assembly had null overhead settings.", "assembly");
            }

            if (assembly.CountrySettings == null)
            {
                throw new ArgumentException("The assembly had null country settings.", "assembly");
            }
        }

        /// <summary>
        /// Checks whether the provided calculation data is complete.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <exception cref="System.ArgumentNullException">The part was null.</exception>
        /// <exception cref="System.ArgumentException">The part had null overhead settings or The part had null country settings</exception>
        protected virtual void CheckCalculationData(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            if (part.OverheadSettings == null)
            {
                throw new ArgumentException("The part had null overhead settings.", "part");
            }

            if (part.CountrySettings == null)
            {
                throw new ArgumentException("The part had null country settings.", "part");
            }
        }

        #endregion Misc helpers
    }
}
