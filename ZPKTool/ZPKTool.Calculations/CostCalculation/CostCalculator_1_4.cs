﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.Calculations.CostCalculation
{
    /// <summary>
    /// The cost calculator implementation v1.4
    /// </summary>
    /// <remarks>
    /// Changes:
    ///     - Added support for specifying lease/rental cost for machines.
    ///     - Updated the calculation algorithm for Net Lifetime Production Quantity.
    ///     - Added the possibility to apply Sales and Administration overhead on external parts and assemblies.
    ///     - Updated the <see cref="CalculateYearlyProductionQuantity"/> method to use the formula from <see cref="CalculateNetLifetimeProductionQuantity"/>.
    /// </remarks>
    internal class CostCalculator_1_4 : CostCalculator_1_3, ICostCalculator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_4" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_4(BasicSetting basicSettings)
            : base(basicSettings)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CostCalculator_1_4" /> class.
        /// </summary>
        /// <param name="basicSettings">The basic settings to be used for calculations.</param>
        /// <param name="measurementUnits">The measurement units used for calculations.</param>
        /// <exception cref="System.ArgumentNullException">The basic settings were null.</exception>
        public CostCalculator_1_4(BasicSetting basicSettings, IEnumerable<MeasurementUnit> measurementUnits)
            : base(basicSettings, measurementUnits)
        {
        }

        /// <summary>
        /// Calculates the net number of pieces produced in an assembly's lifetime.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The net number of assemblies.
        /// </returns>
        public override decimal CalculateNetLifetimeProductionQuantity(Assembly assembly)
        {
            decimal batchSizePerYear = assembly.BatchSizePerYear.GetValueOrDefault();
            decimal yearlyProductionQuantity = assembly.YearlyProductionQuantity.GetValueOrDefault();
            decimal lifeTime = assembly.LifeTime.GetValueOrDefault();

            decimal total = 0m;
            if (batchSizePerYear != 0m)
            {
                total = Math.Ceiling(yearlyProductionQuantity / batchSizePerYear) * batchSizePerYear * lifeTime;
            }

            return total;
        }

        /// <summary>
        /// Calculates the net number of pieces produced in a part's lifetime.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The net number of parts.
        /// </returns>
        public override decimal CalculateNetLifetimeProductionQuantity(Part part)
        {
            decimal batchSizePerYear = part.BatchSizePerYear.GetValueOrDefault();
            decimal yearlyProductionQuantity = part.YearlyProductionQuantity.GetValueOrDefault();
            decimal lifeTime = part.LifeTime.GetValueOrDefault();

            decimal total = 0m;
            if (batchSizePerYear != 0m)
            {
                total = Math.Ceiling(yearlyProductionQuantity / batchSizePerYear) * batchSizePerYear * lifeTime;
            }

            return total;
        }

        /// <summary>
        /// Calculates the machine investment for a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>
        /// The calculated machine investment.
        /// </returns>
        protected override decimal CalculateMachineInvestment(Machine machine)
        {
            bool isMachineLeased = machine.MachineLeaseCosts.HasValue;
            if (isMachineLeased)
            {
                // The investment is the yearly rent times the depreciation period.
                return machine.MachineLeaseCosts.GetValueOrDefault() * 12m * machine.DepreciationPeriod.GetValueOrDefault();
            }
            else
            {
                return machine.MachineInvestment.GetValueOrDefault();
            }
        }

        /// <summary>
        /// Calculates the cost of all sub-assemblies and parts used in the assembly's process.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parameters">The input parameters for the assembly cost calculation.</param>
        /// <returns>
        /// The cost of sub-parts and sub-assemblies.
        /// </returns>
        protected override SubPartsAndSubAssembliesCost CalculateSubPartsAndSubAssembliesCost(
            Assembly assembly,
            AssemblyCostCalculationParameters parameters)
        {
            if (assembly.Process == null)
            {
                return new SubPartsAndSubAssembliesCost();
            }

            SubPartsAndSubAssembliesCost totalCost = new SubPartsAndSubAssembliesCost();

            foreach (var step in assembly.Process.Steps)
            {
                // Compute the cost of sub-assemblies used in the current step
                foreach (var assemblyAmount in step.AssemblyAmounts)
                {
                    var subAssembly = assembly.Subassemblies.FirstOrDefault(a => a.Guid == assemblyAmount.FindAssemblyId());
                    if (subAssembly == null || subAssembly.IsDeleted)
                    {
                        // The assembly referred to by the amount does not exist. This is the case when the assembly is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    AssemblyCost crtAssyCost = totalCost.SubAssembliesCost.AssemblyCosts.FirstOrDefault(a => a.AssemblyId == subAssembly.Guid);
                    if (crtAssyCost != null)
                    {
                        // If the cost was already calculated for the assembly of the current amount then just update it
                        crtAssyCost.AmountPerAssembly += assemblyAmount.Amount;
                        totalCost.ExternalOverhead += assemblyAmount.Amount * crtAssyCost.ExternalOverhead;
                        totalCost.ExternalMargin += assemblyAmount.Amount * crtAssyCost.ExternalMargin;
                        totalCost.ExternalSGA += assemblyAmount.Amount * crtAssyCost.ExternalSGA;

                        totalCost.SubAssembliesCost.AssemblyAmountsSum += assemblyAmount.Amount;
                        totalCost.SubAssembliesCost.AssemblyCostsSum += assemblyAmount.Amount * crtAssyCost.TargetCost;
                    }
                    else
                    {
                        AssemblyCost assemblyCost = new AssemblyCost();
                        assemblyCost.AssemblyId = subAssembly.Guid;
                        assemblyCost.Name = subAssembly.Name;
                        assemblyCost.Description = subAssembly.Description;
                        assemblyCost.Number = subAssembly.Number;
                        assemblyCost.AssemblyIndex = subAssembly.Index;
                        assemblyCost.TargetPrice = subAssembly.TargetPrice;
                        assemblyCost.AmountPerAssembly = assemblyAmount.Amount;

                        // Calculate the sub-assembly's cost using the appropriate calculation version                        
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subAssembly.CalculationVariant, this.BasicSettings, this.MeasurementUnits);
                        CalculationResult result = calculator.CalculateAssemblyCost(subAssembly, parameters);

                        assemblyCost.FullCalculationResult = result;
                        assemblyCost.TargetCost = result.Summary.TargetCost;

                        // Apply a commodity overhead and margin for external assemblies.
                        if (subAssembly.IsExternal.GetValueOrDefault())
                        {
                            assemblyCost.ExternalOverhead = result.Summary.TargetCost * assembly.OverheadSettings.CommodityOverhead;
                            assemblyCost.ExternalMargin = result.Summary.TargetCost * assembly.OverheadSettings.CommodityMargin;

                            // Apply sales and administration overhead on the external assembly's cost.
                            if (subAssembly.ExternalSGA)
                            {
                                assemblyCost.ExternalSGA = (result.Summary.TargetCost + assemblyCost.ExternalOverhead + assemblyCost.ExternalMargin)
                                    * assembly.OverheadSettings.SalesAndAdministrationOHValue;
                                totalCost.ExternalSGA += assemblyAmount.Amount * assemblyCost.ExternalSGA;
                            }

                            totalCost.ExternalOverhead += assemblyAmount.Amount * assemblyCost.ExternalOverhead;
                            totalCost.ExternalMargin += assemblyAmount.Amount * assemblyCost.ExternalMargin;
                        }

                        totalCost.SubAssembliesCost.AddCost(assemblyCost);
                        totalCost.InvestmentCost.Add(result.InvestmentCost);
                    }
                }

                // Compute the cost of parts used in the current step
                foreach (var partAmount in step.PartAmounts)
                {
                    var part = assembly.Parts.FirstOrDefault(p => p.Guid == partAmount.FindPartId());
                    if (part == null || part.IsDeleted)
                    {
                        // The part referred to by the amount does not exist. This is the case when the part is deleted to trash bin and should not be taken into account during the calculation.
                        continue;
                    }

                    PartCost crtPartCost = totalCost.SubPartsCost.PartCosts.FirstOrDefault(p => p.PartId == part.Guid);
                    if (crtPartCost != null)
                    {
                        // If the cost was already calculated for the assembly of the current amount then just update it
                        crtPartCost.AmountPerAssembly += partAmount.Amount;
                        totalCost.ExternalOverhead += partAmount.Amount * crtPartCost.ExternalOverhead;
                        totalCost.ExternalMargin += partAmount.Amount * crtPartCost.ExternalMargin;
                        totalCost.ExternalSGA += partAmount.Amount * crtPartCost.ExternalSGA;

                        totalCost.SubPartsCost.PartAmountsSum += partAmount.Amount;
                        totalCost.SubPartsCost.PartCostsSum += partAmount.Amount * crtPartCost.TargetCost;
                    }
                    else
                    {
                        PartCost partCost = new PartCost();
                        partCost.PartId = part.Guid;
                        partCost.Name = part.Name;
                        partCost.Description = part.Description;
                        partCost.Number = part.Number;
                        partCost.PartIndex = part.Index;
                        partCost.TargetPrice = part.TargetPrice;
                        partCost.AmountPerAssembly = partAmount.Amount;

                        // Calculate the sub-part's cost using the appropriate calculation version
                        var partCalculationParams = new PartCostCalculationParameters()
                        {
                            ProjectDepreciationPeriod = parameters.ProjectDepreciationPeriod,
                            ProjectDepreciationRate = parameters.ProjectDepreciationRate,
                            ProjectLogisticCostRatio = parameters.ProjectLogisticCostRatio,
                            ProjectCreateDate = parameters.ProjectCreateDate
                        };

                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant, this.BasicSettings, this.MeasurementUnits);
                        CalculationResult result = calculator.CalculatePartCost(part, partCalculationParams);

                        partCost.FullCalculationResult = result;
                        partCost.TargetCost = result.Summary.TargetCost;

                        // Apply a commodity overhead and margin for external assemblies
                        if (part.IsExternal.GetValueOrDefault())
                        {
                            partCost.ExternalOverhead = result.Summary.TargetCost * assembly.OverheadSettings.CommodityOverhead;
                            partCost.ExternalMargin = result.Summary.TargetCost * assembly.OverheadSettings.CommodityMargin;

                            // Apply sales and administration overhead on the external part's cost.
                            if (part.ExternalSGA)
                            {
                                partCost.ExternalSGA = (result.Summary.TargetCost + partCost.ExternalOverhead + partCost.ExternalMargin)
                                    * assembly.OverheadSettings.SalesAndAdministrationOHValue;
                                totalCost.ExternalSGA += partAmount.Amount * partCost.ExternalSGA;
                            }

                            totalCost.ExternalOverhead += partAmount.Amount * partCost.ExternalOverhead;
                            totalCost.ExternalMargin += partAmount.Amount * partCost.ExternalMargin;
                        }

                        totalCost.SubPartsCost.AddCost(partCost);
                        totalCost.InvestmentCost.Add(result.InvestmentCost);
                    }
                }
            }

            return totalCost;
        }

        /// <summary>
        /// Calculates the capacity utilization of every machine in the specified process.
        /// The process's and every machine's cost must be calculated before calling this method.
        /// </summary>
        /// <param name="processSteps">The steps of the process.</param>
        /// <param name="processCost">The process's cost.</param>
        /// <param name="partBatchesPerYear">The batches per year of the parent part/assembly.</param>
        /// <param name="yearlyProductionQuantity">The yearly production quantity of the parent part/assembly.</param>
        protected override void CalculateMachinesCapacityUtilization(
            IEnumerable<ProcessStep> processSteps,
            ProcessCost processCost,
            decimal partBatchesPerYear,
            decimal yearlyProductionQuantity)
        {
            // Calculate the capacity utilization of every machine in the process.
            foreach (var step in processSteps)
            {
                ProcessStepCost stepCost = processCost.StepCosts.FirstOrDefault(sc => sc.StepId == step.Guid);
                if (stepCost != null)
                {
                    foreach (var machine in step.Machines.Where(m => !m.IsDeleted))
                    {
                        MachineCost machineCost = stepCost.MachineCosts.FirstOrDefault(mc => mc.MachineId == machine.Guid);
                        if (machineCost != null)
                        {
                            // The production capacity per year in hours            
                            decimal productionCapacityPerYear = step.ShiftsPerWeek.GetValueOrDefault()
                                * step.HoursPerShift.GetValueOrDefault()
                                * step.ProductionWeeksPerYear.GetValueOrDefault();

                            // The number of hours per year that the machine is down (does not produce anything).
                            decimal yearlyMachineDownTime = (step.SetupsPerBatch.GetValueOrDefault() * step.MaxDownTime.GetValueOrDefault() * partBatchesPerYear) / 3600m;

                            // The number of hours per year the machine is available for work
                            decimal machineAvailabilityPerYear =
                                ((productionCapacityPerYear - yearlyMachineDownTime) * machine.Amount) * machine.Availability.GetValueOrDefault();

                            decimal stepProcessTime = step.ProcessTime.GetValueOrDefault();
                            decimal stepPartsperCycle = step.PartsPerCycle.GetValueOrDefault();

                            if (stepProcessTime != 0m && stepPartsperCycle != 0m)
                            {
                                decimal theoreticalCapacityPerHour = 3600m / (stepProcessTime / stepPartsperCycle);
                                decimal theoreticalCapacityPerYear = Math.Floor(theoreticalCapacityPerHour * machineAvailabilityPerYear);

                                decimal neededPartsPerYear = 0m;
                                if (partBatchesPerYear != 0)
                                {
                                    neededPartsPerYear = Math.Ceiling(yearlyProductionQuantity / partBatchesPerYear) * partBatchesPerYear;
                                }

                                if (theoreticalCapacityPerYear != 0m)
                                {
                                    decimal capacityUtilization = neededPartsPerYear / theoreticalCapacityPerYear;

                                    // If the calculated capacity utilization is over 100% but less than 100.05% the machine overload check determines that
                                    // the machine is not overloaded so we adjust the capacity utilization down to 100% to be in sync with the overload check.
                                    if (capacityUtilization > 1m && capacityUtilization < 1.0005m)
                                    {
                                        capacityUtilization = 1m;
                                    }
                                    else if (capacityUtilization >= 1.0005m && capacityUtilization < 1.005m)
                                    {
                                        // If the utilization is 100.05% or more but less than 100.5% the overload check determines that the machine is overloaded
                                        // so we have to set the utilization to 100.5% so the rounding makes it 101%, which means overloading.
                                        capacityUtilization = 1.01m;
                                    }

                                    machineCost.CapacityUtilization = Math.Round(capacityUtilization * 100m) / 100m;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the yearly production quantity (IO Pieces per Year) for an Assembly, Part or Raw Part.
        /// <para />
        /// In order to calculate a correct result the entity's full parent hierarchy must be loaded, that is, the references chain from the entity
        /// up to its top-most parent assembly must be loaded prior to calling this method.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// The calculated yearly production quantity/
        /// </returns>
        /// <exception cref="System.ArgumentNullException">data;The data can not be null.</exception>
        protected override decimal CalculateYearlyProductionQuantityInternal(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity can not be null.");
            }

            Assembly currentAssy = null;

            // Contains the data for the full parent hierarchy of the entity. A data item contains the entity and some pieces of data common to all entities in the hierarchy.
            var parentHierarchyData = new List<YearlyProductionQuantityData>();

            // If the entity is a Part add it to the hierarchy on the last position. If the part is a raw part, add its parent part to the hierarchy before itself.
            var tempPart = entity as Part;
            if (tempPart != null)
            {
                parentHierarchyData.Add(new YearlyProductionQuantityData(tempPart));
                if (tempPart is RawPart)
                {
                    var parentPart = tempPart.ParentOfRawPart.FirstOrDefault();
                    if (parentPart != null)
                    {
                        parentHierarchyData.Insert(0, new YearlyProductionQuantityData(parentPart));
                        currentAssy = parentPart.Assembly;
                    }
                }
                else
                {
                    currentAssy = tempPart.Assembly;
                }
            }
            else
            {
                var assy = entity as Assembly;
                if (assy != null)
                {
                    currentAssy = assy;
                }
                else
                {
                    throw new InvalidOperationException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "The entity type '{0}' is not supported.",
                        entity.GetType().FullName));
                }
            }

            // Add all assemblies, up to the top-most one, to the hierarchy.            
            while (currentAssy != null)
            {
                parentHierarchyData.Insert(0, new YearlyProductionQuantityData(currentAssy));
                currentAssy = currentAssy.ParentAssembly;
            }

            // Calculate I.O Pieces per Year starting with the 1st assembly/part in the hierarchy. Each subsequent item's I.O Pieces per Year
            // will be calculated based on its parent's Gross batch Size and Batches per Year values.
            decimal parentGrossBatchSize = 0m;
            decimal parentBatchesPerYear = 0m;
            decimal currentIOPiecesPerYear = 0m;

            if (parentHierarchyData.Count > 0)
            {
                // The I.O Pieces per Year of the 1st item is its YearlyProductionQuantity property value.
                var firstItem = parentHierarchyData[0];
                parentBatchesPerYear = firstItem.BatchSizePerYear;

                // Calculate the net lifetime production quantity using the same formula as in the CalculateNetLifetimeProductionQuantity methods.
                if (firstItem.BatchSizePerYear != 0m)
                {
                    currentIOPiecesPerYear = Math.Ceiling(firstItem.YearlyProductionQuantity / firstItem.BatchSizePerYear) * firstItem.BatchSizePerYear;
                }
                else
                {
                    currentIOPiecesPerYear = firstItem.YearlyProductionQuantity;
                }

                parentGrossBatchSize = this.CalculateGrossBatchSize(firstItem.Entity, currentIOPiecesPerYear);

                for (int i = 1; i < parentHierarchyData.Count; i++)
                {
                    var item = parentHierarchyData[i];
                    currentIOPiecesPerYear = parentGrossBatchSize * parentBatchesPerYear;
                    parentBatchesPerYear = item.BatchSizePerYear;
                    parentGrossBatchSize = this.CalculateGrossBatchSize(item.Entity, currentIOPiecesPerYear);
                }
            }

            return currentIOPiecesPerYear;
        }
    }
}
