﻿namespace ZPKTool.Calculations.CostCalculation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The input data for the calculation of a Part's process cost.
    /// </summary>
    internal class PartProcessCostCalculationParameters
    {
        /// <summary>
        ///  Gets or sets the depreciation period of the part's parent project.
        /// </summary>        
        public int ProjectDepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate of the part's parent project.
        /// </summary>        
        public decimal ProjectDepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the cost of the part's raw materials.
        /// </summary>        
        public RawMaterialsCost RawMaterialsCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of the part's commodities.
        /// </summary>        
        public CommoditiesCost CommoditiesCost { get; set; }

        /// <summary>
        /// Gets or sets the cost of the part's Raw Part, if it has any. This value can be null.
        /// </summary>        
        public PartCost RawPartCost { get; set; }

        /// <summary>
        /// Gets or sets the parent project (if it exists) creation date.
        /// </summary>
        public DateTime? ProjectCreateDate { get; set; }
    }
}
