﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ZPKTool.MvvmCore")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("ZPKTool.MvvmCore")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5a23e141-562f-4101-a1bb-c877baf407bf")]

[assembly: CLSCompliant(true)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]

// This maps all CLR namespaces into the "http://zpktool.com/mvvmcore" xmlns with a prefix "mvvmcore:".
[assembly: XmlnsPrefix("http://zpktool.com/mvvmcore", "mvvmcore:")]
[assembly: XmlnsDefinition("http://zpktool.com/mvvmcore", "ZPKTool.MvvmCore")]
[assembly: XmlnsDefinition("http://zpktool.com/mvvmcore", "ZPKTool.MvvmCore.Commands")]
[assembly: XmlnsDefinition("http://zpktool.com/mvvmcore", "ZPKTool.MvvmCore.Services")]
[assembly: XmlnsDefinition("http://zpktool.com/mvvmcore", "ZPKTool.MvvmCore.Helpers")]
