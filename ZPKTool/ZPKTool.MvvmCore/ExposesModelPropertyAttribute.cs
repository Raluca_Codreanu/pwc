﻿using System;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Specifies that the view-model property represents (or exposes) a Model property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ExposesModelPropertyAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExposesModelPropertyAttribute"/> class.
        /// </summary>
        /// <param name="modelProperty">The name of the exposed model property.</param>
        public ExposesModelPropertyAttribute(string modelProperty)
        {
            if (string.IsNullOrWhiteSpace(modelProperty))
            {
                throw new ArgumentException("The model property name can't be null or whitespace", "modelProperty");
            }

            this.ModelProperty = modelProperty;
        }

        /// <summary>
        /// Gets the name of the exposed model property.
        /// </summary>
        public string ModelProperty { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the exposed model property affects or not costs.
        /// </summary>
        public bool AffectsCost { get; set; }
    }
}