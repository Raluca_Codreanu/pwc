﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ZPKTool.MvvmCore.Commands
{
    /// <summary>
    /// This class contains methods for the CommandManager that help avoid memory leaks by using weak references.
    /// </summary>
    internal static class CommandManagerHelper
    {
        /// <summary>
        /// Calls the weak reference handlers from the specified list.
        /// </summary>
        /// <param name="handlers">The handlers.</param>
        internal static void CallWeakReferenceHandlers(List<WeakReference> handlers)
        {
            if (handlers == null)
            {
                return;
            }

            // Take a snapshot of the handlers before we call out to them since the handlers could cause the array to me modified while we are reading it.
            EventHandler[] callees = new EventHandler[handlers.Count];
            int count = 0;

            for (int i = handlers.Count - 1; i >= 0; i--)
            {
                WeakReference reference = handlers[i];
                EventHandler handler = reference.Target as EventHandler;
                if (handler == null)
                {
                    // Clean up old handlers that have been collected
                    handlers.RemoveAt(i);
                }
                else
                {
                    callees[count] = handler;
                    count++;
                }
            }

            // Call the handlers that are alive
            for (int i = 0; i < count; i++)
            {
                EventHandler handler = callees[i];
                handler(null, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Registers the specified handlers to the RequerySuggested event of the <see cref="CommandManager"/> class.
        /// </summary>
        /// <param name="handlers">The handlers.</param>
        internal static void AddHandlersToRequerySuggested(List<WeakReference> handlers)
        {
            if (handlers != null)
            {
                foreach (WeakReference handlerRef in handlers)
                {
                    EventHandler handler = handlerRef.Target as EventHandler;
                    if (handler != null)
                    {
                        CommandManager.RequerySuggested += handler;
                    }
                }
            }
        }

        /// <summary>
        /// Unregisters the specified handlers to the RequerySuggested event of the <see cref="CommandManager"/> class.
        /// </summary>
        /// <param name="handlers">The handlers.</param>
        internal static void RemoveHandlersFromRequerySuggested(List<WeakReference> handlers)
        {
            if (handlers != null)
            {
                foreach (WeakReference handlerRef in handlers)
                {
                    EventHandler handler = handlerRef.Target as EventHandler;
                    if (handler != null)
                    {
                        CommandManager.RequerySuggested -= handler;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a weak reference to the specified handler in the specified list.
        /// </summary>
        /// <param name="handlers">The handlers.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="defaultListSize">Default size of the handlers list that will be used to create the list, if necessary.</param>
        internal static void AddWeakReferenceHandler(ref List<WeakReference> handlers, EventHandler handler, int defaultListSize)
        {
            if (handlers == null)
            {
                handlers = defaultListSize > 0 ? new List<WeakReference>(defaultListSize) : new List<WeakReference>();
            }

            handlers.Add(new WeakReference(handler));
        }

        /// <summary>
        /// Removes the weak reference to the specified handler from the specified list.
        /// </summary>
        /// <param name="handlers">The handlers.</param>
        /// <param name="handler">The handler.</param>
        internal static void RemoveWeakReferenceHandler(List<WeakReference> handlers, EventHandler handler)
        {
            if (handlers != null)
            {
                for (int i = handlers.Count - 1; i >= 0; i--)
                {
                    WeakReference reference = handlers[i];
                    EventHandler existingHandler = reference.Target as EventHandler;
                    if (existingHandler == null || existingHandler == handler)
                    {
                        // Clean up old handlers that have been collected in addition to the handler that is to be removed.
                        handlers.RemoveAt(i);
                    }
                }
            }
        }
    }
}
