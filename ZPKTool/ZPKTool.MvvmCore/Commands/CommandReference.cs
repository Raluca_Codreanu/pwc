﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace ZPKTool.MvvmCore.Commands
{
    /// <summary>
    /// This class facilitates associating a key binding in XAML markup to a command defined in a View Model by exposing a Command dependency property.
    /// The class derives from Freezable to work around a limitation in WPF when data-binding from XAML.    
    /// </summary>
    /// <remarks>
    /// There is a issue with this class with makes it impossible to bind an instance of it to an UI element: the CanExecute method of the wrapped command
    /// is invoked only once and its parameter is set to null even if the parameter of the wrapped command is not. CompositeCommand does not have this
    /// issue so a fix will probably be found in its implementation.
    /// </remarks>
    public class CommandReference : Freezable, ICommand
    {
        /// <summary>
        /// The event handler for the CanExecuteChanged event of the Command.
        /// It is necessary to keep a reference to it here because the event registration is done using the weak events pattern.
        /// </summary>
        private readonly EventHandler onCommandCanExecuteChangedHandler;

        /// <summary>
        /// Weak references to the handlers of the CanExecuteChanged event of this instance.
        /// </summary>
        private List<WeakReference> canExecuteChangedHandlers;
                
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandReference"/> class.
        /// </summary>
        public CommandReference()
        {
            this.onCommandCanExecuteChangedHandler = new EventHandler(this.OnCommandCanExecuteChanged);
        }

        /// <summary>
        /// Occurs when any of the registered commands raise <see cref="ICommand.CanExecuteChanged"/>. You must keep a hard
        /// reference to the handler to avoid garbage collection and unexpected results. See remarks for more information.
        /// </summary>
        /// <remarks>
        /// When subscribing to the <see cref="ICommand.CanExecuteChanged"/> event using code (not when binding using XAML)
        /// will need to keep a hard reference to the event handler. This is to prevent garbage collection of the event handler
        /// because the command implements the Weak Event pattern so it does not have a hard reference to this handler.
        /// In most scenarios, there is no reason to sign up to the CanExecuteChanged event directly, but if you do, you
        /// are responsible for maintaining the reference.
        /// </remarks>
        /// <example>
        /// The following code holds a reference to the event handler. The myEventHandlerReference value should be stored
        /// in an instance member to avoid it from being garbage collected.
        /// <code>
        /// EventHandler myEventHandlerReference = new EventHandler(this.OnCanExecuteChanged);
        /// command.CanExecuteChanged += myEventHandlerReference;
        /// </code>
        /// </example>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                CommandManagerHelper.AddWeakReferenceHandler(ref canExecuteChangedHandlers, value, 2);
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
                CommandManagerHelper.RemoveWeakReferenceHandler(canExecuteChangedHandlers, value);
            }
        }

        /// <summary>
        /// Gets or sets the command wrapped by this instance.
        /// </summary>        
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Dependency property for Command.
        /// </summary>
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandReference), new PropertyMetadata(new PropertyChangedCallback(OnCommandChanged)));

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        /// <returns>
        /// true if this command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(object parameter)
        {
            ICommand cmd = this.Command;
            if (cmd != null)
            {
                return cmd.CanExecute(parameter);
            }

            return false;
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public void Execute(object parameter)
        {
            ICommand cmd = this.Command;
            if (cmd != null)
            {
                cmd.Execute(parameter);
            }
        }

        /// <summary>
        /// Called when the wrapped command has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CommandReference commandReference = d as CommandReference;
            if (commandReference == null)
            {
                return;
            }

            ICommand oldCommand = e.OldValue as ICommand;
            ICommand newCommand = e.NewValue as ICommand;

            if (oldCommand != null)
            {
                oldCommand.CanExecuteChanged -= commandReference.onCommandCanExecuteChangedHandler;
            }

            if (newCommand != null)
            {
                newCommand.CanExecuteChanged += commandReference.onCommandCanExecuteChangedHandler;
            }
        }

        /// <summary>
        /// Called when the CanExecuteChanged event of the wrapped command is triggered.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnCommandCanExecuteChanged(object sender, EventArgs e)
        {
            CommandManagerHelper.CallWeakReferenceHandlers(this.canExecuteChangedHandlers);
        }

        #region Freezable

        /// <summary>
        /// When implemented in a derived class, creates a new instance of the <see cref="T:System.Windows.Freezable"/> derived class.
        /// </summary>
        /// <returns>The new instance.</returns>
        protected override Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
