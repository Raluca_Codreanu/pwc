﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ZPKTool.MvvmCore.Commands
{
    /// <summary>
    /// This class allows delegating the commanding logic to methods passed as parameters,
    /// and enables a View to bind commands to objects that are not part of the element tree (that are in View-Models, for example).
    /// </summary>
    /// <typeparam name="T">Type of the parameter passed to the delegate.</typeparam>
    public class DelegateCommand<T> : ICommand
    {
        #region Fields

        /// <summary>
        /// The method to be called when the command is invoked.
        /// </summary>
        private readonly Action<T> executeMethod;

        /// <summary>
        /// The method that determines whether the command can execute in its current state.
        /// </summary>
        private readonly Func<T, bool> canExecuteMethod;

        /// <summary>
        /// A value indicating whether the command should not register to the <see cref="System.Windows.Input.CommandManager.RequerySuggested"/> event
        /// in order to be notified when it should re-evaluate its state.
        /// </summary>
        private bool isAutomaticRequeryDisabled;

        /// <summary>
        /// The listeners registered to the command's CanExecuteChanged event.
        /// </summary>
        private List<WeakReference> canExecuteChangedHandlers;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}"/> class.
        /// </summary>
        /// <param name="executeMethod">The method to be called by the command.</param>        
        public DelegateCommand(Action<T> executeMethod)
            : this(executeMethod, null, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}" /> class.
        /// </summary>
        /// <param name="executeMethod">The method to be called by the command.</param>
        /// <param name="canExecuteMethod">The method that determines whether the command can execute in its current state.</param>
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
            : this(executeMethod, canExecuteMethod, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand{T}" /> class.
        /// </summary>
        /// <param name="executeMethod">The method to be called by the command.</param>
        /// <param name="canExecuteMethod">The method that determines whether the command can execute in its current state.</param>
        /// <param name="isAutomaticRequeryDisabled">Indicates whether the command should not register to the
        /// <see cref="System.Windows.Input.CommandManager.RequerySuggested" /> event in order to be notified when it should re-evaluate its state.</param>
        /// <exception cref="System.ArgumentNullException">The method to be called by the command was null.</exception>
        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod, bool isAutomaticRequeryDisabled)
        {
            if (executeMethod == null)
            {
                throw new ArgumentNullException("executeMethod", "The method to be called by the command was null.");
            }

            this.executeMethod = executeMethod;
            this.canExecuteMethod = canExecuteMethod;
            this.isAutomaticRequeryDisabled = isAutomaticRequeryDisabled;
        }

        #endregion

        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (!this.isAutomaticRequeryDisabled)
                {
                    CommandManager.RequerySuggested += value;
                }

                CommandManagerHelper.AddWeakReferenceHandler(ref this.canExecuteChangedHandlers, value, 2);
            }

            remove
            {
                if (!this.isAutomaticRequeryDisabled)
                {
                    CommandManager.RequerySuggested -= value;
                }

                CommandManagerHelper.RemoveWeakReferenceHandler(this.canExecuteChangedHandlers, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the command should not register to the
        /// <see cref="System.Windows.Input.CommandManager.RequerySuggested" /> event in order to be notified when it should re-evaluate its state.
        /// </summary>
        public bool IsAutomaticRequeryDisabled
        {
            get
            {
                return this.isAutomaticRequeryDisabled;
            }

            set
            {
                if (this.isAutomaticRequeryDisabled != value)
                {
                    if (value)
                    {
                        CommandManagerHelper.RemoveHandlersFromRequerySuggested(this.canExecuteChangedHandlers);
                    }
                    else
                    {
                        CommandManagerHelper.AddHandlersToRequerySuggested(this.canExecuteChangedHandlers);
                    }

                    this.isAutomaticRequeryDisabled = value;
                }
            }
        }

        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        /// <returns>
        /// true if this command can be executed; otherwise, false.
        /// </returns>
        bool ICommand.CanExecute(object parameter)
        {
            // If T is of value type and the parameter is not set yet, then return false if CanExecute delegate exists, else return true
            if (parameter == null && typeof(T).IsValueType)
            {
                return this.canExecuteMethod == null;
            }

            return this.CanExecute((T)parameter);
        }

        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        void ICommand.Execute(object parameter)
        {
            this.Execute((T)parameter);
        }

        /// <summary>
        /// Determines whether the command can be executed in its current state.
        /// </summary>
        /// <param name="parameter">The parameter passed to the method that determines whether the command can execute.</param>
        /// <returns>
        /// true if this command can execute; otherwise, false.
        /// </returns>
        public bool CanExecute(T parameter)
        {
            if (this.canExecuteMethod != null)
            {
                return this.canExecuteMethod(parameter);
            }

            return true;
        }

        /// <summary>
        /// Calls the method associated with the command.
        /// </summary>
        /// <param name="parameter">The parameter to be passed to the method called by the command.</param>
        public void Execute(T parameter)
        {
            if (this.executeMethod != null)
            {
                this.executeMethod(parameter);
            }
        }

        /// <summary>
        /// Raises the CanExecuteChanged event.
        /// </summary>
        protected virtual void OnCanExecuteChanged()
        {
            CommandManagerHelper.CallWeakReferenceHandlers(this.canExecuteChangedHandlers);
        }
    }
}
