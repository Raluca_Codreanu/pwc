﻿// ****************************************************************************
// <copyright file="EventToCommand.cs" company="GalaSoft Laurent Bugnion">
// Copyright © GalaSoft Laurent Bugnion 2009-2010
// </copyright>
// ****************************************************************************
// <author>Laurent Bugnion</author>
// <email>laurent@galasoft.ch</email>
// <date>3.11.2009</date>
// <project>GalaSoft.MvvmLight.Extras</project>
// <web>http://www.galasoft.ch</web>
// <license>
// See license.txt in this solution or http://www.galasoft.ch/license_MIT.txt
// </license>
// <LastBaseLevel>BL0002</LastBaseLevel>
// ****************************************************************************

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace ZPKTool.MvvmCore.Commands
{
    /// <summary>
    /// Defines a method that processes the event arguments of a routed command and returns a custom object.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The routed command's event arguments</param>
    /// <returns>
    /// A custom object instance.
    /// </returns>
    public delegate object ConvertEventArgsHandler(object sender, RoutedEventArgs e);

    /// <summary>
    /// This <see cref="System.Windows.Interactivity.TriggerAction" /> can be used to bind any event on any FrameworkElement to an <see cref="ICommand" />.
    /// Typically, this element is used in XAML to connect the attached element to a command located in a ViewModel. This trigger can only be attached
    /// to a FrameworkElement or a class deriving from FrameworkElement.
    /// <para>To access the EventArgs of the fired event, use a DelegateCommand{EventArgs} and leave the CommandParameter empty!</para>
    /// <para>To convert the the EventArgs of the fired event to a custom value, subscribe to the ConvertEventArgs event;
    /// then, the parameter of the <see cref="ICommand" /> will be the the object returned by the event's invocation.</para>
    /// </summary>
    public class EventToCommand : TriggerAction<FrameworkElement>
    {
        /// <summary>
        /// Identifies the <see cref="CommandParameter" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(EventToCommand), new PropertyMetadata(null, OnCommandParameterChanged));

        /// <summary>
        /// Identifies the <see cref="Command" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(EventToCommand), new PropertyMetadata(null, OnCommandChanged));

        /// <summary>
        /// Identifies the <see cref="MustToggleIsEnabled" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty MustToggleIsEnabledProperty =
            DependencyProperty.Register("MustToggleIsEnabled", typeof(bool), typeof(EventToCommand), new PropertyMetadata(false, OnMustToggleIsEnabledChanged));

        /// <summary>
        /// Occurs when is time to convert the triggered event's arguments, during its routing to the command specified by the Command property.
        /// The event is triggered only if the CommandParameter property is not set and the PassEventArgsToCommand property is set to false.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "This event handler for is needed in order not to require the views to add a reference to this assembly due to the handler's event arguments.")]
        public event ConvertEventArgsHandler ConvertEventArgs;

        /// <summary>
        /// Gets or sets the ICommand that this trigger is bound to. This is a DependencyProperty.
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets an object that will be passed to the <see cref="Command" /> attached to this trigger. This is a DependencyProperty.
        /// </summary>
        public object CommandParameter
        {
            get { return this.GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attached element must be disabled when the <see cref="Command" /> property's CanExecuteChanged
        /// event fires. If this property is true, and the command's CanExecute method returns false, the element will be disabled. If this property
        /// is false, the element will not be disabled when the command's CanExecute method changes. This is a DependencyProperty.
        /// </summary>
        public bool MustToggleIsEnabled
        {
            get { return (bool)this.GetValue(MustToggleIsEnabledProperty); }
            set { SetValue(MustToggleIsEnabledProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the EventArgs of the event that triggered this action should be passed to the bound Command. If this is true,
        /// the command should accept arguments of the corresponding type (for example Command{MouseButtonEventArgs}).
        /// </summary>
        public bool PassEventArgsToCommand { get; set; }

        /// <summary>
        /// Called when this trigger is attached to a FrameworkElement.
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            this.EnableDisableElement();
        }

        /// <summary>
        /// Provides a simple way to invoke this trigger programmatically without any EventArgs.
        /// </summary>
        public void Invoke()
        {
            this.Invoke(null);
        }

        /// <summary>
        /// Executes the trigger.
        /// <para>To access the EventArgs of the fired event, use a RelayCommand&lt;EventArgs&gt;
        /// and leave the CommandParameter and CommandParameterValue empty!</para>
        /// </summary>
        /// <param name="parameter">The EventArgs of the fired event.</param>
        protected override void Invoke(object parameter)
        {
            // Commented out because it stops routing the event to the command if the associated element is disabled.
            // The "normal" behavior is to route the event regardless of whether the associated element is enabled, but if it is necessary to stop
            // routing the events of a disabled element, that should be enabled using a new flag (like, DontInvokeIfDisabled).
            ////if (AssociatedElementIsDisabled())
            ////{
            ////    return;
            ////}

            var command = this.Command;
            var commandParameter = this.CommandParameter;

            if (commandParameter == null)
            {
                var convertCallback = this.ConvertEventArgs;
                if (convertCallback != null)
                {
                    commandParameter = convertCallback(this, parameter as RoutedEventArgs);
                }
                else if (this.PassEventArgsToCommand)
                {
                    commandParameter = parameter;
                }
            }

            if (command != null && command.CanExecute(commandParameter))
            {
                command.Execute(commandParameter);
            }
        }

        /// <summary>
        /// Called when the Command property has changed.
        /// </summary>
        /// <param name="s">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnCommandChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var element = s as EventToCommand;
            if (element == null)
            {
                return;
            }

            if (e.OldValue != null)
            {
                ((ICommand)e.OldValue).CanExecuteChanged -= element.OnCommandCanExecuteChanged;
            }

            var command = (ICommand)e.NewValue;
            if (command != null)
            {
                command.CanExecuteChanged += element.OnCommandCanExecuteChanged;
            }

            element.EnableDisableElement();
        }

        /// <summary>
        /// Called when the CommandParameter property has changed.
        /// </summary>
        /// <param name="s">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnCommandParameterChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var sender = s as EventToCommand;
            if (sender == null)
            {
                return;
            }

            if (sender.AssociatedObject == null)
            {
                return;
            }

            sender.EnableDisableElement();
        }

        /// <summary>
        /// Called when the MustToggleIsEnabled property has changed.
        /// </summary>
        /// <param name="s">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnMustToggleIsEnabledChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var sender = s as EventToCommand;
            if (sender == null)
            {
                return;
            }

            if (sender.AssociatedObject == null)
            {
                return;
            }

            sender.EnableDisableElement();
        }

        /// <summary>
        /// Determines whether the UI element associated with this instance is disabled.
        /// </summary>
        /// <returns>
        /// true if the associated element is disabled or null; otherwise, false.
        /// </returns>
        private bool IsAssociatedElementDisabled()
        {
            var element = this.AssociatedObject;
            return element == null || (element != null && !element.IsEnabled);
        }

        /// <summary>
        /// Enables or disables the associated element based on whether the Command can be executed and whether this behavior is enabled 
        /// by the MustToggleIsEnabled property.
        /// </summary>
        private void EnableDisableElement()
        {
            var element = this.AssociatedObject;
            if (element == null)
            {
                return;
            }

            var command = this.Command;
            if (this.MustToggleIsEnabled && command != null)
            {
                element.IsEnabled = command.CanExecute(this.CommandParameter);
            }
        }

        /// <summary>
        /// Handles the CanExecuteChanged event of the current value of the Command property.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnCommandCanExecuteChanged(object sender, EventArgs e)
        {
            this.EnableDisableElement();
        }
    }
}