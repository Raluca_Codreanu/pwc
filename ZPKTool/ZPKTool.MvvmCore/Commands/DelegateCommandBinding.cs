﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ZPKTool.MvvmCore.Commands
{
    /// <summary>
    /// A command binding that redirects a RoutedCommand to an ICommand contained in a CommandReference.
    /// </summary>
    public class DelegateCommandBinding : CommandBinding
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommandBinding"/> class.
        /// </summary>
        public DelegateCommandBinding()
        {
            this.CanExecute += (s, e) =>
            {
                if (this.CommandReference != null)
                {
                    e.CanExecute = this.CommandReference.CanExecute(e.Parameter);
                }
            };

            this.Executed += (s, e) =>
            {
                if (this.CommandReference != null)
                {
                    this.CommandReference.Execute(e.Parameter);
                }
            };
        }

        /// <summary>
        /// Gets or sets the CommandReference containing the ICommand to which to redirect the RoutedCommand.
        /// </summary>        
        public CommandReference CommandReference { get; set; }
    }
}
