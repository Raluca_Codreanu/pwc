﻿namespace ZPKTool.MvvmCore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This class is a value wrapper that allows to define non-data properties on a view model with less code.    
    /// </summary>
    /// <remarks>
    /// The properties of this type are automatically initialized by the base ViewModel class, therefore in ViewModel classes you should define automated
    /// properties of this type and in the view bind to the Value property those automated properties.
    /// </remarks>
    /// <typeparam name="T">The type of the property's value.</typeparam>
    public class VMProperty<T> : INotifyPropertyChanged, INotifyPropertyChanging, IVMProperty
    {
        #region Fields

        /// <summary>
        /// The value of this instance.
        /// </summary>
        private T internalValue;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        private EventHandler<ValueChangedEventArgs<object>> valueChangedNonGeneric;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="VMProperty&lt;T&gt;"/> class.
        /// </summary>        
        public VMProperty()
        {
        }

        #region Events

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Occurs when the Value property has changed.
        /// The 1st argument of the handler is the source of the event and the 2nd argument is the old value of the property.
        /// </summary>
        public event EventHandler<ValueChangedEventArgs<T>> ValueChanged;

        /// <summary>
        /// Occurs when the Value property has changed.
        /// The 1st argument of the handler is the source of the event and the 2nd argument is the old value of the property.
        /// </summary>
        event EventHandler<ValueChangedEventArgs<object>> IVMProperty.ValueChanged
        {
            add
            {
                valueChangedNonGeneric += value;
                this.ValueChanged += VMProperty_ValueChanged;
            }

            remove
            {
                valueChangedNonGeneric -= value;
                this.ValueChanged -= VMProperty_ValueChanged;
            }
        }

        /// <summary>
        /// Event raised when the property values changes.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void VMProperty_ValueChanged(object sender, ValueChangedEventArgs<T> e)
        {
            var evt = this.valueChangedNonGeneric;
            if (evt != null)
            {
                evt.Invoke(this, new ValueChangedEventArgs<object>(e.OldValue, e.NewValue));
            }
        }

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the value of this instance. This property should be used by the views for binding.
        /// </summary>
        public T Value
        {
            get
            {
                return this.internalValue;
            }

            set
            {
                bool sameValue = (this.internalValue == null && value == null) || (this.internalValue != null && value != null && this.internalValue.Equals(value));
                if (!sameValue)
                {
                    T oldValue = this.internalValue;
                    this.OnPropertyChanging("Value");
                    this.internalValue = value;
                    this.OnPropertyChanged("Value");

                    // Raise the ValueChanged event.
                    var valueChangedHandler = this.ValueChanged;
                    if (valueChangedHandler != null)
                    {
                        valueChangedHandler(this, new ValueChangedEventArgs<T>(oldValue, this.internalValue));
                    }
                }
            }
        }

        #endregion Properties

        /// <summary>
        /// This raises the INotifyPropertyChanged.PropertyChanged event to indicate a specific property has changed value.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// This raises the INotifyPropertyChanging.PropertyChanging event to indicate a specific property value is changing.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanging(string propertyName)
        {
            var handler = this.PropertyChanging;
            if (handler != null)
            {
                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }
    }
}
