﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Abstract base class for a ViewModel implementation.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.OrderingRules", "SA1204:StaticElementsMustAppearBeforeInstanceElements", Justification = "The methods are grouped in region based on their purpose.")]
    public abstract class ViewModel : INotifyPropertyChanged, INotifyPropertyChanging, IDataErrorInfo, ILifeCycleManager, IUndoable
    {
        #region Fields

        /// <summary>
        /// The cache containing information about the child view models of this instance.
        /// When accessing this member, always acquire a lock on the <see cref="initChildViewModelsInfoSync"/> field.
        /// Use the <see cref="GetChildViewModelsInfo"/> method to obtain data from this field.
        /// </summary>
        private static readonly Dictionary<Type, ChildViewModelsInfo> childViewModelsInfoCache = new Dictionary<Type, ChildViewModelsInfo>();

        /// <summary>
        /// The instance used to sync access to the <see cref="childViewModelsInfoCache"/> member.
        /// </summary>
        private static readonly object initChildViewModelsInfoSync = new object();

        /// <summary>
        /// A value indicating whether the associated view should be read only.
        /// </summary>
        private bool isReadOnly;

        /// <summary>
        /// A value indicating whether the current data loaded in the form has changed.
        /// </summary>
        private bool isChanged;

        /// <summary>
        /// The command that executes the OnLoaded method of this instance.
        /// </summary>
        private ICommand loadedCommand;

        /// <summary>
        /// The command that saves all changes.
        /// </summary>
        private ICommand saveCommand;

        /// <summary>
        /// The command that cancels all changes made to the form.
        /// </summary>
        private ICommand cancelCommand;

        /// <summary>
        /// The command that undo the last change made to the form.
        /// </summary>
        private ICommand undoCommand;

        /// <summary>
        /// The command that cancels and closes this view-model.
        /// </summary>
        private ICommand closeCommand;

        /// <summary>
        /// A value indicating whether this instance is a child of a parent view-model.
        /// </summary>
        private bool isChild;

        /// <summary>
        /// Value indicating whether this instance is in viewer mode.
        /// </summary>
        private bool isInViewerMode;

        /// <summary>
        /// A value indicating whether this instance is loaded.
        /// </summary>
        private bool isLoaded;

        /// <summary>
        /// A value indicating whether the ViewModel is created or edited.
        /// </summary>
        private ViewModelEditMode editMode = ViewModelEditMode.Edit;

        /// <summary>
        /// The undo manager, responsible for the undo command functionality.
        /// </summary>
        private UndoManager undoManager;

        /// <summary>
        /// A value indicating whether the Save command was successfully executed.
        /// </summary>
        private bool saved;

        /// <summary>
        /// Indicates whether the <see cref="IsReadOnly"/> property value was inherited from a parent view-model.
        /// The default value of this field is true; it is set to false when the <see cref="IsReadOnly"/> property is set from outside this class.
        /// </summary>
        private bool isReadOnlyInherited = true;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>        
        protected ViewModel()
            : this(true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>   
        /// <param name="initUndo">A value indicating whether to initialize the UndoManager in the constructor or not.</param>
        protected ViewModel(bool initUndo)
        {
            InitializeChildViewModelsDataCache(this.GetType());

            this.Dispatcher = new DispatcherService();
            this.LoadedCommand = new DelegateCommand(this.OnLoaded);
            this.SaveCommand = new DelegateCommand(this.SaveAction, this.CanSave);
            this.UndoCommand = new DelegateCommand(this.Undo, this.CanUndo);
            this.CancelCommand = new DelegateCommand(this.CancelAction, this.CanCancel);
            this.CloseCommand = new DelegateCommand(this.Close, this.CanClose);

            this.InitializeVMProperties();

            if (initUndo)
            {
                this.UndoManager = new UndoManager();
            }
        }

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating the undo manager, responsible for the undo command functionality.
        /// </summary>
        public UndoManager UndoManager
        {
            get
            {
                return this.undoManager;
            }

            set
            {
                if (this.undoManager != value)
                {
                    OnPropertyChanging(() => this.UndoManager);
                    if (this.undoManager != null)
                    {
                        this.undoManager.StopTrackingObject(this);
                    }

                    this.undoManager = value;
                    this.undoManager.TrackObject(this);
                    OnPropertyChanged(() => this.UndoManager);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loaded.
        /// </summary>
        public bool IsLoaded
        {
            get { return this.isLoaded; }
            set { this.SetProperty(ref this.isLoaded, value, () => this.IsLoaded); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the associated view should be read only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return this.isReadOnly; }
            set { this.SetProperty(ref this.isReadOnly, value, () => this.IsReadOnly, this.OnIsReadOnlyChanged); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the object's state has changed.
        /// </summary>
        /// <returns>true if the object’s content has changed since the last call to <see cref="M:System.ComponentModel.IChangeTracking.AcceptChanges"/>; otherwise, false.</returns>
        public bool IsChanged
        {
            get { return this.isChanged || this.CheckChildViewModelsForChanges(); }
            set { this.SetProperty(ref this.isChanged, value, () => this.IsChanged); }
        }

        /// <summary>
        /// Gets the dispatcher, which can be used to execute code on the UI thread.
        /// </summary>
        public IDispatcherService Dispatcher { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is a child in a parent view-model.
        /// </summary>
        public bool IsChild
        {
            get { return this.isChild; }
            set { this.SetProperty(ref this.isChild, value, () => this.IsChild); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in viewer mode (was created by the Model Viewer).
        /// </summary>        
        public bool IsInViewerMode
        {
            get { return this.isInViewerMode; }
            set { this.SetProperty(ref this.isInViewerMode, value, () => this.IsInViewerMode, this.OnIsInViewerModeChanged); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ViewModel is created or edited.
        /// </summary>
        public ViewModelEditMode EditMode
        {
            get { return this.editMode; }
            set { this.SetProperty(ref this.editMode, value, () => this.EditMode); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="SaveCommand"/> was successfully executed.
        /// </summary>
        public bool Saved
        {
            get { return this.saved; }
            set { this.SetProperty(ref this.saved, value, () => this.Saved); }
        }

        /// <summary>
        /// Gets or sets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        public virtual string Error
        {
            get
            {
                return Validate();
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets the validation error message for the property with the given name.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public virtual string this[string propertyName]
        {
            get
            {
                return ValidateProperty(propertyName);
            }
        }

        /// <summary>
        /// Gets the information about the child view models of this instance.
        /// </summary>
        protected ChildViewModelsInfo ChildViewModels
        {
            get
            {
                lock (initChildViewModelsInfoSync)
                {
                    return childViewModelsInfoCache[this.GetType()];
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Gets or sets the command that executes the OnLoaded method of this instance.
        /// <para />
        /// The view should execute this command after it has been loaded.
        /// </summary>        
        public ICommand LoadedCommand
        {
            get { return this.loadedCommand; }
            set { this.SetProperty(ref this.loadedCommand, value, () => this.LoadedCommand); }
        }

        /// <summary>
        /// Gets or sets the command that saves all changes.
        /// </summary>
        public ICommand SaveCommand
        {
            get { return this.saveCommand; }
            protected set { this.SetProperty(ref this.saveCommand, value, () => this.SaveCommand); }
        }

        /// <summary>
        /// Gets or sets the command that cancels all changes made to the form.
        /// </summary>
        public ICommand CancelCommand
        {
            get { return this.cancelCommand; }
            protected set { this.SetProperty(ref this.cancelCommand, value, () => this.CancelCommand); }
        }

        /// <summary>
        /// Gets or sets the command that undo the last change made to the form.
        /// </summary>
        public ICommand UndoCommand
        {
            get { return this.undoCommand; }
            protected set { this.SetProperty(ref this.undoCommand, value, () => this.UndoCommand); }
        }

        /// <summary>
        /// Gets or sets the command that closes the view-model.        
        /// </summary>        
        public ICommand CloseCommand
        {
            get { return this.closeCommand; }
            set { this.SetProperty(ref this.closeCommand, value, () => this.CloseCommand); }
        }

        #endregion Commands

        #region Virtual methods

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.        
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected virtual bool CanSave()
        {
            return true;
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>        
        protected virtual void Save()
        {
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected virtual bool CanCancel()
        {
            return true;
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected virtual void Cancel()
        {
        }

        /// <summary>
        /// Determines whether this instance can be closed.
        /// <para/>
        /// Called by the CloseCommand command.
        /// </summary>
        /// <returns>
        /// true if this instance can be closed; otherwise, false.
        /// </returns>
        protected virtual bool CanClose()
        {
            return true;
        }

        /// <summary>
        /// Closes this instance.
        /// <para/>
        /// Called by the CloseCommand command.
        /// </summary>
        protected virtual void Close()
        {
        }

        /// <summary>
        /// Determines whether this instance can perform the undo operation. Executed by the UndoCommand.        
        /// </summary>
        /// <returns>
        /// true if the undo operation can be performed, false otherwise.
        /// </returns>
        protected virtual bool CanUndo()
        {
            return this.UndoManager.CanUndo();
        }

        /// <summary>
        /// Performs the undo operation. Executed by the UndoCommand.
        /// </summary>   
        protected virtual void Undo()
        {
            this.UndoManager.Undo();
        }

        #endregion Virtual methods

        #region ILifeCycleManager

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public virtual void OnLoaded()
        {
            this.Saved = false;
            this.IsLoaded = true;
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public virtual bool OnUnloading()
        {
            return true;
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public virtual void OnUnloaded()
        {
            IsLoaded = false;
        }

        #endregion ILifeCycleManager

        #region Helpers

        /// <summary>
        /// Determines if any of this view-model's child view models have changed.
        /// Used by the <see cref="IsChanged "/> property.
        /// </summary>
        /// <returns>True if any child view model has changed; otherwise, false.</returns>
        private bool CheckChildViewModelsForChanges()
        {
            foreach (var childVMProperty in this.ChildViewModels.ChildViewModelProperties)
            {
                var childViewModel = (ViewModel)childVMProperty.GetValue(this, null);
                if (childViewModel != null && childViewModel.IsChanged)
                {
                    return true;
                }
            }

            foreach (var childVMCollectionProperty in this.ChildViewModels.ChildViewModelCollections)
            {
                var childViewModelCollection = (IEnumerable)childVMCollectionProperty.GetValue(this, null);
                if (childViewModelCollection != null)
                {
                    var enumerator = childViewModelCollection.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        var childViewModel = (ViewModel)enumerator.Current;
                        if (childViewModel != null && childViewModel.IsChanged)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// The method executed by the <see cref="CancelCommand"/>
        /// </summary>
        private void CancelAction()
        {
            this.Cancel();
            this.Saved = false;
        }

        /// <summary>
        /// The method executed by the <see cref="SaveCommand"/>
        /// </summary>
        private void SaveAction()
        {
            this.Save();
            this.Saved = true;
        }

        #endregion

        #region Properties related code

        /// <summary>
        /// Called when the value of a property has changed in order to raise the INotifyPropertyChanged.PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>        
        protected void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Called when the value of multiple properties has changed in order to raise the INotifyPropertyChanged.PropertyChanged event.
        /// The event is raised for each property.
        /// </summary>
        /// <param name="propertyNames">The properties that have a new value.</param>        
        protected void OnPropertyChanged(params string[] propertyNames)
        {
            if (propertyNames == null)
            {
                throw new ArgumentNullException("propertyNames");
            }

            foreach (var name in propertyNames)
            {
                this.OnPropertyChanged(name);
            }
        }

        /// <summary>
        /// Called when the value of a property has changed in order to raise the INotifyPropertyChanged.PropertyChanged event.
        /// </summary>
        /// <param name="property">The property that changed.</param>
        /// <remarks>
        /// This method is around 5 times slower that the version that takes the property name as a string (100.000 calls take ~2000ms vs. ~400ms for the other version).
        /// </remarks>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        protected void OnPropertyChanged(Expression<Func<object>> property)
        {
            this.OnPropertyChanged(ReflectionUtils.GetPropertyName(property));
        }

        /// <summary>
        /// Called when the value of a property is changing in order to raise the INotifyPropertyChanging.PropertyChanging event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanging(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            var handler = this.PropertyChanging;
            if (handler != null)
            {
                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Called when the value of a property is changing in order to raise the INotifyPropertyChanging.PropertyChanging event.
        /// </summary>
        /// <param name="property">The property that is changing.</param>
        /// <remarks>
        /// This method is around 5 times slower that the version that takes the property name as a string (100.000 calls take ~2000ms vs. ~400ms for the other version).
        /// </remarks>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        protected void OnPropertyChanging(Expression<Func<object>> property)
        {
            this.OnPropertyChanging(ReflectionUtils.GetPropertyName(property));
        }

        /// <summary>
        /// Sets the backing field of a property to a specified value and raises the INotifyPropertyChanged.PropertyChanged event
        /// to indicate that the property's value has changed.
        /// </summary>
        /// <typeparam name="T">The type of the backing field and the property.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="property">The property.</param>        
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        protected void SetProperty<T>(ref T field, T value, Expression<Func<object>> property)
        {
            this.SetProperty(ref field, value, property, null);
        }

        /// <summary>
        /// Sets the backing field of a property to a specified value and raises the INotifyPropertyChanged.PropertyChanged event
        /// to indicate that the property's value has changed.
        /// </summary>
        /// <typeparam name="T">The type of the backing field and the property.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="property">The property.</param>
        /// <param name="afterPropertyChanged">The action executed after the property value has changed and the PropertyChanged event has been raised.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        protected void SetProperty<T>(ref T field, T value, Expression<Func<object>> property, Action afterPropertyChanged)
        {
            bool sameValue = (field == null && value == null)
                || (field != null && value != null && field.Equals(value));

            if (!sameValue)
            {
                this.OnPropertyChanging(property);
                field = value;
                this.OnPropertyChanged(property);
                if (afterPropertyChanged != null)
                {
                    afterPropertyChanged();
                }

                this.OnPropertyChanged(() => Error);
            }
        }

        /// <summary>
        /// Warns the developer if this object does not have a public property with the specified name. This method does not exist in a Release build.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [Conditional("DEBUG")]
        private void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real, public or non-public, instance property on this object.
            var properties = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (properties.FirstOrDefault(prop => prop.Name == propertyName) == null)
            {
                string msg = "Invalid property name: " + propertyName;
                throw new ZPKException(string.Empty, msg);
            }
        }

        /// <summary>
        /// Initializes all properties of type <see cref="VMProperty"/> using the default constructor.
        /// </summary>
        private void InitializeVMProperties()
        {
            var allProperties = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var vmprops =
                allProperties.Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(VMProperty<>));
            foreach (PropertyInfo vmprop in vmprops)
            {
                object newValue = Activator.CreateInstance(vmprop.PropertyType);
                vmprop.SetValue(this, newValue, null);
            }
        }

        /// <summary>
        /// Called when the <see cref="IsReadOnly"/> property has changed.
        /// </summary>
        private void OnIsReadOnlyChanged()
        {
            // Mark that the IsReadOnly property was explicitly set and did not inherit its value from a parent view-model.
            this.isReadOnlyInherited = false;

            // Set the new value in all child view models.
            bool newValue = this.IsReadOnly;
            var action = new Action<ViewModel>(vm =>
                {
                    // Update the read-only state on all child view models that inherit it from this instance; the children that inherit it are
                    // those for which the IsReadOnly property was never explicitly set.
                    if (vm.isReadOnlyInherited)
                    {
                        // Do not set the IsReadOnly property directly on the child because it will cause it to be considered not inherited and re-enter
                        // the OnIsReadOnlyChanged method. Instead set the backing filed and notify that the property has changed.
                        // Also mark that the property value is inherited.
                        vm.isReadOnly = newValue;
                        vm.isReadOnlyInherited = true;
                        vm.OnPropertyChanged(() => vm.IsReadOnly);
                    }
                });
            this.InvokeActionOnChildViewModels(action);
        }

        /// <summary>
        /// Called when the <see cref="IsInViewerMode"/> property has changed.
        /// </summary>
        private void OnIsInViewerModeChanged()
        {
            var newValue = this.IsInViewerMode;
            var action = new Action<ViewModel>(vm => vm.IsInViewerMode = newValue);
            this.InvokeActionOnChildViewModels(action);
        }

        /// <summary>
        /// Invokes the specified action on every child view model of this instance.
        /// </summary>
        /// <typeparam name="T">The type of the property where the action is invoked.</typeparam>
        /// <param name="action">The action to invoke.</param>
        protected void InvokeActionOnChildViewModels<T>(Action<T> action) where T : class
        {
            foreach (var childVMProperty in this.ChildViewModels.ChildViewModelProperties)
            {
                var childViewModel = childVMProperty.GetValue(this, null) as T;
                if (childViewModel != null)
                {
                    action.Invoke(childViewModel);
                }
            }

            foreach (var childVMCollectionProperty in this.ChildViewModels.ChildViewModelCollections)
            {
                var childViewModelCollection = (IEnumerable)childVMCollectionProperty.GetValue(this, null);
                if (childViewModelCollection != null)
                {
                    var enumerator = childViewModelCollection.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        var childViewModel = enumerator.Current as T;
                        if (childViewModel != null)
                        {
                            action.Invoke(childViewModel);
                        }
                    }
                }
            }
        }

        #endregion Properties related code

        #region Validation

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object or empty string if there is nothing wrong with this instance.</returns>
        private string Validate()
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(
                this,
                new ValidationContext(this, null, null),
                results,
                true);

            return string.Join(Environment.NewLine, results.Select(msg => msg.ErrorMessage));
        }

        /// <summary>
        /// Validates the specified property of this instance.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>An error message describing what is wrong with the property or empty string if there is nothing wrong with it.</returns>
        /// <exception cref="System.InvalidOperationException">A property with the specified name does not exist on this instance's type.</exception>
        private string ValidateProperty(string propertyName)
        {
            object propertyValue = null;
            var propInfo = this.GetType().GetProperty(propertyName);
            if (propInfo != null)
            {
                propertyValue = propInfo.GetValue(this, null);
            }
            else
            {
#if DEBUG
                string messageFormat = "The property named '{0}' could not be found on an object of type '{1}'.";
                throw new InvalidOperationException(string.Format(messageFormat, propertyName, this.GetType().Name));
#endif
            }

            List<ValidationResult> validationResults = new List<ValidationResult>();
            Validator.TryValidateProperty(
                propertyValue,
                new ValidationContext(this, null, null) { MemberName = propertyName },
                validationResults);

            return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
        }

        #endregion Validation

        #region Child View Models

        /// <summary>
        /// Initializes the cache containing the information about the child view models of all ViewModel derived types.
        /// </summary>
        /// <param name="viewModelType">Type of the view model for which to initialize the child view models information.</param>        
        private static void InitializeChildViewModelsDataCache(Type viewModelType)
        {
            lock (initChildViewModelsInfoSync)
            {
                if (childViewModelsInfoCache.ContainsKey(viewModelType))
                {
                    return;
                }

                var viewModelBaseType = typeof(ViewModel);
                var listInterfaceTypes = new List<Type>() { typeof(IEnumerable<>), typeof(IList<>), typeof(ICollection<>), typeof(ISet<>) };
                var viewModelProperties = new List<PropertyInfo>();
                var viewModelcollections = new List<PropertyInfo>();

                var properties = viewModelType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (var property in properties.Where(p => p.CanRead && !p.PropertyType.IsValueType))
                {
                    var propertyType = property.PropertyType;
                    if (propertyType == viewModelBaseType || propertyType.IsSubclassOf(viewModelBaseType))
                    {
                        // A property represents a child view-model if its type if its type is ViewModel or a type derived from ViewModel.
                        viewModelProperties.Add(property);
                    }
                    else if (propertyType.IsGenericType)
                    {
                        // Now we identify properties representing a collection of child view models.
                        // A property representing a collection of child view models has a generic type with exactly 1 generic argument and the generic argument is
                        // ViewModel or a type derived from ViewModel. Also, the property's generic type must implement or be IEnumerable<>, IList<>,
                        // ICollection<> or ISet<>.
                        var propertyTypeGeneric = propertyType.GetGenericTypeDefinition();
                        var genericArgs = propertyType.GetGenericArguments();
                        if (genericArgs.Length == 1
                            && (genericArgs[0] == viewModelBaseType || genericArgs[0].IsSubclassOf(viewModelBaseType))
                            && (listInterfaceTypes.Contains(propertyTypeGeneric)
                            || propertyType.GetInterfaces().Where(t => t.IsGenericType).Select(t => t.GetGenericTypeDefinition()).Any(i => listInterfaceTypes.Contains(i))))
                        {
                            // The property is a collection of child view-models.
                            viewModelcollections.Add(property);
                        }
                    }
                }

                var childViewModelsInfo = new ChildViewModelsInfo(viewModelType, viewModelProperties, viewModelcollections);
                childViewModelsInfoCache[viewModelType] = childViewModelsInfo;
            }
        }

        #endregion Child View Models

        #region Inner classes

        /// <summary>
        /// Contains information about the properties representing child view-models in a parent view-model.
        /// </summary>
        protected class ChildViewModelsInfo
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ChildViewModelsInfo" /> class.
            /// </summary>
            /// <param name="viewModelType">The view model type to which the child view models data applies.</param>
            /// <param name="childViewModelProperties">The properties representing single child view-model instances.</param>
            /// <param name="childViewModelCollections">The properties representing collections of child view-models.</param>
            /// <exception cref="System.ArgumentNullException">The view-model type was null.</exception>
            public ChildViewModelsInfo(
                Type viewModelType,
                IEnumerable<PropertyInfo> childViewModelProperties,
                IEnumerable<PropertyInfo> childViewModelCollections)
            {
                if (viewModelType == null)
                {
                    throw new ArgumentNullException("viewModelType", "The view-model type was null.");
                }

                if (childViewModelProperties == null)
                {
                    throw new ArgumentNullException("childViewModelProperties", "The list of properties representing single child view-model instances was null.");
                }

                if (childViewModelCollections == null)
                {
                    throw new ArgumentNullException("childViewModelCollections", "The list of properties representing collections of child view-model instances was null.");
                }

                this.ViewModelType = viewModelType;
                this.ChildViewModelProperties = new List<PropertyInfo>(childViewModelProperties);
                this.ChildViewModelCollections = new List<PropertyInfo>(childViewModelCollections);
            }

            /// <summary>
            /// Gets the view-model type for which this instance was created.
            /// </summary>
            public Type ViewModelType { get; private set; }

            /// <summary>
            /// Gets the properties representing single child view-models.
            /// </summary>
            public IEnumerable<PropertyInfo> ChildViewModelProperties { get; private set; }

            /// <summary>
            /// Gets the properties representing collections of child view-models.
            /// </summary>
            public IEnumerable<PropertyInfo> ChildViewModelCollections { get; private set; }
        }

        #endregion Inner classes
    }
}