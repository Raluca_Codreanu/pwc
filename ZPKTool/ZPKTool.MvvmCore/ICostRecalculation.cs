﻿namespace ZPKTool.MvvmCore
{
    using System;

    /// <summary>
    /// Defines the cost recalculation.
    /// </summary>
    public interface ICostRecalculation
    {
        /// <summary>
        /// Gets or sets the cost recalculation clone manager.
        /// </summary>
        ICostRecalculationCloneManager CloneManager { get; set; }

        /// <summary>
        /// Stops the recalculation notifications.
        /// </summary>
        void StopRecalculationNotifications();

        /// <summary>
        /// Resumes the recalculation notifications.
        /// </summary>
        /// <param name="notifyNow">if set to true a recalculation notification occurs.</param>
        void ResumeRecalculationNotifications(bool notifyNow);

        /// <summary>
        /// Occurs when the calculation needs to be refreshed.
        /// </summary>
        event EventHandler RequestRecalculation;
    }
}