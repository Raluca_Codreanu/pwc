﻿namespace ZPKTool.MvvmCore
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Helper class for validating the parameters of methods.
    /// </summary>
    public static class Argument
    {
        /// <summary>
        /// Determines whether the specified parameter is not null. The check fails if the parameter value is null.
        /// </summary>
        /// <param name="paramName">The name of the parameter; appears in the error message if the check fails.</param>
        /// <param name="paramValue">The value of the parameter to validate.</param>
        /// <exception cref="ArgumentNullException">The parameter value is null.</exception>
        [DebuggerStepThrough]
        public static void IsNotNull(string paramName, object paramValue)
        {
            if (paramValue == null)
            {
                throw new ArgumentNullException(paramName, string.Format("Argument '{0}' cannot be null.", paramName));
            }
        }

        /// <summary>
        /// Determines whether the specified string parameter is not null or empty. The check fails if the parameter value is null or empty string.
        /// </summary>
        /// <param name="paramName">The name of the parameter; appears in the error message if the check fails.</param>
        /// <param name="paramValue">The value of the parameter to validate.</param>
        /// <exception cref="ArgumentNullException">The parameter value is null or empty.</exception>
        [DebuggerStepThrough]
        public static void IsNotNullOrEmpty(string paramName, string paramValue)
        {
            if (string.IsNullOrEmpty(paramValue))
            {
                throw new ArgumentNullException(paramName, string.Format("Argument '{0}' cannot be null or empty.", paramName));
            }
        }

        /// <summary>
        /// Determines whether the specified string parameter is not null or whitespace. The check fails if the parameter value is null or whitespace.
        /// </summary>
        /// <param name="paramName">The name of the parameter; appears in the error message if the check fails.</param>
        /// <param name="paramValue">The value of the parameter to validate.</param>
        /// <exception cref="ArgumentNullException">The parameter value is null or whitespace.</exception>
        [DebuggerStepThrough]
        public static void IsNotNullOrWhiteSpace(string paramName, string paramValue)
        {
            if (string.IsNullOrWhiteSpace(paramValue))
            {
                throw new ArgumentNullException(paramName, string.Format("Argument '{0}' cannot be null or whitespace.", paramName));
            }
        }
    }
}
