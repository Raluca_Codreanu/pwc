﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Defines the value changed event for a view model property.
    /// </summary>
    public interface IVMProperty
    {
        /// <summary>
        /// Occurs when the value of the property changes.
        /// </summary>
        event EventHandler<ValueChangedEventArgs<object>> ValueChanged;
    }
}