﻿namespace ZPKTool.MvvmCore
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// This class should be used to define view-model data properties. It implements all features necessary (change tracking, change notification, data validation)
    /// to integrate with the binding system and ViewModel classes.
    /// </summary>
    /// <remarks>
    /// The properties of this type are automatically initialized by the base ViewModel class, so, in ViewModel classes, it is enough to define automated
    /// properties of this type.
    /// </remarks>
    /// <typeparam name="T">The type of the property's value.</typeparam>
    public class DataProperty<T> : VMProperty<T>, IDataErrorInfo, IChangeTracking
    {
        #region Fields

        /// <summary>
        /// The original value of the Value property. Used in tracking changes to the Value.
        /// </summary>
        private T originalValue;

        /// <summary>
        /// The validation attributes that apply to this instance.
        /// </summary>
        private List<ValidationAttribute> validationAttributes;

        /// <summary>
        /// The view-model instance that defines this instance as its member.
        /// </summary>
        private object viewModel;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="DataProperty&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="viewModel">The view model for which this instance is created.</param>
        /// <param name="propertyName">The name of the viewModel's property to which this instance is (or will be) assigned.</param>
        /// <exception cref="ArgumentNullException">One of the parameters was null or empty.</exception>        
        /// <exception cref="ArgumentException"><paramref name="propertyName"/> is not a property of <paramref name="viewModel"/></exception>
        public DataProperty(object viewModel, string propertyName)
            : base()
        {
            // Check the arguments for null an empty.
            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel", "The viewModel can't be null.");
            }

            if (string.IsNullOrWhiteSpace(propertyName))
            {
                throw new ArgumentNullException("propertyName", "The propertyName can't be null or whitespace.");
            }

            this.viewModel = viewModel;

            // Check view-model's ModelData property identified by the modelDataProperty argument exists and its value is this instance.
            Type viewModelType = viewModel.GetType();
            PropertyInfo viewModelProperty = viewModelType.GetProperty(propertyName);
            if (viewModelProperty == null)
            {
                string message = string.Format(
                    "The property named '{0}', representing a DataProperty wrapper, could not be found on the '{1}' view-model.",
                    propertyName,
                    viewModelType.Name);
                throw new ArgumentException(message, "propertyName");
            }

            // Get the validation attributes decorating the view-model's property whose value this instance is.            
            this.validationAttributes = viewModelProperty.GetCustomAttributes(typeof(ValidationAttribute), true).OfType<ValidationAttribute>().ToList();
        }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the object's Value has changed.
        /// </summary>
        /// <returns>true if the object’s content has changed since the last call to <see cref="M:System.ComponentModel.IChangeTracking.AcceptChanges"/>; otherwise, false.</returns>
        public bool IsChanged
        {
            get
            {
                var wm = this.viewModel as ViewModel;
                if (wm != null && wm.IsReadOnly)
                {
                    return false;
                }

                bool equal = (this.Value == null && this.originalValue == null)
                    || (this.Value != null && this.originalValue != null && this.Value.Equals(this.originalValue));

                return !equal;
            }
        }

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        public string Error
        {
            get
            {
                return this.ValidateValue();
            }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public string this[string propertyName]
        {
            get
            {
                if (propertyName == "Value")
                {
                    return this.ValidateValue();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion Properties

        /// <summary>
        /// Resets the object’s state to unchanged by accepting the modifications.
        /// </summary>
        public void AcceptChanges()
        {
            this.originalValue = this.Value;
        }

        /// <summary>
        /// Validates the wrapped data (the Value property).
        /// </summary>
        /// <returns>An error message indicating what is wrong with the data. An empty string is returned if there is nothing wrong with it.</returns>
        private string ValidateValue()
        {
            if (validationAttributes.Count == 0)
            {
                return string.Empty;
            }

            // Validate the value of this instance using the validation attributes applied to the property whose instance this is.
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Send the view model instance to the validator as custom data.
            var validationContextItems = new Dictionary<object, object>();
            validationContextItems.Add("ViewModel", viewModel);

            Validator.TryValidateValue(
                this.Value,
                new ValidationContext(this, null, validationContextItems),
                validationResults,
                this.validationAttributes);

            return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
        }
    }
}