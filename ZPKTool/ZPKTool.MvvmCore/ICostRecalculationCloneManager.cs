﻿namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Defines the cost recalculation clone manager.
    /// </summary>
    public interface ICostRecalculationCloneManager
    {
        /// <summary>
        /// Clones the model for the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        void Clone(ViewModel viewModel);
    }
}
