﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// ViewModel Edit State.
    /// </summary>
    public enum ViewModelEditMode
    {
        /// <summary>
        /// The view-model is not editing a Model. This is usually the case when the view-model is in some form of read-only mode.
        /// </summary>
        None,

        /// <summary>
        ///  A ViewModel mode indicating whether the model is in the process of creating.
        /// </summary>
        Create,

        /// <summary>
        /// A ViewModel mode indicating whether the model is in the process of editing.
        /// </summary>
        Edit
    }
}
