﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// This class facilitates associating a key binding in XAML markup to a property or data context defined in a View Model
    /// <remarks>
    /// Binding some properties to elements that are not part of visual tree in some cases can't be done because they don't inherit the properties from parent
    /// This can be solved by using this class that stores the property to be accessed by the element. 
    /// </remarks>
    /// </summary>
    public class BindingProxy : Freezable
    {
        /// <summary>
        ///  Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the data property
        /// </summary>
        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        #region Overrides of Freezable

        /// <summary>
        /// Initializes a new instance of the <see cref="System.Windows.Freezable"/> class.
        /// </summary>
        /// <returns>The new instance.</returns>
        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }

        #endregion
    }
}
