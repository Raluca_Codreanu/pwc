﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// A service used to display windows and message boxes from view-models.
    /// </summary>
    [Export(typeof(IWindowService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class WindowService : IWindowService
    {
        /// <summary>
        /// The UI Dispatcher.
        /// </summary>
        private readonly IDispatcherService dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowService"/> class.
        /// </summary>
        /// <param name="messageDialog">The message dialog service used to display the message dialogs.</param>
        /// <param name="dispatcher">The UI dispatcher.</param>
        [ImportingConstructor]
        public WindowService(IMessageDialogService messageDialog, IDispatcherService dispatcher)
        {
            Argument.IsNotNull("messageDialog", messageDialog);
            Argument.IsNotNull("dispatcher", dispatcher);

            this.MessageDialogService = messageDialog;
            this.dispatcher = dispatcher;
        }

        /// <summary>
        /// Gets or sets the source of an interaction (usually a view), when this instance is used with InteractionTrigger.
        /// </summary>
        public DependencyObject TriggerSource { get; set; }

        /// <summary>
        /// Gets a service that can be used to display message dialogs.
        /// </summary>
        public IMessageDialogService MessageDialogService { get; private set; }

        /// <summary>
        /// Shows the specified window.
        /// </summary>
        /// <param name="window">The window to show.</param>
        public void ShowWindow(Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The window was null.");
            }

            Action action = () =>
            {
                window.Owner = this.DetermineWindowOwner(window);
                window.Show();
            };

            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }
        }

        /// <summary>
        /// Shows the specified window as a modal dialog.
        /// </summary>
        /// <param name="window">The window to show.</param>
        /// <returns>
        /// A value that signifies how the dialog window was closed by the user.
        /// </returns>
        public bool? ShowDialog(Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The window was null.");
            }

            bool? result = null;
            Action action = () =>
            {
                window.Owner = this.DetermineWindowOwner(window);
                result = window.ShowDialog();
            };

            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }

            return result;
        }

        /// <summary>
        /// Shows the view associated with the specified view-model in a modeless window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model template that defines the desired view. Should be set when you have in the same scope multiple templates
        /// for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <param name="windowClosedAction">Represents the action performed when the window is closed. Defaults to null.</param>
        public void ShowViewInWindow(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false, Action windowClosedAction = null)
        {
            var window = this.GetWindowForViewModel(viewModel, templateKey, handleEscKey, singleInstance);

            Action action = () =>
            {
                // If a "window closed" handler is defined register to the window's Closed event and invoke the handler there.
                if (windowClosedAction != null)
                {
                    EventHandler windowClosed = null;
                    windowClosed = (s, e) =>
                    {
                        window.Closed -= windowClosed;
                        if (windowClosedAction != null)
                        {
                            if (this.dispatcher.CheckAccess())
                            {
                                windowClosedAction();
                            }
                            else
                            {
                                this.dispatcher.Invoke(windowClosedAction);
                            }
                        }
                    };

                    window.Closed += windowClosed;
                }

                window.Show();
            };

            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }
        }

        /// <summary>
        /// Shows the view associated with the specified view-model in a dialog window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model template that defines the desired view. Should be set when you have in the same scope multiple templates
        /// for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <returns>
        /// A value that signifies how the dialog window was closed by the user.
        /// </returns>
        public bool? ShowViewInDialog(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false)
        {
            var window = this.GetWindowForViewModel(viewModel, templateKey, handleEscKey, singleInstance);

            bool? result = null;
            Action action = () => result = window.ShowDialog();
            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }

            return result;
        }

        /// <summary>
        /// Closes the window containing the view of a specified view-model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>
        /// true if a window was found for the view and was closed; otherwise false.
        /// </returns>
        public bool CloseViewWindow(object viewModel)
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.DataContext == viewModel)
                {
                    try
                    {
                        wnd.Close();
                    }
                    catch (InvalidOperationException)
                    {
                        // used to handle the situation when the current window is already closing
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the view-model window.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>The window, coresponding to the view-model instance passed as parameter.</returns>
        public Window GetViewModelWindow(object viewModel)
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.DataContext == viewModel)
                {
                    return wnd;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the window for view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="templateKey">The template key.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <returns>
        /// A window containing the view associated with the view model.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// Failed to find a data template for the view-model
        /// or
        /// The found view-model template did not contain a window.
        /// </exception>
        private Window GetWindowForViewModel(object viewModel, object templateKey, bool handleEscKey, bool singleInstance = false)
        {
            Window window = null;

            if (singleInstance)
            {
                window = this.FocusWindow(viewModel);
            }

            if (window == null)
            {
                // Find the DataTemplate associated with the view-model
                var frameworkElement = this.TriggerSource as FrameworkElement;

                var tplKey = templateKey;
                if (tplKey == null)
                {
                    tplKey = new DataTemplateKey(viewModel.GetType());
                }

                // Search for the template in the resources of the object that requested the interaction
                DataTemplate viewModelTemplate = null;
                if (frameworkElement != null)
                {
                    viewModelTemplate = frameworkElement.Resources[tplKey] as DataTemplate;
                }

                if (viewModelTemplate == null)
                {
                    // Search for the template in the global app resources
                    viewModelTemplate = Application.Current.Resources[tplKey] as DataTemplate;
                }

                if (viewModelTemplate == null)
                {
                    throw new InvalidOperationException("Failed to find a data template for the view-model " + viewModel.GetType());
                }

                var view = viewModelTemplate.LoadContent();
                window = view as Window;
                if (window == null)
                {
                    string message = string.Format("The template of the view model {0} did not contain a window (the content was {1}).", viewModel.GetType(), view != null ? view.GetType().ToString() : "null");
                    throw new InvalidOperationException(message);
                }

                window.DataContext = viewModel;

                ILifeCycleManager lcfMngr = viewModel as ILifeCycleManager;
                if (lcfMngr != null)
                {
                    window.Loaded += (s, e) => lcfMngr.OnLoaded();
                    window.Closed += (s, e) => lcfMngr.OnUnloaded();
                    window.Closing += (s, e) => { e.Cancel = !lcfMngr.OnUnloading(); };
                }

                // When the window is loaded, focus the 1st text box                
                RoutedEventHandler loadAction = null;
                loadAction = (s, e) =>
                {
                    window.Loaded -= loadAction;
                    var firstTextBox = this.FindFirstChild<System.Windows.Controls.TextBox>(window);
                    if (firstTextBox != null)
                    {
                        firstTextBox.Focus();
                    }
                };
                window.Loaded += loadAction;

                if (handleEscKey)
                {
                    // Close the window on pressing Esc key
                    window.PreviewKeyDown += (s, e) =>
                    {
                        if (e.Key == System.Windows.Input.Key.Escape)
                        {
                            window.Close();
                        }
                    };
                }

                // Determine the owner of the created window.
                window.Owner = DetermineWindowOwner(window);
            }

            return window;
        }

        /// <summary>
        /// Focuses the window of the specified view model instance.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>True if the window was found and focus setting was attempted; otherwise, false.</returns>
        private Window FocusWindow(object viewModel)
        {
            var window = Application.Current.Windows.OfType<Window>().Reverse().FirstOrDefault(wnd => wnd.IsLoaded & wnd.DataContext != null && wnd.DataContext.GetType() == viewModel.GetType());
            if (window != null)
            {
                Action action = () => window.Focus();
                if (this.dispatcher.CheckAccess())
                {
                    action();
                }
                else
                {
                    this.dispatcher.Invoke(action);
                }

                return window;
            }

            return null;
        }

        /// <summary>
        /// Analyzes both visual tree in order to the 1st element of a given type that is the descendant of the <paramref name="source"/> item.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="source">The root element that marks the source of the search. If the source is already of the requested type, it will not be included in the search.</param>
        /// <returns>
        /// The 1st descendant of <paramref name="source"/> that matches the requested type.
        /// </returns>
        private T FindFirstChild<T>(DependencyObject source) where T : DependencyObject
        {
            if (source == null)
            {
                return null;
            }

            var count = System.Windows.Media.VisualTreeHelper.GetChildrenCount(source);
            for (int i = 0; i < count; i++)
            {
                DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(source, i);
                T typedChild = child as T;
                if (typedChild != null)
                {
                    return typedChild;
                }

                T found = this.FindFirstChild<T>(child);
                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Determines an owner for a specified window.
        /// </summary>
        /// <param name="window">The window for which to determine the owner.</param>
        /// <returns>
        /// A Window or null.
        /// </returns>
        private Window DetermineWindowOwner(Window window)
        {
            // Set the active window as the owner, if there is one.
            var owner = Application.Current.Windows.OfType<Window>().FirstOrDefault(wnd => wnd.IsActive && wnd != window && IsAppWindow(wnd));
            if (owner == null)
            {
                // If there is no active window (the app does not have focus), use the last loaded window from the app windows list as the owner.
                owner = Application.Current.Windows.OfType<Window>().Reverse().FirstOrDefault(wnd => wnd.IsLoaded && wnd != window && IsAppWindow(wnd));
            }

            return owner;
        }

        /// <summary>
        /// Determines whether the specified window was created by the applicatio or is an inner/overlay window created by Avalon Dock or some other framework.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <returns>
        /// true if the window was created by the application; otherwise, false.
        /// </returns>
        /// <remarks>
        /// There's no reference here to the assemblies that define the invalid windows so the type names are hard-coded.
        /// </remarks>
        private bool IsAppWindow(Window window)
        {
            if (window == null)
            {
                return false;
            }

            var windowType = typeof(Window);
            var crtType = window.GetType();
            while (crtType != null && crtType != windowType)
            {
                // Check if the window's type is the base window class from Avalon Dock
                if (crtType.FullName == "AvalonDock.AvalonDockWindow"
                    || crtType.FullName == "Microsoft.XamlDiagnostics.WpfTap.WpfVisualTreeService.Adorners.AdornerLayerWindow")
                {
                    return true;
                }

                crtType = crtType.BaseType;
            }

            return false;
        }
    }
}