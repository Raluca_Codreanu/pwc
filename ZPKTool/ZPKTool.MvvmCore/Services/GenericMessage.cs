﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Passes a generic value to recipients.
    /// </summary>
    /// <typeparam name="T">The type of the generic value.</typeparam>
    public class GenericMessage<T> : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMessage&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="content">The content of the message.</param>
        public GenericMessage(T content)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMessage&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        /// <param name="content">The content of the message.</param>
        public GenericMessage(object sender, T content)
            : base(sender)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMessage&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, amd may be null.</param>
        /// <param name="content">The content of the message.</param>
        public GenericMessage(object sender, object target, T content)
            : base(sender, target)
        {
            this.Content = content;
        }

        /// <summary>
        /// Gets the content of the message.
        /// </summary>
        public T Content { get; private set; }
    }
}
