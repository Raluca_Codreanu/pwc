﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Contains event data for the <see cref="UndoManager.UndoPerformed"/> event.
    /// </summary>
    public class UndoPerformedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UndoPerformedEventArgs"/> class.
        /// </summary>
        /// <param name="instance">The object on which the undo was performed.</param>
        /// <param name="item">The item containing the undo information.</param>
        public UndoPerformedEventArgs(object instance, UndoableItem item)
        {
            this.Instance = instance;
            this.Item = item;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoPerformedEventArgs"/> class.
        /// </summary>
        /// <param name="instance">The object on which the undo was performed.</param>
        /// <param name="item">The item containing the undo information.</param>
        /// <param name="navigateToItem">A value indicating whether to navigate to the undoable item or not.</param>
        public UndoPerformedEventArgs(object instance, UndoableItem item, bool navigateToItem)
            : this(instance, item)
        {
            this.NavigateToItem = navigateToItem;
        }

        /// <summary>
        /// Gets the object on which the undo was performed.
        /// </summary>
        public object Instance { get; private set; }

        /// <summary>
        /// Gets the item containing the undo information.
        /// </summary>
        public UndoableItem Item { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to navigate to the item on the UI after the undo is performed.
        /// </summary>
        public bool NavigateToItem { get; private set; }
    }
}
