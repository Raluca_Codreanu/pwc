﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// A service that allows to browse for folders.
    /// </summary>
    [Export(typeof(IFolderBrowserService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FolderBrowserService : IFolderBrowserService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FolderBrowserService"/> class.
        /// </summary>
        public FolderBrowserService()
        {
        }

        #region IFolderBrowserService Members

        /// <summary>
        /// Gets or sets the path selected by the user.
        /// </summary>
        public string SelectedPath { get; set; }

        /// <summary>
        /// Gets or sets the descriptive text displayed above the folders tree view in the dialog box.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Shows the folder browser dialog that allows the user to select a folder.
        /// </summary>
        /// <returns>
        /// True if the user has selected a folder (clicked the OK button); otherwise false.
        /// </returns>
        public bool ShowFolderBrowserDialog()
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.Description = this.Description;
            dialog.SelectedPath = this.SelectedPath;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            this.SelectedPath = dialog.SelectedPath;
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
