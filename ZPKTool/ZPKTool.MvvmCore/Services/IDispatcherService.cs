﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Threading;

    /// <summary>
    /// Exposes some of the WPF UI Dispatcher's operations as a service.
    /// </summary>
    public interface IDispatcherService
    {
        /// <summary>
        /// Determines whether the calling thread is the thread associated with the UI dispatcher.
        /// </summary>
        /// <returns>true if the calling thread is with the UI dispatcher; otherwise, false.</returns>
        bool CheckAccess();

        /// <summary>
        /// Executes the specified action asynchronously on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        void BeginInvoke(Action action);

        /// <summary>
        /// Executes the specified action asynchronously at the specified priority on the thread the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is execute.</param>
        void BeginInvoke(Action action, DispatcherPriority priority);

        /// <summary>
        /// Executes the specified action on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        void Invoke(Action action);

        /// <summary>
        /// Executes the specified action synchronously at the specified priority on the thread on which the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is executed.</param>
        void Invoke(Action action, DispatcherPriority priority);
    }
}
