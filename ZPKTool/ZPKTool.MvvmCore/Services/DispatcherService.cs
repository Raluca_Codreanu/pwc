﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Text;
    using System.Windows.Threading;

    /// <summary>
    /// Implementation of the Dispatcher service. Safe to be used in unit tests.
    /// </summary>
    [Export(typeof(IDispatcherService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DispatcherService : IDispatcherService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherService"/> class.
        /// </summary>
        public DispatcherService()
        {
        }

        /// <summary>
        /// Determines whether the calling thread is the thread associated with the UI dispatcher.
        /// </summary>
        /// <returns>
        /// true if the calling thread is associated with the UI dispatcher or the UI dispatcher is null; otherwise, false.
        /// </returns>
        public bool CheckAccess()
        {
            var dispatcher = GetCurrentDispatcher();
            if (dispatcher != null)
            {
                return dispatcher.CheckAccess();
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Executes the specified action asynchronously on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void BeginInvoke(Action action)
        {
            if (action == null)
            {
                return;
            }

            var dispatcher = GetCurrentDispatcher();
            if (dispatcher != null)
            {
                dispatcher.BeginInvoke(action);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Executes the specified action asynchronously at the specified priority on the thread the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is execute.</param>
        public void BeginInvoke(Action action, DispatcherPriority priority)
        {
            if (action == null)
            {
                return;
            }

            var dispatcher = GetCurrentDispatcher();
            if (dispatcher != null)
            {
                dispatcher.BeginInvoke(action, priority);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Executes the specified action on the thread that the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void Invoke(Action action)
        {
            if (action == null)
            {
                return;
            }

            var dispatcher = GetCurrentDispatcher();
            if (dispatcher != null)
            {
                dispatcher.Invoke(action);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Executes the specified action synchronously at the specified priority on the thread on which the UI Dispatcher is associated with.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="priority">The priority, relative to the other pending operations in the System.Windows.Threading.Dispatcher event queue,
        /// the specified action is executed.</param>
        public void Invoke(Action action, DispatcherPriority priority)
        {
            if (action == null)
            {
                return;
            }

            var dispatcher = GetCurrentDispatcher();
            if (dispatcher != null)
            {
                dispatcher.Invoke(action, priority);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Gets the dispatcher associated with the current application. Returns null if there is no current application (usually the case of unit tests).
        /// </summary>
        /// <returns>A Dispatcher instance or null if not available.</returns>
        private Dispatcher GetCurrentDispatcher()
        {
            System.Windows.Application currentApp = System.Windows.Application.Current;
            if (currentApp != null)
            {
                return currentApp.Dispatcher;
            }

            return null;
        }
    }
}
