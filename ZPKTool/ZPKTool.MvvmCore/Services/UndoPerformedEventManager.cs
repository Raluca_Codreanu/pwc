﻿using ZPKTool.Common;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// WeakEventManager implementation for the UndoPerformed event of the UndoManager.
    /// </summary>
    public class UndoPerformedEventManager : WeakEventManagerBase<UndoPerformedEventManager, UndoManager>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(UndoManager source)
        {
            source.UndoPerformed += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(UndoManager source)
        {
            source.UndoPerformed -= DeliverEvent;
        }
    }
}
