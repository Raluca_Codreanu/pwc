﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// A class representing the information used by a custom message dialog for configuration
    /// </summary>
    public class MessageDialogInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDialogInfo"/> class.
        /// </summary>
        public MessageDialogInfo()
        {
            this.Actions = new List<MessageDialogActionableItem>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDialogInfo"/> class.
        /// </summary>
        /// <param name="title">The message dialog title</param>
        /// <param name="message">The message that will appear on the message dialog box</param>
        /// <param name="iconResourceKey">The icon resource key on the message dialog box</param>
        /// <param name="actions">
        /// A collection of <see cref="MessageDialogActionableItem"/> that describe UIElements that can perform an action (like a button) 
        /// </param>
        public MessageDialogInfo(string title, string message, object iconResourceKey, IEnumerable<MessageDialogActionableItem> actions)
        {
            this.Title = title;
            this.Message = message;
            this.IconResourceKey = iconResourceKey;
            this.Actions = new List<MessageDialogActionableItem>(actions);
        }

        /// <summary>
        /// Gets or sets the title of the message dialog
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the text that will appear on the message dialog
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the icon of the message dialog
        /// </summary>
        public object IconResourceKey { get; set; }

        /// <summary>
        /// Gets the collection of items that can perform an action
        /// </summary>
        public ICollection<MessageDialogActionableItem> Actions { get; private set; }
    }
}
