﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A message class that allows to specify a callback that can be executed by the recipient when is done handling the message.
    /// </summary>
    public class NotificationMessageWithCallback : NotificationMessage
    {
        /// <summary>
        /// The callback to be executed by the recipient after it has handled the message.
        /// </summary>
        private readonly Delegate callback;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithCallback"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s).</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithCallback(string notification, Delegate callback)
            : base(notification)
        {
            Argument.IsNotNull("callback", callback);
            this.callback = callback;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithCallback"/> class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithCallback(object sender, string notification, Delegate callback)
            : base(sender, notification)
        {
            Argument.IsNotNull("callback", callback);
            this.callback = callback;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithCallback"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="notification">The notification.</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithCallback(object sender, object target, string notification, Delegate callback)
            : base(sender, target, notification)
        {
            Argument.IsNotNull("callback", callback);
            this.callback = callback;
        }

        /// <summary>
        /// Executes the callback provided to the message.
        /// </summary>
        /// <param name="parameters">The parameters for the callback.</param>
        /// <returns>An object returned by the callback.</returns>
        public object ExecuteCallback(params object[] parameters)
        {
            return this.callback.DynamicInvoke(parameters);
        }
    }
}
