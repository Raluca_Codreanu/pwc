﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The types of undo actions.
    /// </summary>
    public enum UndoActionType
    {
        /// <summary>
        /// The Default action.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represents an update action.
        /// </summary>
        Update = 1,

        /// <summary>
        /// Represents an insert action.
        /// </summary>
        Insert = 2,

        /// <summary>
        /// Represents a delete action.
        /// </summary>
        Delete = 3,

        /// <summary>
        /// Represents a move action.
        /// </summary>
        Move = 4,

        /// <summary>
        /// It marks the beginning or end of a sequence of actions.
        /// </summary>
        SequenceOfActions = 5,

        /// <summary>
        /// Represents a delete action.
        /// </summary>
        UndoManager = 6
    }
}
