﻿/*************************************************************************************************************
 This code is a modified version of Messenger.cs from MVVM Light Toolkit (http://mvvmlight.codeplex.com/)
*************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ZPKTool.MvvmCore.Helpers;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The Messenger is a class allowing objects to exchange messages.
    /// </summary>
    [Export(typeof(IMessenger))]
    public class Messenger : IMessenger
    {
        /// <summary>
        /// Used to synchronize the registration.
        /// </summary>
        private readonly object registerLock = new object();

        /// <summary>
        /// Registration list containing the handlers that registered to receive derived messaged too.
        /// </summary>
        private Dictionary<Type, List<WeakActionAndToken>> recipientsOfSubclassesAction;

        /// <summary>
        /// Registration list containing the handlers that registered not to receive derived messaged.
        /// </summary>
        private Dictionary<Type, List<WeakActionAndToken>> recipientsStrictAction;

        #region IMessenger Members

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        public virtual void Register<TMessage>(Action<TMessage> action)
        {
            Register(action, null, false);
        }

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        /// <param name="token">A token for a messaging channel. If an action is registered using a token it will receive only the messages sent using that token.
        /// The actions who were not registered using that token will not be invoked.</param>
        public virtual void Register<TMessage>(Action<TMessage> action, object token)
        {
            Register(action, token, false);
        }

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        /// <param name="receiveDerivedMessagesToo">If true, the action will be invoked also for message types deriving from TMessage (or implementing TMessage,
        /// if TMessage is an interface).
        /// <para>For example, if a SendOrderMessage and an ExecuteOrderMessage derive from OrderMessage (or implement it if its an interface),
        /// registering for OrderMessage and setting receiveDerivedMessagesToo to true will invoke the action for messages of both
        /// SendOrderMessage and ExecuteOrderMessage types.</para></param>
        public virtual void Register<TMessage>(Action<TMessage> action, bool receiveDerivedMessagesToo)
        {
            Register(action, null, receiveDerivedMessagesToo);
        }

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        /// <param name="token">A token for a messaging channel. If an action is registered using a token it will receive only the messages sent using that token.
        /// The actions who were not registered using that token will not be invoked.</param>
        /// <param name="receiveDerivedMessagesToo">If true, the action will be invoked also for message types deriving from TMessage (or implementing TMessage,
        /// if TMessage is an interface).
        /// <para>For example, if a SendOrderMessage and an ExecuteOrderMessage derive from OrderMessage (or implement it if its an interface),
        /// registering for OrderMessage and setting receiveDerivedMessagesToo to true will invoke the action for messages of both
        /// SendOrderMessage and ExecuteOrderMessage types.</para></param>
        public virtual void Register<TMessage>(Action<TMessage> action, object token, bool receiveDerivedMessagesToo)
        {
            lock (registerLock)
            {
                Type messageType = typeof(TMessage);

                Dictionary<Type, List<WeakActionAndToken>> recipients;

                if (receiveDerivedMessagesToo)
                {
                    if (recipientsOfSubclassesAction == null)
                    {
                        recipientsOfSubclassesAction = new Dictionary<Type, List<WeakActionAndToken>>();
                    }

                    recipients = recipientsOfSubclassesAction;
                }
                else
                {
                    if (recipientsStrictAction == null)
                    {
                        recipientsStrictAction = new Dictionary<Type, List<WeakActionAndToken>>();
                    }

                    recipients = recipientsStrictAction;
                }

                List<WeakActionAndToken> list;

                if (!recipients.ContainsKey(messageType))
                {
                    list = new List<WeakActionAndToken>();
                    recipients.Add(messageType, list);
                }
                else
                {
                    list = recipients[messageType];
                }

                var weakAction = new WeakDelegate(action);
                var item = new WeakActionAndToken
                {
                    Action = weakAction,
                    Token = token
                };

                list.Add(item);
            }

            Cleanup();
        }

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        public virtual void Send<TMessage>(TMessage message)
        {
            SendToTargetOrType(message, null, null);
        }

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods, and that
        /// belong to an object with the type TTarget.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <typeparam name="TTarget">The type of target objects that will receive the message. The message won't be sent to target objects of another type.
        /// <para>The target object is the object on which the handler action is defined.</para></typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        [SuppressMessage(
            "Microsoft.Design",
            "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "This syntax is more convenient than other alternatives.")]
        public virtual void Send<TMessage, TTarget>(TMessage message)
        {
            SendToTargetOrType(message, typeof(TTarget), null);
        }

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        /// <param name="token">A token for a messaging channel. Only the recipients that registered with this token will receive the message.</param>
        public virtual void Send<TMessage>(TMessage message, object token)
        {
            SendToTargetOrType(message, null, token);
        }

        /// <summary>
        /// Unregisters all handler actions of an object for all message types. After this method is executed, the object will no longer receive any message.
        /// </summary>
        /// <param name="recipient">The object whose handlers must be unregistered.</param>
        public virtual void Unregister(object recipient)
        {
            UnregisterFromLists(recipient, null, recipientsOfSubclassesAction);
            UnregisterFromLists(recipient, null, recipientsStrictAction);
            Cleanup();
        }

        /// <summary>
        /// Unregisters all handler actions of an object for the specified message type.
        /// The object will not receive messages of type TMessage anymore, but will still receive other message types for which it has registered.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the object's handlers must unregister from.</typeparam>
        /// <param name="recipient">The object whose handlers must be unregistered.</param>
        [SuppressMessage(
            "Microsoft.Design",
            "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "This syntax is more convenient than other alternatives.")]
        public virtual void Unregister<TMessage>(object recipient)
        {
            UnregisterFromLists(recipient, new List<Type>() { typeof(TMessage) }, recipientsOfSubclassesAction);
            UnregisterFromLists(recipient, new List<Type>() { typeof(TMessage) }, recipientsStrictAction);
            Cleanup();
        }

        /// <summary>
        /// Unregisters a specified handler action for a given type of messages. The handler will still be invoked for any other message types
        /// for which it has been registered.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the handler wants to unregister from.</typeparam>
        /// <param name="action">The handler that must be unregistered for the message type TMessage.</param>
        public virtual void Unregister<TMessage>(Action<TMessage> action)
        {
            UnregisterFromLists<TMessage>(action, null, recipientsStrictAction);
            UnregisterFromLists<TMessage>(action, null, recipientsOfSubclassesAction);
            Cleanup();
        }

        /// <summary>
        /// Unregisters a specified handler action for a given type of messages and a specified token. The handler will still be invoked for any other message types
        /// for which it has been registered and for the TMessage type and other tokens.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the handler wants to unregister from.</typeparam>
        /// <param name="action">The handler that must be unregistered for the message type TMessage.</param>
        /// <param name="token">The token for which the handler must be unregistered.</param>
        public virtual void Unregister<TMessage>(Action<TMessage> action, object token)
        {
            UnregisterFromLists<TMessage>(action, token, recipientsStrictAction);
            UnregisterFromLists<TMessage>(action, token, recipientsOfSubclassesAction);
            Cleanup();
        }

        #endregion

        /// <summary>
        /// Removes the weak references that were garbage collected from the specified lists.
        /// </summary>
        /// <param name="lists">The lists to clean up.</param>
        private static void CleanupList(IDictionary<Type, List<WeakActionAndToken>> lists)
        {
            if (lists == null)
            {
                return;
            }

            lock (lists)
            {
                var listsToRemove = new List<Type>();
                foreach (var list in lists)
                {
                    var deadRecipients = list.Value.Where(item => item.Action == null || !item.Action.IsAlive).ToList();
                    foreach (WeakActionAndToken recipient in deadRecipients)
                    {
                        list.Value.Remove(recipient);
                    }

                    if (list.Value.Count == 0)
                    {
                        listsToRemove.Add(list.Key);
                    }
                }

                foreach (Type key in listsToRemove)
                {
                    lists.Remove(key);
                }
            }
        }

        /// <summary>
        /// Checks if a specified type implements a specified interface.
        /// </summary>
        /// <param name="instanceType">The type to check.</param>
        /// <param name="interfaceType">The type of the interface.</param>
        /// <returns>True if <paramref name="instanceType"/> implements <paramref name="interfaceType"/>; otherwise, false.</returns>
        private static bool Implements(Type instanceType, Type interfaceType)
        {
            if (interfaceType == null || instanceType == null)
            {
                return false;
            }

            if (instanceType.GetInterfaces().FirstOrDefault(intrface => intrface == interfaceType) != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Unregisters the specified recipient from the specified registration lists, for some or all message type(s).
        /// </summary>
        /// <param name="recipient">The recipient.</param>
        /// <param name="messageTypes">The types of the message from which to unregister. If set to null it will unregister from all message types.</param>
        /// <param name="lists">The lists.</param>
        private static void UnregisterFromLists(
            object recipient,
            List<Type> messageTypes,
            Dictionary<Type, List<WeakActionAndToken>> lists)
        {
            if (recipient == null
                || lists == null
                || lists.Count == 0)
            {
                return;
            }

            lock (lists)
            {
                // Compile the list of message types for which to unregister the recipient.
                List<Type> typesToUnregisterFrom = null;
                if (messageTypes != null)
                {
                    typesToUnregisterFrom = messageTypes.Where(msgType => lists.ContainsKey(msgType)).ToList();
                }
                else
                {
                    typesToUnregisterFrom = lists.Keys.ToList();
                }

                // Unregister the recipient
                foreach (Type msgType in typesToUnregisterFrom)
                {
                    foreach (WeakActionAndToken item in lists[msgType])
                    {
                        if (item.Action != null && recipient == item.Action.Target)
                        {
                            item.Action.MarkForDeletion();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unregisters the specified handler from the specified registration lists, for some a specified token.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="action">The handler to unregister.</param>
        /// <param name="token">The token to unregister for. Is ignored if set to null..</param>
        /// <param name="lists">The lists from which to unregister.</param>
        private static void UnregisterFromLists<TMessage>(
            Action<TMessage> action,
            object token,
            Dictionary<Type, List<WeakActionAndToken>> lists)
        {
            Type messageType = typeof(TMessage);

            if (action == null || lists == null || lists.Count == 0 || !lists.ContainsKey(messageType))
            {
                return;
            }

            lock (lists)
            {
                foreach (WeakActionAndToken item in lists[messageType])
                {
                    if (item.Action != null && item.Action == action && (token == null || token.Equals(item.Token)))
                    {
                        item.Action.MarkForDeletion();
                    }
                }
            }
        }

        /// <summary>
        /// Sends a specified message to all recipients with a specified type and/or token from a specified list.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message to send.</typeparam>
        /// <param name="message">The message to send.</param>
        /// <param name="list">The list of recipients.</param>
        /// <param name="messageTargetType">Type of the objects that will receive the message.</param>
        /// <param name="token">The token.</param>
        private static void SendToList<TMessage>(
            TMessage message,
            List<WeakActionAndToken> list,
            Type messageTargetType,
            object token)
        {
            if (list != null)
            {
                // Clone to protect from people registering in a "receive message" method                
                List<WeakActionAndToken> listClone = new List<WeakActionAndToken>(list);
                foreach (WeakActionAndToken item in listClone)
                {
                    object actionTarget = item.Action.Target;
                    if (actionTarget != null
                        && (messageTargetType == null || actionTarget.GetType() == messageTargetType || Implements(actionTarget.GetType(), messageTargetType))
                        && ((item.Token == null && token == null) || (item.Token != null && item.Token.Equals(token))))
                    {
                        item.Action.Invoke(message);
                    }
                }
            }
        }

        /// <summary>
        /// Cleans up the weak references to garbage collected objects from the registration lists.
        /// </summary>
        private void Cleanup()
        {
            CleanupList(recipientsOfSubclassesAction);
            CleanupList(recipientsStrictAction);
        }

        /// <summary>
        /// Sends a specified message.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="message">The message.</param>
        /// <param name="messageTargetType">Type of the message recipient.</param>
        /// <param name="token">The token.</param>
        private void SendToTargetOrType<TMessage>(TMessage message, Type messageTargetType, object token)
        {
            Type messageType = typeof(TMessage);

            if (recipientsOfSubclassesAction != null)
            {
                // Clone to protect from people registering in a "receive message" method                
                List<Type> listClone = recipientsOfSubclassesAction.Keys.ToList();
                foreach (Type type in listClone)
                {
                    if (messageType == type || messageType.IsSubclassOf(type) || Implements(messageType, type))
                    {
                        List<WeakActionAndToken> list = recipientsOfSubclassesAction[type];
                        SendToList(message, list, messageTargetType, token);
                    }
                }
            }

            if (recipientsStrictAction != null)
            {
                if (recipientsStrictAction.ContainsKey(messageType))
                {
                    List<WeakActionAndToken> list = recipientsStrictAction[messageType];
                    SendToList(message, list, messageTargetType, token);
                }
            }

            Cleanup();
        }

        #region Nested type: WeakActionAndToken

        /// <summary>
        /// Stores a weak reference to a handler action and its associated registration token.
        /// </summary>
        private struct WeakActionAndToken
        {
            /// <summary>
            /// The weak reference to an action.
            /// </summary>
            public WeakDelegate Action;

            /// <summary>
            /// The associated registration token.
            /// </summary>
            public object Token;
        }

        #endregion
    }
}