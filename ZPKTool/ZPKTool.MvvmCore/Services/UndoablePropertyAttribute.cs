﻿using System;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Specifies that the view-model property supports the undo function.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class UndoablePropertyAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the name of the inner property name.
        /// </summary>
        public object PropertyInstance { get; set; }

        /// <summary>
        /// Gets or sets the name of the inner property name.
        /// </summary>
        public string InnerProperty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the undoable property is selectable or not.
        /// </summary>
        public bool IsUnselectable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the consecutive changes performed on the property are performed at once or not.
        /// </summary>
        public bool GroupConsecutiveChanges { get; set; }
    }
}