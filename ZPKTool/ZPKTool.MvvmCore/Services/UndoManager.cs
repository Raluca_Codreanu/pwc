﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using ZPKTool.Common;

    /// <summary>
    /// Implementation of the Undo service, responsible for the undo command functionality.
    /// </summary>
    public class UndoManager : IUndoManager
    {
        #region Attributes

        /// <summary>
        /// The list of properties that supports the undo function.
        /// </summary>
        private List<UndoInstanceProperty> undoableProperties = new List<UndoInstanceProperty>();

        /// <summary>
        /// The registered instances for which the undo command is working.
        /// </summary>
        private List<object> undoableInstances = new List<object>();

        /// <summary>
        /// The list of properties that can not be selected when undo is performed.
        /// </summary>
        private List<UndoInstanceProperty> unselectableProperties = new List<UndoInstanceProperty>();

        /// <summary>
        /// The list of properties for which the undo action is executed at every change. 
        /// ( The normal behavior is to group several consecutive changes of a property in a single undo action. )
        /// </summary>
        private List<UndoInstanceProperty> undoEachChangeProperties = new List<UndoInstanceProperty>();

        /// <summary>
        /// The undo list.
        /// </summary>
        private List<UndoableItem> undoStack = new List<UndoableItem>();

        /// <summary>
        /// The list of undo actions registered.
        /// </summary>
        private List<UndoActions> undoActions = new List<UndoActions>();

        /// <summary>
        /// The list of undo actions executed when changing a property in a particular instance.
        /// </summary>
        private List<UndoActions> instancesActions = new List<UndoActions>();

        /// <summary>
        /// The maximum size of the undo list.
        /// </summary>
        private int? maxUndoLimit;

        /// <summary>
        /// A value indicating whether the property value change is a result of an undo action performed or not.
        /// </summary>
        private bool isUndoAction;

        /// <summary>
        /// A value indicating weather the undo manager is listening for undoable actions.
        /// </summary>
        private bool isRunning = true;

        /// <summary>
        /// A value indicating whether the UndoManager is started or not.
        /// </summary>
        private bool isStarted;

        /// <summary>
        /// A value indicating the number of pauses executed.
        /// </summary>
        private int pauseCounts;

        /// <summary>
        /// A value indicating whether the current undo operation is part of a sequence of undo actions or not.
        /// </summary>
        private bool isSequenceOfActions;

        /// <summary>
        /// The current sequence of undo items.
        /// </summary>
        private List<UndoableItem> currentActionsSequences = new List<UndoableItem>();

        #endregion Attributes

        #region Events

        /// <summary>
        /// Occurs when the an Undo operation has been performed.
        /// </summary>
        public event EventHandler<UndoPerformedEventArgs> UndoPerformed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating the maximum size of the undo list.
        /// </summary>
        public int? MaxUndoLimit
        {
            get
            {
                return maxUndoLimit;
            }

            set
            {
                if (value.HasValue && value.Value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                maxUndoLimit = value;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Actions can only be undone if there are items in the <see cref="UndoStack"/>.
        /// </summary>
        /// <returns>True -if the action can be undone / False - otherwise.</returns>
        public bool CanUndo()
        {
            return this.undoStack.Count > 0;
        }

        /// <summary>
        /// Undo the last view-model change.
        /// </summary>
        public void Undo()
        {
            this.isUndoAction = true;
            this.UndoLastChange();
            this.isUndoAction = false;
        }

        /// <summary>
        /// Clear all items from the undo list.
        /// </summary>
        public void Reset()
        {
            this.undoStack.Clear();
        }

        /// <summary>
        /// Inserts an object at the top of the undoStack.
        /// </summary>
        /// <param name="item">The undoable item added.</param>
        public void Push(UndoableItem item)
        {
            if (!this.isStarted || !this.isRunning)
            {
                return;
            }

            if (this.isSequenceOfActions)
            {
                if (this.currentActionsSequences.Count == 0)
                {
                    this.currentActionsSequences.Add(new UndoableItem(null, UndoActionType.SequenceOfActions));
                }

                this.currentActionsSequences.Last().ItemsSequence.Add(item);
            }
            else
            {
                this.undoStack.Add(item);
                this.RefreshUndoStack();
            }
        }

        /// <summary>
        /// Removes the object at the top of the Stack.
        /// </summary>
        public void Pop()
        {
            if (this.undoStack.Count > 0)
            {
                this.undoStack.RemoveAt(undoStack.Count - 1);
            }
        }

        /// <summary>
        /// Get the undo stack count.
        /// </summary>
        /// <returns>The undo stack count.</returns>
        public int UndoItemsCount()
        {
            return this.undoStack.Count;
        }

        /// <summary>
        /// Start tracking an object properties changes, object for which the undo command is activated.
        /// </summary>
        /// <typeparam name="T">The type of instance this is.</typeparam>
        /// <param name="instance">The instance this undo item applies to.</param>
        public void TrackObject<T>(T instance) where T : INotifyPropertyChanging, INotifyPropertyChanged
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance", "The UndoManager instance was null.");
            }

            // If the instance was already added do nothing.
            if (this.undoableInstances.Contains(instance))
            {
                return;
            }

            this.undoableInstances.Add(instance);

            // Get all the undoable properties for the current instance.
            var undoableProps =
                instance.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .Where(prop => prop.IsDefined(typeof(UndoablePropertyAttribute), false));

            foreach (var undoableProp in undoableProps)
            {
                this.undoableProperties.Add(new UndoInstanceProperty()
                {
                    PropertyName = undoableProp.Name,
                    PropertyInstance = instance
                });

                var undoablePropAttr = undoableProp.GetCustomAttributes(typeof(UndoablePropertyAttribute), true).FirstOrDefault() as UndoablePropertyAttribute;
                if (undoablePropAttr != null && !undoablePropAttr.GroupConsecutiveChanges)
                {
                    this.undoEachChangeProperties.Add(new UndoInstanceProperty()
                    {
                        PropertyName = undoableProp.Name,
                        PropertyInstance = instance
                    });
                }

                if (undoableProp.PropertyType.IsGenericType)
                {
                    var genericTypeDef = undoableProp.PropertyType.GetGenericTypeDefinition();
                    if (genericTypeDef == typeof(DataProperty<>)
                        || genericTypeDef == typeof(VMProperty<>))
                    {
                        var undoablePropValue = undoableProp.GetValue(instance, null) as INotifyPropertyChanging;
                        if (undoablePropValue != null)
                        {
                            var prop = undoableProp;
                            undoablePropValue.PropertyChanging += (s, e) => this.OnUndoablePropertyChanging(
                                new UndoableItem
                                {
                                    ActionName = "Value",
                                    ActionParentName = prop.Name,
                                    ViewModelInstance = instance
                                },
                                undoablePropValue);
                        }
                    }
                }
            }

            instance.PropertyChanging += OnPropertyChanging;
            instance.PropertyChanged += OnPropertyChanged;
        }

        /// <summary>
        /// Stop tracking the changes for the object passed as parameter, object for which the undo command is activated.
        /// </summary>
        /// <typeparam name="T">The type of instance this is.</typeparam>
        /// <param name="instance">The instance this undo item applies to.</param>
        public void StopTrackingObject<T>(T instance) where T : INotifyPropertyChanging, INotifyPropertyChanged
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance", "The UndoManager instance was null.");
            }

            // If the instance was not already added do nothing.
            if (!this.undoableInstances.Contains(instance))
            {
                return;
            }

            this.undoableInstances.Remove(instance);

            this.undoableProperties.RemoveAll(p => p.PropertyInstance == (object)instance);
            this.undoEachChangeProperties.RemoveAll(p => p.PropertyInstance == (object)instance);

            instance.PropertyChanging -= OnPropertyChanging;
            instance.PropertyChanged -= OnPropertyChanged;
        }

        /// <summary>
        /// Make a property undoable in the current undo service.
        /// </summary>
        /// <param name="propertyExpression">The property.</param>
        /// <param name="propertyInstance">The property instance.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        public void RegisterProperty(Expression<Func<object>> propertyExpression, object propertyInstance)
        {
            var registeredProp =
                this.undoableProperties.FirstOrDefault(
                    p => p.PropertyName == ReflectionUtils.GetPropertyName(propertyExpression) &&
                         p.PropertyInstance == propertyInstance);

            if (registeredProp == null)
            {
                this.undoableProperties.Add(new UndoInstanceProperty()
                {
                    PropertyName = ReflectionUtils.GetPropertyName(propertyExpression),
                    PropertyInstance = propertyInstance
                });
            }
        }

        /// <summary>
        /// Un-register a undoable property.
        /// </summary>
        /// <param name="propertyExpression">The property.</param>
        /// <param name="propertyInstance">The property instance.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        public void UnregisterProperty(Expression<Func<object>> propertyExpression, object propertyInstance)
        {
            var registeredProp =
                this.undoableProperties.FirstOrDefault(
                    p => p.PropertyName == ReflectionUtils.GetPropertyName(propertyExpression) &&
                         p.PropertyInstance == propertyInstance);

            if (registeredProp != null)
            {
                this.undoableProperties.Remove(registeredProp);
            }
        }

        /// <summary>
        /// Register an action to a specific property so that it is executed when Undo is performed.
        /// </summary>
        /// <param name="propertyExpression">The property</param>
        /// <param name="action">The action.</param>
        /// <param name="instance">The property instance.</param>
        /// <param name="isUndoHandledExternal">A bool value indicating whether the Undo is handled external or not.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        public void RegisterPropertyAction(Expression<Func<object>> propertyExpression, Action<UndoableItem> action, object instance, bool isUndoHandledExternal = false)
        {
            var newUndoAction = new UndoActions()
            {
                UndoAction = action,
                PropName = ReflectionUtils.GetPropertyName(propertyExpression),
                Instance = instance,
                IsUndoHandledExternal = isUndoHandledExternal
            };

            this.undoActions.Add(newUndoAction);
        }

        /// <summary>
        /// Register an action to a specific instance so that it is executed when Undo is performed on a property from that instance.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="instance">The property instance.</param>
        /// <param name="isUndoHandledExternal">A bool value indicating whether the Undo is handled external or not.</param>
        public void RegisterInstanceAction(Action<UndoableItem> action, object instance, bool isUndoHandledExternal = false)
        {
            var newInstanceAction = new UndoActions() { UndoAction = action, Instance = instance, IsUndoHandledExternal = isUndoHandledExternal };
            this.instancesActions.Add(newInstanceAction);
        }

        /// <summary>
        /// Start the undo manager listening for changes process.
        /// </summary>
        public void Start()
        {
            if (!this.isStarted)
            {
                this.isStarted = true;
            }
        }

        /// <summary>
        /// Pause the undo manager listening for changes process.
        /// </summary>
        /// <returns>The ResumeUndoHelper instance used to resume properly the UndoManager.</returns> 
        public IDisposable Pause()
        {
            this.pauseCounts++;
            if (this.isStarted)
            {
                this.isRunning = false;
            }

            return new ResumeUndoHelper(this);
        }

        /// <summary>
        /// Resume the undo manager listening for changes process.
        /// </summary>
        public void Resume()
        {
            if (this.pauseCounts > 0)
            {
                this.pauseCounts--;
            }

            if (this.isStarted && this.pauseCounts == 0)
            {
                this.isRunning = true;
            }
        }

        /// <summary>
        /// Start a new undo items sequence.
        /// </summary>
        /// <param name="includePreviousItem">Include the previous item in the new items sequence.</param>
        /// <param name="reverseUndoOrder">Reverse the order in which the sequence items are iterated.</param>
        /// <param name="navigateToBatchControls">A bool value indicating whether to navigate to the undoable item control and select it or not.</param>
        /// <param name="undoEachBatch">A bool value indicating whether to group consecutive changes of this entity or not.</param>
        /// <returns>The StopBatchHelper instance used to end properly the started batch sequence.</returns>
        public IDisposable StartBatch(bool includePreviousItem = false, bool reverseUndoOrder = false, bool navigateToBatchControls = false, bool undoEachBatch = false)
        {
            // Don't start a batch if this is a result of an undo action or if the UndoManager has not been started or is paused.
            if (this.isUndoAction || !this.isStarted || !this.isRunning)
            {
                return new StopBatchHelper(this);
            }

            this.isSequenceOfActions = true;
            this.currentActionsSequences.Add(new UndoableItem(null, UndoActionType.SequenceOfActions)
            {
                ReverseOrder = reverseUndoOrder,
                NavigateToItem = navigateToBatchControls,
                NavigateEachTime = undoEachBatch
            });

            if (includePreviousItem && currentActionsSequences.Count == 1
                && this.undoStack.Count > 0)
            {
                var previousUndoableItem = this.undoStack.Last();
                this.undoStack.RemoveAt(undoStack.Count - 1);
                this.currentActionsSequences.Last().ItemsSequence.Add(previousUndoableItem);
            }

            return new StopBatchHelper(this);
        }

        /// <summary>
        /// End the items sequence.
        /// </summary>
        public void EndBatch()
        {
            // Do nothing if the UndoManager has not been started or is paused.
            if (!this.isStarted || !this.isRunning)
            {
                return;
            }

            if (this.currentActionsSequences.Count == 0 || this.isUndoAction)
            {
                this.currentActionsSequences.Clear();
                this.isSequenceOfActions = false;
                return;
            }

            // Remove the ended batch from the current sequences.
            var lastSequence = this.currentActionsSequences.Last();
            this.currentActionsSequences.RemoveAt(this.currentActionsSequences.Count - 1);

            if (this.currentActionsSequences.Count > 0)
            {
                // If the batch sequence has no items don't add it to the undo stack list.
                if (lastSequence.ActionType != UndoActionType.SequenceOfActions || lastSequence.ItemsSequence.Count != 0)
                {
                    // If there are sequences initiated before the one just ended, add ended sequence as the last element in the sequence that precedes it.
                    this.currentActionsSequences.Last().ItemsSequence.Add(lastSequence);

                    // Set NavigateToItem to false, because navigation will be done by the parent sequence.
                    lastSequence.NavigateToItem = false;
                }
            }
            else
            {
                // Add the sequence in Undo stack.
                this.isSequenceOfActions = false;
                if (lastSequence.ItemsSequence.Count > 0)
                {
                    // If the batch sequence has no items don't add it to the undo stack list.
                    if (lastSequence.ActionType != UndoActionType.SequenceOfActions || lastSequence.ItemsSequence.Count != 0)
                    {
                        this.Push(lastSequence);
                    }
                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Handles the PropertyChanging event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangingEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            // If UndoManager is not started, do nothing.
            if (!this.isStarted)
            {
                return;
            }

            var undoableProp =
                this.undoableProperties.FirstOrDefault(
                    p => p.PropertyName == e.PropertyName &&
                         p.PropertyInstance == sender);

            if (undoableProp != null)
            {
                this.OnUndoablePropertyChanging(new UndoableItem() { ActionName = e.PropertyName }, sender as INotifyPropertyChanging);
            }
        }

        /// <summary>
        /// Called when the Undoable property of an undoable instance has changed.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <param name="instance">The undoable instance.</param>
        private void OnUndoablePropertyChanging(UndoableItem undoableItem, INotifyPropertyChanging instance)
        {
            // If UndoManager is not started, do nothing.
            if (!this.isStarted)
            {
                return;
            }

            if (instance == null)
            {
                throw new ArgumentNullException("instance", "The undoable instance was null.");
            }

            var undoableProp =
                this.undoableProperties.FirstOrDefault(
                    p => (p.PropertyName == undoableItem.ActionParentName || p.PropertyName == undoableItem.ActionName) &&
                         (p.PropertyInstance == instance || p.PropertyInstance == undoableItem.ViewModelInstance));

            if (this.isUndoAction || undoableProp == null)
            {
                return;
            }

            var value = instance.GetType().GetProperty(undoableItem.ActionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).GetValue(instance, null);
            undoableItem.Values.Clear();
            undoableItem.Values.Add(value);
            undoableItem.Instance = instance;
            undoableItem.ActionType = UndoActionType.Update;

            this.Push(undoableItem);
        }

        /// <summary>
        /// Handles the PropertyChangedEventArgs event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var prop = sender.GetType().GetProperty(e.PropertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (prop == null || !prop.IsDefined(typeof(UndoablePropertyAttribute), false))
            {
                return;
            }

            var value = prop.GetValue(sender, null);
            var collectionPropValue = value as INotifyCollectionChanged;
            if (collectionPropValue != null)
            {
                collectionPropValue.CollectionChanged +=
                    (s, ev) =>
                        this.OnCollectionChanged(
                            new UndoableItem
                            {
                                ActionName = "Value",
                                ActionParentName = prop.Name,
                                Instance = s,
                                ViewModelInstance = sender
                            },
                            ev);
            }
        }

        /// <summary>
        /// Handles the NotifyCollectionChangedEventArgs event.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnCollectionChanged(UndoableItem undoableItem, NotifyCollectionChangedEventArgs e)
        {
            // If UndoManager is not started, do nothing.
            if (!this.isStarted)
            {
                return;
            }

            if (this.isUndoAction)
            {
                this.isUndoAction = false;
                return;
            }

            var actionType = UndoActionType.None;
            var action = string.Empty;
            var items = new List<object>();
            var newStartIndex = 0;
            var oldStartIndex = 0;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        actionType = UndoActionType.Delete;
                        action = "RemoveAt";
                        newStartIndex = e.NewStartingIndex;
                        items.AddRange(e.NewItems.Cast<object>());
                        break;
                    }

                case NotifyCollectionChangedAction.Remove:
                    {
                        actionType = UndoActionType.Insert;
                        action = "Insert";
                        oldStartIndex = e.OldStartingIndex;
                        items.AddRange(e.OldItems.Cast<object>());
                        break;
                    }

                case NotifyCollectionChangedAction.Move:
                    {
                        actionType = UndoActionType.Move;
                        action = "Move";
                        newStartIndex = e.NewStartingIndex;
                        oldStartIndex = e.OldStartingIndex;
                        break;
                    }

                default:
                    break;
            }

            undoableItem.Values.Clear();
            undoableItem.Values.AddRange(items);
            undoableItem.NewStartingIndex = newStartIndex;
            undoableItem.OldStartingIndex = oldStartIndex;
            undoableItem.ActionName = action;
            undoableItem.ActionType = actionType;
            this.Push(undoableItem);
        }

        /// <summary>
        /// Undo the last change from Undo stack.
        /// </summary>
        private void UndoLastChange()
        {
            var restoredUndoItem = this.GetUndoItemToRestore();
            if (!this.ExecuteUndo(restoredUndoItem))
            {
                return;
            }

            this.SelectUndoItem(restoredUndoItem);
        }

        /// <summary>
        /// Determines the item to be selected when undo is performed.
        /// </summary>
        /// <param name="undoableItem">The undo item on which undo has been performed.</param>
        private void SelectUndoItem(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType == UndoActionType.SequenceOfActions
                && undoableItem.ItemsSequence.Count > 0)
            {
                var sequenceSelected = false;
                foreach (var item in undoableItem.ItemsSequence)
                {
                    switch (item.ActionType)
                    {
                        case UndoActionType.SequenceOfActions:
                            {
                                if (undoableItem.ItemsSequence.Count > 0)
                                {
                                    this.SelectUndoItem(item);
                                }

                                break;
                            }

                        default:
                            {
                                if (!sequenceSelected
                                    && this.unselectableProperties.FirstOrDefault(p => p.PropertyName == item.ActionParentName && p.PropertyInstance == item.ViewModelInstance) == null)
                                {
                                    this.OnUndo(item.Instance, item, undoableItem.NavigateToItem);
                                    sequenceSelected = true;
                                }
                                else
                                {
                                    this.OnUndo(item.Instance, item, false);
                                }

                                break;
                            }
                    }
                }
            }
            else
            {
                this.OnUndo(
                    undoableItem.Instance,
                    undoableItem,
                    this.unselectableProperties.FirstOrDefault(p => p.PropertyName == undoableItem.ActionParentName && p.PropertyInstance == undoableItem.ViewModelInstance) == null);
            }
        }

        /// <summary>
        /// Get the undo item from the undo stack that need to be restored.
        /// This item is either the last undo item from the undo stack or the first consecutive element of the same type from undo stack, 
        /// where the item is suitable for grouping items in a row when undo is performed.
        /// </summary>
        /// <returns>The undo item that needs to be restored.</returns>
        private UndoableItem GetUndoItemToRestore()
        {
            var lastUndoItem = this.undoStack.Last();
            this.undoStack.RemoveAt(undoStack.Count - 1);

            var lastValidUndoItem = lastUndoItem;
            var nextUndoItem = this.undoStack.LastOrDefault();
            if (nextUndoItem == null || lastUndoItem.NavigateEachTime)
            {
                return lastUndoItem;
            }

            while (this.AreEquals(lastUndoItem, nextUndoItem))
            {
                lastValidUndoItem = nextUndoItem;
                this.undoStack.Remove(nextUndoItem);
                nextUndoItem = this.undoStack.LastOrDefault();
            }

            return lastValidUndoItem;
        }

        /// <summary>
        /// Check if two undo items are of the same type.
        /// </summary>
        /// <param name="firstItem">The first undo item.</param>
        /// <param name="secondItem">The second undo item.</param>
        /// <returns>True - if items are of the same type / False - otherwise.</returns>
        private bool AreEquals(UndoableItem firstItem, UndoableItem secondItem)
        {
            if (firstItem == null || secondItem == null)
            {
                return false;
            }

            if (firstItem.ActionType == UndoActionType.SequenceOfActions
                && firstItem.ActionType == secondItem.ActionType
                && firstItem.ItemsSequence.Count > 0 && secondItem.ItemsSequence.Count > 0)
            {
                return this.AreEquals(firstItem.ItemsSequence.FirstOrDefault(), secondItem.ItemsSequence.FirstOrDefault());
            }

            var undoEachChangeProp = this.undoEachChangeProperties.FirstOrDefault(p => (p.PropertyName == firstItem.ActionName || p.PropertyName == firstItem.ActionParentName)
                && (p.PropertyInstance == firstItem.ViewModelInstance || p.PropertyInstance == firstItem.Instance));
            if (undoEachChangeProp != null || firstItem.ActionType == UndoActionType.UndoManager)
            {
                return false;
            }

            return !firstItem.NavigateEachTime
                   && firstItem.ActionName == secondItem.ActionName
                   && firstItem.ActionParentName == secondItem.ActionParentName
                   && ((firstItem.ViewModelInstance == null && firstItem.Instance != null && firstItem.Instance == secondItem.Instance)
                    || (firstItem.ViewModelInstance != null && firstItem.ViewModelInstance == secondItem.ViewModelInstance));
        }

        /// <summary>
        /// The method that fires the UndoPerformed event.
        /// </summary>
        /// <param name="instance">The undo instance.</param>
        /// <param name="item">The undoable item.</param>
        /// <param name="navigateToItem">A value indicating whether to navigate to the undoable item or not.</param>
        private void OnUndo(object instance, UndoableItem item, bool navigateToItem = true)
        {
            var action = this.UndoPerformed;
            if (action != null)
            {
                action.Invoke(this, new UndoPerformedEventArgs(instance, item, navigateToItem));
            }
        }

        /// <summary>
        /// Ensure that the undo list size does not get bigger than <see cref="MaxUndoLimit"/>.
        /// </summary>
        private void RefreshUndoStack()
        {
            if (MaxUndoLimit.HasValue && MaxUndoLimit.Value > 0)
            {
                while (MaxUndoLimit.Value < this.undoStack.Count)
                {
                    this.undoStack.RemoveAt(0);
                }
            }
        }

        #endregion Private Methods

        #region Undo algorithm

        /// <summary>
        /// Execute the undo operation.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <returns>True - If the operation was completed successfully / False - otherwise.</returns>        
        private bool ExecuteUndo(UndoableItem undoableItem)
        {
            if (undoableItem.Instance == null && undoableItem.ActionType != UndoActionType.SequenceOfActions)
            {
                return false;
            }

            switch (undoableItem.ActionType)
            {
                case UndoActionType.Update:
                    {
                        this.ExecuteUndoForUpdateActionType(undoableItem);
                        break;
                    }

                case UndoActionType.Insert:
                    {
                        this.ExecuteUndoForInsertAction(undoableItem);
                        break;
                    }

                case UndoActionType.Delete:
                    {
                        this.ExecuteUndoForDeleteActionType(undoableItem);
                        break;
                    }

                case UndoActionType.Move:
                    {
                        this.ExecuteUndoForMoveActionType(undoableItem);
                        break;
                    }

                case UndoActionType.SequenceOfActions:
                    {
                        this.ExecuteUndoForSequenceOfActionsActionType(undoableItem);
                        break;
                    }

                case UndoActionType.UndoManager:
                    {
                        this.ExecuteUndoForUndoManagerActionType(undoableItem);
                        break;
                    }

                default:
                    break;
            }

            return true;
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.Update" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForUpdateActionType(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.Update)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            this.isUndoAction = true;
            try
            {
                var propertyValue = undoableItem.Values.First();
                var undoActionItem = this.undoActions.FirstOrDefault(p => (p.PropName == undoableItem.ActionName && p.Instance == undoableItem.Instance) 
                    || (p.PropName == undoableItem.ActionParentName && p.Instance == undoableItem.ViewModelInstance));
                if (undoActionItem != null)
                {
                    // Execute the attached action for the property on which is executed undo.
                    undoActionItem.PerformAction(undoableItem);
                }

                var instanceActionItem = this.instancesActions.FirstOrDefault(p => p.Instance == undoableItem.ViewModelInstance || p.Instance == undoableItem.Instance);
                if (instanceActionItem != null)
                {
                    // Execute the attached action for the instance that contains the property on which is executed undo.
                    instanceActionItem.PerformAction(undoableItem);
                }

                var property = undoableItem.Instance.GetType().GetProperty(undoableItem.ActionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                property.SetValue(undoableItem.Instance, propertyValue, null);
            }
            finally
            {
                this.isUndoAction = false;
            }
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.Insert" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForInsertAction(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.Insert)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            try
            {
                this.isUndoAction = true;

                foreach (var item in undoableItem.Values)
                {
                    var collection = undoableItem.Instance as ICollection;
                    if (collection != null)
                    {
                        var instanceActionItem = this.instancesActions.FirstOrDefault(p => p.Instance == undoableItem.ViewModelInstance);
                        if (instanceActionItem != null)
                        {
                            // Execute the attached action for the instance that contains the property on which is executed undo.
                            instanceActionItem.PerformAction(undoableItem);
                        }

                        var undoActionItem = this.undoActions.FirstOrDefault(p => p.PropName == undoableItem.ActionParentName && p.Instance == undoableItem.ViewModelInstance);
                        if (undoActionItem != null)
                        {
                            // Execute the attached action for the property on which is executed undo.
                            undoActionItem.PerformAction(undoableItem);

                            // If Undo is handled external don't undo the property value.
                            if (!undoActionItem.IsUndoHandledExternal)
                            {
                                var method = collection.GetType().GetMethod(undoableItem.ActionName);
                                method.Invoke(collection, new[] { undoableItem.OldStartingIndex, item });
                            }
                        }
                        else
                        {
                            var method = collection.GetType().GetMethod(undoableItem.ActionName);
                            method.Invoke(collection, new[] { undoableItem.OldStartingIndex, item });
                        }
                    }
                }
            }
            finally
            {
                this.isUndoAction = false;
            }
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.Delete" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForDeleteActionType(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.Delete)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            try
            {
                this.isUndoAction = true;

                var collection = undoableItem.Instance as ICollection;
                if (collection != null && collection.Count > undoableItem.NewStartingIndex)
                {
                    var instanceActionItem = this.instancesActions.FirstOrDefault(p => p.Instance == undoableItem.ViewModelInstance);
                    if (instanceActionItem != null)
                    {
                        // Execute the attached action for the instance that contains the property on which is executed undo.
                        instanceActionItem.PerformAction(undoableItem);
                    }

                    var undoActionItem = this.undoActions.FirstOrDefault(p => p.PropName == undoableItem.ActionParentName && p.Instance == undoableItem.ViewModelInstance);
                    if (undoActionItem != null)
                    {
                        // Execute the attached action for the property on which is executed undo.
                        undoActionItem.PerformAction(undoableItem);

                        // If Undo is handled external don't undo the property value.
                        if (!undoActionItem.IsUndoHandledExternal)
                        {
                            var method = collection.GetType().GetMethod(undoableItem.ActionName, new Type[] { typeof(int) });
                            method.Invoke(collection, new object[] { undoableItem.NewStartingIndex });
                        }
                    }
                    else
                    {
                        var method = collection.GetType().GetMethod(undoableItem.ActionName, new Type[] { typeof(int) });
                        method.Invoke(collection, new object[] { undoableItem.NewStartingIndex });
                    }
                }
            }
            finally
            {
                this.isUndoAction = false;
            }
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.Move" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForMoveActionType(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.Move)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            this.isUndoAction = true;
            try
            {
                var collection = undoableItem.Instance as ICollection;
                if (collection != null)
                {
                    var instanceActionItem = this.instancesActions.FirstOrDefault(p => p.Instance == undoableItem.ViewModelInstance);
                    if (instanceActionItem != null)
                    {
                        // Execute the attached action for the instance that contains the property on which is executed undo.
                        instanceActionItem.PerformAction(undoableItem);
                    }

                    var undoActionItem = this.undoActions.FirstOrDefault(p => p.PropName == undoableItem.ActionParentName && p.Instance == undoableItem.ViewModelInstance);
                    if (undoActionItem != null)
                    {
                        // Execute the attached action for the property on which is executed undo.
                        undoActionItem.PerformAction(undoableItem);

                        // If Undo is handled external don't undo the property value.
                        if (!undoActionItem.IsUndoHandledExternal)
                        {
                            var method = collection.GetType().GetMethod(undoableItem.ActionName, new Type[] { typeof(int), typeof(int) });
                            method.Invoke(collection, new object[] { undoableItem.NewStartingIndex, undoableItem.OldStartingIndex });
                        }
                    }
                    else
                    {
                        var method = collection.GetType().GetMethod(undoableItem.ActionName, new Type[] { typeof(int), typeof(int) });
                        method.Invoke(collection, new object[] { undoableItem.NewStartingIndex, undoableItem.OldStartingIndex });
                    }
                }
            }
            finally
            {
                this.isUndoAction = false;
            }
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.SequenceOfActions" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForSequenceOfActionsActionType(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.SequenceOfActions)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            if (undoableItem.ReverseOrder)
            {
                foreach (var item in undoableItem.ItemsSequence)
                {
                    if (undoableItem.ViewModelInstance != null)
                    {
                        item.ViewModelInstance = undoableItem.ViewModelInstance;
                    }

                    this.ExecuteUndo(item);
                }
            }
            else
            {
                var itemsList = undoableItem.ItemsSequence.Reverse();
                foreach (var item in itemsList)
                {
                    if (undoableItem.ViewModelInstance != null)
                    {
                        item.ViewModelInstance = undoableItem.ViewModelInstance;
                    }

                    this.ExecuteUndo(item);
                }
            }
        }

        /// <summary>
        /// Executes the undo operation of an undoable item having the action type <see cref="UndoActionType.UndoManager" />.
        /// </summary>
        /// <param name="undoableItem">The undoable item.</param>
        /// <exception cref="System.InvalidOperationException">Wrong undoable item action type.</exception>
        private void ExecuteUndoForUndoManagerActionType(UndoableItem undoableItem)
        {
            if (undoableItem.ActionType != UndoActionType.UndoManager)
            {
                throw new InvalidOperationException("Wrong undoable item action type.");
            }

            var undoManager = undoableItem.Instance as IUndoManager;
            if (undoManager != null)
            {
                if (undoManager.CanUndo())
                {
                    var instanceActionItem = this.instancesActions.FirstOrDefault(p => p.Instance == undoManager);
                    if (instanceActionItem != null)
                    {
                        // Execute the attached action for the instance that contains the property on which is executed undo.
                        instanceActionItem.PerformAction(undoableItem);
                    }

                    undoManager.Undo();
                }
            }
        }

        #endregion Undo algorithm

        #region Nested Classes

        /// <summary>
        /// Class used to ensure the successful closing of a batch sequence of operations performed UndoManager.
        /// </summary>
        private class StopBatchHelper : IDisposable
        {
            /// <summary>
            /// The UndoManager for which run this batch.
            /// </summary>
            private UndoManager undoManager;

            /// <summary>
            /// Initializes a new instance of the <see cref="StopBatchHelper" /> class.
            /// </summary>
            /// <param name="undoMngr">The undo manager.</param>
            public StopBatchHelper(UndoManager undoMngr)
            {
                this.undoManager = undoMngr;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                this.undoManager.EndBatch();
            }
        }

        /// <summary>
        /// Class used to ensure the successful resume of a Pause action performed on UndoManager.
        /// </summary>
        private class ResumeUndoHelper : IDisposable
        {
            /// <summary>
            /// The UndoManager for which the Pause command has been performed.
            /// </summary>
            private UndoManager undoManager;

            /// <summary>
            /// Initializes a new instance of the <see cref="ResumeUndoHelper" /> class.
            /// </summary>
            /// <param name="undoMngr">The undo manager.</param>
            public ResumeUndoHelper(UndoManager undoMngr)
            {
                this.undoManager = undoMngr;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                this.undoManager.Resume();
            }
        }

        /// <summary>
        /// Class used to save data about properties. It contains the name of the property and the object instance to which the property belongs.
        /// </summary>
        private class UndoInstanceProperty
        {
            /// <summary>
            /// Gets or sets the property name.
            /// </summary>
            public string PropertyName { get; set; }

            /// <summary>
            /// Gets or sets the property instance object.
            /// </summary>
            public object PropertyInstance { get; set; }
        }

        #endregion
    }
}