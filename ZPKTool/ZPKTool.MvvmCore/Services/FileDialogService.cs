﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Win32;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// WPF implementation of the service that allows to open or save files.
    /// </summary>
    [Export(typeof(IFileDialogService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FileDialogServiceWpf : IFileDialogService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileDialogServiceWpf"/> class.
        /// </summary>
        public FileDialogServiceWpf()
        {
            this.FileNames = new List<string>();
        }

        /// <summary>
        /// Gets or sets a string containing the full path of the file selected in a file dialog.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets an array that contains one file name for each selected file.
        /// </summary>
        /// <value>
        /// A collection of System.String that contains one file name for each selected file.
        /// </value>
        public ICollection<string> FileNames { get; private set; }

        /// <summary>
        /// Gets or sets the filter string that determines what types of files are displayed in a file dialog.
        /// </summary>
        /// <value>
        /// A System.String that contains the filter.
        /// The default is System.String.Empty, which means that no filter is applied and all file types are displayed.
        /// </value>
        /// <exception cref="System.ArgumentException">The filter string is invalid.</exception>
        public string Filter { get; set; }

        /// <summary>
        /// Gets or sets the index of the filter currently selected in a file dialog.
        /// </summary>
        public int FilterIndex { get; set; }

        /// <summary>
        /// Gets or sets the initial directory that is displayed by a file dialog.
        /// </summary>
        public string InitialDirectory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the OpenFileDialog allows users to select multiple files.
        /// </summary>
        /// <value>
        /// true if multiple selections are allowed; otherwise, false. The default is false.
        /// </value>
        public bool Multiselect { get; set; }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file or files that should be opened.
        /// </summary>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        public bool? ShowOpenFileDialog()
        {
            return InternalShowOpenFileDialog(null);
        }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file or files that should be opened.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        public bool? ShowOpenFileDialog(Window owner)
        {
            return InternalShowOpenFileDialog(owner);
        }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file path where to save a file.
        /// </summary>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        public bool? ShowSaveFileDialog()
        {
            return InternalShowSaveFileDialog(null);
        }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file path where to save a file.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        public bool? ShowSaveFileDialog(Window owner)
        {
            return InternalShowSaveFileDialog(owner);
        }

        /// <summary>
        /// Resets all properties to their default values.
        /// </summary>
        public void Reset()
        {
            this.FileName = string.Empty;
            this.FileNames.Clear();
            this.Filter = string.Empty;
            this.FilterIndex = 0;
            this.InitialDirectory = string.Empty;
            this.Multiselect = false;
        }

        /// <summary>
        /// Initializes the properties of a dialog using the corresponding properties of this instance.
        /// </summary>
        /// <param name="dialog">The dialog to initialize.</param>
        private void InitializeDialogProperties(FileDialog dialog)
        {
            dialog.FileName = this.FileName;
            dialog.Filter = this.Filter;
            dialog.FilterIndex = this.FilterIndex;
            dialog.InitialDirectory = this.InitialDirectory;
        }

        /// <summary>
        /// Copies the results from a dialog to this instance.
        /// </summary>
        /// <param name="dialog">The dialog from which to copy.</param>
        private void CopyResultsFromDialog(FileDialog dialog)
        {
            this.FileName = dialog.FileName;
            this.FilterIndex = dialog.FilterIndex;

            this.FileNames.Clear();
            foreach (var fileName in dialog.FileNames)
            {
                this.FileNames.Add(fileName);
            }
        }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file or files that should be opened.
        /// </summary>
        /// <param name="owner">The owner of the dialog. Can be null.</param>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        private bool? InternalShowOpenFileDialog(Window owner)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            InitializeDialogProperties(dialog);
            dialog.Multiselect = this.Multiselect;
            dialog.CheckFileExists = true;
            dialog.RestoreDirectory = true;

            bool? result = null;
            if (owner != null)
            {
                result = dialog.ShowDialog(owner);
            }
            else
            {
                result = dialog.ShowDialog();
            }

            CopyResultsFromDialog(dialog);
            return result;
        }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file path where to save a file.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        private bool? InternalShowSaveFileDialog(Window owner)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            InitializeDialogProperties(dialog);
            dialog.OverwritePrompt = true;
            dialog.RestoreDirectory = true;

            bool? result = null;
            if (owner != null)
            {
                result = dialog.ShowDialog(owner);
            }
            else
            {
                result = dialog.ShowDialog();
            }

            CopyResultsFromDialog(dialog);
            return result;
        }
    }
}
