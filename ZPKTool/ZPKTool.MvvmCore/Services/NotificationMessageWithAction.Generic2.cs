﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// A message class that allows to specify a callback that can be executed by the recipient when is done handling the message.
    /// The callback has two parameters.
    /// </summary>
    /// <typeparam name="TCallbackParam1">The type of the callback's 1st parameter.</typeparam>
    /// <typeparam name="TCallbackParam2">The type of the callback's 2nd parameter.</typeparam>
    public class NotificationMessageWithAction<TCallbackParam1, TCallbackParam2> : NotificationMessageWithCallback
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam1, TCallbackParam2&gt;"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s).</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(string notification, Action<TCallbackParam1, TCallbackParam2> callback)
            : base(notification, callback)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam1, TCallbackParam2&gt;"/> class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(object sender, string notification, Action<TCallbackParam1, TCallbackParam2> callback)
            : base(sender, notification, callback)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam1, TCallbackParam2&gt;"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="notification">The notification.</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(object sender, object target, string notification, Action<TCallbackParam1, TCallbackParam2> callback)
            : base(sender, target, notification, callback)
        {
        }
    }
}
