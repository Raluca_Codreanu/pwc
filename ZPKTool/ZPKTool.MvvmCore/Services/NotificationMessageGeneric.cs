﻿namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Passes a string message (Notification) and a generic value (Content) to a recipient.
    /// </summary>
    /// <typeparam name="T">The type of the message's content.</typeparam>
    public class NotificationMessage<T> : NotificationMessage
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessage&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        /// <param name="content">The message content.</param>
        public NotificationMessage(string notification, T content)
            : base(notification)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        /// <param name="content">The message content.</param>
        public NotificationMessage(object sender, string notification, T content)
            : base(sender, notification)
        {
            this.Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, amd may be null.</param>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        /// <param name="content">The message content.</param>
        public NotificationMessage(object sender, object target, string notification, T content)
            : base(sender, target, notification)
        {
            this.Content = content;
        }

        /// <summary>
        /// Gets the content of the message.
        /// </summary>
        public T Content { get; private set; }
    }
}