﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The interface of a service that allows to brows for folders.
    /// </summary>
    public interface IFolderBrowserService
    {
        /// <summary>
        /// Gets or sets the path selected by the user.
        /// </summary>        
        string SelectedPath { get; set; }

        /// <summary>
        /// Gets or sets the descriptive text displayed above the folders tree view in the dialog box.
        /// </summary>        
        string Description { get; set; }

        /// <summary>
        /// Shows the folder browser dialog that allows the user to select a folder.
        /// </summary>
        /// <returns>True if the user has selected a folder (clicked the OK button); otherwise false.</returns>
        bool ShowFolderBrowserDialog();        
    }
}
