﻿namespace ZPKTool.MvvmCore.Services
{
    using System;

    /// <summary>
    /// Specifies that the view-model property defines a sub-view-model.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class SubViewModelPropertyAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the model property associated name.
        /// </summary>
        public string ModelPropertyAssociated { get; set; }
    }
}
