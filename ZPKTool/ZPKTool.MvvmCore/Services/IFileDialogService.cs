﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The interface of a service that allows to open or save files.
    /// </summary>
    public interface IFileDialogService
    {
        /// <summary>
        /// Gets or sets a string containing the full path of the file selected in a file dialog.
        /// </summary>        
        string FileName { get; set; }

        /// <summary>
        /// Gets an array that contains one file name for each selected file.
        /// </summary>
        /// <value>
        /// A collection of System.String that contains one file name for each selected file.
        /// </value>
        ICollection<string> FileNames { get; }

        /// <summary>
        /// Gets or sets the filter string that determines what types of files are displayed in a file dialog.        
        /// </summary>
        /// <value>A System.String that contains the filter.
        /// The default is System.String.Empty, which means that no filter is applied and all file types are displayed.</value>
        /// <exception cref="System.ArgumentException">The filter string is invalid.</exception>
        string Filter { get; set; }

        /// <summary>
        /// Gets or sets the index of the filter currently selected in a file dialog.
        /// </summary>
        int FilterIndex { get; set; }

        /// <summary>
        /// Gets or sets the initial directory that is displayed by a file dialog.
        /// </summary>        
        string InitialDirectory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the OpenFileDialog allows users to select multiple files.
        /// </summary>
        /// <value>true if multiple selections are allowed; otherwise, false. The default is false.</value>
        bool Multiselect { get; set; }

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file or files that should be opened.
        /// </summary>
        /// <returns>true if the user clicked the OK button of the dialog; otherwise false.</returns>
        bool? ShowOpenFileDialog();

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file or files that should be opened.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>true if the user clicked the OK button of the dialog; otherwise false.</returns>
        bool? ShowOpenFileDialog(Window owner);

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file path where to save a file.
        /// </summary>
        /// <returns>true if the user clicked the OK button of the dialog; otherwise false.</returns>
        bool? ShowSaveFileDialog();

        /// <summary>
        /// Shows a file dialog box that allows the user to specify a file path where to save a file.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <returns>
        /// true if the user clicked the OK button of the dialog; otherwise false.
        /// </returns>
        bool? ShowSaveFileDialog(Window owner);

        /// <summary>
        /// Resets all properties to thier default values.
        /// </summary>
        void Reset();
    }
}
