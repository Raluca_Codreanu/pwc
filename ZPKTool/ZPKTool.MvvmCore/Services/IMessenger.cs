﻿/*************************************************************************************************************
 This code is a modified version of IMessenger.cs from MVVM Light Toolkit (http://mvvmlight.codeplex.com/)
*************************************************************************************************************/

using System;
using System.Diagnostics.CodeAnalysis;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The Messenger is a class that implements the Mediator pattern, allowing objects to exchange messages.
    /// </summary>    
    public interface IMessenger
    {
        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>        
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>        
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        void Register<TMessage>(Action<TMessage> action);

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>        
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        /// <param name="token">A token for a messaging channel. If an action is registered using a token it will receive only the messages sent using that token.
        /// The actions who were not registered using that token will not be invoked.</param>
        void Register<TMessage>(Action<TMessage> action, object token);

        /// <summary>
        /// Registers an action that will handle messages of a specified type.        
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>        
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>        
        /// <param name="receiveDerivedMessagesToo">If true, the action will be invoked also for message types deriving from TMessage (or implementing TMessage,
        /// if TMessage is an interface).
        /// <para>For example, if a SendOrderMessage and an ExecuteOrderMessage derive from OrderMessage (or implement it if its an interface),
        /// registering for OrderMessage and setting receiveDerivedMessagesToo to true will invoke the action for messages of both
        /// SendOrderMessage and ExecuteOrderMessage types.</para>
        /// </param>
        void Register<TMessage>(Action<TMessage> action, bool receiveDerivedMessagesToo);

        /// <summary>
        /// Registers an action that will handle messages of a specified type.
        /// <para>The registration uses weak references to avoid memory leaks.</para>
        /// </summary>
        /// <typeparam name="TMessage">The type of message handled by the action.</typeparam>
        /// <param name="action">The action that will be executed when a message of type TMessage is sent.</param>
        /// <param name="token">A token for a messaging channel. If an action is registered using a token it will receive only the messages sent using that token.
        /// The actions who were not registered using that token will not be invoked.</param>
        /// <param name="receiveDerivedMessagesToo">If true, the action will be invoked also for message types deriving from TMessage (or implementing TMessage,
        /// if TMessage is an interface).
        /// <para>For example, if a SendOrderMessage and an ExecuteOrderMessage derive from OrderMessage (or implement it if its an interface),
        /// registering for OrderMessage and setting receiveDerivedMessagesToo to true will invoke the action for messages of both
        /// SendOrderMessage and ExecuteOrderMessage types.</para>
        /// </param>      
        void Register<TMessage>(Action<TMessage> action, object token, bool receiveDerivedMessagesToo);

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        void Send<TMessage>(TMessage message);

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods, and that
        /// belong to an object with the type TTarget.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <typeparam name="TTarget">The type of target objects that will receive the message. The message won't be sent to target objects of another type.
        /// <para>The target object is the object on which the handler action is defined.</para></typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        [SuppressMessage(
            "Microsoft.Design",
            "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "This syntax is more convenient than other alternatives.")]
        void Send<TMessage, TTarget>(TMessage message);

        /// <summary>
        /// Sends a message to registered handler actions.
        /// The message will reach all handler actions that registered for this message type using one of the Register methods.
        /// </summary>
        /// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
        /// <param name="message">The message to send to registered recipients.</param>
        /// <param name="token">A token for a messaging channel. Only the recipients that registered with this token will receive the message.</param>
        void Send<TMessage>(TMessage message, object token);

        /// <summary>
        /// Unregisters all handler actions of an object for all message types. After this method is executed, the object will no longer receive any message.
        /// </summary>
        /// <param name="recipient">The object whose handlers must be unregistered.</param>
        void Unregister(object recipient);

        /// <summary>
        /// Unregisters all handler actions of an object for the specified message type. 
        /// The object will not receive messages of type TMessage anymore, but will still receive other message types for which it has registered.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the object's handlers must unregister from.</typeparam>
        /// <param name="recipient">The object whose handlers must be unregistered.</param>
        [SuppressMessage(
            "Microsoft.Design",
            "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "This syntax is more convenient than other alternatives.")]
        void Unregister<TMessage>(object recipient);

        /// <summary>
        /// Unregisters a specified handler action for a given type of messages. The handler will still be invoked for any other message types
        /// for which it has been registered.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the handler wants to unregister from.</typeparam>        
        /// <param name="action">The handler that must be unregistered for the message type TMessage.</param>
        void Unregister<TMessage>(Action<TMessage> action);

        /// <summary>
        /// Unregisters a specified handler action for a given type of messages and a specified token. The handler will still be invoked for any other message types
        /// for which it has been registered and for the TMessage type and other tokens.
        /// </summary>
        /// <typeparam name="TMessage">The type of messages that the handler wants to unregister from.</typeparam>        
        /// <param name="action">The handler that must be unregistered for the message type TMessage.</param>
        /// <param name="token">The token for which the handler must be unregistered.</param>
        void Unregister<TMessage>(Action<TMessage> action, object token);
    }
}