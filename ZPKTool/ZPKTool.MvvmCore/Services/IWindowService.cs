﻿using System;
using System.Windows;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// A service used to display windows and message boxes from view-models.
    /// </summary>
    public interface IWindowService
    {
        /// <summary>
        /// Gets or sets the source of the associated WindowServiceTrigger (if the trigger exists).
        /// The trigger should be defined in the corresponding view when using ShowViewIn... methods from the view-model
        /// so that the data templates defined in the view for the view-model can be discovered.
        /// </summary>        
        DependencyObject TriggerSource { get; set; }

        /// <summary>
        /// Gets a service that can be used to display message dialogs.
        /// </summary>
        IMessageDialogService MessageDialogService { get; }

        /// <summary>
        /// Shows the specified window.
        /// </summary>
        /// <param name="window">The window to show.</param>
        void ShowWindow(Window window);

        /// <summary>
        /// Shows the specified window as a modal dialog.
        /// </summary>
        /// <param name="window">The window to show.</param>
        /// <returns>A value that signifies how the dialog window was closed by the user.</returns>
        bool? ShowDialog(Window window);

        /// <summary>
        /// Shows the view associated with the specified view-model in a modeless window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model data template that defines the desired view. Should be set when you have in the same scope
        /// multiple templates for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        /// <param name="windowClosedAction">Represents the action performed when the window is closed. Defaults to null.</param>
        void ShowViewInWindow(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false, Action windowClosedAction = null);

        /// <summary>
        /// Shows the view associated with the specified view-model in a dialog window.
        /// </summary>
        /// <param name="viewModel">The view model whose view will be displayed in a window.</param>
        /// <param name="templateKey">The key of the view-model template that defines the desired view. Should be set when you have in the same scope multiple
        /// templates for the same view-model. If you have just one template it will be automatically found.</param>
        /// <param name="handleEscKey">Indicates whether to handle the escape key or not. Defaults to true.</param>
        /// <returns> A value that signifies how the dialog window was closed by the user. </returns>
        /// /// <param name="singleInstance">Indicates whether only a single instance of this type of window can exist. Defaults to false.</param>
        bool? ShowViewInDialog(object viewModel, object templateKey = null, bool handleEscKey = true, bool singleInstance = false);

        /// <summary>
        /// Closes the window containing the view of a specified view-model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>true if a window was found for the view and was closed; otherwise false.</returns>
        bool CloseViewWindow(object viewModel);

        /// <summary>
        /// Gets the view-model window.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>The window, coresponding to the view-model instance passed as parameter.</returns>
        Window GetViewModelWindow(object viewModel);
    }
}
