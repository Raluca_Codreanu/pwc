﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A message class that allows to specify a callback that can be executed by the recipient when is done handling the message.
    /// The callback has one parameter.
    /// </summary>
    /// <typeparam name="TCallbackParam">The type of the callback's parameter.</typeparam>
    public class NotificationMessageWithAction<TCallbackParam> : NotificationMessageWithCallback
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam&gt;"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s).</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(string notification, Action<TCallbackParam> callback)
            : base(notification, callback)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam&gt;"/> class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(object sender, string notification, Action<TCallbackParam> callback)
            : base(sender, notification, callback)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationMessageWithAction&lt;TCallbackParam&gt;"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="notification">The notification.</param>
        /// <param name="callback">The callback to be executed by the handler of the message.</param>
        public NotificationMessageWithAction(object sender, object target, string notification, Action<TCallbackParam> callback)
            : base(sender, target, notification, callback)
        {
        }
    }
}
