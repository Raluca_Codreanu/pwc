﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// This service shows messages to the user in a modal window.
    /// </summary>
    public interface IMessageDialogService
    {
        /// <summary>
        /// Shows a message dialog that displays to the user some information about the specified exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the dialog.
        /// </returns>
        MessageDialogResult Show(Exception e);

        /// <summary>
        /// Shows the specified message in a message dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type of the message dialog to display.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the dialog.
        /// </returns>
        MessageDialogResult Show(string message, MessageDialogType type);

        /// <summary>
        /// Shows a message dialog with custom content.
        /// </summary>
        /// <param name="dialogInfo">The message dialog information used to compute the window content</param>
        /// <returns>A value resulting from the interaction of the user with the dialog.</returns>
        MessageDialogResult Show(MessageDialogInfo dialogInfo);

        /// <summary>
        /// Shows the specified message in a message dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="additionalMessage">The additional message</param>
        /// <param name="type">The type of the message dialog to display.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the dialog.
        /// </returns>
        MessageDialogResult Show(string message, string additionalMessage, MessageDialogType type);

        /// <summary>
        /// Gets a user-friendly message for the specified exception.
        /// <para />
        /// (This is the method used to obtain the message displayed by the <see cref="Show(Exception e)"/> method.)
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>
        /// A string containing the message.
        /// </returns>
        string GetErrorMessage(Exception e);
    }
}
