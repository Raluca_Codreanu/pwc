﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Specifies the option(s) selected by the user while interacting with a message dialog.
    /// </summary>  
    public enum MessageDialogResult
    {
        /// <summary>
        /// No option chosen.
        /// </summary>
        None,

        /// <summary>
        /// OK was chosen.
        /// </summary>
        OK,

        /// <summary>
        /// Yes was chosen.
        /// </summary>
        Yes,

        /// <summary>
        /// No was chosen.
        /// </summary>
        No,

        /// <summary>
        /// Cancel was chosen.
        /// </summary>
        Cancel
    }
}
