﻿namespace ZPKTool.MvvmCore.Services
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class that keeps the state of a property before it change.
    /// </summary>
    public class UndoableItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UndoableItem"/> class.
        /// </summary>
        public UndoableItem()
        {
            this.Values = new List<object>();
            this.ItemsSequence = new List<UndoableItem>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoableItem"/> class.
        /// </summary>
        /// <param name="instance">The undoable instance.</param>
        /// <param name="actionType">The undo action type.</param>
        public UndoableItem(object instance, UndoActionType actionType)
            : this()
        {
            this.Instance = instance;
            this.ActionType = actionType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoableItem" /> class.
        /// </summary>
        /// <param name="instance">The undoable instance.</param>
        /// <param name="actionType">The undo action type.</param>
        /// <param name="values">The values ​​that will be restored when undo is performed.</param>
        /// <exception cref="System.ArgumentNullException">The values list was null.</exception>
        public UndoableItem(object instance, UndoActionType actionType, IEnumerable<object> values)
            : this(instance, actionType)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values", "The list of values was null.");
            }

            this.Values = new List<object>(values);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoableItem"/> class.
        /// <para />
        /// This constructor is for initializing an item that contains a sequence of undo operations.
        /// </summary>
        /// <param name="actionType">The undo action type.</param>
        /// <param name="itemsSequence">The items of an undo sequence.</param>
        public UndoableItem(UndoActionType actionType, IEnumerable<UndoableItem> itemsSequence)
            : this(null, actionType)
        {
            this.ItemsSequence = new List<UndoableItem>(itemsSequence);
        }

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating the undoable instance.
        /// </summary>
        public object Instance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the view model instance.
        /// </summary>
        public object ViewModelInstance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the undo action type (update / insert / delete).
        /// </summary>
        public UndoActionType ActionType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the property or method name, for which is executed the undo function.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the property or method name, for which is executed the undo function.
        /// </summary>
        public string ActionParentName { get; set; }

        /// <summary>
        /// Gets a list of values ​​will be restored when undo is performed.
        /// For a simple property - the list contains a single element that represents the property value before this change.
        /// For collections - the list contains the values ​​that have been added or removed from the collection.
        /// </summary>
        public IList<object> Values { get; private set; }

        /// <summary>
        /// Gets or sets the index at which a Move, Remove, or Replace action occurred in the collection.
        /// Used for collections.
        /// </summary>
        public int OldStartingIndex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the index at which the change occurred in the collection.
        /// Used for collections.
        /// </summary>
        public int NewStartingIndex { get; set; }

        /// <summary>
        /// Gets the undoable items from a undo sequence.
        /// </summary>
        public IList<UndoableItem> ItemsSequence { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the sequence order must be reversed or not.
        /// </summary>
        public bool ReverseOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to navigate to the undoable item or not.
        /// </summary>
        public bool NavigateToItem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to group consecutive changes of this entity or not.
        /// </summary>
        public bool NavigateEachTime { get; set; }

        #endregion Properties
    }
}
