﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    ///  A class representing the item used for a customizable message dialog
    ///  It's used to describe an item that can perform an action when pressed
    /// </summary>
    public class MessageDialogActionableItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDialogActionableItem"/> class.
        /// </summary>
        public MessageDialogActionableItem()
        {
            this.AutomationId = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDialogActionableItem"/> class.
        /// </summary>
        /// <param name="text">The text that will be displayed on item</param>
        /// <param name="action">The action performed by the item</param>
        /// <param name="automationId">The automation id of the item</param>
        /// <param name="type">The type of the item</param>
        public MessageDialogActionableItem(string text, Action action, string automationId, MessageDialogActionableItemType type)
        {
            this.Text = text;
            this.Action = action;
            this.AutomationId = automationId != null ? automationId : string.Empty;
            this.Type = type;
        }

        /// <summary>
        /// Gets or sets the text that will be displayed on item
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the action performed by the item
        /// </summary>
        public Action Action { get; set; }

        /// <summary>
        /// Gets or sets the automation id of the item
        /// </summary>
        public string AutomationId { get; set; }

        /// <summary>
        /// Gets or sets the type of the item
        /// </summary>
        public MessageDialogActionableItemType Type { get; set; }
    }
}
