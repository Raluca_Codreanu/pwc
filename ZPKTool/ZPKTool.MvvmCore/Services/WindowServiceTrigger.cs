﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Associates a view and a WindowService instance so that the WindowService can find the data templates defined in the view.
    /// </summary>
    public class WindowServiceTrigger : System.Windows.Interactivity.TriggerBase<DependencyObject>
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for WindowService. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty WindowServiceProperty =
            DependencyProperty.Register("WindowService", typeof(IWindowService), typeof(WindowServiceTrigger), new UIPropertyMetadata(null, OnWindowServiceChanged));

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowServiceTrigger"/> class.
        /// </summary>
        public WindowServiceTrigger()
        {
        }

        /// <summary>
        /// Gets or sets the interaction service instance associated with this trigger.
        /// </summary>        
        public IWindowService WindowService
        {
            get { return (IWindowService)GetValue(WindowServiceProperty); }
            set { SetValue(WindowServiceProperty, value); }
        }
                
        /// <summary>
        /// Called when the InteractionService property has changed.
        /// </summary>
        /// <param name="d">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnWindowServiceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            WindowServiceTrigger trigger = d as WindowServiceTrigger;
            if (trigger != null)
            {
                WindowService oldValue = e.OldValue as WindowService;
                if (oldValue != null)
                {
                    oldValue.TriggerSource = null;
                }

                WindowService newValue = e.NewValue as WindowService;
                if (newValue != null)
                {
                    newValue.TriggerSource = trigger.AssociatedObject;
                }
            }
        }
    }
}
