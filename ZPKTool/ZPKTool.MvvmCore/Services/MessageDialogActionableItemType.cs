﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Used to determine the type of UIElement that will be displayed on the message dialog box
    /// </summary>
    public enum MessageDialogActionableItemType
    {
        /// <summary>
        /// Button type
        /// </summary>
        Button = 0
    }
}
