﻿namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Represents an object that have an undo manager property. (which can be used to undo the other properties values)
    /// </summary>
    public interface IUndoable
    {
        /// <summary>
        /// Gets or sets a value indicating the undo manager.
        /// </summary>
        UndoManager UndoManager { get; set; }
    }
}
