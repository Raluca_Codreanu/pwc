﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// Class used to execute a particular action when on a property is performed Undo.
    /// </summary>
    public class UndoActions
    {
        /// <summary>
        /// Gets or sets the action executed when Undo is performed on the property.
        /// </summary>
        public Action<UndoableItem> UndoAction { get; set; }

        /// <summary>
        /// Gets or sets the property name for which the action is executed.
        /// </summary>
        public string PropName { get; set; }

        /// <summary>
        /// Gets or sets the property instance.
        /// </summary>
        public object Instance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Undo is handled external or not.
        /// </summary>
        public bool IsUndoHandledExternal { get; set; }

        /// <summary>
        /// Add a new action to an undoable item.
        /// </summary>
        /// <param name="item">The undoable item.</param>
        public void PerformAction(UndoableItem item)
        {
            var action = this.UndoAction;
            if (action != null)
            {
                action.Invoke(item);
            }
        }
    }
}
