﻿namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The interface of a service that implements the undo function for the various view models of the application.
    /// </summary>
    public interface IUndoManager
    {
        /// <summary>
        /// Determines whether this instance can perform the Undo operation.
        /// </summary>
        /// <returns>True -if the action can be undone / False - otherwise.</returns>
        bool CanUndo();

        /// <summary>
        /// Undo the last view-model change.
        /// </summary>
        void Undo();

        /// <summary>
        /// Clear all items from the undo list.
        /// </summary>
        void Reset();
    }
}
