﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore.Services
{
    /// <summary>
    /// The type of message dialogs; determines the icon and buttons displayed to the user.
    /// </summary>
    public enum MessageDialogType
    {
        /// <summary>
        /// Info message.
        /// </summary>
        Info,

        /// <summary>
        /// Warning message.
        /// </summary>
        Warning,

        /// <summary>
        /// Error message.
        /// </summary>
        Error,

        /// <summary>
        /// Yes/No question.
        /// </summary>
        YesNo,

        /// <summary>
        /// Yes/No/Cancel question.
        /// </summary>
        YesNoCancel
    }
}
