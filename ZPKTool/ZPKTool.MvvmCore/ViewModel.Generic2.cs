﻿using System;
using System.Diagnostics;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// A specialization of the <see cref="ZPKTool.MvvmCore.ViewModel&lt;TModel&gt;"/> that introduce a Data Source manager for managing the Model.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <typeparam name="TDataSource">The type of the data source manager.</typeparam>    
    public abstract class ViewModel<TModel, TDataSource> : ViewModel<TModel>
        where TModel : class
        where TDataSource : class
    {
        /// <summary>
        /// The data source manager to use for data operations involving the Model.
        /// </summary>
        private TDataSource dataSourceManager;

        /// <summary>
        /// A value indicating whether the SaveCommand save the change into data source or just into the model.
        /// </summary>
        private bool savesToDataSource = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel&lt;TModel, TDataSource&gt;"/> class.
        /// </summary>
        protected ViewModel()
            : base()
        {
        }

        /// <summary>
        /// Gets or sets the data source manager to use for data operations involving the Model.
        /// </summary>        
        public TDataSource DataSourceManager
        {
            get { return this.dataSourceManager; }
            set { this.SetProperty(ref this.dataSourceManager, value, () => this.DataSourceManager, this.OnDataSourceManagerChanged); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the SaveCommand save the change into data source or just into the model. The default value is true.
        /// This property is can be set to false to accumulate the changes made by multiple view-models that use the same TDataSource instance and commit
        /// the changes later.
        /// </summary>        
        public bool SavesToDataSource
        {
            get { return this.savesToDataSource; }
            set { this.SetProperty(ref this.savesToDataSource, value, () => this.SavesToDataSource); }
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected virtual void OnDataSourceManagerChanged()
        {
        }

        /// <summary>
        /// Checks if the data source manager is initialized. An exception is thrown if is not.
        /// </summary>
        /// <exception cref="InvalidOperationException">The DataSourceManager is null.</exception>
        [DebuggerStepThrough]
        protected void CheckDataSource()
        {
            if (!this.IsInViewerMode && this.DataSourceManager == null)
            {
                throw new InvalidOperationException("This operation cannot be executed when the DataSourceManager is null.");
            }
        }

        /// <summary>
        /// Checks if the model and data source manager are initialized. If they are not an exception is thrown.
        /// </summary>
        /// <exception cref="InvalidOperationException">The Model or DataSourceManager is null.</exception>
        [DebuggerStepThrough]
        protected void CheckModelAndDataSource()
        {
            this.CheckModel();
            this.CheckDataSource();
        }
    }
}
