﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Initializes the MEF framework.
    /// </summary>
    public abstract class MefBootstrapper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MefBootstrapper"/> class.
        /// </summary>
        protected MefBootstrapper()
        {
            this.ModuleCatalog = new AggregateCatalog();
        }

        /// <summary>
        /// Gets the catalog holding all modules.
        /// </summary>        
        protected AggregateCatalog ModuleCatalog { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is initialized.
        /// </summary>        
        protected bool IsInitialized { get; private set; }

        /// <summary>
        /// Gets the composition container.
        /// </summary>        
        public CompositionContainer CompositionContainer { get; private set; }

        /// <summary>
        /// Perform the bootstrapping.
        /// </summary>
        public void Run()
        {
            this.InitializeComposition();
            this.IsInitialized = true;
        }

        /// <summary>
        /// Adds all necessary modules into the module catalog.
        /// </summary>
        protected abstract void ConfigureModuleCatalog();

        /// <summary>
        /// Discovers and initializes the different UI parts.
        /// </summary>
        private void InitializeComposition()
        {
            this.ModuleCatalog.Catalogs.Add(new AssemblyCatalog(System.Reflection.Assembly.GetExecutingAssembly()));
            ConfigureModuleCatalog();

            this.CompositionContainer = new CompositionContainer(this.ModuleCatalog);
            CompositionBatch batch = new CompositionBatch();
            batch.AddExportedValue(this.CompositionContainer);
            this.CompositionContainer.Compose(batch);
        }
    }
}
