﻿using System;

namespace ZPKTool.MvvmCore.Helpers
{
    /// <summary>
    /// Stores a delegate to a method without keeping a hard reference to the method's object instance.
    /// </summary>    
    public class WeakDelegate
    {
        /// <summary>
        /// The object whose method is wrapped by this instance.
        /// </summary>
        private WeakReference actionTarget;

        /// <summary>
        /// The method wrapped by this instance.
        /// </summary>
        private System.Reflection.MethodInfo actionMethod;

        /// <summary>
        /// The type of the delegate from which this instance was built.
        /// </summary>
        private Type actionType;

        /// <summary>
        /// Initializes a new instance of the <see cref="WeakDelegate"/> class.
        /// </summary>
        /// <param name="action">The delegate that will be associated to this instance.</param>
        /// <exception cref="ArgumentNullException">action is null</exception>
        /// <exception cref="ArgumentException">action's target object is null</exception>
        public WeakDelegate(Delegate action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action", "The action delegate was null.");
            }

            if (action.Target == null)
            {
                throw new ArgumentException("Delegates without a target are not allowed.", "action");
            }

            this.actionTarget = new WeakReference(action.Target);
            this.actionMethod = action.Method;
            this.actionType = action.GetType();
        }

        /// <summary>
        /// Gets a value indicating whether the delegate's owner is still alive, or if it was collected by the Garbage Collector.
        /// </summary>
        public bool IsAlive
        {
            get
            {
                if (this.actionTarget == null)
                {
                    return false;
                }

                return this.actionTarget.IsAlive;
            }
        }

        /// <summary>
        /// Gets the delegate's owner.
        /// </summary>
        public object Target
        {
            get
            {
                if (this.actionTarget == null)
                {
                    return null;
                }

                return this.actionTarget.Target;
            }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="weakAction">The weak action.</param>
        /// <param name="action">The action.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(WeakDelegate weakAction, Delegate action)
        {
            return weakAction.Equals(action);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="weakAction">The weak action.</param>
        /// <param name="action">The action.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(WeakDelegate weakAction, Delegate action)
        {
            return !weakAction.Equals(action);
        }

        /// <summary>
        /// Invokes the method represented by this instance, if its target object is still alive.
        /// </summary>
        public void Invoke()
        {
            Invoke(null);
        }

        /// <summary>
        /// Invokes the method represented by this instance, if its target object is still alive.
        /// </summary>
        /// <param name="args">The parameters to pass to the method.</param>
        public void Invoke(params object[] args)
        {
            object target = this.actionTarget != null ? this.actionTarget.Target : null;
            if (target != null)
            {
                Delegate d = Delegate.CreateDelegate(this.actionType, target, this.actionMethod);
                d.DynamicInvoke(args);
            }
        }

        /// <summary>
        /// Sets the weak reference to the method's target object to null.
        /// </summary>
        public void MarkForDeletion()
        {
            this.actionTarget = null;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            Delegate action = obj as Delegate;
            if (action == null)
            {
                return base.Equals(obj);
            }

            // The object to compare to is a delegate so compare to target and method
            if (this.actionTarget == null)
            {
                return false;
            }

            if (action.Target == this.actionTarget.Target && action.Method == this.actionMethod)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}