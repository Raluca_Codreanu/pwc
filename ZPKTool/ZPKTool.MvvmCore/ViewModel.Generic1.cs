﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Timers;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// A base class for implementing view models for form-like views that require input validation.
    /// Properties should define validation rules by using validation attributes defined in System.ComponentModel.DataAnnotations.
    /// Properties representing Model data should be wrapped in DataProperty&lt;T&gt; instances.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    //// TODO: implement IDIsposable on view-models after all other projects have no more issues.
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Justification = "Temporary suppression. The issue will be fixed later.")]
    public abstract class ViewModel<TModel> : ViewModel, ICostRecalculation where TModel : class
    {
        #region Fields

        /// <summary>
        /// A value indicating whether the input on this form is valid.
        /// </summary>
        private bool isInputValid;

        /// <summary>
        /// A value indicating whether to show the controls that allow to save or reset the view-model.
        /// </summary>
        private bool showSaveControls;

        /// <summary>
        /// The title of the view model.
        /// </summary>
        private string title;

        /// <summary>
        /// An error message describing all validation errors for this object.
        /// </summary>
        private string error;

        /// <summary>
        /// The command that saves all changed back into the model.
        /// </summary>
        private ICommand saveToModelCommand;

        /// <summary>
        /// The names of properties for which the OnPropertyChanged handler should do nothing.        
        /// </summary>
        private List<string> propertyChangeIgnoreList;

        /// <summary>
        /// The list of data properties declared by this type.
        /// </summary>
        private List<PropertyInfo> dataProperties;

        /// <summary>
        /// The model.
        /// </summary>        
        private TModel model;

        /// <summary>
        /// The model clone.
        /// </summary>        
        private TModel modelClone;

        /// <summary>
        /// The properties that affect cost.
        /// </summary>
        private List<string> propertiesThatAffectCosts;

        /// <summary>
        /// A value indicating whether the properties changes are monitored or not.
        /// </summary>
        private bool arePropertiesForCalculationMonitoringActive;

        /// <summary>
        /// A value indicating whether the recalculation notification active or not.
        /// </summary>
        private bool isRecalculationNotificationInactive;

        /// <summary>
        /// The timer to delay the RefreshCalculation method.
        /// </summary>
        private Timer refreshCalculationWithDelayTimer;

        /// <summary>
        /// A value indicating whether the view model performs cost recalculation.
        /// </summary>
        private bool dontPerformCostRecalculation;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel&lt;TModel&gt;"/> class.
        /// </summary>
        protected ViewModel()
            : base(false)
        {
            // Add the properties of this type because they do not represent data and their changes should not trigger data validation.
            this.propertyChangeIgnoreList = typeof(ViewModel<TModel>).GetProperties().Select(prop => prop.Name).ToList();

            // Create the list of data properties to be used internally.
            var allProperties = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            this.dataProperties =
                allProperties.Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(DataProperty<>)).ToList();

            this.propertiesThatAffectCosts = new List<string>();

            this.InitializeDataProperties();
            this.UndoManager = new UndoManager();

            this.ShowSaveControls = true;
            this.SaveToModelCommand = new DelegateCommand(this.SaveToModel, this.CanSaveToModel);

            this.PropertyChanged += this.OnPropertyChanged;

            this.Dispatcher.BeginInvoke(() => this.ValidateData(), System.Windows.Threading.DispatcherPriority.Loaded);
        }

        #region Events

        /// <summary>
        /// Occurs when the calculation needs to be refreshed.
        /// </summary>
        public event EventHandler RequestRecalculation;

        #endregion Events

        #region Commands

        /// <summary>
        /// Gets or sets the command that saves all changed back into the model.
        /// This command is useful to save the changes of a nested view model without saving them in the database.
        /// </summary>
        public ICommand SaveToModelCommand
        {
            get { return this.saveToModelCommand; }
            set { this.SetProperty(ref this.saveToModelCommand, value, () => this.SaveToModelCommand); }
        }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the Model. Its value cannot be null.
        /// </summary>
        /// <exception cref="ArgumentNullException">The value was null.</exception>
        public TModel Model
        {
            get
            {
                return this.model;
            }

            set
            {
                if (value == null)
                {
                    string message = string.Format("The Model cannot be null (view-model: {0}).", this.GetType().Name);
                    throw new ArgumentNullException(string.Empty, message);
                }

                this.model = value;
                this.OnPropertyChanged(() => this.Model);
                this.ArePropertiesForCalculationMonitoringActive = false;
                this.OnModelChanged();

                if (!this.DontPerformCostRecalculation)
                {
                    if (this.refreshCalculationWithDelayTimer == null)
                    {
                        // Initialize the time and delay with 250 ms the call for RefreshCalculation method, to prevent recalculating after each key press.
                        this.refreshCalculationWithDelayTimer = new Timer(TimeSpan.FromMilliseconds(250).Milliseconds);
                        this.refreshCalculationWithDelayTimer.AutoReset = false;
                        this.refreshCalculationWithDelayTimer.Elapsed += (s, e) =>
                        {
                            this.RefreshCalculation();
                        };
                    }

                    if (!this.IsInViewerMode
                        && !this.IsReadOnly)
                    {
                        this.InitializePropertiesThatAffectCostTracking();
                        this.ArePropertiesForCalculationMonitoringActive = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the model clone.
        /// </summary>
        public TModel ModelClone
        {
            get
            {
                return this.modelClone;
            }

            set
            {
                if (value != null)
                {
                    this.modelClone = value;

                    // Get all the sub-view-model properties for the current instance.
                    var subVmProps =
                        this.GetType()
                            .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Where(prop => prop.IsDefined(typeof(SubViewModelPropertyAttribute), false));

                    foreach (var subVmProp in subVmProps)
                    {
                        if (subVmProp == null)
                        {
                            continue;
                        }

                        var subVmPropValue = subVmProp.GetValue(this, null);
                        var subVmPropAttr = subVmProp.GetCustomAttributes(typeof(SubViewModelPropertyAttribute), true)
                                                .FirstOrDefault() as SubViewModelPropertyAttribute;

                        // Set the sub-view-models ModelClone property according to the associated property.
                        if (subVmPropAttr != null && !string.IsNullOrWhiteSpace(subVmPropAttr.ModelPropertyAssociated))
                        {
                            var modelProp = this.Model.GetType().GetProperty(
                                                subVmPropAttr.ModelPropertyAssociated,
                                                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                            var modelPropValue = modelProp.GetValue(this.ModelClone, null);
                            var subVmPropModel = subVmPropValue.GetType().GetProperty(
                                                     ReflectionUtils.GetPropertyName(() => this.ModelClone),
                                                     BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                            subVmPropModel.SetValue(subVmPropValue, modelPropValue, null);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the model parent clone.
        /// </summary>
        public object ModelParentClone { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the input on this view model is valid.
        /// </summary>        
        public bool IsInputValid
        {
            get { return this.isInputValid; }
            set { this.SetProperty(ref this.isInputValid, value, () => this.IsInputValid); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the controls that allow to save or reset the view model.
        /// </summary>
        public bool ShowSaveControls
        {
            get { return this.showSaveControls; }
            set { this.SetProperty(ref this.showSaveControls, value, () => this.ShowSaveControls); }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>        
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the error message describing all validation errors for this object.
        /// </summary>        
        public override string Error
        {
            get { return this.error; }
            set { this.SetProperty(ref this.error, value, () => this.Error); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the properties changes are monitored or not.
        /// </summary>
        protected bool ArePropertiesForCalculationMonitoringActive
        {
            get { return this.arePropertiesForCalculationMonitoringActive; }
            set { this.SetProperty(ref this.arePropertiesForCalculationMonitoringActive, value, () => this.ArePropertiesForCalculationMonitoringActive); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the view model performs cost recalculation.
        /// </summary>
        public bool DontPerformCostRecalculation
        {
            get
            {
                return this.dontPerformCostRecalculation;
            }

            set
            {
                if (this.dontPerformCostRecalculation != value)
                {
                    this.dontPerformCostRecalculation = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the cost recalculation clone manager.
        /// </summary>
        public ICostRecalculationCloneManager CloneManager { get; set; }

        #endregion Properties

        /// <summary>
        /// Registers a data property with the validation engine.
        /// This is necessary when you explicitly instantiate a data property of the view-model.
        /// </summary>
        /// <typeparam name="T">The type of the value wrapped by the DataProperty instance.</typeparam>
        /// <param name="dataProperty">The data property to register.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This syntax allows lambda expressions to be used.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Type of dataProperty parameter is necessary in order to assign lambda expressions to it.")]
        public void RegisterForValidation<T>(Expression<Func<DataProperty<T>>> dataProperty)
        {
            if (dataProperty == null)
            {
                throw new ArgumentNullException("dataProperty", "The dataProperty was null.");
            }

            if (dataProperty.Body == null)
            {
                throw new ArgumentException("The lambda expression was empty.", "dataProperty");
            }

            // Extract the property name from the linq expression
            PropertyInfo propInfo = null;
            Expression body = dataProperty.Body;
            if (body.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression mexp = (MemberExpression)body;
                propInfo = mexp.Member as System.Reflection.PropertyInfo;
            }
            else if (body.NodeType == ExpressionType.Convert)
            {
                UnaryExpression exp = (UnaryExpression)body;
                if (exp.Operand.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression mexp = (MemberExpression)exp.Operand;
                    propInfo = mexp.Member as System.Reflection.PropertyInfo;
                }
            }

            if (propInfo == null)
            {
                throw new ArgumentException("The lambda expression did not contain a property definition.", "dataProperty");
            }

            if (propInfo.DeclaringType != this.GetType())
            {
                throw new ArgumentException("The property defined by the dataProperty lambda expression is not defined by this type.", "dataProperty");
            }

            DataProperty<T> dataPropValue = (DataProperty<T>)propInfo.GetValue(this, null);
            if (dataPropValue == null)
            {
                throw new InvalidOperationException("A data property with null value can't be registered for validation.");
            }

            dataPropValue.PropertyChanged += this.DataProperty_ValuePropertyChanged;
        }

        #region Model related logic

        /// <summary>
        /// Accepts all changes made to this view-model and resets its change state. Does not affect nested view models.
        /// </summary>
        public void AcceptChanges()
        {
            foreach (PropertyInfo dataProp in this.dataProperties)
            {
                object value = dataProp.GetValue(this, null);
                IChangeTracking tracker = value as IChangeTracking;
                if (tracker != null)
                {
                    tracker.AcceptChanges();
                }
            }
        }

        /// <summary>
        /// Sets the Model property to null. This is not allowed through the Model property but sometimes is necessary internally.
        /// </summary>
        protected void ReleaseModel()
        {
            this.model = null;
            this.OnPropertyChanged(() => this.Model);
        }

        /// <summary>
        /// Checks if the model is initialized. An exception is thrown if is not.
        /// </summary>
        /// <exception cref="InvalidOperationException">The Model is null.</exception>
        [DebuggerStepThrough]
        protected void CheckModel()
        {
            if (this.Model == null)
            {
                throw new InvalidOperationException("This operation cannot be executed when the Model is null.");
            }
        }

        /// <summary>
        /// Called when the Model changed.
        /// </summary>        
        protected virtual void OnModelChanged()
        {
            this.Saved = false;
            this.LoadDataFromModel();
        }

        /// <summary>
        /// Loads the data from the current Model instance and resets the changed state.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.        
        /// </summary>
        protected void LoadDataFromModel()
        {
            this.CheckModel();
            this.LoadDataFromModel(this.Model);
            this.AcceptChanges();
            this.IsChanged = false;
        }

        /// <summary>
        /// Loads the data from the specified Model instance into the view-model.        
        /// </summary>
        /// <remarks>
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// The type of a view-model property and its associated model property is assumed to be the same or compatible.
        /// </remarks>
        /// <param name="sourceModel">The model instance from which to load.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="sourceModel"/> argument was null.</exception>
        /// <exception cref="InvalidOperationException">The Model property name declared by a view-model property is invalid (a property with that name does not exist on the Model).</exception>        
        public virtual void LoadDataFromModel(TModel sourceModel)
        {
            Argument.IsNotNull("sourceModel", sourceModel);

            // Get all properties decorated with the ExposesModelProperty attribute.
            Type modelType = sourceModel.GetType();
            var exposedProperties = this.GetType().GetProperties().Where(prop => prop.GetCustomAttributes(typeof(ExposesModelPropertyAttribute), true).Length > 0);
            foreach (PropertyInfo exposedProp in exposedProperties)
            {
                ExposesModelPropertyAttribute attribute =
                    (ExposesModelPropertyAttribute)exposedProp.GetCustomAttributes(typeof(ExposesModelPropertyAttribute), true).First();

                PropertyInfo modelProperty = modelType.GetProperty(attribute.ModelProperty);
                if (modelProperty == null)
                {
                    string message = string.Format(
                        "The model property '{0}' exposed by the view-model property '{1}' could not be found on the Model.",
                        attribute.ModelProperty,
                        exposedProp.Name);
                    throw new InvalidOperationException(message);
                }

                object targetObject = null;
                PropertyInfo targetProperty = null;
                if (this.dataProperties.Contains(exposedProp))
                {
                    // In case of data wrappers set the model property value in the wrapper's Value property.
                    object dataProperty = exposedProp.GetValue(this, null);
                    targetProperty = dataProperty.GetType().GetProperty("Value");
                    targetObject = dataProperty;
                }
                else
                {
                    // Otherwise set the model property value in the view-model property.                    
                    targetObject = this;
                    targetProperty = exposedProp;
                }

                object modelPropertyValue = modelProperty.GetValue(sourceModel, null);

                // If the target property and the model property have different types try to convert the model property value to the target property type.
                Type targetPropertyType = targetProperty.PropertyType;
                if (this.IsNullable(targetPropertyType))
                {
                    targetPropertyType = targetPropertyType.GetGenericArguments()[0];
                }

                Type modelPropertyType = modelProperty.PropertyType;
                if (this.IsNullable(modelPropertyType))
                {
                    modelPropertyType = modelPropertyType.GetGenericArguments()[0];
                }

                if (targetPropertyType != modelPropertyType)
                {
                    // If the load target property is an enumeration, convert the model value to that enumeration.
                    // This is necessary because the enumeration members are stored as numbers in the model.
                    if (targetPropertyType.IsEnum && modelPropertyValue != null)
                    {
                        modelPropertyValue = Enum.ToObject(targetPropertyType, modelPropertyValue);
                    }
                }

                targetProperty.SetValue(targetObject, modelPropertyValue, null);
            }
        }

        #endregion Model related logic

        #region Save/Cancel

        /// <summary>
        /// Determines whether the changes can be save back into the model. Used by the SaveToModelCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the view model can be saved, false otherwise.
        /// </returns>
        protected virtual bool CanSaveToModel()
        {
            return this.IsInputValid;
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. This method is executed by the SaveToModelCommand command.
        /// The default implementation saves back into the model all view-model properties decorated with the ExposedModelProperty attribute.
        /// </summary>
        /// <remarks>The type of a view-model property and its associated model property is assumed to be the same or compatible.</remarks>
        /// <exception cref="InvalidOperationException">The Model property name declared by a view-model property is invalid (a property with that name does not exist on the Model).</exception>
        protected virtual void SaveToModel()
        {
            this.CheckModel();

            // Get all properties decorated with the ExposesModelProperty attribute and save their values back into the model.
            Type modelType = this.Model.GetType();
            var exposedProperties = this.GetType().GetProperties().Where(prop => prop.GetCustomAttributes(typeof(ExposesModelPropertyAttribute), true).Length > 0);
            foreach (PropertyInfo exposedProp in exposedProperties)
            {
                ExposesModelPropertyAttribute attribute =
                    (ExposesModelPropertyAttribute)exposedProp.GetCustomAttributes(typeof(ExposesModelPropertyAttribute), true).First();

                PropertyInfo modelProperty = modelType.GetProperty(attribute.ModelProperty);
                if (modelProperty == null)
                {
                    string message = string.Format(
                        "The model property '{0}' exposed by the view-model property '{1}' could not be found on the Model.",
                        attribute.ModelProperty,
                        exposedProp.Name);
                    throw new InvalidOperationException(message);
                }
                else if (modelProperty.CanWrite)
                {
                    object newValue = null;
                    if (this.dataProperties.Contains(exposedProp))
                    {
                        // Extract the new value from the Value property of data wrappers.
                        object dataProperty = exposedProp.GetValue(this, null);
                        newValue = dataProperty.GetType().GetProperty("Value").GetValue(dataProperty, null);
                    }
                    else
                    {
                        // Otherwise assume that the type of view-model property matches the type of the Model property corresponding to it.
                        newValue = exposedProp.GetValue(this, null);
                    }

                    if (newValue != null)
                    {
                        Type newValueType = newValue.GetType();
                        if (this.IsNullable(newValueType))
                        {
                            newValueType = newValueType.GetGenericArguments()[0];
                        }

                        Type modelPropertyType = modelProperty.PropertyType;
                        if (this.IsNullable(modelPropertyType))
                        {
                            modelPropertyType = modelPropertyType.GetGenericArguments()[0];
                        }

                        // If the new value is an enum member and the model property is not of the same type it is assumed that the
                        // model property has an numeric type compatible with the enum value.
                        if (newValueType != modelPropertyType && newValueType.IsEnum)
                        {
                            // Convert the enum value to its underlying numeric type.
                            newValue = Convert.ChangeType(newValue, modelPropertyType);
                        }
                    }

                    // Save the new value into the model only if it is not equal to the current value.
                    object currentValue = modelProperty.GetValue(this.Model, null);
                    bool sameValue = (currentValue == null && newValue == null)
                        || (currentValue != null && newValue != null && currentValue.Equals(newValue));
                    if (!sameValue)
                    {
                        modelProperty.SetValue(this.Model, newValue, null);
                    }
                }
            }

            this.IsChanged = false;
            this.AcceptChanges();
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return this.IsInputValid;
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return this.Model != null;
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            try
            {
                this.UndoManager.StartBatch();
                this.StopRecalculationNotifications();
                this.LoadDataFromModel();
                this.IsChanged = false;
            }
            finally
            {
                this.UndoManager.EndBatch();
                this.ResumeRecalculationNotifications(true);
            }
        }

        #endregion Save/Cancel

        #region Calculations related

        /// <summary>
        /// Stops the recalculation notifications.
        /// </summary>
        public void StopRecalculationNotifications()
        {
            this.isRecalculationNotificationInactive = true;

            // Stop all child view models.
            var action = new Action<ICostRecalculation>(o => o.StopRecalculationNotifications());
            this.InvokeActionOnChildViewModels(action);
        }

        /// <summary>
        /// Resumes the recalculation notifications.
        /// </summary>
        /// <param name="notifyNow">if set to true a recalculation notification occurs.</param>
        public void ResumeRecalculationNotifications(bool notifyNow)
        {
            this.isRecalculationNotificationInactive = false;

            // Resume all child view models.
            var action = new Action<ICostRecalculation>(o => o.ResumeRecalculationNotifications(false));
            this.InvokeActionOnChildViewModels(action);

            if (notifyNow
                && this.ArePropertiesForCalculationMonitoringActive)
            {
                this.refreshCalculationWithDelayTimer.Stop();
                this.refreshCalculationWithDelayTimer.Start();
            }
        }

        /// <summary>
        /// Called when a property value from a child view model changes.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void CalculationPropertyValueChanged(object sender, EventArgs e)
        {
            this.refreshCalculationWithDelayTimer.Stop();
            this.refreshCalculationWithDelayTimer.Start();
        }

        /// <summary>
        /// Initializes the properties that affect costs.
        /// </summary>
        private void InitializePropertiesThatAffectCostTracking()
        {
            if (this != null)
            {
                var allProperties = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (var prop in allProperties)
                {
                    // Get all the properties that expose a model property and affect the cost.
                    var exposedProp = prop.GetCustomAttributes(typeof(ExposesModelPropertyAttribute), true).FirstOrDefault() as ExposesModelPropertyAttribute;
                    if (exposedProp != null
                        && exposedProp.AffectsCost)
                    {
                        if (prop.PropertyType.IsGenericType
                            && prop.PropertyType.GetGenericTypeDefinition().GetInterfaces().Any(i => i == typeof(IVMProperty)))
                        {
                            var propValue = prop.GetValue(this, null) as IVMProperty;
                            if (propValue != null)
                            {
                                propValue.ValueChanged += (s, e) => this.OnPropertyThatAffectsCostCalculationChanged(propValue, exposedProp.ModelProperty);
                            }
                        }

                        this.propertiesThatAffectCosts.Add(exposedProp.ModelProperty);
                    }
                }

                this.PropertyChanged += this.OnPropertyThatAffectsCostCalculationChanged;

                // Register the child view models to be notified when a calculation property value changes.
                if (!this.IsChild)
                {
                    var action = new Action<ICostRecalculation>(o => o.RequestRecalculation += new EventHandler(this.CalculationPropertyValueChanged));
                    this.InvokeActionOnChildViewModels(action);
                }
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnPropertyThatAffectsCostCalculationChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.propertiesThatAffectCosts.Contains(e.PropertyName))
            {
                this.OnPropertyThatAffectsCostCalculationChanged(null, e.PropertyName);
            }
        }

        /// <summary>
        /// Handles the value changed event for properties that affects cost.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="propertyName">Name of the property.</param>
        private void OnPropertyThatAffectsCostCalculationChanged(object obj, string propertyName)
        {
            if (obj == null
                || this.ModelClone == null
                || string.IsNullOrEmpty(propertyName)
                || !this.propertiesThatAffectCosts.Contains(propertyName)
                || !this.ArePropertiesForCalculationMonitoringActive)
            {
                return;
            }

            // Get the new value of the property.
            object newVal = null;
            if (obj.GetType().IsGenericType
                && obj.GetType().GetGenericTypeDefinition().GetInterfaces().Any(i => i == typeof(IVMProperty)))
            {
                newVal = ReflectionUtils.GetComplexPropertyValue(obj, "Value");
            }
            else
            {
                newVal = ReflectionUtils.GetSimplePropertyValue(this, propertyName);
            }

            // Set the clone property to the new value.
            var propInfo = this.ModelClone.GetType().GetProperty(propertyName);
            if (propInfo != null)
            {
                propInfo.SetValue(this.ModelClone, newVal, null);
            }

            if (!this.isRecalculationNotificationInactive)
            {
                this.refreshCalculationWithDelayTimer.Stop();
                this.refreshCalculationWithDelayTimer.Start();
            }
        }

        /// <summary>
        /// Refreshes the calculation.
        /// </summary>
        protected virtual void RefreshCalculation()
        {
            if (this.RequestRecalculation != null)
            {
                this.RequestRecalculation(null, null);
            }
        }

        #endregion Calculations related

        /// <summary>
        /// Initializes all public properties of type DataProperty&lt;&gt;.
        /// </summary>
        private void InitializeDataProperties()
        {
            foreach (PropertyInfo dataProp in this.dataProperties)
            {
                object newValue = Activator.CreateInstance(dataProp.PropertyType, this, dataProp.Name);
                dataProp.SetValue(this, newValue, null);

                ((INotifyPropertyChanged)newValue).PropertyChanged += this.DataProperty_ValuePropertyChanged;
            }
        }

        /// <summary>
        /// Validates all properties of this view-model.
        /// </summary>
        private void ValidateData()
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Validate the properties of the view-model, to support the view models that did not use DataProperty<T> to wrap the Model's properties.
            // Validate only the non-DataProperty properties. Validating the entire view-model instance is very slow in some scenarios.
            foreach (var property in this.GetType().GetProperties().Where(p => p.CanRead && !this.dataProperties.Contains(p) && p.GetIndexParameters().Length == 0))
            {
                var value = property.GetValue(this, null);
                List<ValidationResult> results = new List<ValidationResult>();
                var validationAttributes = property.GetCustomAttributes(typeof(ValidationAttribute), true).OfType<ValidationAttribute>();

                Validator.TryValidateValue(
                    value,
                    new ValidationContext(this, null, null),
                    results,
                    validationAttributes);

                validationResults.AddRange(results);
            }

            List<string> errorMessages = validationResults.Select(r => r.ErrorMessage).ToList();

            // Also validate all data properties            
            foreach (var property in this.dataProperties)
            {
                object propValue = property.GetValue(this, null);
                IDataErrorInfo errorInfo = propValue as IDataErrorInfo;
                if (errorInfo != null)
                {
                    var propertyError = errorInfo.Error;
                    if (!string.IsNullOrWhiteSpace(propertyError))
                    {
                        errorMessages.Add(propertyError);
                    }
                }
            }

            string errorMessage = string.Empty;
            if (errorMessages.Count == 1)
            {
                errorMessage = errorMessages[0];
            }
            else
            {
                errorMessage = string.Join(Environment.NewLine, errorMessages.Select(msg => "- " + msg));
            }

            // Update the error message and the validity of the input.
            this.Error = errorMessage;
            if (string.IsNullOrEmpty(errorMessage))
            {
                this.IsInputValid = true;
            }
            else
            {
                this.IsInputValid = false;
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Validate the view model data. Normally it should validate just the changed property and update the view model's validation status/message,
            // but we can't do that here so we validate the entire view model by accessing the Error property.
            if (!this.propertyChangeIgnoreList.Contains(e.PropertyName)
                && e.PropertyName != "IsInputValid")
            {
                this.ValidateData();
            }

            if (e.PropertyName == "IsReadOnly" && this.IsReadOnly)
            {
                this.ShowSaveControls = false;
            }
        }

        /// <summary>
        /// Called when the Value property of a data property of this instance has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void DataProperty_ValuePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                this.ValidateData();

                IChangeTracking changeTracker = sender as IChangeTracking;
                if (changeTracker != null && changeTracker.IsChanged)
                {
                    this.IsChanged = true;
                }
            }
        }

        /// <summary>
        /// Determines whether the specified type is nullable.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the specified type is nullable; otherwise, <c>false</c>.
        /// </returns>
        private bool IsNullable(Type type)
        {
            Argument.IsNotNull("type", type);
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.StopRecalculationNotifications();
        }

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            if (this.ArePropertiesForCalculationMonitoringActive)
            {
                this.ResumeRecalculationNotifications(false);
            }
        }
    }
}