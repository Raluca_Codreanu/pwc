﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Contains event data for the <see cref="VMProperty.ValueChanged"/> event.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public class ValueChangedEventArgs<TValue> : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValueChangedEventArgs{TValue}"/> class.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        public ValueChangedEventArgs(TValue oldValue, TValue newValue)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }

        /// <summary>
        /// Gets the old value.
        /// </summary>
        public TValue OldValue { get; private set; }

        /// <summary>
        /// Gets the new value.
        /// </summary>
        public TValue NewValue { get; private set; }
    }
}
