﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.MvvmCore
{
    /// <summary>
    /// Provides a common interface for view-models to perform some actions at certain points in the lifecycle of the view.
    /// </summary>    
    public interface ILifeCycleManager
    {
        /// <summary>
        /// Called after the view has been loaded into a parent.
        /// </summary>
        void OnLoaded();

        /// <summary>
        /// Called before unloading the view from its parent.
        /// A return value of false will cancel the view's unloading.
        /// </summary>
        /// <returns>True if the unloading process should continue and false if it should be canceled.</returns>
        bool OnUnloading();

        /// <summary>
        /// Called after the view has been unloaded from the parent.
        /// </summary>
        void OnUnloaded();
    }
}
