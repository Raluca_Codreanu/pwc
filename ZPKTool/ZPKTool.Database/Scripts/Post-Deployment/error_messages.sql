﻿/*************************************************************************/
/*	Contains the error messages specific to the ZPKTool application      */
/*	The messages number format is 49371xxxx, starting at 493710000       */
/*                                                                       */
/*  Blocks of error messages can be reserved if it is deemed necessary,  */
/*  but they have to respect the format.                                 */
/*	                                                                     */
/*  Note that for every message it is mandatory to add the us_english    */
/*  translation (@lang = 'us_english').                                   */
/*************************************************************************/

-- Raised by the [Currencies_Validate_IsoCode_Trigger] when a duplicate master data currency ISO code is detected during insert or update.
EXEC sp_addmessage @msgnum = 493710000, @severity = 11, @msgtext = 'The master data currency ISO code must be unique.', @lang = 'us_english', @replace = 'replace'