﻿/*
  Post-Deployment script common for all databases.
*/


-- Create login PROimpensUser.
IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = 'PROimpensUser')
BEGIN
	CREATE LOGIN PROimpensUser WITH PASSWORD = 'c,6P4J.m,Y.2', DEFAULT_DATABASE = [$(DatabaseName)]
END
ELSE
BEGIN
    -- Update the old, default, password with one that respects a more strict password policy.
	IF EXISTS(SELECT * from sys.syslogins where name='PROimpensUser'AND pwdCompare ('c,6P4J.m,',password,0) = 1)
	BEGIN
		ALTER LOGIN PROimpensUser 
		WITH PASSWORD = 'c,6P4J.m,Y.2' UNLOCK;
	END
END
GO

USE master
GO

-- Create user PROimpensUser.
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'PROimpensUser')
BEGIN	
	CREATE USER PROimpensUser FOR LOGIN PROimpensUser WITH DEFAULT_SCHEMA = [$(DatabaseName)]
END
GO

USE $(DatabaseName)
GO


-- Add PROimpensUser owner to database.
IF (SELECT SUSER_SNAME(owner_sid) FROM sys.databases WHERE name = '$(DatabaseName)') != 'PROimpensUser'
BEGIN
	ALTER AUTHORIZATION ON DATABASE:: $(DatabaseName) TO PROimpensUser
END
GO

-- Kill All Active Connections To a Database So The Read_Commited_Snapshot = ON can be set.
ALTER DATABASE $(DatabaseName) SET SINGLE_USER WITH ROLLBACK IMMEDIATE
ALTER DATABASE $(DatabaseName) SET ALLOW_SNAPSHOT_ISOLATION ON
ALTER DATABASE $(DatabaseName) SET READ_COMMITTED_SNAPSHOT ON
ALTER DATABASE $(DatabaseName) SET MULTI_USER
GO

-- Create login PROimpensSyncUser.
IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = 'PROimpensSyncUser')
BEGIN
	CREATE LOGIN PROimpensSyncUser WITH PASSWORD = 'c,6P4J.m,Y.2', DEFAULT_DATABASE = [$(DatabaseName)]
END
ELSE
BEGIN
    -- Update the old, default, password with one that respects a more strict password policy.
	IF EXISTS(SELECT * from sys.syslogins where name='PROimpensSyncUser'AND pwdCompare ('c,6P4J.m,',password,0) = 1)
	BEGIN
		ALTER LOGIN PROimpensSyncUser 
		WITH PASSWORD = 'c,6P4J.m,Y.2' UNLOCK;
	END
END
GO

-- Create user PROimpensSyncUser.
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'PROimpensSyncUser')
BEGIN
	CREATE USER PROimpensSyncUser FROM LOGIN PROimpensSyncUser
END
GO

-- Added db_owner role for user PROimpensSyncUser.
EXEC sp_addrolemember 'db_owner', 'PROimpensSyncUser'
GO

/* Migrates the dates from UserRoles table in Roles from Users table.
   Drop the tables that are related with UserRoles. */
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'UserRoles')
BEGIN				 
	DECLARE @RolesValue TABLE
	(
		UserGuid uniqueidentifier,
		RoleValue int		
	);

	INSERT INTO @RolesValue
	SELECT UserGuid, SUM(CASE RoleGuid WHEN '94B2B098-E7EC-4FAE-BC0A-B785A81103E0' THEN 1
									   WHEN '49B91997-F54B-45CF-8DDE-9CC657B241AE' THEN 2
									   WHEN 'CDDF76CE-0546-499B-B07E-1D8DB457EF79' THEN 4
									   WHEN '23D78CA9-08C4-4698-A2CA-2CAED436CF54' THEN 8
									   WHEN 'CBD5687D-2579-42CE-9057-DAE2F47FA241' THEN 16
									   WHEN '53446801-7343-4113-9821-9E500ECEDF7B' THEN 32
									   ELSE 0
									 END)
		FROM UserRoles
		GROUP BY UserGuid

	UPDATE Users SET Roles = 0
	UPDATE Users SET Roles = Roles + RoleValue
		FROM @RolesValue
		WHERE Users.Guid = UserGuid

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'RoleAccessRights')
	BEGIN		
		DROP TABLE RoleAccessRights
	END

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'RoleAccessRights_tracking')
	BEGIN		
		DROP TABLE RoleAccessRights_tracking
	END

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'AccessRights')
	BEGIN		
		DROP TABLE AccessRights
	END

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'AccessRights_tracking')
	BEGIN		
		DROP TABLE AccessRights_tracking
	END

	DROP TABLE UserRoles

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'UserRoles_tracking')
	BEGIN		
		DROP TABLE UserRoles_tracking
	END

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'Roles')
	BEGIN		
		DROP TABLE Roles
	END

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
					WHERE TABLE_NAME = 'Roles_tracking')
	BEGIN		
		DROP TABLE Roles_tracking
	END
END

-- Update GlobaSettings table
PRINT 'Updating the global settings...'
IF NOT EXISTS(SELECT Value FROM [GlobalSettings] WHERE [Guid] = 'F95AB72E-E79C-4885-B6DB-D1F648543051')
BEGIN
	INSERT INTO [GlobalSettings] ([Guid], [Key], Value) VALUES ('F95AB72E-E79C-4885-B6DB-D1F648543051', 'Version', '1.46')
END
ELSE
BEGIN
	UPDATE [GlobalSettings] SET Value = '1.46' WHERE [Guid] = 'F95AB72E-E79C-4885-B6DB-D1F648543051'
END

GO

IF NOT EXISTS (SELECT [Key] FROM [GlobalSettings] WHERE [Guid] = 'F0FA898A-4E41-4772-B82F-238BACEFF025')
BEGIN
	INSERT INTO [GlobalSettings] ([Guid], [Key], Value) VALUES ('F0FA898A-4E41-4772-B82F-238BACEFF025', 'UpdateSyncScopes', '')
END

IF NOT EXISTS (SELECT [Key] FROM [GlobalSettings] WHERE [Guid] = 'FF021C56-8342-4BF6-A749-10FC0745EB91')
BEGIN
	INSERT INTO [GlobalSettings] ([Guid], [Key], Value) VALUES ('FF021C56-8342-4BF6-A749-10FC0745EB91','NumberOfPreviousPasswordsToCheck', '0')
END

IF NOT EXISTS (SELECT [Key] FROM [GlobalSettings] WHERE [Guid] = '4C98056A-CFF0-438A-A9C1-9126A5453579')
BEGIN
	INSERT INTO [GlobalSettings] ([Guid], [Key], Value) VALUES ('4C98056A-CFF0-438A-A9C1-9126A5453579','ContactEmail', 'pcm@pwc.de')
END

PRINT 'Updating the static data...'

:r .\populate_static_data.sql

-- Drop the default from Currencies.IsoCode column - it was there only for the update to succeed.
IF OBJECT_ID('DF_Currencies_IsoCode', 'D') IS NOT NULL
BEGIN
	PRINT 'Dropping [DF_Currencies_IsoCode]...'
	ALTER TABLE [Currencies] DROP CONSTRAINT [DF_Currencies_IsoCode]
END

/***** Generate ISO codes for custom currencies *****/
/* If there Currencies table contains any custom currencies (that are not part of the master data; usually they are created by an Admin user),
   we have to generate unique ISO codes for them and so, we use their symbol as ISO code. In pre-deployment the symbols are checked for validity and if one of them is not valid the deployment process will fail.
   This is done because we need the same ISO codes on both databases.
   By default all custom currencies were assigned the ISO code '000' during the schema update. */

UPDATE Currencies
	SET IsoCode = Symbol
	WHERE IsoCode = '000'
/****************************************************/

GO


PRINT 'Updating the custom error messages...'
:r .\error_messages.sql
GO

PRINT 'Rebuilding database indexes...'
:r .\rebuild_indexes.sql
GO