﻿/*
  Pre-Deployment script common for all databases.
*/

-- Turn off the database's auto-close option
ALTER DATABASE $(DatabaseName) SET AUTO_CLOSE OFF
PRINT 'Turned off the AUTO_CLOSE database option.'

GO

-- Kill All Active Connections To A Database So The Read_Commited_Snapshot = ON can be set.
ALTER DATABASE $(DatabaseName) SET SINGLE_USER WITH ROLLBACK IMMEDIATE
PRINT 'Set single_user.'
ALTER DATABASE $(DatabaseName) SET ALLOW_SNAPSHOT_ISOLATION ON
PRINT 'Turned on the ALLOW_SNAPSHOT_ISOLATION database option.'
ALTER DATABASE $(DatabaseName) SET READ_COMMITTED_SNAPSHOT ON
PRINT 'Turned on the READ_COMMITTED_SNAPSHOT database option.'
ALTER DATABASE $(DatabaseName) SET MULTI_USER
PRINT 'Set multi_user.'

GO

IF EXISTS (SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE table_name = 'DataBaseInfo' AND column_name = 'Version')
BEGIN
	-- Truncating the DataBaseInfo table is necessary when upgrading from the db version, 1.15 in which the table was not a key-value bucket
	-- and instead had a column named 'Version' which contained the db version.
	PRINT 'Truncating the DataBaseInfo table...'
	USE $(DatabaseName)
	TRUNCATE TABLE DataBaseInfo
END

GO

/***** Check currencies symbol for validity *****/
/* Check if the currencies symbol (except euro, dollar and pound) is unique and with three characters because in post-deployment will set their symbol as ISO code and is needs to be unique. */

IF EXISTS (SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE table_name = 'Currencies')
	AND NOT EXISTS (SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE table_name = 'Currencies' AND column_name = 'IsoCode')
BEGIN
	DECLARE @isCurrenciesSymbolValid BIT
	SET @isCurrenciesSymbolValid = 0

	SELECT @isCurrenciesSymbolValid = COUNT(*)
		FROM Currencies
		WHERE Name NOT IN ('Euro', 'US Dollar') AND LEN(Symbol) != 3
		GROUP BY Symbol

	IF @isCurrenciesSymbolValid = 1
	BEGIN
		RAISERROR('Not all the currencies have a valid symbol. The db upgrade cannot continue unless they are all unique and have exactly three alphanumeric characters.', 11, 1)
	END
END
/****************************************************/