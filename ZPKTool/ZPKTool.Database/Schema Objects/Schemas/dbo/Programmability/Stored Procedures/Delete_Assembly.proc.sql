﻿
CREATE PROCEDURE Delete_Assembly
	@assemblyId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##AssemblyIds', 'U') IS NOT NULL
		DROP TABLE ##AssemblyIds

	CREATE TABLE ##AssemblyIds
	(
		Guid uniqueidentifier,
		ProcessGuid uniqueidentifier,
		OverheadSettingsGuid uniqueidentifier,
		CountrySettingsGuid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##AssemblyMediaIds', 'U') IS NOT NULL
		DROP TABLE ##AssemblyMediaIds

	CREATE TABLE ##AssemblyMediaIds
	(
		Guid uniqueidentifier
	)

	IF @assemblyId IS NULL
	BEGIN
		DECLARE @projectId uniqueidentifier

		DECLARE ProjectIdsCursor CURSOR FAST_FORWARD FOR
			SELECT Guid
				FROM ##ProjectIds

		OPEN ProjectIdsCursor
		
		FETCH NEXT FROM ProjectIdsCursor INTO @projectId
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO ##AssemblyIds (Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
				SELECT Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid
						FROM GetAssemblyHierarchyFromProject(@projectId)

				FETCH NEXT FROM ProjectIdsCursor INTO @projectId
			END

		CLOSE ProjectIdsCursor 
		DEALLOCATE ProjectIdsCursor 
	END
	ELSE
	BEGIN
		INSERT INTO ##AssemblyIds (Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
			SELECT Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid
				FROM GetAssemblyHierarchyFromAssembly(@assemblyId)
	END

	INSERT INTO ##AssemblyMediaIds (Guid)
		SELECT MediaGuid
			FROM AssemblyMedia am
			WHERE am.AssemblyGuid IN (SELECT Guid
											FROM ##AssemblyIds)
	
	EXEC Delete_Part NULL

	DELETE
		FROM AssemblyMedia
		WHERE AssemblyGuid IN (SELECT Guid
									FROM ##AssemblyIds)

	EXEC Delete_Media NULL, '##AssemblyMediaIds'

	DELETE
		FROM Assemblies
		WHERE Guid IN (SELECT Guid
							FROM ##AssemblyIds)

	EXEC Delete_Process NULL, 'True'
	EXEC Delete_OverheadSetting NULL, '##AssemblyIds'

	DELETE
		FROM CountrySettings
		WHERE Guid IN (SELECT CountrySettingsGuid
							FROM ##AssemblyIds)

	EXEC Delete_Manufacturer NULL, '##AssemblyIds'

	DROP TABLE ##AssemblyIds
	DROP TABLE ##AssemblyMediaIds
END