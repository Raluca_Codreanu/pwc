﻿
CREATE PROCEDURE Delete_ProjectFolder
	@projectFolderId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##ProjectFolderIds', 'U') IS NOT NULL
		DROP TABLE ##ProjectFolderIds

	CREATE TABLE ##ProjectFolderIds
	(
		Guid uniqueidentifier
	)

	INSERT INTO ##ProjectFolderIds (Guid)
		SELECT Guid
			FROM GetProjectFolderHierarchy(@projectFolderId)
	
	EXEC Delete_Project NULL

	DELETE
		FROM ProjectFolders
		WHERE Guid IN (SELECT Guid
							FROM ##ProjectFolderIds)

	DROP TABLE ##ProjectFolderIds
END