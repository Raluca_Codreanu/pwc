﻿
CREATE PROCEDURE Delete_ProcessStep
	@processStepId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##ProcessStepIds', 'U') IS NOT NULL
		DROP TABLE ##ProcessStepIds

	CREATE TABLE ##ProcessStepIds
	(
		Guid uniqueidentifier
	)
	
	IF OBJECT_ID('tempdb..##ProcessStepMediaIds', 'U') IS NOT NULL
		DROP TABLE ##ProcessStepMediaIds

	CREATE TABLE ##ProcessStepMediaIds
	(
		Guid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##ProcessIds', 'U') IS NOT NULL
	BEGIN
		INSERT INTO ##ProcessStepIds (Guid)
			SELECT ps.Guid
				FROM ProcessSteps ps
					INNER JOIN ##ProcessIds p ON p.Guid = ps.ProcessGuid
	END
	ELSE
	BEGIN
		INSERT INTO ##ProcessStepIds (Guid)
			VALUES (@processStepId)
	END

	INSERT INTO ##ProcessStepMediaIds (Guid)
		SELECT Guid
			FROM Media
			WHERE Guid IN (SELECT Guid
								FROM ##ProcessStepIds)

	EXEC Delete_Die NULL
	EXEC Delete_Machine NULL
	EXEC Delete_Consumable NULL
	EXEC Delete_Commodity NULL
	EXEC Delete_CycleTimeCalculation NULL, '##ProcessStepIds'

	DELETE
		FROM ProcessStepAssemblyAmounts
		WHERE ProcessStepGuid IN (SELECT Guid
							FROM ##ProcessStepIds)

	DELETE
		FROM ProcessStepPartAmounts
		WHERE ProcessStepGuid IN (SELECT Guid
							FROM ##ProcessStepIds)

	DELETE
		FROM ProcessSteps
		WHERE Guid IN (SELECT Guid
							FROM ##ProcessStepIds)
	
	EXEC Delete_Media NULL, '##ProcessStepMediaIds'

	DROP TABLE ##ProcessStepIds
	DROP TABLE ##ProcessStepMediaIds
END