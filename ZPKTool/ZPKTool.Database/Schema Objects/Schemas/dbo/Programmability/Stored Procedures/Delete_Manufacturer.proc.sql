﻿
CREATE PROCEDURE Delete_Manufacturer
	@manufacturerId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM Manufacturers WHERE Guid IN (SELECT ManufacturerGuid FROM ' + @tableName + ' WHERE ManufacturerGuid IS NOT NULL)'
		EXEC (@instr)
	END

	IF @manufacturerId IS NOT NULL
	BEGIN
		DELETE
			FROM Manufacturers
			WHERE Guid = @manufacturerId
	END
END