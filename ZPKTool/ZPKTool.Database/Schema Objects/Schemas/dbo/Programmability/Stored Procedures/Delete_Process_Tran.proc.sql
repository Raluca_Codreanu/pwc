﻿
CREATE PROCEDURE Delete_Process_Tran
	@processId uniqueidentifier,
	@parentIsAssembly bit
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_Process @processId, NULL

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the process with id "' + CAST(@processId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END