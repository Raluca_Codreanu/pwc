﻿
CREATE PROCEDURE Delete_OverheadSetting
	@overheadSettingId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM OverheadSettings WHERE Guid IN (SELECT OverheadSettingsGuid FROM ' + @tableName + ')'
		EXEC (@instr)
	END

	IF @overheadSettingId IS NOT NULL
	BEGIN
		DELETE
			FROM OverheadSettings
			WHERE Guid = @overheadSettingId
	END
END