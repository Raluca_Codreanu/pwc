﻿
CREATE PROCEDURE Delete_Machine
	@machineId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##MachineIds', 'U') IS NOT NULL
		DROP TABLE ##MachineIds

	CREATE TABLE ##MachineIds
	(
		Guid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##MachineMediaIds', 'U') IS NOT NULL
		DROP TABLE ##MachineMediaIds

	CREATE TABLE ##MachineMediaIds
	(
		Guid uniqueidentifier
	)

	IF @machineId IS NULL
	BEGIN
		INSERT INTO ##MachineIds (Guid, ManufacturerGuid)
			SELECT m.Guid, m.ManufacturerGuid
				FROM Machines m
					INNER JOIN ##ProcessStepIds ps ON m.ProcessStepGuid = ps.Guid
	END
	ELSE
	BEGIN
		INSERT INTO ##MachineIds (Guid, ManufacturerGuid)
			SELECT m.Guid, m.ManufacturerGuid
				FROM Machines m
				WHERE m.Guid = @machineId
	END

	INSERT INTO ##MachineMediaIds (Guid)
		SELECT Guid
			FROM Media
			WHERE Guid IN (SELECT Guid
								FROM ##MachineIds)
	
	-- This is used to set all the ManufacturerGuid's that are shared between Machines to NULL, so those Manufacturers are not deleted.
	UPDATE ##MachineIds
		SET ManufacturerGuid = NULL
		WHERE ManufacturerGuid IN (SELECT ManufacturerGuid
										FROM Machines
										GROUP BY ManufacturerGuid
										HAVING (COUNT(ManufacturerGuid) != 1))

	DELETE
		FROM Machines
		WHERE Guid IN (SELECT Guid
							FROM ##MachineIds)
	
	EXEC Delete_Manufacturer NULL, '##MachineIds'
	EXEC Delete_Media NULL, '##MachineMediaIds'

	DROP TABLE ##MachineIds
	DROP TABLE ##MachineMediaIds
END