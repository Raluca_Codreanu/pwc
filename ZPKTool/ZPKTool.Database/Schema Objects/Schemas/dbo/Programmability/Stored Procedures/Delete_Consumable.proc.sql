﻿
CREATE PROCEDURE Delete_Consumable
	@consumableId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##ConsumableIds', 'U') IS NOT NULL
		DROP TABLE ##ConsumableIds

	CREATE TABLE ##ConsumableIds
	(
		Guid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF @consumableId IS NULL
	BEGIN
		INSERT INTO ##ConsumableIds (Guid, ManufacturerGuid)
			SELECT c.Guid, c.ManufacturerGuid
				FROM Consumables c
					INNER JOIN ##ProcessStepIds ps ON c.ProcessStepGuid = ps.Guid
	END
	ELSE
	BEGIN
		INSERT INTO ##ConsumableIds (Guid, ManufacturerGuid)
			SELECT c.Guid, c.ManufacturerGuid
				FROM Consumables c
				WHERE c.Guid = @consumableId
	END

	DELETE
		FROM ConsumablesPriceHistory
		WHERE ConsumableGuid IN (SELECT Guid
							FROM ##ConsumableIds)

	DELETE
		FROM Consumables
		WHERE Guid IN (SELECT Guid
							FROM ##ConsumableIds)

	EXEC Delete_Manufacturer NULL, '##ConsumableIds'

	DROP TABLE ##ConsumableIds
END