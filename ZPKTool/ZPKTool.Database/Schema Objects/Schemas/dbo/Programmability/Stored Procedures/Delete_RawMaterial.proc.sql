﻿
CREATE PROCEDURE Delete_RawMaterial
	@rawMaterialId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##RawMaterialIds', 'U') IS NOT NULL
		DROP TABLE ##RawMaterialIds

	CREATE TABLE ##RawMaterialIds
	(
		Guid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##RawMaterialMediaIds', 'U') IS NOT NULL
		DROP TABLE ##RawMaterialMediaIds

	CREATE TABLE ##RawMaterialMediaIds
	(
		Guid uniqueidentifier
	)

	IF @rawMaterialId IS NULL
	BEGIN
		INSERT INTO ##RawMaterialIds (Guid, ManufacturerGuid)
			SELECT rm.Guid, rm.ManufacturerGuid
				FROM RawMaterials rm
					INNER JOIN ##PartIds p ON rm.PartGuid = p.Guid
	END
	ELSE
	BEGIN
		INSERT INTO ##RawMaterialIds (Guid, ManufacturerGuid)
			SELECT rm.Guid, rm.ManufacturerGuid
				FROM RawMaterials rm
				WHERE rm.Guid = @rawMaterialId
	END

	INSERT INTO ##RawMaterialMediaIds (Guid)
		SELECT Guid
			FROM Media
			WHERE Guid IN (SELECT Guid
								FROM ##RawMaterialIds)

	DELETE
		FROM RawMaterialsPriceHistory
		WHERE RawMaterialGuid IN (SELECT Guid
										FROM ##RawMaterialIds)

	DELETE
		FROM RawMaterials
		WHERE Guid IN (SELECT Guid
							FROM ##RawMaterialIds)
	
	EXEC Delete_Media NULL, '##RawMaterialMediaIds'
	EXEC Delete_Manufacturer NULL, '##RawMaterialIds'

	DROP TABLE ##RawMaterialMediaIds
	DROP TABLE ##RawMaterialIds
END