﻿
CREATE PROCEDURE Delete_CycleTimeCalculation_Tran
	@cycleTimeCalculationId uniqueidentifier
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_CycleTimeCalculation @cycleTimeCalculationId, NULL

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the cycle time calculation with id "' + CAST(@cycleTimeCalculationId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END