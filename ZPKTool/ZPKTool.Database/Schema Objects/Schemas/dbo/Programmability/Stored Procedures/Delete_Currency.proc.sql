﻿CREATE PROCEDURE Delete_Currency
	@currencyId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM Currencies WHERE Guid IN (SELECT Guid FROM ' + @tableName + ')'
		EXEC (@instr)
	END

	IF @currencyId IS NOT NULL
	BEGIN
		DELETE
			FROM Currencies
			WHERE Guid = @currencyId
	END
END