﻿
CREATE PROCEDURE Delete_Customer_Tran
	@customerId uniqueidentifier
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_Customer @customerId, NULL

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the customer with id "' + CAST(@customerId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END