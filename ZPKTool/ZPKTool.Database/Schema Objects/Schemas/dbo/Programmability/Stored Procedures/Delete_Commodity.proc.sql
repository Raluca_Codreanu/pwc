﻿
CREATE PROCEDURE Delete_Commodity
	@commodityId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##CommodityIds', 'U') IS NOT NULL
		DROP TABLE ##CommodityIds

	CREATE TABLE ##CommodityIds
	(
		Guid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##CommodityMediaIds', 'U') IS NOT NULL
		DROP TABLE ##CommodityMediaIds

	CREATE TABLE ##CommodityMediaIds
	(
		Guid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##PartIds', 'U') IS NOT NULL
	BEGIN
		INSERT INTO ##CommodityIds (Guid, ManufacturerGuid)
			SELECT c.Guid, c.ManufacturerGuid
				FROM Commodities c
					INNER JOIN ##PartIds p ON c.PartGuid = p.Guid
	END

	IF OBJECT_ID('tempdb..##ProcessStepIds', 'U') IS NOT NULL
	BEGIN
		INSERT INTO ##CommodityIds (Guid, ManufacturerGuid)
			SELECT c.Guid, c.ManufacturerGuid
				FROM Commodities c
					INNER JOIN ##ProcessStepIds ps ON c.ProcessStepGuid = ps.Guid
	END

	IF @commodityId IS NOT NULL
	BEGIN
		INSERT INTO ##CommodityIds (Guid, ManufacturerGuid)
			SELECT c.Guid, c.ManufacturerGuid
				FROM Commodities c
				WHERE c.Guid = @commodityId
	END

	INSERT INTO ##CommodityMediaIds (Guid)
		SELECT Guid
			FROM Media
			WHERE Guid IN (SELECT Guid
								FROM ##CommodityIds)

	DELETE
		FROM CommoditiesPriceHistory
		WHERE CommodityGuid IN (SELECT Guid
									FROM ##CommodityIds)

	DELETE
		FROM Commodities
		WHERE Guid IN (SELECT Guid
							FROM ##CommodityIds)
	
	EXEC Delete_Media NULL, '##CommodityMediaIds'
	EXEC Delete_Manufacturer NULL, '##CommodityIds'

	DROP TABLE ##CommodityMediaIds
	DROP TABLE ##CommodityIds
END