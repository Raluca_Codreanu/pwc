﻿
CREATE PROCEDURE Delete_Consumable_Tran
	@consumableId uniqueidentifier
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_Consumable @consumableId

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the consumable with id "' + CAST(@consumableId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END