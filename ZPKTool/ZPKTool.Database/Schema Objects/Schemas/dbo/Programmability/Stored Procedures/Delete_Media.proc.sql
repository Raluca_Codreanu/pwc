﻿
CREATE PROCEDURE Delete_Media
	@mediaId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM Media WHERE Guid IN (SELECT Guid FROM ' + @tableName + ')'
		EXEC (@instr)
	END

	IF @mediaId IS NOT NULL
	BEGIN
		DELETE
			FROM Media
			WHERE Guid = @mediaId
	END
END