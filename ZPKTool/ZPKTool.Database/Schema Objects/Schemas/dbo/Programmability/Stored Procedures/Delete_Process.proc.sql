﻿
CREATE PROCEDURE Delete_Process
	@processId uniqueidentifier,
	@parentIsAssembly bit
AS
BEGIN
	IF OBJECT_ID('tempdb..##ProcessStepIds', 'U') IS NOT NULL
		DROP TABLE ##ProcessStepIds

	CREATE TABLE ##ProcessIds
	(
		Guid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##AssemblyIds', 'U') IS NOT NULL AND @parentIsAssembly = 'True'
	BEGIN
		INSERT INTO ##ProcessIds (Guid)
			SELECT ProcessGuid
				FROM ##AssemblyIds
	END

	IF OBJECT_ID('tempdb..##PartIds', 'U') IS NOT NULL AND @parentIsAssembly = 'False'
	BEGIN
		INSERT INTO ##ProcessIds (Guid)
			SELECT ProcessGuid
				FROM ##PartIds
	END

	IF @processId IS NOT NULL
	BEGIN
		INSERT INTO ##ProcessIds (Guid)
			VALUES (@processId)
	END

	EXEC Delete_ProcessStep NULL

	DELETE
		FROM Processes
		WHERE Guid IN (SELECT Guid
							FROM ##ProcessIds)

	DROP TABLE ##ProcessIds
END