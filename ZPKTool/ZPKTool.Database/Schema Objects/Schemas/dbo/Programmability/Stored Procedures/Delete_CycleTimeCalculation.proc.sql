﻿
CREATE PROCEDURE Delete_CycleTimeCalculation
	@cycleTimeCalculationId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM CycleTimeCalculations WHERE ProcessStepGuid IN (SELECT Guid FROM ' + @tableName + ')'
		EXEC (@instr)
	END

	IF @cycleTimeCalculationId IS NOT NULL
	BEGIN
		DELETE
			FROM CycleTimeCalculations
			WHERE ProcessStepGuid IN (SELECT Guid
											FROM ##ProcessStepIds)
	END

	DELETE
		FROM CycleTimeCalculations
		WHERE Guid = @cycleTimeCalculationId
END