﻿
CREATE PROCEDURE Delete_Part
	@partId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##PartIds', 'U') IS NOT NULL
		DROP TABLE ##PartIds

	CREATE TABLE ##PartIds
	(
		Guid uniqueidentifier,
		RawPartId uniqueidentifier,
		ProcessGuid uniqueidentifier,
		OverheadSettingsGuid uniqueidentifier,
		CountrySettingsGuid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##PartMediaIds', 'U') IS NOT NULL
		DROP TABLE ##PartMediaIds

	CREATE TABLE ##PartMediaIds
	(
		Guid uniqueidentifier
	)

	DECLARE @IsRawPart bit
	SET @IsRawPart = 'False'
	
	IF @partId IS NULL
	BEGIN
		IF OBJECT_ID('tempdb..##ProjectIds', 'U') IS NOT NULL
		BEGIN
			INSERT INTO ##PartIds (Guid, RawPartId, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
				SELECT p.Guid, p.RawPartId, p.ProcessGuid, p.OverheadSettingsGuid, p.CountrySettingsGuid, p.ManufacturerGuid
					FROM Parts p
						INNER JOIN ##ProjectIds pr ON pr.Guid = p.ParentProjectGuid
		END

		INSERT INTO ##PartIds (Guid, RawPartId, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
			SELECT p.Guid, p.RawPartId, p.ProcessGuid, p.OverheadSettingsGuid, p.CountrySettingsGuid, p.ManufacturerGuid
				FROM Parts p
					INNER JOIN ##AssemblyIds a ON a.Guid = p.ParentAssemblyGuid
	END
	ELSE
	BEGIN
		INSERT INTO ##PartIds (Guid, RawPartId, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
			SELECT p.Guid, p.RawPartId, p.ProcessGuid, p.OverheadSettingsGuid, p.CountrySettingsGuid, p.ManufacturerGuid
				FROM Parts p
				WHERE p.Guid = @partId
				
		SELECT @IsRawPart = IsRawPart
			FROM Parts
			WHERE Guid = @partId
	END

	-- Insert the raw parts of the inserted parts
	INSERT INTO ##PartIds (Guid, RawPartId, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid)
		SELECT p.Guid, p.RawPartId, p.ProcessGuid, p.OverheadSettingsGuid, p.CountrySettingsGuid, p.ManufacturerGuid
			FROM Parts p
				INNER JOIN ##PartIds pp ON pp.RawPartId = p.Guid

	INSERT INTO ##PartMediaIds (Guid)
		SELECT MediaGuid
			FROM PartMedia
			WHERE PartGuid IN (SELECT Guid
									FROM ##PartIds)
	
	EXEC Delete_Commodity NULL
	EXEC Delete_RawMaterial NULL

	DELETE
		FROM PartMedia
		WHERE PartGuid IN (SELECT Guid
								FROM ##PartIds)

	EXEC Delete_Media NULL, '##PartMediaIds'
	
	IF @IsRawPart = 'True'
	BEGIN
		UPDATE Parts
			SET RawPartId = NULL
			WHERE RawPartId = @partId
	END
	
	DELETE
		FROM Parts
		WHERE Guid IN (SELECT Guid
							FROM ##PartIds)

	EXEC Delete_Process NULL, 'False'
	EXEC Delete_OverheadSetting NULL, '##PartIds'

	DELETE
		FROM CountrySettings
		WHERE Guid IN (SELECT CountrySettingsGuid
							FROM ##PartIds)

	EXEC Delete_Manufacturer NULL, '##PartIds'

	DROP TABLE ##PartIds
	DROP TABLE ##PartMediaIds
END