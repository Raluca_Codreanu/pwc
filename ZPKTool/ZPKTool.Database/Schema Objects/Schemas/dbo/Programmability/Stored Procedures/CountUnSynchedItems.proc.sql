﻿CREATE PROCEDURE [dbo].[CountUnSynchedItems]
	@userId uniqueidentifier,
	@unSynchedCount int OUTPUT
AS
BEGIN
	DECLARE @syncScopeName nvarchar(150)
	DECLARE @baseTimestamp bigint
	DECLARE @xml xml
	DECLARE @tableName nvarchar(100)
	DECLARE @instr nvarchar(MAX)

	SET @syncScopeName = 'MyProjects-' + CONVERT(nvarchar(36), @userId)
	SET @unSynchedCount = 0

	-- Get the base timestamp.
	SELECT @baseTimestamp = scope_timestamp
		FROM scope_info
		WHERE sync_scope_name = @syncScopeName

	-- Get the xml that contains the tables that are used when synchronizing MyProjects.
	SELECT @xml = parameter_data
		FROM scope_parameters
		WHERE sync_scope_name = @syncScopeName

	-- Create a cursor that contains the table names that need to be checked.
	DECLARE TrackingTables_Cursor CURSOR FAST_FORWARD FOR 
		SELECT REPLACE(attributes.attribute.value('.', 'nvarchar(MAX)'), ']','_tracking]') MyProjectsTables
			FROM @xml.nodes('//SqlSyncProviderScopeParameters/AdapterParams/@name')Attributes (Attribute)

	OPEN TrackingTables_Cursor

	-- For each table count the number of rows that are not synchronized.
	FETCH NEXT FROM TrackingTables_Cursor INTO @tableName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF EXISTS (SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE table_name = @tableName AND column_name = 'OwnerGuid')
		BEGIN
			SET @instr = 'SELECT @unSynchedCount = @unSynchedCount + COUNT(*) FROM ' + @tableName + ' WHERE local_update_peer_timestamp > ' + CONVERT(nvarchar(100), @baseTimestamp) + ' AND OwnerGuid = ''' + CONVERT(nvarchar(36), @userId) + ''''
		END
		ELSE
		BEGIN
			SET @instr = 'SELECT @unSynchedCount = @unSynchedCount + COUNT(*) FROM ' + @tableName + ' WHERE local_update_peer_timestamp > ' + CONVERT(nvarchar(100), @baseTimestamp)			
		END

		EXEC SP_EXECUTESQL @instr, N'@unSynchedCount int OUTPUT', @unSynchedCount OUTPUT

		FETCH NEXT FROM TrackingTables_Cursor INTO @tableName
	END

	CLOSE TrackingTables_Cursor
	DEALLOCATE TrackingTables_Cursor
END