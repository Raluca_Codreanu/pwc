﻿
CREATE PROCEDURE Delete_Die
	@dieId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##DieIds', 'U') IS NOT NULL
		DROP TABLE ##DieIds

	CREATE TABLE ##DieIds
	(
		Guid uniqueidentifier,
		ManufacturerGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##DieMediaIds', 'U') IS NOT NULL
		DROP TABLE ##DieMediaIds

	CREATE TABLE ##DieMediaIds
	(
		Guid uniqueidentifier
	)

	IF @dieId IS NULL
	BEGIN
		INSERT INTO ##DieIds (Guid, ManufacturerGuid)
			SELECT d.Guid, d.ManufacturerGuid
				FROM Dies d
					INNER JOIN ##ProcessStepIds ps ON d.ProcessStepGuid = ps.Guid
	END
	ELSE
	BEGIN
		INSERT INTO ##DieIds (Guid, ManufacturerGuid)
			SELECT d.Guid, d.ManufacturerGuid
				FROM Dies d
				WHERE d.Guid = @dieId
	END

	INSERT INTO ##DieMediaIds (Guid)
		SELECT Guid
			FROM Media
			WHERE Guid IN (SELECT Guid
								FROM ##DieIds)

	DELETE
		FROM Dies
		WHERE Guid IN (SELECT Guid
							FROM ##DieIds)

	EXEC Delete_Manufacturer NULL, '##DieIds'
	EXEC Delete_Media NULL, '##DieMediaIds'
	
	DROP TABLE ##DieIds
	DROP TABLE ##DieMediaIds
END