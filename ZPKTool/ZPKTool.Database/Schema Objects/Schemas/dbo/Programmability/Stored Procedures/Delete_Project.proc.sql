﻿
CREATE PROCEDURE Delete_Project
	@projectId uniqueidentifier
AS
BEGIN
	IF OBJECT_ID('tempdb..##ProjectIds', 'U') IS NOT NULL
		DROP TABLE ##ProjectIds

	CREATE TABLE ##ProjectIds
	(
		Guid uniqueidentifier,
		FolderGuid uniqueidentifier,
		CustomerGuid uniqueidentifier,
		OverheadSettingsGuid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##ProjectMediaIds', 'U') IS NOT NULL
		DROP TABLE ##ProjectMediaIds

	CREATE TABLE ##ProjectMediaIds
	(
		Guid uniqueidentifier
	)

	IF OBJECT_ID('tempdb..##ProjectCurrenciesIds', 'U') IS NOT NULL
		DROP TABLE ##ProjectCurrenciesIds

	CREATE TABLE ##ProjectCurrenciesIds
	(
		Guid uniqueidentifier
	)

	IF @projectId IS NULL
		BEGIN
			INSERT INTO ##ProjectIds (Guid, FolderGuid, CustomerGuid, OverheadSettingsGuid)
				SELECT Guid, FolderGuid, CustomerGuid, OverheadSettingsGuid
					FROM Projects
					WHERE FolderGuid IN (SELECT Guid
											FROM ##ProjectFolderIds)
		END
	ELSE
		BEGIN
			INSERT INTO ##ProjectIds (Guid, FolderGuid, CustomerGuid, OverheadSettingsGuid)
				SELECT Guid, FolderGuid, CustomerGuid, OverheadSettingsGuid
					FROM Projects
					WHERE Guid = @projectId
		END

	INSERT INTO ##ProjectMediaIds (Guid)
		SELECT MediaGuid
			FROM ProjectMedia
			WHERE ProjectGuid IN (SELECT Guid
									FROM ##ProjectIds)
	
		INSERT INTO ##ProjectCurrenciesIds (Guid)
		SELECT CurrencyId
			FROM ProjectCurrencies
			WHERE ProjectId IN (SELECT Guid
									FROM ##ProjectIds)

	EXEC Delete_Assembly NULL

	DELETE
		FROM ProjectMedia
		WHERE ProjectGuid IN (SELECT Guid
								FROM ##ProjectIds)

	EXEC Delete_Media NULL, '##ProjectMediaIds'

	DELETE
		FROM ProjectCurrencies
		WHERE ProjectId IN (SELECT Guid
								FROM ##ProjectIds)

	DELETE
		FROM Projects
		WHERE Guid IN (SELECT Guid
							FROM ##ProjectIds)

	EXEC Delete_Currency NULL, '##ProjectCurrenciesIds'
	EXEC Delete_Customer NULL, '##ProjectIds'
	EXEC Delete_OverheadSetting NULL, '##ProjectIds'

	DROP TABLE ##ProjectIds
	DROP TABLE ##ProjectMediaIds
	DROP TABLE ##ProjectCurrenciesIds
END