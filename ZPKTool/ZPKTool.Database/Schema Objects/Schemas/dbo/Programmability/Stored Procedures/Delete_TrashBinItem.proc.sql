﻿
CREATE PROCEDURE Delete_TrashBinItem
	@trashBinItemId uniqueidentifier
AS
BEGIN
	DELETE
		FROM TrashBinItems
		WHERE Guid = @trashBinItemId
END