﻿/*****************************************************************************************************
	Selects the ids of the entities from a specified table where the user is Responsible Calculator
	or Project Leader on the entity parent project, or if the user has Admin role, the ids of the
	entities where his not the owner, are not master data and are not deleted.

	Parameters:
		@UserId: the Id of the user for which the search is performed
		@TableName: the name of the table where to search
******************************************************************************************************/

CREATE PROCEDURE [dbo].[OtherUsersEntitiesId]
	@UserId uniqueidentifier,
	@IsAdmin bit,
	@TableName nvarchar(50)
AS
BEGIN
	DECLARE @CrtId uniqueidentifier
	DECLARE @ResponsableCalculatorId uniqueidentifier
	DECLARE @ProjectLeaderId uniqueidentifier
	DECLARE	@ParentId uniqueidentifier
	DECLARE	@UserRoleId uniqueidentifier
	DECLARE @Results TABLE 
	(
	    Id uniqueidentifier 
	)
	
	-- If the user has the Admin role
	IF (@IsAdmin = 'True')
	BEGIN
		IF (@TableName = 'Projects')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Projects
					WHERE OwnerGuid != @UserId
		END
		ELSE IF (@TableName = 'Assemblies')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Assemblies
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'Parts')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Parts
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'ProcessSteps')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM ProcessSteps
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False'
		END
		ELSE IF (@TableName = 'Machines')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Machines
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'RawMaterials')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM RawMaterials
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'Commodities')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Commodities
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'Consumables')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Consumables
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
		ELSE IF (@TableName = 'Dies')
		BEGIN
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Dies
					WHERE OwnerGuid != @UserId AND OwnerGuid IS NOT NULL AND IsMasterData = 'False' AND IsDeleted = 'False'
		END
	END
	ELSE
	BEGIN
		IF (@TableName = 'Projects')
		BEGIN
			-- If the searched table is Projects we can directly get the ids associated with the user.
			INSERT INTO @Results (Id)
				SELECT Guid
					FROM Projects
					WHERE OwnerGuid != @UserId
						AND (ResponsableCalculatorGuid = @UserId OR ProjectLeaderGuid = @UserId)
		END
		ELSE
		BEGIN
			-- Create the cursor that contains the ids of the entities that need to be verified.
			IF (@TableName = 'Assemblies')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Assemblies
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'Parts')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Parts
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'ProcessSteps')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM ProcessSteps
						WHERE IsMasterData = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'Machines')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Machines
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'RawMaterials')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM RawMaterials
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'Commodities')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Commodities
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'Consumables')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Consumables
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END
			ELSE IF (@TableName = 'Dies')
			BEGIN
				DECLARE EntitiesId CURSOR FOR
					SELECT Guid
						FROM Dies
						WHERE IsMasterData = 'False' AND IsDeleted = 'False'
							AND OwnerGuid IS NOT NULL AND OwnerGuid != @UserId
			END

			--For each id we get the project id and see if it is associated with the user.
			OPEN EntitiesId
			FETCH NEXT FROM EntitiesId INTO @CrtId

			WHILE @@Fetch_Status = 0
			BEGIN
				EXEC [dbo].[FindTopParentId]
					@childId = @CrtId,
					@childTableName = @TableName,
					@parentType = 1,
					@parentId = @ParentId OUTPUT

				SELECT @ResponsableCalculatorId = ResponsableCalculatorGuid, @ProjectLeaderId = ProjectLeaderGuid
					FROM Projects
					WHERE OwnerGuid != @UserId
						AND Guid = @ParentId

			IF (@ResponsableCalculatorId = @UserId OR @ProjectLeaderId = @UserId)
			BEGIN
				INSERT INTO @Results (Id)
					SELECT @CrtId
			END

			FETCH NEXT FROM EntitiesId INTO @CrtId
			END
	
			CLOSE EntitiesId
			DEALLOCATE EntitiesId
		END
	END

	SELECT Id
		FROM @Results
END