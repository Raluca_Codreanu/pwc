﻿
CREATE PROCEDURE Delete_OverheadSetting_Tran
	@overheadSettingId uniqueidentifier
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_OverheadSetting @overheadSettingId, NULL

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the overhead setting with id "' + CAST(@overheadSettingId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END