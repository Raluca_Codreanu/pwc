﻿/*****************************************************************************************************
	Finds the Id of the top-most parent of a specified entity.
	The top-most parent is the project or the root assembly of a hierarchy.

	Parameters:
		@childId: the id of the entity whose top parent must be found
		@childTableName: the name of the table which stored the entity with the id specified
						 by the @childId parameter
		@parentType: the type of top parent to search for; the valid values are 1 for Project
					 and 2 for Assembly, any other value resulting in error
		@parentId: output parameter that will contain the id of the top parent, after it is found
******************************************************************************************************/

CREATE PROCEDURE FindTopParentId
(
	@childId uniqueidentifier,
	@childTableName varchar(50),
	@parentType tinyint,
	@parentId uniqueidentifier OUTPUT
)
AS

SET NOCOUNT ON

-- If the child table is Projects we don't need to go any higher to search for a parent.
IF (@childTableName = 'Projects')
	BEGIN
		SET @parentId = @childId

		RETURN
	END

IF (@parentType < 1 OR @parentType > 2)
BEGIN
	DECLARE @errorMsg nvarchar(150)
	SET @errorMsg = N'The value "' + CAST(@parentType as varchar(25)) + N'" for the @parentType parameter is not supported'
	RAISERROR(@errorMsg, 11, 1)
END

-- The id of the direct Project parent, for children that are direct children of a Project.
DECLARE @parentProjectId uniqueidentifier

-- The id of the direct Assembly parent (the 1st Assembly above the child in the hierarchy).
DECLARE @parentAssyId uniqueidentifier

-- The id of the direct Part parent (the 1st Part above the child in the hierarchy).
DECLARE @parentPartId uniqueidentifier

IF (@childTableName = 'Assemblies')
	BEGIN
		-- In case of an assembly we start the parent search at that assembly, not its parent.
		SET @parentAssyId = @childId
	END
ELSE IF (@childTableName = 'Parts')
	BEGIN
		SET @parentPartId = @childId
	END
ELSE IF (@childTableName = 'RawMaterials')
	BEGIN
		SELECT @parentPartId = PartGuid
			FROM RawMaterials
			WHERE Guid = @childId
	END
ELSE IF (@childTableName = 'Commodities')
	BEGIN
		-- A commodity is either directly into a Part or in the Process of an assembly or part.
		DECLARE @part1 uniqueidentifier
		DECLARE @part2 uniqueidentifier

		SELECT @parentAssyId = a.Guid, @part1 = p.Guid, @part2 = p2.Guid
			FROM Commodities c
				LEFT JOIN Parts p ON c.PartGuid = p.Guid
				LEFT JOIN ProcessSteps ps ON c.ProcessStepGuid = ps.Guid
				LEFT JOIN Processes pr ON ps.ProcessGuid = pr.Guid
				LEFT JOIN Assemblies a on pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p2 on pr.Guid = p2.ProcessGuid
			WHERE c.Guid = @childId
		
		IF (@part1 IS NOT NULL)
			SET @parentPartId = @part1
		ELSE IF (@part2 IS NOT NULL)
			SET @parentPartId = @part2
		END
ELSE IF (@childTableName = 'Consumables')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM Consumables c
				INNER JOIN ProcessSteps ps ON c.ProcessStepGuid = ps.Guid
				INNER JOIN Processes pr ON ps.ProcessGuid = pr.Guid
				LEFT JOIN Assemblies a ON pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p ON pr.Guid = p.ProcessGuid
			WHERE c.Guid = @childId
	END
ELSE IF (@childTableName = 'Dies')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM Dies d
				INNER JOIN ProcessSteps ps ON d.ProcessStepGuid = ps.Guid
				INNER JOIN Processes pr ON ps.ProcessGuid = pr.Guid
				LEFT JOIN Assemblies a ON pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p ON pr.Guid = p.ProcessGuid
		WHERE d.Guid = @childId
	END
ELSE IF (@childTableName = 'Machines')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM Machines m
				INNER JOIN ProcessSteps ps ON m.ProcessStepGuid = ps.Guid
				INNER JOIN Processes pr ON ps.ProcessGuid = pr.Guid
				LEFT JOIN Assemblies a ON pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p ON pr.Guid = p.ProcessGuid
			WHERE m.Guid = @childId
	END
ELSE IF (@childTableName = 'ProcessSteps')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM ProcessSteps ps
				INNER JOIN Processes pr ON ps.ProcessGuid = pr.Guid
				LEFT JOIN Assemblies a ON pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p ON pr.Guid = p.ProcessGuid
			WHERE ps.Guid = @childId
	END
ELSE IF (@childTableName = 'Processes')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM Processes pr
				LEFT JOIN Assemblies a ON pr.Guid = a.ProcessGuid
				LEFT JOIN Parts p ON pr.Guid = p.ProcessGuid
			WHERE pr.Guid = @childId
	END
ELSE IF (@childTableName = 'TransportCostCalculations')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM TransportCostCalculations t
				LEFT JOIN Assemblies a ON t.AssemblyId = a.Guid
				LEFT JOIN Parts p ON t.PartId = p.Guid
			WHERE t.Guid = @childId
	END
ELSE IF (@childTableName = 'TransportCostCalculationSettings')
	BEGIN
		SELECT @parentAssyId = a.Guid, @parentPartId = p.Guid
			FROM TransportCostCalculationSettings s
				INNER JOIN TransportCostCalculations t ON s.TransportCostCalculationId = t.Guid
				LEFT JOIN Assemblies a ON t.AssemblyId = a.Guid
				LEFT JOIN Parts p ON t.PartId = p.Guid
			WHERE s.Guid = @childId
	END
ELSE
	BEGIN
		DECLARE @errorMsg2 nvarchar(150)
		SET @errorMsg2 = N'The table name "' + @childTableName + N'" is not supported'
		RAISERROR(@errorMsg2, 11, 1)
END

-- Find the id of the part's direct parent.
IF(@parentPartId IS NOT NULL)
	BEGIN
		-- If @parentPartId identifies a part, the CTE below select the parent info from the part;
		-- otherwise, if it identifies a RawPart, the CTE navigates to its parent part before selecting the parent info.
		WITH PartHierarchy(PartId, ParentAssemblyId, ParentProjectId, Indx) AS
		(
			SELECT p.Guid, p.ParentAssemblyGuid, p.ParentProjectGuid, 1
				FROM Parts p
				WHERE p.Guid = @parentPartId
			UNION ALL
			SELECT p.Guid, p.ParentAssemblyGuid, p.ParentProjectGuid, Indx + 1
				FROM Parts p
					INNER JOIN PartHierarchy ph ON p.RawPartId = ph.PartId
		)

		SELECT TOP 1 @parentAssyId = ParentAssemblyId, @parentProjectId = ParentProjectId
			FROM PartHierarchy
			ORDER BY Indx DESC
	END

-- Find the id of the top level parent of the assembly.
IF (@parentAssyId IS NOT NULL)
	BEGIN
		DECLARE @assyRowCount int;
		WITH AssyHierarchy([Guid], ParentAssemblyGuid, Ind) AS
		(
			SELECT a.Guid, a.ParentAssemblyGuid, 1
				FROM Assemblies a
				WHERE a.Guid = @parentAssyId
			UNION ALL
			SELECT a.Guid, a.ParentAssemblyGuid, ah.Ind + 1
				FROM Assemblies a
					INNER JOIN AssyHierarchy ah ON a.Guid = ah.ParentAssemblyGuid
		)

		SELECT TOP 1 @parentAssyId = Guid, @assyRowCount = Ind
			FROM AssyHierarchy
			ORDER BY Ind DESC
	
		-- If the query returned no rows we should set @parentAssyId to null because the select doesn't do that.
		IF (@assyRowCount IS NULL)
			SET @parentAssyId = NULL
	END

-- @parentAssyId now contains the id of the top level assembly, if there is one.
IF (@parentType = 1)
	BEGIN
		IF (@parentProjectId IS NOT NULL)
			BEGIN
				SET @parentId = @parentProjectId
			END
		ELSE IF (@parentAssyId IS NOT NULL)
			BEGIN
				SELECT @parentId = p.Guid
					FROM Projects p
						INNER JOIN Assemblies a ON p.Guid = a.ProjectGuid
					WHERE a.Guid = @parentAssyId
			END
	END
ELSE IF(@parentType = 2)
	BEGIN
		-- If we are searching for the top assembly, its id is already in @parentAssyId.
		SET @parentId = @parentAssyId
	END
