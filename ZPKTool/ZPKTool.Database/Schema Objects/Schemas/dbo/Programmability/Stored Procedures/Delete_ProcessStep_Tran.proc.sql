﻿
CREATE PROCEDURE Delete_ProcessStep_Tran
	@processStepId uniqueidentifier
AS
BEGIN
	BEGIN TRANSACTION

	BEGIN TRY
		EXEC Delete_ProcessStep @processStepId

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION

		DECLARE @errorMsg nvarchar(MAX)
		SET @errorMsg = N'Error occured while trying to delete the process step with id "' + CAST(@processStepId as varchar(36)) + '. ' + ERROR_MESSAGE()
		RAISERROR(@errorMsg, 11, 1)
	END CATCH
END