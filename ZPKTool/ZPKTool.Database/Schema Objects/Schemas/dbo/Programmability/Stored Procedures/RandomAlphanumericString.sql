﻿/*****************************************************************************************************
	Generates a random string of a specified length, that contains only alphanumeric characters,
	both lower and upper case.

	Parameters:
		@length: the length of the string to generate; a value of zero generates an empty string.
		@randomString: an output parameter that contains the generated string
******************************************************************************************************/

CREATE PROCEDURE [dbo].[RandomAlphanumericString]
	@length int,
	@randomString varchar(MAX) OUTPUT
AS	
BEGIN	
	/* Define the pool from which each character of the random string is chosen	*/
	DECLARE @charPool char(62)
	DECLARE @charPoolLength int
	SET @charPool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	SET @charPoolLength = DataLength(@charPool)

	DECLARE @randomCharIndex INT
	DECLARE @loopCount int	
	SET @loopCount = 0
	SET @randomString = ''

	/* Randomly take from the characters pool a number of characters equal to the @length parameter and add them
	   to the @randomString parameter */
	WHILE (@loopCount < @length)
	BEGIN
		SET @randomCharIndex = ROUND(((@charPoolLength - 1) * RAND() + 1), 0)
		SET @randomString = @randomString + SUBSTRING(@charPool, @randomCharIndex, 1)
		SET @loopCount = @loopCount + 1	
	END	
END