﻿
CREATE PROCEDURE Delete_Customer
	@customerId uniqueidentifier,
	@tableName varchar(100)
AS
BEGIN
	IF @tableName IS NOT NULL
	BEGIN
		DECLARE @instr varchar(MAX)
		SET @instr = 'DELETE FROM Customers WHERE Guid IN (SELECT CustomerGuid FROM ' + @tableName + ')'
		EXEC (@instr)
	END

	IF @customerId IS NOT NULL
	BEGIN
		DELETE
			FROM Customers
			WHERE Guid = @customerId
	END
END