﻿
/* 
  TODO: this function does not return the hierarchy of a folder, it returns itself plus all children in reverse order.
  It is used for deleting a folder and its content, so its code can be added there and the function deleted.
*/ 
CREATE FUNCTION GetProjectFolderHierarchy(@projectFolderId uniqueidentifier)
RETURNS TABLE
AS
RETURN
(
	WITH ProjectFoldersHierarchy(Guid, Ind) AS
	(
		SELECT pf.Guid, 1
			FROM ProjectFolders pf
			WHERE pf.ParentGuid = @projectFolderId
		UNION ALL
		SELECT pf.Guid, pfh.Ind + 1
			FROM ProjectFolders pf
				INNER JOIN ProjectFoldersHierarchy pfh ON pf.ParentGuid = pfh.Guid
	)

	SELECT Guid, 0 AS Ind
		FROM ProjectFolders
		WHERE Guid = @projectFolderId
	UNION ALL
	SELECT TOP 100000 Guid, Ind
		FROM ProjectFoldersHierarchy
		ORDER BY Ind DESC
)