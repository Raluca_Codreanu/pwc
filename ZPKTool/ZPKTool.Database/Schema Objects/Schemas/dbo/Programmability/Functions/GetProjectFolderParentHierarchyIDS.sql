﻿/*
  This function returns the IDs of all parent folders of the folder with the @folderId ID.
  The 1st row in the returned table is the direct parent of the specified folder, the 2nd is the parent's parent, and so on up to the root folder.
*/
CREATE FUNCTION GetProjectFolderParentHierarchyIDS
(
	@folderId uniqueidentifier	
)
RETURNS TABLE
AS
RETURN
(
	WITH FolderHierarchy([Guid], ParentGuid) AS
	(
		SELECT pf1.[Guid], pf1.ParentGuid
			FROM ProjectFolders pf
				INNER JOIN ProjectFolders pf1 ON pf.ParentGuid = pf1.[Guid]
			WHERE pf.[Guid] = @folderId
		UNION ALL
		SELECT f.[Guid], f.ParentGuid
			FROM ProjectFolders f
				INNER JOIN FolderHierarchy fh ON f.[Guid] = fh.ParentGuid
	)

	SELECT Guid
		FROM FolderHierarchy
)
