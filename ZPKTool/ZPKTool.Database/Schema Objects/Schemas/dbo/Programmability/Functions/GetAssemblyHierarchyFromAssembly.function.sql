﻿
CREATE FUNCTION GetAssemblyHierarchyFromAssembly(@parentAssemblyId uniqueidentifier)
RETURNS TABLE
AS
RETURN
(
	WITH AssyHierarchy(Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid, Ind) AS
	(
		SELECT a.Guid, a.ProcessGuid, a.OverheadSettingsGuid, a.CountrySettingsGuid, a.ManufacturerGuid, 1
			FROM Assemblies a
			WHERE ParentAssemblyGuid = @parentAssemblyId
		UNION ALL
		SELECT a.Guid, a.ProcessGuid, a.OverheadSettingsGuid, a.CountrySettingsGuid, a.ManufacturerGuid, ah.Ind + 1
			FROM Assemblies a
				INNER JOIN AssyHierarchy ah ON a.ParentAssemblyGuid = ah.Guid
	)

	SELECT Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid, 0 AS Ind
		FROM Assemblies
		WHERE Guid = @parentAssemblyId
	UNION ALL
	SELECT TOP 100000 Guid, ProcessGuid, OverheadSettingsGuid, CountrySettingsGuid, ManufacturerGuid, Ind
		FROM AssyHierarchy
		ORDER BY Ind DESC
)