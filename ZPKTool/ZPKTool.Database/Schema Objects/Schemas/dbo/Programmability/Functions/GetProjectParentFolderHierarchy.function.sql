﻿
/*
    Returns the folder hierarchy starting at a project's parent folder up to the root folder.
	The 1st row in the returned table is the project's parent, the next is the parent's parent and so on.
*/
CREATE FUNCTION GetProjectParentFolderHierarchy(@projectId uniqueidentifier)
RETURNS TABLE
AS
RETURN
(
	WITH FolderHierarchy(Guid, ParentGuid) AS
	(
		SELECT pf.Guid, pf.ParentGuid
			FROM ProjectFolders pf
				INNER JOIN Projects p ON pf.Guid = p.FolderGuid
			WHERE p.Guid = @projectId
		UNION ALL
		SELECT f.Guid, f.ParentGuid
			FROM ProjectFolders f
				INNER JOIN FolderHierarchy fh ON f.Guid = fh.ParentGuid
	)

	SELECT Guid
		FROM FolderHierarchy
)