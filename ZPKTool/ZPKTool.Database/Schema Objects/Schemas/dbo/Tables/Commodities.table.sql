﻿CREATE TABLE [dbo].[Commodities] (
    [Name]             NVARCHAR (100)   NOT NULL,
    [Price]            DECIMAL (29, 16) NULL,
    [Weight]           DECIMAL (28, 14) NULL,
    [RejectRatio]      DECIMAL (19, 10) NULL,
    [Remarks]          NVARCHAR (2000)  NULL,
    [Amount]           INT              NULL,
    [PartNumber]       NVARCHAR (100)   NULL,
    [IsMasterData]     BIT              DEFAULT ((0)) NOT NULL,
    [IsDeleted]        BIT              DEFAULT ((0)) NOT NULL,
    [Guid]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [MediaGuid]        UNIQUEIDENTIFIER NULL,
    [ManufacturerGuid] UNIQUEIDENTIFIER NULL,
    [PartGuid]         UNIQUEIDENTIFIER NULL,
    [ProcessStepGuid]  UNIQUEIDENTIFIER NULL,
    [OwnerGuid]        UNIQUEIDENTIFIER NULL,
    [WeightUnitGuid]   UNIQUEIDENTIFIER NULL,
	[IsStockMasterData] BIT             NOT NULL DEFAULT 0,
    [CreateDate]       DATETIME         DEFAULT (getdate()) NULL,
	[CalculationCreateDate] DATETIME    NULL
);

