﻿CREATE TABLE [dbo].[Media] (
    [Type]             SMALLINT         NOT NULL,
    [Content]          VARBINARY (MAX)  NOT NULL,
    [Size]             INT              NULL,
    [OriginalFileName] NVARCHAR (255)   NULL,
    [Guid]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsMasterData]     BIT              DEFAULT ((0)) NOT NULL,
    [OwnerGuid]        UNIQUEIDENTIFIER NULL,
	[IsStockMasterData] BIT             NOT NULL DEFAULT 0,
    [CreateDate]       DATETIME         DEFAULT (getdate()) NULL
);

