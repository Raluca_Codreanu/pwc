﻿CREATE TABLE [dbo].[ProcessStepPartAmounts] (
    [Amount]          INT              NOT NULL,
    [Guid]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsMasterData]    BIT              DEFAULT ((0)) NOT NULL,
    [OwnerGuid]       UNIQUEIDENTIFIER NULL,
    [ProcessStepGuid] UNIQUEIDENTIFIER NOT NULL,
    [PartGuid]        UNIQUEIDENTIFIER NOT NULL,
	[IsStockMasterData] BIT            NOT NULL DEFAULT 0
);
GO


-- Delete the amounts that are not valid, the process of the amount's process step and the process of the parent assembly of the amount's assembly need to be the same.
CREATE TRIGGER [dbo].[ProcessStepPartAmounts_insert_validity_check]
	ON [dbo].[ProcessStepAssemblyAmounts]
	AFTER INSERT
AS 
BEGIN
	DELETE
		FROM ProcessStepAssemblyAmounts
		WHERE Guid IN (SELECT ProcessStepAssemblyAmounts.Guid
							FROM ProcessStepAssemblyAmounts
								INNER JOIN ProcessSteps ON ProcessStepAssemblyAmounts.ProcessStepGuid = ProcessSteps.Guid
								INNER JOIN Assemblies ON ProcessStepAssemblyAmounts.AssemblyGuid = Assemblies.Guid
								INNER JOIN Assemblies AS AssemblyParent ON Assemblies.ParentAssemblyGuid = Assemblies.Guid
							WHERE AssemblyParent.ProcessGuid != ProcessSteps.ProcessGuid)
END
GO