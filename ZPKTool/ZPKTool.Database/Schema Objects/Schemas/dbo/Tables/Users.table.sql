﻿CREATE TABLE [dbo].[Users] (
    [Name]                 NVARCHAR (100)   NOT NULL,
    [Description]          NVARCHAR (2000)  NULL,
    [Username]             NVARCHAR (100)    NOT NULL,
    [Password]             NVARCHAR (200)   NOT NULL,
	[Salt]                 NVARCHAR(50)     NULL,
	[IsPasswordSetByUser]  BIT              DEFAULT ((0)) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsReleased]           BIT              DEFAULT ((0)) NOT NULL,
    [Disabled]             BIT              DEFAULT ((0)) NOT NULL,
    [UICurrencyId]	       UNIQUEIDENTIFIER NULL,
    [CreateDate]           DATETIME         DEFAULT (getdate()) NULL, 
    [Roles]				   NVARCHAR(100)		NOT NULL DEFAULT ((0))  
);

