﻿CREATE TABLE [dbo].[GlobalSettings] (
    [Guid]  UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Key]   VARCHAR (MAX)    NOT NULL,
    [Value] VARCHAR (MAX)    NULL
);

