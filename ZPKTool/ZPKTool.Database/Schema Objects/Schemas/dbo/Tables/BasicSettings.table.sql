﻿CREATE TABLE [dbo].[BasicSettings] (
    [DeprPeriod]        INT              NOT NULL,
    [DeprRate]          DECIMAL (19, 10) NOT NULL,
    [PartBatch]         INT              NOT NULL,
    [ShiftsPerWeek]     DECIMAL (9, 2)   NOT NULL,
    [HoursPerShift]     DECIMAL (9, 2)   NOT NULL,
    [ProdDays]          DECIMAL (9, 2)   NOT NULL,
    [ProdWeeks]         DECIMAL (9, 2)   NOT NULL,
    [AssetRate]         DECIMAL (19, 10) NOT NULL,
    [LogisticCostRatio] DECIMAL (19, 10) NOT NULL,
    [Guid]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
	[CreateDate]        DATETIME         DEFAULT (getdate()) NULL
);

