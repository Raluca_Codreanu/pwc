﻿CREATE TABLE [dbo].[CurrenciesExchangeRateHistory] (
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Timestamp]    DATETIME         DEFAULT (getdate()) NOT NULL,
    [ExchangeRate] DECIMAL (19, 8)  NOT NULL,
    [CurrencyGuid] UNIQUEIDENTIFIER NOT NULL
);

