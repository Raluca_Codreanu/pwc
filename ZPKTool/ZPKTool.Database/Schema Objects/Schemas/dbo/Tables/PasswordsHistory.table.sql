﻿CREATE TABLE [dbo].[PasswordsHistory]
(
	[Guid]		UNIQUEIDENTIFIER	DEFAULT (newsequentialid()) NOT NULL,
	[UserID]	UNIQUEIDENTIFIER	NOT NULL,
	[Password]	NVARCHAR(200)		NOT NULL,
	[Date]		DATETIME			DEFAULT (getdate()) NOT NULL, 
)
