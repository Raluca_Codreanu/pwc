﻿CREATE TABLE [dbo].[TransportCostCalculationSettings]
(
	[Guid] UNIQUEIDENTIFIER NOT NULL  , 
    [DaysPerYear] INT NOT NULL, 
    [AverageLoadTarget] DECIMAL(29, 16) NULL, 
    [VanTypeCost] DECIMAL(29, 16) NOT NULL, 
    [TruckTrailerCost] DECIMAL(29, 16) NOT NULL, 
    [MegaTrailerCost] DECIMAL(29, 16) NOT NULL, 
    [Truck7tCost] DECIMAL(29, 16) NOT NULL, 
    [Truck3_5tCost] DECIMAL(29, 16) NOT NULL, 
    [TransportCostCalculationId] UNIQUEIDENTIFIER NOT NULL, 
    [OwnerId] UNIQUEIDENTIFIER NULL, 
    [Timestamp] DATETIME NOT NULL 
)
