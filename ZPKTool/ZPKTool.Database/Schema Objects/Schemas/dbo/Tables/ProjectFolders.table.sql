﻿CREATE TABLE [dbo].[ProjectFolders] (
    [Name]       NVARCHAR (100)   NOT NULL,
    [Guid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsDeleted]  BIT              DEFAULT ((0)) NOT NULL,
    [ParentGuid] UNIQUEIDENTIFIER NULL,
    [OwnerGuid]  UNIQUEIDENTIFIER NULL,
    [IsOffline]  BIT              DEFAULT ((0)) NOT NULL,
    [IsReleased] BIT              DEFAULT ((0)) NOT NULL,
    [CreateDate] DATETIME         DEFAULT (getdate()) NULL
);

