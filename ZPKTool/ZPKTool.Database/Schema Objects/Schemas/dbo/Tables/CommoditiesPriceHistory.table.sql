﻿CREATE TABLE [dbo].[CommoditiesPriceHistory] (
    [Timestamp]     DATETIME         DEFAULT (getdate()) NOT NULL,
    [Price]         DECIMAL (29, 16) NOT NULL,
    [Guid]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [CommodityGuid] UNIQUEIDENTIFIER NOT NULL
);

