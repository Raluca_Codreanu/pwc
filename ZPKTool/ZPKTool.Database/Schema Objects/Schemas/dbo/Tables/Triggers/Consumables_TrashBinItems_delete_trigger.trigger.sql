﻿CREATE TRIGGER Consumables_TrashBinItems_delete_trigger
	ON Consumables
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)