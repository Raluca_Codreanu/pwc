﻿CREATE TRIGGER CountrySettingsValueHistoryUpdate
	ON CountrySettings
	FOR UPDATE
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @newUnskilledLaborCost decimal(29,16),
				@oldUnskilledLaborCost decimal(29,16),
				@newSkilledLaborCost decimal(29,16),
				@oldSkilledLaborCost decimal(29,16),
				@newForemanCost decimal(29,16),
				@oldForemanCost decimal(29,16),
				@newTechnicianCost decimal(29,16),
				@oldTechnicianCost decimal(29,16),
				@newEngineerCost decimal(29,16),
				@oldEngineerCost decimal(29,16),
				@newEnergyCost decimal(29,16),
				@oldEnergyCost decimal(29,16),
				@newAirCost decimal(29,16),
				@oldAirCost decimal(29,16),
				@newWaterCost decimal(29,16),
				@oldWaterCost decimal(29,16),
				@newProductionAreaRentalCost decimal(29,16),
				@oldProductionAreaRentalCost decimal(29,16),
				@newOfficeAreaRentalCost decimal(29,16),
				@oldOfficeAreaRentalCost decimal(29,16),
				@newInterestRate decimal(19,10),
				@oldInterestRate decimal(19,10),
				@newShiftCharge1ShiftModel decimal(19,10),
				@oldShiftCharge1ShiftModel decimal(19,10),
				@newShiftCharge2ShiftModel decimal(19,10),
				@oldShiftCharge2ShiftModel decimal(19,10),
				@newShiftCharge3ShiftModel decimal(19,10),
				@oldShiftCharge3ShiftModel decimal(19,10),
				@newLaborAvailability decimal(19,10),
				@oldLaborAvailability decimal(19,10),
				@countrySettingGuid uniqueidentifier

		SELECT @newUnskilledLaborCost = UnskilledLaborCost,
				@newSkilledLaborCost = SkilledLaborCost,
				@newForemanCost = ForemanCost,
				@newTechnicianCost = TechnicianCost,
				@newEngineerCost = EngineerCost,
				@newEnergyCost = EnergyCost,
				@newAirCost = AirCost,
				@newWaterCost = WaterCost,
				@newProductionAreaRentalCost = ProductionAreaRentalCost,
				@newOfficeAreaRentalCost = OfficeAreaRentalCost,
				@newInterestRate = InterestRate,
				@newShiftCharge1ShiftModel = ShiftCharge1ShiftModel,
				@newShiftCharge2ShiftModel = ShiftCharge2ShiftModel,
				@newShiftCharge3ShiftModel = ShiftCharge3ShiftModel,
				@newLaborAvailability = LaborAvailability,
				@countrySettingGuid  = [Guid]
			FROM inserted
		   
		SELECT @oldUnskilledLaborCost = UnskilledLaborCost,
				@oldSkilledLaborCost = SkilledLaborCost,
				@oldForemanCost = ForemanCost,
				@oldTechnicianCost = TechnicianCost,
				@oldEngineerCost = EngineerCost,
				@oldEnergyCost = EnergyCost,
				@oldAirCost = AirCost,
				@oldWaterCost = WaterCost,
				@oldProductionAreaRentalCost = ProductionAreaRentalCost,
				@oldOfficeAreaRentalCost = OfficeAreaRentalCost,
				@oldInterestRate = InterestRate,
				@oldShiftCharge1ShiftModel = ShiftCharge1ShiftModel,
				@oldShiftCharge2ShiftModel = ShiftCharge2ShiftModel,
				@oldShiftCharge3ShiftModel = ShiftCharge3ShiftModel,
				@oldLaborAvailability = LaborAvailability,
				@countrySettingGuid  = [Guid]
			FROM deleted

		IF @newUnskilledLaborCost IS NULL 
			SET @newUnskilledLaborCost = 0 
		IF @newSkilledLaborCost IS NULL 
			SET @newSkilledLaborCost = 0 
		IF @newForemanCost IS NULL 
			SET @newForemanCost = 0 
		IF @newTechnicianCost IS NULL 
			SET @newTechnicianCost = 0 
		IF @newEngineerCost IS NULL 
			SET @newEngineerCost = 0 
		IF @newEnergyCost IS NULL 
			SET @newEnergyCost = 0 
		IF @newAirCost IS NULL 
			SET @newAirCost = 0 
		IF @newWaterCost IS NULL 
			SET @newWaterCost = 0 
		IF @newProductionAreaRentalCost IS NULL 
			SET @newProductionAreaRentalCost = 0 
		IF @newOfficeAreaRentalCost IS NULL 
			SET @newOfficeAreaRentalCost = 0 
		IF @newInterestRate IS NULL 
			SET @newInterestRate = 0 
		IF @newShiftCharge1ShiftModel IS NULL 
			SET @newShiftCharge1ShiftModel = 0 
		IF @newShiftCharge2ShiftModel IS NULL 
			SET @newShiftCharge2ShiftModel = 0 
		IF @newShiftCharge3ShiftModel IS NULL 
			SET @newShiftCharge3ShiftModel = 0 
		IF @newLaborAvailability IS NULL 
			SET @newLaborAvailability = 0 
		   
		IF @oldUnskilledLaborCost IS NULL 
			SET @oldUnskilledLaborCost = 0 
		IF @oldSkilledLaborCost IS NULL 
			SET @oldSkilledLaborCost = 0 
		IF @oldForemanCost IS NULL 
			SET @oldForemanCost = 0 
		IF @oldTechnicianCost IS NULL 
			SET @oldTechnicianCost = 0 
		IF @oldEngineerCost IS NULL 
			SET @oldEngineerCost = 0 
		IF @oldEnergyCost IS NULL 
			SET @oldEnergyCost = 0 
		IF @oldAirCost IS NULL 
			SET @oldAirCost = 0 
		IF @oldWaterCost IS NULL 
			SET @oldWaterCost = 0 
		IF @oldProductionAreaRentalCost IS NULL 
			SET @oldProductionAreaRentalCost = 0 
		IF @oldOfficeAreaRentalCost IS NULL 
			SET @oldOfficeAreaRentalCost = 0 
		IF @oldInterestRate IS NULL 
			SET @oldInterestRate = 0 
		IF @oldShiftCharge1ShiftModel IS NULL 
			SET @oldShiftCharge1ShiftModel = 0 
		IF @oldShiftCharge2ShiftModel IS NULL 
			SET @oldShiftCharge2ShiftModel = 0 
		IF @oldShiftCharge3ShiftModel IS NULL 
			SET @oldShiftCharge3ShiftModel = 0 
		IF @oldLaborAvailability IS NULL 
			SET @oldLaborAvailability = 0 

		IF @newUnskilledLaborCost != @oldUnskilledLaborCost OR
				@newSkilledLaborCost != @oldSkilledLaborCost OR
				@newForemanCost != @oldForemanCost OR
				@newTechnicianCost != @oldTechnicianCost OR
				@newEngineerCost != @oldEngineerCost OR
				@newEnergyCost != @oldEnergyCost OR
				@newAirCost != @oldAirCost OR
				@newWaterCost != @oldWaterCost OR
				@newProductionAreaRentalCost != @oldProductionAreaRentalCost OR
				@newOfficeAreaRentalCost != @oldOfficeAreaRentalCost OR
				@newInterestRate != @oldInterestRate OR
				@newShiftCharge1ShiftModel != @oldShiftCharge1ShiftModel OR 	   
				@newShiftCharge2ShiftModel != @oldShiftCharge2ShiftModel OR 	
				@newShiftCharge3ShiftModel != @oldShiftCharge3ShiftModel OR 	
				@newLaborAvailability != @oldLaborAvailability 
			INSERT INTO CountrySettingsValueHistory([Timestamp],
													UnskilledLaborCost,
													SkilledLaborCost,
													ForemanCost,
													TechnicianCost,
													EngineerCost,
													EnergyCost,
													AirCost,
													WaterCost,
													ProductionAreaRentalCost,
													OfficeAreaRentalCost,
													InterestRate,
													ShiftCharge1ShiftModel,
													ShiftCharge2ShiftModel,
													ShiftCharge3ShiftModel,
													LaborAvailability,
													[Guid],											
													CountrySettingsGuid)
											VALUES (GETDATE(),
													@newUnskilledLaborCost,
													@newSkilledLaborCost,
													@newForemanCost,
													@newTechnicianCost,
													@newEngineerCost,
													@newEnergyCost,
													@newAirCost,
													@newWaterCost,
													@newProductionAreaRentalCost,
													@newOfficeAreaRentalCost,
													@newInterestRate,
													@newShiftCharge1ShiftModel,
													@newShiftCharge2ShiftModel,
													@newShiftCharge3ShiftModel,
													@newLaborAvailability,
													NEWID(),
													@countrySettingGuid)	
	END
END