﻿CREATE TRIGGER Commodities_TrashBinItems_delete_trigger
	ON Commodities
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)