﻿CREATE TRIGGER Currencies_ExchangeRateHistory_update_trigger
	ON Currencies
	FOR UPDATE
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @oldExchangeRate decimal(19, 8),	
		        @newExchangeRate decimal(19, 8),	
				@currencyGuid uniqueidentifier			  
		
		SELECT @oldExchangeRate = ExchangeRate
		        FROM deleted
		
		SELECT @newExchangeRate = ExchangeRate,
			   @currencyGuid = [Guid] 
			FROM inserted	
					
	    IF @oldExchangeRate != @newExchangeRate
			INSERT INTO CurrenciesExchangeRateHistory([Timestamp],
														ExchangeRate,
														[Guid],
														CurrencyGuid)
												VALUES (GETDATE(),
														@newExchangeRate,
														NEWID(),
														@currencyGuid)
	END
END