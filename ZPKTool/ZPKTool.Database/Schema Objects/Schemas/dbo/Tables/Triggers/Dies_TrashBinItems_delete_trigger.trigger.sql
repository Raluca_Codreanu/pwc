﻿CREATE TRIGGER Dies_TrashBinItems_delete_trigger
	ON Dies
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)