﻿CREATE TRIGGER Machines_TrashBinItems_delete_trigger
	ON Machines
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)