﻿CREATE TRIGGER Project_TrashBinItems_delete_trigger
	ON Projects
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)