﻿CREATE TRIGGER CommoditiesPriceHistoryInsert
	ON Commodities
	FOR INSERT
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @price decimal(29, 16),			
				@commodityGuid uniqueidentifier

		SELECT @price = Price,
				@commodityGuid = [Guid] 
			FROM inserted
	
		IF @price IS NULL
			SET @price = 0
	
		INSERT INTO CommoditiesPriceHistory([Timestamp],
			                                Price,
											[Guid],
											CommodityGuid)
						              VALUES (GETDATE(),
									          @price,
											  NEWID(),
											  @commodityGuid)
	END
END