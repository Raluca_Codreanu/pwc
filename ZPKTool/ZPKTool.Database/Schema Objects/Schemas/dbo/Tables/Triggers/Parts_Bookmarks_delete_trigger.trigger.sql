﻿CREATE TRIGGER Parts_Bookmarks_delete_trigger
	ON Parts
	FOR DELETE
AS
	DELETE
		FROM Bookmarks
		WHERE EntityId IN (SELECT [Guid]
								FROM deleted)