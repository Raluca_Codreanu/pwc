﻿CREATE TRIGGER RawMaterialsPriceHistoryInsert
	ON RawMaterials
	FOR INSERT
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @price decimal(29, 16),
				@isScrambled bit,
				@rawMaterialGuid uniqueidentifier

		SELECT @price = Price,
				@isScrambled = IsScrambled,
				@rawMaterialGuid = [Guid]
			FROM inserted			

		IF @price IS NULL
			SET @price = 0

		INSERT INTO RawMaterialsPriceHistory([Timestamp],
											Price,
											IsScrambled,
											[Guid],
											RawMaterialGuid)
									VALUES (GETDATE(),
											@price,
											@isScrambled,
											NEWID(),
											@rawMaterialGuid)
	END
END