﻿CREATE TRIGGER ProjectFolders_TrashBinItems_delete_trigger
	ON ProjectFolders
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)