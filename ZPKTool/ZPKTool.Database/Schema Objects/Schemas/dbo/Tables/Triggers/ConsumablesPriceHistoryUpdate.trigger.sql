﻿CREATE TRIGGER ConsumablesPriceHistoryUpdate
	ON Consumables
	FOR UPDATE
AS
BEGIN
    IF  (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @newPrice decimal(29, 16),
				@oldPrice decimal(29, 16),
				@consumableGuid uniqueidentifier

		SELECT @newPrice = Price,
				@consumableGuid = [Guid]
			FROM inserted
       
		SET @oldPrice = (SELECT Price
							FROM deleted)

		IF @newPrice IS NULL
			SET @newPrice = 0
		IF @oldPrice IS NULL
			SET @oldPrice = 0
 
		IF @newPrice != @oldPrice
			INSERT INTO ConsumablesPriceHistory([Timestamp],
												Price,
												[Guid],
												ConsumableGuid)
											VALUES (GETDATE(),
												@newPrice,
												NEWID(),
												@consumableGuid)
	END
END