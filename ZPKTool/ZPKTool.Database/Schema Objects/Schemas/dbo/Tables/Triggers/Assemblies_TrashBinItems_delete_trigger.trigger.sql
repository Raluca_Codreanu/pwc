﻿CREATE TRIGGER Assemblies_TrashBinItems_delete_trigger
	ON Assemblies
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)