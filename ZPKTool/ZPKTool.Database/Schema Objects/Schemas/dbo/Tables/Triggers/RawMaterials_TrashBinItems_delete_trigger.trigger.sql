﻿CREATE TRIGGER RawMaterials_TrashBinItems_delete_trigger
	ON RawMaterials
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)