﻿CREATE TRIGGER CommoditiesPriceHistoryUpdate
	ON Commodities
	FOR UPDATE
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @newPrice decimal(29, 16),
				@oldPrice decimal(29, 16),
				@commodityGuid uniqueidentifier

		SELECT @newPrice = Price,
				@commodityGuid = [Guid]
			FROM inserted
        
		SET @oldPrice = (SELECT Price
							FROM deleted)

		IF @newPrice IS NULL
			SET @newPrice = 0
		IF @oldPrice IS NULL
			SET @oldPrice = 0

		IF @newPrice != @oldPrice
			INSERT INTO CommoditiesPriceHistory([Timestamp],
				                                Price,
												[Guid],
												CommodityGuid)
										VALUES (GETDATE(),
												@newPrice,
												NEWID(),
												@commodityGuid)
	END
END