﻿CREATE TRIGGER Parts_TrashBinItems_delete_trigger
	ON Parts
	FOR DELETE
AS
	DELETE
		FROM TrashBinItems
		WHERE EntityGuid IN (SELECT [Guid]
								FROM deleted)