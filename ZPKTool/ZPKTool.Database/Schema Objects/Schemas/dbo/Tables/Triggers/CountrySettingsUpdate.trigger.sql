﻿CREATE TRIGGER CountrySettingsUpdate
	ON CountrySettings
	FOR UPDATE
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @noOfUpdates int
		SET @noOfUpdates = 0

		SELECT @noOfUpdates = COUNT(*)
			FROM Inserted i
				JOIN Deleted d ON i.Guid = d.Guid
		            AND EXISTS (SELECT i.*
								EXCEPT
								SELECT d.*)

		IF @noOfUpdates != 0
		BEGIN
			UPDATE CountrySettings SET LastChangeTimestamp = DATEADD(d, 1, LastChangeTimestamp)
				WHERE Guid IN (SELECT SettingsGuid FROM Countries)
					AND Guid IN (SELECT Guid FROM Inserted)
		END
	END
END