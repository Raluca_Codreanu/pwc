﻿CREATE TRIGGER CountrySettingsValueHistoryInsert
	ON CountrySettings
	FOR INSERT
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @unskilledLaborCost decimal(29, 16),
				@skilledLaborCost decimal(29, 16),
				@foremanCost decimal(29, 16),
				@technicianCost decimal(29, 16),
				@engineerCost decimal(29, 16),
				@energyCost decimal(29, 16),
				@airCost decimal(29, 16),
				@waterCost decimal(29, 16),
				@productionAreaRentalCost decimal(29, 16),
				@officeAreaRentalCost decimal(29, 16),
				@interestRate decimal(19, 10),
				@shiftCharge1ShiftModel decimal(19, 10),
				@shiftCharge2ShiftModel decimal(19, 10),
				@shiftCharge3ShiftModel decimal(19, 10),
				@laborAvailability decimal(19, 10),
				@isScrambled bit,
				@countrySettingGuid uniqueidentifier

		SELECT @unskilledLaborCost = UnskilledLaborCost,
				@skilledLaborCost = SkilledLaborCost,
				@foremanCost = ForemanCost,
				@technicianCost = TechnicianCost,
				@engineerCost = EngineerCost,
				@energyCost = EnergyCost,
				@airCost = AirCost,
				@waterCost = WaterCost,
				@productionAreaRentalCost = ProductionAreaRentalCost,
				@officeAreaRentalCost = OfficeAreaRentalCost,
				@interestRate = InterestRate,
				@shiftCharge1ShiftModel = ShiftCharge1ShiftModel,
				@shiftCharge2ShiftModel = ShiftCharge2ShiftModel,
				@shiftCharge3ShiftModel = ShiftCharge3ShiftModel,
				@laborAvailability = LaborAvailability,
				@countrySettingGuid = [Guid],
				@isScrambled = IsScrambled
			FROM inserted

		IF @unskilledLaborCost IS NULL
			SET @unskilledLaborCost = 0
		IF @skilledLaborCost IS NULL
	        SET @skilledLaborCost = 0
		IF @foremanCost IS NULL
	       SET @foremanCost = 0
		IF @technicianCost IS NULL
	       SET @technicianCost = 0
		IF @engineerCost IS NULL
 	       SET @engineerCost = 0
		IF @energyCost IS NULL
		   SET @energyCost = 0
		IF @airCost IS NULL
		   SET @airCost = 0
		IF @waterCost IS NULL
		   SET @waterCost = 0
		IF @productionAreaRentalCost IS NULL
		   SET @productionAreaRentalCost = 0
		IF @officeAreaRentalCost IS NULL
		   SET @officeAreaRentalCost = 0
		IF @interestRate IS NULL
		   SET @interestRate = 0
		IF @shiftCharge1ShiftModel IS NULL
		   SET @shiftCharge1ShiftModel = 0
		IF @shiftCharge2ShiftModel IS NULL
		   SET @shiftCharge2ShiftModel = 0
		IF @shiftCharge3ShiftModel IS NULL
		   SET @shiftCharge3ShiftModel = 0
		IF @laborAvailability IS NULL
		   SET @laborAvailability = 0

		INSERT INTO CountrySettingsValueHistory([Timestamp],
												UnskilledLaborCost,
												SkilledLaborCost,
												ForemanCost,
												TechnicianCost,
												EngineerCost,
												EnergyCost,
												AirCost,
												WaterCost,
												ProductionAreaRentalCost,
												OfficeAreaRentalCost,
												InterestRate,
												ShiftCharge1ShiftModel,
												ShiftCharge2ShiftModel,
												ShiftCharge3ShiftModel,
												LaborAvailability,
												[Guid],	
												IsScrambled,											
												CountrySettingsGuid) 
										VALUES (GETDATE(),
												@unskilledLaborCost,
												@skilledLaborCost,
												@foremanCost,
												@technicianCost,
												@engineerCost,
												@energyCost,
												@airCost,
												@waterCost,
												@productionAreaRentalCost,
												@officeAreaRentalCost,
												@interestRate,
												@shiftCharge1ShiftModel,
												@shiftCharge2ShiftModel,
												@shiftCharge3ShiftModel,
												@laborAvailability,
												NEWID(),
												@isScrambled,
												@countrySettingGuid)	
	END
END