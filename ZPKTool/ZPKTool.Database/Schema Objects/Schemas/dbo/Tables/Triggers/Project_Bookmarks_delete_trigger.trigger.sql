﻿CREATE TRIGGER Project_Bookmarks_delete_trigger
	ON Projects
	FOR DELETE
AS
	DELETE
		FROM Bookmarks
		WHERE EntityId IN (SELECT [Guid]
								FROM deleted)