﻿CREATE TRIGGER Currencies_ExchangeRateHistory_insert_trigger
	ON Currencies
	FOR INSERT
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @exchangeRate decimal(19, 8),
				@currencyGuid uniqueidentifier
		
		SELECT @exchangeRate = ExchangeRate,
				@currencyGuid = [Guid]
			FROM inserted
					
		INSERT INTO CurrenciesExchangeRateHistory([Timestamp],
													ExchangeRate,
													[Guid],
													CurrencyGuid)
											VALUES (GETDATE(),
													@exchangeRate,
													NEWID(),
													@currencyGuid)		
	END
END