﻿CREATE TRIGGER ConsumablesPriceHistoryInsert
	ON Consumables
	FOR INSERT
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @price decimal(29, 16),
				@consumableGuid uniqueidentifier

		SELECT @price = Price,
				@consumableGuid = [Guid]
			FROM inserted

		IF @price is NULL
			SET @price = 0

		INSERT INTO ConsumablesPriceHistory([Timestamp],
			                                Price,
											[Guid],
											ConsumableGuid)
									VALUES (GETDATE(),
											@price,
											NEWID(),
											@consumableGuid)
	END
END