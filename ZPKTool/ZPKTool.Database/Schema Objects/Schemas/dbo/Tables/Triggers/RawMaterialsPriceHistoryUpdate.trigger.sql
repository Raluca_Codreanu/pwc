﻿CREATE TRIGGER RawMaterialsPriceHistoryUpdate
	ON RawMaterials
	FOR UPDATE
AS
BEGIN
	IF (SELECT HOST_NAME()) != 'SyncWorker'
	BEGIN
		DECLARE @newPrice decimal(29, 16),	
				@oldPrice decimal(29, 16),	
				@isScrambled bit,			
				@rawMaterialGuid uniqueidentifier

		SELECT @newPrice = Price,
				@isScrambled = IsScrambled,
				@rawMaterialGuid = [Guid]
			FROM inserted

		SET @oldPrice = (SELECT Price
							FROM deleted)

		IF @newPrice IS NULL
			SET @newPrice = 0   
		IF @oldPrice IS NULL
			SET @oldPrice = 0
 
		IF @newPrice != @oldPrice
			INSERT INTO RawMaterialsPriceHistory([Timestamp],
												Price,
												IsScrambled,
												[Guid],
												RawMaterialGuid)
										VALUES (GETDATE(),
												@newPrice,
												@isScrambled,
												NEWID(),
												@rawMaterialGuid)
	END
END