﻿CREATE TRIGGER Assemblies_Bookmarks_delete_trigger
	ON Assemblies
	FOR DELETE
AS
	DELETE
		FROM Bookmarks
		WHERE EntityId IN (SELECT [Guid]
								FROM deleted)