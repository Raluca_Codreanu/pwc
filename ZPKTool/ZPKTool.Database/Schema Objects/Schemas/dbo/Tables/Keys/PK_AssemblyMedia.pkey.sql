﻿ALTER TABLE [dbo].[AssemblyMedia]
    ADD CONSTRAINT [PK_AssemblyMedia] PRIMARY KEY NONCLUSTERED ([MediaGuid] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];

