﻿ALTER TABLE [dbo].[Bookmarks]
    ADD CONSTRAINT [FK_Boommarks_Users] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

