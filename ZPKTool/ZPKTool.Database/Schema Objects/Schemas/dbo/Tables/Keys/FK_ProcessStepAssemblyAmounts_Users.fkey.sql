﻿ALTER TABLE [dbo].[ProcessStepAssemblyAmounts]
    ADD CONSTRAINT [FK_ProcessStepAssemblyAmounts_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

