﻿ALTER TABLE [dbo].[Processes]
    ADD CONSTRAINT [FK_Processes_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

