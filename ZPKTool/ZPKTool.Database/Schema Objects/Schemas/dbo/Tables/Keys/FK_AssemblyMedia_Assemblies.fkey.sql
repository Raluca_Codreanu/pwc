﻿ALTER TABLE [dbo].[AssemblyMedia]
    ADD CONSTRAINT [FK_AssemblyMedia_Assemblies] FOREIGN KEY ([AssemblyGuid]) REFERENCES [dbo].[Assemblies] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

