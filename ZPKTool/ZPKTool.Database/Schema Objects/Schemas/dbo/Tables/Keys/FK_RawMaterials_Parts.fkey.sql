﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_Parts] FOREIGN KEY ([PartGuid]) REFERENCES [dbo].[Parts] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

