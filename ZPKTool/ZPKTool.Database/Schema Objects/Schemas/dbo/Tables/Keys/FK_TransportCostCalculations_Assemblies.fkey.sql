﻿ALTER TABLE [dbo].[TransportCostCalculations]
	ADD CONSTRAINT [FK_TransportCostCalculations_Assemblies]
	FOREIGN KEY ([AssemblyId])
	REFERENCES [Assemblies] (Guid)
	ON DELETE CASCADE
