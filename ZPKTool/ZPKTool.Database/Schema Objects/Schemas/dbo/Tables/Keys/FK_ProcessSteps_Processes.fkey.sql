﻿ALTER TABLE [dbo].[ProcessSteps]
    ADD CONSTRAINT [FK_ProcessSteps_Processes] FOREIGN KEY ([ProcessGuid]) REFERENCES [dbo].[Processes] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

