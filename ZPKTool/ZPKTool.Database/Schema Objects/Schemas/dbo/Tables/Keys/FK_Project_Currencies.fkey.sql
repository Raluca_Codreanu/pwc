﻿ALTER TABLE [dbo].[Projects]
    ADD CONSTRAINT [FK_Projects_Currencies] FOREIGN KEY ([BaseCurrencyId]) REFERENCES [dbo].[Currencies] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;