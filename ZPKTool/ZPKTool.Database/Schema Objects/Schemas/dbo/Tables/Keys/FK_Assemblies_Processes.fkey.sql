﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_Processes] FOREIGN KEY ([ProcessGuid]) REFERENCES [dbo].[Processes] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

