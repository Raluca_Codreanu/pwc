﻿ALTER TABLE [dbo].[TransportCostCalculationSettings]
	ADD CONSTRAINT [FK_TransportCostCalculationSettings_Users]
	FOREIGN KEY (OwnerId)
	REFERENCES [Users] (Guid)
