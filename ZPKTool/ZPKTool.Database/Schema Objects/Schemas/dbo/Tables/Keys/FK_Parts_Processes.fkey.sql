﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_Processes] FOREIGN KEY ([ProcessGuid]) REFERENCES [dbo].[Processes] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

