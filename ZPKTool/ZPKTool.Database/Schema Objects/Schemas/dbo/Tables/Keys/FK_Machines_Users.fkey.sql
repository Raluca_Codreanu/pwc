﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

