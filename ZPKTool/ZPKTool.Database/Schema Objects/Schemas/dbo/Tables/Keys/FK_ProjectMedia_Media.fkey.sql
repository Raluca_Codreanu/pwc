﻿ALTER TABLE [dbo].[ProjectMedia]
    ADD CONSTRAINT [FK_ProjectMedia_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

