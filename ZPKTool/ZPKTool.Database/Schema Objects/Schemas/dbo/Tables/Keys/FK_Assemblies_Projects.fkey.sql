﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_Projects] FOREIGN KEY ([ProjectGuid]) REFERENCES [dbo].[Projects] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

