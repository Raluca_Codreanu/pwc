﻿ALTER TABLE [dbo].[ProcessStepAssemblyAmounts]
    ADD CONSTRAINT [FK_ProcessStepAssemblyAmounts_Assemblies] FOREIGN KEY ([AssemblyGuid]) REFERENCES [dbo].[Assemblies] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

