﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_Projects] FOREIGN KEY ([ParentProjectGuid]) REFERENCES [dbo].[Projects] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

