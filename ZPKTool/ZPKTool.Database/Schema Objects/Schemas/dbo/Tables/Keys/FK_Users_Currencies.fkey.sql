﻿ALTER TABLE [dbo].[Users]
    ADD CONSTRAINT [FK_Users_Currencies] FOREIGN KEY ([UICurrencyId]) REFERENCES [dbo].[Currencies] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;