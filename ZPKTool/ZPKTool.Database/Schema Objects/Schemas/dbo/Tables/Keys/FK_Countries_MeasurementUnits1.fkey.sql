﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_MeasurementUnits1] FOREIGN KEY ([WeightUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

