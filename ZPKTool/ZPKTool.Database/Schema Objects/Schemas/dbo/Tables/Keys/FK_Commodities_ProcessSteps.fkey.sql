﻿ALTER TABLE [dbo].[Commodities]
    ADD CONSTRAINT [FK_Commodities_ProcessSteps] FOREIGN KEY ([ProcessStepGuid]) REFERENCES [dbo].[ProcessSteps] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

