﻿ALTER TABLE [dbo].[ProjectMedia]
    ADD CONSTRAINT [FK_ProjectMedia_Projects] FOREIGN KEY ([ProjectGuid]) REFERENCES [dbo].[Projects] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

