﻿ALTER TABLE [dbo].[Customers]
    ADD CONSTRAINT [FK_Customers_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

