﻿ALTER TABLE [dbo].[PartMedia]
    ADD CONSTRAINT [FK_PartMedia_Parts] FOREIGN KEY ([PartGuid]) REFERENCES [dbo].[Parts] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

