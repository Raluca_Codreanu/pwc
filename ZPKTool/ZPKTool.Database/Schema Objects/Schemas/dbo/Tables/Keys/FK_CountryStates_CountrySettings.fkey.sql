﻿ALTER TABLE [dbo].[CountryStates]
    ADD CONSTRAINT [FK_CountryStates_CountrySettings] FOREIGN KEY ([SettingsGuid]) REFERENCES [dbo].[CountrySettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

