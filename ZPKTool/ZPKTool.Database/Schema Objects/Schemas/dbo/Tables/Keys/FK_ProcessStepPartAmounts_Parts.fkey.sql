﻿ALTER TABLE [dbo].[ProcessStepPartAmounts]
    ADD CONSTRAINT [FK_ProcessStepPartAmounts_Parts] FOREIGN KEY ([PartGuid]) REFERENCES [dbo].[Parts] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

