﻿ALTER TABLE [dbo].[CurrenciesExchangeRateHistory]
    ADD CONSTRAINT [FK_CurrenciesExchangeRateHistory_Currencies] FOREIGN KEY ([CurrencyGuid]) REFERENCES [dbo].[Currencies] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

