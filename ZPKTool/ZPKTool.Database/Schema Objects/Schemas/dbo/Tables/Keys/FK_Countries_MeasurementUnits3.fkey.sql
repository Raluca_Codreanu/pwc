﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_MeasurementUnits3] FOREIGN KEY ([VolumeUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

