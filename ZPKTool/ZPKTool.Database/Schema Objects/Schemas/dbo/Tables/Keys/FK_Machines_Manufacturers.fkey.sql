﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

