﻿ALTER TABLE [dbo].[ProcessSteps]
    ADD CONSTRAINT [FK_ProcessSteps_MeasurementUnits4] FOREIGN KEY ([MaxDownTimeUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

