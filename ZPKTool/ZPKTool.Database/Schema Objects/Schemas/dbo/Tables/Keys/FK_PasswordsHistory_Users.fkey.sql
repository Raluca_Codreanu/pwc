﻿ALTER TABLE [dbo].[PasswordsHistory]
    ADD CONSTRAINT [FK_PasswordsHistory_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;