﻿ALTER TABLE [dbo].[ProcessSteps]
    ADD CONSTRAINT [FK_ProcessSteps_ProcessStepsClassification] FOREIGN KEY ([TypeGuid]) REFERENCES [dbo].[ProcessStepsClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

