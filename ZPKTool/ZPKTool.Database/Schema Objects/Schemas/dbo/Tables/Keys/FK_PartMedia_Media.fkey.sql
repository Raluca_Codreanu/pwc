﻿ALTER TABLE [dbo].[PartMedia]
    ADD CONSTRAINT [FK_PartMedia_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

