﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_MaterialsClassification1] FOREIGN KEY ([ClassificationLevel2Guid]) REFERENCES [dbo].[MaterialsClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

