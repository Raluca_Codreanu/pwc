﻿ALTER TABLE [dbo].[Dies]
    ADD CONSTRAINT [FK_Dies_ProcessSteps] FOREIGN KEY ([ProcessStepGuid]) REFERENCES [dbo].[ProcessSteps] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

