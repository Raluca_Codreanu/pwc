﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_Assemblies] FOREIGN KEY ([ParentAssemblyGuid]) REFERENCES [dbo].[Assemblies] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

