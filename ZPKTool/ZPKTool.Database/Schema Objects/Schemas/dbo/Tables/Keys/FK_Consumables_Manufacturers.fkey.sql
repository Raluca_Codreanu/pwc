﻿ALTER TABLE [dbo].[Consumables]
    ADD CONSTRAINT [FK_Consumables_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

