﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_RawMaterialDeliveryTypes] FOREIGN KEY ([DeliveryTypeGuid]) REFERENCES [dbo].[RawMaterialDeliveryTypes] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

