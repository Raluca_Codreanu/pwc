﻿ALTER TABLE [dbo].[RawMaterialsPriceHistory]
    ADD CONSTRAINT [FK_RawMaterialsPriceHistory_RawMaterials] FOREIGN KEY ([RawMaterialGuid]) REFERENCES [dbo].[RawMaterials] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

