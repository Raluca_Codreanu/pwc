﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_MaterialsClassification2] FOREIGN KEY ([ClassificationLevel3Guid]) REFERENCES [dbo].[MaterialsClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

