﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE SET NULL ON UPDATE NO ACTION;

