﻿ALTER TABLE [dbo].[Commodities]
    ADD CONSTRAINT [FK_Commodities_Parts] FOREIGN KEY ([PartGuid]) REFERENCES [dbo].[Parts] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

