﻿ALTER TABLE [dbo].[Consumables]
    ADD CONSTRAINT [FK_Consumables_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

