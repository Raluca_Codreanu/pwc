﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

