﻿ALTER TABLE [dbo].[CycleTimeCalculations]
    ADD CONSTRAINT [PK_CycleTimeCalculations] PRIMARY KEY NONCLUSTERED ([Guid] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];

