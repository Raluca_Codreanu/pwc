﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

