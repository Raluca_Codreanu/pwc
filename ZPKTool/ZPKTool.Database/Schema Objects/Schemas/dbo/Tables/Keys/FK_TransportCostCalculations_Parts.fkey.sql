﻿ALTER TABLE [dbo].[TransportCostCalculations]
	ADD CONSTRAINT [FK_TransportCostCalculations_Parts]
	FOREIGN KEY (PartId)
	REFERENCES [Parts] (Guid)
	ON DELETE CASCADE	
