﻿ALTER TABLE [dbo].[AssemblyMedia]
    ADD CONSTRAINT [FK_AssemblyMedia_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

