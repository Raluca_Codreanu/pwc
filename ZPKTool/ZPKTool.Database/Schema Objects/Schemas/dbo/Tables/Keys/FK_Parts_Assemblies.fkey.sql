﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_Assemblies] FOREIGN KEY ([ParentAssemblyGuid]) REFERENCES [dbo].[Assemblies] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

