﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_CountrySettings] FOREIGN KEY ([SettingsGuid]) REFERENCES [dbo].[CountrySettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

