﻿ALTER TABLE [dbo].[Commodities]
    ADD CONSTRAINT [FK_Commodities_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE SET NULL ON UPDATE NO ACTION;

