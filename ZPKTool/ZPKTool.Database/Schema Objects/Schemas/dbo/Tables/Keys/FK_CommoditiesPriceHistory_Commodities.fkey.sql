﻿ALTER TABLE [dbo].[CommoditiesPriceHistory]
    ADD CONSTRAINT [FK_CommoditiesPriceHistory_Commodities] FOREIGN KEY ([CommodityGuid]) REFERENCES [dbo].[Commodities] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

