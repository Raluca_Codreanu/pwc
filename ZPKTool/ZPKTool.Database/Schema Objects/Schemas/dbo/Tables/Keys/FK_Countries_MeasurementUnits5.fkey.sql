﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_MeasurementUnits5] FOREIGN KEY ([FloorUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

