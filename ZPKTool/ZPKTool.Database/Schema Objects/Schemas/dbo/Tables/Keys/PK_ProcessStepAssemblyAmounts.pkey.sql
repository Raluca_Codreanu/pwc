﻿ALTER TABLE [dbo].[ProcessStepAssemblyAmounts]
    ADD CONSTRAINT [PK_ProcessStepAssemblyAmounts] PRIMARY KEY NONCLUSTERED ([ProcessStepGuid] ASC, [AssemblyGuid] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];

