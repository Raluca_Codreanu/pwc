﻿ALTER TABLE [dbo].[ProcessStepPartAmounts]
    ADD CONSTRAINT [FK_ProcessStepPartAmounts_ProcessSteps] FOREIGN KEY ([ProcessStepGuid]) REFERENCES [dbo].[ProcessSteps] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

