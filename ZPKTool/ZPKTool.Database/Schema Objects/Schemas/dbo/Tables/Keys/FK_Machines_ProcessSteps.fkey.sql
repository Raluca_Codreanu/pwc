﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_ProcessSteps] FOREIGN KEY ([ProcessStepGuid]) REFERENCES [dbo].[ProcessSteps] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

