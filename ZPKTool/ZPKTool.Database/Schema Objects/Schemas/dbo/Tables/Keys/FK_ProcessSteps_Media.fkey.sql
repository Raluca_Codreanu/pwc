﻿ALTER TABLE [dbo].[ProcessSteps]
    ADD CONSTRAINT [FK_ProcessSteps_Media] FOREIGN KEY ([MediaGuid]) REFERENCES [dbo].[Media] ([Guid]) ON DELETE SET NULL ON UPDATE NO ACTION;

