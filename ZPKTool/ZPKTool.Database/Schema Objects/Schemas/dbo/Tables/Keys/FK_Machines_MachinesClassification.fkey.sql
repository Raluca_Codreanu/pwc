﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_MachinesClassification] FOREIGN KEY ([MainClassificationGuid]) REFERENCES [dbo].[MachinesClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

