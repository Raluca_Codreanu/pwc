﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

