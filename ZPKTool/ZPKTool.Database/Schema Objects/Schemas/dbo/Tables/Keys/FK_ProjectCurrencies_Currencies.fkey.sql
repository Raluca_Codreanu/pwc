﻿ALTER TABLE [dbo].[ProjectCurrencies]
    ADD CONSTRAINT [FK_ProjectCurrencies_Currencies] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currencies] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;