﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_Currencies] FOREIGN KEY ([CurrencyGuid]) REFERENCES [dbo].[Currencies] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

