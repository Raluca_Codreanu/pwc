﻿ALTER TABLE [dbo].[ProcessSteps]
    ADD CONSTRAINT [FK_ProcessSteps_MeasurementUnits2] FOREIGN KEY ([ProcessTimeUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

