﻿ALTER TABLE [dbo].[Machines]
    ADD CONSTRAINT [FK_Machines_MachinesClassification3] FOREIGN KEY ([ClassificationLevel4Guid]) REFERENCES [dbo].[MachinesClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

