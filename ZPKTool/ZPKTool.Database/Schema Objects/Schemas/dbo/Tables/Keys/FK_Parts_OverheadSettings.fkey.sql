﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_OverheadSettings] FOREIGN KEY ([OverheadSettingsGuid]) REFERENCES [dbo].[OverheadSettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

