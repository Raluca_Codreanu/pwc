﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_MaterialsClassification3] FOREIGN KEY ([ClassificationLevel4Guid]) REFERENCES [dbo].[MaterialsClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

