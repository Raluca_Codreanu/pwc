﻿ALTER TABLE [dbo].[CountrySettingsValueHistory]
    ADD CONSTRAINT [FK_CountrySettingsValueHistory_CountrySettings] FOREIGN KEY ([CountrySettingsGuid]) REFERENCES [dbo].[CountrySettings] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

