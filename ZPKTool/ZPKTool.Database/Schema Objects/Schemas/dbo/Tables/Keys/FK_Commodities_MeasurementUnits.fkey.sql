﻿ALTER TABLE [dbo].[Commodities]
    ADD CONSTRAINT [FK_Commodities_MeasurementUnits] FOREIGN KEY ([WeightUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

