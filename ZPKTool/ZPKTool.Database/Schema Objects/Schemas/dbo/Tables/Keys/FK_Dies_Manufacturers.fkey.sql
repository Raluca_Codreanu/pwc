﻿ALTER TABLE [dbo].[Dies]
    ADD CONSTRAINT [FK_Dies_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

