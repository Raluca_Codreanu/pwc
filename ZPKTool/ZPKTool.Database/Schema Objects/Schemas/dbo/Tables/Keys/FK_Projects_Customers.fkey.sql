﻿ALTER TABLE [dbo].[Projects]
    ADD CONSTRAINT [FK_Projects_Customers] FOREIGN KEY ([CustomerGuid]) REFERENCES [dbo].[Customers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

