﻿ALTER TABLE [dbo].[RawMaterials]
    ADD CONSTRAINT [FK_RawMaterials_MaterialsClassification] FOREIGN KEY ([ClassificationLevel1Guid]) REFERENCES [dbo].[MaterialsClassification] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

