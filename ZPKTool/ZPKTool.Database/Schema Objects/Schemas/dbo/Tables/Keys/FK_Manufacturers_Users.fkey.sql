﻿ALTER TABLE [dbo].[Manufacturers]
    ADD CONSTRAINT [FK_Manufacturers_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

