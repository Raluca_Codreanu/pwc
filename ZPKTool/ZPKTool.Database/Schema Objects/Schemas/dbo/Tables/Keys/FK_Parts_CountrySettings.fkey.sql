﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_CountrySettings] FOREIGN KEY ([CountrySettingsGuid]) REFERENCES [dbo].[CountrySettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

