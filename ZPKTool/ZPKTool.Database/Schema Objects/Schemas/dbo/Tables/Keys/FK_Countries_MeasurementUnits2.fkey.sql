﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_MeasurementUnits2] FOREIGN KEY ([LengthUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

