﻿ALTER TABLE [dbo].[Countries]
    ADD CONSTRAINT [FK_Countries_MeasurementUnits4] FOREIGN KEY ([TimeUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

