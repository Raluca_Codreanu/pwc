﻿ALTER TABLE [dbo].[Commodities]
    ADD CONSTRAINT [FK_Commodities_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

