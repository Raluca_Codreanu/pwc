﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_MeasurementUnits1] FOREIGN KEY ([WeightUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

