﻿ALTER TABLE [dbo].[ProjectCurrencies]
    ADD CONSTRAINT [FK_ProjectCurrencies_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;