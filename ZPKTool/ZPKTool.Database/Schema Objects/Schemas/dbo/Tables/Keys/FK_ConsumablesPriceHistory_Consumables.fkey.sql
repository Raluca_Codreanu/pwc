﻿ALTER TABLE [dbo].[ConsumablesPriceHistory]
    ADD CONSTRAINT [FK_ConsumablesPriceHistory_Consumables] FOREIGN KEY ([ConsumableGuid]) REFERENCES [dbo].[Consumables] ([Guid]) ON DELETE CASCADE ON UPDATE NO ACTION;

