﻿ALTER TABLE [dbo].[TransportCostCalculations]
	ADD CONSTRAINT [FK_TransportCostCalculations_Users]
	FOREIGN KEY (OwnerId)
	REFERENCES [Users] (Guid)
