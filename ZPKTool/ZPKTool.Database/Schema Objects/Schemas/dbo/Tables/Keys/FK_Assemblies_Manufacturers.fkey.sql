﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

