﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_CountrySettings] FOREIGN KEY ([CountrySettingsGuid]) REFERENCES [dbo].[CountrySettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

