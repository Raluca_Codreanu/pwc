﻿ALTER TABLE [dbo].[Projects]
    ADD CONSTRAINT [FK_Projects_OverheadSettings] FOREIGN KEY ([OverheadSettingsGuid]) REFERENCES [dbo].[OverheadSettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

