﻿ALTER TABLE [dbo].[Parts]
    ADD CONSTRAINT [FK_Parts_Manufacturers] FOREIGN KEY ([ManufacturerGuid]) REFERENCES [dbo].[Manufacturers] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

