﻿ALTER TABLE [dbo].TransportCostCalculationSettings
	ADD CONSTRAINT [FK_TransportCostCalculationSettings_TransportCostCalculations]
	FOREIGN KEY ([TransportCostCalculationId])
	REFERENCES [TransportCostCalculations] (Guid)
	ON DELETE CASCADE
