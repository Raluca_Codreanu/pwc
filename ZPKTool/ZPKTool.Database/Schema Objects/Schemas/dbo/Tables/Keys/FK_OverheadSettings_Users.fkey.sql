﻿ALTER TABLE [dbo].[OverheadSettings]
    ADD CONSTRAINT [FK_OverheadSettings_Users] FOREIGN KEY ([OwnerGuid]) REFERENCES [dbo].[Users] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

