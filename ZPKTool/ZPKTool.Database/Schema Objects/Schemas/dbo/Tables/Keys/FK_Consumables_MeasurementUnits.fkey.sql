﻿ALTER TABLE [dbo].[Consumables]
    ADD CONSTRAINT [FK_Consumables_MeasurementUnits] FOREIGN KEY ([PriceUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

