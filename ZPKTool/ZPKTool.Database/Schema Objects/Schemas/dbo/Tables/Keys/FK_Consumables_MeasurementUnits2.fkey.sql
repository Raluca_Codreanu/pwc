﻿ALTER TABLE [dbo].[Consumables]
    ADD CONSTRAINT [FK_Consumables_MeasurementUnits2] FOREIGN KEY ([AmountUnitGuid]) REFERENCES [dbo].[MeasurementUnits] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

