﻿ALTER TABLE [dbo].[Assemblies]
    ADD CONSTRAINT [FK_Assemblies_OverheadSettings] FOREIGN KEY ([OverheadSettingsGuid]) REFERENCES [dbo].[OverheadSettings] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

