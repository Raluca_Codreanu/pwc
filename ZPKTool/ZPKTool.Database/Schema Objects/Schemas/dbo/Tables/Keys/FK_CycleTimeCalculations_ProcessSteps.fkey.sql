﻿ALTER TABLE [dbo].[CycleTimeCalculations]
    ADD CONSTRAINT [FK_CycleTimeCalculations_ProcessSteps] FOREIGN KEY ([ProcessStepGuid]) REFERENCES [dbo].[ProcessSteps] ([Guid]) ON DELETE NO ACTION ON UPDATE NO ACTION;

