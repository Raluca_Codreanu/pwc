﻿CREATE TABLE [dbo].[Currencies] (
	[Guid]				UNIQUEIDENTIFIER NOT NULL DEFAULT (newsequentialid()),
	[IsoCode]			VARCHAR(6)			 NOT NULL CONSTRAINT [DF_Currencies_IsoCode] DEFAULT '000',
	[Name]				NVARCHAR (100)   NOT NULL,
	[Symbol]			NVARCHAR (25)    NOT NULL,
	[ExchangeRate]		DECIMAL (19, 8)  NOT NULL,
	[IsReleased]		BIT              NOT NULL DEFAULT 0,
	[IsMasterData]		BIT              NOT NULL DEFAULT 1,
	[IsStockMasterData] BIT				 NOT NULL DEFAULT 0, 
	[CreateDate]        DATETIME         DEFAULT (getdate()) NULL,
    CONSTRAINT [CK_Currencies_IsoCodeFormat] CHECK (NOT [IsoCode] like '%[^A-Z0-9 ]%' AND len(ltrim(rtrim([IsoCode])))>(2) AND len(ltrim(rtrim([IsoCode])))<(7))
);
GO


CREATE TRIGGER [dbo].[Currencies_Validate_IsoCode_Trigger]
    ON [dbo].[Currencies]
    FOR INSERT, UPDATE
    AS
    BEGIN
		DECLARE @codeCount int

		SELECT @codeCount = COUNT(*)
			FROM inserted ic
			WHERE ic.IsMasterData = 'True' AND (SELECT COUNT(*)
													FROM Currencies c
													WHERE c.IsMasterData = 'True' AND c.IsoCode = ic.IsoCode) > 1

		IF @codeCount >= 1
		BEGIN
			ROLLBACK
			RAISERROR(493710000, 11, 1)
		END
    END
GO

CREATE INDEX [IX_Currencies_IsoCode] ON [dbo].[Currencies] ([IsoCode])