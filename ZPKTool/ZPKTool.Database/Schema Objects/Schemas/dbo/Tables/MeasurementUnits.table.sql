﻿CREATE TABLE [dbo].[MeasurementUnits] (
    [Name]           NVARCHAR (100)   NOT NULL,
    [Symbol]         NVARCHAR (25)    NOT NULL,
    [ConversionRate] DECIMAL (28, 14) NOT NULL,
    [Type]           SMALLINT         NOT NULL,
    [IsReleased]     BIT              DEFAULT ((0)) NOT NULL,
    [Guid]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [ScaleID]        INT              NULL,
    [ScaleFactor]    DECIMAL (28, 14) DEFAULT ((1)) NOT NULL,
    [CreateDate]     DATETIME         DEFAULT (getdate()) NULL
);

