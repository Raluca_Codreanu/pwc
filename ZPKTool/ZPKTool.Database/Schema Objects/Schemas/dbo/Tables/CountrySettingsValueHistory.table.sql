﻿CREATE TABLE [dbo].[CountrySettingsValueHistory] (
    [Timestamp]                DATETIME         DEFAULT (getdate()) NOT NULL,
    [UnskilledLaborCost]       DECIMAL (29, 16) NULL,
    [SkilledLaborCost]         DECIMAL (29, 16) NULL,
    [ForemanCost]              DECIMAL (29, 16) NULL,
    [TechnicianCost]           DECIMAL (29, 16) NULL,
    [EngineerCost]             DECIMAL (29, 16) NULL,
    [EnergyCost]               DECIMAL (29, 16) NULL,
    [AirCost]                  DECIMAL (29, 16) NULL,
    [WaterCost]                DECIMAL (29, 16) NULL,
    [ProductionAreaRentalCost] DECIMAL (29, 16) NULL,
    [OfficeAreaRentalCost]     DECIMAL (29, 16) NULL,
    [InterestRate]             DECIMAL (19, 10) NULL,
    [ShiftCharge1ShiftModel]   DECIMAL (19, 10) NULL,
    [ShiftCharge2ShiftModel]   DECIMAL (19, 10) NULL,
    [ShiftCharge3ShiftModel]   DECIMAL (19, 10) NULL,
    [LaborAvailability]        DECIMAL (19, 10) NULL,
    [IsScrambled]              BIT              DEFAULT ((0)) NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [CountrySettingsGuid]      UNIQUEIDENTIFIER NOT NULL
);

