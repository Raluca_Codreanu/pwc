﻿CREATE TABLE [dbo].[Processes] (
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsMasterData] BIT              DEFAULT ((0)) NOT NULL,
    [OwnerGuid]    UNIQUEIDENTIFIER NULL,
	[IsStockMasterData] BIT         NOT NULL DEFAULT 0,
	[CreateDate]   DATETIME         DEFAULT (getdate()) NULL
);

