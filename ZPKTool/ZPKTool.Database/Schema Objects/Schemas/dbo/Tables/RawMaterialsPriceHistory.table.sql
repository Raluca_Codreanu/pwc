﻿CREATE TABLE [dbo].[RawMaterialsPriceHistory] (
    [Timestamp]       DATETIME         DEFAULT (getdate()) NOT NULL,
    [Price]           DECIMAL (29, 16) NOT NULL,
    [IsScrambled]     BIT              DEFAULT ((0)) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [RawMaterialGuid] UNIQUEIDENTIFIER NOT NULL
);

