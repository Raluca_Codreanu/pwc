﻿CREATE TABLE [dbo].[OverheadSettings] (
    [MaterialOverhead]              DECIMAL (19, 10) NOT NULL,
    [ConsumableOverhead]            DECIMAL (19, 10) NOT NULL,
    [CommodityOverhead]             DECIMAL (19, 10) NOT NULL,
    [ExternalWorkOverhead]          DECIMAL (19, 10) NOT NULL,
    [ManufacturingOverhead]         DECIMAL (19, 10) NOT NULL,
    [MaterialMargin]                DECIMAL (19, 10) NOT NULL,
    [ConsumableMargin]              DECIMAL (19, 10) NOT NULL,
    [CommodityMargin]               DECIMAL (19, 10) NOT NULL,
    [ExternalWorkMargin]            DECIMAL (19, 10) NOT NULL,
    [ManufacturingMargin]           DECIMAL (19, 10) NOT NULL,
    [OtherCostOHValue]              DECIMAL (19, 10) NOT NULL,
    [PackagingOHValue]              DECIMAL (19, 10) NOT NULL,
    [LogisticOHValue]               DECIMAL (19, 10) NOT NULL,
    [SalesAndAdministrationOHValue] DECIMAL (19, 10) NOT NULL,
    [CompanySurchargeOverhead]      DECIMAL (19, 10) DEFAULT ((0)) NOT NULL,
    [IsMasterData]                  BIT              DEFAULT ((0)) NOT NULL,
    [LastUpdateTime]                DATETIME         NULL,
    [Guid]                          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [OwnerGuid]                     UNIQUEIDENTIFIER NULL,
	[IsStockMasterData]             BIT              NOT NULL DEFAULT 0,
    [CreateDate]                    DATETIME         DEFAULT (getdate()) NULL
);

