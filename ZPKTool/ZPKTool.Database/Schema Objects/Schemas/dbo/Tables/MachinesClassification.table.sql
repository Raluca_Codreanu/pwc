﻿CREATE TABLE [dbo].[MachinesClassification] (
    [Name]       NVARCHAR (100)   NOT NULL,
    [Guid]       UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsReleased] BIT              DEFAULT ((0)) NOT NULL,
    [ParentGuid] UNIQUEIDENTIFIER NULL,
    [CreateDate] DATETIME         DEFAULT (getdate()) NULL
);

