﻿CREATE TABLE [dbo].[Manufacturers] (
    [Name]         NVARCHAR (100)   NOT NULL,
    [Description]  NVARCHAR (2000)  NULL,
    [IsMasterData] BIT              DEFAULT ((0)) NOT NULL,
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [OwnerGuid]    UNIQUEIDENTIFIER NULL,
	[IsStockMasterData] BIT         NOT NULL DEFAULT 0,
    [CreateDate]   DATETIME         DEFAULT (getdate()) NULL
);

