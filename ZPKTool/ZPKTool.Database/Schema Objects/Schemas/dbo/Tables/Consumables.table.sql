﻿CREATE TABLE [dbo].[Consumables] (
    [Name]             NVARCHAR (100)   NOT NULL,
    [Description]      NVARCHAR (2000)  NULL,
    [Price]            DECIMAL (29, 16) NULL,
    [Amount]           DECIMAL (28, 14) NULL,
    [IsMasterData]     BIT              DEFAULT ((0)) NOT NULL,
    [IsDeleted]        BIT              DEFAULT ((0)) NOT NULL,
    [Guid]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [ManufacturerGuid] UNIQUEIDENTIFIER NULL,
    [PriceUnitGuid]    UNIQUEIDENTIFIER NULL,
    [ProcessStepGuid]  UNIQUEIDENTIFIER NULL,
    [OwnerGuid]        UNIQUEIDENTIFIER NULL,
    [AmountUnitGuid]   UNIQUEIDENTIFIER NULL,
	[IsStockMasterData] BIT             NOT NULL DEFAULT 0,
    [CreateDate]       DATETIME         DEFAULT (getdate()) NULL,
	[CalculationCreateDate] DATETIME    NULL
);

