﻿CREATE TABLE [dbo].[Bookmarks] (
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [EntityTypeId] SMALLINT         NOT NULL,
    [EntityId]     UNIQUEIDENTIFIER NOT NULL,
    [OwnerId]      UNIQUEIDENTIFIER NOT NULL,
    [CreateDate]   DATETIME         DEFAULT (getdate()) NULL
);

