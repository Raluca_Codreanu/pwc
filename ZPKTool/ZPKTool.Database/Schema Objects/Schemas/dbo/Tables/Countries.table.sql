﻿CREATE TABLE [dbo].[Countries] (
    [Name]           NVARCHAR (100)   NOT NULL,
    [IsReleased]     BIT              DEFAULT ((0)) NOT NULL,
    [Guid]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [CurrencyGuid]   UNIQUEIDENTIFIER NOT NULL,
    [WeightUnitGuid] UNIQUEIDENTIFIER NOT NULL,
    [LengthUnitGuid] UNIQUEIDENTIFIER NOT NULL,
    [VolumeUnitGuid] UNIQUEIDENTIFIER NOT NULL,
    [TimeUnitGuid]   UNIQUEIDENTIFIER NOT NULL,
    [FloorUnitGuid]  UNIQUEIDENTIFIER NOT NULL,
    [SettingsGuid]   UNIQUEIDENTIFIER NOT NULL,
	[IsStockMasterData]  BIT          NOT NULL DEFAULT 0,
	[CreateDate]     DATETIME         DEFAULT (getdate()) NULL
);

