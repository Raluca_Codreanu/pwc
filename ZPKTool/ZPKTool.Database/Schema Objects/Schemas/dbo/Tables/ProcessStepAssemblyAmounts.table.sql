﻿CREATE TABLE [dbo].[ProcessStepAssemblyAmounts] (
    [Amount]          INT              NOT NULL,
    [Guid]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsMasterData]    BIT              DEFAULT ((0)) NOT NULL,
    [OwnerGuid]       UNIQUEIDENTIFIER NULL,
    [ProcessStepGuid] UNIQUEIDENTIFIER NOT NULL,
    [AssemblyGuid]    UNIQUEIDENTIFIER NOT NULL,
	[IsStockMasterData] BIT            NOT NULL DEFAULT 0
);
GO


-- Delete the amounts that are not valid, the process of the amount's process step and the process of the parent assembly of the amount's part need to be the same.
CREATE TRIGGER [dbo].[ProcessStepAssemblyAmounts_insert_validity_check]
	ON [dbo].[ProcessStepPartAmounts]
	AFTER INSERT
AS 
BEGIN
	DELETE
		FROM ProcessStepPartAmounts
		WHERE Guid IN (SELECT ProcessStepPartAmounts.Guid
							FROM ProcessStepPartAmounts
								INNER JOIN ProcessSteps ON ProcessStepPartAmounts.ProcessStepGuid = ProcessSteps.Guid
								INNER JOIN Parts ON ProcessStepPartAmounts.PartGuid = Parts.Guid
								INNER JOIN Assemblies ON Parts.ParentAssemblyGuid = Assemblies.Guid
							WHERE Assemblies.ProcessGuid != ProcessSteps.ProcessGuid)
END
GO