﻿CREATE TABLE [dbo].[CycleTimeCalculations] (
    [Description]              NVARCHAR (100)   NOT NULL,
    [RawPartDescription]       NVARCHAR (100)   NULL,
    [MachiningVolume]          DECIMAL (19, 4)  NULL,
    [MachiningType]            SMALLINT         NULL,
    [ToleranceType]            NVARCHAR (100)   NULL,
    [SurfaceType]              NVARCHAR (100)   NULL,
    [TransportTimeFromBuffer]  DECIMAL (19, 3)  NOT NULL,
    [ToolChangeTime]           DECIMAL (19, 3)  NOT NULL,
    [TransportClampingTime]    DECIMAL (19, 3)  NOT NULL,
    [MachiningTime]            DECIMAL (19, 3)  NOT NULL,
    [TakeOffTime]              DECIMAL (19, 3)  NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [IsMasterData]             BIT              DEFAULT ((0)) NOT NULL,
    [Index]                    INT              NULL,
    [MachiningCalculationData] VARBINARY (MAX)  NULL,
    [ProcessStepGuid]          UNIQUEIDENTIFIER NOT NULL,
    [OwnerGuid]                UNIQUEIDENTIFIER NULL,
	[IsStockMasterData]        BIT              NOT NULL DEFAULT 0,
	[CreateDate]               DATETIME         DEFAULT (getdate()) NULL
);

