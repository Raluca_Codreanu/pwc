﻿CREATE TABLE [dbo].[TrashBinItems] (
    [EntityType]      SMALLINT         NOT NULL,
    [EntityGuid]      UNIQUEIDENTIFIER NOT NULL,
    [EntityName]      NVARCHAR (100)   NULL,
    [EntityMediaGuid] UNIQUEIDENTIFIER NULL,
    [Guid]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [OwnerGuid]       UNIQUEIDENTIFIER NULL,
	[CreateDate]      DATETIME         DEFAULT (getdate()) NULL
);

