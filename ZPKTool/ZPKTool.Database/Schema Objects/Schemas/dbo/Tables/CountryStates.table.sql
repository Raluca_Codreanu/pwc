﻿CREATE TABLE [dbo].[CountryStates] (
    [Name]         NVARCHAR (100)   NOT NULL,
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [CountryGuid]  UNIQUEIDENTIFIER NOT NULL,
    [SettingsGuid] UNIQUEIDENTIFIER NOT NULL,
    [CreateDate]   DATETIME         DEFAULT (getdate()) NULL
);

