﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
PRINT '/**********************************************/'
PRINT '/*** Post-Deployment Script execution start ***/'
PRINT '/**********************************************/'

:r ..\..\..\ZPKTool.Database\Scripts\Post-Deployment\Script.PostDeployment.Common.sql

PRINT 'Updating the master data...'
:r .\update_master_data.sql
GO

PRINT 'Updating the master data machines pictures...'
:r .\update_master_data_machines_pictures.sql
GO

PRINT '/********************************************/'
PRINT '/*** Post-Deployment Script execution end ***/'
PRINT '/********************************************/'