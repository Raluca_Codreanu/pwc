﻿/*
 Pre-Deployment Script Template
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT '/*********************************************/'
PRINT '/*** Pre-Deployment Script execution start ***/'
PRINT '/*********************************************/'


:r ..\..\..\ZPKTool.Database\Scripts\Pre-Deployment\Script.PreDeployment.Common.sql

/* For some reason, when updating the central database the deployment app (Sqlpackage.exe) does not drop the 'FK_Users_Countries' foreign key,
   causing an error when trying to drop the Users.FavouriteCountryGuid column because is referred by this foreign key.
   This is strange because it drops it when deploying the local database. To fix that issue we manually drop it here.
*/
IF OBJECT_ID('FK_Users_Countries', 'F') IS NOT NULL
BEGIN
	PRINT 'Dropping [FK_Users_Countries]...'
	ALTER TABLE [Users] DROP CONSTRAINT [FK_Users_Countries]
END


PRINT '/*******************************************/'
PRINT '/*** Pre-Deployment Script execution end ***/'
PRINT '/*******************************************/'