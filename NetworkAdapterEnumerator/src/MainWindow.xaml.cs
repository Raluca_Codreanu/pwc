﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetworkAdapterEnumerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke((Action)(() => this.LoadNetworkAdapersData()));
        }

        private void LoadNetworkAdapersData()
        {
            var adapters = new List<AdapterDTO>();
            bool hasPhysicalAdapterProp = false;

            var query = @"SELECT * FROM Win32_NetworkAdapter";
            var searcher = new ManagementObjectSearcher(query);
            foreach (ManagementBaseObject mbo in searcher.Get())
            {
                var props = mbo.Properties.Cast<PropertyData>();
                var description = props.FirstOrDefault(p => p.Name == "Description").Value;

                if (!hasPhysicalAdapterProp)
                {
                    hasPhysicalAdapterProp = props.FirstOrDefault(p => p.Name == "PhysicalAdapter") != null;
                }

                var adapter = new AdapterDTO();
                adapter.Name = description != null ? description.ToString() : string.Empty;
                adapter.Properties.AddRange(props.Select(p => p.Name + " = " + p.Value));

                adapters.Add(adapter);
            }

            this.Adapters.ItemsSource = adapters;

            this.PhysicalAdapterPropertyText.Text = hasPhysicalAdapterProp ? "Physical adapter present." : "Physical adapter not present.";
        }

        public class AdapterDTO
        {
            public AdapterDTO()
            {
                this.Properties = new List<string>();
            }

            public string Name { get; set; }

            public List<string> Properties { get; private set; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var adapters = this.Adapters.ItemsSource as IEnumerable<AdapterDTO>;
            if (adapters == null)
            {
                MessageBox.Show("Internal error", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = "*.txt|*.txt";
            if (saveDialog.ShowDialog() != true)
            {
                return;
            }

            using (var file = new StreamWriter(saveDialog.FileName, false))
            {
                foreach (var adapter in adapters)
                {
                    file.WriteLine(string.Empty);
                    file.WriteLine("[" + adapter.Name + "]");
                    file.WriteLine(string.Join(Environment.NewLine, adapter.Properties));
                }
            }
        }
    }
}
