﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using ZPKTool.Common;
using ZPKTool.Data;

namespace InstallerHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var operation = Operation.None;
                var server = string.Empty;
                var db = string.Empty;
                var user = string.Empty;
                var pass = string.Empty;

                // Parse the arguments.
                foreach (var arg in args)
                {
                    if (string.Equals(arg, "adbv", StringComparison.OrdinalIgnoreCase))
                    {
                        operation = Operation.AppDbVersion;
                    }
                    else if (string.Equals(arg, "dbv", StringComparison.OrdinalIgnoreCase))
                    {
                        operation = Operation.DbVersion;
                    }
                    else if (arg.StartsWith("server", StringComparison.OrdinalIgnoreCase))
                    {
                        var keyValueIndex = arg.IndexOf("=");
                        if (keyValueIndex > -1)
                        {
                            server = arg.Substring(keyValueIndex + 1);
                        }
                    }
                    else if (arg.StartsWith("db", StringComparison.OrdinalIgnoreCase))
                    {
                        var keyValueIndex = arg.IndexOf("=");
                        if (keyValueIndex > -1)
                        {
                            db = arg.Substring(keyValueIndex + 1);
                        }
                    }
                    else if (arg.StartsWith("user", StringComparison.OrdinalIgnoreCase))
                    {
                        var keyValueIndex = arg.IndexOf("=");
                        if (keyValueIndex > -1)
                        {
                            user = arg.Substring(keyValueIndex + 1);
                        }
                    }
                    else if (arg.StartsWith("pass", StringComparison.OrdinalIgnoreCase))
                    {
                        var keyValueIndex = arg.IndexOf("=");
                        if (keyValueIndex > -1)
                        {
                            pass = arg.Substring(keyValueIndex + 1);
                        }
                    }
                }

                switch (operation)
                {
                    case Operation.None:
                        Console.WriteLine(0);
                        break;
                    case Operation.AppDbVersion:
                        Console.WriteLine(Constants.DataBaseVersion.ToString(CultureInfo.InvariantCulture));
                        break;
                    case Operation.DbVersion:
                        var version = GetDBVersion(server, db, user, pass);
                        Console.WriteLine(version.ToString(CultureInfo.InvariantCulture));
                        break;
                }
            }
            catch
            {
                Console.WriteLine(-1);
            }
        }

        /// <summary>
        /// Gets the database version using the specified parameters.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="db">The database name.</param>
        /// <param name="user">The user name.</param>
        /// <param name="pass">The password.</param>
        /// <returns>The database version.</returns>
        /// <exception cref="ArgumentException">The server and db are mandatory.</exception>
        /// <remarks>Since PCM 1.7 the db version is encrypted in GlobalSettings table, it was previously in plain text in DataBaseInfo table.</remarks>
        private static decimal GetDBVersion(string server, string db, string user, string pass)
        {
            if (string.IsNullOrWhiteSpace(server)
                || string.IsNullOrWhiteSpace(db))
            {
                throw new ArgumentException(string.Format("The server:'{0}' and db:'{1}' are mandatory.", server, db));
            }

            // Build the connection string.
            var connectionString = string.Empty;
            if (!string.IsNullOrWhiteSpace(server)
                && !string.IsNullOrWhiteSpace(db))
            {
                connectionString = string.Format(@"Server={0};Database={1};", server, db);
                if (string.IsNullOrWhiteSpace(user)
                    || string.IsNullOrWhiteSpace(pass))
                {
                    connectionString += "Trusted_Connection=True;";
                }
                else
                {
                    connectionString += "Trusted_Connection=False;";
                    connectionString += string.Format(@"User Id={0};Password={1};", user, pass);
                }
            }

            var dbVersion = -1M;
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Try to get the database version from GlobalSettings table.
                try
                {
                    using (var commnand = new SqlCommand())
                    {
                        commnand.Connection = connection;
                        commnand.CommandText = "SELECT COUNT(*) FROM " + db + ".INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = 'GlobalSettings'";
                        var tableExists = (int)commnand.ExecuteScalar() > 0;
                        if (tableExists)
                        {
                            var dbVersionSettingKey = Encryption.Encrypt("Version", Encryption.EncryptionPassword);
                            commnand.CommandText = "SELECT TOP 1 Value FROM GlobalSettings WHERE [Key] = @key";
                            commnand.Parameters.Add(new SqlParameter("@key", dbVersionSettingKey));
                            var dbVersionSettingValue = (string)commnand.ExecuteScalar();
                            var dbVersionString = Encryption.Decrypt(dbVersionSettingValue, Encryption.EncryptionPassword);
                            decimal.TryParse(dbVersionString, NumberStyles.Any, CultureInfo.InvariantCulture, out dbVersion);
                        }
                    }
                }
                catch
                {
                    dbVersion = -1;
                }

                // If the database version was not retrieved from GlobalSettings table try to get it from DataBaseInfo table.
                if (dbVersion <= 0)
                {
                    try
                    {
                        using (var commnand = new SqlCommand())
                        {
                            commnand.Connection = connection;
                            commnand.CommandText = "SELECT COUNT(*) FROM " + db + ".INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = 'DataBaseInfo'";
                            var tableExists = (int)commnand.ExecuteScalar() > 0;
                            if (tableExists)
                            {
                                commnand.CommandText = "SELECT TOP 1 Value FROM DataBaseInfo WHERE [Key] = 'Version'";
                                var dbVersionString = (string)commnand.ExecuteScalar();
                                decimal.TryParse(dbVersionString, NumberStyles.Any, CultureInfo.InvariantCulture, out dbVersion);
                            }
                        }
                    }
                    catch
                    {
                        dbVersion = -1;
                    }
                }
            }

            return dbVersion;
        }
    }

    /// <summary>
    /// The operation.
    /// </summary>
    internal enum Operation
    {
        None,

        // Database version from code.
        AppDbVersion,

        // Database version from db.
        DbVersion
    }
}