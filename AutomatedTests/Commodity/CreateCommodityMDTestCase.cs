﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.Commodity
{
    [TestClass]
    public class CreateCommodityMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        [TestMethod]
        [TestCategory("Commodity")]
        public void CreateCommodityMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode commoditiesNode = app.MainScreen.ProjectsTree.MasterDataCommodities;
                Assert.IsNotNull(commoditiesNode, "The Commodities node was not found.");                
                commoditiesNode.Click();

                ListViewControl commoditiesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(commoditiesDataGrid, "Commodities data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 5
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                CommodityTestsHelper.CreateCommodity(app);
            }
        }

        [TestMethod]
        [TestCategory("Commodity")]
        public void CancelCreateCommodityMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                TreeNode commoditiesNode = app.MainScreen.ProjectsTree.MasterDataCommodities;
                Assert.IsNotNull(commoditiesNode, "The Commodities node was not found.");

                commoditiesNode.Click();
                ListViewControl commoditiesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(commoditiesDataGrid, "Commodities data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                Window createCommodity = app.Windows.CommodityWindow;

                //step 2
                Button cancelButton = createCommodity.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3                
                addButton.Click();
                createCommodity = app.Windows.CommodityWindow;
                TextBox nameTextBox = createCommodity.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found");
                nameTextBox.Text = "commodity" + DateTime.Now.Ticks;
                TextBox amountTextBox = createCommodity.Get<TextBox>(CommodityAutomationIds.AmountTextBox);
                Assert.IsNotNull(amountTextBox, "Amount text box was not found");
                amountTextBox.Text = "10";

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox manufacturerName = createCommodity.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(manufacturerName, "Manufacturer name text box was not found.");
                manufacturerName.Text = "manufacturerTest";
                TextBox descriptionTextBox = createCommodity.Get<TextBox>(CommodityAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";

                cancelButton = createCommodity.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 4
                createCommodity.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 5                
                cancelButton.Click();
                createCommodity.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 6                
                cancelButton.Click();
                createCommodity.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 7                
                addButton.Click();
                createCommodity = Wait.For(() => app.Windows.CommodityWindow);
                var mouse = createCommodity.Mouse;
                mouse.Location = new System.Windows.Point(createCommodity.Bounds.TopRight.X - 10, createCommodity.Bounds.TopRight.Y + 6);
                mouse.Click();

                //step 8                
                addButton.Click();
                createCommodity = app.Windows.CommodityWindow;
                nameTextBox = createCommodity.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "11commodity";
                amountTextBox = createCommodity.Get<TextBox>(CommodityAutomationIds.AmountTextBox);
                amountTextBox.Text = "10";

                manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                manufacturerName = createCommodity.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";
                                
                createCommodity.Close();

                //step 9
                app.Windows.CommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 10                
                createCommodity.Close();
                app.Windows.CommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper
        #endregion Helper
    }
}
