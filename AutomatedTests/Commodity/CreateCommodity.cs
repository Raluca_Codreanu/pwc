﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using ZPKTool.Data;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.Commodity
{
    [TestClass]
    public class CreateCommodity
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            CreateCommodity.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Create Commodity Test Case (id=374).
        /// </summary>
        [TestMethod]
        [TestCategory("Commodity")]
        public void CreateCommodityTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectCommodity");
                project1Node.SelectEx();

                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectCommodity");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectCommodity node was not found.");

                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                Menu createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();

                CommodityTestsHelper.CreateCommodity(app);
            }
        }

        /// <summary>
        /// An automated test for Cancel Create Commodity Test Case (id=372).
        /// </summary>
        [TestMethod]
        [TestCategory("Commodity")]
        public void CancelCreateCommodityTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectCancelCreateCommodity");

                project1Node.Click();
                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectCancelCreateCommodity");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectCancelCreateCommodity node was not found.");

                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.Collapse();
                partNode.SelectEx();

                //step 1
                Menu createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();
                Window createCommodityWindow = app.Windows.CommodityWindow;

                //step 2
                Button cancelButton = createCommodityWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3
                project1Node = myProjectsNode.GetNode("projectCancelCreateCommodity");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectCommodity node was not found.");

                partNode = project1Node.GetNode("simplePartProject");
                partNode.Collapse();
                partNode.SelectEx();

                createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();
                createCommodityWindow = app.Windows.CommodityWindow;

                TextBox nameTextBox = createCommodityWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "commodityTest";

                createCommodityWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);

                cancelButton = createCommodityWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 4                
                Assert.IsTrue(createCommodityWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");

                createCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 5                
                cancelButton.Click();
                createCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 6                
                cancelButton.Click();
                Assert.IsNotNull(createCommodityWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");
                createCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 7
                project1Node = myProjectsNode.GetNode("projectCancelCreateCommodity");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectCancelCreateCommodity node was not found.");

                partNode = project1Node.GetNode("simplePartProject");
                partNode.Collapse();
                partNode.SelectEx();

                createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();

                createCommodityWindow = app.Windows.CommodityWindow;

                // press X button in order to close the message window                
                createCommodityWindow.Close();

                //step 8
                project1Node = myProjectsNode.GetNode("projectCancelCreateCommodity");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectCancelCreateCommodity node was not found.");

                partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();

                createCommodityWindow = app.Windows.CommodityWindow;

                nameTextBox = createCommodityWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "commodityTest";

                //step 9                
                createCommodityWindow.Close();
                Assert.IsTrue(createCommodityWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");
                createCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 10                
                createCommodityWindow.Close();

                //step 9                
                Assert.IsTrue(createCommodityWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");
                createCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper
        /// <summary>
        /// Creates the data for this test
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectCommodity";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();

            Project project2 = new Project();
            project2.Name = "projectCancelCreateCommodity";
            project2.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer1";
            project2.Customer = c;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 1;
            project2.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project2);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
