﻿@EditCommodity
Feature: EditCommodity

Scenario: Cancel edit Commodity no changes
	Given I select a process step
	And I select the Commodity tab
	And I select a row to press Edit 
	When I press cancel
	Then the edit window is closed

Scenario: Edit Commodity dbl click
	Given I select a process step
	And I select the Commodity tab 
	And I press double click to a row
	And I change a field
	When I press save btn
	Then value is saved