﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ProjectTests;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;

namespace ZPKTool.AutomatedTests.Commodity
{
    /// <summary>
    /// The automated test of Delete Commodity Test Case.
    /// </summary>
    [TestClass]
    public class DeleteCommodityTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]   
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Commodity Test Case. (id = 38)
        /// </summary>
        [TestMethod]
        [TestCategory("Commodity")]
        public void DeleteCommodityTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 2
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();

                Window createProjectWindow = app.Windows.ProjectWindow;
                //step 3
                TextBox nameTextBox = createProjectWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "projectCommodityDelete";

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);                
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.SelectEx();
                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();
                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                //step 5
                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assemblyCommodityDelete";

                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();

                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("France");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
                mainWindow.WaitForAsyncUITasks();
                //step 6
                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.Collapse();

                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();

                //step 7                
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);
                mainWindow.WaitForAsyncUITasks();

                myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                
                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.ExpandEx();

                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.ExpandEx();

                //step 8
                TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                Menu createCommodityMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityMenuItem.Click();
                mainWindow.WaitForAsyncUITasks();

                //step 9        
                CommodityTestsHelper.CreateCommodity(app);

                //step 10                
                partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                //step 11
                TreeNode commodityNode = materialsNode.GetNode("commodity1");
                commodityNode.SelectEx();

                //step 12
                Menu deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 13
                deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                //step 14
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                ListViewControl trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                ListViewRow commodity1Row = trashBinDataGrid.Row("Name", "commodity1");
                Assert.IsNotNull(commodity1Row, "Project1Row trash bin item was not found.");

                ListViewCell selectedCell = trashBinDataGrid. CellByIndex(0, commodity1Row);
                AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;

                //step 15
                Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
                viewDetailsButton.Click();

                //step 16
                Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                recoverButton.Click();
                Thread.Sleep(200);

                //step 17                
                Assert.IsTrue(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not found.");
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                Thread.Sleep(200);

                recoverButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish             

                //step 19                
                myProjectsNode.ExpandEx();

                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.ExpandEx();
                project1Node.SelectEx();

                partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                commodityNode = materialsNode.GetNode("commodity1");
                commodityNode.SelectEx();

                deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                TreeNode trashbinnode = app.MainScreen.ProjectsTree.TrashBin;
                trashbinnode.SelectEx();
                trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                commodity1Row = trashBinDataGrid.Row("Name", "commodity1");
                Assert.IsNotNull(commodity1Row, "Project1Row trash bin item was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, commodity1Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                Thread.Sleep(500);

                Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

                permanentlyDeleteButton.Click();

                Assert.IsNotNull(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not found.");
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                //step 20                
                myProjectsNode.ExpandEx();
                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.ExpandEx();

                TreeNode assemblyNode = project1Node.GetNode("assemblyCommodityDelete");
                assemblyNode.SelectEx();

                createPartMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.SelectEx();

                assemblyNode = project1Node.GetNode("assemblyCommodityDelete");
                assemblyNode.ExpandEx();

                TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                partsNode.ExpandEx();

                partNode = partsNode.GetNode("simplePartProject");
                partNode.ExpandEx();

                //step 8
                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                createCommodityMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityMenuItem.Click();

                //step 9        
                CommodityTestsHelper.CreateCommodity(app);

                assemblyNode = project1Node.GetNode("assemblyCommodityDelete");
                assemblyNode.ExpandEx();

                partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                partsNode.ExpandEx();

                //step 10                
                partNode = partsNode.GetNode("simplePartProject");
                partNode.ExpandEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.ExpandEx();

                //step 11
                commodityNode = materialsNode.GetNode("commodity1");
                commodityNode.SelectEx();

                //step 12
                deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 13
                deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                //step 14          
                TreeNode trashbinNode = app.MainScreen.ProjectsTree.TrashBin;     
                trashbinNode.SelectEx();
                trashbinNode.Click();
                trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                commodity1Row = trashBinDataGrid.Row("Name", "commodity1");
                Assert.IsNotNull(commodity1Row, "Project1Row trash bin item was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, commodity1Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                Thread.Sleep(500);
                //step 15
                viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
                viewDetailsButton.Click();

                //step 16
                recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                recoverButton.Click();
                Thread.Sleep(200);

                //step 17                
                Assert.IsNotNull(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not found.");
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                Thread.Sleep(200);

                recoverButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish             

                //step 19                
                myProjectsNode.ExpandEx();

                project1Node = myProjectsNode.GetNode("projectCommodityDelete");
                project1Node.ExpandEx();

                assemblyNode = project1Node.GetNode("assemblyCommodityDelete");
                assemblyNode.ExpandEx();

                partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                partsNode.SelectEx();

                partNode = partsNode.GetNode("simplePartProject");
                partNode.ExpandEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.ExpandEx();

                commodityNode = materialsNode.GetNode("commodity1");
                commodityNode.SelectEx();

                deleteMenuItem = commodityNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                TreeNode trashBinnode = app.MainScreen.ProjectsTree.TrashBin;
                trashBinnode.SelectEx();
                trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));

                commodity1Row = trashBinDataGrid.Row("Name", "commodity1");
                Assert.IsNotNull(commodity1Row, "Project1Row trash bin item was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, commodity1Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                Thread.Sleep(500);

                permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

                permanentlyDeleteButton.Click();

                Assert.IsNotNull(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not found.");
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }
    }
}
