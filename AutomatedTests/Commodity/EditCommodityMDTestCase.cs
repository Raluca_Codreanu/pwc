﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.Commodity
{
    [TestClass]
    public class EditCommodityMDTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditCommodityMDTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        [TestMethod]
        [TestCategory("Commodity")]
        public void EditCommodityMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode commoditiesNode = app.MainScreen.ProjectsTree.MasterDataCommodities;
                Assert.IsNotNull(commoditiesNode, "The Commodities node was not found.");

                //step 4
                commoditiesNode.Click();
                ListViewControl commoditiesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(commoditiesDataGrid, "Commodities data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 6
                commoditiesDataGrid.Select("Name", "1commodity");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");
                editButton.Click();
                Window editCommodityWindow = app.Windows.CommodityWindow;

                //step 7
                TextBox nameTextBox = editCommodityWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "commodity_Updated";

                TextBox partNumberTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.PartNumberTextBox);
                partNumberTextBox.Text = "10";

                TextBox priceTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.PriceTextBox);
                priceTextBox.Text = "10";

                TextBox amountTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.AmountTextBox);
                amountTextBox.Text = "10";
                TextBox weightTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.WeightTextBox);
                weightTextBox.Text = "10";

                Button cancelButton = editCommodityWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();
                //step 8
                editCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                cancelButton = editCommodityWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                editCommodityWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 12
                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();
                Assert.IsNotNull(editButton, "The Edit button was not found.");

                editCommodityWindow = app.Windows.CommodityWindow;
                nameTextBox = editCommodityWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = string.Empty;

                // Move the focus from the name text box otherwise the validity of the screen input is not evaluated.
                partNumberTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.PartNumberTextBox);
                partNumberTextBox.Focus();

                //step 13
                Button saveButton = editCommodityWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsFalse(saveButton.Enabled, "Save Button should be disabled");

                nameTextBox.Text = "commodity_Updated";

                partNumberTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.PartNumberTextBox);
                partNumberTextBox.Text = "100";

                priceTextBox = editCommodityWindow.Get<TextBox>(CommodityAutomationIds.PriceTextBox);
                priceTextBox.Text = "10";
                saveButton.Click();
            }
        }

        #region Helper

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            ZPKTool.Data.Commodity commodity = new ZPKTool.Data.Commodity();
            commodity.SetOwner(adminUser);
            commodity.IsMasterData = true;
            commodity.Name = "1commodity";
            commodity.Amount = 100;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            commodity.Manufacturer = manufacturer1;

            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();
        }
        #endregion Helper
    }
}
