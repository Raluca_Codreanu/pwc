﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests.Commodity
{
    [Binding]
    public class CommodityImportExportSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string commodityExportPath;

        public CommodityImportExportSteps()
        {
        }

        public static void MyClassInitialize()
        {
            CommodityImportExportSteps.commodityExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".commodity");
            if (File.Exists(CommodityImportExportSteps.commodityExportPath))
            {
                File.Delete(CommodityImportExportSteps.commodityExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(CommodityImportExportSteps.commodityExportPath))
            {
                File.Delete(CommodityImportExportSteps.commodityExportPath);
            }
        }

        [BeforeFeature("CommodityImportExport")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");

            importProjectMenuItem.Click();
            System.Threading.Thread.Sleep(200);

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectIECommodity.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CommodityImportExport")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I select the commodity")]
        public void GivenISelectTheCommodity()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIECommodity");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIECommodity");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            TreeNode commodityNode = materialsNode.GetNode("commodityTest");
            commodityNode.SelectEx();
        }

        [Given(@"I choose the Export menu item")]
        public void GivenIChooseTheExportMenuItem()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            var exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }

        [Given(@"I click Cancel")]
        public void GivenIClickCancel()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }

        [Given(@"I select again the Commodity to export it")]
        public void GivenISelectAgainTheCommodityToExportIt()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            var exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }

        [Given(@"I press the save button")]
        public void GivenIPressTheSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(CommodityImportExportSteps.commodityExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(CommodityImportExportSteps.commodityExportPath), "The commodity was not exported at the specified location.");
        }

        [Given(@"I select the Materials node to import commodity")]
        public void GivenISelectTheMaterialsNodeToImportCommodity()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIECommodity");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIECommodity");
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            Menu importPartMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportCommodity);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem.Click();
        }

        [When(@"I select the commodity which will be imported")]
        public void WhenISelectTheCommodityWhichWillBeImported()
        {
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\commodityTest.commodity"));
            mainWindow.WaitForAsyncUITasks();
        }

        [Then(@"the Commodity is imported")]
        public void ThenTheCommodityIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIECommodity");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIECommodity");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            var importedPartNode = materialsNode.GetNode("commodityTest");
            Assert.IsNotNull(importedPartNode, "The imported commodity did not appear in projects tree.");
        }
    }
}
