﻿@CommodityImportExport
Feature: CommodityImportExport

Scenario: Import Export Commodity
	Given I select the commodity
	And I choose the Export menu item
	And I click Cancel 
	And I select again the Commodity to export it
	And I press the save button
	And I select the Materials node to import commodity
	When I select the commodity which will be imported
	Then the Commodity is imported
