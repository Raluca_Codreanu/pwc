﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.Commodity
{
    public static class CommodityAutomationIds
    {
        public const string PartNumberTextBox = "PartNumberTextBox";
        public const string PriceTextBox = "PriceTextBox";
        public const string AmountTextBox = "AmountTextBox";
        public const string WeightTextBox = "WeightTextBox";
        public const string RemarksTextBox = "RemarksTextBox";           
        public const string DescriptionTextBox = "DescriptionTextBox";            
        public const string CreateCommodityWindow = "Create Commodity";
        public const string RejectRatioTextBox = "RejectRatioTextBox";
        public const string ProcessNode = "ProcessNode";
        public const string CommoditiesTabItem = "CommoditiesTabItem";
        public const string CommoditiesDataGrid = "CommoditiesDataGrid";
        public const string ImportCommodity = "ImportCommodity";
    }
}
