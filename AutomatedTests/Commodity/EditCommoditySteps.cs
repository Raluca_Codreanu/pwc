﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.Commodity
{
    [Binding]
    public class EditCommoditySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("EditCommodity")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("EditCommodity")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a process step")]
        public void GivenISelectAProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(CommodityAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();
        }
        
        [Given(@"I select the Commodity tab")]
        public void GivenISelectTheCommodityTab()
        {
            TabPage commodityTab = mainWindow.Get<TabPage>(CommodityAutomationIds.CommoditiesTabItem);
            commodityTab.Click();    
        }
        
        [Given(@"I select a row to press Edit")]
        public void GivenISelectARowToPressEdit()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");

            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I press double click to a row")]
        public void GivenIPressDoubleClickToARow()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");
            row.DoubleClick();
        }
        
        [Given(@"I change a field")]
        public void GivenIChangeAField()
        {
            Window editCommodityWindow = application.Windows.CommodityWindow;

            TextBox nameTextBox = editCommodityWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "3_Updated";

            editCommodityWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I press cancel")]
        public void WhenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I press save btn")]
        public void WhenIPressSaveBtn()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();

            Button saveProcessStepButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveProcessStepButton.Click();  
        }
        
        [Then(@"the edit window is closed")]
        public void ThenTheEditWindowIsClosed()
        {
            Window editCommodityWindow = application.Windows.CommodityWindow;
            Assert.IsNull(editCommodityWindow, "The Edit window is not closed");
        }
        
        [Then(@"value is saved")]
        public void ThenValueIsSaved()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            Assert.IsNotNull(commoditiesDataGrid, "Manage Master Data grid was not found.");
            commoditiesDataGrid.Select("Name", "3_Updated");  
        }
    }
}
