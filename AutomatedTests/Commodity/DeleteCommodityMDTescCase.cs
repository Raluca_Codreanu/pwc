﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.Commodity
{
    [TestClass]
    public class DeleteCommodityMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteCommodityMDTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        [TestMethod]
        [TestCategory("Commodity")]
        public void DeleteCommodityMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode commoditiesNode = app.MainScreen.ProjectsTree.MasterDataCommodities;
                Assert.IsNotNull(commoditiesNode, "The Commodities node was not found.");

                //step 4
                commoditiesNode.Click();
                ListViewControl commoditiesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(commoditiesDataGrid, "Commodities data grid was not found.");

                mainWindow.WaitForAsyncUITasks();
                commoditiesDataGrid.Select("Name", "1commodity");

                //step 6
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 8                
                deleteButton.Click();

                //step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                mainWindow.WaitForAsyncUITasks();

                //step 10      
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "1commodity"), "A commodity from Master Data was deleted permanently and it doesn't appear on the Trash Bin.");
            }
        }

        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            ZPKTool.Data.Commodity commodity = new ZPKTool.Data.Commodity();
            commodity.SetOwner(adminUser);
            commodity.IsMasterData = true;
            commodity.Name = "1commodity";
            commodity.Amount = 100;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            commodity.Manufacturer = manufacturer1;

            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();
        }
        #endregion Helper

        #region Inner classes
        #endregion Inner classes
    }
}
