﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.Commodity
{
    [Binding]
    public class CopyCommoditySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyCommodity")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyCommodity")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Commodity tab from step")]
        public void GivenIOpenTheCommodityTabFromStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(CommodityAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step1");
            step2Node.SelectEx();

            TabPage commodityTab = mainWindow.Get<TabPage>(CommodityAutomationIds.CommoditiesTabItem);
            commodityTab.Click();
        }
        
        [Given(@"I select a Commodity and copy it")]
        public void GivenISelectACommodityAndCopyIt()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");

            Menu copyMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [Given(@"I select a Commodity to Copy to Master Data")]
        public void GivenISelectACommodityToCopyToMasterData()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");
            Menu copyToMDMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMDMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            //mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
        }
        
        [When(@"I select another process step and press Paste")]
        public void WhenISelectAnotherProcessStepAndPressPaste()
        {

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(CommodityAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            Menu pasteMenuItem = step2Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I select the dataGrid, right-click and paste")]
        public void WhenISelectTheDataGridRight_ClickAndPaste()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");

            Menu pasteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I open the Commodity from Master Data")]
        public void WhenIOpenTheCommodityFromMasterData()
        {
            TreeNode commodityNode = application.MainScreen.ProjectsTree.MasterDataCommodities;
            Assert.IsNotNull(commodityNode, "The Dies node was not found.");
            commodityNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the Commodity is copied into this step")]
        public void ThenTheCommodityIsCopiedIntoThisStep()
        {
            ListViewControl commodityNode = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commodityNode.Select("Name", "commodity1");   
        }
        
        [Then(@"a Commodity with the name containing copy of is added")]
        public void ThenACommodityWithTheNameContainingCopyOfIsAdded()
        {
            ListViewControl commodityDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commodityDataGrid.Select("Name", copyTranslation + "commodity1");
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Then(@"a copy of Commodity can be viewed")]
        public void ThenACopyOfCommodityCanBeViewed()
        {
            ListViewControl commodityDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(commodityDataGrid, "Manage Master Data grid was not found.");
            commodityDataGrid.Select("Name", "commodity1"); 
        }
    }
}
