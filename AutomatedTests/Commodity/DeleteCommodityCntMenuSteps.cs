﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.Commodity
{
    [Binding]
    public class DeleteCommodityCntMenuSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteCommodity")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteCommodity")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Commodity tab")]
        public void GivenIOpenTheCommodityTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(CommodityAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();

            TabPage commodityTab = mainWindow.Get<TabPage>(CommodityAutomationIds.CommoditiesTabItem);
            commodityTab.Click();
        }
        
        [Given(@"I select a commodity, press right click to delete it")]
        public void GivenISelectACommodityPressRightClickToDeleteIt()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            commoditiesDataGrid.Select("Name", "commodity1");

            ListViewRow row = commoditiesDataGrid.Row("Name", "commodity1");

            Menu deleteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }
        
        [Given(@"I press No in question message")]
        public void GivenIPressNoInQuestionMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press Yes in question message")]
        public void GivenIPressYesInQuestionMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I open the Trash Bin node")]
        public void GivenIOpenTheTrashBinNode()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            trashBinNode.Click();
        }
        
        [Given(@"I press the Empty Trash Bin")]
        public void GivenIPressTheEmptyTrashBin()
        {
            Button emptyTrashBin = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
            emptyTrashBin.Click();
        }
        
        [When(@"I press yes")]
        public void WhenIPressYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the grid is empty")]
        public void ThenTheGridIsEmpty()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

            Assert.IsNull(trashBinDataGrid.Row("Name", "commodity"), "empty trash");
        }
    }
}
