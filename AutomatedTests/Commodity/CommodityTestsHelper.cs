﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using System.Threading;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.Utils;

namespace ZPKTool.AutomatedTests.Commodity
{
    public static class CommodityTestsHelper
    {
        public static void CreateCommodity(ApplicationEx app)
        {
            Window createCommodityWnd = Wait.For(() => app.Windows.CommodityWindow);

            //step 6
            TextBox nameTextBox = createCommodityWnd.Get<TextBox>(AutomationIds.NameTextBox);
            ValidationHelper.CheckTextFieldValidators(createCommodityWnd, nameTextBox, 100);
            nameTextBox.Text = "commodity1";

            int maxAllowedValueOfDecimalFields = 1000000000;

            TextBox partNumberTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.PartNumberTextBox);
            partNumberTextBox.Text = "10";
            //step 7
            TextBox priceTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.PriceTextBox);
            ValidationHelper.CheckNumericFieldValidators(createCommodityWnd, priceTextBox, maxAllowedValueOfDecimalFields, true);
            priceTextBox.Text = "10";

            TextBox amountTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.AmountTextBox);
            ValidationHelper.CheckNumericFieldValidators(createCommodityWnd, amountTextBox, maxAllowedValueOfDecimalFields, true);
            amountTextBox.Text = "10";

            TextBox rejectRatioTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.RejectRatioTextBox);
            rejectRatioTextBox.Text = "10";

            TextBox weightTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.WeightTextBox);
            ValidationHelper.CheckNumericFieldValidators(createCommodityWnd, weightTextBox, maxAllowedValueOfDecimalFields, true);
            weightTextBox.Text = "10";

            //step 14
            TextBox remarksTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.RemarksTextBox);
            remarksTextBox.Text = "remarks";

            //step 15            
            createCommodityWnd.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);

            //step 24
            TabPage manufacturerTab = createCommodityWnd.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            //step 25
            TextBox manufacturerName = createCommodityWnd.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            //step 26
            TextBox descriptionTextBox = createCommodityWnd.Get<TextBox>(CommodityAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
            descriptionTextBox.Text = "description of manufacturer";

            Button saveButton = createCommodityWnd.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();

            Assert.IsNull(app.Windows.CommodityWindow, "The commodity window did not close after save.");
        }
    }
}
