﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ProjectTests;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.Commodity
{
    [TestClass]
    public class EditCommodity
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditCommodity.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Edit Commodity Test Case (id = 37).
        /// </summary>
        [TestMethod]
        [TestCategory("Commodity")]
        public void EditCommodityTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectEditCommodity");

                project1Node.Click();
                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectEditCommodity");
                project1Node.SelectEx();
                Assert.IsNotNull(project1Node, "projectCommodity node was not found.");

                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                Menu createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();
                CommodityTestsHelper.CreateCommodity(app);

                //step 2
                project1Node = myProjectsNode.GetNode("projectEditCommodity");
                project1Node.Expand();
                Assert.IsNotNull(project1Node, "projectEditCommodity node was not found.");

                partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();

                //step 3
                TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                //step 4
                ListViewControl commoditiesDataGrid = mainWindow.GetListView(AutomationIds.MaterialsDataGrid);
                commoditiesDataGrid.Select("Name", "commodity1");

                //step 5
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");

                editButton.Click();
                //step 7
                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();
                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "commodity_Updated";

                TextBox partNumberTextBox = mainWindow.Get<TextBox>(CommodityAutomationIds.PartNumberTextBox);
                partNumberTextBox.Text = "10";

                TextBox priceTextBox = mainWindow.Get<TextBox>(CommodityAutomationIds.PriceTextBox);
                priceTextBox.Text = "10";

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
        }

        #region Helper
        /// <summary>
        /// Creates the data for this test
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectEditCommodity";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
