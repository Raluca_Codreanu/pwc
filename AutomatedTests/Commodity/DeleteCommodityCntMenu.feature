﻿@DeleteCommodity
Feature: DeleteCommodityCntMenu

Scenario: Delete Commodity unsing context menu option
	Given I open the Commodity tab
	And I select a commodity, press right click to delete it
	And I press No in question message
	And I select a commodity, press right click to delete it
	And I press Yes in question message
	And I open the Trash Bin node
	And I press the Empty Trash Bin
	When I press yes
	Then the grid is empty