﻿@CopyCommodity
Feature: CopyCommodity

Scenario: Copy Commodity to another step
	Given I open the Commodity tab from step
	And I select a Commodity and copy it
	When I select another process step and press Paste
	Then the Commodity is copied into this step

Scenario: Copy Commodity into the same step
	Given I open the Commodity tab from step
	And I select a Commodity and copy it
	When I select the dataGrid, right-click and paste
	Then a Commodity with the name containing copy of is added

Scenario: Copy Commodity to Master Data
	Given I open the Commodity tab from step
	And I select a Commodity to Copy to Master Data 
	When I open the Commodity from Master Data
	Then a copy of Commodity can be viewed
