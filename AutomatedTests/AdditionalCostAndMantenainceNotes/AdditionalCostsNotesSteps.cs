﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using ZPKTool.AutomatedTests.ProcessStepTests;


namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class AdditionalCostsNotesSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("AdditionalCostsNotes")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project3.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("AdditionalCostsNotes")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I click on Result Details node of Part Node")]
        public void GivenIClickOnResultDetailsNodeOfPartNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("part3");
            partNode.ExpandEx();

            TreeNode resultDetailsNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
            resultDetailsNode.SelectEx();

            
        }
        
        [Given(@"I select Suymmary Tab")]
        public void GivenISelectSuymmaryTab()
        {
            TabPage summaryTab = mainWindow.Get<TabPage>(AutomationIds.ResultDetailsSummaryTab);
            summaryTab.Click();
        }
        
        [When(@"I write some information in Additional Costs Notes Text box")]
        public void WhenIWriteSomeInformationInAdditionalCostsNotesTextBox()
        {
            TextBox additionaCostsNotes = mainWindow.Get<TextBox>(AutomationIds.AdditionalCostsNotesTextBox);
            additionaCostsNotes.Text = "Additional Costs Notes part";
        }
        
        [When(@"I click on project node")]
        public void WhenIClickOnProjectNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
        }
        
        [When(@"I click again on result details node to check Additional Costs Notes Text box")]
        public void WhenIClickAgainOnResultDetailsNodeToCheckAdditionalCostsNotesTextBox()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("part3");
            partNode.ExpandEx();

            TreeNode resultDetailsNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
            resultDetailsNode.SelectEx();
            TabPage summaryTab = mainWindow.Get<TabPage>(AutomationIds.ResultDetailsSummaryTab);
            summaryTab.Click();

            TextBox additionaCostsNotes = mainWindow.Get<TextBox>(AutomationIds.AdditionalCostsNotesTextBox);
            Assert.AreEqual(additionaCostsNotes.Text, "Additional Costs Notes part");
        }
        
        [When(@"I expand assembly node")]
        public void WhenIExpandAssemblyNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("assembly3");
            assemblyNode.ExpandEx();
        }
        
        [When(@"I click on Result Details node of Assembly Node")]
        public void WhenIClickOnResultDetailsNodeOfAssemblyNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            TreeNode assemblyNode = projectNode.GetNode("assembly3");
            assemblyNode.ExpandEx();
            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [When(@"I select Suymmary Tab of Assembly Result Details")]
        public void WhenISelectSuymmaryTabOfAssemblyResultDetails()
        {
            TabPage summaryTab = mainWindow.Get<TabPage>(AutomationIds.ResultDetailsSummaryTab);
            summaryTab.Click();
        }
        
        [When(@"I write some information in Additional Costs Notes Text box of Assembly Result Details")]
        public void WhenIWriteSomeInformationInAdditionalCostsNotesTextBoxOfAssemblyResultDetails()
        {
            TextBox additionaCostsNotes = mainWindow.Get<TextBox>(AutomationIds.AdditionalCostsNotesTextBox);
            additionaCostsNotes.Text = "Additional Costs Notes assembly";
        }
        
        [When(@"I click on the project node")]
        public void WhenIClickOnTheProjectNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
        }
        
        [Then(@"I click again on Result Details node of Assembly Result Details to check Additional Costs Notes Text box")]
        public void ThenIClickAgainOnResultDetailsNodeOfAssemblyResultDetailsToCheckAdditionalCostsNotesTextBox()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            TreeNode assemblyNode = projectNode.GetNode("assembly3");
            assemblyNode.ExpandEx();
            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
            resultDetailsNode.SelectEx();
            TabPage summaryTab = mainWindow.Get<TabPage>(AutomationIds.ResultDetailsSummaryTab);
            summaryTab.Click();

            TextBox additionaCostsNotes = mainWindow.Get<TextBox>(AutomationIds.AdditionalCostsNotesTextBox);
            Assert.AreEqual(additionaCostsNotes.Text, "Additional Costs Notes assembly");
        }
    }
}
