﻿@AdditionalCostsNotes
Feature: AdditionalCostsNotes

Scenario: Additional Costs Notes
	Given I click on Result Details node of Part Node
	And I select Suymmary Tab
	When I write some information in Additional Costs Notes Text box
	When I click on project node 
	When I click again on result details node to check Additional Costs Notes Text box
	When I expand assembly node
	When I click on Result Details node of Assembly Node
	When I select Suymmary Tab of Assembly Result Details
	When I write some information in Additional Costs Notes Text box of Assembly Result Details
	When I click on the project node 
	Then I click again on Result Details node of Assembly Result Details to check Additional Costs Notes Text box

