﻿@MaintenanceNotes
Feature: MaintenanceNotes

Scenario: Maintenance Notes
	Given I click on Process Node from Part Node
	And I create a new Step
	When I select Machines Tab
	When I add a new machine
	When I select Maintenance tab
	When I write some informations in Notes TextBox
	When I click on save Button
	When I click on Edit Machine Button
	When I go to Maintenance Tab to check the note
	When I click on Process Node from Assembly
	When I create a new Step for Assmbly Process
	When I select Machines Tab of the Assembly Process Step
	When I add a new machine in Assembly Process Step
	When I select Maintenance tab of Assembly Process Step Machine
	When I write some informations in Mainenance Notes TextBox
	When I click on Save Button
	When I click on Edit Assembly Process Step Machine Button
	Then I go to Maintenance Tab to check if the note is saved
