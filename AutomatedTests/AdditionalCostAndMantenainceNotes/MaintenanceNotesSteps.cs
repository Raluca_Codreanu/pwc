﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using ZPKTool.AutomatedTests.ProcessStepTests;


namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class MaintenanceNotesSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("MaintenanceNotes")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project3.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("MaintenanceNotes")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I click on Process Node from Part Node")]
        public void GivenIClickOnProcessNodeFromPartNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project3");
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("part3");
            partNode.ExpandEx();

            TreeNode processNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            processNode.SelectEx();

            TabPage processView = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessViewTab);
            processView.Click();
        }
        
        [Given(@"I create a new Step")]
        public void GivenICreateANewStep()
        {
            Button addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
            addStep.Click();

            TabPage informationTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessStepInfoTab);
            informationTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "step1";
        }
        
        [When(@"I select Machines Tab")]
        public void WhenISelectMachinesTab()
        {
            TabPage machinesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.MachinesTabList);
            machinesTab.Click();
        }
        
        [When(@"I add a new machine")]
        public void WhenIAddANewMachine()
        {
            Button addMachine = mainWindow.Get<Button>(ProcessStepAutomationIds.AddMachineButton);
            addMachine.Click();
            TextBox machineNameTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MachineNameTextBox);
            machineNameTextBox.Text = "machine3";
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.MachineManufacturerTab);
            manufacturerTab.Click();
            TextBox machineManufacturerNameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            machineManufacturerNameTextBox.Text = "manufacturer3";
        }
        
        [When(@"I select Maintenance tab")]
        public void WhenISelectMaintenanceTab()
        {
            TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MachineMaintenanceTab);
            maintenanceTab.Click();
        }
        
        [When(@"I write some informations in Notes TextBox")]
        public void WhenIWriteSomeInformationsInNotesTextBox()
        {
            TextBox maintenanceNotesTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceNotesTextBox);
            maintenanceNotesTextBox.Text = "maintenance note";
        }
        
        [When(@"I click on save Button")]
        public void WhenIClickOnSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I click on Edit Machine Button")]
        public void WhenIClickOnEditMachineButton()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [When(@"I go to Maintenance Tab to check the note")]
        public void WhenIGoToMaintenanceTabToCheckTheNote()
        {
            TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MachineMaintenanceTab);
            maintenanceTab.Click();

            TextBox maintenanceNotesTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceNotesTextBox);
            Assert.AreEqual(maintenanceNotesTextBox.Text, "maintenance note");

            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
            mainWindow.WaitForAsyncUITasks();

        }
        
        [When(@"I click on Process Node from Assembly")]
        public void WhenIClickOnProcessNodeFromAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickYes();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("project3");
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("assembly3");
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            processNode.SelectEx();

            TabPage processView = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessViewTab);
            processView.Click();
        }
        
        [When(@"I create a new Step for Assmbly Process")]
        public void WhenICreateANewStepForAssmblyProcess()
        {
            Button addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
            addStep.Click();

            TabPage informationTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessStepInfoTab);
            informationTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "step1";
        }
        
        [When(@"I select Machines Tab of the Assembly Process Step")]
        public void WhenISelectMachinesTabOfTheAssemblyProcessStep()
        {
            TabPage machinesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.MachinesTabList);
            machinesTab.Click();
        }
        
        [When(@"I add a new machine in Assembly Process Step")]
        public void WhenIAddANewMachineInAssemblyProcessStep()
        {
            Button addMachine = mainWindow.Get<Button>(ProcessStepAutomationIds.AddMachineButton);
            addMachine.Click();
            TextBox machineNameTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MachineNameTextBox);
            machineNameTextBox.Text = "machine3 assembly";
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.MachineManufacturerTab);
            manufacturerTab.Click();
            TextBox machineManufacturerNameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            machineManufacturerNameTextBox.Text = "manufacturer3 assembly";
        }
        
        [When(@"I select Maintenance tab of Assembly Process Step Machine")]
        public void WhenISelectMaintenanceTabOfAssemblyProcessStepMachine()
        {
            TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MachineMaintenanceTab);
            maintenanceTab.Click();
        }
        
        [When(@"I write some informations in Mainenance Notes TextBox")]
        public void WhenIWriteSomeInformationsInMainenanceNotesTextBox()
        {
            TextBox maintenanceNotesTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceNotesTextBox);
            maintenanceNotesTextBox.Text = "maintenance note assembly";
        }
        
        [When(@"I click on Save Button")]
        public void WhenIClickOnTheSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I click on Edit Assembly Process Step Machine Button")]
        public void WhenIClickOnEditAssemblyProcessStepMachineButton()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Then(@"I go to Maintenance Tab to check if the note is saved")]
        public void ThenIGoToMaintenanceTabToCheckIfTheNoteIsSaved()
        {
            TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MachineMaintenanceTab);
            maintenanceTab.Click();

            TextBox maintenanceNotesTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceNotesTextBox);
            Assert.AreEqual(maintenanceNotesTextBox.Text, "maintenance note assembly");
        }
    }
}
