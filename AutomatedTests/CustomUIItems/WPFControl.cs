﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using White.Core.UIItems.Actions;
using White.Core.UIItems.Custom;

namespace ZPKTool.AutomatedTests.CustomUIItems
{
    /// <summary>
    /// A UI item that can be used to get any WPF control that does not have a dedicated UIItem implementation (like user controls, grids, borders, etc).
    /// </summary>
    [ControlTypeMapping(CustomUIItemType.Custom)]
    public class WPFControl : CustomUIItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WPFControl" /> class.
        /// </summary>
        protected WPFControl()
        {
            // Empty constructor is mandatory with protected or public access modifier.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WPFControl" /> class.
        /// </summary>
        /// <param name="automationElement">The automation element.</param>
        /// <param name="actionListener">The action listener.</param>
        public WPFControl(AutomationElement automationElement, ActionListener actionListener)
            : base(automationElement, actionListener)
        {
        }
    }
}
