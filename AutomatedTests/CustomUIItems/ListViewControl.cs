﻿namespace ZPKTool.AutomatedTests.CustomUIItems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Automation;
    using White.Core.UIItems;
    using White.Core.UIItems.Actions;
    using White.Core.UIItems.Finders;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ListViewControl : ListView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewControl"/> class.
        /// </summary>
        public ListViewControl()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewControl" /> class.
        /// </summary>
        /// <param name="automationElement">The automation element.</param>
        /// <param name="actionListener">The action listener.</param>
        public ListViewControl(AutomationElement automationElement, ActionListener actionListener)
            : base(automationElement, actionListener)
        {
        }

        /// <summary>
        /// Gets a row that contains a specified value on the specified column.
        /// </summary>
        /// <param name="column">The column where to search the specified value.</param>
        /// <param name="value">The searched value.</param>
        /// <returns>The row that contains a specified value on the specified column.</returns>
        public override ListViewRow Row(string column, string value)
        {
            var verticalScrollBar = this.ScrollBars.Vertical;
            if (verticalScrollBar.IsScrollable)
            {
                verticalScrollBar.SetToMinimum();
            }

            // this flag forces the loop to run once more after the scrolling reaches the maximum.
            bool runOnceMore = false;

            do
            {
                foreach (ListViewRow row in this.Rows)
                {
                    var col = row.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ClassNameProperty, "DataGridCell"))
                        .Cast<AutomationElement>()
                        .ToList();
                    ListViewCells cells = new ListViewCells(col, this.ActionListener, this.Header);

                    var cellsTextElements = cells.Select(c => c.GetElement(SearchCriteria.ByControlType(ControlType.Text)));
                    var searchedValueElem = cellsTextElements.FirstOrDefault(elem => elem != null && elem.Current.Name == value);
                    if (searchedValueElem != null)
                    {
                        return row;
                    }
                }

                var valueBeforScroll = verticalScrollBar.Value;
                verticalScrollBar.ScrollDownLarge();

                if (!runOnceMore)
                {
                    // If we have scrolled to the end run the do loop once more to search in the current rows, which were updated after this scrolling.
                    if (valueBeforScroll < 100 && verticalScrollBar.Value == 100)
                    {
                        runOnceMore = true;
                    }
                }
                else
                {
                    runOnceMore = false;
                }
            }
            while ((verticalScrollBar.IsScrollable && verticalScrollBar.Value < verticalScrollBar.MaximumValue) || runOnceMore);

            return null;
        }

        /// <summary>
        /// Selects a specified row.
        /// </summary>
        /// <param name="column">The column on which the search value is found.</param>
        /// <param name="value">The searched value.</param>
        public override void Select(string column, string value)
        {
            ListViewRow row = this.Row(column, value);
            ListViewCell cell = this.Cell(column, row);
            cell.Click();
        }

        /// <summary>
        /// Gets the specified cell.
        /// </summary>
        /// <param name="column">The column on which the cell is found.</param>
        /// <param name="row">The row in which to search for the cell.</param>
        /// <returns>
        /// A ListViewCell.
        /// </returns>
        public ListViewCell Cell(string column, ListViewRow row)
        {
            this.ScrollToElement(row.AutomationElement);
            var coll = row.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ClassNameProperty, "DataGridCell"))
                .Cast<AutomationElement>()
                .ToList();
            ListViewCells cells = new ListViewCells(coll, this.ActionListener, this.Header);

            return cells[column];
        }

        public ListViewCell CellByIndex(int index, ListViewRow row)
        {
            this.ScrollToElement(row.AutomationElement);
            var coll = row.AutomationElement.FindAll(TreeScope.Descendants, new PropertyCondition(AutomationElement.ClassNameProperty, "DataGridCell"))
                .Cast<AutomationElement>()
                .ToList();
            ListViewCells cells = new ListViewCells(coll, this.ActionListener, this.Header);

            return cells[index];
        }


        /// <summary>
        /// Scroll to the specified automation element .
        /// </summary>
        /// <param name="element">The element.</param>
        private void ScrollToElement(AutomationElement element)
        {
            AutomationPattern scrollItemPattern =
                element.GetSupportedPatterns().FirstOrDefault(pattern => pattern.ProgrammaticName == "ScrollItemPatternIdentifiers.Pattern");
            if (scrollItemPattern != null)
            {
                ScrollItemPattern scrollPattern = (ScrollItemPattern)element.GetCurrentPattern(scrollItemPattern);
                if (scrollPattern != null)
                {
                    scrollPattern.ScrollIntoView();
                }
            }
        }
    }
}
