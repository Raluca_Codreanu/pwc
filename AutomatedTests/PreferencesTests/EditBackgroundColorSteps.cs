﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;

namespace ZPKTool.AutomatedTests.PreferencesTests
{
    [Binding]
    public class EditBackgroundColorSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [Given(@"I have opened the Preferences - UI Settings screen")]
        public void GivenIHaveOpenedThePreferences_UISettingsScreen()
        {
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = Wait.For(() => application.Windows.Preferences);
        }

        [Given(@"I have changed the Background color to another Color")]
        public void GivenIHaveChangedTheBackgroundColorToAnother()
        {
            Window preferencesWindow = application.Windows.Preferences;
            ComboBox backgroundColorComboBox = preferencesWindow.Get<ComboBox>("backgroundColorComboBox");
            Assert.IsNotNull(backgroundColorComboBox, "The backgroundColorComboBox was not found.");
            backgroundColorComboBox.SelectItem(2);
        }

        [When(@"I press Save button")]
        public void WhenIPressSaveButton()
        {
            Window preferencesWindow = application.Windows.Preferences;
            Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
            //Wait.For(() => application.Windows.Preferences == null);
            //Assert.IsNull(preferencesWindow, "Preferences window is not closed.");
        }

        [Then(@"the result should be to set the Color backgound and the Preferences window is closed")]
        public void ThenTheResultShouldBeToSetTheBackgoundAndThePreferencesWindowIsClosed()
        {
            // check result
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;
            ComboBox backgroundColorComboBox = preferencesWindow.Get<ComboBox>("backgroundColorComboBox");
            Assert.IsNotNull(backgroundColorComboBox, "The backgroundColorComboBox was not found.");
            var a = backgroundColorComboBox.SelectedItemText;
            Assert.AreEqual(backgroundColorComboBox.SelectedItemText, "Silver");

        }

        [BeforeScenario("BackgroundColor")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterScenario("BackgroundColor")]
        private static void KillApplication()
        {
            application.Kill();
        }
    }
}
