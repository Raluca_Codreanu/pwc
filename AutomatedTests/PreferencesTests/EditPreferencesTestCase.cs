﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.ListBoxItems;
using System.Threading;
using ZPKTool.Data;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.ProjectTests;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.ProcessStepTests;
using White.Core.UIItems.ListViewItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.SearchTool;
using White.Core.InputDevices;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.SettingsTests;
using System.IO;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.PreferencesTests
{
    /// <summary>
    /// Summary description for EditPreferencesTestCase
    /// </summary>
    [TestClass]
    public class EditPreferencesTestCase
    {
        /// <summary>
        /// The project used in the Edit Preferences tests.
        /// </summary>
        private static Project project;

        private static string configFilePath;
        private static string tempConfigFilePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditPreferencesTestCase"/> class.
        /// </summary>
        public EditPreferencesTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            EditPreferencesTestCase.CreateTestData();

            // Save the preferences file so it can be restored after the tests
            configFilePath = UserSettingsManager.Instance.SettingsFilePath;
            tempConfigFilePath = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());

            File.Copy(configFilePath, tempConfigFilePath, true);
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            //Helper.DeleteAppSettingsFile();

            // Restore the preferences save at initialize
            File.Copy(tempConfigFilePath, configFilePath, true);
        }

        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Helper.DeleteAppSettingsFile();
        }

        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Edit Preferences test case -> Edit UI Language setting.
        /// </summary>
        [TestMethod]
        public void EditUILanguageTest()
        {
            ApplicationEx app = null;
            try
            {
                // step 1
                app = ApplicationEx.LaunchAndGoToMainScreen();
                Window mainWindow = app.Windows.MainWindow;

                // step 3                      
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = app.Windows.Preferences;

                // step 4
                ComboBox uiLanguageComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UILanguageComboBox);
                Assert.IsNotNull(uiLanguageComboBox, "The UI Language combo box was not found.");

                Assert.AreEqual(2, uiLanguageComboBox.Items.Count, "The number of UI Languages displayed into the comboBox is not the expected one.");

                ListItem englishItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "English");
                Assert.IsNotNull(englishItem, "The 'English' item was not found.");
                Assert.IsTrue(englishItem.IsSelected, "The default UI Language should be English");

                ListItem germanItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "German");
                uiLanguageComboBox.Click();
                germanItem.Click();
                Assert.IsNotNull(germanItem, "The 'German' item was not found.");

                // step 5
                
                Button restartButton = mainWindow.Get<Button>(PreferencesAutomationIds.RestartButton);
                Assert.IsNotNull(restartButton, "The Restart button was not found.");
                Assert.IsTrue(restartButton.Visible, "The restart button is hidden although the selected UI language is different than the current UI language.");

                // step 6
                uiLanguageComboBox.Click();
                Thread.Sleep(300);

                englishItem.Click();
                Assert.IsFalse(restartButton.Visible, "The restart button is visible although the selected UI language is the same as the current UI language.");

                // step 7
                uiLanguageComboBox.Click();
                Thread.Sleep(300);

                germanItem.Click();

                restartButton.Click();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the application.");

                app = ApplicationEx.Attach();
                app.Windows.WaitForMainWindowToLoad();
                app.LoginAndGoToMainScreen();
                mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                uiLanguageComboBox = mainWindow.Get<ComboBox>(PreferencesAutomationIds.UILanguageComboBox);
                uiLanguageComboBox.Click();
                Thread.Sleep(300);

                englishItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "Englisch");
                Assert.IsNotNull(englishItem, "The Englisch item was not found into the UI Language comboBox.");

                germanItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "Deutsch");
                Assert.IsNotNull(germanItem, "The Deutsch item was not found into the UI Language comboBox.");
                Assert.IsTrue(germanItem.IsSelected, "The Deutsch item should be selected.");

                // step 8
                uiLanguageComboBox.Click();
                Thread.Sleep(300);

                englishItem.Click();
                restartButton = mainWindow.Get<Button>(PreferencesAutomationIds.RestartButton);
                Assert.IsNotNull(restartButton, "The Restart button was not found.");
                Assert.IsTrue(restartButton.Visible, "The Restart button is not visible although the selected language is different than the current language.");

                restartButton.Click();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the application.");

                app = ApplicationEx.Attach();
                app.LoginAndGoToMainScreen();
                mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                uiLanguageComboBox = mainWindow.Get<ComboBox>(PreferencesAutomationIds.UILanguageComboBox);
                uiLanguageComboBox.Click();
                Thread.Sleep(300);

                englishItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "English");
                Assert.IsNotNull(englishItem, "The English item was not found into the UI Language comboBox.");
                Assert.IsTrue(englishItem.IsSelected, "The English item is not selected.");
            }

            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> Edit Raw Material localized name setting.
        /// </summary>
        [TestMethod]
        public void EditRawMaterialLocalizedNameTest()
        {
            SearchToolTestCase.SetIsAutoHiddenProperty(false);
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                ComboBox uiLanguageComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UILanguageComboBox);
                ListItem germanItem = uiLanguageComboBox.Items.FirstOrDefault(i => i.Text == "German");
                uiLanguageComboBox.Click();
                germanItem.Click();
                ComboBox rawMaterialsComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UIRawMaterialsNameCombo);
                Assert.IsNotNull(rawMaterialsComboBox, "Raw Materials combo box was not found.");

                rawMaterialsComboBox.Click();
                Thread.Sleep(100);
                rawMaterialsComboBox.Click();
                Assert.AreEqual("German", rawMaterialsComboBox.SelectedItemText.Trim(), "The default Raw Material localized name should be the German Name.");
                preferencesWindow.Close();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                
                app.MainMenu.SearchViewMenuItem.Click();
                SearchToolTestCase.SearchForAnyEnityIntoAllLocations(mainWindow);
                Thread.Sleep(1000);
                VerifyRawMaterialLocalizedName(app, RawMaterialLocalisedName.GermanName);

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);

                rawMaterialsComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UIRawMaterialsNameCombo);
                rawMaterialsComboBox.Click();
                Thread.Sleep(300);

                rawMaterialsComboBox.Item("UK").Click();
                Thread.Sleep(300);
                rawMaterialsComboBox.Click();

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");

                saveButton.Click();

                SearchToolTestCase.SearchForAnyEnityIntoAllLocations(mainWindow);
                VerifyRawMaterialLocalizedName(app, RawMaterialLocalisedName.UKName);

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);

                rawMaterialsComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UIRawMaterialsNameCombo);
                rawMaterialsComboBox.Click();
                Thread.Sleep(300);

                rawMaterialsComboBox.Item("US").Click();
                Thread.Sleep(300);
                rawMaterialsComboBox.Click();

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                SearchToolTestCase.SearchForAnyEnityIntoAllLocations(mainWindow);
                VerifyRawMaterialLocalizedName(app, RawMaterialLocalisedName.USName);

                //set initial value (German) for raw material localized name 
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);

                rawMaterialsComboBox = preferencesWindow.Get<ComboBox>(PreferencesAutomationIds.UIRawMaterialsNameCombo);
                rawMaterialsComboBox.Click();
                Thread.Sleep(300);

                rawMaterialsComboBox.Item("German").Click();
                Thread.Sleep(300);
                rawMaterialsComboBox.Click();

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                SearchToolTestCase.SearchForAnyEnityIntoAllLocations(mainWindow);
                VerifyRawMaterialLocalizedName(app, RawMaterialLocalisedName.GermanName);
            }

            SearchToolTestCase.SetIsAutoHiddenProperty(true);
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Display Version and Timestamp" setting.
        /// </summary>
        [TestMethod]
        public void EditDisplayVersionAndTimestampTest()
        {
            ApplicationEx app = null;
            try
            {
                // step 1
                app = ApplicationEx.LaunchAndGoToMainScreen();
                Window mainWindow = app.Windows.MainWindow;

                // step 14
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);

                CheckBox displayVersionAndTimestampCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayVersionAndTimestampCheckBox);
                Assert.IsNotNull(displayVersionAndTimestampCheckBox, "The Display Version and Timestamp checkBox was not found.");
                Assert.IsTrue(displayVersionAndTimestampCheckBox.Checked, "Display Version and Timestamp should be checked by default.");

                preferencesWindow.Close();

                // step 15
                VerifyDisplayVersionAndTimestamp(app, true);

                // step 16
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                displayVersionAndTimestampCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayVersionAndTimestampCheckBox);
                displayVersionAndTimestampCheckBox.Checked = false;

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                // step 17
                VerifyDisplayVersionAndTimestamp(app, false);
            }
            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Display Capacity Utilization" setting.
        /// </summary>
        [TestMethod]
        public void EditDisplayCapacityUtilizationTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                // step 1                
                Window mainWindow = app.Windows.MainWindow;

                // step 2                
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                CheckBox displayCapacityUtilizationCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayCapacityUtilizationCheckBox);
                Assert.IsNotNull(displayCapacityUtilizationCheckBox, "Display Capacity Utilization check box was not found.");
                Assert.IsTrue(displayCapacityUtilizationCheckBox.Checked, "Display Capacity Utilization checkbox should be checked by default.");

                preferencesWindow.Close();
                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");

                EditPreferencesTestCase.VerifyDisplayCapacityUtilization(app, true);

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                displayCapacityUtilizationCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayCapacityUtilizationCheckBox);
                displayCapacityUtilizationCheckBox.Checked = false;

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                EditPreferencesTestCase.VerifyDisplayCapacityUtilization(app, false);
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Show Start Screen" setting.
        /// </summary>
        [TestMethod]
        public void EditShowStartScreenTest()
        {
            ApplicationEx app = null;
            try
            {
                app = ApplicationEx.LaunchAndGoToMainScreen();
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                CheckBox showStartScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowStartScreenCheckBox);
                Assert.IsNotNull(showStartScreenCheckBox, "Show Start Screen checkBox was not found.");
                showStartScreenCheckBox.Checked = true;
                Button savePrefsButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePrefsButton.Click();

                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");

                Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
                Assert.IsNotNull(homeButton, "The Home button was not found.");
                Assert.IsTrue(homeButton.Enabled, "The Home button is disabled.");

                homeButton.Click();
                var welcomeScreen = Wait.For(() => app.WelcomeScreen);
                Button skipButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
                skipButton.Click();

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                showStartScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowStartScreenCheckBox);
                showStartScreenCheckBox.Checked = false;
                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                app.MainMenu.System_Logout.Click();
                app.Login();

                welcomeScreen = Wait.For(() => app.WelcomeScreen, 500);
                Assert.IsNull(welcomeScreen, "The welcome screen was displayed even though the 'Show Start Screen' setting was false.");

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                showStartScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowStartScreenCheckBox);
                showStartScreenCheckBox.Checked = true;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();
            }

            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Number of Digits to Display" setting.
        /// </summary>
        [TestMethod]
        public void EditNumberOfDigitsToDisplayTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                ComboBox noOfDigitsToDisplayComboBox = preferencesWindow.Get<ComboBox>("noOfDigitsToDisplayComboBox");
                Assert.IsNotNull(noOfDigitsToDisplayComboBox, "noOfDigitsToDisplayComboBox was not found.");
                noOfDigitsToDisplayComboBox.SelectItem(1);
                Thread.Sleep(600);

                preferencesWindow.Close();
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Display Calculation Result" setting.
        /// </summary>
        [TestMethod]
        public void EditDisplayCalculationResultTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();

                Window preferencesWindow = app.Windows.Preferences;

                //Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                //TabPage headerTab = preferencesWindow.Get<TabPage>(PreferencesAutomationIds.HeaderTab);

                //headerTab.Click();
                CheckBox displayCalculationResultCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayCalculationResultCheckBox);
                Assert.IsNotNull(displayCalculationResultCheckBox, "The Display Calculation Result checkbox was not found.");
                Assert.IsTrue(displayCalculationResultCheckBox.Checked, "Display Calculation Result checkbox should be checked by default.");

                preferencesWindow.Close();
                VerifyDisplayCalculationResult(app, true);

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = app.Windows.Preferences;
                //headerTab = preferencesWindow.Get<TabPage>(PreferencesAutomationIds.HeaderTab);

                //headerTab.Click();
                displayCalculationResultCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayCalculationResultCheckBox);
                displayCalculationResultCheckBox.Checked = false;

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                Assert.IsTrue(Wait.For(() => preferencesWindow.IsClosed), "Failed to close the Preferences window.");
                VerifyDisplayCalculationResult(app, false);
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Display Weight" setting.
        /// </summary>
        [TestMethod]
        public void EditDisplayWeightTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = app.Windows.Preferences;

                CheckBox displayWeightCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayWeightCheckBox);
                Assert.IsNotNull(displayWeightCheckBox, "The Display Weight check box was not found.");
                Assert.IsTrue(displayWeightCheckBox.Checked, "Display Weight check box should be checked by default.");

                preferencesWindow.Close();
                Assert.IsTrue(Wait.For(() => preferencesWindow.IsClosed), "Failed to close the Preferences window.");

                EditPreferencesTestCase.VerifyDisplayWeight(app, true);
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit "Display Investment" setting.
        /// </summary>
        [TestMethod]
        public void EditDisplayInvestmentTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                //TabPage headerTab = preferencesWindow.Get<TabPage>(PreferencesAutomationIds.HeaderTab);
                //Assert.IsNotNull(headerTab, "The Header tab was not found.");

                //headerTab.Click();
                CheckBox investmentCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayInvestmentCheckBox);
                Assert.IsNotNull(investmentCheckBox, "The Investment checkbox was not found.");
                Assert.IsTrue(investmentCheckBox.Checked, "The Investment checkbox should be checked by default.");

                preferencesWindow.Close();
                EditPreferencesTestCase.VerifyDisplayInvestment(app, true);

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                //headerTab = preferencesWindow.Get<TabPage>(PreferencesAutomationIds.HeaderTab);
                //headerTab.Click();

                investmentCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.DisplayInvestmentCheckBox);
                investmentCheckBox.Checked = true;

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");

                saveButton.Click();
                EditPreferencesTestCase.VerifyDisplayInvestment(app, false);
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit Local db settings.
        /// </summary>
        [TestMethod]
        public void EditLocalcDbSettingsTest()
        {
            ApplicationEx app = null;
            try
            {
                // step 1
                app = ApplicationEx.LaunchAndGoToMainScreen();

                // step 31
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                // step 32
                databaseTab.Click();
                TextBox serverNameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbServerNameTextBox);
                Assert.IsNotNull(serverNameTextBox, "The Server Name text box was not found.");
                serverNameTextBox.Text = Helper.LocalDbDataSource;

                CheckBox useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForLocalDbCheckBox);
                Assert.IsNotNull(useWindowAuthCheckBox, "Use Windows Authentication text box was not found.");

                useWindowAuthCheckBox.Checked = false;

                TextBox usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox);
                Assert.IsNotNull(usernameTextBox, "The Username text box was not found.");
                usernameTextBox.Text = Helper.LocalDbUserId;

                TextBox passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox);
                Assert.IsNotNull(passwordBox, "The Password box  was not found.");
                passwordBox.Text = "mockPass";

                Assert.IsNotNull(useWindowAuthCheckBox, "Use Windows Authentication text box was not found.");

                useWindowAuthCheckBox.Checked = true;
                Assert.IsFalse(preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox).Enabled, "Username field is enabled after the Use Window Authentication has been checked.");
                Assert.IsFalse(preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox).Enabled, "Password field is enabled after the Use Window Authentication has been checked.");

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                // step 33
                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROimpens application.");
                app = ApplicationEx.LaunchAndGoToMainScreen();

                // step 34
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForLocalDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox);
                usernameTextBox.Text = "mockUsername";

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox);
                passwordBox.Text = Helper.LocalDbPassword;

                useWindowAuthCheckBox.Checked = true;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                // step 35
                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                // step 36
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForLocalDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox);
                usernameTextBox.Text = Helper.LocalDbUserId;

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox);
                passwordBox.Text = "mockPass";

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROimpens application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                Assert.IsTrue(app.Windows.MainWindow.IsMessageDialogDisplayed(MessageDialogType.Error), "Failed to display an error message at login although the password of the local db is wrong and the SQL Authentication is used.");
                app.Windows.MainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 37
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForLocalDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox);
                usernameTextBox.Text = "mockUsername";

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox);
                passwordBox.Text = Helper.LocalDbPassword;

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                app.Windows.MainWindow.Close(); ;
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROImpens application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                Assert.IsTrue(app.Windows.MainWindow.IsMessageDialogDisplayed(MessageDialogType.Error), "Failed to display an error message at login although the Username of the local db is wrong and the SQL Authentication is used.");
                app.Windows.MainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // added initial values in order to be able to run tests that follow this
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForLocalDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbUsernameTextBox);
                usernameTextBox.Text = Helper.LocalDbUserId;

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.LocalDbPasswordBox);
                passwordBox.Text = Helper.LocalDbPassword;

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        /// <summary>
        /// An automated test for Edit Preferences test case -> edit Central db settings.
        /// </summary>
        [TestMethod]
        public void EditCentralDbSettingsTest()
        {
            ApplicationEx app = null;
            try
            {
                // step 1
                app = ApplicationEx.LaunchAndGoToMainScreen();

                // step 38
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = app.Windows.Preferences;
                TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();

                CheckBox useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForCentralDbCheckBox);
                Assert.IsNotNull(useWindowAuthCheckBox, "Use Windows Authentication text box was not found.");
                useWindowAuthCheckBox.Checked = false;

                TextBox serverNameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbServerNameTextBox);
                Assert.IsNotNull(serverNameTextBox, "The Server Name text box was not found.");
                serverNameTextBox.Text = Helper.CentralDbDataSource;

                TextBox usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbUsernameTextBox);
                Assert.IsNotNull(usernameTextBox, "The Username text box was not found.");
                usernameTextBox.Text = Helper.CentralDbUserId;

                TextBox passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbPasswordTextBox);
                Assert.IsNotNull(passwordBox, "The Password box  was not found.");
                passwordBox.Text = "mockPass";

                Assert.IsNotNull(useWindowAuthCheckBox, "Use Windows Authentication text box was not found.");

                useWindowAuthCheckBox.Checked = true;
                Assert.IsFalse(preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbUsernameTextBox).Enabled, "Username field is enabled after the Use Window Authentication has been checked.");
                Assert.IsFalse(preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbPasswordTextBox).Enabled, "Password field is enabled after the Use Window Authentication has been checked.");

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "The Save button was not found.");

                saveButton.Click();

                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROimpens application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                Label onlineStatusLabel = app.Windows.MainWindow.Get<Label>(PreferencesAutomationIds.OnlineStatusLabel);
                Assert.IsNotNull(onlineStatusLabel, "The Online Status header was not found.");
                Assert.AreEqual("Online", onlineStatusLabel.Text, "Wrong Status displayed.");

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForCentralDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbUsernameTextBox);
                usernameTextBox.Text = "mockUsername";

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbPasswordTextBox);
                passwordBox.Text = Helper.LocalDbPassword;

                useWindowAuthCheckBox.Checked = true;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                onlineStatusLabel = app.Windows.MainWindow.Get<Label>(PreferencesAutomationIds.OnlineStatusLabel);
                Assert.AreEqual("Online", onlineStatusLabel.Text, "Wrong Status displayed.");

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(PreferencesAutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForCentralDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbUsernameTextBox);
                usernameTextBox.Text = Helper.LocalDbUserId;

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbPasswordTextBox);
                passwordBox.Text = "mockPass";

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROimpens application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                onlineStatusLabel = app.Windows.MainWindow.Get<Label>(PreferencesAutomationIds.OnlineStatusLabel);
                Assert.AreEqual("Offline", onlineStatusLabel.Text, "Wrong Status displayed.");

                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);

                databaseTab.Click();
                useWindowAuthCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.UseWindowsAuthForCentralDbCheckBox);
                useWindowAuthCheckBox.Checked = false;

                usernameTextBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbUsernameTextBox);
                usernameTextBox.Text = Helper.CentralDbUserId;

                passwordBox = preferencesWindow.Get<TextBox>(PreferencesAutomationIds.CentralDbPasswordTextBox);
                passwordBox.Text = Helper.LocalDbPassword;

                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the PROImpens application.");

                app = ApplicationEx.LaunchAndGoToMainScreen();

                onlineStatusLabel = app.Windows.MainWindow.Get<Label>(PreferencesAutomationIds.OnlineStatusLabel);
                Assert.AreEqual("Online", onlineStatusLabel.Text, "Wrong Status displayed.");
            }
            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        #region Helper

        /// <summary>
        /// Verifies if the versions and timestamps of the test project and its children (parts and assemblies) are displayed.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="displayVersionAndTimestamp">True if the version and the timestamp should be displayed, false otherwise.</param>
        private static void VerifyDisplayVersionAndTimestamp(ApplicationEx app, bool displayVersionAndTimestamp)
        {
            Window mainWindow = app.Windows.MainWindow;
            TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            Assert.IsNotNull(myProjectsNode, "My Projects node was not found.");
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode(EditPreferencesTestCase.project.Name);
            Assert.IsNotNull(projectNode, "The project's node was not found.");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            Label lastModifiedLabel = Wait.For(() => mainWindow.Get<Label>(PreferencesAutomationIds.LastModifiedLabel));
            Assert.IsNotNull(lastModifiedLabel, "The Last Modified label was not found.");
            Assert.AreEqual(displayVersionAndTimestamp, lastModifiedLabel.Visible, "The Last Modified label's visibility is not the expected one.");

            Label versionLabel = null;
            foreach (Assembly assembly in EditPreferencesTestCase.project.Assemblies)
            {
                TreeNode assemblyNode = projectNode.GetNode(assembly.Name);
                Assert.IsNotNull(assemblyNode, "The assembly node was not found.");
                assemblyNode.SelectEx();

                lastModifiedLabel = mainWindow.Get<Label>(AssemblyAutomationIds.LastModifiedLabel);
                Assert.AreEqual(displayVersionAndTimestamp, lastModifiedLabel.Visible, "The Last Modified label's visibility is not the expected one.");

                assemblyNode.ExpandEx();
                TreeNode resultDetailsNode = assemblyNode.GetNode(AutomationIds.ResultDetailsNode);
                Assert.IsNotNull(resultDetailsNode, "The Result Details node was not found.");

                resultDetailsNode.Click();

                // Workaround(if this label is not visible the "Visible" property returns true) -> if the Bounding Rectangle is empty => the label is collapsed otherwise is visible
                lastModifiedLabel = Wait.For(() => mainWindow.Get<Label>(PreferencesAutomationIds.ResultDetailsViewLastModifiedDateLabel));
                Assert.IsNotNull(lastModifiedLabel, "The Last Modified label was not found.");
                bool isVisible = (System.Windows.Rect)lastModifiedLabel.AutomationElement.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty) != System.Windows.Rect.Empty ? true : false;
                Assert.AreEqual(displayVersionAndTimestamp, isVisible, "The Last Modified label's visibility is not the expected one.");

                versionLabel = mainWindow.Get<Label>(PreferencesAutomationIds.ResultDetailsViewVersionLabel);
                isVisible = (System.Windows.Rect)versionLabel.AutomationElement.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty) != System.Windows.Rect.Empty ? true : false;
                Assert.IsNotNull(versionLabel, "The Version label was not found.");
                Assert.AreEqual(displayVersionAndTimestamp, isVisible, "The Version label's visibility is not the expected one.");
            }

            foreach (Part part in project.Parts)
            {
                TreeNode partNode = projectNode.GetNode(part.Name);
                Assert.IsNotNull(partNode, "The part node was not found.");
                partNode.SelectEx();

                lastModifiedLabel = Wait.For(() => mainWindow.Get<Label>(AssemblyAutomationIds.LastModifiedLabel));
                Assert.IsNotNull(lastModifiedLabel, "The Last Modified label was not found.");
                Assert.AreEqual(displayVersionAndTimestamp, lastModifiedLabel.Visible, "The Last Modified label's visibility is not the expected one.");

                partNode.ExpandEx();
                TreeNode resultDetailsNode = partNode.GetNode(AutomationIds.ResultDetailsNode);
                Assert.IsNotNull(resultDetailsNode, "The Result Details node was not found.");

                resultDetailsNode.Click();

                lastModifiedLabel = Wait.For(() => mainWindow.Get<Label>(PreferencesAutomationIds.ResultDetailsViewLastModifiedDateLabel));
                Assert.IsNotNull(lastModifiedLabel, "The Last Modified label was not found.");
                bool isVisible = (System.Windows.Rect)lastModifiedLabel.AutomationElement.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty) != System.Windows.Rect.Empty ? true : false;
                Assert.AreEqual(displayVersionAndTimestamp, isVisible, "The Last Modified label's visibility is not the expected one.");

                versionLabel = mainWindow.Get<Label>(PreferencesAutomationIds.ResultDetailsViewVersionLabel);
                isVisible = (System.Windows.Rect)versionLabel.AutomationElement.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty) != System.Windows.Rect.Empty ? true : false;
                Assert.IsNotNull(versionLabel, "The Version label was not found.");
                Assert.AreEqual(displayVersionAndTimestamp, isVisible, "The Version label's visibility is not the expected one.");
            }
        }

        /// <summary>
        /// Verifies if the "Capacity Utilization" column of the process steps are displayed.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="displayCapacityUtilization">True is the Capacity Utilization column should be displayed, false otherwise.</param>
        private static void VerifyDisplayCapacityUtilization(ApplicationEx app, bool displayCapacityUtilization)
        {
            Window mainWindow = app.Windows.MainWindow;
            TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            Assert.IsNotNull(myProjectsNode, "My Projects node was not found.");
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode(EditPreferencesTestCase.project.Name);
            Assert.IsNotNull(projectNode, "The Project node was not found.");
            projectNode.ExpandEx();

            foreach (Assembly assembly in project.Assemblies)
            {
                TreeNode assemblyNode = projectNode.GetNode(assembly.Name);
                Assert.IsNotNull(assemblyNode, "The assembly's node was not found.");
                assemblyNode.ExpandEx();

                TreeNode processNode = assemblyNode.GetNode("Process");
                Assert.IsNotNull(processNode, "The process node was not found.");
                processNode.ExpandEx();

                foreach (ProcessStep step in assembly.Process.Steps)
                {
                    TreeNode stepNode = processNode.GetNode(step.Name);
                    Assert.IsNotNull(stepNode, "The step's node was not found.");
                    stepNode.SelectEx();

                    TabPage machineTab = mainWindow.Get<TabPage>(AutomationIds.ProcessStepMachinesTab);
                    machineTab.Click();

                    var machinesDataGrid = mainWindow.Get<ListView>(ProcessStepAutomationIds.MachinesDataGrid);
                    Assert.IsNotNull(machinesDataGrid, "The Machines data grid was not found.");

                    ListViewColumn capacityUtilizationColumn = machinesDataGrid.Header.Column("Capacity Utilization");
                    if (displayCapacityUtilization)
                    {
                        Assert.IsNotNull(capacityUtilizationColumn, "Failed to display the Capacity Utilization column.");
                    }
                    else
                    {
                        Assert.IsNull(capacityUtilizationColumn, "The Capacity Utilization column was displayed although the related setting is unchecked.");
                    }
                }
            }

            foreach (Part part in project.Parts)
            {
                TreeNode partNode = projectNode.GetNode(part.Name);
                Assert.IsNotNull(partNode, "The assembly's node was not found.");
                partNode.ExpandEx();

                TreeNode processNode = partNode.GetNode("Process");
                Assert.IsNotNull(processNode, "The process node was not found.");
                processNode.ExpandEx();

                foreach (ProcessStep step in part.Process.Steps)
                {
                    TreeNode stepNode = processNode.GetNode(step.Name);
                    Assert.IsNotNull(stepNode, "The step's node was not found.");
                    stepNode.SelectEx();

                    var machinesTab = mainWindow.Get<TabPage>(AutomationIds.ProcessStepMachinesTab);
                    machinesTab.Click();

                    var machinesDataGrid = mainWindow.Get<ListView>(PartAutomationIds.MachinesDataGrid);
                    Assert.IsNotNull(machinesDataGrid, "The Machines data grid was not found.");

                    ListViewColumn capacityUtilizationColumn = machinesDataGrid.Header.Column("Capacity Utilization");
                    if (displayCapacityUtilization)
                    {
                        Assert.IsNotNull(capacityUtilizationColumn, "Failed to display the Capacity Utilization column.");
                    }
                    else
                    {
                        Assert.IsNull(capacityUtilizationColumn, "The Capacity Utilization column was displayed although the related setting is unchecked.");
                    }
                }
            }
        }

        /// <summary>
        /// Verifies if the "Calculation Result" of assemblies and parts entities of the test project are displayed into the application Header.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="displayCalculationResult">True if the calculation result should be displayed, false otherwise.</param>
        private static void VerifyDisplayCalculationResult(ApplicationEx app, bool displayCalculationResult)
        {
            Window mainWindow = app.Windows.MainWindow;
            TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            Assert.IsNotNull(myProjectsNode, "My Projects node was not found.");
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode(EditPreferencesTestCase.project.Name);
            Assert.IsNotNull(projectNode, "The Project node was not found.");
            projectNode.ExpandEx();

            Label resultDetailsLabel = null;
            foreach (Assembly assembly in EditPreferencesTestCase.project.Assemblies)
            {
                TreeNode assemblyNode = projectNode.GetNode(assembly.Name);
                Assert.IsNotNull(assemblyNode, "The assembly's node was not found.");
                assemblyNode.SelectEx();
                assemblyNode.ExpandEx();

                // TO DO -> Calculation Result label is not found (is an Extended Label Control)
                resultDetailsLabel = mainWindow.Get<Label>(PreferencesAutomationIds.CalculationResultsLabel);
                //Assert.IsNotNull(resultDetailsLabel, "The Result Details label was not found.");
                //Assert.AreEqual(displayCalculationResult, resultDetailsLabel.Visible, "The Result Details label's visibility is different than the expected one.");
            }

            foreach (Part part in EditPreferencesTestCase.project.Parts)
            {
                TreeNode partNode = projectNode.GetNode(part.Name);
                Assert.IsNotNull(partNode, "The part's node was not found.");
                partNode.SelectEx();

                resultDetailsLabel = mainWindow.Get<Label>(PreferencesAutomationIds.CalculationResultsLabel);
                //Assert.IsNotNull(resultDetailsLabel, "The Result Details label was not found.");
                //Assert.AreEqual(displayCalculationResult, resultDetailsLabel.Visible, "The Result Details label's visibility is different than the expected one.");
            }
        }

        /// <summary>
        /// Verifies if the Weight of assemblies and parts from the first level of the test project are displayed or not into the application header.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="displayWeight">True if the weight should be displayed, false otherwise.</param>
        private static void VerifyDisplayWeight(ApplicationEx app, bool displayWeight)
        {
            Window mainWindow = app.Windows.MainWindow;
            TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            Assert.IsNotNull(myProjectsNode, "My Projects node was not found.");
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode(EditPreferencesTestCase.project.Name);
            Assert.IsNotNull(projectNode, "The Project node was not found.");
            projectNode.ExpandEx();

            Label weightLabel = null;
            foreach (Assembly assembly in EditPreferencesTestCase.project.Assemblies)
            {
                TreeNode assemblyNode = projectNode.GetNode(assembly.Name);
                Assert.IsNotNull(assemblyNode, "The assembly's node was not found.");
                assemblyNode.SelectEx();
                //assemblyNode.Click();
                //Thread.Sleep(300);
                 weightLabel = mainWindow.Get<Label>(PreferencesAutomationIds.WeightLabel);
                Assert.IsNotNull(weightLabel, "The Weight label was not found.");
                Assert.AreEqual(displayWeight, weightLabel.Visible, "The Weight label's visibility is different than the expected one.");
            }

            foreach (Part part in EditPreferencesTestCase.project.Parts)
            {
                TreeNode partNode = projectNode.GetNode(part.Name);
                Assert.IsNotNull(partNode, "The part's node was not found.");
                partNode.SelectEx();

                //partNode.Click();
                //Thread.Sleep(300);

                weightLabel = mainWindow.Get<Label>(PreferencesAutomationIds.WeightLabel);
                //Assert.IsNotNull(weightLabel, "The Weight label was not found.");
                //Assert.AreEqual(displayWeight, weightLabel.Visible, "The Weight label's visibility is different than the expected one.");
            }
        }

        /// <summary>
        /// Verifies if the Investment of assemblies and parts from he first level of the test project are displayed or not into the application header.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="displayInvestment">True if the investment should be displayed, false otherwise.</param>
        private static void VerifyDisplayInvestment(ApplicationEx app, bool displayInvestment)
        {
            Window mainWindow = app.Windows.MainWindow;
            TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            Assert.IsNotNull(myProjectsNode, "My Projects node was not found.");
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode(EditPreferencesTestCase.project.Name);
            Assert.IsNotNull(projectNode, "The Project node was not found.");
            projectNode.ExpandEx();

            Label investmentLabel = null;
            foreach (Assembly assembly in EditPreferencesTestCase.project.Assemblies)
            {
                TreeNode assemblyNode = projectNode.GetNode(assembly.Name);              
                assemblyNode.SelectEx();

                investmentLabel = mainWindow.Get<Label>(PreferencesAutomationIds.InvestmentLabel);
                //Assert.IsNotNull(investmentLabel, "The Investment label was not found.");
                //Assert.AreEqual(displayInvestment, investmentLabel.Visible, "The Investment label's visibility is different than the expected one.");
            }

            foreach (Part part in EditPreferencesTestCase.project.Parts)
            {
                TreeNode partNode = projectNode.GetNode(part.Name);
                //MainScreenHelper.GetNodeByText(app, part.Name, projectNode, new List<TreeNode>());
                Assert.IsNotNull(partNode, "The part's node was not found.");

                partNode.Click();
                Thread.Sleep(300);

                investmentLabel = mainWindow.Get<Label>(PreferencesAutomationIds.InvestmentLabel);
                //Assert.IsNotNull(investmentLabel, "The Investment label was not found.");
                //Assert.AreEqual(displayInvestment, investmentLabel.Visible, "The Investment label's visibility is different than the expected one.");
            }
        }

        /// <summary>
        /// Verifies if the displayed name of raw material node is the localized name.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="localisedName">The raw material localized name.</param>
        private static void VerifyRawMaterialLocalizedName(ApplicationEx app, RawMaterialLocalisedName localisedName)
        {
            Tree searchResultsTree = app.MainScreen.SearchResultsTree;
            TreeNode rawMaterialsNode = searchResultsTree.Nodes.FirstOrDefault(n => n.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name.StartsWith(SearchToolAutomationIds.RawMaterialsNode));
            rawMaterialsNode.ExpandEx();
            Thread.Sleep(300);

            Window mainWindow = app.Windows.MainWindow;
            foreach (Part part in EditPreferencesTestCase.project.Parts)
            {
                foreach (RawMaterial material in part.RawMaterials)
                {
                    string expectedName = null;
                    switch (localisedName)
                    {
                        case RawMaterialLocalisedName.UKName:
                            expectedName = material.NameUK == null ? material.Name : material.NameUK;
                            break;
                        case RawMaterialLocalisedName.USName:
                            expectedName = material.NameUS == null ? material.Name : material.NameUS;
                            break;
                        case RawMaterialLocalisedName.GermanName:
                        default:
                            expectedName = material.Name;
                            break;
                    }

                    TreeNode materialNode = rawMaterialsNode.GetNode(expectedName);
                    Assert.IsNotNull(materialNode, "The raw material node was not found.");
                }
            }
        }

        /// <summary>
        /// Creates the data used in the EditPreferences tests.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            EditPreferencesTestCase.project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = project.Customer.Guid.ToString();
            project.Customer.Country = dataContext.CountryRepository.GetAll().FirstOrDefault().Name;

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.ManufacturingCountry = project.Customer.Country;
            part.Manufacturer = new Manufacturer();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.NameUS = material1.Name + "US";
            material1.Manufacturer = new Manufacturer();
            material1.Manufacturer.Name = material1.Manufacturer.Guid.ToString();
            part.RawMaterials.Add(material1);

            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            material2.NameUK = material2.Name + "UK";
            material2.Manufacturer = new Manufacturer();
            material2.Manufacturer.Name = material2.Manufacturer.Guid.ToString();
            part.RawMaterials.Add(material2);

            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.NameUK = material3.Name + "UK";
            material3.NameUS = material3.Name + "US";
            material3.Manufacturer = new Manufacturer();
            material3.Manufacturer.Name = material3.Manufacturer.ToString();
            part.RawMaterials.Add(material3);

            part.Process = new Process();
            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = 0;
            part.Process.Steps.Add(step1);

            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.Manufacturer = new Manufacturer();
            machine1.Manufacturer.Name = machine1.Guid.ToString();
            step1.Machines.Add(machine1);

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.AssemblingCountry = project.Customer.Country;
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            assembly.Process = new Process();
            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            step2.Accuracy = 0;
            assembly.Process.Steps.Add(step2);

            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = new Manufacturer();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            step2.Machines.Add(machine2);

            User owner = dataContext.UserRepository.GetByUsername("admin", false);
            project.SetOwner(owner);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
