﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.ProjectTests;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ProcessStepTests;
using ZPKTool.AutomatedTests.AssemblyTests;
using White.Core.InputDevices;
using System.IO;

namespace ZPKTool.AutomatedTests.PreferencesTests
{
    /// <summary>
    /// A test class for Edit option "Hide Empty Subassemblies and Parts Node" test case. 
    /// </summary>
    [TestClass]
    public class ShowImportConfirmationScreen
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Verifies if the empty subassemblies and parts node are hidden
        /// </summary>    
        [TestMethod]
        public void EdiShowImportConfirmationScreen()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                CheckBox dontShowImportConfirmationScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                Assert.IsNotNull(dontShowImportConfirmationScreenCheckBox, "Show Import Confirmation Screen checkBox was not found.");
                dontShowImportConfirmationScreenCheckBox.Checked = false;
                var savePrefsBtn = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePrefsBtn.Click();

                //step 2                
                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");

                //step 3
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
                importProjectMenuItem.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
                Thread.Sleep(1500);

                //step 4                
                Window confirmationWin = app.Windows.ImportConfirmationWindow;
                var cancelImportBtn = confirmationWin.Get<Button>(AutomationIds.CancelButton, true);
                cancelImportBtn.Click();

                //step 5                
                importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
                importProjectMenuItem.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));

                //step  6
                Button importButton = app.Windows.ImportConfirmationWindow.Get<Button>(AutomationIds.ImportButton);
                importButton.Click();                
                Thread.Sleep(2000);

                //step 7
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                dontShowImportConfirmationScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                Assert.IsNotNull(dontShowImportConfirmationScreenCheckBox, "Show Import Confirmation Screen checkBox was not found.");
                dontShowImportConfirmationScreenCheckBox.Checked = true;

                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                //step 8                
                TreeNode myProject = myProjectsNode.GetNode("myProject");
                Thread.Sleep(1200);

                Menu importAssemblyMenuItem = myProject.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.AssemblyImport);
                Assert.IsTrue(importAssemblyMenuItem.Enabled, "Import -> Assembly menu item is disabled.");
                importAssemblyMenuItem.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\30-2 NEXTEER - Motor Iskra.assembly"));                
                Thread.Sleep(4000);

                //step 9
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                dontShowImportConfirmationScreenCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                Assert.IsNotNull(dontShowImportConfirmationScreenCheckBox, "Show Import Confirmation Screen checkBox was not found.");
                dontShowImportConfirmationScreenCheckBox.Checked = false;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                //step 10                                
                myProject.SelectEx();
                Menu importPartMenuItem = myProject.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.ImportPart);
                Assert.IsTrue(importAssemblyMenuItem.Enabled, "Import -> Part menu item is disabled.");
                importPartMenuItem.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\part1.part"));

                CheckBox importConfirmationScreenCheckBox = app.Windows.ImportConfirmationWindow.Get<CheckBox>(AutomationIds.ImportConfirmationScreen);
                Assert.IsFalse(importConfirmationScreenCheckBox.Checked, "The check-box 'Don;t show this screen again' should be unchecked.");

                importButton = app.Windows.ImportConfirmationWindow.Get<Button>(AutomationIds.ImportButton);
                importButton.Click();
                Thread.Sleep(2500);
                // TODO: assert whether the part was imported
            }
        }
    }
}