﻿@BackgroundColor
Feature: EditBackgroundColor
	In order to set new color background
	I want to be able to change the background color

# comment
Scenario: Change the background color
	Given I have opened the Preferences - UI Settings screen
	And I have changed the Background color to another Color
	When I press Save button
	Then the result should be to set the Color backgound and the Preferences window is closed