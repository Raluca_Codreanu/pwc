﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.PreferencesTests
{
    public static class PreferencesAutomationIds
    {

        public const string ResultDetailsViewLastModifiedDateLabel = "LastModifiedDateLabel";
        public const string ResultDetailsViewVersionLabel = "VersionLabel";
        public const string LastModifiedLabel = "LastModifiedLabel";
        public const string UILanguageComboBox = "UILanguageCombo";
        public const string RestartButton = "RestartButton";
        public const string DisplayVersionAndTimestampCheckBox = "DisplayVersionTimestampInResultDetails";
        public const string DisplayCapacityUtilizationCheckBox = "DisplayCapacityUtilization";
        public const string ShowStartScreenCheckBox = "ShowStartScreen";
        public const string CalculationResultsLabel = "CalcResults";
        public const string DisplayCalculationResultCheckBox = "DisplayCalculationResult";
        public const string DisplayWeightCheckBox = "DisplayWeightCheckBox";
        public const string DisplayInvestmentCheckBox = "DisplayInvestmentCheckBox";
        public const string HeaderTab = "HeaderSettingsTabItem";
        public const string DatabaseTab = "DatabaseTabItem";
        public const string InvestmentLabel = "InvestmentLabelControl";
        public const string WeightLabel = "WeightLabelControl";
        public const string OnlineStatusLabel = "OnlineStatusLabel";
        public const string LocalDbServerNameTextBox = "LocalDbServerName";
        public const string UseWindowsAuthForLocalDbCheckBox = "UseWindowsAuthForLocalDbCheckBox";
        public const string LocalDbUsernameTextBox = "LocalDbUsername";
        public const string LocalDbPasswordBox = "LocalDbPassword";
        public const string CentralDbServerNameTextBox = "CentralDbServerName";
        public const string UseWindowsAuthForCentralDbCheckBox = "UseWindowsAuthForCentralDbCheckBox";
        public const string CentralDbUsernameTextBox = "CentralDbUsername";
        public const string CentralDbPasswordTextBox = "CentralDbPassword";
        public const string UIRawMaterialsNameCombo = "UIRawMaterialsNameCombo";
        public const string PreferencesTab = "PreferencesTab";
        public const string ShowImportConfirmationScreen = "ShowImportConfirmationScreen";
        public const string ShowEmptySubassembliesNodesCheckBox = "ShowEmptySubassembliesNodesCheckBox";
        public const string ProjectNameTextBox = "NameTextBox";
        public const string SupplierNameTextBox = "NameTextBox";
        public const string StaticDataAndSettingsCheckBox = "StaticDataAndSettingsCheckBox";
        public const string ReleasedProjectsCheckBox = "ReleasedProjectsCheckBox";
        public const string MasterDataCheckBox = "MasterDataCheckBox";
        public const string MyProjectsCheckBox = "MyProjectsCheckBox";
    }
}
