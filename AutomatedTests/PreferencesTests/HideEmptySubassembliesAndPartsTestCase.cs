﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.ProjectTests;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ProcessStepTests;
using ZPKTool.AutomatedTests.AssemblyTests;
using White.Core.InputDevices;
using System.IO;

namespace ZPKTool.AutomatedTests.PreferencesTests
{
    /// <summary>
    /// A test class for Edit option "Hide Empty Subassemblies and Parts Node" test case. 
    /// </summary>
    [TestClass]
    public class HideEmptySubassembliesAndPartsTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Verifies if the empty subassemblies and parts node are hidden
        /// </summary>    
        [TestMethod]
        public void EditHideEmptySubassembliesAndPartsNode()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;



                //step 3
                app.MainMenu.Options_Preferences.Click();

                //step4
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);
                CheckBox hideEmptySubassembliesNodesCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowEmptySubassembliesNodesCheckBox);
                Assert.IsNotNull(hideEmptySubassembliesNodesCheckBox, "Hide Empty Subassemblies and Parts node checkBox was not found.");

                hideEmptySubassembliesNodesCheckBox.Checked = false;
                Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                //step 5
                preferencesWindow.Close();
                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");

                // ensure that import summary screen is hidden
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);

                CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                displayImportSummaryScreen.Checked = true;

                Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(savePreferencesButton, "The Save button was not found.");

                savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePreferencesButton.Click();
                Wait.For(() => app.Windows.Preferences == null);

                //step 6
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(PreferencesAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "projectHide";

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(PreferencesAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.ExpandEx();

                TreeNode project1Node = myProjectsNode.GetNode("projectHide");
                project1Node.SelectEx();
                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();
                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assemblyHideComponents";

                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Cyprus");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
                mainWindow.WaitForAsyncUITasks();

                project1Node = myProjectsNode.GetNode("projectHide");
                project1Node.SelectEx();
                project1Node.ExpandEx();

                TreeNode assemblyNode = project1Node.GetNode("assemblyHideComponents");
                assemblyNode.SelectEx();
                assemblyNode.ExpandEx();

                TreeNode subAssemblyNode = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
                subAssemblyNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                partsNode.SelectEx();

                //step 7
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                hideEmptySubassembliesNodesCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowEmptySubassembliesNodesCheckBox);
                Assert.IsNotNull(hideEmptySubassembliesNodesCheckBox, "Show Start Screen checkBox was not found.");
                hideEmptySubassembliesNodesCheckBox.Checked = true;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                preferencesWindow.Close();
                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");

                myProjectsNode.ExpandEx();

                project1Node = myProjectsNode.GetNode("projectHide");
                project1Node.ExpandEx();

                assemblyNode = project1Node.GetNode("assemblyHideComponents");
                assemblyNode.ExpandEx();

                TreeNode subA1 = null;
                TreeNode part1 = null; 

                try
                {
                    subA1 = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
                }
                catch
                {
                }
                finally {
                    Assert.IsNull(subA1, "Subassemblies node should be hidden");
                }

                try
                {
                    part1 = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                }
                catch { }
                finally {
                    Assert.IsNull(part1, "Parts node should be hidden");
                }

                //step 8
                assemblyNode = project1Node.GetNode("assemblyHideComponents");
                assemblyNode.SelectEx();
                Thread.Sleep(3000);

                //step 9                
                Menu importPartMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportPart);
                Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
                importPartMenuItem.Click();

                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\part1.part"));
                mainWindow.WaitForAsyncUITasks();

                assemblyNode = project1Node.GetNode("assemblyHideComponents");
                assemblyNode.SelectEx();
                assemblyNode.ExpandEx();

                try
                {
                    subA1 = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
                }
                catch
                {
                }
                finally
                {
                    Assert.IsNull(subA1, "Subassemblies node should be hidden");
                }

                partsNode = assemblyNode.GetNode("Parts");
                partsNode.SelectEx();
                partsNode.ExpandEx();

                //step 11
                TreeNode part1Node = partsNode.GetNode("part1");
                part1Node.SelectEx();

                Menu deleteMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                try
                {
                    part1 = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
                }
                catch { }
                finally
                {
                    Assert.IsNull(part1, "Parts node should be hidden");
                }

                //step 13
                app.MainMenu.Options_Preferences.Click();
                preferencesWindow = Wait.For(() => app.Windows.Preferences);
                hideEmptySubassembliesNodesCheckBox = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowEmptySubassembliesNodesCheckBox);
                Assert.IsNotNull(hideEmptySubassembliesNodesCheckBox, "Show Start Screen checkBox was not found.");
                hideEmptySubassembliesNodesCheckBox.Checked = false;
                saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                preferencesWindow.Close();
                Assert.IsTrue(Wait.For(() => app.Windows.Preferences == null), "Failed to close the Preferences window.");
            }
        }
    }
}
