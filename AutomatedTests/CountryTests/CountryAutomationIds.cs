﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.CountryTests
{
    public static class CountryAutomationIds
    {
        public const string CountriesDataGrid = "CountriesDataGrid";
        public const string AddCountryButton = "AddButton";
        public const string CreateCountryButton = "SaveButton";
        public const string AreaComboBox = "AreaComboBox";
        public const string CurrencyComboBox = "CurrencyComboBox";
        public const string LengthComboBox = "LengthComboBox";
        public const string TimeComboBox = "TimeComboBox";
        public const string VolumeComboBox = "VolumeComboBox";
        public const string WeightComboBox = "WeightComboBox";
        public const string LabourCost1ShiftTextBox = "OneShiftModelRatioTextBox";
        public const string LabourCost2ShiftTextBox = "TwoShiftModelRatioTextBox";
        public const string LabourCost3ShiftTextBox = "ThreeShiftModelRatioTextBox";
        public const string UnskilledLabourCostTextBox = "UnskilledLabourCostTextBox";
        public const string SkilledLabourCostTextBox = "SkilledLabourCostTextBox";
        public const string ForemanCostTextBox = "ForemanCostTextBox";
        public const string TechnicianCostTextBox = "TechnicianCostTextBox";
        public const string EngineerCostTextBox = "EngineerCostTextBox";
        public const string AvailabilityTextBox = "AvailabilityTextBox";
        public const string EnergyCostTextBox = "EnergyCostTextBox";
        public const string AirCostTextBox = "AirCostTextBox";
        public const string WaterCostTextBox = "WaterCostTextBox";
        public const string ProductionAreaCostTextBox = "ProductionAreaCostTextBox";
        public const string OfficeAreaCostTextBox = "OfficeAreaCostTextBox";
        public const string InterestRateTextBox = "InterestRateTextBox";
        public const string CountrySettingsTab = "Country Settings";
        public const string SupplierSettingsTab = "Supplier Settings";
        public const string GeneralTab = "General";
        public const string CreateCountryWindow = "Create Country";            
        public const string CountriesComboBox = "CountriesComboBox";
        public const string SuppliersDataGrid = "StatesDataGrid";
        public const string AddStateButton = "AddStateButton";
        public const string CreateSupplierWindow = "Create Supplier";           
    }
}
