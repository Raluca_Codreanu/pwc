﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.Utils;

namespace ZPKTool.AutomatedTests.CountryTests
{
    /// <summary>
    /// The automated test of Create Country Test Case.
    /// </summary>
    [TestClass]
    public class CreateCountryTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCountryTestCase"/> class.
        /// </summary>
        public CreateCountryTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test of Create Country test case.
        /// </summary>
        [TestMethod]
        public void CreateCountryTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {                
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode countriesNode = app.MainScreen.ProjectsTree.MasterDataCountries;
                countriesNode.Click();
                ListViewControl countriesDataGrid = mainWindow.GetListView(CountryAutomationIds.CountriesDataGrid);
                Assert.IsNotNull(countriesDataGrid, "Manage Countries data grid was not found.");

                // step 5
                Button addButton = mainWindow.Get<Button>(CountryAutomationIds.AddCountryButton);
                Assert.IsNotNull(addButton, "Add button was not found.");
                addButton.Click();
                Window createCountryWindow = app.Windows.CountryWindow;

                // step 6                
                TextBox nameTextBox = createCountryWindow.Get<TextBox>(AutomationIds.NameTextBox);
                ValidationHelper.CheckTextFieldValidators(createCountryWindow, nameTextBox, 100);

                // step 7
                nameTextBox.Text = "Country" + DateTime.Now.Ticks;

                // step 8
                ComboBox areaComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.AreaComboBox);
                Assert.IsNotNull(areaComboBox, "Area combo box was not found.");

                areaComboBox.SelectItem(0);

                ComboBox currencyComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.CurrencyComboBox);
                Assert.IsNotNull(currencyComboBox, "Currency combo box was not found.");

                currencyComboBox.SelectItem(0);

                ComboBox lengthComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.LengthComboBox);
                Assert.IsNotNull(lengthComboBox, "Length combo box was not found.");

                lengthComboBox.SelectItem(0);

                ComboBox timeComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.TimeComboBox);
                Assert.IsNotNull(timeComboBox, "Time combo box was not found.");
                timeComboBox.SelectItem(0);

                ComboBox volumeComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.VolumeComboBox);
                Assert.IsNotNull(volumeComboBox, "Volume combo box was not found.");

                volumeComboBox.SelectItem(0);

                ComboBox weightComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.WeightComboBox);
                Assert.IsNotNull(weightComboBox, "Weight combo box was not found.");

                weightComboBox.SelectItem(0);

                TabPage countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.CountrySettingsTab);                
                countrySettingsTab.Click();

                // step 9 -> 14
                int maxAllowedValueOfDecimalFields = 1000000000;
                TextBox unskilledLabourCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.IsNotNull(unskilledLabourCostTextBox, "Unskilled Labour Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, unskilledLabourCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox skilledLabourCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.IsNotNull(skilledLabourCostTextBox, "Skilled Labour Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, skilledLabourCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox foremanCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.IsNotNull(foremanCostTextBox, "Foreman Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, foremanCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox technicianCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.IsNotNull(technicianCostTextBox, "Technician Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, technicianCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox engineerCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.IsNotNull(engineerCostTextBox, "Engineer Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, engineerCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox energyCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.IsNotNull(energyCostTextBox, "Energy Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, energyCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox airCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.IsNotNull(airCostTextBox, "Air Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, airCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox waterCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.IsNotNull(waterCostTextBox, "Water Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, waterCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox productionAreaCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.IsNotNull(productionAreaCostTextBox, "Production Area Rental Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, productionAreaCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox officeAreaCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.IsNotNull(officeAreaCostTextBox, "Office Area Rental Cost text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, officeAreaCostTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox labourAvailabilityTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.IsNotNull(labourAvailabilityTextBox, "Labour Availability text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, labourAvailabilityTextBox, 100, true);

                TextBox interestRateTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.IsNotNull(interestRateTextBox, "Interest Rate text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, interestRateTextBox, 100, true);

                TextBox firstShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.IsNotNull(firstShiftTextBox, "1 Shift text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, firstShiftTextBox, 999, true);

                TextBox secondShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.IsNotNull(secondShiftTextBox, "2 Shifts text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, secondShiftTextBox, 999, true);

                TextBox thirdShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.IsNotNull(thirdShiftTextBox, "3 Shifts text box was not found.");
                ValidationHelper.CheckNumericFieldValidators(createCountryWindow, thirdShiftTextBox, 999, true);

                // step 15
                Random random = new Random();
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();
                foremanCostTextBox.Text = random.Next(1000).ToString();
                technicianCostTextBox.Text = random.Next(1000).ToString();
                engineerCostTextBox.Text = random.Next(1000).ToString();
                energyCostTextBox.Text = random.Next(1000).ToString();
                airCostTextBox.Text = random.Next(1000).ToString();
                waterCostTextBox.Text = random.Next(1000).ToString();
                productionAreaCostTextBox.Text = random.Next(1000).ToString();
                officeAreaCostTextBox.Text = random.Next(1000).ToString();
                labourAvailabilityTextBox.Text = random.Next(1).ToString();
                interestRateTextBox.Text = random.Next(100).ToString();
                firstShiftTextBox.Text = random.Next(900).ToString();
                secondShiftTextBox.Text = random.Next(900).ToString();
                thirdShiftTextBox.Text = random.Next(900).ToString();

                Button cancelButton = createCountryWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");

                cancelButton.Click();
              
                // step 16                
                createCountryWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                //app.GetMessageDialog().ClickNo();
                
                // step 17
                cancelButton.Click();
                createCountryWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                //app.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                Assert.IsTrue(Wait.For(() => app.Windows.CountryAndSupplierBrowser == null), "Failed to close the Create Country window at Cancel.");

                // step 19                
                ListViewRow newCountryRow = countriesDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNull(newCountryRow, "The country was created although the cancel operation has been performed.");

                addButton.Click();
                createCountryWindow = Wait.For(() => app.Windows.CountryWindow);
                nameTextBox = createCountryWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Country" + DateTime.Now.Ticks;

                areaComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.AreaComboBox);
                areaComboBox.SelectItem(0);

                currencyComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.CurrencyComboBox);
                currencyComboBox.SelectItem(0);

                lengthComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.LengthComboBox);
                lengthComboBox.SelectItem(0);

                timeComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.TimeComboBox);
                timeComboBox.SelectItem(0);
                volumeComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.VolumeComboBox);
                volumeComboBox.SelectItem(0);
                weightComboBox = createCountryWindow.Get<ComboBox>(CountryAutomationIds.WeightComboBox);
                weightComboBox.SelectItem(0);
                countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.CountrySettingsTab);                
                countrySettingsTab.Click();

                unskilledLabourCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();

                skilledLabourCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();

                foremanCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                foremanCostTextBox.Text = random.Next(1000).ToString();

                technicianCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                technicianCostTextBox.Text = random.Next(1000).ToString();

                engineerCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                engineerCostTextBox.Text = random.Next(1000).ToString();

                labourAvailabilityTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                labourAvailabilityTextBox.Text = random.Next(100).ToString();

                energyCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                energyCostTextBox.Text = random.Next(1000).ToString();

                airCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                airCostTextBox.Text = random.Next(1000).ToString();

                waterCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                waterCostTextBox.Text = random.Next(1000).ToString();

                productionAreaCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                productionAreaCostTextBox.Text = random.Next(1000).ToString();

                officeAreaCostTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                officeAreaCostTextBox.Text = random.Next(1000).ToString();

                interestRateTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                interestRateTextBox.Text = random.Next(100).ToString();

                firstShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                firstShiftTextBox.Text = random.Next(900).ToString();

                secondShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                secondShiftTextBox.Text = random.Next(900).ToString();

                thirdShiftTextBox = createCountryWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                thirdShiftTextBox.Text = random.Next(900).ToString();

                // step 18 
                Button SaveButton = createCountryWindow.Get<Button>(CountryAutomationIds.CreateCountryButton);
                Assert.IsNotNull(SaveButton, "Create button was not found.");

                SaveButton.Click();
                // step 19
                countriesDataGrid.Select("Name", nameTextBox.Text);
                Assert.AreEqual(nameTextBox.Text, mainWindow.Get<TextBox>(AutomationIds.NameTextBox).Text, "The newly created country's name is wrong.");
            }
        }

        #region Helper
        #endregion Helper
    }
}
