﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using ZPKTool.AutomatedTests.CustomUIItems;
using System.Windows.Automation;
using White.Core.UIItems.Finders;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.InputDevices;
using ZPKTool.Data;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.Utils;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.CountryTests
{
    /// <summary>
    /// The automated test of Create Supplier Test Case.
    /// </summary>
    [TestClass]
    public class CreateSupplierTestCase
    {
        /// <summary>
        /// The parent country of the created supplier.
        /// </summary>
        private static Country country;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateSupplierTestCase"/> class.
        /// </summary>
        public CreateSupplierTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            CreateSupplierTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Create Supplier test case.
        /// </summary>
        [TestMethod]
        public void CreateSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {                
                Window mainWindow = app.Windows.MainWindow;
           
                // step 4
                TreeNode countriesNode = app.MainScreen.ProjectsTree.MasterDataCountries;
                countriesNode.Click();

                ListViewControl countriesDataGrid = mainWindow.GetListView(CountryAutomationIds.CountriesDataGrid);
                Assert.IsNotNull(countriesDataGrid, "Countries data grid was not found.");

                // step 5
                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SuppliersTab);
                supplierTab.Click();

                ComboBox countriesComboBox = Wait.For(() => mainWindow.Get<ComboBox>(CountryAutomationIds.CountriesComboBox));

                ListViewControl suppliersDataGrid = mainWindow.GetListView(CountryAutomationIds.SuppliersDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");

                // step 6
                countriesComboBox.Click();
                ListItem countryItem = countriesComboBox.Items.FirstOrDefault(c => c.Text == CreateSupplierTestCase.country.Name);
                Assert.IsNotNull(countryItem, "The country created for this test was not found in Countries combo box.");
                countryItem.Select();

                // step 7
                Button addButton = mainWindow.Get<Button>(CountryAutomationIds.AddStateButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");

                addButton.Click();
                Window createSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                // step 8
                Button SaveButton = createSupplierWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(SaveButton, "Create button was not found.");
                Assert.IsFalse(SaveButton.Enabled, "The save button should not be enabled.");

                // step 9
                TextBox nameTextBox = createSupplierWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, nameTextBox, 100);

                // step 10
                nameTextBox.Text = "Supplier" + DateTime.Now.Ticks;

                TabPage supplierSettingsTab = mainWindow.Get<TabPage>(AutomationIds.SupplierSettingsTab);                
                supplierSettingsTab.Click();

                TextBox unskilledLabourCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.IsNotNull(unskilledLabourCostTextBox, "The Unskilled Labour Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.UnskilledLaborCost).ToString(), unskilledLabourCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox skilledLabourCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.IsNotNull(skilledLabourCostTextBox, "The Skilled Labour Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.SkilledLaborCost).ToString(), skilledLabourCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox foremanCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.IsNotNull(foremanCostTextBox, "The Foreman Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.ForemanCost).ToString(), foremanCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox technicianCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.IsNotNull(technicianCostTextBox, "The Technician Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.TechnicianCost).ToString(), technicianCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox engineerCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.IsNotNull(engineerCostTextBox, "The Engineer Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.EngineerCost).ToString(), engineerCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox labourAvailabilityTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.IsNotNull(labourAvailabilityTextBox, "The Labour Availability text box was not found.");
                Assert.AreEqual((CreateSupplierTestCase.country.CountrySetting.LaborAvailability * 100).ToString(), labourAvailabilityTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox energyCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.IsNotNull(energyCostTextBox, "The Energy Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.EnergyCost).ToString(), energyCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox airCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.IsNotNull(airCostTextBox, "The Air Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.AirCost).ToString(), airCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox waterCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.IsNotNull(waterCostTextBox, "The Water Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.WaterCost).ToString(), waterCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox productionAreaCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.IsNotNull(productionAreaCostTextBox, "The Production Area Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.ProductionAreaRentalCost).ToString(), productionAreaCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox officeAreaCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.IsNotNull(officeAreaCostTextBox, "The Office Area Cost text box was not found.");
                Assert.AreEqual(Formatter.FormatMoney(CreateSupplierTestCase.country.CountrySetting.OfficeAreaRentalCost).ToString(), officeAreaCostTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox interestRateTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.IsNotNull(interestRateTextBox, "The Interest Rate text box was not found.");
                Assert.AreEqual((CreateSupplierTestCase.country.CountrySetting.InterestRate * 100).ToString(), interestRateTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox firstShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.IsNotNull(firstShiftTextBox, "The First Shift text box was not found.");
                Assert.AreEqual((CreateSupplierTestCase.country.CountrySetting.ShiftCharge1ShiftModel * 100).ToString(), firstShiftTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox secondShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.IsNotNull(secondShiftTextBox, "The Second Shift text box was not found.");
                Assert.AreEqual((CreateSupplierTestCase.country.CountrySetting.ShiftCharge2ShiftModel * 100).ToString(), secondShiftTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                TextBox thirdShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.IsNotNull(thirdShiftTextBox, "The Third Shift text box was not found.");
                Assert.AreEqual((CreateSupplierTestCase.country.CountrySetting.ShiftCharge3ShiftModel * 100).ToString(), thirdShiftTextBox.Text, "The displayed Cost is different than the cost of the selected country.");

                // step 12
                unskilledLabourCostTextBox.Text = string.Empty;
                skilledLabourCostTextBox.Text = string.Empty;
                foremanCostTextBox.Text = string.Empty;
                technicianCostTextBox.Text = string.Empty;
                engineerCostTextBox.Text = string.Empty;
                labourAvailabilityTextBox.Text = string.Empty;
                energyCostTextBox.Text = string.Empty;
                airCostTextBox.Text = string.Empty;
                waterCostTextBox.Text = string.Empty;
                productionAreaCostTextBox.Text = string.Empty;
                officeAreaCostTextBox.Text = string.Empty;
                interestRateTextBox.Text = string.Empty;
                firstShiftTextBox.Text = string.Empty;
                secondShiftTextBox.Text = string.Empty;
                thirdShiftTextBox.Text = string.Empty;

                // step 13
                Assert.IsFalse(SaveButton.Enabled, "The save button should not be enabled.");

                // step 14 -> 19
                int maxValueForDecimalFileds = 1000000000;
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, unskilledLabourCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, skilledLabourCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, foremanCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, technicianCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, engineerCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, labourAvailabilityTextBox, 100, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, energyCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, airCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, waterCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.VerifyNumericFieldValidators(createSupplierWindow, productionAreaCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.VerifyNumericFieldValidators(createSupplierWindow, officeAreaCostTextBox, maxValueForDecimalFileds, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, interestRateTextBox, 100, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, firstShiftTextBox, 999, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, secondShiftTextBox, 999, true);
                ValidationHelper.CheckNumericFieldValidators(createSupplierWindow, thirdShiftTextBox, 999, true);

                // step 20
                Random random = new Random();
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();
                foremanCostTextBox.Text = random.Next(1000).ToString();
                technicianCostTextBox.Text = random.Next(1000).ToString();
                engineerCostTextBox.Text = random.Next(1000).ToString();
                labourAvailabilityTextBox.Text = random.Next(100).ToString();
                energyCostTextBox.Text = random.Next(1000).ToString();
                airCostTextBox.Text = random.Next(1000).ToString();
                waterCostTextBox.Text = random.Next(1000).ToString();
                productionAreaCostTextBox.Text = random.Next(1000).ToString();
                officeAreaCostTextBox.Text = random.Next(1000).ToString();
                interestRateTextBox.Text = random.Next(100).ToString();
                firstShiftTextBox.Text = random.Next(900).ToString();
                secondShiftTextBox.Text = random.Next(900).ToString();
                thirdShiftTextBox.Text = random.Next(900).ToString();

                Button cancelButton = createSupplierWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");
                cancelButton.Click();

                // step 21
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                // step 22
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                //Assert.IsNull(CreateSupplierTestCase.GetCreateSupplierWindow(app), "Create Supplier window was not closed at Cancel.");

                ListViewRow supplierRow = suppliersDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNull(supplierRow, "The supplier was created although the create supplier operation has been cancelled.");

                // step 23
                addButton.Click();
                createSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);
                nameTextBox = createSupplierWindow.Get<TextBox>(AutomationIds.NameTextBox);

                nameTextBox.Text = "Supplier" + DateTime.Now.Ticks;

                supplierSettingsTab = mainWindow.Get<TabPage>(AutomationIds.SupplierSettingsTab);                
                supplierSettingsTab.Click();

                unskilledLabourCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();

                skilledLabourCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();

                foremanCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                foremanCostTextBox.Text = random.Next(1000).ToString();

                technicianCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                technicianCostTextBox.Text = random.Next(1000).ToString();

                engineerCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                engineerCostTextBox.Text = random.Next(1000).ToString();

                labourAvailabilityTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                labourAvailabilityTextBox.Text = random.Next(100).ToString();

                energyCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                energyCostTextBox.Text = random.Next(1000).ToString();

                airCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                airCostTextBox.Text = random.Next(1000).ToString();

                waterCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                waterCostTextBox.Text = random.Next(1000).ToString();

                productionAreaCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                productionAreaCostTextBox.Text = random.Next(1000).ToString();

                officeAreaCostTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                officeAreaCostTextBox.Text = random.Next(1000).ToString();

                interestRateTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                interestRateTextBox.Text = random.Next(100).ToString();

                firstShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                firstShiftTextBox.Text = random.Next(900).ToString();

                secondShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                secondShiftTextBox.Text = random.Next(900).ToString();

                thirdShiftTextBox = createSupplierWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                thirdShiftTextBox.Text = random.Next(900).ToString();

                SaveButton = createSupplierWindow.Get<Button>(AutomationIds.SaveButton);
                SaveButton.Click();
                Assert.IsTrue(Wait.For(() => app.Windows.SupplierWindow == null), "Failed to close the Create Supplier window.");
                supplierRow = suppliersDataGrid.Row("Name", nameTextBox.Text);

                // step 24
                suppliersDataGrid.Select("Name", nameTextBox.Text);
                Assert.AreEqual(nameTextBox.Text, mainWindow.Get<TextBox>(AutomationIds.NameTextBox).Text, "Wrong name is displayed in Supplier View.");
                Assert.AreEqual(nameTextBox.Text, suppliersDataGrid.Cell("Name", supplierRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong name is displayed in Suppliers data grid.");

                TabPage countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.SupplierSettingsTab);
                countrySettingsTab.Click();

                Assert.AreEqual(unskilledLabourCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox).Text);
                Assert.AreEqual(skilledLabourCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox).Text);
                Assert.AreEqual(foremanCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox).Text);
                Assert.AreEqual(technicianCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox).Text);
                Assert.AreEqual(engineerCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox).Text);
                Assert.AreEqual(labourAvailabilityTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox).Text);
                Assert.AreEqual(energyCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox).Text);
                Assert.AreEqual(airCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox).Text);
                Assert.AreEqual(waterCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox).Text);
                Assert.AreEqual(productionAreaCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox).Text);
                Assert.AreEqual(officeAreaCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox).Text);
                Assert.AreEqual(interestRateTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox).Text);
                Assert.AreEqual(firstShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox).Text);
                Assert.AreEqual(secondShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox).Text);
                Assert.AreEqual(thirdShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox).Text);

                // step 25
                countriesComboBox.SelectNextItem();

                supplierRow = suppliersDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNull(supplierRow, "The displayed supplier doesn't belong to the selected country.");

                // step 26
                countriesComboBox.SelectItem(CreateSupplierTestCase.country.Name);
                supplierRow = suppliersDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNotNull(supplierRow, "The created supplier was not found into the Suppliers data grid.");
            }
        }

        #region Helper

        /// <summary>
        /// Gets the Create Supplier window.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <returns>The Create Supplier window.</returns>
        //public static Window GetCreateSupplierWindow(Application app)
        //{

        //    Window createSupplierWindow = app.GetWindows().FirstOrDefault(w => w.Title == SupplierAutomationIds.CreateSupplierWindow);

        //    return createSupplierWindow;
        //}

        /// <summary>
        /// Creates the data that will be used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            CreateSupplierTestCase.country = new Country();
            CreateSupplierTestCase.country.Name = "Country" + DateTime.Now.Ticks;

            var units = dataContext.MeasurementUnitRepository.GetBaseMeasurementUnits();

            CreateSupplierTestCase.country.WeightMeasurementUnit = units.FirstOrDefault(m => m.Name == "Kilogram" && !m.IsReleased);
            CreateSupplierTestCase.country.FloorMeasurementUnit = units.FirstOrDefault(m => m.Name == "Square Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.LengthMeasurementUnit = units.FirstOrDefault(m => m.Name == "Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.VolumeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Cubic Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.TimeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Second" && !m.IsReleased);
            CreateSupplierTestCase.country.Currency = dataContext.CurrencyRepository.GetBaseCurrencies().FirstOrDefault(c => c.Name == "Euro" && !c.IsReleased);

            Random random = new Random();
            CountrySetting countrySettings = new CountrySetting();
            countrySettings.UnskilledLaborCost = random.Next(10000);
            countrySettings.SkilledLaborCost = random.Next(10000);
            countrySettings.ForemanCost = random.Next(10000);
            countrySettings.TechnicianCost = random.Next(10000);
            countrySettings.EngineerCost = random.Next(10000);
            countrySettings.EnergyCost = random.Next(10000);
            countrySettings.AirCost = random.Next(10000);
            countrySettings.WaterCost = random.Next(10000);
            countrySettings.ProductionAreaRentalCost = random.Next(10000);
            countrySettings.OfficeAreaRentalCost = random.Next(10000);
            countrySettings.InterestRate = random.Next(1);
            countrySettings.ShiftCharge1ShiftModel = random.Next(9);
            countrySettings.ShiftCharge2ShiftModel = random.Next(9);
            countrySettings.ShiftCharge3ShiftModel = random.Next(9);
            countrySettings.LaborAvailability = random.Next(1);
            CreateSupplierTestCase.country.CountrySetting = countrySettings;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
