﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.CountryTests
{
    /// <summary>
    /// The automated test of Edit Country Test Case.
    /// </summary>
    [TestClass]
    public class EditCountryTestCase
    {
        /// <summary>
        /// The country which will be edited.
        /// </summary>
        private static Country country;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditCountryTestCase"/> class.
        /// </summary>
        public EditCountryTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditCountryTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// The automated test of Edit Country Test Case.
        /// </summary>
        [TestMethod]
        public void EditCountryTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {                
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode countriesNode = app.MainScreen.ProjectsTree.MasterDataCountries;
                countriesNode.Click();

                ListViewControl countriesDataGrid = mainWindow.GetListView(CountryAutomationIds.CountriesDataGrid);
                Assert.IsNotNull(countriesDataGrid, "Countries data grid was not found.");

                // step 5
                countriesDataGrid.Select("Name", EditCountryTestCase.country.Name);
                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");

                ComboBox currencyComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.CurrencyComboBox);
                Assert.IsNotNull(currencyComboBox, "The Currency combo box was not found.");

                ComboBox lengthComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.LengthComboBox);
                Assert.IsNotNull(lengthComboBox, "The Length combo box was not found.");

                ComboBox areaComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.AreaComboBox);
                Assert.IsNotNull(areaComboBox, "The Area combo box was not found.");

                ComboBox volumeComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.VolumeComboBox);
                Assert.IsNotNull(volumeComboBox, "The Volume combo box was not found.");

                ComboBox timeComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.TimeComboBox);
                Assert.IsNotNull(timeComboBox, "The Time combo box was not found.");

                ComboBox weightComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.WeightComboBox);
                Assert.IsNotNull(weightComboBox, "The Weight combo box was not found.");

                // step 6                                                
                nameTextBox.Text = string.Empty;
                weightComboBox.Focus(); // move the focus so the save button state gets updated.

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                Assert.IsFalse(saveButton.Enabled, "The Save button is enabled although the Name text box is empty.");

                // step 7
                nameTextBox.Text = "Country" + DateTime.Now.Ticks;

                // step 8
                currencyComboBox.SelectItem(1);
                lengthComboBox.SelectItem(1);

                areaComboBox.SelectItem(0);

                volumeComboBox.SelectItem(0);

                timeComboBox.SelectItem(0);

                weightComboBox.SelectItem(0);

                // step 9
                TabPage countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.CountrySettingsTab);                
                countrySettingsTab.Click();

                Random random = new Random();
                TextBox unskilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.IsNotNull(unskilledLabourCostTextBox, "The Unskilled Labour Cost text box was not found.");
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();

                TextBox skilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.IsNotNull(skilledLabourCostTextBox, "The Skilled Labour Cost text box was not found.");
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();

                TextBox foremanCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.IsNotNull(foremanCostTextBox, "The Foreman Cost text box was not found.");
                foremanCostTextBox.Text = random.Next(1000).ToString();

                TextBox technicianCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.IsNotNull(technicianCostTextBox, "The Technician Cost text box was not found.");
                technicianCostTextBox.Text = random.Next(1000).ToString();

                TextBox engineerCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.IsNotNull(engineerCostTextBox, "The Engineer Cost text box was not found.");
                engineerCostTextBox.Text = random.Next(1000).ToString();

                TextBox labourAvailabilityTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.IsNotNull(labourAvailabilityTextBox, "The Labour Availability text box was not found.");
                labourAvailabilityTextBox.Text = random.Next(100).ToString();

                TextBox energyCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.IsNotNull(energyCostTextBox, "The Energy Cost text box was not found.");
                energyCostTextBox.Text = random.Next(1000).ToString();

                TextBox airCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.IsNotNull(airCostTextBox, "The Air Cost text box was not found.");
                airCostTextBox.Text = random.Next(1000).ToString();

                TextBox waterCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.IsNotNull(waterCostTextBox, "The Water Cost text box was not found.");
                waterCostTextBox.Text = random.Next(1000).ToString();

                TextBox productionAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.IsNotNull(productionAreaCostTextBox, "The Production Area Cost text box was not found.");
                productionAreaCostTextBox.Text = random.Next(1000).ToString();

                TextBox officeAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.IsNotNull(officeAreaCostTextBox, "The Office Area Cost text box was not found.");
                officeAreaCostTextBox.Text = random.Next(1000).ToString();

                TextBox interestRateTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.IsNotNull(interestRateTextBox, "The Interest Rate text box was not found.");
                interestRateTextBox.Text = random.Next(100).ToString();

                TextBox firstShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.IsNotNull(firstShiftTextBox, "The First Shift text box was not found.");
                firstShiftTextBox.Text = random.Next(900).ToString();

                TextBox secondShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.IsNotNull(secondShiftTextBox, "The Second Shift text box was not found.");
                secondShiftTextBox.Text = random.Next(900).ToString();

                TextBox thirdShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.IsNotNull(thirdShiftTextBox, "The Third Shift text box was not found.");
                thirdShiftTextBox.Text = random.Next(900).ToString();

                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click();
                
                // step 10
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "The Save button is disabled after the Cancel -> No buttons were clicked.");
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.CancelButton).Enabled, "The Cancel button is disabled after the Cancel -> No buttons were clicked.");

                // step 11
                cancelButton.Click();
                //app.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                unskilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.UnskilledLaborCost).ToString(), unskilledLabourCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                skilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.SkilledLaborCost).ToString(), skilledLabourCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                foremanCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.ForemanCost).ToString(), foremanCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                technicianCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.TechnicianCost).ToString(), technicianCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                engineerCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.EngineerCost).ToString(), engineerCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                labourAvailabilityTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.AreEqual((EditCountryTestCase.country.CountrySetting.LaborAvailability * 100).ToString(), labourAvailabilityTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                energyCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.EnergyCost).ToString(), energyCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                airCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.AirCost).ToString(), airCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                waterCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.WaterCost).ToString(), waterCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                productionAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.ProductionAreaRentalCost).ToString(), productionAreaCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                officeAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditCountryTestCase.country.CountrySetting.OfficeAreaRentalCost).ToString(), officeAreaCostTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                interestRateTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.AreEqual((EditCountryTestCase.country.CountrySetting.InterestRate * 100).ToString(), interestRateTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                firstShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.AreEqual((EditCountryTestCase.country.CountrySetting.ShiftCharge1ShiftModel * 100).ToString(), firstShiftTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                secondShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.AreEqual((EditCountryTestCase.country.CountrySetting.ShiftCharge2ShiftModel * 100).ToString(), secondShiftTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                thirdShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.AreEqual((EditCountryTestCase.country.CountrySetting.ShiftCharge3ShiftModel * 100).ToString(), thirdShiftTextBox.Text, "The country was updated although the Cancel -> Yes option was selected.");

                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.CountriesTab);                
                generalTab.Click();

                nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.AreEqual(EditCountryTestCase.country.Name, nameTextBox.Text, "The country name was updated although the Cancel -> Yes option was selected.");

                //currencyComboBox = mainWindow.Get<ComboBox>(CreateCountryTestCase.UIItemsIdentifiers.CurrencyComboBox);
                //Assert.AreEqual(EditCountryTestCase.country.Currency.Name, currencyComboBox.SelectedItemText, "The country currency was updated although the Cancel -> Yes option was selected.");

                lengthComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.LengthComboBox);
                Assert.AreEqual(EditCountryTestCase.country.LengthMeasurementUnit.Name, lengthComboBox.SelectedItemText, "The country length unit was updated although the Cancel -> Yes option was selected.");

                volumeComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.VolumeComboBox);
                Assert.AreEqual(EditCountryTestCase.country.VolumeMeasurementUnit.Name, volumeComboBox.SelectedItemText, "The country volume unit was updated although the Cancel -> Yes option was selected.");

                areaComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.AreaComboBox);
                Assert.AreEqual(EditCountryTestCase.country.FloorMeasurementUnit.Name, areaComboBox.SelectedItemText, "The country floor unit was updated although the Cancel -> Yes option was selected.");

                timeComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.TimeComboBox);
                Assert.AreEqual(EditCountryTestCase.country.TimeMeasurementUnit.Name, timeComboBox.SelectedItemText, "The country time unit was updated although the Cancel -> Yes option was selected.");

                weightComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.WeightComboBox);
                Assert.AreEqual(EditCountryTestCase.country.WeightMeasurementUnit.Name, weightComboBox.SelectedItemText, "The country weight unit was updated although the Cancel -> Yes option was selected.");

                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "The Save button is enabled after the Cancel");
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.CancelButton).Enabled, "The Cancel button is enabled after the Cancel");

                // step 12
                nameTextBox.Text = "Country" + DateTime.Now.Ticks;
                currencyComboBox.SelectItem(1);
                lengthComboBox.SelectItem(1);

                areaComboBox.SelectItem(0);

                volumeComboBox.SelectItem(0);

                timeComboBox.SelectItem(0);

                weightComboBox.SelectItem(0);

                countrySettingsTab.Click();
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();
                foremanCostTextBox.Text = random.Next(1000).ToString();
                technicianCostTextBox.Text = random.Next(1000).ToString();
                energyCostTextBox.Text = random.Next(1000).ToString();
                engineerCostTextBox.Text = random.Next(1000).ToString();
                airCostTextBox.Text = random.Next(1000).ToString();
                waterCostTextBox.Text = random.Next(1000).ToString();
                productionAreaCostTextBox.Text = random.Next(1000).ToString();
                officeAreaCostTextBox.Text = random.Next(1000).ToString();
                interestRateTextBox.Text = random.Next(100).ToString();
                firstShiftTextBox.Text = random.Next(900).ToString();
                secondShiftTextBox.Text = random.Next(900).ToString();
                thirdShiftTextBox.Text = random.Next(900).ToString();
                labourAvailabilityTextBox.Text = random.Next(100).ToString();

                saveButton.Click();
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "Save button is enabled after the changes were saved.");
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.CancelButton).Enabled, "Cancel button is enabled after the changes were saved.");

                countriesDataGrid = mainWindow.GetListView(CountryAutomationIds.CountriesDataGrid);
                ListViewRow updatedCountryRow = countriesDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNotNull(updatedCountryRow, "The row of the updated country was not found.");
                
                ListViewCell currencyCell = countriesDataGrid.CellByIndex(1, updatedCountryRow);
                Assert.IsNotNull(currencyCell, "The Currency cell was not found.");
                Assert.AreEqual(currencyComboBox.SelectedItemText, currencyCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Currency in Countries data grid.");

                ListViewCell lengthUnitCell = countriesDataGrid.CellByIndex(3, updatedCountryRow);
                Assert.IsNotNull(lengthUnitCell, "The Length cell was not found.");
                Assert.AreEqual(lengthComboBox.SelectedItemText, lengthUnitCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Length unit in Countries data grid.");

                ListViewCell volumeUnitCell = countriesDataGrid.CellByIndex(5, updatedCountryRow);
                Assert.IsNotNull(volumeUnitCell, "The Volume cell was not found.");
                Assert.AreEqual(volumeComboBox.SelectedItemText, volumeUnitCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Volume unit in Countries data grid.");

                ListViewCell areaUnitCell = countriesDataGrid.CellByIndex(2, updatedCountryRow);
                Assert.IsNotNull(areaUnitCell, "The Area cell was not found.");
                Assert.AreEqual(areaComboBox.SelectedItemText, areaUnitCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Area unit in Countries data grid.");

                ListViewCell timeUnitCell = countriesDataGrid.CellByIndex(4, updatedCountryRow);
                Assert.IsNotNull(timeUnitCell, "The Time cell was not found.");
                Assert.AreEqual(timeComboBox.SelectedItemText, timeUnitCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Time unit in Countries data grid.");

                ListViewCell weightUnitCell = countriesDataGrid.CellByIndex(6, updatedCountryRow);
                Assert.IsNotNull(weightUnitCell, "The Weight cell was not found.");
                Assert.AreEqual(weightComboBox.SelectedItemText, weightUnitCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the Weight unit in Countries data grid.");

                countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.CountrySettingsTab);
                countrySettingsTab.Click();

                Assert.AreEqual(unskilledLabourCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox).Text);
                Assert.AreEqual(skilledLabourCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox).Text);
                Assert.AreEqual(foremanCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox).Text);
                Assert.AreEqual(technicianCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox).Text);
                Assert.AreEqual(engineerCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox).Text);
                Assert.AreEqual(labourAvailabilityTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox).Text);
                Assert.AreEqual(energyCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox).Text);
                Assert.AreEqual(airCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox).Text);
                Assert.AreEqual(waterCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox).Text);
                Assert.AreEqual(productionAreaCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox).Text);
                Assert.AreEqual(officeAreaCostTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox).Text);
                Assert.AreEqual(interestRateTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox).Text);
                Assert.AreEqual(firstShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox).Text);
                Assert.AreEqual(secondShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox).Text);
                Assert.AreEqual(thirdShiftTextBox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox).Text);
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data for this test. Add a country in Master Data.
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            country = new Country();
            country.Name = "Country" + DateTime.Now.Ticks;


            var units = dataContext.MeasurementUnitRepository.GetBaseMeasurementUnits();
            country.WeightMeasurementUnit = units.FirstOrDefault(m => m.Name == "Kilogram" && !m.IsReleased && m.IsBaseUnit);
            country.FloorMeasurementUnit = units.FirstOrDefault(m => m.Name == "Square Meter" && !m.IsReleased && m.IsBaseUnit);
            country.LengthMeasurementUnit = units.FirstOrDefault(m => m.Name == "Meter" && !m.IsReleased && m.IsBaseUnit);
            country.VolumeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Cubic Meter" && !m.IsReleased && m.IsBaseUnit);
            country.TimeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Second" && !m.IsReleased && m.IsBaseUnit);
            country.Currency = dataContext.CurrencyRepository.GetBaseCurrencies().FirstOrDefault(c => c.Name == "Euro" && !c.IsReleased);

            Random random = new Random();
            CountrySetting countrySettings = new CountrySetting();
            countrySettings.UnskilledLaborCost = random.Next(10000);
            countrySettings.SkilledLaborCost = random.Next(10000);
            countrySettings.ForemanCost = random.Next(10000);
            countrySettings.TechnicianCost = random.Next(10000);
            countrySettings.EngineerCost = random.Next(10000);
            countrySettings.EnergyCost = random.Next(10000);
            countrySettings.AirCost = random.Next(10000);
            countrySettings.WaterCost = random.Next(10000);
            countrySettings.ProductionAreaRentalCost = random.Next(10000);
            countrySettings.OfficeAreaRentalCost = random.Next(10000);
            countrySettings.InterestRate = random.Next(1);
            countrySettings.ShiftCharge1ShiftModel = random.Next(9);
            countrySettings.ShiftCharge2ShiftModel = random.Next(9);
            countrySettings.ShiftCharge3ShiftModel = random.Next(9);
            countrySettings.LaborAvailability = random.Next(1);
            country.CountrySetting = countrySettings;
            countrySettings.IsScrambled = false;

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
