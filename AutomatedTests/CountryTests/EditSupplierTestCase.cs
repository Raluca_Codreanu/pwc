﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using ZPKTool.Data;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.CountryTests
{
    /// <summary>
    /// The automated test of Edit Supplier Test Case.
    /// </summary>
    [TestClass]
    public class EditSupplierTestCase
    {
        private static CountryState supplier;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditSupplierTestCase"/> class.
        /// </summary>
        public EditSupplierTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditSupplierTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// The automated test of Edit Supplier Test Case (Test Link id = 91).
        /// </summary>
        [TestMethod]
        public void EditSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode countriesNode = app.MainScreen.ProjectsTree.MasterDataCountries;
                countriesNode.Click();

                // step 5
                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SuppliersTab);
                supplierTab.Click();

                ComboBox countriesComboBox = mainWindow.Get<ComboBox>(CountryAutomationIds.CountriesComboBox);
                Assert.IsNotNull(countriesComboBox, "Countries combo box was not found.");

                countriesComboBox.Click();
                countriesComboBox.Items.FirstOrDefault(c => c.Text == EditSupplierTestCase.supplier.Country.Name).Select();

                ListViewControl suppliersDataGrid = mainWindow.GetListView(CountryAutomationIds.SuppliersDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");

                // The country created for this test has just one supplier
                ListViewRow supplierRow = suppliersDataGrid.Row("Name", EditSupplierTestCase.supplier.Name);
                Assert.IsNotNull(supplierRow, "The supplier's row was not found.");

                // step 6
                suppliersDataGrid.Select("Name", EditSupplierTestCase.supplier.Name);

                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
                Assert.AreEqual(EditSupplierTestCase.supplier.Name, nameTextBox.Text, "The displayed name is wrong.");

                // step 7
                nameTextBox.Text = string.Empty;
                suppliersDataGrid.Focus(); // move the focus from the name text box to trigger the state update for the save button.

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                Assert.IsFalse(saveButton.Enabled, "The Save button is enabled although the Name text box is empty.");

                // step 8
                nameTextBox.Text = "Supplier1";

                // step 9                
                TabPage supplierSettingsTab = mainWindow.Get<TabPage>(AutomationIds.SupplierSettingsTab);
                supplierSettingsTab.Click();

                Random random = new Random();
                TextBox unskilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.IsNotNull(unskilledLabourCostTextBox, "The Unskilled Labour Cost text box was not found.");
                unskilledLabourCostTextBox.Text = random.Next(1000).ToString();

                TextBox skilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.IsNotNull(skilledLabourCostTextBox, "The Skilled Labour Cost text box was not found.");
                skilledLabourCostTextBox.Text = random.Next(1000).ToString();

                TextBox foremanCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.IsNotNull(foremanCostTextBox, "The Foreman Cost text box was not found.");
                foremanCostTextBox.Text = random.Next(1000).ToString();

                TextBox technicianCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.IsNotNull(technicianCostTextBox, "The Technician Cost text box was not found.");
                technicianCostTextBox.Text = random.Next(1000).ToString();

                TextBox engineerCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.IsNotNull(engineerCostTextBox, "The Engineer Cost text box was not found.");
                engineerCostTextBox.Text = random.Next(1000).ToString();

                TextBox labourAvailabilityTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.IsNotNull(labourAvailabilityTextBox, "The Labour Availability text box was not found.");
                labourAvailabilityTextBox.Text = random.Next(100).ToString();

                TextBox energyCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.IsNotNull(energyCostTextBox, "The Energy Cost text box was not found.");
                energyCostTextBox.Text = random.Next(1000).ToString();

                TextBox airCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.IsNotNull(airCostTextBox, "The Air Cost text box was not found.");
                airCostTextBox.Text = random.Next(1000).ToString();

                TextBox waterCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.IsNotNull(waterCostTextBox, "The Water Cost text box was not found.");
                waterCostTextBox.Text = random.Next(1000).ToString();

                TextBox productionAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.IsNotNull(productionAreaCostTextBox, "The Production Area Cost text box was not found.");
                productionAreaCostTextBox.Text = random.Next(1000).ToString();

                TextBox officeAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.IsNotNull(officeAreaCostTextBox, "The Office Area Cost text box was not found.");
                officeAreaCostTextBox.Text = random.Next(1000).ToString();

                TextBox interestRateTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.IsNotNull(interestRateTextBox, "The Interest Rate text box was not found.");
                interestRateTextBox.Text = random.Next(100).ToString();

                TextBox firstShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.IsNotNull(firstShiftTextBox, "The First Shift text box was not found.");
                firstShiftTextBox.Text = random.Next(900).ToString();

                TextBox secondShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.IsNotNull(secondShiftTextBox, "The Second Shift text box was not found.");
                secondShiftTextBox.Text = random.Next(900).ToString();

                TextBox thirdShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.IsNotNull(thirdShiftTextBox, "The Third Shift text box was not found.");
                thirdShiftTextBox.Text = random.Next(900).ToString();

                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click();
                
                // step 10
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "The Save button is disabled after the Cancel -> No buttons were clicked.");
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.CancelButton).Enabled, "The Cancel button is disabled after the Cancel -> No buttons were clicked.");

                // step 11
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                unskilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.UnskilledLaborCost).ToString(), unskilledLabourCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                skilledLabourCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.SkilledLaborCost).ToString(), skilledLabourCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                foremanCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.ForemanCost).ToString(), foremanCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                technicianCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.TechnicianCost).ToString(), technicianCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                engineerCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.EngineerCost).ToString(), engineerCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                labourAvailabilityTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                Assert.AreEqual((EditSupplierTestCase.supplier.CountrySettings.LaborAvailability * 100).ToString(), labourAvailabilityTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                energyCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.EnergyCost).ToString(), energyCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                airCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.AirCost).ToString(), airCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                waterCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.WaterCost).ToString(), waterCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                productionAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.ProductionAreaRentalCost).ToString(), productionAreaCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                officeAreaCostTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                Assert.AreEqual(Formatter.FormatMoney(EditSupplierTestCase.supplier.CountrySettings.OfficeAreaRentalCost).ToString(), officeAreaCostTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                interestRateTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                Assert.AreEqual((EditSupplierTestCase.supplier.CountrySettings.InterestRate * 100).ToString(), interestRateTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                firstShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                Assert.AreEqual((EditSupplierTestCase.supplier.CountrySettings.ShiftCharge1ShiftModel * 100).ToString(), firstShiftTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                secondShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                Assert.AreEqual((EditSupplierTestCase.supplier.CountrySettings.ShiftCharge2ShiftModel * 100).ToString(), secondShiftTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                thirdShiftTextBox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                Assert.AreEqual((EditSupplierTestCase.supplier.CountrySettings.ShiftCharge3ShiftModel * 100).ToString(), thirdShiftTextBox.Text, "The supplier was updated although the Cancel -> Yes option was selected.");

                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();

                nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.AreEqual(EditSupplierTestCase.supplier.Name, nameTextBox.Text, "The supplier name was updated although the Cancel -> Yes option was selected.");

                // step 12
                TreeNode currenciesNode = app.MainScreen.ProjectsTree.MasterDataCurrencies;
                currenciesNode.Click();
                countriesNode.Click();
                TabPage suppliersTab = mainWindow.Get<TabPage>(AutomationIds.SuppliersTab);
                suppliersTab.Click();

                ComboBox countriesCombobox = mainWindow.Get<ComboBox>(CountryAutomationIds.CountriesComboBox);
                countriesCombobox.Click();
                countriesCombobox.Items.FirstOrDefault(c => c.Text == "CountrySupplier").Select();

                ListViewControl suppliersDatagrid = mainWindow.GetListView(CountryAutomationIds.SuppliersDataGrid);
                Assert.IsNotNull(suppliersDatagrid, "Suppliers data grid was not found.");

                Assert.IsNotNull(supplierRow, "The supplier's row was not found.");

                suppliersDatagrid.Select("Name", "CountrySupplier");

                TextBox nameTextbox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextbox.Text = "Supplier2";
                Button savebutton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                savebutton.Click();
                Assert.AreEqual(nameTextbox.Text, mainWindow.Get<TextBox>(AutomationIds.NameTextBox).Text);

                TabPage supplierSettingstab = mainWindow.Get<TabPage>(AutomationIds.SupplierSettingsTab);
                supplierSettingstab.Click();

                TextBox unskilledLabourCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox);
                unskilledLabourCostTextbox.Text = random.Next(1000).ToString();
                TextBox skilledLabourCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox);
                skilledLabourCostTextbox.Text = random.Next(1000).ToString();
                TextBox foremanCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox);
                foremanCostTextbox.Text = random.Next(1000).ToString();
                TextBox technicianCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox);
                technicianCostTextbox.Text = random.Next(1000).ToString();
                TextBox energyCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox);
                energyCostTextbox.Text = random.Next(1000).ToString();
                TextBox engineerCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox);
                engineerCostTextbox.Text = random.Next(1000).ToString();
                TextBox airCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox);
                airCostTextbox.Text = random.Next(1000).ToString();
                TextBox waterCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox);
                waterCostTextbox.Text = random.Next(1000).ToString();
                TextBox productionAreaCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox);
                productionAreaCostTextbox.Text = random.Next(1000).ToString();
                TextBox officeAreaCostTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox);
                officeAreaCostTextbox.Text = random.Next(1000).ToString();
                TextBox interestRateTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox);
                interestRateTextbox.Text = random.Next(100).ToString();
                TextBox firstShiftTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox);
                firstShiftTextbox.Text = random.Next(900).ToString();
                TextBox secondShiftTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox);
                secondShiftTextbox.Text = random.Next(900).ToString();
                TextBox thirdShiftTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox);
                thirdShiftTextbox.Text = random.Next(900).ToString();
                TextBox labourAvailabilityTextbox = mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox);
                labourAvailabilityTextbox.Text = random.Next(100).ToString();

                Button saveButtonSettingsTab=mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButtonSettingsTab.Click();
                Assert.IsTrue(mainWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "Save button is enabled after the changes were saved.");

                suppliersDataGrid = mainWindow.GetListView(CountryAutomationIds.SuppliersDataGrid);
                supplierRow = suppliersDataGrid.Row("Name", nameTextbox.Text);

                Assert.AreEqual(unskilledLabourCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.UnskilledLabourCostTextBox).Text);
                Assert.AreEqual(skilledLabourCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.SkilledLabourCostTextBox).Text);
                Assert.AreEqual(foremanCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ForemanCostTextBox).Text);
                Assert.AreEqual(technicianCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.TechnicianCostTextBox).Text);
                Assert.AreEqual(engineerCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EngineerCostTextBox).Text);
                Assert.AreEqual(labourAvailabilityTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AvailabilityTextBox).Text);
                Assert.AreEqual(energyCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.EnergyCostTextBox).Text);
                Assert.AreEqual(airCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.AirCostTextBox).Text);
                Assert.AreEqual(waterCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.WaterCostTextBox).Text);
                Assert.AreEqual(productionAreaCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.ProductionAreaCostTextBox).Text);
                Assert.AreEqual(officeAreaCostTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.OfficeAreaCostTextBox).Text);
                Assert.AreEqual(interestRateTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.InterestRateTextBox).Text);
                Assert.AreEqual(firstShiftTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost1ShiftTextBox).Text);
                Assert.AreEqual(secondShiftTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost2ShiftTextBox).Text);
                Assert.AreEqual(thirdShiftTextbox.Text, mainWindow.Get<TextBox>(CountryAutomationIds.LabourCost3ShiftTextBox).Text);
                
            }
        }

        #region Helper

        /// <summary>
        /// Creates the test data. A country with a supplier.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            Country country = new Country();
            country.Name = "CountrySupplier";

            var units = dataContext.MeasurementUnitRepository.GetBaseMeasurementUnits();

            country.WeightMeasurementUnit = units.FirstOrDefault(m => m.Name == "Kilogram" && !m.IsReleased && m.IsBaseUnit);
            country.FloorMeasurementUnit = units.FirstOrDefault(m => m.Name == "Square Meter" && !m.IsReleased && m.IsBaseUnit);
            country.LengthMeasurementUnit = units.FirstOrDefault(m => m.Name == "Meter" && !m.IsReleased && m.IsBaseUnit);
            country.VolumeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Cubic Meter" && !m.IsReleased && m.IsBaseUnit);
            country.TimeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Second" && !m.IsReleased && m.IsBaseUnit);
            country.Currency = dataContext.CurrencyRepository.GetBaseCurrencies().FirstOrDefault(c => c.Name == "Euro" && !c.IsReleased);

            Random random = new Random();
            CountrySetting countrySettings = new CountrySetting();
            countrySettings.UnskilledLaborCost = random.Next(10000);
            countrySettings.SkilledLaborCost = random.Next(10000);
            countrySettings.ForemanCost = random.Next(10000);
            countrySettings.TechnicianCost = random.Next(10000);
            countrySettings.EngineerCost = random.Next(10000);
            countrySettings.EnergyCost = random.Next(10000);
            countrySettings.AirCost = random.Next(10000);
            countrySettings.WaterCost = random.Next(10000);
            countrySettings.ProductionAreaRentalCost = random.Next(10000);
            countrySettings.OfficeAreaRentalCost = random.Next(10000);
            countrySettings.InterestRate = random.Next(1);
            countrySettings.ShiftCharge1ShiftModel = random.Next(9);
            countrySettings.ShiftCharge2ShiftModel = random.Next(9);
            countrySettings.ShiftCharge3ShiftModel = random.Next(9);
            countrySettings.LaborAvailability = random.Next(1);
            country.CountrySetting = countrySettings;

            EditSupplierTestCase.supplier = new CountryState();
            EditSupplierTestCase.supplier.Name = "CountrySupplier";
            EditSupplierTestCase.supplier.CountrySettings = countrySettings;
            country.States.Add(EditSupplierTestCase.supplier);

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();
        }

        #endregion Helper

        #region Inner classes
        #endregion Inner classes
    }
}
