﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyPartToTheSameProjectSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyPartToTheSameProject")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart1.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyPartToTheSameProject")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the first part node to choose copy option")]
        public void GivenISelectTheFirstPartNodeToChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("partToBeCopied1");
            part1Node.SelectEx();

            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [Given(@"I select the same project for choosing the paste option")]
        public void GivenISelectTheSameProjectForChoosingThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the copied part to Copy it")]
        public void GivenISelectTheCopiedPartToCopyIt()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.SelectEx();

            TreeNode part1Node = projectNode.GetNode(copyTranslation + "partToBeCopied1");
            part1Node.SelectEx();

            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [When(@"I select the same node to choose paste")]
        public void WhenISelectTheSameNodeToChoosePaste()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Then(@"the copy of the part is copied")]
        public void ThenTheCopyOfThePartIsCopied()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode(copyTranslation + copyTranslation + "partToBeCopied1");
            partNode.SelectEx();
        }
    }
}
