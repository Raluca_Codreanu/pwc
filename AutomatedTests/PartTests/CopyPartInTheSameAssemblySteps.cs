﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyPartInTheSameAssemblySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyPartInTheSameAssembly")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart1.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyPartInTheSameAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"I select project -> assembly -> part node to choose copy option")]
        public void GivenISelectProject_Assembly_PartNodeToChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopyPart1");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();

            TreeNode part1Node = partsNode.GetNode("partAssembly");
            part1Node.SelectEx();

            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [Given(@"I select the same Assembly to choose Paste option")]
        public void GivenISelectTheSameAssemblyToChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopyPart1");
            assemblyNode.SelectEx();

            Menu pasteMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the copied Part from assembly to choose Copy option")]
        public void GivenISelectTheCopiedPartFromAssemblyToChooseCopyOption()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopyPart1");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();

            TreeNode part1Node = partsNode.GetNode(copyTranslation + "partAssembly");
            part1Node.SelectEx();


            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();   
        }
        
        [When(@"I select the same Assembly node to choose Paste option")]
        public void WhenISelectTheSameAssemblyNodeToChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopyPart1");
            assemblyNode.SelectEx();
            Menu pasteMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Then(@"a copy of Part is available in Assembly node")]
        public void ThenACopyOfPartIsAvailableInAssemblyNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopyPart1");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();

            TreeNode part1Node = partsNode.GetNode(copyTranslation + copyTranslation + "partAssembly");
            part1Node.SelectEx();    
        }
    }
}
