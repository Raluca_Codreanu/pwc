﻿@CutPartPasteInProject
Feature: CutPartPasteInProject

Scenario: Cut Part Paste In Project
	Given I select a project -> part node to select Cut option
	And I select another project node to select Paste option
	And I select the moved part and select Cut option
	When I select the project node and select Paste option
	Then the paste is disabled
