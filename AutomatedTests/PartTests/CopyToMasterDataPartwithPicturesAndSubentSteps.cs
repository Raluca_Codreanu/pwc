﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class CopyToMasterDataPartwithPicturesAndSubentSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CopyToMasterDataPartwithPicturesAndSubent")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectPartPictures.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyToMasterDataPartwithPicturesAndSubent")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a part to choose the Copy to Master Data option")]
        public void GivenISelectAPartToChooseTheCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectPartPictures");
            projectPartVideoNode.ExpandEx();
            projectPartVideoNode.SelectEx();

            TreeNode partWithVideoNode = projectPartVideoNode.GetNode("partPictures");
            partWithVideoNode.SelectEx();

            Menu copyToMasterData = partWithVideoNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterData.Click();
        }
        
        [Given(@"I press the No button in the confirmation window")]
        public void GivenIPressTheNoButtonInTheConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I select the part to choose the Copy To Master Data option")]
        public void GivenISelectThePartToChooseTheCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectPartPictures");
            projectPartVideoNode.ExpandEx();
            projectPartVideoNode.SelectEx();

            TreeNode partWithVideoNode = projectPartVideoNode.GetNode("partPictures");
            partWithVideoNode.SelectEx();

            Menu copyToMasterData = partWithVideoNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterData.Click();    
        }
        
        [Given(@"I press the Yes button in the confirmation scren")]
        public void GivenIPressTheYesButtonInTheConfirmationScren()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the part is copied to the master data")]
        public void ThenThePartIsCopiedToTheMasterData()
        {
            TreeNode partsNode = application.MainScreen.ProjectsTree.MasterDataParts;
            partsNode.Click();
            partsNode.SelectEx();
            mainWindow.WaitForAsyncUITasks();
            ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);

            partsDataGrid.Select("Name", "partPictures");

            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
            mainWindow.WaitForAsyncUITasks();    
        }
        
        [Then(@"pictures are copied")]
        public void ThenPicturesAreCopied()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsTrue(deleteMediaButton.Enabled, "Media panel contains pictures");
        }
        
        [Then(@"commodity is copied")]
        public void ThenCommodityIsCopied()
        {
            TreeNode partsNode = application.MainScreen.ProjectsTree.MasterDataParts;
            partsNode.ExpandEx();
            mainWindow.WaitForAsyncUITasks();

            TreeNode partNode = partsNode.GetNode("partPictures");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode commodityNode = materialsNode.GetNode("commodity1");
            commodityNode.SelectEx();
        }
    }
}
