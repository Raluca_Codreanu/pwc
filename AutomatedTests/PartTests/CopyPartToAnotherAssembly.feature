﻿@CopyPartToAnotherAssembly
Feature: CopyPartToAnotherAssembly

Scenario: Copy Part To Another Assembly
Given I select the Project->Assembly->Part node to select copy option
When I select the second assembly to select the paste option
Then the copy of part is copied in the second assembly
