﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class CancelCreatePartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectCancelCreatePart";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CancelCreatePart")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;            
        }

        [AfterFeature("CancelCreatePart")]
        private static void KillApplication()
        {
            application.Kill();
        }
        
        [Given(@"I open the Create Part screen")]
        public void GivenIOpenTheCreatePartScreen()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectCancelCreatePart");
            projectNode1.SelectEx();

            Menu createPartMenuItem = projectNode1.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
            createPartMenuItem.Click();
        }
        
        [Given(@"I press Cancel button")]
        public void GivenIPressCancelButton()
        {
            Window createPartWindow = application.Windows.PartWindow;

            Button cancelButton = createPartWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I open the create Part screen and fill some fields")]
        public void GivenIOpenTheCreatePartScreenAndFillSomeFields()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectCancelCreatePart");
            projectNode1.SelectEx();

            Menu createPartMenuItem = projectNode1.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
            createPartMenuItem.Click();

            Window createPartWindow = application.Windows.PartWindow;
            TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "cancePartProject";

            TextBox numberTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.NumberTextBox);
            numberTextBox.Text = "123";

            TextBox versionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";
        }

        [Given(@"I click Cancel agaian")]
        public void GivenIClickCancelAgaian()
        {
            Window createPartWindow = application.Windows.PartWindow;
            Button cancelButton = createPartWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I press the No button in confirmation window")]
        public void GivenIPressTheNoButtonInConfirmationWindow()
        {
            application.Windows.PartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
               
        [Given(@"I Close \(X\) the confirmation window")]
        public void GivenICloseXTheConfirmationWindow()
        {
            application.Windows.PartWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Given(@"I press Cancel button again")]
        public void GivenIPressCancelButtonAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();      
        }
        
        [Given(@"I press the Yes button in confirmation")]
        public void GivenIPressTheYesButtonInConfirmation()
        {
            application.Windows.PartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Given(@"I open the create part window and close it")]
        public void GivenIOpenTheCreatePartWindowAndCloseIt()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectCancelCreatePart");
            projectNode1.SelectEx();

            Menu createPartMenuItem = projectNode1.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
            createPartMenuItem.Click();

            Window createPartWindow = application.Windows.PartWindow;
            createPartWindow.Close();
        }

        [Given(@"I open the create part window")]
        public void GivenIOpenTheCreatePartWindow()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectCancelCreatePart");
            projectNode1.SelectEx();

            Menu createPartMenuItem = projectNode1.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
            createPartMenuItem.Click();
        }

        [Given(@"I fill some fields")]
        public void GivenIFillSomeFields()
        {
            Window createPartWindow = application.Windows.PartWindow;
            TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "cancePartProject";

            TextBox numberTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.NumberTextBox);
            numberTextBox.Text = "123";

            TextBox versionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";
        }
        
        [Given(@"I close the create part window")]
        public void GivenICloseTheCreatePartWindow()
        {
            Window createPartWindow = application.Windows.PartWindow;
            createPartWindow.Close();   
        }
        
        [Given(@"I press No in the confirmation window")]
        public void GivenIPressNoInTheConfirmationWindow()
        {
            application.Windows.PartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [When(@"I press Yes in the confirmation window")]
        public void WhenIPressYesInTheConfirmationWindow()
        {
            application.Windows.PartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the create part window is closed")]
        public void ThenTheCreatePartWindowIsClosed()
        {
            Window createPartWindow = application.Windows.PartWindow;
            Assert.IsNull(createPartWindow, "The window is closed");
        }
    }
}
