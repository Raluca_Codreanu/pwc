﻿@CancelCreatePart
Feature: CancelCreatePart
	
Scenario: Cancel Create a Part
	Given I open the Create Part screen 
	And I press Cancel button
	And I open the create Part screen and fill some fields
	And I press Cancel button
	And I press the No button in confirmation window
	And I click Cancel agaian
	And I Close (X) the confirmation window
	And I press Cancel button again
	And I press the Yes button in confirmation 
	And I open the create part window and close it
	And I open the create part window 
	And I fill some fields
	And I close the create part window
	And I press No in the confirmation window
	And I close the create part window 
	When I press Yes in the confirmation window
	Then the create part window is closed 
