﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class CutPartSteps
    {
        // Feature scoped data
        // TestLink id = 446, id = 447
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CutPart")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);


            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectCutPart1.project"));
            mainWindow.WaitForAsyncUITasks();
            
            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.Collapse();
 
            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importAnotherProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectCutPart2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CutPart")]
        private static void KillApplication()
        {
            application.Kill();
        }


        [Given(@"I select a part node to choose Cut option")]
        public void GivenISelectAPartNodeToChooseCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart1");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("cutPart12");
            partNode.SelectEx();

            Menu cutMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }

        [Given(@"I select an assembly node to choose Paste option")]
        public void GivenISelectAnAssemblyNodeToChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCutPart");
            assemblyNode.SelectEx();

            Menu pasteMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I select the part from assembly an choose Cut option")]
        public void GivenISelectThePartFromAssemblyAnChooseCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCutPart");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();

            TreeNode partNode2 = partsNode.GetNode("cutPart2");
            partNode2.SelectEx();

            Menu cutMenuItem = partNode2.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }

        [When(@"I select the same assembly node and choose Paste option")]
        public void WhenISelectTheSameAssemblyNodeAndChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCutPart");
            assemblyNode.ExpandEx();
            assemblyNode.SelectEx();
        }
        
        [Then(@"the Paste is disabled")]
        public void ThenThePasteIsDisabled()
        {
            Menu pasteMenuItem = application.MainMenu.Edit_Paste;

            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
            mainWindow.WaitForAsyncUITasks();
        }
    }
}
