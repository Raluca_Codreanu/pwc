﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class ImportExportPartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string partExportPath;

        public ImportExportPartSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportPartSteps.partExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".part");
            if (File.Exists(ImportExportPartSteps.partExportPath))
            {
                File.Delete(ImportExportPartSteps.partExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportPartSteps.partExportPath))
            {
                File.Delete(ImportExportPartSteps.partExportPath);
            }
        }

        [BeforeFeature("ImportExportPart")]
        private static void LaunchApplication()
        {
            MyClassInitialize();

            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();

        }

        [AfterFeature("ImportExportPart")]
        private static void KillApplication()
        {
            MyClassCleanup();
            application.Kill();            
        }
        [Given(@"I select the Part node to choose Export option")]
        public void GivenISelectThePartNodeToChooseExportOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("part1");
            part1Node.Collapse();
            part1Node.SelectEx();
            Menu exportMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I cancel the save as window")]
        public void GivenICancelTheSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I press again the Export Option")]
        public void GivenIPressAgainTheExportOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("part1");
            part1Node.SelectEx();
            part1Node.Collapse();

            Menu exportMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press the Save Button")]
        public void GivenIPressTheSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportPartSteps.partExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ImportExportPartSteps.partExportPath), "Part was not exported at the specified location.");
        }
        
        [When(@"I select the project node to import the exported part")]
        public void WhenISelectTheProjectNodeToImportTheExportedPart()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.SelectEx();

            Menu importPartMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.ImportPart);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem.Click();
        }
        
        [Then(@"the part is imported in the project")]
        public void ThenThePartIsImportedInTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("part1");
            partNode.SelectEx();
        }
    }
}
