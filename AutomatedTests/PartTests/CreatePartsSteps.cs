﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using ZPKTool.AutomatedTests.PartTests;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class CreatePartsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Project project = new Project();
            project.Name = "projectTestParts";
            project.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 1;
            project.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CreateParts")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CreateParts")]
        private static void KillApplication()
        {
            application.Kill();
        }


        [Given(@"I select projectPartsTest node and choose the Create Part option")]
        public void GivenISelectProjectPartsTestNodeAndChooseTheCreatePartOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectTestParts");
            projectNode.SelectEx();
            Menu createPartMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
            createPartMenuItem.Click();
        }
        
        [Given(@"I fill all the mandatory fields from General tab")]
        public void GivenIFillAllTheMandatoryFieldsFromGeneralTab()
        {
            Window createPartWindow = application.Windows.PartWindow;

            TextBox partName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            partName.Text = "partTest";

            TextBox numberTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.NumberTextBox);
            numberTextBox.Text = "123";

            TextBox versionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";

            TextBox descriptionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "description of part";

            Button browseCountryButton = createPartWindow.Get<Button>("BrowseCountryMasterData");
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = application.Windows.CountryAndSupplierBrowser;

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Belgium");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");
            selectButton.Click();
            TextBox countryTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
        }
        
        [Given(@"I select the accuracy type")]
        public void GivenISelectTheAccuracyType(Table table)
        {
            foreach (var row in table.Rows)
            {
                var p0 = row["accuracyType"];
                Window createPartWindow = application.Windows.PartWindow;
                ComboBox calculationAccuracyComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationAccuracyComboBox);
                calculationAccuracyComboBox.SelectItem(Int32.Parse(p0));
            }
        }
        
        [Given(@"I fill Estimated Cost field and the Weight field")]
        public void GivenIFillEstimatedCostFieldAndTheWeightField()
        {
            Window createPartWindow = application.Windows.PartWindow;
            TextBox estimatedCostTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.EstimatedCostTextBox);
            Assert.IsNotNull(estimatedCostTextBox, "EstimatedCostTextBox was not found");
            estimatedCostTextBox.Text = "1000";

            //step 5
            TextBox weightTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.WeightTextBox);
            Assert.IsNotNull(weightTextBox, "Weight Text Box was not found");
            weightTextBox.Text = "10";
        }
        
        [Given(@"I add one video file")]
        public void GivenIAddOneVideoFile()
        {
            var mediaControl = application.Windows.PartWindow.GetMediaControl();
            mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Wildlife.wmv", UriKind.Relative);
        }
        
        [Given(@"I fill all the mandatory fields from Manufacturer tab")]
        public void GivenIFillAllTheMandatoryFieldsFromManufacturerTab()
        {
            Window createPartWindow = application.Windows.PartWindow;
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";
        }
        
        [When(@"I click on the save button")]
        public void WhenIClickOnTheSaveButton()
        {
            Window createPartWindow = application.Windows.PartWindow;
            Button saveButton = createPartWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [Then(@"the part with the seleced Accuracy")]
        public void ThenThePartWithTheSelecedAccuracy()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.Collapse();
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectTestParts");
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("partTest (123)");
            Assert.IsNotNull(partNode);

        }
    }
}
