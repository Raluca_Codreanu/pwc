﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class CutPartPasteInProjectSteps
    {
        // Feature scoped data
        // TestLink id = 446, id = 447
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CutPartPasteInProject")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);


            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectCutPart1.project"));
            mainWindow.WaitForAsyncUITasks();

            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.SelectEx();
            myProjectsnode.Collapse();

            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importAnotherProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectCutPart2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CutPartPasteInProject")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"I select a project -> part node to select Cut option")]
        public void GivenISelectAProject_PartNodeToSelectCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart1");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("cutPart1");
            partNode.SelectEx();

            Menu cutMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }
        
        [Given(@"I select another project node to select Paste option")]
        public void GivenISelectAnotherProjectNodeToSelectPasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.SelectEx();
            projectNode.Collapse();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Given(@"I select the moved part and select Cut option")]
        public void GivenISelectTheMovedPartAndSelectCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.Collapse();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("cutPart1");
            partNode.SelectEx();

            Menu cutMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }
        
        [When(@"I select the project node and select Paste option")]
        public void WhenISelectTheProjectNodeAndSelectPasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.Collapse();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCutPart2");
            projectNode.SelectEx();
            projectNode.Collapse();
        }
        
        [Then(@"the paste is disabled")]
        public void ThenThePasteIsDisabled()
        {
            Menu pasteMenuItem = application.MainMenu.Edit_Paste;

            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
            mainWindow.WaitForAsyncUITasks();
        }
    }
}
