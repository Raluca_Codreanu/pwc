﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.Commodity;
using ZPKTool.AutomatedTests.RawMaterialTests;
using System.Threading;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.PartTests
{
    [TestClass]
    public class CreatePartTest
    {
        /// <summary>
        /// Create a Part with Fine Calculation Accuracy Test Case (test Link = 287)
        /// </summary>
        [TestMethod]
        [TestCategory("Part")]
        public void CreatePartWithFineCalculationAccuracy()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();

                Window createProjectWindow = app.Windows.ProjectWindow;
                var projectName = EncryptionManager.Instance.GenerateRandomString(10, true);

                TextBox projectNameTextBox = createProjectWindow.Get<TextBox>(PartAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectNameTextBox, "Project Name was not found.");
                projectNameTextBox.Text = projectName;

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(PartAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.ExpandEx();
                TreeNode projectNode = myProjectsNode.GetNode(projectName);

                Menu createPartMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreatePart(app, app.Windows.PartWindow);

                TreeNode partNode = projectNode.GetNode("partProject (123)");

                Menu createCommodityItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateCommodity);
                createCommodityItem.Click();
                CommodityTestsHelper.CreateCommodity(app);

                Menu createRawMaterialItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateRawMaterial);
                createRawMaterialItem.Click();
                RawMaterialTestsHelper.CreateRawMaterial(app);

                Menu createRawPartItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateRawPart);
                createRawPartItem.Click();
                PartTestsHelper.CreatePart(app, app.Windows.RawPartWindow);

                // check if only 1 raw part / part could be added
                createRawPartItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateRawPart);
                createRawPartItem.Click();

                mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
            }
        }

        [TestMethod]
        [TestCategory("Part")]
        public void CreatePartMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode partsNode = app.MainScreen.ProjectsTree.MasterDataParts;
                Assert.IsNotNull(partsNode, "The Parts node was not found.");
                partsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();

                ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Parts data grid was not found.");

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                PartTestsHelper.CreatePart(app, app.Windows.PartWindow);
            }
        }

        [TestMethod]
        [TestCategory("Part")]
        public void CancelCreatePartMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode partsNode = app.MainScreen.ProjectsTree.MasterDataParts;
                Assert.IsNotNull(partsNode, "The Parts node was not found.");
                partsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();

                ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Parts data grid was not found.");

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                //step 2
                Window createPartWindow = app.Windows.PartWindow;
                Button cancelButton = createPartWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3                
                addButton.Click();
                createPartWindow = app.Windows.PartWindow;

                //step 4
                TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "part" + DateTime.Now.Ticks;

                TextBox versionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description of part";

                // open Country pop up
                Button browseCountryButton = createPartWindow.Get<Button>(PartAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;
                browseMasterDataWindow.WaitForAsyncUITasks();

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");
                selectButton.Click();

                TextBox countryTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                ComboBox calculationAccuracyComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationAccuracyComboBox);
                calculationAccuracyComboBox.SelectItem(1);

                TabPage manufacturerTab = createPartWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox manufacturerTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
                manufacturerTextBox.Text = "description of manufacturer";

                cancelButton = createPartWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 5
                createPartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 6                
                cancelButton.Click();
                createPartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 8                
                addButton.Click();
                app.Windows.PartWindow.Close();

                Assert.IsTrue(Wait.For(() => app.Windows.PartWindow == null), "The Part window was not closed.");
            }
        }
    }
}
