﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyPartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyPart")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart1.project"));
            mainWindow.WaitForAsyncUITasks();
            
            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.SelectEx();
            myProjectsnode.Collapse();
 
            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importAnotherProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyPart")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the project -> assembly -> deletedPart to choose the copy option")]
        public void GivenISelectTheProject_Assembly_DeletedPartToChooseTheCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyPart2");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();
            partsNode.SelectEx();

            TreeNode part1Node = partsNode.GetNode("deletedPart");
            part1Node.SelectEx();

            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }

        [Given(@"I select again the part to choose delete option")]
        public void GivenISelectAgainThePartToChooseDeleteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyPart2");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.ExpandEx();

            TreeNode part1Node = partsNode.GetNode("deletedPart");
            part1Node.SelectEx();

            Menu deleteMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }

        [Given(@"I press the Yes button in message window")]
        public void GivenIPressTheYesButtonInMessageWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I select the Parts node select the Paste menu item which is disabled")]
        public void GivenISelectThePartsNodeSelectThePasteMenuItemWhichIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyPart2");
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.SelectEx();

            Menu pasteMenuItem = partsNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }

        [Given(@"I Go to Options -> Preferences to change the Central db server name")]
        public void GivenIGoToOptions_PreferencesToChangeTheCentralDbServerName()
        {
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = Wait.For(() => application.Windows.Preferences);

            TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);
            databaseTab.Click();

            TextBox centralDbServerName = preferencesWindow.Get<TextBox>("CentralDbServerName");
            centralDbServerName.Text = "123";

            Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();

            mainWindow.Close();
            Assert.IsTrue(Wait.For(() => application.HasExited), "Failed to close the application.");
        }

        [Given(@"I press the Restart button")]
        public void GivenIPressTheRestartButton()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [Given(@"I select an part to choose Copy To Master Data option")]
        public void GivenISelectAnPartToChooseCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("partToBeCopied1");
            part1Node.SelectEx();

            Menu copyToMasterDataMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
        }

        [Given(@"I click the Yes button")]
        public void GivenIClickTheYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

       
        [When(@"I select the project -> assembly node to select the Paste menu but it is disabled")]
        public void WhenISelectTheProject_AssemblyNodeToSelectThePasteMenuButItIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyPart2");
            assemblyNode.SelectEx();

            Menu pasteMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }

        [When(@"I press Ok")]
        public void WhenIPressOk()
        {
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
        }

        [Then(@"I select the project node to select the Paste menu but it is disabled")]
        public void ThenISelectTheProjectNodeToSelectThePasteMenuButItIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }

        [Then(@"the error message is closed")]
        public void ThenTheErrorMessageIsClosed()
        {
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = Wait.For(() => application.Windows.Preferences);

            TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);
            databaseTab.Click();

            TextBox centralDbServerName = preferencesWindow.Get<TextBox>("CentralDbServerName");
            centralDbServerName.Text = @"dorinp-pc";

            Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
    }
}
