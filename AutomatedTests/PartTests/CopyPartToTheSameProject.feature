﻿@CopyPartToTheSameProject
Feature: CopyPartToTheSameProject

Scenario: Copy Part To The Same Project
	Given I select the first part node to choose copy option
	And I select the same project for choosing the paste option
	And I select the copied part to Copy it
	When I select the same node to choose paste
	Then the copy of the part is copied 
