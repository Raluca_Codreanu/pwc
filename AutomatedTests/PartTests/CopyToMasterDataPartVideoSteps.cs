﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.InputDevices;
using White.Core.UIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyToMasterDataPartVideoSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
       
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectPartVideo";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CopyToMasterDataPartVideo")]
        private static void LaunchApplication()
        {
            CreateTestData();

            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectPartVideo");
            projectPartVideoNode.SelectEx();

            Menu importProjectMenuItem = projectPartVideoNode.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.ImportPart);
            importProjectMenuItem.Click();
            
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\partWithVideo.part"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyToMasterDataPartVideo")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a part to choose Copy to Master Data option")]
        public void GivenISelectAPartToChooseCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectPartVideo");
            projectPartVideoNode.ExpandEx();
            projectPartVideoNode.SelectEx();

            TreeNode partWithVideoNode = projectPartVideoNode.GetNode("partWithVideo");
            partWithVideoNode.SelectEx();

            Menu copyToMasterData = partWithVideoNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterData.Click();
        }
        
        [Given(@"I press the No in the confirmation window")]
        public void GivenIPressTheNoInTheConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I select the part to choose Copy To Master Data option")]
        public void GivenISelectThePartToChooseCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectPartVideo");
            projectPartVideoNode.ExpandEx();
            projectPartVideoNode.SelectEx();

            TreeNode partWithVideoNode = projectPartVideoNode.GetNode("partWithVideo");
            partWithVideoNode.SelectEx();

            Menu copyToMasterData = partWithVideoNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterData.Click();    
        }
        
        [Given(@"I press the Yes button in confirmation scren")]
        public void GivenIPressTheYesButtonInConfirmationScren()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the part is copied to master data")]
        public void ThenThePartIsCopiedToMasterData()
        {
            TreeNode partsNode = application.MainScreen.ProjectsTree.MasterDataParts;
            partsNode.Click();
            mainWindow.WaitForAsyncUITasks();
            ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);

            partsDataGrid.Select("Name", "partWithVideo");

            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);         
            editButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"videos are not copied")]
        public void ThenVideosAreNotCopied()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsFalse(deleteMediaButton.Enabled, "Media panel should be empty");
        }
    }
}
