﻿@CutPart
Feature: CutPart

Scenario: Cut Part Paste In Assembly
	Given I select a part node to choose Cut option
	And I select an assembly node to choose Paste option
	And I select the part from assembly an choose Cut option
	When I select the same assembly node and choose Paste option
	Then the Paste is disabled
