﻿@DeletePart
Feature: DeletePart

Scenario: Delete Part
	Given I select a part to delete it
	And I press No in confirmations creen
	And I press again delete
	And I press the Yes button in the confirmation win
	And I select the Trash bin node
	And I select the deleted part
	And I press View Details node
	And I press Recover button
	And I press No in confirmation win
	And I press Recover
	And I select Yes from confirmation screen
	And I select the part and delete it
	And I Press yes
	And I select the Trash bin node
	And I checked the deleted part
	And I press the Permanently Delete button
	And I press the No buton in confirmation window
	And I click Permanently Delete button 
	When I press Yes in confirmation screen
	Then the part is deleted from trash bin
