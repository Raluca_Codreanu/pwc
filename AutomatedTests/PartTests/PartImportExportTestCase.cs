﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using System.IO;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.PartTests
{
    /// <summary>
    /// The automated test for Import Export
    /// </summary>
    [TestClass]
    public class PartImportExportTestCase
    {
        /// <summary>
        /// The path where part1 is exported during the test.
        /// </summary>
        private static string partExportPath;

        public PartImportExportTestCase()
        {
        }

        #region Additional test attributes

        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            PartImportExportTestCase.partExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".part");
            if (File.Exists(PartImportExportTestCase.partExportPath))
            {
                File.Delete(PartImportExportTestCase.partExportPath);
            }
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void MyClassCleanup()
        {
            if (File.Exists(PartImportExportTestCase.partExportPath))
            {
                File.Delete(PartImportExportTestCase.partExportPath);
            }
        }

        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Import Export Part Test Case.
        /// </summary>
        [TestMethod]
        [TestCategory("Part")]
        public void ImportExportPartTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // ensure that import summary screen is hidden
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);

                CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                displayImportSummaryScreen.Checked = true;

                Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(savePreferencesButton, "The Save button was not found.");

                savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePreferencesButton.Click();
                Wait.For(() => app.Windows.Preferences == null);

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
                importProjectMenuItem.Click();

                mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
                mainWindow.WaitForAsyncUITasks();

                TreeNode project1Node = myProjectsNode.GetNode("myProject");
                Assert.IsNotNull(project1Node, "myProject node was not found");
                project1Node.SelectEx();
                project1Node.ExpandEx();

                TreeNode part1Node = project1Node.GetNode("part1");
                part1Node.SelectEx();

                // step 2                
                Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
                projectMenu.Click();
                var exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
                exportMenu.Click();

                //step 3    
                mainWindow.GetSaveFileDialog().Cancel();

                //step 4
                projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
                projectMenu.Click();
                exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
                exportMenu.Click();

                mainWindow.GetSaveFileDialog().SaveFile(PartImportExportTestCase.partExportPath);
                mainWindow.WaitForAsyncUITasks();
                mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
                Assert.IsTrue(File.Exists(PartImportExportTestCase.partExportPath), "The part was not exported at the specified location.");

                TreeNode projectNode = myProjectsNode.GetNode("myProject");
                projectNode.SelectEx();
                projectNode.Collapse();

                Menu importPartMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.ImportPart);
                Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
                importPartMenuItem.Click();

                mainWindow.GetOpenFileDialog().OpenFile(PartImportExportTestCase.partExportPath);
                mainWindow.WaitForAsyncUITasks();

                var importedPartNode = projectNode.GetNode("part1");
                Assert.IsNotNull(importedPartNode, "The imported part did not appear in projects tree.");
            }
        }

        #region Helper
        #endregion Helper
    }
}
