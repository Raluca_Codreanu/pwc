﻿@CopyToMasterDataPartVideo
Feature: CopyToMasterDataPartVideo

Scenario: Copy To Master Data Part With Video
	Given I select a part to choose Copy to Master Data option 
	And I press the No in the confirmation window
	And I select the part to choose Copy To Master Data option
	And I press the Yes button in confirmation scren
	Then the part is copied to master data
	And videos are not copied
