﻿@CopyToMasterDataPartwithPicturesAndSubent
Feature: CopyToMasterDataPartwithPicturesAndSubent

Scenario: Copy to Master Data Part with Pictures and Subentities
	Given I select a part to choose the Copy to Master Data option 
	And I press the No button in the confirmation window
	And I select the part to choose the Copy To Master Data option
	And I press the Yes button in the confirmation scren
	Then the part is copied to the master data
	And pictures are copied
	And commodity is copied

