﻿@CopyPart
Feature: copyPart

Scenario: Copy Deleted Part
	Given I select the project -> assembly -> deletedPart to choose the copy option
	And I select again the part to choose delete option
	And I press the Yes button in message window
	And I select the Parts node select the Paste menu item which is disabled
	When I select the project -> assembly node to select the Paste menu but it is disabled 
	Then I select the project node to select the Paste menu but it is disabled 


Scenario: Copy Part To Master - Central Server Off
	Given I Go to Options -> Preferences to change the Central db server name
	And I press the Restart button
	And I select an part to choose Copy To Master Data option
	And I click the Yes button 
	When I press Ok
	Then the error message is closed