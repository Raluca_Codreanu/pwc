﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.AssemblyTests;
using System.IO;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class UpdatePartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Part part1 = new Part();
            part1.Name = "updatedPartMD";
            part1.SetOwner(adminUser);
            part1.IsMasterData = true;
            part1.ManufacturingCountry = "Austria"; 
            CountrySetting country = new CountrySetting();
            part1.CountrySettings = country;

            part1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            part1.Manufacturer = manufacturer1;

            dataContext.PartRepository.Add(part1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("UpdatePart")]
        private static void LaunchApplication()
        {
            CreateTestData();

            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectUpdatePart.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("UpdatePart")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I change some fields: Name, Lifetime")]
        public void GivenIChangeSomeFieldsNameLifetime()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode project1Node = myProjectsNode.GetNode("projectUpdatePart");
            project1Node.ExpandEx();

            TreeNode partNode = project1Node.GetNode("partUpdate");
            partNode.SelectEx();

            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTexBox = mainWindow.Get<TextBox>("NameTextBox");
            nameTexBox.Text = "part_Updated_1";

            TextBox lifetime = mainWindow.Get<TextBox>("LifeTimeTextBox");
            lifetime.Text = "12";
        }

        [Given(@"I change some fields: Name, LifetimeMD")]
        public void GivenIChangeSomeFieldsNameLifetimeMD()
        {
            //TreeNode partsNode = application.MainScreen.ProjectsTree.MasterDataParts;
            //Assert.IsNotNull(partsNode, "The Manufacturers node was not found.");
            //partsNode.SelectEx();
            //mainWindow.WaitForAsyncUITasks();
            //ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            //Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");
            //partsDataGrid.Select("Name", "updatedPartMD");

            //TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            //myProjectsNode.ExpandEx();

            //TreeNode project1Node = myProjectsNode.GetNode("projectUpdatePart");
            //project1Node.ExpandEx();

            //TreeNode partNode = project1Node.GetNode("updatedPartMD");
            //partNode.SelectEx();

            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTexBox = mainWindow.Get<TextBox>("NameTextBox");
            nameTexBox.Text = "part_Updated_1";

            TextBox lifetime = mainWindow.Get<TextBox>("LifeTimeTextBox");
            lifetime.Text = "12";
        }

        [Given(@"I change some fields from Manufacturer tab")]
        public void GivenIChangeSomeFieldsFromManufacturerTab()
        {
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "Name text box was not found.");
            nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;
        }
        
        [Given(@"I add a document in Documents tab")]
        public void GivenIAddADocumentInDocumentsTab()
        {
            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();

            Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
            addDocumentsButton.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
        }
        
        [Given(@"I press the Cancel button without changing any fields")]
        public void GivenIPressTheCancelButtonWithoutChangingAnyFields()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I change some properies")]
        public void GivenIChangeSomeProperies()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTexBox = mainWindow.Get<TextBox>("NameTextBox");
            nameTexBox.Text = "part_Updated_1";
        }


        [Given(@"I press click on the Cancel button")]
        public void GivenIPressClickOnTheCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I press the No in confirmation window")]
        public void GivenIPressTheNoInConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I click the Cancel btn again")]
        public void GivenIClickTheCancelBtnAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();  
        }

        [Given(@"I close\(X\) the confirmation window")]
        public void GivenICloseXTheConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }

        [Given(@"I press the Yes btn")]
        public void GivenIPressTheYesBtn()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Given(@"I click the Home button")]
        public void GivenIClickTheHomeButton()
        {
            Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
            Assert.IsNotNull(homeButton, "The Home button was not found.");
            Assert.IsTrue(homeButton.Enabled, "The Home button is disabled.");

            homeButton.Click();  
        }

        [Given(@"I select a part form Parts node")]
        public void GivenISelectAPartFormPartsNode()
        {
            TreeNode partsNode = application.MainScreen.ProjectsTree.MasterDataParts;
            Assert.IsNotNull(partsNode, "The Manufacturers node was not found.");
            partsNode.SelectEx();
            mainWindow.WaitForAsyncUITasks();
            ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");
            partsDataGrid.Select("Name", "updatedPartMD");
        }

        [Given(@"I press click on the Edit button")]
        public void GivenIPressClickOnTheEditButton()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            Assert.IsNotNull(editButton, "The Edit button was not found.");
            editButton.Click();
        }

        [When(@"I click the Save button")]
        public void WhenIClickTheSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }

        [When(@"I press No")]
        public void WhenIPressNo()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Then(@"the chnages are saved")]
        public void ThenTheChnagesAreSaved()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode project1Node = myProjectsNode.GetNode("projectUpdatePart");
            project1Node.ExpandEx();

            TreeNode partNode = project1Node.GetNode("part_Updated_1");
            partNode.SelectEx();    
        }
        
        [Then(@"the user is log out")]
        public void ThenTheUserIsLogOut()
        {
            var welcomeScreen = Wait.For(() => application.WelcomeScreen);
            Assert.IsNotNull(welcomeScreen, "Log out");
            Button continueButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
            continueButton.Click();
        }
    }
}
