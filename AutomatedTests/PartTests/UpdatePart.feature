﻿@UpdatePart
Feature: UpdatePart

Scenario: Update Part
	Given I change some fields: Name, Lifetime
	And I change some fields from Manufacturer tab
	And I add a document in Documents tab
	When I click the Save button
	Then the chnages are saved

Scenario: Cancel Update Part
	Given I press the Cancel button without changing any fields
	And I change some properies
	And I press click on the Cancel button 
	And I press the No in confirmation window
	And I click the Cancel btn again
	And I close(X) the confirmation window
	And I press click on the Cancel button
	And I press the Yes btn
	And I change some fields: Name, Lifetime
	And I click the Home button
	When I press No
	Then the user is log out 

Scenario: Cancel Update Part from Master Data
	Given I select a part form Parts node 
	And I press click on the Edit button
	And I press the Cancel button without changing any fields
	And I change some properies
	And I press click on the Cancel button 
	And I press the No in confirmation window
	And I click the Cancel btn again
	And I close(X) the confirmation window
	And I press click on the Cancel button
	And I press the Yes btn
	And I change some fields: Name, LifetimeMD
	And I click the Home button
	When I press No
	Then the user is log out 