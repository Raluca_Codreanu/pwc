﻿@CreateParts
Feature: CreateParts
	In order to test the create part functionality	
	I want to be able to add part with different kids of Accuracy: Estimation, Rough Calculation and Offer




@CreatePartsScenario

Scenario: Create Parts
	Given I select projectPartsTest node and choose the Create Part option
	And I fill all the mandatory fields from General tab
	And I select the accuracy type
	| accuracyType |
	| 0            |
	| 1            |
	| 2            |
	| 3            |
	And I fill Estimated Cost field and the Weight field
	And I add one video file
	And I fill all the mandatory fields from Manufacturer tab
	When I click on the save button
	Then the part with the seleced Accuracy 
