﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using System.Threading;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.PartTests
{
    public class PartTestsHelper
    {
        public static void CreatePart(ApplicationEx app, Window createPartWindow)
        {            
            TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "partProject";

            TextBox numberTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.NumberTextBox);
            numberTextBox.Text = "123";

            TextBox versionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";

            TextBox descriptionTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "description of part";

            //step 6
            // open Country pop up
            Button browseCountryButton = createPartWindow.Get<Button>(PartAutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            Window mainWindow = app.Windows.MainWindow;
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Belgium");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");
            selectButton.Click();

            TextBox countryTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
            Assert.AreEqual("Belgium", countryTextBox.Text);

            ComboBox calculationStatusComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationStatusComboBox);
            Assert.IsNotNull(calculationStatusComboBox, "CAlculation combo box can not be found");
            calculationStatusComboBox.SelectItem(1);

            //step 7
            ComboBox calculationAccuracyComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationAccuracyComboBox);
            calculationAccuracyComboBox.SelectItem(1);

            ComboBox calculationApproachComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationApproachComboBox);
            calculationApproachComboBox.SelectItem(1);

            ComboBox calculationVariantComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculationVariantComboBox);
            calculationVariantComboBox.SelectItem(5);

            ComboBox calculatorComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.CalculatorComboBox);
            calculatorComboBox.SelectItem(0);

            TextBox yearlyProdQtyTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.YearlyProdQtyTextBox);
            yearlyProdQtyTextBox.Text = "10";

            TextBox lifeTimeTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.LifeTimeTextBox);
            lifeTimeTextBox.Text = "11";

            //step 8
            for (int i = 0; i < 10; i++)
            {
                var mediaControl = createPartWindow.GetMediaControl();
                mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);
                Thread.Sleep(250);
            }

            //step 9
            TabPage manufacturerTab = createPartWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            TextBox manufacturerTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
            manufacturerTextBox.Text = "description of manufacturer";

            TabPage settingsTab = createPartWindow.Get<TabPage>(AutomationIds.SettingsTab);
            settingsTab.Click();

            TextBox purchasePriceTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.PurchasePriceTextBox);
            purchasePriceTextBox.Text = "123";

            TextBox targetPriceTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.TargetPriceTextBox);
            targetPriceTextBox.Text = "456";

            ComboBox deliveryTypeComboBox = createPartWindow.Get<ComboBox>(PartAutomationIds.DeliveryTypeComboBox);
            deliveryTypeComboBox.Click();
            deliveryTypeComboBox.Items.Item(1).Select();

            TextBox assetRateTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.AssetRateTextBox);
            assetRateTextBox.Text = "5";

            Button saveButton = createPartWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }

        public static void CreateSimplePart(ApplicationEx app, Window createPartWindow)
        {
            TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "simplePartProject";

            var mainWindow = app.Windows.MainWindow;

            // open Country pop up
            Button browseCountryButton = createPartWindow.Get<Button>(PartAutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();

            Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Canada");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");
            selectButton.Click();

            TextBox countryTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
            Assert.AreEqual("Canada", countryTextBox.Text);

            TabPage manufacturerTab = createPartWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerPartTest";

            TextBox manufacturerTextBox = createPartWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
            manufacturerTextBox.Text = "description of manufacturer";

            Button saveButton = createPartWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
    }
}
