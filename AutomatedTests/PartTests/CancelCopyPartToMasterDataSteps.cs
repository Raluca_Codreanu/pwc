﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CancelCopyPartToMasterDataSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CancelCopyPartToMasterData")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart1.project"));
            mainWindow.WaitForAsyncUITasks();

        }

        [AfterFeature("CancelCopyPartToMasterData")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a part to Choose Copy To Master Data")]
        public void GivenISelectAPartToChooseCopyToMasterData()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode part1Node = projectNode.GetNode("partToBeCopied1");
            part1Node.SelectEx();

            Menu copyToMasterDataMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
        }

        [Given(@"I click No Button")]
        public void GivenIClickNoButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I select again the part")]
        public void ISelectAgainThePart()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("partToBeCopied1");
            part1Node.SelectEx();
            part1Node.ExpandEx();
            part1Node.Collapse();
            part1Node.SelectEx();
            Menu copyToMasterDataMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
        }

        [When(@"I close the confirmation Window")]
        public void WhenICloseTheConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }

        [Then(@"the part is not copied to MasterData")]
        public void ThenThePartIsNotCopiedToMasterData()
        {
            TreeNode partNode = application.MainScreen.ProjectsTree.MasterDataParts;
            Assert.IsNotNull(partNode, "The Parts node was not found.");
            partNode.Click();
            ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");

            mainWindow.WaitForAsyncUITasks();
            Assert.IsNull(partsDataGrid.Row("Name", "partToBeCopied1"), "The part shouldn't be copied");
        }
    }
}
