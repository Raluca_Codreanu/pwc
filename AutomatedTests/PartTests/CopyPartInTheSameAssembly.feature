﻿@CopyPartInTheSameAssembly
Feature: CopyPartInTheSameAssembly
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Scenario: Copy Part In The Same Assembly
	Given I select project -> assembly -> part node to choose copy option
	And I select the same Assembly to choose Paste option
	And I select the copied Part from assembly to choose Copy option
	When I select the same Assembly node to choose Paste option
	Then a copy of Part is available in Assembly node
