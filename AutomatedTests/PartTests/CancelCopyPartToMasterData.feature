﻿@CancelCopyPartToMasterData
Feature: CancelCopyPartToMasterData

Scenario: Cancel Copy Part To Master Data
	Given I select a part to Choose Copy To Master Data
	And I click No Button
	And I select again the part
	When I close the confirmation Window
	Then the part is not copied to MasterData
