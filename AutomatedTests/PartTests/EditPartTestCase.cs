﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.MenuItems;
using System.Threading;
using System.IO;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.PartTests
{
    [TestClass]
    public class EditPartTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditPartTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        [TestMethod]
        [TestCategory("Part")]
        public void EditPartMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode partsNode = app.MainScreen.ProjectsTree.MasterDataParts;
                Assert.IsNotNull(partsNode, "The Manufacturers node was not found.");
                partsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");
                partsDataGrid.Select("Name", "part1");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");
                editButton.Click();

                mainWindow.WaitForAsyncUITasks();
                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();
                mainWindow.WaitForAsyncUITasks();
                //step 1
                TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "part_Updated";

                TextBox versionTextBox = mainWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = mainWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description of part";

                //step 2      
                // open Country pop up
                Button browseCountryButton = mainWindow.Get<Button>(PartAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Austria");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = mainWindow.Get<TextBox>(PartAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                //step 4
                ComboBox calculationStatusComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.CalculationStatusComboBox);
                Assert.IsNotNull(calculationStatusComboBox, "calculationStatusComboBox can not be found");
                calculationStatusComboBox.SelectItem(1);

                ComboBox calculationAccuracyComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.CalculationAccuracyComboBox);
                Assert.IsNotNull(calculationAccuracyComboBox, "calculationAccuracy combo box was not found.");
                calculationAccuracyComboBox.SelectItem(1);

                //step 5
                //TextBox estimatedCostTextBox = mainWindow.Get<TextBox>(CreatePartTestCase.UIItemsIdentifiers.EstimatedCostTextBox);
                //estimatedCostTextBox.Text = "100";
                //Thread.Sleep(600);
                //TextBox manufacturingRatioTextBox = mainWindow.Get<TextBox>(CreatePartTestCase.UIItemsIdentifiers.ManufacturingRatioTextBox);
                //manufacturingRatioTextBox.Text = "10";

                ComboBox calculationApproachComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.CalculationApproachComboBox);
                calculationApproachComboBox.SelectItem(1);
                ComboBox calculationVariantComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.CalculationVariantComboBox);
                calculationVariantComboBox.SelectItem(4);

                ComboBox calculatorComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.CalculatorComboBox);
                calculatorComboBox.SelectItem(0);

                TextBox yearlyProdQtyTextBox = mainWindow.Get<TextBox>(PartAutomationIds.YearlyProdQtyTextBox);
                yearlyProdQtyTextBox.Text = "10";

                TextBox lifeTimeTextBox = mainWindow.Get<TextBox>(PartAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "11";

                //step 3
                mainWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);

                //step 6
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox manufacturerTextBox = mainWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
                manufacturerTextBox.Text = "description of manufacturer";

                //step 7
                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);                
                settingsTab.Click();

                TextBox purchasePriceTextBox = mainWindow.Get<TextBox>(PartAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = "123";

                TextBox targetPriceTextBox = mainWindow.Get<TextBox>(PartAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = "456";

                ComboBox deliveryTypeComboBox = mainWindow.Get<ComboBox>(PartAutomationIds.DeliveryTypeComboBox);
                deliveryTypeComboBox.SelectItem(1);

                TextBox assetRateTextBox = mainWindow.Get<TextBox>(PartAutomationIds.AssetRateTextBox);
                assetRateTextBox.Text = "5";

                //step 9
                TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);                
                documentsTab.Click();

                //step 10
                Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                for (int i = 0; i <= 10; i++)
                {                    
                    addDocumentsButton.Click();
                    mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
                }

                mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                //step 11
                TabPage overheadSettingsTab = mainWindow.Get<TabPage>(AutomationIds.OHSettingsTab);                
                overheadSettingsTab.Click();

                OverheadSetting updatedSettings = SettingsTests.EditOverheadSettingsTestCase.CreateOverheadSettings();
                SettingsTests.EditOverheadSettingsTestCase.FillOverheadSettingsFieldsAndAssert(mainWindow, updatedSettings);

                //step 13
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Part part1 = new Part();
            part1.Name = "part1";
            part1.SetOwner(adminUser);
            part1.IsMasterData = true;
            part1.ManufacturingCountry = "Austria";
            CountrySetting country = new CountrySetting();
            part1.CountrySettings = country;

            part1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            part1.Manufacturer = manufacturer1;

            dataContext.PartRepository.Add(part1);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}

