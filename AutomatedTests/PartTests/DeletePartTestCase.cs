﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.PartTests
{
    [TestClass]
    public class DeletePartTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeletePartTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        [TestMethod]
        [TestCategory("Part")]
        public void DeletePartMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode partsNode = app.MainScreen.ProjectsTree.MasterDataParts;
                Assert.IsNotNull(partsNode, "The Manufacturers node was not found.");

                //step 1
                partsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");
                partsDataGrid.Select("Name", "part1");

                //step 2
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                // step 3
                mainWindow.GetMessageDialog().ClickNo();

                deleteButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                deleteButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                mainWindow.WaitForAsyncUITasks();

                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");

                trashBinNode.Select();
                trashBinNode.Click();
                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "die1"), "A supplier from Master Data was deleted to the Trash Bin.");
                                                
                partsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Manage Master Data grid was not found.");

                ListViewRow part2row = partsDataGrid.Row("Name", "part2");
                Assert.IsNotNull(part2row, "Part2row trash bin item was not found.");

                //step 7
                part2row.DoubleClick();
                
                TreeNode partNode2 = partsNode.GetNode("part2");
                partNode2.SelectEx();

                //step 8
                partNode2.RightClickAt(partNode2.ClickablePoint);
                Menu deleteMenuItem = partNode2.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                mainWindow.GetMessageDialog().ClickYes();
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Part part1 = new Part();
            part1.Name = "part1";
            part1.SetOwner(adminUser);
            part1.IsMasterData = true;
            part1.ManufacturingCountry = "Austria";
            CountrySetting country = new CountrySetting();
            part1.CountrySettings = country;

            part1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            part1.Manufacturer = manufacturer1;

            dataContext.PartRepository.Add(part1);
            dataContext.SaveChanges();

            Part part2 = new Part();
            part2.Name = "part2";
            part2.SetOwner(adminUser);
            part2.IsMasterData = true;
            part2.ManufacturingCountry = "Austria";
            CountrySetting country2 = new CountrySetting();
            part2.CountrySettings = country2;

            part2.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer2 = new Manufacturer();
            manufacturer2.Name = "Manufacturer" + manufacturer2.Guid.ToString();
            part2.Manufacturer = manufacturer2;

            dataContext.PartRepository.Add(part2);
            dataContext.SaveChanges();
        }

        #endregion Helper

        #region Inner classes

        #endregion Inner classes
    }
}

