﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyPartToAnotherProjectSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyPartToAnotherProject")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart1.project"));
            mainWindow.WaitForAsyncUITasks();

            myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.Collapse();

            importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\copyPart2.project"));
            mainWindow.WaitForAsyncUITasks();

        }

        [AfterFeature("CopyPartToAnotherProject")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"I select the part node to choose copy option")]
        public void GivenISelectThePartNodeToChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart1");
            projectNode.ExpandEx();

            TreeNode part1Node = projectNode.GetNode("partToBeCopied1");
            part1Node.SelectEx();

            Menu copyMenuItem = part1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [When(@"I made the selection of the second project to choose the paste option")]
        public void WhenIMadeTheSelectionOfTheSecondProjectToChooseThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Then(@"the part is copied in the second project")]
        public void ThenThePartIsCopiedInTheSecondProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyPart2");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partToBeCopied1");
            partNode.SelectEx();
        }
    }
}
