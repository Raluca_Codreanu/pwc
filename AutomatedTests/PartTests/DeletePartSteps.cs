﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.PartTests
{
    [Binding]
    public class DeletePartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeletePart")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
          
            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            
            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDeletePart.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeletePart")]
        private static void KillApplication()
        {
            application.Kill();
        }


        [Given(@"I select a part to delete it")]
        public void GivenISelectAPartToDeleteIt()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode project1Node = myProjectsNode.GetNode("projectDeletePart");
            project1Node.SelectEx();
            project1Node.ExpandEx();
            
            TreeNode partNode = project1Node.GetNode("deletePart");
            partNode.SelectEx();

            Menu deleteMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click(); 
        }
        
        [Given(@"I press No in confirmations creen")]
        public void GivenIPressNoInConfirmationsCreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press again delete")]
        public void GivenIPressAgainDelete()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode project1Node = myProjectsNode.GetNode("projectDeletePart");
            project1Node.ExpandEx();

            TreeNode partNode = project1Node.GetNode("deletePart");
            partNode.SelectEx();

            Menu deleteMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();         
        }
        
        [Given(@"I press the Yes button in the confirmation win")]
        public void GivenIPressTheYesButtonInTheConfirmationWin()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the Trash bin node")]
        public void GivenISelectTheTrashBinNode()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            trashBinNode.SelectEx();
        }
        
        [Given(@"I select the deleted part")]
        public void GivenISelectTheDeletedPart()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            ListViewRow partRow = trashBinDataGrid.Row("Name", "deletePart");

            ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, partRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;
        }
        
        [Given(@"I press View Details node")]
        public void GivenIPressViewDetailsNode()
        {
            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();
        }
        
        [Given(@"I press Recover button")]
        public void GivenIPressRecoverButton()
        {
            Button revoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            revoverButton.Click();
        }
        
        [Given(@"I press No in confirmation win")]
        public void GivenIPressNoInConfirmationWin()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press Recover")]
        public void GivenIPressRecover()
        {
            Button revoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            revoverButton.Click();   
        }
        
        [Given(@"I select Yes from confirmation screen")]
        public void GivenISelectYesFromConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the part and delete it")]
        public void GivenISelectThePartAndDeleteIt()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode project1Node = Wait.For(() => myProjectsNode.GetNode("projectDeletePart"));
            project1Node.ExpandEx();

            TreeNode partNode = project1Node.GetNode("deletePart");
            partNode.SelectEx();

            Menu deleteMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();        
        }
        
        [Given(@"I Press yes")]
        public void GivenIPressYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I checked the deleted part")]
        public void GivenICheckedTheDeletedPart()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;            
            trashBinNode.SelectEx();
       
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            ListViewRow partRow = trashBinDataGrid.Row("Name", "deletePart");

            ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, partRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;    
        }
        
        [Given(@"I press the Permanently Delete button")]
        public void GivenIPressThePermanentlyDeleteButton()
        {
            Button permanentlyDeteleBtn = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
            permanentlyDeteleBtn.Click();
        }
        
        [Given(@"I press the No buton in confirmation window")]
        public void GivenIPressTheNoButonInConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I click Permanently Delete button")]
        public void GivenIClickPermanentlyDeleteButton()
        {
            Button permanentlyDeteleBtn = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
            permanentlyDeteleBtn.Click();
        }
        
        [When(@"I press Yes in confirmation screen")]
        public void WhenIPressYesInConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the part is deleted from trash bin")]
        public void ThenThePartIsDeletedFromTrashBin()
        {
            var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);           
            Assert.IsNull(trashBinDataGrid.Row("Name", "deletePart"), "deletePart item was found.");  
        }
    }
}
