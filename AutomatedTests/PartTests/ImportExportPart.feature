﻿@ImportExportPart
Feature: ImportExportPart

Scenario: Import Export Part
	Given I select the Part node to choose Export option
	And I cancel the save as window
	And I press again the Export Option
	And I press the Save Button
	When I select the project node to import the exported part	
	Then the part is imported in the project
