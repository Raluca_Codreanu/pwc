﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.MachineTests
{
    /// <summary>
    /// The automated test for Delete Machine Master Data (test link id = 60)
    /// </summary>
    [TestClass]
    public class DeleteMachineMDTesteCase
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="DeleteMachineMDTesteCase"/> class.
        ///// </summary>
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteMachineMDTesteCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Machine Data Test Case.
        /// </summary>
        [TestMethod]
        [TestCategory("Machines")]
        public void DeleteMachineMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Machines node was not found.");

                machinesNode.Click();
                ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");

                //step 4
                mainWindow.WaitForAsyncUITasks();
                machinesDataGrid.Select("Name", "10Machine");
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 5
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                

                //step 6
                deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 7                
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                //step 8 
                mainWindow.WaitForAsyncUITasks();

                //step 10      
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "11raw"), "A raw Material from Master Data was deleted permanently and it doesn't appear on the Trash Bin.");
            }
        }

        #region Helper

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Machine machine = new Machine();
            machine.Name = "10Machine";
            machine.SetOwner(adminUser);
            machine.IsMasterData = true;

            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();
        }
        #endregion Helper

        #region Inner classes

        #endregion Inner classes
    }
}
