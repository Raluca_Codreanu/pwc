﻿@UpdateMachine
Feature: UpdateMachine

Scenario: Edit Machine in Process step
	Given I select the Machine tab
	And I select a row from Machine data grid and press the Edit button
	And I update fields
	When I press the Save bttn
	Then the edit Machine window is closed

Scenario: Cancel Edit Machine_no changes
	Given I select the Machine tab
	And I select another row from Machine data grid and press the Edit button
	When I press the Cancel btn
	Then the edit Machine window is closed

Scenario: Cancel Update Machine
	Given I select the Machine tab
	And I select another row from Machine data grid and press the Edit button
	And I update fields
	And I press the cancel btn
	And I press the No button in question win message
	And I press cancel btn
	When I press the yes button in question win message
	Then the edit Machine window is closed	

Scenario: Edit Machine using dbl click
	Given I select the Machine tab
	And I select a row from data grid and press double-click
	And I update fields
	When I press the Save bttn
	Then the edit Machine window is closed	

Scenario: Cancel Edit Machine MD_no changes
	Given I select the MD, Machines tab
	And I select a row from MD -> Machine data grid and press the Edit button
	When I press the Cancel btn
	Then the edit Machine window is closed

Scenario: Cancel Update Machine MD
	Given I select the MD, Machines tab
	And I select a row from MD -> Machine data grid and press the Edit button
	And I update some of the fields
	And I press the cancel btn
	And I press the No button in question win message
	And I press cancel btn
	When I press the yes button in question win message
	Then the edit Machine window is closed	