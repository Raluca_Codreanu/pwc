﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ConsumableTests;

namespace ZPKTool.AutomatedTests.MachineTests
{
    [Binding]
    public class CreateMachineSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CreateMachine")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CreateMachine")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Machine tab from process step")]
        public void GivenISelectTheMachineTabFromProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();
        }
        
        [Given(@"I press the Add")]
        public void GivenIPressTheAdd()
        {
            Button addMachineButton = mainWindow.Get<Button>(MachineAutomationIds.AddMachineButton, true);
            addMachineButton.Click();  
        }
        
        [Given(@"I fill mandatory fields")]
        public void GivenIFillMandatoryFields()
        {
            Window createMachinesWindow = application.Windows.MachineWindow;
            createMachinesWindow.WaitForAsyncUITasks();

            TextBox name = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "1machine";

            TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            depreciationPeriodTextBox.Text = "10";

            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            Button saveButton = createMachinesWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [Given(@"I fill some of the Create Machine window's fields")]
        public void GivenIFillSomeOfTheCreateMachineWindowSFields()
        {
            Window createMachinesWindow = application.Windows.MachineWindow;
            createMachinesWindow.WaitForAsyncUITasks();

            TextBox name = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "1machine";

            TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            depreciationPeriodTextBox.Text = "10";

            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";
        }
        
        [Given(@"I press Cancel bttn")]
        public void GivenIPressCancelBttn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click(); 
        }
        
        [Given(@"I press No button in Question window")]
        public void GivenIPressNoButtonInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();    
        }
        
        [Given(@"I press the Cancel bttn")]
        public void GivenIPressTheCancelBttn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();   
        }
        
        [When(@"I press Save btn")]
        public void WhenIPressSaveBtn()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [When(@"I press Cancel")]
        public void WhenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();
        }
        
        [When(@"I press the Yes btn in Question window")]
        public void WhenIPressTheYesBtnInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the Machine data grid contains a record")]
        public void ThenTheMachineDataGridContainsARecord()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            Assert.IsTrue(machinesDataGrid.Rows.Capacity >= 2, "Test");      
        }
        
        [Then(@"Create Machine window is closed")]
        public void ThenCreateMachineWindowIsClosed()
        {
            Window createMachineWindow = application.Windows.MachineWindow;
            Assert.IsNull(createMachineWindow, "Create machine window should be closed");
        }
    }
}
