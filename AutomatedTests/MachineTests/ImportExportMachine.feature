﻿@ImportExportMachine
Feature: ImportExportMachine

Scenario: Import Export Machine
	Given I open the Machine tab from Process step
	And I select a Machine, press right click to export it
	And I press the Cancel in Save As window
	And I select a Machine, press right click to export it
	And I press the Save btn
	When I select the step node to import Machine
	Then the Machine is imported into the Machine datagrid
