﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.MachineTests
{
    [Binding]
    public class DeleteMachineProcessSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteMachine")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteMachine")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Machine tab")]
        public void GivenIOpenTheMachineTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();
        }
        
        [Given(@"I select a Machine")]
        public void GivenISelectAMachine()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machineDelete2");
        }
        
        [Given(@"I press the Delete button from machine grid")]
        public void GivenIPressTheDeleteButtonFromMachineGrid()
        {
            Button deleteButton = mainWindow.Get<Button>(MachineAutomationIds.DeleteButton);
            deleteButton.Click();  
        }
        
        [Given(@"I press the No btn in question")]
        public void GivenIPressTheNoBtnInQuestion()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press the Yes in question")]
        public void GivenIPressTheYesInQuestion()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I open the Trash Bin screen")]
        public void GivenIOpenTheTrashBinScreen()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            trashBinNode.Click();
        }
        
        [Given(@"I select the deleted row from data grid")]
        public void GivenISelectTheDeletedRowFromDataGrid()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

            ListViewRow deletedRow = trashBinDataGrid.Row("Name", "machineDelete2");

            ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, deletedRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;
        }
        
        [Given(@"I press View Details button")]
        public void GivenIPressViewDetailsButton()
        {
            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press Recover Button")]
        public void GivenIPressRecoverButton()
        {
            Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            Assert.IsNotNull(recoverButton, "Recover button was not found.");
            recoverButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish
        }
        
        [Given(@"I press Yes Button")]
        public void GivenIPressYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks(); 

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I select a Machine, press right click to delete it")]
        public void GivenISelectAMachinePressRightClickToDeleteIt()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machineDelete1");

            ListViewRow row = machinesDataGrid.Row("Name", "machineDelete1");

            Menu deleteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }
        
        [Given(@"I press the Empty Trash Bin btn")]
        public void GivenIPressTheEmptyTrashBinBtn()
        {
            Button emptyTrashBin = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
            emptyTrashBin.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I opened the Machine tab")]
        public void WhenIOpenedTheMachineTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();
        }
        
        [When(@"I press Yes button")]
        public void WhenIPressYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the data grid contains the deleted Machine")]
        public void ThenTheDataGridContainsTheDeletedMachine()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            ListViewRow row = machinesDataGrid.Row("Name", "machineDelete2");

            Assert.IsNotNull(row, "row exists");
        }
        
        [Then(@"the Machine is deleted from trash bin")]
        public void ThenTheMachineIsDeletedFromTrashBin()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

            Assert.IsNull(trashBinDataGrid.Row("Name", "machineDelete2"), "empty trash");
        }
    }
}
