﻿@CreateMachine
Feature: CreateMachine

Scenario: Create Machine in Process Step
	Given I select the Machine tab from process step
	And I press the Add
	And I fill mandatory fields
	When I press Save btn
	Then the Machine data grid contains a record

Scenario: Cancel Create Machine_no changes
	Given I select the Machine tab from process step 
	And I press the Add
	When I press Cancel
	Then Create Machine window is closed

Scenario: Cancel Create Machine 
	Given I select the Machine tab from process step
	And I press the Add
	And I fill some of the Create Machine window's fields
	And I press Cancel bttn
	And I press No button in Question window
	And I press the Cancel bttn
	When I press the Yes btn in Question window
	Then Create Machine window is closed