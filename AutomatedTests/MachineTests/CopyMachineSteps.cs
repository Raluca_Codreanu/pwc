﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.MachineTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyMachineSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyMachine")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyMachine")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Machine tab from step")]
        public void GivenIOpenTheMachineTabFromStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            TabPage commodityTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            commodityTab.Click();
        }
        
        [Given(@"I select a Machine and copy it")]
        public void GivenISelectAMachineAndCopyIt()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");

            ListViewRow row = machinesDataGrid.Row("Name", "machine1");

            Menu copyMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [Given(@"I select a Machine to Copy to Master Data")]
        public void GivenISelectAMachineToCopyToMasterData()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");

            ListViewRow row = machinesDataGrid.Row("Name", "machine1");

            Menu copyToMDMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMDMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [When(@"I select another process step press Paste")]
        public void WhenISelectAnotherProcessStepPressPaste()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step1");
            step2Node.SelectEx();

            Menu pasteMenuItem = step2Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I select the MachineGrid, right-click and paste")]
        public void WhenISelectTheMachineGridRight_ClickAndPaste()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");

            ListViewRow row = machinesDataGrid.Row("Name", "machine1");

            Menu pasteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I open the Machine from Master Data")]
        public void WhenIOpenTheMachineFromMasterData()
        {
            TreeNode machineNode = application.MainScreen.ProjectsTree.MasterDataMachines;
            Assert.IsNotNull(machineNode, "The Machine node was not found.");
            machineNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the Machine is copied into this step")]
        public void ThenTheMachineIsCopiedIntoThisStep()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");
        }
        
        [Then(@"a Machine with the name containing copy of is added")]
        public void ThenAMachineWithTheNameContainingCopyOfIsAdded()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", copyTranslation + "machine1");
        }
        
        [Then(@"a copy of Machine can be viewed")]
        public void ThenACopyOfMachineCanBeViewed()
        {
            ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(machinesDataGrid, "Manage Master Data grid was not found.");
            machinesDataGrid.Select("Name", "machine1"); 
        }
    }
}
