﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.MachineTests;
using ZPKTool.DataAccess;
using ZPKTool.Data;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class UpdateMachineSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Machine machine = new Machine();
            machine.Name = "1MachineToEdit";
            machine.SetOwner(adminUser);
            machine.IsMasterData = true;

            Manufacturer m = new Manufacturer();
            m.Name = "test";
            machine.Manufacturer = m;

            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();
        }

        [BeforeFeature("UpdateMachine")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            CreateTestData();

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("UpdateMachine")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Machine tab")]
        public void GivenISelectTheMachineTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();
        }
        
        [Given(@"I select a row from Machine data grid and press the Edit button")]
        public void GivenISelectARowFromMachineDataGridAndPressTheEditButton()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");

            Button editButton = mainWindow.Get<Button>(MachineAutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I update fields")]
        public void GivenIUpdateFields()
        {
            Window editMachinesWindow = application.Windows.MachineWindow;

            TextBox name = editMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "machine1_Updated";

            TextBox depreciationPeriodTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            depreciationPeriodTextBox.Text = "10";

            TextBox depreciationRateTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
            depreciationPeriodTextBox.Text = "10";

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I select another row from Machine data grid and press the Edit button")]
        public void GivenISelectAnotherRowFromMachineDataGridAndPressTheEditButton()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machineToEdit");

            Button editButton = mainWindow.Get<Button>(MachineAutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I press the cancel btn")]
        public void GivenIPressTheCancelBtn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press the No button in question win message")]
        public void GivenIPressTheNoButtonInQuestionWinMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press cancel btn")]
        public void GivenIPressCancelBtn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I select a row from data grid and press double-click")]
        public void GivenISelectARowFromDataGridAndPressDouble_Click()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machineDblClick");

            ListViewRow machineRow = machinesDataGrid.Row("Name", "machineDblClick");
            machineRow.DoubleClick();
        }
        
        [Given(@"I select the MD, Machines tab")]
        public void GivenISelectTheMDMachinesTab()
        {
            TreeNode machinesNode = application.MainScreen.ProjectsTree.MasterDataMachines;
            Assert.IsNotNull(machinesNode, "The Machines node was not found.");

            machinesNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select a row from MD -> Machine data grid and press the Edit button")]
        public void GivenISelectARowFromMD_MachineDataGridAndPressTheEditButton()
        {
            ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");
            machinesDataGrid.Select("Name", "1MachineToEdit");

            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            Assert.IsNotNull(editButton, "The Delete button was not found.");
            editButton.Click();
            Window editMachinesWindow = application.Windows.MachineWindow;
        }

        [Given(@"I update some of the fields")]
        public void GivenIUpdateSomeOfTheFields()
        {
            Window editMachinesWindow = application.Windows.MachineWindow;

            TextBox name = editMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "111111";

            TextBox depreciationPeriodTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            depreciationPeriodTextBox.Text = "10";

            TextBox depreciationRateTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
            depreciationPeriodTextBox.Text = "10";    
        }

        [When(@"I press the Save bttn")]
        public void WhenIPressTheSaveBttn()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I press the Cancel btn")]
        public void WhenIPressTheCancelBtn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I press the yes button in question win message")]
        public void WhenIPressTheYesButtonInQuestionWinMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }
        
        [Then(@"the edit Machine window is closed")]
        public void ThenTheEditMachineWindowIsClosed()
        {
            Window editMachineWindow = application.Windows.MachineWindow;
            Assert.IsNull(editMachineWindow, "Edit machines window should be closed");
        }
    }
}
