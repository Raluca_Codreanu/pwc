﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using System.IO;

namespace ZPKTool.AutomatedTests.MachineTests
{
    [Binding]
    public class ImportExportMachineSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string machineExportPath;

        public ImportExportMachineSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportMachineSteps.machineExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".machine");
            if (File.Exists(ImportExportMachineSteps.machineExportPath))
            {
                File.Delete(ImportExportMachineSteps.machineExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportMachineSteps.machineExportPath))
            {
                File.Delete(ImportExportMachineSteps.machineExportPath);
            }
        }

        [BeforeFeature("ImportExportMachine")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportMachine")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I open the Machine tab from Process step")]
        public void GivenIOpenTheMachineTabFromProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();
        }
        
        [Given(@"I select a Machine, press right click to export it")]
        public void GivenISelectAMachinePressRightClickToExportIt()
        {
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "machine1");

            ListViewRow row = machinesDataGrid.Row("Name", "machine1");

            Menu exportMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press the Cancel in Save As window")]
        public void GivenIPressTheCancelInSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I press the Save btn")]
        public void GivenIPressTheSaveBtn()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportMachineSteps.machineExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ImportExportMachineSteps.machineExportPath), "The machine was not exported at the specified location.");
        }
        
        [When(@"I select the step node to import Machine")]
        public void WhenISelectTheStepNodeToImportMachine()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            Menu importPartMenuItem = step1Node.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportMachine);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Machine menu item is disabled.");
            importPartMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\machine1.machine"));
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the Machine is imported into the Machine datagrid")]
        public void ThenTheMachineIsImportedIntoTheMachineDatagrid()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(MachineAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            TabPage machinesTab = mainWindow.Get<TabPage>(MachineAutomationIds.MachinesListTab);
            machinesTab.Click();  
        }
    }
}
