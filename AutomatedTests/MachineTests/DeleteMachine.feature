﻿@DeleteMachine
Feature: Delete Machine Process

Scenario: Delete Machine
	Given I open the Machine tab
	And I select a Machine 
	And I press the Delete button from machine grid
	And I press the No btn in question 
	And I press the Delete button from machine grid
	And I press the Yes in question
	And I open the Trash Bin screen
	And I select the deleted row from data grid
	And I press View Details button
	And I press Recover Button
	When I opened the Machine tab 
	Then the data grid contains the deleted Machine

Scenario: Delete Machine context menu
	Given I open the Machine tab
	And I select a Machine, press right click to delete it
	And I press the No btn in question 
	And I select a Machine, press right click to delete it
	And I press Yes Button 
	And I open the Trash Bin screen
	And I press the Empty Trash Bin btn
	When I press Yes button
	Then the Machine is deleted from trash bin