﻿@CopyMachine
Feature: CopyMachine

Scenario: Copy Machine to another step
	Given I open the Machine tab from step
	And I select a Machine and copy it
	When I select another process step press Paste
	Then the Machine is copied into this step

Scenario: Copy Machine into the same step
	Given I open the Machine tab from step
	And I select a Machine and copy it
	When I select the MachineGrid, right-click and paste
	Then a Machine with the name containing copy of is added

Scenario: Copy Machine to Master Data
	Given I open the Machine tab from step
	And I select a Machine to Copy to Master Data 
	When I open the Machine from Master Data
	Then a copy of Machine can be viewed