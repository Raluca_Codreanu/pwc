﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.MachineTests
{
    /// <summary>
    /// Summary description for CreateRawMaterialMDTestCase
    /// </summary>
    [TestClass]
    public class CreateMachineMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        /// <summary>
        /// An automated test for Create Machine Master Data Test Case (Test Link id = 263)
        /// </summary
        [TestMethod]
        [TestCategory("Machines")]
        public void CreateMachinesMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Machines node was not found.");

                machinesNode.Click();
                ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                Window createMachinesWindow = app.Windows.MachineWindow;
                createMachinesWindow.WaitForAsyncUITasks();

                //step 2
                TextBox name = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "1machine";

                //step 4
                TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
                depreciationPeriodTextBox.Text = "10";

                //step 5
                TextBox depreciationRateTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
                Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
                depreciationPeriodTextBox.Text = "10";

                //step 6
                TextBox registrationNumberTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.RegistrationNumberTextBox);
                registrationNumberTextBox.Text = "10";

                //step 7
                TextBox descriptionOfFunctionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DescriptionOfFunctionTextBox);
                descriptionOfFunctionTextBox.Text = "machines_description";

                //step 9   
                var mediaControl = createMachinesWindow.GetMediaControl();
                mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);

                //step 10
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox descriptionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";

                //step 11
                TabPage floorEnergy = mainWindow.Get<TabPage>(AutomationIds.FloorEnergyData);
                floorEnergy.Click();

                TextBox floorSizeTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.FloorSizeTextBox);
                floorSizeTextBox.Text = "10";

                TextBox workspaceAreaTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.WorkspaceAreaTextBox);
                workspaceAreaTextBox.Text = "10";

                TextBox powerConsumptionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.PowerConsumptionTextBox);
                powerConsumptionTextBox.Text = "10";

                TextBox airConsumptionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.AirConsumptionTextBox);
                airConsumptionTextBox.Text = "10";

                TextBox waterConsumptionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.WaterConsumptionTextBox);
                waterConsumptionTextBox.Text = "10";

                TextBox fullLoadRateTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.FullLoadRateTextBox);
                fullLoadRateTextBox.Text = "10";

                //step 12
                TabPage investmentTab = mainWindow.Get<TabPage>(AutomationIds.InvestmentData);
                investmentTab.Click();

                TextBox machineInvestmentTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineInvestmentTextBox);
                machineInvestmentTextBox.Text = "10";

                TextBox setupInvestmentTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.SetupInvestmentTextBox);
                setupInvestmentTextBox.Text = "10";
                TextBox additionalEquipmentTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.AdditionalEquipmentTextBox);
                additionalEquipmentTextBox.Text = "10";
                TextBox foundationSetupTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.FoundationSetupTextBox);
                foundationSetupTextBox.Text = "10";
                TextBox investmentRemarksTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.InvestmentRemarksTextBox);
                investmentRemarksTextBox.Text = "inverstemnt remark";

                //step 13
                TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MaintenanceData);
                maintenanceTab.Click();

                CheckBox calcWithKValueCheckBox = createMachinesWindow.Get<CheckBox>(MachineAutomationIds.CalcWithKValueCheckBox);
                calcWithKValueCheckBox.Checked = false;

                TextBox externalWorkCostTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.ExternalWorkCostTextBox);
                externalWorkCostTextBox.Text = "10";

                TextBox materialCostTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MaterialCostTextBox);
                materialCostTextBox.Text = "10";

                //step 15
                TabPage propertiesTab = mainWindow.Get<TabPage>(AutomationIds.PropertiesTabItem);
                propertiesTab.Click();

                TextBox mountingCubeLengthTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MountingCubeLengthTextBox);
                mountingCubeLengthTextBox.Text = "5";

                TextBox mountingCubeWidthTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MountingCubeWidthTextBox);
                mountingCubeWidthTextBox.Text = "5";

                TextBox mountingCubeHeightTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MountingCubeHeightTextBox);
                mountingCubeHeightTextBox.Text = "5";
                TextBox maxDiameterRodPerChuckMaxThicknessTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MaxDiameterRodPerChuckMaxThicknessTextBox);
                maxDiameterRodPerChuckMaxThicknessTextBox.Text = "5";
                TextBox maxPartsWeightPerCapacityTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MaxPartsWeightPerCapacityTextBox);
                maxPartsWeightPerCapacityTextBox.Text = "5";

                TextBox lockingForceTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.LockingForceTextBox);
                lockingForceTextBox.Text = "5";

                //step 16
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                //step 17
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Raw Materials node was not found.");

                machinesNode.Click();
                machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Raw materials data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                machinesDataGrid.Select("Name", "1machine");
            }
        }

        /// <summary>
        /// An automated test for Cancel Create Machine Master Data Test Case (Test Link id = 262)
        /// </summary
        [TestMethod]
        [TestCategory("Machines")]
        public void CancelCreateMachinesMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Machines node was not found.");

                machinesNode.Click();
                ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                //step 2
                Window createMachinesWindow = app.Windows.MachineWindow;
                createMachinesWindow.WaitForAsyncUITasks();
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3
                addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                addButton.Click();
                createMachinesWindow = app.Windows.MachineWindow;
                TextBox name = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "11machine";
                cancelButton = createMachinesWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 4
                createMachinesWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 5                
                cancelButton.Click();
                createMachinesWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 6                
                cancelButton.Click();
                createMachinesWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 7
                addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                addButton.Click();
                createMachinesWindow = app.Windows.MachineWindow;
                createMachinesWindow.Close();

                //step 8
                addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                addButton.Click();
                createMachinesWindow = app.Windows.MachineWindow;
                name = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "11machine";
                TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
                depreciationPeriodTextBox.Text = "10";

                TextBox depreciationRateTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
                Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
                depreciationPeriodTextBox.Text = "10";
                TextBox registrationNumberTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.RegistrationNumberTextBox);
                registrationNumberTextBox.Text = "10";

                TextBox descriptionOfFunctionTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.DescriptionOfFunctionTextBox);
                descriptionOfFunctionTextBox.Text = "machines_description";

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TabPage maintenanceTab = mainWindow.Get<TabPage>(AutomationIds.MaintenanceData);
                maintenanceTab.Click();

                CheckBox calcWithKValueCheckBox = createMachinesWindow.Get<CheckBox>(MachineAutomationIds.CalcWithKValueCheckBox);
                calcWithKValueCheckBox.Checked = false;

                TextBox externalWorkCostTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.ExternalWorkCostTextBox);
                externalWorkCostTextBox.Text = "10";

                TextBox materialCostTextBox = createMachinesWindow.Get<TextBox>(MachineAutomationIds.MaterialCostTextBox);
                materialCostTextBox.Text = "10";

                createMachinesWindow.Close();

                //step 9
                createMachinesWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 10
                createMachinesWindow.Close();

                createMachinesWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper
        #endregion Helper
    }
}


