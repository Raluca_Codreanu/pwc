﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.MachineTests
{
    /// <summary>
    /// The automated test for Edit Machine Master Data (test link id = 265)
    /// </summary>
    [TestClass]
    public class EditMachineMDTestCase
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="DeleteMachineMDTesteCase"/> class.
        ///// </summary>
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditMachineMDTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Update Machine Data Test Case (test Link id = 265).
        /// </summary>
        [TestMethod]
        [TestCategory("Machines")]
        public void EditMachineMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Machines node was not found.");

                machinesNode.Click();
                ListViewControl machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");

                //step 1
                mainWindow.WaitForAsyncUITasks();
                machinesDataGrid.Select("Name", "11Machine");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();
                Window editMachinesWindow = app.Windows.MachineWindow;

                //step 2
                TextBox name = editMachinesWindow.Get<TextBox>(MachineAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "11machine_Updated";

                TextBox depreciationPeriodTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
                depreciationPeriodTextBox.Text = "10";

                TextBox depreciationRateTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
                Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
                depreciationPeriodTextBox.Text = "10";
                TextBox registrationNumberTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.RegistrationNumberTextBox);
                registrationNumberTextBox.Text = "10";
                TextBox descriptionOfFunctionTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DescriptionOfFunctionTextBox);
                descriptionOfFunctionTextBox.Text = "machines_description";

                //step 3
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = editMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox descriptionTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";

                TabPage floorEnergy = mainWindow.Get<TabPage>(AutomationIds.FloorEnergyData);
                floorEnergy.Click();

                TextBox floorSizeTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.FloorSizeTextBox);
                floorSizeTextBox.Text = "11";

                TextBox workspaceAreaTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.WorkspaceAreaTextBox);
                workspaceAreaTextBox.Text = "11";

                TextBox powerConsumptionTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.PowerConsumptionTextBox);
                powerConsumptionTextBox.Text = "11";

                TextBox airConsumptionTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.AirConsumptionTextBox);
                airConsumptionTextBox.Text = "11";

                TextBox waterConsumptionTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.WaterConsumptionTextBox);
                waterConsumptionTextBox.Text = "11";

                TextBox fullLoadRateTextBox = editMachinesWindow.Get<TextBox>(MachineAutomationIds.FullLoadRateTextBox);
                fullLoadRateTextBox.Text = "11";

                //step 8
                Button saveButton = editMachinesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                //step 9
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                machinesNode = app.MainScreen.ProjectsTree.MasterDataMachines;
                Assert.IsNotNull(machinesNode, "The Machines node was not found.");

                machinesNode.Click();
                machinesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(machinesDataGrid, "Machines data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                machinesDataGrid.Select("Name", "11machine_Updated");
            }
        }

        #region Helper

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Machine machine = new Machine();
            machine.Name = "11Machine";
            machine.SetOwner(adminUser);
            machine.IsMasterData = true;

            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();
        }
        #endregion Helper
    }
}
