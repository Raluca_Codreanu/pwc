﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.MachineTests
{
    public static class MachineAutomationIds
    {
        public const string MachineNameTextBox = "MachineNameTextBox";
        public const string DepreciationPeriodTextBox = "DepreciationPeriodTextBox";
        public const string DepreciationRateTextBox = "DepreciationRateTextBox";
        public const string RegistrationNumberTextBox = "RegistrationNumberTextBox";
        public const string DescriptionOfFunctionTextBox = "DescriptionOfFunctionTextBox";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string FloorSizeTextBox = "FloorSizeTextBox";
        public const string WorkspaceAreaTextBox = "WorkspaceAreaTextBox";
        public const string AirConsumptionTextBox = "AirConsumptionTextBox";
        public const string PowerConsumptionTextBox = "PowerConsumptionTextBox";
        public const string WaterConsumptionTextBox = "WaterConsumptionTextBox";
        public const string FullLoadRateTextBox = "FullLoadRateTextBox";
        public const string SetupInvestmentTextBox = "SetupInvestmentTextBox";
        public const string MachineInvestmentTextBox = "MachineInvestmentTextBox";
        public const string AdditionalEquipmentTextBox = "AdditionalEquipmentTextBox";
        public const string FoundationSetupTextBox = "FoundationSetupTextBox";
        public const string InvestmentRemarksTextBox = "InvestmentRemarksTextBox";
        public const string ExternalWorkCostTextBox = "ExternalWorkCostTextBox";
        public const string MaterialCostTextBox = "MaterialCostTextBox";
        public const string RuptureStrengthTextBox = "RuptureStrengthTextBox";
        public const string MountingCubeWidthTextBox = "MountingCubeWidthTextBox";
        public const string MountingCubeLengthTextBox = "MountingCubeLengthTextBox";
        public const string MountingCubeHeightTextBox = "MountingCubeHeightTextBox";
        public const string MaxDiameterRodPerChuckMaxThicknessTextBox = "MaxDiameterRodPerChuckMaxThicknessTextBox";
        public const string RxTextBox = "MaxPartsWeightPerCapacityTextBox";
        public const string MaxPartsWeightPerCapacityTextBox = "MaxPartsWeightPerCapacityTextBox";
        public const string CalcWithKValueCheckBox = "CalcWithKValueCheckBox";
        public const string LockingForceTextBox = "LockingForceTextBox";
        public const string MachinesListTab = "MachinesListTab";
        public const string MachinesDataGrid = "MachinesDataGrid";
        public const string ProcessNode = "ProcessNode";
        public const string DeleteButton = "DeleteButton";
        public const string EditButton = "EditButton";
        public const string AddMachineButton = "AddMachineButton";
    }
}
