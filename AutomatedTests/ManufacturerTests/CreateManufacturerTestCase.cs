﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.Utils;

namespace ZPKTool.AutomatedTests.ManufacturerTests
{
    /// <summary>
    /// The automated test of CreateManufacturerTestCase
    /// </summary>
    [TestClass]
    public class CreateManufacturerTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateManufacturerTestCase"/> class.
        /// </summary>
        public CreateManufacturerTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext){}
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup(){}
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// An automated test for Create Manufacturer Test Case step 1-> step 10.
        /// </summary>
        [TestMethod]
        [TestCategory("Manufacturer")]
        public void CancelCreateManufacturerTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                
                // step 4
                TreeNode manufacturersNode = app.MainScreen.ProjectsTree.MasterDataManufacturers;
                manufacturersNode.Click();

                ListViewControl manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(manufacturersDataGrid, "Manufacturers data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                // step 5
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                Window createManufacturerWindow = Wait.For(() => app.Windows.ManufacturerWindow);

                // step 6
                TextBox nameTextBox = createManufacturerWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");

                TextBox descriptionTextBox = createManufacturerWindow.Get<TextBox>(ManufacturerAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");

                nameTextBox.Text = "ManufacturerTest";
                descriptionTextBox.Text = "Description of Manufacturer";

                // step 7 
                Button cancelButton = createManufacturerWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");

                cancelButton.Click();

                // step 8
                createManufacturerWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                // step 9
                cancelButton.Click();

                // step 10
                createManufacturerWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                
                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNull(manufacturersDataGrid.Row("Name", "ManufacturerTest"), "A manufacturer has been created although the Cancel operation has been performed.");
            }
        }

        /// <summary>
        /// An automated test for Create Manufacturer Test Case step 11-> step 16.
        /// </summary>
        [TestMethod]
        [TestCategory("Manufacturer")]
        public void CreateManufacturerTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode manufacturersNode = app.MainScreen.ProjectsTree.MasterDataManufacturers; 
                manufacturersNode.Click();
                ListViewControl manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                mainWindow.WaitForAsyncUITasks();
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                // step 11
                addButton.Click();                
                Window createManufacturerWindow = Wait.For(() => app.Windows.ManufacturerWindow);

                Button SaveButton = createManufacturerWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(SaveButton, "Create button was not found");

                // step 12                
                Assert.IsFalse(SaveButton.Enabled, "the save button should not be enabled.");

                // step 13
                TextBox nameTextBox = createManufacturerWindow.Get<TextBox>(AutomationIds.NameTextBox);
                ValidationHelper.CheckTextFieldValidators(createManufacturerWindow, nameTextBox, 100);

                // step 14
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                // step 15
                TextBox descriptionTextBox = createManufacturerWindow.Get<TextBox>(ManufacturerAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "Description of Manufacturer";

                // step 16
                SaveButton.Click();                

                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                ListViewRow manufacturerRow = manufacturersDataGrid.SelectedRows.FirstOrDefault();
                Assert.AreEqual(nameTextBox.Text, manufacturersDataGrid.Cell("Name", manufacturerRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The name displayed in the data grid is wrong.");
                Assert.AreEqual(descriptionTextBox.Text, manufacturersDataGrid.Cell("Description", manufacturerRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The description displayed in the data grid is wrong.");
                // Assert.AreEqual(nameTextBox.Text, mainWindow.Get<Label>(CreateManufacturerTestCase.UIItemsIdentifiers.NameContentLabel).Text, "Wrong Name displayed in the Manufacturer View.");
                // Assert.AreEqual(descriptionTextBox.Text, mainWindow.Get<Label>(CreateManufacturerTestCase.UIItemsIdentifiers.DescriptionContentLabel).Text, "Wrong Description displayed in the Manufacturer View.");
            }
        }

        #endregion Test Methods

        #region Helpers

        #endregion Helpers
    }
}
