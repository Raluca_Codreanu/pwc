﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ManufacturerTests
{
    /// <summary>
    /// The automated test of Delete Manufacturer Test Case.
    /// </summary>
    [TestClass]
    public class DeleteManufacturerTestCase
    {
        /// <summary>
        /// A list of manufacturers that represents the test data.
        /// </summary>
        private static List<Manufacturer> manufacturers = new List<Manufacturer>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteManufacturerTestCase"/> class.
        /// </summary>
        public DeleteManufacturerTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteManufacturerTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Manufacturer Test Case.
        /// </summary>
        [TestMethod]
        [TestCategory("Manufacturer")]
        public void DeleteManufacturerTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode manufacturersNode = app.MainScreen.ProjectsTree.MasterDataManufacturers; 
                Assert.IsNotNull(manufacturersNode, "The Manufacturers node was not found.");
                manufacturersNode.Click();
                ListViewControl manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(manufacturersDataGrid, "Manage Master Data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                // step 5
                Manufacturer manufacturer1 = DeleteManufacturerTestCase.manufacturers.FirstOrDefault(m => m.RawMaterials.Count == 0);
                manufacturersDataGrid.Select("Name", manufacturer1.Name);

                // step 6
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");

                deleteButton.Click();
              
                // step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                ListViewRow manufacturerRow = manufacturersDataGrid.SelectedRows.FirstOrDefault();
                Assert.IsTrue(manufacturerRow != null && manufacturer1.Name == manufacturersDataGrid.Cell("Name", manufacturerRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name,
                              "The manufacturer was deleted although the No button was clicked in the confirmation window.");

                // step 8
                deleteButton.Click();

                // step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                mainWindow.WaitForAsyncUITasks();

                // step 10
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "The Trash Bin node was not found.");
                trashBinNode.SelectEx();

                var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                manufacturerRow = trashBinDataGrid.Row("Name", manufacturer1.Name);
                Assert.IsNull(manufacturerRow, "Deleted manufacturer was moved into the Trash Bin, but it should be permanently deleted.");

                // step 11
                Manufacturer manufacturer2 = DeleteManufacturerTestCase.manufacturers.FirstOrDefault(m => m.RawMaterials.Count > 0);
                manufacturersNode.Click();
                mainWindow.WaitForAsyncUITasks();
                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                manufacturersDataGrid.Select("Name", manufacturer2.Name);

                deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                deleteButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
                
                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(manufacturersDataGrid.Row("Name", manufacturer2.Name), "A manufacturer that is a part of another entity was deleted.");
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            manufacturer1.Description = "Description" + DateTime.Now.Ticks;
            manufacturer1.IsMasterData = true;
            DeleteManufacturerTestCase.manufacturers.Add(manufacturer1);

            Manufacturer manufacturer2 = new Manufacturer();
            manufacturer2.Name = "Manufacturer" + manufacturer2.Guid.ToString();
            manufacturer2.Description = "Description" + DateTime.Now.Ticks;
            DeleteManufacturerTestCase.manufacturers.Add(manufacturer2);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = "Raw Material" + DateTime.Now.Ticks;
            rawMaterial.Manufacturer = manufacturer2;
            rawMaterial.SetIsMasterData(true);

            dataContext.ManufacturerRepository.Add(manufacturer1);
            dataContext.RawMaterialRepository.Add(rawMaterial);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
