﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.ManufacturerTests
{
    public static class ManufacturerAutomationIds
    {
        public const string EditManufacturerWindow = "Edit Manufacturer";
        public const string CreateManufacturerWindow = "Create Manufacturer";
        public const string NameContentLabel = "NameContentLabel";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string DescriptionContentLabel = "DescriptionContentLabel";            
    }
}
