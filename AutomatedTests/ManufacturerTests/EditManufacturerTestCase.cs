﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.WindowItems;
using White.Core;
using ZPKTool.Data;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ManufacturerTests
{
    /// <summary>
    /// The automated test of Edit Manufacturer Test Case.
    /// </summary>
    [TestClass]
    public class EditManufacturerTestCase
    {
        /// <summary>
        /// The manufacturer which will be edited.
        /// </summary>
        private static Manufacturer manufacturer;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditManufacturerTestCase"/> class.
        /// </summary>
        public EditManufacturerTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditManufacturerTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Edit Manufacturer test case step 1-> 13.
        /// </summary>
        [TestMethod]
        [TestCategory("Manufacturer")]
        public void CancelEditManufacturerTest()
        {
            // Refresh the manufacturer in case the EditManufacturerTest was executed before this one.
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            EditManufacturerTestCase.manufacturer = dataContext.ManufacturerRepository.GetById(EditManufacturerTestCase.manufacturer.Guid);

            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                // step 4
                TreeNode manufacturerNode = app.MainScreen.ProjectsTree.MasterDataManufacturers;
                Assert.IsNotNull(manufacturerNode, "The Manufacturer node was not found.");

                manufacturerNode.Click();
                ListViewControl manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(manufacturersDataGrid, "Manage Master Data grid was not found.");

                //// step 5
                mainWindow.WaitForAsyncUITasks();
                manufacturersDataGrid.Select("Name", EditManufacturerTestCase.manufacturer.Name);

                // step 6
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");

                editButton.Click();
                Window editManufacturerWindow = Wait.For(() => app.Windows.ManufacturerWindow);

                // step 7
                TextBox nameTextBox = editManufacturerWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
                Assert.AreEqual(EditManufacturerTestCase.manufacturer.Name, nameTextBox.Text, "The displayed name is different than the manufacturer name.");

                nameTextBox.Text = string.Empty;
                editManufacturerWindow.Get<TextBox>(ManufacturerAutomationIds.DescriptionTextBox).Focus();

                Button saveButton = editManufacturerWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                Assert.IsFalse(saveButton.Enabled, "The Save button is disabled although the Name text box is empty.");

                // step 9
                TextBox descriptionTextBox = editManufacturerWindow.Get<TextBox>(ManufacturerAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "The Description text box was not found.");
                Assert.AreEqual(EditManufacturerTestCase.manufacturer.Description, descriptionTextBox.Text, "The displayed description is different than the manufacturer description.");

                nameTextBox.Text = "Manufacturer Updated" + DateTime.Now.Ticks;
                descriptionTextBox.Text = "Description Updated" + DateTime.Now.Ticks;

                Button cancelButton = editManufacturerWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click(); // Related defect #1347                
                
                // step 10
                editManufacturerWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                Assert.IsNotNull(app.Windows.ManufacturerWindow, "Edit Manufacturer window was closed although the Cancel -> No option was selected.");

                // step 11
                cancelButton.Click();

                // step 12
                editManufacturerWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                Assert.IsNull(app.Windows.ManufacturerWindow, "Failed to close the Edit Manufacturer window at Cancel.");

                // step 13
                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                ListViewRow manufacturerRow = manufacturersDataGrid.Row("Name", EditManufacturerTestCase.manufacturer.Name);
                Assert.IsNotNull(manufacturerRow, "The manufacturer was updated although the Cancel button was clicked.");

                ListViewCell descriptionCell = manufacturersDataGrid.Cell("Description", manufacturerRow);
                Assert.IsNotNull(descriptionCell, "The Description cell was not found.");
                Assert.AreEqual(EditManufacturerTestCase.manufacturer.Description, descriptionCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The manufacturer was updated although the Cancel button was clicked.");
            }
        }

        /// <summary>
        /// An automated test for Edit Manufacturer test case step 1,2, 13 -> 16.
        /// </summary>
        [TestMethod]
        [TestCategory("Manufacturer")]
        public void EditManufacturerTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                // step 4
                TreeNode manufacturersNode = app.MainScreen.ProjectsTree.MasterDataManufacturers;
                manufacturersNode.Click();
                mainWindow.WaitForAsyncUITasks();
                // step 14
                ListViewControl manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                manufacturersDataGrid.Select("Name", EditManufacturerTestCase.manufacturer.Name);
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();

                Window editManufacturerWindow = Wait.For(() => app.Windows.ManufacturerWindow);

                // step 15
                TextBox nameTextBox = editManufacturerWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Manufacturer Updated" + DateTime.Now.Ticks;

                TextBox descriptionTextBox = editManufacturerWindow.Get<TextBox>(ManufacturerAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "Description Updated" + DateTime.Now.Ticks;

                Button saveButton = editManufacturerWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsTrue(saveButton.Enabled, "The Save button is disabled.");

                Button cancelButton = editManufacturerWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsTrue(cancelButton.Enabled, "The Cancel button is disabled.");

                // step 16
                saveButton.Click();
                manufacturersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                ListViewRow manufacturerRow = manufacturersDataGrid.SelectedRows.FirstOrDefault();

                Assert.AreEqual(nameTextBox.Text, manufacturersDataGrid.Cell("Name", manufacturerRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong Name displayed into the data grid.");
                //                Assert.AreEqual(nameTextBox.Text, mainWindow.Get<Label>(CreateManufacturerTestCase.UIItemsIdentifiers.NameContentLabel).Text, "Wrong Name displayed in the Manufacturer View.");
                Assert.AreEqual(descriptionTextBox.Text, manufacturersDataGrid.Cell("Description", manufacturerRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong Description displayed into the data grid.");
                //                Assert.AreEqual(descriptionTextBox.Text, mainWindow.Get<Label>(CreateManufacturerTestCase.UIItemsIdentifiers.DescriptionContentLabel).Text, "Wrong Description displayed in the Manufacturer View.");
            }
        }

        #region Helper
        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            EditManufacturerTestCase.manufacturer = new Manufacturer();
            EditManufacturerTestCase.manufacturer.Name = "Manufacturer " + DateTime.Now.Ticks;
            EditManufacturerTestCase.manufacturer.Description = "Description " + DateTime.Now.Ticks;
            EditManufacturerTestCase.manufacturer.IsMasterData = true;

            dataContext.ManufacturerRepository.Add(EditManufacturerTestCase.manufacturer);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
