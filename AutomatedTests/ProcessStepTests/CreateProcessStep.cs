﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TabItems;
using System.Windows.Automation;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.InputDevices;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.Commodity;
using ZPKTool.AutomatedTests.DieTests;
using ZPKTool.AutomatedTests.ConsumableTests;
using ZPKTool.AutomatedTests.Utils;

namespace ZPKTool.AutomatedTests.ProcessStepTests
{
    /// <summary>
    /// Summary description for CreateProcessStep
    /// </summary>
    [TestClass]
    public class CreateProcessStep
    {
        public CreateProcessStep()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CreatePartProcess()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();

                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(ProcessStepAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "project1";

                TabPage supplierTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProjectSupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(ProcessStepAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("project1");
                project1Node.SelectEx();

                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();

                //step 4
                Window createPartWindow = app.Windows.PartWindow;
                TextBox name = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "part1";

                Button browseCountryButton = createPartWindow.Get<Button>(ProcessStepAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createPartWindow.Get<TextBox>(ProcessStepAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = createPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox manufacturerTextBox = createPartWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
                manufacturerTextBox.Text = "description of manufacturer";

                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                project1Node = Wait.For(() => myProjectsNode.GetNode("project1"));
                project1Node.ExpandEx();

                TreeNode partNode1 = project1Node.GetNode("part1");
                partNode1.ExpandEx();

                //step 5
                TreeNode processNode = partNode1.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
                processNode.SelectEx();

                TabPage processView = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessViewTab);
                processView.Click();

                //step 6
                Button addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
                addStep.Click();

                TabPage informationTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessStepInfoTab);
                informationTab.Click();

                //step 7
                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);

                //step 8
                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                ValidationHelper.CheckTextFieldValidators(mainWindow, nameTextBox, 100);

                //step 9
                nameTextBox.Text = "step1";
                //step 10
                ComboBox accuracyComboBox = mainWindow.Get<ComboBox>(ProcessStepAutomationIds.AccuracyComboBox);
                //accuracyComboBox.Click();

                //step 11
                accuracyComboBox.SelectItem(1);

                TextBox estimatedCostTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.EstimatedCostTextBox);
                int maxAllowedValueOfDecimalFields = 1000000000;
                ValidationHelper.CheckNumericFieldValidators(mainWindow, estimatedCostTextBox, maxAllowedValueOfDecimalFields, true);

                estimatedCostTextBox.Text = "100";

                //step 16
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 17
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 18                
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 19 
                addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
                addStep.Click();

                //step 20
                nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "step1";

                TextBox processTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
                processTimeTextBox.Text = "1200";

                TextBox cycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
                cycleTimeTextBox.Text = "1200";

                TextBox batchSizeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                //step 23
                CheckBox exceedShiftCostCheckbox = mainWindow.Get<CheckBox>(ProcessStepAutomationIds.ExceedShiftCostCheckbox);
                exceedShiftCostCheckbox.Checked = true;
                TextBox exceededShiftCostRatioTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ExceededShiftCostRatioTextBox);
                exceededShiftCostRatioTextBox.Text = "10";

                TextBox extraShiftsNumberTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ExtraShiftsNumberTextBox);
                extraShiftsNumberTextBox.Text = "10";
                //step 43
                TextBox descriptionTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "step_description";

                TabPage labourSettingsTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.LaborSettingsTabItem);
                labourSettingsTab.Click();

                TextBox prodUnskilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdUnskilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodUnskilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                prodUnskilledLabourTextBox.Text = "10";

                TextBox prodSkilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdSkilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodSkilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                prodUnskilledLabourTextBox.Text = "10";

                TextBox prodForemanTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdForemanTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodForemanTextBox, maxAllowedValueOfDecimalFields, true);
                prodForemanTextBox.Text = "10";

                TextBox prodTechniciansTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdTechniciansTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodTechniciansTextBox, maxAllowedValueOfDecimalFields, true);
                prodTechniciansTextBox.Text = "10";

                TextBox prodEngineersTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdEngineersTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodEngineersTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox setupUnskilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupUnskilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupUnskilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                setupUnskilledLabourTextBox.Text = "10";

                TextBox setupSkilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupSkilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupSkilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                setupUnskilledLabourTextBox.Text = "10";

                TextBox setupForemanTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupForemanTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupForemanTextBox, maxAllowedValueOfDecimalFields, true);
                setupForemanTextBox.Text = "10";

                TextBox setupTechniciansTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTechniciansTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupTechniciansTextBox, maxAllowedValueOfDecimalFields, true);
                setupTechniciansTextBox.Text = "10";

                TextBox setupEngineersTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupEngineersTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupEngineersTextBox, maxAllowedValueOfDecimalFields, true);
                setupEngineersTextBox.Text = "10";

                //step 47
                TabPage additionalSettingsTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.AdditionalSettingsTabItem);
                additionalSettingsTab.Click();

                //step 48
                CheckBox externalCheckBox = mainWindow.Get<CheckBox>(ProcessStepAutomationIds.ExternalCheckBox);
                externalCheckBox.Checked = true;

                TextBox transportCostTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.TransportCostTextBox);
                transportCostTextBox.Text = "10";

                //step 50
                TabPage machinesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.MachinesTabList);
                machinesTab.Click();

                Button addMachineButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddMachineButton);
                addMachineButton.Click();

                Window createMachinesWindow = app.Windows.MachineWindow;
                name = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "1machine";

                TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DepreciationPeriodTextBox);
                depreciationPeriodTextBox.Text = "10";

                TextBox depreciationRateTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DepreciationRateTextBox);
                Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
                depreciationPeriodTextBox.Text = "10";

                TextBox registrationNumberTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.RegistrationNumberTextBox);

                TextBox descriptionOfFunctionTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionOfFunctionTextBox);
                descriptionOfFunctionTextBox.Text = "machines_description";

                manufacturerTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.Manufacturer);
                manufacturerTab.Click();

                manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TabPage floorEnergy = mainWindow.Get<TabPage>(ProcessStepAutomationIds.FloorEnergy);
                floorEnergy.Click();

                TextBox floorSizeTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.FloorSizeTextBox);
                floorSizeTextBox.Text = "10";

                TextBox workspaceAreaTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.WorkspaceAreaTextBox);
                workspaceAreaTextBox.Text = "10";

                TextBox powerConsumptionTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.PowerConsumptionTextBox);
                powerConsumptionTextBox.Text = "10";
                saveButton = createMachinesWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                //step 51
                TabPage diesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.DiesTabItem);
                diesTab.Click();

                Button addDieButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddDieButton, true);
                addDieButton.Click();

                DieTestsHelper.CreateDie(app);

                TabPage consumablesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ConsumablesTabItem);
                consumablesTab.Click();

                Button addConsumableButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddConsumableButton, true);
                addConsumableButton.Click();

                ConsumableTestsHelper.CreateConsumable(app);

                //step 52
                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                project1Node = myProjectsNode.GetNode("project1");                    
                project1Node.Click();

                TreeNode partNode = project1Node.GetNode("part1");
                partNode.ExpandEx();

                TreeNode resultDetails = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
                resultDetails.SelectEx();
                resultDetails.Click();
            }
        }

        [TestMethod]
        public void CreateAssemblyProcessStep()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();

                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(ProcessStepAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "projectAssembly";

                TabPage supplierTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProjectSupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(ProcessStepAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.Expand();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly");
                project1Node.SelectEx();

                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                //step 4
                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox name = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "assemblyStep";

                Button browseCountryButton = createAssemblyWindow.Get<Button>(ProcessStepAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(ProcessStepAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox manufacturerTextBox = createAssemblyWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
                manufacturerTextBox.Text = "description of manufacturer";

                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                TreeNode partNode1 = project1Node.GetNode("assemblyStep");                
                partNode1.ExpandEx();

                //step 5
                TreeNode processNode = partNode1.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
                processNode.SelectEx();

                TabPage processView = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessViewTab);
                processView.Click();

                //step 6
                Button addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
                addStep.Click();

                TabPage informationTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ProcessStepInfoTab);
                informationTab.Click();

                //step 7
                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);

                //step 8
                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                ValidationHelper.CheckTextFieldValidators(mainWindow, nameTextBox, 100);

                //step 9
                nameTextBox.Text = "step1";

                //step 10
                ComboBox accuracyComboBox = mainWindow.Get<ComboBox>(ProcessStepAutomationIds.AccuracyComboBox);
                //step 11
                accuracyComboBox.SelectItem(1);

                TextBox estimatedCostTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.EstimatedCostTextBox);
                int maxAllowedValueOfDecimalFields = 1000000000;
                ValidationHelper.CheckNumericFieldValidators(mainWindow, estimatedCostTextBox, maxAllowedValueOfDecimalFields, true);

                estimatedCostTextBox.Text = "100";

                //step 16
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 17
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 18                
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 19 
                addStep = mainWindow.Get<Button>(ProcessStepAutomationIds.AddNewStepButton, true);
                addStep.Click();
                //step 20
                nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "stepAssembly1";

                TextBox processTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
                processTimeTextBox.Text = "1200";

                TextBox cycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
                cycleTimeTextBox.Text = "1200";

                TextBox batchSizeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                //step 23
                CheckBox exceedShiftCostCheckbox = mainWindow.Get<CheckBox>(ProcessStepAutomationIds.ExceedShiftCostCheckbox);
                exceedShiftCostCheckbox.Checked = true;

                TextBox exceededShiftCostRatioTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ExceededShiftCostRatioTextBox);
                exceededShiftCostRatioTextBox.Text = "10";

                TextBox extraShiftsNumberTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ExtraShiftsNumberTextBox);
                extraShiftsNumberTextBox.Text = "10";
                //step 43
                TextBox descriptionTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "step_description";

                TabPage labourSettingsTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.LaborSettingsTabItem);
                labourSettingsTab.Click();

                TextBox prodUnskilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdUnskilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodUnskilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                prodUnskilledLabourTextBox.Text = "10";

                TextBox prodSkilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdSkilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodSkilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                prodUnskilledLabourTextBox.Text = "10";

                TextBox prodForemanTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdForemanTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodForemanTextBox, maxAllowedValueOfDecimalFields, true);
                prodForemanTextBox.Text = "10";

                TextBox prodTechniciansTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdTechniciansTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodTechniciansTextBox, maxAllowedValueOfDecimalFields, true);
                prodTechniciansTextBox.Text = "10";

                TextBox prodEngineersTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProdEngineersTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, prodEngineersTextBox, maxAllowedValueOfDecimalFields, true);

                TextBox setupUnskilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupUnskilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupUnskilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                setupUnskilledLabourTextBox.Text = "10";

                TextBox setupSkilledLabourTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupSkilledLabourTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupSkilledLabourTextBox, maxAllowedValueOfDecimalFields, true);
                setupUnskilledLabourTextBox.Text = "10";

                TextBox setupForemanTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupForemanTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupForemanTextBox, maxAllowedValueOfDecimalFields, true);
                setupForemanTextBox.Text = "10";

                TextBox setupTechniciansTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTechniciansTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupTechniciansTextBox, maxAllowedValueOfDecimalFields, true);
                setupTechniciansTextBox.Text = "10";

                TextBox setupEngineersTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupEngineersTextBox);
                ValidationHelper.CheckNumericFieldValidators(mainWindow, setupEngineersTextBox, maxAllowedValueOfDecimalFields, true);
                setupEngineersTextBox.Text = "10";

                //step 47
                TabPage additionalSettingsTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.AdditionalSettingsTabItem);
                additionalSettingsTab.Click();
                //step 48
                CheckBox externalCheckBox = mainWindow.Get<CheckBox>(ProcessStepAutomationIds.ExternalCheckBox);
                externalCheckBox.Checked = true;
                TextBox transportCostTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.TransportCostTextBox);
                transportCostTextBox.Text = "10";
                //step 50
                TabPage machinesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.MachinesTabList);
                machinesTab.Click();

                Button addMachineButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddMachineButton);
                addMachineButton.Click();

                Window createMachinesWindow = app.Windows.MachineWindow;
                name = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.MachineNameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "1machine";

                TextBox depreciationPeriodTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DepreciationPeriodTextBox);
                depreciationPeriodTextBox.Text = "10";

                TextBox depreciationRateTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DepreciationRateTextBox);
                Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Rate TextBox was not found.");
                depreciationPeriodTextBox.Text = "10";

                TextBox registrationNumberTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.RegistrationNumberTextBox);

                TextBox descriptionOfFunctionTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.DescriptionOfFunctionTextBox);
                descriptionOfFunctionTextBox.Text = "machines_description";

                manufacturerTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.Manufacturer);                
                manufacturerTab.Click();

                manufacturerName = createMachinesWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TabPage floorEnergy = mainWindow.Get<TabPage>(AutomationIds.FloorEnergyData);                
                floorEnergy.Click();

                TextBox floorSizeTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.FloorSizeTextBox);
                floorSizeTextBox.Text = "10";

                TextBox workspaceAreaTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.WorkspaceAreaTextBox);
                workspaceAreaTextBox.Text = "10";

                TextBox powerConsumptionTextBox = createMachinesWindow.Get<TextBox>(ProcessStepAutomationIds.PowerConsumptionTextBox);
                powerConsumptionTextBox.Text = "10";
                saveButton = createMachinesWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                //step 51
                TabPage diesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.DiesTabItem);
                diesTab.Click();

                Button addDieButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddDieButton);
                addDieButton.Click();

                DieTestsHelper.CreateDie(app);

                TabPage consumablesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.ConsumablesTabItem);
                consumablesTab.Click();

                Button addConsumableButton = mainWindow.Get<Button>(ProcessStepAutomationIds.AddConsumableButton);
                addConsumableButton.Click();

                ConsumableTestsHelper.CreateConsumable(app);

                TabPage commoditiesTab = mainWindow.Get<TabPage>(ProcessStepAutomationIds.CommoditiesTabItem);
                commoditiesTab.Click();

                Button addCommodity = mainWindow.Get<Button>(ProcessStepAutomationIds.AddCommodityButton);
                addCommodity.Click();

                CommodityTestsHelper.CreateCommodity(app);

                //step 52
                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                project1Node = myProjectsNode.GetNode("projectAssembly");
                project1Node.Click();
                project1Node.ExpandEx();
                TreeNode assemblyNode = project1Node.GetNode("assemblyStep");
                assemblyNode.ExpandEx();
                TreeNode resultDetails = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ResultDetailsNode);
                resultDetails.SelectEx();
                resultDetails.Click();
            }
        }
        #region Helper

        #endregion Helper
    }
}
