﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.ProcessStepTests
{
    class ProcessStepAutomationIds
    {

            public const string MachinesTab = "Machines";
            public const string MachinesDataGrid = "MachinesDataGrid";
            public const string CapacityUtilizationColumn = "CapacityUtilizationColumn";
            public const string CreatePartWindow = "Create Part";
            public const string ProjectNameTextBox = "NameTextBox";
            public const string SupplierTab = "Supplier";
            public const string SupplierNameTextBox = "NameTextBox";
            public const string BrowseCountryButton = "BrowseCountryMasterData";
            public const string CountryTextBox = "CountryTextBox";
            public const string Manufacturer = "ManufacturerTabItem";
            public const string DescriptionTextBox = "DescriptionTextBox";
            public const string AddNewStepButton = "AddNewStepButton";
            public const string ProcessStepTabControl = "ProcessStepTabControl";
            public const string AccuracyComboBox = "AccuracyComboBox";
            public const string EstimatedCostTextBox = "EstimatedCostTextBox";
            public const string ExceedShiftCostCheckbox = "ExceedShiftCostCheckbox";
            public const string ExtraShiftsNumberTextBox = "ExtraShiftsNumberTextBox";
            public const string ProdUnskilledLabourTextBox = "ProdUnskilledLabourTextBox";
            public const string ProdForemanTextBox = "ProdForemanTextBox";
            public const string ProdSkilledLabourTextBox = "ProdSkilledLabourTextBox";
            public const string ProdTechniciansTextBox = "ProdTechniciansTextBox";
            public const string SetupSkilledLabourTextBox = "SetupSkilledLabourTextBox";
            public const string SetupUnskilledLabourTextBox = "SetupUnskilledLabourTextBox";
            public const string SetupForemanTextBox = "SetupForemanTextBox";
            public const string SetupEngineersTextBox = "SetupEngineersTextBox";
            public const string ProdEngineersTextBox = "ProdEngineersTextBox";
            public const string SetupTechniciansTextBox = "SetupTechniciansTextBox";
            public const string ExternalCheckBox = "ExternalCheckBox";
            public const string MachineNameTextBox = "MachineNameTextBox";
            public const string DepreciationPeriodTextBox = "DepreciationPeriodTextBox";
            public const string DepreciationRateTextBox = "DepreciationRateTextBox";
            public const string RegistrationNumberTextBox = "RegistrationNumberTextBox";
            public const string DescriptionOfFunctionTextBox = "DescriptionOfFunctionTextBox";
            public const string AddMachineButton = "AddMachineButton";
            public const string AddDieButton = "AddDieButton";
            public const string CreateDieWindow = "dieWindow";
            public const string InvestmentTextBox = "InvestmentTextBox";
            public const string DieTypeComboBox = "DieTypeComboBox";
            public const string FloorEnergy = "FloorEnergyData";
            public const string FloorSizeTextBox = "FloorSizeTextBox";
            public const string WorkspaceAreaTextBox = "WorkspaceAreaTextBox";
            public const string AirConsumptionTextBox = "AirConsumptionTextBox";
            public const string PowerConsumptionTextBox = "PowerConsumptionTextBox";
            public const string TransportCostTextBox = "TransportCostTextBox";
            public const string AddCommodityButton = "AddCommodityButton";
            public const string PartNumberTextBox = "PartNumberTextBox";
            public const string PriceTextBox = "PriceTextBox";
            public const string AmountTextBox = "AmountTextBox";
            public const string WeightTextBox = "WeightTextBox";
            public const string RemarksTextBox = "RemarksTextBox";
            public const string AddConsumableButton = "AddConsumableButton";
            public const string ProcessTimeTextBox = "ProcessTimeTextBox";
            public const string CycleTimeTextBox = "CycleTimeTextBox";
            public const string BatchSizeTextBox = "BatchSizeTextBox";
            public const string ExceededShiftCostRatioTextBox = "ExceededShiftCostRatioTextBox";
            public const string MachinesTabList = "MachinesListTab";
            public const string DiesTabItem = "DiesTabItem";
            public const string ConsumablesTabItem = "ConsumablesTabItem";
            public const string CommoditiesTabItem = "CommoditiesTabItem";
            public const string AdditionalSettingsTabItem = "AdditionalSettingsTabItem";
            public const string ProjectSupplierTab = "SupplierTab";
            public const string ProcessStepInfoTab = "ProcessStepInfoTab";
            public const string LaborSettingsTabItem = "LaborSettingsTabItem";
            public const string ProcessViewTab = "ProcessViewTab";
            public const string ProcessNode = "ProcessNode";
            public const string ResultDetailsNode = "ResultDetailsNode";
            public const string PartsPerCycleTextBox = "PartsPerCycleTextBox";
            public const string RejectTextBox = "AmountScrap";
            public const string SetupsPerBatchTextBox = "SetupsPerBatchTextBox";
            public const string MaxDowntimeTextBox = "MaxDowntimeTextBox";
            public const string SetupTimeTextBox = "SetupTimeTextBox";
            public const string ManufacturingOHTextBox = "ManufacturingOHTextBox";
    }
}
