﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    [Binding]
    public class CancelCreateRawMaterialSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CancelCreateRawMaterial")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);            
            importProjectMenuItem.Click();
            
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectPartPictures.project"));;
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CancelCreateRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select Materials node")]
        public void GivenISelectMaterialsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectPartPictures");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partPictures");
            partNode.SelectEx();
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();
        }
        
        [Given(@"I press Add Raw Material button")]
        public void GivenIPressAddRawMaterialButton()
        {
            Button addRawMaterialButton = mainWindow.Get<Button>("addRawMaterialButton");
            addRawMaterialButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press the Cancel button on create raw material")]
        public void GivenIPressTheCancelButtonOnCreateRawMaterial()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I press again the Add Raw Material")]
        public void GivenIPressAgainTheAddRawMaterial()
        {
            Button addRawMaterialButton = mainWindow.Get<Button>("addRawMaterialButton");
            addRawMaterialButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I fill some fields from create raw material win and press Cancel")]
        public void GivenIFillSomeFieldsFromCreateRawMaterialWinAndPressCancel()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "rawMaterialNode";

            Button cancelButton = createRawMaterialWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I press the No")]
        public void GivenIPressTheNo()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I press the Cancel btn again")]
        public void GivenIPressTheCancelBtnAgain()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            Button cancelButton = createRawMaterialWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I press \(X\) in confirmation win")]
        public void GivenIPressXInConfirmationWin()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Given(@"I Press Cancel")]
        public void GivenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I select Yes")]
        public void GivenISelectYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }
        
        [Given(@"I press Add Raw Material")]
        public void GivenIPressAddRawMaterial()
        {
            Button addRawMaterialButton = mainWindow.Get<Button>("addRawMaterialButton");
            addRawMaterialButton.Click();   
        }
        
        [Given(@"I close the create raw material window")]
        public void GivenICloseTheCreateRawMaterialWindow()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.Close();
        }
        
        [Given(@"I press Add Raw material window")]
        public void GivenIPressAddRawMaterialWindow()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectPartPictures");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partPictures");
            partNode.SelectEx();
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();
            Button addRawMaterialButton = mainWindow.Get<Button>("addRawMaterialButton");
            addRawMaterialButton.Click();      
        }
        
        [Given(@"I fill fields and close the window")]
        public void GivenIFillFieldsAndCloseTheWindow()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "rawMaterialNode";

            createRawMaterialWindow.Close();
        }
        
        [Given(@"I press the No btn")]
        public void GivenIPressTheNoBtn()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [When(@"I press the Yes button in confirmation screen")]
        public void WhenIPressTheYesButtonInConfirmationScreen()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.Close();
            createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }
        
        [Then(@"the create raw material window is closed")]
        public void ThenTheCreateRawMaterialWindowIsClosed()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            Assert.IsNull(createRawMaterialWindow, "Create raw material window should be closed");
        }
    }
}
