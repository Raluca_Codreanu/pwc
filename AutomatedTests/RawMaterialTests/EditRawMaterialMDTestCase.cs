﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using System.Threading;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    /// <summary>
    /// Summary description for EditRawMaterialMDTestCase
    /// </summary>
    [TestClass]
    public class EditRawMaterialMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditRawMaterialMDTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        /// <summary>
        /// An automated test for Update Raw Material Master Data Test Case (Test Link id = 250)
        /// </summary
        [TestMethod]
        [TestCategory("RawMaterial")]
        public void EditRawMaterialMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                rawMaterialsNode.Click();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");
                rawMaterialsDataGrid.Select("Name", "1raw");

                //step 1
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Add button was not found.");
                editButton.Click();
                Window createRawMaterialWindow = app.Windows.RawMaterialWindow;

                //step 2
                TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "1rawMaterial_Updated";
                TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
                UKNameTextBox.Text = "test_Updated";

                TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
                USNameTextBox.Text = "test_Updated";

                TextBox normNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.NormNameTextBox);
                normNameTextBox.Text = "test_Updated";

                //step 3
                ComboBox deliveryTypeComboBox = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.DeliveryTypeComboBox);
                Assert.IsNotNull(deliveryTypeComboBox, "Delivery combo box can not be found");
                deliveryTypeComboBox.SelectItem(2);

                TextBox priceTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PriceTextBox);
                Assert.IsNotNull(priceTextBox, "Price Text box was not found");
                priceTextBox.Text = "5";
                //step 4
                TextBox partWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PartWeightTextBox);
                Assert.IsNotNull(partWeightTextBox, "Part Weight text box eas not found");
                partWeightTextBox.Text = "5";

                TextBox materialWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaterialWeightTextBox);
                materialWeightTextBox.Text = "5";

                TextBox lossTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.LossTextBox);
                Assert.IsNotNull(lossTextBox, "Loss Value Text box was not found");
                lossTextBox.Text = "5";
                TextBox scrapRefundRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.ScrapRefundRatioTextBox);
                scrapRefundRatioTextBox.Text = "5";
                TextBox rejectRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RejectRatioTextBox);
                rejectRatioTextBox.Text = "5";
                TextBox recyclingRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RecyclingRatioTextBox);
                Assert.IsNotNull(recyclingRatioTextBox, "Recycling Ratio text box eas not found");
                recyclingRatioTextBox.Text = "5";
                
                //step 6
                CheckBox stockKeepingCheckbox = createRawMaterialWindow.Get<CheckBox>(RawMaterialAutomationIds.StockKeepingCheckbox);
                stockKeepingCheckbox.Checked = false;

                //step5
                ComboBox scrapCalculationTypeCombo = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.ScrapCalculationTypeCombo);
                scrapCalculationTypeCombo.Click();
                scrapCalculationTypeCombo.Items.Item(0).Select();

                TextBox remarksTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RemarksTextBox);
                remarksTextBox.Text = "test_Updated_remark";
                
                //step 8
                var mediaControl = createRawMaterialWindow.GetMediaControl();
                mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);
                mediaControl.ClickDelete();
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 9
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox manufacturerName = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest_Updated";

                TextBox descriptionTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "update description of manufacturer";

                //step 10
                TabPage propertiesTab = mainWindow.Get<TabPage>(AutomationIds.PropertiesTabItem);                
                propertiesTab.Click();

                TextBox yieldStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.YieldStrengthTextBox);
                yieldStrengthTextBox.Text = "5";

                TextBox ruptureStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RuptureStrengthTextBox);
                ruptureStrengthTextBox.Text = "5";

                TextBox densityTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.DensityTextBox);
                densityTextBox.Text = "5";
                TextBox maxElongationTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaxElongationTextBox);
                maxElongationTextBox.Text = "5";
                TextBox glassTemperatureTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.GlassTemperatureTextBox);
                glassTemperatureTextBox.Text = "5";

                TextBox rxTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RxTextBox);
                rxTextBox.Text = "5";

                TextBox rmTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RmTextBox);
                rmTextBox.Text = "5";
                //step 11
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
                Thread.Sleep(500);

                //step 12
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();
                rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                rawMaterialsNode.Click();
                mainWindow.WaitForAsyncUITasks();
                rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");
                rawMaterialsDataGrid.Select("Name", "1rawMaterial_Updated");                
            }
        }

        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = "1raw";
            rawMaterial1.SetOwner(adminUser);
            rawMaterial1.IsMasterData = true;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            rawMaterial1.Manufacturer = manufacturer1;

            dataContext.RawMaterialRepository.Add(rawMaterial1);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
