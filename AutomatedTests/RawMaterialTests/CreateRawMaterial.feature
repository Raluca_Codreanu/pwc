﻿@CreateRawMaterial
Feature: CreateRawMaterial
	I want to test the create raw material functionality

Scenario: CreateRawMaterial
	Given I press the Add Raw Material button
	And I fill the Name field
	And I fill the Uk Name, US Name 
	And I fill the Norm Name
	And I select a delivery type
	And I enter valid values for Price, Part Weight, Material Weight
	And I fill the fileds: Loss, Reject ratio
	And I add a media file on media panel
	And I fill the Name in Manufacturer tab
	And I fill the fields form Properties tab	
	When I press the Save button from Create Raw Material node
	Then I logout
	And  I login
	And check that the Raw Material node is created in the Materials node
