﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    /// <summary>
    /// Summary description for CreateRawMaterialMDTestCase
    /// </summary>
    [TestClass]
    public class CreateRawMaterialMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        /// <summary>
        /// An automated test for Create Raw Material Master Data Test Case (Test Link id = 248)
        /// </summary
        [TestMethod]
        [TestCategory("RawMaterial")]
        public void CreateRawMaterialMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                rawMaterialsNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                RawMaterialTestsHelper.CreateRawMaterial(app);

                //step 16
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                rawMaterialsNode.Click();
                mainWindow.WaitForAsyncUITasks();
                rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");

                rawMaterialsDataGrid.Select("Name", "rawMaterial1");
            }
        }

        /// <summary>
        /// An automated test for Cancel Create Raw Material - Master Data (test link id = 247)
        /// </summary
        [TestMethod]
        [TestCategory("RawMaterial")]
        public void CancelCreateRawMaterialMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                rawMaterialsNode.Click();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                Window createRawMaterialWindow = app.Windows.RawMaterialWindow;

                //step 2
                Button cancelButton = createRawMaterialWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3                
                addButton.Click();
                createRawMaterialWindow = app.Windows.RawMaterialWindow;

                TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "rawMaterial1";
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox manufacturerName = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                cancelButton = createRawMaterialWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                
                //step 4
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 5                
                cancelButton.Click();                
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 6                
                cancelButton.Click();
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 7                
                addButton.Click();
                createRawMaterialWindow = app.Windows.RawMaterialWindow;
                createRawMaterialWindow.Close();

                //step 8                
                addButton.Click();

                createRawMaterialWindow = app.Windows.RawMaterialWindow;
                name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(name, "Name text box was not found");
                name.Text = "rawMaterial1";

                TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
                UKNameTextBox.Text = "test";

                TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
                USNameTextBox.Text = "test";

                TextBox normNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.NormNameTextBox);
                normNameTextBox.Text = "test";

                ComboBox deliveryTypeComboBox = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.DeliveryTypeComboBox);
                Assert.IsNotNull(deliveryTypeComboBox, "Delivery combo box can not be found");

                deliveryTypeComboBox.SelectItem(1);
                TextBox priceTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PriceTextBox);
                Assert.IsNotNull(priceTextBox, "Price Text box was not found");
                priceTextBox.Text = "10";

                TextBox partWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PartWeightTextBox);
                Assert.IsNotNull(partWeightTextBox, "Part Weight text box was not found");
                partWeightTextBox.Text = "5";
                createRawMaterialWindow.Close();

                //step 9
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                createRawMaterialWindow.WaitForAsyncUITasks();
                //step 10                
                createRawMaterialWindow.Close();
                createRawMaterialWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper
        #endregion Helper
    }
}

