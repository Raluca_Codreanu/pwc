﻿@CancelUpdateRawMaterialMD
Feature: CancelUpdateRawMaterialMD

Scenario: Cancel Update Raw Material from Master Data
	Given I select a raw material
	And I press the Edit btn 
	And I click the Cancel button 
	And I press the Edit again
	And I change some values
	And I press the Cancel btn
	And I press the No button from confirmation window
	And press again Cancel
	And I close the confirmation window using X
	And I click the Cancel button
	And I press the Yes 
	And I press Edit
	And I change some field's values
	And I close the edit window
	And I press No
	And I press Edit
	And I change some values
	And I close (X) the window
	And I close the confirmation screen
	And I press edit
	And I change some fields
	And I press the x button
	When I press Yes in confimation screen
	Then confirmation screen in closed
