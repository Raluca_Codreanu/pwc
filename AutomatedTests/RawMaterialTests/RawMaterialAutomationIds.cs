﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    public static class RawMaterialAutomationIds
    {
        public const string LastModifiedLabel = "LastModifiedLabel";
        public const string CreatePartWindow = "Create Part";
        public const string InvestmentTextBox = "InvestmentTextBox";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string CalculationStatusComboBox = "CalculationStatusComboBox";
        public const string EstimatedCostTextBox = "EstimatedCostTextBox";
        public const string CalculationAccuracyComboBox = "CalculationAccuracyComboBox";
        public const string ManufacturingRatioTextBox = "ManufacturingRatioTextBox";
        public const string CalculationApproachComboBox = "CalculationApproachComboBox";
        public const string CalculationVariantComboBox = "CalculationVariantComboBox";
        public const string AllDiesPaydByCustomerCheckBox = "AllDiesPaydByCustomerCheckBox";
        public const string LifeTimeTextBox = "LifeTimeTextBox";
        public const string CostAllocationBasedOnNbOfPartsTextBox = "CostAllocationBasedOnNbOfPartsTextBox";
        public const string CalculatorComboBox = "CalculatorComboBox";
        public const string YearlyProdQtyTextBox = "YearlyProdQtyTextBox";
        public const string RemarkTextBox = "RemarkTextBox";
        public const string PurchasePriceTextBox = "PurchasePriceTextBox";
        public const string TargetPriceTextBox = "TargetPriceTextBox";
        public const string DeliveryTypeComboBox = "DeliveryTypeComboBox";
        public const string AssetRateTextBox = "AssetRateTextBox";
        public const string BrowseCountryButton = "BrowseCountryMasterData";
        public const string CountryTextBox = "CountryTextBox";
        public const string CreateRawMaterialWindow = "Create Raw Material";
        public const string UKNameTextBox = "UKNameTextBox";
        public const string USNameTextBox = "USNameTextBox";
        public const string NormNameTextBox = "NormNameTextBox";
        public const string PriceTextBox = "PriceTextBox";
        public const string PartWeightTextBox = "PartWeightTextBox";
        public const string MaterialWeightTextBox = "MaterialWeightTextBox";
        public const string LossTextBox = "LossTextBox";
        public const string RatioOfAllocation = "RatioOfAllocation";
        public const string ScrapRefundRatioTextBox = "ScrapRefundRatioTextBox";
        public const string RejectRatioTextBox = "RejectRatioTextBox";
        public const string RecyclingRatioTextBox = "RecyclingRatioTextBox";
        public const string StockKeepingCheckbox = "StockKeepingCheckbox";
        public const string Manufacturer = "Manufacturer";
        public const string StockKeepingTextBox = "StockKeepingTextBox";
        public const string ScrapCalculationTypeCombo = "ScrapCalculationTypeCombo";
        public const string RemarksTextBox = "RemarksTextBox";
        public const string Properties = "Properties";
        public const string RuptureStrengthTextBox = "RuptureStrengthTextBox";
        public const string YieldStrengthTextBox = "YieldStrengthTextBox";
        public const string MaxElongationTextBox = "MaxElongationTextBox";
        public const string DensityTextBox = "DensityTextBox";
        public const string GlassTemperatureTextBox = "GlassTemperatureTextBox";
        public const string RxTextBox = "RxTextBox";
        public const string RmTextBox = "RmTextBox";                        
    }
}
