﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using System.Threading;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    public static class RawMaterialTestsHelper
    {
        public static void CreateRawMaterial(ApplicationEx app)
        {
            Window createRawMaterialWindow = Wait.For(() => app.Windows.RawMaterialWindow);

            //step 2
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "rawMaterial1";

            //step 3 
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test";

            //step 4
            TextBox normNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.NormNameTextBox);
            normNameTextBox.Text = "test";

            //step 5
            ComboBox deliveryTypeComboBox = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.DeliveryTypeComboBox);
            Assert.IsNotNull(deliveryTypeComboBox, "Delivery combo box can not be found");
            deliveryTypeComboBox.SelectItem(1);

            //step 6
            TextBox priceTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PriceTextBox);
            Assert.IsNotNull(priceTextBox, "Price Text box was not found");
            priceTextBox.Text = "10";

            TextBox partWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PartWeightTextBox);
            Assert.IsNotNull(partWeightTextBox, "Part Weight text box eas not found");
            partWeightTextBox.Text = "5";

            TextBox materialWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaterialWeightTextBox);
            materialWeightTextBox.Text = "6";
            //step 7
            TextBox lossTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.LossTextBox);
            Assert.IsNotNull(lossTextBox, "Loss Value Text box was not found");
            lossTextBox.Text = "10";
            TextBox scrapRefundRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.ScrapRefundRatioTextBox);
            scrapRefundRatioTextBox.Text = "10";
            TextBox rejectRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RejectRatioTextBox);
            rejectRatioTextBox.Text = "10";
            TextBox recyclingRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RecyclingRatioTextBox);
            Assert.IsNotNull(recyclingRatioTextBox, "Recycling Ratio text box eas not found");
            recyclingRatioTextBox.Text = "10";

            //step 8
            CheckBox stockKeepingCheckbox = createRawMaterialWindow.Get<CheckBox>(RawMaterialAutomationIds.StockKeepingCheckbox);
            stockKeepingCheckbox.Checked = true;
            TextBox stockKeepingTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.StockKeepingTextBox);
            stockKeepingTextBox.Text = "10";

            //GroupBox classificationGroupBox = createRawMaterialWindow.Get<GroupBox>("ClassificationGroupBox");
            //Button openClassification = createRawMaterialWindow.Get<Button>("OpenClassificationTreeButton");
            //openClassification.Click();

            //step 9
            ComboBox scrapCalculationTypeCombo = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.ScrapCalculationTypeCombo);
            scrapCalculationTypeCombo.SelectItem(1);

            //step 11
            TextBox remarksTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RemarksTextBox);
            remarksTextBox.Text = "test_remark";

            //step 12
            createRawMaterialWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);

            //step 13             
            TabPage manufacturerTab = createRawMaterialWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            TextBox descriptionTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
            descriptionTextBox.Text = "description of manufacturer";

            //step 14
            TabPage propertiesTab = createRawMaterialWindow.Get<TabPage>(AutomationIds.PropertiesTabItem);
            propertiesTab.Click();

            TextBox yieldStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.YieldStrengthTextBox);
            yieldStrengthTextBox.Text = "10";

            TextBox ruptureStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RuptureStrengthTextBox);
            ruptureStrengthTextBox.Text = "10";

            TextBox densityTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.DensityTextBox);
            densityTextBox.Text = "10";
            TextBox maxElongationTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaxElongationTextBox);
            maxElongationTextBox.Text = "10";
            TextBox glassTemperatureTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.GlassTemperatureTextBox);
            glassTemperatureTextBox.Text = "10";

            TextBox rxTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RxTextBox);
            rxTextBox.Text = "10";

            TextBox rmTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RmTextBox);
            rmTextBox.Text = "10";

            Button saveButton = createRawMaterialWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
    }
}
