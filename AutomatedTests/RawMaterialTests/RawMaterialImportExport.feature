﻿@ImportExportRawMaterial
Feature: RawMaterialImportExport
	I want be able to export a raw material and import it

Scenario: Import Export Raw Material
	Given I select the ASA material
	And I choose the Export option
	And I press the Cancel button from Save As window
	And I select the Export option again
	And I press Save button
	And I select the Materials node to import a material
	When I select the ASA material to be imported
	Then the Raw Material is imported
