﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    [Binding]
    public class CreateRawMaterialSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CreateRawMaterial")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CreateRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press the Add Raw Material button")]
        public void GivenIPressTheAddRawMaterialButton()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("part1");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            Button addRawMaterialButton = mainWindow.Get<Button>("addRawMaterialButton");
            addRawMaterialButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I fill the Name field")]
        public void GivenIFillTheNameField()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "rawMaterialNode";
        }

        [Given(@"I fill the Uk Name, US Name")]
        public void GivenIFillTheUkNameUSName()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test";
        }

        [Given(@"I fill the Norm Name")]
        public void GivenIFillTheNormName()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox normNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.NormNameTextBox);
            normNameTextBox.Text = "test";
        }

        [Given(@"I select a delivery type")]
        public void GivenISelectADeliveryType()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            ComboBox deliveryTypeComboBox = createRawMaterialWindow.Get<ComboBox>(RawMaterialAutomationIds.DeliveryTypeComboBox);
            Assert.IsNotNull(deliveryTypeComboBox, "Delivery combo box can not be found");
            deliveryTypeComboBox.Items.Item(1).Select();
        }

        [Given(@"I enter valid values for Price, Part Weight, Material Weight")]
        public void GivenIEnterValidValuesForPricePartWeightMaterialWeight()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;

            TextBox priceTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PriceTextBox);
            Assert.IsNotNull(priceTextBox, "Price Text box was not found");
            priceTextBox.Text = "10";

            TextBox partWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.PartWeightTextBox);
            Assert.IsNotNull(partWeightTextBox, "Part Weight text box eas not found");
            partWeightTextBox.Text = "5";

            TextBox materialWeightTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaterialWeightTextBox);
            materialWeightTextBox.Text = "6";
        }

        [Given(@"I fill the fileds: Loss, Reject ratio")]
        public void GivenIFillTheFiledsLossRejectRatio()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox lossTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.LossTextBox);
            Assert.IsNotNull(lossTextBox, "Loss Value Text box was not found");
            lossTextBox.Text = "10";
            TextBox rejectRatioTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RejectRatioTextBox);
            rejectRatioTextBox.Text = "10";
        }

        [Given(@"I add a media file on media panel")]
        public void GivenIAddAMediaFileOnMediaPanel()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);
        }

        [Given(@"I fill the Name in Manufacturer tab")]
        public void GivenIFillTheNameInManufacturerTab()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";
        }

        [Given(@"I fill the fields form Properties tab")]
        public void GivenIFillTheFieldsFormPropertiesTab()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TabPage propertiesTab = mainWindow.Get<TabPage>(AutomationIds.PropertiesTabItem);
            propertiesTab.Click();

            TextBox yieldStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.YieldStrengthTextBox);
            yieldStrengthTextBox.Text = "9";

            TextBox ruptureStrengthTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RuptureStrengthTextBox);
            ruptureStrengthTextBox.Text = "9";

            TextBox densityTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.DensityTextBox);
            densityTextBox.Text = "9";
            TextBox maxElongationTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.MaxElongationTextBox);
            maxElongationTextBox.Text = "9";
            TextBox glassTemperatureTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.GlassTemperatureTextBox);
            glassTemperatureTextBox.Text = "9";

            TextBox rxTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RxTextBox);
            rxTextBox.Text = "9";

            TextBox rmTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.RmTextBox);
            rmTextBox.Text = "9";
        }

        [When(@"I press the Save button from Create Raw Material node")]
        public void WhenIPressTheSaveButtonFromCreateRawMaterialNode()
        {
            Button saveButton = application.Windows.RawMaterialWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }

        [Then(@"I logout")]
        public void ThenILogout()
        {
            application.MainMenu.System_Logout.Click();
        }

        [Then(@"I login")]
        public void ThenILogin()
        {
            application.LoginAndGoToMainScreen("admin", "admin");
        }

        [Then(@"check that the Raw Material node is created in the Materials node")]
        public void ThenCheckThatTheRawMaterialNodeIsCreatedInTheMaterialsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("part1");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterialNode");
            rawMaterialNode.SelectEx();
        }
    }
}
