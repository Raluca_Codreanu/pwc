﻿@UpdateRawMaterial
Feature: UpdateRawMaterial

Scenario: Update Raw Material
	Given I select a Raw Material
	And I change some fields from General tab
	And I chnage some fields from Manufacturer tab
	And I change some fields from Properties tab
	When I press the Save btn 
	Then the new entered values are saved
