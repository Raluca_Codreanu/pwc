﻿@CancelCreateRawMaterial
Feature: CancelCreateRawMaterial

Scenario: Cancel Create Raw Material
	Given I select Materials node
	And I press Add Raw Material button
	And I press the Cancel button on create raw material
	And I press again the Add Raw Material 
	And I fill some fields from create raw material win and press Cancel
	And I press the No 
	And I press the Cancel btn again
	And I press (X) in confirmation win
	And I Press Cancel 
	And I select Yes
	And I press Add Raw Material 
	And I close the create raw material window
	And I press Add Raw material window
	And I fill fields and close the window	
	And I press the No btn
	And I close the create raw material window
	When I press the Yes button in confirmation screen
	Then the create raw material window is closed
