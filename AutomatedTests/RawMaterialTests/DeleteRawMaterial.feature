﻿@DeleteRawMaterial
Feature: DeleteRawMaterial

Scenario: Delete Raw Material
	Given I select the material to choose Delete option
	And from confirmation screen I press No 
	And I press Delete button 
	And I press Yes
	And I select the Trash Bin
	And I select the deleted raw material
	And I press the Recover button
	And I select the raw material in main tree view
	And I press Delete button
	And I press the Yes button from confirmation
	And I select the Trash Bin
	And I select the raw material
	When I press Permanently Delete button	
	Then the raw material disappears from Trah bin
