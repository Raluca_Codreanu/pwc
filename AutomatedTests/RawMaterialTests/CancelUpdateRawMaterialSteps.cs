﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.RawMaterialTests;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CancelUpdateRawMaterialSteps
    {
        // test id = 244
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CancelUpdateRawMaterial")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
            
            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRawMaterial.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CancelUpdateRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
        }


        [Given(@"I press Cancel btn")]
        public void GivenIPressCancelBtn()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();

            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterial");
            rawMaterialNode.SelectEx();
        }
        
        [Given(@"I change some of the fields")]
        public void GivenIChangeSomeOfTheFields()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "rawMaterial_Updated";

            TextBox UKNameTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test";

            TextBox USNameTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test";
        }
        
        [Given(@"I press cancel")]
        public void GivenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press no buton")]
        public void GivenIPressNoButon()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I click cancel")]
        public void GivenIClickCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();  
        }
        
        [Given(@"I close X the confirmation screen")]
        public void GivenICloseXTheConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Given(@"I click the Yes")]
        public void GivenIClickTheYes()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Given(@"I clear the name field")]
        public void GivenIClearTheNameField()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = string.Empty;
        }
        
        [Given(@"I select the Materials node")]
        public void GivenISelectTheMaterialsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            
            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();

            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx(); 
        }
        
        [Given(@"I click the no button")]
        public void GivenIClickTheNoButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Given(@"I go back to the edited raw material")]
        public void GivenIGoBackToTheEditedRawMaterial()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();

            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterial");
            rawMaterialNode.SelectEx();

            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = string.Empty;

        }
        
        [Given(@"I close the confirm screen")]
        public void GivenICloseTheConfirmScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).Close();
        }
        
        [Given(@"I select the edited raw material")]
        public void GivenISelectTheEditedRawMaterial()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();

            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterial");
            rawMaterialNode.SelectEx();

            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();   

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = string.Empty;
        }
        
        [Given(@"I press Yes in the confirm screen")]
        public void GivenIPressYesInTheConfirmScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickYes();
        }
        
        [Given(@"I fill the name field")]
        public void GivenIFillTheNameField()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "aaa";
        }
        
        [Given(@"I press the home button")]
        public void GivenIPressTheHomeButton()
        {
            Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
            homeButton.Click();   
        }
        
        [When(@"I press No in the confirmation screen")]
        public void WhenIPressNoInTheConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Then(@"the user logs out")]
        public void ThenTheUserLogsOut()
        {
            var welcomeScreen = Wait.For(() => application.WelcomeScreen);
            Button skipButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
            skipButton.Click();
            mainWindow.WaitForAsyncUITasks();        
        }
    }
}
