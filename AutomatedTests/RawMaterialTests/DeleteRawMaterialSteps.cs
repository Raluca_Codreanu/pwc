﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Automation;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    [Binding]
    public class DeleteRawMaterialSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteRawMaterial")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDeleteMaterial.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the material to choose Delete option")]
        public void GivenISelectTheMaterialToChooseDeleteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDeleteMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partDeleteMaterial");
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();

            TreeNode rawMaterial = materialsNode.GetNode("deleteMaterial");
            rawMaterial.SelectEx();

            //step 12
            Menu deleteMenuItem = rawMaterial.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();            
        }
        
        [Given(@"from confirmation screen I press No")]
        public void GivenFromConfirmationScreenIPressNo()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I press Delete button")]
        public void GivenIPressDeleteButton()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDeleteMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partDeleteMaterial");
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            ListViewControl metarialsDataGrid = mainWindow.GetListView(AutomationIds.MaterialsDataGrid);
            metarialsDataGrid.Select("Name", "deleteMaterial");

            Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
            deleteButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press Yes")]
        public void GivenIPressYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the Trash Bin")]
        public void GivenISelectTheTrashBin()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");
            trashBinNode.SelectEx();
            trashBinNode.Click();
        }
        
        [Given(@"I select the deleted raw material")]
        public void GivenISelectTheDeletedRawMaterial()
        {          
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

            ListViewRow materialRow = trashBinDataGrid.Row("Name", "deleteMaterial");
            Assert.IsNotNull(materialRow, "Project1Row trash bin item was not found.");

            ListViewCell selectedCell = trashBinDataGrid.Cell("Selected", materialRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;

            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press the Recover button")]
        public void GivenIPressTheRecoverButton()
        {
            Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            Assert.IsNotNull(recoverButton, "Recover button was not found.");
            recoverButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
            mainWindow.WaitForAsyncUITasks(); 
        }
        
        [Given(@"I select the raw material in main tree view")]
        public void GivenISelectTheRawMaterialInMainTreeView()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDeleteMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partDeleteMaterial");
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            ListViewControl metarialsDataGrid = mainWindow.GetListView(AutomationIds.MaterialsDataGrid);
            metarialsDataGrid.Select("Name", "deleteMaterial");

            Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
            deleteButton.Click();             
        }
        
        [Given(@"I press the Yes button from confirmation")]
        public void GivenIPressTheYesButtonFromConfirmation()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
            mainWindow.WaitForAsyncUITasks();            
        }
        
        [Given(@"I select the raw material")]
        public void GivenISelectTheRawMaterial()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");
            trashBinNode.SelectEx();
            trashBinNode.Click();

            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

            ListViewRow materialRow = trashBinDataGrid.Row("Name", "deleteMaterial");
            Assert.IsNotNull(materialRow, "Project1Row trash bin item was not found.");

            ListViewCell selectedCell = trashBinDataGrid.Cell("Selected", materialRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;          
        }
        
        [When(@"I press Permanently Delete button")]
        public void WhenIPressPermanentlyDeleteButton()
        {
            Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
            Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
            Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

            permanentlyDeleteButton.Click();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the raw material disappears from Trah bin")]
        public void ThenTheRawMaterialDisappearsFromTrahBin()
        {
            var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
            Assert.IsNull(trashBinDataGrid.Row("Name", "deleteMaterial"), "The raw material was deleted permanently and it doesn't appear on the Trash Bin.");
        }
    }
}
                          