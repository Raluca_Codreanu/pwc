﻿@CancelUpdateRawMaterial
Feature: CancelUpdateRawMaterial

Scenario: Cancel Update the Raw Material
	Given I press Cancel btn
	And I change some of the fields
	And I press cancel
	And I press no buton
	And I click cancel 
	And I close X the confirmation screen
	And I click cancel
	And I click the Yes 
	And I clear the name field
	And I select the Materials node
	And I click the no button
	And I go back to the edited raw material
	And I clear the name field
	And I select the Materials node
	And I close the confirm screen
	And I select the edited raw material
	And I clear the name field
	And I select the Materials node
	And I press Yes in the confirm screen
	And I fill the name field
	And I press the home button 
	When I press No in the confirmation screen
	Then the user logs out
