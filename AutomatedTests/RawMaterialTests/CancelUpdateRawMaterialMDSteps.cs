﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    [Binding]
    public class CancelUpdateRawMaterialMDSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = "1rawMaterialUpdated";
            rawMaterial1.SetOwner(adminUser);
            rawMaterial1.IsMasterData = true;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            rawMaterial1.Manufacturer = manufacturer1;

            dataContext.RawMaterialRepository.Add(rawMaterial1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CancelUpdateRawMaterialMD")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;            
        }

        [AfterFeature("CancelUpdateRawMaterialMD")]
        private static void KillApplication()
        {
            application.Kill();
        }


        [Given(@"I select a raw material")]
        public void GivenISelectARawMaterial()
        {
            TreeNode rawMaterialsNode = application.MainScreen.ProjectsTree.MasterDataRawMaterials;
            Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

            rawMaterialsNode.Click();
            mainWindow.WaitForAsyncUITasks();
            ListViewControl rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");

            rawMaterialsDataGrid.Select("Name", "1rawMaterialUpdated");   
        }
        
        [Given(@"I press the Edit btn")]
        public void GivenIPressTheEditBtn()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            Assert.IsNotNull(editButton, "The Delete button was not found.");
            editButton.Click();
        }
        
        [Given(@"I click the Cancel button")]
        public void GivenIClickTheCancelButton()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
        }
        
        [Given(@"I press the Edit again")]
        public void GivenIPressTheEditAgain()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I change some values")]
        public void GivenIChangeSomeValues()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "1rawMaterial_Updated";
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test_Updated";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test_Updated";

            TextBox normNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.NormNameTextBox);
            normNameTextBox.Text = "test_Updated";
        }
        
        [Given(@"I press the Cancel btn")]
        public void GivenIPressTheCancelBtn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press the No button from confirmation window")]
        public void GivenIPressTheNoButtonFromConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"press again Cancel")]
        public void GivenPressAgainCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();    
        }
        
        [Given(@"I close the confirmation window using X")]
        public void GivenICloseTheConfirmationWindowUsingX()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Given(@"I press the Yes")]
        public void GivenIPressTheYes()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Given(@"I press Edit")]
        public void GivenIPressEdit()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I change some field's values")]
        public void GivenIChangeSomeFieldSValues()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "1rawMaterial_Updated";
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test_Updated";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test_Updated";   
        }
        
        [Given(@"I close the edit window")]
        public void GivenICloseTheEditWindow()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.Close();
        }
        
        [Given(@"I press No")]
        public void GivenIPressNo()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Given(@"I close \(X\) the window")]
        public void GivenICloseXTheWindow()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();

            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = "1rawMaterial_Updated";
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test_Updated";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test_Updated";

            createRawMaterialWindow.Close();
        }
        
        [Given(@"I close the confirmation screen")]
        public void GivenICloseTheConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).Close();
        }
        
        [Given(@"I press edit")]
        public void GivenIPressEdit1()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I change some fields")]
        public void GivenIChangeSomeFields()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            TextBox name = createRawMaterialWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(name, "Name text box was not found");
            name.Text = string.Empty;
            TextBox UKNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test_Updated";

            TextBox USNameTextBox = createRawMaterialWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test_Updated";
        }
        
        [Given(@"I press the x button")]
        public void GivenIPressTheXButton()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            createRawMaterialWindow.Close();
        }
        
        [When(@"I press Yes in confimation screen")]
        public void WhenIPressYesInConfimationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickYes();
        }
        
        [Then(@"confirmation screen in closed")]
        public void ThenConfirmationScreenInClosed()
        {
            Window createRawMaterialWindow = application.Windows.RawMaterialWindow;
            Assert.IsNotNull(createRawMaterialWindow, "edit Raw Materials screen should be opened");
        }
    }
}
