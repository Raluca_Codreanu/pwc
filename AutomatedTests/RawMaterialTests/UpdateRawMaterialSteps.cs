﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    [Binding]
    public class UpdateRawMaterialSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("UpdateRawMaterial")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRawMaterial.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("UpdateRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a Raw Material")]
        public void GivenISelectARawMaterial()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();
            
            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterial");
            rawMaterialNode.SelectEx();
        }
        
        [Given(@"I change some fields from General tab")]
        public void GivenIChangeSomeFieldsFromGeneralTab()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);         
            name.Text = "rawMaterial_Updated";

            //step 3 
            TextBox UKNameTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.UKNameTextBox);
            UKNameTextBox.Text = "test";

            TextBox USNameTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.USNameTextBox);
            USNameTextBox.Text = "test";
        }
        
        [Given(@"I chnage some fields from Manufacturer tab")]
        public void GivenIChnageSomeFieldsFromManufacturerTab()
        {
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);            
            manufacturerTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);            
            nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

            TextBox manufacturerDescriptionTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.DescriptionTextBox);                                 manufacturerDescriptionTextBox.Text = "Description of Manufacturer";
        }
        
        [Given(@"I change some fields from Properties tab")]
        public void GivenIChangeSomeFieldsFromPropertiesTab()
        {
            TabPage propertiesTab = mainWindow.Get<TabPage>(AutomationIds.PropertiesTabItem);            
            propertiesTab.Click();

            TextBox yieldStrengthTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.YieldStrengthTextBox);
            yieldStrengthTextBox.Text = "9";

            TextBox ruptureStrengthTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.RuptureStrengthTextBox);
            ruptureStrengthTextBox.Text = "9";

            TextBox densityTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.DensityTextBox);
            densityTextBox.Text = "9";
            TextBox maxElongationTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.MaxElongationTextBox);
            maxElongationTextBox.Text = "9";
            TextBox glassTemperatureTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.GlassTemperatureTextBox);
            glassTemperatureTextBox.Text = "9";

            TextBox rxTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.RxTextBox);
            rxTextBox.Text = "9";

            TextBox rmTextBox = mainWindow.Get<TextBox>(RawMaterialAutomationIds.RmTextBox);
            rmTextBox.Text = "9";
        }
        
        [When(@"I press the Save btn")]
        public void WhenIPressTheSaveBtn()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Then(@"the new entered values are saved")]
        public void ThenTheNewEnteredValuesAreSaved()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectPartVideoNode = myProjectsNode.GetNode("projectRawMaterial");
            projectPartVideoNode.ExpandEx();

            TreeNode partNode = projectPartVideoNode.GetNode("partRawMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();

            TreeNode rawMaterialNode = materialsNode.GetNode("rawMaterial_Updated");
            rawMaterialNode.SelectEx();     
        }
    }
}
