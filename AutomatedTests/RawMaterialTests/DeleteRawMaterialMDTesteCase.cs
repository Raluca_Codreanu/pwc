﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.RawMaterialTests
{
    /// <summary>
    /// The automated test for Delete Raw Material Maste Data
    /// </summary>
    [TestClass]
    public class DeleteRawMaterialMDTesteCase
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="EditDieMasterDataTestCase"/> class.
        ///// </summary>
        //public EditDieMasterDataTestCase()
        //{
        //}

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteRawMaterialMDTesteCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Raw Material Master Data Test Case (testLink id = 35).
        /// </summary>
        [TestMethod]
        [TestCategory("RawMaterial")]
        public void DeleteRawMaterialMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 4
                TreeNode rawMaterialsNode = app.MainScreen.ProjectsTree.MasterDataRawMaterials;
                Assert.IsNotNull(rawMaterialsNode, "The Raw Materials node was not found.");

                //step 5
                rawMaterialsNode.Click();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl rawMaterialsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(rawMaterialsDataGrid, "Raw materials data grid was not found.");

                rawMaterialsDataGrid.Select("Name", "11raw");
                //step 6
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 8
                deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                //step 10      
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();
                var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "11raw"), "A raw Material from Master Data was deleted permanently and it doesn't appear on the Trash Bin.");
            }
        }

        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = "11raw";
            rawMaterial1.SetOwner(adminUser);
            rawMaterial1.IsMasterData = true;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            rawMaterial1.Manufacturer = manufacturer1;

            dataContext.RawMaterialRepository.Add(rawMaterial1);
            dataContext.SaveChanges();
        }
        #endregion Helper
    }
}
