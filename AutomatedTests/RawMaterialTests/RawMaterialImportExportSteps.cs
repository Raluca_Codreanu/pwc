﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class RawMaterialImportExportSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string materialExportPath;

        public RawMaterialImportExportSteps()
        {
        }

        public static void MyClassInitialize()
        {
            RawMaterialImportExportSteps.materialExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".rawmaterial");
            if (File.Exists(RawMaterialImportExportSteps.materialExportPath))
            {
                File.Delete(RawMaterialImportExportSteps.materialExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(RawMaterialImportExportSteps.materialExportPath))
            {
                File.Delete(RawMaterialImportExportSteps.materialExportPath);
            }
        }

        [BeforeFeature("ImportExportRawMaterial")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);           
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectIEMaterial.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportRawMaterial")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I select the ASA material")]
        public void GivenISelectTheASAMaterial()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            TreeNode ASAmaterialNode = materialsNode.GetNode("ASA");
            ASAmaterialNode.SelectEx();
        }
        
        [Given(@"I choose the Export option")]
        public void GivenIChooseTheExportOption()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            var exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }
        
        [Given(@"I press the Cancel button from Save As window")]
        public void GivenIPressTheCancelButtonFromSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I select the Export option again")]
        public void GivenISelectTheExportOptionAgain()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            Menu exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }
        
        [Given(@"I press Save button")]
        public void GivenIPressSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(RawMaterialImportExportSteps.materialExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(RawMaterialImportExportSteps.materialExportPath), "The rawMaterial was not exported at the specified location.");
        }

        [Given(@"I select the Materials node to import a material")]
        public void GivenISelectTheMaterialsNodeToImportAMaterial()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            Menu importPartMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportRawMaterial);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem.Click();
        }

        [When(@"I select the ASA material to be imported")]
        public void WhenISelectTheASAMaterialToBeImported()
        {
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\ASA.rawmaterial"));
            mainWindow.WaitForAsyncUITasks();
        }

        [Then(@"the Raw Material is imported")]
        public void ThenTheRawMaterialIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            var importedPartNode = materialsNode.GetNode("ASA");
            Assert.IsNotNull(importedPartNode, "The imported part did not appear in projects tree.");
        }
    }
}



