﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class DeleteCurrencyFeatureSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        /// <summary>
        /// Currency which will be edited.
        /// </summary>
        private static ZPKTool.Data.Currency currency;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditCurrencyTestCase"/> class.
        /// </summary>
        public DeleteCurrencyFeatureSteps()
        {
        }

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            DeleteCurrencyFeatureSteps.currency = new ZPKTool.Data.Currency();
            DeleteCurrencyFeatureSteps.currency.Name = "Currency_Chile_Peso";
            DeleteCurrencyFeatureSteps.currency.Symbol = "C";
            DeleteCurrencyFeatureSteps.currency.ExchangeRate = 2;
            DeleteCurrencyFeatureSteps.currency.IsMasterData = true;
            DeleteCurrencyFeatureSteps.currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();
        }

        [BeforeFeature("DeleteCurrency")]
        private static void LaunchApplication()
        {
            CreateTestData();

            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;            
        }
        [Given(@"I open the Master Data -> Entities -> Currencies page")]
        public void GivenIOpenTheMasterData_Entities_CurrenciesPage()
        {
            TreeNode currenciesNode = application.MainScreen.ProjectsTree.MasterDataCurrencies; 
            currenciesNode.SelectEx();
            currenciesNode.Click();
        }

        [Given(@"I select the currency name Chile Peso currency")]
        public void GivenISelectTheCurrencyNameChilePesoCurrency()
        {
            ListViewControl manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
            ListViewRow currencyRow = manageCurrenciesDataGrid.Row("Name", "Currency_Chile_Peso");
            Assert.IsNotNull(currencyRow, "The currency created for this test was not found.");
            ListViewCell nameCell = manageCurrenciesDataGrid.Cell("Name", currencyRow);
            nameCell.Click();
        }

        [Given(@"I press the Delete button")]
        public void GivenIPressTheDeleteButton()
        {
            Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
            Assert.IsNotNull(deleteButton, "Save button was not found.");
            deleteButton.Click();
        }

        [Given(@"the confirmation window is displayed and the No button is pressed")]
        public void GivenTheConfirmationWindowIsDisplayedAndTheNoButtonIsPressed()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I press the Delete button again")]
        public void GivenIPressTheDeleteButtonAgain()
        {
            Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
            Assert.IsNotNull(deleteButton, "Save button was not found.");
            deleteButton.Click();
        }

        [When(@"the confirmation window is displayed and the Yes button is pressed")]
        public void WhenTheConfirmationWindowIsDisplayedAndTheYesButtonIsPressed()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }

        [Then(@"the currency is delete but it is not put into the Trash Bin")]
        public void ThenTheCurrencyIsDeleteButItIsNotPutIntoTheTrashBin()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");
            trashBinNode.SelectEx();

            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
        }

        [AfterFeature("DeleteCurrency")]
        private static void KillApplication()
        {
            application.Kill();
        }
    }
}
