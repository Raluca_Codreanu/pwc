﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.CurrencyTests
{
    /// <summary>
    /// The automated test of Edit Currency Test Case.
    /// </summary>
    [TestClass]
    public class EditCurrencyTestCase
    {
        /// <summary>
        /// Currency which will be edited.
        /// </summary>
        private static ZPKTool.Data.Currency currency;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditCurrencyTestCase"/> class.
        /// </summary>
        public EditCurrencyTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext){}
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            CreateTestData();
        }

        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// The automated test of Edit Currency test case.
        /// </summary>
        [TestMethod]
        public void EditCurrencyTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                
                // step 3
                TreeNode currenciesNode = app.MainScreen.ProjectsTree.MasterDataCurrencies;
                currenciesNode.Click();

                // step 4
                ListViewControl manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
                Assert.IsNotNull(manageCurrenciesDataGrid, "Manage Currencies data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                // step 5
                ListViewRow currencyRow = manageCurrenciesDataGrid.Row("Name", EditCurrencyTestCase.currency.Name);
                Assert.IsNotNull(currencyRow, "The currency created for this test was not found.");
                ListViewCell nameCell = manageCurrenciesDataGrid.Cell("Name", currencyRow);
                nameCell.Click();

                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                Assert.AreEqual(EditCurrencyTestCase.currency.Name, nameTextBox.Text, "The Name text box does not contain the name of the selected currency.");

                TextBox symbolTextBox = mainWindow.Get<TextBox>(CurrencyAutomationIds.SymbolTextBox);
                Assert.IsNotNull(symbolTextBox, "Symbol text box was not found.");
                Assert.AreEqual(EditCurrencyTestCase.currency.Symbol, symbolTextBox.Text, "The Symbol text box does not contain the symbol of the selected currency.");

                TextBox exchangeRateTextBox = mainWindow.Get<TextBox>(CurrencyAutomationIds.ExchangeRateTextBox);
                Assert.IsNotNull(exchangeRateTextBox, "Exchange Rate text box was not found.");
                Assert.AreEqual(EditCurrencyTestCase.currency.ExchangeRate.ToString(), exchangeRateTextBox.Text, "The Exchange Rate text box does not contain the exchange rate of the selected currency.");

                // step 6                
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "Save button was not found.");

                nameTextBox.Text = string.Empty;
                nameTextBox.Click();
                Assert.IsFalse(saveButton.Enabled, "The Save button is disabled although the Name text box is empty.");

                //step 7
                nameTextBox.Text = "Currency" + DateTime.Now.Ticks;

                // step 8
                symbolTextBox.Text = EditCurrencyTestCase.currency.Symbol + "_Updated";
                exchangeRateTextBox.Text = (EditCurrencyTestCase.currency.ExchangeRate + 1).ToString();

                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");
                cancelButton.Click();
               
                // step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                // step 10
                cancelButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.AreEqual(EditCurrencyTestCase.currency.Name, nameTextBox.Text, "The currency name was updated although the Cancel operation has been performed.");

                symbolTextBox = mainWindow.Get<TextBox>(CurrencyAutomationIds.SymbolTextBox);
                Assert.AreEqual(EditCurrencyTestCase.currency.Symbol, symbolTextBox.Text, "The currency symbol was updated although the Cancel operation has been performed.");

                exchangeRateTextBox = mainWindow.Get<TextBox>(CurrencyAutomationIds.ExchangeRateTextBox);
                Assert.AreEqual(EditCurrencyTestCase.currency.ExchangeRate.ToString(), exchangeRateTextBox.Text, "The currency exchange rate was updated although the Cancel operation has been performed.");

                // step 11
                nameTextBox.Text = "Currency" + DateTime.Now.Ticks;
                symbolTextBox.Text = "$";
                exchangeRateTextBox.Text = "9";

                saveButton.Click();

                manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
                currencyRow = manageCurrenciesDataGrid.Row("Name", nameTextBox.Text);
                nameCell = manageCurrenciesDataGrid.Cell("Name", currencyRow);

                Assert.IsNotNull(currencyRow, "The updated currency row was not found.");
                //framework bug
                //now it works only if virtualization is disabled
                //Assert.IsTrue(currencyRow.IsSelected, "Failed to select the updated currency row.");
            }
        }

        #region Helper

        /// <summary>
        /// Creates the test data specified in the Edit Currency Test Case preconditions.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            EditCurrencyTestCase.currency = new ZPKTool.Data.Currency();
            EditCurrencyTestCase.currency.Name = "Currency" + DateTime.Now.Ticks;
            EditCurrencyTestCase.currency.Symbol = "C";
            EditCurrencyTestCase.currency.ExchangeRate = 2;
            EditCurrencyTestCase.currency.IsMasterData = true;
            EditCurrencyTestCase.currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            dataContext.CurrencyRepository.Add(currency);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
