﻿@DeleteCurrency
Feature: DeleteCurrencyFeature	
	In order be sure that a record is deleted
	I want to test it  

#comment
Scenario: Delete currency
	Given I open the Master Data -> Entities -> Currencies page
	And I select the currency name Chile Peso currency
	And I press the Delete button
	And the confirmation window is displayed and the No button is pressed
	And I press the Delete button again
	When the confirmation window is displayed and the Yes button is pressed
	Then the currency is delete but it is not put into the Trash Bin

