﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.CurrencyTests
{
    class CurrencyAutomationIds
    {
        public const string CurrenciesDataGrid = "CurrenciesDataGrid";
        public const string SymbolTextBox = "SymbolTextBox";
        public const string ExchangeRateTextBox = "ExchangeRateTextBox";
        public const string IsoCodeTextBox = "IsoCodeTextBox";
    }
}
