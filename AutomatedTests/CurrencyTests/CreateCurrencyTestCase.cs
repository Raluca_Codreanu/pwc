﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using ZPKTool.AutomatedTests.Utils;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.CurrencyTests
{
    /// <summary>
    /// The automated test of Create Currency Test Case.
    /// </summary>
    [TestClass]
    public class CreateCurrencyTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCurrencyTestCase"/> class.
        /// </summary>
        public CreateCurrencyTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Create Currency Test Case.
        /// </summary>
        [TestMethod]
        public void CreateCurrencyTest()
        {

            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode currenciesNode = app.MainScreen.ProjectsTree.MasterDataCurrencies;
                currenciesNode.Click();
                ListViewControl manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
                Assert.IsNotNull(manageCurrenciesDataGrid, "Manage Currencies data grid was not found.");

                // step 5
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "Add button was not found.");

                addButton.Click();
                Window createCurrencyWindow = Wait.For(() => app.Windows.CurrencyWindow);

                Button SaveButton = createCurrencyWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(SaveButton, "Create button was not found.");

                //SaveButton.Click();
                Assert.IsNotNull(app.Windows.CurrencyWindow, "Create Currency window was not found.");

                // step 7
                TextBox nameTextBox = createCurrencyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createCurrencyWindow, nameTextBox, 100);

                // step 8
                nameTextBox.Text = "Currency" + DateTime.Now.Ticks;
                nameTextBox.Click();

                //// step 9                
                Assert.IsFalse(SaveButton.Enabled, "The save button should not be enabled.");

                TextBox isoCode = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.IsoCodeTextBox);
                isoCode.Text = EncryptionManager.Instance.GenerateRandomString(3, true);                   

                // step 10
                TextBox symbolTextBox = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.SymbolTextBox);
                Assert.IsNotNull(symbolTextBox, "The Symbol text box was not found.");

                symbolTextBox.Text = "$";
                
                // step 11
                TextBox exchangeRateTextBox = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.ExchangeRateTextBox);
                Assert.IsNotNull(exchangeRateTextBox, "Exchange Rate text box was not found.");

                // step 12 - 14
                ValidationHelper.CheckNumericFieldValidators(createCurrencyWindow, exchangeRateTextBox, 1000000000, true);

                // step 15
                exchangeRateTextBox.Text = "1";
                Button cancelButton = createCurrencyWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");

                cancelButton.Click();
              
                // step 16
                createCurrencyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                

                // step 17
                cancelButton = createCurrencyWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();
                createCurrencyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
                ListViewRow currencyRow = manageCurrenciesDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNull(currencyRow, "The currency was added although the create currency operation has been cancelled.");

                // step 18
                addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                addButton.Click();

                createCurrencyWindow = Wait.For(() => app.Windows.CurrencyWindow);
                nameTextBox = createCurrencyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Currency" + DateTime.Now.Ticks;

                isoCode = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.IsoCodeTextBox);
                isoCode.Text = EncryptionManager.Instance.GenerateRandomString(3, true);

                symbolTextBox = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.SymbolTextBox);
                symbolTextBox.Text = "$";

                exchangeRateTextBox = createCurrencyWindow.Get<TextBox>(CurrencyAutomationIds.ExchangeRateTextBox);
                exchangeRateTextBox.Text = "1";

                SaveButton = createCurrencyWindow.Get<Button>(AutomationIds.SaveButton);
                SaveButton.Click();

                manageCurrenciesDataGrid = mainWindow.GetListView(CurrencyAutomationIds.CurrenciesDataGrid);
                currencyRow = manageCurrenciesDataGrid.Row("Name", nameTextBox.Text);
                Assert.IsNotNull(currencyRow, "The row of the newly created currency was not found.");

                //              currencyRow.Click();
                //               Assert.AreEqual(nameTextBox.Text, mainWindow.Get<TextBox>(CreateCurrencyTestCase.UIItemsIdentifiers.NameTextBox).Text, "The displayed currency name is wrong.");
                //               Assert.AreEqual(symbolTextBox.Text, mainWindow.Get<TextBox>(CreateCurrencyTestCase.UIItemsIdentifiers.SymbolTextBox).Text, "The displayed currency symbol is wrong.");
                //              Assert.AreEqual(exchangeRateTextBox.Text, mainWindow.Get<TextBox>(CreateCurrencyTestCase.UIItemsIdentifiers.ExchangeRateTextBox).Text, "The displayed currency exchange rate is wrong.");
            }
        }

        #region Helper
        #endregion Helper
    }
}
