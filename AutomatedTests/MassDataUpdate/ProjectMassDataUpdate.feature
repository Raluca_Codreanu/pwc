﻿@ProjectMassDataUpdate
Feature: ProjectMassDataUpdate

Scenario: Project General Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select General Tab
	When I complete the General Tab fields
	When I click on Update Button
	Then I verify that General tab fields are updated

Scenario: Project Shift Information Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Shift Information Tab
	When I complete the Shift Information Tab fields
	When I click on Update Button
	When I verify that Shift Information tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Shift Information Tab
	When I complete the Shift Information Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Shift Information tab fields are updated with AP values

Scenario: Project Process Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Process Tab
	When I complete the Process Tab fields
	When I click on Update Button
	When I verify that Process tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Process Tab
	When I complete the Process Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Process tab fields are updated with AP values

Scenario: Project Quantity Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Quantity Tab
	When I complete the Quantity Tab fields
	When I click on Update Button
	When I verify that Quantity tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Quantity Tab
	When I complete the Quantity Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Quantity tab fields are updated with AP values

Scenario: Project Overhead and Margin Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Overhead and Margin Tab
	When I complete the Overhead and Margin Tab fields
	When I click on Update Button
	When I verify that Overhead and Margin tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Overhead and Margin Tab
	When I complete the Overhead and Margin Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Overhead and Margin tab fields are updated with AP values

Scenario: Project Country Settings Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Country Settings Tab
	When I complete the Country Settings Tab fields
	When I click on Update Button
	Then I verify that Country Settings tab fields are updated

Scenario: Project Machines Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Machines tab
	When I complete the Machines Tab fields
	When I click on Update Button
	When I verify that Machines tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Machines tab
	When I complete the Machines Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Machines tab fields are updated with AP values

Scenario: Project Additional Costs Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Additional Costs tab
	When I complete the Additional Costs Tab fields
	When I click on Update Button
	When I verify that Additional Costs tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I check Apply updates to sub-folders too
	When I select Additional Costs tab
	When I complete the Additional Costs Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Additional Costs tab fields are updated with AP values

Scenario: Project Raw Materials Tab Mass Data Update
	Given I select the project
	And I click on Data-Mass Data Update
	When I select Raw Materials tab
	When I check Apply updates to sub-folders too for RawMaterials
	When I complete the Raw Materials Tab fields
	When I click on Update Button
	When I verify that Raw Materials tab fields are updated
	When I select the project
	When I click on Data-Mass Data Update
	When I select Raw Materials tab
	When I check Apply updates to sub-folders too for RawMaterials
	When I complete the Raw Materials Tab Applied Percentage fields
	When I click on Update Button
	Then I verify that Raw Materials tab fields are updated with AP values