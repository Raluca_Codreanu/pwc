﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;
using System.Windows.Controls.Primitives;
using White.Core.UIItems.Custom;
using ZPKTool.AutomatedTests.ProjectTests;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using ZPKTool.AutomatedTests.ProcessStepTests;
using ZPKTool.AutomatedTests.SettingsTests;
using ZPKTool.AutomatedTests.MachineTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ProjectMassDataUpdateSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();
        #region Data
        private static string version;
        private static bool toolingActive;
        private static string shiftsPerWeek;
        private static string hoursPerShift;
        private static string productionDaysPerWeek;
        private static string productionWeeksPerYear;
        private static string processTime;
        private static string cycleTime;
        private static string partsPerCycle;
        private static string reject;
        private static string setupPerBatch;
        private static string maxDowntime;
        private static string setupTime;
        private static string batchSize;
        private static string processManufacturingOverhead;
        private static string quantityBatchSize;
        private static string annualProductionQty;
        private static string lifetime;
        private static string calculatedBatchSize;
        private static string materialOverhead;
        private static string consumableOverhead;
        private static string commodityOverhead;
        private static string externalWorkOverhead;
        private static string manufacturingOverhead;
        private static string otherCostOverhead;
        private static string packagingOverhead;
        private static string internalLogisticsOverhead;
        private static string salesAndAdministrationOverhead;
        private static string companySurchargeOverhead;
        private static string materialMargin;
        private static string consumableMargin;
        private static string commodityMargin;
        private static string externalWorkMargin;
        private static string manufacturingMargin;
        private static string manufacturingLocation;
        private static string depreciationPeriod;
        private static string interestRate;
        private static string maintenanceValue;
        private static string consumableCost;
        private static string floorSize;
        private static string workspaceArea;
        private static string powerConsumption;
        private static string airConsumption;
        private static string waterConsumption;
        private static string fullLoadRate;
        private static string maxCapacity;
        private static string oee;
        private static string availability;
        private static string developmentCost;
        private static string projectInvest;
        private static string internalLogisticCost;
        private static string transportCost;
        private static string packagingCost;
        private static string otherCost;
        private static string paymentTerms;
        private static string price;
        private static string yieldStrength;
        private static string ruptureStrength;
        private static string density;
        private static string maxElongation;
        private static string glassTransitionTemperature;
        private static string rx;
        private static string rm;
        #endregion Data

        [BeforeFeature("ProjectMassDataUpdate")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\ProjectMDU2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ProjectMassDataUpdate")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the project")]
        public void GivenISelectTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
        }

        [Given(@"I click on Data-Mass Data Update")]
        public void GivenIClickOnData_MassDataUpdate()
        {
            application.MainMenu.Data_MassDataUpdate.Click();
        }

        [When(@"I select General Tab")]
        public void WhenISelectGeneralTab()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.MDUGeneralTab);
            generalTab.Click();
        }

        [When(@"I complete the General Tab fields")]
        public void WhenICompleteTheGeneralTabFields()
        {
            TextBox versionTextBox = mainWindow.Get<TextBox>(AutomationIds.MDUVersionTextBox);
            versionTextBox.Text = "10";
            version = versionTextBox.Text;
            CheckBox toolingActiveCheckBox = mainWindow.Get<CheckBox>(AutomationIds.MDUToolingActiveNVCheckBox);
            toolingActiveCheckBox.Checked = true;
            toolingActive = toolingActiveCheckBox.Checked;
        }

        [Then(@"I verify that General tab fields are updated")]
        public void ThenIVerifyThatGeneralTabFieldsAreUpdated()
        {
            //version
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TextBox projectVersion = mainWindow.Get<TextBox>(ProjectAutomationIds.VersionTextBox);
            Assert.AreEqual(version, projectVersion.Text);
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            mainWindow.WaitWhileBusy();
            TextBox assemblyVersion = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            Assert.AreEqual(version, assemblyVersion.Text);
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TextBox partVersion = mainWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            Assert.AreEqual(version, partVersion.Text);
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.ExpandEx();
            materialsNode.SelectEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TextBox rawPartVersion = mainWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
            Assert.AreEqual(version, rawPartVersion.Text);
        }

        [When(@"I select Shift Information Tab")]
        public void WhenISelectShiftInformationTab()
        {
            TabPage shiftInformationTab = mainWindow.Get<TabPage>(AutomationIds.MDUShiftInformationTab);
            shiftInformationTab.Click();
        }

        [When(@"I complete the Shift Information Tab fields")]
        public void WhenICompleteTheShiftInformationTabFields()
        {
            TextBox shiftsPerWeekNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ShiftsPerWeekNVTextBox);
            shiftsPerWeekNVTextBox.Text = "10";
            shiftsPerWeek = shiftsPerWeekNVTextBox.Text;
            TextBox hoursPerShiftNVTextBox = mainWindow.Get<TextBox>(AutomationIds.HoursPerShiftNVTextBox);
            hoursPerShiftNVTextBox.Text = "10";
            hoursPerShift = hoursPerShiftNVTextBox.Text;
            TextBox productionDaysPerWeekNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ProductionDaysPerWeekNVTextBox);
            productionDaysPerWeekNVTextBox.Text = "5";
            productionDaysPerWeek = productionDaysPerWeekNVTextBox.Text;
            TextBox productionWeeksPerYearNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ProductionWeeksPerYearNVTextBox);
            productionWeeksPerYearNVTextBox.Text = "10";
            productionWeeksPerYear = productionWeeksPerYearNVTextBox.Text;
        }

        [When(@"I verify that Shift Information tab fields are updated")]
        public void WhenIVerifyThatShiftInformationTabFieldsAreUpdated()
        {
            // verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TabPage projectSettingTab = mainWindow.Get<TabPage>("SettingsTab");
            projectSettingTab.Click();
            TextBox projectShiftPerWeeks = mainWindow.Get<TextBox>(ProjectAutomationIds.ShiftsPerWeekTextBox);
            TextBox projectHoursPerShift = mainWindow.Get<TextBox>(ProjectAutomationIds.HoursPerShiftTextBox);
            TextBox projectProductionDaysPerWeek = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionDaysPerWeekTextBox);
            TextBox projectProductionWeeksPerYear = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionWeeksPerYearTextBox);
            Assert.AreEqual(shiftsPerWeek, projectShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, projectHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, projectProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, projectProductionWeeksPerYear.Text);

            //verify in Assembly Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TextBox assemblyProcessShiftPerWeeks = mainWindow.Get<TextBox>("ShiftsPerWeekTextBox");
            TextBox assemblyProcessHoursPerShift = mainWindow.Get<TextBox>("HoursPerShiftTextBox");
            TextBox assemblyProcessProductionDaysPerWeek = mainWindow.Get<TextBox>("ProductionDaysPerWeekTextBox");
            TextBox assemblyProcessProductionWeeksPerYear = mainWindow.Get<TextBox>("ProductionWeeksPerYearTextBox");
            Assert.AreEqual(shiftsPerWeek, assemblyProcessShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, assemblyProcessHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, assemblyProcessProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, assemblyProcessProductionWeeksPerYear.Text);

            //verify in Part Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessShiftPerWeeks = mainWindow.Get<TextBox>("ShiftsPerWeekTextBox");
            TextBox partProcessHoursPerShift = mainWindow.Get<TextBox>("HoursPerShiftTextBox");
            TextBox partProcessProductionDaysPerWeek = mainWindow.Get<TextBox>("ProductionDaysPerWeekTextBox");
            TextBox partProcessProductionWeeksPerYear = mainWindow.Get<TextBox>("ProductionWeeksPerYearTextBox");
            Assert.AreEqual(shiftsPerWeek, partProcessShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, partProcessHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, partProcessProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, partProcessProductionWeeksPerYear.Text);
        }


        [When(@"I select the project")]
        public void WhenISelectTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
        }

        [When(@"I click on Data-Mass Data Update")]
        public void WhenIClickOnData_MassDataUpdate()
        {
            application.MainMenu.Data_MassDataUpdate.Click();
        }

        [When(@"I complete the Shift Information Tab Applied Percentage fields")]
        public void ICompleteTheShiftInformationTabAppliedPercentageFields()
        {
            TextBox shiftsPerWeekAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ShiftsPerWeekAPTextBox);
            shiftsPerWeekAPTextBox.Text = "10";
            int shiftsPerWeekValue = ((Convert.ToInt32(shiftsPerWeekAPTextBox.Text)) * (Convert.ToInt32(shiftsPerWeek)))/100;
            shiftsPerWeek = shiftsPerWeekValue.ToString();
            TextBox hoursPerShiftAPTextBox = mainWindow.Get<TextBox>(AutomationIds.HoursPerShiftAPTextBox);
            hoursPerShiftAPTextBox.Text = "10";
            int hoursPerShiftValue = ((Convert.ToInt32(hoursPerShiftAPTextBox.Text)) * (Convert.ToInt32(hoursPerShift)))/100;
            hoursPerShift = hoursPerShiftValue.ToString();
            TextBox productionDaysPerWeekAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ProductionDaysPerWeekAPTextBox);
            productionDaysPerWeekAPTextBox.Text = "20";
            int productionDaysPerWeekValue = ((Convert.ToInt32(productionDaysPerWeekAPTextBox.Text)) * (Convert.ToInt32(productionDaysPerWeek)))/100;
            productionDaysPerWeek = productionDaysPerWeekValue.ToString();
            TextBox productionWeeksPerYearAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ProductionWeeksPerYearAPTextBox);
            productionWeeksPerYearAPTextBox.Text = "10";
            int productionWeeksPerYearValue = ((Convert.ToInt32(productionWeeksPerYearAPTextBox.Text)) * (Convert.ToInt32(productionWeeksPerYear)))/100;
            productionWeeksPerYear = productionWeeksPerYearValue.ToString();
        }

        [Then(@"I verify that Shift Information tab fields are updated with AP values")]
        public void ThenIVerifyThatShiftInformationTabFieldsAreUpdatedWithAPValues()
        {
            // verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TabPage projectSettingTab = mainWindow.Get<TabPage>("SettingsTab");
            projectSettingTab.Click();
            TextBox projectShiftPerWeeks = mainWindow.Get<TextBox>(ProjectAutomationIds.ShiftsPerWeekTextBox);
            TextBox projectHoursPerShift = mainWindow.Get<TextBox>(ProjectAutomationIds.HoursPerShiftTextBox);
            TextBox projectProductionDaysPerWeek = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionDaysPerWeekTextBox);
            TextBox projectProductionWeeksPerYear = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionWeeksPerYearTextBox);
            Assert.AreEqual(shiftsPerWeek, projectShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, projectHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, projectProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, projectProductionWeeksPerYear.Text);

            //verify in Assembly Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TextBox assemblyProcessShiftPerWeeks = mainWindow.Get<TextBox>("ShiftsPerWeekTextBox");
            TextBox assemblyProcessHoursPerShift = mainWindow.Get<TextBox>("HoursPerShiftTextBox");
            TextBox assemblyProcessProductionDaysPerWeek = mainWindow.Get<TextBox>("ProductionDaysPerWeekTextBox");
            TextBox assemblyProcessProductionWeeksPerYear = mainWindow.Get<TextBox>("ProductionWeeksPerYearTextBox");
            Assert.AreEqual(shiftsPerWeek, assemblyProcessShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, assemblyProcessHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, assemblyProcessProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, assemblyProcessProductionWeeksPerYear.Text);

            //verify in Part Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessShiftPerWeeks = mainWindow.Get<TextBox>("ShiftsPerWeekTextBox");
            TextBox partProcessHoursPerShift = mainWindow.Get<TextBox>("HoursPerShiftTextBox");
            TextBox partProcessProductionDaysPerWeek = mainWindow.Get<TextBox>("ProductionDaysPerWeekTextBox");
            TextBox partProcessProductionWeeksPerYear = mainWindow.Get<TextBox>("ProductionWeeksPerYearTextBox");
            Assert.AreEqual(shiftsPerWeek, partProcessShiftPerWeeks.Text);
            Assert.AreEqual(hoursPerShift, partProcessHoursPerShift.Text);
            Assert.AreEqual(productionDaysPerWeek, partProcessProductionDaysPerWeek.Text);
            Assert.AreEqual(productionWeeksPerYear, partProcessProductionWeeksPerYear.Text);
        }

        [When(@"I select Process Tab")]
        public void WhenISelectProcessTab()
        {
            TabPage processTab = mainWindow.Get<TabPage>(AutomationIds.MDUProcessTab);
            processTab.Click();
        }

        [When(@"I complete the Process Tab fields")]
        public void WhenICompleteTheProcessTabFields()
        {
            TextBox processTimeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ProcessTimeTextBox);
            processTimeNVTextBox.Text = "10";
            processTime = processTimeNVTextBox.Text;
            TextBox cycleTimeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.CycleTimeTextBox);
            cycleTimeNVTextBox.Text = "10";
            cycleTime = cycleTimeNVTextBox.Text;
            TextBox partsPerCycleTextBox = mainWindow.Get<TextBox>(AutomationIds.PartsPerCycleTextBox);
            partsPerCycleTextBox.Text = "10";
            partsPerCycle = partsPerCycleTextBox.Text;
            TextBox rejectNVTextBox = mainWindow.Get<TextBox>(AutomationIds.RejectTextBox);
            rejectNVTextBox.Text = "10";
            reject = rejectNVTextBox.Text;
            TextBox setupsPerBatchTextBox = mainWindow.Get<TextBox>(AutomationIds.SetupsPerBatchTextBox);
            setupsPerBatchTextBox.Text = "10";
            setupPerBatch = setupsPerBatchTextBox.Text;
            TextBox maxDowntimeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxDowntimeTextBox);
            maxDowntimeNVTextBox.Text = "10";
            maxDowntime = maxDowntimeNVTextBox.Text;
            TextBox setupTimeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.SetupTimeTextBox);
            setupTimeNVTextBox.Text = "10";
            setupTime = setupTimeNVTextBox.Text;
            TextBox batchSizeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.BatchSizeTextBox);
            batchSizeNVTextBox.Text = "10";
            batchSize = batchSizeNVTextBox.Text;
            TextBox manufacturingOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingOHTextBox);
            manufacturingOverheadNVTextBox.Text = "10";
            processManufacturingOverhead = manufacturingOverheadNVTextBox.Text;
        }

        [When(@"I verify that Process tab fields are updated")]
        public void WhenIVerifyThatProcessTabFieldsAreUpdated()
        {
            //verify in Assembly Process Step
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TextBox assemblyProcessTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
            TextBox assemblyCycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
            TextBox assemblyPartsPerCycleTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.PartsPerCycleTextBox);
            TextBox assemblyRejectTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.RejectTextBox);
            TextBox assemblySetupsPerBatchTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupsPerBatchTextBox);
            TextBox assemblyMaxDowntimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MaxDowntimeTextBox);
            TextBox assemblySetupTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTimeTextBox);
            TextBox assemblyBatchSizeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.BatchSizeTextBox);
            TextBox assemblyManufacturingOHTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ManufacturingOHTextBox);

            Assert.AreEqual(processTime, assemblyProcessTimeTextBox.Text);
            Assert.AreEqual(cycleTime, assemblyCycleTimeTextBox.Text);
            Assert.AreEqual(partsPerCycle, assemblyPartsPerCycleTextBox.Text);
            Assert.AreEqual(reject, assemblyRejectTextBox.Text);
            Assert.AreEqual(setupPerBatch, assemblySetupsPerBatchTextBox.Text);
            Assert.AreEqual(maxDowntime, assemblyMaxDowntimeTextBox.Text);
            Assert.AreEqual(setupTime, assemblySetupTimeTextBox.Text);
            Assert.AreEqual(batchSize, assemblyBatchSizeTextBox.Text);
            Assert.AreEqual(processManufacturingOverhead, assemblyManufacturingOHTextBox.Text);

            //verify in Part Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
            TextBox partCycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
            TextBox partPartsPerCycleTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.PartsPerCycleTextBox);
            TextBox partRejectTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.RejectTextBox);
            TextBox partSetupsPerBatchTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupsPerBatchTextBox);
            TextBox partMaxDowntimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MaxDowntimeTextBox);
            TextBox partSetupTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTimeTextBox);
            TextBox partBatchSizeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.BatchSizeTextBox);
            TextBox partManufacturingOHTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ManufacturingOHTextBox);

            Assert.AreEqual(processTime, partProcessTimeTextBox.Text);
            Assert.AreEqual(cycleTime, partCycleTimeTextBox.Text);
            Assert.AreEqual(partsPerCycle, partPartsPerCycleTextBox.Text);
            Assert.AreEqual(reject, partRejectTextBox.Text);
            Assert.AreEqual(setupPerBatch, partSetupsPerBatchTextBox.Text);
            Assert.AreEqual(maxDowntime, partMaxDowntimeTextBox.Text);
            Assert.AreEqual(setupTime, partSetupTimeTextBox.Text);
            Assert.AreEqual(batchSize, partBatchSizeTextBox.Text);
            Assert.AreEqual(processManufacturingOverhead, partManufacturingOHTextBox.Text);
        }

        [When(@"I complete the Process Tab Applied Percentage fields")]
        public void WhenICompleteTheProcessTabAppliedPercentageFields()
        {
            TextBox processTimeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ProcessTimeAPTextBox);
            processTimeAPTextBox.Text = "10";
            int processTimeValue = ((Convert.ToInt32(processTimeAPTextBox.Text)) * (Convert.ToInt32(processTime))) / 100;
            processTime = processTimeValue.ToString();
            TextBox cycleTimeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.CycleTimeAPTextBox);
            cycleTimeAPTextBox.Text = "10";
            int cycleTimeValue = ((Convert.ToInt32(cycleTimeAPTextBox.Text)) * (Convert.ToInt32(cycleTime))) / 100;
            cycleTime = cycleTimeValue.ToString();
            TextBox rejectAPTextBox = mainWindow.Get<TextBox>(AutomationIds.RejectAPTextBox);
            rejectAPTextBox.Text = "10";
            int rejectValue = ((Convert.ToInt32(rejectAPTextBox.Text)) * (Convert.ToInt32(reject))) / 100;
            reject = rejectValue.ToString();
            TextBox maxDowntimeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxDowntimeAPTextBox);
            maxDowntimeAPTextBox.Text = "10";
            int maxDowntimeValue = ((Convert.ToInt32(maxDowntimeAPTextBox.Text)) * (Convert.ToInt32(maxDowntime))) / 100;
            maxDowntime = maxDowntimeValue.ToString();
            TextBox setupTimeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.SetupTimeAPTextBox);
            setupTimeAPTextBox.Text = "10";
            int setupTimeValue = ((Convert.ToInt32(setupTimeAPTextBox.Text)) * (Convert.ToInt32(setupTime))) / 100;
            setupTime = setupTimeValue.ToString();
            TextBox manufacturingOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingOHAPTextBox);
            manufacturingOverheadAPTextBox.Text = "10";
            int sprocessManufacturingOverheadValue = ((Convert.ToInt32(manufacturingOverheadAPTextBox.Text)) * (Convert.ToInt32(processManufacturingOverhead))) / 100;
            processManufacturingOverhead = sprocessManufacturingOverheadValue.ToString();
        }

        [Then(@"I verify that Process tab fields are updated with AP values")]
        public void ThenIVerifyThatProcessTabFieldsAreUpdatedWithAPValues()
        {
            //verify in Assembly Process Step
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TextBox assemblyProcessTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
            TextBox assemblyCycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
            TextBox assemblyRejectTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.RejectTextBox);
            TextBox assemblyMaxDowntimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MaxDowntimeTextBox);
            TextBox assemblySetupTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTimeTextBox);
            TextBox assemblyManufacturingOHTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ManufacturingOHTextBox);

            Assert.AreEqual(processTime, assemblyProcessTimeTextBox.Text);
            Assert.AreEqual(cycleTime, assemblyCycleTimeTextBox.Text);
            Assert.AreEqual(reject, assemblyRejectTextBox.Text);
            Assert.AreEqual(maxDowntime, assemblyMaxDowntimeTextBox.Text);
            Assert.AreEqual(setupTime, assemblySetupTimeTextBox.Text);
            Assert.AreEqual(processManufacturingOverhead, assemblyManufacturingOHTextBox.Text);

            //verify in Part Process Step
            projectNode.SelectEx();
            projectNode.Expand();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ProcessTimeTextBox);
            TextBox partCycleTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.CycleTimeTextBox);
            TextBox partRejectTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.RejectTextBox);
            TextBox partMaxDowntimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.MaxDowntimeTextBox);
            TextBox partSetupTimeTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.SetupTimeTextBox);
            TextBox partManufacturingOHTextBox = mainWindow.Get<TextBox>(ProcessStepAutomationIds.ManufacturingOHTextBox);

            Assert.AreEqual(processTime, partProcessTimeTextBox.Text);
            Assert.AreEqual(cycleTime, partCycleTimeTextBox.Text);
            Assert.AreEqual(reject, partRejectTextBox.Text);
            Assert.AreEqual(maxDowntime, partMaxDowntimeTextBox.Text);
            Assert.AreEqual(setupTime, partSetupTimeTextBox.Text);
            Assert.AreEqual(processManufacturingOverhead, partManufacturingOHTextBox.Text);
        }


        [When(@"I select Quantity Tab")]
        public void WhenISelectQuantityTab()
        {
            TabPage quantityTab = mainWindow.Get<TabPage>(AutomationIds.MDUQuantityTab);
            quantityTab.Click();
        }

        [When(@"I complete the Quantity Tab fields")]
        public void WhenICompleteTheQuantityTabFields()
        {
            TextBox quantityBatchSizeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.BatchSizeNVTextBox);
            quantityBatchSizeNVTextBox.Text = "10";
            quantityBatchSize = quantityBatchSizeNVTextBox.Text;
            TextBox yearlyProdQuantityNVTextBox = mainWindow.Get<TextBox>(AutomationIds.YearlyProdQuantityNVTextBox);
            yearlyProdQuantityNVTextBox.Text = "10";
            annualProductionQty = yearlyProdQuantityNVTextBox.Text;
            TextBox lifeTimeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.LifeTimeNVTextBox);
            lifeTimeNVTextBox.Text = "10";
            lifetime = lifeTimeNVTextBox.Text;
            TextBox calculatedBatchSizeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.CalculatedBatchSizeNVTextBox);
            calculatedBatchSizeNVTextBox.Text = "10";
            calculatedBatchSize = calculatedBatchSizeNVTextBox.Text;
        }

        [When(@"I verify that Quantity tab fields are updated")]
        public void WhenIVerifyThatQuantityTabFieldsAreUpdated()
        {
            //verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TextBox projectAnnualProdQtyTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.AnnualProdQty);
            TextBox projectLifetimeTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.LifetimeTextBox);
            Assert.AreEqual(annualProductionQty, projectAnnualProdQtyTextBox.Text);
            Assert.AreEqual(lifetime, projectLifetimeTextBox.Text);

            //verify in Assembly
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            TextBox assemblyBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            TextBox assemblyAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            TextBox assemblyLifetimeTextBox = mainWindow.Get<TextBox>("LifeTimeTextBox");
            Assert.AreEqual(quantityBatchSize, assemblyBatchSizeTextBox.Text);
            Assert.AreEqual(annualProductionQty, assemblyAnnualProdQtyTextBox.Text);
            Assert.AreEqual(lifetime, assemblyLifetimeTextBox.Text);

            //verify in Assembly ProcessStep
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TabPage assemblyProcessStepInformationTab = mainWindow.Get<TabPage>("ProcessStepInfoTab");
            assemblyProcessStepInformationTab.Click();
            TextBox assemblyProcessStepBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            Assert.AreEqual(calculatedBatchSize, assemblyProcessStepBatchSizeTextBox.Text);

            //verify in Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TextBox partBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            TextBox partAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            TextBox partLifetimeTextBox = mainWindow.Get<TextBox>("LifeTimeTextBox");
            Assert.AreEqual(quantityBatchSize, partBatchSizeTextBox.Text);
            Assert.AreEqual(annualProductionQty, partAnnualProdQtyTextBox.Text);
            Assert.AreEqual(lifetime, partLifetimeTextBox.Text);

            //verify in Raw Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TextBox rawPartBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            TextBox rawPartAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            TextBox rawPartLifetimeTextBox = mainWindow.Get<TextBox>("LifeTimeTextBox");
            Assert.AreEqual(quantityBatchSize, rawPartBatchSizeTextBox.Text);
            Assert.AreEqual(annualProductionQty, rawPartAnnualProdQtyTextBox.Text);
            Assert.AreEqual(lifetime, rawPartLifetimeTextBox.Text);

            //verify in Part ProcessStep
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessStepBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            Assert.AreEqual(calculatedBatchSize, partProcessStepBatchSizeTextBox.Text);
        }

        [When(@"I complete the Quantity Tab Applied Percentage fields")]
        public void WhenICompleteTheQuantityTabAppliedPercentageFields()
        {
            TextBox yearlyProdQuantityAPTextBox = mainWindow.Get<TextBox>(AutomationIds.YearlyProdQuantityAPTextBox);
            yearlyProdQuantityAPTextBox.Text = "10";
            int annualProductionQtyValue = ((Convert.ToInt32(yearlyProdQuantityAPTextBox.Text)) * (Convert.ToInt32(annualProductionQty))) / 100;
            annualProductionQty = annualProductionQtyValue.ToString();
            TextBox calculatedBatchSizeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.CalculatedBatchSizeAPTextBox);
            calculatedBatchSizeAPTextBox.Text = "10";
            int calculatedBatchSizeValue = ((Convert.ToInt32(calculatedBatchSizeAPTextBox.Text)) * (Convert.ToInt32(calculatedBatchSize))) / 100;
            calculatedBatchSize = calculatedBatchSizeValue.ToString();
        }

        [Then(@"I verify that Quantity tab fields are updated with AP values")]
        public void ThenIVerifyThatQuantityTabFieldsAreUpdatedWithAPValues()
        {
            //verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TextBox projectAnnualProdQtyTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.AnnualProdQty);
            Assert.AreEqual(annualProductionQty, projectAnnualProdQtyTextBox.Text);

            //verify in Assembly
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();

            TextBox assemblyAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            Assert.AreEqual(annualProductionQty, assemblyAnnualProdQtyTextBox.Text);

            //verify in Assembly ProcessStep
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TabPage assemblyProcessStepInformationTab = mainWindow.Get<TabPage>("ProcessStepInfoTab");
            assemblyProcessStepInformationTab.Click();
            TextBox assemblyProcessStepBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            Assert.AreEqual(calculatedBatchSize, assemblyProcessStepBatchSizeTextBox.Text);

            //verify in Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TextBox partAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            Assert.AreEqual(annualProductionQty, partAnnualProdQtyTextBox.Text);

            //verify in Raw Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TextBox rawPartAnnualProdQtyTextBox = mainWindow.Get<TextBox>("YearlyProdQtyTextBox");
            Assert.AreEqual(annualProductionQty, rawPartAnnualProdQtyTextBox.Text);

            //verify in Part ProcessStep
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TextBox partProcessStepBatchSizeTextBox = mainWindow.Get<TextBox>("BatchSizeTextBox");
            Assert.AreEqual(calculatedBatchSize, partProcessStepBatchSizeTextBox.Text);
        }


        [When(@"I select Overhead and Margin Tab")]
        public void WhenISelectOverheadAndMarginTab()
        {
            TabPage overheadAndMarginTab = mainWindow.Get<TabPage>(AutomationIds.MDUOverheadAndMarginTab);
            overheadAndMarginTab.Click();
        }

        [When(@"I complete the Overhead and Margin Tab fields")]
        public void WhenICompleteTheOverheadAndMarginTabFields()
        {
            TextBox materialOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.MaterialOverheadNVTextBox);
            materialOverheadNVTextBox.Text = "10";
            materialOverhead = materialOverheadNVTextBox.Text;
            TextBox consumableOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumableOverheadNVTextBox);
            consumableOverheadNVTextBox.Text = "10";
            consumableOverhead = consumableOverheadNVTextBox.Text;
            TextBox commodityOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.CommodityOverheadNVTextBox);
            commodityOverheadNVTextBox.Text = "10";
            commodityOverhead = commodityOverheadNVTextBox.Text;
            TextBox externalWorkOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ExternalWorkOverheadNVTextBox);
            externalWorkOverheadNVTextBox.Text = "10";
            externalWorkOverhead = externalWorkOverheadNVTextBox.Text;
            TextBox manufacturingOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingOverheadNVTextBox);
            manufacturingOverheadNVTextBox.Text = "10";
            manufacturingOverhead = manufacturingOverheadNVTextBox.Text;
            TextBox otherCostOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostOverheadNVTextBox);
            otherCostOverheadNVTextBox.Text = "10";
            otherCostOverhead = otherCostOverheadNVTextBox.Text;
            TextBox packagingOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.PackagingOverheadNVTextBox);
            packagingOverheadNVTextBox.Text = "10";
            packagingOverhead = packagingOverheadNVTextBox.Text;
            TextBox logisticOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticOverheadNVTextBox);
            logisticOverheadNVTextBox.Text = "10";
            internalLogisticsOverhead = logisticOverheadNVTextBox.Text;
            TextBox salesAndAdminOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.SalesAndAdminOverheadNVTextBox);
            salesAndAdminOverheadNVTextBox.Text = "10";
            salesAndAdministrationOverhead = salesAndAdminOverheadNVTextBox.Text;
            TextBox companySurchargeOverheadNVTextBox = mainWindow.Get<TextBox>(AutomationIds.CompanySurchargeOverheadNVTextBox);
            companySurchargeOverheadNVTextBox.Text = "10";
            companySurchargeOverhead = companySurchargeOverheadNVTextBox.Text;
            TextBox materialMarginNVTextBox = mainWindow.Get<TextBox>(AutomationIds.MaterialMarginNVTextBox);
            materialMarginNVTextBox.Text = "10";
            materialMargin = materialMarginNVTextBox.Text;
            TextBox consumableMaginNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumableMaginNVTextBox);
            consumableMaginNVTextBox.Text = "10";
            consumableMargin = consumableMaginNVTextBox.Text;
            TextBox commodityMarginNVTextBox = mainWindow.Get<TextBox>(AutomationIds.CommodityMarginNVTextBox);
            commodityMarginNVTextBox.Text = "10";
            commodityMargin = commodityMarginNVTextBox.Text;
            TextBox externalWorkMarginNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ExternalWorkMarginNVTextBox);
            externalWorkMarginNVTextBox.Text = "10";
            externalWorkMargin = externalWorkMarginNVTextBox.Text;
            TextBox manufacturingMarginNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingMarginNVTextBox);
            manufacturingMarginNVTextBox.Text = "10";
            manufacturingMargin = manufacturingMarginNVTextBox.Text;
        }

        [When(@"I verify that Overhead and Margin tab fields are updated")]
        public void WhenIVerifyThatOverheadAndMarginTabFieldsAreUpdated()
        {
            //verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TabPage projectOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            projectOHSettingsTab.Click();
            TextBox projectMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, projectMaterialOHTextBox.Text);
            TextBox projectCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, projectCommodityOHTextBox.Text);
            TextBox projectManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, projectManufacturingOHTextBox.Text);
            TextBox projectPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, projectPackagingOverheadTextBox.Text);
            TextBox projectSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, projectSalesAndAdministrationOverheadTextBox.Text);
            TextBox projectConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, projectConsumableOHTextBox.Text);
            TextBox projectExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, projectExternalWorkOHTextBox.Text);
            TextBox projectOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, projectOtherCostOHTextBox.Text);
            TextBox projectLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, projectLogisticOverheadTextBox.Text);
            TextBox projectCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, projectCompanySurchargeOHTextBox.Text);
            TextBox projectMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, projectMaterialMarginTextBox.Text);
            TextBox projectCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, projectCommodityMarginTextBox.Text);
            TextBox projectManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, projectManufacturingMarginTextBox.Text);
            TextBox projectConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, projectConsumableMarginTextBox.Text);
            TextBox projectExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, projectExternalWorkMarginTextBox.Text);

            //assembly
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            TabPage assemblyOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            assemblyOHSettingsTab.Click();
            TextBox assemblyMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, assemblyMaterialOHTextBox.Text);
            TextBox assemblyCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, assemblyCommodityOHTextBox.Text);
            TextBox assemblyManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, assemblyManufacturingOHTextBox.Text);
            TextBox assemblyPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, assemblyPackagingOverheadTextBox.Text);
            TextBox assemblySalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, assemblySalesAndAdministrationOverheadTextBox.Text);
            TextBox assemblyConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, assemblyConsumableOHTextBox.Text);
            TextBox assemblyExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, assemblyExternalWorkOHTextBox.Text);
            TextBox assemblyOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, assemblyOtherCostOHTextBox.Text);
            TextBox assemblyLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, assemblyLogisticOverheadTextBox.Text);
            TextBox assemblyCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, assemblyCompanySurchargeOHTextBox.Text);
            TextBox assemblyMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, assemblyMaterialMarginTextBox.Text);
            TextBox assemblyCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, assemblyCommodityMarginTextBox.Text);
            TextBox assemblyManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, assemblyManufacturingMarginTextBox.Text);
            TextBox assemblyConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, assemblyConsumableMarginTextBox.Text);
            TextBox assemblyExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, assemblyExternalWorkMarginTextBox.Text);

            //verify in part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TabPage partOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            partOHSettingsTab.Click();
            TextBox partMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, partMaterialOHTextBox.Text);
            TextBox partCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, partCommodityOHTextBox.Text);
            TextBox partManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, partManufacturingOHTextBox.Text);
            TextBox partPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, partPackagingOverheadTextBox.Text);
            TextBox partSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, partSalesAndAdministrationOverheadTextBox.Text);
            TextBox partConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, partConsumableOHTextBox.Text);
            TextBox partExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, partExternalWorkOHTextBox.Text);
            TextBox partOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, partOtherCostOHTextBox.Text);
            TextBox partLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, partLogisticOverheadTextBox.Text);
            TextBox partCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, partCompanySurchargeOHTextBox.Text);
            TextBox partMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, partMaterialMarginTextBox.Text);
            TextBox partCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, partCommodityMarginTextBox.Text);
            TextBox partManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, partManufacturingMarginTextBox.Text);
            TextBox partConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, partConsumableMarginTextBox.Text);
            TextBox partExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, partExternalWorkMarginTextBox.Text);

            //verify in Raw Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TabPage rawPartOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            rawPartOHSettingsTab.Click();
            TextBox rawPartMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, rawPartMaterialOHTextBox.Text);
            TextBox rawPartCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, rawPartCommodityOHTextBox.Text);
            TextBox rawPartfacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, rawPartfacturingOHTextBox.Text);
            TextBox rawPartPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, rawPartPackagingOverheadTextBox.Text);
            TextBox rawPartSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, rawPartSalesAndAdministrationOverheadTextBox.Text);
            TextBox rawPartConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, rawPartConsumableOHTextBox.Text);
            TextBox rawPartExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, rawPartExternalWorkOHTextBox.Text);
            TextBox rawPartOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, rawPartOtherCostOHTextBox.Text);
            TextBox rawPartLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, rawPartLogisticOverheadTextBox.Text);
            TextBox rawPartCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, rawPartCompanySurchargeOHTextBox.Text);
            TextBox rawPartMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, rawPartMaterialMarginTextBox.Text);
            TextBox rawPartCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, rawPartCommodityMarginTextBox.Text);
            TextBox rawPartManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, rawPartManufacturingMarginTextBox.Text);
            TextBox rawPartConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, rawPartConsumableMarginTextBox.Text);
            TextBox rawPartExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, rawPartExternalWorkMarginTextBox.Text);
        }

        [When(@"I complete the Overhead and Margin Tab Applied Percentage fields")]
        public void WhenICompleteTheOverheadAndMarginTabAppliedPercentageFields()
        {
            TextBox materialOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.MaterialOverheadAPTextBox);
            materialOverheadAPTextBox.Text = "10";
            int materialOverheadValue = ((Convert.ToInt32(materialOverheadAPTextBox.Text)) * (Convert.ToInt32(materialOverhead))) / 100;
            materialOverhead = materialOverheadValue.ToString();
            TextBox consumableOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumableOverheadAPTextBox);
            consumableOverheadAPTextBox.Text = "10";
            int consumableOverheadValue = ((Convert.ToInt32(consumableOverheadAPTextBox.Text)) * (Convert.ToInt32(consumableOverhead))) / 100;
            consumableOverhead = consumableOverheadValue.ToString();
            TextBox commodityOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.CommodityOverheadAPTextBox);
            commodityOverheadAPTextBox.Text = "10";
            int commodityOverheadValue = ((Convert.ToInt32(commodityOverheadAPTextBox.Text)) * (Convert.ToInt32(commodityOverhead))) / 100;
            commodityOverhead = commodityOverheadValue.ToString();
            TextBox externalWorkOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ExternalWorkOverheadAPTextBox);
            externalWorkOverheadAPTextBox.Text = "10";
            int externalWorkOverheadValue = ((Convert.ToInt32(externalWorkOverheadAPTextBox.Text)) * (Convert.ToInt32(externalWorkOverhead))) / 100;
            externalWorkOverhead = externalWorkOverheadValue.ToString();
            TextBox manufacturingOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingOverheadAPTextBox);
            manufacturingOverheadAPTextBox.Text = "10";
            int manufacturingOverheadValue = ((Convert.ToInt32(manufacturingOverheadAPTextBox.Text)) * (Convert.ToInt32(manufacturingOverhead))) / 100;
            manufacturingOverhead = manufacturingOverheadValue.ToString();
            TextBox otherCostOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostOverheadAPTextBox);
            otherCostOverheadAPTextBox.Text = "10";
            int otherCostOverheadValue = ((Convert.ToInt32(otherCostOverheadAPTextBox.Text)) * (Convert.ToInt32(otherCostOverhead))) / 100;
            otherCostOverhead = manufacturingOverheadValue.ToString();
            TextBox packagingOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.PackagingOverheadAPTextBox);
            packagingOverheadAPTextBox.Text = "10";
            int packagingOverheadValue = ((Convert.ToInt32(packagingOverheadAPTextBox.Text)) * (Convert.ToInt32(packagingOverhead))) / 100;
            packagingOverhead = packagingOverheadValue.ToString();
            TextBox logisticOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticOverheadAPTextBox);
            logisticOverheadAPTextBox.Text = "10";
            int internalLogisticsOverheadValue = ((Convert.ToInt32(logisticOverheadAPTextBox.Text)) * (Convert.ToInt32(internalLogisticsOverhead))) / 100;
            internalLogisticsOverhead = internalLogisticsOverheadValue.ToString();
            TextBox salesAndAdminOverheadApTextBox = mainWindow.Get<TextBox>(AutomationIds.SalesAndAdminOverheadAPTextBox);
            salesAndAdminOverheadApTextBox.Text = "10";
            int salesAndAdministrationOverheadValue = ((Convert.ToInt32(salesAndAdminOverheadApTextBox.Text)) * (Convert.ToInt32(salesAndAdministrationOverhead))) / 100;
            salesAndAdministrationOverhead = salesAndAdministrationOverheadValue.ToString();
            TextBox companySurchargeOverheadAPTextBox = mainWindow.Get<TextBox>(AutomationIds.CompanySurchargeOverheadAPTextBox);
            companySurchargeOverheadAPTextBox.Text = "10";
            int companySurchargeOverheadValue = ((Convert.ToInt32(companySurchargeOverheadAPTextBox.Text)) * (Convert.ToInt32(companySurchargeOverhead))) / 100;
            companySurchargeOverhead = companySurchargeOverheadValue.ToString();
            TextBox materialMarginAPTextBox = mainWindow.Get<TextBox>(AutomationIds.MaterialMarginAPTextBox);
            materialMarginAPTextBox.Text = "10";
            int materialMarginValue = ((Convert.ToInt32(materialMarginAPTextBox.Text)) * (Convert.ToInt32(materialMargin))) / 100;
            materialMargin = materialMarginValue.ToString();
            TextBox consumableMaginAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumableMaginAPTextBox);
            consumableMaginAPTextBox.Text = "10";
            int consumableMarginValue = ((Convert.ToInt32(consumableMaginAPTextBox.Text)) * (Convert.ToInt32(consumableMargin))) / 100;
            consumableMargin = consumableMarginValue.ToString();
            TextBox commodityMarginAPTextBox = mainWindow.Get<TextBox>(AutomationIds.CommodityMarginAPTextBox);
            commodityMarginAPTextBox.Text = "10";
            int commodityMarginValue = ((Convert.ToInt32(commodityMarginAPTextBox.Text)) * (Convert.ToInt32(commodityMargin))) / 100;
            commodityMargin = commodityMarginValue.ToString();
            TextBox externalWorkMarginAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ExternalWorkMarginAPTextBox);
            externalWorkMarginAPTextBox.Text = "10";
            int externalWorkMarginValue = ((Convert.ToInt32(externalWorkMarginAPTextBox.Text)) * (Convert.ToInt32(externalWorkMargin))) / 100;
            externalWorkMargin = externalWorkMarginValue.ToString();
            TextBox manufacturingMarginAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingMarginAPTextBox);
            manufacturingMarginAPTextBox.Text = "10";
            int manufacturingMarginValue = ((Convert.ToInt32(manufacturingMarginAPTextBox.Text)) * (Convert.ToInt32(manufacturingMargin))) / 100;
            manufacturingMargin = manufacturingMarginValue.ToString();


        }

        [Then(@"I verify that Overhead and Margin tab fields are updated with AP values")]
        public void ThenIVerifyThatOverheadAndMarginTabFieldsAreUpdatedWithAPValues()
        {
            //verify in project
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            TabPage projectOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            projectOHSettingsTab.Click();
            TextBox projectMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, projectMaterialOHTextBox.Text);
            TextBox projectCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, projectCommodityOHTextBox.Text);
            TextBox projectManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, projectManufacturingOHTextBox.Text);
            TextBox projectPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, projectPackagingOverheadTextBox.Text);
            TextBox projectSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, projectSalesAndAdministrationOverheadTextBox.Text);
            TextBox projectConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, projectConsumableOHTextBox.Text);
            TextBox projectExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, projectExternalWorkOHTextBox.Text);
            TextBox projectOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, projectOtherCostOHTextBox.Text);
            TextBox projectLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, projectLogisticOverheadTextBox.Text);
            TextBox projectCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, projectCompanySurchargeOHTextBox.Text);
            TextBox projectMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, projectMaterialMarginTextBox.Text);
            TextBox projectCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, projectCommodityMarginTextBox.Text);
            TextBox projectManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, projectManufacturingMarginTextBox.Text);
            TextBox projectConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, projectConsumableMarginTextBox.Text);
            TextBox projectExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, projectExternalWorkMarginTextBox.Text);

            //assembly
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            TabPage assemblyOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            assemblyOHSettingsTab.Click();
            TextBox assemblyMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, assemblyMaterialOHTextBox.Text);
            TextBox assemblyCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, assemblyCommodityOHTextBox.Text);
            TextBox assemblyManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, assemblyManufacturingOHTextBox.Text);
            TextBox assemblyPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, assemblyPackagingOverheadTextBox.Text);
            TextBox assemblySalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, assemblySalesAndAdministrationOverheadTextBox.Text);
            TextBox assemblyConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, assemblyConsumableOHTextBox.Text);
            TextBox assemblyExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, assemblyExternalWorkOHTextBox.Text);
            TextBox assemblyOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, assemblyOtherCostOHTextBox.Text);
            TextBox assemblyLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, assemblyLogisticOverheadTextBox.Text);
            TextBox assemblyCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, assemblyCompanySurchargeOHTextBox.Text);
            TextBox assemblyMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, assemblyMaterialMarginTextBox.Text);
            TextBox assemblyCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, assemblyCommodityMarginTextBox.Text);
            TextBox assemblyManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, assemblyManufacturingMarginTextBox.Text);
            TextBox assemblyConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, assemblyConsumableMarginTextBox.Text);
            TextBox assemblyExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, assemblyExternalWorkMarginTextBox.Text);

            //verify in part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TabPage partOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            partOHSettingsTab.Click();
            TextBox partMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, partMaterialOHTextBox.Text);
            TextBox partCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, partCommodityOHTextBox.Text);
            TextBox partManufacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, partManufacturingOHTextBox.Text);
            TextBox partPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, partPackagingOverheadTextBox.Text);
            TextBox partSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, partSalesAndAdministrationOverheadTextBox.Text);
            TextBox partConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, partConsumableOHTextBox.Text);
            TextBox partExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, partExternalWorkOHTextBox.Text);
            TextBox partOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, partOtherCostOHTextBox.Text);
            TextBox partLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, partLogisticOverheadTextBox.Text);
            TextBox partCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, partCompanySurchargeOHTextBox.Text);
            TextBox partMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, partMaterialMarginTextBox.Text);
            TextBox partCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, partCommodityMarginTextBox.Text);
            TextBox partManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, partManufacturingMarginTextBox.Text);
            TextBox partConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, partConsumableMarginTextBox.Text);
            TextBox partExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, partExternalWorkMarginTextBox.Text);

            //verify in Raw Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TabPage rawPartOHSettingsTab = mainWindow.Get<TabPage>("OHSettingsTab");
            rawPartOHSettingsTab.Click();
            TextBox rawPartMaterialOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.AreEqual(materialOverhead, rawPartMaterialOHTextBox.Text);
            TextBox rawPartCommodityOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.AreEqual(commodityOverhead, rawPartCommodityOHTextBox.Text);
            TextBox rawPartfacturingOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.AreEqual(manufacturingOverhead, rawPartfacturingOHTextBox.Text);
            TextBox rawPartPackagingOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.AreEqual(packagingOverhead, rawPartPackagingOverheadTextBox.Text);
            TextBox rawPartSalesAndAdministrationOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.AreEqual(salesAndAdministrationOverhead, rawPartSalesAndAdministrationOverheadTextBox.Text);
            TextBox rawPartConsumableOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.AreEqual(consumableOverhead, rawPartConsumableOHTextBox.Text);
            TextBox rawPartExternalWorkOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.AreEqual(externalWorkOverhead, rawPartExternalWorkOHTextBox.Text);
            TextBox rawPartOtherCostOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.AreEqual(otherCostOverhead, rawPartOtherCostOHTextBox.Text);
            TextBox rawPartLogisticOverheadTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.AreEqual(internalLogisticsOverhead, rawPartLogisticOverheadTextBox.Text);
            TextBox rawPartCompanySurchargeOHTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.AreEqual(companySurchargeOverhead, rawPartCompanySurchargeOHTextBox.Text);
            TextBox rawPartMaterialMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.AreEqual(materialMargin, rawPartMaterialMarginTextBox.Text);
            TextBox rawPartCommodityMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.AreEqual(commodityMargin, rawPartCommodityMarginTextBox.Text);
            TextBox rawPartManufacturingMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.AreEqual(manufacturingMargin, rawPartManufacturingMarginTextBox.Text);
            TextBox rawPartConsumableMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.AreEqual(consumableMargin, rawPartConsumableMarginTextBox.Text);
            TextBox rawPartExternalWorkMarginTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.AreEqual(externalWorkMargin, rawPartExternalWorkMarginTextBox.Text);
        }


        [When(@"I select Country Settings Tab")]
        public void WhenISelectCountrySettingsTab()
        {
            TabPage countrySettingsTab = mainWindow.Get<TabPage>(AutomationIds.MDUCountrySettingsTab);
            countrySettingsTab.Click();
        }

        [When(@"I complete the Country Settings Tab fields")]
        public void WhenICompleteTheCountrySettingsTabFields()
        {
            TextBox manufacturingLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.MDUManufacturingLocationTextBox);
            Button browseCountryButton = mainWindow.Get<Button>(AutomationIds.MDUManufacturingLocationButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = application.Windows.CountryAndSupplierBrowser;
            browseMasterDataWindow.WaitForAsyncUITasks();

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Belgium");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");
            selectButton.Click();
            manufacturingLocation = manufacturingLocationTextBox.Text;
        }

        [Then(@"I verify that Country Settings tab fields are updated")]
        public void ThenIVerifyThatCountrySettingsTabFieldsAreUpdated()
        {
            //assembly
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            TextBox projectCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.AreEqual(manufacturingLocation, projectCountryTextBox.Text);

            //part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            TextBox partCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.AreEqual(manufacturingLocation, partCountryTextBox.Text);

            //verify in Raw Part
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TextBox rawPartCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.AreEqual(manufacturingLocation, rawPartCountryTextBox.Text);
        }


        [When(@"I select Machines tab")]
        public void WhenISelectMachinesTab()
        {
            TabPage mschinesTab = mainWindow.Get<TabPage>(AutomationIds.MDUMachinesTab);
            mschinesTab.Click();
        }

        [When(@"I complete the Machines Tab fields")]
        public void WhenICompleteTheMachinesTabFields()
        {
            //var data = mainWindow.Get<DateTimePicker>(SearchCriteria.ByAutomationId(AutomationIds.ManufacturingYearPicker));
            //DateTime selectedDate = new DateTime(2014);
            //data.SetDate(selectedDate, DateFormat.CultureDefault);
            //data.SetValue(selectedDate);
            TextBox manufacturingYearNewValueNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ManufacturingYearNewValueNVTextBox);
            TextBox depreciationPeriodNewValueNVTextBox = mainWindow.Get<TextBox>(AutomationIds.DepreciationPeriodNewValueNVTextBox);
            depreciationPeriodNewValueNVTextBox.Text = "10";
            depreciationPeriod = depreciationPeriodNewValueNVTextBox.Text;
            TextBox interestRateNVTextBox = mainWindow.Get<TextBox>(AutomationIds.InterestRateNVTextBox);
            interestRateNVTextBox.Text = "10";
            interestRate = interestRateNVTextBox.Text;
            TextBox maintenanceValueNVTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceValueNVTextBox);
            maintenanceValueNVTextBox.Text = "10";
            maintenanceValue = maintenanceValueNVTextBox.Text;
            TextBox consumablesCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumablesCostNVTextBox);
            consumablesCostNVTextBox.Text = "10";
            consumableCost = consumablesCostNVTextBox.Text;
            CheckBox projectSpecificCheckBox = mainWindow.Get<CheckBox>(AutomationIds.ProjectSpecificCheckBox);
            projectSpecificCheckBox.Checked = false;
            TextBox floorSizeNVTextBox = mainWindow.Get<TextBox>(AutomationIds.FloorSizeTextBox);
            floorSizeNVTextBox.Text = "10";
            floorSize = floorSizeNVTextBox.Text;
            TextBox workspaceAreaNVTextBox = mainWindow.Get<TextBox>(AutomationIds.WorkspaceAreaTextBox);
            workspaceAreaNVTextBox.Text = "10";
            workspaceArea = workspaceAreaNVTextBox.Text;
            TextBox powerConsumptionNVTextBox = mainWindow.Get<TextBox>(AutomationIds.PowerConsumptionTextBox);
            powerConsumptionNVTextBox.Text = "10";
            powerConsumption = powerConsumptionNVTextBox.Text;
            TextBox airConsumptionNVTextBox = mainWindow.Get<TextBox>(AutomationIds.AirConsumptionTextBox);
            airConsumptionNVTextBox.Text = "10";
            airConsumption = airConsumptionNVTextBox.Text;
            TextBox waterConsumptionNVTextBox = mainWindow.Get<TextBox>(AutomationIds.WaterConsumptionTextBox);
            waterConsumptionNVTextBox.Text = "10";
            waterConsumption = waterConsumptionNVTextBox.Text;
            TextBox fullLoadRateNVTextBox = mainWindow.Get<TextBox>(AutomationIds.FullLoadRateTextBox);
            fullLoadRateNVTextBox.Text = "10";
            fullLoadRate = fullLoadRateNVTextBox.Text;
            TextBox maxCapacityNVTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxCapacityTextBox);
            maxCapacityNVTextBox.Text = "10";
            maxCapacity = maxCapacityNVTextBox.Text;
            TextBox oEENVTextBox = mainWindow.Get<TextBox>(AutomationIds.OEETextBox);
            oEENVTextBox.Text = "10";
            oee = oEENVTextBox.Text;
            TextBox availabilityNVTextBox = mainWindow.Get<TextBox>(AutomationIds.AvailabilityTextBox);
            availabilityNVTextBox.Text = "10";
            availability = availabilityNVTextBox.Text;
        }

        [When(@"I verify that Machines tab fields are updated")]
        public void WhenIVerifyThatMachinesTabFieldsAreUpdated()
        {
            //verify in assembly process step
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TabPage assemblyProcessStepMachineTab = mainWindow.Get<TabPage>("MachinesListTab");
            assemblyProcessStepMachineTab.Click();
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "Machine");
            Button editButton = mainWindow.Get<Button>("EditButton");
            editButton.Click();

            TabPage assemblyProcessStepMachineGeneralTab = mainWindow.Get<TabPage>("GeneralTabItem");
            assemblyProcessStepMachineGeneralTab.Click();
            TextBox assemblyProcessStepDepreciationPeriodTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            Assert.AreEqual(depreciationPeriod, assemblyProcessStepDepreciationPeriodTextBox.Text);
            TextBox assemblyProcessStepDepreciationRateTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.AreEqual(interestRate, assemblyProcessStepDepreciationRateTextBox.Text);

            TabPage assemblyProcessStepMachineMaintenanceTab = mainWindow.Get<TabPage>("MaintenanceData");
            assemblyProcessStepMachineMaintenanceTab.Click();
            TextBox assemblyProcessStepConsumablesCostTextBox = mainWindow.Get<TextBox>("ManualConsumableCostPercentageTextBox");
            Assert.AreEqual(consumableCost, assemblyProcessStepConsumablesCostTextBox.Text);
            TextBox assemblyProcessStepKValueTextBox = mainWindow.Get<TextBox>("KValueTextBox");
            Assert.AreEqual(maintenanceValue, assemblyProcessStepKValueTextBox.Text);

            TabPage assemblyProcessStepMachineFloorEnergyTab = mainWindow.Get<TabPage>("FloorEnergyData");
            assemblyProcessStepMachineFloorEnergyTab.Click();
            TextBox assemblyProcessStepFloorSizeTextBox = mainWindow.Get<TextBox>("FloorSizeTextBox");
            Assert.AreEqual(floorSize, assemblyProcessStepFloorSizeTextBox.Text);
            TextBox assemblyProcessStepWorkspaceAreaTextBox = mainWindow.Get<TextBox>("WorkspaceAreaTextBox");
            Assert.AreEqual(workspaceArea, assemblyProcessStepWorkspaceAreaTextBox.Text);
            TextBox assemblyProcessStepPowerConsumptionTextBox = mainWindow.Get<TextBox>("PowerConsumptionTextBox");
            Assert.AreEqual(powerConsumption, assemblyProcessStepPowerConsumptionTextBox.Text);
            TextBox assemblyProcessStepAirConsumptionTextBox = mainWindow.Get<TextBox>("AirConsumptionTextBox");
            Assert.AreEqual(airConsumption, assemblyProcessStepAirConsumptionTextBox.Text);
            TextBox assemblyProcessStepWaterConsumptionTextBox = mainWindow.Get<TextBox>("WaterConsumptionTextBox");
            Assert.AreEqual(waterConsumption, assemblyProcessStepWaterConsumptionTextBox.Text);
            TextBox assemblyProcessStepFullLoadRateTextBox = mainWindow.Get<TextBox>("FullLoadRateTextBox");
            Assert.AreEqual(fullLoadRate, assemblyProcessStepFullLoadRateTextBox.Text);
            TextBox assemblyProcessStepMaxCapacityTextBox = mainWindow.Get<TextBox>("MaxCapacityTextBox");
            Assert.AreEqual(maxCapacity, assemblyProcessStepMaxCapacityTextBox.Text);
            TextBox assemblyProcessStepOEETextBox = mainWindow.Get<TextBox>("OEETextBox");
            Assert.AreEqual(oee, assemblyProcessStepOEETextBox.Text);
            TextBox assemblyProcessStepAvailabilityTextBox = mainWindow.Get<TextBox>("AvailabilityTextBox");
            Assert.AreEqual(availability, assemblyProcessStepAvailabilityTextBox.Text);
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            //verify in part process step
            myProjectsNode.ExpandEx();
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TabPage partProcessStepMachineTab = mainWindow.Get<TabPage>("MachinesListTab");
            partProcessStepMachineTab.Click();
            ListViewControl partMachinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            partMachinesDataGrid.Select("Name", "MachinePart");
            Button editMachineButton = mainWindow.Get<Button>("EditButton");
            editMachineButton.Click();
            TabPage partProcessStepMachineGeneralTab = mainWindow.Get<TabPage>("GeneralTabItem");
            partProcessStepMachineGeneralTab.Click();
            TextBox partProcessStepDepreciationPeriodTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            Assert.AreEqual(depreciationPeriod, partProcessStepDepreciationPeriodTextBox.Text);
            TextBox partProcessStepDepreciationRateTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.AreEqual(interestRate, partProcessStepDepreciationRateTextBox.Text);

            TabPage partProcessStepMachineMaintenanceTab = mainWindow.Get<TabPage>("MaintenanceData");
            partProcessStepMachineMaintenanceTab.Click();
            TextBox partProcessStepConsumablesCostTextBox = mainWindow.Get<TextBox>("ManualConsumableCostPercentageTextBox");
            Assert.AreEqual(consumableCost, partProcessStepConsumablesCostTextBox.Text);
            TextBox partProcessStepKValueTextBox = mainWindow.Get<TextBox>("KValueTextBox");
            Assert.AreEqual(maintenanceValue, partProcessStepKValueTextBox.Text);

            TabPage partProcessStepMachineFloorEnergyTab = mainWindow.Get<TabPage>("FloorEnergyData");
            partProcessStepMachineFloorEnergyTab.Click();
            TextBox partProcessStepFloorSizeTextBox = mainWindow.Get<TextBox>("FloorSizeTextBox");
            Assert.AreEqual(floorSize, partProcessStepFloorSizeTextBox.Text);
            TextBox partProcessStepWorkspaceAreaTextBox = mainWindow.Get<TextBox>("WorkspaceAreaTextBox");
            Assert.AreEqual(workspaceArea, partProcessStepWorkspaceAreaTextBox.Text);
            TextBox partProcessStepPowerConsumptionTextBox = mainWindow.Get<TextBox>("PowerConsumptionTextBox");
            Assert.AreEqual(powerConsumption, partProcessStepPowerConsumptionTextBox.Text);
            TextBox partProcessStepAirConsumptionTextBox = mainWindow.Get<TextBox>("AirConsumptionTextBox");
            Assert.AreEqual(airConsumption, partProcessStepAirConsumptionTextBox.Text);
            TextBox partProcessStepWaterConsumptionTextBox = mainWindow.Get<TextBox>("WaterConsumptionTextBox");
            Assert.AreEqual(waterConsumption, partProcessStepWaterConsumptionTextBox.Text);
            TextBox partProcessStepFullLoadRateTextBox = mainWindow.Get<TextBox>("FullLoadRateTextBox");
            Assert.AreEqual(fullLoadRate, partProcessStepFullLoadRateTextBox.Text);
            TextBox partProcessStepMaxCapacityTextBox = mainWindow.Get<TextBox>("MaxCapacityTextBox");
            Assert.AreEqual(maxCapacity, partProcessStepMaxCapacityTextBox.Text);
            TextBox partProcessStepOEETextBox = mainWindow.Get<TextBox>("OEETextBox");
            Assert.AreEqual(oee, partProcessStepOEETextBox.Text);
            TextBox partProcessStepAvailabilityTextBox = mainWindow.Get<TextBox>("AvailabilityTextBox");
            Assert.AreEqual(availability, partProcessStepAvailabilityTextBox.Text);
            Button partCancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            partCancelButton.Click();

        }

        [When(@"I complete the Machines Tab Applied Percentage fields")]
        public void WhenICompleteTheMachinesTabAppliedPercentageFields()
        {
            TextBox interestRateAPTextBox = mainWindow.Get<TextBox>(AutomationIds.InterestRateAPTextBox);
            interestRateAPTextBox.Text = "10";
            int interestRateValue = ((Convert.ToInt32(interestRateAPTextBox.Text)) * (Convert.ToInt32(interestRate))) / 100;
            interestRate = interestRateValue.ToString();

            TextBox maintenanceValueAPTextBox = mainWindow.Get<TextBox>(AutomationIds.MaintenanceValueAPTextBox);
            maintenanceValueAPTextBox.Text = "10";
            int maintenanceValueValue = ((Convert.ToInt32(maintenanceValueAPTextBox.Text)) * (Convert.ToInt32(maintenanceValue))) / 100;
            maintenanceValue = maintenanceValueValue.ToString();
            TextBox consumablesCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ConsumablesCostAPTextBox);
            consumablesCostAPTextBox.Text = "10";
            int consumableCostValue = ((Convert.ToInt32(consumablesCostAPTextBox.Text)) * (Convert.ToInt32(consumableCost))) / 100;
            consumableCost = consumableCostValue.ToString();
            TextBox floorSizeAPTextBox = mainWindow.Get<TextBox>(AutomationIds.FloorSizeAPTextBox);
            floorSizeAPTextBox.Text = "10";
            int floorSizeValue = ((Convert.ToInt32(floorSizeAPTextBox.Text)) * (Convert.ToInt32(floorSize))) / 100;
            floorSize = floorSizeValue.ToString();
            TextBox workspaceAreaAPTextBox = mainWindow.Get<TextBox>(AutomationIds.WorkspaceAreaAPTextBox);
            workspaceAreaAPTextBox.Text = "10";
            int workspaceAreaValue = ((Convert.ToInt32(workspaceAreaAPTextBox.Text)) * (Convert.ToInt32(workspaceArea))) / 100;
            workspaceArea = workspaceAreaValue.ToString();
            TextBox powerConsumptionAPTextBox = mainWindow.Get<TextBox>(AutomationIds.PowerConsumptionAPTextBox);
            powerConsumptionAPTextBox.Text = "10";
            int powerConsumptionValue = ((Convert.ToInt32(powerConsumptionAPTextBox.Text)) * (Convert.ToInt32(powerConsumption))) / 100;
            powerConsumption = powerConsumptionValue.ToString();
            TextBox airConsumptionAPTextBox = mainWindow.Get<TextBox>(AutomationIds.AirConsumptionAPTextBox);
            airConsumptionAPTextBox.Text = "10";
            int airConsumptionValue = ((Convert.ToInt32(airConsumptionAPTextBox.Text)) * (Convert.ToInt32(airConsumption))) / 100;
            airConsumption = airConsumptionValue.ToString();
            TextBox waterConsumptionAPTextBox = mainWindow.Get<TextBox>(AutomationIds.WaterConsumptionAPTextBox);
            waterConsumptionAPTextBox.Text = "10";
            int waterConsumptionValue = ((Convert.ToInt32(waterConsumptionAPTextBox.Text)) * (Convert.ToInt32(waterConsumption))) / 100;
            waterConsumption = waterConsumptionValue.ToString();
            TextBox fullLoadRateAPTextBox = mainWindow.Get<TextBox>(AutomationIds.FullLoadRateAPTextBox);
            fullLoadRateAPTextBox.Text = "10";
            int fullLoadRateValue = ((Convert.ToInt32(fullLoadRateAPTextBox.Text)) * (Convert.ToInt32(fullLoadRate))) / 100;
            fullLoadRate = fullLoadRateValue.ToString();
            TextBox oEEAPTextBox = mainWindow.Get<TextBox>(AutomationIds.OEEAPTextBox);
            oEEAPTextBox.Text = "10";
            int oeeValue = ((Convert.ToInt32(oEEAPTextBox.Text)) * (Convert.ToInt32(oee))) / 100;
            oee = fullLoadRateValue.ToString();
            TextBox availabilityAPTextBox = mainWindow.Get<TextBox>(AutomationIds.AvailabilityAPTextBox);
            availabilityAPTextBox.Text = "10";
            int availabilityValue = ((Convert.ToInt32(availabilityAPTextBox.Text)) * (Convert.ToInt32(availability))) / 100;
            availability = availabilityValue.ToString();
        }

        [Then(@"I verify that Machines tab fields are updated with AP values")]
        public void ThenIVerifyThatMachinesTabFieldsAreUpdatedWithAPValues()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyProcessNode = assemblyNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            assemblyProcessNode.SelectEx();
            assemblyProcessNode.ExpandEx();
            TreeNode assemblyProcessStepNode = assemblyProcessNode.GetNode("Step1");
            assemblyProcessStepNode.SelectEx();
            TabPage assemblyProcessStepMachineTab = mainWindow.Get<TabPage>("MachinesListTab");
            assemblyProcessStepMachineTab.Click();
            ListViewControl machinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            machinesDataGrid.Select("Name", "Machine");
            machinesDataGrid.DoubleClick();
            Button editButton = mainWindow.Get<Button>("EditButton");
            editButton.Click();

            TabPage assemblyProcessStepMachineGeneralTab = mainWindow.Get<TabPage>("GeneralTabItem");
            assemblyProcessStepMachineGeneralTab.Click();
            TextBox assemblyProcessStepDepreciationPeriodTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            Assert.AreEqual(depreciationPeriod, assemblyProcessStepDepreciationPeriodTextBox.Text);
            TextBox assemblyProcessStepDepreciationRateTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.AreEqual(interestRate, assemblyProcessStepDepreciationRateTextBox.Text);

            TabPage assemblyProcessStepMachineMaintenanceTab = mainWindow.Get<TabPage>("MaintenanceData");
            assemblyProcessStepMachineMaintenanceTab.Click();
            TextBox assemblyProcessStepConsumablesCostTextBox = mainWindow.Get<TextBox>("ManualConsumableCostPercentageTextBox");
            Assert.AreEqual(consumableCost, assemblyProcessStepConsumablesCostTextBox.Text);
            TextBox assemblyProcessStepKValueTextBox = mainWindow.Get<TextBox>("KValueTextBox");
            Assert.AreEqual(maintenanceValue, assemblyProcessStepKValueTextBox.Text);

            TabPage assemblyProcessStepMachineFloorEnergyTab = mainWindow.Get<TabPage>("FloorEnergyData");
            assemblyProcessStepMachineFloorEnergyTab.Click();
            TextBox assemblyProcessStepFloorSizeTextBox = mainWindow.Get<TextBox>("FloorSizeTextBox");
            Assert.AreEqual(floorSize, assemblyProcessStepFloorSizeTextBox.Text);
            TextBox assemblyProcessStepWorkspaceAreaTextBox = mainWindow.Get<TextBox>("WorkspaceAreaTextBox");
            Assert.AreEqual(workspaceArea, assemblyProcessStepWorkspaceAreaTextBox.Text);
            TextBox assemblyProcessStepPowerConsumptionTextBox = mainWindow.Get<TextBox>("PowerConsumptionTextBox");
            Assert.AreEqual(powerConsumption, assemblyProcessStepPowerConsumptionTextBox.Text);
            TextBox assemblyProcessStepAirConsumptionTextBox = mainWindow.Get<TextBox>("AirConsumptionTextBox");
            Assert.AreEqual(airConsumption, assemblyProcessStepAirConsumptionTextBox.Text);
            TextBox assemblyProcessStepWaterConsumptionTextBox = mainWindow.Get<TextBox>("WaterConsumptionTextBox");
            Assert.AreEqual(waterConsumption, assemblyProcessStepWaterConsumptionTextBox.Text);
            TextBox assemblyProcessStepFullLoadRateTextBox = mainWindow.Get<TextBox>("FullLoadRateTextBox");
            Assert.AreEqual(fullLoadRate, assemblyProcessStepFullLoadRateTextBox.Text);
            TextBox assemblyProcessStepMaxCapacityTextBox = mainWindow.Get<TextBox>("MaxCapacityTextBox");
            Assert.AreEqual(maxCapacity, assemblyProcessStepMaxCapacityTextBox.Text);
            TextBox assemblyProcessStepOEETextBox = mainWindow.Get<TextBox>("OEETextBox");
            Assert.AreEqual(oee, assemblyProcessStepOEETextBox.Text);
            TextBox assemblyProcessStepAvailabilityTextBox = mainWindow.Get<TextBox>("AvailabilityTextBox");
            Assert.AreEqual(availability, assemblyProcessStepAvailabilityTextBox.Text);
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            //verify in part process step
            myProjectsNode.ExpandEx();
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partProcessNode = partNode.GetNodeByAutomationId(ProcessStepAutomationIds.ProcessNode);
            partProcessNode.SelectEx();
            partProcessNode.ExpandEx();
            TreeNode partProcessStepNode = partProcessNode.GetNode("StepPart");
            partProcessStepNode.SelectEx();
            TabPage partProcessStepMachineTab = mainWindow.Get<TabPage>("MachinesListTab");
            partProcessStepMachineTab.Click();
            ListViewControl partMachinesDataGrid = Wait.For(() => mainWindow.GetListView(MachineAutomationIds.MachinesDataGrid));
            partMachinesDataGrid.Select("Name", "MachinePart");
            Button editMachineButton = mainWindow.Get<Button>("EditButton");
            editMachineButton.Click();
            TabPage partProcessStepMachineGeneralTab = mainWindow.Get<TabPage>("GeneralTabItem");
            partProcessStepMachineGeneralTab.Click();
            TextBox partProcessStepDepreciationPeriodTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationPeriodTextBox);
            Assert.AreEqual(depreciationPeriod, partProcessStepDepreciationPeriodTextBox.Text);
            TextBox partProcessStepDepreciationRateTextBox = mainWindow.Get<TextBox>(MachineAutomationIds.DepreciationRateTextBox);
            Assert.AreEqual(interestRate, partProcessStepDepreciationRateTextBox.Text);

            TabPage partProcessStepMachineMaintenanceTab = mainWindow.Get<TabPage>("MaintenanceData");
            partProcessStepMachineMaintenanceTab.Click();
            TextBox partProcessStepConsumablesCostTextBox = mainWindow.Get<TextBox>("ManualConsumableCostPercentageTextBox");
            Assert.AreEqual(consumableCost, partProcessStepConsumablesCostTextBox.Text);
            TextBox partProcessStepKValueTextBox = mainWindow.Get<TextBox>("KValueTextBox");
            Assert.AreEqual(maintenanceValue, partProcessStepKValueTextBox.Text);

            TabPage partProcessStepMachineFloorEnergyTab = mainWindow.Get<TabPage>("FloorEnergyData");
            partProcessStepMachineFloorEnergyTab.Click();
            TextBox partProcessStepFloorSizeTextBox = mainWindow.Get<TextBox>("FloorSizeTextBox");
            Assert.AreEqual(floorSize, partProcessStepFloorSizeTextBox.Text);
            TextBox partProcessStepWorkspaceAreaTextBox = mainWindow.Get<TextBox>("WorkspaceAreaTextBox");
            Assert.AreEqual(workspaceArea, partProcessStepWorkspaceAreaTextBox.Text);
            TextBox partProcessStepPowerConsumptionTextBox = mainWindow.Get<TextBox>("PowerConsumptionTextBox");
            Assert.AreEqual(powerConsumption, partProcessStepPowerConsumptionTextBox.Text);
            TextBox partProcessStepAirConsumptionTextBox = mainWindow.Get<TextBox>("AirConsumptionTextBox");
            Assert.AreEqual(airConsumption, partProcessStepAirConsumptionTextBox.Text);
            TextBox partProcessStepWaterConsumptionTextBox = mainWindow.Get<TextBox>("WaterConsumptionTextBox");
            Assert.AreEqual(waterConsumption, partProcessStepWaterConsumptionTextBox.Text);
            TextBox partProcessStepFullLoadRateTextBox = mainWindow.Get<TextBox>("FullLoadRateTextBox");
            Assert.AreEqual(fullLoadRate, partProcessStepFullLoadRateTextBox.Text);
            TextBox partProcessStepMaxCapacityTextBox = mainWindow.Get<TextBox>("MaxCapacityTextBox");
            Assert.AreEqual(maxCapacity, partProcessStepMaxCapacityTextBox.Text);
            TextBox partProcessStepOEETextBox = mainWindow.Get<TextBox>("OEETextBox");
            Assert.AreEqual(oee, partProcessStepOEETextBox.Text);
            TextBox partProcessStepAvailabilityTextBox = mainWindow.Get<TextBox>("AvailabilityTextBox");
            Assert.AreEqual(availability, partProcessStepAvailabilityTextBox.Text);
            Button partCancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            partCancelButton.Click();
        }

        [When(@"I select Additional Costs tab")]
        public void WhenISelectAdditionalCostsTab()
        {
            TabPage additionalCostsTab = mainWindow.Get<TabPage>(AutomationIds.MDUAdditionalCostsTab);
            additionalCostsTab.Click();
        }

        [When(@"I complete the Additional Costs Tab fields")]
        public void WhenICompleteTheAdditionalCostsTabFields()
        {
            TextBox developmentCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostNVTextBox);
            developmentCostNVTextBox.Text = "10";
            developmentCost = developmentCostNVTextBox.Text;
            TextBox projectInvestNVTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestNVTextBox);
            projectInvestNVTextBox.Text = "10";
            projectInvest = projectInvestNVTextBox.Text;
            CheckBox autoCalculateCheckBox = mainWindow.Get<CheckBox>(AutomationIds.AutoCalculateLogisticCostCheckBox);
            autoCalculateCheckBox.Checked = false;
            TextBox internalLogisticCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.InternalLogisticCostNVTextBox);
            internalLogisticCostNVTextBox.Text = "10";
            internalLogisticCost = internalLogisticCostNVTextBox.Text;
            TextBox transportCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostNVTextBox);
            transportCostNVTextBox.Text = "10";
            transportCost = transportCostNVTextBox.Text;
            TextBox packagingCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.PackagingCostNVTextBox);
            packagingCostNVTextBox.Text = "10";
            packagingCost = packagingCostNVTextBox.Text;
            TextBox otherCostNVTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostNVTextBox);
            otherCostNVTextBox.Text = "10";
            otherCost = otherCostNVTextBox.Text;
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            paymentTermsTextBox.Text = "10";
            paymentTerms = paymentTermsTextBox.Text;
        }

        [When(@"I verify that Additional Costs tab fields are updated")]
        public void WhenIVerifyThatAdditionalCostsTabFieldsAreUpdated()
        {
            //verify in assembly Result Details
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();            
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyResultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            assemblyResultDetailsNode.SelectEx();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, developmentCostTextBox.Text);
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, transportCostTextBox.Text);
            TextBox packingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, packingCostTextBox.Text);
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, paymentTermsTextBox.Text);
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, projectInvestTextBox.Text);
            TextBox internalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, internalLogisticCostTextBox.Text);
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, otherCostTextBox.Text);


            //verify in part Result Details
            myProjectsNode.ExpandEx();
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partResultDetailsNode = partNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            partResultDetailsNode.SelectEx();
            TextBox partDevelopmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, partDevelopmentCostTextBox.Text);
            TextBox partTransportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, partTransportCostTextBox.Text);
            TextBox PartPackingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, PartPackingCostTextBox.Text);
            TextBox PartPaymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, PartPaymentTermsTextBox.Text);
            TextBox partProjectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, partProjectInvestTextBox.Text);
            TextBox partInternalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, partInternalLogisticCostTextBox.Text);
            TextBox partOtherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, partOtherCostTextBox.Text);

            //verify in raw part Result Details
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            rawPartNode.ExpandEx();
            TreeNode rawPartResultDetailsNode = rawPartNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            rawPartResultDetailsNode.SelectEx();
            TextBox rawPartDevelopmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, rawPartDevelopmentCostTextBox.Text);
            TextBox rawPartTransportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, rawPartTransportCostTextBox.Text);
            TextBox rawPartPackingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, rawPartPackingCostTextBox.Text);
            TextBox rawPartPaymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, rawPartPaymentTermsTextBox.Text);
            TextBox rawPartProjectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, rawPartProjectInvestTextBox.Text);
            TextBox rawPartInternalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, rawPartInternalLogisticCostTextBox.Text);
            TextBox rawPartOtherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, rawPartOtherCostTextBox.Text);

        }

        [When(@"I complete the Additional Costs Tab Applied Percentage fields")]
        public void WhenICompleteTheAdditionalCostsTabAppliedPercentageFields()
        {
            TextBox developmentCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostAPTextBox);
            developmentCostAPTextBox.Text = "10";
            int developmentCostValue = ((Convert.ToInt32(developmentCostAPTextBox.Text)) * (Convert.ToInt32(developmentCost))) / 100;
            developmentCost = developmentCostValue.ToString();
            TextBox projectInvestAPTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestAPTextBox);
            projectInvestAPTextBox.Text = "10";
            int projectInvestValue = ((Convert.ToInt32(projectInvestAPTextBox.Text)) * (Convert.ToInt32(projectInvest))) / 100;
            projectInvest = projectInvestValue.ToString();
            TextBox internalLogisticCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.InternalLogisticCostAPTextBox);
            internalLogisticCostAPTextBox.Text = "10";
            int internalLogisticCostValue = ((Convert.ToInt32(internalLogisticCostAPTextBox.Text)) * (Convert.ToInt32(internalLogisticCost))) / 100;
            internalLogisticCost = internalLogisticCostValue.ToString();
            TextBox transportCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostAPTextBox);
            transportCostAPTextBox.Text = "10";
            int transportCostValue = ((Convert.ToInt32(transportCostAPTextBox.Text)) * (Convert.ToInt32(transportCost))) / 100;
            transportCost = transportCostValue.ToString();
            TextBox packagingCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.PackagingCostAPTextBox);
            packagingCostAPTextBox.Text = "10";
            int packagingCostValue = ((Convert.ToInt32(packagingCostAPTextBox.Text)) * (Convert.ToInt32(packagingCost))) / 100;
            packagingCost = packagingCostValue.ToString();
            TextBox otherCostAPTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostAPTextBox);
            otherCostAPTextBox.Text = "10";
            int otherCostValue = ((Convert.ToInt32(otherCostAPTextBox.Text)) * (Convert.ToInt32(otherCost))) / 100;
            otherCost = packagingCostValue.ToString();
        }

        [Then(@"I verify that Additional Costs tab fields are updated with AP values")]
        public void ThenIVerifyThatAdditionalCostsTabFieldsAreUpdatedWithAPVvalues()
        {
            //verify in assembly Result Details
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode assemblyNode = projectNode.GetNode("AssemblyMDU2");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();
            TreeNode assemblyResultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            assemblyResultDetailsNode.SelectEx();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, developmentCostTextBox.Text);
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, transportCostTextBox.Text);
            TextBox packingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, packingCostTextBox.Text);
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, paymentTermsTextBox.Text);
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, projectInvestTextBox.Text);
            TextBox internalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, internalLogisticCostTextBox.Text);
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, otherCostTextBox.Text);


            //verify in part Result Details
            myProjectsNode.ExpandEx();
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode partResultDetailsNode = partNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            partResultDetailsNode.SelectEx();
            TextBox partDevelopmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, partDevelopmentCostTextBox.Text);
            TextBox partTransportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, partTransportCostTextBox.Text);
            TextBox PartPackingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, PartPackingCostTextBox.Text);
            TextBox PartPaymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, PartPaymentTermsTextBox.Text);
            TextBox partProjectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, partProjectInvestTextBox.Text);
            TextBox partInternalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, partInternalLogisticCostTextBox.Text);
            TextBox partOtherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, partOtherCostTextBox.Text);

            //verify in raw part Result Details
            projectNode.SelectEx();
            projectNode.ExpandEx();
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawPartNode = materialsNode.GetNode("RawPart1");
            rawPartNode.SelectEx();
            TreeNode rawPartResultDetailsNode = rawPartNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            rawPartResultDetailsNode.SelectEx();
            TextBox rawPartDevelopmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(developmentCost, rawPartDevelopmentCostTextBox.Text);
            TextBox rawPartTransportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCost, rawPartTransportCostTextBox.Text);
            TextBox rawPartPackingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packagingCost, rawPartPackingCostTextBox.Text);
            TextBox rawPartPaymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(paymentTerms, rawPartPaymentTermsTextBox.Text);
            TextBox rawPartProjectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(projectInvest, rawPartProjectInvestTextBox.Text);
            TextBox rawPartInternalLogisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(internalLogisticCost, rawPartInternalLogisticCostTextBox.Text);
            TextBox rawPartOtherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(otherCost, rawPartOtherCostTextBox.Text);
        }

        [When(@"I select Raw Materials tab")]
        public void WhenISelectRawMaterialsTab()
        {
            TabPage rawMaterialsTab = mainWindow.Get<TabPage>(AutomationIds.MDURawMaterialsTab);
            rawMaterialsTab.Click();
        }

        [When(@"I complete the Raw Materials Tab fields")]
        public void WhenICompleteTheRawMaterialsTabFields()
        {
            
            Button deselectAllButton = mainWindow.Get<Button>(AutomationIds.DeselectAllButton);
            deselectAllButton.Click();
            Button selectAllButton = mainWindow.Get<Button>(AutomationIds.SelectAllButton);
            selectAllButton.Click();
            TextBox priceNVTextBox = mainWindow.Get<TextBox>(AutomationIds.PriceNVTextBox);
            priceNVTextBox.Text = "10";
            price = priceNVTextBox.Text;
            TextBox yieldStrengthTextBox = mainWindow.Get<TextBox>(AutomationIds.YieldStrengthTextBox);
            yieldStrengthTextBox.Text = "10";
            yieldStrength = yieldStrengthTextBox.Text;
            TextBox ruptureStrengthTextBox = mainWindow.Get<TextBox>(AutomationIds.RuptureStrengthTextBox);
            ruptureStrengthTextBox.Text = "10";
            ruptureStrength = ruptureStrengthTextBox.Text;
            TextBox densityTextBox = mainWindow.Get<TextBox>(AutomationIds.DensityTextBox);
            densityTextBox.Text = "10";
            density = densityTextBox.Text;
            TextBox maxElongationTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxElongationTextBox);
            maxElongationTextBox.Text = "10";
            maxElongation = maxElongationTextBox.Text;
            TextBox glassTTTextBox = mainWindow.Get<TextBox>(AutomationIds.GlassTTTextBox);
            glassTTTextBox.Text = "10";
            glassTransitionTemperature = glassTTTextBox.Text;
            TextBox rxTextBox = mainWindow.Get<TextBox>(AutomationIds.RxTextBox);
            rxTextBox.Text = "10";
            rx = rxTextBox.Text;
            TextBox rmTextBox = mainWindow.Get<TextBox>(AutomationIds.RmTextBox);
            rmTextBox.Text = "10";
            rm = rmTextBox.Text;
        }

        [When(@"I verify that Raw Materials tab fields are updated")]
        public void WhenIVerifyThatRawMaterialsTabFieldsAreUpdated()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawMaterialsNode = materialsNode.GetNode("RawMaterial1");
            rawMaterialsNode.SelectEx();
            TabPage generalTab = mainWindow.Get<TabPage>("GeneralTabItem");
            generalTab.Click();
            TextBox priceTextBox = mainWindow.Get<TextBox>("PriceTextBox");
            Assert.AreEqual(price, priceTextBox.Text);
            TabPage propertiesTab = mainWindow.Get<TabPage>("PropertiesTabItem");
            propertiesTab.Click();
            TextBox yieldStrengthTextBox = mainWindow.Get<TextBox>("YieldStrengthTextBox");
            Assert.AreEqual(yieldStrength, yieldStrengthTextBox.Text);
            TextBox ruptureStrengthTextBox = mainWindow.Get<TextBox>("RuptureStrengthTextBox");
            Assert.AreEqual(ruptureStrength, ruptureStrengthTextBox.Text);
            TextBox densityTextBox = mainWindow.Get<TextBox>("DensityTextBox");
            Assert.AreEqual(density, densityTextBox.Text);
            TextBox maxElongationTextBox = mainWindow.Get<TextBox>("MaxElongationTextBox");
            Assert.AreEqual(maxElongation, maxElongationTextBox.Text);
            TextBox glassTemperatureTextBox = mainWindow.Get<TextBox>("GlassTemperatureTextBox");
            Assert.AreEqual(glassTransitionTemperature, glassTemperatureTextBox.Text);
            TextBox rxTextBox = mainWindow.Get<TextBox>("RxTextBox");
            Assert.AreEqual(rx, rxTextBox.Text);
            TextBox rmTextBox = mainWindow.Get<TextBox>("RmTextBox");
            Assert.AreEqual(rm, rmTextBox.Text);       
        }

        [When(@"I complete the Raw Materials Tab Applied Percentage fields")]
        public void WhenICompleteTheRawMaterialsTabAppliedPercentageFields()
        {
            Button deselectAllButton = mainWindow.Get<Button>(AutomationIds.DeselectAllButton);
            deselectAllButton.Click();
            Button selectAllButton = mainWindow.Get<Button>(AutomationIds.SelectAllButton);
            selectAllButton.Click();
            TextBox priceAPTextBox = mainWindow.Get<TextBox>(AutomationIds.PriceAPTextBox);
            priceAPTextBox.Text = "10";
            int priceValue = ((Convert.ToInt32(priceAPTextBox.Text)) * (Convert.ToInt32(price))) / 100;
            price = priceValue.ToString();
        }

        [Then(@"I verify that Raw Materials tab fields are updated with AP values")]
        public void ThenIVerifyThatRawMaterialsTabFieldsAreUpdatedWithAPValues()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("ProjectMDU2");
            projectNode.SelectEx();
            projectNode.ExpandEx();
            TreeNode partNode = projectNode.GetNode("PartMDU2");
            partNode.SelectEx();
            partNode.ExpandEx();
            TreeNode materialsNode = partNode.GetNodeByAutomationId("MaterialsNode");
            materialsNode.SelectEx();
            materialsNode.ExpandEx();
            TreeNode rawMaterialsNode = materialsNode.GetNode("RawMaterial1");
            rawMaterialsNode.SelectEx();
            TabPage generalTab = mainWindow.Get<TabPage>("GeneralTabItem");
            generalTab.Click();
            TextBox priceTextBox = mainWindow.Get<TextBox>("PriceTextBox");
            Assert.AreEqual(price, priceTextBox.Text);
        }

        [When(@"I check Apply updates to sub-folders too")]
        public void WhenICheckApplyUpdatesToSub_FoldersToo()
        {
            CheckBox applyUpdetesToSubObjectsCheckBox = mainWindow.Get<CheckBox>(AutomationIds.ApplyUpdetesToSubObjectsCheckBox);
            applyUpdetesToSubObjectsCheckBox.Checked = true;
            mainWindow.WaitForAsyncUITasks();
        }

        [When(@"I check Apply updates to sub-folders too for RawMaterials")]
        public void WhenICheckApplyUpdatesToSub_FoldersTooForRawmaterials()
        {
            CheckBox applyUpdetesToSubObjectsCheckBox = mainWindow.Get<CheckBox>(AutomationIds.ApplyUpdetesToSubObjectsCheckBox);
            applyUpdetesToSubObjectsCheckBox.Checked = true;
            System.Threading.Thread.Sleep(2000);     
            
        }

        [When(@"I click on Update Button")]
        public void WhenIClickOnUpdateButton()
        {
            Button updateButton = mainWindow.Get<Button>("UpdateButton");
            updateButton.Click();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
        }
    }
}