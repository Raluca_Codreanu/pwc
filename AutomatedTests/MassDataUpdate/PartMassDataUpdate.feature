﻿@PartMassDataUpdate
Feature: PartMassDataUpdate

Scenario: Part General Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the General tab
	When I complete the General tab Fields
	When I click on the Update button
	Then I verify if General tab Fields are updated

Scenario: Part Shift Information Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Shift Information tab
	When I complete Shift Information Tab Fields
	When I click on the Update button
	When I verify that if Shift Information tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Shift Information tab
	When I complete the Shift Information Tab applied percentage Fields
	When I click on the Update button
	Then I verify that if Shift Information tab Fields are updated with AP values

Scenario: Part Process Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Process tab
	When I complete Process Tab Fields
	When I click on the Update button
	When I verify if Process tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Process tab
	When I complete the Process Tab applied percentage Fields
	When I click on the Update button
	Then I verify if Process tab Fields are updated with AP values

Scenario: Part Quantity Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Quantity tab
	When I complete the Quantity tab Fields
	When I click on the Update button
	When I verify if Quantity tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Quantity tab
	When I complete the Quantity Tab applied percentage Fields
	When I click on the Update button
	Then I verify if Quantity tab Fields are updated with AP values

Scenario: Part Overhead and Margin Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Overhead and Margin tab
	When I complete the Overhead and Margin tab Fields
	When I click on the Update button
	When I verify if Overhead and Margin tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Overhead and Margin tab
	When I complete the Overhead and Margin Tab applied percentage Fields
	When I click on the Update button
	Then I verify if Overhead and Margin tab Fields are updated with AP values

Scenario: Part Country Settings Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Country Settings tab
	When I complete the Country Settings tab Fields
	When I click on the Update button
	Then I verify if Country Settings tab Fields are updated

Scenario: Part Machines Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Machines tab
	When I complete the Machines tab Fields
	When I click on the Update button
	When I verify if Machines tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Machines tab
	When I complete the Machines Tab applied percentage Fields
	When I click on the Update button
	Then I verify if Machines tab Fields are updated with AP values

Scenario: Part Additional Costs Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Additional Costs Tab
	When I complete the Additional Costs tab Fields
	When I click on the Update button
	When I verify if Additional Costs tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I check Apply Updates to sub-folders too CheckBox
	When I select the Additional Costs Tab
	When I complete the Additional Costs Tab applied percentage Fields
	When I click on the Update button
	Then I verify if Additional Costs tab Fields are updated with AP values

Scenario: Part Raw Materials Tab Mass Data Update
	Given I select the Part
	And I go to Mass Data Update window
	When I select Raw Materials Tab
	When I check Apply updates to sub-folders too Check Box for RawMaterials
	When I complete the Raw Materials Tab Fields
	When I click on the Update button
	When I verify that Raw Materials tab Fields are updated
	When I select The Part
	When I go to Mass Data Update window
	When I select Raw Materials Tab
	When I check Apply updates to sub-folders too Check Box for RawMaterials
	When I complete the Raw Materials Tab Applied Percentage Fields
	When I click on the Update button
	Then I verify that Raw Materials tab Fields are updated with AP values