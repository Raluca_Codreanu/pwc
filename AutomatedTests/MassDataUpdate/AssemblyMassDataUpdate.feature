﻿@AssemblyMassDataUpdate
Feature: AssemblyMassDataUpdate

Scenario: Assembly General Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the General Tab
	When I complete the General tab fields
	When I click on the Update Button
	Then I verify if General tab fields are updated

Scenario: Assembly Shift Information Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Shift Information Tab
	When I complete Shift Information Tab fields
	When I click on the Update Button
	When I verify that if Shift Information tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Shift Information Tab
	When I complete the Shift Information Tab applied percentage fields
	When I click on the Update Button
	Then I verify that if Shift Information tab fields are updated with AP values

Scenario: Assembly Process Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Process Tab
	When I complete Process Tab fields
	When I click on the Update Button
	When I verify if Process tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Process Tab
	When I complete the Process Tab applied percentage fields
	When I click on the Update Button
	Then I verify if Process tab fields are updated with AP values

Scenario: Assembly Quantity Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Quantity Tab
	When I complete the Quantity tab fields
	When I click on the Update Button
	When I verify if Quantity tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Quantity Tab
	When I complete the Quantity Tab applied percentage fields
	When I click on the Update Button
	Then I verify if Quantity tab fields are updated with AP values

Scenario: Assembly Overhead and Margin Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select Overhead and Margin tab
	When I complete the Overhead and Margin tab fields
	When I click on the Update Button
	When I verify if Overhead and Margin tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select Overhead and Margin tab
	When I complete the Overhead and Margin Tab applied percentage fields
	When I click on the Update Button
	Then I verify if Overhead and Margin tab fields are updated with AP values

Scenario: Assembly Country Settings Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select Country Settings tab
	When I complete the Country Settings tab fields
	When I click on the Update Button
	Then I verify if Country Settings tab fields are updated

Scenario: Assembly Machines Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Machines Tab
	When I complete the Machines tab fields
	When I click on the Update Button
	When I verify if Machines tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Machines Tab
	When I complete the Machines Tab applied percentage fields
	When I click on the Update Button
	Then I verify if Machines tab fields are updated with AP values

Scenario: Assembly Additional Costs Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Additional Costs tab
	When I complete the Additional Costs tab fields
	When I click on the Update Button
	When I verify if Additional Costs tab fields are updated
	When I select The Assembly
	When I click on the Data-Mass Data Update
	When I check Apply updates to sub-folders too CheckBox
	When I select the Additional Costs tab
	When I complete the Additional Costs Tab applied percentage fields
	When I click on the Update Button
	Then I verify if Additional Costs tab fields are updated with AP values

Scenario: Assembly Raw Materials Tab Mass Data Update
	Given I select the Assembly
	And I click on the Data-Mass Data Update
	When I select the Raw Materials tab
	When I check Apply updates to sub-folders too CheckBox for RawMaterials
	When I complete the Raw Materials tab fields
	When I click on the Update Button
	Then I verify if Raw Materials tab fields are updated
