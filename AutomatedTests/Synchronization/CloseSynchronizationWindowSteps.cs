﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.Synchronization
{
    [Binding]
    public class CloseSynchronizationWindowSteps
    {
        private static ApplicationEx application;
        private static Window mainWindow;
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectTest";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SupplierRepository.Save(c);
            dataContext.OverheadSettingsRepository.Save(overheadSetting);
            dataContext.ProjectRepository.Save(project1);
            dataContext.SaveChanges();

        }

        [BeforeFeature("CloseSynchronizationWindow")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CloseSynchronizationWindow")]
        private static void KillApplication()
        {
            application.Kill();
        }
    
        [Given(@"I select From the Meniu Data-> Synchronize data")]
        public void GivenISelectFromTheMeniuData_SynchronizeData()
        {
            application.MainMenu.SyncDataMenuItem.Click();
            var sw = application.Windows.SynchronizationWindow;
            Window synchronizationWindow = Wait.For(() => application.Windows.SynchronizationWindow);
        }
        
        [Given(@"I Check all the Check Boxes")]
        public void GivenICheckAllTheCheckBoxes()
        {
            CheckBox myProjectsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.MyProjectsCheckBox);
            myProjectsCheckBox.Checked = true;
            CheckBox staticDataAndSettingsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.StaticDataAndSettingsCheckBox);
            staticDataAndSettingsCheckBox.Checked = true;
            CheckBox releasedProjectsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.ReleasedProjectsCheckBox);
            releasedProjectsCheckBox.Checked = true;
            CheckBox masterDataCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.MasterDataCheckBox);
            masterDataCheckBox.Checked = true;
        }
        
        [Given(@"I press Synchronize button")]
        public void GivenIPressSynchronizeButton()
        {
            Button synchronizeButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.SynchronizeButton);
            synchronizeButton.Click();
        }
        
        [When(@"I press close button")]
        public void WhenIPressCloseButton()
        {
            application.Windows.SynchronizationWindow.Close();
            
        }
        
        [Then(@"I press on Yes Button")]
        // Synchronization continue to run in background after synchronization window is closed
        public void ThenIPressOnYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            Window synchWindow = Wait.For(() => application.Windows.SynchronizationWindow);
            Assert.IsFalse(false, "Synchronization window is not closed", synchWindow);

            Button syncButton = application.Windows.MainWindow.Get<Button>(AutomationIds.SyncButton);
            Assert.IsTrue(true, "Synchronization is not running in background", syncButton);

            application.MainMenu.SyncDataMenuItem.Click();
            Window synchronizationWindow = Wait.For(() => application.Windows.SynchronizationWindow);
            Button runInBackgroundButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.RunInBackgroundButton);
            var runInBackgroundButtonValue = runInBackgroundButton.IsAvailable();
            Assert.AreEqual(runInBackgroundButtonValue, true);
        }
    }
}
