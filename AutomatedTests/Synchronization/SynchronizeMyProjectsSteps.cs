﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;


namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class SynchronizeMyProjectsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectUpdate1";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SupplierRepository.Save(c);
            dataContext.OverheadSettingsRepository.Save(overheadSetting);
            dataContext.ProjectRepository.Save(project1);
            dataContext.SaveChanges();

        }

        [BeforeFeature("SynchronizeMyProjects")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("SynchronizeMyProjects")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select From the meniu Data -> Synchronize data")]
        public void GivenISelectFromTheMeniuData_SynchronizeData()
        {
            application.MainMenu.SyncDataMenuItem.Click();
            var sw = application.Windows.SynchronizationWindow;
            Window synchronizationWindow = Wait.For(() => application.Windows.SynchronizationWindow);
        }
        
        [Given(@"I check MyProjects ckeck box")]
        public void GivenICheckMyProjectsCkeckBox()
        {
            CheckBox myProjectsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.MyProjectsCheckBox);
            myProjectsCheckBox.Checked = true;
        }
        
        [Given(@"I press Synchronize Button")]
        public void GivenIPressSynchronizeButton()
        {
            Button synchronizeButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.SynchronizeButton);
            synchronizeButton.Click();
        }
        
        [When(@"I press Run in Background button")]
        public void WhenIPressRunInBackgroundButton()
        {
            Button runInBackgroundButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.RunInBackgroundButton);
           
            runInBackgroundButton.Click();
        }
        
        [Then(@"the synchronization is running in background")]
        public void ThenTheSynchronizationIsRunningInBackground()
        {
            Assert.IsNull(application.Windows.SynchronizationWindow, "Synchronization should run in Background");
            Button syncButton = mainWindow.Get<Button>(AutomationIds.SyncButton);
            Assert.IsNotNull(syncButton, "Synchronization should run in Background");
        }
    }
}
