﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.SupplierTests
{
    public static class SupplierAutomationIds
    {
        public const string TypeComboBox = "TypeComboBox";
        public const string NumberTextBox = "NumberTextBox";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string BrowseCountryButton = "BrowseCountryMasterData";
        public const string BrowseCountryStateButton = "BrowseStateMasterData";
        public const string AddressTextBox = "AddressTextBox";
        public const string CityTextBox = "CityTextBox";
        public const string ZipCodeTextBox = "ZipCodeTextBox";
        public const string CountryTextBox = "CountryTextBox";
        public const string StateTextBox = "StateTextBox";
        public const string PhoneNoTextBox = "FirstContactPhoneNoTextBox";
        public const string FaxNoTextBox = "FirstContactFaxNoTextBox";
        public const string MobileNoTextBox = "FirstContactMobileNoTextBox";
        public const string EmailTextBox = "FirstContactEmailTextBox";
        public const string WebPageTextBox = "FirstContactWebPageTextBox";
        public const string SecondContactNameTextBox = "SecondContactNameTextBox";
        public const string SecondContactPhoneNoTextBox = "SecondContactPhoneNoTextBox";
        public const string SecondContactMobileNoTextBox = "SecondContactMobileNoTextBox";
        public const string SecondContactFaxNoTextBox = "SecondContactFaxNoTextBox";
        public const string SecondContactEmailTextBox = "SecondContactEmailTextBox";
        public const string SecondContactWebPageTextBox = "SecondContactWebPageTextBox";
    }
}
