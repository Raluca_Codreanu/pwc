﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.Utils;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.SupplierTests
{
    /// <summary>
    /// The automated test of Create Supplier Test Case.
    /// </summary>
    [TestClass]
    public class CreateSupplierTestCase
    {
        /// <summary>
        /// The country of the created supplier.
        /// </summary>
        private static Country country;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateSupplierTestCase"/> class.
        /// </summary>
        public CreateSupplierTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            CreateSupplierTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Cancel Create Supplier test case
        /// </summary>
        [TestMethod]
        public void CancelCreateSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode suppliersNode = app.MainScreen.ProjectsTree.MasterDataSuppliers;
                Assert.IsNotNull(suppliersNode, "The Suppliers node was not found.");

                suppliersNode.Click();
                ListViewControl suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                // step 5
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                Window createSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                // step 2 
                Button cancelButton = createSupplierWindow.Get<Button>(AutomationIds.CancelButton, true);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");
                cancelButton.Click();

                // step 8
                addButton.Click();
                createSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                // step 9
                TextBox nameTextBox = createSupplierWindow.Get<TextBox>();
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, nameTextBox, 100);

                // step 10
                nameTextBox.Text = "Supplier" + DateTime.Now.Ticks;
                var name = nameTextBox.Text;
                // step 11 -> Related defect #1347
                cancelButton = createSupplierWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();
               
                // step 12
                createSupplierWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                Assert.IsNotNull(app.Windows.SupplierWindow, "Create Supplier window was closed although the Cancel -> No buttons were clicked.");

                // step 13
                cancelButton.Click();

                // step 14
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                Assert.IsNull(app.Windows.SupplierWindow, "Failed to close the Create Supplier window at Cancel.");

                suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNull(suppliersDataGrid.Row("Name", name), "The supplier was created although the Cancel button was clicked.");
            }
        }

        /// <summary>
        /// An automated test for Create Supplier test case -> step 15 -> 29.
        /// </summary>
        [TestMethod]
        public void CreateSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode suppliersNode = app.MainScreen.ProjectsTree.MasterDataSuppliers;
                suppliersNode.Click();
                mainWindow.WaitForAsyncUITasks();
                // step 15
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                Window createSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                // step 16
                TextBox nameTextBox = createSupplierWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Supplier" + DateTime.Now.Ticks;

                // step 17
                ComboBox typeComboBox = createSupplierWindow.Get<ComboBox>();
                Assert.IsNotNull(typeComboBox, "Type combo box was not found.");

                typeComboBox.Click();
                typeComboBox.Items.FirstOrDefault().Select();

                // step 18
                TextBox numberTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "The Number text box was not found.");
                numberTextBox.Text = DateTime.Now.Ticks.ToString();

                // step 19
                TextBox descriptionTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "The Description text box was not found.");
                descriptionTextBox.Text = "Description" + DateTime.Now.Ticks;

                // step 20
                TextBox addressTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.AddressTextBox);
                Assert.IsNotNull(addressTextBox, "The Address text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, addressTextBox, 100);
                addressTextBox.Text = "Address" + DateTime.Now.Ticks;

                TextBox cityTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.CityTextBox);
                Assert.IsNotNull(cityTextBox, "The City text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, cityTextBox, 50);
                cityTextBox.Text = "City" + DateTime.Now.Ticks;

                TextBox zipCodeTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.ZipCodeTextBox);
                Assert.IsNotNull(zipCodeTextBox, "The Zip Code text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, zipCodeTextBox, 50);
                zipCodeTextBox.Text = DateTime.Now.Ticks.ToString();

                // step 21
                Button browseCountryButton = createSupplierWindow.Get<Button>(SupplierAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");

                browseCountryButton.Click();
                Window browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                // step 22               
                Button cancelButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserCancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click();                
                //Assert.IsNull(app.Windows.CountryAndSupplierBrowser, "Failed to close Browse Master Data window at Cancel.");
                TextBox countryTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
                Assert.IsTrue(string.IsNullOrEmpty(countryTextBox.Text), "The country was selected although the Cancel button was clicked.");

                // step 23
                browseCountryButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                var masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode(CreateSupplierTestCase.country.Name);

                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                countryTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.CountryTextBox);
                Assert.IsNull(app.Windows.CountryAndSupplierBrowser, "Failed to close the Master Data Browser after a country was selected.");

                // step 24
                Button browseSupplierButton = createSupplierWindow.Get<Button>(SupplierAutomationIds.BrowseCountryStateButton);
                Assert.IsNotNull(browseSupplierButton, "Browse Supplier button  was not found.");

                browseSupplierButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                foreach (CountryState state in CreateSupplierTestCase.country.States)
                {
                    TreeNode stateNode = masterDataTree.GetNode(state.Name);
                    Assert.IsNotNull(stateNode, "Not all country states were displayed.");
                }

                // step 25
                masterDataTree.Nodes.FirstOrDefault().SelectEx();
                cancelButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserCancelButton);

                cancelButton.Click();
                Assert.IsNull(app.Windows.CountryAndSupplierBrowser, "Failed to close Master Data Browser at Cancel.");

                TextBox stateTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.StateTextBox);
                Assert.IsNotNull(stateTextBox, "The State text box was not found.");
                Assert.IsTrue(string.IsNullOrEmpty(stateTextBox.Text), "A state was selected although the Cancel button was clicked.");

                // step 26
                browseSupplierButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                selectButton.Click();
                Assert.IsNull(app.Windows.CountryAndSupplierBrowser, "Failed to close the Master Data Browser after a state was selected.");

                stateTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.StateTextBox);
                //Assert.AreEqual(selectedStateNode.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, stateTextBox.Text, "The displayed state is other than the selected one.");

                // step 27
                TextBox phoneNoTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.PhoneNoTextBox);
                Assert.IsNotNull(phoneNoTextBox, "The Phone Number text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, phoneNoTextBox, 50);
                phoneNoTextBox.Text = "0264123456";

                TextBox faxNoTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.FaxNoTextBox);
                Assert.IsNotNull(faxNoTextBox, "The Fax Number text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, faxNoTextBox, 50);
                faxNoTextBox.Text = "0263990991";

                TextBox mobileNoTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.MobileNoTextBox);
                Assert.IsNotNull(mobileNoTextBox, "The Mobile Number text box was not found.");
                ValidationHelper.CheckTextFieldValidators(createSupplierWindow, mobileNoTextBox, 50);
                mobileNoTextBox.Text = "0767332112";

                TextBox emailTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.EmailTextBox);
                Assert.IsNotNull(emailTextBox, "The Email text box was not found.");
                emailTextBox.Text = "test@gmail.com";

                TextBox webPageTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.WebPageTextBox);
                Assert.IsNotNull(webPageTextBox, "The Web page text box was not found.");
                webPageTextBox.Text = "www.test.com";

                TextBox secondNameTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactNameTextBox);
                secondNameTextBox.Text = "name2";

                TextBox secondPhoneNoTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactPhoneNoTextBox);
                secondPhoneNoTextBox.Text = "123";

                TextBox secondMobileNoTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactMobileNoTextBox);
                secondMobileNoTextBox.Text = "456";

                TextBox secondFaxNoText = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactFaxNoTextBox);
                secondFaxNoText.Text = "4444";

                TextBox secondEmailTexBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactEmailTextBox);
                secondEmailTexBox.Text = "email2";

                TextBox secondWebPageTextBox = createSupplierWindow.Get<TextBox>(SupplierAutomationIds.SecondContactWebPageTextBox);
                secondWebPageTextBox.Text = "www";

                Button saveButton = createSupplierWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");

                saveButton.Click();
                ListViewControl suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");

                ListViewRow supplierRow = suppliersDataGrid.Row("Name", nameTextBox.Text);
                mainWindow.WaitForAsyncUITasks();
                Assert.IsNotNull(supplierRow, "The newly created supplier was not found.");
                Assert.AreEqual(numberTextBox.Text, suppliersDataGrid.Cell("Number", supplierRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong Number is displayed in the data grid.");
                Assert.AreEqual(countryTextBox.Text, suppliersDataGrid.Cell("Country", supplierRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong Country is displayed in the data grid.");
                Assert.AreEqual(descriptionTextBox.Text, suppliersDataGrid.Cell("Description", supplierRow).GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Wrong Description is displayed in the data grid.");

                // Verifies if the displayed supplier details are correct
                Assert.AreEqual(nameTextBox.Text, mainWindow.Get<TextBox>(AutomationIds.NameTextBox).Text, "The displayed Name is wrong.");
                Assert.AreEqual(numberTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.NumberTextBox).Text, "The displayed Number is wrong.");
                Assert.AreEqual(descriptionTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.DescriptionTextBox).Text, "The displayed Description is wrong.");
                Assert.AreEqual(addressTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.AddressTextBox).Text, "The displayed Address is wrong.");
                Assert.AreEqual(cityTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.CityTextBox).Text, "The displayed City is wrong.");
                Assert.AreEqual(zipCodeTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.ZipCodeTextBox).Text, "The displayed Zip Code is wrong.");
                Assert.AreEqual(countryTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.CountryTextBox).Text, "The displayed Country is wrong.");
                Assert.AreEqual(stateTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.StateTextBox).Text, "The displayed State is wrong.");
                Assert.AreEqual(phoneNoTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.PhoneNoTextBox).Text, "The displayed Phone No is wrong.");
                Assert.AreEqual(faxNoTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.FaxNoTextBox).Text, "The displayed Fax No is wrong.");
                Assert.AreEqual(mobileNoTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.MobileNoTextBox).Text, "The displayed Mobile No is wrong.");
                Assert.AreEqual(emailTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.EmailTextBox).Text, "The displayed Email is wrong.");
                Assert.AreEqual(webPageTextBox.Text, mainWindow.Get<TextBox>(SupplierAutomationIds.WebPageTextBox).Text, "The displayed Web page is wrong.");

                // TO DO -> Verify that the Type is correct (White issue - SelectedItemText = null unless the combo box was clicked before, but the Type cb is disabled => Click doesn't work)
            }
        }

        #region Helper

        ///// <summary>
        ///// Gets the Create Supplier window.
        ///// </summary>
        ///// <param name="app">The current application.</param>
        ///// <returns>The Create Supplier window.</returns>
        //public static Window GetCreateSupplierWindow(Application app)
        //{
        //    return app.GetWindows().FirstOrDefault(w => w.Title == SupplierAutomationIds.CreateSupplierWindow);
        //}

        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            CreateSupplierTestCase.country = new Country();
            CreateSupplierTestCase.country.Name = "Country" + DateTime.Now.Ticks;

            var units = dataContext.MeasurementUnitRepository.GetBaseMeasurementUnits();

            CreateSupplierTestCase.country.WeightMeasurementUnit = units.FirstOrDefault(m => m.Name == "Kilogram" && !m.IsReleased);
            CreateSupplierTestCase.country.FloorMeasurementUnit = units.FirstOrDefault(m => m.Name == "Square Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.LengthMeasurementUnit = units.FirstOrDefault(m => m.Name == "Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.VolumeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Cubic Meter" && !m.IsReleased);
            CreateSupplierTestCase.country.TimeMeasurementUnit = units.FirstOrDefault(m => m.Name == "Second" && !m.IsReleased);
            CreateSupplierTestCase.country.Currency = dataContext.CurrencyRepository.GetBaseCurrencies().FirstOrDefault(c => c.Name == "Euro" && !c.IsReleased);
            CreateSupplierTestCase.country.CountrySetting = new CountrySetting();

            CountryState state = new CountryState();
            state.Name = "State" + DateTime.Now.Ticks;
            state.CountrySettings = new CountrySetting();
            CreateSupplierTestCase.country.States.Add(state);

            dataContext.CountryRepository.Add(country);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
