﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.SupplierTests
{
    /// <summary>
    /// The test class of Delete Supplier Test Case.
    /// </summary>
    [TestClass]
    public class DeleteSupplierTestCase
    {
        /// <summary>
        /// The supplier which will be deleted.
        /// </summary>
        private static Customer supplier;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteSupplierTestCase"/> class.
        /// </summary>
        public DeleteSupplierTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteSupplierTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Supplier Test Case.
        /// </summary>
        [TestMethod]
        public void DeleteSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode suppliersNode = app.MainScreen.ProjectsTree.MasterDataSuppliers;

                // step 4
                suppliersNode.Click();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");

                // step 5
                suppliersDataGrid.Select("Name", DeleteSupplierTestCase.supplier.Name);

                // step 6
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");

                deleteButton.Click();
                
                // step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid.Row("Name", DeleteSupplierTestCase.supplier.Name), "The supplier was deleted although the Cancel button was clicked.");

                // step 8
                deleteButton.Click();
                
                // step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                mainWindow.WaitForAsyncUITasks();
                suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);

                // step 10
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");
                trashBinNode.SelectEx();

                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", DeleteSupplierTestCase.supplier.Name), "A supplier from Master Data was deleted to the Trash Bin.");
            }
        }

        #region Helper

        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            DeleteSupplierTestCase.supplier = EditSupplierTestCase.CreateSupplier();
            supplier.SetIsMasterData(true);

            dataContext.SupplierRepository.Add(DeleteSupplierTestCase.supplier);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
