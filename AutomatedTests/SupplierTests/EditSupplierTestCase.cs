﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.SupplierTests
{
    /// <summary>
    /// The automated test of Edit Supplier Test Case.
    /// </summary>
    [TestClass]
    public class EditSupplierTestCase
    {
        /// <summary>
        /// The Supplier which will be edited.
        /// </summary>
        private static Customer supplier;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditSupplierTestCase"/> class.
        /// </summary>
        public EditSupplierTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditSupplierTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Edit Supplier test case -> step 1 - 12.
        /// </summary>
        [TestMethod]
        public void CancelEditSupplierTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode suppliersNode = app.MainScreen.ProjectsTree.MasterDataSuppliers;
                Assert.IsNotNull(suppliersNode, "The Suppliers node was not found.");

                suppliersNode.Click();

                ListViewControl suppliersDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "Suppliers data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                // step 5
                suppliersDataGrid.Select("Name", EditSupplierTestCase.supplier.Name);
                EditSupplierTestCase.VerifySupplierInformation(mainWindow, EditSupplierTestCase.supplier);

                // step 6
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");

                editButton.Click();
                Window editSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                Button saveButton = editSupplierWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "Save button was not found.");
                Assert.IsTrue(saveButton.Enabled, "The Save button is disabled.");

                Button cancelButton = editSupplierWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");
                Assert.IsTrue(cancelButton.Enabled, "The Cancel button is disabled.");

                // step 7,9
                TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");

                nameTextBox.Text = string.Empty;
                Assert.IsFalse(editSupplierWindow.Get<Button>(AutomationIds.SaveButton).Enabled, "Save button is enabled although the Name text box is empty.");

                // step 8 - TO DO (move mouse cursor over the top right corner of Name text box ....)

                // step 10
                EditSupplierTestCase.FillSupplierViewAndAssert(app, editSupplierWindow, EditSupplierTestCase.supplier);
                cancelButton.Click();

                // step 11                
                editSupplierWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                
                // step 12
                cancelButton.Click();                
                editSupplierWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                EditSupplierTestCase.VerifySupplierInformation(mainWindow, EditSupplierTestCase.supplier);
            }
        }

        /// <summary>
        /// An automated test for Edit Supplier test case -> step 13 -> 16
        /// </summary>
        [TestMethod]
        public void EditSupplierFromMasterDateTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                // step 4
                TreeNode suppliersNode = app.MainScreen.ProjectsTree.MasterDataSuppliers;
                Assert.IsNotNull(suppliersNode, "Suppliers node was not found.");

                suppliersNode.Click();
                ListViewControl suppliersDataGrid = mainWindow.GetListView( AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(suppliersDataGrid, "The Suppliers data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                // step 13
                suppliersDataGrid.Select("Name", EditSupplierTestCase.supplier.Name);
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");

                editButton.Click();
                Window editSupplierWindow = Wait.For(() => app.Windows.SupplierWindow);

                Button saveButton = editSupplierWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "Save button was not found.");
                Assert.IsTrue(saveButton.Enabled, "Save button is disabled.");

                Button cancelButton = editSupplierWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");
                Assert.IsTrue(cancelButton.Enabled, "Cancel button is disabled.");
                EditSupplierTestCase.VerifySupplierInformation(editSupplierWindow, EditSupplierTestCase.supplier);

                // step 14
                Customer newSupplier = EditSupplierTestCase.CreateSupplier();
                EditSupplierTestCase.FillSupplierViewAndAssert(app, editSupplierWindow, newSupplier);

                saveButton.Click();
                EditSupplierTestCase.VerifySupplierInformation(mainWindow, newSupplier);
            }
        }

        #region Helper

        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            EditSupplierTestCase.supplier = EditSupplierTestCase.CreateSupplier();
            EditSupplierTestCase.supplier.SetIsMasterData(true);

            dataContext.SupplierRepository.Add(supplier);
            dataContext.SaveChanges();
        }

        /// <summary>
        /// Creates a supplier entity.
        /// </summary>
        /// <returns>The created supplier.</returns>
        public static Customer CreateSupplier()
        {
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Customer supplier = new Customer();
            supplier.Name = "Supplier" + supplier.Guid.ToString();
            supplier.Number = DateTime.Now.Ticks.ToString();
            supplier.Type = (short)SupplierType.CorporateGroup;
            supplier.Description = "Description" + DateTime.Now.Ticks;
            supplier.StreetAddress = "Address" + DateTime.Now.Ticks;
            supplier.City = "City" + DateTime.Now.Ticks;
            supplier.ZipCode = DateTime.Now.Ticks.ToString();
            supplier.FirstContactPhone = "0264445112" + DateTime.Now.Ticks;
            supplier.FirstContactFax = "0263441224" + DateTime.Now.Ticks;
            supplier.FirstContactMobile = "0745113554" + DateTime.Now.Ticks;
            supplier.FirstContactEmail = "test@gmail.com" + DateTime.Now.Ticks;
            supplier.FirstContactWebAddress = "www.test.com" + DateTime.Now.Ticks;

            Country country = dataContext.CountryRepository.GetAll().FirstOrDefault();
            supplier.Country = country != null ? country.Name : string.Empty;

            CountryState state = dataContext.CountrySupplierRepository.FindAll().FirstOrDefault(s => s.Country != null && s.Country.Guid == country.Guid);
            supplier.State = state != null ? state.Name : string.Empty;

            return supplier;
        }

        /// <summary>
        /// Fills the supplier view with the specified data.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="supplierView">The window that represents the supplier view.</param>
        /// <param name="supplier">The supplier which represents the model of the view.</param>
        public static void FillSupplierViewAndAssert(ApplicationEx app, Window supplierView, Customer supplier)
        {
            Window mainWindow = app.Windows.MainWindow;

            TextBox nameTextBox = supplierView.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
            nameTextBox.Text = supplier.Name;

            TextBox numberTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.NumberTextBox);
            Assert.IsNotNull(numberTextBox, "The Number text box was not found.");
            numberTextBox.Text = supplier.Number;

            ComboBox typeComboBox = supplierView.Get<ComboBox>(SupplierAutomationIds.TypeComboBox);
            Assert.IsNotNull(typeComboBox, "The Type combo box was not found.");
            typeComboBox.Click();

            TextBox descriptionTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "The Description text box was not found.");
            descriptionTextBox.Text = supplier.Description;

            TextBox addressTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.AddressTextBox);
            Assert.IsNotNull(addressTextBox, "The Address text box was not found.");
            addressTextBox.Text = supplier.StreetAddress;

            TextBox cityTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.CityTextBox);
            Assert.IsNotNull(cityTextBox, "The City text box was not found.");
            cityTextBox.Text = supplier.City;

            TextBox zipCodeTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.ZipCodeTextBox);
            Assert.IsNotNull(zipCodeTextBox, "The Zip text box was not found.");
            zipCodeTextBox.Text = supplier.ZipCode;

            Button browseCountryButton = supplierView.Get<Button>(SupplierAutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

            var masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode(supplier.Country);
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");

            selectButton.Click();
            TextBox countryTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
            Assert.AreEqual(supplier.Country, countryTextBox.Text, "The displayed country is different than the selected one.");

            Button browseSupplierButton = supplierView.Get<Button>(SupplierAutomationIds.BrowseCountryStateButton);
            Assert.IsNotNull(browseSupplierButton, "Browse supplier button was not found.");

            browseSupplierButton.Click();
            browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

            masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode(supplier.State, false);
            selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");

            selectButton.Click();
            TextBox stateTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.StateTextBox);
            Assert.IsNotNull(stateTextBox, "The Country text box was not found.");
            Assert.AreEqual(supplier.State, stateTextBox.Text, "The displayed state is different than the selected one.");

            TextBox phoneNoTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.PhoneNoTextBox);
            Assert.IsNotNull(phoneNoTextBox, "The Phone No text box was not found.");
            phoneNoTextBox.Text = supplier.FirstContactPhone;

            TextBox faxNoTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.FaxNoTextBox);
            Assert.IsNotNull(faxNoTextBox, "The Fax No text box was not found.");
            faxNoTextBox.Text = supplier.FirstContactFax;

            TextBox mobileNoTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.MobileNoTextBox);
            Assert.IsNotNull(mobileNoTextBox, "The Mobile No text box was not found.");
            mobileNoTextBox.Text = supplier.FirstContactMobile;

            TextBox emailAddressTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.EmailTextBox);
            Assert.IsNotNull(emailAddressTextBox, "The Email text box was not found.");
            emailAddressTextBox.Text = supplier.FirstContactEmail;

            TextBox webPageTextBox = supplierView.Get<TextBox>(SupplierAutomationIds.WebPageTextBox);
            Assert.IsNotNull(webPageTextBox, "The Web Page text box was not found.");
            webPageTextBox.Text = supplier.FirstContactWebAddress;
        }

        /// <summary>
        /// Checks if the information displayed in the Supplier view is correct.
        /// </summary>
        /// <param name="supplierView">The supplier view.</param>
        /// <param name="selectedSupplier">The supplier which represents the model of the view.</param>
        public static void VerifySupplierInformation(Window supplierView, Customer selectedSupplier)
        {
            Assert.AreEqual(selectedSupplier.Name, supplierView.Get<TextBox>(AutomationIds.NameTextBox).Text, "The displayed Name is wrong.");
            Assert.AreEqual(selectedSupplier.Number, supplierView.Get<TextBox>(SupplierAutomationIds.NumberTextBox).Text, "The displayed Number is wrong.");
            Assert.AreEqual(selectedSupplier.Description, supplierView.Get<TextBox>(SupplierAutomationIds.DescriptionTextBox).Text, "The displayed Description is wrong.");
            Assert.AreEqual(selectedSupplier.StreetAddress, supplierView.Get<TextBox>(SupplierAutomationIds.AddressTextBox).Text, "The displayed Address is wrong.");
            Assert.AreEqual(selectedSupplier.City, supplierView.Get<TextBox>(SupplierAutomationIds.CityTextBox).Text, "The displayed City is wrong.");
            Assert.AreEqual(selectedSupplier.ZipCode, supplierView.Get<TextBox>(SupplierAutomationIds.ZipCodeTextBox).Text, "The displayed Zip Code is wrong.");
            Assert.AreEqual(selectedSupplier.Country, supplierView.Get<TextBox>(SupplierAutomationIds.CountryTextBox).Text, "The displayed Country is wrong.");
            Assert.AreEqual(selectedSupplier.State, supplierView.Get<TextBox>(SupplierAutomationIds.StateTextBox).Text, "The displayed State is wrong.");
            Assert.AreEqual(selectedSupplier.FirstContactPhone, supplierView.Get<TextBox>(SupplierAutomationIds.PhoneNoTextBox).Text, "The displayed Phone No is wrong.");
            Assert.AreEqual(selectedSupplier.FirstContactFax, supplierView.Get<TextBox>(SupplierAutomationIds.FaxNoTextBox).Text, "The displayed Fax No is wrong.");
            Assert.AreEqual(selectedSupplier.FirstContactMobile, supplierView.Get<TextBox>(SupplierAutomationIds.MobileNoTextBox).Text, "The displayed Mobile No is wrong.");
            Assert.AreEqual(selectedSupplier.FirstContactEmail, supplierView.Get<TextBox>(SupplierAutomationIds.EmailTextBox).Text, "The displayed Email is wrong.");
            Assert.AreEqual(selectedSupplier.FirstContactWebAddress, supplierView.Get<TextBox>(SupplierAutomationIds.WebPageTextBox).Text, "The displayed Web page is wrong.");
        }

        #endregion Helper
    }
}
