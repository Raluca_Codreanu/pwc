﻿namespace White.Core.UIItems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using White.Core.UIItems.Finders;
    using White.Core.UIItems.WindowItems;
    using ZPKTool.AutomatedTests.CustomUIItems;

    /// <summary>
    /// Contains extensions for the <see cref="UIItemContainer"/> class.
    /// </summary>
    public static class UIItemContainerExtensions
    {
        /// <summary>
        /// Gets the specified UI item.
        /// </summary>
        /// <typeparam name="T">The type of the UI item.</typeparam>
        /// <param name="container">The container in which to search for the item.</param>
        /// <param name="criteria">The criteria to use for searching.</param>
        /// <param name="onlyOnScreen">
        /// If set to true, the item is returned only if is visible on the screen. In case multiple items with the same identification are present, the 1st visible item is returned.
        /// </param>        
        /// <returns>The item.</returns>
        /// <exception cref="White.Core.AutomationException">An item matching the specified criteria was not found.</exception>
        public static T Get<T>(
            this UIItemContainer container,
            SearchCriteria criteria,
            bool onlyOnScreen)
            where T : UIItem
        {
            if (onlyOnScreen)
            {
                var matchingElements = container.GetMultiple(criteria);
                var element = matchingElements.FirstOrDefault(item => !item.IsOffScreen && item.Visible);

                var castElement = element as T;

                if (castElement == null)
                {
                    string message = element != null
                        ? "The UI item with the specified identification was not of type '" + typeof(T).Name + "'."
                        : "The UI item with the specified identification was not found";
                    throw new AutomationException(message, string.Empty);
                }

                return castElement;
            }
            else
            {
                return container.Get<T>(criteria);
            }
        }

        /// <summary>
        /// Gets the specified UI item.
        /// </summary>
        /// <typeparam name="T">The type of the UI item.</typeparam>
        /// <param name="container">The container in which to search for the item.</param>
        /// <param name="primaryIdentification">The primary identification of the item (automation id).</param>
        /// <param name="onlyOnScreen">
        /// If set to true, the item is returned only if is visible on the screen. In case multiple items with the same identification are present, the 1st visible item is returned.
        /// </param>        
        /// <returns>The item.</returns>
        /// <exception cref="White.Core.AutomationException">The item with the specified identification was not found.</exception>
        public static T Get<T>(
            this UIItemContainer container,
            string primaryIdentification,
            bool onlyOnScreen)
            where T : UIItem
        {
            return Get<T>(container, SearchCriteria.ByAutomationId(primaryIdentification), onlyOnScreen);
        }
        
        /// <summary>
        /// Gets the specified list view wrapped in <see cref="ListViewControl" /> instance, which allows to manipulated virtualized list views or data grids.
        /// </summary>
        /// <param name="container">The container from which to get the list view.</param>
        /// <param name="primaryIdentification">The id of the list view.</param>
        /// <returns>
        /// The list view wrapper.
        /// </returns>
        public static ListViewControl GetListView(this UIItemContainer container, string primaryIdentification)
        {
            ListView listView = container.Get<ListView>(primaryIdentification);
            if (listView != null)
            {
                return new ListViewControl(listView.AutomationElement, listView.ActionListener);
            }
            else
            {
                return null;
            }
        }
    }
}
