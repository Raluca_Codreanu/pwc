﻿namespace White.Core.UIItems.ListBoxItems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using White.Core;

    /// <summary>
    /// Contains extension methods for the white's ComboBox class.
    /// </summary>
    public static class ComboBoxExtensions
    {
        /// <summary>
        /// Selects the item at the specified index.
        /// </summary>
        /// <param name="comboBox">The combo box.</param>
        /// <param name="index">The index.</param>
        public static void SelectItem(this ComboBox comboBox, int index)
        {
            if (comboBox == null)
            {
                throw new ArgumentNullException("comboBox", "The combo box was null.");
            }

            int keyPresses = index + 1;
            bool goup = false;
            if (comboBox.SelectedItem != null)
            {
                // Determine whether we go up or down in the combo box and by how many positions.
                var selectedIndex = comboBox.Items.IndexOf(comboBox.SelectedItem);
                keyPresses = index - selectedIndex;
                if (keyPresses < 0)
                {
                    goup = true;
                    keyPresses = Math.Abs(keyPresses);
                }
            }

            var key = goup ? White.Core.WindowsAPI.KeyboardInput.SpecialKeys.UP : White.Core.WindowsAPI.KeyboardInput.SpecialKeys.DOWN;
            comboBox.Focus();
            for (int i = 0; i < keyPresses; i++)
            {
                comboBox.KeyIn(key);
            }
        }

        /// <summary>
        /// Selects the item with the specified text.
        /// </summary>
        /// <param name="comboBox">The combo box.</param>
        /// <param name="itemText">The item text.</param>
        public static void SelectItem(this ComboBox comboBox, string itemText)
        {
            if (comboBox == null)
            {
                throw new ArgumentNullException("comboBox", "The combo box was null.");
            }

            if (itemText == null)
            {
                throw new ArgumentNullException("itemText", "The item text can not be null.");
            }

            var item = comboBox.Items.FirstOrDefault(it => it.Text == itemText);
            if (item == null)
            {
                throw new AutomationException(string.Format("Could not find a combo box item with the '{0}' text to select it.", itemText), string.Empty);
            }

            var index = comboBox.Items.IndexOf(item);
            SelectItem(comboBox, index);
        }

        /// <summary>
        /// Selects the item following the currently selected item. If no item is selected, it selects the 1st item.
        /// </summary>
        /// <param name="comboBox">The combo box.</param>
        public static void SelectNextItem(this ComboBox comboBox)
        {
            if (comboBox == null)
            {
                throw new ArgumentNullException("comboBox", "The combo box was null.");
            }

            int nextItemIndex = 0;
            if (comboBox.SelectedItem != null)
            {
                nextItemIndex = comboBox.Items.IndexOf(comboBox.SelectedItem) + 1;
            }

            SelectItem(comboBox, nextItemIndex);
        }
    }
}
