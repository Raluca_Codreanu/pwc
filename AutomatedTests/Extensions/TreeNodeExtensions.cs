﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Automation;
using White.Core.UIItems.Finders;

namespace White.Core.UIItems.TreeItems
{
    /// <summary>
    /// Extensions for white's TreeNode class.
    /// </summary>
    public static class TreeNodeExtensions
    {
        /// <summary>
        /// Expands the tree node and waits for it to load its children if it implements lazy loading.
        /// </summary>
        /// <param name="treeNode">The tree node.</param>
        /// <param name="timeout">The time to wait for the loading to finish, in milliseconds. The default is 5 minutes.</param>
        public static void ExpandEx(this TreeNode treeNode, int timeout = 300000)
        {
            if (treeNode == null)
            {
                throw new ArgumentNullException("treeNode", "The tree node was null.");
            }

            // If the item loading has started, wait for it to finish otherwise Expand() will fail because the "expander" thumb on the UI is missing while loading.
            WaitForNodeToLoad(treeNode, timeout, preWaitSleepTime: 0);

            treeNode.Expand();
            WaitForNodeToLoad(treeNode, timeout);
        }

        /// <summary>
        /// Selects the tree node and waits for it to load its children if it implements lazy loading.
        /// </summary>
        /// <param name="treeNode">The tree node.</param>
        /// <param name="timeout">The time to wait for the loading to finish, in milliseconds. The default is 5 minutes.</param>
        public static void SelectEx(this TreeNode treeNode, int timeout = 300000)
        {
            treeNode.Select();
            WaitForNodeToLoad(treeNode, timeout);
        }

        /// <summary>
        /// Finds the direct child node of a tree node by its text.
        /// </summary>
        /// <param name="treeNode">The node in which to search.</param>
        /// <param name="nodeText">The text of the searched child node.</param>
        /// <param name="throwIfNotFound">if set to true it throws an exception if a child node with the specified text was found.</param>
        /// <returns>
        /// The searched child node or null if not found.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The tree node was null.</exception>
        public static TreeNode GetNode(this TreeNode treeNode, string nodeText, bool throwIfNotFound = true)
        {
            if (treeNode == null)
            {
                throw new ArgumentNullException("treeNode", "The tree node was null.");
            }

            var childNode = treeNode.Nodes.FirstOrDefault(n =>
                {
                    var elemByAutomationId = n.GetElement(SearchCriteria.ByAutomationId("TreeItemText"));
                    if (elemByAutomationId != null && elemByAutomationId.Current.Name == nodeText)
                    {
                        return true;
                    }
                    else
                    {
                        var elemByText = n.GetElement(SearchCriteria.ByControlType(ControlType.Text));
                        if (elemByText != null && elemByText.Current.Name == nodeText)
                        {
                            return true;
                        }
                    }

                    return false;
                });

            if (childNode == null && throwIfNotFound)
            {
                string msg = string.Format("Could not find the child node having the '{0}' text.", nodeText);
                throw new AutomationException(msg, string.Empty);
            }

            return childNode;
        }

        public static TreeNode GetNodeByAutomationId(this TreeNode treeNode, string automationId, bool throwIfNotFound = true)
        {
            if (treeNode == null)
            {
                throw new ArgumentNullException("tree", "The tree was null.");
            }

            //// TODO: scrolling is necessary for virtualized trees.

            var childNode = treeNode.Nodes.FirstOrDefault(n => n.AutomationElement.Current.AutomationId == automationId);
            if (childNode == null && throwIfNotFound)
            {
                string msg = string.Format("Could not find the child node having the '{0}' automation id.", automationId);
                throw new AutomationException(msg, string.Empty);
            }

            return childNode;
        }

        /// <summary>
        /// Waits for the specified tree node to load its children.
        /// You must select or expand the node before calling this method, in order to trigger the children loading.
        /// </summary>
        /// <param name="treeNode">The tree node.</param>
        /// <param name="timeout">The maximum time to wait, in milliseconds. The default is 5 minutes.</param>
        /// <param name="waitInterval">How much time to wait between loading status checks.</param>
        /// <param name="preWaitSleepTime">How much time to wait before checking the loading status for the first time. The default is 50 milliseconds.</param>
        /// <exception cref="White.Core.AutomationException">The loading has not finished before the timeout period has elapsed.</exception>
        private static void WaitForNodeToLoad(TreeNode treeNode, int timeout = 300000, int waitInterval = 500, int preWaitSleepTime = 100)
        {
            // Sleep a bit in case the asynchronous loading of the tree node did not start (just to be safe).
            Thread.Sleep(preWaitSleepTime);

            // Wait while the item is loading.
            var startTime = DateTime.Now;
            while (DateTime.Now.Subtract(startTime).TotalMilliseconds < timeout
                && treeNode.AutomationElement.Current.ItemStatus == "Loading")
            {
                Thread.Sleep(waitInterval);
            }

            // Throw an exception if the item is still loading after the maximum wait time has elapsed.
            if (treeNode.AutomationElement.Current.ItemStatus == "Loading")
            {
                decimal maxTime = timeout / 1000m / 60m;
                string timeUnit = "min.";
                if (maxTime < 1)
                {
                    maxTime = timeout / 1000m;
                    timeUnit = "sec.";
                    if (maxTime < 1)
                    {
                        maxTime = timeout;
                        timeUnit = "sec.";
                    }
                }

                string formattedTime = string.Format("{0:#,0.##} {1}", maxTime, timeUnit);
                throw new AutomationException(string.Format("The loading of a tree node took more than the maximum wait time of {0}", formattedTime), string.Empty);
            }
        }
    }
}
