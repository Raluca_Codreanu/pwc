﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using White.Core.UIItems.Finders;

namespace White.Core.UIItems.TreeItems
{
    /// <summary>
    /// Extensions for the white's Tree class.
    /// </summary>
    public static class TreeExtensions
    {
        /// <summary>
        /// Finds the direct child node of a tree by its text.
        /// </summary>
        /// <param name="tree">The tree in which to search.</param>
        /// <param name="nodeText">The text of the searched child node.</param>
        /// <param name="throwIfNotFound">if set to true it throws an exception if a child node with the specified text was found.</param>
        /// <returns>
        /// The searched child node or null if not found.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The tree was null.</exception>
        public static TreeNode GetNode(this Tree tree, string nodeText, bool throwIfNotFound = true)
        {
            if (tree == null)
            {
                throw new ArgumentNullException("tree", "The tree was null.");
            }

            //// TODO: scrolling is necessary for virtualized trees.

            var childNode = tree.Nodes.FirstOrDefault(n =>
                {
                    var elemByAutomationId = n.GetElement(SearchCriteria.ByAutomationId("TreeItemText"));
                    if (elemByAutomationId != null && elemByAutomationId.Current.Name == nodeText)
                    {
                        return true;
                    }
                    else
                    {
                        var elemByText = n.GetElement(SearchCriteria.ByControlType(ControlType.Text));
                        if (elemByText != null && elemByText.Current.Name == nodeText)
                        {
                            return true;
                        }
                    }

                    return false;
                });

            if (childNode == null && throwIfNotFound)
            {
                string msg = string.Format("Could not find the child node having the '{0}' text.", nodeText);
                throw new AutomationException(msg, string.Empty);
            }

            return childNode;
        }

        /// <summary>
        /// Gets a tree node by specifying the path to it.
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="pathToNode">The path to node. Each string specifies the text of a parent; the 1st string is the text of the root parent and the last is the text of the searched node.</param>
        /// <returns>The searched node.</returns>
        /// <exception cref="System.ArgumentNullException">The tree was null.</exception>
        /// <exception cref="System.ArgumentException">The path to the searched node was empty.</exception>
        /// <exception cref="White.Core.AutomationException">The node was not found.</exception>
        public static TreeNode GetNode(this Tree tree, params string[] pathToNode)
        {
            if (tree == null)
            {
                throw new ArgumentNullException("tree", "The tree was null.");
            }

            if (pathToNode == null || pathToNode.Length == 0)
            {
                throw new ArgumentException("The path to the searched node was empty.", "pathToNode");
            }

            var crtNode = tree.GetNode(pathToNode[0], false);
            for (int i = 1; i < pathToNode.Length; i++)
            {
                if (crtNode == null)
                {
                    break;
                }

                crtNode.ExpandEx();
                crtNode = crtNode.GetNode(pathToNode[i], false);
            }

            if (crtNode == null)
            {
                string msg = string.Format("Could not find the child node having the '{0}' text.", pathToNode[pathToNode.Length - 1]);
                throw new AutomationException(msg, string.Empty);
            }

            return crtNode;
        }

        public static TreeNode GetNodeByAutomationId(this Tree tree, string automationId, bool throwIfNotFound = true)
        {
            if (tree == null)
            {
                throw new ArgumentNullException("tree", "The tree was null.");
            }

            //// TODO: scrolling is necessary for virtualized trees.
            
            var childNode = tree.Nodes.FirstOrDefault(n => n.AutomationElement.Current.AutomationId == automationId);            
            if (childNode == null && throwIfNotFound)
            {
                string msg = string.Format("Could not find the child node having the '{0}' automation id.", automationId);
                throw new AutomationException(msg, string.Empty);
            }

            return childNode;
        }

        /// <summary>
        /// Finds the direct child node of a tree by its text and selects it.
        /// <para/>
        /// This method works around the issue that, for some reason, calling the Select method of a node that is the direct child of a tree
        /// causes the tree to scroll back and forth between the currently selected item and the item that wants to be selected.
        /// </summary>
        /// <param name="tree">The tree in which to search.</param>
        /// <param name="nodeText">The text of the searched child node.</param>
        /// <param name="throwIfNotFound">if set to true it throws an exception if a child node with the specified text was found.</param>
        /// <returns>
        /// The selected child node or null if not found.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The tree was null.</exception>
        public static TreeNode SelectNode(this Tree tree, string nodeText, bool throwIfNotFound = true)
        {
            if (tree == null)
            {
                throw new ArgumentNullException("treeNode", "The tree node was null.");
            }

            var child = tree.GetNode(nodeText, throwIfNotFound);

            if (child != null)
            {
                while (child.IsOffScreen)
                {
                    var lastVisibleChild = tree.Nodes.Last(n => !n.IsOffScreen);
                    lastVisibleChild.SelectEx();
                    tree.ScrollBars.Vertical.ScrollDownLarge();
                }

                child.SelectEx();
            }

            return child;
        }
    }
}
