﻿namespace White.Core.UIItems
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Automation;
    using White.Core.UIItems.Finders;
    using White.Core.UIItems.MenuItems;
    using White.Core.UIItems.TreeItems;
    using White.Core.UIItems.WindowItems;

    /// <summary>
    /// Extension methods for white's UIItem class.
    /// </summary>
    public static class UIItemExtensions
    {
        /// <summary>
        /// Determines whether the specified UIItem is available on screen or it has been removed.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// true if the item is available; otherwise, false.
        /// </returns>
        public static bool IsAvailable(this UIItem item)
        {
            try
            {
                // If the item is no longer available, accessing the Enabled property (or many other properties) will throw an exception.
                var enabled = item.Enabled;
            }
            catch (ElementNotAvailableException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the context menu item or sub-item matching the specified criteria.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="window">The window displaying the item.</param>
        /// <param name="menuItemCriteria">The criteria identifying the menu item.</param>
        /// <param name="subMenuItemsCriteria">A list of criteria identifying the sub-menu items in the order they appear on the UI.</param>
        /// <returns>The menu item or, if sub-menu items criteria is specified, the item corresponding to the last sub-menu item.</returns>
        /// <exception cref="White.Core.AutomationException">The menu item was not found.</exception>
        public static Menu GetContextMenu(this UIItem item, Window window, SearchCriteria menuItemCriteria, params SearchCriteria[] subMenuItemsCriteria)
        {
            TreeNode treeNode = item as TreeNode;
            if (treeNode != null)
            {
                // If the tree node has children, the right click will work correctly only if the node is collapsed
                if (treeNode.IsExpanded())
                {
                    treeNode.Collapse();
                }

                // Select the tree node in case is lazy loaded -> the context menu appears only after the item's loading has finished.
                treeNode.SelectEx();
            }

            item.Click();
            item.RightClickAt(item.ClickablePoint);

            Menu contextMenuItem = window.Popup.ItemBy(menuItemCriteria);
            if (contextMenuItem == null)
            {
                throw new AutomationException(string.Format("Menu item identified by the '{0}' criteria was not found.", menuItemCriteria.ToString()), null);
            }

            if (subMenuItemsCriteria.Length > 0)
            {
                // Navigate the sub-menus until we find the last sub-menu specified by the subMenuItemsText list. 
                for (int i = 0; i < subMenuItemsCriteria.Length; i++)
                {
                    contextMenuItem.RightClick();
                    contextMenuItem = contextMenuItem.SubMenu(subMenuItemsCriteria[i]);
                    if (contextMenuItem == null)
                    {
                        throw new AutomationException(string.Format("Sub-menu item identified by the '{0}' criteria was not found.", subMenuItemsCriteria[i].ToString()), null);
                    }
                }
            }

            return contextMenuItem;
        }

        /// <summary>
        /// Gets the context menu item or sub-item having the specified automationId.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="window">The window displaying the item.</param>
        /// <param name="menuItemText">The menu item text.</param>
        /// <param name="subMenuItemsText">A list of strings representing the text of the sub-menu items in the order they appear on the UI.</param>
        /// <returns>The menu item or, if sub-menu items id is specified, the item corresponding to the last sub-menu item.</returns>
        /// <exception cref="White.Core.AutomationException">The menu item was not found.</exception>
        public static Menu GetContextMenuById(this UIItem item, Window window, string menuItemText, params string[] subMenuItemsText)
        {
            return item.GetContextMenu(window, SearchCriteria.ByAutomationId(menuItemText), subMenuItemsText.Select(text => SearchCriteria.ByAutomationId(text)).ToArray());
        }

        public static Menu GetContextMenuByText(this UIItem item, Window window, string menuItemText, params string[] subMenuItemsText)
        {
            return item.GetContextMenu(window, SearchCriteria.ByText(menuItemText), subMenuItemsText.Select(text => SearchCriteria.ByText(text)).ToArray());
        }
    }
}
