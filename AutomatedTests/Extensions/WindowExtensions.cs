﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests;
using ZPKTool.AutomatedTests.CustomUIItems;

namespace White.Core.UIItems.WindowItems
{
    /// <summary>
    /// Contains extensions for white's Window class.
    /// </summary>
    public static class WindowExtensions
    {
        /// <summary>
        /// Gets all child windows of a window while avoiding the ElementNotAvailableException sometimes thrown by ModalWindows().
        /// </summary>
        /// <param name="window">The window.</param>
        /// <returns>A list containing the child windows</returns>
        public static List<Window> ModalWindowsSafe(this Window window)
        {
            return ModalWindowsSafeInternal(window, 0);
        }

        /// <summary>
        /// Gets all child windows of a window while avoiding the ElementNotAvailableException sometimes thrown by ModalWindows().
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="count">A counter used to retry this call for a finite number of time if Window.ModalWindows() keeps failing.</param>
        /// <returns>
        /// A list containing the child windows
        /// </returns>
        /// <exception cref="System.ArgumentNullException">window;The window was null.</exception>
        private static List<Window> ModalWindowsSafeInternal(Window window, int count)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The window was null.");
            }

            List<Window> windows;
            try
            {
                windows = window.ModalWindows();
            }
            catch (ElementNotAvailableException)
            {
                System.Threading.Thread.Sleep(500);

                if (count < 5)
                {
                    windows = ModalWindowsSafeInternal(window, count + 1);
                }
                else
                {
                    throw;
                }
            }

            return windows;
        }

        /// <summary>
        /// Gets the message dialog currently displayed by a window.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="waitTimeout">The period to wait for the message dialog to become available.</param>
        /// <returns>
        /// The message dialog or null if no message dialog was displayed before the waiting time expired.
        /// </returns>
        public static MessageDialog GetMessageDialog(this Window window, int waitTimeout = 5000)
        {
            var dialog = MessageDialog.GetCurrent(window, waitTimeout);

            if (dialog == null)
            {
                Assert.Fail("No message dialog was displayed by the application.");
            }

            return dialog;
        }

        /// <summary>
        /// Gets the message dialog currently displayed by a window and having the specified type.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="dialogType">Type of the message dialog to get.</param>
        /// <param name="waitTimeout">The period to wait for the message dialog to become available..</param>
        /// <returns>
        /// The message dialog or null if no message dialog of the specified type was displayed before the waiting time expired.
        /// </returns>
        public static MessageDialog GetMessageDialog(this Window window, MessageDialogType dialogType, int waitTimeout = 5000)
        {
            var messageDlg = MessageDialog.GetCurrent(window, waitTimeout);
            if (messageDlg != null && messageDlg.Type == dialogType)
            {
                return messageDlg;
            }
            else
            {
                Assert.Fail("No message dialog of the '{0}' type was displayed by the application.", Enum.GetName(typeof(MessageDialogType), dialogType));
                return null;
            }
        }

        /// <summary>
        /// Determines whether a message dialog is currently displayed by the window.
        /// This method does not use any timeout period in which it waits for a dialog to become available or un-available.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="dialogType">Type of the message dialog. If is null it checks if any message dialog is displayed.</param>
        /// <returns>
        /// true if a message dialog of the specified type is displayed; otherwise, false.
        /// </returns>
        public static bool IsMessageDialogDisplayed(this Window window, MessageDialogType? dialogType = null)
        {
            MessageDialog dialog = null;
            if (dialogType.HasValue)
            {
                dialog = window.GetMessageDialog(dialogType.Value, 0);
            }
            else
            {
                dialog = window.GetMessageDialog(0);
            }

            return dialog != null;
        }

        /// <summary>
        /// Waits until the asynchronous task(s) currently being executed in a window has(have) finished.
        /// </summary>
        /// <param name="window">The window.</param>
        public static void WaitForAsyncUITasks(this Window window)
        {
            // Wait until the current Please Wait service task has finished (if there is one started).
            var pleaseWaitControl = window.GetElement(SearchCriteria.ByAutomationId("PleaseWaitUserControl"));
            while (pleaseWaitControl != null)
            {
                System.Threading.Thread.Sleep(500);
                pleaseWaitControl = window.GetElement(SearchCriteria.ByAutomationId("PleaseWaitUserControl"));
            }
        }

        /// <summary>
        /// Gets the open file dialog currently displayed by the window.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="customTitle">The custom title of the open file dialog, in case it has one.</param>
        /// <returns>
        /// A FileDialog instance that allows to manipulate the dialog.
        /// </returns>
        public static FileDialog GetOpenFileDialog(this Window window, string customTitle = null)
        {
            var title = customTitle ?? FileDialog.OpenFileDialogTitle;
            return new FileDialog(window, title);
        }

        /// <summary>
        /// Gets the save file dialog currently displayed by the window.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="customTitle">The custom title of the open file dialog, in case it has one.</param>
        /// <returns>A FileDialog instance that allows to manipulate the dialog.</returns>
        public static FileDialog GetSaveFileDialog(this Window window, string customTitle = null)
        {
            var title = customTitle ?? FileDialog.SaveFileDialogTitle;
            return new FileDialog(window, title);
        }

        /// <summary>
        /// Gets the media control from a container.
        /// </summary>
        /// <param name="window">The window hosting the media control.</param>        
        /// <param name="primaryId">The primary id of the media control. If is not set the default value (MediaControl.DefaultAutomationId) is used.</param>
        /// <returns>
        /// The media control.
        /// </returns>
        public static MediaControl GetMediaControl(this Window window, string primaryId = null)
        {
            return new MediaControl(primaryId ?? MediaControl.DefaultAutomationId, window);
        }        
    }
}
