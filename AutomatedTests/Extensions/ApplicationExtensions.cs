﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests;

namespace White.Core
{
    /// <summary>
    /// Extensions for the <see cref="White.Core.Application"/> class.
    /// </summary>
    public static class ApplicationExtensions
    {        
        #region Windows related

        /// <summary>
        /// Gets a window by its title.
        /// </summary>
        /// <param name="app">The application containing the window.</param>
        /// <param name="title">The title of the searched window.</param>
        /// <param name="throwIfNotFound">if set to true it throws and exception if the window is not found; otherwise it returns null.</param>
        /// <returns>The window or null if the window is not found and <paramref name="throwIfNotFound"/> is false.</returns>
        /// <exception cref="System.ArgumentNullException">The application was null.</exception>
        public static Window GetWindow(this Application app, string title, bool throwIfNotFound = true)
        {
            if (app == null)
            {
                throw new ArgumentNullException("app", "The application was null.");
            }
            
            if (throwIfNotFound)
            {
                return app.GetWindow(title);
            }
            else
            {
                return app.GetWindowsSafe().FirstOrDefault(wnd => wnd.Title == title);
            }
        }

        /// <summary>
        /// Gets all widows opened by the application, avoiding a bug that causes sometimes an exception to be thrown.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>
        /// A list of windows.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The application was null.</exception>
        /// <remarks>
        /// Due to a bug in White, the call to Application.GetWindows() sometimes throws ElementNotAvailableException.
        /// This method retries the call, which usually makes it succeed.
        /// </remarks>
        public static List<Window> GetWindowsSafe(this Application app)
        {
            return GetWindowsSafeInternal(app, 0);
        }

        /// <summary>
        /// Gets all widows opened by the application, avoiding a bug that causes sometimes an exception to be thrown.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="count">Internal count for the number of recursive calls. Should be left with the default value.</param>
        /// <returns>
        /// A list of windows.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">app;The application was null.</exception>
        /// <remarks>
        /// Due to a bug in White, the call to Application.GetWindows() sometimes throws ElementNotAvailableException.
        /// This method retries the call, which usually makes it succeed.
        /// </remarks>
        private static List<Window> GetWindowsSafeInternal(Application app, int count)
        {
            if (app == null)
            {
                throw new ArgumentNullException("app", "The application was null.");
            }

            List<Window> windows;
            try
            {
                windows = app.GetWindows();
            }
            catch (ElementNotAvailableException)
            {
                System.Threading.Thread.Sleep(500);

                if (count < 5)
                {
                    windows = GetWindowsSafeInternal(app, count + 1);
                }
                else
                {
                    throw;
                }
            }

            return windows;
        }

        #endregion Windows related

        #region Message Dialog

        /// <summary>
        /// Gets the message dialog currently displayed by the application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="waitTimeout">The period to wait for the message dialog to become available.</param>
        /// <returns>
        /// The message dialog or null if no message dialog was displayed before the waiting time expired.
        /// </returns>
        public static MessageDialog GetMessageDialog(this Application app, int waitTimeout = 5000)
        {
            var dialog = MessageDialog.GetCurrent(app, waitTimeout);

            if (dialog == null)
            {
                Assert.Fail("No message dialog was displayed by the application.");
            }

            return dialog;
        }

        /// <summary>
        /// Gets the message dialog currently displayed by the application and having the specified type.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="dialogType">Type of the message dialog to get.</param>
        /// <param name="waitTimeout">The period to wait for the message dialog to become available..</param>
        /// <returns>
        /// The message dialog or null if no message dialog of the specified type was displayed before the waiting time expired.
        /// </returns>
        public static MessageDialog GetMessageDialog(this Application app, MessageDialogType dialogType, int waitTimeout = 5000)
        {
            var messageDlg = MessageDialog.GetCurrent(app, waitTimeout);
            if (messageDlg != null && messageDlg.Type == dialogType)
            {
                return messageDlg;
            }
            else
            {
                Assert.Fail("No message dialog of the '{0}' type was displayed by the application.", Enum.GetName(typeof(MessageDialogType), dialogType));
                return null;
            }
        }

        /// <summary>
        /// Determines whether a message dialog is currently displayed by the application.
        /// This method does not use any timeout period in which it waits for a dialog to become available or un-available.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="dialogType">Type of the message dialog. If is null it checks if any message dialog is displayed.</param>
        /// <returns>
        /// true if a message dialog of the specified type is displayed; otherwise, false.
        /// </returns>
        public static bool IsMessageDialogDisplayed(this Application app, MessageDialogType? dialogType = null)
        {
            MessageDialog dialog = null;
            if (dialogType.HasValue)
            {
                dialog = app.GetMessageDialog(dialogType.Value, 0);
            }
            else
            {
                dialog = app.GetMessageDialog(0);
            }

            return dialog != null;
        }

        #endregion Message Dialog
    }
}
