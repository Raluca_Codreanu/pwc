﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.DataAccess;
namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [TestClass]
    public class DeleteAssemblyMDTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteAssemblyMDTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Assembly Master Data Test Case. (id=360)
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void DeleteAssemblyMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode assembliesNode = app.MainScreen.ProjectsTree.MasterDataAssemblies;
                assembliesNode.SelectEx();

                ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                mainWindow.WaitForAsyncUITasks();
                Assert.IsNotNull(assembliesDataGrid, "Manage Master Data grid was not found.");
                assembliesDataGrid.Select("Name", "assemblyToDelete");

                //step 2
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                // step 3
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 4
                deleteButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 5
                deleteButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish

                //step 6
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                trashBinNode.Select();
                trashBinNode.Click();

                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "assemblyToDelete"), "A supplier from Master Data was deleted to the Trash Bin.");

                //step 7                                
                assembliesNode.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(assembliesDataGrid, "Manage Master Data grid was not found.");                
                ListViewRow assemblyToDelete2row = assembliesDataGrid.Row("Name", "assemblyToDelete2");

                //step 7
                assemblyToDelete2row.DoubleClick();
                TreeNode assembliesNode2 = Wait.For(() => assembliesNode.GetNode("assemblyToDelete2", false));
                assembliesNode2.SelectEx();

                //step 8                
                Menu deleteMenuItem = assembliesNode2.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                                
                //step 9
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 12                
                deleteMenuItem = assembliesNode2.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Assembly assembly1 = new Assembly();
            assembly1.Name = "assemblyToDelete";
            assembly1.SetOwner(adminUser);
            assembly1.IsMasterData = true;
            CountrySetting country = new CountrySetting();
            assembly1.CountrySettings = country;

            assembly1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            assembly1.Manufacturer = manufacturer1;

            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.SaveChanges();

            Assembly assembly2 = new Assembly();
            assembly2.Name = "assemblyToDelete2";
            assembly2.SetOwner(adminUser);
            assembly2.IsMasterData = true;
            CountrySetting country2 = new CountrySetting();
            assembly2.CountrySettings = country2;

            assembly2.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer2 = new Manufacturer();
            manufacturer2.Name = "Manufacturer" + manufacturer2.Guid.ToString();
            assembly2.Manufacturer = manufacturer2;

            dataContext.AssemblyRepository.Add(assembly2);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
