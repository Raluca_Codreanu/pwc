﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    public static class AssemblyAutomationIds
    {
        public const string LastModifiedLabel = "LastModifiedLabel";            
        public const string NumberTextBox = "NumberTextBox";
        public const string VersionTextBox = "VersionTextBox";
        public const string DescriptionTextBox = "DescriptionTextBox";            
        public const string CalculationAccuracyComboBox = "CalculationAccuracyComboBox";
        public const string BatchSizeTextBox = "BatchSizeTextBox";
        public const string YearlyProdQtyTextBox = "YearlyProdQtyTextBox";
        public const string LifeTimeTextBox = "LifeTimeTextBox";
        public const string CalculationApproachComboBox = "CalculationApproachComboBox";
        public const string PurchasePriceTextBox = "PurchasePriceTextBox";
        public const string TargetPriceTextBox = "TargetPriceTextBox";
        public const string DeliveryTypeComboBox = "DeliveryTypeComboBox";
        public const string AssetRateTextBox = "AssetRateTextBox";       
        public const string EstimatedCostTextBox = "EstimatedCostTextBox";
        public const string WeightTextBox = "WeightTextBox";
    }
}
