﻿@CopyToMasterDataAssemblyWithPictures
Feature: CopyToMasterDataAssemblyWithPictures
	I want to copy to master data an assembly with pictures

Scenario: Copy To Master Data an Assembly with pictures and subentities
	Given I select the assemblyPictures to choose 'Copy To Master Data'
	And I press the Yes button in confirmation window
	And I go to Master Data Entities Assemblies
	And I select the assemblyPictures from dataGrid
	And I press the Edit button
	And I expand the assembly from tree and select the part node	
	When I delete one of the pictures from assemblie's Media control panel	
	And I press the Yes button from confirmation screen 
	Then all the pictures are deleted and the Delete button is disabled
