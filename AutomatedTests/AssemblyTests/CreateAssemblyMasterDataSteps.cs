﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using White.Core.UIItems.Finders;


namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CreateAssemblyMasterDataSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CreateAssemblyMD")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
        }

        [AfterFeature("CreateAssemblyMD")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press Add button")]
        public void GivenIPressAddButton()
        {
            Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
            addButton.Click();
        }

        [Given(@"I fill the fields from General tab")]
        public void GivenIFillTheFieldsFromGeneralTab()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTexBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTexBox.Text = "createdAssembly";

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "11";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "test";
        }

        [Given(@"I choose a country")]
        public void GivenIChooseACountry()
        {
            Button browseCountryButton = mainWindow.Get<Button>(AutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = Wait.For(() => application.Windows.CountryAndSupplierBrowser);

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Belgium");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");

            selectButton.Click();
            TextBox countryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
        }

        [Given(@"I add a media field")]
        public void GivenIAddAMediaField()
        {
            Window dieWin = application.Windows.DieWindow;
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);
        }

        [Given(@"I select Manufacturer tab and fill the name field")]
        public void GivenISelectManufacturerTabAndFillTheNameField()
        {
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "Name text box was not found.");
            nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

            TextBox manufacturerDescriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
            manufacturerDescriptionTextBox.Text = "Description of Manufacturer";
        }

        [When(@"I press Save")]
        public void WhenIPressSave()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Then(@"the assembly is saved and it is displayed on data grid")]
        public void ThenTheAssemblyIsSavedAndItIsDisplayedOnDataGrid()
        {
            ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(assembliesDataGrid.Row("Name", "createdAssembly"), "The assembly was created.");
        }
    }
}
