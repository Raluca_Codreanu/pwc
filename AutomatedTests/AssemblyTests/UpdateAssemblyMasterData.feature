﻿@UpdateAssemblyMD
Feature: UpdateAssemblyMasterData
	I want to be able to edit an assembly which is located in Master Data

Scenario: Update Assembly from Master Data
	Given I press edit button
	And I change some filed's values
	And I change the country 
	And I change the media
	And I select the Manufacturer tab in order to change some fields
	And I add some Documents in this tab
	And I make some changes in Overhead Settings tab
	When I press the Save button
	Then the assembly is saved in data grid
