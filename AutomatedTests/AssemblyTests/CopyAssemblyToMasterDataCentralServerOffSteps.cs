﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.IO;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CopyAssemblyToMasterDataCentralServerOffSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CopyAssemblyToMDCentralServerOff")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");

            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyAssemblyToMDCentralServerOff")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I go to Options-> Preferences, Database tab to change the Central db server name")]
        public void GivenIGoToOptions_PreferencesDatabaseTabToChangeTheCentralDbServerName()
        {
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = Wait.For(() => application.Windows.Preferences);

            TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);
            databaseTab.Click();

            TextBox centralDbServerName = preferencesWindow.Get<TextBox>("CentralDbServerName");
            centralDbServerName.Text = "123";

            // TODO: closing the main app window is not part of this step.
            Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();

            mainWindow.Close();
            Assert.IsTrue(Wait.For(() => application.HasExited), "Failed to close the application.");
        }

        [Given(@"I press Restart button")]
        public void GivenIPressRestartButton()
        {
            // TODO: here you are supposed to press the restart button, wait for the app to start again and go back to the main screen.
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [Given(@"I select an assembly to choose the Copy To master Data option")]
        public void GivenISelectAnAssemblyToChooseTheCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly2");
            assemblyNode.SelectEx();

            Menu copyToMasterDataMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I press Yes button")]
        public void GivenIPressYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }

        [When(@"I press the Ok button")]
        public void WhenIPressTheOkButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
        }

        [Then(@"the message is closed")]
        public void ThenTheMessageIsClosed()
        {
            application.MainMenu.Options_Preferences.Click();            
            Window preferencesWindow = Wait.For(() => application.Windows.Preferences);

            TabPage databaseTab = preferencesWindow.Get<TabPage>(AutomationIds.DatabaseTab);
            databaseTab.Click();

            TextBox centralDbServerName = preferencesWindow.Get<TextBox>("CentralDbServerName");
            centralDbServerName.Text = @"localhost\SQLEXPRESS";

            Button saveButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
    }
}
