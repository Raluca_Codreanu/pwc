﻿@AssemblyImportExport
Feature: AssemblyImportExport

Scenario: Import and Export Assembly
	Given I select the assembly node to choose Export option	
	And I cancel the Save As window
	And I press again the Export option
	And I press the Save button	
	When I select the project node to import the exported assembly	 
	Then the assembly is imported in the project
