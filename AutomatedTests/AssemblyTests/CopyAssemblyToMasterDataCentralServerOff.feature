﻿@CopyAssemblyToMDCentralServerOff
Feature: CopyAssemblyToMasterDataCentralServerOff

Scenario: Copy Assembly To master data central Server offline
	Given I go to Options-> Preferences, Database tab to change the Central db server name
	And I press Restart button 
	And I select an assembly to choose the Copy To master Data option
	And I press Yes button 
	When I press the Ok button
	Then the message is closed
