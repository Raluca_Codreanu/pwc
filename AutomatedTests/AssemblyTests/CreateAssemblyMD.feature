﻿@CreateAssemblyMD
Feature: CreateAssemblyMasterData

Scenario: Create Assembly in Master Data
	Given I press Add button
	And I fill the fields from General tab
	And I choose a country
	And I add a media field
	And I select Manufacturer tab and fill the name field
	When I press Save
	Then the assembly is saved and it is displayed on data grid

