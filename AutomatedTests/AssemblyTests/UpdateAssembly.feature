﻿@UpdateAssembly
Feature: UpdateAssembly
	I want to be able to edit an assembly

Scenario: Update Assembly
	Given I change the value of fields from General tab
	And I select another country
	And I add a media file
	And I select Manufacturer tab and change field's vaulues
	And I select Settings tab and I change the value of: Purchase Price, Target Price, Delivery Type 
	And I select Documents tab and I add a document file
	When I press the Save buttton
	Then I select the General tab
