﻿@CopyAssembly
Feature: CopyAssembly

@SameProject
Scenario: Copy Assembly in the same project
	Given I select the project -> assembly node in order to choose Copy option
	And I select the project to choose Paste option
	And I select the copied assembly to choose the Copy option 
	When I select the project node to choose the Paste option	 
	Then the assembly is copied in the project

@SameAssembly
Scenario: Copy Assembly in the same assembly
	Given I select an assembly2 from Subassembly node to be copied	
	And I select the Subassembly node to choose the Paste option
	And I select the copied assembly2 to choose Copy option 
	When I select the Subassembly node to choose the Paste option	 
	Then the assebmly2 is copied in the project
