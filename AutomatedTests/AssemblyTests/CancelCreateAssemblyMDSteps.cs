﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CancelCreateAssemblyMDSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CancelCreateAssemblyMD")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode masterDataAssembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            masterDataAssembliesNode.SelectEx();
        }

        [AfterFeature("CancelCreateAssemblyMD")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press the add button")]
        public void GivenIPressTheAddButton()
        {
            Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
            addButton.Click();
        }
        
        [Given(@"I click the Cancel")]
        public void GivenIClickTheCancel()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            Button cancelButton = createAssemblyWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press Add")]
        public void GivenIPressAdd()
        {
            Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
            addButton.Click();
        }
        
        [Given(@"I fill some fields from Create Assembly window")]
        public void GivenIFillSomeFieldsFromCreateAssemblyWindow()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
            assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;
            TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
            Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
            numberTextBox.Text = "123";

            TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";

            TextBox descriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "description123";

            TextBox batchSizeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.BatchSizeTextBox);
            batchSizeTextBox.Text = "10";
        }
        
        [Given(@"I press the Cancel button in Create Assembly window")]
        public void GivenIPressTheCancelButtonInCreateAssemblyWindow()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            Button cancelButton = createAssemblyWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();    
        }
        
        [Given(@"I press the No button from confirmation win")]
        public void GivenIPressTheNoButtonFromConfirmationWin()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press the Cancel again")]
        public void GivenIPressTheCancelAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();   
        }
        
        [Given(@"I close the confirmation wind")]
        public void GivenICloseTheConfirmationWind()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }

        [Given(@"I press the Cancel buttn")]
        public void GivenIPressTheCancelButtn()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            Button cancelButton = createAssemblyWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();  
        }
        
        [Given(@"I press Yes in confimation screen")]
        public void GivenIPressYesInConfimationScreen()
        {
              Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Given(@"I press the Add button")]
        public void GivenIPressTheAddButton1()
        {
            Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
            addButton.Click();    
        }
        
        [Given(@"I close the Create assembly window")]
        public void GivenICloseTheCreateAssemblyWindow()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.Close();
        }

        [Given(@"I fill some of the fields")]
        public void GivenIFillSomeOfTheFields()
        {
            Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
            addButton.Click();
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
            assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;
            TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
            Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
            numberTextBox.Text = "123";

            TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123";
        }

        [Given(@"I press the No button in the confirmation win")]
        public void GivenIPressTheNoButtonInTheConfirmationWin()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.Close();
            createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I close the create assembly window")]
        public void GivenICloseTheCreateAssemblyWindow1()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.Close();
        }
        
        [When(@"I press yes in confirmation window")]
        public void WhenIPressYesInConfirmationWindow()
        {
            Window createAssemblyWindow = application.Windows.AssemblyWindow;
            createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the create assembly window is closed")]
        public void ThenTheCreateAssemblyWindowIsClosed()
        {            
            Assert.IsNull(application.Windows.AssemblyWindow, "no closed");
        }
    }
}
