﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CancelUpdateAssemblyMDSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Assembly assembly1 = new Assembly();
            assembly1.Name = "cancelUpdateassemblyMD";
            assembly1.SetOwner(adminUser);
            assembly1.IsMasterData = true;
            CountrySetting country = new CountrySetting();
            assembly1.CountrySettings = country;

            assembly1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            assembly1.Manufacturer = manufacturer1;

            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CancelUpdateAssemblyMD")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CancelUpdateAssemblyMD")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select an assembly from Master Data")]
        public void GivenISelectAnAssemblyFromMasterData()
        {
            ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(assembliesDataGrid, "Manage Master Data grid was not found.");
            assembliesDataGrid.Select("Name", "cancelUpdateassemblyMD");
        }
        
        [Given(@"I press the Edit")]
        public void GivenIPressTheEdit()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            Assert.IsNotNull(editButton, "The Delete button was not found.");
            editButton.Click();
            mainWindow.WaitForAsyncUITasks();   
        }
        
        [Given(@"I press Cancel")]
        public void GivenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I update some fields")]
        public void GivenIUpdateSomeFields()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTexBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTexBox.Text = "createdAssembly";

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "11";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "test";  
        }
        
        [Given(@"I press No in confirmation window")]
        public void GivenIPressNoInConfirmationWindow()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();            
        }
        
        [Given(@"I close \(X\) the confirmation window")]
        public void GivenICloseXTheConfirmationWindow()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();            
        }
        
        [Given(@"I click the Cancel btn")]
        public void GivenIClickTheCancelBtn()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press the Yes button in confirmation screen")]
        public void GivenIPressTheYesButtonInConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }
        
        [Given(@"I press Home button")]
        public void GivenIPressHomeButton()
        {
            Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
            Assert.IsNotNull(homeButton, "The Home button was not found.");
            Assert.IsTrue(homeButton.Enabled, "The Home button is disabled.");

            homeButton.Click();            
        }
        
        [When(@"I press the No button in confirmation screen")]
        public void WhenIPressTheNoButtonInConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();            
        }
        
        [Then(@"the users lougout")]
        public void ThenTheUsersLougout()
        {
            var welcomeScreen = Wait.For(() => application.WelcomeScreen);
            Button skipButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
            skipButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
    }
}
