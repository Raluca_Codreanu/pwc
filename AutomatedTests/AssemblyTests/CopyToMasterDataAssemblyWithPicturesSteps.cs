﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyToMasterDataAssemblyWithPicturesSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CopyToMasterDataAssemblyWithPictures")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);            
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectAssemblyPictures.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyToMasterDataAssemblyWithPictures")]
        private static void KillApplication()
        {
            application.Kill();
        }
        
        [Given(@"I select the assemblyPictures to choose '(.*)'")]
        public void GivenISelectTheAssemblyPicturesToChoose(string p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectAssemblyPictures");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyPictures");
            assemblyNode.SelectEx();

            Menu copyToMasterDataMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click(); 
        }

        [Given(@"I press the Yes button in confirmation window")]
        public void GivenIPressTheYesButtonInConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Given(@"I go to Master Data Entities Assemblies")]
        public void GivenIGoToMasterDataEntitiesAssemblies()
        {
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
        }
                
        [Given(@"I select the assemblyPictures from dataGrid")]
        public void GivenISelectTheAssemblyPicturesFromDataGrid()
        {
            ListViewControl assemblyDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            mainWindow.WaitForAsyncUITasks();
            assemblyDataGrid.Select("Name", "assemblyPictures");
        }

        [Given(@"I press the Edit button")]
        public void GivenIPressTheEditButton()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }

        [Given(@"I expand the assembly from tree and select the part node")]
        public void GivenIExpandTheAssemblyFromTreeAndSelectThePartNode()
        {
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();

            TreeNode assemblyNode = assembliesNode.GetNode("assemblyPictures");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode partsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.PartsNode);
            partsNode.SelectEx();
            partsNode.ExpandEx();

            TreeNode partNode = partsNode.GetNode("part1");
            partNode.SelectEx();
        }
        
       
        [When(@"I delete one of the pictures from assemblie's Media control panel")]
        public void WhenIDeleteOneOfThePicturesFromAssemblieSMediaControlPanel()
        {
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.ExpandEx();

            TreeNode assemblyNode = assembliesNode.GetNode("assemblyPictures");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            deleteMediaButton.Click();
        }
        
        [When(@"I press the Yes button from confirmation screen")]
        public void WhenIPressTheYesButtonFromConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }
        
        [Then(@"all the pictures are deleted and the Delete button is disabled")]
        public void ThenAllThePicturesAreDeletedAndTheDeleteButtonIsDisabled()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsFalse(deleteMediaButton.Enabled, "Media panel should be empty");
        }
    }
}

