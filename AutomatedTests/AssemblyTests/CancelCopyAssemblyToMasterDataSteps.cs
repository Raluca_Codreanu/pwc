﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CancelCopyAssemblyToMasterDataSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CancelCopyAssemblyToMasterData")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CancelCopyAssemblyToMasterData")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select an assembly to choose the Copy To Master Data option")]
        public void GivenISelectAnAssemblyToChooseTheCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly2");
            assemblyNode.SelectEx();

            Menu copyToMasterDataMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press No in confirmation screen")]
        public void GivenIPressNoInConfirmationScreen()
        {            
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I select an assembly to choose to Copy To Master Data option")]
        public void GivenISelectAnAssemblyToChooseToCopyToMasterDataOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly2");
            assemblyNode.SelectEx();
            assemblyNode.Collapse();

            Menu copyToMasterDataMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();   
        }
        
        [When(@"I close the confirmation screen")]
        public void WhenICloseTheConfirmationScreen()
        {            
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Then(@"the assembly is not copied in Master Data")]
        public void ThenTheAssemblyIsNotCopiedInMasterData()
        {
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            Assert.IsNotNull(assembliesNode, "The Assemblies node was not found.");
            assembliesNode.Click();
            ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(assembliesDataGrid, "Manage Master Data grid was not found.");

            mainWindow.WaitForAsyncUITasks();
            Assert.IsNull(assembliesDataGrid.Row("Name", "assemblyToDelete"), "Assebmly is copied");
        }
    }
}
