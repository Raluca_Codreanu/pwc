﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class UpdateAssemblyMasterDataSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Assembly assembly1 = new Assembly();
            assembly1.Name = "assemblyMD";
            assembly1.SetOwner(adminUser);
            assembly1.IsMasterData = true;
            CountrySetting country = new CountrySetting();
            assembly1.CountrySettings = country;

            assembly1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            assembly1.Manufacturer = manufacturer1;

            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("UpdateAssemblyMD")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("UpdateAssemblyMD")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press edit button")]
        public void GivenIPressEditButton()
        {
            ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(assembliesDataGrid, "Manage Master Data grid was not found.");
            assembliesDataGrid.Select("Name", "assemblyMD");
                                              
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            Assert.IsNotNull(editButton, "The Edit button was not found.");
            editButton.Click();
            mainWindow.WaitForAsyncUITasks();         
        }
        
        [Given(@"I change some filed's values")]
        public void GivenIChangeSomeFiledSValues()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();
            mainWindow.WaitForAsyncUITasks();   

            TextBox nameTexBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTexBox.Text = "createdAssembly";

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "11";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "test";    
        }
        
        [Given(@"I change the country")]
        public void GivenIChangeTheCountry()
        {
            Button browseCountryButton = mainWindow.Get<Button>(AutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();

            Window browseMasterDataWindow = application.Windows.CountryAndSupplierBrowser;
            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Sweden");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");

            selectButton.Click();
            TextBox countryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
        }
        
        [Given(@"I change the media")]
        public void GivenIChangeTheMedia()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button addMediaButton = Wait.For(() => mainWindow.Get<Button>("AddMediaButton"));
            addMediaButton.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\Tulips.jpg"));
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the Manufacturer tab in order to change some fields")]
        public void GivenISelectTheManufacturerTabInOrderToChangeSomeFields()
        {
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "Name text box was not found.");
            nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

            TextBox manufacturerDescriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
            manufacturerDescriptionTextBox.Text = "Description of Manufacturer";
        }
        
        [Given(@"I add some Documents in this tab")]
        public void GivenIAddSomeDocumentsInThisTab()
        {
            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);            
            documentsTab.Click();

            Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
            addDocumentsButton.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I make some changes in Overhead Settings tab")]
        public void GivenIMakeSomeChangesInOverheadSettingsTab()
        {
            TabPage overheadSettingsTab = mainWindow.Get<TabPage>(AutomationIds.OHSettingsTab);            
            overheadSettingsTab.Click();

            OverheadSetting updatedSettings = SettingsTests.EditOverheadSettingsTestCase.CreateOverheadSettings();
            SettingsTests.EditOverheadSettingsTestCase.FillOverheadSettingsFieldsAndAssert(mainWindow, updatedSettings);
        }
        
        [When(@"I press the Save button")]
        public void WhenIPressTheSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [Then(@"the assembly is saved in data grid")]
        public void ThenTheAssemblyIsSavedInDataGrid()
        {
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
            mainWindow.WaitForAsyncUITasks();
            ListViewControl assembliesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            mainWindow.WaitForAsyncUITasks();
            Assert.IsNotNull(assembliesDataGrid.Row("Name", "createdAssembly"), "The assembly was created.");
        }
    }
}
