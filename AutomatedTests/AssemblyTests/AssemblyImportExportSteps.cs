﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class AssemblyImportExportSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string assemblyExportPath;

        public AssemblyImportExportSteps()
        {
        }

        public static void MyClassInitialize()
        {
            AssemblyImportExportSteps.assemblyExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".assembly");
            if (File.Exists(AssemblyImportExportSteps.assemblyExportPath))
            {
                File.Delete(AssemblyImportExportSteps.assemblyExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(AssemblyImportExportSteps.assemblyExportPath))
            {
                File.Delete(AssemblyImportExportSteps.assemblyExportPath);
            }
        }

        [BeforeFeature("AssemblyImportExport")]
        private static void LaunchApplication()
        {
            MyClassInitialize();

            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();

        }

        [AfterFeature("AssemblyImportExport")]
        private static void KillApplication()
        {
            MyClassCleanup();
            application.Kill();            
        }

        [Given(@"I select the assembly node to choose Export option")]
        public void GivenISelectTheAssemblyNodeToChooseExportOption()
        {

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.Collapse();
            assembly1Node.SelectEx();
            Menu exportMenuItem = assembly1Node.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
               
        [Given(@"I cancel the Save As window")]
        public void GivenICancelTheSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I press again the Export option")]
        public void GivenIPressAgainTheExportOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.SelectEx();
            assembly1Node.Collapse();

            Menu exportMenuItem = assembly1Node.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press the Save button")]
        public void GivenIPressTheSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(AssemblyImportExportSteps.assemblyExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(AssemblyImportExportSteps.assemblyExportPath), "The assembly was not exported at the specified location.");
        }

        [When(@"I select the project node to import the exported assembly")]
        public void WhenISelectTheProjectNodeToImportTheExportedAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.SelectEx();

            Menu importPartMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.ImportProjectNode, AutomationIds.AssemblyImport);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Assembly menu item is disabled.");
            importPartMenuItem.Click();
        }
                
        [Then(@"the assembly is imported in the project")]
        public void ThenTheAssemblyIsImportedInTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly1");
            assemblyNode.SelectEx();
        }
    }
}
