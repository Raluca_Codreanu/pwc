﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CopyAssemblySteps
    {
        // Feature scoped data
        // TestLink id = 446, id = 447
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyAssembly")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport , AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectCopyAssembly.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the project -> assembly node in order to choose Copy option")]
        public void GivenISelectTheProject_AssemblyNodeInOrderToChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();

            Menu copyMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();

        }
        
        [Given(@"I select the project to choose Paste option")]
        public void GivenISelectTheProjectToChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();

            Menu pasteMenu = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenu.Click();
        }
        
        [Given(@"I select the copied assembly to choose the Copy option")]
        public void GivenISelectTheCopiedAssemblyToChooseTheCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode(copyTranslation + "assemblyCopy");
            assemblyNode.SelectEx();

            Menu copyMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select an assembly(.*) from Subassembly node to be copied")]
        public void GivenISelectAnAssemblyFromSubassemblyNodeToBeCopied(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode subAssembly = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();
            subAssembly.ExpandEx();

            TreeNode assembly2 = subAssembly.GetNode("assemblyCopy2");
            assembly2.SelectEx();

            Menu copyMenuItem = assembly2.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();  

        }
        
        [Given(@"I select the Subassembly node to choose the Paste option")]
        public void GivenISelectTheSubassemblyNodeToChooseThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode subAssembly = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();

            Menu pasteMenu = subAssembly.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenu.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the copied assembly(.*) to choose Copy option")]
        public void GivenISelectTheCopiedAssemblyToChooseCopyOption(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode subAssembly = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();
            subAssembly.ExpandEx();

            TreeNode assembly2 = subAssembly.GetNode(copyTranslation + "assemblyCopy2");
            assembly2.SelectEx();

            Menu copyMenuItem = assembly2.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();   
        }
        
        [When(@"I select the project node to choose the Paste option")]
        public void WhenISelectTheProjectNodeToChooseThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();
            mainWindow.WaitForAsyncUITasks();

            Menu pasteMenu = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenu.Click();   
        }
        
        [When(@"I select the Subassembly node to choose the Paste option")]
        public void WhenISelectTheSubassemblyNodeToChooseThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode subAssembly = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();

            Menu pasteMenu = subAssembly.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenu.Click();   
        }
        
        [Then(@"the assembly is copied in the project")]
        public void ThenTheAssemblyIsCopiedInTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode(copyTranslation + copyTranslation + "assemblyCopy");
            assemblyNode.SelectEx();    
        }
        
        [Then(@"the assebmly(.*) is copied in the project")]
        public void ThenTheAssebmlyIsCopiedInTheProject(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectCopyAssembly");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyCopy");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode subAssembly = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();
            subAssembly.ExpandEx();

            TreeNode assembly2 = subAssembly.GetNode(copyTranslation + copyTranslation + "assemblyCopy2");
            assembly2.SelectEx();   
        }
    }
}
