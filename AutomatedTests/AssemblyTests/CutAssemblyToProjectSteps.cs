﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CutAssemblyToProjectSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CutAssemblyToProject")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectAssemblyPictures.project"));
            mainWindow.WaitForAsyncUITasks();

            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.Collapse();

            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importAnotherProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CutAssemblyToProject")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I expand myProject, select assembly(.*) and choose Cut option")]
        public void GivenIExpandMyProjectSelectAssemblyAndChooseCutOption(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly1");
            assemblyNode.SelectEx();
            mainWindow.WaitForAsyncUITasks();
            Menu cutMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click(); 
        }
        
        [When(@"I select the projectAssemblyPictures Project and choose Paste option")]
        public void WhenISelectTheProjectAssemblyPicturesProjectAndChoosePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectAssemblyPictures");
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [Then(@"I expand projectAssemblyPictures node, the assembly(.*) is moved here")]
        public void ThenIExpandProjectAssemblyPicturesNodeTheAssemblyIsMovedHere(int p0)
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectAssemblyPictures");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly1");
            assemblyNode.SelectEx();                    
        }
    }
}
