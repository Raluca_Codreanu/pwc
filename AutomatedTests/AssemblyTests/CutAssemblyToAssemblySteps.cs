﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CutAssemblyToAssemblySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CutAssemblyToAssembly")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);            
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project1.project"));
            mainWindow.WaitForAsyncUITasks();

            myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            //myProjectsNode.SelectEx();
            myProjectsNode.Collapse();
            importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);           
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project2.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CutAssemblyToAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select an assembly and choose Cut option")]
        public void GivenISelectAnAssemblyAndChooseCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project1");
            projectNode.SelectEx();
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly11");
            assemblyNode.SelectEx();

            Menu cutMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I expand the second project, an assembly to choose the Paste option")]
        public void WhenIExpandTheSecondProjectAnAssemblyToChooseThePasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project2");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly2");
            assemblyNode.SelectEx();

            Menu pasteMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"I expand second project -> assembly -> Subaasembly and the assembly is moved here")]
        public void ThenIExpandSecondProject_Assembly_SubaasemblyAndTheAssemblyIsMovedHere()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("project2");
            projectNode.SelectEx();

            TreeNode assemblyNode = projectNode.GetNode("assembly2");
            assemblyNode.ExpandEx();
            assemblyNode.SelectEx();

            TreeNode subassemblies = assemblyNode.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subassemblies.ExpandEx();

            TreeNode assembly1Node = subassemblies.GetNode("assembly11");
            assembly1Node.SelectEx();
        }
    }
}
