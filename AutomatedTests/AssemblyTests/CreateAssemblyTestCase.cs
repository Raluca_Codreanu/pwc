﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Automation;
using White.Core;
using White.Core.InputDevices;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.UserAccess;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    /// <summary>
    /// Summary description for CreateAssemblyTestCase
    /// </summary>
    [TestClass]
    public class CreateAssemblyTestCase
    {
        public CreateAssemblyTestCase()
        {
        }

        #region Additional test attributes
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            CreateAssemblyTestCase.CreateTestData();
        }
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Create Assembly with Fine Calculation accuracy Test Case (id=350).
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CreateAssemblyFineCalculationTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly");
                project1Node.SelectEx();

                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;

                //step 2
                TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
                numberTextBox.Text = "123";

                TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description123";

                TextBox batchSizeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                TextBox yearlyProdQtyTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.YearlyProdQtyTextBox);
                yearlyProdQtyTextBox.Text = "12";

                TextBox lifeTimeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "12";

                ComboBox calculationApproachComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationApproachComboBox);
                calculationApproachComboBox.SelectItem(0);

                //step 4
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;
                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
               
                //step 8
                ComboBox accuracyComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationAccuracyComboBox);
                Assert.IsNotNull(accuracyComboBox, "TypeComboBox was not found.");

                // item(1) = Fine Calculation 
                accuracyComboBox.SelectItem(1);

                //step 10
                //manufacturer tab data
                TabPage manufacturerTab = createAssemblyWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                TabPage settingsTab = createAssemblyWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Click();

                TextBox purchasePriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = "123";

                TextBox targetPriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = "456";

                ComboBox deliveryTypeComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.DeliveryTypeComboBox);
                deliveryTypeComboBox.SelectItem(1);

                TextBox assetRateTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.AssetRateTextBox);
                assetRateTextBox.Text = "5";

                //documents tab
                TabPage documentsTab = createAssemblyWindow.Get<TabPage>(AutomationIds.DocumentsTab);
                documentsTab.Click();

                Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                addDocumentsButton.Click();

                createAssemblyWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));

                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
        }

        /// <summary>
        /// An automated test for Create Assembly with Rough Calculation accuracy Test Case (id=352).
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CreateAssemblyRoughCalculationTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects; 
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly2");
                project1Node.SelectEx();
                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();
                
                Window createAssemblyWindow = app.Windows.AssemblyWindow;
                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;

                //step 2
                TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
                numberTextBox.Text = "123";

                TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description123";

                //step 6
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");
                selectButton.Click();

                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                TextBox batchSizeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                TextBox yearlyProdQtyTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.YearlyProdQtyTextBox);
                yearlyProdQtyTextBox.Text = "12";

                TextBox lifeTimeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "12";

                ComboBox calculationApproachComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationApproachComboBox);
                calculationApproachComboBox.SelectItem(0);

                //step 3
                ComboBox accuracyComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationAccuracyComboBox);
                Assert.IsNotNull(accuracyComboBox, "TypeComboBox was not found.");
                accuracyComboBox.SelectItem(0); // item(0) = Rough Calculation


                //step 4
                TextBox estimatedCostTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.EstimatedCostTextBox);
                Assert.IsNotNull(estimatedCostTextBox, "EstimatedCostTextBox was not found");
                estimatedCostTextBox.Text = "1000";

                //step 5
                TextBox weightTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.WeightTextBox);
                Assert.IsNotNull(weightTextBox, "Weight Text Box was not found");
                weightTextBox.Text = "10";

                //step 7
                createAssemblyWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Wildlife.wmv", UriKind.Relative);
                
                //step 10
                //manufacturer tab data
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Click();

                TextBox purchasePriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = "123";

                TextBox targetPriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = "456";

                ComboBox deliveryTypeComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.DeliveryTypeComboBox);
                deliveryTypeComboBox.SelectItem(1);

                TextBox assetRateTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.AssetRateTextBox);
                assetRateTextBox.Text = "5";

                //documents tab
                TabPage documentsTab = createAssemblyWindow.Get<TabPage>(AutomationIds.DocumentsTab);
                documentsTab.Click();

                Button addDocumentsButton = createAssemblyWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                addDocumentsButton.Click();

                createAssemblyWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
                
                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
            }
        }

        /// <summary>
        /// An automated test for Create Assembly with Estimation Accuracy Test Case (id=351).
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CreateAssemblyEstimationAccuracyTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly2");

                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;

                //step 2
                TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
                numberTextBox.Text = "123";

                TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description123";

                TextBox batchSizeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                TextBox yearlyProdQtyTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.YearlyProdQtyTextBox);
                yearlyProdQtyTextBox.Text = "12";

                TextBox lifeTimeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "12";

                ComboBox calculationApproachComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationApproachComboBox);
                calculationApproachComboBox.SelectItem(0);

                //step 3
                ComboBox accuracyComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationAccuracyComboBox);
                Assert.IsNotNull(accuracyComboBox, "TypeComboBox was not found.");

                // item(2) = Estimation 
                accuracyComboBox.SelectItem(2);

                //step 4
                TextBox estimatedCostTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.EstimatedCostTextBox);
                Assert.IsNotNull(estimatedCostTextBox, "EstimatedCostTextBox was not found");
                estimatedCostTextBox.Text = "1000";

                //step 5
                TextBox weightTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.WeightTextBox);
                Assert.IsNotNull(weightTextBox, "Weight Text Box was not found");
                weightTextBox.Text = "10";

                //step 6
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                createAssemblyWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Wildlife.wmv", UriKind.Relative);
                
                //step 7
                //manufacturer tab data
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);                
                settingsTab.Click();

                TextBox purchasePriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = "123";

                TextBox targetPriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = "456";

                ComboBox deliveryTypeComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.DeliveryTypeComboBox);
                deliveryTypeComboBox.SelectItem(1);

                TextBox assetRateTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.AssetRateTextBox);
                assetRateTextBox.Text = "5";

                //documents tab
                TabPage documentsTab = createAssemblyWindow.Get<TabPage>(AutomationIds.DocumentsTab);                
                documentsTab.Click();

                Button addDocumentsButton = createAssemblyWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                addDocumentsButton.Click();

                createAssemblyWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));

                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
        }

        /// <summary>
        /// An automated test for Create Assembly with Offer/External Calculation Accuracy Test Case (id=353).
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CreateAssemblyOfferAccuracyTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly3");

                project1Node.SelectEx();
                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;

                //step 2
                TextBox numberTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found");
                numberTextBox.Text = "123";

                TextBox versionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                TextBox descriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "description123";

                TextBox batchSizeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.BatchSizeTextBox);
                batchSizeTextBox.Text = "10";

                TextBox yearlyProdQtyTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.YearlyProdQtyTextBox);
                yearlyProdQtyTextBox.Text = "12";

                TextBox lifeTimeTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "12";

                ComboBox calculationApproachComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationApproachComboBox);
                calculationApproachComboBox.SelectItem(0);

                //step 3
                ComboBox accuracyComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.CalculationAccuracyComboBox);
                Assert.IsNotNull(accuracyComboBox, "TypeComboBox was not found.");
                accuracyComboBox.SelectItem(3); // item(3) = Offer/External Calculation 

                //step 4
                TextBox estimatedCostTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.EstimatedCostTextBox);
                Assert.IsNotNull(estimatedCostTextBox, "EstimatedCostTextBox was not found");
                estimatedCostTextBox.Text = "1000";

                //step 5
                TextBox weightTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.WeightTextBox);
                Assert.IsNotNull(weightTextBox, "Weight Text Box was not found");
                weightTextBox.Text = "10";

                //step 6
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                createAssemblyWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Wildlife.wmv", UriKind.Relative);
                
                //step 7
                //manufacturer tab data
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Click();

                TextBox purchasePriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = "123";

                TextBox targetPriceTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = "456";

                ComboBox deliveryTypeComboBox = createAssemblyWindow.Get<ComboBox>(AssemblyAutomationIds.DeliveryTypeComboBox);
                deliveryTypeComboBox.SelectItem(1);

                TextBox assetRateTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.AssetRateTextBox);
                assetRateTextBox.Text = "5";

                //documents tab
                TabPage documentsTab = createAssemblyWindow.Get<TabPage>(AutomationIds.DocumentsTab);
                documentsTab.Click();

                Button addDocumentsButton = createAssemblyWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                addDocumentsButton.Click();

                createAssemblyWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));

                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                Assert.IsNull(app.Windows.AssemblyWindow, "The create assembly window was not closed after save.");
            }
        }

        /// <summary>
        /// An automated test for Cancel Create Assembly (test link id = 349)
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CancelCreateAssemblyTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectAssembly3");
                project1Node.SelectEx();

                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                //step 2
                Window createAssemblyWindow = app.Windows.AssemblyWindow;
                Button cancelButton = createAssemblyWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3
                createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                //step 4
                createAssemblyWindow = app.Windows.AssemblyWindow;
                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                cancelButton = createAssemblyWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 5
                createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 6
                cancelButton.Click();
                createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 7               
                cancelButton.Click();
                createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 8
                project1Node.SelectEx();
                createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                createAssemblyWindow = Wait.For(() => app.Windows.AssemblyWindow);
                createAssemblyWindow.Close();

                //step 9
                project1Node.SelectEx();
                createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                createAssemblyWindow = Wait.For(() => app.Windows.AssemblyWindow);
                assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assembly" + DateTime.Now.Ticks;
                manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";
                createAssemblyWindow.Close();

                //step 10              
                Assert.IsTrue(createAssemblyWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");

                createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 11
                // press X button in order to close the message window
                createAssemblyWindow.Close();

                //step 10                
                Assert.IsTrue(createAssemblyWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Cancel.");
                createAssemblyWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

         #region Helper
        /// <summary>
        /// Creates the data for this test
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectAssembly";
            project1.SetOwner(adminUser);
                        
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();

            Project project2 = new Project();
            project2.Name = "projectAssembly2";
            project2.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project2.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 2;
            project2.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project2);
            dataContext.SaveChanges();

            Project project3 = new Project();
            project3.Name = "projectAssembly3";
            project3.SetOwner(adminUser);
            Customer c3 = new Customer();
            c3.Name = "customer3";
            project3.Customer = c3;
            OverheadSetting overheadSetting3 = new OverheadSetting();
            overheadSetting3.MaterialOverhead = 3;
            project3.OverheadSettings = overheadSetting3;

            dataContext.ProjectRepository.Add(project3);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
