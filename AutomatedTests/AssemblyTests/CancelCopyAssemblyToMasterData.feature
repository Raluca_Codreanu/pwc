﻿@CancelCopyAssemblyToMasterData
Feature: CancelCopyAssemblyToMasterData

Scenario: Cancel Copy Assembly to Master Data   
	Given I select an assembly to choose the Copy To Master Data option
	And I press No in confirmation screen 
	And I select an assembly to choose to Copy To Master Data option
	When I close the confirmation screen
	Then the assembly is not copied in Master Data
