﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using White.Core.UIItems.Finders;
using System.IO;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class UpdateAssemblySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Assembly assembly1 = new Assembly();
            assembly1.Name = "assemblyUpdated";
            assembly1.SetOwner(adminUser);
            assembly1.IsMasterData = true;
            assembly1.Description = "test";
            assembly1.CalculationVariant = "1.4";
            CountrySetting country = new CountrySetting();
            assembly1.CountrySettings = country;

            assembly1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            assembly1.Manufacturer = manufacturer1;

            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.SaveChanges();

            Project project = new Project();
            project.Name = "projectUpdateAssembly";
            project.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 1;
            project.OverheadSettings = overheadSetting2;

            project.Assemblies.Add(assembly1);
            project.Assemblies.Add(assembly1);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();
        }

        [BeforeFeature("UpdateAssembly")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectUpdateAssembly");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assemblyUpdated");
            assembly1Node.SelectEx();
        }

        [AfterFeature("UpdateAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"I change the value of fields from General tab")]
        public void GivenIChangeTheValueOfFieldsFromGeneralTab()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123456";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "test";
        }

        [Given(@"I select another country")]
        public void GivenISelectAnotherCountry()
        {
            Button browseCountryButton = mainWindow.Get<Button>(AutomationIds.BrowseCountryButton);
            Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
            browseCountryButton.Click();
            mainWindow.WaitForAsyncUITasks();
            Window browseMasterDataWindow = Wait.For(() => application.Windows.CountryAndSupplierBrowser);

            Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
            masterDataTree.SelectNode("Germany");
            Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
            Assert.IsNotNull(selectButton, "The Select button was not found.");

            selectButton.Click();
            TextBox countryTextBox = mainWindow.Get<TextBox>(AutomationIds.CountryTextBox);
            Assert.IsNotNull(countryTextBox, "The Country text box was not found.");
        }

        [Given(@"I add a media file")]
        public void GivenIAddAMediaFile()
        {
            mainWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Wildlife.wmv", UriKind.Relative);
        }

        [Given(@"I select Manufacturer tab and change field's vaulues")]
        public void GivenISelectManufacturerTabAndChangeFieldSVaulues()
        {
            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "Name text box was not found.");
            nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

            TextBox manufacturerDescriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
            manufacturerDescriptionTextBox.Text = "Description of Manufacturer";
        }

        [Given(@"I select Settings tab and I change the value of: Purchase Price, Target Price, Delivery Type")]
        public void GivenISelectSettingsTabAndIChangeTheValueOfPurchasePriceTargetPriceDeliveryType()
        {
            TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
            settingsTab.Click();

            TextBox purchasePriceTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.PurchasePriceTextBox);
            purchasePriceTextBox.Text = "123";

            TextBox targetPriceTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.TargetPriceTextBox);
            targetPriceTextBox.Text = "456";
        }

        [Given(@"I select Documents tab and I add a document file")]
        public void GivenISelectDocumentsTabAndIAddADocumentFile()
        {
            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();

            Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
            addDocumentsButton.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
        }

        [When(@"I press the Save buttton")]
        public void WhenIPressTheSaveButtton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }

        [Then(@"I select the General tab")]
        public void ThenISelectTheGeneralTab()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();
        }
    }
}
