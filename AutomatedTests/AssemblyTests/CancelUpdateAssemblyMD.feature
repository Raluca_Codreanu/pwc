﻿@CancelUpdateAssemblyMD
Feature: CancelUpdateAssemblyMD

Scenario: Cancel Update Assembly From Master Data
	Given I select an assembly from Master Data
	And I press the Edit
	And I press Cancel
	And I update some fields
	And I press Cancel
	And I press No in confirmation window
	And I press Cancel
	And I close (X) the confirmation window
	And I click the Cancel btn
	And I press the Yes button in confirmation screen 
	And I update some fields	 
	And I press Home button
	When I press the No button in confirmation screen
	Then the users lougout
