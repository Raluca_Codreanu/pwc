﻿@CutAssemblyToProject
Feature: CutAssemblyToProject

Scenario: Cut Assembly and Paste it in a Project
	Given I expand myProject, select assembly1 and choose Cut option 
	When I select the projectAssemblyPictures Project and choose Paste option
	Then I expand projectAssemblyPictures node, the assembly1 is moved here
