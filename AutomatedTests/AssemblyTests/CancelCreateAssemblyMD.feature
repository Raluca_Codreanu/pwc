﻿@CancelCreateAssemblyMD
Feature: CancelCreateAssemblyMD

Scenario: Cancel Create Assembly From Master Data
	Given I press the add button
	And I click the Cancel
	And I press Add
	And I fill some fields from Create Assembly window
	And I press the Cancel button in Create Assembly window
	And I press the No button from confirmation win
	And I press the Cancel again
	And I close the confirmation wind
	And I press the Cancel buttn
	And I press Yes in confimation screen
	And I press the Add button
	And I close the Create assembly window
	And I press the Add button
	And I fill some of the fields
	And I close the create assembly window
	And I press the No button in the confirmation win
	And I close the create assembly window
	When I press yes in confirmation window
	Then the create assembly window is closed
