﻿@CopyDeletedAssembly
Feature: CopyDeletedAssembly
	I want to test the functionality of copying a deleted assembly

Scenario: CopyDeletedAssemblyToSubassembly
	Given I select the project -> assembly press right click and choose Copy option
	And I press right click in order to choose Delete option
	And from confirmation window I press the Yes button
	And I select the Subassemblies node
	And I press right click to select the Paste menu item which is disabled
	And I select the assembly
	And I press right click to select the Paste menu item which is disabled
	When I select the Project 
	Then I press right click to select Paste menu item which is disabled
