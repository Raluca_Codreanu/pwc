﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.MenuItems;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [TestClass]
    public class CopyAseemblyTestCase
    {
        #region Additional test attributes
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            CopyAseemblyTestCase.CreateTestData();
        }
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Copy Assembly where the target is project (366)
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CopyAssemblyToAnotherProject()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                var mainWindow = app.Windows.MainWindow;

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectCopyFromAssembly");
                project1Node.SelectEx();

                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                Window createAssemblyWindow = app.Windows.AssemblyWindow;
                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assemblyToBeCopied";
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();

                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;
                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                //manufacturer tab data
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                project1Node.ExpandEx();

                TreeNode assemblyNode = project1Node.GetNode("assemblyToBeCopied");
                assemblyNode.SelectEx();

                Menu copyMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
                copyMenuItem.Click();

                //step 2
                TreeNode projectToNode = myProjectsNode.GetNode("projectCopyToAssembly");
                projectToNode.SelectEx();

                //step 3
                Menu pasteMenuItem = projectToNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
                pasteMenuItem.Click();

                mainWindow.WaitForAsyncUITasks();

                projectToNode.Collapse();
                projectToNode.ExpandEx();
                mainWindow.WaitForAsyncUITasks();
                TreeNode assemblyCopied = projectToNode.GetNode("assemblyToBeCopied");
                assemblyCopied.SelectEx();
            }
        }

        /// <summary>
        /// An automated test for copy assembly where the target is project (id = 365)
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void CopyAssemblyToAnotherAsembly()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {                
                Window mainWindow = app.Windows.MainWindow;

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                TreeNode project1Node = myProjectsNode.GetNode("projectCopyFromAssembly");
                project1Node.SelectEx();

                //create the assembly which will be copied                
                Menu createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();
                Window createAssemblyWindow = app.Windows.AssemblyWindow;

                TextBox assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assemblyToBeCopied1";
                Button browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                //manufacturer tab data
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                TextBox manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                Button saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                //create the second project                
                project1Node.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                project1Node.Collapse();

                //create the assembly which will be copied                
                createAssemblyMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateAssembly);
                createAssemblyMenuItem.Click();

                createAssemblyWindow = Wait.For(() => app.Windows.AssemblyWindow);
                assemblyNameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                assemblyNameTextBox.Text = "assemblyToBeCopied2";
                browseCountryButton = createAssemblyWindow.Get<Button>(AutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Finland");
                selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                countryTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                //manufacturer tab data
                manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                nameTextBox = createAssemblyWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "Name text box was not found.");
                nameTextBox.Text = "Manufacturer" + DateTime.Now.Ticks;

                manufacturerDescriptionTextBox = createAssemblyWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerDescriptionTextBox, "Description text box was not found.");
                manufacturerDescriptionTextBox.Text = "Description of Manufacturer";

                saveButton = createAssemblyWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                TreeNode assembly1Node = project1Node.GetNode("assemblyToBeCopied1");
                assembly1Node.SelectEx();

                Menu copyMenuItem = assembly1Node.GetContextMenuById(mainWindow, AutomationIds.Copy);
                copyMenuItem.Click();

                TreeNode assemblyToNode = project1Node.GetNode("assemblyToBeCopied2");
                assemblyToNode.SelectEx();
                assemblyToNode.Collapse();

                Menu pasteMenuItemAssembly = assemblyToNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
                pasteMenuItemAssembly.Click();
                mainWindow.WaitForAsyncUITasks();

                project1Node.ExpandEx();

                assembly1Node = project1Node.GetNode("assemblyToBeCopied2");
                assembly1Node.ExpandEx();

                TreeNode subassemblies = assembly1Node.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
                assembly1Node.ExpandEx();

                assemblyToNode = subassemblies.GetNode("assemblyToBeCopied1");
                assemblyToNode.SelectEx();
            }
        }

        #region Helper
        /// <summary>
        /// Creates the data for this test
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectCopyFromAssembly";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();

            Project project2 = new Project();
            project2.Name = "projectCopyToAssembly";
            project2.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project2.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 2;
            project2.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project2);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}

