﻿@CancelEditAssembly
Feature: CancelUpdateAssembly
	I want to test the functionality of Cancelling an edited assembly

Scenario: Cancel Update Assembly
	Given I have select the assembly1, click Cancel and nothing happens
	And I set a new value for Version and Description fields
	And I press the Cancel button 
	And from confirmation message I choose No option
	And I press the Cancel button again
	And I close the confirmation window
	And I press the Cancel button again
	And I press Yes button from confirmation screen
	And I select the assembly2 from main tree view
	And I clear the value for Name field and I fill the field Version
	And I select the assembly1 node
	And I press No button from confirmation window
	And I change the value of assembly1's Version field
	And I select the assembly2 node
	And I close the confirmation message (x)
	And I change the name of assembly2 
	And I click the Home buttom
	When the confirmation window is dsplayed and I press the No button
	Then the Home screen in opened an the Login screen is available
