﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    /// <summary>
    /// The automated test of Delete Assembly Test Case.
    /// </summary>
    [TestClass]
    public class DeleteAssemblyTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteAssemblyTestCase"/> class.
        /// </summary>
        public DeleteAssemblyTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]   
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Assembly Test Case. (id=19)
        /// </summary>
        [TestMethod]
        [TestCategory("Assembly")]
        public void DeleteAssemblyTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                // ensure that import summary screen is hidden                                    
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = app.Windows.Preferences;

                CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                displayImportSummaryScreen.Checked = true;

                Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(savePreferencesButton, "The Save button was not found.");

                savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePreferencesButton.Click();
                Wait.For(() => app.Windows.Preferences == null);
                mainWindow.WaitForAsyncUITasks();

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
                importProjectMenuItem.Click();

                mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
                mainWindow.WaitForAsyncUITasks();

                // Wait until the imported project's projects tree item appears.
                TreeNode project1Node = Wait.For(() => myProjectsNode.GetNode("myProject"));
                Assert.IsNotNull(project1Node, "project1 node was not found.");
                project1Node.ExpandEx();

                //step 1
                TreeNode assemblyNode1 = project1Node.GetNode("assembly1");
                assemblyNode1.SelectEx();
                Menu deleteMenuItem = assemblyNode1.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                //step 2
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 3
                deleteMenuItem = assemblyNode1.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                //step 4
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !assemblyNode1.IsAvailable()); // Wait for the deleted assembly tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                //step 5
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                // step 7
                Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                Assert.IsFalse(recoverButton.Enabled, "Recover button should be disabled.");

                //step 8
                ListViewRow assembly1Row = trashBinDataGrid.Row(AutomationIds.ColumnName, "assembly1");
                Assert.IsNotNull(assembly1Row, "Assembly1Row trash bin item was not found.");

                ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, assembly1Row);
                //ListViewCell selectedCell = trashBinDataGrid.Cell("Selected", assembly1Row);
                AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;

                Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
                viewDetailsButton.Click();

                recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                recoverButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                recoverButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish

                //step 9                
                myProjectsNode.ExpandEx();
                project1Node = myProjectsNode.GetNode("myProject");
                project1Node.SelectEx();
                project1Node.ExpandEx();

                assemblyNode1 = project1Node.GetNode("assembly1");
                assemblyNode1.SelectEx();
                assemblyNode1.Click(); // click again to get the focus (workaround for a UI bug).

                //Delete assembly from main menu -> Edit Menu 
                app.MainMenu.Edit_Delete.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !assemblyNode1.IsAvailable()); // Wait for the deleted assembly tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                trashBinNode.SelectEx();
                trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                assembly1Row = trashBinDataGrid.Row("Name", "assembly1");
                Assert.IsNotNull(assembly1Row, "Assembly1Row trash bin item was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, assembly1Row);
                //selectedCell = trashBinDataGrid.Cell("Selected", assembly1Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;

                //step 11
                Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

                permanentlyDeleteButton.Click();

                //step 12
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 13
                permanentlyDeleteButton.Click();

                //step 14
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the permanently delete operation to finish

                assembly1Row = trashBinDataGrid.Row("Name", "assembly1");
                Assert.IsNull(assembly1Row, "The deleted assembly was not removed from the trash bin.");
            }
        }
    }
}
