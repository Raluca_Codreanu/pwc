﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.SettingsTests;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CopyToMasterDataAssemblyVideoSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CopyToMasterDataAssemblyVideo")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectAssemblyVideo.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyToMasterDataAssemblyVideo")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select and assembly with video to press '(.*)'")]
        public void GivenISelectAndAssemblyWithVideoToPress(string p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectAssemblyVideo");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyVideo");
            assemblyNode.SelectEx();

            Menu copyToMasterDataMenuItem = assemblyNode.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMasterDataMenuItem.Click();
        }
        
        [Given(@"I press the Yes button from confirmation window")]
        public void GivenIPressTheYesButtonFromConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I go to Master Data->Entities-Assemblies")]
        public void GivenIGoToMasterData_Entities_Assemblies()
        {            
            TreeNode assembliesNode = application.MainScreen.ProjectsTree.MasterDataAssemblies;
            assembliesNode.Click();
        }
        
        [Given(@"I select the copied assembly")]
        public void GivenISelectTheCopiedAssembly()
        {
            ListViewControl assemblyDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            mainWindow.WaitForAsyncUITasks();
            assemblyDataGrid.Select("Name", "assemblyVideo");           
        }
        
        [When(@"I press Edit")]
        public void WhenIPressEdit()
        {
            Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
            editButton.Click();
        }
        
        [Then(@"the media panel is empty")]
        public void ThenTheMediaPanelIsEmpty()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y + 20);
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsFalse(deleteMediaButton.Enabled, "Media panel should be empty");
        }
    }
}
