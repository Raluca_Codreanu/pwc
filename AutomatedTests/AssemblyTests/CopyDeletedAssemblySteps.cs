﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CopyDeletedAssemblySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CopyDeletedAssembly")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);            
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyDeletedAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the project -> assembly press right click and choose Copy option")]
        public void GivenISelectTheProject_AssemblyPressRightClickAndChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.ExpandEx();
            assembly1Node.SelectEx();

            TreeNode subAssembly = assembly1Node.GetNodeByAutomationId(AutomationIds.SubassembliesNode);            
            subAssembly.SelectEx();
            subAssembly.ExpandEx();

            TreeNode subAssembly1 = subAssembly.GetNode("suassembly1");
            subAssembly1.SelectEx();
            
            Menu copyMenuItem = subAssembly1.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
 
        }
        
        [Given(@"I press right click in order to choose Delete option")]
        public void GivenIPressRightClickInOrderToChooseDeleteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.ExpandEx();
            assembly1Node.SelectEx();

            TreeNode subAssembly = assembly1Node.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();
            subAssembly.ExpandEx();

            TreeNode subAssembly1 = subAssembly.GetNode("suassembly1");
            subAssembly1.SelectEx();

            Menu deleteMenuItem = subAssembly1.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
  
        }
        
        [Given(@"from confirmation window I press the Yes button")]
        public void GivenFromConfirmationWindowIPressTheYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the Subassemblies node")]
        public void GivenISelectTheSubassembliesNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.ExpandEx();

            TreeNode subAssembly = assembly1Node.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();
        }
        
        [Given(@"I press right click to select the Paste menu item which is disabled")]
        public void GivenIPressRightClickToSelectThePasteMenuItemWhichIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.ExpandEx();

            TreeNode subAssembly = assembly1Node.GetNodeByAutomationId(AutomationIds.SubassembliesNode);
            subAssembly.SelectEx();

            Menu pasteMenuItem = subAssembly.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }
        
        [Given(@"I select the assembly")]
        public void GivenISelectTheAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.ExpandEx();
            
            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.SelectEx();

            Menu pasteMenuItem = assembly1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");

            projectNode.Collapse();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I select the Project")]
        public void WhenISelectTheProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.Collapse();
            mainWindow.WaitForAsyncUITasks();
        }

        [Then(@"I press right click to select Paste menu item which is disabled")]
        public void ThenIPressRightClickToSelectPasteMenuItemWhichIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.Collapse();
            projectNode.SelectEx();

            Menu pasteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");

        }
    }
}
