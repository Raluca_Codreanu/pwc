﻿@CopyToMasterDataAssemblyVideo
Feature: CopyToMasterDataAssemblyVideo

Scenario: Copy to Master Data Assembly with Video
	Given I select and assembly with video to press 'Copy To Master Data'
	And I press the Yes button from confirmation window
	And I go to Master Data->Entities-Assemblies
	And I select the copied assembly
	When I press Edit
	Then the media panel is empty
