﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.AssemblyTests
{
    [Binding]
    public class CancelUpdateAssemblySteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Assembly assembly1 = new Assembly();
            assembly1.Name = "assembly1";
            assembly1.SetOwner(adminUser);
            assembly1.IsMasterData = true;
            CountrySetting country = new CountrySetting();
            assembly1.CountrySettings = country;

            assembly1.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();
            assembly1.Manufacturer = manufacturer1;

            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.SaveChanges();

            Assembly assembly2 = new Assembly();
            assembly2.Name = "assembly2";
            assembly2.SetOwner(adminUser);
            assembly2.IsMasterData = true;
            CountrySetting country2 = new CountrySetting();
            assembly2.CountrySettings = country2;

            assembly2.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer2 = new Manufacturer();
            manufacturer2.Name = "Manufacturer" + manufacturer2.Guid.ToString();
            assembly2.Manufacturer = manufacturer2;

            dataContext.AssemblyRepository.Add(assembly2);
            dataContext.SaveChanges();

            Project project = new Project();
            project.Name = "projectCancelUpdateA";
            project.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 1;
            project.OverheadSettings = overheadSetting2;

            project.Assemblies.Add(assembly1);
            project.Assemblies.Add(assembly2);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CancelEditAssembly")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CancelEditAssembly")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I have select the assembly(.*), click Cancel and nothing happens")]
        public void GivenIHaveSelectTheAssemblyClickCancelAndNothingHappens(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectCancelUpdateA");
            projectNode.ExpandEx();

            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.SelectEx();

            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();
        }

        [Given(@"I set a new value for Version and Description fields")]
        public void GivenISetANewValueForVersionAndDescriptionFields()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123456";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "testUpdated";
        }

        [Given(@"I press the Cancel button")]
        public void GivenIPressTheCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();
        }

        [Given(@"from confirmation message I choose No option")]
        public void GivenFromConfirmationMessageIChooseNoOption()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();            
        }

        [Given(@"I press the Cancel button again")]
        public void GivenIPressTheCancelButtonAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();
        }

        [Given(@"I close the confirmation window")]
        public void GivenICloseTheConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();            
        }

        [Given(@"I press Yes button from confirmation screen")]
        public void GivenIPressYesButtonFromConfirmationScreen()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
            cancelButton.Click();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();            
        }

        [Given(@"I select the assembly(.*) from main tree view")]
        public void GivenISelectTheAssemblyFromMainTreeView(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectCancelUpdateA");
            projectNode.ExpandEx();
            projectNode.SelectEx();
            TreeNode assembly1Node = projectNode.GetNode("assembly2");
            assembly1Node.SelectEx();
        }

        [Given(@"I clear the value for Name field and I fill the field Version")]
        public void GivenIClearTheValueForNameFieldAndIFillTheFieldVersion()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = string.Empty;

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123456";
        }

        [Given(@"I select the assembly(.*) node")]
        public void GivenISelectTheAssemblyNode(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectCancelUpdateA");
            projectNode.ExpandEx();
            projectNode.SelectEx();
            TreeNode assembly1Node = projectNode.GetNode("assembly1");
            assembly1Node.SelectEx();
        }

        [Given(@"I press No button from confirmation window")]
        public void GivenIPressNoButtonFromConfirmationWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();            
        }

        [Given(@"I change the value of assembly(.*)'s Version field")]
        public void GivenIChangeTheValueOfAssemblySVersionField(int p0)
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox versionTextBox = mainWindow.Get<TextBox>(AssemblyAutomationIds.VersionTextBox);
            versionTextBox.Text = "123456";
        }

        [Given(@"I close the confirmation message \(x\)")]
        public void GivenICloseTheConfirmationMessageX()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectCancelUpdateA");
            projectNode.ExpandEx();
            projectNode.SelectEx();
            TreeNode assembly1Node = projectNode.GetNode("assembly2");
            assembly1Node.SelectEx();
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).Close();
        }

        [Given(@"I change the name of assembly(.*)")]
        public void GivenIChangeTheNameOfAssembly(int p0)
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "test";
        }

        [Given(@"I click the Home buttom")]
        public void GivenIClickTheHomeButtom()
        {
            Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
            homeButton.Click();
        }

        [When(@"the confirmation window is dsplayed and I press the No button")]
        public void WhenTheConfirmationWindowIsDsplayedAndIPressTheNoButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();            
        }

        [Then(@"the Home screen in opened an the Login screen is available")]
        public void ThenTheHomeScreenInOpenedAnTheLoginScreenIsAvailable()
        {
            Button skipButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
            skipButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
    }
}
