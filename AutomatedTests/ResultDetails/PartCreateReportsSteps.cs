﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class PartCreateReportsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();
        private static string partCreateReportPath;

        public static void MyClassInitialize()
        {
            PartCreateReportsSteps.partCreateReportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            if (File.Exists(PartCreateReportsSteps.partCreateReportPath))
            {
                File.Delete(PartCreateReportsSteps.partCreateReportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(PartCreateReportsSteps.partCreateReportPath))
            {
                File.Delete(PartCreateReportsSteps.partCreateReportPath);
            }
        }

        [BeforeFeature("PartCreateReports")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectPartRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("PartCreateReports")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I go to part Result Details Node")]
        public void GivenIGoToPartResultDetailsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectPartRD");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partRD");
            partNode.ExpandEx();

            TreeNode resultDetailsNode = partNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I complete the fields")]
        public void GivenICompleteTheFields()
        {
            Random random = new Random();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            developmentCostTextBox.Text = random.Next(1000).ToString();
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            transportCostTextBox.Text = random.Next(1000).ToString();
            TextBox packagingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            packagingCostTextBox.Text = random.Next(1000).ToString();
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            paymentTermsTextBox.Text = random.Next(1000).ToString();
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            projectInvestTextBox.Text = random.Next(1000).ToString();
            CheckBox calculateInternalLogisticCostCheckBox = mainWindow.Get<CheckBox>(AutomationIds.CalculateLogisticCostCheckBox);
            calculateInternalLogisticCostCheckBox.Checked = false;
            TextBox internalLocisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            internalLocisticCostTextBox.Text = random.Next(1000).ToString();
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            otherCostTextBox.Text = random.Next(1000).ToString();
        }
        
        [When(@"I click on the Create Report Button")]
        public void WhenIClickOnTheCreateReportButton()
        {
            Button createReport = mainWindow.Get<Button>(AutomationIds.CreateReportsButton);
            createReport.Click();
        }
        
        [When(@"I click on the Regular XLS Report Button")]
        public void WhenIClickOnTheRegularXLSReportButton()
        {
            Button regularReportXlsButton = mainWindow.Get<Button>(AutomationIds.RegularReportXlsButton);
            regularReportXlsButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(PartCreateReportsSteps.partCreateReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I assure that the Open Created file Button is enabled")]
        public void WhenIAssureThatTheOpenCreatedFileButtonIsEnabled()
        {
            Button openCreatedFileButton = mainWindow.Get<Button>(AutomationIds.OpenLastCreatedReportButton);
            Assert.IsTrue(true, "Chart not exported", openCreatedFileButton.Enabled);
        }
        
        [When(@"I click on  the close button")]
        public void WhenIClickOnTheCloseButton()
        {
            Button closeButton = mainWindow.Get<Button>(AutomationIds.CloseButton);
            closeButton.Click();
        }
        
        [When(@"I click on the Regular PDF Report Button")]
        public void WhenIClickOnTheRegularPDFReportButton()
        {
            Button regularReportPdfButton = mainWindow.Get<Button>(AutomationIds.RegularReportPdfButton);
            regularReportPdfButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(PartCreateReportsSteps.partCreateReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"I click on the close button")]
        public void ThenIClickOnTheCloseButton()
        {
            Button closeButton = mainWindow.Get<Button>(AutomationIds.CloseButton);
            closeButton.Click();
        }
    }
}
