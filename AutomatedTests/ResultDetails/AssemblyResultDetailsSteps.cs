﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class AssemblyResultDetailsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("AssemblyResultDetails")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("AssemblyResultDetails")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select Result Details Node")]
        public void GivenIClickOnResultDetailsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I select Per Part radio button")]
        public void GivenISelectPerPartRadioButton()
        {
            RadioButton perPartRadioButton = mainWindow.Get<RadioButton>(AutomationIds.PerPartsRadioButton);
            perPartRadioButton.Click();
        }

        [When(@"I complete all the fields")]
        public void WhenICompleteAllTheFields()
        {
            Random random = new Random();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            developmentCostTextBox.Text = random.Next(1000).ToString();
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            transportCostTextBox.Text = random.Next(1000).ToString();
            TextBox packagingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            packagingCostTextBox.Text = random.Next(1000).ToString();
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            paymentTermsTextBox.Text = random.Next(1000).ToString();
            TextBox notesTextBox = mainWindow.Get<TextBox>(AutomationIds.AdditionalCostsNotesTextBox);
            notesTextBox.Text = "Notes";
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            projectInvestTextBox.Text = random.Next(1000).ToString();
            CheckBox calculateInternalLogisticCostCheckBox = mainWindow.Get<CheckBox>(AutomationIds.CalculateLogisticCostCheckBox);
            calculateInternalLogisticCostCheckBox.Checked = false;
            TextBox internalLocisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            internalLocisticCostTextBox.Text = random.Next(1000).ToString();
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            otherCostTextBox.Text = random.Next(1000).ToString();

            //those are multiplied by 100 (10x10) because there are 10 batches per year and Annual Prouction Qty is 10
            var developmentCost = ((decimal.Parse(developmentCostTextBox.Text)) * 100);
            var transportCost = ((decimal.Parse(transportCostTextBox.Text)) * 100);
            var packagingCost = ((decimal.Parse(packagingCostTextBox.Text)) * 100);
            var paymentTerms = decimal.Parse(paymentTermsTextBox.Text);
            var projectInvest = ((decimal.Parse(projectInvestTextBox.Text)) * 100);
            var internalLocisticCost = ((decimal.Parse(internalLocisticCostTextBox.Text)) * 100);
            var otherCost = ((decimal.Parse(otherCostTextBox.Text)) * 100);

            RadioButton perPartsTotalRadioButton = mainWindow.Get<RadioButton>(AutomationIds.PerPartsTotalRadio);
            perPartsTotalRadioButton.Click();

            //I check the changes on the fields after selecting Per Parts Total radio Button 
            TextBox developmentCostTesxtBox2 = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            Assert.AreEqual(decimal.Parse(developmentCostTesxtBox2.Text), developmentCost);
            TextBox transportCostTextBox2 = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(decimal.Parse(transportCostTextBox2.Text), transportCost);
            TextBox packagingCostTextBox2 = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(decimal.Parse(packagingCostTextBox2.Text), packagingCost);
            TextBox paymentTermsTextBox2 = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            Assert.AreEqual(decimal.Parse(paymentTermsTextBox2.Text), paymentTerms);
            TextBox projectInvestTextBox2 = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            Assert.AreEqual(decimal.Parse(projectInvestTextBox2.Text), projectInvest);
            TextBox internalLocisticCostTextBox2 = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            Assert.AreEqual(decimal.Parse(internalLocisticCostTextBox2.Text), internalLocisticCost);
            TextBox otherCostTextBox2 = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            Assert.AreEqual(decimal.Parse(otherCostTextBox2.Text), otherCost);
        }
        
        [When(@"I uncheck Enhanced Breakdown CheckBox")]
        public void WhenIUncheckEnhancedBreakdownCheckBox()
        {
            CheckBox enhancedBreakdownCheckbox = mainWindow.Get<CheckBox>(AutomationIds.EnhancedBreakdownCheckbox);
            enhancedBreakdownCheckbox.Checked = false;
        }
        
        [When(@"I go through each tab")]
        public void WhenIGoThroughEachTab()
        {
           TabPage overheadAndMarginTab = mainWindow.Get<TabPage>(AutomationIds.OverheadAndMarginTab);
           overheadAndMarginTab.Click();
           TabPage assemblingTab = mainWindow.Get<TabPage>(AutomationIds.AssemblingTab);
           assemblingTab.Click();
           TabPage processStepsTab = mainWindow.Get<TabPage>(AutomationIds.ProcessStepsTab);
           processStepsTab.Click();
           TabPage commoditiesTab = mainWindow.Get<TabPage>(AutomationIds.CommoditiesTab);
           commoditiesTab.Click();
           TabPage consumablesTab = mainWindow.Get<TabPage>(AutomationIds.ConsumablesTab);
           consumablesTab.Click();
           TabPage partListTab = mainWindow.Get<TabPage>(AutomationIds.PartListBreakdownTab);
           partListTab.Click();
           TabPage billOfMaterialsTab = mainWindow.Get<TabPage>(AutomationIds.BillOfMaterialsTab);
           billOfMaterialsTab.Click();
           TabPage investmentTab = mainWindow.Get<TabPage>(AutomationIds.InvestmentTab);
           investmentTab.Click();
        }
        
        [When(@"I check Enhanced Breakdown CheckBox")]
        public void WhenICheckEnhancedBreakdownCheckBox()
        {
            CheckBox enhancedBreakdownCheckbox = mainWindow.Get<CheckBox>(AutomationIds.EnhancedBreakdownCheckbox);
            enhancedBreakdownCheckbox.Checked = true;
        }
        
        [Then(@"I go to to RawMaterials Tab")]
        public void ThenIGoToToRawMaterialsTab()
        {
            TabPage rawMaterialsTab = mainWindow.Get<TabPage>(AutomationIds.RawMaterialsBreakdownTab);
            rawMaterialsTab.Click();
        }
    }
}
