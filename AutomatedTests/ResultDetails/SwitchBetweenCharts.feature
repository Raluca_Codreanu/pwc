﻿@SwitchBetweenCharts
Feature: SwitchBetweenCharts


Scenario: Switch Between Charts
	Given I click on the Result Details node
	And I complete all the fields which influence the chart
	When I click on Switch chart button
	Then I change some ofthe fields which influence the chart
