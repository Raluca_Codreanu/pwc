﻿@ExportChart
Feature: ExportChart

Scenario: Export Chart
	Given I go to the Result Details node
	And I complete all the fields which influence the Chart
	When I click on Export Chart to PDF Button
	When I assure that Open created file Button is enabled
	Then I prees Ok button
