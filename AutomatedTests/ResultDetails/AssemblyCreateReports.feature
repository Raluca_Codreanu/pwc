﻿@AssemblyCreateReports
Feature: AssemblyCreateReports

Scenario: Assembly Create Reports
	Given I click on the Result Details Node
	And I complete some fields
	When I click on Create Report Button
	When I click on Regular XLS Report Button
	When I assure that Open Created file Button is enabled
	When I click on close button
	When I click on Create Report Button
	When I click on Regular PDF Report Button
	When I assure that Open Created file Button is enabled
	When I click on close button
	When I click on Create Report Button
	When I click on Enhanced XLS Report Button
	When I assure that Open Created file Button is enabled
	When I click on close button
	When I click on Create Report Button
	When I click on Enhanced PDF Report Button
	When I assure that Open Created file Button is enabled
	Then I click on Close Button
