﻿@PartCreateReports
Feature: PartCreateReports

Scenario: Part Create Reports
Given I go to part Result Details Node
	And I complete the fields
	When I click on the Create Report Button
	When I click on the Regular XLS Report Button
	When I assure that the Open Created file Button is enabled
	When I click on  the close button
	When I click on the Create Report Button
	When I click on the Regular PDF Report Button
	When I assure that the Open Created file Button is enabled
	Then I click on the close button
