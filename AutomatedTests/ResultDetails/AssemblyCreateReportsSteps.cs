﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class AssemblyCreateReportsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();
        private static string createReportPath;

        public static void MyClassInitialize()
        {
            AssemblyCreateReportsSteps.createReportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            if (File.Exists(AssemblyCreateReportsSteps.createReportPath))
            {
                File.Delete(AssemblyCreateReportsSteps.createReportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(AssemblyCreateReportsSteps.createReportPath))
            {
                File.Delete(AssemblyCreateReportsSteps.createReportPath);
            }
        }

        [BeforeFeature("AssemblyCreateReports")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("AssemblyCreateReports")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I click on Result Details Node")]
        public void GivenIClickOnResultDetailsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I click on the Result Details Node")]
        public void GivenIClickOnTheResultDetailsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I complete some fields")]
        public void GivenICompleteSomeFields()
        {
            Random random = new Random();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            developmentCostTextBox.Text = random.Next(1000).ToString();
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            transportCostTextBox.Text = random.Next(1000).ToString();
            TextBox packagingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            packagingCostTextBox.Text = random.Next(1000).ToString();
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            paymentTermsTextBox.Text = random.Next(1000).ToString();
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            projectInvestTextBox.Text = random.Next(1000).ToString();
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            otherCostTextBox.Text = random.Next(1000).ToString();
        }
        
        [When(@"I click on Create Report Button")]
        public void WhenIClickOnCreateReportButton()
        {
            Button createReport = mainWindow.Get<Button>(AutomationIds.CreateReportsButton);
            createReport.Click();
        }
        
        [When(@"I click on Regular XLS Report Button")]
        public void WhenIClickOnRegularXLSReportButton()
        {
            Button regularReportXlsButton = mainWindow.Get<Button>(AutomationIds.RegularReportXlsButton);
            regularReportXlsButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(AssemblyCreateReportsSteps.createReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I assure that Open Created file Button is enabled")]
        public void WhenIAssureThatOpenCreatedFileButtonIsEnabled()
        {
            Button openCreatedFileButton = mainWindow.Get<Button>(AutomationIds.OpenLastCreatedReportButton);
            Assert.IsTrue(true, "Chart not exported", openCreatedFileButton.Enabled);
        }
        
        [When(@"I click on close button")]
        public void WhenIClickOnCloseButton()
        {
            Button closeButton = mainWindow.Get<Button>(AutomationIds.CloseButton);
            closeButton.Click();
        }
        
        [When(@"I click on Regular PDF Report Button")]
        public void WhenIClickOnRegularPDFReportButton()
        {
            Button regularReportPdfButton = mainWindow.Get<Button>(AutomationIds.RegularReportPdfButton);
            regularReportPdfButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(AssemblyCreateReportsSteps.createReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I click on Enhanced XLS Report Button")]
        public void WhenIClickOnEnhancedXLSReportButton()
        {
            Button enhancedReportXlsButton = mainWindow.Get<Button>(AutomationIds.EnhancedReportXlsButton);
            enhancedReportXlsButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(AssemblyCreateReportsSteps.createReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I click on Enhanced PDF Report Button")]
        public void WhenIClickOnWnhancedPDFReportButton()
        {
            Button enhancedReportPdfButton = mainWindow.Get<Button>(AutomationIds.EnhancedReportPdfButton);
            enhancedReportPdfButton.Click();

            application.Windows.ReportsSelectionWindow.GetSaveFileDialog().SaveFile(AssemblyCreateReportsSteps.createReportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"I click on Close Button")]
        public void ThenIClickOnCloseButton()
        {
            Button closeButton = mainWindow.Get<Button>(AutomationIds.CloseButton);
            closeButton.Click();
        }
    }
}
