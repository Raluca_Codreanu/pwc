﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ExportChartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();
        private static string chartExportPath;

        public static void MyClassInitialize()
        {
            ExportChartSteps.chartExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            if (File.Exists(ExportChartSteps.chartExportPath))
            {
                File.Delete(ExportChartSteps.chartExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ExportChartSteps.chartExportPath))
            {
                File.Delete(ExportChartSteps.chartExportPath);
            }
        }

        [BeforeFeature("ExportChart")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ExportChart")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"I go to the Result Details node")]
        public void GivenIGoToTheResultDetailsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I complete all the fields which influence the Chart")]
        public void GivenICompleteAllTheFieldsWhichInfluenceTheChart()
        {
            Random random = new Random();
            TextBox developmentCostTextBox = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            developmentCostTextBox.Text = random.Next(1000).ToString();
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            transportCostTextBox.Text = random.Next(1000).ToString();
            TextBox packagingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            packagingCostTextBox.Text = random.Next(1000).ToString();
            TextBox paymentTermsTextBox = mainWindow.Get<TextBox>(AutomationIds.PaymentTermsTextBox);
            paymentTermsTextBox.Text = random.Next(1000).ToString();
            TextBox projectInvestTextBox = mainWindow.Get<TextBox>(AutomationIds.ProjectInvestTextBox);
            projectInvestTextBox.Text = random.Next(1000).ToString();
            CheckBox calculateInternalLogisticCostCheckBox = mainWindow.Get<CheckBox>(AutomationIds.CalculateLogisticCostCheckBox);
            calculateInternalLogisticCostCheckBox.Checked = false;
            TextBox internalLocisticCostTextBox = mainWindow.Get<TextBox>(AutomationIds.LogisticCostTextBox);
            internalLocisticCostTextBox.Text = random.Next(1000).ToString();
            TextBox otherCostTextBox = mainWindow.Get<TextBox>(AutomationIds.OtherCostTextBox);
            otherCostTextBox.Text = random.Next(1000).ToString();
        }
        
        [When(@"I click on Export Chart to PDF Button")]
        public void WhenIClickOnExportChartToPDFButton()
        {
            TextBox mouseArea = mainWindow.Get<TextBox>(AutomationIds.DevelopmentCostTextBox);
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X + 200, mouseArea.Bounds.Y - 200);
            Button exportChartButton = mainWindow.Get<Button>(AutomationIds.ExportChartButton);
            exportChartButton.Click();

            mainWindow.GetSaveFileDialog().SaveFile(ExportChartSteps.chartExportPath);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I assure that Open created file Button is enabled")]
        public void WhenIAssureThatOpenCreatedFileButtonIsEnabled()
        {
            Button openCreatedFileButton = mainWindow.Get<Button>(AutomationIds.OpenCreatedFileButton);
            Assert.IsTrue(true, "Chart not exported", openCreatedFileButton.Enabled );
        }
        
        [Then(@"I prees Ok button")]
        public void ThenIPreesOkButton()
        {
            Button okButton = mainWindow.Get<Button>(AutomationIds.OkButton);
        }
    }
}
