﻿@AssemblyResultDetails
Feature: AssemblyResultDetails

Scenario: Assembly Result Details
	Given I select Result Details Node
	And I select Per Part radio button
	When I complete all the fields
	When I uncheck Enhanced Breakdown CheckBox
	When I go through each tab
	When I check Enhanced Breakdown CheckBox
	Then I go to to RawMaterials Tab
