﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    public static class ConsumableAutomationIds
    {
        public const string PriceUnitBaseComboBox = "PriceUnitBaseComboBox";
        public const string PriceTextBox = "PriceTextBox";
        public const string AmountTextBox = "AmountTextBox";
        public const string WeightTextBox = "WeightTextBox";
        public const string RejectRatioTextBox = "RejectRatioTextBox";
        public const string RemarksTextBox = "RemarksTextBox";
        public const string Manufacturer = "Manufacturer";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string ProcessNode = "ProcessNode";
        public const string ConsumablesTabItem = "ConsumablesTabItem";
        public const string AddConsumableButton = "AddConsumableButton";
        public const string ConsumablesDataGrid = "ConsumablesDataGrid";
        public const string EditButton = "EditButton";
        public const string DeleteButton = "DeleteButton";
        public const string ImportConsumable = "ImportConsumable";
    }
}
