﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using System.Threading;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    public static class ConsumableTestsHelper
    {
        public static void CreateConsumable(ApplicationEx app)
        {
            Window createConsumable = app.Windows.ConsumableWindow;

            TextBox nameTextBox = createConsumable.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "consumable" + DateTime.Now.Ticks;

            ComboBox priceUnitBaseComboBox = createConsumable.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
            priceUnitBaseComboBox.SelectItem(1);

            TextBox amountTextBox = createConsumable.Get<TextBox>(ConsumableAutomationIds.AmountTextBox);
            amountTextBox.Text = "10";

            TextBox priceTextBox = createConsumable.Get<TextBox>(ConsumableAutomationIds.PriceTextBox);
            priceTextBox.Text = "10";
            TabPage manufacturerTab = createConsumable.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Focus();
            manufacturerTab.Click();

            TextBox manufacturerName = createConsumable.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            TextBox descriptionTextBox = createConsumable.Get<TextBox>(ConsumableAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
            descriptionTextBox.Text = "description of manufacturer";

            Button saveButton = createConsumable.Get<Button>(AutomationIds.SaveButton , true);
            saveButton.Click();
        }
    }
}
