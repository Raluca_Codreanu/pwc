﻿@ConsumableImportExport
Feature: ConsumableImportExport

Scenario: Import Export Consumable
	Given I select the Consumable
	And I press the Export from menu item
	And I click the  cancel 
	And I select again the Consumable to export it
	And I press save button
	And I select a step to import Consumable
	When I select the Consumable which will be imported
	Then the Consumable is imported
