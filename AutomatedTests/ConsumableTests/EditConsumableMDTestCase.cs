﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [TestClass]
    public class EditConsumableMDTestCase
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditConsumableMDTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        [TestMethod]
        [TestCategory("Consumables")]
        public void EditConsumableMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 4
                TreeNode consumableNode = app.MainScreen.ProjectsTree.MasterDataConsumables;
                Assert.IsNotNull(consumableNode, "The Consumable node was not found");
                
                //step 5
                consumableNode.Click();
                ListViewControl consumablesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(consumablesDataGrid, "Consumables data grid was not found");
                mainWindow.WaitForAsyncUITasks();
                
                //step 6
                consumablesDataGrid.Select("Name", "1consumableMD");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Edit button was not found.");
                editButton.Click();
                Window editConsumableWindow = app.Windows.ConsumableWindow;
                
                //step 7-10
                TextBox nameTextBox = editConsumableWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "1commodity_Updated";

                ComboBox priceUnitBaseComboBox = editConsumableWindow.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
                priceUnitBaseComboBox.SelectItem(3);

                TextBox amountTextBox = editConsumableWindow.Get<TextBox>(ConsumableAutomationIds.AmountTextBox);
                amountTextBox.Text = "10";

                TextBox priceTextBox = editConsumableWindow.Get<TextBox>(ConsumableAutomationIds.PriceTextBox);
                priceTextBox.Text = "10";

                TextBox description = editConsumableWindow.Get<TextBox>(ConsumableAutomationIds.DescriptionTextBox);
                description.Text = "consumables_Description";
                //step 11
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox manufacturerName = editConsumableWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox descriptionTextBox = editConsumableWindow.Get<TextBox>(ConsumableAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";
                
                Button saveButton = editConsumableWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
            }
        }

        #region Helper

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Consumable consumable = new Consumable();
            consumable.SetOwner(adminUser);
            consumable.IsMasterData = true;
            consumable.Name = "1consumableMD";
            consumable.Price = 12;
            consumable.Amount = 12;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            consumable.Manufacturer = manufacturer1;

            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();
        }
        #endregion Helper

        #region Inner classes
        #endregion Inner classes

    }
}
