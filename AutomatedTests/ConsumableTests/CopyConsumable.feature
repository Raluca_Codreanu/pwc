﻿@CopyConsumable
Feature: CopyConsumable

Scenario: Copy Consumable to another step
	Given I open the Consumable tab from step
	And I select a consumable and copy it
	When I select another step and press Paste
	Then the consumable is copied into this step

Scenario: Copy Consumable into the same step
	Given I open the Consumable tab from step
	And I select a consumable and copy it
	When I select the data grid, right-click and paste
	Then a consumable with the name containing copy of is added

Scenario: Copy Consumable to Master Data
	Given I open the Consumable tab from step
	And I select a consumable to Copy to Master Data 
	When I open the Consumable from Master Data
	Then a copy of consumable can be viewed