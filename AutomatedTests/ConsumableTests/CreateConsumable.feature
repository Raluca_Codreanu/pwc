﻿@CreateConsumable
Feature: CreateConsumable

Scenario: Create Consumable in Process Step
	Given I select the Consumable tab from process step
	And I press the Add btn
	And I fill all the mandatory fields
	When I press the Save
	Then the Consumable data grid contains a record

Scenario: Cancel Create consumable_no changes
	Given I select the Consumable tab from process step 
	And I press the Add btn
	When I press Cancel button
	Then Create Consumable window is closed

Scenario: Cancel Create Consumable 
	Given I select the Consumable tab from process step
	And I press the Add btn
	And I fill some of the Create window's fields
	And I pressCancel button
	And I press the No button in Question window
	And I pressCancel
	When I press the Yes button in Question window
	Then Create Consumable window is closed