﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [Binding]
    public class DeleteConsumableProcessSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteConsumable")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteConsumable")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Consumable tab")]
        public void GivenIOpenTheConsumableTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I select a consumable")]
        public void GivenISelectAConsumable()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesDataGrid.Select("Name", "consumableDelete1");
        }
        
        [Given(@"I press the Delete button from data grid")]
        public void GivenIPressTheDeleteButtonFromDataGrid()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesDataGrid.Select("Name", "consumableDelete1");
            Button deleteButton = mainWindow.Get<Button>(ConsumableAutomationIds.DeleteButton);
            deleteButton.Click();            
        }
        
        [Given(@"I press No in question")]
        public void GivenIPressNoInQuestion()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press Yes in question")]
        public void GivenIPressYesInQuestion()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I open the Trash Bin")]
        public void GivenIOpenTheTrashBin()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;            
            trashBinNode.Click();
        }
        
        [Given(@"I select the deleted row")]
        public void GivenISelectTheDeletedRow()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            ListViewRow deletedRow = trashBinDataGrid.Row(AutomationIds.ColumnName, "consumableDelete1");

            ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, deletedRow);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;
        }
        
        [Given(@"I press the View Details button")]
        public void GivenIPressTheViewDetailsButton()
        {
            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();
        }
        
        [Given(@"I press the Recover Button")]
        public void GivenIPressTheRecoverButton()
        {
            Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            Assert.IsNotNull(recoverButton, "Recover button was not found.");
            recoverButton.Click();
        }
        
        [Given(@"I press the Yes Button")]
        public void GivenIPressTheYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish
        }
        
        [Given(@"I select a consumable, press right click to delete it")]
        public void GivenISelectAConsumablePressRightClickToDeleteIt()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesDataGrid.Select("Name", "consumableDelete2");

            ListViewRow row = consumablesDataGrid.Row("Name", "consumableDelete2");

            Menu deleteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }
        
        [Given(@"I press Empty Trash Bin")]
        public void GivenIPressEmptyTrashBin()
        {
            Button emptyTrashBin = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
            emptyTrashBin.Click();
        }
        
        [When(@"I opened the Consumable tab")]
        public void WhenIOpenedTheConsumableTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [When(@"I press the Yes")]
        public void WhenIPressTheYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the data grid contains the deleted consumable")]
        public void ThenTheDataGridContainsTheDeletedConsumable()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            ListViewRow row = consumablesDataGrid.Row("Name", "consumableDelete1");

            Assert.IsNotNull(row, "row exists");
        }
        
        [Then(@"the consumable is deleted from trash bin")]
        public void ThenTheConsumableIsDeletedFromTrashBin()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

            Assert.IsNull(trashBinDataGrid.Row("Name", "consumableDelete1"), "empty trash");
        }
    }
}
