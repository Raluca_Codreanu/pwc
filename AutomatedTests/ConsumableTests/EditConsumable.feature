﻿@EditConsumable
Feature: EditConsumable

Scenario: Edit consumable in Process step
	Given I select the Consumables tab
	And I select a row and press the Edit button
	And I change some fields' values
	When I press the save
	Then the edit Consumable window is closed

Scenario: Cancel Edit_no changes
	Given I select the Consumables tab
	And I select a row and press the Edit button
	When I press the Cancel
	Then the edit Consumable window is closed

Scenario: Cancel Edit Consumable
	Given I select the Consumables tab
	And I select a row and press the Edit button
	And I change some fields' values
	And I press  Cancel
	And I press the No button in window message
	And I press  Cancel
	When I press the yes button
	Then the edit Consumable window is closed	

Scenario: Edit consumable using dbl click
	Given I select the Consumables tab
	And I select a row and press double click
	And I change some the price
	When I press the save
	Then the edit Consumable window is closed