﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [TestClass]
    public class DeleteConsumableMDTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteConsumableMDTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        [TestMethod]
        [TestCategory("Consumables")]
        public void DeleteConsumableMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 3
                TreeNode consumableNode = app.MainScreen.ProjectsTree.MasterDataConsumables;
                Assert.IsNotNull(consumableNode, "The Commodities node was not found.");

                //step 4
                consumableNode.Click();
                ListViewControl consumablesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(consumablesDataGrid, "Commodities data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                consumablesDataGrid.Select("Name", "1consumableMD_Delete");

                //step 6
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 7
                mainWindow.GetMessageDialog().ClickNo();

                //step 8
                deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                //step 9
                mainWindow.GetMessageDialog().ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                //step 10      

                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                ListViewControl trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "1consumableMD_Delete"), "A consumable from Master Data was deleted permanently and it doesn't appear on the Trash Bin.");
            }
        }

        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Consumable consumable = new Consumable();
            consumable.SetOwner(adminUser);
            consumable.IsMasterData = true;
            consumable.Name = "1consumableMD_Delete";
            consumable.Price = 12;
            consumable.Amount = 12;

            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            consumable.Manufacturer = manufacturer1;

            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();
        }
        #endregion Helper

        #region Inner classes
        #endregion Inner classes
    }
}