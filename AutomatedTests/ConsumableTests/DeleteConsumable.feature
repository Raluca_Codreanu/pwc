﻿@DeleteConsumable
Feature: Delete Consumable Process

Scenario: Delete Consumable
	Given I open the Consumable tab
	And I select a consumable 
	And I press the Delete button from data grid
	And I press No in question 
	And I press the Delete button from data grid
	And I press Yes in question
	And I open the Trash Bin
	And I select the deleted row
	And I press the View Details button
	And I press the Recover Button
	And I press the Yes Button 
	When I opened the Consumable tab 
	Then the data grid contains the deleted consumable

Scenario: Delete Consumable context menu
	Given I open the Consumable tab
	And I select a consumable, press right click to delete it
	And I press No in question 
	And I select a consumable, press right click to delete it
	And I press Yes in question
	And I open the Trash Bin
	And I press Empty Trash Bin
	When I press the Yes
	Then the consumable is deleted from trash bin