﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [Binding]
    public class EditConsumableSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("EditConsumable")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("EditConsumable")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Consumables tab")]
        public void GivenISelectTheConsumablesTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I select a row and press the Edit button")]
        public void GivenISelectARowAndPressTheEditButton()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesDataGrid.Select("Name", "2");

            Button editButton = mainWindow.Get<Button>("EditButton");
            editButton.Click();
        }
        
        [Given(@"I change some fields' values")]
        public void GivenIChangeSomeFieldsValues()
        {
            Window editConsumableWindow = application.Windows.ConsumableWindow;

            TextBox nameTextBox = editConsumableWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "1commodity_Updated";

            ComboBox priceUnitBaseComboBox = editConsumableWindow.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
            priceUnitBaseComboBox.Select(2);
            editConsumableWindow.WaitForAsyncUITasks();
        }

        [Given(@"I press  Cancel")]
        public void GivenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click(); 
        }
        
        [Given(@"I press the No button in window message")]
        public void GivenIPressTheNoButtonInWindowMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }


        [Given(@"I select a row and press double click")]
        public void GivenISelectARowAndPressDoubleClick()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesDataGrid.Select("Name", "3");

            ListViewRow row = consumablesDataGrid.Row("Name", "3");
            row.DoubleClick();

            Window editConsumableWindow = application.Windows.ConsumableWindow;

            TextBox nameTextBox = editConsumableWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "3_Updated";

            ComboBox priceUnitBaseComboBox = editConsumableWindow.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
            priceUnitBaseComboBox.Select(2);
            editConsumableWindow.WaitForAsyncUITasks();
        }

        [Given(@"I change some the price")]
        public void GivenIChangeSomeThePrice()
        {
            Window editConsumableWindow = application.Windows.ConsumableWindow;

            TextBox nameTextBox = editConsumableWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "1commodity_Updated";

            ComboBox priceUnitBaseComboBox = editConsumableWindow.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
            priceUnitBaseComboBox.Select(2);
            editConsumableWindow.WaitForAsyncUITasks();
        }

        [When(@"I press the save")]
        public void WhenIPressTheSave()
        {
            //save the changes from consumables
            Window editConsumableWindow = application.Windows.ConsumableWindow;
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();

            //save the process step changes
            Button saveButtonProcess = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButtonProcess.Click();
        }

        [When(@"I press the Cancel")]
        public void WhenIPressTheCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [When(@"I press the yes button")]
        public void WhenIPressTheYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes(); 
        }

        [Then(@"the edit Consumable window is closed")]
        public void ThenTheEditConsumableWindowIsClosed()
        {
            Window editConsumableWindow = application.Windows.ConsumableWindow;
            Assert.IsNull(editConsumableWindow, "Edit consumable window should be closed");
        }
    }
}






