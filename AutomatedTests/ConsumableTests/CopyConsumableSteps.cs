﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [Binding]
    public class CopyConsumableSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyConsumable")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyConsumable")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Consumable tab from step")]
        public void GivenIOpenTheConsumableTabFromStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I select a consumable and copy it")]
        public void GivenISelectAConsumableAndCopyIt()
        {
            ListViewControl consumablessDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablessDataGrid.Select("Name", "2");

            ListViewRow row = consumablessDataGrid.Row("Name", "2");

            Menu copyMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
        
        [Given(@"I select a consumable to Copy to Master Data")]
        public void GivenISelectAConsumableToCopyToMasterData()
        {
            ListViewControl consumablessDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablessDataGrid.Select("Name", "2");

            ListViewRow row = consumablessDataGrid.Row("Name", "2");

            Menu copyToMDMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMDMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [When(@"I select another step and press Paste")]
        public void WhenISelectAnotherStepAndPressPaste()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();

            Menu pasteMenuItem = step1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I select the data grid, right-click and paste")]
        public void WhenISelectTheDataGridRight_ClickAndPaste()
        {
            ListViewControl consumablessDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablessDataGrid.Select("Name", "2");

            ListViewRow row = consumablessDataGrid.Row("Name", "2");

            Menu pasteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I open the Consumable from Master Data")]
        public void WhenIOpenTheConsumableFromMasterData()
        {
            TreeNode consumablesNode = application.MainScreen.ProjectsTree.MasterDataConsumables;
            Assert.IsNotNull(consumablesNode, "The Dies node was not found.");
            consumablesNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the consumable is copied into this step")]
        public void ThenTheConsumableIsCopiedIntoThisStep()
        {
            ListViewControl consumablesNode = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablesNode.Select("Name", "2");    
        }
        
        [Then(@"a consumable with the name containing copy of is added")]
        public void ThenAConsumableWithTheNameContainingCopyOfIsAdded()
        {
            ListViewControl consumablessDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumablessDataGrid.Select("Name", copyTranslation + "2");
        }
        
        [Then(@"a copy of consumable can be viewed")]
        public void ThenACopyOfConsumableCanBeViewed()
        {
            ListViewControl consumablessDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(consumablessDataGrid, "Manage Master Data grid was not found.");
            consumablessDataGrid.Select("Name", "2");     
        }
    }
}
