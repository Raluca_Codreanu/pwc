﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ConsumableTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CreateConsumableSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CreateConsumable")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CreateConsumable")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Consumable tab from process step")]
        public void GivenISelectTheConsumableTabFromProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I press the Add btn")]
        public void GivenIPressTheAddBtn()
        {
            Button addConsumableButton = mainWindow.Get<Button>(ConsumableAutomationIds.AddConsumableButton, true);
            addConsumableButton.Click();            
        }
        
        [Given(@"I fill all the mandatory fields")]
        public void GivenIFillAllTheMandatoryFields()
        {
            ConsumableTestsHelper.CreateConsumable(application);    
        }
        
        [Given(@"I fill some of the Create window's fields")]
        public void GivenIFillSomeOfTheCreateWindowSFields()
        {
            Window createConsumable = application.Windows.ConsumableWindow;
            TextBox nameTextBox = createConsumable.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "consumable" + DateTime.Now.Ticks;

            ComboBox priceUnitBaseComboBox = createConsumable.Get<ComboBox>(ConsumableAutomationIds.PriceUnitBaseComboBox);
            priceUnitBaseComboBox.Select(1);

            TextBox amountTextBox = createConsumable.Get<TextBox>(ConsumableAutomationIds.AmountTextBox);
            amountTextBox.Text = "10";    
        }

        [Given(@"I pressCancel button")]
        public void GivenIPressCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();  
        }

        [Given(@"I press the No button in Question window")]
        public void GivenIPressTheNoButtonInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();    
        }

        [Given(@"I pressCancel")]
        public void GivenIPressCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();   
        }
        
        [When(@"I press the Save")]
        public void WhenIPressTheSave()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [When(@"I press Cancel button")]
        public void WhenIPressCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I press the Yes button in Question window")]
        public void WhenIPressTheYesButtonInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Then(@"the Consumable data grid contains a record")]
        public void ThenTheConsumableDataGridContainsARecord()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            Assert.IsTrue(consumablesDataGrid.Rows.Capacity >= 2, "Test");                  
        }
        
        [Then(@"Create Consumable window is closed")]
        public void ThenCreateConsumableWindowIsClosed()
        {
            Window createConsumableWindow = application.Windows.ConsumableWindow;
            Assert.IsNull(createConsumableWindow, "Create consumable window should be closed");
        }
    }
}




