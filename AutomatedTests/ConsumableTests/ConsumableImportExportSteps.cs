﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.ConsumableTests
{
    [Binding]
    public class ConsumableImportExportSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string consumableExportPath;

        public ConsumableImportExportSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ConsumableImportExportSteps.consumableExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".consumable");
            if (File.Exists(ConsumableImportExportSteps.consumableExportPath))
            {
                File.Delete(ConsumableImportExportSteps.consumableExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ConsumableImportExportSteps.consumableExportPath))
            {
                File.Delete(ConsumableImportExportSteps.consumableExportPath);
            }
        }

        [BeforeFeature("ConsumableImportExport")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");

            importProjectMenuItem.Click();
            System.Threading.Thread.Sleep(200);

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ConsumableImportExport")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I select the Consumable")]
        public void GivenISelectTheConsumable()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step1");
            step2Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I press the Export from menu item")]
        public void GivenIPressTheExportFromMenuItem()
        {
            ListViewControl consumableDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumableDataGrid.Select("Name", "consumableIE");

            ListViewRow row = consumableDataGrid.Row("Name", "consumableIE");

            Menu exportMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I click the  cancel")]
        public void GivenIClickTheCancel()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I select again the Consumable to export it")]
        public void GivenISelectAgainTheConsumableToExportIt()
        {
            ListViewControl consumableDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumableDataGrid.Select("Name", "consumableIE");

            ListViewRow row = consumableDataGrid.Row("Name", "consumableIE");

            Menu exportMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press save button")]
        public void GivenIPressSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ConsumableImportExportSteps.consumableExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ConsumableImportExportSteps.consumableExportPath), "The consumable was not exported at the specified location.");
        }

        [Given(@"I select a step to import Consumable")]
        public void GivenISelectAStepToImportConsumable()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            Menu importConsumableMenuItem = step1Node.GetContextMenuById(mainWindow, AutomationIds.Import, ConsumableAutomationIds.ImportConsumable);
            Assert.IsTrue(importConsumableMenuItem.Enabled, "Import -> Consumable menu item is disabled.");
            importConsumableMenuItem.Click();
        }

        [When(@"I select the Consumable which will be imported")]
        public void WhenISelectTheConsumableWhichWillBeImported()
        {
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\consumableIE.consumable"));
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Then(@"the Consumable is imported")]
        public void ThenTheConsumableIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(ConsumableAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();
            TabPage consumablesTab = mainWindow.Get<TabPage>(ConsumableAutomationIds.ConsumablesTabItem);
            consumablesTab.Click();
            ListViewControl consumableDataGrid = Wait.For(() => mainWindow.GetListView(ConsumableAutomationIds.ConsumablesDataGrid));
            consumableDataGrid.Select("Name", "consumableIE");
        }
    }
}
