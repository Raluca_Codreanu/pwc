﻿@MultipleRoutes
Feature: MultipleRoutes
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers


Scenario: Multiple Routes
	Given I go to the result details node of an assembly
	And I click on Transport Cost Calculator Button
	When I click on Add Calculation Route Button
	When I complete the fields for the route
	When I click on the Done Button
	When I assure that Transport Cost and Packaging Cost Text boxes are not editable
	When I click on Transport Cost Calculator Button
	When I click on Add Calculation Route Button
	When I complete the fields for the route
	When I go to summary tab
	When I verify that both Routes are displayed on data grid
	When I click on the Done Button
	When I assure that Transport Cost text box and Packaging Cost text box are not editable
	Then I assure that Transport Cost and Packaging Cost Text boxes values are changed