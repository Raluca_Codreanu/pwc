﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class TransportCostCalculatorSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("TransportCostCalculator")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("TransportCostCalculator")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I go to result details node of an assembly")]
        public void GivenIGoToResultDetailsNodeOfAnAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I click on Transport cost calculator Button")]
        public void GivenIClickOnTransportCostCalculatorButton()
        {
            Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
            transportCostCalculatorButton.Click();
        }
        
        [When(@"I click on Add calculation route button")]
        public void WhenIClickOnAddCalculationRouteButton()
        {
            Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            addCalculationRouteButton.Click();
        }
        
        [When(@"I complete the fields for Route No one")]
        public void WhenICompleteTheFieldsForRouteNo()
        {

            //ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(CommodityAutomationIds.CommoditiesDataGrid));
            //commoditiesDataGrid.Select("Name", "commodity1");

            Random random = new Random();
            TextBox partNameTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            partNameTextBox.Text = "partAssemblyRD";
            TextBox partNumberTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNumberTextBox);
            partNumberTextBox.Text = random.Next(1000).ToString();
            TextBox partWeightTextBox = mainWindow.Get<TextBox>(AutomationIds.PartWeightTextBox);
            partWeightTextBox.Text = random.Next(1000).ToString();
            TextBox supplierTextBox = mainWindow.Get<TextBox>(AutomationIds.SupplierTextBox);
            supplierTextBox.Text = "Supplier";

            TextBox mouseZonePartInformation = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseZonePartInformation.Bounds.X - 10, mouseZonePartInformation.Bounds.Y - 10);
            mouse.Click();


            var sourceExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderSource);
            sourceExpander.Click();
            TextBox sourceLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            sourceLocationTextBox.Text = "Source Location";
            TextBox sourceCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceCountrytextBox);
            sourceCountryTextBox.Text = "Source Country";
            TextBox sourceZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceZipCodeTextBox);
            sourceZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox sourceLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLatitudeTextBox);
            sourceLatitudeTextBox.Text = "-90";
            TextBox sourceLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLongitudeTextBox);
            sourceLongitudeTextBox.Text = "-180";

            TextBox mouseZoneSource = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneSource.Bounds.X - 10, mouseZoneSource.Bounds.Y - 10);
            mouse.Click();

            var destinationExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderDestination);
            destinationExpander.Click();
            TextBox destinationLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            destinationLocationTextBox.Text = "Destination Location";
            TextBox destinationCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationCountrytextBox);
            destinationCountryTextBox.Text = "Destination Country";
            TextBox destinationZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationZipCodeTextBox);
            destinationZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox destinationLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLatitudeTextBox);
            destinationLatitudeTextBox.Text = "90";
            TextBox destinationLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLongitudeTextBox);
            destinationLongitudeTextBox.Text = "180";

            TextBox mouseZoneDestination = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneDestination.Bounds.X - 10, mouseZoneDestination.Bounds.Y - 10);
            mouse.Click();

            TextBox distanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DistanceTextBox);
            distanceTextBox.Text = random.Next(1000).ToString();

           // mainWindow.ScrollBars.Vertical.ScrollDown();

            var packingDetailsExpander = mainWindow.Get < GroupBox>(AutomationIds.ExpanderPackingDetails);
            packingDetailsExpander.Click();
            TextBox dailyNeededQuantityTextBox = mainWindow.Get<TextBox>(AutomationIds.DailyNeededQuantityTextBox);
            dailyNeededQuantityTextBox.Text = random.Next(1000).ToString();
            ComboBox partTypeComboBox = mainWindow.Get<ComboBox>(AutomationIds.PartTypeComboBox);
            partTypeComboBox.Select(1); // item(1) = Bulk Goods
            TextBox quantityPerPackTextBox = mainWindow.Get<TextBox>(AutomationIds.QuantityPerPackTextBox);
            quantityPerPackTextBox.Text = random.Next(1000).ToString();
            ComboBox typeOfPackagingCombobox = mainWindow.Get<ComboBox>(AutomationIds.TypeOfPackagingCombobox);
            typeOfPackagingCombobox.Select(0); // item(0) = Individual Packing
            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "Description";
            TextBox costByPackageTextBox = mainWindow.Get<TextBox>(AutomationIds.CostByPackageTextBox);
            costByPackageTextBox.Text = random.Next(1000).ToString();
            TextBox packageWeightTextBox = mainWindow.Get<TextBox>(AutomationIds.PackageWeightTextBox);
            packageWeightTextBox.Text = random.Next(1000).ToString();

            TextBox mouseZonePackingDetails = mainWindow.Get<TextBox>(AutomationIds.DailyNeededQuantityTextBox);
            mouse.Location = new System.Windows.Point(mouseZonePackingDetails.Bounds.X - 10, mouseZonePackingDetails.Bounds.Y - 10);
            mouse.Click();

            var bouncingBoxExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderBouncingBox);
            bouncingBoxExpander.Click();
            TextBox bouncingBoxLengthTextBox = mainWindow.Get<TextBox>(AutomationIds.BouncingBoxLengthTextBox);
            bouncingBoxLengthTextBox.Text = random.Next(1000).ToString();
            TextBox bouncingBoxWidthTextBox = mainWindow.Get<TextBox>(AutomationIds.BouncingBoxWidthTextBox);
            bouncingBoxWidthTextBox.Text = random.Next(1000).ToString();
            TextBox bouncingBoxHeightTextBox = mainWindow.Get<TextBox>(AutomationIds.BouncingBoxHeightTextBox);
            bouncingBoxHeightTextBox.Text = random.Next(1000).ToString();
            TextBox packingDensityTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingDensityTextBox);
            packingDensityTextBox.Text = random.Next(100).ToString();


            TextBox mouseZoneBouncingBox = mainWindow.Get<TextBox>(AutomationIds.BouncingBoxLengthTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneBouncingBox.Bounds.X - 10, mouseZoneBouncingBox.Bounds.Y - 10);
            mouse.Click();

            var carrierPackagingSizeExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderCarrierPackagingSize);
            carrierPackagingSizeExpander.Click();
            TextBox carrierLengthTextBox = mainWindow.Get<TextBox>(AutomationIds.CarrierLengthTextBox);
            carrierLengthTextBox.Text = random.Next(1000).ToString();
            TextBox carrierWidthTextBox = mainWindow.Get<TextBox>(AutomationIds.CarrierWidthTextBox);
            carrierWidthTextBox.Text = random.Next(1000).ToString();
            TextBox carrierHeightTextBox = mainWindow.Get<TextBox>(AutomationIds.CarrierHeightTextBox);
            carrierHeightTextBox.Text = random.Next(1000).ToString();
            TextBox netWeightTxt = mainWindow.Get<TextBox>(AutomationIds.NetWeightTxt);
            netWeightTxt.Text = random.Next(1000).ToString();

            TextBox mouseZonePackingSize = mainWindow.Get<TextBox>(AutomationIds.CarrierLengthTextBox);
            mouse.Location = new System.Windows.Point(mouseZonePackingSize.Bounds.X - 10, mouseZonePackingSize.Bounds.Y - 45);
            mouse.Click();

            var routeExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderRoute);
            routeExpander.Click();
            ComboBox transporterTypeComboBox = mainWindow.Get<ComboBox>(AutomationIds.TransporterTypeComboBox);
            transporterTypeComboBox.Select(0);
            TextBox costPerKmTextBox = mainWindow.Get<TextBox>(AutomationIds.CostPerKmTextBox);
            costPerKmTextBox.Text = random.Next(1000).ToString();
            TextBox routeLengthTextBox = mainWindow.Get<TextBox>(AutomationIds.RouteLengthTextBox);
            routeLengthTextBox.Text = random.Next(1000).ToString();
            TextBox routeWidthTextBox = mainWindow.Get<TextBox>(AutomationIds.RouteWidthTextBox);
            routeWidthTextBox.Text = random.Next(1000).ToString();
            TextBox routeHeightTextBox = mainWindow.Get<TextBox>(AutomationIds.RouteHeightTextBox);
            routeHeightTextBox.Text = random.Next(1000).ToString();
            
            Button settingsButton = mainWindow.Get<Button>(AutomationIds.OpenSettingsPopupButton);
            settingsButton.Click();

            TextBox daysPerYearTextBox = mainWindow.Get<TextBox>(AutomationIds.DaysPerYearTextBox);
            daysPerYearTextBox.Text = random.Next(366).ToString();
            TextBox averageLoadTargetTextBox = mainWindow.Get<TextBox>(AutomationIds.AverageLoadTargetTextBox);
            averageLoadTargetTextBox.Text = random.Next(1000).ToString();
            TextBox truckTrailerTextBox = mainWindow.Get<TextBox>(AutomationIds.TruckTrailerTextBox);
            truckTrailerTextBox.Text = random.Next(1000).ToString();
            TextBox megaTrailerTextBox = mainWindow.Get<TextBox>(AutomationIds.MegaTrailerTextBox);
            megaTrailerTextBox.Text = random.Next(1000).ToString();
            TextBox truck7tTextBox = mainWindow.Get<TextBox>(AutomationIds.Truck7tTextBox);
            truck7tTextBox.Text = random.Next(1000).ToString();
            TextBox truck3_5tTextBox = mainWindow.Get<TextBox>(AutomationIds.Truck3_5tTextBox);
            truck3_5tTextBox.Text = random.Next(1000).ToString();
            TextBox vanTypeTextBox = mainWindow.Get<TextBox>(AutomationIds.VanTypeTextBox);
            vanTypeTextBox.Text = random.Next(1000).ToString();

            TextBox mouseArea = mainWindow.Get<TextBox>(AutomationIds.CostPerKmTextBox);
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X + 10, mouseArea.Bounds.Y +10);
            mouse.Click();

            TextBox mouseZoneRoute = mainWindow.Get<TextBox>(AutomationIds.CostPerKmTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneRoute.Bounds.X - 10, mouseZoneRoute.Bounds.Y - 45);
            mouse.Click();

            TextBox maxPackageCarrierTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxPackageCarrierTextBox);
            maxPackageCarrierTextBox.Text = random.Next(100).ToString();
            TextBox maxLoadCapacityTextBox = mainWindow.Get<TextBox>(AutomationIds.MaxLoadCapacityTextBox);
            maxLoadCapacityTextBox.Text = "1000000000";
        }
        
        [When(@"I click on Done Button")]
        public void WhenIClickOnDoneButton()
        {
            Button doneButon = mainWindow.Get<Button>(AutomationIds.DoneButton);
            doneButon.Click();
        }
        
        [Then(@"I assure that Transport Cost and Packaging Cost Text boxes are not editable")]
        public void ThenIAssureThatTransportCostAndPackagingCostTextBoxesAreNotEditable()
        {
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCostTextBox.Enabled, false);
            TextBox packingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packingCostTextBox.Enabled, false);
        }
    }
}
