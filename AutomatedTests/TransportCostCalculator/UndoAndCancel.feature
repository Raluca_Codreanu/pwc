﻿@UndoAndCancel
Feature: UndoAndCancel

Scenario: Undo Command
	Given I click on result detalis node of an assembly
	And I click on cost calculator button
	When I click on add calculation route button
	When I click on Summary Tab
	When I verify if route is present in Calculation Data Grid
	When I click on Undo Button
	When I verify if the route is not present on Calculation Data Grid
	When I click on add calculation route button
	When I complete a field
	When I click on Undo Button
	When I verify if the field value has been undone
	When I coplete some fields
	When I click on Summary Tab
	When I select the route
	When I click on Delete Route Button
	When I click on No Button
	When I click on Delete Route Button
	When I click on Yes Button
	When I verify that The route was deleted from Data Grid	
	When I click on Undo Button
	When I click on Summary Tab
	Then I verify that the Route reappeared in Calculation Data Grid

Scenario: Cancel Command
	Given I click on result detalis node of an assembly
	And I click on cost calculator button
	When I click on Cancel Button
	When I click again on cost calculator button
	When I click on add calculation route button
	When I coplete some fields
	When I click on Cancel Button
	When I click on No Button
	When I click on Summary Tab
	When I select the route
	When I click on Delete Route Button
	When I click on Yes Button
	When I click on Cancel Button
	When I click on Yes Button
	Then I verify Transport Cost and Packaging Cost Text fields