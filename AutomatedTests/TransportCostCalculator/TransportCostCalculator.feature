﻿@TransportCostCalculator
Feature: TransportCostCalculator

Scenario: Transport Cost Calculator
	Given I go to result details node of an assembly
	And I click on Transport cost calculator Button
	When I click on Add calculation route button
	When I complete the fields for Route No one
	When I click on Done Button
	Then I assure that Transport Cost and Packaging Cost Text boxes are not editable
