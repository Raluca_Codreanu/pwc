﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class UndoAndCancelSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("UndoAndCancel")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("UndoAndCancel")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I click on result detalis node of an assembly")]
        public void GivenIClickOnResultDetalisNodeOfAnAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }
        
        [Given(@"I click on cost calculator button")]
        public void GivenIClickOnCostCalculatorButton()
        {
            Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
            transportCostCalculatorButton.Click();
        }
        
        [When(@"I click on add calculation route button")]
        public void WhenIClickOnAddCalculationRouteButton()
        {
            Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            addCalculationRouteButton.Click();
        }
        
        [When(@"I click on Summary Tab")]
        public void WhenIClickOnSummaryTab()
        {
            Tab mouseArea = mainWindow.Get<Tab>("MainTab");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X + 20, mouseArea.Bounds.Y + 15);
            mouse.Click();
        }
        
        [When(@"I verify if route is present in Calculation Data Grid")]
        public void WhenIVerifyIfRouteIsPresentInCalculationDataGrid()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.IsNotNull(calculationDataGrid.Row("PartName", "assemblyRD"));
        }
        
        [When(@"I click on Undo Button")]
        public void WhenIClickOnUndoButton()
        {
            Button undoButton = mainWindow.Get<Button>(AutomationIds.UndoButton);
            undoButton.Click();
        }
        
        [When(@"I verify if the route is not present on Calculation Data Grid")]
        public void WhenIVerifyIfTheRouteIsNotPresentOnCalculationDataGrid()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.IsNull(calculationDataGrid.Row("PartName", "assemblyRD"));
        }
        
        [When(@"I complete a field")]
        public void WhenICompleteAField()
        {
            TextBox partNumberTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNumberTextBox);
            partNumberTextBox.Text = "100";
        }
        
        [When(@"I verify if the field value has been undone")]
        public void WhenIVerifyIfTheFieldValueHasBeenUndone()
        {
            mainWindow.WaitForAsyncUITasks();
            TextBox partNumberTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNumberTextBox);
            Assert.AreEqual(partNumberTextBox.Text,"");
        }
        
        [When(@"I coplete some fields")]
        public void WhenICopleteSomeFields()
        {
            TextBox partWeightTextBox = mainWindow.Get<TextBox>(AutomationIds.PartWeightTextBox);
            partWeightTextBox.Text = "10";
            TextBox supplierTextBox = mainWindow.Get<TextBox>(AutomationIds.SupplierTextBox);
            supplierTextBox.Text = "Supplier";
        }
        
        [When(@"I select the route")]
        public void WhenISelectTheRoute()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.AreEqual(calculationDataGrid.Rows.Count(), 1);
            calculationDataGrid.Row("PartName", "assemblyRD").Select();
        }
        
        [When(@"I click on Delete Route Button")]
        public void WhenIClickOnDeleteRouteButton()
        {
            Button mouseArea = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X + 50, mouseArea.Bounds.Y);
            mouse.Click();

            
        }
        
        [When(@"I verify that The route was deleted from Data Grid")]
        public void WhenIVerifyThatTheRouteWasDeletedFromDataGrid()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.IsNull(calculationDataGrid.Row("PartName", "assemblyRD"));
        }
        
        [When(@"I click on Cancel Button")]
        public void WhenIClickOnCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I click on No Button")]
        public void WhenIClickOnNoButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [When(@"I click on Yes Button")]
        public void WhenIClickOnYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [When(@"I click again on cost calculator button")]
        public void WhenIClickAgainOnCostCalculatorButton()
        {
            Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
            transportCostCalculatorButton.Click();
        }

        [Then(@"I verify that the Route reappeared in Calculation Data Grid")]
        public void ThenIVerifyThatTheRouteReappearedInCalculationDataGrid()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.IsNotNull(calculationDataGrid.Row("PartName", "assemblyRD"));
        }

        [Then(@"I verify Transport Cost and Packaging Cost Text fields")]
        public void ThenIVerifyTransportCostAndPackagingFields()
        {
            TextBox transportCostTextBox = mainWindow.Get<TextBox>(AutomationIds.TransportCostTextBox);
            Assert.AreEqual(transportCostTextBox.Enabled, true);
            TextBox packingCostTextBox = mainWindow.Get<TextBox>(AutomationIds.PackingCostTextBox);
            Assert.AreEqual(packingCostTextBox.Enabled, true);
        }
    }
}
