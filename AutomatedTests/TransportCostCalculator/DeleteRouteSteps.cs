﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class DeleteRouteSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static Window transportCostCalculatorSummaryWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("DeleteRoute")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
            transportCostCalculatorSummaryWindow = application.Windows.TransportCostCalculatorSummaryWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteRoute")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I am on Transport Cost Calculator Window")]
        public void GivenIAmOnTransportCostCalculatorWindow()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();

            Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
            transportCostCalculatorButton.Click();
        }
        
        [Given(@"I Add a route")]
        public void GivenIAddARoute()
        {
            Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            addCalculationRouteButton.Click();

            Random random = new Random();
            TextBox partNameTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            partNameTextBox.Text = "partAssemblyRD1";
            TextBox partNumberTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNumberTextBox);
            partNumberTextBox.Text = random.Next(1000).ToString();
            TextBox partWeightTextBox = mainWindow.Get<TextBox>(AutomationIds.PartWeightTextBox);
            partWeightTextBox.Text = random.Next(1000).ToString();
            TextBox supplierTextBox = mainWindow.Get<TextBox>(AutomationIds.SupplierTextBox);
            supplierTextBox.Text = "Supplier";

            TextBox mouseZonePartInformation = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseZonePartInformation.Bounds.X - 10, mouseZonePartInformation.Bounds.Y - 10);
            mouse.Click();


            var sourceExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderSource);
            sourceExpander.Click();
            TextBox sourceLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            sourceLocationTextBox.Text = "Source Location";
            TextBox sourceCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceCountrytextBox);
            sourceCountryTextBox.Text = "Source Country";
            TextBox sourceZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceZipCodeTextBox);
            sourceZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox sourceLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLatitudeTextBox);
            sourceLatitudeTextBox.Text = "-90";
            TextBox sourceLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLongitudeTextBox);
            sourceLongitudeTextBox.Text = "-180";

            TextBox mouseZoneSource = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneSource.Bounds.X - 10, mouseZoneSource.Bounds.Y - 10);
            mouse.Click();

            var destinationExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderDestination);
            destinationExpander.Click();
            TextBox destinationLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            destinationLocationTextBox.Text = "Destination Location";
            TextBox destinationCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationCountrytextBox);
            destinationCountryTextBox.Text = "Destination Country";
            TextBox destinationZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationZipCodeTextBox);
            destinationZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox destinationLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLatitudeTextBox);
            destinationLatitudeTextBox.Text = "90";
            TextBox destinationLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLongitudeTextBox);
            destinationLongitudeTextBox.Text = "180";

            TextBox mouseZoneDestination = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneDestination.Bounds.X - 10, mouseZoneDestination.Bounds.Y - 10);
            mouse.Click();

            TextBox distanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DistanceTextBox);
            distanceTextBox.Text = random.Next(1000).ToString();

            var packingDetailsExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderPackingDetails);
            packingDetailsExpander.Click();
            TextBox dailyNeededQuantityTextBox = mainWindow.Get<TextBox>(AutomationIds.DailyNeededQuantityTextBox);
            dailyNeededQuantityTextBox.Text = random.Next(1000).ToString();
            ComboBox partTypeComboBox = mainWindow.Get<ComboBox>(AutomationIds.PartTypeComboBox);
            partTypeComboBox.Select(1); // item(1) = Bulk Goods
            TextBox quantityPerPackTextBox = mainWindow.Get<TextBox>(AutomationIds.QuantityPerPackTextBox);
            quantityPerPackTextBox.Text = random.Next(1000).ToString();
            ComboBox typeOfPackagingCombobox = mainWindow.Get<ComboBox>(AutomationIds.TypeOfPackagingCombobox);
            typeOfPackagingCombobox.Select(0); // item(0) = Individual Packing
            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "Description";
        }
        
        [When(@"I Add another route")]
        public void WhenIAddAnotherRoute()
        {
            Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            addCalculationRouteButton.Click();

            Random random = new Random();
            TextBox partNameTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            partNameTextBox.Text = "partAssemblyRD2";
            TextBox partNumberTextBox = mainWindow.Get<TextBox>(AutomationIds.PartNumberTextBox);
            partNumberTextBox.Text = random.Next(1000).ToString();
            TextBox partWeightTextBox = mainWindow.Get<TextBox>(AutomationIds.PartWeightTextBox);
            partWeightTextBox.Text = random.Next(1000).ToString();
            TextBox supplierTextBox = mainWindow.Get<TextBox>(AutomationIds.SupplierTextBox);
            supplierTextBox.Text = "Supplier2";

            TextBox mouseZonePartInformation = mainWindow.Get<TextBox>(AutomationIds.PartNameTextBox);
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseZonePartInformation.Bounds.X - 10, mouseZonePartInformation.Bounds.Y - 10);
            mouse.Click();


            var sourceExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderSource);
            sourceExpander.Click();
            TextBox sourceLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            sourceLocationTextBox.Text = "Source Location2";
            TextBox sourceCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceCountrytextBox);
            sourceCountryTextBox.Text = "Source Country2";
            TextBox sourceZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceZipCodeTextBox);
            sourceZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox sourceLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLatitudeTextBox);
            sourceLatitudeTextBox.Text = "-45";
            TextBox sourceLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLongitudeTextBox);
            sourceLongitudeTextBox.Text = "-130";

            TextBox mouseZoneSource = mainWindow.Get<TextBox>(AutomationIds.SourceLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneSource.Bounds.X - 10, mouseZoneSource.Bounds.Y - 10);
            mouse.Click();

            var destinationExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderDestination);
            destinationExpander.Click();
            TextBox destinationLocationTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            destinationLocationTextBox.Text = "Destination Location";
            TextBox destinationCountryTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationCountrytextBox);
            destinationCountryTextBox.Text = "Destination Country";
            TextBox destinationZipCodeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationZipCodeTextBox);
            destinationZipCodeTextBox.Text = random.Next(1000).ToString();
            TextBox destinationLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLatitudeTextBox);
            destinationLatitudeTextBox.Text = "23";
            TextBox destinationLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLongitudeTextBox);
            destinationLongitudeTextBox.Text = "150";

            TextBox mouseZoneDestination = mainWindow.Get<TextBox>(AutomationIds.DestinationLocationTextBox);
            mouse.Location = new System.Windows.Point(mouseZoneDestination.Bounds.X - 10, mouseZoneDestination.Bounds.Y - 10);
            mouse.Click();

            TextBox distanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DistanceTextBox);
            distanceTextBox.Text = random.Next(1000).ToString();

            var packingDetailsExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderPackingDetails);
            packingDetailsExpander.Click();
            TextBox dailyNeededQuantityTextBox = mainWindow.Get<TextBox>(AutomationIds.DailyNeededQuantityTextBox);
            dailyNeededQuantityTextBox.Text = random.Next(1000).ToString();
            ComboBox partTypeComboBox = mainWindow.Get<ComboBox>(AutomationIds.PartTypeComboBox);
            partTypeComboBox.Select(1); // item(1) = Bulk Goods
            TextBox quantityPerPackTextBox = mainWindow.Get<TextBox>(AutomationIds.QuantityPerPackTextBox);
            quantityPerPackTextBox.Text = random.Next(1000).ToString();
            ComboBox typeOfPackagingCombobox = mainWindow.Get<ComboBox>(AutomationIds.TypeOfPackagingCombobox);
            typeOfPackagingCombobox.Select(0); // item(0) = Individual Packing
            TextBox descriptionTextBox = mainWindow.Get<TextBox>(AutomationIds.DescriptionTextBox);
            descriptionTextBox.Text = "Description";

        }
        
        [When(@"I go to summary Tabpage")]
        public void WhenIGoToSummaryTabpage()
        {
            Tab mouseArea = mainWindow.Get<Tab>("MainTab");
            AttachedMouse mouse = mainWindow.Mouse;
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X + 20, mouseArea.Bounds.Y + 15);
            mouse.Click();
        }
        
        [When(@"I select a route to be deleted")]
        public void WhenISelectARouteToBeDeleted()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.AreEqual(calculationDataGrid.Rows.Count(), 2);
            calculationDataGrid.Row("PartName", "partAssemblyRD1").Select();

        }
        
        [When(@"I click on Delete Calculation Route Button")]
        public void WhenIClickOnDeleteCalculationRouteButton()
        {
            AttachedMouse mouse = mainWindow.Mouse;
            Button mouseArea = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            mouse.Location = new System.Windows.Point(mouseArea.Bounds.X +50, mouseArea.Bounds.Y);
            mouse.Click();
            
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"verify if the route has been deleted")]
        public void ThenVerifyIfTheRouteHasBeenDeleted()
        {
            ListViewControl calculationDataGrid = mainWindow.GetListView(AutomationIds.CalculationsDataGrid);
            Assert.AreEqual(calculationDataGrid.Rows.Count(), 1);
            Assert.IsNull(calculationDataGrid.Row("PartName", "partAssembly1"));
        }
    }
}
