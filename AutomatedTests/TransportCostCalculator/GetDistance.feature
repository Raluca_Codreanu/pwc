﻿@GetDistance
Feature: GetDistance

Scenario: Get Distance
	Given I click on result details node of an assembly
	And I click on the Transport cost calculator Button
	When I click on the Add calculation route button
	When I complete latitude and longitude fields for Source and Destination
	When I click on Get Dinstance Button
	When I assure that the map with the route is shown
	When I click on Ok Button and I check the Distance field
	When I go to preferences window
	When I uncheck ShowViewMap checkBox
	When I click on Save button
	When I go back to Transport Cost Calculator Window
	When I complete again latitude and longitude fields for Source and Destination
	When I click on The Get Dinstance Button
	Then I check the Distance text Box