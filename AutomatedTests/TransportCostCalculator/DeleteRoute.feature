﻿@DeleteRoute
Feature: DeleteRoute

Scenario: Delete Route
	Given I am on Transport Cost Calculator Window
	And I Add a route
	When I go to summary Tabpage
	When I Add another route
	When I go to summary Tabpage
	When I select a route to be deleted
	When I click on Delete Calculation Route Button
	Then verify if the route has been deleted
