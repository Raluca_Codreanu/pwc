﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class GetDistanceSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("GetDistance")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;
            CheckBox ShowMapWindowCheckBox = mainWindow.Get<CheckBox>(AutomationIds.PreferencesShowMapWindowCheckBox);
            ShowMapWindowCheckBox.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("GetDistance")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I click on result details node of an assembly")]
        public void GivenIClickOnResultDetailsNodeOfAnAssembly()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectRD");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
            assemblyNode.ExpandEx();

            TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
            resultDetailsNode.SelectEx();
        }

        [Given(@"I click on the Transport cost calculator Button")]
        public void GivenIClickOnTheTransportCostCalculatorButton()
        {
            Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
            transportCostCalculatorButton.Click();
        }
        
        [When(@"I click on the Add calculation route button")]
        public void WhenIClickOnTheAddCalculationRouteButton()
        {
            Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
            addCalculationRouteButton.Click();
        }
        
        [When(@"I complete latitude and longitude fields for Source and Destination")]
        public void WhenICompleteLatitudeAndLongitudeFieldsForSourceAndDestination()
        {
            var sourceExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderSource);
            sourceExpander.Click();

            TextBox sourceLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLatitudeTextBox);
            sourceLatitudeTextBox.Text = "11";
            TextBox sourceLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLongitudeTextBox);
            sourceLongitudeTextBox.Text = "12";

            var destinationExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderDestination);
            destinationExpander.Click();
            TextBox destinationLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLatitudeTextBox);
            destinationLatitudeTextBox.Text = "11";
            TextBox destinationLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLongitudeTextBox);
            destinationLongitudeTextBox.Text = "13";
        }
        
        [When(@"I click on Get Dinstance Button")]
        public void WhenIClickOnGetDinstanceButton()
        {
            Button getDistanceButton = mainWindow.Get<Button>(AutomationIds.GetDistanceButton);
            getDistanceButton.Click();
            mainWindow.WaitWhileBusy();
            
        }
        
        [When(@"I assure that the map with the route is shown")]
        public void WhenIAssureThatTheMapWithTheRouteIsShown()
        {
            CheckBox drivingRouteShowMapWindowCheckBox = mainWindow.Get<CheckBox>(AutomationIds.DrivingRouteShowMapWindowCheckBox);
            Assert.AreEqual(drivingRouteShowMapWindowCheckBox.Checked, true);
            
        }
        
        [When(@"I click on Ok Button and I check the Distance field")]
        public void WhenIClickOnOkButtonAndICheckTheDistanceField()
        {
            TextBox drivingRouteDistanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DrivingRouteDistanceTextBox);
            Button oKButton = mainWindow.Get<Button>(AutomationIds.DrivingRouteOkButton);
            oKButton.Click();
            TextBox distanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DistanceTextBox);
            Assert.AreEqual(drivingRouteDistanceTextBox.Text, distanceTextBox.Text);
        }

        [When(@"I go to preferences window")]
        public void WhenIGoToPreferencesWindow()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;
        }

        [When(@"I uncheck ShowViewMap checkBox")]
        public void WhenIUncheckShowViewMapCheckBox()
        {
            CheckBox ShowMapWindowCheckBox = mainWindow.Get<CheckBox>(AutomationIds.PreferencesShowMapWindowCheckBox);
            ShowMapWindowCheckBox.Checked = false;
        }

         [When(@"I click on Save button")]
        public void WhenIClickOnSaveButton()
        {
            Button savePreferencesButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
        }

         [When(@"I go back to Transport Cost Calculator Window")]
         public void WhenIGoBackToTransportCostCalculatorWindow()
         {
             TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
             myProjectsNode.ExpandEx();
             TreeNode projectNode = myProjectsNode.GetNode("projectRD");
             projectNode.ExpandEx();

             TreeNode assemblyNode = projectNode.GetNode("assemblyRD");
             assemblyNode.ExpandEx();

             TreeNode resultDetailsNode = assemblyNode.GetNodeByAutomationId(AutomationIds.resultDetailsNode);
             resultDetailsNode.SelectEx();

             Button transportCostCalculatorButton = mainWindow.Get<Button>(AutomationIds.TransportCostCalculatorButton);
             transportCostCalculatorButton.Click();

             Button addCalculationRouteButton = mainWindow.Get<Button>(AutomationIds.AddCalculationRouteButton);
             addCalculationRouteButton.Click();
         }

        [When(@"I complete again latitude and longitude fields for Source and Destination")]
         public void WhenICompleteAgainLatitudeAndLongitudeFieldsForSourceAndDestination()
         {
             var sourceExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderSource);
             sourceExpander.Click();

             TextBox sourceLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLatitudeTextBox);
             sourceLatitudeTextBox.Text = "41";
             TextBox sourceLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.SourceLongitudeTextBox);
             sourceLongitudeTextBox.Text = "41";

             var destinationExpander = mainWindow.Get<GroupBox>(AutomationIds.ExpanderDestination);
             destinationExpander.Click();
             TextBox destinationLatitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLatitudeTextBox);
             destinationLatitudeTextBox.Text = "41";
             TextBox destinationLongitudeTextBox = mainWindow.Get<TextBox>(AutomationIds.DestinationLongitudeTextBox);
             destinationLongitudeTextBox.Text = "42";
         }

        [When(@"I click on The Get Dinstance Button")]
        public void WhenIClickOnTheGetDinstanceButton()
         {
             Button getDistanceButton = mainWindow.Get<Button>(AutomationIds.GetDistanceButton);
             getDistanceButton.Click();
             mainWindow.WaitForAsyncUITasks();
         }

        [Then(@"I check the Distance text Box")]
        public void ThenICheckTheDistanceTextBox()
         {
             TextBox distanceTextBox = mainWindow.Get<TextBox>(AutomationIds.DistanceTextBox);
             Assert.IsNotNull(distanceTextBox.Text);
             Assert.AreNotEqual(distanceTextBox.Text, "0");
         }
    }
}