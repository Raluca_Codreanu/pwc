﻿@ImportExportFolder
Feature: ImportExportProjectFolder

Scenario: Import Export Project Folder
	Given I press right-click on My Projects node to choose Export option
	And I press Cancel in Save As screen
	And I press right-click and choose Export option again
	And I press the Save
	When I select the My Projects node to import the exported folder
	Then the folder is imported
