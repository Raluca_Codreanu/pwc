﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    [Binding]
    public class ImportExportProjectFolderSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string folderExportPath;

        public ImportExportProjectFolderSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportProjectFolderSteps.folderExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".folder");
            if (File.Exists(ImportExportProjectFolderSteps.folderExportPath))
            {
                File.Delete(ImportExportProjectFolderSteps.folderExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportProjectFolderSteps.folderExportPath))
            {
                File.Delete(ImportExportProjectFolderSteps.folderExportPath);
            }
        }

        [BeforeFeature("ImportExportFolder")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProjectFolder);        
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\folder1.folder"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportFolder")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I press right-click on My Projects node to choose Export option")]
        public void GivenIPressRight_ClickOnMyProjectsNodeToChooseExportOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();
            
            TreeNode folderNode = myProjects.GetNode("folder1");
            folderNode.SelectEx();

            Menu exportMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press Cancel in Save As screen")]
        public void GivenIPressCancelInSaveAsScreen()
        {
            mainWindow.GetSaveFileDialog().Cancel();            
        }
        
        [Given(@"I press right-click and choose Export option again")]
        public void GivenIPressRight_ClickAndChooseExportOptionAgain()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("folder1");
            folderNode.SelectEx();
            folderNode.Collapse();

            Menu exportMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press the Save")]
        public void GivenIPressTheSave()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportProjectFolderSteps.folderExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ImportExportProjectFolderSteps.folderExportPath), "The folder was not exported at the specified location.");
        }
        
        [When(@"I select the My Projects node to import the exported folder")]
        public void WhenISelectTheMyProjectsNodeToImportTheExportedFolder()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();
            myProjects.Collapse();

            Menu importMenuItem = myProjects.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProjectFolder);
            importMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\folder1.folder"));
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the folder is imported")]
        public void ThenTheFolderIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode folderNode = myProjectsNode.GetNode("folder1");
            folderNode.ExpandEx();
            folderNode.SelectEx();
        }
    }
}
