﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    [Binding]
    public class CutFolderSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CutFolder")]
        public static void CreateTestData()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            CreateData();
        }

        public static void CreateData()
        {     
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = "Folder1";
            folder1.SetOwner(adminUser);
            dataContext.ProjectFolderRepository.Add(folder1);
            dataContext.SaveChanges();

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = "Folder2";
            folder2.SetOwner(adminUser);
            dataContext.ProjectFolderRepository.Add(folder2);
            dataContext.SaveChanges();
        }

        [AfterFeature("CutFolder")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a folder, right click and choose Cut option")]
        public void GivenISelectAFolderRightClickAndChooseCutOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("Folder1");
            folderNode.SelectEx();

            Menu cutMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }
        
        [When(@"I select the second folder, right-click and choose Paste option")]
        public void WhenISelectTheSecondFolderRight_ClickAndChoosePasteOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("Folder2");
            folderNode.SelectEx();

            Menu pasteMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Then(@"second folder is moved into the first one")]
        public void ThenSecondFolderIsMovedIntoTheFirstOne()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("Folder2");
            folderNode.SelectEx();
            folderNode.ExpandEx();

            TreeNode folderNode2 = folderNode.GetNode("Folder1");
            folderNode2.SelectEx();
        }
    }
}
