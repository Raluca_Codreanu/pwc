﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    public static class ProjectFolderAutomationIds
    {
        public const string RecoverMenuItem = "RecoverMenuItem";
        public const string ImportMenuItem = "ImportMenuItem";
        public const string CreateFolderMenuItem = "CreateFolderMenuItem";
        public const string CreateProjectMenuItem = "CreateProjectMenuItem";
        public const string RenameFolderWindow = "Rename Folder";
        public const string ProjectNameTextBox = "NameTextBox";        
        public const string FolderNameTextBox = "FolderNameTextBox";

    }
}
