﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.ProjectTests;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.IO;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    /// <summary>
    /// A test class for Edit Project Folder test case. 
    /// </summary>
    [TestClass]
    public class EditProjectFolderTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditProjectFolderTestCase"/> class.
        /// </summary>
        public EditProjectFolderTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// The automated test of Edit Project Folder test case.
        /// </summary>
        [TestMethod]
        public void EditProjectFolderTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                // step 2                 
                Window mainWindow = app.Windows.MainWindow;

                // ensure that import summary screen is hidden                    
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = Wait.For(() => app.Windows.Preferences);

                CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
                displayImportSummaryScreen.Checked = true;

                Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(savePreferencesButton, "The Save button was not found.");

                savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePreferencesButton.Click();

                // step 4
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                app.MainMenu.Project_CreateFolder.Click();
                Window createFolderWindow = Wait.For(() => app.Windows.FolderWindow);

                TextBox folderNameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                Assert.IsNotNull(folderNameTextBox, "Folder Name text box was not found.");

                folderNameTextBox.Text = "Folder";
                Button saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "Save button was not found.");
                saveButton.Click();

                TreeNode folderNode = myProjectsNode.GetNode("Folder");
                Assert.IsNotNull(folderNode, "Folder's node was not found into MyProjects node.");

                // step 5
                Menu renameMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Rename);
                Assert.IsTrue(renameMenuItem.Enabled, "Rename menu item is disabled.");
                renameMenuItem.Click();
                Window renameFolderWindow = app.Windows.FolderWindow;

                // step 6
                folderNameTextBox = renameFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                folderNameTextBox.Text = "Folder_Updated";

                Button cancelButton = renameFolderWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");
                cancelButton.Click();

                folderNode = myProjectsNode.GetNode("Folder");
                Assert.IsNotNull(folderNode, "Folder was renamed although the rename operation has been cancelled.");

                // step 7
                renameMenuItem = folderNode.GetContextMenuById(mainWindow, "Rename");
                renameMenuItem.Click();

                renameFolderWindow = app.Windows.FolderWindow;
                folderNameTextBox = renameFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                folderNameTextBox.Text = "Folder1";

                saveButton = renameFolderWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsNotNull(saveButton, "Rename button was not found.");
                saveButton.Click();

                folderNode = myProjectsNode.GetNode("Folder1");
                Assert.IsNotNull(folderNode, "Failed to rename a Project Folder.");

                // step 8                
                Menu importProjectMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");
                importProjectMenuItem.Click();

                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\Lear.project"));
                mainWindow.WaitForAsyncUITasks();
                                
                // step 10
                folderNode.Collapse();
                mainWindow.WaitForAsyncUITasks();
                Menu createProjectMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = Wait.For(() => app.Windows.ProjectWindow);

                // step 11
                TextBox projectNameTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectNameTextBox, "Project Name text box was not found.");
                projectNameTextBox.Text = "Project";

                TabPage supplierTab = createProjectWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierNameTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                Assert.IsNotNull(supplierNameTextBox, "Supplier Name text box is null.");
                supplierNameTextBox.Text = "Supplier";

                cancelButton = createProjectWindow.Get<Button>(AutomationIds.CancelButton, true);
                Assert.IsNotNull(cancelButton, "Cancel button was not found.");
                cancelButton.Click();

                // step 12
                createProjectWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                createProjectWindow = app.Windows.ProjectWindow;
                Assert.IsNotNull(createProjectWindow, "Create Project window was closed when the Cancel -> No button was clicked.");

                // step 13
                cancelButton.Click();
                createProjectWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                // step 14
                folderNode.SelectEx();
                createProjectMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                createProjectWindow = Wait.For(() => app.Windows.ProjectWindow);

                // step 15
                projectNameTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
                projectNameTextBox.Text = "Project";

                supplierTab = createProjectWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                supplierNameTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierNameTextBox.Text = "Supplier";

                saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                Wait.For(() => folderNode.GetNode("Project") != null);

                // step 16                
                folderNode.SelectEx();
                app.MainMenu.Project_CreateProject.Click();
                createProjectWindow = Wait.For(() => app.Windows.ProjectWindow);
                cancelButton = createProjectWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                // step 17
                folderNode.SelectEx();
                app.MainMenu.Project_CreateProject.Click();
                createProjectWindow = Wait.For(() => app.Windows.ProjectWindow);

                projectNameTextBox = createProjectWindow.Get<TextBox>(ProjectFolderAutomationIds.ProjectNameTextBox);
                projectNameTextBox.Text = "Project2";

                supplierTab = createProjectWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                supplierNameTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierNameTextBox.Text = "Supplier2";

                saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
                Wait.For(() => folderNode.GetNode("Project2") != null);

                // step 18
                folderNode.Collapse();
                mainWindow.WaitForAsyncUITasks();
                Menu createFolderMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateFolder);
                createFolderMenuItem.Click();
                createFolderWindow = Wait.For(() => app.Windows.FolderWindow);

                // step 19
                folderNameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                folderNameTextBox.Text = "Folder2";

                saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                Wait.For(() => folderNode.GetNode("Folder2") != null);
                
                // step 20                
                folderNode.SelectEx();
                app.MainMenu.Project_CreateFolder.Click();                
                createFolderWindow = Wait.For(() => app.Windows.FolderWindow);

                // step 21
                folderNameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                folderNameTextBox.Text = "Folder3";

                saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                Wait.For(() => folderNode.GetNode("Folder3") != null);

                // step 22
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();
                
                // step 23
                Assert.IsNotNull(myProjectsNode.GetNode("Folder1"), "Folder1 node was not found.");
                Assert.IsNotNull(folderNode.GetNode("Folder2"), "Folder2 node was not found.");
                Assert.IsNotNull(folderNode.GetNode("Folder3"), "Folder3 node was not found.");
                Assert.IsNotNull(folderNode.GetNode("Project"), "Project node was not found.");
                Assert.IsNotNull(folderNode.GetNode("Project2"), "Project2 node was not found.");
                Assert.IsNotNull(folderNode.GetNode("Lear"), "Lear node was not found.");
            }
        }

        #region Helpers

        #endregion Helpers

        #region Inner classes

        #endregion Inner classes
    }
}
