﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    /// <summary>
    /// The automated test of Delete ProjectFolder Test Case.
    /// </summary>
    [TestClass]
    public class DeleteProjectFolderTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteProjectFolderTestCase"/> class.
        /// </summary>
        public DeleteProjectFolderTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            DeleteProjectFolderTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Delete Project Folder Test Case.
        /// </summary>
        [TestMethod]
        public void DeleteProjectFolderTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                // step 2                
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();

                TreeNode folder1Node = myProjectsNode.GetNode("Folder1");
                Assert.IsNotNull(folder1Node, "Folder1 node was not found.");
                Menu deleteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                // step 5
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 6
                deleteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                // step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                // step 8 
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");

                trashBinNode.SelectEx();
                trashBinNode.Click();

                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                ListViewRow folder1Row = trashBinDataGrid.Row("Name", "Folder1");
                Assert.IsNotNull(folder1Row, "Folder1 trash bin item was not found.");

                // step 9
                Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                Assert.IsFalse(recoverButton.Enabled, "Recover button is disabled.");

                // step 10                
                ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, folder1Row);
                
                AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                Thread.Sleep(100);

                recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                recoverButton.Click();

                // step 11
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 12
                recoverButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                // step 13
                folder1Node = myProjectsNode.GetNode("Folder1");
                folder1Node.SelectEx();

                //Delete folder1 from main menu -> Edit Menu 
                app.MainMenu.Edit_Delete.Click();
                Assert.IsTrue(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not displayed at Cancel.");

                // step 14
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 15                
                folder1Node.SelectEx();
                app.MainMenu.Edit_Delete.Click();

                // step 16
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                // step 17
                trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.SelectEx();

                trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                folder1Row = trashBinDataGrid.Row("Name", "Folder1");
                Assert.IsNotNull(folder1Row, "Folder1 trash bin item was not found.");

                // step 18 -> Workaround until the issue #1397 will be fixed(clicks on Name cell and not on the folder1 row)
                ListViewCell nameCell = trashBinDataGrid.Cell("Name", folder1Row);
                var recoverMenuItem = nameCell.GetContextMenuById(mainWindow, ProjectFolderAutomationIds.RecoverMenuItem);
                recoverMenuItem.Click();

                // step 19
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 20
                recoverMenuItem = nameCell.GetContextMenuById(mainWindow, ProjectFolderAutomationIds.RecoverMenuItem);
                recoverMenuItem.Click();

                // step 21
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();
                folder1Node = Wait.For(() => myProjectsNode.GetNode("Folder1"));

                // step 22                
                folder1Node.SelectEx();
                app.MainMenu.Edit_Delete.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                TreeNode folder2Node = myProjectsNode.GetNode("Folder2");
                Assert.IsNotNull(folder2Node, "Folder2 node was not found.");
                folder2Node.SelectEx();

                deleteMenuItem = folder2Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                // step 23
                trashBinNode.SelectEx();

                Button permanentlyDeleteButton = Wait.For(() => mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton, true));
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsFalse(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

                // step 24
                trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                ListViewRow folder2Row = trashBinDataGrid.Row("Name", "Folder2");
                Assert.IsNotNull(folder2Row, "Folder2's row was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, folder2Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                Thread.Sleep(100);

                permanentlyDeleteButton.Click();
                Assert.IsTrue(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message at Permanently Delete.");

                // step 25
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                Assert.IsNotNull(trashBinDataGrid.Row("Name", "Folder2"), "Folder2's row was removed after the permanently delete operation was cancelled.");

                // step 26
                permanentlyDeleteButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                // step 27
                Button emptyTrashButton = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
                Assert.IsNotNull(emptyTrashButton, "Empty Trash button was not found.");

                emptyTrashButton.Click();
                Assert.IsTrue(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Failed to display a confirmation message after the Empty Trash button was clicked.");

                // step 28
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                Assert.IsTrue(trashBinDataGrid.Rows.Count > 0, "The Trash Bin was emptied although the Empty Trash operation was cancelled.");

                // step 29
                emptyTrashButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();
                Assert.IsTrue(trashBinDataGrid.Rows.Count == 0, "Failed to remove all trash bin items.");

                // step 30
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                // step 31
                trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                trashBinNode.SelectEx();

                trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsTrue(trashBinDataGrid.Rows.Count == 0, "Trash Bin is not empty.");
            }
        }

        #region Helper

        /// <summary>
        /// Creates the data for this test -> Two project folders "Folder1" and "Folder2".
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = "Folder1";
            folder1.SetOwner(adminUser);
            dataContext.ProjectFolderRepository.Add(folder1);
            dataContext.SaveChanges();

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = "Folder2";
            folder2.SetOwner(adminUser);
            dataContext.ProjectFolderRepository.Add(folder2);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
