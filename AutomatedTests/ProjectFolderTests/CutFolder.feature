﻿@CutFolder
Feature: CutFolder

Scenario: Cut Folder Paste In Folder
	Given I select a folder, right click and choose Cut option	
	When I select the second folder, right-click and choose Paste option
	Then second folder is moved into the first one