﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using System.Threading;
using White.Core.InputDevices;
using White.Core.UIItems.Finders;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.ProjectFolderTests
{
    /// <summary>
    /// A test class for Create Project Folder test case. 
    /// </summary>
    [TestClass]
    public class CreateProjectFolderTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateProjectFolderTestCase"/> class.
        /// </summary>
        public CreateProjectFolderTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// The automated test of Create Project Folder test case.
        /// </summary>
        [TestMethod]
        public void CreateProjectFolderTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                // step 2                 
                Window mainWindow = app.Windows.MainWindow;

                // step 4
                // ensure that import summary screen is hidden                                    
                app.MainMenu.Options_Preferences.Click();
                Window preferencesWindow = app.Windows.Preferences;

                CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
                displayImportSummaryScreen.Checked = true;

                Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

                savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
                savePreferencesButton.Click();
                Wait.For(() => app.Windows.Preferences == null);

                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createFolderMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateFolder);
                createFolderMenuItem.Click();
                Window createFolderWindow = app.Windows.FolderWindow;
                Assert.IsNotNull(createFolderWindow, "Create Folder window was not found.");

                Button saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                Assert.IsFalse(saveButton.Enabled, "The Save button should be enabled if Folder Name field is empty.");

                // step 6
                TextBox nameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
                nameTextBox.Text = "Folder";

                Button cancelButton = createFolderWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click();
                Assert.IsNull(app.Windows.FolderWindow, "Failed to close Create Folder window after the Cancel button was clicked.");

                // step 7                
                createFolderMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateFolder);
                createFolderMenuItem.Click();

                createFolderWindow = Wait.For(() => app.Windows.FolderWindow);
                nameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                nameTextBox.Text = "Folder1";

                saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                TreeNode folder1Node = myProjectsNode.GetNode("Folder1");
                Assert.IsNotNull(folder1Node, "The created folder's node was not found.");
                Assert.IsTrue(folder1Node.IsSelected, "Failed to select the newly created folder.");

                // step 8
                myProjectsNode.SelectEx();

                Menu projectMenuItem = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
                Assert.IsNotNull(projectMenuItem, "The Project menu item was not found.");
                projectMenuItem.Click();
                foreach (Menu item in projectMenuItem.ChildMenus)
                {
                    if (item.Name == ProjectFolderAutomationIds.CreateProjectMenuItem ||
                        item.Name == ProjectFolderAutomationIds.CreateFolderMenuItem)
                    {
                        Assert.IsTrue(item.Enabled, string.Format("Project -> {0} menu item is disabled if My Projects node is selected in the Main Tree View.", item.Name));
                    }
                    else if (item.Name == ProjectFolderAutomationIds.ImportMenuItem)
                    {
                        Assert.IsTrue(item.Enabled, string.Format("Project -> {0} menu item is disabled if My Projects node is selected in the Main Tree View.", item.Name));
                        item.Click();

                        foreach (Menu childItem in item.ChildMenus)
                        {
                            if (childItem.Name == "Project")
                            {
                                Assert.IsNotNull(childItem, "Import -> Project menu item was not found.");
                                Assert.IsTrue(childItem.Enabled, "Import -> Project menu item is disabled if My Projects node is selected in the Main Tree View.");
                            }
                            else
                            {
                                // Assert.IsFalse(childItem.Enabled, string.Format("Import -> {0} menu item is enabled if My Projects node is selected in the Main Tree View.", item.Name));
                            }
                        }
                    }
                    else
                    {
                        //Assert.IsFalse(item.Enabled, string.Format("{0} menu item is enabled if My Projects node is selected in the Main Tree View.", item.Name));
                    }
                }

                // step 9
                myProjectsNode.SelectEx();
                app.MainMenu.Project_CreateFolder.Click();
                createFolderWindow = app.Windows.FolderWindow;
                Assert.IsNotNull(createFolderWindow, "Create Folder window was not found.");

                // step 10
                nameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                nameTextBox.Text = "Folder";

                cancelButton = createFolderWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                createFolderWindow = app.Windows.FolderWindow;
                Assert.IsNull(createFolderWindow, "Failed to close Create Folder window at Cancel.");

                // step 11
                myProjectsNode.SelectEx();
                app.MainMenu.Project_CreateFolder.Click();

                createFolderWindow = app.Windows.FolderWindow;
                nameTextBox = createFolderWindow.Get<TextBox>(ProjectFolderAutomationIds.FolderNameTextBox);
                nameTextBox.Text = "Folder2";

                saveButton = createFolderWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                TreeNode folder2Node = myProjectsNode.GetNode("Folder2");
                folder2Node.SelectEx();
                Assert.IsNotNull(folder2Node, "The created folder's node was not found.");
                Assert.IsTrue(folder2Node.IsSelected, "Failed to select the newly created folder.");

                // step 12                
                createFolderMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateFolder);
                Assert.IsTrue(createFolderMenuItem.Enabled, "Create -> Folder menu item is disabled for Folder node.");

                Menu createProjectMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                Assert.IsTrue(createProjectMenuItem.Enabled, "Create -> Project menu item is disabled for Folder node.");

                Menu renameMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Rename);
                Assert.IsTrue(renameMenuItem.Enabled, "Rename menu item is disabled.");

                Menu cutMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Cut);
                Assert.IsTrue(cutMenuItem.Enabled, "Cut menu item is disabled.");

                Menu pasteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
                Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");

                Menu deleteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                Assert.IsTrue(deleteMenuItem.Enabled, "Delete menu item is disabled.");

                Menu importProjectMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportProject);
                Assert.IsTrue(importProjectMenuItem.Enabled, "Import -> Project menu item is disabled.");

                folder1Node = myProjectsNode.GetNode("Folder1");
                folder1Node.SelectEx();

                // step 13
                cutMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Cut);
                cutMenuItem.Click();

                // step 14
                folder2Node.Click();
                folder2Node.RightClickAt(folder2Node.ClickablePoint);

                foreach (Menu item in mainWindow.Popup.Items)
                {
                    Assert.IsTrue(item.Enabled, string.Format("{0} menu item is disabled.", item.Name));

                    foreach (Menu childMenu in item.ChildMenus)
                    {
                        Assert.IsTrue(childMenu.Enabled, string.Format("{0} menu item is disabled.", item.Name));
                    }
                }

                // step 15
                folder2Node = myProjectsNode.GetNode("Folder2");
                folder2Node.SelectEx();
                mainWindow.WaitForAsyncUITasks();
                pasteMenuItem = folder2Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
                pasteMenuItem.Click();
                mainWindow.WaitForAsyncUITasks();

                folder2Node = myProjectsNode.GetNode("Folder2");
                folder2Node.SelectEx();
                folder2Node.ExpandEx();

                folder1Node = folder2Node.GetNode("Folder1");
                Assert.IsNotNull(folder1Node, "Failed to add the Folder1 node as a child node of Folder2.");

                // step 16
                cutMenuItem = folder2Node.GetContextMenuById(mainWindow, AutomationIds.Cut);
                cutMenuItem.Click();

                folder2Node.ExpandEx();
                pasteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
                pasteMenuItem.Click();
                mainWindow.WaitForAsyncUITasks();

                mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 17
                folder2Node.SelectEx();
                app.MainMenu.Edit_Cut.Click();

                folder1Node.SelectEx();

                app.MainMenu.Edit_Paste.Click();
                mainWindow.WaitForAsyncUITasks();
                mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 18
                cutMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Cut);
                cutMenuItem.Click();

                // step 19
                myProjectsNode.SelectEx();

                app.MainMenu.Edit_Paste.Click();
                mainWindow.WaitForAsyncUITasks();

                // step 20
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                // step 21
                myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                folder1Node = myProjectsNode.GetNode("Folder1");
                folder2Node = myProjectsNode.GetNode("Folder2");

                Assert.IsNotNull(folder1Node, "The Folder1 node was not found.");
                Assert.IsNotNull(folder2Node, "The Folder2 node was not found.");
            }
        }
        #region Helper
        #endregion Helper
    }
}
