
-- assemblies
delete from assemblies where IsMasterData='True'

-- parts
delete from parts where IsMasterData='True' and IsRawPart='False'

-- raw parts
delete from parts where IsMasterData='True' and IsRawPart='True'

-- machines
delete from machines where IsMasterData='True'

-- rawmaterials
delete from rawmaterials where IsMasterData='True'

-- consumables
delete from consumables where IsMasterData='True'

-- commodities
delete from commodities where IsMasterData='True'

-- dies
delete from dies where IsMasterData='True'

-- manufacturers
delete from manufacturers where IsMasterData='True'

-- suppliers
delete from customers where IsMasterData='True'
