﻿@ChangePassword
Feature: ChangePassword
	I want to be able to change the password

Scenario: Change the password
	Given I go to options from main menu and select Change Password option
	And I fill all the fields with value 123
	And I press the Change password button
	And I press the OK button in the error screen
	And I fill the Current password with admin, New password with 123, Confirm new password with 1234
	And I press Change password button
	And I press the Ok button from error message screen
	And I fill the Current password with admin, the New password with 123, the Confirm new password with 123
	And I press the Change button
	And I press the OK button
	And I Logout
	When I login with the credentials: username - admin, password - 123
	Then the login performed
