﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CancelChangePasswordSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CancelChangePassword")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CancelChangePassword")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I go to Options -> Change Password")]
        public void GivenIGoToOptions_ChangePassword()
        {
            application.MainMenu.Options_ChangePassword.Click();
        }

        [Given(@"I press the Cancel button on Change Password window")]
        public void GivenIPressTheCancelButtonOnChangePasswordWindow()
        {
            Button cancelButton = application.Windows.ChangePasswordWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

        [Given(@"I fill al fields and I press the Cancel button")]
        public void GivenIFillAlFieldsAndIPressTheCancelButton()
        {
            var changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "aaa";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "aaa";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "aaa";

            Button cancelButton = changePassWindow.Get<Button>("CancelButton");
            cancelButton.Click();
        }

        [Given(@"I press the No button from confirmation screen")]
        public void GivenIPressTheNoButtonFromConfirmationScreen()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I press the Cancel")]
        public void GivenIPressTheCancel()
        {
            GivenIPressTheCancelButtonOnChangePasswordWindow();
        }

        [Given(@"I close the Change Password window \(X\)")]
        public void GivenICloseTheChangePasswordWindowX()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }

        [Given(@"I press the Yes button")]
        public void GivenIPressTheYesButton()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Given(@"I reopen the Change Password window")]
        public void GivenIReopenTheChangePasswordWindow()
        {
            application.MainMenu.Options_ChangePassword.Click();
            Assert.IsNotNull(application.Windows.ChangePasswordWindow);
        }

        [Given(@"I fill all the fields")]
        public void GivenIFillAllTheFields()
        {
            Window changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "aaa";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "aaa";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "aaa";

            changePassWindow.Close();
        }

        [Given(@"I press the No button")]
        public void GivenIPressTheNoButton()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }

        [Given(@"I close the window")]
        public void GivenICloseTheWindow()
        {
            application.Windows.ChangePasswordWindow.Close();
        }

        [When(@"I press the Yes button")]
        public void WhenIPressTheYesButton()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Then(@"the change password window is closed")]
        public void ThenTheChangePasswordWindowIsClosed()
        {
            Assert.IsNull(application.Windows.ChangePasswordWindow, "The Change Password window should be closed");
        }
    }
}
