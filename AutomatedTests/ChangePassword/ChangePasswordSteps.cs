﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ChangePasswordSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("ChangePassword")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("ChangePassword")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I go to options from main menu and select Change Password option")]
        public void GivenIGoToOptionsFromMainMenuAndSelectChangePasswordOption()
        {
            application.MainMenu.Options_ChangePassword.Click();
            Assert.IsNotNull(application.Windows.ChangePasswordWindow);
        }

        [Given(@"I fill all the fields with value (.*)")]
        public void GivenIFillAllTheFieldsWithValue(int p0)
        {
            var changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "123";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "123";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "123";
        }

        [Given(@"I press the Change password button")]
        public void GivenIPressTheChangePasswordButton()
        {
            Button changeButton = application.Windows.ChangePasswordWindow.Get<Button>("ChangeButton");
            changeButton.Click();
        }

        [Given(@"I press the OK button in the error screen")]
        public void GivenIPressTheOKButtonInTheErrorScreen()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
        }

        [Given(@"I fill the Current password with admin, New password with (.*), Confirm new password with (.*)")]
        public void GivenIFillTheCurrentPasswordWithAdminNewPasswordWithConfirmNewPasswordWith(int p0, int p1)
        {
            var changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "admin";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "123";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "1234";
        }

        [Given(@"I press Change password button")]
        public void GivenIPressChangePasswordButton()
        {
            Button changeButton = application.Windows.ChangePasswordWindow.Get<Button>("ChangeButton");
            changeButton.Click();
        }

        [Given(@"I press the Ok button from error message screen")]
        public void GivenIPressTheOkButtonFromErrorMessageScreen()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
        }

        [Given(@"I fill the Current password with admin, the New password with (.*), the Confirm new password with (.*)")]
        public void GivenIFillTheCurrentPasswordWithAdminTheNewPasswordWithTheConfirmNewPasswordWith(int p0, int p1)
        {
            var changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "admin";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "123";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "123";
        }


        [Given(@"I press the Change button")]
        public void GivenIPressTheChangeButton()
        {
            Button changeButton = application.Windows.ChangePasswordWindow.Get<Button>("ChangeButton");
            changeButton.Click();
        }

        [Given(@"I press the OK button")]
        public void GivenIPressTheOKButton()
        {
            application.Windows.ChangePasswordWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
        }

        [Given(@"I Logout")]
        public void GivenILogout()
        {
            application.MainMenu.System_Logout.Click();
        }

        [When(@"I login with the credentials: username - admin, password - (.*)")]
        public void WhenILoginWithTheCredentialsUsername_AdminPassword_(int p0)
        {
            application.LoginAndGoToMainScreen("admin", "123");
        }

        [Then(@"the login performed")]
        public void ThenTheLoginPerformed()
        {
            Assert.IsNotNull(application.MainScreen);

            application.MainMenu.Options_ChangePassword.Click();
            var changePassWindow = application.Windows.ChangePasswordWindow;

            TextBox oldPasswordTextBox = changePassWindow.Get<TextBox>("OldPassword");
            oldPasswordTextBox.Text = "123";

            TextBox newPasswordTextBox = changePassWindow.Get<TextBox>("NewPassword");
            newPasswordTextBox.Text = "admin";

            TextBox confirmNewPasswordTextBpx = changePassWindow.Get<TextBox>("ConfirmNewPassword");
            confirmNewPasswordTextBpx.Text = "admin";

            Button changeButton = changePassWindow.Get<Button>("ChangeButton");
            changeButton.Click();

            changePassWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
        }
    }
}
