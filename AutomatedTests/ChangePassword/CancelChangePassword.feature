﻿@CancelChangePassword
Feature: CancelChangePassword

Scenario: Cancel Change Password
	Given I go to Options -> Change Password
	And I press the Cancel button on Change Password window
	And I go to Options -> Change Password
	And I fill al fields and I press the Cancel button
	And I press the No button from confirmation screen
	And I press the Cancel
	And I close the Change Password window (X)
	And I press the Cancel
	And I press the Yes button
	And I reopen the Change Password window
	And I fill all the fields
	And I press the No button
	And I close the window 
	When I press the Yes button
	Then the change password window is closed
