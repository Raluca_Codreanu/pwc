﻿@ImportExportRawPart
Feature: ImportExportRawPart

Scenario: Import Export Raw Part
	Given I select the Materials node to import a Raw Part
	And I select the Raw Part to be imported
	And The Raw Part is imported
	And I select the Raw Part to be exported
	And I choose the Export Option
	And I press the Cancel button from Save As Window
	And I select the Export Option again
	When I press Save Button
	Then Raw part is exported
