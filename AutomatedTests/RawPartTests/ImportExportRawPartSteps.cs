﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using System.IO;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ImportExportRawPartSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string rawPartExportPath;

        public ImportExportRawPartSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportRawPartSteps.rawPartExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".rawpart");
            if (File.Exists(ImportExportRawPartSteps.rawPartExportPath))
            {
                File.Delete(ImportExportRawPartSteps.rawPartExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportRawPartSteps.rawPartExportPath))
            {
                File.Delete(ImportExportRawPartSteps.rawPartExportPath);
            }
        }

        [BeforeFeature("ImportExportRawPart")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectIEMaterial.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportRawPart")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }
        [Given(@"I select the Materials node to import a Raw Part")]
        public void GivenISelectTheMaterialsNodeToImportARawPart()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            Menu importPartMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportRawMaterial);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem.Click();        
        }
        
        [Given(@"I select the Raw Part to be imported")]
        public void GivenISelectTheRawPartToBeImported()
        {
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\IERawPart.rawpart"));
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"The Raw Part is imported")]
        public void GivenTheRawPartIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();

            var importedPartNode = materialsNode.GetNode("RawPart");
            Assert.IsNotNull(importedPartNode, "The imported part did not appear in projects tree.");

            // try to import more than one raw part           
            myProjectsNode.ExpandEx();
           
            projectNode.ExpandEx();
           
            partNode.ExpandEx();
            partNode.SelectEx();
          
            materialsNode.SelectEx();

            Menu importPartMenuItem = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportRawMaterial);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\IERawPart.rawpart"));
            mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
            mainWindow.WaitForAsyncUITasks();

            // delete existing raw part
            myProjectsNode.ExpandEx();

            projectNode.ExpandEx();

            partNode.ExpandEx();
            partNode.SelectEx();

            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            importedPartNode.SelectEx();

            Menu deleteMenuItem = importedPartNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
            Wait.For(() => !importedPartNode.IsAvailable()); // Wait for the deleted rawPartNode tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
            trashBinNode.Click();
            mainWindow.WaitForAsyncUITasks();

            ListViewControl trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

            ListViewRow rawPartDeleted = trashBinDataGrid.Row("Name", "RawPart");
            Assert.IsNotNull(rawPartDeleted, "Raw Part trash bin item was not found.");
            ListViewCell selectedCell = trashBinDataGrid.Cell("Selected", rawPartDeleted);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;

            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();

            Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
            Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
            Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");
            permanentlyDeleteButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
          

            //Import a rawpart again 
            myProjectsNode.ExpandEx();
            
            projectNode.ExpandEx();

            partNode.ExpandEx();
            partNode.SelectEx();
            
            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            Menu importPartMenuItem2 = materialsNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportRawMaterial);
            Assert.IsTrue(importPartMenuItem2.Enabled, "Import -> Part menu item is disabled.");
            importPartMenuItem2.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\IERawPart.rawpart"));
            mainWindow.WaitForAsyncUITasks();
       
            myProjectsNode.ExpandEx();
            
            projectNode.ExpandEx();

            partNode.ExpandEx();
            partNode.SelectEx();
            
            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            var importedPartNode2 = materialsNode.GetNode("RawPart");
            Assert.IsNotNull(importedPartNode2, "The imported part did not appear in projects tree.");
        }
        
        [Given(@"I select the Raw Part to be exported")]
        public void GivenISelectTheRawPartToBeExported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectIEMaterial");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partIEMaterial");
            partNode.ExpandEx();
            partNode.SelectEx();

            TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
            materialsNode.SelectEx();
            materialsNode.ExpandEx();

            TreeNode ASAmaterialNode = materialsNode.GetNode("RawPart");
            ASAmaterialNode.SelectEx();
        }
        
        [Given(@"I choose the Export Option")]
        public void GivenIChooseTheExportOption()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            var exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }
        
        [Given(@"I press the Cancel button from Save As Window")]
        public void GivenIPressTheCancelButtonFromSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I select the Export Option again")]
        public void GivenISelectTheExportOptionAgain()
        {
            Menu projectMenu = mainWindow.Get<Menu>(SearchCriteria.ByAutomationId(AutomationIds.ProjectMenuItem));
            projectMenu.Click();
            Menu exportMenu = projectMenu.SubMenu(SearchCriteria.ByAutomationId(AutomationIds.ExportMenuItem));
            exportMenu.Click();
        }
        
        [When(@"I press Save Button")]
        public void WhenIPressSaveButton()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportRawPartSteps.rawPartExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            
        }
        
        [Then(@"Raw part is exported")]
        public void ThenRawPartIsExported()
        {
            Assert.IsTrue(File.Exists(ImportExportRawPartSteps.rawPartExportPath), "The rawPart was not exported at the specified location.");
        }
    }
}
