﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using System.Threading;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.RawPartTests
{
    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion Additional test attributes
    [TestClass]

    public class EditRawPart
    {
        [TestMethod]
        [TestCategory("RawPart")]
        public void EditRawPartTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 2
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(PartAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "projectEditedRawPart";

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);                
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(PartAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplierRawPart";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                //step 3                
                myProjectsNode.Expand();
                TreeNode project1Node = myProjectsNode.GetNode("projectEditedRawPart");
                project1Node.SelectEx();

                //step 4                
                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectEditedRawPart");
                project1Node.Expand();
                Assert.IsNotNull(project1Node, "projectDeleteRawPart node was not found.");

                //step 5
                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.SelectEx();
                partNode.Expand();

                //step 6
                TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                //step 7                
                Menu createRawPartMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateRawPart);
                createRawPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.RawPartWindow);

                //step 8
                project1Node = myProjectsNode.GetNode("projectEditedRawPart");
                Assert.IsNotNull(project1Node, "projectDeleteRawPart node was not found.");
                project1Node.ExpandEx();

                partNode = project1Node.GetNode("simplePartProject");
                partNode.ExpandEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.ExpandEx();

                TreeNode rawPartNode = materialsNode.GetNode("simplePartProject");
                rawPartNode.SelectEx();

                //step 11
                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();
                
                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = string.Empty;
                
                //step 14 -15
                TextBox numberTextBox = mainWindow.Get<TextBox>(PartAutomationIds.NumberTextBox);
                numberTextBox.Text = string.Empty;

                TextBox versionTextBox = mainWindow.Get<TextBox>(PartAutomationIds.VersionTextBox);
                versionTextBox.Text = string.Empty;

                TextBox descriptionTextBox = mainWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = string.Empty;

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = string.Empty;

                TextBox manufacturerTextBox = mainWindow.Get<TextBox>(PartAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(manufacturerTextBox, "Description text box was not found.");
                manufacturerTextBox.Text = string.Empty;

                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Focus();
                settingsTab.Click();

                TextBox purchasePriceTextBox = mainWindow.Get<TextBox>(PartAutomationIds.PurchasePriceTextBox);
                purchasePriceTextBox.Text = string.Empty;

                TextBox targetPriceTextBox = mainWindow.Get<TextBox>(PartAutomationIds.TargetPriceTextBox);
                targetPriceTextBox.Text = string.Empty;
                //step 12
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                //step 13
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                
                //step 16
                generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();
                
                name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "editedPart";

                manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);                
                manufacturerTab.Click();

                manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "editedRawPart_manufacturer";

                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();                
            }
        }
    }
}

