﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.MenuItems;
using System.Threading;


namespace ZPKTool.AutomatedTests.RawPartTests
{
    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion Additional test attributes
    [TestClass]

    public class DeleteRawPartTestCase
    {
        [TestMethod]
        [TestCategory("RawPart")]
        public void DeleteRawPartTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 2
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(PartAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "projectDeleteRawPart";

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);                
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(PartAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplierRawPart";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                //step 3                
                myProjectsNode.Expand();

                TreeNode project1Node = myProjectsNode.GetNode("projectDeleteRawPart");
                project1Node.SelectEx();

                //step 4                
                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);
                Thread.Sleep(2000);
                project1Node = myProjectsNode.GetNode("projectDeleteRawPart");
                Assert.IsNotNull(project1Node, "projectDeleteRawPart node was not found.");
                project1Node.ExpandEx();

                //step 5                
                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.ExpandEx();

                //step 6
                TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                //step 7                
                Menu createRawPartMenuItem = partNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateRawPart);
                createRawPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.RawPartWindow);

                //step 8
                project1Node = myProjectsNode.GetNode("projectDeleteRawPart");
                project1Node.ExpandEx();
                Assert.IsNotNull(project1Node, "projectDeleteRawPart node was not found.");

                partNode = project1Node.GetNode("simplePartProject");
                partNode.ExpandEx();

                materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.ExpandEx();

                TreeNode rawPartNode = materialsNode.GetNode("simplePartProject");
                rawPartNode.SelectEx();

                //step 10
                Menu deleteMenuItem = rawPartNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                //step  11
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step  12
                rawPartNode = materialsNode.GetNode("simplePartProject");
                rawPartNode.SelectEx();
                deleteMenuItem = rawPartNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                //step 13
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !rawPartNode.IsAvailable()); // Wait for the deleted rawPartNode tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                //step 14
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.Click();
                mainWindow.WaitForAsyncUITasks();

                //step 15
                ListViewControl trashBinDataGrid = Wait.For(() => mainWindow.GetListView(AutomationIds.TrashBinDataGrid));
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                ListViewRow rawPartDeleted = trashBinDataGrid.Row("Name", "simplePartProject");
                Assert.IsNotNull(rawPartDeleted, "Raw Part trash bin item was not found.");
                ListViewCell selectedCell = trashBinDataGrid.Cell("Selected", rawPartDeleted);
                AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;
                
                Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
                viewDetailsButton.Click();

                //step 22
                Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");
                
                //step 23
                permanentlyDeleteButton.Click();

                //step 24
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 25
                permanentlyDeleteButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }
    }
}
