﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.RawPartTests
{
    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion Additional test attributes
    [TestClass]
    public class CreateRawPartMDTestCase
    {
        /// <summary>
        /// Tested Create Raw Part functionality (id = 395)
        /// </summary>
        [TestMethod]
        [TestCategory("RawPart")]
        public void CreateRawPartTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                TreeNode rawPartsNode = app.MainScreen.ProjectsTree.MasterDataRawParts;
                Assert.IsNotNull(rawPartsNode, "The Raw Parts node was not found.");

                rawPartsNode.Click();
                ListViewControl partsDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(partsDataGrid, "Parts data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                //step 2
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.RawPartWindow);
            }
        }

        /// <summary>
        /// Tested Cancel Create Raw Part functionality (id = 393)
        /// </summary>
        [TestMethod]
        [TestCategory("RawPart")]
        public void CancelCreateRawPartTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 2
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                TextBox projectName = createProjectWindow.Get<TextBox>(PartAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "projectRawPart";

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);                
                supplierTab.Click();
                
                TextBox supplierName = createProjectWindow.Get<TextBox>(PartAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplierRawPart";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                myProjectsNode.ExpandEx();

                TreeNode project1Node = myProjectsNode.GetNode("projectRawPart");
                project1Node.SelectEx();

                //step 3                
                Menu createPartMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreatePart);
                createPartMenuItem.Click();
                PartTestsHelper.CreateSimplePart(app, app.Windows.PartWindow);

                project1Node = myProjectsNode.GetNode("projectRawPart");
                Assert.IsNotNull(project1Node, "projectPart node was not found.");
                project1Node.ExpandEx();

                //step 4
                TreeNode partNode = project1Node.GetNode("simplePartProject");
                partNode.ExpandEx();

                //step 5
                TreeNode materialsNode = partNode.GetNodeByAutomationId(AutomationIds.MaterialsNode);
                materialsNode.SelectEx();

                //step 6        

                Button addRawPartButton = mainWindow.Get<Button>(AutomationIds.AddRawPartButton, true);
                addRawPartButton.Click();

                var createRawPartWindow = app.Windows.RawPartWindow;
                TextBox nameTexBox = createRawPartWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTexBox.Text = "rawPart";

                //step 7
                Button cancelButton = createRawPartWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                createRawPartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 8
                cancelButton = createRawPartWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                createRawPartWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                Button addRawPart = mainWindow.Get<Button>("addRawPart");
                addRawPart.Click();
            }
        }
    }
}
