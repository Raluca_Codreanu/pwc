﻿@BasicSettings
Feature: Basic Settings
	In order to provide new basic settings to the users
	I want to be able to edit the basic settings and save or cancel the changes

# comment
Scenario: Save valid basic settings
	Given I am in the basic settings management screen
	And I enter the following values for basic settings: 
		| DeprPeriod | DeprRate | PartBatch | ShiftsPerWeek | HoursPerShift | ProdDays | ProdWeeks | AssetRate | LogisticCostRatio |
		| 1          | 2        | 3         | 4             | 5             | 6        | 7         | 8         | 9                 |
	When I press save
	And I navigate to another screen and then return
	Then The basic settings should have the values I entered

Scenario: Save empty basic settings
	Given I am in the basic settings management screen
	And I enter empty string for each basic settings value
	Then I am not allowed to save the changes	