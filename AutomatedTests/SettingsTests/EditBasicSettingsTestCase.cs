﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems;
using White.Core;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.Utils;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.SettingsTests
{
    /// <summary>
    /// An automated test for Edit Basic Settings Test Case.
    /// </summary>
    [TestClass]
    public class EditBasicSettingsTestCase
    {
        /// <summary>
        /// The Basic Setting entity that will be edited.
        /// </summary>
        private static BasicSetting basicSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditBasicSettingsTestCase"/> class.
        /// </summary>
        public EditBasicSettingsTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditBasicSettingsTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for the Edit Basic Settings test case step 1 - step 14 .
        /// </summary>
        [TestMethod]
        public void CancelEditBasicSettingsTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                
                // step 4
                TreeNode basicSettingsNode = app.MainScreen.ProjectsTree.MasterDataBasicSettings;
                Assert.IsNotNull(basicSettingsNode, "Basic Settings node was not found.");
                basicSettingsNode.Click();

                // step 5
                EditBasicSettingsTestCase.FillBasicSettingsFieldsAndAssert(mainWindow, null);

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsFalse(saveButton.Enabled, "Save button is enabled although the mandatory fields are empty.");
                
                // step 6 -> 11
                EditBasicSettingsTestCase.FillBasicSettingsFieldsWithInvalidDataAndAssert(mainWindow);

                // step 12
                BasicSetting updatedBasicSettings = EditBasicSettingsTestCase.CreateBasicSettings();
                EditBasicSettingsTestCase.FillBasicSettingsFieldsAndAssert(mainWindow, updatedBasicSettings);
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                
                // step 13
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();                
                //EditBasicSettingsTestCase.VerifyBasicSettingInformation(mainWindow, updatedBasicSettings, false);

                // step 14
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
                //EditBasicSettingsTestCase.VerifyBasicSettingInformation(mainWindow, EditBasicSettingsTestCase.basicSettings, true);
            }
        }

        /// <summary>
        /// An automated test for Edit Basic Settings test case.
        /// </summary>
        [TestMethod]
        public void EditBasicSettingsTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                
                // step 4
                TreeNode basicSettingsNode = app.MainScreen.ProjectsTree.MasterDataBasicSettings;
                basicSettingsNode.Click();

                // step 15
                BasicSetting updatedSettings = EditBasicSettingsTestCase.CreateBasicSettings();
                EditBasicSettingsTestCase.FillBasicSettingsFieldsAndAssert(mainWindow, updatedSettings);

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

               // EditBasicSettingsTestCase.VerifyBasicSettingInformation(mainWindow, updatedSettings, false);
               // Assert.IsFalse(saveButton.Enabled, "The Save button is enabled although the basic settings were not changed.");
            }
        }

        #region Helper

        /// <summary>
        /// Fills the fields from BasicSettings view with the specified settings or
        /// with empty string if the basic settings entity is null. 
        /// </summary>
        /// <param name="basicSettingsView">The basic settings view.</param>
        /// <param name="settings">The basic settings entity which represents the model of the view.</param>
        public static void FillBasicSettingsFieldsAndAssert(Window basicSettingsView, BasicSetting settings)
        {
            TextBox depreciationPeriodTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox);
            Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Period text box was not found.");
            depreciationPeriodTextBox.Text = settings == null ? string.Empty : settings.DeprPeriod.ToString();

            TextBox interestRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.InterestRateTextBox);
            Assert.IsNotNull(interestRateTextBox, "Interest Rate text box was not found.");
            interestRateTextBox.Text = settings == null ? string.Empty : settings.DeprRate.ToString();

            TextBox batchSizeTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox);
            Assert.IsNotNull(batchSizeTextBox, "Batch size text box was not found.");
            batchSizeTextBox.Text = settings == null ? string.Empty : settings.PartBatch.ToString();

            TextBox shiftsPerWeekTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox);
            Assert.IsNotNull(shiftsPerWeekTextBox, "Shifts per week text box was not found.");
            shiftsPerWeekTextBox.Text = settings == null ? string.Empty : settings.ShiftsPerWeek.ToString();

            TextBox hoursPerShiftTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox);
            Assert.IsNotNull(hoursPerShiftTextBox, "Hours per shift text box was not found.");
            hoursPerShiftTextBox.Text = settings == null ? string.Empty : settings.HoursPerShift.ToString();

            TextBox productionDaysTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox);
            Assert.IsNotNull(productionDaysTextBox, "Production Days text box was not found.");
            productionDaysTextBox.Text = settings == null ? string.Empty : settings.ProdDays.ToString();

            TextBox productionWeeksTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox);
            Assert.IsNotNull(productionWeeksTextBox, "Production Weeks text box was not found.");
            productionWeeksTextBox.Text = settings == null ? string.Empty : settings.ProdWeeks.ToString();

            TextBox assertRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.AssetRateTextBox);
            //            Assert.IsNotNull(assertRateTextBox, "Assert Rate text box was not found.");
            assertRateTextBox.Text = settings == null ? string.Empty : settings.AssetRate.ToString();

            TextBox logisticCostRatioTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticCostRatioTextBox);
            Assert.IsNotNull(logisticCostRatioTextBox, "Logistic Cost Ratio text box was not found.");
            logisticCostRatioTextBox.Text = settings == null ? string.Empty : settings.LogisticCostRatio.ToString();
        }

        /// <summary>
        /// Fills the fields from BasicSettings view with invalid values.
        /// </summary>
        /// <param name="basicSettingsView">The Basic Settings view.</param>
        public static void FillBasicSettingsFieldsWithInvalidDataAndAssert(Window basicSettingsView)
        {
            int maxAllowedValueForDecimalFields = 1000000000;
            TextBox depreciationPeriodTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox);
            Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Period text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, depreciationPeriodTextBox, maxAllowedValueForDecimalFields, false);

            int maxAllowedValueForPercentageFields = 100;
            TextBox interestRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.InterestRateTextBox);
            Assert.IsNotNull(interestRateTextBox, "Interest Rate text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, interestRateTextBox, maxAllowedValueForPercentageFields, true);

            TextBox batchSizeTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox);
            Assert.IsNotNull(batchSizeTextBox, "Batch size text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, batchSizeTextBox, maxAllowedValueForDecimalFields, false);

            TextBox shiftsPerWeekTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox);
            Assert.IsNotNull(shiftsPerWeekTextBox, "Shifts per week text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, shiftsPerWeekTextBox, maxAllowedValueForDecimalFields, false);

            TextBox hoursPerShiftTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox);
            Assert.IsNotNull(hoursPerShiftTextBox, "Hours per shift text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, hoursPerShiftTextBox, 999999, true);

            TextBox productionDaysTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox);
            Assert.IsNotNull(productionDaysTextBox, "Production Days text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, productionDaysTextBox, maxAllowedValueForDecimalFields, false);

            TextBox productionWeeksTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox);
            //            Assert.IsNotNull(productionWeeksTextBox, "Production Weeks text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, productionWeeksTextBox, maxAllowedValueForDecimalFields, false);

            TextBox assertRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.AssetRateTextBox);
            Assert.IsNotNull(assertRateTextBox, "Assert Rate text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, assertRateTextBox, maxAllowedValueForPercentageFields, true);

            TextBox logisticCostRatioTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticCostRatioTextBox);
            Assert.IsNotNull(logisticCostRatioTextBox, "Logistic Cost Ratio text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(basicSettingsView, logisticCostRatioTextBox, maxAllowedValueForPercentageFields, true);
        }

        /// <summary>
        /// Checks if the information displayed in the Basic Settings view is correct.
        /// </summary>
        /// <param name="basicSettingsView">The Basic Settings view.</param>
        /// <param name="settings">The model of the Basic Settings view.</param>
        /// <param name="percentageFieldsAreInDbRepresentation">True if the values from percentage fields are in db representation, false otherwise.</param>
        public static void VerifyBasicSettingInformation(Window basicSettingsView, BasicSetting settings, bool percentageFieldsAreInDbRepresentation)
        {
            string expectedDeprRate = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.DeprRate * 100, 2) : Formatter.FormatNumber(settings.DeprRate, 2);
            Assert.AreEqual(expectedDeprRate, basicSettingsView.Get<TextBox>(SettingsAutomationIds.InterestRateTextBox).Text, "The value displayed into the Depreciation Rate field is wrong.");

            string expectedAssetRate = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.AssetRate * 100, 2) : Formatter.FormatNumber(settings.AssetRate, 2);
            Assert.AreEqual(expectedAssetRate, basicSettingsView.Get<TextBox>(SettingsAutomationIds.AssetRateTextBox).Text, "The value displayed into the Asset Rate field is wrong.");

            string expectedLogisticCostRatio = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.LogisticCostRatio * 100, 2) : Formatter.FormatNumber(settings.LogisticCostRatio, 2);
            Assert.AreEqual(expectedLogisticCostRatio, basicSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticCostRatioTextBox).Text, "The value displayed into the Logistic Cost Ratio field is wrong.");

            Assert.AreEqual(Formatter.FormatNumber(settings.PartBatch, 0), basicSettingsView.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox).Text, "The value displayed into the Batch Size field is wrong.");
            Assert.AreEqual(Formatter.FormatNumber(settings.ShiftsPerWeek, 0), basicSettingsView.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox).Text, "The value displayed into the Shifts per week field is wrong.");
            Assert.AreEqual(Formatter.FormatNumber(settings.HoursPerShift, 2), basicSettingsView.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox).Text, "The value displayed into the Hours per shift field is wrong.");
            Assert.AreEqual(Formatter.FormatNumber(settings.ProdDays, 0), basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox).Text, "The value displayed into the Production Days field is wrong.");
            Assert.AreEqual(Formatter.FormatNumber(settings.ProdWeeks, 0), basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox).Text, "The value displayed into the Production Weeks field is wrong.");
            Assert.AreEqual(Formatter.FormatNumber(settings.DeprPeriod, 0), basicSettingsView.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox).Text, "The value displayed into the Depreciation Period field is wrong.");
        }

        /// <summary>
        /// Creates a basic settings entity.
        /// </summary>
        /// <returns>The basic settings entity.</returns>
        public static BasicSetting CreateBasicSettings()
        {
            Random random = new Random();
            BasicSetting basicSettings = new BasicSetting();
            basicSettings.DeprPeriod = random.Next(100);
            basicSettings.DeprRate = random.Next(100);
            basicSettings.PartBatch = random.Next(300);
            basicSettings.ShiftsPerWeek = random.Next(400);
            basicSettings.HoursPerShift = random.Next(200);
            basicSettings.ProdDays = random.Next(300);
            basicSettings.ProdWeeks = random.Next(100);
            basicSettings.AssetRate = random.Next(100);
            basicSettings.LogisticCostRatio = random.Next(100);

            return basicSettings;
        }

        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            EditBasicSettingsTestCase.basicSettings = dataContext.BasicSettingsRepository.GetBasicSettings();
        }

        #endregion Helper
    }
}
