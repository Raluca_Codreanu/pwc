﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using ZPKTool;

namespace ZPKTool.AutomatedTests.SettingsTests
{
    [Binding]
    public class BasicSettingsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        // Scenario scoped data
        private BasicSetting enteredBasicSettingsForSave;

        [Given(@"I am in the basic settings management screen")]
        public void GivenIAmInTheBasicSettingsManagementScreen()
        {
            TreeNode basicSettingsNode = application.MainScreen.ProjectsTree.MasterDataBasicSettings;
            basicSettingsNode.SelectEx();
        }

        [Given(@"I enter the following values for basic settings:")]
        public void GivenIEnterTheFollowingValuesForBasicSettings(Table table)
        {
            var settings = table.CreateInstance<BasicSetting>();
            FillBasicSettingsFields(mainWindow, settings);
            this.enteredBasicSettingsForSave = settings;
        }

        [When(@"I press save")]
        public void WhenIPressSave()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            Assert.IsTrue(saveButton.Enabled, "The Save button was disabled.");
            saveButton.Click();
        }

        [When(@"I navigate to another screen and then return")]
        public void WhenINavigateToAnotherScreenAndThenReturn()
        {
            // Navigate to the master data node and back.
            var mdTreeNode = application.MainScreen.ProjectsTree.MasterData;                
            mdTreeNode.SelectEx();

            TreeNode basicSettingsNode = application.MainScreen.ProjectsTree.MasterDataBasicSettings;
            basicSettingsNode.SelectEx();

        }

        [Then(@"The basic settings should have the values I entered")]
        public void ThenTheBasicSettingsShouldHaveTheValuesIEntered()
        {
            VerifyBasicSettingInformation(mainWindow, this.enteredBasicSettingsForSave);
        }

        [Given(@"I enter empty string for each basic settings value")]
        public void GivenIEnterEmptyStringForEachBasicSettingsValue()
        {
            string p0 = string.Empty;

            TextBox depreciationPeriodTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox);
            depreciationPeriodTextBox.Text = p0;

            TextBox interestRateTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.InterestRateTextBox);
            interestRateTextBox.Text = p0;

            TextBox batchSizeTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox);
            Assert.IsNotNull(batchSizeTextBox, "Batch size text box was not found.");
            batchSizeTextBox.Text = p0;

            TextBox shiftsPerWeekTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox);
            Assert.IsNotNull(shiftsPerWeekTextBox, "Shifts per week text box was not found.");
            shiftsPerWeekTextBox.Text = p0;

            TextBox hoursPerShiftTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox);
            Assert.IsNotNull(hoursPerShiftTextBox, "Hours per shift text box was not found.");
            hoursPerShiftTextBox.Text = p0;

            TextBox productionDaysTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox);
            Assert.IsNotNull(productionDaysTextBox, "Production Days text box was not found.");
            productionDaysTextBox.Text = p0;

            TextBox productionWeeksTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox);
            Assert.IsNotNull(productionWeeksTextBox, "Production Weeks text box was not found.");
            productionWeeksTextBox.Text = p0;

            TextBox assertRateTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.AssetRateTextBox);
            Assert.IsNotNull(assertRateTextBox, "Assert Rate text box was not found.");
            assertRateTextBox.Text = p0;

            TextBox logisticCostRatioTextBox = mainWindow.Get<TextBox>(SettingsAutomationIds.LogisticCostRatioTextBox);
            Assert.IsNotNull(logisticCostRatioTextBox, "Logistic Cost Ratio text box was not found.");
            logisticCostRatioTextBox.Text = p0;
        }

        [Then(@"I am not allowed to save the changes")]
        public void ThenIAmNotAllowedToSaveTheChanges()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            Assert.IsFalse(saveButton.Enabled, "I was allowed to save the basic settings with invalid data.");
        }


        [BeforeFeature("BasicSettings")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("BasicSettings")]
        private static void KillApplication()
        {
            application.Kill();
        }

        #region Helpers

        /// <summary>
        /// Fills the fields from BasicSettings view with the specified settings or with empty string if the basic settings entity is null. 
        /// </summary>
        /// <param name="basicSettingsView">The basic settings view.</param>
        /// <param name="settings">The basic settings entity which represents the model of the view.</param>
        private static void FillBasicSettingsFields(Window basicSettingsView, BasicSetting settings)
        {
            TextBox depreciationPeriodTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox);
            Assert.IsNotNull(depreciationPeriodTextBox, "Depreciation Period text box was not found.");
            depreciationPeriodTextBox.Text = settings == null ? string.Empty : settings.DeprPeriod.ToString();

            TextBox interestRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.InterestRateTextBox);
            Assert.IsNotNull(interestRateTextBox, "Interest Rate text box was not found.");
            interestRateTextBox.Text = settings == null ? string.Empty : settings.DeprRate.ToString();

            TextBox batchSizeTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox);
            Assert.IsNotNull(batchSizeTextBox, "Batch size text box was not found.");
            batchSizeTextBox.Text = settings == null ? string.Empty : settings.PartBatch.ToString();

            TextBox shiftsPerWeekTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox);
            Assert.IsNotNull(shiftsPerWeekTextBox, "Shifts per week text box was not found.");
            shiftsPerWeekTextBox.Text = settings == null ? string.Empty : settings.ShiftsPerWeek.ToString();

            TextBox hoursPerShiftTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox);
            Assert.IsNotNull(hoursPerShiftTextBox, "Hours per shift text box was not found.");
            hoursPerShiftTextBox.Text = settings == null ? string.Empty : settings.HoursPerShift.ToString();

            TextBox productionDaysTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox);
            Assert.IsNotNull(productionDaysTextBox, "Production Days text box was not found.");
            productionDaysTextBox.Text = settings == null ? string.Empty : settings.ProdDays.ToString();

            TextBox productionWeeksTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox);
            Assert.IsNotNull(productionWeeksTextBox, "Production Weeks text box was not found.");
            productionWeeksTextBox.Text = settings == null ? string.Empty : settings.ProdWeeks.ToString();

            TextBox assertRateTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.AssetRateTextBox);
            Assert.IsNotNull(assertRateTextBox, "Assert Rate text box was not found.");
            assertRateTextBox.Text = settings == null ? string.Empty : settings.AssetRate.ToString();

            TextBox logisticCostRatioTextBox = basicSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticCostRatioTextBox);
            Assert.IsNotNull(logisticCostRatioTextBox, "Logistic Cost Ratio text box was not found.");
            logisticCostRatioTextBox.Text = settings == null ? string.Empty : settings.LogisticCostRatio.ToString();
        }

        /// <summary>
        /// Checks if the information displayed in the Basic Settings view is correct.
        /// </summary>
        /// <param name="window">The windows displaying the basic settings screen.</param>
        /// <param name="settings">The basic settings to compare with.</param>
        private static void VerifyBasicSettingInformation(Window window, BasicSetting settings)
        {
            Assert.AreEqual(
                settings.PartBatch.ToString(CultureInfo.CurrentCulture),
                window.Get<TextBox>(SettingsAutomationIds.BatchSizeTextBox).Text,
                "The value displayed into the Batch Size field is wrong.");

            Assert.AreEqual(
                settings.ShiftsPerWeek.ToString(CultureInfo.CurrentCulture),
                window.Get<TextBox>(SettingsAutomationIds.ShiftsPerWeekTextBox).Text,
                "The value displayed into the Shifts per week field is wrong.");

            Assert.AreEqual(
                settings.HoursPerShift.ToString(CultureInfo.CurrentCulture),
                window.Get<TextBox>(SettingsAutomationIds.HoursPerShiftTextBox).Text,
                "The value displayed into the Hours per shift field is wrong.");

            Assert.AreEqual(
                settings.ProdDays.ToString(CultureInfo.InvariantCulture),
                window.Get<TextBox>(SettingsAutomationIds.ProductionDaysTextBox).Text,
                "The value displayed into the Production Days field is wrong.");

            Assert.AreEqual(
                settings.ProdWeeks.ToString(CultureInfo.InvariantCulture),
                window.Get<TextBox>(SettingsAutomationIds.ProductionWeeksTextBox).Text,
                "The value displayed into the Production Weeks field is wrong.");

            Assert.AreEqual(
                settings.DeprPeriod.ToString(CultureInfo.InvariantCulture),
                window.Get<TextBox>(SettingsAutomationIds.DepreciationPeriodTextBox).Text,
                "The value displayed into the Depreciation Period field is wrong.");
        }

        #endregion Helpers
    }
}
