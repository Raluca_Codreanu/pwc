﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.Data;
using System.Threading;
using ZPKTool.AutomatedTests.Utils;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.SettingsTests
{
    /// <summary>
    /// The test class of EditOverheadSettings Test Case.
    /// </summary>
    [TestClass]
    public class EditOverheadSettingsTestCase
    {
        private static OverheadSetting overheadSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditOverheadSettingsTestCase"/> class.
        /// </summary>
        public EditOverheadSettingsTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EditOverheadSettingsTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for EditOverheadSettings test case.
        /// </summary>
        [TestMethod]
        public void EditOverheadSettingsTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;
                               
                // step 4
                TreeNode overheadSettingsNode = app.MainScreen.ProjectsTree.MasterDataOverheadSettings;
                overheadSettingsNode.Click();

                // step 14
                OverheadSetting updatedSettings = EditOverheadSettingsTestCase.CreateOverheadSettings();
                EditOverheadSettingsTestCase.FillOverheadSettingsFieldsAndAssert(mainWindow, updatedSettings);

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                Assert.IsNotNull(saveButton, "The Save button was not found.");
                saveButton.Click();

                EditOverheadSettingsTestCase.VerifyOverheadSettingInformation(mainWindow, updatedSettings, false);
            }
        }

        #region Helper

        /// <summary>
        /// Checks if the information displayed in the Basic Settings view is correct.
        /// </summary>
        /// <param name="overheadSettingsView">The Basic Settings view.</param>
        /// <param name="settings">The model of the Basic Settings view.</param>
        /// <param name="percentageFieldsAreInDbRepresentation">True if the values of the percentage fields are in db representation, false otherwise.</param>
        public static void VerifyOverheadSettingInformation(Window overheadSettingsView, OverheadSetting settings, bool percentageFieldsAreInDbRepresentation)
        {
            string expectedMaterialOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.MaterialOverhead * 100, 2) : Formatter.FormatNumber(settings.MaterialOverhead, 2);
            Assert.AreEqual(expectedMaterialOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox).Text, "The value displayed into the Material OH field is wrong.");

            string expectedConsumableOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ConsumableOverhead * 100, 2) : Formatter.FormatNumber(settings.ConsumableOverhead, 2);
            Assert.AreEqual(expectedConsumableOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox).Text, "The value displayed into the Consumable OH field is wrong.");

            string expectedCommodityOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.CommodityOverhead * 100, 2) : Formatter.FormatNumber(settings.CommodityOverhead, 2);
            Assert.AreEqual(expectedCommodityOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox).Text, "The value displayed into the Commodity OH field is wrong.");

            string expectedExternalWorkOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ExternalWorkOverhead * 100, 2) : Formatter.FormatNumber(settings.ExternalWorkOverhead, 2);
            Assert.AreEqual(expectedExternalWorkOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox).Text, "The value displayed into the External Work Overhead field is wrong.");

            string expectedManufacturingOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ManufacturingOverhead * 100, 2) : Formatter.FormatNumber(settings.ManufacturingOverhead, 2);
            Assert.AreEqual(expectedManufacturingOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox).Text, "The value displayed into the Manufacturing Overhead field is wrong.");

            string expectedOtherCostOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.OtherCostOHValue * 100, 2) : Formatter.FormatNumber(settings.OtherCostOHValue, 2);
            Assert.AreEqual(expectedOtherCostOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox).Text, "The value displayed into the Other Cost OH field is wrong.");

            string expectedPackagingOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.PackagingOHValue * 100, 2) : Formatter.FormatNumber(settings.PackagingOHValue, 2);
            Assert.AreEqual(expectedPackagingOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox).Text, "The value displayed into the Packaging OH field is wrong.");

            string expectedLogisticOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.LogisticOHValue * 100, 2) : Formatter.FormatNumber(settings.LogisticOHValue, 2);
            Assert.AreEqual(expectedLogisticOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox).Text, "The value displayed into the Logistic OH field is wrong.");

            string expectedSalesAndAdministrationOH = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.SalesAndAdministrationOHValue * 100, 2) : Formatter.FormatNumber(settings.SalesAndAdministrationOHValue, 2);
            Assert.AreEqual(expectedSalesAndAdministrationOH, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox).Text, "The value displayed into the Sales And Administration Overhead, field is wrong.");

            string expectedMaterialMargin = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.MaterialMargin * 100, 2) : Formatter.FormatNumber(settings.MaterialMargin, 2);
            Assert.AreEqual(expectedMaterialMargin, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox).Text, "The value displayed into the Material Margin field is wrong.");

            string expectedConsumableMargin = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ConsumableMargin * 100, 2) : Formatter.FormatNumber(settings.ConsumableMargin, 2);
            Assert.AreEqual(expectedConsumableMargin, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox).Text, "The value displayed into the Consumable Margin field is wrong.");

            string expectedCommodityMargin = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.CommodityMargin * 100, 2) : Formatter.FormatNumber(settings.CommodityMargin, 2);
            Assert.AreEqual(expectedCommodityMargin, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox).Text, "The value displayed into the Commodity Margin field is wrong.");

            string expectedExternalWorkMargin = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ExternalWorkMargin * 100, 2) : Formatter.FormatNumber(settings.ExternalWorkMargin, 2);
            Assert.AreEqual(expectedExternalWorkMargin, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox).Text, "The value displayed into the External Work Margin field is wrong.");

            string expectedManufacturingMargin = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.ManufacturingMargin * 100, 2) : Formatter.FormatNumber(settings.ManufacturingMargin, 2);
            Assert.AreEqual(expectedManufacturingMargin, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox).Text, "The value displayed into the Manufacturing Margin field is wrong.");

            string expectedCompanySurchargeOverhead = percentageFieldsAreInDbRepresentation ? Formatter.FormatNumber(settings.CompanySurchargeOverhead * 100, 2) : Formatter.FormatNumber(settings.CompanySurchargeOverhead, 2);
            Assert.AreEqual(expectedCompanySurchargeOverhead, overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox).Text, "The value displayed into the Manufacturing Margin field is wrong.");

        }

        /// <summary>
        /// Fills the fields from OverheadSettings view with the specified settings or
        /// with empty string if the basic settings entity is null.
        /// </summary>
        /// <param name="overheadSettingsView">The overhead settings view.</param>
        /// <param name="settings">The overhead settings entity which represents the model of the view.</param>
        public static void FillOverheadSettingsFieldsAndAssert(Window overheadSettingsView, OverheadSetting settings)
        {
            TextBox materialOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.IsNotNull(materialOHTextBox, "Material Overhead text box was not found.");
            materialOHTextBox.Text = settings == null ? string.Empty : settings.MaterialOverhead.ToString();

            TextBox consumableOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.IsNotNull(consumableOHTextBox, "Consumable Overhead text box was not found.");
            consumableOHTextBox.Text = settings == null ? string.Empty : settings.ConsumableOverhead.ToString();

            TextBox commodityOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.IsNotNull(commodityOHTextBox, "Commodity Overhead text box was not found.");
            commodityOHTextBox.Text = settings == null ? string.Empty : settings.CommodityOverhead.ToString();

            TextBox externalWorkOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.IsNotNull(externalWorkOHTextBox, "External Work Overhead text box was not found.");
            externalWorkOHTextBox.Text = settings == null ? string.Empty : settings.ExternalWorkOverhead.ToString();

            TextBox manufacturingOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.IsNotNull(manufacturingOHTextBox, "Manufacturing Overhead text box was not found.");
            manufacturingOHTextBox.Text = settings == null ? string.Empty : settings.ManufacturingOverhead.ToString();

            TextBox otherCostOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.IsNotNull(otherCostOverheadTextBox, "Other Cost Overhead text box was not found.");
            otherCostOverheadTextBox.Text = settings == null ? string.Empty : settings.OtherCostOHValue.ToString();

            TextBox packagingOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.IsNotNull(packagingOverheadTextBox, "Packaging Cost Overhead text box was not found.");
            packagingOverheadTextBox.Text = settings == null ? string.Empty : settings.PackagingOHValue.ToString();

            TextBox logisticOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.IsNotNull(logisticOverheadTextBox, "Logistic Overhead text box was not found.");
            logisticOverheadTextBox.Text = settings == null ? string.Empty : settings.LogisticOHValue.ToString();

            TextBox salesAndAdministrationOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.IsNotNull(salesAndAdministrationOHTextBox, "Sales And Administration Overhead text box was not found.");
            salesAndAdministrationOHTextBox.Text = settings == null ? string.Empty : settings.SalesAndAdministrationOHValue.ToString();

            TextBox materialMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.IsNotNull(materialMarginTextBox, "Material Margin text box was not found.");
            materialMarginTextBox.Text = settings == null ? string.Empty : settings.MaterialMargin.ToString();

            TextBox consumableMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.IsNotNull(consumableMarginTextBox, "Consumable Margin text box was not found.");
            consumableMarginTextBox.Text = settings == null ? string.Empty : settings.ConsumableMargin.ToString();

            TextBox commodityMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.IsNotNull(commodityMarginTextBox, "Commodity Margin text box was not found.");
            commodityMarginTextBox.Text = settings == null ? string.Empty : settings.CommodityMargin.ToString();

            TextBox externalWorkMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.IsNotNull(externalWorkMarginTextBox, "External Work Margin text box was not found.");
            externalWorkMarginTextBox.Text = settings == null ? string.Empty : settings.ExternalWorkMargin.ToString();

            TextBox manufacturingMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.IsNotNull(manufacturingMarginTextBox, "Manufacturing Margin text box was not found.");
            manufacturingMarginTextBox.Text = settings == null ? string.Empty : settings.ManufacturingMargin.ToString();

            TextBox companySurchargeOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.IsNotNull(companySurchargeOHTextBox, "Company Surcharge Overhead text box was not found.");
            companySurchargeOHTextBox.Text = settings == null ? string.Empty : settings.CompanySurchargeOverhead.ToString();
        }

        /// <summary>
        /// Fills the fields from OverheadSettings view with invalid values.
        /// </summary>
        /// <param name="overheadSettingsView">The Overhead Settings view.</param>
        public static void FillOverheadSettingsFieldsWithInvalidDataAndAssert(Window overheadSettingsView)
        {
            int maxAllowedValue = 1000;
            TextBox materialOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialOHTextBox);
            Assert.IsNotNull(materialOHTextBox, "Material Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, materialOHTextBox, maxAllowedValue, true);
            Thread.Sleep(600);
            TextBox consumableOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableOHTextBox);
            Assert.IsNotNull(consumableOHTextBox, "Consumable Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, consumableOHTextBox, maxAllowedValue, true);

            TextBox commodityOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityOHTextBox);
            Assert.IsNotNull(commodityOHTextBox, "Commodity Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, commodityOHTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox externalWorkOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkOHTextBox);
            Assert.IsNotNull(externalWorkOHTextBox, "External Work Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, externalWorkOHTextBox, maxAllowedValue, true);
            Thread.Sleep(600);
            TextBox manufacturingOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingOHTextBox);
            Assert.IsNotNull(manufacturingOHTextBox, "Manufacturing Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, manufacturingOHTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox otherCostOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.OtherCostOHTextBox);
            Assert.IsNotNull(otherCostOverheadTextBox, "Other Cost Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, otherCostOverheadTextBox, maxAllowedValue, true);
            
            Thread.Sleep(600);
            TextBox packagingOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.PackagingOverheadTextBox);
            Assert.IsNotNull(packagingOverheadTextBox, "Packaging Cost Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, packagingOverheadTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox logisticOverheadTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.LogisticOverheadTextBox);
            Assert.IsNotNull(logisticOverheadTextBox, "Logistic Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, logisticOverheadTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox salesAndAdministrationTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.SalesAndAdministrationOverheadTextBox);
            Assert.IsNotNull(salesAndAdministrationTextBox, "Sales And Administration text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, salesAndAdministrationTextBox, maxAllowedValue, true);

            TextBox materialMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.MaterialMarginTextBox);
            Assert.IsNotNull(materialMarginTextBox, "Material Margin text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, materialMarginTextBox, maxAllowedValue, true);

            TextBox consumableMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ConsumableMarginTextBox);
            Assert.IsNotNull(consumableMarginTextBox, "Consumable Margin text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, consumableMarginTextBox, maxAllowedValue, true);

            TextBox commodityMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CommodityMarginTextBox);
            Assert.IsNotNull(commodityMarginTextBox, "Commodity Margin text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, commodityMarginTextBox, maxAllowedValue, true);

            TextBox externalWorkMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ExternalWorkMarginTextBox);
            Assert.IsNotNull(externalWorkMarginTextBox, "External Work Margin text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, externalWorkMarginTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox manufacturingMarginTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.ManufacturingMarginTextBox);
            Assert.IsNotNull(manufacturingMarginTextBox, "Manufacturing Margin text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, manufacturingMarginTextBox, maxAllowedValue, true);

            Thread.Sleep(600);
            TextBox companySurchargeOHTextBox = overheadSettingsView.Get<TextBox>(SettingsAutomationIds.CompanySurchargeOHTextBox);
            Assert.IsNotNull(companySurchargeOHTextBox, "Company Surcharge Overhead text box was not found.");
            ValidationHelper.CheckNumericFieldValidators(overheadSettingsView, companySurchargeOHTextBox, maxAllowedValue, true);
        }

        /// <summary>
        /// Creates a basic settings entity.
        /// </summary>
        /// <returns>The basic settings entity.</returns>
        public static OverheadSetting CreateOverheadSettings()
        {
            Random random = new Random();
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.MaterialOverhead = random.Next(1000);
            overheadSettings.ConsumableOverhead = random.Next(1000);
            overheadSettings.CommodityOverhead = random.Next(1000);
            overheadSettings.ExternalWorkOverhead = random.Next(1000);
            overheadSettings.ManufacturingOverhead = random.Next(1000);
            overheadSettings.OtherCostOHValue = random.Next(1000);
            overheadSettings.PackagingOHValue = random.Next(1000);
            overheadSettings.LogisticOHValue = random.Next(1000);
            overheadSettings.SalesAndAdministrationOHValue = random.Next(1000);
            overheadSettings.MaterialMargin = random.Next(1000);
            overheadSettings.ConsumableMargin = random.Next(1000);
            overheadSettings.CommodityMargin = random.Next(1000);
            overheadSettings.ExternalWorkMargin = random.Next(1000);
            overheadSettings.ManufacturingMargin = random.Next(1000);
            overheadSettings.CompanySurchargeOverhead = random.Next(1000);

            return overheadSettings;
        }

        /// <summary>
        /// Creates the test data.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            EditOverheadSettingsTestCase.overheadSettings = dataContext.OverheadSettingsRepository.GetMasterData();
        }

        #endregion Helper
    }
}
