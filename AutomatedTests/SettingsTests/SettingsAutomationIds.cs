﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.SettingsTests
{
    public static class  SettingsAutomationIds
    {
        public const string MaterialOverheadTextBox = "MaterialOHTextBox";
        public const string SalesAndAdministrationOverheadTextBox = "SalesAndAdministrationOHTextBox";
        public const string LogisticOverheadTextBox = "LogisticOHTextBox";
        public const string PackagingOverheadTextBox = "PackagingOHTextBox";
        public const string OtherCostOHTextBox = "OtherCostOHTextBox";
        public const string ManufacturingOHTextBox = "ManufacturingOHTextBox";
        public const string ExternalWorkOHTextBox = "ExternalWorkOHTextBox";
        public const string CommodityOHTextBox = "CommodityOHTextBox";
        public const string ConsumableOHTextBox = "ConsumableOHTextBox";
        public const string MaterialOHTextBox = "MaterialOHTextBox";
        public const string ManufacturingMarginTextBox = "ManufacturingMarginTextBox";
        public const string ExternalWorkMarginTextBox = "ExternalWorkMarginTextBox";
        public const string CommodityMarginTextBox = "CommodityMarginTextBox";
        public const string ConsumableMarginTextBox = "ConsumableMarginTextBox";
        public const string MaterialMarginTextBox = "MaterialMarginTextBox";
        public const string DepreciationPeriodTextBox = "DepreciationPeriodTextBox";
        public const string InterestRateTextBox = "DepreciationRateTextBox";
        public const string BatchSizeTextBox = "BatchSizePerYearTextBox";
        public const string ShiftsPerWeekTextBox = "ShiftsPerWeekTextBox";
        public const string HoursPerShiftTextBox = "HoursPerShiftTextBox";
        public const string ProductionDaysTextBox = "ProductionDaysTextBox";
        public const string ProductionWeeksTextBox = "ProductionWeeksTextBox";
        public const string AssetRateTextBox = "AssetRateTextBox";
        public const string LogisticCostRatioTextBox = "LogisticCostRatioTextBox";
        public const string CompanySurchargeOHTextBox = "CompanySurchargeOHTextBox";
    }
}
