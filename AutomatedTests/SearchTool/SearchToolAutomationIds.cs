﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.SearchTool
{
    class SearchToolAutomationIds
    {
        public const string SearchResultsTree = "SearchResultsTree";
        public const string RawMaterialsNode = "Raw Materials";
        public const string DiesNode = "Dies";
        public const string SearchForTextBox = "SearchForTextBox";
        public const string SearchButton = "SearchButton";
        public const string StopButton = "StopButton";
        public const string FiltersButton = "FiltersButton";
        public const string LookInButton = "SearchScopeButton";
        public const string FilterSelectAllCheckbox = "SelectAllCheckbox";
        public const string LookInSelectAllCheckbox = "SelectAllCheckbox";
        public const string FiltersLabel = "Filters";
        public const string LookInLabel = "Look in";
        public const string CloseFiltersButton = "CloseFiltersButton";
        public const string CloseScopeButton = "CloseScopeButton";
    }
}
