﻿using System;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using White.Core.UIItems.TreeItems;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using System.Threading;

namespace ZPKTool.AutomatedTests.SearchTool
{
    /// <summary>
    /// Summary description for SearchToolTestCase
    /// </summary>
    [TestClass]
    public class SearchToolTestCase
    {
        public SearchToolTestCase()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void SearchForAnyEnityIntoAllLocationsMethod()
        {
            SetIsAutoHiddenProperty(false);
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {                
                Window mainWindow = app.Windows.MainWindow;                
                
                SearchForAnyEnityIntoAllLocations(mainWindow);
            }
            SetIsAutoHiddenProperty(true);
        }

        #region Helper

        /// <summary>
        /// Sets the "IsAutoHidden" property of the Search Pane and select it.
        /// </summary>
        /// <param name="isAutoHidden">True if the Search Pane is auto hidden, false otherwise.</param>
        public static void SetIsAutoHiddenProperty(bool isAutoHidden)
        {
            string uiLayoutFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PCMData\\UILayout.xml";
            if (File.Exists(uiLayoutFilePath))
            {
                //FileStream reader = new FileStream(uiLayoutFilePath, FileMode.Open, FileAccess.ReadWrite);
                XmlDocument uiLayoutFile = new XmlDocument();
                uiLayoutFile.Load(uiLayoutFilePath);

                XmlNodeList nodes = uiLayoutFile.GetElementsByTagName("DockablePane");
                XmlNode searchPaneNode = null;
                int count = 0;
                while (searchPaneNode == null && count < nodes.Count)
                {
                    foreach (XmlNode node in nodes[count].ChildNodes)
                    {
                        if (node.Attributes["Name"].Value == "SearchToolContent")
                        {
                            searchPaneNode = nodes[count];
                        }
                    }

                    count++;
                }

                if (searchPaneNode != null)
                {
                    searchPaneNode.Attributes["IsAutoHidden"].Value = isAutoHidden.ToString().ToLower();
                    // searchPaneNode.Attributes["SelectedIndex"].Value = "0";
                }

                Thread.Sleep(1000);
                uiLayoutFile.Save(uiLayoutFilePath);
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Search for any text filtered by all entities into all locations. 
        /// Before calling this method the "IsAutoHidden" property(from UILayout.xml) of the Search Panel should be false.
        /// </summary>
        /// <param name="app">The current application.</param>
        public static void SearchForAnyEnityIntoAllLocations(Window mainWindow)
        {            
            TextBox searchForTextBox = mainWindow.Get<TextBox>(SearchToolAutomationIds.SearchForTextBox);
            Assert.IsNotNull(searchForTextBox, "The 'Search For' text box was not found.");
            searchForTextBox.Text = "*";

            Button filtersButton = mainWindow.Get<Button>(SearchToolAutomationIds.FiltersButton);
            Assert.IsNotNull(filtersButton, "The Filters button was not found.");

            filtersButton.Click();
            Thread.Sleep(300);

            Window filtersPopup = mainWindow.ModalWindowsSafe()[0];
            Assert.IsNotNull(filtersPopup, "The Filters popup was not found.");

            CheckBox selectAllCheckbox = filtersPopup.Get<CheckBox>(SearchToolAutomationIds.FilterSelectAllCheckbox);
            Assert.IsNotNull(selectAllCheckbox, "Select All check box was not found.");
            if (searchForTextBox.Name == "Check All")
            {
                selectAllCheckbox.Checked = true;
            }

            Button closeButton = filtersPopup.Get<Button>(SearchToolAutomationIds.CloseFiltersButton);
            Assert.IsNotNull(closeButton, "Close filters button was not found.");
            closeButton.Click();

            Button lookInButton = mainWindow.Get<Button>(SearchToolAutomationIds.LookInButton);
            Assert.IsNotNull(lookInButton, "Look In button was not found.");

            lookInButton.Click();
            Thread.Sleep(300);

            Window lookInPopup = mainWindow.ModalWindowsSafe()[0];
            Assert.IsNotNull(lookInPopup, "The Look In popup was not found.");

            selectAllCheckbox = lookInPopup.Get<CheckBox>(SearchToolAutomationIds.LookInSelectAllCheckbox);
            Assert.IsNotNull(selectAllCheckbox, "Select All check box was not found.");
            if (selectAllCheckbox.Name == "Check All")
            {
                selectAllCheckbox.Checked = true;
            }

            closeButton = lookInPopup.Get<Button>(SearchToolAutomationIds.CloseScopeButton);
            Assert.IsNotNull(closeButton, "Close search scope button was not found.");
            closeButton.Click();

            Button searchButton = mainWindow.Get<Button>(SearchToolAutomationIds.SearchButton);
            Assert.IsNotNull(searchButton, "The Search button was not found.");

            searchButton.Click();
            Assert.IsTrue(Wait.For(() => mainWindow.Get<Button>(SearchToolAutomationIds.SearchButton).Enabled), "The Search button is still disabled after a certain time.");
        }
        #endregion Helper
    }
}
