﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.Finders;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.WindowItems;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Offers quick access to all items of the app's main menu.
    /// </summary>
    public class MainMenu
    {
        /// <summary>
        /// The window hosting the tree.
        /// </summary>
        private Window window;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="window">The window hosting the main screen.</param>
        /// <exception cref="System.ArgumentNullException">The host window can not be null.</exception>
        public MainMenu(Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The host window can not be null.");
            }

            this.window = window;
        }

        /// <summary>
        /// Gets the System menu item.
        /// </summary>
        public Menu System
        {
            get { return this.GetMenu(AutomationIds.System); }
        }

        /// <summary>
        /// Gets the System -> Model viewer menu item.
        /// </summary>
        public Menu System_ModelViewer
        {
            get { return this.GetMenu(AutomationIds.System, AutomationIds.System_ModelViewer); }
        }

        /// <summary>
        /// Gets the System -> Logout menu item.
        /// </summary>
        public Menu System_Logout
        {
            get { return this.GetMenu(AutomationIds.System, AutomationIds.System_Logout); }
        }

        /// <summary>
        /// Gets the System -> Exit menu item.
        /// </summary>
        public Menu System_Exit
        {
            get { return this.GetMenu(AutomationIds.System, AutomationIds.System_Exit); }
        }

        /// <summary>
        /// Gets the Edit menu item.
        /// </summary>
        public Menu Edit
        {
            get { return this.GetMenu(AutomationIds.Edit); }
        }

        /// <summary>
        /// Gets the Edit -> Cut menu item.
        /// </summary>
        public Menu Edit_Cut
        {
            get { return this.GetMenu(AutomationIds.Edit, AutomationIds.Edit_Cut); }
        }

        /// <summary>
        /// Gets the Edit -> Copy menu item.
        /// </summary>
        public Menu Edit_Copy
        {
            get { return this.GetMenu(AutomationIds.Edit, AutomationIds.Edit_Copy); }
        }

        /// <summary>
        /// Gets the Edit -> Paste menu item.
        /// </summary>
        public Menu Edit_Paste
        {
            get { return this.GetMenu(AutomationIds.Edit, AutomationIds.Edit_Paste); }
        }

        /// <summary>
        /// Gets the Edit -> Delete menu item.
        /// </summary>
        public Menu Edit_Delete
        {
            get { return this.GetMenu(AutomationIds.Edit, AutomationIds.Edit_Delete); }
        }

        /// <summary>
        /// Gets the Options menu item.
        /// </summary>
        public Menu Options
        {
            get { return this.GetMenu(AutomationIds.Options); }
        }

        /// <summary>
        /// Gets the Options -> Preferences menu item.
        /// </summary>
        public Menu Options_Preferences
        {
            get { return this.GetMenu(AutomationIds.Options, AutomationIds.Options_Preferences); }
        }

        /// <summary>
        /// Gets the Options -> Change Password menu item.
        /// </summary>
        public Menu Options_ChangePassword
        {
            get { return this.GetMenu(AutomationIds.Options, AutomationIds.Options_ChangePassword); }
        }

        /// <summary>
        /// Gets the Project menu item.
        /// </summary>
        public Menu Project
        {
            get { return this.GetMenu(AutomationIds.Project); }
        }

        /// <summary>
        /// Gets the Project -> Create Project menu item.
        /// </summary>
        public Menu Project_CreateProject
        {
            get { return this.GetMenu(AutomationIds.Project, AutomationIds.Project_CreateProject); }
        }

        /// <summary>
        /// Gets the Data menu item.
        /// </summary>
        public Menu Data
        {
            get { return this.GetMenu(AutomationIds.Data); }
        }

        /// <summary>
        /// Gets the Data -> Synchronize Data menu item.
        /// </summary>
        public Menu SyncDataMenuItem
        {
            get { return this.GetMenu(AutomationIds.Data, AutomationIds.SyncDataMenuItem); }
        }

        /// <summary>
        /// Gets the Data -> Synchronize Data menu item.
        /// </summary>
        public Menu Data_MassDataUpdate
        {
            get { return this.GetMenu(AutomationIds.Data, AutomationIds.Data_MassDataUpdate); }
        }

        /// <summary>
        /// Gets the View menu item.
        /// </summary>
        public Menu View
        {
            get { return this.GetMenu(AutomationIds.View); }
        }

        /// <summary>
        /// Gets the View -> Bookmark menu item.
        /// </summary>
        public Menu BookmarkViewMenuItem
        {
            get { return this.GetMenu(AutomationIds.View, AutomationIds.View_Bookmark); }
        }

        /// <summary>
        /// Gets the View -> Search menu item.
        /// </summary>
        public Menu SearchViewMenuItem
        {
            get { return this.GetMenu(AutomationIds.View, AutomationIds.View_Search); }
        }

        /// <summary>
        /// Gets the View -> Advanced Search menu item.
        /// </summary>
        public Menu AdvancedSearchViewMenuItem
        {
            get { return this.GetMenu(AutomationIds.View, AutomationIds.View_AdvancedSearch); }
        }

        /// <summary>
        /// Gets the View -> Compare menu item.
        /// </summary>
        public Menu CompareViewMenuItem
        {
            get { return this.GetMenu(AutomationIds.View, AutomationIds.View_Compare); }
        }


        /// <summary>
        /// Gets the Project -> Create Folder menu item.
        /// </summary>
        public Menu Project_CreateFolder
        {
            get { return this.GetMenu(AutomationIds.Project, AutomationIds.Project_CreateFolder); }
        }

        /// <summary>
        /// Gets the menu item by a path (of automation IDs of items in the path).
        /// </summary>
        /// <param name="automationIds">The automation IDs path.</param>
        /// <returns>
        /// The menu item at the end of the path
        /// </returns>
        /// <exception cref="System.ArgumentException">The automation IDs list can not be empty.</exception>
        public Menu GetMenu(params string[] automationIds)
        {
            return GetMenu(automationIds.Select(id => SearchCriteria.ByAutomationId(id)).ToArray());
        }

        /// <summary>
        /// Gets the menu item by a criteria path.
        /// </summary>
        /// <param name="criteria">The criteria path.</param>
        /// <returns>The menu item at the end of the path</returns>
        /// <exception cref="System.ArgumentException">The criteria list can not be empty.</exception>
        public Menu GetMenu(params SearchCriteria[] criteria)
        {
            if (criteria.Length == 0)
            {
                throw new ArgumentException("The criteria list can not be empty.", "criteria");
            }

            var menu = this.window.Get<Menu>(criteria[0]);
            if (menu == null)
            {
                Assert.IsNotNull(menu, "The menu item '{0}' was not found.", criteria[0].ToString());
            }

            for (int i = 1; i < criteria.Length; i++)
            {
                menu.Click();
                menu = menu.SubMenu(criteria[i]);
                if (menu == null)
                {
                    Assert.IsNotNull(menu, "The menu item '{0}' was not found.", criteria[0].ToString());
                }
            }

            return menu;
        }

        /// <summary>
        /// The automation ids of the main menu items.
        /// </summary>
        public static class AutomationIds
        {
            public const string System = "SystemMenuItem";
            public const string System_ModelViewer = "ModelViewerMenuItem";
            public const string System_Logout = "LogOutMenuItem";
            public const string System_Exit = "ExitMenuItem";

            public const string Edit = "EditMenuItem";
            public const string Edit_Cut = "CutMenuItem";
            public const string Edit_Copy = "CopyMenuItem";
            public const string Edit_Paste = "PasteMenuItem";
            public const string Edit_Delete = "DeleteMenuItem";

            public const string Options = "OptionsMenuItem";
            public const string Options_Preferences = "PreferencesMenuItem";
            public const string Options_ChangePassword = "ChangePwdMenuItem";

            public const string Project = "ProjectMenuItem";
            public const string Project_CreateProject = "CreateProjectMenuItem";
            public const string Project_CreateFolder = "CreateFolderMenuItem";

            public const string Data = "dataMenuItem";
            public const string Data_MassDataUpdate = "MassDataUpdateMenuItem";
            public const string SyncDataMenuItem = "SyncDataMenuItem";

            public const string View = "ViewMenuItem";
            public const string View_Bookmark = "BookmarkViewMenuItem";
            public const string View_Search = "SearchViewMenuItem";
            public const string View_AdvancedSearch = "AdvancedSearchViewMenuItem";
            public const string View_Compare = "CompareViewMenuItem";
        }
    }
}
