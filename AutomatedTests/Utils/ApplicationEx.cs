﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Wrapper for the White Application class that offer easy access to the PCM app's UI resources.
    /// </summary>
    public class ApplicationEx : IDisposable
    {
        /// <summary>
        /// The wrapped application instance.
        /// </summary>
        private Application application;

        /// <summary>
        /// The main screen
        /// </summary>
        private MainScreen mainScreen;

        /// <summary>
        /// The main menu
        /// </summary>
        private MainMenu mainMenu;

        /// <summary>
        /// Prevents a default instance of the <see cref="ApplicationEx" /> class from being created.
        /// </summary>
        /// <param name="app">The application instance to wrap.</param>
        private ApplicationEx(Application app)
        {
            this.application = app;
            this.Windows = new AppWindows(app);
        }

        /// <summary>
        /// Gets the windows.
        /// </summary>
        public AppWindows Windows { get; private set; }

        /// <summary>
        /// Gets the welcome screen.
        /// </summary>
        public WPFControl WelcomeScreen
        {
            get
            {
                var element = this.Windows.MainWindow.Get(SearchCriteria.ByAutomationId("WelcomeView"));
                return new WPFControl(element.AutomationElement, element.ActionListener);
            }
        }

        /// <summary>
        /// Gets the main screen.
        /// </summary>
        public MainScreen MainScreen
        {
            get
            {
                if (this.mainScreen == null)
                {
                    if (this.Windows.MainWindow == null)
                    {
                        throw new InvalidOperationException("The main window is not available.");
                    }

                    this.mainScreen = new MainScreen(this.Windows.MainWindow);
                }

                return this.mainScreen;
            }
        }

        /// <summary>
        /// Gets the main menu.
        /// </summary>
        public MainMenu MainMenu
        {
            get
            {
                if (this.mainMenu == null)
                {
                    if (this.Windows.MainWindow == null)
                    {
                        throw new InvalidOperationException("The main window is not available.");
                    }

                    this.mainMenu = new MainMenu(this.Windows.MainWindow);
                }

                return this.mainMenu;
            }
        }

        public bool HasExited
        {
            get { return this.application.HasExited; }
        }

        /// <summary>
        /// Launches the application process.
        /// </summary>
        /// <returns>An object representing the application.</returns>
        public static ApplicationEx Launch()
        {
            var processes = System.Diagnostics.Process.GetProcessesByName("PCM");
            foreach (var process in processes)
            {
                process.Kill();
            }

            var app = Application.Launch(@"..\..\TestBuild\PCM.exe");
            return new ApplicationEx(app);
        }

        /// <summary>
        /// Attaches to an existing application process.
        /// </summary>
        /// <returns>An object representing the application.</returns>
        public static ApplicationEx Attach()
        {
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("PCM");
            if (processes.Length == 0)
            {
                throw new AutomationException("Could not find an application process to which to attach.", string.Empty);
            }
            else
            {
                var app = Application.Attach(processes[0].Id);
                return new ApplicationEx(app);
            }
        }

        /// <summary>
        /// Launches the application and wait for main window to load.
        /// </summary>
        /// <returns>An object representing the application.</returns>
        public static ApplicationEx LaunchAndWaitForMainWindow()
        {
            var app = ApplicationEx.Launch();
            app.Windows.WaitForMainWindowToLoad();

            return app;
        }

        /// <summary>
        /// Launches the application and navigates to the main screen.
        /// </summary>
        /// <param name="username">The username to use for logging into the application.</param>
        /// <param name="password">The password to use for logging into the application.</param>
        /// <returns>An object representing the application.</returns>
        public static ApplicationEx LaunchAndGoToMainScreen(string username = "admin", string password = "admin")
        {
            var app = ApplicationEx.LaunchAndWaitForMainWindow();
            app.LoginAndGoToMainScreen(username, password);

            return app;
        }

        /// <summary>
        /// Logs in and navigates to the main screen. It assumes that the application is on the log in screen when this call is made.
        /// </summary>
        /// <param name="username">The username to use for logging into the application.</param>
        /// <param name="password">The password to use for logging into the application.</param>
        public void LoginAndGoToMainScreen(string username = "admin", string password = "admin")
        {
            this.Login(username, password);

            // Skip the welcome screen
            this.SkipWelcomeScreen();
        }

        /// <summary>
        /// Logs in using the specified username and password. It assumes that the application is on the log in screen when this call is made.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public void Login(string username = "admin", string password = "admin")
        {
            // TODO: check if the app is on the log in screen

            // Wait for the main window to load.            
            this.Windows.WaitForMainWindowToLoad();
            var mainWindow = this.Windows.MainWindow;

            TextBox userNameTextBox = mainWindow.Get<TextBox>("UserNameTextBox");
            Assert.IsNotNull(userNameTextBox, "The user name textbox was not found");
            userNameTextBox.SetValue(username);

            TextBox passwordTextBox = mainWindow.Get<TextBox>("PasswordTextBox");
            Assert.IsNotNull(passwordTextBox, "The password textbox was not found");
            passwordTextBox.SetValue(password);

            Button loginButton = mainWindow.Get<Button>("LoginButton");
            Assert.IsNotNull(loginButton, "The login button was not found");
            loginButton.Click();

            // Wait for the login process to finish (it is implemented asynchronously).
            // The login progress overlay displayed after pressing the log in button contains a label with the "LoggingInMessageLabel" AutomationId. We wait until it is no longer visible.
            var loggingInLabel = mainWindow.Get<Label>("LoggingInMessageLabel");
            while (loggingInLabel != null && loggingInLabel.Visible)
            {
                System.Threading.Thread.Sleep(500);
            }

            // Wait until the database maintenance operation that sometimes happens after login finishes (the operation is executed using the Please Wait service).
            mainWindow.WaitForAsyncUITasks();
        }

        /// <summary>
        /// Skips the welcome screen, if is currently displayed by the app.
        /// </summary>
        public void SkipWelcomeScreen()
        {
            // Skip the welcome screen (the welcome screen may not be available due to the Show Start Screen setting being off).
            var welcomeScreen = Wait.For(() => this.WelcomeScreen, 500);
            if (welcomeScreen != null)
            {
                Button skipButton = Wait.For(() => this.Windows.MainWindow.Get<Button>(AutomationIds.SkipButton));
                skipButton.Click();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.application != null)
            {
                this.application.Dispose();
            }
        }

        /// <summary>
        /// Kills the applications and waits till it is closed.
        /// </summary>
        public void Kill()
        {
            this.application.Kill();
        }
    }
}
