﻿namespace ZPKTool.AutomatedTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Automation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using White.Core;
    using White.Core.UIItems;
    using White.Core.UIItems.Actions;
    using White.Core.UIItems.Custom;
    using White.Core.UIItems.ListBoxItems;
    using White.Core.UIItems.WindowItems;
    using ZPKTool.AutomatedTests.CustomUIItems;

    /// <summary>
    /// Encapsulates the set of operations available on the Media View user control.
    /// </summary>
    public class MediaControl
    {
        /// <summary>
        /// The default automation id of the media controls.
        /// </summary>
        public const string DefaultAutomationId = "MediaViewId";

        /// <summary>
        /// The delay after which the overlay containing the media controls appears.
        /// </summary>
        private const int ControlsOverlayDelay = 300;

        /// <summary>
        /// The window in which the media WPF control resides.
        /// </summary>
        private Window parentWindow;

        /// <summary>
        /// The media control on which to perform the operations.
        /// </summary>
        private WPFControl mediaControl;

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaControl" /> class.
        /// </summary>
        /// <param name="primaryId">The primary id of the media control.</param>
        /// <param name="parentWindow">The window in which the media WPF control resides.</param>
        public MediaControl(string primaryId, Window parentWindow)
        {
            this.parentWindow = parentWindow;
            this.mediaControl = this.parentWindow.Get<WPFControl>(primaryId);
            Assert.IsNotNull(this.mediaControl, "The window did not contain a media control with the '{0}' id.", primaryId);
        }

        /// <summary>
        /// Moves the mouse over the media control , in its top left corner.
        /// </summary>
        public void MoveMouseOver()
        {
            this.parentWindow.Mouse.Location = new System.Windows.Point(
                this.mediaControl.Bounds.X + (this.mediaControl.Bounds.Width / 2),
                this.mediaControl.Bounds.Y + (this.mediaControl.Bounds.Height / 2));
        }

        /// <summary>
        /// Clicks the add button of the media control.
        /// </summary>
        public void ClickAdd()
        {
            this.MoveMouseOver();

            // The controls overlay appears over the media view with a delay.
            System.Threading.Thread.Sleep(MediaControl.ControlsOverlayDelay);

            Button addButton = Wait.For(() => this.parentWindow.Get<Button>("AddMediaButton"));
            addButton.Click();
        }

        /// <summary>
        /// Clicks the delete button of the media control.
        /// </summary>
        public void ClickDelete()
        {
            this.MoveMouseOver();

            // The controls overlay appears over the media view with a delay.
            System.Threading.Thread.Sleep(MediaControl.ControlsOverlayDelay);

            Button deleteButton = Wait.For(() => this.parentWindow.Get<Button>("DeleteMediaButton"));
            deleteButton.Click();
        }

        public void ClickFullScreen()
        {
            this.MoveMouseOver();

            // The controls overlay appears over the media view with a delay.
            System.Threading.Thread.Sleep(MediaControl.ControlsOverlayDelay);

            Button fullScreenButton = Wait.For(() => this.parentWindow.Get<Button>("ToggleFullScreenButton"));
            fullScreenButton.Click();
        }
        
        public void ClicSaveAs()
        {
            this.MoveMouseOver();

            // The controls overlay appears over the media view with a delay.
            System.Threading.Thread.Sleep(MediaControl.ControlsOverlayDelay);

            Button saveAsButton = Wait.For(() => this.parentWindow.Get<Button>("SaveAsButton"));
            saveAsButton.Click();
        }

        /// <summary>
        /// Adds a new file in the media control (picture, movie, etc.)
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="pathKind">The kind of the path.</param>
        /// <exception cref="System.InvalidOperationException">UriKind.RelativeOrAbsolute is not supported.</exception>
        public void AddFile(string filePath, UriKind pathKind)
        {
            if (pathKind == UriKind.RelativeOrAbsolute)
            {
                throw new InvalidOperationException("UriKind.RelativeOrAbsolute is not supported.");
            }

            if (pathKind == UriKind.Relative)
            {
                filePath = System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\Search.png");
            }

            this.ClickAdd();
            this.parentWindow.GetOpenFileDialog().OpenFile(filePath);
        }
    }
}
