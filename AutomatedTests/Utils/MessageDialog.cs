﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// This class wraps a message dialog and exposes the operations available on it as simple methods.
    /// </summary>
    public class MessageDialog
    {
        /// <summary>
        /// The message dialog window.
        /// </summary>
        private Window dialogWindow;

        /// <summary>
        /// Prevents a default instance of the <see cref="MessageDialog"/> class from being created.
        /// </summary>
        /// <param name="messageDialog">The message dialog window.</param>
        private MessageDialog(Window messageDialog)
        {
            if (messageDialog == null)
            {
                throw new ArgumentNullException("messageDialog", "The message dialog window was null.");
            }

            this.dialogWindow = messageDialog;

            // Determine the type of the dialog            
            this.Type = FindDialogType(this.dialogWindow);
        }

        /// <summary>
        /// Gets the type of the message dialog.
        /// </summary>
        public MessageDialogType Type { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this dialog is closed.
        /// </summary>
        public bool IsClosed
        {
            get { return this.dialogWindow.IsClosed; }
        }

        /// <summary>
        /// Gets the current message dialog displayed by the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="waitTimeout">The wait timeout for getting the dialog.</param>
        /// <returns>The message dialog or null if the dialog is not available after waiting for the specified time period.</returns>
        public static MessageDialog GetCurrent(Application app, int waitTimeout = 5000)
        {
            Window messageDialog = Wait.For(
                () => app.GetWindowsSafe().FirstOrDefault(w => w.Title == "Info"
                    || w.Title == "Warning"
                    || w.Title == "Error"
                    || w.Title == "Question"),
                waitTimeout);

            if (messageDialog == null)
            {
                return null;
            }
            else
            {
                return new MessageDialog(messageDialog);
            }
        }

        /// <summary>
        /// Gets the current message dialog displayed by the specified window.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="waitTimeout">The wait timeout for getting the dialog.</param>
        /// <returns>The message dialog or null if the dialog is not available after waiting for the specified time period.</returns>
        public static MessageDialog GetCurrent(Window window, int waitTimeout = 5000)
        {
            Window messageDialog = Wait.For(
                () => window.ModalWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == "Info"
                    || w.AutomationElement.Current.AutomationId == "Warning"
                    || w.AutomationElement.Current.AutomationId == "Error"
                    || w.AutomationElement.Current.AutomationId == "Question"),
                waitTimeout);

            if (messageDialog == null)
            {
                return null;
            }
            else
            {
                return new MessageDialog(messageDialog);
            }
        }

        /// <summary>
        /// Clicks the OK button.
        /// </summary>
        public void ClickOk()
        {
            var button = this.dialogWindow.Get<Button>("OkButton");
            button.Click();
        }

        /// <summary>
        /// Clicks the Cancel button.
        /// </summary>
        public void ClickCancel()
        {
            var button = this.dialogWindow.Get<Button>("CancelButton");
            button.Click();
        }

        /// <summary>
        /// Clicks the Yes button.
        /// </summary>
        public void ClickYes()
        {
            var button = this.dialogWindow.Get<Button>("YesButton");
            button.Click();
        }

        /// <summary>
        /// Clicks the No button.
        /// </summary>
        public void ClickNo()
        {
            var button = this.dialogWindow.Get<Button>("NoButton");
            button.Click();
        }

        /// <summary>
        /// Clicks a custom button on the dialog.
        /// </summary>
        /// <param name="criteria">The criteria for finding the button.</param>
        public void ClickCustomButton(SearchCriteria criteria)
        {
            var button = this.dialogWindow.Get<Button>(criteria);
            button.Click();
        }

        /// <summary>
        /// Closes the dialog using the default window closing button (the X button in the top right corner of the window).
        /// </summary>
        public void Close()
        {
            this.dialogWindow.Close();
        }

        /// <summary>
        /// Finds the type of the specified message dialog.
        /// </summary>
        /// <param name="dialogWindow">The message dialog window.</param>
        /// <returns>The message dialog's type.</returns>
        private MessageDialogType FindDialogType(Window dialogWindow)
        {
            MessageDialogType type = MessageDialogType.Custom;

            if (dialogWindow.AutomationElement.Current.AutomationId == "Info")
            {
                type = MessageDialogType.Info;
            }
            else if (this.dialogWindow.AutomationElement.Current.AutomationId == "Warning")
            {
                type = MessageDialogType.Warning;
            }
            else if (this.dialogWindow.AutomationElement.Current.AutomationId == "Error")
            {
                type = MessageDialogType.Error;
            }
            else
            {
                // Each message dialog has at least two extra buttons: one is the default window close button (the x in top right)
                // and the other is the Send Error Report button which is visible only in some situations.
                var buttons = this.dialogWindow.GetMultiple(SearchCriteria.ByControlType(System.Windows.Automation.ControlType.Button));
                var visibleButtons = buttons.Where(b => b.Visible && b.Framework.WPF && b.AutomationElement.Current.AutomationId != "SendErrorReportBtn");

                if (visibleButtons.Count() == 2
                    && buttons.FirstOrDefault(btn => btn.AutomationElement.Current.AutomationId == "YesButton") != null
                    && buttons.FirstOrDefault(btn => btn.AutomationElement.Current.AutomationId == "NoButton") != null)
                {
                    type = MessageDialogType.YesNo;
                }
                else if (visibleButtons.Count() == 3
                    && buttons.FirstOrDefault(btn => btn.AutomationElement.Current.AutomationId == "YesButton") != null
                    && buttons.FirstOrDefault(btn => btn.AutomationElement.Current.AutomationId == "NoButton") != null
                    && buttons.FirstOrDefault(btn => btn.AutomationElement.Current.AutomationId == "CancelButton") != null)
                {
                    type = MessageDialogType.YesNoCancel;
                }
                else
                {
                    type = MessageDialogType.Custom;
                }
            }

            return type;
        }
    }
}
