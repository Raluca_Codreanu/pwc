﻿namespace ZPKTool.AutomatedTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using White.Core;

    /// <summary>
    /// Helper class for waiting until conditions are met or UI elements become available or un-available.
    /// </summary>
    public static class Wait
    {
        /// <summary>
        /// Sleeps until the specified condition is met or the specified timeout elapses.
        /// </summary>
        /// <param name="condition">The condition that must be met in order to exit from this method.</param>
        /// <param name="timeout">A value that indicates how much to wait until exiting the method even if the condition is not met, in milliseconds.</param>
        /// <param name="waitInterval">The interval at which the condition is re-evaluated, in milliseconds.</param>
        /// <returns>The value returned by the condition method before exiting the method.</returns>
        public static bool For(Func<bool> condition, int timeout = 5000, int waitInterval = 200)
        {
            var startTime = DateTime.Now;
            while (DateTime.Now.Subtract(startTime).TotalMilliseconds < timeout && !condition())
            {
                Thread.Sleep(waitInterval);
            }

            return condition();
        }

        /// <summary>
        /// Sleeps until the specified method returns a value that is not the default of its type or until the specified timeout elapses.        
        /// </summary>
        /// <typeparam name="T">The type of value returned by the method.</typeparam>
        /// <param name="getMethod">The method that executes an operation and returns a value.</param>
        /// <param name="timeout">A value that indicates how much to wait until exiting the method even if the method did not return a non-null value (in milliseconds).</param>
        /// <param name="waitInterval">The interval at which the get method is called, in milliseconds.</param>
        /// <returns>The value returned by the method or default(T) if it timed out before the method returned a non-default value.</returns>
        public static T For<T>(Func<T> getMethod, int timeout = 5000, int waitInterval = 200)
        {
            T value = default(T);
            var startTime = DateTime.Now;
            while (DateTime.Now.Subtract(startTime).TotalMilliseconds < timeout)
            {
                try
                {
                    value = getMethod();
                }
                catch (AutomationException)
                {
                }

                if (value != null && !value.Equals(default(T)))
                {
                    return value;
                }

                System.Threading.Thread.Sleep(waitInterval);
            }

            try
            {
                value = getMethod();
            }
            catch (AutomationException)
            {
            }

            return value;
        }
    }
}
