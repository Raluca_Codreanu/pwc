﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// The types of the message dialog
    /// </summary>
    public enum MessageDialogType
    {
        /// <summary>
        /// Info message dialog.
        /// </summary>
        Info,

        /// <summary>
        /// Warning message dialog.
        /// </summary>
        Warning,

        /// <summary>
        /// Error message dialog.
        /// </summary>
        Error,

        /// <summary>
        /// Yes/No question message dialog.
        /// </summary>
        YesNo,

        /// <summary>
        /// Yes/No/Cancel question message dialog.
        /// </summary>
        YesNoCancel,

        /// <summary>
        /// A customized message dialog.
        /// </summary>
        Custom
    }
}
