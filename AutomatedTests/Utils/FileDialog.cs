﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.WindowItems;
using White.Core.WindowsAPI;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Implements operations on the common file dialogs (Save File, Open File).
    /// </summary>
    public class FileDialog
    {
        /// <summary>
        /// The title of the open file dialog.
        /// </summary>
        public const string OpenFileDialogTitle = "Open";

        /// <summary>
        /// The title of the save file dialog.
        /// </summary>
        public const string SaveFileDialogTitle = "Save As";

        /// <summary>
        /// The dialog.
        /// </summary>
        private Window dialogWindow;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileDialog"/> class.
        /// </summary>
        /// <param name="window">The window that opened the dialog.</param>
        /// <param name="title">The title of the dialog.</param>
        /// <exception cref="System.ArgumentNullException">The file dialog host window can not be null.</exception>
        /// <exception cref="System.ArgumentException">The file dialog title can not be empty.;title</exception>
        public FileDialog(Window window, string title)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The file dialog host window can not be null.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException("The file dialog title can not be empty.", "title");
            }

            var dialog = window.ModalWindow(title);
            window.WaitWhileBusy();
            Assert.IsNotNull(dialog, "The File Dialog titled '{0}' could not be found.", title);
            this.dialogWindow = dialog;
        }

        /// <summary>
        /// Opens the file specified by a path.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        public void OpenFile(string path)
        {
            var filenameTextBox = this.dialogWindow.Get<TextBox>(SearchCriteria.ByAutomationId(AutomationIds.OpenFileFileNameTextBox));
            filenameTextBox.SetValue(path);

            //// Get the file type combo box
            //// ComboBox fileTypeComboBox = dialogWindow.Get<ComboBox>("Files of type:");

            var openButton = this.dialogWindow.Get<Button>(SearchCriteria.ByAutomationId(AutomationIds.OpenButton));
            openButton.Focus();
            openButton.Click();
        }

        /// <summary>
        /// Saves the file to the specified path. If <paramref name="path"/> is empty it is assumed that the save path is already set in the save dialog.
        /// </summary>
        /// <param name="path">The path.</param>
        public void SaveFile(string path)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                var filenameTextBox = this.dialogWindow.Get<TextBox>(SearchCriteria.ByAutomationId(AutomationIds.SaveFileFileNameTextBox));
                filenameTextBox.SetValue(path);

                // The next two lines are necessary to make the dialog detect and use the path inputted above.
                filenameTextBox.Focus();
                filenameTextBox.KeyIn(KeyboardInput.SpecialKeys.SPACE);
            }

            var saveButton = this.dialogWindow.Get<Button>(SearchCriteria.ByAutomationId(AutomationIds.SaveButton));
            saveButton.Focus();
            saveButton.Click();
        }

        /// <summary>
        /// Cancels the file dialog (clicks the cancel button).
        /// </summary>
        public void Cancel()
        {
            Button cancelButton = this.dialogWindow.Get<Button>(SearchCriteria.ByAutomationId(AutomationIds.CancelButton));
            cancelButton.Click();
        }

        /// <summary>
        /// The automation ids of UI elements from the file dialogs.
        /// </summary>
        public static class AutomationIds
        {
            /// <summary>
            /// The automation id of the Open button
            /// </summary>
            public const string OpenButton = "1";

            /// <summary>
            /// The automation id of the save button
            /// </summary>
            public const string SaveButton = "1";

            /// <summary>
            /// The automation id of the cancel button
            /// </summary>
            public const string CancelButton = "2";

            /// <summary>
            /// The automation id of the file name text box on the Open File dialog.
            /// </summary>
            public const string OpenFileFileNameTextBox = "1148";

            /// <summary>
            /// The automation id of the file name text box on the save file dialog.
            /// </summary>
            public const string SaveFileFileNameTextBox = "1001";
        }
    }
}
