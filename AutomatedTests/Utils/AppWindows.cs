﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core;
using White.Core.UIItems.Finders;
using White.Core.UIItems.WindowItems;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Helper class for easy access to the well-known windows of the application.
    /// </summary>
    public class AppWindows
    {
        /// <summary>
        /// The application
        /// </summary>
        private Application application;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppWindows"/> class.
        /// </summary>
        /// <param name="app">The application .</param>
        public AppWindows(Application app)
        {
            this.application = app;
        }

        /// <summary>
        /// Gets the main window.
        /// </summary>
        public Window MainWindow
        {
            get
            {
                return application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.MainWindow);
            }
        }

        /// <summary>
        /// Gets the Assembly window.
        /// </summary>
        public Window AssemblyWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.AssemblyWindow);
            }
        }

        /// <summary>
        /// Gets the Preferences window.
        /// </summary>
        public Window Preferences
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.PreferencesWindow);
            }
        }

        /// <summary>
        /// Gets the Country and Supplier Browser window.
        /// </summary>
        public Window CountryAndSupplierBrowser
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.CountryAndSupplierBrowser);
            }
        }

        /// <summary>
        /// Gets the Project window.
        /// </summary>        
        public Window ProjectWindow
        {
            get
            {
                return this.application.GetWindows().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ProjectWindow);
            }
        }

        /// <summary>
        /// Gets the Create Country window.
        /// </summary>        
        public Window CountryWindow
        {
            get
            {
                return this.application.GetWindows().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.CountryWindow);
            }
        }

        /// <summary>
        /// Gets the Create Currency window.
        /// </summary>        
        public Window CurrencyWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.CurrencyWindow);
            }
        }

        /// <summary>
        /// Gets the Create Supplier window.
        /// </summary>        
        public Window SupplierWindow
        {
            get
            {
                return this.application.GetWindows().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.CountrySupplierWindow);
            }
        }

        /// <summary>
        /// Gets the Create Manufacturer window.
        /// </summary>        
        public Window ManufacturerWindow
        {
            get
            {
                return this.application.GetWindows().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ManufacturerWindow);
            }
        }

        /// <summary>
        /// Gets the Part window.
        /// </summary>
        public Window PartWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.PartWindow);
            }
        }

        /// <summary>
        /// Gets the Raw Part window.
        /// </summary>
        public Window RawPartWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.Title == "Create Raw Part");
            }
        }

        /// <summary>
        /// Gets the Commodity window.
        /// </summary>        
        public Window CommodityWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.CommodityWindow);
            }
        }

        /// <summary>
        /// Gets the Raw Material window.
        /// </summary>        
        public Window RawMaterialWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.RawMaterialWindow);
            }
        }

        /// <summary>
        /// Gets the Machine window.
        /// </summary>        
        public Window MachineWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.MachineWindow);
            }
        }

        /// <summary>
        /// Gets the Die window.
        /// </summary>        
        public Window DieWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.DieWindow);
            }
        }

        /// <summary>
        /// Gets the Consumable window.
        /// </summary>        
        public Window ConsumableWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ConsumableWindow);
            }
        }

        /// <summary>
        /// Gets the Folder window.
        /// </summary>        
        public Window FolderWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.FolderWindow);
            }
        }

        /// <summary>
        /// Gets the Synchronization window.
        /// </summary>        
        public Window SynchronizationWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.SynchronizationWindow);
            }
        }

        /// <summary>
        /// Gets the Transport Cost Calculator window.
        /// </summary>        
        public Window TransportCostCalculatorSummaryWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.TransportCostCalculatorSummaryWindow);
            }
        }

        /// <summary>
        /// Gets the Report selection window.
        /// </summary>        
        public Window ReportsSelectionWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ReportsSelectionWindow);
            }
        }

        /// <summary>
        /// Gets the Change Password window.
        /// </summary>        
        public Window ChangePasswordWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ChangePasswordWindow);
            }
        }

        /// <summary>
        /// Gets the Result Details window.
        /// </summary>        
        public Window ResultDetailsWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ResultDetailsWindow);
            }
        }

        /// <summary>
        /// Gets the Import Confirmation window.
        /// </summary>        
        public Window ImportConfirmationWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ImportConfirmationWindow);
            }
        }

        /// <summary>
        /// Gets the User window.
        /// </summary>        
        public Window UserWindow
        {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.UserWindow);
            }
        }

        public Window ProcessWindow {
            get
            {
                return this.application.GetWindowsSafe().FirstOrDefault(w => w.AutomationElement.Current.AutomationId == AutomationIds.ProcessViewTab);
            }
        }
        /// <summary>
        /// Waits for the main window to load.
        /// </summary>
        /// <param name="waitTimeout">How much to wait before timing out. The default is 2 seconds.</param>
        /// <returns>
        /// The main window.
        /// </returns>
        /// <exception cref="White.Core.AutomationException">The main window was not found before timing out.</exception>
        public Window WaitForMainWindowToLoad(int waitTimeout = 120000)
        {
            Window mainWnd = null;
            int waitedTime = 0;
            do
            {
                try
                {
                    mainWnd = application.GetWindow(
                        SearchCriteria.ByAutomationId(AutomationIds.MainWindow),
                        White.Core.Factory.InitializeOption.NoCache);
                }
                catch
                {
                }

                if (mainWnd == null)
                {
                    // app.GetWindow waits 5000 ms by default before throwing an exception.
                    waitedTime += 5000;
                }
            }
            while (mainWnd == null && (waitedTime < waitTimeout));

            if (mainWnd == null && waitedTime >= waitTimeout)
            {
                throw new AutomationException(string.Format("The main window has failed to show after waiting for {0} seconds.", waitTimeout / 1000), string.Empty);
            }

            return mainWnd;
        }

        /// <summary>
        /// The automation ids of the application's windows.
        /// </summary>
        public static class AutomationIds
        {
            /// <summary>
            /// The main window's automation id.
            /// </summary>
            public const string MainWindow = "ShellWindow";

            /// <summary>
            /// The id of the Country and Supplier Browser window.
            /// </summary>
            public const string CountryAndSupplierBrowser = "CountryAndSupplierBrowserWindow";
            public const string PreferencesWindow = "PreferencesWindow";
            public const string PartWindow = "PartWindow";
            public const string AssemblyWindow = "AssemblyWindow";
            public const string ProjectWindow = "ProjectWindow";
            public const string ChangePasswordWindow = "ChangePasswordWindow";
            public const string RawMaterialWindow = "RawMaterialWindow";
            public const string ConsumableWindow = "ConsumableWindow";
            public const string CommodityWindow = "CommodityWindow";
            public const string CountryWindow = "CountryWindow";
            public const string UserWindow = "UserWindow";
            public const string SupplierWindow = "SupplierWindow";
            public const string FolderWindow = "FolderWindow";
            public const string ManufacturerWindow = "ManufacturerWindow";
            public const string CountrySupplierWindow = "CountrySupplierWindow";
            public const string CurrencyWindow = "CurrencyWindow";
            public const string MachineWindow = "MachineWindow";
            public const string DieWindow = "DieWindow";
            public const string ImportConfirmationWindow = "ImportConfirmationWindow";
            public const string ProcessViewTab = "ProcessViewTab";
            public const string SynchronizationWindow = "SynchronizationWindow";
            public const string ResultDetailsWindow = "UserControl";
            public const string ReportsSelectionWindow = "ReportsSelectionWindow";
            public const string TransportCostCalculatorSummaryWindow = "TransportCostCalculatorSummary";
        }
    }
}
