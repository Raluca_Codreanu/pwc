﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Wraps the main screen's tree view that displays the projects and offers easy access to its predefined elements.
    /// </summary>
    public class ProjectsTree
    {
        /// <summary>
        /// The window hosting the tree.
        /// </summary>
        private Window window;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsTree"/> class.
        /// </summary>
        /// <param name="window">The window hosting the main screen.</param>
        /// <exception cref="System.ArgumentNullException">The host window can not be null.</exception>
        public ProjectsTree(Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The host window can not be null.");
            }

            this.window = window;
        }

        /// <summary>
        /// Gets the tree that displays the projects.
        /// </summary>
        public Tree Tree
        {
            get
            {
                return this.window.Get<Tree>(AutomationIds.Tree);
            }
        }

        public TreeNode ReleasedProjects
        {
            get
            {             
                return this.Tree.GetNodeByAutomationId("Released Projects");
            }
        }

        /// <summary>
        /// Gets the My Projects tree node.
        /// </summary>
        public TreeNode MyProjects
        {
            get
            {                
                return this.Tree.GetNodeByAutomationId("MyProjectsTreeItem");
            }
        }

        public TreeNode TrashBin
        {
            get
            {
                return this.Tree.GetNodeByAutomationId("Trash Bin");
            }
        }

        /// <summary>
        /// Gets the Other Users Projects node.
        /// </summary>        
        public TreeNode OtherUsersProjectsNode
        {
            get
            {
                return this.Tree.GetNodeByAutomationId("OthetUsersProjectsTreeItem");
            }
        }

        /// <summary>
        /// Gets the Master Data node.
        /// </summary>
        public TreeNode MasterData
        {
            get
            {
                return this.Tree.GetNodeByAutomationId("Master Data");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Entities node.
        /// </summary>
        public TreeNode MasterDataEntities
        {
            get
            {
                this.MasterData.ExpandEx();
                return this.MasterData.GetNodeByAutomationId("MDEntities");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Entities -> Assemblies node.
        /// </summary>
        public TreeNode MasterDataAssemblies
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesAssemblies");
            }
        }

        /// <summary>
        /// Gets the Parts node from Master Data.
        /// </summary>        
        public TreeNode MasterDataParts
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesParts");
            }
        }

        /// <summary>
        /// Gets the Machines node from Master Data.
        /// </summary>        
        public TreeNode MasterDataMachines
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesMachines");
            }
        }

        /// <summary>
        /// Gets the Raw Materials node from Master Data.
        /// </summary>        
        public TreeNode MasterDataRawMaterials
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesRawMaterials");
            }
        }

        /// <summary>
        /// Gets the Consumables node from Master Data.
        /// </summary>        
        public TreeNode MasterDataConsumables
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesConsumables");
            }
        }

        /// <summary>
        /// Gets the Commodities node from Master Data.
        /// </summary>        
        public TreeNode MasterDataCommodities
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesCommodities");
            }
        }

        /// <summary>
        /// Gets the Raw Parts node from Master Data.
        /// </summary>        
        public TreeNode MasterDataRawParts
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesRawParts");
            }
        }

        /// <summary>
        /// Gets the Dies node from Master Data.
        /// </summary>        
        public TreeNode MasterDataDies
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesDies");
            }
        }

        /// <summary>
        /// Gets the Manufacturers node from Master Data.
        /// </summary>        
        public TreeNode MasterDataManufacturers
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesManufacturer");
            }
        }

        /// <summary>
        /// Gets the Suppliers node from Master Data.
        /// </summary>        
        public TreeNode MasterDataSuppliers
        {
            get
            {
                this.MasterDataEntities.ExpandEx();
                return this.MasterDataEntities.GetNodeByAutomationId("MDEntitiesSuppliers");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Settings node.
        /// </summary>
        public TreeNode MasterDataSettings
        {
            get
            {
                this.MasterData.ExpandEx();
                return this.MasterData.GetNodeByAutomationId("MDSettings");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Settings -> Basic Settings node.
        /// </summary>
        public TreeNode MasterDataBasicSettings
        {
            get
            {
                this.MasterDataSettings.ExpandEx();
                return this.MasterDataSettings.GetNodeByAutomationId("MDSettingsBasicSettings");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Settings -> Overhead Settings node.
        /// </summary>
        public TreeNode MasterDataOverheadSettings
        {
            get
            {
                this.MasterDataSettings.ExpandEx();
                return this.MasterDataSettings.GetNodeByAutomationId("MDSettingsOH");
            }
        }

        /// <summary>
        /// Gets the Admin node.
        /// </summary>
        public TreeNode Admin
        {
            get
            {
                return this.Tree.GetNodeByAutomationId("Admin");
            }
        }

        /// <summary>
        /// Gets the Admin -> Manage Users node.
        /// </summary>
        public TreeNode ManageUsers
        {
            get
            {
                this.Admin.ExpandEx();
                return this.Admin.GetNodeByAutomationId("AdminManageUsers");
            }
        }

        /// <summary>
        /// Gets the Admin -> Manage Projects node.
        /// </summary>
        public TreeNode ManageProjects
        {
            get
            {
                this.Admin.ExpandEx();
                return this.Admin.GetNodeByAutomationId("AdminManageProjects");
            }
        }

        /// <summary>
        /// Gets the Admin -> Manage Released Projects node.
        /// </summary>
        public TreeNode ManageReleasedProjects
        {
            get
            {
                this.Admin.ExpandEx();
                return this.Admin.GetNodeByAutomationId("ManageReleasedProjects");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Settings -> Countries node.
        /// </summary>
        public TreeNode MasterDataCountries
        {
            get
            {
                this.MasterDataSettings.ExpandEx();
                return this.MasterDataSettings.GetNodeByAutomationId("MDSettingsCountries");
            }
        }

        /// <summary>
        /// Gets the Master Data -> Settings -> BCurrencies node.
        /// </summary>
        public TreeNode MasterDataCurrencies
        {
            get
            {
                this.MasterDataSettings.ExpandEx();
                return this.MasterDataSettings.GetNodeByAutomationId("MDSettingsCurrencies");
            }
        }

        /// <summary>
        /// Automation ids for projects tree related elements.
        /// </summary>
        public static class AutomationIds
        {
            /// <summary>
            /// The automation id of the tree view that displays the projects.
            /// </summary>
            public const string Tree = "ProjectsTree";
        }
    }
}
