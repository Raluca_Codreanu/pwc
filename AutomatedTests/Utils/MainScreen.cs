﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;

namespace ZPKTool.AutomatedTests
{
    /// <summary>
    /// Offers structured access to UI assets of the main screen of the application.
    /// </summary>
    public class MainScreen
    {
        /// <summary>
        /// The window hosting the main screen.
        /// </summary>
        private Window window;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainScreen"/> class.
        /// </summary>
        /// <param name="window">The window hosting the main screen.</param>
        public MainScreen(Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("window", "The host window can not be null.");
            }

            this.window = window;
            this.ProjectsTree = new ProjectsTree(window);
        }

        /// <summary>
        /// Gets the projects tree.
        /// </summary>
        public ProjectsTree ProjectsTree { get; private set; }

        /// <summary>
        /// Gets the Search Results tree.
        /// </summary>        
        public Tree SearchResultsTree
        {
            get
            {
                return this.window.Get<Tree>("SearchResultsTree");
            }
        }
    }
}
