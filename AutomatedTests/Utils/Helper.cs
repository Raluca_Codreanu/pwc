﻿namespace ZPKTool.AutomatedTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Automation;
    using White.Core;
    using White.Core.UIItems;
    using White.Core.UIItems.Finders;
    using White.Core.UIItems.WindowItems;
    using ZPKTool.AutomatedTests.CustomUIItems;
    using ZPKTool.AutomatedTests.UserAccess;
    using System.Xml;
    using ZPKTool.Common;
    using ZPKTool.DataAccess;

    /// <summary>
    /// Helper methods
    /// </summary>
    public static class Helper
    {
        #region Constants

        public const string LocalDbInitialCatalog = "ZPKTool";
        public const string LocalDbDataSource = @"dorinp-pc";
        public const string LocalDbUserId = "PROimpensUser";
        public const string LocalDbEncryptedPassword = "iRl9VE3o1n1OlxzcTaOA0g==";
        public const string LocalDbPassword = "c,6P4J.m,";
        public const string CentralDbInitialCatalog = "ZPKToolCentral";
        public const string CentralDbDataSource = @"dorinp-pc";
        public const string CentralDbUserId = "PROimpensUser";
        public const string CentralDbEncryptedPassword = "iRl9VE3o1n1OlxzcTaOA0g==";
        public const string CentralDbPassword = "c,6P4J.m,";

        #endregion Constants

        /// <summary>
        /// Configures the database access for the data access layer.
        /// Due to implementation details, this method should be called before attempting any call to data access methods.
        /// </summary>
        public static void ConfigureDbAccess()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.InitialCatalog = Helper.LocalDbInitialCatalog;
            localConnParams.DataSource = Helper.LocalDbDataSource;
            localConnParams.UserId = Helper.LocalDbUserId;
            localConnParams.Password = Helper.LocalDbEncryptedPassword;
            localConnParams.PasswordEncryptionKey = Common.Constants.PasswordEncryptionKey;

            DbConnectionInfo centralConnParams = new DbConnectionInfo();
            centralConnParams.InitialCatalog = Helper.CentralDbInitialCatalog;
            centralConnParams.DataSource = Helper.CentralDbDataSource;
            centralConnParams.UserId = Helper.CentralDbUserId;
            centralConnParams.Password = Helper.CentralDbEncryptedPassword;
            centralConnParams.PasswordEncryptionKey = Common.Constants.PasswordEncryptionKey;

            ConnectionConfiguration.Initialize(localConnParams, centralConnParams);
        }

        /// <summary>
        /// Delete the settings.xml file from the AppData -> PROimpens folder.
        /// </summary>
        public static void DeleteAppSettingsFile()
        {
            string settingsFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PCMData\\settings.xml";
            if (File.Exists(settingsFilePath))
            {
                try
                {
                    File.Delete(settingsFilePath);
                }
                catch (Exception)
                {
                    // Do not throw any exception if the delete operation failed
                }
            }
        }

        public static string CopyElementValue()
        {
            string settingsFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PCMData\\settings.xml";

            string copy = string.Empty;
            string language = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(settingsFilePath);
            XmlNodeList nodes = doc.SelectNodes("SettingsFile/Settings/Setting");

            foreach (XmlNode node in nodes)
            {
                XmlAttributeCollection nodeAtt = node.Attributes;
                if (nodeAtt["Name"].Value.ToString() == "UILanguage")
                {
                    language = node.Value;
                    break;
                }
            }

            if (language == "en-US")
            {
                copy = "Copy of";
            }
            else if (language == "de-DE")
            {
                copy = "Kopie von";
            }

            return copy;
        }
    }
}

