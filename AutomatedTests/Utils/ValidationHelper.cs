﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core;
using White.Core.UIItems.WindowItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using System.Windows.Automation;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.Utils
{
    public static class ValidationHelper
    {
        /// <summary>
        /// Verifies that the correct validations are applied on the specified numeric field.
        /// </summary>
        /// <param name="parentWindow">The parent window of the text box.</param>
        /// <param name="numericField">The numeric field.</param>
        /// <param name="maxAllowedValue">The max allowed value for the specified numeric field.</param>
        /// <param name="allowsDecimalPlaces">True if the decimal places are allowed, false otherwise.</param>
        public static void CheckNumericFieldValidators(Window parentWindow, TextBox numericField, decimal maxAllowedValue, bool allowsDecimalPlaces)
        {
            AttachedKeyboard keyboard = parentWindow.Keyboard;

            // Introduce a string into the numerical field.
            numericField.Text = string.Empty;
            keyboard.Enter("mock");

            // Paste a string into the numerical field
            numericField.Text = string.Empty;
            System.Windows.Clipboard.SetText("mock");
            Menu pasteMenuItem = numericField.GetContextMenuByText(parentWindow, "Paste");
            pasteMenuItem.Click();

            // Introduce a value greater than the max allowed value
            numericField.Text = string.Empty;
            keyboard.Enter(maxAllowedValue.ToString() + "0");

            if (allowsDecimalPlaces)
            {
                // Introduce more than one decimal point
                numericField.Text = string.Empty;
                keyboard.Enter("1..2");
            }
            else
            {
                numericField.Text = string.Empty;
                keyboard.Enter("1.");
            }
        }

        /// <summary>
        /// Verifies that the correct validations are applied on the specified text fields.
        /// </summary>
        /// <param name="parentWindow">The parent window of the text box.</param>
        /// <param name="textField">The Text field.</param>
        /// <param name="maxTextLength">The max allowed length of the text.</param>
        public static void CheckTextFieldValidators(Window parentWindow, TextBox textField, int maxTextLength)
        {
            AttachedKeyboard keyboard = parentWindow.Keyboard;
            textField.Click();

            for (int i = 0; i <= maxTextLength + 1; i++)
            {
                keyboard.Enter("a");
            }
            Assert.IsTrue(textField.Text.Length <= maxTextLength, string.Format("You are allowed to introduce more than {0} characters into the field.", maxTextLength));
        }

        /// <summary>
        /// Verify that the correct validations are applied on the specified numeric field.
        /// </summary>
        /// <param name="parentWindow">The parent window of the text box.</param>
        /// <param name="numericField">The numeric field.</param>
        /// <param name="maxAllowedValue">The max allowed value for the specified numeric field.</param>
        /// <param name="allowsDecimalPlaces">True if the decimal places are allowed, false otherwise.</param>
        public static void VerifyNumericFieldValidators(Window parentWindow, TextBox numericField, int maxAllowedValue, bool allowsDecimalPlaces)
        {
            AttachedKeyboard keyboard = parentWindow.Keyboard;

            // Introduce a string into the numerical field.
            numericField.Text = string.Empty;
            keyboard.Enter("mock");
            Assert.IsTrue(string.IsNullOrEmpty(numericField.Text), string.Format("Your are allowed to introduce strings into the {0} text box", numericField.Name));

            // Paste a string into the numerical field
            numericField.Text = string.Empty;
            System.Windows.Clipboard.SetText("mock");

            Menu pasteMenuItem = numericField.GetContextMenuByText(parentWindow, "Paste");

            pasteMenuItem.Click();
            Assert.IsTrue(string.IsNullOrEmpty(numericField.Text), string.Format("Your are allowed to introduce strings into the {0} text box", numericField.Name));

            // Introduce a value greater than the max allowed value
            numericField.Text = string.Empty;
            keyboard.Enter(maxAllowedValue.ToString() + "0");
            Assert.IsTrue(numericField.Text == maxAllowedValue.ToString(), string.Format("You are allowed to introduce a value greater than the max allowed value into {0} text box", numericField));

            if (allowsDecimalPlaces)
            {
                // Introduce more than one decimal point
                numericField.Text = string.Empty;
                //keyboard.Enter("1..2");
                keyboard.Enter("1.2");
                Assert.IsTrue(numericField.Text == "1.2", string.Format("You are allowed to introduce more than one decimal point into {0} text box", numericField.Name));
            }
            else
            {
                numericField.Text = string.Empty;
                keyboard.Enter("1.");
                Assert.IsTrue(numericField.Text == "1", string.Format("You are allowed to introduce decimal points into the {0} text box.", numericField.Name));
            }
        }
    }
}
