﻿@SaveDocumentsToDisk
Feature: SaveDocumentsToDisk

Scenario: Save Documents To Disk
	Given The documents tab is open
	And I select a document to save to disk
	When I click on save button
	Then the file is saved
