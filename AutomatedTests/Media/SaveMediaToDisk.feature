﻿@SaveMediaToDisk
Feature: SaveMediaToDisk

Scenario: Save Media To Disk_Full Screen Off
	Given I select a comodity
	And I add a picture to media panel
	And I press the Save As button
	When I click the Save 
	Then the picture is saved

Scenario: Save Media To Disk_Full Screen On
	Given I select a comodity	
	And I add a picture to media panel
	And I click the Full Screen image button
	And I press the Save As button
	When I click the Save 
	Then the picture is saved
