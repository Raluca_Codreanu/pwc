﻿@ValidateAddDocument
Feature: ValidateAddDocument

Scenario: Add more than 10 Document
	Given I select the imported project
	And I select its Doc tab
	And I add 10 docs
	When I press ok 
	Then I cancel all the changes

	
Scenario: Add a document which exceeds 50MB
	Given I select the imported project
	And I select its Doc tab
	And I a document 
	When I press ok in error message
	Then the error is closed




