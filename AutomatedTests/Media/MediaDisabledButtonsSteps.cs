﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using System.IO;
using System.Windows.Automation;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.Media
{
    [Binding]
    public class MediaDisabledButtonsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("MediaDisabledButtons")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\Lear.project"));
            mainWindow.WaitForAsyncUITasks();

            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.SelectEx();
            myProjectsnode.Collapse();

            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject); 
            importAnotherProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDocs.project"));
            mainWindow.WaitForAsyncUITasks();
            myProjectsNode.Collapse();
        }

        [AfterFeature("MediaDisabledButtons")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I delete a project")]
        public void GivenIDeleteAProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("Lear");
            projectNode.SelectEx();

            Assert.IsNotNull(projectNode, "projectNode node was not found.");
            Menu deleteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I select the Trash Bin node")]
        public void GivenISelectTheTrashBinNode()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
            trashBinNode.Click();

            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

            ListViewRow project1Row = trashBinDataGrid.Row("Name", "Lear");
            Assert.IsNotNull(project1Row, "Project1Row trash bin item was not found.");

            ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, project1Row);
            AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
            selectedCheckBox.Checked = true;
        }
        
        [Given(@"I select the project to see details")]
        public void GivenISelectTheProjectToSeeDetails()
        {
            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();   
        }

        [Given(@"I select a project which has no images")]
        public void GivenISelectAProjectWhichHasNoImages()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDocs");
            projectNode.SelectEx();
        }
                
        [When(@"I check the media control")]
        public void WhenICheckTheMediaControl()
        {
            var mediaControl = mainWindow.Get<WPFControl>("MediaViewId");
            AttachedMouse mouse = mainWindow.Mouse;

            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();
            mouse.Location = new System.Windows.Point(mediaControl.Bounds.X + 20, mediaControl.Bounds.Y +20);
        }
        
        [Then(@"all the buttons are disabled at least Toggle on/off")]
        public void ThenAllTheButtonsAreDisabledAtLeastToggleOnOff()
        {

            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsFalse(deleteMediaButton.Enabled, "Media panel should be empty");

            Button addMediaButton = Wait.For(() => mainWindow.Get<Button>("AddMediaButton"));
            Assert.IsFalse(addMediaButton.Enabled, "Media panel should be empty");

            Button saveMediaButton = Wait.For(() => mainWindow.Get<Button>("SaveAsButton"));
            Assert.IsFalse(saveMediaButton.Enabled, "Media panel should be empty");

            // Toogle on/off button is enable
            Button toogleOnOffButton = Wait.For(() => mainWindow.Get<Button>("ToggleFullScreenButton"));
            mainWindow.GetMediaControl().ClickFullScreen();
            Assert.IsTrue(toogleOnOffButton.Enabled, "previewMedia Button should be empty");

            Button previewMediaButton = Wait.For(() => mainWindow.Get<Button>("PreviewMediaButton"));
            Assert.IsTrue(previewMediaButton.Enabled, "previewMedia Button should be empty");

            Button pasteButton = mainWindow.Get<Button>("PasteButton");
            Assert.IsFalse(pasteButton.Enabled, "Paste button should be disabled");
            
            Button nextButton = mainWindow.Get<Button>("NextMediaButton");
            Assert.IsFalse(nextButton.Enabled, "Paste button should be disabled");

            Button previousButton = mainWindow.Get<Button>("PreviousMediaButton");
            Assert.IsFalse(previousButton.Enabled, "Paste button should be disabled");

            mainWindow.GetMediaControl().ClickFullScreen();                   
        }

        [Then(@"all the buttons are disabled at least the Add")]
        public void ThenAllTheButtonsAreDisabledAtLeastTheAdd()
        {
            Button deleteMediaButton = Wait.For(() => mainWindow.Get<Button>("DeleteMediaButton"));
            Assert.IsFalse(deleteMediaButton.Enabled, "Media panel should be empty");

            Button addMediaButton = Wait.For(() => mainWindow.Get<Button>("AddMediaButton"));
            Assert.IsTrue(addMediaButton.Enabled, "Media panel should be empty");

            Button saveMediaButton = Wait.For(() => mainWindow.Get<Button>("SaveAsButton"));
            Assert.IsFalse(saveMediaButton.Enabled, "Media panel should be empty");

            Button toggleMediaButton = Wait.For(() => mainWindow.Get<Button>("ToggleFullScreenButton"));
            Assert.IsFalse(toggleMediaButton.Enabled, "Media panel should be empty");

            Button previewMediaButton = Wait.For(() => mainWindow.Get<Button>("PreviewMediaButton"));
            Assert.IsFalse(previewMediaButton.Visible, "previewMedia Button should be empty");

            Button pasteButton = mainWindow.Get<Button>("PasteButton");
            Assert.IsFalse(pasteButton.Enabled, "Paste button should be disabled");

            Button nextButton = mainWindow.Get<Button>("NextMediaButton");
            Assert.IsFalse(nextButton.Visible, "Next button button should be disabled");

            Button previousButton = mainWindow.Get<Button>("PreviousMediaButton");
            Assert.IsFalse(previousButton.Visible, "Previous button should be hidden");      
        }
    }
}
        

