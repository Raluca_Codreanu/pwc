﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using System.IO;

namespace ZPKTool.AutomatedTests.Media
{
    [Binding]
    public class SaveMediaToDiskSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("SaveMediaToDisk")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectSingleMedia.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("SaveMediaToDisk")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a comodity")]
        public void GivenISelectAComodity()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectSingleMedia");
            projectNode.ExpandEx();

            TreeNode partNode = projectNode.GetNode("partSingleMedia");
            partNode.ExpandEx();

            TreeNode materialsNode = partNode.GetNode("Materials");
            materialsNode.ExpandEx();

            TreeNode commodityNode = materialsNode.GetNode("singleMedia");
            commodityNode.SelectEx();
        }
        
        [Given(@"I add a picture to media panel")]
        public void GivenIAddAPictureToMediaPanel()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            mainWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);
        }

        [Given(@"I click the Full Screen image button")]
        public void GivenIClickTheFullScreenImageButton()
        {
            var mediaControl = mainWindow.GetMediaControl();
            mediaControl.ClickFullScreen();
        }

        [Given(@"I press the Save As button")]
        public void GivenIPressTheSaveAsButton()
        {
            var mediaControl = mainWindow.GetMediaControl();
            mediaControl.ClicSaveAs();
        }
                
        [When(@"I click the Save")]
        public void WhenIClickTheSave()
        {
            string path = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".png");
            mainWindow.GetSaveFileDialog().SaveFile(path);
            mainWindow.WaitForAsyncUITasks();

        }
        
        [Then(@"the picture is saved")]
        public void ThenThePictureIsSaved()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectSingleMedia");
            projectNode.SelectEx();
        }
    }
}
