﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ValidateAddDocumentSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("ValidateAddDocument")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\Lear.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ValidateAddDocument")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the imported project")]
        public void GivenISelectTheImportedProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("Lear");
            projectNode.SelectEx();
        }
        
        [Given(@"I select its Doc tab")]
        public void GivenISelectItsDocTab()
        {
            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();  
        }
        
        [Given(@"I add (.*) docs")]
        public void GivenIAddDocs(int p0)
        {
            Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
            for (int i = 0; i <= p0; i++)
            {
                addDocumentsButton.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
            }
        }
        
        [Given(@"I a document")]
        public void GivenIADocument()
        {
            Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
            addDocumentsButton.Click();
            
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\mouser.pdf"));   
        }
        
        [When(@"I press ok")]
        public void WhenIPressOk()
        {
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
        }

        [When(@"I press ok in error message")]
        public void WhenIPressOkInErrorMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
        }

        [Then(@"I cancel all the changes")]
        public void ThenICancelAllTheChanges()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();

            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Then(@"the error is closed")]
        public void ThenTheErrorIsClosed()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }

    }
}
