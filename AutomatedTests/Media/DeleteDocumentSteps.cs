﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using System.IO;

namespace ZPKTool.AutomatedTests.Media
{
    [Binding]
    public class DeleteDocumentSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteDocument")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDocs.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteDocument")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I have open Documents tab")]
        public void GivenIHaveOpenDocumentsTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDocs");
            projectNode.SelectEx();

            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();
        }
        
        [Given(@"I click the Delete button")]
        public void GivenIClickTheDeleteButton()
        {
            Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
            deleteButton.Click();
        }
        
        [When(@"I press save button")]
        public void WhenIPressSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Then(@"the project is saved")]
        public void ThenTheProjectIsSaved()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDocs");
            projectNode.SelectEx();

            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();
        }
    }
}
