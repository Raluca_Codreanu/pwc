﻿@MediaDisabledButtons
Feature: MediaDisabledButtons

Scenario: Disabled Buttons in Trash Bin
	Given I delete a project
	And I select the Trash Bin node
	And I select the project to see details 
	When I check the media control 
	Then all the buttons are disabled at least Toggle on/off

Scenario: Disabled Buttons - Media no pictures
	Given I select a project which has no images	
	When I check the media control 
	Then all the buttons are disabled at least the Add