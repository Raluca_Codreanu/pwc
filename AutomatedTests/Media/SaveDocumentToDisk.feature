﻿@SaveDocumentToDisk
Feature: SaveDocumentToDisk

Scenario Outline: Save Document To Disk
	Given I have opened the Documents tab
	And I select a <file> to save document to Disk
	When I click Save
	Then the document is saved

Examples: 
	| file              |
	| 555.xls           |
	| Koala.jpg         |
	| myPart.pdf        |
	| project1.project  |
	| ZPK_Custom Log.doc|
