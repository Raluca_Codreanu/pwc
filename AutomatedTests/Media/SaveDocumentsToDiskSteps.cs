﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using ZPKTool.AutomatedTests.ConsumableTests;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class SaveDocumentsToDiskSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("SaveDocumentsToDisk")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDocs.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("SaveDocumentsToDisk")]
        private static void KillApplication()
        {
            application.Kill();
        }
        [Given(@"The documents tab is open")]
        public void GivenTheDocumentsTabIsOpen()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDocs");
            projectNode.SelectEx();

            TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
            documentsTab.Click();
        }
        
        [Given(@"I select a document to save to disk")]
        public void GivenISelectADocumentToSaveToDisk()
        {
            Button saveToDiskButton = mainWindow.Get<Button>("SaveToDiskButton");
            saveToDiskButton.Click();
            
        }
        
        [When(@"I click on save button")]
        public void WhenIClickOnSaveButton()
        {
            string path = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            mainWindow.GetSaveFileDialog().SaveFile(path);
            mainWindow.WaitForAsyncUITasks();

            Button saveToDiskButton2 = mainWindow.Get<Button>("SaveToDiskButton");
            saveToDiskButton2.Click();

            path = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            mainWindow.GetSaveFileDialog().SaveFile(path);
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the file is saved")]
        public void ThenTheFileIsSaved()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectDocs");
            projectNode.SelectEx();
        }
    }
}
