﻿@ImportExportDie
Feature: ImportExportDie

Scenario: Import Export Die
	Given I open the Die tab from Process step
	And I select a die, press right click to export it
	And I press Cancel in Save As window
	And I select a die, press right click to export it
	And I press Save btn
	When I select the step node to import die
	Then the die is imported into the Tooling datagrid
