﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.DieTests
{
    [Binding]
    public class CreateDieSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("CreateDie")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CreateDie")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Dies tab from process step")]
        public void GivenISelectTheDiesTabFromProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I press the Add button from tab")]
        public void GivenIPressTheAddButtonFromTab()
        {
            Button addConsumableButton = mainWindow.Get<Button>(DieAutomationIds.AddDieButton, true);
            addConsumableButton.Click();      
        }
        
        [Given(@"I fill all mandatory fields")]
        public void GivenIFillAllMandatoryFields()
        {
            Window createDieWindow = application.Windows.DieWindow;

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "die" + DateTime.Now.Ticks;

            createDieWindow = application.Windows.DieWindow;
            ComboBox dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
            Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
            dieTypeComboBox.SelectItem(2);

            TextBox investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
            investmentTextBox.Text = "11";

            TextBox reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
            reusableInvestTextBox.Text = "11";

            CheckBox wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
            wearKValueCheckBox.Checked = false;

            TextBox wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
            wearTextBox.Text = "11";

            CheckBox maintenanceKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.MaintenanceKValueCheckBox);
            maintenanceKValueCheckBox.Checked = false;

            TextBox maintenanceTextBox = mainWindow.Get<TextBox>(DieAutomationIds.MaintenanceTextBox);
            maintenanceTextBox.Text = "11";

            TextBox lifeTimeTextBox = mainWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox);
            lifeTimeTextBox.Text = "11";

            TabPage allocationOfCostTab = mainWindow.Get<TabPage>(AutomationIds.CostAllocationTabItem);
            allocationOfCostTab.Click();

            TextBox ratioOfAllocation = mainWindow.Get<TextBox>(DieAutomationIds.RatioOfAllocation);
            ratioOfAllocation.Text = "11";

            CheckBox allDiesPaydByCustomerCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.AllDiesPaydByCustomerCheckBox);
            allDiesPaydByCustomerCheckBox.Checked = false;
            TextBox numberOfDiesPaidByCustomerTextBox = mainWindow.Get<TextBox>(DieAutomationIds.NumberOfDiesPaidByCustomerTextBox);
            numberOfDiesPaidByCustomerTextBox.Text = "11";

            CheckBox costAllocationBasedOnPartsPerLifeTimeCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.CostAllocationBasedOnPartsPerLifeTimeCheckBox);
            costAllocationBasedOnPartsPerLifeTimeCheckBox.Checked = false;
            TextBox costAllocationBasedOnNbOfPartsTextBox = mainWindow.Get<TextBox>(DieAutomationIds.CostAllocationBasedOnNbOfPartsTextBox);
            costAllocationBasedOnNbOfPartsTextBox.Text = "11";

            TextBox remarkTextBox = mainWindow.Get<TextBox>(DieAutomationIds.RemarkTextBox);
            remarkTextBox.Text = "remark";

            TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
            manufacturerTab.Click();

            TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            TextBox descriptionTextBox = mainWindow.Get<TextBox>(DieAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
            descriptionTextBox.Text = "description of manufacturer";

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
        
        [Given(@"I fill some of the Create Die window's fields")]
        public void GivenIFillSomeOfTheCreateDieWindowSFields()
        {
            Window createDieWindow = application.Windows.DieWindow;

            TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "die" + DateTime.Now.Ticks;

            createDieWindow = application.Windows.DieWindow;
            ComboBox dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
            Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
            dieTypeComboBox.SelectItem(2);    
        }
        
        [Given(@"I press the cancel")]
        public void GivenIPressTheCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press No in Question window")]
        public void GivenIPressNoInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [When(@"I press the save button")]
        public void WhenIPressTheSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I press the Cancel button")]
        public void WhenIPressTheCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I press Yes button in Question window")]
        public void WhenIPressYesButtonInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the Dies data grid contains a new record")]
        public void ThenTheDiesDataGridContainsANewRecord()
        {
            ListViewControl diesDataGrid = mainWindow.GetListView(DieAutomationIds.DiesDataGrid);           
            Assert.IsTrue(diesDataGrid.Rows.Capacity > 0, "Test");      
        }
        
        [Then(@"Create Die window is closed")]
        public void ThenCreateDieWindowIsClosed()
        {
            Window createDieWindow = application.Windows.DieWindow;
            Assert.IsNull(createDieWindow, "Create die window should be closed");
        }
    }
}

