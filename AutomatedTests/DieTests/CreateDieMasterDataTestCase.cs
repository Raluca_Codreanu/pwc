﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.DieTests
{
    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion Additional test attributes

    /// <summary>
    /// Summary description for CreateDieMasterDataTestCase
    /// </summary>
    [TestClass]
    public class CreateDieMasterDataTestCase
    {
        public static Window mainWindow;
        [TestMethod]
        [TestCategory("Die")]
        public void CreateDieMasterDataTest()
        {

            using (ApplicationEx application = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window createDieWindow = application.Windows.DieWindow;

                mainWindow = application.Windows.MainWindow;

                TreeNode diesNode = application.MainScreen.ProjectsTree.MasterDataDies;
                Assert.IsNotNull(diesNode, "The Dies node was not found.");

                diesNode.Click();
                ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Dies data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();

                TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "die" + DateTime.Now.Ticks;

                createDieWindow = application.Windows.DieWindow;
                ComboBox dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
                Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
                dieTypeComboBox.SelectItem(3);

                TextBox investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "11";

                TextBox reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "11";

                CheckBox wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = false;

                TextBox wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
                wearTextBox.Text = "11";

                CheckBox maintenanceKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.MaintenanceKValueCheckBox);
                maintenanceKValueCheckBox.Checked = false;

                TextBox maintenanceTextBox = mainWindow.Get<TextBox>(DieAutomationIds.MaintenanceTextBox);
                maintenanceTextBox.Text = "11";

                TextBox lifeTimeTextBox = mainWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "11";

                TabPage allocationOfCostTab = mainWindow.Get<TabPage>(AutomationIds.CostAllocationTabItem);
                allocationOfCostTab.Click();

                TextBox ratioOfAllocation = mainWindow.Get<TextBox>(DieAutomationIds.RatioOfAllocation);
                ratioOfAllocation.Text = "11";

                CheckBox allDiesPaydByCustomerCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.AllDiesPaydByCustomerCheckBox);
                allDiesPaydByCustomerCheckBox.Checked = false;
                TextBox numberOfDiesPaidByCustomerTextBox = mainWindow.Get<TextBox>(DieAutomationIds.NumberOfDiesPaidByCustomerTextBox);
                numberOfDiesPaidByCustomerTextBox.Text = "11";

                CheckBox costAllocationBasedOnPartsPerLifeTimeCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.CostAllocationBasedOnPartsPerLifeTimeCheckBox);
                costAllocationBasedOnPartsPerLifeTimeCheckBox.Checked = false;
                TextBox costAllocationBasedOnNbOfPartsTextBox = mainWindow.Get<TextBox>(DieAutomationIds.CostAllocationBasedOnNbOfPartsTextBox);
                costAllocationBasedOnNbOfPartsTextBox.Text = "11";

                TextBox remarkTextBox = mainWindow.Get<TextBox>(DieAutomationIds.RemarkTextBox);
                remarkTextBox.Text = "remark";

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox descriptionTextBox = mainWindow.Get<TextBox>(DieAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();
            }
        }

        [TestMethod]
        [TestCategory("Die")]
        public void CancelCreateDieMDTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode diesNode = app.MainScreen.ProjectsTree.MasterDataDies;
                Assert.IsNotNull(diesNode, "The Dies node was not found.");
                diesNode.Click();

                ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Dies data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 1
                Button addButton = mainWindow.Get<Button>(AutomationIds.AddButton);
                Assert.IsNotNull(addButton, "The Add button was not found.");
                addButton.Click();
                Window createDieWindow = app.Windows.DieWindow;

                //step 2
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3                
                addButton.Click();

                createDieWindow = app.Windows.DieWindow;
                TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "die" + DateTime.Now.Ticks;

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 4
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 6                
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }
    }
}


