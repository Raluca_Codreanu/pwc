﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using System.IO;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ImportExportDieSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string dieExportPath;

        public ImportExportDieSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportDieSteps.dieExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".die");
            if (File.Exists(ImportExportDieSteps.dieExportPath))
            {
                File.Delete(ImportExportDieSteps.dieExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportDieSteps.dieExportPath))
            {
                File.Delete(ImportExportDieSteps.dieExportPath);
            }
        }

        [BeforeFeature("ImportExportDie")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportDie")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I open the Die tab from Process step")]
        public void GivenIOpenTheDieTabFromProcessStep()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage diesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            diesTab.Click();
        }
        
        [Given(@"I select a die, press right click to export it")]
        public void GivenISelectADiePressRightClickToExportIt()
        {
            ListViewControl commoditiesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            commoditiesDataGrid.Select("Name", "dieEI");

            ListViewRow row = commoditiesDataGrid.Row("Name", "dieEI");

            Menu exportMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }
        
        [Given(@"I press Cancel in Save As window")]
        public void GivenIPressCancelInSaveAsWindow()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }
        
        [Given(@"I press Save btn")]
        public void GivenIPressSaveBtn()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportDieSteps.dieExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ImportExportDieSteps.dieExportPath), "The commodity was not exported at the specified location.");
        }
        
        [When(@"I select the step node to import die")]
        public void WhenISelectTheStepNodeToImportDie()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            Menu importPartMenuItem = step1Node.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportDie);
            Assert.IsTrue(importPartMenuItem.Enabled, "Import -> Die menu item is disabled.");
            importPartMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\dieEI.die"));
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Then(@"the die is imported into the Tooling datagrid")]
        public void ThenTheDieIsImportedIntoTheToolingDatagrid()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage diesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            diesTab.Click();    
        }
    }
}
