﻿@EditDie
Feature: EditDie

Scenario: Edit die in Process step
	Given I select the Dies tab
	And I select a row from data grid and press the Edit button
	And I update some fields' values
	When I press the save btn
	Then the edit Die window is closed

Scenario: Cancel Edit_no changes
	Given I select the Dies tab
	And I select a row from data grid and press the Edit button
	When I press the cancel
	Then the edit Die window is closed

Scenario: Cancel Edit Die
	Given I select the Dies tab
	And I select a row from data grid and press the Edit button
	And I update some fields' values
	And I press cancel button
	And I press the No button in question message
	And I press cancel button
	When I press the yes button in question message
	Then the edit Die window is closed	

Scenario: Edit die using dbl click
	Given I select the Dies tab
	And I select a row and press double-click
	And I update some fields' values
	When I press the save btn
	Then the edit Die window is closed	