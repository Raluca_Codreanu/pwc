﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.DieTests
{
    [Binding]
    public class EditDieSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("EditDie")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("EditDie")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select the Dies tab")]
        public void GivenISelectTheDiesTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage consumablesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            consumablesTab.Click();
        }
        
        [Given(@"I select a row from data grid and press the Edit button")]
        public void GivenISelectARowFromDataGridAndPressTheEditButton()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "die1");

            Button editButton = mainWindow.Get<Button>(DieAutomationIds.EditButton);
            editButton.Click();
        }
        
        [Given(@"I update some fields' values")]
        public void GivenIUpdateSomeFieldsValues()
        {
            Window editDieWindow = application.Windows.DieWindow;

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "die1_Updated";

            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press cancel button")]
        public void GivenIPressCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press the No button in question message")]
        public void GivenIPressTheNoButtonInQuestionMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I select a row and press double-click")]
        public void GivenISelectARowAndPressDouble_Click()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "die2");

            ListViewRow row = diesDataGrid.Row("Name", "die2");
            row.DoubleClick();

            Window editDieWindow = application.Windows.DieWindow;

            TextBox nameTextBox = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
            nameTextBox.Text = "die2_Updated";

            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I press the save btn")]
        public void WhenIPressTheSaveBtn()
        {
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();

            //save the process step
            Button saveButtonProcesStep = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButtonProcesStep.Click();
        }
        
        [When(@"I press the cancel")]
        public void WhenIPressTheCancel()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [When(@"I press the yes button in question message")]
        public void WhenIPressTheYesButtonInQuestionMessage()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Then(@"the edit Die window is closed")]
        public void ThenTheEditDieWindowIsClosed()
        {
            Window editDieWindow = application.Windows.DieWindow;
            Assert.IsNull(editDieWindow, "The Edit window is not closed");
        }
    }
}
