﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.TabItems;
using White.Core.UIItems.Finders;


namespace ZPKTool.AutomatedTests.DieTests
{
    public static class DieTestsHelper
    {
        public static void CreateDie(ApplicationEx app)
        {
            Window createDieWindow = app.Windows.DieWindow;

            TextBox name = createDieWindow.Get<TextBox>(AutomationIds.NameTextBox);
            name.Text = "die" + DateTime.Now.Ticks;

            ComboBox dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
            Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
            dieTypeComboBox.SelectItem(1);

            TextBox investmentTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
            investmentTextBox.Text = "11";

            TextBox reusableInvestTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
            reusableInvestTextBox.Text = "11";

            CheckBox wearKValueCheckBox = createDieWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
            wearKValueCheckBox.Checked = false;

            TextBox wearTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
            wearTextBox.Text = "11";

            CheckBox maintenanceKValueCheckBox = createDieWindow.Get<CheckBox>(DieAutomationIds.MaintenanceKValueCheckBox);
            maintenanceKValueCheckBox.Checked = false;

            TextBox maintenanceTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.MaintenanceTextBox);
            maintenanceTextBox.Text = "11";

            TextBox lifeTimeTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox);
            lifeTimeTextBox.Text = "11";

            TabPage allocationOfCostTab = createDieWindow.Get<TabPage>(AutomationIds.CostAllocationTabItem);
            allocationOfCostTab.Click();

            TextBox ratioOfAllocation = createDieWindow.Get<TextBox>(DieAutomationIds.RatioOfAllocation);
            ratioOfAllocation.Text = "11";

            CheckBox allDiesPaydByCustomerCheckBox = createDieWindow.Get<CheckBox>(DieAutomationIds.AllDiesPaydByCustomerCheckBox);
            allDiesPaydByCustomerCheckBox.Checked = false;
            TextBox numberOfDiesPaidByCustomerTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.NumberOfDiesPaidByCustomerTextBox);
            numberOfDiesPaidByCustomerTextBox.Text = "11";

            CheckBox costAllocationBasedOnPartsPerLifeTimeCheckBox = createDieWindow.Get<CheckBox>(DieAutomationIds.CostAllocationBasedOnPartsPerLifeTimeCheckBox);
            costAllocationBasedOnPartsPerLifeTimeCheckBox.Checked = false;
            TextBox costAllocationBasedOnNbOfPartsTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.CostAllocationBasedOnNbOfPartsTextBox);
            costAllocationBasedOnNbOfPartsTextBox.Text = "11";

            TextBox remarkTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.RemarkTextBox);
            remarkTextBox.Text = "remark";

            TabPage manufacturerTab = createDieWindow.Get<TabPage>(AutomationIds.ManufacturerTab);            
            manufacturerTab.Click();

            TextBox manufacturerName = createDieWindow.Get<TextBox>(AutomationIds.NameTextBox);
            manufacturerName.Text = "manufacturerTest";

            TextBox descriptionTextBox = createDieWindow.Get<TextBox>(DieAutomationIds.DescriptionTextBox);
            Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
            descriptionTextBox.Text = "description of manufacturer";

            Button saveButton = createDieWindow.Get<Button>(AutomationIds.SaveButton, true);
            saveButton.Click();
        }
    }
}
