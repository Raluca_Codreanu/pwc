﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using White.Core.Utility;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.DieTests
{
    /// <summary>
    /// Summary description for CreateDieMasterDataTestCase
    /// </summary>
    [TestClass]
    public class DeleteMasterDataTestCase
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            DeleteMasterDataTestCase.CreateTestData();
        }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        [TestMethod]
        [TestCategory("Die")]
        public void DeleteDieMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                // step 1                
                Window mainWindow = app.Windows.MainWindow;
                                
                // step 3
                TreeNode diesNode = app.MainScreen.ProjectsTree.MasterDataDies; ;
                Assert.IsNotNull(diesNode, "The Manufacturers node was not found.");
                diesNode.Click();

                ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");
                mainWindow.WaitForAsyncUITasks();

                //step 4
                diesDataGrid.Select("Name", "die1");

                //step 5
                Button deleteButton = mainWindow.Get<Button>(AutomationIds.DeleteButton);
                Assert.IsNotNull(deleteButton, "The Delete button was not found.");
                deleteButton.Click();

                // step 6
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                Assert.IsNotNull(diesDataGrid.Row("Name", "die1"), "The supplier was deleted although the Cancel button was clicked.");

                // step 7
                deleteButton.Click();

                // step 8
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                // step 9
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found.");
                trashBinNode.SelectEx();

                var trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");
                Assert.IsNull(trashBinDataGrid.Row("Name", "die1"), "A supplier from Master Data was deleted to the Trash Bin.");

            }
        }

        #region Helper
        /// <summary>
        /// Creates the data used in this test.
        /// </summary>
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Die die1 = new Die();
            die1.Name = "die1";
            die1.SetOwner(adminUser);
            die1.IsMasterData = true;
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            die1.Manufacturer = manufacturer1;

            dataContext.DieRepository.Add(die1);
            dataContext.SaveChanges();
        }

        #endregion Helper

        #region Inner classes

        #endregion Inner classes
    }
}
