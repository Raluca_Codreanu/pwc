﻿@DeleteDie
Feature: Delete Die Process

Scenario: Delete Die
	Given I open the Die tab from process
	And I select a Die 
	And I press the Die button from data grid
	And I press No in question win
	And I press Delete button from data grid
	And I press Yes in question win
	And I open Trash Bin
	And I select the deleted row in tras data grid 
	And I press the View Details
	And I press the Recover
	And I press the yes Button to recover
	When I opened the Die tab 
	Then the data grid contains the deleted Die

Scenario: Delete Die context menu
	Given I open the Die tab from process
	And I select a Die, press right click to delete it
	And I press No in question win
	And I select a Die, press right click to delete it
	And I press Yes Button in Question Window
	And I open Trash Bin
	And I press the Empty Trash Bin button
	When I press the yes
	Then the Die is deleted from trash bin