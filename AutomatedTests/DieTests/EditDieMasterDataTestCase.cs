﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using ZPKTool.DataAccess;
using ZPKTool.Common;
namespace ZPKTool.AutomatedTests.DieTests
{
    /// <summary>
    /// The automated test for Update Die Master Data
    /// </summary>
    [TestClass]
    public class EditDieMasterDataTestCase
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="EditDieMasterDataTestCase"/> class.
        ///// </summary>
        //public EditDieMasterDataTestCase()
        //{
        //}

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            //Helper.CreateDb();
            EditDieMasterDataTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// An automated test for Edit Die Master Data Test Case.
        /// </summary>
        [TestMethod]
        [TestCategory("Die")]
        public void EditDieMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode diesNode = app.MainScreen.ProjectsTree.MasterDataDies;
                Assert.IsNotNull(diesNode, "The Manufacturers node was not found.");

                diesNode.Click();
                ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");
                mainWindow.WaitForAsyncUITasks();
                diesDataGrid.Select("Name", "die1");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();

                Window createDieWindow = app.Windows.DieWindow;

                //step 2
                TextBox name = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                name.Text = "die_Updated";

                createDieWindow = app.Windows.DieWindow;
                ComboBox dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
                Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
                dieTypeComboBox.SelectItem(1);

                //step 3
                TextBox investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "11";

                TextBox reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "11";

                CheckBox wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = false;

                TextBox wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
                wearTextBox.Text = "11";

                CheckBox maintenanceKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.MaintenanceKValueCheckBox);
                maintenanceKValueCheckBox.Checked = false;

                TextBox maintenanceTextBox = mainWindow.Get<TextBox>(DieAutomationIds.MaintenanceTextBox);
                maintenanceTextBox.Text = "11";

                TextBox lifeTimeTextBox = mainWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "11";

                //step 4
                Window dieWindow = app.Windows.DieWindow;
                dieWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);
                var mediaControl = mainWindow.GetMediaControl();
                //step 5
                TabPage allocationOfCostTab = mainWindow.Get<TabPage>(AutomationIds.CostAllocationTabItem);
                allocationOfCostTab.Click();

                TextBox ratioOfAllocation = mainWindow.Get<TextBox>(DieAutomationIds.RatioOfAllocation);
                ratioOfAllocation.Text = "11";

                CheckBox allDiesPaydByCustomerCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.AllDiesPaydByCustomerCheckBox);
                allDiesPaydByCustomerCheckBox.Checked = false;
                TextBox numberOfDiesPaidByCustomerTextBox = mainWindow.Get<TextBox>(DieAutomationIds.NumberOfDiesPaidByCustomerTextBox);
                numberOfDiesPaidByCustomerTextBox.Text = "11";

                CheckBox costAllocationBasedOnPartsPerLifeTimeCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.CostAllocationBasedOnPartsPerLifeTimeCheckBox);
                costAllocationBasedOnPartsPerLifeTimeCheckBox.Checked = false;
                TextBox costAllocationBasedOnNbOfPartsTextBox = mainWindow.Get<TextBox>(DieAutomationIds.CostAllocationBasedOnNbOfPartsTextBox);
                costAllocationBasedOnNbOfPartsTextBox.Text = "11";

                TextBox remarkTextBox = mainWindow.Get<TextBox>(DieAutomationIds.RemarkTextBox);
                remarkTextBox.Text = "remark";

                //step 6
                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox descriptionTextBox = mainWindow.Get<TextBox>(DieAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "Description text box was not found.");
                descriptionTextBox.Text = "description of manufacturer";

                //step 7
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                //step 8
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();

                //step 9
                diesNode = app.MainScreen.ProjectsTree.MasterDataDies;
                Assert.IsNotNull(diesNode, "The Manufacturers node was not found.");

                diesNode.Click();
                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();

                ListViewRow dieRow = diesDataGrid.Row("Name", "die_Updated");
                dieRow.DoubleClick();

                createDieWindow = app.Windows.DieWindow;

                ////step 10

                dieWindow = app.Windows.DieWindow;
                dieWindow.GetMediaControl().ClickDelete();

                //// step 6
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                createDieWindow = app.Windows.DieWindow;
                dieTypeComboBox = createDieWindow.Get<ComboBox>(DieAutomationIds.DieTypeComboBox);
                Assert.IsNotNull(dieTypeComboBox, "Die combo box can not be found");
                dieTypeComboBox.SelectItem(3);

                //step 3
                investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "6";

                reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "6";

                wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = true;

                maintenanceKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.MaintenanceKValueCheckBox);
                maintenanceKValueCheckBox.Checked = true;

                lifeTimeTextBox = mainWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox);
                lifeTimeTextBox.Text = "6";

                saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();
                dieRow = diesDataGrid.Row("Name", "die_Updated");
                ListViewCell dieCell = diesDataGrid.Cell("Name", dieRow);
                dieCell.Click();

                Assert.AreEqual(name.Text, mainWindow.Get<TextBox>(AutomationIds.NameTextBox).Text);
                Assert.AreEqual(investmentTextBox.Text, mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox).Text);
                Assert.AreEqual(lifeTimeTextBox.Text, mainWindow.Get<TextBox>(DieAutomationIds.LifeTimeTextBox).Text);
            }
        }

        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Die die1 = new Die();
            die1.Name = "die1";
            die1.SetOwner(adminUser);
            die1.IsMasterData = true;
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            die1.Manufacturer = manufacturer1;

            dataContext.DieRepository.Add(die1);
            dataContext.SaveChanges();
        }
        #endregion Helper
    }
}
