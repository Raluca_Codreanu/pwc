﻿@CopyDie
Feature: CopyDie

Scenario: Copy Die to another step
	Given I open the Die tab
	And I select a die and copy it
	When I select another step to press Paste
	Then the commdity is copied into this step

Scenario: Copy Die into the same step
	Given I open the Die tab
	And I select a die and copy it
	When I select the grid, right-click and paste
	Then a diw with the name 'copy of' is added

Scenario: Copy Die to Master Data
	Given I open the Die tab
	And I select a die and Copy To Master Data
	When I open the Die from Master Data
	Then a copy of die can be viewed