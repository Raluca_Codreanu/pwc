﻿@CreateDie
Feature: CreateDie

Scenario: Create Die in Process Step
	Given I select the Dies tab from process step
	And I press the Add button from tab
	And I fill all mandatory fields
	When I press the save button
	Then the Dies data grid contains a new record

Scenario: Cancel Create Die_no changes
	Given I select the Dies tab from process step
	And I press the Add button from tab
	When I press the Cancel button
	Then Create Die window is closed

Scenario: Cancel Create Die 
	Given I select the Dies tab from process step
	And I press the Add button from tab
	And I fill some of the Create Die window's fields
	And I press the cancel
	And I press No in Question window
	And I press the cancel
	When I press Yes button in Question window
	Then Create Die window is closed