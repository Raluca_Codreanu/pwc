﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.DieTests
{
    public static class DieAutomationIds
    {
        public const string LastModifiedLabel = "LastModifiedLabel";
        public const string CreatePartWindow = "Create Part";
        public const string InvestmentTextBox = "InvestmentTextBox";
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string CalculationStatusComboBox = "CalculationStatusComboBox";
        public const string EstimatedCostTextBox = "EstimatedCostTextBox";
        public const string CalculationAccuracyComboBox = "CalculationAccuracyComboBox";
        public const string ManufacturingRatioTextBox = "ManufacturingRatioTextBox";
        public const string CalculationApproachComboBox = "CalculationApproachComboBox";
        public const string CalculationVariantComboBox = "CalculationVariantComboBox";
        public const string AllDiesPaydByCustomerCheckBox = "AllDiesPaydByCustomerCheckBox";
        public const string LifeTimeTextBox = "LifeTimeTextBox";
        public const string CostAllocationBasedOnNbOfPartsTextBox = "CostAllocationBasedOnNbOfPartsTextBox";
        public const string CalculatorComboBox = "CalculatorComboBox";
        public const string YearlyProdQtyTextBox = "YearlyProdQtyTextBox";
        public const string RemarkTextBox = "RemarkTextBox";
        public const string PurchasePriceTextBox = "PurchasePriceTextBox";
        public const string TargetPriceTextBox = "TargetPriceTextBox";
        public const string DeliveryTypeComboBox = "DeliveryTypeComboBox";
        public const string AssetRateTextBox = "AssetRateTextBox";
        public const string BrowseCountryButton = "BrowseCountryMasterData";
        public const string CountryTextBox = "CountryTextBox";
        public const string DieTypeComboBox = "DieTypeComboBox";
        public const string ReusableInvestTextBox = "ReusableInvestTextBox";
        public const string WearTextBox = "WearTextBox";
        public const string WearKValueCheckBox = "WearKValueCheckBox";
        public const string MaintenanceTextBox = "MaintenanceTextBox";
        public const string AllocationOfCost = "Allocation of Cost";
        public const string RatioOfAllocation = "RatioOfAllocation";
        public const string CostAllocationBasedOnPartsPerLifeTimeCheckBox = "CostAllocationBasedOnPartsPerLifeTimeCheckBox";
        public const string NumberOfDiesPaidByCustomerTextBox = "NumberOfDiesPaidByCustomerTextBox";
        public const string MaintenanceKValueCheckBox = "MaintenanceKValueCheckBox";
        public const string DiesTabItem = "DiesTabItem";
        public const string ProcessNode = "ProcessNode";
        public const string AddDieButton = "AddDieButton";
        public const string DiesDataGrid = "DiesDataGrid";
        public const string EditButton = "EditButton";
        public const string DeleteButton = "DeleteButton";
    }
}
