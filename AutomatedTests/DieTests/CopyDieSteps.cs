﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyDieSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyDie")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcess.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyDie")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Die tab")]
        public void GivenIOpenThenDieTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step2Node = processNode.GetNode("step2");
            step2Node.SelectEx();

            TabPage diesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            diesTab.Click();
        }
        
        [Given(@"I select a die and copy it")]
        public void GivenISelectADieAndCopyIt()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "dieCopy");

            ListViewRow row = diesDataGrid.Row("Name", "dieCopy");

            Menu copyMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }
                        
        [Given(@"I select a die and Copy To Master Data")]
        public void GivenISelectADieAndCopyToMasterData()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "dieCopy");

            ListViewRow row = diesDataGrid.Row("Name", "dieCopy");

            Menu copyToMDMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.CopyToMasterData);
            copyToMDMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [When(@"I select another step to press Paste")]
        public void WhenISelectAnotherStepToPressPaste()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step1");
            step1Node.SelectEx();

            Menu pasteMenuItem = step1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I select the grid, right-click and paste")]
        public void WhenISelectTheGridRight_ClickAndPaste()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "dieCopy");

            ListViewRow row = diesDataGrid.Row("Name", "dieCopy");

            Menu pasteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [When(@"I open the Die from Master Data")]
        public void WhenIOpenTheDieFromMasterData()
        {
            TreeNode diesNode = application.MainScreen.ProjectsTree.MasterDataDies;
            Assert.IsNotNull(diesNode, "The Dies node was not found.");
            diesNode.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the commdity is copied into this step")]
        public void ThenTheCommdityIsCopiedIntoThisStep()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "dieCopy");    
        }
        
        [Then(@"a diw with the name '(.*)' is added")]
        public void ThenADiwWithTheNameIsAdded(string p0)
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", copyTranslation + "dieCopy");
        }
        
        [Then(@"a copy of die can be viewed")]
        public void ThenACopyOfDieCanBeViewed()
        {
            ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
            Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");
            diesDataGrid.Select("Name", "dieCopy");  
        }
    }
}
