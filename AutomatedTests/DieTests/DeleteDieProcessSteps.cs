﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests.DieTests
{
    [Binding]
    public class DeleteDieProcessSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        [BeforeFeature("DeleteDie")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectProcessDeleteDie.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("DeleteDie")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I open the Die tab from process")]
        public void GivenIOpenTheDieTabFromProcess()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage diesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            diesTab.Click();
        }
        
        [Given(@"I select a Die")]
        public void GivenISelectADie()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            diesDataGrid.Select("Name", "die1");
        }
        
        [Given(@"I press the Die button from data grid")]
        public void GivenIPressTheDieButtonFromDataGrid()
        {
            Button deleteButton = mainWindow.Get<Button>(DieAutomationIds.DeleteButton);
            deleteButton.Click();   
        }
        
        [Given(@"I press No in question win")]
        public void GivenIPressNoInQuestionWin()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press Delete button from data grid")]
        public void GivenIPressDeleteButtonFromDataGrid()
        {
            Button deleteButton = mainWindow.Get<Button>(DieAutomationIds.DeleteButton);
            deleteButton.Click();
        }
        
        [Given(@"I press Yes in question win")]
        public void GivenIPressYesInQuestionWin()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I open Trash Bin")]
        public void GivenIOpenTrashBin()
        {
            TreeNode trashBinNode = application.MainScreen.ProjectsTree.TrashBin;
            trashBinNode.Click();
        }
        
        [Given(@"I select the deleted row in tras data grid")]
        public void GivenISelectTheDeletedRowInTrasDataGrid()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);

            ListViewRow deletedRow = trashBinDataGrid.Row("Name", "die1");

           // ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, deletedRow);
            AutomationElement selectedElement = deletedRow.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
            CheckBox selectedCheckBox = new CheckBox(selectedElement,deletedRow.ActionListener);
            selectedCheckBox.Checked = true;
        }
        
        [Given(@"I press the View Details")]
        public void GivenIPressTheViewDetails()
        {
            Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
            viewDetailsButton.Click();
        }
        
        [Given(@"I press the Recover")]
        public void GivenIPressTheRecover()
        {
            Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
            Assert.IsNotNull(recoverButton, "Recover button was not found.");
            recoverButton.Click();
        }
        
        [Given(@"I press the yes Button")]
        public void GivenIPressTheYesButton()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish
            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }

        [Given(@"I press the yes Button to recover")]
        public void GivenIPressTheYesButtonToRecover()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I select a Die, press right click to delete it")]
        public void GivenISelectADiePressRightClickToDeleteIt()
        {
            ListViewControl consumablesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            consumablesDataGrid.Select("Name", "dieDelete");

            ListViewRow row = consumablesDataGrid.Row("Name", "dieDelete");

            Menu deleteMenuItem = row.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }

        [Given(@"I press Yes Button in Question Window")]
        public void IPressYesButtonInQuestionWindow()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

            mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }

        
        [Given(@"I press the Empty Trash Bin button")]
        public void GivenIPressTheEmptyTrashBinButton()
        {
            Button emptyTrashBin = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
            emptyTrashBin.Click();
        }
        
        [When(@"I opened the Die tab")]
        public void WhenIOpenedTheDieTab()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("projectProcess");
            projectNode.ExpandEx();

            TreeNode assemblyNode = projectNode.GetNode("assemblyProcess");
            assemblyNode.SelectEx();
            assemblyNode.ExpandEx();

            TreeNode processNode = assemblyNode.GetNodeByAutomationId(DieAutomationIds.ProcessNode);
            processNode.SelectEx();
            processNode.ExpandEx();

            TreeNode step1Node = processNode.GetNode("step2");
            step1Node.SelectEx();

            TabPage diesTab = mainWindow.Get<TabPage>(DieAutomationIds.DiesTabItem);
            diesTab.Click();
        }
        
        [When(@"I press the yes")]
        public void WhenIPressTheYes()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the data grid contains the deleted Die")]
        public void ThenTheDataGridContainsTheDeletedDie()
        {
            ListViewControl diesDataGrid = Wait.For(() => mainWindow.GetListView(DieAutomationIds.DiesDataGrid));
            ListViewRow row = diesDataGrid.Row("Name", "die1");

            Assert.IsNotNull(row, "row exists");
        }
        
        [Then(@"the Die is deleted from trash bin")]
        public void ThenTheDieIsDeletedFromTrashBin()
        {
            ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
            
            Assert.IsNull(trashBinDataGrid.Row("Name", "dieDelete"), "empty trash");
        }
    }
}
