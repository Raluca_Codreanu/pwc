﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using ZPKTool.DataAccess;
using ZPKTool.Common;
namespace ZPKTool.AutomatedTests.DieTests
{
    /// <summary>
    /// The automated test for Update Die Master Data
    /// </summary>
    [TestClass]
    public class CancelUpdateDieMasterDataTestCase
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="EditDieMasterDataTestCase"/> class.
        ///// </summary>
        //public EditDieMasterDataTestCase()
        //{
        //}

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            //Helper.CreateDb();
            CancelUpdateDieMasterDataTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes
        /// <summary>
        /// An automated test for Cancel Update Die Master Data Test Case
        /// </summary>
        [TestMethod]
        [TestCategory("Die")]
        public void CancelUpdateDieMasterDataTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode diesNode = app.MainScreen.ProjectsTree.MasterDataDies;
                Assert.IsNotNull(diesNode, "The Manufacturers node was not found.");

                diesNode.Click();
                mainWindow.WaitForAsyncUITasks();
                ListViewControl diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");
                diesDataGrid.Select("Name", "die1");

                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();

                //step 2
                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                //step 3
                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();

                diesDataGrid.Select("Name", "die1");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();

                Window createDieWindow = app.Windows.DieWindow;

                TextBox investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "99";

                TextBox reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "99";

                CheckBox wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = false;

                TextBox wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
                wearTextBox.Text = "99";

                TabPage manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                TextBox manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();

                //step 4
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 5                
                cancelButton.Click();

                // press X button in order to close the message window
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();

                //step 6                
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks();

                //step 7
                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();

                diesDataGrid.Select("Name", "die1");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();
                createDieWindow = app.Windows.DieWindow;

                //step 8
                cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();



                //step 9
                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();

                diesDataGrid.Select("Name", "die1");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();
                createDieWindow = app.Windows.DieWindow;

                investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "99";

                reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "99";

                wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = false;

                wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
                wearTextBox.Text = "99";

                manufacturerTab = mainWindow.Get<TabPage>(AutomationIds.ManufacturerTab);
                manufacturerTab.Click();

                manufacturerName = mainWindow.Get<TextBox>(AutomationIds.NameTextBox);
                manufacturerName.Text = "manufacturerTest";

                TextBox manufacturerDesc = mainWindow.Get<TextBox>(DieAutomationIds.DescriptionTextBox);
                manufacturerDesc.Text = "description";

                //step 10
                cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                //step 11
                diesDataGrid = mainWindow.GetListView(AutomationIds.ManageMasterDataGrid);
                Assert.IsNotNull(diesDataGrid, "Manage Master Data grid was not found.");

                mainWindow.WaitForAsyncUITasks();

                diesDataGrid.Select("Name", "die1");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                Assert.IsNotNull(editButton, "The Delete button was not found.");
                editButton.Click();
                createDieWindow = app.Windows.DieWindow;

                investmentTextBox = mainWindow.Get<TextBox>(DieAutomationIds.InvestmentTextBox);
                investmentTextBox.Text = "99";

                reusableInvestTextBox = mainWindow.Get<TextBox>(DieAutomationIds.ReusableInvestTextBox);
                reusableInvestTextBox.Text = "99";

                wearKValueCheckBox = mainWindow.Get<CheckBox>(DieAutomationIds.WearKValueCheckBox);
                wearKValueCheckBox.Checked = false;

                wearTextBox = mainWindow.Get<TextBox>(DieAutomationIds.WearTextBox);
                wearTextBox.Text = "99";

                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
        }


        #region Helper
        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            Die die1 = new Die();
            die1.Name = "die1";
            die1.SetOwner(adminUser);
            die1.IsMasterData = true;
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = "Manufacturer" + manufacturer1.Guid.ToString();

            die1.Manufacturer = manufacturer1;

            dataContext.DieRepository.Add(die1);
            dataContext.SaveChanges();
        }
        #endregion Helper
    }
}
