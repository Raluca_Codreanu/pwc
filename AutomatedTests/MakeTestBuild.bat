@echo off
REM Create a PROimpens build from the local source code for the Automated Tests

rmdir /s /q TestBuild
mkdir TestBuild

@echo on
"c:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.com" "..\ZPKTool\ZPKTool.sln" /Rebuild "Debug|x86"
@echo off
if NOT %ERRORLEVEL% == 0 GOTO BuildError
GOTO CopyBuild

:BuildError
@echo on
"c:\Program Files\Microsoft Visual Studio 11.0\Common7\IDE\devenv.com" "..\ZPKTool\ZPKTool.sln" /Rebuild "Debug|x86"
@echo off
if %ERRORLEVEL% == 0 GOTO CopyBuild
GOTO BuildError2

:CopyBuild
robocopy ..\ZPKTool\ZPKTool.Gui\bin\Debug TestBuild /E

IF "%1" == "nodb" GOTO End

REM Create the databases
"../3rdParty\DAC\SqlPackage.exe" /Action:Publish /SourceFile:"..\ZPKTool\ZPKTool.Database\bin\debug\ZPKTool.Database.dacpac" /TargetServerName:"localhost\sqlexpress" /TargetDatabaseName:"ZPKTool" /p:CreateNewDatabase=True /p:BackupDatabaseBeforeChanges=False /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True

"../3rdParty\DAC\SqlPackage.exe" /Action:Publish /SourceFile:..\ZPKTool\ZPKTool.Database.Central\bin\Debug\ZPKTool.Database.Central.dacpac /TargetServerName:"localhost\sqlexpress" /TargetDatabaseName:"ZPKToolCentral" /p:CreateNewDatabase=True /p:BackupDatabaseBeforeChanges=False /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True /p:IncludeCompositeObjects=True

REM Delete the master data from the central database so it does not interfere with the tests (it is not needed by them)
sqlcmd -S localhost\sqlexpress -d ZPKToolCentral -i Resources\DeleteMasterData.sql

GOTO End

:BuildError2
echo A build error occurred so the Test Build was not created
goto End

:End
echo Done.