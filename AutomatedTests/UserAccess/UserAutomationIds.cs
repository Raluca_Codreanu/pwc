﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.UserAccess
{
    public static class UserAutomationIds
    {
        public const string LoggedInAsLabel = "LoggedInAsLabel";         
        public const string NewButton = "NewButton";
        public const string UsernameTextBox = "UsernameTextBox";           
        public const string PasswordBox = "UserPasswordBox";           
        public const string DescriptionTextBox = "DescriptionTextBox";
        public const string ManageUsersDataGrid = "UsersGrid";
        public const string SaveUserButton = "SaveButton";
        public const string AddRoleButton = "AddRoleButton";
        public const string RemoveRoleButton = "RemoveRoleButton";
        public const string AdminRoleItem = "Admin";
        public const string KeyUserRoleItem = "Key User";
        public const string DataManagerRoleItem = "Data Manager";
        public const string ProjectLeaderRoleItem = "Project Leader";
        public const string UserRoleItem = "User";
        public const string ViewerRoleItem = "Viewer";
        public const string AvailableRolesList = "AvailableRolesList";
        public const string CurrentRolesList = "CurrentRolesList";
        public const string CreateUserWindow = "Create User";
        public const string LoginButton = "LoginButton";
        public const string RolesComboBox = "RolesComboBox";
        public const string PasswordTextBox = "PasswordTextBox";
        public const string OkButton = "OKButton";            
        public const string DisabledCheckBox = "DisabledCheckbox";
        public const string ShowDisabledUsersCheckBox = "ShowDisabledUsers";
        public const string UserNameTextBox = "UserNameTextBox";
    }
}
