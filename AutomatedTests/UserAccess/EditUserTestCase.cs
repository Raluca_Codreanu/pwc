﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.TreeItems;
using White.Core.InputDevices;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.Data;
using System.Windows.Automation;
using White.Core.UIItems.Finders;
using System.Threading;
using White.Core.Configuration;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.TabItems;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.UserAccess
{
    /// <summary>
    /// A test class that represents the automated test for Edit User Test Case. 
    /// </summary>
    [TestClass]
    public class EditUserTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditUserTestCase"/> class.
        /// </summary>
        public EditUserTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            //Helper.CreateDb();
            EditUserTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        /// <summary>
        /// Automated test for Edit User Test Case.
        /// </summary>
        [TestMethod]
        public void EditUserTest()
        {
            ApplicationEx app = null;

            try
            {
                // step 1
                app = ApplicationEx.LaunchAndWaitForMainWindow();
                Window mainWindow = app.Windows.MainWindow;

                app.Login("A", "userwithmultipleroles");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.AdminRoleItem);

                // step 2
                app.SkipWelcomeScreen();

                TreeNode manageUsersNode = app.MainScreen.ProjectsTree.ManageUsers;
                manageUsersNode.SelectEx();
                ListViewControl manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);

                // step 3
                manageUsersDataGrid.Select("Username", "B");
                Button editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();

                Window editUserWindow = Wait.For(() => app.Windows.UserWindow);

                // step 4
                TextBox passworTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);
                passworTextBox.Text = "admin";
                TextBox usernameTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
                usernameTextBox.Text = "B_Updated";

                TextBox descriptionTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "Description updated.";

                //// step 5
                TabPage rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);
                rolesTab.Click();

                // step 6
                ListBox availableRolesList = editUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList);
                Button addRoleButton = editUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
                foreach (ListItem roleItem in availableRolesList.Items)
                {
                    roleItem.Click();
                    addRoleButton.Click();
                }

                Assert.IsTrue(availableRolesList.Items.Count == 0, "Failed to remove the roles from Available Roles list.");

                // step 7
                Button updateButton = editUserWindow.Get<Button>(UserAutomationIds.SaveUserButton);
                updateButton.Click();
                Assert.IsNotNull(editUserWindow.IsMessageDialogDisplayed(MessageDialogType.Info), "The Info message was not found");

                // step 8
                editUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                ListViewRow updatedUserRow = manageUsersDataGrid.Row("Username", usernameTextBox.Text);
                ListViewCell descriptionCell = manageUsersDataGrid.Cell("Description", updatedUserRow);
                Assert.AreEqual(descriptionTextBox.Text, descriptionCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "Failed to update the user's description.");

                // step 9
                app.Windows.MainWindow.Close();
                Assert.IsTrue(Wait.For(() => app.HasExited), "Failed to close the application from the Close button.");

                // step 10
                app = ApplicationEx.LaunchAndWaitForMainWindow();
                mainWindow = app.Windows.MainWindow;

                // step 11
                app.Login("B_Updated", "admin");

                // step 12
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.AdminRoleItem);
                app.SkipWelcomeScreen();

                // step 13
                manageUsersNode = app.MainScreen.ProjectsTree.ManageUsers;
                manageUsersNode.SelectEx();
                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);
                manageUsersDataGrid.Select("Username", "A");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();
                editUserWindow = Wait.For(() => app.Windows.UserWindow);

                // step 14
                TextBox passwordTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);
                passwordTextBox.Text = "password";

                usernameTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);

                rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);
                rolesTab.Click();

                ListBox currentRolesList = editUserWindow.Get<ListBox>(UserAutomationIds.CurrentRolesList);
                Button removeRoleButton = editUserWindow.Get<Button>(UserAutomationIds.RemoveRoleButton);
                foreach (ListItem currentRole in currentRolesList.Items)
                {
                    currentRole.Click();
                    removeRoleButton.Click();
                }

                // step 15
                updateButton = editUserWindow.Get<Button>(UserAutomationIds.SaveUserButton);
                updateButton.Click();
                Assert.IsNotNull(editUserWindow.IsMessageDialogDisplayed(MessageDialogType.Error), "The error message was not found.");
                editUserWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 16
                availableRolesList = editUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList);
                availableRolesList.Items.FirstOrDefault(r => r.Text == UserAutomationIds.ViewerRoleItem).Click();

                addRoleButton = editUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
                addRoleButton.Click();

                currentRolesList = editUserWindow.Get<ListBox>(UserAutomationIds.CurrentRolesList);
                Assert.IsTrue(currentRolesList.Items.Count == 1, "Failed to add a role in Current Roles List.");

                // step 17
                updateButton.Click();
                editUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                updatedUserRow = manageUsersDataGrid.Row("Username", usernameTextBox.Text);
                ListViewCell rolesCell = manageUsersDataGrid.Cell("Roles", updatedUserRow);
                string[] actualRoles = rolesCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name.Split(',');
                foreach (ListItem roleItem in currentRolesList.Items)
                {
                    string actualRole = actualRoles.FirstOrDefault(r => r == roleItem.Text);
                    Assert.IsNotNull(actualRole, "Failed to display all roles of the updated user.");
                }

                // step 18
                app.MainMenu.System_Logout.Click();
                app.Login("A", "userwithmultipleroles");

                app.Windows.MainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 19
                app.LoginAndGoToMainScreen("A", "password");

                // step 20
                LoginTestCase.CheckProjectsTreeByUserRole(app, UserRole.Viewer);

                // step 21
                app.MainMenu.System_Logout.Click();
                app.Login("B_Updated", "admin");

                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.AdminRoleItem);
                app.SkipWelcomeScreen();

                // step 22
                manageUsersNode = app.MainScreen.ProjectsTree.ManageUsers;
                manageUsersNode.SelectEx();
                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);
                manageUsersDataGrid.Select("Username", "B_Updated");

                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();

                editUserWindow = Wait.For(() => app.Windows.UserWindow);
                CheckBox disabledCheckBox = editUserWindow.Get<CheckBox>(UserAutomationIds.DisabledCheckBox);
                Assert.IsNotNull(disabledCheckBox, "The Disable check box was not found.");
                Assert.IsFalse(disabledCheckBox.Checked, "A user can disable its own account.");

                // step 23
                editUserWindow.Close();
                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);
                manageUsersDataGrid.Select("Username", "A");
                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();

                editUserWindow = Wait.For(() => app.Windows.UserWindow);

                // step 24
                disabledCheckBox = editUserWindow.Get<CheckBox>(UserAutomationIds.DisabledCheckBox);
                disabledCheckBox.Checked = true;

                updateButton = editUserWindow.Get<Button>(UserAutomationIds.SaveUserButton);
                updateButton.Click();

                editUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                // step 25
                CheckBox showDisabledUsersCheckBox = mainWindow.Get<CheckBox>(UserAutomationIds.ShowDisabledUsersCheckBox);
                Assert.IsNotNull(showDisabledUsersCheckBox, "Show Disabled Users checkbox was not found.");

                showDisabledUsersCheckBox.Checked = true;
                updatedUserRow = manageUsersDataGrid.Row("Username", "A");
                Assert.IsNotNull(updatedUserRow, "The disabled user was not displayed in Manage Users data grid after the 'Show Disabled Users' checkbox was checked.");

                // step 26
                app.MainMenu.System_Logout.Click();
                app.Login("A", "password");
                app.Windows.MainWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                // step 27
                app.Login("B_Updated", "admin");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.AdminRoleItem);
                app.SkipWelcomeScreen();

                app.MainScreen.ProjectsTree.ManageUsers.SelectEx();
                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);
                showDisabledUsersCheckBox = mainWindow.Get<CheckBox>(UserAutomationIds.ShowDisabledUsersCheckBox);
                showDisabledUsersCheckBox.Checked = true;

                // step 28
                manageUsersDataGrid.Select("Username", "A");
                editButton = mainWindow.Get<Button>(AutomationIds.EditButton);
                editButton.Click();

                editUserWindow = Wait.For(() => app.Windows.UserWindow);

                disabledCheckBox = editUserWindow.Get<CheckBox>(UserAutomationIds.DisabledCheckBox);
                disabledCheckBox.Checked = false;

                TextBox userPasswordTextBox = editUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);
                userPasswordTextBox.Text = "password";

                updateButton = editUserWindow.Get<Button>(UserAutomationIds.SaveUserButton);
                updateButton.Click();

                editUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                // step 29
                app.MainMenu.System_Logout.Click();
                app.Login("A", "password");

                Assert.IsTrue(Wait.For(() => app.WelcomeScreen != null), "The welcome screen was not found.");
            }

            finally
            {
                if (app != null && !app.HasExited)
                {
                    app.Dispose();
                }
            }
        }

        #region Helpers

        /// <summary>
        /// Creates data used in the tests.
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User userA = new User();
            userA.Username = "A";
            userA.Password = EncryptionManager.Instance.EncodeMD5("userwithmultipleroles");
            userA.Name = "A";

            Country favouriteCountry = dataContext.CountryRepository.GetAll().FirstOrDefault();          
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.Admin)));
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.KeyUser)));
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.DataManager)));
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.ProjectLeader)));
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.User)));
            userA.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.Viewer)));
            dataContext.UserRepository.Add(userA);
            dataContext.SaveChanges();

            User userB = new User();
            userB.Username = "B";
            userB.Password = EncryptionManager.Instance.EncodeMD5("admin");
            userB.Name = "B";            
            userB.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.Admin)));
            dataContext.UserRepository.Add(userB);
            dataContext.SaveChanges();
        }

        #endregion Helpers
    }
}
