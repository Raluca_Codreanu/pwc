﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.InputDevices;
using White.Core.UIItems;
using White.Core.UIItems.Finders;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.AutomatedTests.UserAccess
{
    /// <summary>
    /// A test class that will automate the Login User test case. 
    /// </summary>
    [TestClass]
    public class LoginTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginTestCase"/> class.
        /// </summary>
        public LoginTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            LoginTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test method which automates the steps 1 -> 10 of "Login User" test case.
        /// </summary>
        [TestMethod]
        public void LoginUserTestCase1()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndWaitForMainWindow())
            {
                Window mainWindow = app.Windows.MainWindow;

                TextBox userNameTextBox = mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox);
                Assert.IsNotNull(userNameTextBox, "The user name textbox was not found");
                TextBox passwordTextBox = mainWindow.Get<TextBox>(UserAutomationIds.PasswordTextBox);
                Assert.IsNotNull(passwordTextBox, "The password textbox was not found");
                Button loginButton = mainWindow.Get<Button>(UserAutomationIds.LoginButton);

                userNameTextBox.SetValue("admin1");
                passwordTextBox.SetValue("admin");
                loginButton.Click();
                mainWindow.GetMessageDialog().ClickOk();

                userNameTextBox.SetValue(string.Empty);
                passwordTextBox.SetValue("admin");
                Assert.IsFalse(loginButton.Enabled, "The Login button is not disabled if the Username textbox is empty.");

                passwordTextBox.SetValue(string.Empty);
                userNameTextBox.SetValue("admin");
                Assert.IsFalse(loginButton.Enabled, "The login button is not disabled if the Password textbox is empty.");

                userNameTextBox.SetValue("admin");
                passwordTextBox.SetValue("admin1");
                passwordTextBox.Click();
                loginButton.Click();
                mainWindow.GetMessageDialog().ClickOk();
            }
        }

        /// <summary>
        /// A test method which automates the steps 11 -> 14 of "Login User" test case.
        /// </summary>
        [TestMethod]
        public void LoginUserTestCase2()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                Label loggedInAsLabel = mainWindow.Get<Label>(UserAutomationIds.LoggedInAsLabel);
                Assert.IsNotNull(loggedInAsLabel, "The 'Logged In' label was not found.");

             //   string expectedLoggedInLblText = "Logged in as: Administrator (Admin)";
             //   Assert.AreEqual(expectedLoggedInLblText, loggedInAsLabel.Text, "Failed to displayed the current user's name and role.");
            }
        }

        /// <summary>
        /// A test method which automates the steps 15 -> 21 of "Login User" test case.
        /// </summary>
        [TestMethod]
        public void LoginUserTestCase3()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                CheckProjectsTreeByUserRole(app, UserRole.Admin);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the Key User                
                app.LoginAndGoToMainScreen("key user", "keyuser");
                CheckProjectsTreeByUserRole(app, UserRole.KeyUser);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the Data Manager role.
                app.LoginAndGoToMainScreen("data manager", "datamanager");
                CheckProjectsTreeByUserRole(app, UserRole.DataManager);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the Project Leader role.
                app.LoginAndGoToMainScreen("project leader", "projectleader");
                CheckProjectsTreeByUserRole(app, UserRole.ProjectLeader);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user role.
                app.LoginAndGoToMainScreen("user", "user");
                CheckProjectsTreeByUserRole(app, UserRole.User);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the viewer role.
                app.LoginAndGoToMainScreen("viewer", "viewer");
                CheckProjectsTreeByUserRole(app, UserRole.Viewer);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the Admin role.
                app.Login("user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.AdminRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.Admin);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the Key User role.
                app.Login("user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.KeyUserRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.KeyUser);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the Data Manager role.
                app.Login( "user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.DataManagerRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.DataManager);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the Project Leader role.
                app.Login( "user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.ProjectLeaderRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.ProjectLeader);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the User role.
                app.Login( "user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.UserRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.User);
                app.MainMenu.System_Logout.Click();
                Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");

                // Login with the user that has multiple roles and select the Viewer role.
                app.Login( "user with multiple roles", "user");
                LoginTestCase.SelectRoleItemAndClickOk(app, UserAutomationIds.ViewerRoleItem);
                app.SkipWelcomeScreen();
                CheckProjectsTreeByUserRole(app, UserRole.Viewer);
            }
        }

        #endregion Test Methods

        #region Helper

        /// <summary>
        /// Verify if the corresponding tree view items were displayed in the main tree view.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="role">The role of the current user.</param>
        public static void CheckProjectsTreeByUserRole(ApplicationEx app, UserRole role)
        {
            TreeNode releasedProjectsNode = app.MainScreen.ProjectsTree.ReleasedProjects;
            //TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
            //TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
            //TreeNode otherUsersProjectsNode = app.MainScreen.ProjectsTree.OtherUsersProjectsNode;
            //TreeNode masterDataNode = app.MainScreen.ProjectsTree.MasterData;
            //TreeNode adminNode = app.MainScreen.ProjectsTree.Admin;

            Assert.IsNotNull(releasedProjectsNode, string.Format("The Released Projects node was not found, the current user has {0} right.", role.ToString()));
            switch (role)
            {
                case UserRole.Admin:
                    TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                    TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                    TreeNode otherUsersProjectsNode = app.MainScreen.ProjectsTree.OtherUsersProjectsNode;
                    TreeNode masterDataNode = app.MainScreen.ProjectsTree.MasterData;
                    TreeNode adminNode = app.MainScreen.ProjectsTree.Admin;
                    Assert.IsNotNull(myProjectsNode, string.Format("The My Projects node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(trashBinNode, string.Format("Trash Bin node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(otherUsersProjectsNode, string.Format("Other Users Projects node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(masterDataNode, string.Format("Master Data node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(adminNode, string.Format("Admin node was not found, the current user has admin right.", role.ToString()));
                    break;
                case UserRole.KeyUser:
                    TreeNode myProjectsNodeKeyUser = app.MainScreen.ProjectsTree.MyProjects;
                    TreeNode trashBinNodeKeyUser = app.MainScreen.ProjectsTree.TrashBin;
                    TreeNode otherUsersProjectsNodeKeyUser = app.MainScreen.ProjectsTree.OtherUsersProjectsNode;
                    TreeNode masterDataNodeKeyUser = app.MainScreen.ProjectsTree.MasterData;
                    Assert.IsNotNull(myProjectsNodeKeyUser, string.Format("The My Projects node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(trashBinNodeKeyUser, string.Format("Trash Bin node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(otherUsersProjectsNodeKeyUser, string.Format("Other Users Projects node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(masterDataNodeKeyUser, string.Format("Master Data node was not found, the current user has {0} right.", role.ToString()));
                    break;
                case UserRole.DataManager:
                    TreeNode masterDataNodeMasterData = app.MainScreen.ProjectsTree.MasterData;
                    Assert.IsNotNull(masterDataNodeMasterData, string.Format("Master Data node was not found, the current user has {0} right.", role.ToString()));
                    break;
                case UserRole.ProjectLeader:
                    TreeNode otherUsersProjectsNodeProjectLeader = app.MainScreen.ProjectsTree.OtherUsersProjectsNode;
                    Assert.IsNotNull(otherUsersProjectsNodeProjectLeader, string.Format("Other Users Projects node was not found, the current user has {0} right.", role.ToString()));
                    break;
                case UserRole.User:
                    TreeNode myProjectsNodeUser = app.MainScreen.ProjectsTree.MyProjects;
                    TreeNode trashBinNodeUser = app.MainScreen.ProjectsTree.TrashBin;
                    TreeNode otherUsersProjectsNodeUser = app.MainScreen.ProjectsTree.OtherUsersProjectsNode;
                    Assert.IsNotNull(myProjectsNodeUser, string.Format("The My Projects node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(trashBinNodeUser, string.Format("Trash Bin node was not found, the current user has {0} right.", role.ToString()));
                    Assert.IsNotNull(otherUsersProjectsNodeUser, string.Format("Other Users Projects node was not found, the current user has {0} right.", role.ToString()));
                    break;
                case UserRole.Viewer:
                    TreeNode releasedProjectsNodeViewer = app.MainScreen.ProjectsTree.ReleasedProjects;
                    Assert.IsNotNull(releasedProjectsNodeViewer, string.Format("The Released Projects node was not found, the current user has {0} right.", role.ToString()));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Select the specified item in Role Selection Combobox and click OK button.
        /// </summary>
        /// <param name="app">The current application.</param>
        /// <param name="itemToSelect">The text of the item to be selected.</param>
        public static void SelectRoleItemAndClickOk(ApplicationEx app, string itemToSelect)
        {
            Window mainWindow = app.Windows.MainWindow;
            ComboBox roleSelectionCb = Wait.For(() => mainWindow.Get<ComboBox>(UserAutomationIds.RolesComboBox));
            roleSelectionCb.Click();
            
            Thread.Sleep(200); // Mandatory so the selection on item works.

            ListItem item = roleSelectionCb.Items.FirstOrDefault(i => i.Text == itemToSelect);
            Assert.IsNotNull(item, string.Format("The {0} item was not found in Role selection combo box.", itemToSelect));
            item.Click();

            Thread.Sleep(200); // Mandatory so the click on button works.

            Button okButton = mainWindow.Get<Button>(UserAutomationIds.OkButton);
            Assert.IsTrue(okButton.Enabled, "OK button is disabled.");
            okButton.Click();
        }

        /// <summary>
        /// Creates data used in the tests.
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            User keyUser = new User();
            keyUser.Username = "key user";
            keyUser.Password = EncryptionManager.Instance.EncodeMD5("keyuser");
            keyUser.Name = "Key User";

            Role keyUserRole = dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.KeyUser));
            Country favouriteCountry = dataContext.CountryRepository.GetAll().FirstOrDefault();
            keyUser.Roles.Add(keyUserRole);
            dataContext.UserRepository.Add(keyUser);
            dataContext.SaveChanges();

            User dataManager = new User();
            dataManager.Username = "data manager";
            dataManager.Password = EncryptionManager.Instance.EncodeMD5("datamanager");
            dataManager.Name = "Data Manager";

            Role dataManagerRole = dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.DataManager));
            dataManager.Roles.Add(dataManagerRole);            
            dataContext.UserRepository.Add(dataManager);
            dataContext.SaveChanges();

            User projectLeader = new User();
            projectLeader.Username = "project leader";
            projectLeader.Password = EncryptionManager.Instance.EncodeMD5("projectleader");
            projectLeader.Name = "Project Leader";

            Role projectLeaderRole = dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.ProjectLeader));
            projectLeader.Roles.Add(projectLeaderRole);            
            dataContext.UserRepository.Add(projectLeader);
            dataContext.SaveChanges();

            User user = new User();
            user.Username = "user";
            user.Password = EncryptionManager.Instance.EncodeMD5("user");
            user.Name = "User";

            Role userRole = dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.User));
            user.Roles.Add(userRole);            
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User viewer = new User();
            viewer.Username = "viewer";
            viewer.Password = EncryptionManager.Instance.EncodeMD5("viewer");
            viewer.Name = "Viewer";

            Role viewerRole = dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.Viewer));
            viewer.Roles.Add(viewerRole);            
            dataContext.UserRepository.Add(viewer);
            dataContext.SaveChanges();

            User userWithMultipleRoles = new User();
            userWithMultipleRoles.Username = "user with multiple roles";
            userWithMultipleRoles.Password = EncryptionManager.Instance.EncodeMD5("user");
            userWithMultipleRoles.Name = "User With Multiple Roles";            

            userWithMultipleRoles.Roles.Add(dataContext.RoleRepository.FindAll().FirstOrDefault(r => r.Equals(UserRole.Admin)));
            userWithMultipleRoles.Roles.Add(keyUserRole);
            userWithMultipleRoles.Roles.Add(dataManagerRole);
            userWithMultipleRoles.Roles.Add(projectLeaderRole);
            userWithMultipleRoles.Roles.Add(userRole);
            userWithMultipleRoles.Roles.Add(viewerRole);
            dataContext.UserRepository.Add(userWithMultipleRoles);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
