﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Automation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.InputDevices;
using White.Core.UIItems.Finders;
using White.Core.UIItems.Custom;
using White.Core.UIItems.Actions;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;

namespace ZPKTool.AutomatedTests.UserAccess
{
    /// <summary>
    /// The automated test of Create User Test Case.
    /// </summary>
    [TestClass]
    public class CreateUserTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserTestCase"/> class.
        /// </summary>
        public CreateUserTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// The automated test of Create User Test Case. 
        /// </summary>
        [TestMethod]
        public void CreateUserTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                TreeNode manageUsersNode = app.MainScreen.ProjectsTree.ManageUsers;
                manageUsersNode.Click();

                Button newUserButton = Wait.For(() => mainWindow.Get<Button>(UserAutomationIds.NewButton));
                newUserButton.Click();

                Window createUserWindow = Wait.For(() => app.Windows.UserWindow);

                TextBox usernameTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
                usernameTextBox.Text = "hank" + DateTime.Now.Ticks;

                // Fill the password box.
                TextBox passwordTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);
                passwordTextBox.Text = "hankel9";

                TextBox nameTextBox = createUserWindow.Get<TextBox>(AutomationIds.NameTextBox);
                Assert.IsNotNull(nameTextBox, "The Name text box was not found.");

                TextBox descriptionTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.DescriptionTextBox);
                Assert.IsNotNull(descriptionTextBox, "The Description text box was not found.");

                Button saveButton = createUserWindow.Get<Button>(AutomationIds.SaveButton, true);
                Assert.IsFalse(saveButton.Enabled, "The Create User Button was not found");

                nameTextBox.Text = "Hank" + DateTime.Now.Ticks;
                descriptionTextBox.Text = "Description of user";

                TabPage rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);                
                rolesTab.Click();

                ListBox availableRolesList = Wait.For(() => createUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList));

                ListBox currentRolesList = createUserWindow.Get<ListBox>(UserAutomationIds.CurrentRolesList);
                Assert.IsNotNull(currentRolesList, "The Current Roles list was not found.");

                ListItem adminItem = availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.AdminRoleItem);
                Assert.IsNotNull(adminItem, "The Admin role was not found.");
                adminItem.Click();

                Button addRoleButton = createUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
                Assert.IsNotNull(addRoleButton, "The Add Role button was not found.");
                addRoleButton.Click();

                Assert.IsNotNull(currentRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.AdminRoleItem), "Failed to add the selected item into Current Roles list.");
                Assert.IsNull(availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.AdminRoleItem), "Failed to remove the selected item from the Available Role list.");

                ListItem keyUserItem = availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem);
                Assert.IsNotNull(keyUserItem, "The Key User item was not found in the Available Roles list.");
                keyUserItem.Click();
                addRoleButton.Click();

                Assert.IsNotNull(currentRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem), "Failed to add the selected item into Current Roles list.");
                Assert.IsNull(availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem), "Failed to remove the selected item from the Available Role list.");

                Button removeRoleButton = createUserWindow.Get<Button>(UserAutomationIds.RemoveRoleButton);
                Assert.IsNotNull(removeRoleButton, "The Remove Role button was not found.");

                currentRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.AdminRoleItem).Click();
                removeRoleButton.Click();

                currentRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem).Click();
                removeRoleButton.Click();

                saveButton.Click();
                
                createUserWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();

                Button cancelButton = createUserWindow.Get<Button>(AutomationIds.CancelButton);
                Assert.IsNotNull(cancelButton, "The Cancel button was not found.");

                cancelButton.Click();

                createUserWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                cancelButton.Click();

                createUserWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();

                ListViewControl manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);

                ListViewRow createdUserRow = manageUsersDataGrid.Row("Username", usernameTextBox.Text);
                //Assert.IsNull(createdUserRow, "The user was created although the create operation was cancelled.");

                Wait.For(() => mainWindow.Get<Button>(UserAutomationIds.NewButton)).Click();

                createUserWindow = Wait.For(() => app.Windows.UserWindow);

                string commonUserName = "hank" + DateTime.Now.Ticks;
                usernameTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
                usernameTextBox.Text = commonUserName;

                nameTextBox = createUserWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Hank" + DateTime.Now.Ticks;

                descriptionTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "This is the user: " + nameTextBox.Text;

                passwordTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);                
                passwordTextBox.Text = "hankel9";
                
                rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);                
                rolesTab.Click();

                availableRolesList = createUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList);
                addRoleButton = createUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
                foreach (ListItem item in availableRolesList.Items)
                {
                    item.Click();
                    addRoleButton.Click();
                }

                saveButton = createUserWindow.Get<Button>(AutomationIds.SaveButton, true);
                saveButton.Click();

                createUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);

                createdUserRow = manageUsersDataGrid.Row("Username", usernameTextBox.Text);
                Assert.IsNotNull(createdUserRow, "The user was not found in the data grid.");

                ListViewCell nameCell = manageUsersDataGrid.Cell("Name", createdUserRow);
                Assert.AreEqual(nameTextBox.Text, nameCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The displayed Name is not the created user's name.");

                ListViewCell descriptionCell = manageUsersDataGrid.CellByIndex(2, createdUserRow);
                Assert.AreEqual(descriptionTextBox.Text, descriptionCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The displayed description is different than the description of the created user.");

                ListViewCell rolesCell = manageUsersDataGrid.CellByIndex(3, createdUserRow);
                string[] actualRoles = rolesCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name.Split(',');
                foreach (ListItem roleItem in currentRolesList.Items)
                {
                    string actualRole = actualRoles.FirstOrDefault(r => r == roleItem.Text);
                    Assert.IsNotNull(actualRole, "Failed to display all roles of the updated user.");
                }

                newUserButton = mainWindow.Get<Button>(UserAutomationIds.NewButton);
                newUserButton.Click();

                createUserWindow = Wait.For(() => app.Windows.UserWindow);

                usernameTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
                usernameTextBox.Text = commonUserName;

                nameTextBox = createUserWindow.Get<TextBox>(AutomationIds.NameTextBox);
                nameTextBox.Text = "Nick" + DateTime.Now.Ticks;

                descriptionTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.DescriptionTextBox);
                descriptionTextBox.Text = "This is the user: " + nameTextBox.Text;

                passwordTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);                
                passwordTextBox.Text = "nickolas9";

                rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);                
                rolesTab.Click();

                availableRolesList = createUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList);
                availableRolesList.Items.FirstOrDefault().Click();

                addRoleButton = createUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
                addRoleButton.Click();

                saveButton = createUserWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();

                createUserWindow.GetMessageDialog(MessageDialogType.Error).ClickOk();
                
                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);                
                generalTab.Click();

                usernameTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
                usernameTextBox.Text = "nick" + DateTime.Now.Ticks;
                saveButton.Click();

                createUserWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();

                manageUsersDataGrid = mainWindow.GetListView(UserAutomationIds.ManageUsersDataGrid);
                Assert.IsNotNull(manageUsersDataGrid, "The Manage Users data grid was not found.");

                createdUserRow = manageUsersDataGrid.Row("Username", usernameTextBox.Text);
                Assert.IsNotNull(createdUserRow, "The user was not found in the data grid.");

                nameCell = manageUsersDataGrid.Cell("Name", createdUserRow);
                Assert.AreEqual(nameTextBox.Text, nameCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The displayed Name is not the created user's name.");

                descriptionCell = manageUsersDataGrid.CellByIndex(2, createdUserRow);
                Assert.AreEqual(descriptionTextBox.Text, descriptionCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name, "The displayed description is different than the description of the created user.");

                //rolesCell = manageUsersDataGrid.Cell("Roles", createdUserRow);
                rolesCell = manageUsersDataGrid.CellByIndex(3, createdUserRow);
                actualRoles = rolesCell.GetElement(SearchCriteria.ByControlType(ControlType.Text)).Current.Name.Split(',');
                foreach (ListItem roleItem in currentRolesList.Items)
                {
                    string actualRole = actualRoles.FirstOrDefault(r => r == roleItem.Text);
                    Assert.IsNotNull(actualRole, "Failed to display all roles of the updated user.");
                }
            }
        }

        #endregion Test Methods

        #region Helpers

        #endregion Helpers

        #region Inner classes
        #endregion
    }
}
