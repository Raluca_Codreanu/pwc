﻿using System;
using TechTalk.SpecFlow;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems.WindowItems;
using White.Core.UIItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using White.Core.InputDevices;
using White.Core.UIItems.TabItems;
using ZPKTool.AutomatedTests.PreferencesTests;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.DieTests;
using White.Core.UIItems.Scrolling;


namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class ManageProjectsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("ManageProjects")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

        }

        [AfterFeature("ManageProjects")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I import some projects for Admin user")]
        public void GivenIImportSomeProjectsForAdminUser()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectDocs.project"));
            mainWindow.WaitForAsyncUITasks();

            myProjectsNode.SelectEx();
            myProjectsNode.Collapse();
            Menu importAnotherProject = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importAnotherProject.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\projectRD.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I synchronize my projects for Admin")]
        public void GivenISynchronizeMyProjectsForAdmin()
        {
            application.MainMenu.SyncDataMenuItem.Click();
            var sw = application.Windows.SynchronizationWindow;
            Window synchronizationWindow = Wait.For(() => application.Windows.SynchronizationWindow);

            CheckBox myProjectsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.MyProjectsCheckBox);
            myProjectsCheckBox.Checked = true;

            Button closeButton = synchronizationWindow.Get<Button>("CloseButton");
            Button synchronizeButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.SynchronizeButton);
            synchronizeButton.Click();
           
            //mainWindow.WaitForAsyncUITasks();
            //Wait.For(() => closeButton.Enabled == true);
            while (closeButton.Enabled == false)
            {
                System.Threading.Thread.Sleep(1000);
            }
            if (closeButton.Enabled == true)
            { closeButton.Click(); }
        }

        [When(@"I create a new user")]
        public void WhenICreateANewUser()
        {
            TreeNode manageUsersNode = application.MainScreen.ProjectsTree.ManageUsers;
            manageUsersNode.Click();

            Button newUserButton = Wait.For(() => mainWindow.Get<Button>(UserAutomationIds.NewButton));
            newUserButton.Click();

            Window createUserWindow = Wait.For(() => application.Windows.UserWindow);

            TextBox usernameTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.UsernameTextBox);
            usernameTextBox.Text = "userTest";

            TextBox passwordTextBox = createUserWindow.Get<TextBox>(UserAutomationIds.PasswordBox);
            passwordTextBox.Text = "testFortech";

            TextBox nameTextBox = createUserWindow.Get<TextBox>(AutomationIds.NameTextBox);
            Assert.IsNotNull(nameTextBox, "The Name text box was not found.");
            nameTextBox.Text = "test";

            TabPage rolesTab = mainWindow.Get<TabPage>(AutomationIds.RolesTab);
            rolesTab.Click();

            ListBox availableRolesList = Wait.For(() => createUserWindow.Get<ListBox>(UserAutomationIds.AvailableRolesList));

            ListBox currentRolesList = createUserWindow.Get<ListBox>(UserAutomationIds.CurrentRolesList);
            Assert.IsNotNull(currentRolesList, "The Current Roles list was not found.");

            ListItem adminItem = availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.AdminRoleItem);
            Assert.IsNotNull(adminItem, "The Admin role was not found.");
            adminItem.Click();

            ListItem keyUserItem = availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem);
            Assert.IsNotNull(keyUserItem, "The Key User item was not found in the Available Roles list.");
            keyUserItem.Click();

            Button addRoleButton = createUserWindow.Get<Button>(UserAutomationIds.AddRoleButton);
            Assert.IsNotNull(addRoleButton, "The Add Role button was not found.");
            addRoleButton.Click();
            Assert.IsNotNull(currentRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem), "Failed to add the selected item into Current Roles list.");
            Assert.IsNull(availableRolesList.Items.FirstOrDefault(n => n.Text == UserAutomationIds.KeyUserRoleItem), "Failed to remove the selected item from the Available Role list.");

            Button saveButton = createUserWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk(); 
        }

        [When(@"I Log Out")]
        public void WhenILogOut()
        {
            application.MainMenu.System_Logout.Click();
            Assert.IsTrue(Wait.For(() => mainWindow.Get<TextBox>(UserAutomationIds.UserNameTextBox) != null), "Failed to logout from the application.");
        }

        [When(@"I Log In with the new created user")]
        public void WhenILogInWithTheNewCreatedUser()
        {          
            application.LoginAndGoToMainScreen("userTest", "testFortech");
        }

        [When(@"I import some projects for the new user")]
        public void WhenIImportSomeProjectsForTheNewUser()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\project1.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [When(@"I Synchronize My projects for new user")]
        public void WhenISynchronizeMyProjectsForNewUser()
        {
            application.MainMenu.SyncDataMenuItem.Click();
            var sw = application.Windows.SynchronizationWindow;
            Window synchronizationWindow = Wait.For(() => application.Windows.SynchronizationWindow);

            CheckBox myProjectsCheckBox = application.Windows.SynchronizationWindow.Get<CheckBox>(AutomationIds.MyProjectsCheckBox);
            myProjectsCheckBox.Checked = true;

            Button closeButton = synchronizationWindow.Get<Button>("CloseButton");
            Button synchronizeButton = application.Windows.SynchronizationWindow.Get<Button>(AutomationIds.SynchronizeButton);
            synchronizeButton.Click();
            //mainWindow.WaitForAsyncUITasks();
            //Wait.For(() => closeButton.Enabled == true);
            while (closeButton.Enabled == false)
            {
                System.Threading.Thread.Sleep(1000);
            }
            if (closeButton.Enabled == true)
            { closeButton.Click(); }
        }

        [When(@"I Log In with Admin User")]
        public void WhenILogInWithAdminUser()
        {
            application.LoginAndGoToMainScreen("admin", "admin");
        }

        [When(@"I go to Manage Projets")]
        public void WhenIGoToManageProjets()
        {
            TreeNode manageProjectsNode = application.MainScreen.ProjectsTree.ManageProjects;
            manageProjectsNode.Click();
        }

        [When(@"I select the new User from User list")]
        public void WhenISelectTheNewUserFromUserList()
        {

            ListBox userListBox = mainWindow.Get<ListBox>("UsersListBox");
            var newUserListItem = userListBox.Item(" userTest (test)");
            newUserListItem.Select();
        }

        [When(@"I select a project")]
        public void WhenISelectAProject()
        {
            ListView  assignedProjectsDataGrid = mainWindow.GetListView("ProjectsDataGrid");
            ListViewRow project2Row = assignedProjectsDataGrid.Row("Name", "project1");
            project2Row.Select();
            
        }

        [When(@"I change the owner to Administrator")]
        public void WhenIChangeTheOwnerToAdministrator()
        {
            ComboBox ownerComboBox = mainWindow.Get<ComboBox>("OwnerComboBox");
            ownerComboBox.SelectItem(0);
        }

        [When(@"I click on the save Button")]
        public void WhenIClickOnTheSaveButton()
        {
            Button saveButton = mainWindow.Get<Button>("SaveButton");
            saveButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }

        [When(@"I select admin user from User List")]
        public void WhenISelectAdminUserFromUserList()
        {
            ListBox userListBox = mainWindow.Get<ListBox>("UsersListBox");
            var newUserListItem = userListBox.Item(" admin (Administrator)");
            newUserListItem.Select();
        }

        [Then(@"I verify if the project from the new user is now owned by admin")]
        public void ThenIVerifyIfTheProjectFromTheNewUserIsNowOwnedByAdmin()
        {
            ListView assignedProjectsDataGrid = mainWindow.GetListView("ProjectsDataGrid");
            ListViewRow project1Row = assignedProjectsDataGrid.Row("Name", "project1");
            Assert.IsNotNull(project1Row);
        }
    }
}
