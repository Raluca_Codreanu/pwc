﻿@CancelButton
Feature: CancelButton

Scenario: Cancel Button
	Given I import some projects for admin user
	And I synchronize my projects for admin
	When I create a new User
	When I go to Manage Projets Node
	When I select the admin User from User list
	When I select a Project 
	When I change the owner to the new user
	When I click on Cancel button
	When I click on the Save Button
	Then I verify if the project is still owned by admin