﻿@ManageProjects
Feature: ManageProjects

Scenario: ManageProjects
	Given I import some projects for Admin user
	And I synchronize my projects for Admin
	When I create a new user
	When I Log Out
	When I Log In with the new created user
	When I import some projects for the new user
	When I Synchronize My projects for new user
	When I Log Out
	When I Log In with Admin User
	When I go to Manage Projets
	When I select the new User from User list
	When I select a project 
	When I change the owner to Administrator
	When I click on the save Button
	When I select admin user from User List
	Then I verify if the project from the new user is now owned by admin

