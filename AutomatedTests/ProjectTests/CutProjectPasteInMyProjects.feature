﻿@CutProjectPasteInMyProjects
Feature: CutProjectPasteInMyProjects

Scenario: Cut Project Paste In My Projects
	Given I press right click on a project from My Projects to choose Cut option
	And I press right click to choose Paste on My Projects node	
	And I expand a folder node
	And I press right click on a project to choose Cut option
	When I press right click on a project from My Projects to choose Paste option 
	Then the project is moved in My Projects node
