﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    public static class ProjectAutomationIds
    {
        public const string ProjectFolderNode = "PROimpens.Gui.ViewModels.ProjectFolderTreeItem";
        public const string ProjectNode = "PROimpens.Gui.ViewModels.ProjectTreeItem";
        public const string ProjectNameTextBox = "NameTextBox";
        public const string SupplierNameTextBox = "NameTextBox";
        public const string StartDatePicker = "StartDatePicker";            
        public const string OverheadSetting = "Overhead Settings";            
        public const string LastModifiedLabel = "LastModifiedLabel";
        public const string StatusComboBox = "StatusComboBox";
        public const string TypeComboBox = "TypeComboBox";
        public const string VersionTextBox = "VersionTextBox";
        public const string NumberTextBox = "NumberTextBox";
        public const string BrowseCountryButton = "BrowseCountryMasterData";
        public const string CountryTextBox = "CountryTextBox";
        public const string StateTextBox = "StateTextBox";
        public const string BrowseCountryStateButton = "BrowseStateMasterData";
        public const string ResponsibleCalculatorComboBox = "ResponsibleCalculatorComboBox";
        public const string LeaderComboBox = "LeaderComboBox";
        public const string AnnualProdQty = "AnnualProdQty";
        public const string LifetimeTextBox = "LifetimeTextBox";
        public const string DepartmentTextBox = "DepartmentTextBox";
        public const string AddressTextBox = "AddressTextBox";
        public const string CityTextBox = "CityTextBox";
        public const string DepreciationTextBox = "DepreciationTextBox";
        public const string InterestRateTextBox = "InterestRateTextBox";
        public const string LogisticCostTextBox = "LogisticCostTextBox";
        public const string ShiftsPerWeekTextBox = "ShiftsPerWeekTextBox";
        public const string HoursPerShiftTextBox = "HoursPerShiftTextBox";
        public const string ProductionDaysPerWeekTextBox = "ProductionDaysPerWeekTextBox";
        public const string ProductionWeeksPerYearTextBox = "ProductionWeeksPerYearTextBox";
        public const string FileNameComboBox = "File name:";
        public const string FileTypeComboBox = "Files of type:";
        public const string BaseCurrencyCombo = "BaseCurrencyCombo";
    }
}
