﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ZPKTool.AutomatedTests.PreferencesTests;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    [Binding]
    public class ImportExportProjectSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        private static string projectExportPath;

        public ImportExportProjectSteps()
        {
        }

        public static void MyClassInitialize()
        {
            ImportExportProjectSteps.projectExportPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".project");
            if (File.Exists(ImportExportProjectSteps.projectExportPath))
            {
                File.Delete(ImportExportProjectSteps.projectExportPath);
            }
        }

        public static void MyClassCleanup()
        {
            if (File.Exists(ImportExportProjectSteps.projectExportPath))
            {
                File.Delete(ImportExportProjectSteps.projectExportPath);
            }
        }

        [BeforeFeature("ImportExportProject")]
        private static void LaunchApplication()
        {
            MyClassInitialize();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
            
            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);
            
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("ImportExportProject")]
        private static void KillApplication()
        {
            application.Kill();
            MyClassCleanup();
        }

        [Given(@"I press right-click on project node to choose Export option")]
        public void GivenIPressRight_ClickOnProjectNodeToChooseExportOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
            projectNode.SelectEx();

            Menu exportMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }

        [Given(@"I press the Cancel in Save As screen")]
        public void GivenIPressTheCancelInSaveAsScreen()
        {
            mainWindow.GetSaveFileDialog().Cancel();
        }

        [Given(@"I press right-click and choose the Export option again")]
        public void GivenIPressRight_ClickAndChooseTheExportOptionAgain()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
            projectNode.SelectEx();

            Menu exportMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Export);
            exportMenuItem.Click();
        }

        [Given(@"I press Save")]
        public void GivenIPressSave()
        {
            mainWindow.GetSaveFileDialog().SaveFile(ImportExportProjectSteps.projectExportPath);
            mainWindow.WaitForAsyncUITasks();
            mainWindow.GetMessageDialog(MessageDialogType.Info).ClickOk();
            Assert.IsTrue(File.Exists(ImportExportProjectSteps.projectExportPath), "The project was not exported at the specified location.");
        }

        [When(@"I select the projects node to import the exported project")]
        public void WhenISelectTheProjectsNodeToImportTheExportedProject()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();
            myProjects.Collapse();

            Menu importMenuItem = myProjects.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importMenuItem.Click();
        }

        [Then(@"the project is imported")]
        public void ThenTheProjectIsImported()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode = myProjectsNode.GetNode("myProject");
            projectNode.SelectEx();
        }
    }
}
