﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;
using White.Core.UIItems.MenuItems;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CutProjectPasteInFolderSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            ProjectFolder projFolder1 = new ProjectFolder();
            projFolder1.SetOwner(adminUser);
            projFolder1.Name = "cutFolder1";
            dataContext.ProjectFolderRepository.Add(projFolder1);
            dataContext.SaveChanges();

            ProjectFolder projFolder2 = new ProjectFolder();
            projFolder2.SetOwner(adminUser);
            projFolder2.Name = "cutFolder2";
            dataContext.ProjectFolderRepository.Add(projFolder2);
            dataContext.SaveChanges();

            Project project1 = new Project();
            project1.Name = "cutProject1";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;
            project1.ProjectFolder = projFolder1;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CutProjectPasteInFolder")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;            
        }

        [AfterFeature("CutProjectPasteInFolder")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press right-click on a project from folderCut(.*) to choose Cut option")]
        public void GivenIPressRight_ClickOnAProjectFromFolderCutToChooseCutOption(int p0)
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode folder1Node = myProjectsNode.GetNode("cutFolder1");
            folder1Node.ExpandEx();
            folder1Node.SelectEx();

            TreeNode projectNode = folder1Node.GetNode("cutProject1");
            projectNode.SelectEx();

            Menu cutMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press right-click on the second folder to press Paste option")]
        public void GivenIPressRight_ClickOnTheSecondFolderToPressPasteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode folder1Node = myProjectsNode.GetNode("cutFolder2");
            folder1Node.SelectEx();

            Menu pasteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press right-click and Cut on the moved project")]
        public void GivenIPressRight_ClickAndCutOnTheMovedProject()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode folderNode = myProjectsNode.GetNode("cutFolder2");
            folderNode.ExpandEx();
            folderNode.SelectEx();

            TreeNode projectNode = folderNode.GetNode("cutProject1");
            projectNode.SelectEx();

            Menu cutMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [When(@"I press right-click and Paste on the first foder")]
        public void WhenIPressRight_ClickAndPasteOnTheFirstFoder()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode folder1Node = myProjectsNode.GetNode("cutFolder1");
            folder1Node.SelectEx();

            Menu pasteMenuItem = folder1Node.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the project is moved from the second folder into the first one")]
        public void ThenTheProjectIsMovedFromTheSecondFolderIntoTheFirstOne()
        {
            mainWindow.WaitForAsyncUITasks();
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode folder1Node = myProjectsNode.GetNode("cutFolder1");
            folder1Node.ExpandEx();

            TreeNode projectNode = folder1Node.GetNode("cutProject1");
            projectNode.SelectEx();   
        }
    }
}
