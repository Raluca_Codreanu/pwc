﻿@ImportExportProject
Feature: ImportExportProject

Scenario: Import Export Project
	Given I press right-click on project node to choose Export option
	And I press the Cancel in Save As screen
	And I press right-click and choose the Export option again
	And I press Save
	When I select the projects node to import the exported project
	Then the project is imported

