﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core.UIItems.WindowItems;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TabItems;
using System.Windows.Automation;
using White.Core.UIItems.ListBoxItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.Data;
using White.Core.InputDevices;
using White.Core.UIItems.Finders;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    /// <summary>
    /// The test class of CreateProject Test Case.
    /// </summary>
    [TestClass]
    public class CreateProjectTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateProjectTestCase"/> class.
        /// </summary>
        public CreateProjectTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CreateProjectTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                //step 1                
                Window mainWindow = app.Windows.MainWindow;

                //step 2                
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.SelectEx();

                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                Button cancelButton = createProjectWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();

                app.MainMenu.Project_CreateProject.Click();
                createProjectWindow = Wait.For(() => app.Windows.ProjectWindow);

                TextBox projectName = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "project" + DateTime.Now.Ticks;

                TextBox numberTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found.");
                numberTextBox.Text = "123";

                TextBox versionTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.VersionTextBox);
                versionTextBox.Text = "123";

                ComboBox responsibleCalculatorComboBox = createProjectWindow.Get<ComboBox>(ProjectAutomationIds.ResponsibleCalculatorComboBox);
                Assert.IsNotNull(responsibleCalculatorComboBox, "ResponsibleCalculatorComboBox was not found");
                responsibleCalculatorComboBox.Click();
                responsibleCalculatorComboBox.Items.FirstOrDefault().Select();

                ComboBox leaderComboBox = createProjectWindow.Get<ComboBox>(ProjectAutomationIds.LeaderComboBox);
                Assert.IsNotNull(leaderComboBox, "LeaderComboBox was not found");
                leaderComboBox.Click();
                leaderComboBox.Items.FirstOrDefault().Select();

                TextBox annualProdQtyTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.AnnualProdQty);
                annualProdQtyTextBox.Text = "10";

                TextBox lifetimeTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.LifetimeTextBox);
                lifetimeTextBox.Text = "10";

                TextBox departmentTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.DepartmentTextBox);
                departmentTextBox.Text = "123";

                createProjectWindow.GetMediaControl().AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);

                ComboBox statusComboBox = createProjectWindow.Get<ComboBox>(ProjectAutomationIds.StatusComboBox);
                Assert.IsNotNull(statusComboBox, "statusComboBox was not found.");
                statusComboBox.SelectItem(2);

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                TextBox zipCodeTextBox = createProjectWindow.Get<TextBox>("ZipCodeTextBox");
                zipCodeTextBox.Text = "123456";

                ComboBox typeComboBox = createProjectWindow.Get<ComboBox>(ProjectAutomationIds.TypeComboBox);
                Assert.IsNotNull(typeComboBox, "TypeComboBox was not found.");

                typeComboBox.SelectItem(2);

                TextBox addressTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.AddressTextBox);
                addressTextBox.Text = "addressTest";

                TextBox cityTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.CityTextBox);
                cityTextBox.Text = "cityTest";

                // open Country pop up
                Button browseCountryButton = createProjectWindow.Get<Button>(ProjectAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();

                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                Tree masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("Belgium");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                // open Supplier pop up
                Button browseSupplierButton = createProjectWindow.Get<Button>(ProjectAutomationIds.BrowseCountryStateButton);
                Assert.IsNotNull(browseSupplierButton, "Browse supplier button was not found.");
                browseSupplierButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                if (masterDataTree.Nodes.Count > 0)
                {
                    masterDataTree.Nodes[0].SelectEx();
                }
                selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");
                selectButton.Click();

                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Click();

                TextBox depreciationTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.DepreciationTextBox);
                depreciationTextBox.Text = "12";

                TextBox interestRateTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.InterestRateTextBox);
                interestRateTextBox.Text = "12";

                TextBox logisticCostTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.LogisticCostTextBox);
                logisticCostTextBox.Text = "12";

                TextBox shiftsPerWeekTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ShiftsPerWeekTextBox);
                shiftsPerWeekTextBox.Text = "12";

                TextBox hoursPerShiftTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.HoursPerShiftTextBox);
                hoursPerShiftTextBox.Text = "12";

                TextBox productionDaysPerWeekTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProductionDaysPerWeekTextBox);
                productionDaysPerWeekTextBox.Text = "12";

                TextBox productionWeeksPerYearTextBox = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProductionWeeksPerYearTextBox);
                productionWeeksPerYearTextBox.Text = "12";

                Button saveButton = createProjectWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
            }
        }

        /// <summary>
        /// An automated test for Cancel Create Project
        /// </summary>
        [TestMethod]
        public void CancelCreateProjectTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                Menu createProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Create, AutomationIds.CreateProject);
                createProjectMenuItem.Click();
                Window createProjectWindow = app.Windows.ProjectWindow;

                //step 2
                TextBox projectName = createProjectWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                AttachedMouse mouse = mainWindow.Mouse;
                mouse.Location = new System.Windows.Point(projectName.Bounds.TopRight.X - 2, projectName.Bounds.TopRight.Y - 2);

                projectName.Text = "project" + DateTime.Now.Ticks;

                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierName = createProjectWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier";

                Button cancelButton = createProjectWindow.Get<Button>(AutomationIds.CancelButton, true);
                cancelButton.Click();
                createProjectWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                cancelButton.Click();
                createProjectWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }

        #region Helper
        #endregion Helper

        #region Inner classes
        #endregion Inner classes
    }
}




