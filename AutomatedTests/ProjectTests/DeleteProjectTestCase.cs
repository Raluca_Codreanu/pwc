﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using ZPKTool.DataAccess;


namespace ZPKTool.AutomatedTests.ProjectTests
{
    /// <summary>
    /// The automated test of Delete ProjectFolder Test Case.
    /// </summary>
    [TestClass]
    public class DeleteProjectTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteProjectFolderTestCase"/> class.
        /// </summary>
        public DeleteProjectTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            DeleteProjectTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes


        /// <summary>
        /// An automated test for Delete Project Folder Test Case.
        /// </summary>
        [TestMethod]
        public void DeleteProjectTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 2
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();

                TreeNode project1Node = myProjectsNode.GetNode("project1");
                project1Node.SelectEx();
                Assert.IsNotNull(project1Node, "project1 node was not found.");
                Menu deleteMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                //step 3
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 4
                deleteMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !project1Node.IsAvailable()); // Wait for the deleted project1Node tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                myProjectsNode.Expand();

                TreeNode project2Node = myProjectsNode.GetNode("project2");
                //step 6
                project2Node.Click();
                project2Node.Select();

                //Delete project2 from main menu -> Edit Menu 
                app.MainMenu.Edit_Delete.Click();
                Assert.IsNotNull(mainWindow.IsMessageDialogDisplayed(MessageDialogType.YesNo), "Confirmation message was not displayed at Cancel.");

                //step 7
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                //step 8
                app.MainMenu.Edit_Delete.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish

                // step 9 
                TreeNode trashBinNode = app.MainScreen.ProjectsTree.TrashBin;
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.Click();

                ListViewControl trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                //step 11
                ListViewRow project1Row = trashBinDataGrid.Row("Name", "project1");
                Assert.IsNotNull(project1Row, "Project1Row trash bin item was not found.");

                ListViewRow project2Row = trashBinDataGrid.Row("Name", "project2");
                Assert.IsNotNull(project2Row, "Project2Row trash bin item was not found.");

                ListViewCell selectedCell = trashBinDataGrid.CellByIndex(0, project1Row);
                AutomationElement selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;

                ListViewCell selectedCell2 = trashBinDataGrid.CellByIndex(0, project2Row);
                AutomationElement selectedElement2 = selectedCell2.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                CheckBox selectedCheckBox2 = new CheckBox(selectedElement2, selectedCell2.ActionListener);
                selectedCheckBox2.Checked = true;

                //step 12
                Button recoverButton = mainWindow.Get<Button>(AutomationIds.RecoverButton);
                Assert.IsNotNull(recoverButton, "Recover button was not found.");
                recoverButton.Click();

                //step 13
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();

                // step 14
                recoverButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the recover to finish              

                //step 15                
                myProjectsNode.Expand();

                project1Node = myProjectsNode.GetNode("project1");
                project1Node.Click();
                Assert.IsNotNull(project1Node, "project1 node was not found.");
                deleteMenuItem = project1Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !project1Node.IsAvailable()); // Wait for the deleted assembly tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                project2Node = myProjectsNode.GetNode("project2");
                project2Node.Click();
                Assert.IsNotNull(project1Node, "project2 node was not found.");
                deleteMenuItem = project2Node.GetContextMenuById(mainWindow, AutomationIds.Delete);
                deleteMenuItem.Click();

                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
                mainWindow.WaitForAsyncUITasks(); // Wait for the delete to finish
                Wait.For(() => !project2Node.IsAvailable()); // Wait for the deleted assembly tree node to disappear from the UI, otherwise it may interfere with clicking the trash bin node.

                //step 16                
                Assert.IsNotNull(trashBinNode, "Trash Bin node was not found in the main menu.");
                trashBinNode.Click();

                trashBinDataGrid = mainWindow.GetListView(AutomationIds.TrashBinDataGrid);
                Assert.IsNotNull(trashBinDataGrid, "Trash Bin data grid was not found.");

                //step 17
                project1Row = trashBinDataGrid.Row("Name", "project1");
                Assert.IsNotNull(project1Row, "Project1Row trash bin item was not found.");

                selectedCell = trashBinDataGrid.CellByIndex(0, project1Row);
                selectedElement = selectedCell.GetElement(SearchCriteria.ByControlType(ControlType.CheckBox).AndAutomationId(AutomationIds.TrashBinSelectedCheckBox));
                selectedCheckBox = new CheckBox(selectedElement, selectedCell.ActionListener);
                selectedCheckBox.Checked = true;

                Button viewDetailsButton = mainWindow.Get<Button>(AutomationIds.ViewDetailsButton);
                viewDetailsButton.Click();

                Button permanentlyDeleteButton = mainWindow.Get<Button>(AutomationIds.PermanentlyDeleteButton);
                Assert.IsNotNull(permanentlyDeleteButton, "Permanently Delete button was not found.");
                Assert.IsTrue(permanentlyDeleteButton.Enabled, "Permanently Delete button is enabled if the selected trash bin item is null.");

                permanentlyDeleteButton.Click();

                //step 18
                Button emptyTrashButton = mainWindow.Get<Button>(AutomationIds.EmptyTrashButton);
                emptyTrashButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            }
        }
        #region Helper

        /// <summary>
        /// Creates the data for this test -> Two project folders "Folder1" and "Folder2".
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "project1";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();

            Project project2 = new Project();
            project2.Name = "project2";
            project2.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project2.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 1;
            project2.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project2);
            dataContext.SaveChanges();
        }

        #endregion Helper

    }
}
