﻿@CancelUpdateProject
Feature: CancelUpdateProject

Scenario: Cancel Update a project
	Given I select a project
	And I Press the Cancel button
	And I update some properties
	And I press the cancel button
	And I press no from confirmation screen
	And I press Cancel again
	And I close the confirmation
	And I click the Cancel button again
	And I press Yes from confirmation screen
	And I make some changes
	And I select another project node
	And I press the No button in confirmation screen
	And I make some changes
	And I select another node
	And I close(X) the confirmation screen
	And I make some changes
	And I press the Home button 
	When I press the No
	Then the logout performs
