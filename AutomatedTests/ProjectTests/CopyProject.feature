﻿@CopyProject
Feature: CopyProject

Scenario: Copy project to My Projects
	Given I press right-click on project node to choose Copy option
	And I press right-click on My Projects to choose Paste option
	And I press right-click on the copied project to choose Copy
	When I press right-click on My Projects and choose Paste
	Then the project is copied in My Projects

Scenario: Copy project to Project Folder
	Given I press right click on a project node from My Projects to choose Copy option
	And I press right click on 'folder1' and choose Paste option
	And I press right click on a project from folder to choose Copy option
	When I press right click on 'folder1' and choose Paste option
	Then the project is copied in 'folder1'
