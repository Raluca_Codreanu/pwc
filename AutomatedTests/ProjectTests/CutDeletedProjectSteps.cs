﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    [Binding]
    public class CutDeletedProjectSteps
    {
        // testLink id = 342
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "cutDeletedProject";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CutDeletedProject")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CutDeletedProject")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a project and choose Cut option")]
        public void GivenISelectAProjectAndChooseCutOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("cutDeletedProject");
            projectNode.ExpandEx();

            Menu cutMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
        }

        [Given(@"I press right click and choose the Delete option")]
        public void GivenIPressRightClickAndChooseTheDeleteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("cutDeletedProject");
            projectNode.SelectEx();

            Menu deleteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }

        [Given(@"I press the Yes button in the confirmation screen")]
        public void GivenIPressTheYesButtonInTheConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }

        [Given(@"I press right click on My Projects node but paste is disabled")]
        public void GivenIPressRightClickOnMyProjectsNodeButPasteIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

          //  Menu pasteMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Paste);

         //   Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }

        [When(@"I select the My Projects node")]
        public void WhenISelectTheMyProjectsNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
        }

        [Then(@"the Paste option from main menu is disabled")]
        public void ThenThePasteOptionFromMainMenuIsDisabled()
        {
            Menu pasteMenuItem = application.MainMenu.Edit_Paste;
            pasteMenuItem.Enabled.Equals(false);
            //pasteMenuItem.Click();
            Assert.IsFalse(pasteMenuItem.Enabled, "The 'Paste' menu item should be disabled.");
        }
    }
}
