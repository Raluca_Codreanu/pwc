﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.AutomatedTests.UserAccess;
using White.Core.UIItems.TreeItems;
using System.Threading;
using White.Core.UIItems.MenuItems;
using ZPKTool.Data;
using White.Core.UIItems.Finders;
using System.Windows.Automation;
using ZPKTool.AutomatedTests.CustomUIItems;
using White.Core.InputDevices;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TabItems;
using System.IO;
using ZPKTool.DataAccess;
using ZPKTool.Common;


namespace ZPKTool.AutomatedTests.ProjectTests
{
    /// <summary>
    /// The automated test of Delete ProjectFolder Test Case.
    /// </summary>
    [TestClass]
    public class EditProjectTestCase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteProjectFolderTestCase"/> class.
        /// </summary>
        public EditProjectTestCase()
        {
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            // Helper.CreateDb();
            EditProjectTestCase.CreateTestData();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes


        /// <summary>
        /// An automated test for Edit Project Test Case (testLink id = 3).
        /// </summary>
        [TestMethod]
        public void EditProjectTest()
        {
            using (ApplicationEx app = ApplicationEx.LaunchAndGoToMainScreen())
            {
                Window mainWindow = app.Windows.MainWindow;

                //step 1
                TreeNode myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();

                TreeNode project1Node = myProjectsNode.GetNode("project1");
                Assert.IsNotNull(project1Node, "project1 node was not found.");
                project1Node.SelectEx();

                TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();

                TextBox projectName = mainWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
                Assert.IsNotNull(projectName, "Project Name was not found.");
                projectName.Text = "project_Updated";

                TextBox numberTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found.");
                numberTextBox.Text = "123456";

                TextBox versionTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.VersionTextBox);
                versionTextBox.Text = "123456";
                                
                //step 3
                ComboBox statusComboBox = mainWindow.Get<ComboBox>(ProjectAutomationIds.StatusComboBox);
                Assert.IsNotNull(statusComboBox, "statusComboBox was not found.");
                statusComboBox.SelectItem(1);

                ComboBox responsibleCalculatorComboBox = mainWindow.Get<ComboBox>(ProjectAutomationIds.ResponsibleCalculatorComboBox);
                Assert.IsNotNull(responsibleCalculatorComboBox, "ResponsibleCalculatorComboBox was not found");
                responsibleCalculatorComboBox.Click();
                responsibleCalculatorComboBox.Items.FirstOrDefault().Select();

                ComboBox leaderComboBox = mainWindow.Get<ComboBox>(ProjectAutomationIds.LeaderComboBox);
                Assert.IsNotNull(leaderComboBox, "LeaderComboBox was not found");
                leaderComboBox.Click();
                leaderComboBox.Items.FirstOrDefault().Select();

                TextBox annualProdQtyTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.AnnualProdQty);
                annualProdQtyTextBox.Text = "11";

                TextBox lifetimeTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.LifetimeTextBox);
                lifetimeTextBox.Text = "11";

                TextBox departmentTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.DepartmentTextBox);
                departmentTextBox.Text = "123456";

                //step 5 
                var mediaControl = mainWindow.GetMediaControl();
                mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Search.png", UriKind.Relative);

                //add the second image
                mediaControl.AddFile(@"..\..\..\AutomatedTests\Resources\Tulips.jpg", UriKind.Relative);
                
                //step 6
                TabPage supplierTab = mainWindow.Get<TabPage>(AutomationIds.SupplierTab);
                supplierTab.Click();

                TextBox supplierName = mainWindow.Get<TextBox>(ProjectAutomationIds.SupplierNameTextBox);
                supplierName.Text = "supplier_Updated";

                TextBox zipCodeTextBox = mainWindow.Get<TextBox>("ZipCodeTextBox");
                zipCodeTextBox.Text = "1111";

                ComboBox typeComboBox = mainWindow.Get<ComboBox>(ProjectAutomationIds.TypeComboBox);
                Assert.IsNotNull(typeComboBox, "TypeComboBox was not found.");

                typeComboBox.Click();
                Thread.Sleep(500);
                typeComboBox.Items.Item(2).Select();

                TextBox addressTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.AddressTextBox);
                addressTextBox.Text = "addressTest_Updated";

                TextBox cityTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.CityTextBox);
                cityTextBox.Text = "cityTest_Updated";

                // open Country pop up
                Button browseCountryButton = mainWindow.Get<Button>(ProjectAutomationIds.BrowseCountryButton);
                Assert.IsNotNull(browseCountryButton, "Browse Country button was not found.");
                browseCountryButton.Click();
                mainWindow.WaitForAsyncUITasks();
                Window browseMasterDataWindow = app.Windows.CountryAndSupplierBrowser;

                var masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                masterDataTree.SelectNode("France");
                Button selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");

                selectButton.Click();
                TextBox countryTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.CountryTextBox);
                Assert.IsNotNull(countryTextBox, "The Country text box was not found.");

                // open Supplier pop up
                Button browseSupplierButton = mainWindow.Get<Button>(ProjectAutomationIds.BrowseCountryStateButton);
                Assert.IsNotNull(browseSupplierButton, "Browse supplier button was not found.");
                browseSupplierButton.Click();
                browseMasterDataWindow = Wait.For(() => app.Windows.CountryAndSupplierBrowser);

                masterDataTree = browseMasterDataWindow.Get<Tree>(AutomationIds.MasterDataTree);
                if (masterDataTree.Nodes.Count > 0)
                {
                    masterDataTree.Nodes[0].SelectEx();
                }
                selectButton = browseMasterDataWindow.Get<Button>(AutomationIds.MasterDataBrowserSelectButton);
                Assert.IsNotNull(selectButton, "The Select button was not found.");
                selectButton.Click();

                //step 7
                TabPage settingsTab = mainWindow.Get<TabPage>(AutomationIds.SettingsTab);
                settingsTab.Click();

                TextBox depreciationTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.DepreciationTextBox);
                depreciationTextBox.Text = "11";

                TextBox interestRateTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.InterestRateTextBox);
                interestRateTextBox.Text = "11";

                TextBox logisticCostTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.LogisticCostTextBox);
                logisticCostTextBox.Text = "11";

                TextBox shiftsPerWeekTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.ShiftsPerWeekTextBox);
                shiftsPerWeekTextBox.Text = "11";

                TextBox hoursPerShiftTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.HoursPerShiftTextBox);
                hoursPerShiftTextBox.Text = "11";

                TextBox productionDaysPerWeekTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionDaysPerWeekTextBox);
                productionDaysPerWeekTextBox.Text = "11";

                TextBox productionWeeksPerYearTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.ProductionWeeksPerYearTextBox);
                productionWeeksPerYearTextBox.Text = "11";

                //step 8 
                TabPage documentsTab = mainWindow.Get<TabPage>(AutomationIds.DocumentsTab);
                documentsTab.Click();

                Button addDocumentsButton = mainWindow.Get<Button>(AutomationIds.AddDocumentsButton);
                addDocumentsButton.Click();
                mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myPart.pdf"));
                                
                //step 9
                TabPage overheadSettingsTab = mainWindow.Get<TabPage>(AutomationIds.OHSettingsTab);
                overheadSettingsTab.Click();

                OverheadSetting updatedSettings = SettingsTests.EditOverheadSettingsTestCase.CreateOverheadSettings();
                SettingsTests.EditOverheadSettingsTestCase.FillOverheadSettingsFieldsAndAssert(mainWindow, updatedSettings);
                Thread.Sleep(600);

                Button update = mainWindow.Get<Button>("UpdateOHSettingsButton");
                update.Click();

                //step 10
                Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
                saveButton.Click();
                mainWindow.WaitForAsyncUITasks();

                //step 11
                app.MainMenu.System_Logout.Click();
                app.LoginAndGoToMainScreen();                

                myProjectsNode = app.MainScreen.ProjectsTree.MyProjects;
                myProjectsNode.ExpandEx();
                
                TreeNode projectNode = myProjectsNode.GetNode("project_Updated (123456)");
                Assert.IsNotNull(projectNode, "Project_Updated doesn't extst");                
                projectNode.SelectEx();
                
                generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
                generalTab.Click();

                numberTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.NumberTextBox);
                Assert.IsNotNull(numberTextBox, "Number Text Box was not found.");
                numberTextBox.Text = "1";

                Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
                
                cancelButton.Click();
                mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();                
            }
        }
        #region Helper

        /// <summary>
        ///// Creates the data for this test -> Two project folders "Folder1" and "Folder2".
        /// </summary>
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "project1";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;

            Currency currency = new Currency();
            currency.Name = "Euro";
            currency.Symbol = "€";
            currency.ExchangeRate = 1m;
            currency.IsoCode = EncryptionManager.Instance.GenerateRandomString(3, true);
            project1.Currencies.Add(currency);
            project1.BaseCurrency = currency;
            
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        #endregion Helper
    }
}
