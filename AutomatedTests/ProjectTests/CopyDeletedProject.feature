﻿@CopyDeletedProject
Feature: CopyDeletedProject

Scenario: Copy a deleted project
	Given I select a project and choose Copy option 
	And I press right click and choose Delete option
	And I press Yes in confirmation screen
	And I press right click on My Project node paste is disabled
	When I select the My Project node
	Then the Paste from main menu is disabled
