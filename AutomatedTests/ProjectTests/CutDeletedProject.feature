﻿@CutDeletedProject
Feature: CutDeletedProject

Scenario: Cut Deleted Project
	Given I select a project and choose Cut option 
	And I press right click and choose the Delete option
	And I press the Yes button in the confirmation screen
	And I press right click on My Projects node but paste is disabled
	When I select the My Projects node
	Then the Paste option from main menu is disabled
