﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests
{
    [Binding]
    public class CopyDeletedProjectSteps
    {
        // testLink id = 345
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "copyDeletedProject";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CopyDeletedProject")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CopyDeletedProject")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I select a project and choose Copy option")]
        public void GivenISelectAProjectAndChooseCopyOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyDeletedProject");
            projectNode.ExpandEx();

            Menu copyMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();
        }

        [Given(@"I press right click and choose Delete option")]
        public void GivenIPressRightClickAndChooseDeleteOption()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();
            TreeNode projectNode = myProjectsNode.GetNode("copyDeletedProject");
            projectNode.SelectEx();

            Menu deleteMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Delete);
            deleteMenuItem.Click();
        }

        [Given(@"I press Yes in confirmation screen")]
        public void GivenIPressYesInConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
            mainWindow.WaitForAsyncUITasks();
        }

        [Given(@"I press right click on My Project node paste is disabled")]
        public void GivenIPressRightClickOnMyProjectNodePasteIsDisabled()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu pasteMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "Paste menu item is disabled.");
        }

        [When(@"I select the My Project node")]
        public void WhenISelectTheMyProjectNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();
        }

        [Then(@"the Paste from main menu is disabled")]
        public void ThenThePasteFromMainMenuIsDisabled()
        {
            Menu pasteMenuItem = application.MainMenu.Edit_Paste;
            Assert.IsFalse(pasteMenuItem.Enabled, "The 'Paste' menu item should be disabled.");
        }
    }
}
