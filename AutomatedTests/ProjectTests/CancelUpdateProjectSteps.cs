﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using White.Core.UIItems.Finders;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    [Binding]
    public class CancelUpdateProjectSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        public static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");
            Project project1 = new Project();
            project1.Name = "projectUpdate1";
            project1.SetOwner(adminUser);
            Customer c = new Customer();
            c.Name = "customer1";
            project1.Customer = c;
            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            project1.OverheadSettings = overheadSetting;

            dataContext.ProjectRepository.Add(project1);
            dataContext.SaveChanges();

            Project project2 = new Project();
            project2.Name = "projectUpdate2";
            project2.SetOwner(adminUser);
            Customer c2 = new Customer();
            c2.Name = "customer2";
            project2.Customer = c2;
            OverheadSetting overheadSetting2 = new OverheadSetting();
            overheadSetting2.MaterialOverhead = 2;
            project2.OverheadSettings = overheadSetting2;

            dataContext.ProjectRepository.Add(project2);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CancelUpdateProject")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;
        }

        [AfterFeature("CancelUpdateProject")]
        private static void KillApplication()
        {
            application.Kill();
        }
        
        [Given(@"I select a project")]
        public void GivenISelectAProject()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectUpdate1");
            projectNode1.SelectEx();
        }
        
        [Given(@"I Press the Cancel button")]
        public void GivenIPressTheCancelButton()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I update some properties")]
        public void GivenIUpdateSomeProperties()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();
          
            TextBox projectName = mainWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
            Assert.IsNotNull(projectName, "Project Name was not found.");
            projectName.Text = "project_Updated";

            TextBox numberTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.NumberTextBox);
            Assert.IsNotNull(numberTextBox, "Number Text Box was not found.");
            numberTextBox.Text = "123456";
        }
        
        [Given(@"I press the cancel button")]
        public void GivenIPressTheCancelButton1()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press no from confirmation screen")]
        public void GivenIPressNoFromConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickNo();
        }
        
        [Given(@"I press Cancel again")]
        public void GivenIPressCancelAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I close the confirmation")]
        public void GivenICloseTheConfirmation()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).Close();
        }
        
        [Given(@"I click the Cancel button again")]
        public void GivenIClickTheCancelButtonAgain()
        {
            Button cancelButton = mainWindow.Get<Button>(AutomationIds.CancelButton);
            cancelButton.Click();
        }
        
        [Given(@"I press Yes from confirmation screen")]
        public void GivenIPressYesFromConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNo).ClickYes();
        }
        
        [Given(@"I make some changes")]
        public void GivenIMakeSomeChanges()
        {
            TabPage generalTab = mainWindow.Get<TabPage>(AutomationIds.GeneralTab);
            generalTab.Click();

            TextBox projectName = mainWindow.Get<TextBox>(ProjectAutomationIds.ProjectNameTextBox);
            Assert.IsNotNull(projectName, "Project Name was not found.");
            projectName.Text = "project_Updated";

            TextBox numberTextBox = mainWindow.Get<TextBox>(ProjectAutomationIds.NumberTextBox);
            Assert.IsNotNull(numberTextBox, "Number Text Box was not found.");
            numberTextBox.Text = "123456";
        }
        
        [Given(@"I select another project node")]
        public void GivenISelectAnotherProjectNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectUpdate2");
            projectNode1.SelectEx();
        }
        
        [Given(@"I press the No button in confirmation screen")]
        public void GivenIPressTheNoButtonInConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Given(@"I select another node")]
        public void GivenISelectAnotherNode()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode projectNode1 = myProjectsNode.GetNode("projectUpdate1");
            projectNode1.SelectEx(); 
        }
        
        [Given(@"I close\(X\) the confirmation screen")]
        public void GivenICloseXTheConfirmationScreen()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).Close();
        }
        
        [Given(@"I press the Home button")]
        public void GivenIPressTheHomeButton()
        {
            Button homeButton = mainWindow.Get<Button>(AutomationIds.GoToWelcomeScreenButton);
            Assert.IsNotNull(homeButton, "The Home button was not found.");
            Assert.IsTrue(homeButton.Enabled, "The Home button is disabled.");

            homeButton.Click();    
        }
        
        [When(@"I press the No")]
        public void WhenIPressTheNo()
        {
            mainWindow.GetMessageDialog(MessageDialogType.YesNoCancel).ClickNo();
        }
        
        [Then(@"the logout performs")]
        public void ThenTheLogoutPerforms()
        {
            var welcomeScreen = Wait.For(() => application.WelcomeScreen);
            Button skipButton = mainWindow.Get<Button>(AutomationIds.SkipButton);
            skipButton.Click();
            mainWindow.WaitForAsyncUITasks();
        }
    }
}
