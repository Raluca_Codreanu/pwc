﻿@CutProjectPasteInFolder
Feature: CutProjectPasteInFolder

Scenario: Cut Project Paste in Folder
	Given I press right-click on a project from folderCut1 to choose Cut option
	And I press right-click on the second folder to press Paste option
	And I press right-click and Cut on the moved project
	When I press right-click and Paste on the first foder
	Then the project is moved from the second folder into the first one
