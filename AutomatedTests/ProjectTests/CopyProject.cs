﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.AutomatedTests.AssemblyTests;
using ZPKTool.AutomatedTests.PartTests;
using White.Core.UIItems.Finders;
using ZPKTool.AutomatedTests.PreferencesTests;
using System.Windows.Automation;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    [Binding]
    public class CopyProject
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;
        string copyTranslation = Helper.CopyElementValue();

        [BeforeFeature("CopyProject")]
        private static void LaunchApplication()
        {
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(PreferencesAutomationIds.ShowImportConfirmationScreen);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();

            TreeNode myProjectsnode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsnode.SelectEx();
            myProjectsnode.Collapse();
            Menu importAnotherProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProjectFolder);
            importAnotherProjectMenuItem.Click();

            mainWindow.GetOpenFileDialog().OpenFile(System.IO.Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\folder1.folder"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CopyProject")]
        private static void KillApplication()
        {
            application.Kill();
        }
        
        [Given(@"I press right-click on project node to choose Copy option")]
        public void GivenIPressRight_ClickOnProjectNodeToChooseCopyOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
          //  projectNode.SelectEx();
            projectNode.Collapse();

            Menu copyMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();    
        }
        
        [Given(@"I press right-click on My Projects to choose Paste option")]
        public void GivenIPressRight_ClickOnMyProjectsToChoosePasteOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();

            Menu pasteMenuItem = myProjects.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();

            Button saveButton = mainWindow.Get<Button>(AutomationIds.SaveButton);
            saveButton.Click();
        }
        
        [Given(@"I press right-click on the copied project to choose Copy")]
        public void GivenIPressRight_ClickOnTheCopiedProjectToChooseCopy()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();
            myProjects.ExpandEx();
            mainWindow.WaitForAsyncUITasks();

            TreeNode projectNode = myProjects.GetNode(copyTranslation + "myProject");
            projectNode.SelectEx();

            Menu copyMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click(); 
        }
        
        [Given(@"I press right click on a project node from My Projects to choose Copy option")]
        public void GivenIPressRightClickOnAProjectNodeFromMyProjectsToChooseCopyOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
            projectNode.SelectEx();

            Menu copyMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();      
        }
        
        [Given(@"I press right click on '(.*)' and choose Paste option")]
        public void GivenIPressRightClickOnAndChoosePasteOption(string p0)
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode(p0);
            folderNode.Collapse();
         //   folderNode.SelectEx();

            Menu pasteMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
            myProjects.Collapse();
        }

        [Given(@"I press right click on a project from folder to choose Copy option")]
        public void GivenIPressRightClickOnAProjectFromFolderToChooseCopyOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("folder1");
            folderNode.SelectEx();
            folderNode.ExpandEx();

            TreeNode projectNode = folderNode.GetNode("project1");
            //projectNode.SelectEx();
            projectNode.Collapse();

            Menu copyMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Copy);
            copyMenuItem.Click();   
        }
        
        [When(@"I press right-click on My Projects and choose Paste")]
        public void WhenIPressRight_ClickOnMyProjectsAndChoosePaste()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;            
            //myProjectsNode.SelectEx();
            myProjectsNode.Collapse();

            Menu pasteMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [When(@"I press right click on '(.*)' and choose Paste option")]
        public void WhenIPressRightClickOnAndChoosePasteOption(string p0)
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode(p0);
            folderNode.SelectEx();

            Menu pasteMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Then(@"the project is copied in My Projects")]
        public void ThenTheProjectIsCopiedInMyProjects()
        {
            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.ExpandEx();

            TreeNode copiedProject = myProjectsNode.GetNode(copyTranslation + copyTranslation + "myProject");
            copiedProject.SelectEx();
        }
        
        [Then(@"the project is copied in '(.*)'")]
        public void ThenTheProjectIsCopiedIn(string p0)
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("folder1");
            folderNode.ExpandEx();

            TreeNode projectNode = folderNode.GetNode(copyTranslation + "project1");
            projectNode.SelectEx();

        }
    }
}
