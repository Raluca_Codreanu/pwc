﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using TechTalk.SpecFlow.Assist;
using White.Core;
using White.Core.UIItems;
using White.Core.UIItems.WindowItems;
using ZPKTool.Data;
using White.Core.UIItems.ListBoxItems;
using White.Core.UIItems.TreeItems;
using ZPKTool.AutomatedTests.CustomUIItems;
using ZPKTool.AutomatedTests.CurrencyTests;
using White.Core.UIItems.MenuItems;
using White.Core.UIItems.TabItems;
using White.Core.InputDevices;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ZPKTool.DataAccess;

namespace ZPKTool.AutomatedTests.ProjectTests
{
    [Binding]
    public class CutProjectPasteInMyProjectsSteps
    {
        // Feature scoped data
        private static ApplicationEx application;
        private static Window mainWindow;

        private static void CreateTestData()
        {
            Helper.ConfigureDbAccess();
            var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            User adminUser = dataContext.UserRepository.GetAll(false).FirstOrDefault(u => u.Username == "admin");

            ProjectFolder folder = new ProjectFolder();
            folder.Name = "folder";
            folder.SetOwner(adminUser);
            dataContext.ProjectFolderRepository.Add(folder);
            dataContext.SaveChanges();
        }

        [BeforeFeature("CutProjectPasteInMyProjects")]
        private static void LaunchApplication()
        {
            CreateTestData();
            application = ApplicationEx.LaunchAndGoToMainScreen();
            mainWindow = application.Windows.MainWindow;

            // ensure that import summary screen is hidden                                    
            application.MainMenu.Options_Preferences.Click();
            Window preferencesWindow = application.Windows.Preferences;

            CheckBox displayImportSummaryScreen = preferencesWindow.Get<CheckBox>(AutomationIds.ShowImportConfirmationScreenCheckBox);
            displayImportSummaryScreen.Checked = true;

            Button savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);

            savePreferencesButton = preferencesWindow.Get<Button>(AutomationIds.SaveButton);
            savePreferencesButton.Click();
            Wait.For(() => application.Windows.Preferences == null);

            TreeNode myProjectsNode = application.MainScreen.ProjectsTree.MyProjects;
            myProjectsNode.SelectEx();

            Menu importProjectMenuItem = myProjectsNode.GetContextMenuById(mainWindow, AutomationIds.MyProjectsImport, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
            
            myProjectsNode.SelectEx();
            myProjectsNode.ExpandEx();

            TreeNode folderNode = myProjectsNode.GetNode("folder");
            folderNode.SelectEx();

            importProjectMenuItem = folderNode.GetContextMenuById(mainWindow, AutomationIds.Import, AutomationIds.ImportProject);
            importProjectMenuItem.Click();
            mainWindow.GetOpenFileDialog().OpenFile(Path.GetFullPath(@"..\..\..\AutomatedTests\Resources\myProject.project"));
            mainWindow.WaitForAsyncUITasks();
        }

        [AfterFeature("CutProjectPasteInMyProjects")]
        private static void KillApplication()
        {
            application.Kill();
        }

        [Given(@"I press right click on a project from My Projects to choose Cut option")]
        public void GivenIPressRightClickOnAProjectFromMyProjectsToChooseCutOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
            projectNode.SelectEx();

            Menu cutMenuItem = projectNode.GetContextMenuById(mainWindow, AutomationIds.Cut);
            cutMenuItem.Click();
            mainWindow.WaitForAsyncUITasks();
        }
        
        [Given(@"I press right click to choose Paste on My Projects node")]
        public void GivenIPressRightClickToChoosePasteOnMyProjectsNode()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();

            Menu pasteMenuItem = myProjects.GetContextMenuById(mainWindow, AutomationIds.Paste);
            Assert.IsFalse(pasteMenuItem.Enabled, "The 'Paste' menu item should be disabled.");
        }
        
        [Given(@"I expand a folder node")]
        public void GivenIExpandAFolderNode()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode folderNode = myProjects.GetNode("folder");
            folderNode.ExpandEx();

            TreeNode projectNode = folderNode.GetNode("myProject");
            projectNode.SelectEx();
        }
        
        [Given(@"I press right click on a project to choose Cut option")]
        public void GivenIPressRightClickOnAProjectToChooseCutOption()
        {
            application.MainMenu.Edit_Cut.Click();            
        }
        
        [When(@"I press right click on a project from My Projects to choose Paste option")]
        public void WhenIPressRightClickOnAProjectFromMyProjectsToChoosePasteOption()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.SelectEx();

            Menu pasteMenuItem = myProjects.GetContextMenuById(mainWindow, AutomationIds.Paste);
            pasteMenuItem.Click();
        }
        
        [Then(@"the project is moved in My Projects node")]
        public void ThenTheProjectIsMovedInMyProjectsNode()
        {
            TreeNode myProjects = application.MainScreen.ProjectsTree.MyProjects;
            myProjects.ExpandEx();

            TreeNode projectNode = myProjects.GetNode("myProject");
            myProjects.SelectEx();
        }
    }
}
