﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsCompression
{
    /// <summary>
    /// The compressed document.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// The content compressed.
        /// </summary>
        public byte[] contentCompressed;

        /// <summary>
        /// The size of compressed document.
        /// </summary>
        public string sizeOfCompressedDocument;
    }
}
