﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;

namespace DocumentsCompression
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = string.Empty;
            var database = string.Empty;
            var autenthicationMode = AutenthicationMode.Windows;
            var username = string.Empty;
            var password = string.Empty;

            // Read the parameters.
            foreach (var arg in args)
            {
                if (arg.StartsWith("/server=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    server = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/database=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    database = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/username=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    username = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/password=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    password = GetArgumentValue(arg);
                }
            }

            // Check if all the necessary parameters were supplied.
            if (string.IsNullOrWhiteSpace(server))
            {
                Console.WriteLine("The server name is missing. Cannot continue ...");
            }
            else if (string.IsNullOrWhiteSpace(database))
            {
                Console.WriteLine("The database name is missing. Cannot continue ...");
            }
            else if (string.IsNullOrWhiteSpace(username)
                && !string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("The username is missing but the password is present. Cannot continue ...");
            }
            else if (!string.IsNullOrWhiteSpace(username)
                && string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("The password is missing but the username is present. Cannot continue ...");
            }
            else
            {
                // If the username and password were provided use the SQL Server authentication mode.
                if (!string.IsNullOrWhiteSpace(username)
                    && !string.IsNullOrWhiteSpace(password))
                {
                    autenthicationMode = AutenthicationMode.SQLServer;
                }

                // Build the connection string.
                var connectionString = string.Format(@"Server={0};Database={1};", server, database);
                if (autenthicationMode == AutenthicationMode.Windows)
                {
                    connectionString += "Trusted_Connection=True;";
                }
                else
                {
                    connectionString += string.Format(@"User Id={1};Password={2};", username, password);
                }

                try
                {
                    // Encrypt the users data.
                    if (CompressDocuments(connectionString))
                    {
                        Console.WriteLine("No need to compressed the documents.");
                    }
                    else
                    {
                        Console.WriteLine("The documents was successfully compressed.");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Compresses the documents.
        /// </summary>
        /// <param name="sqlConnString">The SQL connection string.</param>
        /// <returns>True if the documents are already compressed, false otherwise.</returns>
        private static bool CompressDocuments(string sqlConnString)
        {
            bool isDocumentCompressed = false;
            using (SqlConnection connection = new SqlConnection(sqlConnString))
            {
                connection.Open();
                using (SqlCommand sqlCommandSelectMedia = new SqlCommand("Select TOP 1 Content from Media Where Type = 2", connection))
                {
                    var content = sqlCommandSelectMedia.ExecuteScalar() as byte[];
                    if (content == null)
                    {
                        return true;
                    }

                    if (IsDocumentCompressed(content))
                    {
                        isDocumentCompressed = true;
                    }
                }

                if (!isDocumentCompressed)
                {
                    using (SqlCommand sqlCommand = new SqlCommand("SELECT Guid, Content, Size, OriginalFileName FROM Media WHERE Type = 2", connection))
                    {
                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            using (var updateConnection = new SqlConnection(sqlConnString))
                            {
                                updateConnection.Open();
                                using (SqlTransaction transaction = updateConnection.BeginTransaction())
                                {
                                    try
                                    {
                                        while (reader.Read())
                                        {
                                            var content = reader["Content"] as byte[];
                                            var size = reader["Size"].ToString();
                                            var originalFileName = reader["OriginalFileName"].ToString();
                                            var mediaId = reader["Guid"].ToString();

                                            if (!IsDocumentCompressed(content))
                                            {
                                                var compressedDocument = CompressDocument(content, originalFileName);
                                                var contentCompressed = compressedDocument.contentCompressed;
                                                var sizeOfCompressedDocument = compressedDocument.sizeOfCompressedDocument;

                                                SqlCommand sqlCommandUpdate = new SqlCommand("UPDATE Media SET Content = @contentCompressed, Size = @sizeOfCompressedDocument WHERE Guid = @mediaId", updateConnection);
                                                sqlCommandUpdate.Transaction = transaction;
                                                var parameterMediaId = new SqlParameter("@mediaId", mediaId);
                                                var parameterContent = new SqlParameter("@contentCompressed", contentCompressed);
                                                var parameterSize = new SqlParameter("@sizeOfCompressedDocument", sizeOfCompressedDocument);

                                                sqlCommandUpdate.Parameters.Add(parameterMediaId);
                                                sqlCommandUpdate.Parameters.Add(parameterContent);
                                                sqlCommandUpdate.Parameters.Add(parameterSize);
                                                sqlCommandUpdate.ExecuteNonQuery();
                                            }
                                        }

                                        transaction.Commit();
                                    }
                                    catch
                                    {
                                        transaction.Rollback();
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isDocumentCompressed;
        }

        /// <summary>
        /// Determines whether the document is compressed.
        /// </summary>
        /// <param name="content">The content of the document.</param>
        /// <returns>True if the document is already compressed, false otherwise.</returns>
        private static bool IsDocumentCompressed(byte[] content)
        {
            using (MemoryStream contentMemoryStream = new MemoryStream())
            {
                contentMemoryStream.Write(content, 0, content.Length);
                contentMemoryStream.Position = 0;
                try
                {
                    var zip = ZipFile.Read(contentMemoryStream);

                    // Check if the file is a zip and contains only one zip entry.
                    // This is because the new office formats (ex. file.xlsx) are actually archives and this is why we check if the current file contains only one entry.
                    if (zip != null
                        && zip.Entries.Count == 1)
                    {
                        return true;
                    }
                }
                catch
                {
                }
            }

            return false;
        }

        /// <summary>
        /// Compresses the documents.
        /// </summary>
        /// <param name="content">The content of the document.</param>
        /// <param name="originalFileName">The original name of the document.</param>
        /// <returns>An compressed document.</returns>U
        private static Document CompressDocument(byte[] content, string originalFileName)
        {
            Document document = new Document();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ZipFile zip = new ZipFile())
                {
                    var newZipEntry = zip.AddEntry(originalFileName, content);
                    zip.Save(memoryStream);

                    document.contentCompressed = memoryStream.ToArray();
                    document.sizeOfCompressedDocument = newZipEntry.CompressedSize.ToString();
                }
            }

            return document;
        }

        static string GetArgumentValue(string arg)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                return string.Empty;
            }

            return arg.Replace(arg.Remove(arg.IndexOf('=') + 1), string.Empty);
        }
    }
}
