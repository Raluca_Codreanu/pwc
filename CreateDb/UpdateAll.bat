@echo off

REM Retrieve Visual Studio .exe location
SET VS_PATH=
IF EXIST "c:\Program Files\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com"	(	
	SET VS_PATH="c:\Program Files\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com"
) ELSE IF EXIST "c:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com" (	
	SET VS_PATH="c:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com" )

REM Build the Database and additional projects
@echo on
%VS_PATH% "..\ZPKTool\ZPKToolDatabase.sln" /Rebuild Debug
%VS_PATH% "..\DataEncryption\DataEncryption.sln" /Rebuild Debug
%VS_PATH% "..\DocumentsCompression\DocumentsCompression.sln" /Rebuild Debug
@echo off
if NOT %ERRORLEVEL% == 0 GOTO BuildError

SET localServerName=localhost\sqlexpress
SET centralServerName=localhost\sqlexpress
SET localDatabaseName=ZPKTool
SET centralDatabaseName=ZPKToolCentral

REM Create the database and execute the utilities
echo --- Updating the local database ---
"../3rdParty\DAC\SqlPackage.exe" /Action:Publish /SourceFile:"..\ZPKTool\ZPKTool.Database\bin\debug\ZPKTool.Database.dacpac" /TargetServerName:%localServerName% /TargetDatabaseName:%localDatabaseName% /p:CreateNewDatabase=False /p:BackupDatabaseBeforeChanges=False /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True /p:BlockOnPossibleDataLoss=False

echo --- Encrypting the data in the local database ---
"../DataEncryption\DataEncryption\bin\debug\DataEncryption.exe" /server=%localServerName% /database=%localDatabaseName%

echo --- Compressing the documents in the local database ---
"../DocumentsCompression\DocumentsCompression\bin\debug\DocumentsCompression.exe" /server=%localServerName% /database=%localDatabaseName%

echo --- Updating the central database ---
"../3rdParty\DAC\SqlPackage.exe" /Action:Publish /SourceFile:"..\ZPKTool\ZPKTool.Database.Central\bin\Debug\ZPKTool.Database.Central.dacpac" /TargetServerName:%centralServerName% /TargetDatabaseName:%centralDatabaseName% /p:CreateNewDatabase=False /p:BackupDatabaseBeforeChanges=False /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True /p:IncludeCompositeObjects=True /p:BlockOnPossibleDataLoss=False

echo --- Encrypting the data in the central database ---
"../DataEncryption\DataEncryption\bin\debug\DataEncryption.exe" /server=%centralServerName% /database=%centralDatabaseName%

echo --- Compressing the documents in the central database ---
"../DocumentsCompression\DocumentsCompression\bin\debug\DocumentsCompression.exe" /server=%centralServerName% /database=%centralDatabaseName%
GOTO End

:BuildError
echo Build failed.
GOTO End

:End
echo Done.