﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerBackend
{
    /// <summary>
    /// The exception thrown by the business layer.
    /// </summary>
    public class BackendException : System.Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        public BackendException()
            : base(string.Empty)
        {
            ErrorCode = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        public BackendException(string errorCode)
            : base(string.Empty)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        /// <param name="errorMessage">The error message</param>
        public BackendException(string errorCode, string errorMessage)
            : base(errorMessage)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The base exception</param>
        public BackendException(string errorCode, string errorMessage, Exception ex)
            : base(errorMessage, ex)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="exception">The base UIException</param>
        public BackendException(BackendException exception)
            : base(exception.Message, exception)
        {
            ErrorCode = exception.ErrorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public BackendException(string errorCode, Exception ex)
            : base(string.Empty, ex)
        {
            ErrorCode = errorCode;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the error code of the exception.
        /// </summary>
        public string ErrorCode { get; set; }

        #endregion Properties
    }
}
