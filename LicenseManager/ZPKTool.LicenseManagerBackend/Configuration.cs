﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerBackend
{
    /// <summary>
    /// This class creates, updates the database.
    /// </summary>
    public sealed class Configuration
    {
        /// <summary>
        /// The name of the file that contains the database schema.
        /// </summary>
        private const string DbSchemaFile = "DatabaseSchema.sql";

        /// <summary>
        /// Database File Name.
        /// </summary>
        public const string DatabaseFileName = "LicenseManager.db";

        /// <summary>
        /// Database Backup File Name
        /// </summary>
        public const string DatabaseBackupFileName = "LicenseManagerBackup.db";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly Configuration instance = new Configuration();

        /// <summary>
        /// Prevents a default instance of the <see cref="Configuration" /> class from being created.
        /// </summary>
        private Configuration()
        {
            this.ConnectionString = "metadata=res://*;provider=System.Data.SQLite;provider connection string=\"data source=LicenseManager.db\"";
        }

        /// <summary>
        /// Gets the path to the folder where the application stores data.
        /// </summary>
        public static string ApplicationDataFolderPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PCM License Manager";
            }
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static Configuration Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets the database connection string.
        /// </summary>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Initializes the data base.
        /// </summary>
        public void InitializeDataBase()
        {
            this.ConnectionString = "metadata=res://*;provider=System.Data.SQLite;provider connection string=\"data source=" + ApplicationDataFolderPath + "\\" + "LicenseManager.db\"";

            try
            {
                // Create the app data folder if it does not exist
                if (!Directory.Exists(ApplicationDataFolderPath))
                {
                    Directory.CreateDirectory(ApplicationDataFolderPath);
                }

                // Check if the app database file exists
                if (!File.Exists(Path.Combine(ApplicationDataFolderPath, DatabaseFileName)))
                {
                    // Create the database.
                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(this.GetType().Namespace + "." + DbSchemaFile))
                    {
                        if (stream == null)
                        {
                            throw new InvalidOperationException("The database schema file could not be found.");
                        }

                        var reader = new StreamReader(stream);
                        var createDbScript = reader.ReadToEnd();

                        SQLiteConnection.CreateFile(Path.Combine(ApplicationDataFolderPath, DatabaseFileName));
                        ExecuteNonQueryCommand(createDbScript);
                    }
                }
                else
                {
                    // Check the version of the existing database and determine if an update is necessary.
                    long version = GetDataBaseVersion();
                    if (version < ZPKTool.LicenseManagerBackend.Properties.Settings.Default.DataBaseVersion)
                    {
                        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(this.GetType().Namespace + "." + DbSchemaFile))
                        {
                            if (stream == null)
                            {
                                throw new InvalidOperationException("The database schema file could not be found.");
                            }

                            StreamReader reader = new StreamReader(stream);
                            var updateDbScript = reader.ReadToEnd();

                            // Back-up the database
                            File.Copy(
                                Path.Combine(ApplicationDataFolderPath, DatabaseFileName),
                                Path.Combine(ApplicationDataFolderPath, DatabaseBackupFileName),
                                true);

                            // Update the database
                            ExecuteNonQueryCommand(updateDbScript);

                            // Delete the database back-up.
                            File.Delete(Path.Combine(ApplicationDataFolderPath, DatabaseBackupFileName));
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                log.ErrorException("Error occured when creating/updating the database", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                log.ErrorException("Error occured when creating/updating the database", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Gets the data base version.
        /// </summary>
        /// <returns>The database version</returns>
        private static long GetDataBaseVersion()
        {
            long version = 0;

            SQLiteConnection connection = null;

            try
            {
                var connectionStringWithVersion = "Data Source = " + Path.Combine(ApplicationDataFolderPath, DatabaseFileName) + "; Version=3;";
                connection = new SQLiteConnection(connectionStringWithVersion);
                connection.Open();

                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = "SELECT Version FROM Version;";
                command.Prepare();

                SQLiteDataReader sqlDataReader = command.ExecuteReader();
                sqlDataReader.Read();
                if (sqlDataReader != null)
                {
                    version = sqlDataReader.GetInt64(0);
                }

                sqlDataReader.Close();
            }
            catch (SQLiteException ex)
            {
                log.ErrorException("Error occured when creating the database", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }

            return version;
        }

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="commandText">The command text.</param>
        private static void ExecuteNonQueryCommand(string commandText)
        {
            SQLiteConnection connection = null;
            SQLiteCommand command = null;

            try
            {
                string connectionStringWithVersion = "Data Source = " + Path.Combine(ApplicationDataFolderPath, DatabaseFileName) + "; Version=3;";
                connection = new SQLiteConnection(connectionStringWithVersion);
                connection.Open();
                command = connection.CreateCommand();
                command.CommandText = commandText;
                command.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                log.ErrorException("Error occured when creating the database", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }
    }
}
