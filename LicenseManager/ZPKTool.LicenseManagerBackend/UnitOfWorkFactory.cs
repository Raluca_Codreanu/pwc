﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerBackend
{
    public class UnitOfWorkFactory
    {
        private static UnitOfWork unit;

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <returns></returns>
        public static UnitOfWork GetUnitOfWork()
        {
            if (unit == null)
            {
                var context = new DataContext(Configuration.Instance.ConnectionString);    
                unit = new UnitOfWork(context);
            }

            return unit;
        }

        /// <summary>
        /// Creates an instance of unit of work.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>UnitOfWorkInstance</returns>
        public static UnitOfWork CreateUnitOfWork(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException("connectionString");
            }

            var context = new DataContext(connectionString);
            return new UnitOfWork(context);
        }

        /// <summary>
        /// Creates an instance of unit of work.
        /// </summary>
        /// <returns></returns>
        public static UnitOfWork CreateUnitOfWork()
        {                  
            var context = new DataContext(Configuration.Instance.ConnectionString);
            return new UnitOfWork(context);
        }
    }
}
