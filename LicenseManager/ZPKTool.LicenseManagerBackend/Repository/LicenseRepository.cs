﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data;
using ZPKTool.Common;
using ZPKTool.LicenseManagerBackend.Interfaces;

namespace ZPKTool.LicenseManagerBackend.Repository
{
    /// <summary>
    /// Repository performs create, read, update, and delete operations. 
    /// </summary>
    public partial class LicenseRepository : Repository<License>, ILicenseRepository
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private ObjectContext context;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public LicenseRepository(ObjectContext context)
            : base(context)
        {
            this.context = context;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the lincese by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The license with specified id.</returns>
        /// <exception cref="DataException">Data Exception.</exception>
        public override License GetById(long id)
        {
            try
            {
                return objectSet.SingleOrDefault(e => e.ID == id);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get license with id = " + id, ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Gets the by user.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public IEnumerable<License> GetByUser(long userID)
        {
            try
            {
                return base.Query(p => p.User.ID == userID);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get license with userid = " + userID, ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public override void Remove(License entity)
        {
            base.Remove(entity);
        }

        /// <summary>
        /// Checks if license uses distributor.
        /// </summary>
        /// <param name="distributorID">The distributor ID.</param>
        /// <returns>True if there is no license with this distributor, false otherwise.</returns>
        /// <exception cref="DataExcetion">DataException</exception>
        public bool CheckIfLicenseUsesDistributor(long distributorID)
        {
            try
            {
                return base.Query(l => l.Distributor.ID == distributorID).Count() > 0;
            }
            catch (DataException ex)
            {
                log.ErrorException("Check If License Uses Distributor failed", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        #endregion
    }
}
