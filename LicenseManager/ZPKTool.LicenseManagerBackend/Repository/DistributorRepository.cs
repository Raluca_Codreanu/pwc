﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Data;
using ZPKTool.Common;
using ZPKTool.LicenseManagerBackend.Interfaces;

namespace ZPKTool.LicenseManagerBackend.Repository
{
    /// <summary>
    /// Repository performs create, read, update, and delete operations. 
    /// </summary>
    public partial class DistributorRepository : Repository<Distributor>, IDistribuitorRepository
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public DistributorRepository(ObjectContext context)
            : base(context)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the distributor by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The distribuitor with specified id.</returns>
        /// <exception cref="DataException">Data Exception.</exception>
        public override Distributor GetById(long id)
        {
            try
            {
                return objectSet.SingleOrDefault(e => e.ID == id);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get distributor.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        #endregion
    }
}
