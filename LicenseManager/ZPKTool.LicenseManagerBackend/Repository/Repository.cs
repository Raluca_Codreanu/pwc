﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ZPKTool.LicenseManagerBackend.Interfaces;
using ZPKTool.Common;
using System.Data;

namespace ZPKTool.LicenseManagerBackend.Repository
{
    /// <summary>
    /// Repository performs create, read, update, and delete operations. 
    /// </summary>
    /// <typeparam name="T">Generic type.</typeparam>
    public abstract class Repository<T> : IRepository<T>
                                  where T : class
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Represents a typed entity set that is used to perform create, read, update, and delete operations. 
        /// </summary>
        protected IObjectSet<T> objectSet;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="DataException">Data Exception.</exception>
        public Repository(ObjectContext context)
        {
            try
            {
                objectSet = context.CreateObjectSet<T>();
            }
            catch (InvalidOperationException ex)
            {
                log.ErrorException("Invalid Operation exception while creating object set.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        #endregion

        #region IRepository<T> Members

        /// <summary>
        /// Gets all entities of type T from database.
        /// </summary>
        /// <returns>A IEnumerable collection with all entities.</returns>
        public IEnumerable<T> GetAll()
        {
            return objectSet;
        }

        /// <summary>
        /// Gets the entity with the specified id.
        /// </summary>
        /// <param name="id">The entity's id.</param>
        /// <returns>The entity or null if it was not found.</returns>
        public abstract T GetById(long id);

        /// <summary>
        /// Queries the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>A IEnumerable collection of entities.</returns>
        /// <exception cref="DataException">Data Exception.</exception>
        public IEnumerable<T> Query(Expression<Func<T, bool>> filter)
        {
            try
            {
                return objectSet.Where(filter);
            }
            catch (DataException ex)
            {
                log.ErrorException("Get entity with where clause exception.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="DataException">Data Exception.</exception>
        public void Add(T entity)
        {
            try
            {
                objectSet.AddObject(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Add entity exception.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="DataException">Data Exception.</exception>
        public virtual void Remove(T entity)
        {
            try
            {
                objectSet.DeleteObject(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Remove entity exception.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        #endregion
    }
}
