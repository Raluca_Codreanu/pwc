﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data;
using ZPKTool.Common;
using ZPKTool.LicenseManagerBackend.Interfaces;

namespace ZPKTool.LicenseManagerBackend.Repository
{
    /// <summary>
    /// Repository performs create, read, update, and delete operations. 
    /// </summary>
    public partial class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private ObjectContext context;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public CompanyRepository(ObjectContext context)
            : base(context)
        {
            this.context = context;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the company by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The entity of type company.</returns>
        /// <exception cref="DataException">Data Exception.</exception>
        public override Company GetById(long id)
        {
            try
            {
                return objectSet.SingleOrDefault(e => e.ID == id);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get company.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public override void Remove(Company entity)
        {
            foreach (User user in entity.Users.ToList())
            {
                IRepository<User> userRepository = new UserRepository(this.context);
                userRepository.Remove(user);
            }

            base.Remove(entity);
        }

        #endregion
    }
}
