﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Data;
using ZPKTool.Common;
using ZPKTool.LicenseManagerBackend.Interfaces;

namespace ZPKTool.LicenseManagerBackend.Repository
{
    /// <summary>
    /// Repository performs create, read, update, and delete operations. 
    /// </summary>
    public partial class UserRepository : Repository<User>, IUserRepository
    {

        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private ObjectContext context;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UserRepository(ObjectContext context)
            : base(context)
        {
            this.context = context;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the user by id.
        /// </summary>
        /// <param name="id">The users's id.</param>
        /// <returns>Get user with specified id.</returns>
        /// <exception cref="DataException">Data Exception.</exception>
        public override User GetById(long id)
        {
            try
            {
                return objectSet.SingleOrDefault(e => e.ID == id);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get the user with id = " + id, ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Gets the users with company id.
        /// </summary>
        /// <param name="Id">The company id.</param>
        /// <returns>A IEnumerable of users.</returns>
        public IEnumerable<User> GetByCompany(long Id)
        {
            try
            {
                return base.Query(user => user.Company.ID == Id);
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not get the user with company id = " + Id, ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Removes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public override void Remove(User entity)
        {
            foreach (License license in entity.Licenses.ToList())
            {
                IRepository<License> licenseRepository = new LicenseRepository(this.context);
                licenseRepository.Remove(license);
            }

            base.Remove(entity);
        }

        #endregion
    }
}
