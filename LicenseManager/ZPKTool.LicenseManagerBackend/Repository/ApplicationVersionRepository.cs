﻿namespace ZPKTool.LicenseManagerBackend.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Objects;
    using System.Linq;
    using System.Text;
    using ZPKTool.Common;
    using ZPKTool.LicenseManagerBackend.Interfaces;

    /// <summary>
    /// The repository for the <see cref="ApplicationVersion"/> entity.
    /// </summary>
    public class ApplicationVersionRepository : Repository<ApplicationVersion>, IApplicationVersionRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The object context used to perform database operations.
        /// </summary>
        private ObjectContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationVersionRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ApplicationVersionRepository(ObjectContext context)
            : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets the entity with the specified id.
        /// </summary>
        /// <param name="id">The entity's id.</param>
        /// <returns>
        /// The entity or null if it was not found.
        /// </returns>
        /// <exception cref="BackendException">A data error occurred.</exception>
        public override ApplicationVersion GetById(long id)
        {
            try
            {
                return objectSet.SingleOrDefault(e => e.ID == id);
            }
            catch (DataException ex)
            {
                Log.ErrorException("Could not get the application version with id = " + id, ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Determines whether the specified version is assigned to at least one license.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// true if the version is assigned to at least one license; otherwise, false.
        /// </returns>
        /// <exception cref="BackendException">A data error has occurred.</exception>
        public bool IsAssignedToLicense(string version)
        {
            try
            {
                var licenseRepository = new LicenseRepository(this.context);
                var licenseCount = licenseRepository.Query(lic => lic.ApplicationVersion.Trim() == version.Trim()).Count();
                return licenseCount > 0;
            }
            catch (DataException ex)
            {
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }
    }
}
