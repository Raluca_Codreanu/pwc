﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;
using ZPKTool.LicenseManagerBackend.Interfaces;
using ZPKTool.LicenseManagerBackend.Repository;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerBackend
{
    /// <summary>
    /// Maintains a list of objects affected by a business transaction and coordinates the writing out of changes and the resolution of concurrency problems.
    /// </summary>
    public partial class UnitOfWork : IUnitOfWork
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private readonly ObjectContext context;
        private CompanyRepository companies;
        private DistributorRepository distributors;
        private LicenseRepository licenses;
        private UserRepository users;
        private IApplicationVersionRepository applicationVersions;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(ObjectContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context wasn't supplied");
            }

            this.context = context;
        }

        #endregion

        #region IUnitOfWork Members

        /// <summary>
        /// Gets the companies.
        /// </summary>
        /// <value>The companies.</value>
        public ICompanyRepository Companies
        {
            get { return companies ?? (companies = new CompanyRepository(context)); }
        }

        /// <summary>
        /// Gets the distributors.
        /// </summary>
        /// <value>The distributors.</value>
        public IDistribuitorRepository Distributors
        {
            get { return distributors ?? (distributors = new DistributorRepository(context)); }
        }

        /// <summary>
        /// Gets the licenses.
        /// </summary>
        /// <value>The licenses.</value>
        public ILicenseRepository Licenses
        {
            get { return licenses ?? (licenses = new LicenseRepository(context)); }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <value>The users.</value>
        public IUserRepository Users
        {
            get { return users ?? (users = new UserRepository(context)); }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <value>The users.</value>
        public IApplicationVersionRepository ApplicationVersions
        {
            get { return this.applicationVersions ?? (this.applicationVersions = new ApplicationVersionRepository(context)); }
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <exception cref="BackendException">BackendException.</exception>
        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DataException ex)
            {
                log.ErrorException("Could not save changes.", ex);
                throw new BackendException(ErrorCodes.InternalError, ex);
            }
        }

        #endregion
    }
}
