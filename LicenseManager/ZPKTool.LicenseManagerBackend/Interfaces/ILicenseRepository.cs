﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerBackend.Interfaces
{
    public interface ILicenseRepository : IRepository<License>
    {
        IEnumerable<License> GetByUser(long id);
        bool CheckIfLicenseUsesDistributor(long id);
    }
}
