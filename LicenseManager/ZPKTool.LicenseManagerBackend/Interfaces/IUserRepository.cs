﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerBackend.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {       
       IEnumerable<User> GetByCompany(long id);
    }
}
