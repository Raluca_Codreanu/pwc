﻿namespace ZPKTool.LicenseManagerBackend.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The interface of the repository for the <see cref="ApplicationVersion"/> entity.
    /// </summary>
    public interface IApplicationVersionRepository : IRepository<ApplicationVersion>
    {
        /// <summary>
        /// Determines whether the specified version is assigned to at least one license.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// true if the version is assigned to at least one license; otherwise, false.
        /// </returns>
        /// <exception cref="BackendException">A data error has occurred.</exception>
        bool IsAssignedToLicense(string version);
    }
}
