﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace ZPKTool.LicenseManagerBackend.Interfaces
{
    public interface IRepository<T> where T : class
    {
        #region	Methods

        T GetById(long id);
        IEnumerable<T> GetAll();       
        void Add(T entity);
        void Remove(T entity);

        #endregion
    }
}
