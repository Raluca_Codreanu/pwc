﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerBackend.Interfaces
{
    public interface IUnitOfWork
    {
        #region	Methods

        ICompanyRepository Companies { get; }
        IDistribuitorRepository Distributors { get; }
        ILicenseRepository Licenses { get; }
        IUserRepository Users { get; }
        void Commit();

        #endregion
    }
}
