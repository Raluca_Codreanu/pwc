﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ZPKTool.LicenseManagerBackend
{
    public partial class License
    {
        public License()
        {
            this.DistributorReference.AssociationChanged += (sender, e) => { if (e.Action == CollectionChangeAction.Remove) { OnPropertyChanging("Distributor"); } else { OnPropertyChanged("Distributor"); } };
            this.UserReference.AssociationChanged += (sender, e) => { if (e.Action == CollectionChangeAction.Remove) { OnPropertyChanging("User"); } else { OnPropertyChanged("User"); } };
        }
    }
}
