﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ZPKTool.LicenseManagerBackend
{
    public partial class User
    {
        public User()
        {
            this.CompanyReference.AssociationChanged += (sender, e) => { if (e.Action == CollectionChangeAction.Remove) { OnPropertyChanging("Company"); } else { OnPropertyChanged("Company"); } };
        }        
    }
}
