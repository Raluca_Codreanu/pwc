﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace ZPKTool.LicenseManagerUI.Views
{
    /// <summary>
    /// Interaction logic for VersionsView.xaml
    /// </summary>
    public partial class VersionsView : Window
    {
        /// <summary>
        /// A value indicating whether the name cell for a row will get the focus and begin editing.
        /// </summary>
        private bool beginEditCell = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionsView"/> class.
        /// </summary>
        public VersionsView()
        {
            this.InitializeComponent();

            var itemCollection = CollectionViewSource.GetDefaultView(this.VersionsDataGrid.Items) as INotifyCollectionChanged;
            if (itemCollection != null)
            {
                itemCollection.CollectionChanged += VersionsDataGrid_ItemsCollectionChanged;
            }
        }

        /// <summary>
        /// Handles the CollectionChanged event of the VersionsDataGrid control's Items collection.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void VersionsDataGrid_ItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems.Count == 1)
            {
                // If the item collection is changed by adding a new item, set the begin edit cell flag to true.
                this.beginEditCell = true;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the VersionsDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void VersionsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var addedItem = e.AddedItems[0];
                // Bring the selected item into view because the view model has no way of doing it.
                this.VersionsDataGrid.ScrollIntoView(addedItem);

                if (this.beginEditCell)
                {
                    try
                    {
                        // If the selected item is newly added, get the name cell of it's row.
                        var row = this.VersionsDataGrid.ItemContainerGenerator.ContainerFromItem(addedItem) as DataGridRow;
                        if (row != null)
                        {
                            var nameCell = this.VersionsDataGrid.GetCell(row, 0);
                            if (nameCell != null)
                            {
                                // Focus on the name cell for the added item and begin edit.
                                nameCell.Focus();
                                this.VersionsDataGrid.BeginEdit();
                            }
                        }
                    }
                    finally
                    {
                        this.beginEditCell = false;
                    }
                }
            }
        }
    }
}
