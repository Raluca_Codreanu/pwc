﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ZPKTool.LicenseManagerUI.Views
{
    /// <summary>
    /// Interaction logic for LicensesView.xaml
    /// </summary>
    public partial class LicensesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicensesView"/> class.
        /// </summary>
        public LicensesView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the AddUserButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            SerialNumberTxt.Dispatcher.BeginInvoke(new Action(delegate
            {
                SerialNumberTxt.Focusable = true;
                SerialNumberTxt.Focus();
                Keyboard.Focus(SerialNumberTxt);
            }), DispatcherPriority.Render);
        }
    }
}
