﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.MvvmCore;
using System.Windows.Threading;

namespace ZPKTool.LicenseManagerUI.Views
{
    /// <summary>
    /// Interaction logic for DistributorsView.xaml
    /// </summary>
    public partial class DistributorsView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorsView"/> class.
        /// </summary>
        public DistributorsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the AddDistributorButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void AddDistributorButton_Click(object sender, RoutedEventArgs e)
        {
            NameTxt.Dispatcher.BeginInvoke(new Action(delegate
            {
                NameTxt.Focusable = true;
                NameTxt.Focus();
                Keyboard.Focus(NameTxt);
            }), DispatcherPriority.Render);
        }
    }
}
