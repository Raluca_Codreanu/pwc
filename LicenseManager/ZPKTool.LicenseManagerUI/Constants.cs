﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// Defines global constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The max value of the textboxes if they contain numbers.
        /// </summary>
        public const decimal MaxNumberValue = 1000000000m;

        /// <summary>
        /// The max value for the smaller text boxes.
        /// </summary>
        public const decimal MaxSmallValue = 999999.99m;

        /// <summary>
        /// Font size for titles across the application.
        /// </summary>
        public const double TitleSize = 28;

        /// <summary>
        /// Font size for subtitles across the application.
        /// </summary>
        public const double SubtitleSize = 16;

        /// <summary>
        /// Font size for normal text across the application.
        /// </summary>
        public const double LabelFontSize = 12;

        /// <summary>
        /// Font size for text in input boxes across the application.
        /// </summary>
        public const double InputTextSize = 12;

        /// <summary>
        /// Font size for group box titles across the application.
        /// </summary>
        public const double GroupBoxTitleSize = 12;

        /// <summary>
        /// Font size for tab tiles across the application.
        /// </summary>
        public const double TabLabelSize = 12;

        /// <summary>
        /// The default padding applied to buttons in the application for a consistent look.
        /// </summary>
        public static readonly Thickness DefaultButtonPadding = new Thickness(9, 1, 9, 1);
    }
}
