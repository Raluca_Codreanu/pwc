﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AddCompanyViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The unit of work : Maintains a list of objects affected by a business
        /// transaction and coordinates the writing out of changes.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// The company's name.
        /// </summary>
        private string name;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AddCompanyViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messanger">The messenger.</param>
        [ImportingConstructor]
        public AddCompanyViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger)
        {
            this.container = container;
            this.messanger = messanger;
            this.windowService = windowService;
            this.AddCompanyCommand = new DelegateCommand(AddCompany);
            this.CancelCompanyCommand = new DelegateCommand(CloseAction);
            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add company command.
        /// </summary>
        /// <value>The add company command.</value>
        public ICommand AddCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the cancel company command.
        /// </summary>
        /// <value>The cancel company command.</value>
        public ICommand CancelCompanyCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the company name property.
        /// </summary>
        /// <value>The company name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        #endregion Properties

        #region Validation

        /// <summary>
        /// Validates the company input.
        /// </summary>
        private void IsValiCompanyInput()
        {
            List<string> errorList = new List<string>();
            if (this.Name == null || this.Name.Trim().Equals(string.Empty))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        #endregion

        #region Command Actions

        /// <summary>
        /// Adds a new company.
        /// </summary>
        /// <exception cref="UIException">UIException</exception>
        private void AddCompany()
        {
            try
            {
                IsValiCompanyInput();

                Company newCompany = new Company();
                newCompany.Name = this.Name;

                try
                {
                    this.unitOfWork.Companies.Add(newCompany);
                    this.unitOfWork.Commit();
                    var notification = new NotificationMessage<Company>(this, Notification.NewCompanyAdded, newCompany);
                    this.messanger.Send<NotificationMessage<Company>>(notification);
                    this.windowService.CloseViewWindow(this);
                }
                catch (BackendException ex)
                {
                    newCompany = null;
                    log.ErrorException("Error while adding company. ", ex);
                    throw new UIException(ErrorCodes.InternalError, ex);
                }
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Closes the window.
        /// </summary>
        private void CloseAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion
    }
}