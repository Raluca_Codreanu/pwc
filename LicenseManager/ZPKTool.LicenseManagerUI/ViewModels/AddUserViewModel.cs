﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AddUserViewModel : ViewModel
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The unit of work : Maintains a list of objects affected by a business
        /// transaction and coordinates the writing out of changes.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// The user's name.
        /// </summary>
        private string name;

        /// <summary>
        /// The user's phone information.
        /// </summary>
        private string phone;

        /// <summary>
        /// The user's fax information.
        /// </summary>
        private string fax;

        /// <summary>
        /// The user's email information.
        /// </summary>
        private string email;

        /// <summary>
        /// The user's street information.
        /// </summary>
        private string street;

        /// <summary>
        /// The user's city information.
        /// </summary>
        private string city;

        /// <summary>
        /// The user's postcode.
        /// </summary>
        private string postcode;

        /// <summary>
        /// The user's country.
        /// </summary>
        private string country;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AddUserViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messanger">The messanger.</param>
        /// <param name="company">The company.</param>
        [ImportingConstructor]
        public AddUserViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger, Company company)
        {
            this.container = container;
            this.windowService = windowService;
            this.messanger = messanger;
            this.Company = company;
            this.AddUserCommand = new DelegateCommand(AddUser);
            this.CancelUserCommand = new DelegateCommand(CloseAction);
            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add user command.
        /// </summary>
        /// <value>The add user command.</value>
        public ICommand AddUserCommand { get; private set; }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        /// <value>The cancel user command.</value>
        public ICommand CancelUserCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the User.
        /// </summary>
        /// <value>The user's name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                if (this.phone != value)
                {
                    this.phone = value;
                    OnPropertyChanged("Phone");
                }
            }
        }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The user's fax.</value>
        public string Fax
        {
            get
            {
                return this.fax;
            }

            set
            {
                if (this.fax != value)
                {
                    this.fax = value;
                    OnPropertyChanged("Fax");
                }
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (this.email != value)
                {
                    this.email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street
        {
            get
            {
                return this.street;
            }

            set
            {
                if (this.street != value)
                {
                    this.street = value;
                    OnPropertyChanged("Street");
                }
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The user's city.</value>
        public string City
        {
            get
            {
                return this.city;
            }

            set
            {
                if (this.city != value)
                {
                    this.city = value;
                    OnPropertyChanged("City");
                }
            }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        public string PostCode
        {
            get
            {
                return this.postcode;
            }

            set
            {
                if (this.postcode != value)
                {
                    this.postcode = value;
                    OnPropertyChanged("PostCode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country
        {
            get
            {
                return this.country;
            }

            set
            {
                if (this.country != value)
                {
                    this.country = value;
                    OnPropertyChanged("Country");
                }
            }
        }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public Company Company { get; set; }

        #endregion Properties

        #region Command actions

        /// <summary>
        /// Closes the window.
        /// </summary>
        private void CloseAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Adds an user.
        /// </summary>
        private void AddUser()
        {
            try
            {
                IsValidUserInput();

                User user = new User();
                user.Name = this.Name;
                user.Phone = this.Phone;
                user.Email = this.Email;
                user.Street = this.Street;
                user.Country = this.Country;
                user.PostCode = this.PostCode;
                user.Fax = this.Fax;
                user.City = this.City;
                user.Company = this.Company;

                this.unitOfWork.Users.Add(user);
                this.unitOfWork.Commit();

                this.messanger.Send(new NotificationMessage<User>(this, Notification.NewUserAdded, user));
                this.windowService.CloseViewWindow(this);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error while adding user. ", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Validates the user input.
        /// </summary>
        /// <exception cref="ValidationException">ValidationException</exception>
        private void IsValidUserInput()
        {
            List<string> errorList = new List<string>();

            if (this.Name == null || (this.Name != null && this.Name.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        #endregion
    }
}