﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the ICompanyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompanyViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The selected company.
        /// </summary>
        private Company selectedCompany;

        /// <summary>
        /// The unit of work : Maintains a list of objects affected by a business
        /// transaction and coordinates the writing out of changes.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// The new company added.
        /// </summary>
        private Company newCompany;

        /// <summary>
        /// The company name.
        /// </summary>
        private string name;

        /// <summary>
        /// True if new company is added, false otherwise.
        /// </summary>
        private bool isNewCompanyAdded;

        /// <summary>
        /// Indicates whether the company information grid is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isCompanyInformationGridEnabled;

        /// <summary>
        /// Indicates whether the save button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isSaveButtonEnabled;

        /// <summary>
        ///  Indicates whether the remove button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isRemoveButtonEnabled;

        /// <summary>
        /// Indicates whether the new button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isNewButtonEnabled;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messanger">The messenger.</param>
        [ImportingConstructor]
        public CompanyViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger)
        {
            this.container = container;
            this.windowService = windowService;
            this.messanger = messanger;

            // Delegate commands
            this.NewCompanyCommand = new DelegateCommand(this.AddNewCompany);
            this.RemoveCompanyCommand = new DelegateCommand(this.RemoveCompany);
            this.SaveCompanyCommand = new DelegateCommand(this.SaveCompany);
            this.CompaniesDataGridSelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(this.CompaniesDataGridSelectionChanged);
            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
            this.Companies = new ObservableCollection<Company>(this.unitOfWork.Companies.GetAll());
            this.WindowClosingCommand = new DelegateCommand<CancelEventArgs>(this.WindowClosing);
            this.PreviewMouseLeftButtonDownDataGridCommand = new DelegateCommand<MouseButtonEventArgs>(this.PreviewMouseLeftButtonDownDataGrid);

            this.IsCompanyInformationGridEnabled = false;
            this.IsSaveButtonEnabled = false;
            this.IsRemoveButtonEnabled = false;
            this.IsNewButtonEnabled = true;
            this.IsNewCompanyAdded = false;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>The add command.</value>
        public ICommand NewCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the remove command.
        /// </summary>
        /// <value>The remove command.</value>
        public ICommand RemoveCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the save company command.
        /// </summary>
        /// <value>The save company command.</value>
        public ICommand SaveCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the window closing command.
        /// </summary>
        /// <value>The window closing.</value>
        public ICommand WindowClosingCommand { get; private set; }

        /// <summary>
        /// Gets the Companys data grid selection changed command.
        /// </summary>
        /// <value>The Companys data grid selection changed command.</value>
        public ICommand CompaniesDataGridSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the preview mouse left button down data grid command.
        /// </summary>
        /// <value>The preview mouse left button down data grid command.</value>
        public ICommand PreviewMouseLeftButtonDownDataGridCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the Companies observable collection.
        /// </summary>
        /// <value>The Companies.</value>
        public ObservableCollection<Company> Companies { get; private set; }

        /// <summary>
        /// Gets or sets the selected company.
        /// </summary>
        /// <value>The selected company.</value>
        public Company SelectedCompany
        {
            get
            {
                return this.selectedCompany;
            }

            set
            {
                if (this.selectedCompany != value)
                {
                    this.selectedCompany = value;
                    OnPropertyChanged("SelectedCompany");
                }
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The company name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [company information grid is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [company information grid is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsCompanyInformationGridEnabled
        {
            get
            {
                return this.isCompanyInformationGridEnabled;
            }

            set
            {
                if (this.isCompanyInformationGridEnabled != value)
                {
                    this.isCompanyInformationGridEnabled = value;
                    OnPropertyChanged("IsCompanyInformationGridEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [save button is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [save button is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSaveButtonEnabled
        {
            get
            {
                return this.isSaveButtonEnabled;
            }

            set
            {
                if (this.isSaveButtonEnabled != value)
                {
                    this.isSaveButtonEnabled = value;
                    OnPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is remove button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is remove button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemoveButtonEnabled
        {
            get
            {
                return this.isRemoveButtonEnabled;
            }

            set
            {
                if (this.isRemoveButtonEnabled != value)
                {
                    this.isRemoveButtonEnabled = value;
                    OnPropertyChanged("IsRemoveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewButtonEnabled
        {
            get
            {
                return this.isNewButtonEnabled;
            }

            set
            {
                if (this.isNewButtonEnabled != value)
                {
                    this.isNewButtonEnabled = value;
                    OnPropertyChanged("IsNewButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new company added.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new company added; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewCompanyAdded
        {
            get
            {
                return this.isNewCompanyAdded;
            }

            set
            {
                if (this.isNewCompanyAdded != value)
                {
                    this.isNewCompanyAdded = value;
                    OnPropertyChanged("IsNewCompanyAdded");
                }
            }
        }

        #endregion Properties

        #region Command Actions

        /// <summary>
        /// Adds a new Company.
        /// </summary>
        private void AddNewCompany()
        {
            if (!this.IsNewCompanyAdded)
            {
                if (this.SelectedCompany != null && this.CompanyEdited(this.SelectedCompany))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataItemAdd, MessageDialogType.YesNo);
                    if (MessageDialogResult.Yes == messageBoxResult)
                    {
                        this.UpdateCompany();
                    }
                }

                this.IsSaveButtonEnabled = true;
                this.IsRemoveButtonEnabled = true;
                this.newCompany = new Company();
                this.Companies.Add(this.newCompany);
                this.SelectedCompany = this.newCompany;
                this.IsNewCompanyAdded = true;
                this.IsNewButtonEnabled = false;
            }
        }

        /// <summary>
        /// Clears the Company properties.
        /// </summary>
        private void ClearCompanyProperties()
        {
            this.Name = string.Empty;
        }

        /// <summary>
        /// Remove company action.
        /// </summary>
        /// <exception cref="UIException">UIException.</exception>
        private void RemoveCompany()
        {
            if (!this.IsNewCompanyAdded)
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_DeleteCompanyWarning, MessageDialogType.YesNo);
                if (MessageDialogResult.Yes == messageBoxResult)
                {
                    try
                    {
                        Company companyToRemove = this.SelectedCompany;
                        this.ClearCompanyProperties();
                        this.unitOfWork.Companies.Remove(companyToRemove);
                        this.unitOfWork.Commit();
                        this.Companies.Remove(companyToRemove);
                        this.messanger.Send(new NotificationMessage<Company>(this, Notification.CompanyRemoved, companyToRemove));
                    }
                    catch (BackendException ex)
                    {
                        log.ErrorException("Exception while removing company.", ex);
                        throw new UIException(ErrorCodes.InternalError, ex);
                    }
                }
            }
            else
            {
                this.ClearCompanyProperties();
                this.Companies.Remove(this.SelectedCompany);
            }
        }

        /// <summary>
        /// Saves the current selected Company.
        /// </summary>
        private void SaveCompany()
        {
            if (this.IsNewCompanyAdded == true)
            {
                this.CreateCompany();
            }
            else
            {
                this.UpdateCompany();
            }

            this.IsNewButtonEnabled = true;
        }

        /// <summary>
        /// Creates a new Company.
        /// </summary>
        /// <exception cref="UiException">UIException</exception>
        private void CreateCompany()
        {
            try
            {
                this.ValidateCompanyInput();
                this.newCompany.Name = this.Name;

                try
                {
                    this.unitOfWork.Companies.Add(this.newCompany);
                    this.unitOfWork.Commit();
                    this.messanger.Send(new NotificationMessage<Company>(this, Notification.NewCompanyAdded, this.newCompany));
                }
                catch (BackendException ex)
                {
                    log.ErrorException("Error while creating company.", ex);
                    throw new UIException(ErrorCodes.InternalError, ex);
                }
                finally
                {
                    this.IsNewCompanyAdded = false;
                }
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Validates the user input.
        /// </summary>
        /// <exception cref="ValidationException">ValidationException</exception>
        private void ValidateCompanyInput()
        {
            List<string> errorList = new List<string>();

            if (this.Name == null || (this.Name != null && this.Name.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        /// <summary>
        /// Updates the Company.
        /// </summary>
        private void UpdateCompany()
        {
            try
            {
                this.ValidateCompanyInput();
                this.SelectedCompany.Name = this.Name;
                this.unitOfWork.Commit();
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error encontered while updating a company ", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Indicates whether the selected company is in edit mode.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <returns>
        /// True if the selected company is currently edited, false otherwise.
        /// </returns>
        private bool CompanyEdited(Company company)
        {
            return this.IsNewCompanyAdded == true ||
                ((this.Name != null) && company != null &&
                company.Name != null &&
                !company.Name.Trim().Equals(this.Name.Trim()));
        }

        /// <summary>
        /// Occurs when the data grid selection changes.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CompaniesDataGridSelectionChanged(SelectionChangedEventArgs arg)
        {
            // enable/disable save button and company information grid
            if (this.SelectedCompany == null)
            {
                this.IsRemoveButtonEnabled = false;
                this.IsSaveButtonEnabled = false;
                this.IsCompanyInformationGridEnabled = false;
            }
            else
            {
                this.IsRemoveButtonEnabled = true;
                this.IsSaveButtonEnabled = true;
                this.IsCompanyInformationGridEnabled = true;
            }

            if (this.IsNewCompanyAdded == true)
            {
                this.IsNewButtonEnabled = true;
                this.Companies.Remove(this.newCompany);
                this.IsNewCompanyAdded = false;
            }

            if (arg.OriginalSource is DataGrid)
            {
                DataGrid dataGrid = arg.OriginalSource as DataGrid;
                Company selectedCompany = dataGrid.SelectedItem as Company;

                if (selectedCompany != null)
                {
                    dataGrid.ScrollIntoView(selectedCompany);
                    this.Name = this.selectedCompany.Name;
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down data grid.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PreviewMouseLeftButtonDownDataGrid(MouseButtonEventArgs arg)
        {
            Company targetCompany = null;
            System.Windows.UIElement source = arg.OriginalSource as System.Windows.UIElement;

            DataGridRow container = source as DataGridRow;
            while (container == null && source != null)
            {
                source = System.Windows.Media.VisualTreeHelper.GetParent(source) as System.Windows.UIElement;
                container = source as DataGridRow;
            }

            if (container != null && container.Item != null)
            {
                targetCompany = container.Item as Company;

                if (this.CompanyEdited(this.SelectedCompany))
                {
                    if (this.SelectedCompany != targetCompany)
                    {
                        MessageDialogResult messageBoxResult =
                            this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                        if (MessageDialogResult.Yes == messageBoxResult)
                        {
                            this.SelectedCompany = targetCompany;
                        }
                        else
                        {
                            arg.Handled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Occurs directly after Close is called, and can be handled to cancel window closure.
        /// </summary>
        /// <param name="arg">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void WindowClosing(CancelEventArgs arg)
        {
            if (this.SelectedCompany != null && this.CompanyEdited(this.SelectedCompany))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedData, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Cancel = true;
                }
            }
        }

        #endregion
    }
}