﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UsersViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The mediator.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The name of the user.
        /// </summary>
        private string name;

        /// <summary>
        /// The user's phone information.
        /// </summary>
        private string phone;

        /// <summary>
        /// The user's fax information.
        /// </summary>
        private string fax;

        /// <summary>
        /// The user's email information.
        /// </summary>
        private string email;

        /// <summary>
        /// The user's street information.
        /// </summary>
        private string street;

        /// <summary>
        /// The user's city information.
        /// </summary>
        private string city;

        /// <summary>
        /// The user's postcode.
        /// </summary>
        private string postcode;

        /// <summary>
        /// The user's country.
        /// </summary>
        private string country;

        /// <summary>
        /// The selected user.
        /// </summary>
        private User selectedUser;

        /// <summary>
        /// The unit of work : Maintains a list of objects affected by a business
        /// transaction and coordinates the writing out of changes.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// The added new user.
        /// </summary>
        private User newUser;

        /// <summary>
        /// The selected company from the combobox.
        /// </summary>
        private Company selectedCompany;

        /// <summary>
        /// The added new user flag.
        /// </summary>
        private bool isNewUserAdded = false;

        /// <summary>
        /// Indicates whether the users data grid is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isUsersInformationGridIsEnabled;

        /// <summary>
        /// Indicates whether the save button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isSaveButtonEnabled;

        /// <summary>
        ///  Indicates whether the remove button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isRemoveButtonEnabled;

        /// <summary>
        /// Indicates whether the new button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isNewButtonEnabled;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messanger">The messanger.</param>
        [ImportingConstructor]
        public UsersViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger)
        {
            this.container = container;
            this.windowService = windowService;
            this.messanger = messanger;
            this.NewUserCommand = new DelegateCommand(AddNewUser);
            this.RemoveUserCommand = new DelegateCommand(RemoveUser);
            this.SaveUserCommand = new DelegateCommand(SaveUser);
            this.AddNewCompanyCommand = new DelegateCommand(AddNewCompany);
            this.UsersDataGridSelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(UsersDataGridSelectionChanged);
            this.CloseUsersCommand = new DelegateCommand(CloseUsers);
            this.WindowClosingCommand = new DelegateCommand<CancelEventArgs>(WindowClosing);
            this.PreviewMouseLeftButtonDownDataGridCommand = new DelegateCommand<MouseButtonEventArgs>(PreviewMouseLeftButtonDownDataGrid);

            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
            this.Users = new ObservableCollection<User>(unitOfWork.Users.GetAll());
            this.Companies = new ObservableCollection<Company>(unitOfWork.Companies.GetAll());

            this.messanger.Register<NotificationMessage<Company>>(ReceiveMessage);

            this.UsersInformationGridIsEnabled = false;
            this.IsSaveButtonEnabled = false;
            this.IsRemoveButtonEnabled = false;
            this.IsNewButtonEnabled = true;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>The add command.</value>
        public ICommand NewUserCommand { get; private set; }

        /// <summary>
        /// Gets the remove command.
        /// </summary>
        /// <value>The remove command.</value>
        public ICommand RemoveUserCommand { get; private set; }

        /// <summary>
        /// Gets the save user command.
        /// </summary>
        /// <value>The save user command.</value>
        public ICommand SaveUserCommand { get; private set; }

        /// <summary>
        /// Gets the add new company command.
        /// </summary>
        /// <value>The add new company command.</value>
        public ICommand AddNewCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the users data grid selection changed command.
        /// </summary>
        /// <value>The users data grid selection changed command.</value>
        public ICommand UsersDataGridSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the close users command.
        /// </summary>
        /// <value>The close users command.</value>
        public ICommand CloseUsersCommand { get; private set; }

        /// <summary>
        /// Gets the window closing.
        /// </summary>
        /// <value>The window closing.</value>
        public ICommand WindowClosingCommand { get; private set; }

        /// <summary>
        /// Gets the preview mouse left button down data grid command.
        /// </summary>
        /// <value>The preview mouse left button down data grid command.</value>
        public ICommand PreviewMouseLeftButtonDownDataGridCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [save button is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [save button is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSaveButtonEnabled
        {
            get
            {
                return this.isSaveButtonEnabled;
            }

            set
            {
                if (this.isSaveButtonEnabled != value)
                {
                    this.isSaveButtonEnabled = value;
                    OnPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is remove button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is remove button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemoveButtonEnabled
        {
            get
            {
                return this.isRemoveButtonEnabled;
            }

            set
            {
                if (this.isRemoveButtonEnabled != value)
                {
                    this.isRemoveButtonEnabled = value;
                    OnPropertyChanged("IsRemoveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewButtonEnabled
        {
            get
            {
                return this.isNewButtonEnabled;
            }

            set
            {
                if (this.isNewButtonEnabled != value)
                {
                    this.isNewButtonEnabled = value;
                    OnPropertyChanged("IsNewButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [users information grid is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [users information grid is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool UsersInformationGridIsEnabled
        {
            get
            {
                return this.isUsersInformationGridIsEnabled;
            }

            set
            {
                if (this.isUsersInformationGridIsEnabled != value)
                {
                    this.isUsersInformationGridIsEnabled = value;
                    OnPropertyChanged("UsersInformationGridIsEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected user.
        /// </summary>
        /// <value>The selected user.</value>
        public User SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                if (this.selectedUser != value)
                {
                    this.selectedUser = value;
                    OnPropertyChanged("SelectedUser");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected company.
        /// </summary>
        /// <value>The selected company.</value>
        public Company SelectedCompany
        {
            get
            {
                return this.selectedCompany;
            }

            set
            {
                if (this.selectedCompany != value)
                {
                    this.selectedCompany = value;
                    OnPropertyChanged("SelectedCompany");
                }
            }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <value>The users.</value>
        public ObservableCollection<User> Users { get; private set; }

        /// <summary>
        /// Gets the companies.
        /// </summary>
        /// <value>The companies.</value>
        public ObservableCollection<Company> Companies { get; private set; }

        /// <summary>
        /// Gets or sets the name of the User.
        /// </summary>
        /// <value>The user name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                if (this.phone != value)
                {
                    this.phone = value;
                    OnPropertyChanged("Phone");
                }
            }
        }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The user's fax.</value>
        public string Fax
        {
            get
            {
                return this.fax;
            }

            set
            {
                if (this.fax != value)
                {
                    this.fax = value;
                    OnPropertyChanged("Fax");
                }
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (this.email != value)
                {
                    this.email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street
        {
            get
            {
                return this.street;
            }

            set
            {
                if (this.street != value)
                {
                    this.street = value;
                    OnPropertyChanged("Street");
                }
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The user's city.</value>
        public string City
        {
            get
            {
                return this.city;
            }

            set
            {
                if (this.city != value)
                {
                    this.city = value;
                    OnPropertyChanged("City");
                }
            }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        public string PostCode
        {
            get
            {
                return this.postcode;
            }

            set
            {
                if (this.postcode != value)
                {
                    this.postcode = value;
                    OnPropertyChanged("PostCode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country
        {
            get
            {
                return this.country;
            }

            set
            {
                if (this.country != value)
                {
                    this.country = value;
                    OnPropertyChanged("Country");
                }
            }
        }

        #endregion Properties

        #region MessangerMessages

        /// <summary>
        /// Receives the message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveMessage(NotificationMessage<Company> message)
        {
            if (message.Notification == Notification.NewCompanyAdded)
            {
                if (message.Content != null)
                {
                    Company addedCompany = message.Content;
                    this.Companies.Add(addedCompany);
                    this.SelectedCompany = addedCompany;
                }
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Validates the user input.
        /// </summary>
        /// <exception cref="ValidationException">ValidationException</exception>
        private void IsValidUserInput()
        {
            List<string> errorList = new List<string>();

            if (this.Name == null || (this.Name != null && this.Name.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (this.SelectedCompany == null)
            {
                errorList.Add(LanguageResource.ValidationError_CompanyNotSelected);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        #endregion

        #region Command Actions

        /// <summary>
        /// Adds a new company.
        /// </summary>
        private void AddNewCompany()
        {
            var viewModel = this.container.GetExportedValue<AddCompanyViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Adds a new user.
        /// </summary>
        private void AddNewUser()
        {
            if (!this.isNewUserAdded)
            {
                if (this.SelectedUser != null && this.IsUserEdited(this.SelectedUser))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataItemAdd, MessageDialogType.YesNo);
                    if (MessageDialogResult.Yes == messageBoxResult)
                    {
                        this.UpdateUser();
                    }
                }

                this.IsSaveButtonEnabled = true;
                this.IsRemoveButtonEnabled = true;
                this.newUser = new User();
                this.Users.Add(this.newUser);
                this.SelectedUser = this.newUser;
                this.isNewUserAdded = true;
                this.IsNewButtonEnabled = false;
            }
        }

        /// <summary>
        /// Clears the user properties.
        /// </summary>
        private void ClearUserProperties()
        {
            this.Name = string.Empty;
            this.Country = string.Empty;
            this.Email = string.Empty;
            this.Fax = string.Empty;
            this.Phone = string.Empty;
            this.PostCode = string.Empty;
            this.Street = string.Empty;
            this.City = string.Empty;
        }

        /// <summary>
        /// Removes the user.
        /// </summary>
        /// <exception cref="UIException">UIException</exception>
        private void RemoveUser()
        {
            if (!this.isNewUserAdded)
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_DeleteUser, MessageDialogType.YesNo);
                if (MessageDialogResult.Yes == messageBoxResult)
                {
                    try
                    {
                        User userToRemove = this.SelectedUser;
                        this.unitOfWork.Users.Remove(userToRemove);
                        this.unitOfWork.Commit();
                        this.ClearUserProperties();
                        this.Users.Remove(userToRemove);
                        this.messanger.Send(new NotificationMessage<User>(this, Notification.UserRemoved, userToRemove));
                    }
                    catch (BackendException ex)
                    {
                        log.ErrorException("Exception while removing user", ex);
                        throw new UIException(ErrorCodes.InternalError, ex);
                    }
                }
            }
            else
            {
                this.ClearUserProperties();
                this.Users.Remove(this.SelectedUser);
            }
        }

        /// <summary>
        /// Saves the user.
        /// </summary>
        private void SaveUser()
        {
            if (this.isNewUserAdded == true)
            {
                this.CreateUser();
            }
            else
            {
                this.UpdateUser();
            }

            this.IsNewButtonEnabled = true;
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        private void CreateUser()
        {
            try
            {
                this.IsValidUserInput();
                this.SetNewUsersProperties();
                this.newUser.Company = this.SelectedCompany;
                this.unitOfWork.Users.Add(this.newUser);
                this.unitOfWork.Commit();
                this.messanger.Send(new NotificationMessage<User>(this, Notification.NewUserAdded, this.newUser));
                this.isNewUserAdded = false;
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error while creating user.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Sets the new users properties.
        /// </summary>
        private void SetNewUsersProperties()
        {
            this.newUser.Name = this.Name;
            this.newUser.Company = this.SelectedCompany;
            this.newUser.Country = this.Country;
            this.newUser.Email = this.Email;
            this.newUser.Fax = this.Fax;
            this.newUser.City = this.City;
            this.newUser.Street = this.Street;
            this.newUser.PostCode = this.PostCode;
            this.newUser.Phone = this.Phone;
        }

        /// <summary>
        /// Updates the user.
        /// </summary>
        private void UpdateUser()
        {
            try
            {
                IsValidUserInput();

                if (this.Name != null && !this.SelectedUser.Name.Equals(this.Name.Trim()))
                {
                    this.SelectedUser.Name = this.Name;
                }

                if (this.SelectedUser.Company != this.SelectedCompany)
                {
                    this.SelectedUser.Company = this.SelectedCompany;
                }

                if (this.SelectedCompany != null)
                {
                    this.SelectedUser.Company = this.SelectedCompany;
                }

                if (this.Country != null)
                {
                    this.SelectedUser.Country = this.Country;
                }

                if (this.Email != null)
                {
                    this.SelectedUser.Email = this.Email;
                }

                if (this.Fax != null)
                {
                    this.SelectedUser.Fax = this.Fax;
                }

                if (this.Phone != null)
                {
                    this.SelectedUser.Phone = this.Phone;
                }

                if (this.Street != null)
                {
                    this.SelectedUser.Street = this.Street;
                }

                if (this.PostCode != null)
                {
                    this.SelectedUser.PostCode = this.PostCode;
                }

                if (this.City != null)
                {
                    this.SelectedUser.City = this.City;
                }

                this.unitOfWork.Commit();
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error while editing user.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Userses the data grid selection changed.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void UsersDataGridSelectionChanged(SelectionChangedEventArgs arg)
        {
            // enable/disable save button and company information grid
            if (this.SelectedUser == null)
            {
                this.IsRemoveButtonEnabled = false;
                this.IsSaveButtonEnabled = false;
                this.UsersInformationGridIsEnabled = false;
            }
            else
            {
                this.IsRemoveButtonEnabled = true;
                this.IsSaveButtonEnabled = true;
                this.UsersInformationGridIsEnabled = true;
            }

            if (this.isNewUserAdded == true)
            {
                this.IsNewButtonEnabled = true;
                this.Users.Remove(this.newUser);
                this.isNewUserAdded = false;
            }

            if (arg.OriginalSource is DataGrid)
            {
                DataGrid dataGrid = arg.OriginalSource as DataGrid;
                User selectedUser = dataGrid.SelectedItem as User;

                if (selectedUser != null)
                {
                    dataGrid.ScrollIntoView(selectedUser);
                    this.Name = selectedUser.Name;
                    this.SelectedCompany = selectedUser.Company;
                    this.Country = selectedUser.Country;
                    this.Email = selectedUser.Email;
                    this.Fax = selectedUser.Fax;
                    this.PostCode = selectedUser.PostCode;
                    this.Phone = selectedUser.Phone;
                    this.City = selectedUser.City;
                    this.Street = selectedUser.Street;
                }
            }
        }

        /// <summary>
        /// Closes the users.
        /// </summary>
        private void CloseUsers()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Windows the closing.
        /// </summary>
        /// <param name="arg">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void WindowClosing(CancelEventArgs arg)
        {
            if (this.IsUserEdited(this.SelectedUser))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedData, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down data grid.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PreviewMouseLeftButtonDownDataGrid(MouseButtonEventArgs arg)
        {
            User targetUser = null;

            System.Windows.UIElement source = arg.OriginalSource as System.Windows.UIElement;

            DataGridRow container = source as DataGridRow;
            while (container == null && source != null)
            {
                source = System.Windows.Media.VisualTreeHelper.GetParent(source) as System.Windows.UIElement;
                container = source as DataGridRow;
            }

            if (container != null && container.Item != null)
            {
                targetUser = container.Item as User;

                if (this.SelectedUser != targetUser)
                {
                    if (this.IsUserEdited(this.SelectedUser))
                    {
                        MessageDialogResult messageBoxResult =
                            this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                        if (MessageDialogResult.Yes == messageBoxResult)
                        {
                            this.SelectedUser = targetUser;
                        }
                        else
                        {
                            arg.Handled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Indicates if a user was edited.
        /// </summary>
        /// <param name="user">The user witch is checked.</param>
        /// <returns>
        /// True if user was edited, false otherwise.
        /// </returns>
        private bool IsUserEdited(User user)
        {
            return this.isNewUserAdded == true ||
               (this.Name != null && user != null && user.Name != null && !user.Name.Trim().Equals(this.Name.Trim())) ||
               (this.Phone != null && user != null && user.Phone != null && !user.Phone.Trim().Equals(this.Phone.Trim())) ||
               (this.Email != null && user != null && user.Email != null && !user.Email.Trim().Equals(this.Email.Trim())) ||
               (this.Street != null && user != null && user.Street != null && !user.Street.Trim().Equals(this.Street.Trim())) ||
               (this.Country != null && user != null && user.Country != null && !user.Country.Trim().Equals(this.Country.Trim())) ||
               (this.PostCode != null && user != null && user.PostCode != null && !user.PostCode.Trim().Equals(this.PostCode.Trim())) ||
               (this.Fax != null && user != null && user.Fax != null && !user.Fax.Trim().Equals(this.Fax.Trim())) ||
               (this.City != null && user != null && user.City != null && !user.City.Trim().Equals(this.City.Trim())) ||
               (this.SelectedCompany != null && user != null && user.Company != null && this.SelectedCompany.ID != user.Company.ID);
        }

        #endregion
    }
}