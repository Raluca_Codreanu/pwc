﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AddDistributorViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The name of the Distributor.
        /// </summary>
        private string name;

        /// <summary>
        /// The Distributor's phone information.
        /// </summary>
        private string phone;

        /// <summary>
        /// The Distributor's fax information.
        /// </summary>
        private string fax;

        /// <summary>
        /// The Distributor's email information.
        /// </summary>
        private string email;

        /// <summary>
        /// The Distributor's street information.
        /// </summary>
        private string street;

        /// <summary>
        /// The Distributor's city information.
        /// </summary>
        private string city;

        /// <summary>
        /// The Distributor's postcode.
        /// </summary>
        private string postcode;

        /// <summary>
        /// The distributor's webpage
        /// </summary>
        private string webPage;

        /// <summary>
        /// The unit of work : Maintains a list of objects affected by a business
        /// transaction and coordinates the writing out of changes.
        /// </summary>
        private UnitOfWork unitOfWork;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AddDistributorViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messanger">The messanger.</param>
        [ImportingConstructor]
        public AddDistributorViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger)            
        {
            this.container = container;
            this.windowService = windowService;
            this.messanger = messanger;

            this.AddDistributorCommand = new DelegateCommand(AddDistributor);
            this.CancelDistributorCommand = new DelegateCommand(CloseAction);
            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add company command.
        /// </summary>
        /// <value>The add company command.</value>
        public ICommand AddDistributorCommand { get; private set; }

        /// <summary>
        /// Gets the cancel distributor command.
        /// </summary>
        /// <value>The cancel distributor command.</value>
        public ICommand CancelDistributorCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the Distributor.
        /// </summary>
        /// <value>The distributor name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                if (this.phone != value)
                {
                    this.phone = value;
                    OnPropertyChanged("Phone");
                }
            }
        }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The distributor fax.</value>
        public string Fax
        {
            get
            {
                return this.fax;
            }

            set
            {
                if (this.fax != value)
                {
                    this.fax = value;
                    OnPropertyChanged("Fax");
                }
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (this.email != value)
                {
                    this.email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street
        {
            get
            {
                return this.street;
            }

            set
            {
                if (this.street != value)
                {
                    this.street = value;
                    OnPropertyChanged("Street");
                }
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The distributor's city.</value>
        public string City
        {
            get
            {
                return this.city;
            }

            set
            {
                if (this.city != value)
                {
                    this.city = value;
                    OnPropertyChanged("City");
                }
            }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        public string PostCode
        {
            get
            {
                return this.postcode;
            }

            set
            {
                if (this.postcode != value)
                {
                    this.postcode = value;
                    OnPropertyChanged("PostCode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the web page.
        /// </summary>
        /// <value>The web page.</value>
        public string WebPage
        {
            get
            {
                return this.webPage;
            }

            set
            {
                if (this.webPage != value)
                {
                    this.webPage = value;
                    OnPropertyChanged("WebPage");
                }
            }
        }

        #endregion Properties

        #region Validation

        /// <summary>
        /// Validates the Distributor input.
        /// </summary>
        /// <exception cref="ValidationException">ValidationException</exception>
        private void ValidateDistributorInput()
        {
            List<string> errorList = new List<string>();
            if (this.Name == null || (this.Name != null && this.Name.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        #endregion

        #region Command Actions

        /// <summary>
        /// Adds a new distributor.
        /// </summary>
        /// <exception cref="UIException">UIException</exception>
        private void AddDistributor()
        {
            try
            {
                ValidateDistributorInput();

                Distributor newDistributor = new Distributor();
                newDistributor.Name = this.Name;
                newDistributor.City = this.City;
                newDistributor.Email = this.Email;
                newDistributor.Fax = this.Fax;
                newDistributor.Phone = this.Phone;
                newDistributor.PostCode = this.PostCode;
                newDistributor.Street = this.Street;
                newDistributor.WebPage = this.WebPage;

                try
                {
                    this.unitOfWork.Distributors.Add(newDistributor);
                    this.unitOfWork.Commit();
                    this.messanger.Send(new NotificationMessage<Distributor>(this, Notification.NewDistributorAdded, newDistributor));
                    this.windowService.CloseViewWindow(this);
                }
                catch (BackendException ex)
                {
                    log.ErrorException("Error while adding company. ", ex);
                    throw new UIException(ErrorCodes.InternalError, ex);
                }
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Closes the window.
        /// </summary>
        private void CloseAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion
    }
}