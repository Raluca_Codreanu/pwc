﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Common;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IDistributorsView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DistributorsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The name of the Distributor.
        /// </summary>
        private string name;

        /// <summary>
        /// The Distributor's phone information.
        /// </summary>
        private string phone;

        /// <summary>
        /// The Distributor's fax information.
        /// </summary>
        private string fax;

        /// <summary>
        /// The Distributor's email information.
        /// </summary>
        private string email;

        /// <summary>
        /// The Distributor's street information.
        /// </summary>
        private string street;

        /// <summary>
        /// The Distributor's city information.
        /// </summary>
        private string city;

        /// <summary>
        /// The Distributor's postcode.
        /// </summary>
        private string postcode;

        /// <summary>
        /// The distributor's webpage
        /// </summary>
        private string webPage;

        /// <summary>
        /// Indicates whether the save button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isSaveButtonEnabled;

        /// <summary>
        ///  Indicates whether the remove button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isRemoveButtonEnabled;

        /// <summary>
        /// Indicates whether the new button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isNewButtonEnabled;

        /// <summary>
        /// The selected Distributor from the data grid.
        /// </summary>
        private Distributor selectedDistributor;

        /// <summary>
        /// The Unit of work responsible for Manage distributors.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// The distributor that will be added.
        /// </summary>
        private Distributor newDistributor;

        /// <summary>
        /// True if new company is currently added, false otherwise.
        /// </summary>
        private bool isNewDistributorAdded = false;

        /// <summary>
        /// Indicates whether the distributor information grid is enabled or not, true if it is enabled,
        /// false otherwise.
        /// </summary>
        private bool isDistributorsInformationGridIsEnabled;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorsViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="messageBoxService">The message box service.</param>
        /// <param name="messanger">The messenger.</param>
        [ImportingConstructor]
        public DistributorsViewModel(CompositionContainer container, IWindowService windowService, IMessenger messanger)
        {
            this.container = container;
            this.windowService = windowService;
            this.messanger = messanger;
            this.NewDistributorCommand = new DelegateCommand(AddNewDistributor);
            this.RemoveDistributorCommand = new DelegateCommand(RemoveDistributor);
            this.SaveDistributorCommand = new DelegateCommand(SaveDistributor);
            this.DistributorsDataGridSelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(DistributorsDataGridSelectionChanged);
            this.WindowClosingCommand = new DelegateCommand<CancelEventArgs>(WindowClosing);
            this.PreviewMouseLeftButtonDownDataGridCommand = new DelegateCommand<MouseButtonEventArgs>(PreviewMouseLeftButtonDownDataGrid);
            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();
            this.Distributors = new ObservableCollection<Distributor>(unitOfWork.Distributors.GetAll());
            this.DistributorsInformationGridIsEnabled = false;
            this.IsSaveButtonEnabled = false;
            this.IsRemoveButtonEnabled = false;
            this.IsNewButtonEnabled = true;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>The add command.</value>
        public ICommand NewDistributorCommand { get; private set; }

        /// <summary>
        /// Gets the remove command.
        /// </summary>
        /// <value>The remove command.</value>
        public ICommand RemoveDistributorCommand { get; private set; }

        /// <summary>
        /// Gets the save Distributor command.
        /// </summary>
        /// <value>The save Distributor command.</value>
        public ICommand SaveDistributorCommand { get; private set; }

        /// <summary>
        /// Gets the Distributors data grid selection changed command.
        /// </summary>
        /// <value>The Distributors data grid selection changed command.</value>
        public ICommand DistributorsDataGridSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the window closing.
        /// </summary>
        /// <value>The window closing.</value>
        public ICommand WindowClosingCommand { get; private set; }

        /// <summary>
        /// Gets the preview mouse left button down data grid command.
        /// </summary>
        /// <value>The preview mouse left button down data grid command.</value>
        public ICommand PreviewMouseLeftButtonDownDataGridCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the selected Distributor.
        /// </summary>
        /// <value>The selected Distributor.</value>
        public Distributor SelectedDistributor
        {
            get
            {
                return this.selectedDistributor;
            }

            set
            {
                if (this.selectedDistributor != value)
                {
                    this.selectedDistributor = value;
                    OnPropertyChanged("SelectedDistributor");
                }
            }
        }

        /// <summary>
        /// Gets the Distributors.
        /// </summary>
        /// <value>The Distributors.</value>
        public ObservableCollection<Distributor> Distributors { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [distributors information grid is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [distributors information grid is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool DistributorsInformationGridIsEnabled
        {
            get
            {
                return this.isDistributorsInformationGridIsEnabled;
            }

            set
            {
                if (this.isDistributorsInformationGridIsEnabled != value)
                {
                    this.isDistributorsInformationGridIsEnabled = value;
                    OnPropertyChanged("DistributorsInformationGridIsEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the Distributor.
        /// </summary>
        /// <value>The distributor name.</value>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                if (this.phone != value)
                {
                    this.phone = value;
                    OnPropertyChanged("Phone");
                }
            }
        }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The  distributor fax.</value>
        public string Fax
        {
            get
            {
                return this.fax;
            }

            set
            {
                if (this.fax != value)
                {
                    this.fax = value;
                    OnPropertyChanged("Fax");
                }
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (this.email != value)
                {
                    this.email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street
        {
            get
            {
                return this.street;
            }

            set
            {
                if (this.street != value)
                {
                    this.street = value;
                    OnPropertyChanged("Street");
                }
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The distributor city.</value>
        public string City
        {
            get
            {
                return this.city;
            }

            set
            {
                if (this.city != value)
                {
                    this.city = value;
                    OnPropertyChanged("City");
                }
            }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        public string PostCode
        {
            get
            {
                return this.postcode;
            }

            set
            {
                if (this.postcode != value)
                {
                    this.postcode = value;
                    OnPropertyChanged("PostCode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the web page.
        /// </summary>
        /// <value>The web page.</value>
        public string WebPage
        {
            get
            {
                return this.webPage;
            }

            set
            {
                if (this.webPage != value)
                {
                    this.webPage = value;
                    OnPropertyChanged("WebPage");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewButtonEnabled
        {
            get
            {
                return this.isNewButtonEnabled;
            }

            set
            {
                if (this.isNewButtonEnabled != value)
                {
                    this.isNewButtonEnabled = value;
                    OnPropertyChanged("IsNewButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is remove button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is remove button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemoveButtonEnabled
        {
            get
            {
                return this.isRemoveButtonEnabled;
            }

            set
            {
                if (this.isRemoveButtonEnabled != value)
                {
                    this.isRemoveButtonEnabled = value;
                    OnPropertyChanged("IsRemoveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [save button is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [save button is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSaveButtonEnabled
        {
            get
            {
                return this.isSaveButtonEnabled;
            }

            set
            {
                if (this.isSaveButtonEnabled != value)
                {
                    this.isSaveButtonEnabled = value;
                    OnPropertyChanged("IsSaveButtonEnabled");
                }
            }
        }

        #endregion Properties

        #region Validation

        /// <summary>
        /// Validates the Distributor input.
        /// </summary>
        /// <exception cref="ValidationException">ValidationException</exception>
        private void ValidateDistributorInput()
        {
            List<string> errorList = new List<string>();

            if (this.Name == null || (this.Name != null && this.Name.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_NameEmpty);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        #endregion

        #region Command Actions

        /// <summary>
        /// Adds a new distributor.
        /// </summary>
        private void AddNewDistributor()
        {
            if (!this.isNewDistributorAdded)
            {
                if (this.SelectedDistributor != null && this.IsDistributorEdited(this.SelectedDistributor))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataItemAdd, MessageDialogType.YesNo);
                    if (MessageDialogResult.Yes == messageBoxResult)
                    {
                        this.UpdateDistributor();
                    }
                }

                this.IsSaveButtonEnabled = true;
                this.IsRemoveButtonEnabled = true;
                this.IsNewButtonEnabled = false;
                this.newDistributor = new Distributor();
                this.Distributors.Add(newDistributor);
                this.SelectedDistributor = newDistributor;
                this.isNewDistributorAdded = true;
            }
        }

        /// <summary>
        /// Clears the distributor properties.
        /// </summary>
        private void ClearDistributorProperties()
        {
            this.Name = string.Empty;
            this.Email = string.Empty;
            this.Fax = string.Empty;
            this.Phone = string.Empty;
            this.PostCode = string.Empty;
            this.Street = string.Empty;
            this.City = string.Empty;
            this.WebPage = string.Empty;
        }

        /// <summary>
        /// Removes the Distributor.
        /// </summary>
        private void RemoveDistributor()
        {
            if (!this.isNewDistributorAdded)
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_DeleteDistributorWarning, MessageDialogType.YesNo);
                if (MessageDialogResult.Yes == messageBoxResult)
                {
                    try
                    {
                        Distributor distributorToRemove = SelectedDistributor;

                        if (!this.unitOfWork.Licenses.CheckIfLicenseUsesDistributor(distributorToRemove.ID))
                        {
                            this.unitOfWork.Distributors.Remove(distributorToRemove);
                            this.unitOfWork.Commit();
                            this.ClearDistributorProperties();
                            this.Distributors.Remove(distributorToRemove);
                            this.messanger.Send(new NotificationMessage<Distributor>(this, Notification.DistributorRemoved, distributorToRemove));
                        }
                        else
                        {
                            this.windowService.MessageDialogService.Show(LanguageResource.Distributor_DistributorIsUsedByLicense, MessageDialogType.Info);
                        }
                    }
                    catch (BackendException ex)
                    {
                        log.ErrorException("Error encountered while deleting distributor.", ex);
                        throw new UIException(ErrorCodes.InternalError, ex);
                    }
                }
            }
            else
            {
                this.ClearDistributorProperties();
                this.Distributors.Remove(this.SelectedDistributor);
            }
        }

        /// <summary>
        /// Saves the Distributor.
        /// </summary>
        private void SaveDistributor()
        {
            if (isNewDistributorAdded == true)
            {
                this.CreateDistributor();
            }
            else
            {
                this.UpdateDistributor();
            }

            this.IsNewButtonEnabled = true;
        }

        /// <summary>
        /// Creates a new distributor.
        /// </summary>
        private void CreateDistributor()
        {
            try
            {
                this.ValidateDistributorInput();

                this.newDistributor.Name = this.Name;
                this.newDistributor.Email = this.Email;
                this.newDistributor.Fax = this.Fax;
                this.newDistributor.City = this.City;
                this.newDistributor.Street = this.Street;
                this.newDistributor.PostCode = this.PostCode;
                this.newDistributor.Phone = this.Phone;
                this.newDistributor.WebPage = this.webPage;
                this.isNewDistributorAdded = false;

                try
                {
                    unitOfWork.Distributors.Add(this.newDistributor);
                    unitOfWork.Commit();
                    this.messanger.Send(new NotificationMessage<Distributor>(this, Notification.NewDistributorAdded, newDistributor));
                }
                catch (BackendException ex)
                {
                    log.ErrorException("Error encountered while creating distributor.", ex);
                    throw new UIException(ErrorCodes.InternalError, ex);
                }
                finally
                {
                    this.isNewDistributorAdded = false;
                }
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Updates the distributor.
        /// </summary>
        private void UpdateDistributor()
        {
            try
            {
                ValidateDistributorInput();

                this.SelectedDistributor.Name = this.Name;
                this.SelectedDistributor.Email = this.Email;
                this.SelectedDistributor.Fax = this.Fax;
                this.SelectedDistributor.Phone = this.Phone;
                this.SelectedDistributor.Street = this.Street;
                this.SelectedDistributor.PostCode = this.PostCode;
                this.SelectedDistributor.City = this.City;
                this.SelectedDistributor.WebPage = this.WebPage;
                try
                {
                    unitOfWork.Commit();
                }
                catch (BackendException ex)
                {
                    log.ErrorException("Error while updating distributor.", ex);
                    throw new UIException(ErrorCodes.InternalError, ex);
                }
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Distributorses the data grid selection changed.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void DistributorsDataGridSelectionChanged(SelectionChangedEventArgs arg)
        {
            // enable/disable save button and company information grid          
            if (this.SelectedDistributor == null)
            {
                this.IsRemoveButtonEnabled = false;
                this.IsSaveButtonEnabled = false;
                this.DistributorsInformationGridIsEnabled = false;
            }
            else
            {
                this.IsRemoveButtonEnabled = true;
                this.IsSaveButtonEnabled = true;
                this.DistributorsInformationGridIsEnabled = true;
            }

            // At this point a new company is added.
            if (this.isNewDistributorAdded == true)
            {
                this.IsNewButtonEnabled = true;
                this.Distributors.Remove(this.newDistributor);
                this.isNewDistributorAdded = false;
            }

            if (arg.OriginalSource is DataGrid)
            {
                DataGrid dataGrid = arg.OriginalSource as DataGrid;
                Distributor selectedDistributor = dataGrid.SelectedItem as Distributor;

                if (selectedDistributor != null)
                {
                    dataGrid.ScrollIntoView(selectedDistributor);
                    this.Name = selectedDistributor.Name;
                    this.Email = selectedDistributor.Email;
                    this.Fax = selectedDistributor.Fax;
                    this.PostCode = selectedDistributor.PostCode;
                    this.Phone = selectedDistributor.Phone;
                    this.City = selectedDistributor.City;
                    this.Street = selectedDistributor.Street;
                    this.WebPage = selectedDistributor.WebPage;
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down data grid.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PreviewMouseLeftButtonDownDataGrid(MouseButtonEventArgs arg)
        {
            Distributor targetDistributor = null;

            System.Windows.UIElement source = arg.OriginalSource as System.Windows.UIElement;

            DataGridRow container = source as DataGridRow;
            while (container == null && source != null)
            {
                source = System.Windows.Media.VisualTreeHelper.GetParent(source) as System.Windows.UIElement;
                container = source as DataGridRow;
            }

            if (container != null && container.Item != null)
            {
                targetDistributor = container.Item as Distributor;

                if (this.SelectedDistributor != targetDistributor)
                {
                    if (this.IsDistributorEdited(this.SelectedDistributor))
                    {
                        MessageDialogResult messageBoxResult =
                            this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                        if (MessageDialogResult.Yes == messageBoxResult)
                        {
                            this.SelectedDistributor = targetDistributor;
                        }
                        else
                        {
                            arg.Handled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Windows the closing.
        /// </summary>
        /// <param name="arg">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void WindowClosing(CancelEventArgs arg)
        {
            if (IsDistributorEdited(this.SelectedDistributor))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedData, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Indicates if a distributor was edited.
        /// </summary>
        /// <param name="distributor">The distributor.</param>
        /// <returns>
        /// True if distributor was edited, false otherwise.
        /// </returns>
        private bool IsDistributorEdited(Distributor distributor)
        {
            return this.isNewDistributorAdded == true ||
                ((this.Name != null) && distributor != null && distributor.Name != null && !distributor.Name.Trim().Equals(this.Name.Trim())) ||
                ((this.Phone != null) && distributor != null && distributor.Phone != null && !distributor.Phone.Trim().Equals(this.Phone.Trim())) ||
                ((this.Street != null) && distributor != null && distributor.Street != null && !distributor.Street.Trim().Equals(this.Street.Trim())) ||
                ((this.City != null) && distributor != null && distributor.City != null && !distributor.City.Trim().Equals(this.City.Trim())) ||
                ((this.Email != null) && distributor != null && distributor.Email != null && !distributor.Email.Trim().Equals(this.Email.Trim())) ||
                ((this.WebPage != null) && distributor != null && distributor.WebPage != null && !distributor.WebPage.Trim().Equals(this.WebPage.Trim())) ||
                ((this.Fax != null) && distributor != null && distributor.Fax != null && !distributor.Fax.Trim().Equals(this.Fax.Trim())) ||
                ((this.PostCode != null) && distributor != null && distributor.PostCode != null && !distributor.PostCode.Trim().Equals(this.PostCode.Trim()));
        }

        #endregion
    }
}