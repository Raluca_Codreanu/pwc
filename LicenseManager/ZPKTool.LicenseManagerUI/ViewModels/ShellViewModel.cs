﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ShellViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The view displayed in the shell.
        /// </summary>
        private object content;

        /// <summary>
        /// Indicates whether this instance is loading.
        /// </summary>
        private bool isLoading;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public ShellViewModel(CompositionContainer container, IWindowService windowService, IFileDialogService fileDialogService)
        {
            this.container = container;
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;

            // Init commands            
            this.ClosingCommand = new DelegateCommand<CancelEventArgs>(ClosingAction);
            this.ManageUsersCommand = new DelegateCommand(ManageUsersAction);
            this.ManageDistributorsCommand = new DelegateCommand(ManageDistributorsAction);
            this.ManageCompaniesCommand = new DelegateCommand(ManagerCompaniesAction);
            this.ManageVersionsCommand = new DelegateCommand(ManageVersionsAction);
            this.BackupDatabaseCommand = new DelegateCommand(BackupDatabaseAction);
            this.RestoreDatabaseCommand = new DelegateCommand(RestoreDatabaseAction);

            InitializeMainMenuContent();

            this.IsLoading = true;
            this.DisplayInitialView();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command executed befor the shell view is closed.
        /// </summary>
        public ICommand ClosingCommand { get; private set; }

        /// <summary>
        /// Gets the distributors command.
        /// </summary>
        /// <value>The distributors command.</value>
        public ICommand ManageDistributorsCommand { get; private set; }

        /// <summary>
        /// Gets the users command.
        /// </summary>
        /// <value>The users command.</value>
        public ICommand ManageUsersCommand { get; private set; }

        /// <summary>
        /// Gets the manage companies command.
        /// </summary>
        /// <value>The manage companies command.</value>
        public ICommand ManageCompaniesCommand { get; private set; }

        /// <summary>
        /// Gets the manage versions command.
        /// </summary>
        /// <value>The manage versions command.</value>
        public ICommand ManageVersionsCommand { get; private set; }

        /// <summary>
        /// Gets the backup database command.
        /// </summary>
        public ICommand BackupDatabaseCommand { get; private set; }

        /// <summary>
        /// Gets the restore database command.
        /// </summary>
        public ICommand RestoreDatabaseCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the items to be displayed in the main menu along with the default ones.
        /// </summary>
        public ObservableCollection<MenuItem> MainMenuItems { get; private set; }

        /// <summary>
        /// Gets or sets the content view.
        /// </summary>        
        public object Content
        {
            get
            {
                return this.content;
            }

            set
            {
                LoadContent(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading.
        /// </summary>
        public bool IsLoading
        {
            get { return this.isLoading; }
            set { this.SetProperty(ref this.isLoading, value, () => this.IsLoading); }
        }

        #endregion Properties

        #region Main menu

        /// <summary>
        /// Initializes the content of the main menu.
        /// </summary>
        private void InitializeMainMenuContent()
        {
            this.MainMenuItems = new ObservableCollection<MenuItem>();
            Thickness margin = new Thickness(5, 0, 5, 0);

            MenuItem systemItem = new MenuItem();
            systemItem.Name = "SystemMenuItem";
            systemItem.Header = LanguageResource.General_System;
            systemItem.Margin = margin;
            this.MainMenuItems.Add(systemItem);

            MenuItem exitItem = new MenuItem();
            exitItem.Name = "ExitMenuItem";
            exitItem.Header = LanguageResource.General_Exit;
            exitItem.Margin = margin;
            exitItem.Command = this.CloseCommand;
            systemItem.Items.Add(exitItem);

            MenuItem manageItem = new MenuItem();
            manageItem.Name = "ManageMenuItem";
            manageItem.Header = LanguageResource.MenuItem_Manage;
            manageItem.Margin = margin;
            this.MainMenuItems.Add(manageItem);

            MenuItem distributorsItem = new MenuItem();
            distributorsItem.Name = "DistributorsItem";
            distributorsItem.Header = LanguageResource.MenuItem_Distributors;
            distributorsItem.Margin = margin;
            distributorsItem.Command = ManageDistributorsCommand;
            manageItem.Items.Add(distributorsItem);

            MenuItem companiesItem = new MenuItem();
            companiesItem.Name = "CompaniesItem";
            companiesItem.Header = LanguageResource.MenuItem_Companies;
            companiesItem.Margin = margin;
            companiesItem.Command = ManageCompaniesCommand;
            manageItem.Items.Add(companiesItem);

            MenuItem usersItem = new MenuItem();
            usersItem.Name = "UsersItem";
            usersItem.Header = LanguageResource.MenuItem_Users;
            usersItem.Margin = margin;
            usersItem.Command = ManageUsersCommand;
            manageItem.Items.Add(usersItem);

            MenuItem versionsItem = new MenuItem();
            versionsItem.Name = "VersionsItem";
            versionsItem.Header = LanguageResource.MenuItem_Versions;
            versionsItem.Margin = margin;
            versionsItem.Command = ManageVersionsCommand;
            manageItem.Items.Add(versionsItem);

            MenuItem databaseItem = new MenuItem();
            databaseItem.Name = "DatabaseItem";
            databaseItem.Header = LanguageResource.MenuItem_Database;
            databaseItem.Margin = margin;
            this.MainMenuItems.Add(databaseItem);

            MenuItem backupItem = new MenuItem();
            backupItem.Name = "BackupDatabaseItem";
            backupItem.Header = LanguageResource.MenuItem_Backup;
            backupItem.Margin = margin;
            backupItem.Command = BackupDatabaseCommand;
            databaseItem.Items.Add(backupItem);

            MenuItem restoreItem = new MenuItem();
            restoreItem.Name = "RestoreItem";
            restoreItem.Header = LanguageResource.MenuItem_Restore;
            restoreItem.Margin = margin;
            restoreItem.Command = RestoreDatabaseCommand;
            databaseItem.Items.Add(restoreItem);
        }

        /// <summary>
        /// Adds a menu item into menu on the position specified by the Tag property of the item.
        /// If the Tag property is not set or it specifies an invalid index the item is added on the last position.
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <param name="menuItemsCollection">The menu items collection.</param>
        /// <param name="itemIsTopLevel">if set to true the item is added in the manu, else is added in a submenu.</param>
        private void AddItemToMainMenu(MenuItem item, Collection<MenuItem> menuItemsCollection, bool itemIsTopLevel)
        {
            if (item == null || menuItemsCollection == null)
            {
                return;
            }

            if (itemIsTopLevel)
            {
                item.Margin = new Thickness(5, 0, 5, 0);
            }

            int insertIndex = item.Tag is int ? (int)item.Tag : menuItemsCollection.Count;
            if (insertIndex < 0 || insertIndex > menuItemsCollection.Count)
            {
                insertIndex = menuItemsCollection.Count;
            }

            menuItemsCollection.Insert(insertIndex, item);
        }

        #endregion Main menu

        /// <summary>
        /// Decide which is the initial screen and display it.
        /// </summary>
        private void DisplayInitialView()
        {
            // Initialize database (create, update)
            try
            {
                Configuration.Instance.InitializeDataBase();
                this.Content = this.container.GetExportedValue<LicensesViewModel>();
            }
            catch (BackendException)
            {
                this.windowService.MessageDialogService.Show(LanguageResource.Error_CreateUpdateDataBase, MessageDialogType.Error);
            }
            finally
            {
                this.IsLoading = false;
            }
        }

        #region View Load / Unload / Loaded

        /// <summary>
        /// Loads the specified content into the shell view.
        /// </summary>
        /// <param name="newContent">The content to load.</param>
        /// <returns>True if the content was loaded, false if its loading was canceled.</returns>
        private bool LoadContent(object newContent)
        {
            if (this.content == newContent)
            {
                return true;
            }

            object currentContent = this.content;
            ILifeCycleManager currentViewModel = currentContent as ILifeCycleManager;

            // Execute the OnBeforeUnload callback of the view being unloaded and return if the unloading was canceled.            
            if (currentViewModel != null)
            {
                bool canUnload = currentViewModel.OnUnloading();
                if (!canUnload)
                {
                    return false;
                }
            }

            // Load the new view
            this.content = newContent;
            this.OnPropertyChanged(() => this.Content);

            ContentUnloaded(currentContent);

            ContentLoaded(newContent);

            return true;
        }

        /// <summary>
        /// Performs actions after the content has been unloaded.
        /// </summary>
        /// <param name="content">The unloaded content.</param>
        private void ContentUnloaded(object content)
        {
            if (content == null)
            {
                return;
            }

            // Unload the view's main menu items
            IMainMenuContentProvider provider = content as IMainMenuContentProvider;
            if (provider != null)
            {
                foreach (MenuItem item in provider.GetMainMenuItems())
                {
                    this.MainMenuItems.Remove(item);
                }
            }

            // Execute the OnUnloaded callback of the unloaded view
            ILifeCycleManager lfcMngr = content as ILifeCycleManager;
            if (lfcMngr != null)
            {
                lfcMngr.OnUnloaded();
            }
        }

        /// <summary>
        /// Performs actions after new content has been loaded.
        /// </summary>
        /// <param name="content">The loaded view.</param>
        private void ContentLoaded(object content)
        {
            // Load the new view's menu items and unload the items of the previous view
            // TODO: implement way to define the view's menu items in xaml
            if (content == null)
            {
                return;
            }

            // Load the view's main menu items
            IMainMenuContentProvider provider = content as IMainMenuContentProvider;
            if (provider != null)
            {
                foreach (MenuItem item in provider.GetMainMenuItems())
                {
                    AddItemToMainMenu(item, this.MainMenuItems, true);
                }
            }

            // Execute the view's OnLoaded callback 
            ILifeCycleManager lfcMngr = content as ILifeCycleManager;
            if (lfcMngr != null)
            {
                lfcMngr.OnLoaded();
            }
        }
        #endregion

        #region Command actions

        protected override void Close()
        {
            base.Close();
            Application.Current.Shutdown();
        }

        /// <summary>
        /// The action performed by the Closing command.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void ClosingAction(CancelEventArgs e)
        {
            // Load a null view so the current view is unloaded. If its unloading was canceled, cancel the shell closing.
            bool canClose = LoadContent(null);
            if (!canClose)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// The action performed by the Manage distributors command.
        /// </summary>
        private void ManageDistributorsAction()
        {
            var viewModel = this.container.GetExportedValue<DistributorsViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// The action performed by the Manage users command.
        /// </summary>
        private void ManageUsersAction()
        {
            var viewModel = this.container.GetExportedValue<UsersViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// The action performed by the Manage companies command.
        /// </summary>
        private void ManagerCompaniesAction()
        {
            var viewModel = this.container.GetExportedValue<CompanyViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Manages the versions action.
        /// </summary>
        private void ManageVersionsAction()
        {
            var viewModel = this.container.GetExportedValue<VersionsViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Backups the database.
        /// </summary>
        private void BackupDatabaseAction()
        {
            string destinationPath = SelectPath();

            if (destinationPath != null)
            {
                while (destinationPath.Equals(Path.Combine(Configuration.ApplicationDataFolderPath, Configuration.DatabaseFileName)))
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_DatabaseBackupDestinationSameAsSource, MessageDialogType.Info);
                    destinationPath = SelectPath();
                }

                try
                {
                    // back-up database
                    File.Copy(Path.Combine(Configuration.ApplicationDataFolderPath, Configuration.DatabaseFileName), destinationPath, true);
                    this.windowService.MessageDialogService.Show(LanguageResource.Database_SuccesfulyBackuped, MessageDialogType.Info);
                }
                catch (IOException ex)
                {
                    log.ErrorException("Error occurred when saving the database backup file", ex);
                    this.windowService.MessageDialogService.Show(LanguageResource.Database_ErrorIO, MessageDialogType.Error);
                }
                catch (UnauthorizedAccessException ex)
                {
                    log.ErrorException("Error occurred when saving the database backup file", ex);
                    this.windowService.MessageDialogService.Show(LanguageResource.Database_ErrorUnauthorizedAccess, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Restores the database.
        /// </summary>
        private void RestoreDatabaseAction()
        {
            string destinationPath = OpenFileDialogPath();

            if (destinationPath != null)
            {
                try
                {
                    // back-up database
                    File.Copy(destinationPath, Path.Combine(Configuration.ApplicationDataFolderPath, Configuration.DatabaseFileName), true);

                    // initialize the restored database, it applies updates and checks for version compatibility.
                    Configuration.Instance.InitializeDataBase();

                    var viewModel = this.container.GetExportedValue<LicensesViewModel>();
                    this.container.GetExportedValue<ShellViewModel>().Content = viewModel;

                    this.windowService.MessageDialogService.Show(LanguageResource.Database_SuccesfulyRestored, MessageDialogType.Info);
                }
                catch (BackendException)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_CreateUpdateDataBase, MessageDialogType.Error);
                }
                catch (IOException ex)
                {
                    log.ErrorException("Error occurred when saving the database backup file", ex);
                    this.windowService.MessageDialogService.Show(LanguageResource.Database_ErrorIO, MessageDialogType.Error);
                }
                catch (UnauthorizedAccessException ex)
                {
                    log.ErrorException("Error occurred when saving the database backup file", ex);
                    this.windowService.MessageDialogService.Show(LanguageResource.Database_ErrorUnauthorizedAccess, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Selects a path.
        /// </summary>
        /// <returns>The path selected.</returns>
        private string OpenFileDialogPath()
        {
            // Configure open file dialog box   
            fileDialogService.Filter = LanguageResource.LicenseFilesFileDialogFilter + " (*.db)|*.db";
            fileDialogService.Multiselect = false;
            fileDialogService.FileName = Configuration.DatabaseFileName;

            // Show open file dialog box
            if (fileDialogService.ShowOpenFileDialog() == true)
            {
                try
                {
                    FileInfo fi = new FileInfo(fileDialogService.FileName);
                    if (!fi.Directory.Exists)
                    {
                        this.windowService.MessageDialogService.Show(LanguageResource.Error_FileNotExists, MessageDialogType.Error);
                        log.Error("File doesn't exists");
                    }
                }
                catch (SecurityException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (UnauthorizedAccessException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (NotSupportedException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_InvalidFileName, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_InvalidFileName, ex);
                }
                catch (PathTooLongException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePathTooLong, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePathTooLong, ex);
                }

                return fileDialogService.FileName;
            }

            return null;
        }

        /// <summary>
        /// Selects a path.
        /// </summary>
        /// <returns>The path selected.</returns>
        private string SelectPath()
        {
            // Configure open file dialog box   
            fileDialogService.Filter = LanguageResource.LicenseFilesFileDialogFilter + " (*.db)|*.db";
            fileDialogService.Multiselect = false;
            fileDialogService.FileName = Configuration.DatabaseBackupFileName;

            // Show open file dialog box
            if (fileDialogService.ShowSaveFileDialog() == true)
            {
                try
                {
                    FileInfo fi = new FileInfo(fileDialogService.FileName);
                    if (!fi.Directory.Exists)
                    {
                        this.windowService.MessageDialogService.Show(LanguageResource.Error_FileNotExists, MessageDialogType.Error);
                        log.Error("File doesn't exists");
                    }
                }
                catch (SecurityException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (UnauthorizedAccessException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (NotSupportedException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_InvalidFileName, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_InvalidFileName, ex);
                }
                catch (PathTooLongException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePathTooLong, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePathTooLong, ex);
                }

                return fileDialogService.FileName;
            }

            return null;
        }

        #endregion
    }
}