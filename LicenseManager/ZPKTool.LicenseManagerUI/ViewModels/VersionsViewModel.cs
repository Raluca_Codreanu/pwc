﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IVersionsView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class VersionsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The message service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The selected version.
        /// </summary>
        private ApplicationVersionItem selectedVersion;

        /// <summary>
        /// The manager for accessing the database.
        /// </summary>
        private UnitOfWork dataManager;

        /// <summary>
        /// The application versions as loaded from the database.
        /// </summary>
        private List<ApplicationVersion> versionsFromDatabase;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionsViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public VersionsViewModel(IWindowService windowService, IMessenger messenger)
        {
            this.windowService = windowService;
            this.messanger = messenger;

            // Delegate commands
            this.AddVersionCommand = new DelegateCommand(this.AddNewVersion);
            this.DeleteVersionCommand = new DelegateCommand(this.DeleteSelectedVersion, this.CanDeleteSelectedVersion);
            this.WindowClosingCommand = new DelegateCommand<CancelEventArgs>(this.WindowClosing);

            this.dataManager = UnitOfWorkFactory.CreateUnitOfWork();

            this.versionsFromDatabase = this.dataManager.ApplicationVersions.GetAll().ToList();
            var versions = this.versionsFromDatabase.Select(v => new ApplicationVersionItem(v));
            this.Versions = new ObservableCollection<ApplicationVersionItem>(versions);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the add command.
        /// </summary>
        /// <value>The add command.</value>
        public ICommand AddVersionCommand { get; private set; }

        /// <summary>
        /// Gets the remove command.
        /// </summary>
        /// <value>The remove command.</value>
        public ICommand DeleteVersionCommand { get; private set; }

        /// <summary>
        /// Gets the window closing command.
        /// </summary>
        /// <value>The window closing.</value>
        public ICommand WindowClosingCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the versions.
        /// </summary>
        /// <value>The versions.</value>
        public ObservableCollection<ApplicationVersionItem> Versions { get; private set; }

        /// <summary>
        /// Gets or sets the selected version.
        /// </summary>        
        public ApplicationVersionItem SelectedVersion
        {
            get { return this.selectedVersion; }
            set { this.SetProperty(ref this.selectedVersion, value, () => this.SelectedVersion); }
        }

        #endregion Properties

        #region Command Actions

        /// <summary>
        /// Adds a new version.
        /// </summary>
        private void AddNewVersion()
        {
            var newVersion = new ApplicationVersion();
            var newVersionItem = new ApplicationVersionItem(newVersion);
            this.Versions.Add(newVersionItem);
            this.SelectedVersion = newVersionItem;
        }

        /// <summary>
        /// Determines whether the selected version can be deleted.
        /// </summary>
        /// <returns>
        /// true if the deleted version can be deleted; otherwise, false.
        /// </returns>
        private bool CanDeleteSelectedVersion()
        {
            return this.SelectedVersion != null;
        }

        /// <summary>
        /// Deletes the selected version.
        /// </summary>
        private void DeleteSelectedVersion()
        {
            if (this.SelectedVersion == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(this.SelectedVersion.Version)
                && this.dataManager.ApplicationVersions.IsAssignedToLicense(this.SelectedVersion.Version))
            {
                this.windowService.MessageDialogService.Show(LanguageResource.ValidationError_ApplicationVersionUsedByLicense, MessageDialogType.Error);
            }
            else
            {
                this.Versions.Remove(this.SelectedVersion);
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return !this.Versions.Any(v => !string.IsNullOrEmpty(v.Error));
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            // Check for duplicate versions
            var duplicateVersions =
                this.Versions.Where(v => string.IsNullOrEmpty(v.Error)).GroupBy(v => v.Version).Where(g => g.Count() > 1).Select(g => g.Key);
            if (duplicateVersions.Count() > 0)
            {
                string message = string.Format(LanguageResource.ValidationError_DuplicateAppVersions, string.Join(", ", duplicateVersions));
                this.windowService.MessageDialogService.Show(message, MessageDialogType.Error);
                return;
            }

            // Save changes from version item into version entity
            foreach (var versionItem in this.Versions)
            {
                versionItem.SaveChanges();
            }

            var deletedVersions = this.versionsFromDatabase.Where(dbVersion => !this.Versions.Any(v => v.ApplicationVersion == dbVersion));
            foreach (var version in deletedVersions.ToList())
            {
                this.dataManager.ApplicationVersions.Remove(version);
                this.versionsFromDatabase.Remove(version);
            }

            var addedVersionItems = this.Versions.Where(v => !this.versionsFromDatabase.Contains(v.ApplicationVersion));
            foreach (var versionItem in addedVersionItems.ToList())
            {
                this.dataManager.ApplicationVersions.Add(versionItem.ApplicationVersion);
                this.versionsFromDatabase.Add(versionItem.ApplicationVersion);
            }

            this.dataManager.Commit();

            // Send message to notify other components that the versions have changed.
            this.messanger.Send(new NotificationMessage(Notification.ApplicationVersionsChanged));

            this.Close();
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            base.Cancel();
            this.Close();
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Windows the closing.
        /// </summary>
        /// <param name="arg">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void WindowClosing(CancelEventArgs arg)
        {
            if (this.CheckIfVersionsHaveChanged())
            {
                var messageBoxResult = this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedData, MessageDialogType.YesNo);
                if (messageBoxResult == MessageDialogResult.No)
                {
                    arg.Cancel = true;
                }
            }
        }

        #endregion

        /// <summary>
        /// Checks if the versions in the view model have changed (added, deleted or edited).
        /// </summary>
        /// <returns>True if at least one version change was detected; otherwise, false.</returns>
        private bool CheckIfVersionsHaveChanged()
        {
            if (this.Versions.Any(v => v.IsChanged))
            {
                return true;
            }

            var deletedVersions = this.versionsFromDatabase.Where(dbVersion => !this.Versions.Any(v => v.ApplicationVersion == dbVersion));
            if (deletedVersions.Count() > 0)
            {
                return true;
            }

            var addedVersionItems = this.Versions.Where(v => !this.versionsFromDatabase.Contains(v.ApplicationVersion));
            if (addedVersionItems.Count() > 0)
            {
                return true;
            }

            return false;
        }

        #region Inner classes

        /// <summary>
        /// Represents a data item used to display on the UI an instance of <see cref="ZPKTool.LicenseManagerBackend.ApplicationVersion"/> data class.
        /// </summary>
        public class ApplicationVersionItem : ObservableObject, IEditableObject, IDataErrorInfo
        {
            /// <summary>
            /// The application version number.
            /// </summary>
            private string version;

            /// <summary>
            /// Prevents a default instance of the <see cref="ApplicationVersionItem" /> class from being created.
            /// </summary>
            private ApplicationVersionItem()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ApplicationVersionItem"/> class.
            /// </summary>
            /// <param name="appVersion">The application version entity wrapped by this instance.</param>
            public ApplicationVersionItem(ApplicationVersion appVersion)
            {
                if (appVersion == null)
                {
                    throw new ArgumentNullException("appVersion", "The application version was null.");
                }

                this.ApplicationVersion = appVersion;
                this.version = appVersion.Version;
            }

            /// <summary>
            /// Gets the <see cref="ApplicationVersion"/> entity wrapped by this instance.
            /// </summary>
            public ApplicationVersion ApplicationVersion { get; private set; }

            /// <summary>
            /// Gets a value indicating whether this instance is changed.
            /// </summary>        
            public bool IsChanged
            {
                get { return this.Version != this.ApplicationVersion.Version; }
            }

            /// <summary>
            /// Gets or sets the application version number to be consumed by the UI.
            /// </summary>
            [Required(ErrorMessageResourceName = "ValidationError_EmptyAppVersionFormat", ErrorMessageResourceType = typeof(LanguageResource))]
            [RegularExpression(@"^\d{1,3}\.\d{1,3}(?:\.\d{1,6})?$", ErrorMessageResourceName = "ValidationError_InvalidAppVersionFormat", ErrorMessageResourceType = typeof(LanguageResource))]
            public string Version
            {
                get { return this.version; }
                set { this.SetProperty(ref this.version, value, () => this.Version); }
            }

            /// <summary>
            /// Gets an error message indicating what is wrong with this object.
            /// </summary>
            /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
            public string Error
            {
                get
                {
                    List<ValidationResult> validationResults = new List<ValidationResult>();

                    Validator.TryValidateObject(
                        this,
                        new ValidationContext(this, null, null),
                        validationResults,
                        true);

                    return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
                }
            }

            /// <summary>
            /// Gets the error message for the property with the given name.
            /// </summary>
            /// <param name="columnName">Name of the column.</param>
            /// <returns>The error message for the property or empty string if there is no error.</returns>
            public string this[string columnName]
            {
                get
                {
                    List<ValidationResult> validationResults = new List<ValidationResult>();

                    Validator.TryValidateProperty(
                        this.GetType().GetProperty(columnName).GetValue(this, null),
                        new ValidationContext(this, null, null) { MemberName = columnName },
                        validationResults);

                    return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
                }
            }

            /// <summary>
            /// Begins an edit on an object.
            /// </summary>
            public void BeginEdit()
            {
            }

            /// <summary>
            /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit" /> call.
            /// </summary>
            public void CancelEdit()
            {
            }

            /// <summary>
            /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit" /> or <see cref="M:System.ComponentModel.IBindingList.AddNew" /> call into the underlying object.
            /// </summary>
            public void EndEdit()
            {
            }

            /// <summary>
            /// Saves the changes from this instance into the underlying <see cref="ApplicationVersion"/> entity instance.
            /// </summary>
            public void SaveChanges()
            {
                if (!string.IsNullOrEmpty(this.Error))
                {
                    throw new InvalidOperationException("Can not save changes if the item has validation errors.");
                }

                this.ApplicationVersion.Version = this.Version;
            }
        }

        #endregion Inner classes
    }    
}