﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.LicenseGenerator;
using ZPKTool.LicenseManagerBackend;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.LicenseManagerUI.Notifications;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.LicenseManagerUI.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class LicensesViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The RGB IV used for encryption.
        /// </summary>
        private const string EncryptionRgbIV = "ruojvlzmdalyglrj";

        /// <summary>
        /// The Key used for encryption.
        /// </summary>
        private const string EncryptionKey = "hcxilkqbbhczfeultgbskdmaunivmfuo";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The mediator.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private UnitOfWork unitOfWork;

        /// <summary>
        /// Represents a dynamic data collection with companies that provides notifications when items get added, removed, or when the whole list is refreshed
        /// </summary>
        private ObservableCollection<Company> companies;

        /// <summary>
        /// Represents a dynamic data collection with users that provides notifications when items get added, removed, or when the whole list is refreshed
        /// </summary>
        private ObservableCollection<User> users;

        /// <summary>
        /// Represents a dynamic data collection with licenses that provides notifications when items get added, removed, or when the whole list is refreshed
        /// </summary>
        private ObservableCollection<License> licenses;

        /// <summary>
        /// Represents a dynamic data collection with distributors that provides notifications when items get added, removed, or when the whole list is refreshed
        /// </summary>
        private ObservableCollection<Distributor> distributors;

        /// <summary>
        /// The selected company from the combobox.
        /// </summary>
        private Company selectedCompany;

        /// <summary>
        /// The selected user from the combobox.
        /// </summary>
        private User selectedUser;

        /// <summary>
        ///  /// Represents a dynamic data collection with license types that provides 
        /// notifications when items get added, removed, or when the whole list is refreshed        /// 
        /// </summary>
        private ObservableCollection<Pair<string, LicenseType>> licenseTypes;

        /// <summary>
        /// Represents a dynamic data collection with license levels that provides 
        /// notifications when items get added, removed, or when the whole list is refreshed        /// 
        /// </summary>
        private ObservableCollection<Pair<string, LicenseLevel>> licenseLevels;

        /// <summary>
        /// The selected license type.
        /// </summary>
        private Pair<string, LicenseType> selectedLicenseType;

        /// <summary>
        /// The selected license level.
        /// </summary>
        private Pair<string, LicenseLevel> selectedLicenseLevel;

        /// <summary>
        /// The selected distributor.
        /// </summary>
        private Distributor selectedDistributor;

        /// <summary>
        /// The license serial number.
        /// </summary>
        private string serialNumber;

        /// <summary>
        /// The license starting date.
        /// </summary>
        private DateTime startingDate;

        /// <summary>
        /// The license expiration date.
        /// </summary>
        private DateTime expirationDate;

        /// <summary>
        /// The license file name.
        /// </summary>
        private string fileName;

        /// <summary>
        /// The license output directory.
        /// </summary>
        private string outputDirectory;

        /// <summary>
        /// Indicates whether the a new license is added.
        /// </summary>
        private bool isNewLicenseAdded = false;

        /// <summary>
        /// Indicates whether the generate button is enabled.
        /// </summary>
        private System.Windows.Visibility createButtonVisibility;

        /// <summary>
        /// The selected license.
        /// </summary>
        private License selectedLicense;

        /// <summary>
        /// The new license.
        /// </summary>
        private License newLicense;

        /// <summary>
        /// Indicates whether the license information grid is enabled.
        /// </summary>
        private bool licenseInformationGridIsEnabled;

        /// <summary>
        /// Indicates whether the add user image button is enabled.
        /// </summary>
        private bool isEnabledAddUserImageButton;

        /// <summary>
        /// Indicates whether the save button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private System.Windows.Visibility saveButtonVisibility;

        /// <summary>
        ///  Indicates whether the remove button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isRemoveButtonEnabled;

        /// <summary>
        /// Indicates whether the new button is enabled or not, true if it is enabled, false otherwise.
        /// </summary>
        private bool isNewButtonEnabled;

        /// <summary>
        /// Indicates if the license should be saved to file.
        /// </summary>
        private bool saveLicenseToFile;

        /// <summary>
        /// Handles company selection.
        /// </summary>
        private bool handleCompaySelection = true;

        /// <summary>
        /// handles User selection.
        /// </summary>
        private bool handleUserSelection = true;

        /// <summary>
        /// The selected version.
        /// </summary>
        private string selectedVersion;

        /// <summary>
        /// The application versions collection.
        /// </summary>
        private ObservableCollection<string> applicationVersions;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicensesViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public LicensesViewModel(CompositionContainer container, IWindowService windowService, IMessenger messenger, IFileDialogService fileDialogService)
        {
            this.container = container;
            this.windowService = windowService;
            this.messenger = messenger;
            this.fileDialogService = fileDialogService;

            this.NewLicenseCommand = new DelegateCommand(AddNewLicense);
            this.RemoveLicenseCommand = new DelegateCommand(RemoveLicense);
            this.OutputDirectoryCommand = new DelegateCommand(OutputDirectoryAction);
            this.GenerateLicenseCommand = new DelegateCommand(GenerateLicense);
            this.CompanySelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(CompanySelectionChanged);
            this.UserSelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(UserSelectionChanged);
            this.LicensesDataGridSelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(DataGridSelectionChanged);
            this.AddNewCompanyCommand = new DelegateCommand(AddNewCompany);
            this.AddNewUserCommand = new DelegateCommand(AddNewUser);
            this.AddNewDistributorCommand = new DelegateCommand(AddNewDistributor);
            this.PreviewMouseLeftButtonDownDataGridCommand = new DelegateCommand<MouseButtonEventArgs>(PreviewMouseLeftButtonDownDataGrid);
            this.PreviewMouseLeftButtonDownAddCompanyCommand = new DelegateCommand<MouseButtonEventArgs>(PreviewMouseLeftButtonDownAddCompany);
            this.SaveLicenseCommand = new DelegateCommand(SaveLicense);

            this.unitOfWork = UnitOfWorkFactory.GetUnitOfWork();

            this.Companies = new ObservableCollection<Company>(unitOfWork.Companies.GetAll());
            this.LicenseTypes = new ObservableCollection<Pair<string, LicenseType>>();
            this.LicenseTypes.Add(new Pair<string, LicenseType>("Nodelocked", LicenseType.Nodelocked));

            this.Distributors = new ObservableCollection<Distributor>(unitOfWork.Distributors.GetAll());

            this.LicenseLevels = new ObservableCollection<Pair<string, LicenseLevel>>();
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>("Trial", LicenseLevel.Trial));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>("Basic", LicenseLevel.Basic));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>("ProfessionalMech", LicenseLevel.ProfessionalMech));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>("ProfessionalMechIncMDUpdates", LicenseLevel.ProfessionalMechIncMDUpdates));

            this.InitializeApplicationVersions();

            this.StartingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            this.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            this.OutputDirectory = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["OutputDirectory"]);
            this.Licenses = new ObservableCollection<License>();

            this.LicenseInformationGridIsEnabled = false;
            this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
            this.IsEnabledAddUserImageButton = false;
            this.SaveButtonVisibility = System.Windows.Visibility.Collapsed;
            this.IsRemoveButtonEnabled = false;
            this.IsNewButtonEnabled = false;

            this.messenger.Register<NotificationMessage<Company>>(ReceiveNewCompanyAddedMessage);
            this.messenger.Register<NotificationMessage<User>>(ReceiveNewUserAddedMessage);
            this.messenger.Register<NotificationMessage<Distributor>>(ReceiveNewDistributorAddedMessage);
            this.messenger.Register<NotificationMessage<Distributor>>(ReceiveDeleteDistributorMessage);
            this.messenger.Register<NotificationMessage<Company>>(ReceiveDeleteCompanyMessage);
            this.messenger.Register<NotificationMessage<User>>(ReceiveDeleteUserMessage);

            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessages);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the new license command.
        /// </summary>
        /// <value>The new license command.</value>
        public ICommand NewLicenseCommand { get; private set; }

        /// <summary>
        /// Gets the remove license command.
        /// </summary>
        /// <value>The remove license command.</value>
        public ICommand RemoveLicenseCommand { get; private set; }

        /// <summary>
        /// Gets the output directory command.
        /// </summary>
        /// <value>The output directory command.</value>
        public ICommand OutputDirectoryCommand { get; private set; }

        /// <summary>
        /// Gets the generate license command.
        /// </summary>
        /// <value>The generate license command.</value>
        public ICommand GenerateLicenseCommand { get; private set; }

        /// <summary>
        /// Gets the company selection changed command.
        /// </summary>
        /// <value>The company selection changed command.</value>
        public ICommand CompanySelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets  the user selection changed command.
        /// </summary>
        /// <value>The user selection changed command.</value>
        public ICommand UserSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the licenses data grid selection changed command.
        /// </summary>
        /// <value>The licenses data grid selection changed command.</value>
        public ICommand LicensesDataGridSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the add new company command.
        /// </summary>
        /// <value>The add new company command.</value>
        public ICommand AddNewCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the add new user command.
        /// </summary>
        /// <value>The add new user command.</value>
        public ICommand AddNewUserCommand { get; private set; }

        /// <summary>
        /// Gets the add new distributor command.
        /// </summary>
        /// <value>The add new distributor command.</value>
        public ICommand AddNewDistributorCommand { get; private set; }

        /// <summary>
        /// Gets the preview mouse left button down data grid command.
        /// </summary>
        /// <value>The preview mouse left button down data grid command.</value>
        public ICommand PreviewMouseLeftButtonDownDataGridCommand { get; private set; }

        /// <summary>
        /// Gets the preview mouse left button down add company command.
        /// </summary>
        /// <value>The preview mouse left button down add company command.</value>
        public ICommand PreviewMouseLeftButtonDownAddCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the save license command.
        /// </summary>
        /// <value>The save license command.</value>
        public ICommand SaveLicenseCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [save license to file].
        /// </summary>
        /// <value><c>true</c> if [save license to file]; otherwise, <c>false</c>.</value>
        public bool SaveLicenseToFile
        {
            get
            {
                return this.saveLicenseToFile;
            }

            set
            {
                if (this.saveLicenseToFile != value)
                {
                    this.saveLicenseToFile = value;
                    OnPropertyChanged("SaveLicenseToFile");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [save button is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [save button is enabled]; otherwise, <c>false</c>.
        /// </value>
        public System.Windows.Visibility SaveButtonVisibility
        {
            get
            {
                return this.saveButtonVisibility;
            }

            set
            {
                if (this.saveButtonVisibility != value)
                {
                    this.saveButtonVisibility = value;
                    OnPropertyChanged("SaveButtonVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is remove button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is remove button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsRemoveButtonEnabled
        {
            get
            {
                return this.isRemoveButtonEnabled;
            }

            set
            {
                if (this.isRemoveButtonEnabled != value)
                {
                    this.isRemoveButtonEnabled = value;
                    OnPropertyChanged("IsRemoveButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is new button enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is new button enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewButtonEnabled
        {
            get
            {
                return this.isNewButtonEnabled;
            }

            set
            {
                if (this.isNewButtonEnabled != value)
                {
                    this.isNewButtonEnabled = value;
                    OnPropertyChanged("IsNewButtonEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the private key.
        /// </summary>
        /// <value>The private key.</value>
        private string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the companies.
        /// </summary>
        /// <value>The companies.</value>
        public ObservableCollection<Company> Companies
        {
            get
            {
                return this.companies;
            }

            set
            {
                if (this.companies != value)
                {
                    this.companies = value;
                    OnPropertyChanged("Companies");
                }
            }
        }

        /// <summary>
        /// Gets or sets the available application versions.
        /// </summary>
        /// <value>The application version.</value>
        public ObservableCollection<string> ApplicationVersions
        {
            get
            {
                return this.applicationVersions;
            }

            set
            {
                if (this.applicationVersions != value)
                {
                    this.applicationVersions = value;
                    OnPropertyChanged("ApplicationVersions");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected version.
        /// </summary>
        /// <value>The selected version.</value>
        public string SelectedVersion
        {
            get
            {
                return this.selectedVersion;
            }

            set
            {
                if (this.selectedVersion != value)
                {
                    this.selectedVersion = value;
                    if (this.SelectedCompany != null && this.SelectedUser != null)
                    {
                        this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + value;
                    }

                    OnPropertyChanged("SelectedVersion");
                }
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>The users.</value>
        public ObservableCollection<User> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                if (this.users != value)
                {
                    this.users = value;
                    OnPropertyChanged("Users");
                }
            }
        }

        /// <summary>
        /// Gets or sets the licenses.
        /// </summary>
        /// <value>The licenses.</value>
        public ObservableCollection<License> Licenses
        {
            get
            {
                return this.licenses;
            }

            set
            {
                if (this.licenses != value)
                {
                    this.licenses = value;
                    OnPropertyChanged("Licenses");
                }
            }
        }

        /// <summary>
        /// Gets or sets the license types.
        /// </summary>
        /// <value>The license types.</value>
        public ObservableCollection<Pair<string, LicenseType>> LicenseTypes
        {
            get
            {
                return this.licenseTypes;
            }

            set
            {
                if (this.licenseTypes != value)
                {
                    this.licenseTypes = value;
                    OnPropertyChanged("LicenseTypes");
                }
            }
        }

        /// <summary>
        /// Gets or sets the license levels.
        /// </summary>
        /// <value>The license levels.</value>
        public ObservableCollection<Pair<string, LicenseLevel>> LicenseLevels
        {
            get
            {
                return this.licenseLevels;
            }

            set
            {
                if (this.licenseLevels != value)
                {
                    this.licenseLevels = value;
                    OnPropertyChanged("LicenseLevels");
                }
            }
        }

        /// <summary>
        /// Gets or sets the distributors.
        /// </summary>
        /// <value>The distributors.</value>
        public ObservableCollection<Distributor> Distributors
        {
            get
            {
                return this.distributors;
            }

            set
            {
                if (this.distributors != value)
                {
                    this.distributors = value;
                    OnPropertyChanged("Distributors");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected company.
        /// </summary>
        /// <value>The selected company.</value>
        public Company SelectedCompany
        {
            get
            {
                return this.selectedCompany;
            }

            set
            {
                if (this.selectedCompany != value)
                {
                    this.selectedCompany = value;

                    if (this.SelectedCompany != null && this.SelectedUser != null && this.SelectedVersion != null)
                    {
                        this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedVersion;
                    }

                    OnPropertyChanged("SelectedCompany");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected user.
        /// </summary>
        /// <value>The selected user.</value>
        public User SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                if (this.selectedUser != value)
                {
                    this.selectedUser = value;
                    OnPropertyChanged("SelectedUser");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected distributor.
        /// </summary>
        /// <value>The selected distributor.</value>
        public Distributor SelectedDistributor
        {
            get
            {
                return this.selectedDistributor;
            }

            set
            {
                if (this.selectedDistributor != value)
                {
                    this.selectedDistributor = value;
                    OnPropertyChanged("SelectedDistributor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected license.
        /// </summary>
        /// <value>The selected license.</value>
        public License SelectedLicense
        {
            get
            {
                return this.selectedLicense;
            }

            set
            {
                if (this.selectedLicense != value)
                {
                    this.selectedLicense = value;
                    OnPropertyChanged("SelectedLicense");
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the selected license.
        /// </summary>
        /// <value>The type of the selected license.</value>
        public Pair<string, LicenseType> SelectedLicenseType
        {
            get
            {
                return this.selectedLicenseType;
            }

            set
            {
                if (this.selectedLicenseType != value)
                {
                    this.selectedLicenseType = value;
                    OnPropertyChanged("SelectedLicenseType");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected license level.
        /// </summary>
        /// <value>The selected license level.</value>
        public Pair<string, LicenseLevel> SelectedLicenseLevel
        {
            get
            {
                return this.selectedLicenseLevel;
            }

            set
            {
                if (this.selectedLicenseLevel != value)
                {
                    this.selectedLicenseLevel = value;
                    OnPropertyChanged("SelectedLicenseLevel");
                }
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>The serial number.</value>
        public string SerialNumber
        {
            get
            {
                return this.serialNumber;
            }

            set
            {
                if (this.serialNumber != value)
                {
                    this.serialNumber = value;
                    OnPropertyChanged("SerialNumber");
                }
            }
        }

        /// <summary>
        /// Gets or sets the starting date.
        /// </summary>
        /// <value>The starting date.</value>
        public DateTime StartingDate
        {
            get
            {
                return this.startingDate;
            }

            set
            {
                if (value > ExpirationDate)
                {
                    this.ExpirationDate = value;
                }

                if (this.startingDate != value)
                {
                    this.startingDate = value;
                    OnPropertyChanged("StartingDate");
                }
            }
        }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public DateTime ExpirationDate
        {
            get
            {
                return this.expirationDate;
            }

            set
            {
                if (this.expirationDate != value)
                {
                    this.expirationDate = new DateTime(value.Year, value.Month, value.Day, 23, 59, 59);
                    OnPropertyChanged("ExpirationDate");
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get
            {
                return this.fileName;
            }

            set
            {
                if (this.fileName != value)
                {
                    this.fileName = value;
                    OnPropertyChanged("FileName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the output directory.
        /// </summary>
        /// <value>The output directory.</value>
        public string OutputDirectory
        {
            get
            {
                return this.outputDirectory;
            }

            set
            {
                if (this.outputDirectory != value)
                {
                    this.outputDirectory = value;
                    OnPropertyChanged("OutputDirectory");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [generate button is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [generate button is enabled]; otherwise, <c>false</c>.
        /// </value>
        public System.Windows.Visibility CreateButtonVisibility
        {
            get
            {
                return this.createButtonVisibility;
            }

            set
            {
                if (this.createButtonVisibility != value)
                {
                    this.createButtonVisibility = value;
                    OnPropertyChanged("CreateButtonVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [license information grid is enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [license information grid is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool LicenseInformationGridIsEnabled
        {
            get
            {
                return this.licenseInformationGridIsEnabled;
            }

            set
            {
                if (this.licenseInformationGridIsEnabled != value)
                {
                    this.licenseInformationGridIsEnabled = value;
                    OnPropertyChanged("LicenseInformationGridIsEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled add user image button.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled add user image button; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabledAddUserImageButton
        {
            get
            {
                return this.isEnabledAddUserImageButton;
            }

            set
            {
                if (this.isEnabledAddUserImageButton != value)
                {
                    this.isEnabledAddUserImageButton = value;
                    OnPropertyChanged("IsEnabledAddUserImageButton");
                }
            }
        }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the list of application versions.
        /// </summary>
        private void InitializeApplicationVersions()
        {
            var versions = this.unitOfWork.ApplicationVersions.GetAll().Select(v => v.Version).OrderByDescending(v => v);
            this.ApplicationVersions = new ObservableCollection<string>(versions);
            this.SelectedVersion = this.ApplicationVersions.FirstOrDefault();
        }

        #endregion Initialization

        #region Command Actions

        /// <summary>
        /// Adds new license.
        /// </summary>
        private void AddNewLicense()
        {
            if (!this.isNewLicenseAdded && this.SelectedCompany != null && this.SelectedUser != null)
            {
                if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataItemAdd, MessageDialogType.YesNo);
                    if (MessageDialogResult.Yes == messageBoxResult)
                    {
                        this.SaveLicense();
                    }
                }

                this.isNewLicenseAdded = true;
                this.IsNewButtonEnabled = false;
                this.SaveButtonVisibility = System.Windows.Visibility.Collapsed;
                this.IsRemoveButtonEnabled = true;
                this.CreateButtonVisibility = System.Windows.Visibility.Visible;
                this.ClearLicense();
                this.newLicense = new License();
                this.Licenses.Add(this.newLicense);
                this.SelectedLicense = this.newLicense;
                this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedVersion;
                this.StartingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                this.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            }
        }

        /// <summary>
        /// Clears the license informations.
        /// </summary>
        private void ClearLicense()
        {
            this.SerialNumber = string.Empty;
            this.StartingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            this.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            this.FileName = string.Empty;
        }

        /// <summary>
        /// Removes license.
        /// </summary>
        private void RemoveLicense()
        {
            if (!this.isNewLicenseAdded)
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_DeleteLicense, MessageDialogType.YesNo);
                if (MessageDialogResult.Yes == messageBoxResult)
                {
                    try
                    {
                        License licenseToRemove = this.SelectedLicense;
                        this.unitOfWork.Licenses.Remove(licenseToRemove);
                        this.unitOfWork.Commit();
                        this.ClearLicense();
                        this.Licenses.Remove(licenseToRemove);

                        if (!(this.Licenses.Count > 0))
                        {
                            this.SaveButtonVisibility = System.Windows.Visibility.Collapsed;
                        }

                        this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (BackendException ex)
                    {
                        log.ErrorException("Exception while removing license", ex);
                        throw new UIException(ErrorCodes.InternalError, ex);
                    }
                }
            }
            else
            {
                this.ClearLicense();
                this.Licenses.Remove(this.SelectedLicense);
                this.isNewLicenseAdded = false;
                this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Outputs the directory.
        /// </summary>
        private void OutputDirectoryAction()
        {
            using (System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.Description = LanguageResource.License_OutputFileDirectory;
                dialog.ShowNewFolderButton = true;
                dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.OutputDirectory = dialog.SelectedPath;
                    this.UpdateOutputDirectoryConfigFile(dialog.SelectedPath);
                }
            }
        }

        /// <summary>
        /// Validates the user input.
        /// </summary>
        private void ValidateUserInput()
        {
            List<string> errorList = new List<string>();

            if (string.IsNullOrWhiteSpace(this.SerialNumber)
                || (this.SerialNumber != null && (this.SerialNumber.Trim().Length < 40 || this.SerialNumber.Trim().Length > 44)))
            {
                errorList.Add(LanguageResource.ValidationError_SerialNumberFormat);
            }

            if (this.SelectedVersion == null || (this.ApplicationVersions != null && this.SelectedVersion.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_ApplicationVersionEmpty);
            }

            if (this.SelectedCompany == null)
            {
                errorList.Add(LanguageResource.ValidationError_CompanyNotSelected);
            }

            if (this.SelectedDistributor == null)
            {
                errorList.Add(LanguageResource.ValidationError_DistributorNotSelected);
            }

            if (this.StartingDate > this.ExpirationDate)
            {
                errorList.Add(LanguageResource.ValidationError_EndingDateMustBeLaterThanTheStartDate);
            }

            if (this.FileName == null || (this.FileName != null && this.FileName.Trim().Equals(string.Empty)))
            {
                errorList.Add(LanguageResource.ValidationError_InvalidFileName);
            }

            if (!Directory.Exists(this.OutputDirectory))
            {
                errorList.Add(LanguageResource.ValidationError_DirectoryDoesNotExist);
            }

            if (errorList.Count > 0)
            {
                throw new ValidationException("Validation Errors", errorList);
            }
        }

        /// <summary>
        /// Generates the license.
        /// </summary>
        private void GenerateLicense()
        {
            try
            {
                // validate license user input.
                this.ValidateUserInput();

                // set license related information.
                this.SetLicenseRelatedInformation();

                this.newLicense.FileName = this.FileName;
                string fullPath = System.IO.Path.Combine(this.OutputDirectory, this.FileName);

                string fileExtension = System.IO.Path.GetExtension(fullPath);
                if (string.IsNullOrEmpty(fileExtension)
                    || string.Compare(fileExtension, ".lic", StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    fullPath += ".lic";
                }

                // generate license.
                this.SetPrivateKey();
                var generator = new LicensingGenerator(this.PrivateKey);
                var key = generator.Generate(this.newLicense);

                // save license to db.
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] bytes = encoding.GetBytes(key);
                this.newLicense.FileContent = bytes;
                this.unitOfWork.Licenses.Add(this.newLicense);
                this.unitOfWork.Commit();
                this.isNewLicenseAdded = false;
                this.IsNewButtonEnabled = true;
                this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
                this.SaveButtonVisibility = System.Windows.Visibility.Visible;

                if (this.SaveLicenseToFile)
                {
                    this.WriteDataToFile(fullPath, key);
                }
            }
            catch (LicenseGeneratorException ex)
            {
                log.ErrorException("Error while generating license key content.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error while saving license.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Saves the edited license.
        /// </summary>
        private void SaveLicense()
        {
            try
            {
                // validate license user input.
                this.ValidateUserInput();

                this.SelectedLicense.ApplicationVersion = this.SelectedVersion;
                this.SelectedLicense.Distributor = this.SelectedDistributor;
                this.SelectedLicense.StartingDate = new DateTime(this.StartingDate.Year, this.StartingDate.Month, this.StartingDate.Day, 0, 0, 0);
                this.SelectedLicense.ExpirationDate = new DateTime(this.ExpirationDate.Year, this.ExpirationDate.Month, this.ExpirationDate.Day, 23, 59, 59);
                this.SelectedLicense.Level = (short)this.SelectedLicenseLevel.Second;
                this.SelectedLicense.SerialNumber = this.SerialNumber;
                this.SelectedLicense.Type = (short)this.SelectedLicenseType.Second;
                this.SelectedLicense.User = this.SelectedUser;

                this.SelectedLicense.FileName = this.FileName;

                // generate license.
                this.SetPrivateKey();
                var generator = new LicensingGenerator(this.PrivateKey);
                var key = generator.Generate(this.SelectedLicense);

                // save license to db.
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] bytes = encoding.GetBytes(key);
                this.SelectedLicense.FileContent = bytes;
                this.unitOfWork.Commit();
            }
            catch (LicenseGeneratorException ex)
            {
                log.ErrorException("Error while generating license key xml content.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (BackendException ex)
            {
                log.ErrorException("Error while saving license.", ex);
                throw new UIException(ErrorCodes.InternalError, ex);
            }
            catch (ValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (string error in ex.errorList)
                {
                    errorMessage += error + System.Environment.NewLine;
                }

                this.windowService.MessageDialogService.Show(LanguageResource.ValidationException_GeneralMessage + System.Environment.NewLine + System.Environment.NewLine + errorMessage, MessageDialogType.Warning);
            }
        }

        /// <summary>
        /// Sets the license related information.
        /// </summary>
        private void SetLicenseRelatedInformation()
        {
            this.newLicense.ApplicationVersion = this.SelectedVersion;
            this.newLicense.Distributor = this.SelectedDistributor;
            this.newLicense.StartingDate = new DateTime(this.StartingDate.Year, this.StartingDate.Month, this.StartingDate.Day, 0, 0, 0);
            this.newLicense.ExpirationDate = new DateTime(this.ExpirationDate.Year, this.ExpirationDate.Month, this.ExpirationDate.Day, 23, 59, 59);
            this.newLicense.Level = (short)this.SelectedLicenseLevel.Second;
            this.newLicense.SerialNumber = this.SerialNumber;
            this.newLicense.Type = (short)this.SelectedLicenseType.Second;
            this.newLicense.User = this.SelectedUser;
        }

        /// <summary>
        /// Sets the private key.
        /// </summary>
        private void SetPrivateKey()
        {
            string resourceEmbedded = this.GetType().Namespace + "." + "privateKey.xml";

            System.Reflection.Assembly asm = Assembly.GetExecutingAssembly();
            using (System.IO.Stream xmlStream = asm.GetManifestResourceStream(resourceEmbedded))
            {
                StreamReader reader = new StreamReader(xmlStream);
                this.PrivateKey = reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Action occurs when the company selection change event is triggered.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CompanySelectionChanged(SelectionChangedEventArgs arg)
        {
            if (this.handleCompaySelection)
            {
                if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                    if (MessageDialogResult.No == messageBoxResult)
                    {
                        arg.Handled = true;
                    }
                }

                if (arg.Handled == false)
                {
                    this.SelectedLicense = null;
                    this.IsEnabledAddUserImageButton = true;
                    this.ClearLicense();

                    if (this.SelectedCompany != null)
                    {
                        this.FileName = this.SelectedCompany.Name;
                        if (this.SelectedUser != null)
                        {
                            this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name;

                            if (this.SelectedVersion != null)
                            {
                                this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedVersion;
                            }
                        }

                        this.Licenses.Clear();
                        this.Users = new ObservableCollection<User>(unitOfWork.Users.GetByCompany(this.SelectedCompany.ID));
                    }
                }
                else
                {
                    this.handleCompaySelection = false;
                    this.SelectedCompany = arg.RemovedItems[0] as Company;
                    return;
                }
            }

            this.handleCompaySelection = true;
        }

        /// <summary>
        /// Action occurs when the user selection change event is triggered.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void UserSelectionChanged(SelectionChangedEventArgs arg)
        {
            if (this.handleUserSelection)
            {
                if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
                {
                    MessageDialogResult messageBoxResult =
                        this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                    if (MessageDialogResult.No == messageBoxResult)
                    {
                        arg.Handled = true;
                    }
                }

                if (arg.Handled == false)
                {
                    this.ClearLicense();
                    this.IsNewButtonEnabled = true;

                    if (this.SelectedUser != null)
                    {
                        this.IsNewButtonEnabled = true;
                        this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name;
                        if (this.SelectedVersion != null)
                        {
                            this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedVersion;
                        }

                        this.Licenses = new ObservableCollection<License>(unitOfWork.Licenses.GetByUser(this.SelectedUser.ID));
                    }
                    else
                    {
                        this.IsNewButtonEnabled = false;
                    }
                }
                else
                {
                    this.handleUserSelection = false;
                    this.SelectedUser = arg.RemovedItems[0] as User;
                    return;
                }
            }

            this.handleUserSelection = true;
        }

        /// <summary>
        /// Datas the grid selection changed.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void DataGridSelectionChanged(SelectionChangedEventArgs arg)
        {
            // enable/disable save button and company information grid
            if (this.SelectedLicense == null)
            {
                this.IsRemoveButtonEnabled = false;
                this.SaveButtonVisibility = System.Windows.Visibility.Collapsed;
                this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
                this.LicenseInformationGridIsEnabled = false;
                this.IsNewButtonEnabled = true;
            }
            else if (this.SelectedLicense != null && this.isNewLicenseAdded == false)
            {
                this.LicenseInformationGridIsEnabled = true;
                this.IsRemoveButtonEnabled = true;
                this.SaveButtonVisibility = System.Windows.Visibility.Visible;
                this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
                this.IsNewButtonEnabled = true;
            }
            else if (this.SelectedLicense != null && this.isNewLicenseAdded == true)
            {
                this.IsNewButtonEnabled = true;
                this.LicenseInformationGridIsEnabled = true;
                this.IsRemoveButtonEnabled = true;
                this.CreateButtonVisibility = System.Windows.Visibility.Visible;
                this.SaveButtonVisibility = System.Windows.Visibility.Collapsed;
                if (this.SelectedLicense != this.newLicense)
                {
                    this.Licenses.Remove(this.newLicense);
                    this.isNewLicenseAdded = false;
                    this.CreateButtonVisibility = System.Windows.Visibility.Collapsed;
                    this.SaveButtonVisibility = System.Windows.Visibility.Visible;
                }
            }

            if (arg.OriginalSource is DataGrid)
            {
                DataGrid dataGrid = arg.OriginalSource as DataGrid;
                License selectedLicense = dataGrid.SelectedItem as License;
                if (selectedLicense != null)
                {
                    dataGrid.ScrollIntoView(selectedLicense);
                }

                if (this.SelectedLicense != null)
                {
                    this.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedLicense.ApplicationVersion;
                    this.SerialNumber = this.SelectedLicense.SerialNumber;
                    this.SelectedLicenseType = this.LicenseTypes.First(p => (short)p.Second == this.SelectedLicense.Type);
                    this.SelectedLicenseLevel = this.LicenseLevels.First(p => (short)p.Second == this.SelectedLicense.Level);
                    this.StartingDate = new DateTime(this.SelectedLicense.StartingDate.Year, this.SelectedLicense.StartingDate.Month, this.SelectedLicense.StartingDate.Day, 0, 0, 0);
                    this.ExpirationDate = new DateTime(this.SelectedLicense.ExpirationDate.Year, this.SelectedLicense.ExpirationDate.Month, this.SelectedLicense.ExpirationDate.Day, 23, 59, 59);
                    if (this.SelectedLicense.ApplicationVersion != null)
                    {
                        this.SelectedVersion = this.ApplicationVersions.FirstOrDefault(p => p.Equals(this.SelectedLicense.ApplicationVersion));
                    }

                    this.SelectedDistributor = this.SelectedLicense.Distributor;
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down data grid.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void PreviewMouseLeftButtonDownDataGrid(MouseButtonEventArgs arg)
        {
            // handle export file action.
            System.Windows.UIElement source2 = arg.OriginalSource as System.Windows.UIElement;
            AnimatedImageButton container2 = source2 as AnimatedImageButton;

            while (container2 == null && source2 != null)
            {
                source2 = System.Windows.Media.VisualTreeHelper.GetParent(source2) as System.Windows.UIElement;
                container2 = source2 as AnimatedImageButton;
            }

            if (container2 != null && !this.isNewLicenseAdded)
            {
                ExportAction();
            }

            License targetLicense = null;
            System.Windows.UIElement source = arg.OriginalSource as System.Windows.UIElement;
            DataGridRow container = source as DataGridRow;

            while (container == null && source != null)
            {
                source = System.Windows.Media.VisualTreeHelper.GetParent(source) as System.Windows.UIElement;
                container = source as DataGridRow;
            }

            if (container != null && container.Item != null)
            {
                targetLicense = container.Item as License;

                if (this.SelectedLicense != targetLicense)
                {
                    if (this.IsLicenseEdited(this.SelectedLicense) && this.SelectedLicense != targetLicense)
                    {
                        MessageDialogResult messageBoxResult =
                            this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                        if (MessageDialogResult.Yes == messageBoxResult)
                        {
                            this.SelectedLicense = targetLicense;
                        }
                        else
                        {
                            arg.Handled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down add company.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void PreviewMouseLeftButtonDownAddCompany(MouseButtonEventArgs arg)
        {
            if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Handled = true;
                }
                else
                {
                    this.AddNewCompany();
                }
            }
        }

        /// <summary>
        /// Previews the mouse left button down add user.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void PreviewMouseLeftButtonDownAddUser(MouseButtonEventArgs arg)
        {
            if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Handled = true;
                }
                else
                {
                    this.AddNewUser();
                }
            }
        }

        /// <summary>
        /// Determines whether [is license edited] [the specified license].
        /// </summary>
        /// <param name="license">The license.</param>
        /// <returns>
        /// <c>true</c> if [is license edited] [the specified license]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsLicenseEdited(License license)
        {
            return this.isNewLicenseAdded == true ||
                 (this.SerialNumber != null && license != null && license.SerialNumber != null && !license.SerialNumber.Trim().Equals(this.SerialNumber.Trim())) ||
                 (this.SelectedVersion != null && this.ApplicationVersions != null && license != null && license.ApplicationVersion != null && !license.ApplicationVersion.Trim().Equals(this.SelectedVersion.Trim())) ||
                 (this.SelectedLicenseType != null && license != null && license.Type != (short)this.SelectedLicenseType.Second) ||
                 (this.SelectedLicenseLevel != null && license != null && license.Level != (short)this.SelectedLicenseLevel.Second) ||
                 (this.SelectedDistributor != null && license != null && license.Distributor != null && license.Distributor.ID != this.SelectedDistributor.ID) ||
                 (this.StartingDate != null && license != null && license.StartingDate != null && !license.StartingDate.Equals(this.StartingDate)) ||
                 (this.ExpirationDate != null && license != null && license.ExpirationDate != null && !license.ExpirationDate.Equals(this.ExpirationDate));
        }

        /// <summary>
        /// Adds a new company.
        /// </summary>
        private void AddNewCompany()
        {
            var viewModel = this.container.GetExportedValue<AddCompanyViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Adds the new user.
        /// </summary>
        private void AddNewUser()
        {
            if (this.SelectedLicense != null && this.IsLicenseEdited(this.SelectedLicense))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedDataSelectionChanged, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    return;
                }
            }

            AddUserViewModel addUserViewModel =
                  new AddUserViewModel(
                      this.container,
                      this.windowService,
                      this.messenger,
                      this.SelectedCompany);

            this.windowService.ShowViewInDialog(addUserViewModel);
        }

        /// <summary>
        /// Adds a new distributor.
        /// </summary>
        private void AddNewDistributor()
        {
            AddDistributorViewModel addDistributorViewModel = this.container.GetExportedValue<AddDistributorViewModel>();
            this.windowService.ShowViewInDialog(addDistributorViewModel);
        }

        /// <summary>
        /// Export license file content action.
        /// </summary>
        private void ExportAction()
        {
            string path = SelectPath();

            if (path != null)
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                string constructedString = encoding.GetString(this.SelectedLicense.FileContent);

                WriteDataToFile(path, constructedString);
            }
        }

        /// <summary>
        /// Called when [before unload].
        /// </summary>
        /// <returns>True if the user want's to leave without saving the data, false otherwise</returns>
        public override bool OnUnloading()
        {
            if (this.IsLicenseEdited(this.SelectedLicense))
            {
                MessageDialogResult messageBoxResult =
                    this.windowService.MessageDialogService.Show(LanguageResource.General_UnsavedData, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.container.GetExportedValue<ShellViewModel>().CloseCommand.Execute(null);
        }

        #endregion

        #region Messages Received

        /// <summary>
        /// Receives the message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveNewCompanyAddedMessage(NotificationMessage<Company> message)
        {
            if (this.Companies != null)
            {
                if (message.Notification == Notification.NewCompanyAdded)
                {
                    if (message.Content != null)
                    {
                        Company addedCompany = message.Content;
                        this.Companies.Add(addedCompany);
                        var sender = message.Sender;
                        if (sender is AddCompanyViewModel)
                        {
                            if (this.Licenses != null)
                            {
                                this.Licenses.Clear();
                            }

                            if (this.Users != null)
                            {
                                this.Users.Clear();
                            }

                            this.ClearLicense();
                            this.SelectedCompany = addedCompany;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives the user message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveNewUserAddedMessage(NotificationMessage<User> message)
        {
            if (this.Users != null)
            {
                if (message.Notification == Notification.NewUserAdded)
                {
                    if (message.Content != null)
                    {
                        User addedUser = message.Content;
                        if (this.SelectedCompany != null && this.SelectedCompany.ID == addedUser.Company.ID)
                        {
                            this.Users.Add(addedUser);
                            var sender = message.Sender;
                            if (sender is AddUserViewModel)
                            {
                                if (this.Licenses != null)
                                {
                                    this.Licenses.Clear();
                                }

                                this.ClearLicense();
                                this.SelectedUser = addedUser;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives the new distributor added message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveNewDistributorAddedMessage(NotificationMessage<Distributor> message)
        {
            if (this.Distributors != null)
            {
                if (message.Notification == Notification.NewDistributorAdded)
                {
                    if (message.Content != null)
                    {
                        Distributor addedDistritbutor = message.Content;
                        this.Distributors.Add(addedDistritbutor);
                        var sender = message.Sender;
                        if (sender is AddDistributorViewModel)
                        {
                            this.SelectedDistributor = addedDistritbutor;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives the delete distributor message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveDeleteDistributorMessage(NotificationMessage<Distributor> message)
        {
            if (this.Distributors != null)
            {
                if (message.Notification == Notification.DistributorRemoved)
                {
                    if (message.Content != null)
                    {
                        Distributor removedDistributor = message.Content;
                        if (this.Distributors.Contains(removedDistributor))
                        {
                            this.Distributors.Remove(removedDistributor);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives the delete company message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveDeleteCompanyMessage(NotificationMessage<Company> message)
        {
            if (this.Companies != null)
            {
                if (message.Notification == Notification.CompanyRemoved)
                {
                    if (message.Content != null)
                    {
                        Company removedCompany = message.Content;
                        if (this.Companies.Contains(removedCompany))
                        {
                            if (this.Users != null)
                            {
                                this.handleUserSelection = false;
                                this.Users.Clear();
                                this.handleUserSelection = true;
                            }

                            if (this.Licenses != null)
                            {
                                this.Licenses.Clear();
                                this.isNewLicenseAdded = false;
                            }

                            this.Companies.Remove(removedCompany);
                        }

                        if (this.SelectedCompany == null)
                        {
                            this.IsEnabledAddUserImageButton = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Receives the delete user message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveDeleteUserMessage(NotificationMessage<User> message)
        {
            if (this.Users != null)
            {
                if (message.Notification == Notification.UserRemoved)
                {
                    if (message.Content != null)
                    {
                        User removedUser = message.Content;
                        if (this.Users.Contains(removedUser))
                        {
                            if (this.Licenses != null)
                            {
                                this.Licenses.Clear();
                                this.isNewLicenseAdded = false;
                            }

                            this.Users.Remove(removedUser);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the notification messages.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessages(NotificationMessage message)
        {
            if (message.Notification == Notification.ApplicationVersionsChanged)
            {
                var selection = this.SelectedVersion;
                this.InitializeApplicationVersions();
                this.SelectedVersion = selection;
            }
        }

        #endregion

        #region Util

        /// <summary>
        /// Writes the data to file.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <param name="key">The key that will be written to file.</param>
        private void WriteDataToFile(string fullPath, string key)
        {
            string encryptedData = string.Empty;
            encryptedData = LicenseGenerator.Encrypt.EncryptString(key, EncryptionRgbIV, EncryptionKey);

            if (fullPath != null && encryptedData != null)
            {
                try
                {
                    File.WriteAllText(fullPath, encryptedData);
                }
                catch (SecurityException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (UnauthorizedAccessException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (NotSupportedException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_InvalidFileName, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_InvalidFileName, ex);
                }
                catch (PathTooLongException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePathTooLong, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePathTooLong, ex);
                }
            }
        }

        /// <summary>
        /// Updates the output directory element from config xml file.
        /// </summary>
        /// <param name="newPath">The new path.</param>
        private void UpdateOutputDirectoryConfigFile(string newPath)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            if (xmlDoc.DocumentElement != null)
            {
                foreach (XmlElement element in xmlDoc.DocumentElement)
                {
                    if (element.Name.Equals("appSettings"))
                    {
                        foreach (XmlNode node in element.ChildNodes)
                        {
                            if (node.Attributes != null && node.Attributes[0].Value.Equals("OutputDirectory"))
                            {
                                node.Attributes[1].Value = newPath;
                            }
                        }
                    }
                }
            }

            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// Selects the file path.
        /// </summary>
        /// <returns>The Selected Path.</returns>
        private string SelectPath()
        {
            // Configure open file dialog box   
            fileDialogService.Filter = LanguageResource.LicenseFilesFileDialogFilter + " (*.lic)|*.lic";
            fileDialogService.Multiselect = false;
            fileDialogService.FileName = this.SelectedCompany.Name + "_" + this.SelectedUser.Name + "_" + this.SelectedLicense.ApplicationVersion;

            // Show open file dialog box
            if (fileDialogService.ShowSaveFileDialog() == true)
            {
                try
                {
                    FileInfo fi = new FileInfo(fileDialogService.FileName);
                    if (!fi.Directory.Exists)
                    {
                        this.windowService.MessageDialogService.Show(LanguageResource.Error_FileNotExists, MessageDialogType.Error);
                        log.Error("File doesn't exists");
                    }
                }
                catch (SecurityException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (UnauthorizedAccessException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePermissionDenied, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePermissionDenied, ex);
                }
                catch (NotSupportedException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_InvalidFileName, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_InvalidFileName, ex);
                }
                catch (PathTooLongException ex)
                {
                    this.windowService.MessageDialogService.Show(LanguageResource.Error_FilePathTooLong, MessageDialogType.Error);
                    log.ErrorException(LanguageResource.Error_FilePathTooLong, ex);
                }

                return fileDialogService.FileName;
            }

            return null;
        }

        #endregion
    }
}