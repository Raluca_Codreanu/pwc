﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// Provides the way to add content to the main menu.
    /// </summary>
    public interface IMainMenuContentProvider
    {
        /// <summary>
        /// Gets the items to be diplayed in the main menu along with the default ones.
        /// Each item should have the Tag property set to the index it must have in the menu. If the Tag property is not set or
        /// outside the valid range, the item is added on the last position.
        /// </summary>
        /// <returns>The list of menu items to appear in the main menu.</returns>
        List<MenuItem> GetMainMenuItems();
    }
}
