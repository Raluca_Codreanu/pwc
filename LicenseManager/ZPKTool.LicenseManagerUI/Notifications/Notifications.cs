﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseManagerUI.Notifications
{
    public static class Notification
    {
        public static readonly string NewCompanyAdded = Guid.NewGuid().ToString();

        public static readonly string NewUserAdded = Guid.NewGuid().ToString();

        public static readonly string DistributorRemoved = Guid.NewGuid().ToString();

        public static readonly string CompanyRemoved = Guid.NewGuid().ToString();

        public static readonly string UserRemoved = Guid.NewGuid().ToString();

        public static readonly string NewDistributorAdded = Guid.NewGuid().ToString();

        /// <summary>
        /// Signals that the list of application versions has changed (added, deleted or updated).
        /// </summary>
        public static readonly string ApplicationVersionsChanged = Guid.NewGuid().ToString();
    }
}
