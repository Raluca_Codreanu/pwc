﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// This exception is thrown when the input on the UI is not valid.
    /// </summary>
    public class ValidationException : Exception
    {
        /// <summary>
        /// The list of messages.
        /// </summary>
        public List<string> errorList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorList">The error list.</param>
        public ValidationException(string errorCode, List<string> errorList)
            : base(string.Empty)
        {
            this.errorList = errorList;
        }
    }
}
