﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.IO;
using System.Reflection;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        private Bootstrapper bootstrapper;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Do not allow more than one instance of the application to run.
            System.Diagnostics.Process thisProc = System.Diagnostics.Process.GetCurrentProcess();
            System.Diagnostics.Process[] runningProcs = System.Diagnostics.Process.GetProcessesByName(thisProc.ProcessName);
            if (runningProcs.Length > 1)
            {
                Application.Current.Shutdown();
                return;
            }

            base.OnStartup(e);

            log.Info("--- Starting the application ---");

            // Renames the app data folder due to branding changes.
            this.RenameAppDataFolder();

            this.bootstrapper = new Bootstrapper();
            this.bootstrapper.Run();
            var controller = this.bootstrapper.CompositionContainer.GetExportedValue<ShellController>();

            controller.Run();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Exit"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.Windows.ExitEventArgs"/> that contains the event data.</param>
        protected override void OnExit(ExitEventArgs e)
        {
            log.Info("--- Shutting down the application ---");

            base.OnExit(e);
        }

        /// <summary>
        /// Handles the DispatcherUnhandledException event of the Application.
        /// This event is triggered when an application exception is not handled.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Threading.DispatcherUnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            log.ErrorException("An unhandled exception reached the UI dispatcher.", e.Exception);
            MessageWindow.Show("Internal Error.", MessageDialogType.Error);
            e.Handled = true;
        }

        /// <summary>
        /// Renames the folder where the application keeps some data in the current user's LOCALAPPDATA location
        /// according to the current branding name.
        /// </summary>
        private void RenameAppDataFolder()
        {
            var oldAppDataFolderPath =
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PROimpens License Manager";
            var currentAppDataFolderPath = ZPKTool.LicenseManagerBackend.Configuration.ApplicationDataFolderPath;
            if (Directory.Exists(oldAppDataFolderPath) && !Directory.Exists(currentAppDataFolderPath))
            {
                try
                {
                    Directory.Move(oldAppDataFolderPath, currentAppDataFolderPath);
                }
                catch (Exception e)
                {
                    log.ErrorException("Failed to rename an old app data folder to the current name.", e);
                }
            }
        }
    }
}
