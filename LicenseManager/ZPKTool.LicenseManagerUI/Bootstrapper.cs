﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.MvvmCore;
using System.ComponentModel.Composition.Hosting;


namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// Configure and initialize the UI composition framework.
    /// </summary>
    class Bootstrapper : MefBootstrapper
    {
        /// <summary>
        /// Adds all necessary modules into the module catalog.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            this.ModuleCatalog.Catalogs.Add(new AssemblyCatalog(System.Reflection.Assembly.GetExecutingAssembly()));
        }
    }
}
