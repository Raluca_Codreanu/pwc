﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Media;
using ZPKTool.Common;
using ZPKTool.LicenseManagerUI.Languages;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// This class represents the message windows in the application
    /// </summary>
    public partial class MessageWindow
    {
        /// <summary>
        /// The result returned by the Show method.
        /// </summary>
        private static MessageDialogResult result = MessageDialogResult.None;

        /// <summary>
        /// The type of the message dialog to display. It determines the icon and buttons displayed.
        /// </summary>
        private MessageDialogType windowType;

        #region Constructors

        /// <summary>
        /// Prevents a default instance of the <see cref="MessageWindow"/> class from being created.
        /// </summary>
        private MessageWindow()
        {
            this.InitializeComponent();
            this.Owner = this.DetermineOwner();
            if (this.Owner != null)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }
            else
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindow"/> class.
        /// </summary>
        /// <param name="message">The message to display in the window.</param>
        /// <param name="type">The type of the window. Influences the icon and available buttons.</param>
        private MessageWindow(string message, MessageDialogType type)
            : this()
        {
            this.MessageText.Text = message;
            this.windowType = type;

            switch (type)
            {
                case MessageDialogType.Info:
                    AutomationProperties.SetAutomationId(this, "Info");
                    this.Title = LanguageResource.General_Info;
                    this.AddButton(LanguageResource.General_OK, MessageDialogResult.OK, true);
                    this.MessageImage.Source = Application.Current.Resources["InfoIcon"] as ImageSource;
                    break;

                case MessageDialogType.Warning:
                    AutomationProperties.SetAutomationId(this, "Warning");
                    this.Title = LanguageResource.General_Warning;
                    this.AddButton(LanguageResource.General_OK, MessageDialogResult.OK, true);
                    this.MessageImage.Source = Application.Current.Resources["WarningIcon"] as ImageSource;
                    break;

                case MessageDialogType.YesNo:
                    AutomationProperties.SetAutomationId(this, "Question");
                    this.Title = LanguageResource.General_Question;
                    this.AddButton(LanguageResource.General_Yes, MessageDialogResult.Yes, true);
                    this.AddButton(LanguageResource.General_No, MessageDialogResult.No, false);
                    this.MessageImage.Source = Application.Current.Resources["QuestionImage"] as ImageSource;
                    break;

                case MessageDialogType.YesNoCancel:
                    AutomationProperties.SetAutomationId(this, "Question");
                    this.Title = LanguageResource.General_Question;
                    this.AddButton(LanguageResource.General_Yes, MessageDialogResult.Yes, true);
                    this.AddButton(LanguageResource.General_No, MessageDialogResult.No, false);
                    this.AddButton(LanguageResource.General_Cancel, MessageDialogResult.Cancel, false);
                    this.MessageImage.Source = Application.Current.Resources["QuestionImage"] as ImageSource;
                    break;

                case MessageDialogType.Error:
                    AutomationProperties.SetAutomationId(this, "Error");
                    this.Title = LanguageResource.General_Error;
                    this.AddButton(LanguageResource.General_OK, MessageDialogResult.OK, true);
                    this.MessageImage.Source = Application.Current.Resources["ErrorIcon"] as ImageSource;
                    break;

                default:
                    break;
            }
        }

        #endregion Constructors

        /// <summary>
        /// Shows a message window.
        /// </summary>
        /// <param name="message">The message to displayed.</param>
        /// <param name="type">The type of the message.</param>
        /// <returns>
        /// A value signifying the action performed by the user on the message window.
        /// </returns>
        public static MessageDialogResult Show(string message, MessageDialogType type)
        {
            MessageWindow wnd = new MessageWindow(message, type);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Shows a message window.
        /// </summary>
        /// <param name="e">The exception for which it will display an error message.</param>
        /// <returns>
        /// A value signifying the action performed by the user on the message window.
        /// </returns>
        public static MessageDialogResult Show(Exception e)
        {
            string message = GetErrorMessage(e);
            MessageWindow wnd = new MessageWindow(message, MessageDialogType.Error);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Returns the error message to display for the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>
        /// The error message.
        /// </returns>
        public static string GetErrorMessage(Exception exception)
        {
            // Exception thrown by code invoked using reflection are wrapped in a TargetInvocationException.
            if (exception is System.Reflection.TargetInvocationException && exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            var compositionEx = exception as System.ComponentModel.Composition.CompositionException;
            if (compositionEx != null)
            {
                // A CompositionException can appear not only because of issues in composing MEF parts (which usually is due to bugs on our side),
                // but also if a part's constructor throws an exception. For example, a view-model is exported as a part and in the constructor makes a db call;
                // if the call fails, usually an exception that identifies the problem is thrown. The CompositionException hides the app specific exceptions so we
                // have to extract them.
                var compositionError1 = compositionEx.Errors[0];
                if (compositionError1.Exception != null)
                {
                    // We handle just the 1st error from CompositionException because if there are more they are not app related errors (they are real composition errors). 
                    // There is one composition error for each part, and that error usually contains another CompositionException, so we handle that recursively.
                    // The CompositionException from the composition error should contain a ComposablePartException, which will contain the application related exception.
                    return GetErrorMessage(compositionError1.Exception);
                }
            }

            var composablePartEx = exception as System.ComponentModel.Composition.Primitives.ComposablePartException;
            if (composablePartEx != null)
            {
                // The ComposablePartException should contain the actual application exception in the InnerException property.
                // If it doesn't, the recursive call will fall back to internal error.
                if (composablePartEx.InnerException != null)
                {
                    return GetErrorMessage(composablePartEx.InnerException);
                }
            }

            string message = null;
            ZPKException zpkException = exception as ZPKException;
            if (zpkException != null)
            {
                // Get the error message
                message = string.IsNullOrWhiteSpace(zpkException.ErrorCode)
                    ? exception.Message
                    : LanguageResource.ResourceManager.GetString(zpkException.ErrorCode);

                // Format the error message using the error message format parameters
                if (!string.IsNullOrWhiteSpace(message)
                    && zpkException.ErrorMessageFormatArgs != null
                    && zpkException.ErrorMessageFormatArgs.Any())
                {
                    message = string.Format(message, zpkException.ErrorMessageFormatArgs.ToArray());
                }
            }
            else
            {
                message = LanguageResource.ResourceManager.GetString(ErrorCodes.InternalError);
            }

            return message;
        }

        /// <summary>
        /// Determines the owner of the message window.
        /// </summary>
        /// <returns>A window or null.</returns>
        private Window DetermineOwner()
        {
            // Set the active window as the owner, if there is one.
            Window owner = Application.Current.Windows.OfType<Window>().FirstOrDefault(wnd => wnd.IsActive && wnd != this);
            if (owner == null)
            {
                // If there is no active window (the app does not have focus), use the last window from the app windows list as the owner.
                owner = Application.Current.Windows.OfType<Window>().Reverse().FirstOrDefault(wnd => wnd != this && wnd.IsLoaded);
            }

            return owner;
        }

        /// <summary>
        /// Create and add a button to the window
        /// </summary>
        /// <param name="buttonText">The text on the button</param>
        /// <param name="buttonType">Type of the button.</param>
        /// <param name="isDefault">if set to true the button will be the default button, invoked when ENTER is pressed.</param>
        private void AddButton(string buttonText, MessageDialogResult buttonType, bool isDefault)
        {
            // create a button
            ZPKTool.Controls.Button button = new ZPKTool.Controls.Button();
            button.IsDefault = isDefault;
            button.Content = buttonText;
            button.Margin = new Thickness(5, 0, 5, 0);

            this.ButtonsStackPanel.Children.Add(button);

            // based on the button type, set the event handler
            switch (buttonType)
            {
                case MessageDialogResult.OK:
                    button.Name = "OkButton";
                    button.MinWidth = 60;
                    button.Click += new RoutedEventHandler(OKButton_Click);
                    break;
                case MessageDialogResult.Yes:
                    button.Name = "YesButton";
                    button.Click += new RoutedEventHandler(YesButton_Click);
                    break;
                case MessageDialogResult.No:
                    button.Name = "NoButton";
                    button.Click += new RoutedEventHandler(NoButton_Click);
                    break;
                case MessageDialogResult.Cancel:
                    button.Name = "CancelButton";
                    button.Click += new RoutedEventHandler(CancelButton_Click);
                    break;
                default:
                    break;
            }
        }

        #region Events

        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the NoButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.No;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the YesButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.Yes;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the OKButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.OK;
            Close();
        }

        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (result == MessageDialogResult.None)
            {
                if (this.windowType == MessageDialogType.Error
                    || this.windowType == MessageDialogType.Info
                    || this.windowType == MessageDialogType.Warning)
                {
                    result = MessageDialogResult.OK;
                }
                else if (this.windowType == MessageDialogType.YesNo)
                {
                    result = MessageDialogResult.No;
                }
                else if (this.windowType == MessageDialogType.YesNoCancel)
                {
                    result = MessageDialogResult.Cancel;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Handles the KeyDownEvent
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                Close();
            }
        }

        #endregion
    }
}
