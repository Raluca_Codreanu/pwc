﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using ZPKTool.MvvmCore.Services;
using ZPKTool.MvvmCore;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// Implementation of IMessageService that shows the message in a modal window.
    /// </summary>
    [Export(typeof(IMessageDialogService))]
    public class MessageDialogService : IMessageDialogService
    {
        /// <summary>
        /// The service that offers access to the Dispatcher associated with this application instance.
        /// </summary>
        private readonly IDispatcherService dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDialogService"/> class.
        /// </summary>
        /// <param name="dispatcher">The dispatcher service.</param>
        [ImportingConstructor]
        public MessageDialogService(IDispatcherService dispatcher)
        {
            Argument.IsNotNull("dispatcher", dispatcher);

            this.dispatcher = dispatcher;
        }

        /// <summary>
        /// Shows a message for the specified exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the message.
        /// </returns>
        public MessageDialogResult Show(Exception e)
        {
            if (e == null)
            {
                throw new ArgumentNullException("e", "The exception was null.");
            }
                        
            MessageDialogResult result = MessageDialogResult.None;
            Action action = () => result = MessageWindow.Show(e);

            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }

            return result;
        }

        /// <summary>
        /// Shows the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type of the message.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the message.
        /// </returns>
        public MessageDialogResult Show(string message, MessageDialogType type)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException("message", "The message was null or empty.");
            }
            
            MessageDialogResult result = MessageDialogResult.None;
            Action action = () => result = MessageWindow.Show(message, type);

            if (this.dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                this.dispatcher.Invoke(action);
            }

            return result;
        }

        /// <summary>
        /// Shows a message dialog with custom content.
        /// </summary>
        /// <param name="dialogInfo">The message dialog information used to compute the window content</param>
        /// <returns>A value resulting from the interaction of the user with the dialog.</returns>
        public MessageDialogResult Show(MessageDialogInfo dialogInfo)
        {
            throw new NotImplementedException("Not implemented. Copy implementation from Gui if it is needed.");
        }

        /// <summary>
        /// Shows the specified message in a message dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="additionalMessage">The additional message</param>
        /// <param name="type">The type of the message dialog to display.</param>
        /// <returns>
        /// A value resulting from the interaction of the user with the dialog.
        /// </returns>
        public MessageDialogResult Show(string message, string additionalMessage, MessageDialogType type)
        {
            throw new NotImplementedException("Not implemented. Copy implementation from Gui if it is needed.");
        }

        /// <summary>
        /// Gets a user-friendly message for the specified exception.
        /// <para />
        /// (This is the method used to obtain the message displayed by the <see cref="Show(Exception e)" /> method.)
        /// </summary>
        /// <param name="e">The exception.</param>
        /// <returns>
        /// A string containing the message.
        /// </returns>
        public string GetErrorMessage(Exception e)
        {
            return MessageWindow.GetErrorMessage(e);
        }
    }
}
