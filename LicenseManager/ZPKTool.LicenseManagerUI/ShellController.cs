﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using ZPKTool.MvvmCore;
using ZPKTool.LicenseManagerUI.ViewModels;
using ZPKTool.Common;
using ZPKTool.LicenseManagerUI.Views;
using System.Windows;

namespace ZPKTool.LicenseManagerUI
{
    /// <summary>
    /// The application controller used to glue together the UI components
    /// </summary>
    [Export]
    public class ShellController
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container that contains all UI parts.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The shell of the application.
        /// </summary>
        private ShellViewModel shell;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellController"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        [ImportingConstructor]
        public ShellController(CompositionContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            this.container = container;
        }

        /// <summary>
        /// Initializes the application and displays the shell.
        /// </summary>
        public void Run()
        {
            this.shell = this.container.GetExportedValue<ShellViewModel>();
            ShellView view = new ShellView();
            Application.Current.MainWindow = view;
            view.DataContext = this.shell;
            view.Show();
        }
    }
}
