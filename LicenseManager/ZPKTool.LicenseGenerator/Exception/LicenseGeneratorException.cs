﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseGenerator
{
    /// <summary>
    /// Exception thrown by code in the <see cref="ZPKTool.LicenseGenerator"/> assembly.
    /// </summary>
    public class LicenseGeneratorException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGeneratorException"/> class.
        /// </summary>
        public LicenseGeneratorException()
            : base(string.Empty)
        {
            this.ErrorCode = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGeneratorException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public LicenseGeneratorException(string errorCode)
            : base(string.Empty)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGeneratorException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        public LicenseGeneratorException(string errorCode, string errorMessage)
            : base(errorMessage)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGeneratorException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The ex.</param>
        public LicenseGeneratorException(string errorCode, string errorMessage, Exception ex)
            : base(errorMessage, ex)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGeneratorException"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public LicenseGeneratorException(Exception exception)
            : base(exception.Message, exception)
        {
        }

        #region Properties

        /// <summary>
        /// Gets or sets the error code of the exception.
        /// </summary>
        public string ErrorCode { get; set; }

        #endregion Properties
    }
}
