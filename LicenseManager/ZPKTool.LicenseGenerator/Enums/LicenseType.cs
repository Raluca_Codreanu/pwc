﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseGenerator
{
    /// <summary>
    /// The available types for a license.
    /// </summary>
    public enum LicenseType
    {
        /// <summary>
        /// Node-locked license.
        /// </summary>
        Nodelocked = 0
    }
}
