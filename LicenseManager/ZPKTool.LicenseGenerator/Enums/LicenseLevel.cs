﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseGenerator
{
    /// <summary>
    /// The level of access to application features provided by a license.
    /// </summary>
    public enum LicenseLevel
    {
        /// <summary>
        /// Trial license.
        /// </summary>
        Trial = 0,

        /// <summary>
        /// Basic license.
        /// </summary>
        Basic = 1,

        /// <summary>
        /// Professional license.
        /// </summary>
        ProfessionalMech = 2,

        /// <summary>
        /// Professional license that includes master data updates.
        /// </summary>
        ProfessionalMechIncMDUpdates = 3
    }
}
