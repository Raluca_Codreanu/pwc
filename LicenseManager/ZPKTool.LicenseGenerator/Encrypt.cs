﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.LicenseGenerator
{
    /// <summary>
    /// Encrypts a string.
    /// </summary>
    public static class Encrypt
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Encrypts the string using a chaining mode called cipher block chaining (CBC), 
        /// which requires a key (Key) and an initialization vector (IV) to perform 
        /// cryptographic transformations on data. To decrypt data that was encrypted 
        /// using one of the SymmetricAlgorithm classes, you must set the Key property 
        /// and the IV property to the same values that were used for encryption. 
        /// For a symmetric algorithm to be useful, the secret key must be known only 
        /// to the sender and the receiver.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <param name="rgbIV">The RGB IV used for encryption.</param>
        /// <param name="key">The key used for encryption.</param>
        /// <returns>The encrypted string.</returns>
        public static string EncryptString(string clearText, string rgbIV, string key)
        {
            if (string.IsNullOrWhiteSpace(clearText))
            {
                throw new ArgumentNullException("EncryptString: clearText parameter is null");
            }

            if (string.IsNullOrWhiteSpace(rgbIV))
            {
                throw new ArgumentNullException("EncryptString: rgbIV parameter is null");
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException("EncryptString: key parameter is null");
            }

            try
            {
                // @coderevdone: Validate input parameters, catch exceptions. Document what encryption algorithm is used (ex: Encrypts a string using RSA-512). Make the class static.
                byte[] clearTextBytes = Encoding.UTF8.GetBytes(clearText);
                System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();

                MemoryStream ms = new MemoryStream();
                byte[] rgbIVBytes = Encoding.ASCII.GetBytes(rgbIV);
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);

                using (CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(keyBytes, rgbIVBytes), CryptoStreamMode.Write))
                {
                    cs.Write(clearTextBytes, 0, clearTextBytes.Length);
                    cs.Close();
                }

                return Convert.ToBase64String(ms.ToArray());
            }
            catch (EncoderFallbackException ex)
            {
                Log.ErrorException("EncoderFallbackException encountered while trying to get the bytes from source text.", ex);
                throw new LicenseGeneratorException(ex);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Log.ErrorException("ArgumentOutOfRangeException encountered while trying to write the encoded text.", ex);
                throw new LicenseGeneratorException(ex);
            }
            catch (NotSupportedException ex)
            {
                Log.ErrorException(
                    "The System.Security.Cryptography.CryptoStreamMode associated with current System.Security.Cryptography.CryptoStream object does not match the underlying stream. For example, this exception is thrown when using System.Security.Cryptography.CryptoStreamMode.Write with an underlying stream that is read only.",
                    ex);
                throw new LicenseGeneratorException(ex);
            }
        }
    }
}
