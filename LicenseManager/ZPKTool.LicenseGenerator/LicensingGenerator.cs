﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using ZPKTool.LicenseManagerBackend;

namespace ZPKTool.LicenseGenerator
{
    /// <summary>
    /// It is responsible for generating xml digital signature license files.
    /// </summary>
    public class LicensingGenerator
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The private key used for generating the XML Digital Signature license.
        /// </summary>
        private readonly string privateKey;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicensingGenerator"/> class.
        /// </summary>
        /// <param name="privateKey">The private key.</param>
        public LicensingGenerator(string privateKey)
        {
            if (string.IsNullOrWhiteSpace(privateKey))
            {
                throw new ArgumentNullException("privateKey", "License generator: private key parameter is null.");
            }

            // @coderevdone: validate params
            this.privateKey = privateKey;
        }

        #endregion

        #region Generate

        /// <summary>
        /// Generates the Xml Digital Signature License file using the RSA Crypto Service Provider.
        /// </summary>
        /// <param name="license">The license.</param>
        /// <returns>
        /// Returns a string representing the xml digital signature file.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// <paramref name="license "/> was null or contained some invalid data.
        /// </exception>
        /// <exception cref="ZPKTool.LicenseGenerator.LicenseGeneratorException">
        /// </exception>
        public string Generate(License license)
        {
            if (license == null)
            {
                throw new ArgumentNullException("license");
            }

            if (license.ApplicationVersion == null)
            {
                throw new ArgumentNullException("license.ApplicationVersion");
            }

            if (license.Distributor == null)
            {
                throw new ArgumentNullException("license.Distributor");
            }

            if (license.SerialNumber == null)
            {
                throw new ArgumentNullException("license.SerialNumber");
            }

            if (license.StartingDate == null)
            {
                throw new ArgumentNullException("license.StartingDate");
            }

            try
            {
                // @coderevdone: You can shorten the parameters list by putting them in a structure. Validate the parameters that must not be null or empty.
                // Catch exceptions and if you can't handle them wrap them in a license validation specific exception.
                using (var rsa = new RSACryptoServiceProvider())
                {
                    rsa.FromXmlString(privateKey);
                    var doc = CreateDocument(license);

                    var signature = CreateXmlDigitalSignature(doc, rsa);
                    doc.FirstChild.AppendChild(doc.ImportNode(signature, true));

                    var ms = new MemoryStream();
                    var writer = XmlWriter.Create(ms, new XmlWriterSettings { Indent = true, Encoding = Encoding.UTF8 });
                    doc.Save(writer);
                    ms.Position = 0;
                    return new StreamReader(ms).ReadToEnd();
                }
            }
            catch (IOException ex)
            {
                Log.ErrorException("An I/O error occurs while reading from the xml in-memory stream.", ex);
                throw new LicenseGeneratorException(ex);
            }
            catch (OutOfMemoryException ex)
            {
                Log.ErrorException("There is insufficient memory to allocate a buffer for the returned string.", ex);
                throw new LicenseGeneratorException(ex);
            }
            catch (XmlException ex)
            {
                Log.ErrorException("The operation would not result in a well formed XML document (for example, no document element or duplicate XML declarations).", ex);
                throw new LicenseGeneratorException(ex);
            }
            catch (BackendException ex)
            {
                throw new LicenseGeneratorException(ex);
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                Log.ErrorException("The format of the xmlString parameter (privateKey) is not valid.", ex);
                throw new LicenseGeneratorException(ex);
            }
        }

        #endregion

        #region Document

        /// <summary>
        /// Creates the XML digital signature.
        /// </summary>
        /// <param name="document">The xml document.</param>
        /// <param name="key">The private Key.</param>
        /// <returns>
        /// The signature represented as an xml element.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// <paramref name="document"/> was null or <paramref name="key"/> was null.
        /// </exception>
        /// <exception cref="BackendException">An internal error occurred.</exception>        
        private static XmlElement CreateXmlDigitalSignature(XmlDocument document, AsymmetricAlgorithm key)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            try
            {                
                var signedXml = new SignedXml(document) { SigningKey = key };
                var reference = new Reference { Uri = string.Empty };
                reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                signedXml.AddReference(reference);
                signedXml.ComputeSignature();
                return signedXml.GetXml();
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                throw new BackendException("InternalError", ex);
            }
        }

        /// <summary>
        /// Creates the xml document used to create the xml digital signature file.
        /// </summary>
        /// <param name="license">The license.</param>
        /// <returns>
        /// It creates the xml file.
        /// </returns>
        private static XmlDocument CreateDocument(License license)
        {
            var doc = new XmlDocument();

            // license element
            XmlElement licenseElement = doc.CreateElement("license");
            doc.AppendChild(licenseElement);

            // distributor element
            XmlElement distribuitorElement = doc.CreateElement("distributor");
            licenseElement.AppendChild(distribuitorElement);

            XmlElement distribuitorNameElement = doc.CreateElement("name");
            distribuitorElement.AppendChild(distribuitorNameElement);
            if (license.Distributor != null && license.Distributor.Name != null)
            {
                distribuitorNameElement.InnerText = license.Distributor.Name;
            }

            XmlElement distribuitorPhoneElement = doc.CreateElement("phone");
            distribuitorElement.AppendChild(distribuitorPhoneElement);
            if (license.Distributor != null && license.Distributor.Phone != null)
            {
                distribuitorPhoneElement.InnerText = license.Distributor.Phone;
            }

            XmlElement distribuitorFaxElement = doc.CreateElement("fax");
            distribuitorElement.AppendChild(distribuitorFaxElement);
            if (license.Distributor != null && license.Distributor.Fax != null)
            {
                distribuitorFaxElement.InnerText = license.Distributor.Fax;
            }

            XmlElement distribuitorStreetElement = doc.CreateElement("street");
            distribuitorElement.AppendChild(distribuitorStreetElement);
            if (license.Distributor != null && license.Distributor.Street != null)
            {
                distribuitorStreetElement.InnerText = license.Distributor.Street;
            }

            XmlElement distribuitorCityElement = doc.CreateElement("city");
            distribuitorElement.AppendChild(distribuitorCityElement);
            if (license.Distributor != null && license.Distributor.City != null)
            {
                distribuitorCityElement.InnerText = license.Distributor.City;
            }

            XmlElement distribuitorPostCodeElement = doc.CreateElement("postcode");
            distribuitorElement.AppendChild(distribuitorPostCodeElement);
            if (license.Distributor != null && license.Distributor.PostCode != null)
            {
                distribuitorPostCodeElement.InnerText = license.Distributor.PostCode;
            }

            XmlElement distribuitorEmailElement = doc.CreateElement("email");
            distribuitorElement.AppendChild(distribuitorEmailElement);
            if (license.Distributor != null && license.Distributor.Email != null)
            {
                distribuitorEmailElement.InnerText = license.Distributor.Email;
            }

            XmlElement distribuitorWebPageElement = doc.CreateElement("webpage");
            distribuitorElement.AppendChild(distribuitorWebPageElement);
            if (license.Distributor != null && license.Distributor.WebPage != null)
            {
                distribuitorWebPageElement.InnerText = license.Distributor.WebPage;
            }

            // company element 
            XmlElement companyElement = doc.CreateElement("company");
            licenseElement.AppendChild(companyElement);

            XmlElement companyNameElement = doc.CreateElement("name");
            companyElement.AppendChild(companyNameElement);
            if (license.User != null && license.User.Company != null && license.User.Company.Name != null)
            {
                companyNameElement.InnerText = license.User.Company.Name;
            }

            // user element
            XmlElement userElement = doc.CreateElement("user");
            licenseElement.AppendChild(userElement);

            XmlElement userNameElement = doc.CreateElement("name");
            userElement.AppendChild(userNameElement);
            if (license.User != null && license.User.Name != null)
            {
                userNameElement.InnerText = license.User.Name;
            }

            XmlElement userPhoneElement = doc.CreateElement("phone");
            userElement.AppendChild(userPhoneElement);
            if (license.User != null && license.User.Phone != null)
            {
                userPhoneElement.InnerText = license.User.Phone;
            }

            XmlElement userFaxElement = doc.CreateElement("fax");
            userElement.AppendChild(userFaxElement);
            if (license.User != null && license.User.Fax != null)
            {
                userFaxElement.InnerText = license.User.Fax;
            }

            XmlElement userStreetElement = doc.CreateElement("street");
            userElement.AppendChild(userStreetElement);
            if (license.User != null && license.User.Street != null)
            {
                userStreetElement.InnerText = license.User.Street;
            }

            XmlElement userCityElement = doc.CreateElement("city");
            userElement.AppendChild(userCityElement);
            if (license.User != null && license.User.City != null)
            {
                userCityElement.InnerText = license.User.City;
            }

            XmlElement userCountryElement = doc.CreateElement("country");
            userElement.AppendChild(userCountryElement);
            if (license.User != null && license.User.Country != null)
            {
                userCountryElement.InnerText = license.User.Country;
            }

            XmlElement userEmailElement = doc.CreateElement("email");
            userElement.AppendChild(userEmailElement);
            if (license.User != null && license.User.Email != null)
            {
                userEmailElement.InnerText = license.User.Email;
            }

            XmlElement userPostCodeElement = doc.CreateElement("postcode");
            userElement.AppendChild(userPostCodeElement);
            if (license.User != null && license.User.PostCode != null)
            {
                userPostCodeElement.InnerText = license.User.PostCode;
            }

            // license elements
            XmlElement serialNumberElement = doc.CreateElement("serialnumber");
            licenseElement.AppendChild(serialNumberElement);
            if (license.SerialNumber != null)
            {
                serialNumberElement.InnerText = license.SerialNumber;
            }

            XmlElement typeElement = doc.CreateElement("type");
            licenseElement.AppendChild(typeElement);
            typeElement.InnerText = license.Type.ToString();

            XmlElement levelElement = doc.CreateElement("level");
            licenseElement.AppendChild(levelElement);
            levelElement.InnerText = license.Level.ToString();

            XmlElement startingDateElement = doc.CreateElement("startingDate");
            licenseElement.AppendChild(startingDateElement);
            if (license.StartingDate != null)
            {
                startingDateElement.InnerText = license.StartingDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            XmlElement expirationDateElement = doc.CreateElement("expirationDate");
            licenseElement.AppendChild(expirationDateElement);
            if (license.ExpirationDate != null)
            {
                expirationDateElement.InnerText = license.ExpirationDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            XmlElement versionElement = doc.CreateElement("applicationVersion");
            licenseElement.AppendChild(versionElement);
            if (license.ApplicationVersion != null)
            {
                versionElement.InnerText = license.ApplicationVersion;
            }

            return doc;
        }

        #endregion
    }
}
