@echo off
REM %1 param should contain the application version in the N.N.N.N format

REM documentation: http://www.cameyo.com/zero/index.html
REM this part of the XML assemblie to use for ZeroInstaller to pack files

IF "%1"== "" GOTO ParamError

REM the path to de files that needs to pe packed
SET InputFolder=InputViewer\App
SET StartupExe=PCM.exe

REM the file viewerpckgdef.xml contains the information that the ZeroInstaller will use to pack files
echo ^<ZeroInstallerXml^> >viewerpckgdef.xml
echo: >>viewerpckgdef.xml

echo ^<^!-- variable: AppVersion, path: to the Debug or Release folder--^> >>viewerpckgdef.xml
echo: >>viewerpckgdef.xml

REM AppName: Name of the package, AppVersion:automated retrieved,the application version, IconFile:gets the icon from an .exe file
echo 	^<Properties^> >>viewerpckgdef.xml
echo 		^<Property AppName= "PCMViewer" ^/^> >>viewerpckgdef.xml
echo 		^<Property AppVersion= "%1" ^/^> >>viewerpckgdef.xml
echo 		^<Property IconFile="%InputFolder%\PCM.exe" ^/^> >>viewerpckgdef.xml
echo 	^<^/Properties^> >>viewerpckgdef.xml
echo: >>viewerpckgdef.xml

REM in this part is included every file that will be packed, source: the relative path to the file to be included, target: the place int the virtualized app where to put the file
REM with autoLaunch specify which is the app's startup file (exe usually)
echo 	^<FileSystem^> >>viewerpckgdef.xml

REM walk the input folder recursively and add every file to the pack
setlocal EnableDelayedExpansion
set parentfolder=%CD%\
set baseappfolder=%parentfolder%%InputFolder%\
REM change the current directory to be the App file folder so the for statement will work; at the end change back to the original directory
CD %InputFolder%
for /r . %%g in (*.*) do (
  set file=%%g
  set file=!file:%parentfolder%=!
  set folder=%%~dpg
  set folder=!folder:%baseappfolder%=!
  if /I "%%~nxg" == "%StartupExe%" (
	echo ^<File source="!file!" targetdir="%%Program Files%%\PCMViewer\!folder!" autoLaunch="[AppName]" ^/^> >> "%parentfolder%\viewerpckgdef.xml"
  ) else (  
    echo ^<File source="!file!" targetdir="%%Program Files%%\PCMViewer\!folder!" ^/^> >> "%parentfolder%\viewerpckgdef.xml"
  )
)
setlocal DisableDelayedExpansion
CD %parentfolder%

echo 		^<File source="%InputFolder%\GlobalSettings.xml" targetdir="%%Program Files%%\PCMViewer" ^/^> >>viewerpckgdef.xml

echo 	^<^/FileSystem^> >>viewerpckgdef.xml
echo: >>viewerpckgdef.xml

REM the registry changes that the pack will do
echo 	^<Registry^> >>viewerpckgdef.xml
echo 	^<^/Registry^> >>viewerpckgdef.xml
echo: >>viewerpckgdef.xml
 
REM settings for file acces and registry acces 
echo 	^<Sandbox^> >>viewerpckgdef.xml
echo 		^<FileSystem access="Full" ^/^> >>viewerpckgdef.xml
echo 		^<Registry access="Full" ^/^> >>viewerpckgdef.xml
echo 	^<^/Sandbox^> >>viewerpckgdef.xml
echo: >>viewerpckgdef.xml

echo ^<^/ZeroInstallerXml^> >>viewerpckgdef.xml

REM create the GlobalSettings.xml file
echo ^<^?xml version="1.0" encoding="utf-8"?^> >GlobalSettings.xml
echo ^<SettingsFile^> >>GlobalSettings.xml
echo 	^<Settings^> >>GlobalSettings.xml
echo 		^<Setting Name="StartInViewerMode" Type="System.Boolean"^> >>GlobalSettings.xml
echo 		^<Value^>True^<^/Value^> >>GlobalSettings.xml
echo 		^<^/Setting^> >>GlobalSettings.xml
echo 	^<^/Settings^> >>GlobalSettings.xml
echo ^<^/SettingsFile^> >>GlobalSettings.xml

REM move the GlobalSettings.xml file in InputViewer\App
IF NOT EXIST GlobalSettings.xml GOTO MoveErr2
REM - if not exist create folder
IF NOT EXIST .\InputViewer\App\ mkdir .\InputViewer\App\
MOVE GlobalSettings.xml InputViewer\App\

REM try to pack files using command: ZeroInstaller\ZeroInstaller.exe viewerpckgdef.xml
IF NOT EXIST viewerpckgdef.xml  GOTO PackErr
ZeroInstaller\ZeroInstaller.exe viewerpckgdef.xml

REM try to delete viewerpckgdef.xml
DEL viewerpckgdef.xml


REM try to move created package to Output\ProductCostManager\PCMViewer.exe
IF NOT EXIST PCMViewer.exe  GOTO MoveErr
REM - if not exist create folder
IF NOT EXIST .\Output\ProductCostManager\ mkdir .\Output\ProductCostManager\
MOVE PCMViewer.exe Output\ProductCostManager\

GOTO End

REM if the version number doesn't exist than generate error
:ParamError
echo "Version parameter value was missing"
EXIT /B 1

:PackErr
echo "viewerpckgdef.xml could not be found"
EXIT /B 1

:MoveErr
echo "PCMViewer.exe could not be found"
EXIT /B 1

:MoveErr2
echo "GlobalSettings.xml could not be found"
EXIT /B 1

REM end of file
:End

