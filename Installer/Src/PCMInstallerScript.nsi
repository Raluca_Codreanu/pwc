######################################################################
#       Product Cost Manager Installer Script
#
#       This install script requires the following symbols to be defined via the /D commandline switch:
#
#       VERSION           N.N.N.N format version number
#       DBVERSION         N format version number
#
#       The symbols are not case sensitive and available in script using the ${symbol_name} syntax.
#       This script is usually run by the BuildPCMInstaller.cmd batch file.
#
#       Example:
#       makensis /DVERSION=1.2.0.123 /DDBVERSION=1.46 PCMInstallerScript.nsi
#
######################################################################

;--------------------------------
;Constants

  !define ApplicationName "Product Cost Manager"
  !define Company "3C"

  !define DefaultInstallDirName "Product Cost Manager"
  !define LnkName "Product Cost Manager.lnk"
  !define ExeName "PCM.exe"
  !define UninstallLnkName "Uninstall.lnk"
  !define AppDataFolderPath "$LOCALAPPDATA\PCMData"
  !define OldAppDataFolderPath "$LOCALAPPDATA\PROimpens"  ;The path to the old app local data folder, before the PROimpens -> Product Cost Manager rebranding
  !define UserSettingsFile "${AppDataFolderPath}\settings.xml"
  !define GlobalSettingsFile "$INSTDIR\GlobalSettings.xml"
  !define ExeConfigPath "$INSTDIR\PCM.exe.config"
  !define LicenseFilePath ".\EULA.rtf"

  !define AppRegistryRootKey "Software\Product Cost Manager" ;the registry root key under which app related information is stored
  !define UninstallRegistryRootKey "Software\Microsoft\Windows\CurrentVersion\Uninstall\{FE0E5740-1D50-4872-9092-898F7CD3CBAA}" ;the registry root key for the app uninstall information

  !define InputDir "..\Input\"
  !define OutputFileName "..\Output\ProductCostManager\PCMSetup.exe"

;--------------------------------
;General Installer Information

  SetCompressor /FINAL /SOLID lzma
  Name "${ApplicationName}"
  OutFile ${OutputFileName}
  BrandingText "${ApplicationName} v${VERSION}, Database v${DBVERSION}"
  XPStyle on

;--------------------------------
; Version information for the installer output (.exe) file

  VIProductVersion ${VERSION}
  VIAddVersionKey ProductName "${ApplicationName}"
  VIAddVersionKey ProductVersion "${VERSION}"
  VIAddVersionKey FileVersion "${VERSION}"
  VIAddVersionKey CompanyName "${Company}"
  VIAddVersionKey FileDescription "${ApplicationName} Installer"
  VIAddVersionKey LegalCopyright "� ${Company}"
  VIAddVersionKey LegalTrademarks "${ApplicationName} is a trademark of ${Company}"
  
;--------------------------------
; Multi user installation setup

  !define MULTIUSER_MUI
  !define MULTIUSER_EXECUTIONLEVEL Highest
  !define MULTIUSER_INSTALLMODE_COMMANDLINE
  !define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_KEY "${AppRegistryRootKey}"
  !define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_VALUENAME "InstallMode"
  !define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY "${AppRegistryRootKey}"
  !define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUENAME "InstallFolder"
  !define MULTIUSER_INSTALLMODE_INSTDIR "${DefaultInstallDirName}"
  !define MULTIUSER_INIT_TEXT_ADMINREQUIRED "$(AdminPrivilegesRequired)"
  !include MultiUser.nsh

;--------------------------------
; Include scripts

  !include MUI2.nsh
  !include FileFunc.nsh
  !include WordFunc.nsh
  !include Common.nsh
  !include defines.nsh
  !include x64.nsh

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_WELCOMEFINISHPAGE_BITMAP "WelcomeFinishBitmap.bmp" ;defines the image to display on welcome and finish pages
  !define MUI_ICON "app.ico"
  !define MUI_UNICON "app.ico"
  !define MUI_CUSTOMFUNCTION_GUIINIT CheckIfAlreadyInstalled

;----------------------------------
;Language Selection Dialog Settings

  ;Remember the installer language
  !define MUI_LANGDLL_REGISTRY_ROOT "SHCTX"
  !define MUI_LANGDLL_REGISTRY_KEY "${AppRegistryRootKey}"
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"
  
;----------------------------------
;Initialize the Uninstall Log (logging of installed files in order to delete them when uninstalling).
  !define INSTDIR_REG_ROOT "SHCTX"
  !define INSTDIR_REG_KEY "${UninstallRegistryRootKey}"

  ;The uninstaller executable name (without the extension). Must be set before including AdvUninstLog.nsh
  ;Use UNINST_EXE to get the full uninstaller file path (including the install folder path).
  !define UNINSTALL_LOG "Uninstall"
  
  !include AdvUninstLog.nsh
  !insertmacro UNATTENDED_UNINSTALL

  !insertmacro DetectSqlcmdPathMACRO ""
  !insertmacro DetectSqlcmdPathMACRO "un."
  !insertmacro FindSqlcmdPathMACRO ""
  !insertmacro FindSqlcmdPathMACRO "un."

;--------------------------------
;Variables

  var SQLAuthentification        ;Used to authentificate with slqcmd.exe; -E - windows authentification, -U, -P - SQL Server authentification.
  var UserHasCreateDBPermission  ;Indicates wheter the used Sql Server user has the necessary permission to create a db.
  var UserHasAlterDBPermission   ;Indicates wheter the used Sql Server user has the necessary permission to alter a db.
  var SqlcmdPath                 ;The path where sqlcmd.exe is located.
  var TargetDbName               ;The name of the target database.
  var TargetDbNameQuoted         ;The name of the target database, quoted.
  var StartMenuFolder
  var SkipDbInstall              ;Indicates whether to skip installing the database; 0 - install (don't skip); 1 - don't install (skip).
  var InstallDbMode              ;Indicates whether to create or upgrade the database; 0 - create, 1 - upgrade.
  var SkipDbBackup               ;Indicates whether to backup the database before installation.
  var SQLServerAddr              ;Local database server address.
  var SQLServerAuthType          ;Local db server authentication type; 0 for Windows Authentication, 1 for SQL Server Authentication.
  var SQLServerUsername          ;The username to use for connection to the local db server during installation.
  var SQLServerPassword          ;The password to use for connection to the local db server during installation.
  var UseWindowsAuthForLocalDb   ;Use windows authentification for local database on the Application Settings dialog.
  var UseWindowsAuthForCentralDb ;Use windows authentification for central database on the Application Settings dialog.
  var UseWindowsAuthForLocalDbSync    ;Use windows authentification for local database synchronization.
  var UseWindowsAuthForCentralDbSync  ;Use windows authentification for central database synchronization.
  var CentralSQLServerAddr       ;Central database server address.
  var UseSSLSupportForCentralDb  ;Central database server use SSL.
  var UpdateMode                 ;Indicates whether the installer is running in update mode (1 - update mode is on, 0 - off).
                                 ;The Update mode is like the Silent mode except it uses some info provided in the command line.

  var CrtVersionUninstaller      ;Contains the path to the uninstaller of the currently installed app version. The "current" version will be uninstalled before installing the files of the new version.
  var CrtVersionInstallFolder    ;The folder where the current app version is installed (in case the app is already installed).

  var UninstalSQLServerAddr      ;Uninstal local database server address.
  var DeleteLocalDb              ;Indicates whether to delete the local database or not; 0 - don't delete the Database; otherwise delete the Database.
  var DeleteLocalData            ;Indicates whether to delete the local user data/settings or not; 0 - don't delete the user settings; otherwise delete the settings.
  var InstallSyncFramework       ;Indicates whether to install or not the SyncFramework; 0 - don't install SyncFramework; otherwise install it.
  var InstallDotNetFramework     ;Indicates whether to install or not the DotNetFramework; 0 - don't install DotNetFramework; otherwise install it.

;Page variables containing data necessary in other pages and sections
  var AutoUpdateLocation         ;The value inputted in the Auto-Update text box on the Application Settings dialog

;--------------------------------
;Pages

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_WELCOMEPAGE_TITLE_3LINES
  !insertmacro MUI_PAGE_WELCOME

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_LICENSEPAGE_CHECKBOX
  !define MUI_LICENSEPAGE_CHECKBOX_TEXT "$(AgreeLicenseCheckBoxText)"
  !insertmacro MUI_PAGE_LICENSE ${LicenseFilePath}
  
  !define MUI_PAGE_CUSTOMFUNCTION_LEAVE ComponentsPageLeaveFunction
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MUI_PAGE_COMPONENTS
  
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MULTIUSER_PAGE_INSTALLMODE
  
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MUI_PAGE_DIRECTORY
  
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "SHCTX"
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "${AppRegistryRootKey}"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "${ApplicationName}"
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  Page custom ApplicationSettingsPage ApplicationSettingsPageLeave
  Page custom DatabasesConfigPage DatabasesConfigPageLeave
  Page custom LocalDatabaseInstallationPage LocalDatabaseInstallationPageLeave
  Page custom LocalDatabaseInstallPage LocalDatabaseInstallPageLeave
  
  !define MUI_PAGE_CUSTOMFUNCTION_PRE InstFilesPre
  !insertmacro MUI_PAGE_INSTFILES

  !define MUI_FINISHPAGE_RUN $INSTDIR\${ExeName}
  ;!define MUI_FINISHPAGE_RUN_NOTCHECKED
  !define MUI_FINISHPAGE_SHOWREADME
  !define MUI_FINISHPAGE_SHOWREADME_TEXT "$(ShowReleaseNotes)"
  !define MUI_FINISHPAGE_SHOWREADME_FUNCTION ShowReleaseNotes
  ;!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_WELCOME
  UninstPage custom un.UninstallSettingsPage un.UninstallSettingsPageLeave
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

  !define MUI_LANGDLL_ALLLANGUAGES
  !include PCMInstallerLanguage_en.nsh
  !include PCMInstallerLanguage_de.nsh

;--------------------------------
;Reserve Files

  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  !insertmacro MUI_RESERVEFILE_LANGDLL
  ReserveFile "${NSISDIR}\Plugins\*.dll"

;-----------------------------------
;Installer Sections

;The .NET Framework 4.0 section; must appear before the section that deploys the database (DbInstallSection).
Section "$(COMPONENTS_DotNet)" DotNetFrameworkSection

  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\dotNetFx40_Full_setup.exe"
  SetOutPath "$INSTDIR"

  DetailPrint "$(InstallingDotNet)"
  ExecWait "$PLUGINSDIR\dotNetFx40_Full_setup.exe /passive /norestart" $R0
  ${If} $R0 != 0
  ${AndIf} $R0 != 1641
  ${AndIf} $R0 != 3010
    MessageBox MB_OK|MB_ICONSTOP "$(DotNetInstallFailed)"
  ${EndIf}

SectionEnd

;The Sync Framework section
  Var SyncSetup
  Var DatabaseProvidersSetup

Section "$(COMPONENTS_SyncFramework)" SyncFrameworkSection

  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\SyncFrameworkRedist\*"
  SetOutPath "$INSTDIR"

  DetailPrint "$(InstallingSyncFramework)"

  ${If} ${RunningX64}
    StrCpy $SyncSetup '"msiexec" /i "$PLUGINSDIR\Synchronization-v2.1-x64-ENU.msi" /passive /norestart'
    StrCpy $DatabaseProvidersSetup '"msiexec" /i "$PLUGINSDIR\DatabaseProviders-v3.1-x64-ENU.msi" /passive /norestart'
  ${Else}
    StrCpy $SyncSetup '"msiexec" /i "$PLUGINSDIR\Synchronization-v2.1-x86-ENU.msi" /passive /norestart'
    StrCpy $DatabaseProvidersSetup '"msiexec" /i "$PLUGINSDIR\DatabaseProviders-v3.1-x86-ENU.msi" /passive /norestart'
  ${EndIf}

  ExecWait $SyncSetup
  ExecWait $DatabaseProvidersSetup

SectionEnd

;Hidden section that installs the database before copying the files in order to be able to abort the installation if the database deployment fails.
Section "-Database" DbInstallSection

  ;Stop the app if it's running
  nsExec::Exec "taskkill /f /im ${ExeName}"
  Sleep 500  ;sleep necessary because sometimes the app dlls are not unlocked at the exact moment the Exec call returns

  ;The user chose to skip db installation
  strcmp $SkipDbInstall "1" SkipDbInstall

  ;$R4 holds the message displayed during db install/upgrade
  ${if} $InstallDbMode == "0"
    StrCpy $R4 "$(DbInstall_CreatingDb)"
  ${else}
    StrCpy $R4 "$(DbInstall_UpgradingDb)"
  ${endif}
    
  DetailPrint $R4

  ;Prepare the connection params
  StrCpy $R0 ""
  StrCpy $R1 ""
  ${If} $SQLServerAuthType == "1"
    StrCpy $R0 '$SQLServerUsername'
    StrCpy $R1 '$SQLServerPassword'
  ${EndIf}

  ;Extract the database install app
  SetOutPath "$PLUGINSDIR"
  File /r "${InputDir}\LocalDbPackage\"

  ;Install the database
  StrCpy $R2 "$TEMP\InstallDb.log"
  DetailPrint $R4

  ;If the O.S. is Windows XP add a key to the registry to make SqlPackage.exe to support this operating system.
  ${If} ${IsWinXP}
    nsExec::Exec 'REGEDIT /S $PLUGINSDIR\DeploymentUtility\Reg\crypt.reg'
  ${EndIf}

  ;Create a temp bat file that will invoke the database deployment app. This is necessary in order to capture the console output.
  FileOpen $9 "publish_db.bat" w
  FileWrite $9 "@echo off$\r$\n"
  FileWrite $9 'DeploymentUtility\SqlPackage.exe /Action:Publish /SourceFile:"Database\ZPKTool.Database.dacpac" /TargetServerName:"$SQLServerAddr" /TargetDatabaseName:"$TargetDbName" /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True /p:BlockOnPossibleDataLoss=False'
  ${If} $SQLServerAuthType == "1"
    FileWrite $9 ' /TargetUser:$R0 /TargetPassword:$R1'
  ${EndIf}
  ${If} $InstallDbMode == "0"
    FileWrite $9 ' /p:CreateNewDatabase=True'
  ${Else}
    FileWrite $9 ' /p:CreateNewDatabase=False'
    ${If} $SkipDbBackup == "0"
      FileWrite $9 ' /p:BackupDatabaseBeforeChanges=True'
    ${Else}
      FileWrite $9 ' /p:BackupDatabaseBeforeChanges=False'
    ${EndIf}
  ${EndIf}
  FileClose $9
  
  ;Execute the temp bat file and write the output in the log file.
  nsExec::Exec 'publish_db.bat 1>$R2 2>&1'
  Pop $R3
  
  ;Write the db installer exit code in the log file.
  FileOpen $9 "$R2" a
  FileSeek $9 0 END
  FileWrite $9 "$\r$\nDeployment Utility exit code: $R3"
  FileClose $9

  ;Copy the database deployment log file to the app data dir and remove it from the temp folder.
  StrCpy $0 "${AppDataFolderPath}\InstallDb.log"
  CopyFiles /SILENT $R2 $0
  Delete $R2
        
  ;Show error message if an error occurred
  ${If} $R3 != 0
    MessageBox MB_YESNO|MB_DEFBUTTON2|MB_ICONSTOP "$(DbInstall_InstallFailed) ($0).\
        $\r$\n$\r$\n$(DbInstall_InstallClosed)" IDNO QuitInst
    ;Open the log file
    ExecShell "open" "$0"
    QuitInst:
      SetErrorLevel 10
      Quit
  ${EndIf}

  ;Extract and execute the DataEncryption app depending on the authentication type.
  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\Utilities\DataEncryption.exe"

  ${If} $SQLServerAuthType == "1"
        nsExec::Exec '"DataEncryption.exe" /server="$SQLServerAddr" /database="$TargetDbName" /username="$SQLServerUsername" /password="$SQLServerPassword"'
    ${Else}
        nsExec::Exec '"DataEncryption.exe" /server="$SQLServerAddr" /database="$TargetDbName"'
  ${EndIf}

    ;Extract and execute the DocumentsCompression app depending on the authentication type.
  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\Utilities\DocumentsCompression.exe"

  ${If} $SQLServerAuthType == "1"
        nsExec::Exec '"DocumentsCompression.exe" /server="$SQLServerAddr" /database="$TargetDbName" /username="$SQLServerUsername" /password="$SQLServerPassword"'
    ${Else}
        nsExec::Exec '"DocumentsCompression.exe" /server="$SQLServerAddr" /database="$TargetDbName"'
  ${EndIf}
  
  ${If} $InstallDbMode == "1"
  ${AndIf} $SkipDbBackup == "0"
  ${AndIf} $UpdateMode == "0"
    ;Create a temp sql file to determine the full path of the database backup.
    FileOpen $9 "backup_info.sql" w
    FileWrite $9 ":EXIT(SELECT TOP 1 physical_device_name + CHAR(13) FROM msdb..backupset bckset INNER JOIN msdb..backupmediafamily bckfmly ON bckset.media_set_id = bckfmly.media_set_id WHERE database_name = '$TargetDbName' ORDER BY backup_finish_date DESC)"
    FileClose $9

    nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -i backup_info.sql -o backup_info'

    ;Read the full path of the backup from the created file, the result is on the 3rd row.
    FileOpen $9 "backup_info" r
    FileRead $9 $R5
    FileRead $9 $R5
    FileRead $9 $R5
    FileClose $9
    
    ;Delete the carriage return from the string with the full path of the backup.
    ${WordReplace} $R5 '$\r' '' '+' $R5

    ${If} $R5 != "$\r$\n"
      ;Surround the full path of the backup with inverted commas because it will be used in the command shell.
      StrCpy $R5 '"$R5"'

      ;If the backup dir does not exists in the application folder path create it.
      ${If} ${FileExists} '${AppDataFolderPath}\Backup\*.*'
      ${Else}
        SetOutPath ${AppDataFolderPath}\Backup
      ${EndIf}

      StrCpy $R6 '"${AppDataFolderPath}\Backup\"'

      ;Create a temp sql file to move de db backup file from the default path to the application data folder.
      SetOutPath $PLUGINSDIR
      FileOpen $9 "move_backup.sql" w
      FileWrite $9 "EXEC sp_configure 'show advanced options', 1$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "EXEC sp_configure 'xp_cmdshell', 1$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "GO$\r$\n"
      FileWrite $9 "EXEC master..xp_cmdshell 'MOVE $R5 $R6'$\r$\n"
      FileWrite $9 "GO$\r$\n"
      FileWrite $9 "EXEC sp_configure 'xp_cmdshell', 0$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "EXEC sp_configure 'show advanced options', 0$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "GO"
      FileClose $9

      nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -i move_backup.sql -o NUL'

      ;Get the file name of the backup from the full backup path.
      ${GetFileName} $R5 $R5

      ;Delete the leftover '"' characters.
      ${WordReplace} $R5 '"' '' '+' $R5
      ${WordReplace} $R6 '"' '' '+' $R6

      ${If} ${FileExists} '$R6$R5'
        MessageBox MB_OK|MB_ICONINFORMATION '$(DbInstall_BackupCreated) ("$R6$R5").'
      ${EndIf}
      
    ${EndIf}
  ${EndIf}

  SkipDbInstall:
    #Set the output path to the destination dir so the plugins dir can be deleted at the end
    SetOutPath "$INSTDIR"

SectionEnd
;-----------------------------------

;The Main section
Section "$(COMPONENTS_Main)" MainSection

  SectionIn RO

  ;Uninstall the currently installed app version, if it is the case
  ${If} $CrtVersionUninstaller != ""
    ;Before uninstalling the current version, copy the global settings file into the plugins dir so the settings can
    ;be copied back into the (new) installation dir and not be lost.
    CreateDirectory $PLUGINSDIR
    CopyFiles /SILENT $CrtVersionInstallFolder\GlobalSettings.xml $PLUGINSDIR\GlobalSettings.xml

    ;Invoke the uninstaller silently
    ClearErrors
    ExecWait '"$CrtVersionUninstaller" /S _?=$CrtVersionInstallFolder'
    ${IfNot} ${Errors}
      ;Manually delete the install folder and uninstaller .exe (the _? param specifies that it should be executed from the source folder and not copy itself in TEMP so it can delete the source folder).
      Delete "$CrtVersionUninstaller"
      RMDir "$CrtVersionInstallFolder"
    ${EndIf}
  ${EndIf}

  ;Rename the app local data folder from PROimpens to PCM, if necessary, and also the license file
  ${If} ${FileExists} ${OldAppDataFolderPath}
  ${AndIfNot} ${FileExists} ${AppDataFolderPath}
    Rename ${OldAppDataFolderPath} ${AppDataFolderPath}
    Rename "${AppDataFolderPath}\PROimpensLicense.lic" "${AppDataFolderPath}\License.lic"
  ${EndIf}
    
  ;If something failed during the rename above, create the app data folder because data is written in it during the installation.
  ${IfNot} ${FileExists} ${AppDataFolderPath}
    CreateDirectory ${AppDataFolderPath}
  ${EndIf}

  ;Extract the configuration files
  SetOutPath "$PLUGINSDIR"
  File /r "${InputDir}\ConfigFiles\"

  ;If does not exists, copy the advanced search configurations file to the app data folder.
  ${IfNot} ${FileExists} ${AppDataFolderPath}\AdvancedSearchConfigurations.xml
    CopyFiles /SILENT "$PLUGINSDIR\AdvancedSearchConfigurations.xml" "${AppDataFolderPath}\AdvancedSearchConfigurations.xml"
  ${EndIf}

  SetOutPath "$INSTDIR"
  !insertmacro UNINSTALL.LOG_OPEN_INSTALL
    ;Extract the application files in the install destination folder
    File /r "${InputDir}\App\"

    ;Copy the global settings file into the install folder. If there was no current version to uninstall the settings file should not exist in the plugins dir, so nothing should happen.
    CopyFiles /SILENT $PLUGINSDIR\GlobalSettings.xml $INSTDIR\GlobalSettings.xml
  !insertmacro UNINSTALL.LOG_CLOSE_INSTALL
  
  ;Compute the installed size in $R9 so it can be written in the uninstall registry information
  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $R9 "0x%08X" $0
  
  ;Store in registry installation folder, installation mode and app version
  WriteRegStr SHCTX "${AppRegistryRootKey}" "InstallFolder" "$INSTDIR"
  WriteRegStr SHCTX "${AppRegistryRootKey}" "InstallMode" "$MultiUser.InstallMode"
  WriteRegStr SHCTX "${AppRegistryRootKey}" "Version" "${VERSION}"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    ;Create shortcuts (remove any that already exist)
    ${If} $StartMenuFolder != ""
      RMDir /r "$SMPROGRAMS\$StartMenuFolder"
    ${EndIf}
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${LnkName}" "$INSTDIR\${ExeName}"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${UninstallLnkName}" "${UNINST_EXE}"
  !insertmacro MUI_STARTMENU_WRITE_END

  ;Add uninstall information (for Control Panel -> Programs or Add/Remove Programs
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayName" "${ApplicationName}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayIcon" "$INSTDIR\${ExeName}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "InstallLocation" "$INSTDIR"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "UninstallString" "${UNINST_EXE}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "QuietUninstallString" "$\"${UNINST_EXE}$\" /S"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayVersion" "${VERSION}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "Publisher" "${Company}"
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "EstimatedSize" "$R9"
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "NoModify" 1
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "NoRepair" 1

SectionEnd

;The Desktop Icon section
Section "$(COMPONENTS_DesktopIcon)" DesktopIconSection

  ;Create the desktop icon
  DetailPrint "$(CreatingDesktopShortcut)"
  CreateShortCut "$DESKTOP\${LnkName}" "$INSTDIR\${ExeName}"
  
SectionEnd

;The section that allows to select the file extensions to associate with the app
SectionGroup "$(COMPONENTS_FileTypeAssoc)" FileTypesAssociationsSection

  !insertmacro CreateFileAssociations

SectionGroupEnd

;This hidden section creates the app "profile" to which the file extensions selected above are linked, thus completing the file association process.
;It must always folow the 'FileTypesAssociationsSection' section.
Section "-RegisterFileExtensions" RegisterFileExtensionsSection

  !insertmacro CreateFileAssociationProfile

  ;Refresh the shell so the new file association icon(s) are displayed right away.
  !insertmacro RefreshShell
  
SectionEnd

;Hidden section that writes some settings in the app's settings file at the end of the installation process.
;This section should usually be last to make sure all setting related changes are written in the settings file.
Section "-Write settings" WriteSettingsSection

  DetailPrint "$(WritingSettings)"

  StrCmp $UpdateMode "1" WriteAppCfg

  ;Write some of the data inputted during the installation into the application's settings file.
  ;If the file does not exist, create it; if necessary parts of it do not exist, create them.
  nsisXML::create
  nsisXML::load "${GlobalSettingsFile}"
  ${if} $0 == 0
    ;Failed to load the app settings file; the file does not exist or is invalid, either way we have to create a new settings file.
    nsisXML::create
    nsisXML::createProcessingInstruction "xml" 'version="1.0" encoding="utf-8"'
    nsisXML::appendChild
  ${endif}

  ;Save the current xml element in R1 before all operations that change $1
  StrCpy $R1 $1

  ;Select the SettingsFile root element. Create it if it does not exist.
  nsisXML::select '/SettingsFile'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the document element
    nsisXML::createElement "SettingsFile" ;create root element node (SettingsFile)
    nsisXML::appendChild                  ;add it to the document
    StrCpy $R1 $2                         ;move to the root element
  ${else}
    StrCpy $R1 $1    ; if the element exists, move to it
  ${endif}

  ;Select the Settings element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the SettingsFile element
    nsisXML::createElement "Settings"     ;create the Settings element
    nsisXML::appendChild                  ;add it to the SettingsFile element
    StrCpy $R1 $2                         ;move to the Settings element
  ${else}
    StrCpy $R1 $1    ; if the element exists, move to it
  ${endif}

  ;Select the CentralConnectionDataSource Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="CentralConnectionDataSource"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "CentralConnectionDataSource"
    nsisXML::setAttribute "Type" "System.String"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  nsisXML::setText "$CentralSQLServerAddr"

  ;Select the UseSSLSupportForCentralDb Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseSSLSupportForCentralDb"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "UseSSLSupportForCentralDb"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  ${if} "$UseSSLSupportForCentralDb" == "1"
    nsisXML::setText "True"
  ${else}
    nsisXML::setText "False"
  ${endif}

  ;Select the LocalConnectionDataSource Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="LocalConnectionDataSource"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "LocalConnectionDataSource"
    nsisXML::setAttribute "Type" "System.String"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  nsisXML::setText "$SQLServerAddr"

  ;Select the AutomaticUpdatePath Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="AutomaticUpdatePath"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "AutomaticUpdatePath"
    nsisXML::setAttribute "Type" "System.String"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}
  
   nsisXML::setText "$AutoUpdateLocation"
  
  ;Select the UseWindowsAuthForLocalDb Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForLocalDb"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "UseWindowsAuthForLocalDb"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}
  
   ${if} "$UseWindowsAuthForLocalDb" == "1"
    nsisXML::setText "True"
  ${else}
    nsisXML::setText "False"
  ${endif}
  
  ;Select the UseWindowsAuthForCentralDb Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForCentralDb"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "UseWindowsAuthForCentralDb"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  ${if} "$UseWindowsAuthForCentralDb" == "1"
    nsisXML::setText "True"
  ${else}
    nsisXML::setText "False"
  ${endif}
  
  ;Select the UseWindowsAuthForLocalDbSync Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForLocalDbSync"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "UseWindowsAuthForLocalDbSync"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  ${if} "$UseWindowsAuthForLocalDbSync" == "1"
    nsisXML::setText "True"
  ${else}
    nsisXML::setText "False"
  ${endif}

  ;Select the UseWindowsAuthForCentralDbSync Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForCentralDbSync"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "UseWindowsAuthForCentralDbSync"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}

  ${if} "$UseWindowsAuthForCentralDbSync" == "1"
    nsisXML::setText "True"
  ${else}
    nsisXML::setText "False"
  ${endif}
  
  ;Log the creation of the global settings file, if it occurs
  SetOutPath "$INSTDIR"
  !insertmacro UNINSTALL.LOG_OPEN_INSTALL
    nsisXML::save "${GlobalSettingsFile}"
  !insertmacro UNINSTALL.LOG_CLOSE_INSTALL

WriteAppCfg:

  ;Done with the app settings.
  ;Now write some data in the exe's config file (not app settings). This is done also in the update mode.
  nsisXML::create
  nsisXML::load "${ExeConfigPath}"
  ${if} $0 != 0
    ;Write the log file path
    nsisXML::select '/configuration/nlog/targets/target[@name="file"]'
    nsisXML::setAttribute "fileName" "$${specialfolder:folder=LocalApplicationData}\PCMData\App.log"
    nsisXML::setAttribute "archiveFileName" "$${specialfolder:folder=LocalApplicationData}\PCMData\App.{##}.log"

    nsisXML::save "${ExeConfigPath}"
  ${endif}

SectionEnd

;Hidden section that creates the log containing the installed files.
;THIS SECTION SHOULD ALWAYS BE LAST (in order to capture all the file related operations).
Section "-WriteFilesLog"

  SectionIn RO
  !insertmacro UNINSTALL.LOG_UPDATE_INSTALL

SectionEnd

;--------------------------------
;Section Descriptions

  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${MainSection} $(DESC_MainSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${DotNetFrameworkSection} $(DESC_dotNetSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${SyncFrameworkSection} $(DESC_SyncFrameworkSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${DesktopIconSection} $(DESC_DesktopIconSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${FileTypesAssociationsSection} $(DESC_FileTypeAssoc)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Installer Functions

Function ComponentsPageLeaveFunction

  ;If the user does not have admin privileges and .NET Framework is not installed show a warning to tell him that .NET Framework needs to be installed in order to run the application.
  ${DetectDotNetFramework} $R1
  SectionGetFlags ${DotNetFrameworkSection} $R2

  ${If} $MultiUser.Privileges == Admin
  ${OrIf} $MultiUser.Privileges == Power
    IntOp $R3 $R2 & ${SF_SELECTED}
    ${If} $R3 != ${SF_SELECTED}             ;The ".NET Framework 4.0" component is not selected
      ${If} $R1 == ${DotNet_None}
        MessageBox MB_ICONSTOP "$(DotNetNotInstalled_HasAdminPermission)"
        Abort
      ${ElseIf} $R1 == ${DotNet_v4_Client}
        MessageBox MB_ICONSTOP "$(DotNet4FullNotInstalled_HasAdminPermission)"
        Abort
      ${EndIf}
    ${EndIf}
  ${Else}
    ;The user does not have Admin privileges -> .NET can not be installed (he ".NET Framework 4.0" component is disabled and un-selected)
    ${If} $R1 == ${DotNet_None}
      MessageBox MB_ICONSTOP "$(DotNetNotInstalled_DoesntHaveAdminPermission)"
      Abort
    ${ElseIf} $R1 == ${DotNet_v4_Client}
      MessageBox MB_ICONSTOP "$(DotNet4FullNotInstalled_DoesntHaveAdminPermission)"
      Abort
    ${EndIf}
  ${EndIf}

  ;If the user wants to install Sync Framework 2.1 and an older version is present show a warning.
  SectionGetFlags ${SyncFrameworkSection} $0
  IntOp $0 $0 & ${SF_SELECTED}
  ReadRegStr $R1 HKLM "Software\Microsoft\Microsoft Sync Framework\MSIRefCount" "Uninstall"
  ReadRegStr $R2 HKLM "Software\Microsoft\Microsoft Sync Framework\v2.0\RefCount\Synchronizationx86\1033" "Uninstall"

  ${If} $0 = ${SF_SELECTED}
  ${AndIf} $R1 != ''
    MessageBox MB_ICONEXCLAMATION $(OlderSyncFrameworkInstalled)
  ${EndIf}

  ${If} $0 = ${SF_SELECTED}
  ${AndIf} $R2 != ''
    MessageBox MB_ICONEXCLAMATION $(OlderSyncFrameworkInstalled)
  ${EndIf}

FunctionEnd

Function .onInit

  !insertmacro MULTIUSER_INIT

  ;SqlPackage.exe, the application that deploys the database, does not officially support Windows XP, so we need to add a key
  ;to the registry for it to work. This operation requires admin privileges => stop the installer if Admin privileges are not available.
  ${If} ${IsWinXP}
  ${AndIf} $MultiUser.Privileges != Admin
  ${AndIf} $MultiUser.Privileges != Power
    MessageBox MB_ICONSTOP "${MULTIUSER_INIT_TEXT_ADMINREQUIRED}" /SD IDOK
    Quit
  ${EndIf}

  ;Initialize some variables
  strcpy $UserHasCreateDBPermission "0"
  strcpy $UserHasAlterDBPermission "0"
  strcpy $TargetDbName "ZPKTool"
  strcpy $TargetDbNameQuoted "'$TargetDbName'"
  strcpy $SkipDbInstall "0"
  strcpy $InstallDbMode "0"
  strcpy $SkipDbBackup "0"

  call DetectSqlcmdPath
  strcpy $SqlcmdPath $0
  ${if} $SqlcmdPath == ""
    MessageBox MB_OK|MB_ICONSTOP "$(SqlcmdNotFound)"
    SetErrorLevel 8
    Abort
  ${endif}

  ;Extract the installer helper app
  SetOutPath $PLUGINSDIR\InstallerHelper
  File "${InputDir}\Installers\InstallerHelper\*"
  
  ${If} ${RunningX64}
    ;set the registry view early so the same registry set is accessed on all flows
    SetRegView 64
  ${EndIf}

  ${GetParameters} $R0
  ClearErrors

  ;check if the update mode is turned on
  ${GetOptions} $R0 /UPD $0
  IfErrors UpdateModeOff
   
  ;get the dir where to install the update
  ${GetOptions} $R0 /DIR= $0
  ${if} $0 == ""
    SetErrorLevel 3
    goto AbortUpdate
  ${else}
    StrCpy $INSTDIR $0
  ${endif}

  ;get the central server address (for the purpose of the update)
  ${GetOptions} $R0 /CS= $CentralSQLServerAddr

  ;get the local server address (for the purpose of the update)
  ${GetOptions} $R0 /LS= $SQLServerAddr

  ;get the username for logging into the local server during the update
  ${GetOptions} $R0 /LSUSR= $SQLServerUsername
   
  ;get the password for logging into the local server during the update
  ${GetOptions} $R0 /LSPASS= $SQLServerPassword
   
  ;validate the cmd line params read above and abort the update if one of them is not set
  ${if} $CentralSQLServerAddr == ""
    SetErrorLevel 4
    goto AbortUpdate
  ${endif}

  ${if} $SQLServerAddr == ""
    SetErrorLevel 5
    goto AbortUpdate
  ${endif}
   
  ${if} $SQLServerUsername == ""
    SetErrorLevel 6
    goto AbortUpdate
  ${endif}
   
  ${if} $SQLServerPassword == ""
    SetErrorLevel 7
    goto AbortUpdate
  ${endif}

  StrCpy $SQLServerAuthType "0" ;in update mode, the local server authentication mode is Windows Authentication because the PROimpensUser does not have enough rights to create another user
  StrCpy $SkipDbInstall '0'     ;install the database
  StrCpy $InstallDbMode '1'     ;upgrade the database
  StrCpy $UpdateMode "1"
  
  ;Select/unselect sections to preserve the options made by the user on the previous installation.
  Call UpdateModeInitializeSections
  
  ;Manually load the language from registry when in update mode. Don't use MUI_LANGDLL_DISPLAY to avoid displaying the language dialog.
  ReadRegStr $0 "${MUI_LANGDLL_REGISTRY_ROOT}" "${MUI_LANGDLL_REGISTRY_KEY}" "${MUI_LANGDLL_REGISTRY_VALUENAME}"
  ${If} $0 != ""
    StrCpy $LANGUAGE $0
  ${EndIf}

  Goto Continue

  AbortUpdate:
    Abort

  UpdateModeOff:
  
    ${If} ${Silent}
      Call InitSilentInstallation
      Goto Continue
    ${EndIf}
    
    ; else continue with the normal installer flow
    !insertmacro MUI_LANGDLL_DISPLAY
    StrCpy $UpdateMode "0"
    StrCpy $SQLServerAuthType "0"
    Call ReadAppSettings
    
    ;Disable and deselect .NET and Sync Framework sections if the user does not have admin privileges
    ;because their installation will not succeed.
    ${If} $MultiUser.Privileges != Admin
    ${AndIf} $MultiUser.Privileges != Power
      IntOp $0 0 | ${SF_RO}
      SectionSetFlags ${dotNetFrameworkSection} $0   ;.NET framework section
      SectionSetFlags ${SyncFrameworkSection} $0     ;Sync framework section
    ${Else}
      ;If Sync Framework 2.1 or .Net4 are already installed deselect them.
      ReadRegStr $R1 HKLM "Software\Microsoft\Microsoft Sync Framework\v2.1\RefCount\Synchronizationx64\1033" "Uninstall"
      ${If} $R1 != ''
        !insertmacro UnselectSection ${SyncFrameworkSection}
      ${EndIf}
	  
      ReadRegStr $R1 HKLM "Software\Microsoft\Microsoft Sync Framework\v2.1\RefCount\Synchronizationx86\1033" "Uninstall"
      ${If} $R1 != ''
        !insertmacro UnselectSection ${SyncFrameworkSection}
      ${EndIf}

      ${DetectDotNetFramework} $R1
      ${If} $R1 == ${DotNet_v4_Full}
        !insertmacro UnselectSection ${DotNetFrameworkSection}
      ${EndIf}
    ${EndIf}

    Goto Continue

  Continue:
    !insertmacro UNINSTALL.LOG_PREPARE_INSTALL

FunctionEnd

Function .onInstSuccess

  ;Start the app after a successful update
  ${If} $UpdateMode == 1
      Exec "$INSTDIR\${ExeName}"
  ${EndIf}

FunctionEnd

;Initializes the sections during the update mode
Function UpdateModeInitializeSections

  ;Deselect some sections
  !insertmacro UnselectSection ${DotNetFrameworkSection}
  !insertmacro UnselectSection ${SyncFrameworkSection}
  !insertmacro UnselectSection ${DesktopIconSection}
  !insertmacro UnselectSection ${RegisterFileExtensionsSection}
  !insertmacro UnselectSection ${FileTypesAssociationsSection}

  ;The uninstaller of the current installed version is executed before installing the new version. This causes the desktop shortcut
  ;and file associations to be removed. Here we determine if a desktop shortcut was created and which file associations were created
  ;during the normal installation and activate their corresponding sections so they will be created again.
  ${If} ${FileExists} "$DESKTOP\${LnkName}"
    !insertmacro SelectSection ${DesktopIconSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.project" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocProjectSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.assembly" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocAssemblySec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.part" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocPartSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.rawpart" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocRawPartSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

FunctionEnd

Function InitSilentInstallation
  ;read the current app settings (if the app is installed) because some of them will be left untouched in silent mode
  Call ReadAppSettings

  ;Read from the cmdline the configuration parameters supported and copy the values in the appropriate variables
  ${GetParameters} $R0
  ClearErrors

  ${GetOptions} $R0 /CentralServerName= $CentralSQLServerAddr
  ${If} $CentralSQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(CentralServerNameEmpty)"
    Abort
  ${EndIf}
  
  ${GetOptions} $R0 /UseSSLForCentralDb= $UseSSLSupportForCentralDb
  ${If} $UseSSLSupportForCentralDb == ""
    StrCpy $UseSSLSupportForCentralDb "0"
  ${EndIf}

  ${GetOptions} $R0 /ServerName= $SQLServerAddr
  ${If} $SQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(ServerNameEmpty)"
    Abort
  ${EndIf}

  ${GetOptions} $R0 /ServerAuthType= $SQLServerAuthType
  ${If} $SQLServerAuthType == ""
    StrCpy $SQLServerAuthType 0
  ${EndIf}

  ${GetOptions} $R0 /ServerUsername= $SQLServerUsername
  ${If} $SQLServerAuthType == 1
  ${AndIf} $SQLServerUsername == ""
    MessageBox MB_OK|MB_ICONSTOP "$(UserNameEmpty)"
    Abort
  ${EndIf}

  ${GetOptions} $R0 /ServerPassword= $SQLServerPassword
  ${If} $SQLServerAuthType == 1
  ${AndIf} $SQLServerPassword == ""
    MessageBox MB_OK|MB_ICONSTOP "$(PasswordEmpty)"
    Abort
  ${EndIf}
  
  ${GetOptions} $R0 /UseWindowsAuthForLocalDb= $UseWindowsAuthForLocalDb
  ${If} $UseWindowsAuthForLocalDb == ""
    StrCpy $UseWindowsAuthForLocalDb "0"
  ${EndIf}
  
  ${GetOptions} $R0 /UseWindowsAuthForCentralDb= $UseWindowsAuthForCentralDb
  ${If} $UseWindowsAuthForCentralDb == ""
    StrCpy $UseWindowsAuthForCentralDb "0"
  ${EndIf}
  
  ${GetOptions} $R0 /UseWindowsAuthForLocalDbSync= $UseWindowsAuthForLocalDbSync
  ${If} $UseWindowsAuthForLocalDbSync == ""
    StrCpy $UseWindowsAuthForLocalDbSync "0"
  ${EndIf}

  ${GetOptions} $R0 /UseWindowsAuthForCentralDbSync= $UseWindowsAuthForCentralDbSync
  ${If} $UseWindowsAuthForCentralDbSync == ""
    StrCpy $UseWindowsAuthForCentralDbSync "0"
  ${EndIf}
  
  ${GetOptions} $R0 /InstallDbMode= $InstallDbMode
  ${If} $InstallDbMode == ""
    StrCpy $InstallDbMode 1
  ${ElseIf} $InstallDbMode == "2"
    ;skip db install
    StrCpy $InstallDbMode ""
    StrCpy $SkipDbInstall "1"
  ${EndIf}

  ${GetOptions} $R0 /SkipDbBackup= $SkipDbBackup
  ${If} $SkipDbBackup == ""
    StrCpy $SkipDbBackup 0
  ${EndIf}
   
  ${GetOptions} $R0 /DIR= $0
  ${If} $0 != ""
    StrCpy $INSTDIR $0
  ${EndIf}

  ${GetOptions} $R0 /AutoUpdateLocation= $0
  ${If} $0 != ""
    StrCpy $AutoUpdateLocation $0
  ${EndIf}
  
  ;unselect all optional sections/components
  ${GetOptions} $R0 /InstallDotNetFramework= $InstallDotNetFramework
  ${If} $InstallDotNetFramework = 0
    !insertmacro UnselectSection ${DotNetFrameworkSection}
  ${EndIf}
  
  ${GetOptions} $R0 /InstallSyncFramework= $InstallSyncFramework
  ${If} $InstallSyncFramework = 0
    !insertmacro UnselectSection ${SyncFrameworkSection}
  ${EndIf}

  !insertmacro UnselectSection ${DesktopIconSection}
  !insertmacro UnselectSection ${FileTypesAssociationsSection}
  !insertmacro UnselectSection ${RegisterFileExtensionsSection}

  ;set the rest of the variables to necessary defaults
  StrCpy $UpdateMode "0"
  
  Call CheckIfAlreadyInstalled

FunctionEnd

; Reads some values from the app settings file which will be used as defaults during the installation
Function ReadAppSettings

  ;Set default values before reading the setings file, in case these values could not be read from the settings
  StrCpy $CentralSQLServerAddr "localhost\sqlexpress"
  StrCpy $UseSSLSupportForCentralDb "0"
  StrCpy $SQLServerAddr "localhost\sqlexpress"
  StrCpy $UseWindowsAuthForLocalDb "0"
  StrCpy $UseWindowsAuthForCentralDb "0"
  StrCpy $UseWindowsAuthForLocalDbSync "0"
  StrCpy $UseWindowsAuthForCentralDbSync "0"
  
  ${If} ${FileExists} ${GlobalSettingsFile}
    ;The settings read below were moved to the global settings file starting with v1.3
    StrCpy $R0 ${GlobalSettingsFile}
  ${Else}
    ;In the previous versions they were kept in the user settings ($LOCALAPPDATA\PROimpens\settings.xml)
    StrCpy $R0 "${OldAppDataFolderPath}\settings.xml"
  ${EndIf}

  nsisXML::create
  nsisXML::load "$R0"
  ${if} $0 == 0
    ;Failed to load the app settings file (probably does not exist)
    goto End
  ${endif}

  ;Read the central db server location from the settings file
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="CentralConnectionDataSource"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    strcpy $CentralSQLServerAddr $3
  ${endif}

  ;Read the central db server use SSL from the settings file
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseSSLSupportForCentralDb"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    ${if} $3 == 'True'
      strcpy $UseSSLSupportForCentralDb 1
    ${endif}
  ${endif}
    
  ;Read the local db server location from the settings file
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="LocalConnectionDataSource"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    strcpy $SQLServerAddr $3
  ${endif}
    
  ;Read the Auto-Update Path from the settings file
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="AutomaticUpdatePath"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    strcpy $AutoUpdateLocation $3
  ${endif}
  
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForLocalDb"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    ${if} $3 == 'True'
      strcpy $UseWindowsAuthForLocalDb 1
    ${endif}
  ${endif}
  
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForCentralDb"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    ${if} $3 == 'True'
      strcpy $UseWindowsAuthForCentralDb 1
    ${endif}
  ${endif}
  
   nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForLocalDbSync"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    ${if} $3 == 'True'
      strcpy $UseWindowsAuthForLocalDbSync 1
    ${endif}
  ${endif}

  nsisXML::select '/SettingsFile/Settings/Setting[@Name="UseWindowsAuthForCentralDbSync"]/Value'
  ${if} $2 != 0
    nsisXML::getText
    ${if} $3 == 'True'
      strcpy $UseWindowsAuthForCentralDbSync 1
    ${endif}
  ${endif}

  End:

FunctionEnd

;Checks if the app is already installed and asks for confirmation
Function CheckIfAlreadyInstalled

  ;In auto-update check if the user has enough privileges to uninstall the app and install it back. This is the same check as in un.onInit
  ${If} $UpdateMode == 1
    ReadRegStr $0 SHCTX "${AppRegistryRootKey}" "InstallMode"
    ${If} $0 != CurrentUser
    ${AndIf} $MultiUser.Privileges != Admin
    ${AndIf} $MultiUser.Privileges != Power
      MessageBox MB_ICONSTOP "$(NoPrivilegesToUpdate)"
      Quit
    ${EndIf}
  ${EndIf}
  
  ;Check if the app is already installed by looking for its uninstall information stored in registry
  ReadRegStr $0 SHCTX "${UninstallRegistryRootKey}" "UninstallString"
  ReadRegStr $1 SHCTX "${UninstallRegistryRootKey}" "DisplayVersion"

  ${If} ${FileExists} "$0"
  
    ;Ask the user for confirmation for uninstalling the current version. In update mode and if silent the confirmation is not necessary.
    ${If} $UpdateMode == 0
    ${AndIfNot} ${Silent}
      ${VersionCompare} "$1" "${VERSION}" $2
      ${If} $2 == 1
        MessageBox MB_YESNO|MB_ICONSTOP "$(DowngradeConfirmation)" IDYES Continue
        Abort
      ${ElseIf} $2 == 2
        MessageBox MB_YESNO|MB_ICONQUESTION "$(UpgradeConfirmation)" IDYES Continue
        Abort
      ${Else}
        MessageBox MB_YESNO|MB_ICONQUESTION "$(ReinstallConfirmation)" IDYES Continue
        Abort
      ${EndIf}
    ${EndIf}

    Continue:

    ;Store the path to the uninstaller and the install path of the currently installed app so it can be uninstalled before installing the files of the new version
    StrCpy $CrtVersionUninstaller "$0"
    ReadRegStr $CrtVersionInstallFolder SHCTX "${UninstallRegistryRootKey}" "InstallLocation"

  ${EndIf}

FunctionEnd

Function SkipPageInUpdateMode

  ${if} $UpdateMode == "1"
    Abort
  ${endif}
    
FunctionEnd

Function InstFilesPre

  ;In update mode, dont' show this page if an error occured before it
  ${if} $UpdateMode == "1"
    GetErrorLevel $R0
    ${if} $R0 > 0
      Abort
    ${endif}
  ${endif}

FunctionEnd

Function ShowReleaseNotes

  ClearErrors
  ExecShell "open" '"$INSTDIR\ReleaseNotes.pdf"'
  ${If} ${Errors}
    ExecShell "open" '"$INSTDIR\ReleaseNotes.rtf"'
  ${EndIf}

FunctionEnd

;--- Application settings page ---
var AppSettingsDialog
var AppSettingsDialog_AutoUpdateTextbox

Function ApplicationSettingsPage

  #Don't show the page in update mode
  ${If} $UpdateMode == "1"
    Abort
  ${EndIf}

  !insertmacro MUI_HEADER_TEXT "$(AppSettingsPageTitle)" "$(AppSettingsPageSubtitle)"

  nsDialogs::Create 1018
  Pop $AppSettingsDialog
  ${If} $AppSettingsDialog == error
    Abort
  ${EndIf}
  ${NSD_CreateLabel} 0 0 140px 20u "$(AppSettingsPage_AutoUpdate)"
  Pop $0
  ${NSD_AddStyle} $0 ${SS_CENTER}

  ${NSD_CreateText} 145 0 250px 12u "$AutoUpdateLocation"
  Pop $AppSettingsDialog_AutoUpdateTextbox
  ${NSD_SetFocus} $AppSettingsDialog_AutoUpdateTextbox

  ${NSD_CreateLabel} 145 13u 100% 12u "(* http://update-server.com or \\update\folder\path)"
  Pop $0

  nsDialogs::Show

FunctionEnd

Function ApplicationSettingsPageLeave

  ${NSD_GetText} $AppSettingsDialog_AutoUpdateTextbox $AutoUpdateLocation

FunctionEnd

;--- Databases Configuration Page ---
var CentralDbConfig_ServerNameText
var LocalDbConfig_ServerNameText
var CentralDbConfig_UseSSL
var UseWindowsAuth_LocalDb
var UseWindowsAuth_CentralDb
var UseWindowsAuth_LocalDbSync
var UseWindowsAuth_CentralDbSync

Function DatabasesConfigPage

  ;Don't show the page in update mode
  ${if} $UpdateMode == "1"
    Abort
  ${endif}

  !insertmacro MUI_HEADER_TEXT "$(DatabasesConfigPageTitle)" "$(DatabasesConfigPageSubtitle)"

  nsDialogs::Create 1018
  
  ${NSD_CreateGroupBox} 0u 0u 100% 65u "$(CentralDatabaseConfiguration)"

  ${NSD_CreateLabel} 5u 12u 75u 8u "$(SqlServer)"
  ${NSD_CreateText} 85u 11u 120u 13u "$CentralSQLServerAddr"
  Pop $CentralDbConfig_ServerNameText
  ${NSD_SetFocus} $CentralDbConfig_ServerNameText

  ${NSD_CreateCheckbox} 5u 24u 90% 12u "$(WindowsAuthForCentralDb)"
  Pop $UseWindowsAuth_CentralDb

  ${if} $UseWindowsAuthForCentralDb == "1"
    ${NSD_SetState} $UseWindowsAuth_CentralDb ${BST_CHECKED}
  ${endif}

  ${NSD_CreateCheckbox} 5u 36u 90% 12u "$(WindowsAuthForCentralDbSync)"
  Pop $UseWindowsAuth_CentralDbSync

  ${if} $UseWindowsAuthForCentralDbSync == "1"
    ${NSD_SetState} $UseWindowsAuth_CentralDbSync ${BST_CHECKED}
  ${endif}

  ${NSD_CreateCheckBox} 5u 48u 90% 12u "$(UseSSL)"
  Pop $CentralDbConfig_UseSSL
  
  ${if} $UseSSLSupportForCentralDb == "1"
    ${NSD_SetState} $CentralDbConfig_UseSSL ${BST_CHECKED}
  ${endif}
  
  ${NSD_CreateGroupBox} 0u 68u 100% 54u "$(LocalDatabaseConfiguration)"
  
  ${NSD_CreateLabel} 5u 80u 75u 8u "$(SqlServer)"
  ${NSD_CreateText} 85u 79u 120u 13u "$SQLServerAddr"
  Pop $LocalDbConfig_ServerNameText

  ${NSD_CreateCheckbox} 5u 92u 90% 12u "$(WindowsAuthForLocalDb)"
  Pop $UseWindowsAuth_LocalDb

  ${if} $UseWindowsAuthForLocalDb == "1"
   ${NSD_SetState} $UseWindowsAuth_LocalDb ${BST_CHECKED}
  ${endif}

  ${NSD_CreateCheckbox} 5u 104u 90% 12u "$(WindowsAuthForLocalDbSync)"
  Pop $UseWindowsAuth_LocalDbSync

  ${if} $UseWindowsAuthForLocalDbSync == "1"
   ${NSD_SetState} $UseWindowsAuth_LocalDbSync ${BST_CHECKED}
  ${endif}

  nsDialogs::Show

FunctionEnd

; This callback is triggered when the user leaves the Databases Configuration page
Function DatabasesConfigPageLeave

  ;Validate the connection string
  ${NSD_GetText} $CentralDbConfig_ServerNameText $CentralSQLServerAddr
  ${if} $CentralSQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(ServerNameEmpty)"
    Abort
  ${endif}
  
  ${NSD_GetText} $LocalDbConfig_ServerNameText $SQLServerAddr
  ${if} $SQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(ServerNameEmpty)"
    Abort
  ${endif}

  ${NSD_GetState} $CentralDbConfig_UseSSL $0
  StrCpy $UseSSLSupportForCentralDb $0
  
  ${NSD_GetState} $UseWindowsAuth_LocalDb $0
    StrCpy $UseWindowsAuthForLocalDb $0

  ${NSD_GetState} $UseWindowsAuth_CentralDb $0
    StrCpy $UseWindowsAuthForCentralDb $0

  ${NSD_GetState} $UseWindowsAuth_LocalDbSync $0
    StrCpy $UseWindowsAuthForLocalDbSync $0

  ${NSD_GetState} $UseWindowsAuth_CentralDbSync $0
    StrCpy $UseWindowsAuthForCentralDbSync $0

FunctionEnd

;--- Local Database Installation Page ---
var LocalDbInstall_ServerNameText
var LocalDbInstall_WinAuthCheck
var LocalDbInstall_SqlAuthCheck
var LocalDbInstall_SqlAuthUsername
var LocalDbInstall_SqlAuthPassword
var LocalDbInstall_SkipDbInstallCheck

Function LocalDatabaseInstallationPage

  !insertmacro MUI_HEADER_TEXT "$(LocalDatabaseInstallationPage_Title)" "$(LocalDatabaseInstallationPage_Subtitle)"

  ;Don't show the page in update mode, just test the connection
  ${if} $UpdateMode == "1"
    call TestLocalSqlServerConnection
    Abort
  ${endif}

  nsDialogs::Create 1018

  ${NSD_CreateLabel} 0u 1u 75u 8u "$(SqlServer)"
  ${NSD_CreateText} 80u 0u 120u 13u "$SQLServerAddr"
  Pop $LocalDbInstall_ServerNameText
  EnableWindow $LocalDbInstall_ServerNameText 0
  
  ;The Authentication groupbox
  ${NSD_CreateGroupBox} 0u 15u 100% 72u "$(Authentication)"

  ;The Windows Authentication radio button
  ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(WindowsAuth)"
  Pop $LocalDbInstall_WinAuthCheck
  GetFunctionAddress $0 OnAuthRbClick
  nsDialogs::OnClick $LocalDbInstall_WinAuthCheck $0

  ;The SQL Server Authentication radio button
  ${NSD_CreateRadioButton} 5u 37u 90% 8u "$(SqlServerAuth)"
  Pop $LocalDbInstall_SqlAuthCheck
  GetFunctionAddress $0 OnAuthRbClick
  nsDialogs::OnClick $LocalDbInstall_SqlAuthCheck $0

  ;Select the appropriate authentication radio button based on the authentication method set internally
  ${if} $SQLServerAuthType == "0"
    ${NSD_Check} $LocalDbInstall_WinAuthCheck
  ${else}
    ${NSD_Check} $LocalDbInstall_SqlAuthCheck
  ${endif}
  
  ;The Username and Password inputs associated with the SQL Server Authentication radio button
  ${NSD_CreateLabel} 16u 51u 64u 8u "$(Username)"
  ${NSD_CreateText} 80u 50u 120u 13u "$SQLServerUsername"
  Pop $LocalDbInstall_SqlAuthUsername


  ${NSD_CreateLabel} 16u 66u 64u 8u "$(Password)"
  ${NSD_CreatePassword} 80u 65u 120u 13u "$SQLServerPassword"
  Pop $LocalDbInstall_SqlAuthPassword

  ;The Skip Database Installation check box
  ${NSD_CreateCheckBox} 0u 92u 100% 8u "$(SkipDatabaseInstall)"
  Pop $LocalDbInstall_SkipDbInstallCheck
  ${NSD_OnClick} $LocalDbInstall_SkipDbInstallCheck HandleSkipDbInstallClick
  ${NSD_SetState} $LocalDbInstall_SkipDbInstallCheck $SkipDbInstall

  call HandleSkipDbInstallClick
  call OnAuthRbClick

  nsDialogs::Show

FunctionEnd

;This callback is triggered when the user leaves the Local Database Installation page
Function LocalDatabaseInstallationPageLeave

  ;Read the sql server name
  ${NSD_GetText} $LocalDbInstall_ServerNameText $SQLServerAddr
  ${if} $SQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(ServerNameEmpty)"
    Abort
  ${endif}

  ;If the db installation is skipped, don't retrieve the db connection parameters and perform the connection testing
  ${NSD_GetState} $LocalDbInstall_SkipDbInstallCheck $SkipDbInstall
  ${if} $SkipDbInstall == 1
    goto End
  ${endif}

  ;Read the authentication type
  ${NSD_GetState} $LocalDbInstall_WinAuthCheck $0
  ${if} $0 == "1"
    StrCpy $SQLServerAuthType "0"
    StrCpy $SQLServerUsername ""
    StrCpy $SQLServerPassword ""
  ${endif}

  ${NSD_GetState} $LocalDbInstall_SqlAuthCheck $0
  ${if} $0 == "1"
    StrCpy $SQLServerAuthType "1"
    ${NSD_GetText} $LocalDbInstall_SqlAuthUsername $SQLServerUsername
    ${NSD_GetText} $LocalDbInstall_SqlAuthPassword $SQLServerPassword

    ${if} $SQLServerUsername == ""
      MessageBox MB_OK|MB_ICONSTOP "$(UserNameEmpty)"
      Abort
    ${elseif} $SQLServerPassword == ""
      MessageBox MB_OK|MB_ICONSTOP "$(PasswordEmpty)"
      Abort
    ${endif}
  ${endif}

  StrCpy $R0 ""
  StrCpy $R1 ""
  ${if} $SQLServerAuthType == "1"
    StrCpy $R0 '"$SQLServerUsername"'
    StrCpy $R1 '"$SQLServerPassword"'
    StrCpy $SQLAuthentification ' -U $R0 -P $R1'
  ${else}
    StrCpy $SQLAuthentification ' -E'
  ${endif}

  call TestLocalSqlServerConnection
  
  ;Check to see if the used SQL Server user has the necessary permission in order to create the application's db.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM fn_my_permissions(NULL, ""SERVER"") WHERE permission_name = ""CREATE ANY DATABASE"")" -o NUL'
  Pop $R2

  ${if} $R2 == "1"
    strcpy $UserHasCreateDBPermission "1"
  ${else}
    strcpy $UserHasCreateDBPermission "0"
  ${endif}

  ;Check to see if the used SQL Server user has the necessary permission in order to modify the application's db.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM fn_my_permissions(NULL, ""SERVER"") WHERE permission_name = ""ALTER ANY DATABASE"")" -o NUL'
  Pop $R2

  ${if} $R2 == "1"
    strcpy $UserHasAlterDBPermission "1"
  ${else}
    strcpy $UserHasAlterDBPermission "0"
  ${endif}

  ;Depending on the permissions that used SQL Server user has a specific message box is displayed.
  ${if} $UserHasCreateDBPermission == "0"
  ${andif} $UserHasAlterDBPermission == "0"
    MessageBox MB_OK|MB_ICONSTOP "$(NoCreateAlterDBPermissions)"
    Abort
  ${elseif} $UserHasCreateDBPermission == "0"
    MessageBox MB_OK|MB_ICONEXCLAMATION "$(NoCreateDBPermission)"
  ${elseif} $UserHasAlterDBPermission == "0"
    MessageBox MB_OK|MB_ICONEXCLAMATION "$(NoAlterDBPermission)"
  ${endif}
  
  End:

FunctionEnd

;Checks the connection to the local sql server
Function TestLocalSqlServerConnection

  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q "EXIT"'
  Pop $R2

  ${if} $R2 == "1"
    MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionTest_Failed)"
    SetErrorLevel 9
    Abort
  ${endif}

FunctionEnd

Function OnAuthRbClick

  ${NSD_GetState} $LocalDbInstall_WinAuthCheck $0

  ;$1 contains the param for the EnableWindow call taht will enable or disable the other page controls
  ${if} $0 == "1"
    strcpy $1 "0"
  ${else}
    strcpy $1 "1"
  ${endif}

  EnableWindow $LocalDbInstall_SqlAuthUsername $1
  EnableWindow $LocalDbInstall_SqlAuthPassword $1

FunctionEnd

Function HandleSkipDbInstallClick

  ${NSD_GetState} $LocalDbInstall_SkipDbInstallCheck $0

  ;$1 contains the param for the EnableWindow call taht will enable or disable the other page controls
  ${if} $0 == "1"
    strcpy $1 "0"
  ${else}
    strcpy $1 "1"
  ${endif}

  EnableWindow $LocalDbInstall_WinAuthCheck $1
  EnableWindow $LocalDbInstall_SqlAuthCheck $1
  EnableWindow $LocalDbInstall_SqlAuthUsername $1
  EnableWindow $LocalDbInstall_SqlAuthPassword $1

FunctionEnd

;--- Local Database Installation Page ---
var LocalDbInstallOptionsDlg
var LocalDbInstall_CreateOption
var LocalDbInstall_UpgradeOption
var LocalDbInstall_SkipBackupCheck

Function LocalDatabaseInstallPage

  ;Don't show the page in update mode
  ${if} $UpdateMode == "1"
    Abort
  ${endif}

  ;The user chose to skip the database installation so there is no point in showing this page
  ${if} $SkipDbInstall == 1
    Abort
  ${endif}

  !insertmacro MUI_HEADER_TEXT "$(LocalDatabaseInstallPage_Title)" "$(LocalDatabaseInstallPage_Subtitle)"

  nsDialogs::Create 1018
  Pop $LocalDbInstallOptionsDlg

  ${NSD_CreateGroupBox} 0 0 100% 40u "$(LocalDatabaseInstallPage_GroupBoxTitle)"

  ${NSD_CreateRadioButton} 5u 12u 90% 8u "$(LocalDatabaseInstallPage_CreateDbRadio) (v${DBVERSION}) *"
  Pop $LocalDbInstall_CreateOption
  ${if} $UserHasCreateDBPermission == "0"
    EnableWindow $LocalDbInstall_CreateOption 0
  ${endif}
  GetFunctionAddress $0 OnDbRadioClick
  nsDialogs::OnClick $LocalDbInstall_CreateOption $0

  ;Check to see if the target database exists and set the default install db mode.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM master.sys.sysdatabases WHERE name = $TargetDbNameQuoted)" -o NUL'
  Pop $R2

  ${If} $R2 == "0"
    StrCpy $InstallDbMode 0
  ${Else}
    StrCpy $InstallDbMode 1
  ${Endif}

  ;Execute InstallerHelper app, depending on the authentication type, to get the current db version.
  StrCpy $R3 0
  ${If} $InstallDbMode == 1
    ${If} $SQLServerAuthType == "1"
      nsExec::ExecToStack '"InstallerHelper.exe" dbv server="$SQLServerAddr" db="$TargetDbName" user="$SQLServerUsername" pass="$SQLServerPassword"'
    ${Else}
      nsExec::ExecToStack '"InstallerHelper.exe" dbv server="$SQLServerAddr" db="$TargetDbName"'
    ${EndIf}

    ;Get result.
    Pop $R3
    Pop $R3

    ;Remove extra characters.
    ${WordReplace} $R3 '$\n' '' '+' $R3
    ${WordReplace} $R3 '$\r' '' '+' $R3
  ${EndIf}

  ;Display the db. version only if it's the case.
  ${If} $R3 > 0
    ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(LocalDatabaseInstallPage_UpgradeDbRadio) (v$R3 -> v${DBVERSION}) **"
  ${Else}
    ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(LocalDatabaseInstallPage_UpgradeDbRadio) **"
  ${EndIf}

  ;The upgrade db. option is only available if the db existas and the user has the rigth to upgrade it.
  Pop $LocalDbInstall_UpgradeOption
  ${if} $UserHasAlterDBPermission == "0"
    EnableWindow $LocalDbInstall_UpgradeOption 0
  ${endif}
  GetFunctionAddress $0 OnDbRadioClick
  nsDialogs::OnClick $LocalDbInstall_UpgradeOption $0
  
  ${if} $InstallDbMode == "0"
  ${andif} $UserHasCreateDBPermission == "1"
    ${NSD_Check} $LocalDbInstall_CreateOption
    EnableWindow $LocalDbInstall_UpgradeOption 0
  ${endif}

  ${if} $InstallDbMode == "1"
  ${andif} $UserHasAlterDBPermission == "1"
    ${NSD_Check} $LocalDbInstall_UpgradeOption
  ${endif}

  ${NSD_CreateCheckBox} 15u 45u 90% 8u "$(LocalDatabaseInstallPage_SkipDbBackup)"
  Pop $LocalDbInstall_SkipBackupCheck
  ${NSD_SetState} $LocalDbInstall_SkipBackupCheck $SkipDbBackup

  ${NSD_CreateLabel} 0u 58u 15u 8u "*"
  ${NSD_CreateLabel} 15u 58u 90% 16u "$(LocalDatabaseInstallPage_Star1)"
  ${NSD_CreateLabel} 0u 76u 15u 8u "**"
  ${NSD_CreateLabel} 15u 76u 90% 24u "$(LocalDatabaseInstallPage_Star2)"
  ${NSD_CreateLabel} 0u 102u 15u 8u "***"
  ${NSD_CreateLabel} 15u 102u 90% 16u "$(LocalDatabaseInstallPage_Star3)"

  ;This is called to set the default options depending on the Db install mode.
  ;It needs to be at the end because all the elements of the UI need to be created before calling it.
  call OnDbRadioClick

  nsDialogs::Show

FunctionEnd

;The event of selecting a db install option.
Function OnDbRadioClick

  ${NSD_GetState} $LocalDbInstall_CreateOption $0

  ${If} $0 = 0
    ${NSD_SetState} $LocalDbInstall_SkipBackupCheck 0
    EnableWindow $LocalDbInstall_SkipBackupCheck 1
    StrCpy $SkipDbBackup 1
  ${Else}
    ${NSD_SetState} $LocalDbInstall_SkipBackupCheck 1
    EnableWindow $LocalDbInstall_SkipBackupCheck 0
    StrCpy $SkipDbBackup 0
  ${EndIf}

FunctionEnd

Function LocalDatabaseInstallPageLeave

  ${NSD_GetState} $LocalDbInstall_CreateOption $0
  ${NSD_GetState} $LocalDbInstall_UpgradeOption $1
  ${if} $0 == "1"
    strcpy $InstallDbMode "0"
  ${else}
    strcpy $InstallDbMode "1"
  ${endif}

  ${NSD_GetState} $LocalDbInstall_SkipBackupCheck $SkipDbBackup

FunctionEnd

;--- Uninstall Settings Page ---
var DeleteLocalDataCheckBox
var DeleteLocalDbCheckBox

Function un.UninstallSettingsPage

  !insertmacro MUI_HEADER_TEXT "$(UninstallSettingsPage_Title)" "$(UninstallSettingsPage_Subtitle)"
  StrCpy $UninstalSQLServerAddr "localhost\sqlexpress"

  nsDialogs::Create 1018
  
  Pop $AppSettingsDialog
  ${If} $AppSettingsDialog == error
    Abort
  ${EndIf}

  ${NSD_CreateCheckbox} 5u 0 90% 15u "$(UninstallDeleteUserLocalData)"
  Pop $DeleteLocalDataCheckBox
  ${NSD_OnClick} $DeleteLocalDataCheckBox un.HandleLocalDataDeletionClick

  ${NSD_CreateCheckbox} 5u 20u 90% 15u "$(UninstallDeleteLocalDatabase)"
  Pop $DeleteLocalDbCheckBox
  ${NSD_OnClick} $DeleteLocalDbCheckBox un.HandleDbDeletionClick

  ${NSD_CreateGroupBox} 0 40u 100% 100u "$(LocalDatabaseConnectionCredentials_GroupBoxTitle)"

  ${NSD_CreateLabel} 5u 55u 70u 8u "$(SqlServer)"
  ${NSD_CreateText} 80u 56u 120u 13u "$UninstalSQLServerAddr"
  Pop $LocalDbInstall_ServerNameText
  ${NSD_SetFocus} $LocalDbInstall_ServerNameText
  
  ;The Windows Authentication radio button
  ${NSD_CreateRadioButton} 5u 75u 90% 8u "$(WindowsAuth)"
  Pop $LocalDbInstall_WinAuthCheck
  ${NSD_Check} $LocalDbInstall_WinAuthCheck
  ${NSD_OnClick} $LocalDbInstall_WinAuthCheck  un.HandleSqlAuthClick

  ;The SQL Server Authentication radio button
  ${NSD_CreateRadioButton} 5u 90u 90% 8u "$(SqlServerAuth)"
  Pop $LocalDbInstall_SqlAuthCheck
  ${NSD_OnClick} $LocalDbInstall_SqlAuthCheck un.HandleSqlAuthClick

  ;The Username and Password inputs associated with the SQL Server Authentication radio button
  ${NSD_CreateLabel} 16u 103u 64u 8u "$(Username)"
  ${NSD_CreateText} 80u 104u 120u 13u "$SQLServerUsername"
  Pop $LocalDbInstall_SqlAuthUsername

  ${NSD_CreateLabel} 16u 119u 64u 8u "$(Password)"
  ${NSD_CreatePassword} 80u 120u 120u 13u "$SQLServerPassword"
  Pop $LocalDbInstall_SqlAuthPassword

  call un.HandleDbDeletionClick

  nsDialogs::Show

FunctionEnd

Function un.UninstallSettingsPageLeave

${If} $DeleteLocalDb == '1'
  ;Read the sql server name
  ${NSD_GetText} $LocalDbInstall_ServerNameText $SQLServerAddr
  ${If} $SQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(ServerNameEmpty)"
    Abort
  ${EndIf}
  
  StrCpy $SQLServerAuthType "0"
  ${NSD_GetState} $LocalDbInstall_SqlAuthCheck $0
  ${If} $0 == "1"
    StrCpy $SQLServerAuthType "1"
    ${NSD_GetText} $LocalDbInstall_SqlAuthUsername $SQLServerUsername
    ${NSD_GetText} $LocalDbInstall_SqlAuthPassword $SQLServerPassword

    ${If} $SQLServerUsername == ""
      MessageBox MB_OK|MB_ICONSTOP "$(UserNameEmpty)"
      Abort
    ${ElseIf} $SQLServerPassword == ""
      MessageBox MB_OK|MB_ICONSTOP "$(PasswordEmpty)"
      Abort
    ${EndIf}
  ${EndIf}
  
${EndIf}

FunctionEnd

Function un.HandleLocalDataDeletionClick

${NSD_GetState} $DeleteLocalDataCheckBox $DeleteLocalData

FunctionEnd

Function un.HandleDbDeletionClick

  ${NSD_GetState} $DeleteLocalDbCheckBox $DeleteLocalDb

  ;$1 contains the param for the EnableWindow call that will enable or disable the other page controls
  ${if} $DeleteLocalDb == "1"
    strcpy $1 "1"
  ${else}
    strcpy $1 "0"
  ${endif}

  EnableWindow $LocalDbInstall_ServerNameText $1
  EnableWindow $LocalDbInstall_WinAuthCheck $1
  EnableWindow $LocalDbInstall_SqlAuthCheck $1

  call un.HandleSqlAuthClick
FunctionEnd

Function un.HandleSqlAuthClick

  ${NSD_GetState} $LocalDbInstall_SqlAuthCheck $1

  ${if} $DeleteLocalDb == "1"
  ${AndIf} $1 == "1"
    strcpy $2 "1"
  ${else}
    strcpy $2 "0"
  ${endif}

  EnableWindow $LocalDbInstall_SqlAuthUsername $2
  EnableWindow $LocalDbInstall_SqlAuthPassword $2

FunctionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  nsExec::Exec 'taskkill /f /im "${ExeName}"'
  Sleep 500 ; sleep necessary because sometimes the app dlls are not unlocked at the exact moment the Exec call returns
  
  ;Uninstall the app files
  !insertmacro UNINSTALL.LOG_BEGIN_UNINSTALL
  !insertmacro UNINSTALL.LOG_UNINSTALL "$INSTDIR"
  !insertmacro UNINSTALL.LOG_END_UNINSTALL

  ;Remove the shortcuts
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  ${If} $StartMenuFolder != ""
    RMDir /r "$SMPROGRAMS\$StartMenuFolder"
  ${EndIf}
  Delete "$DESKTOP\${LnkName}"
  
  ;Remove the app registry key and the uninstall registry key
  DeleteRegKey SHCTX "${AppRegistryRootKey}"
  DeleteRegKey SHCTX "${UninstallRegistryRootKey}"

  !insertmacro DeleteFileAssociations

  ;Remove the app local data
  ${If} $DeleteLocalData == "1"
    RMDir /r "${AppDataFolderPath}"
  ${EndIf}
  
  strcpy $TargetDbName "ZPKTool"
  call un.DetectSqlcmdPath
  strcpy $SqlcmdPath $0

  ;Remove the app local database
  ${If} $DeleteLocalDb == "1"
    ${If} $SQLServerAuthType == "1"
        nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -U $SQLServerUsername -P $SQLServerPassword -S "$SQLServerAddr" -Q "ALTER DATABASE $TargetDbName SET MULTI_USER WITH ROLLBACK IMMEDIATE DROP DATABASE $TargetDbName"'
    ${Else}
        nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" -Q "ALTER DATABASE $TargetDbName SET SINGLE_USER WITH ROLLBACK IMMEDIATE DROP DATABASE $TargetDbName"'
    ${EndIf}
  ${EndIf}

SectionEnd

;--------------------------------
; Uninstaller functions

Function un.onInit

  !insertmacro MULTIUSER_UNINIT

  ;Read the app's install mode from registry. If the install mode is missing it means that the app was installed for all users or the registry entry was manually deleted (unlikely).
  ReadRegStr $0 SHCTX "${AppRegistryRootKey}" "InstallMode"

  ${If} ${Silent}
    Call un.InitSilentUnInstallation
  ${Else}
    ;Check if the user has enough privileges to be able to uninstall the app.
    ${If} $0 != CurrentUser
    ${AndIf} $MultiUser.Privileges != Admin
    ${AndIf} $MultiUser.Privileges != Power
      MessageBox MB_ICONSTOP "$(NoPrivilegesToUninstall)" /SD IDOK
      Abort
    ${EndIf}
    !insertmacro MUI_UNGETLANGUAGE
  ${EndIf}
      
FunctionEnd

Function un.InitSilentUnInstallation

  ;Read from the cmd line the configuration parameters supported
  ${GetParameters} $R0
  ClearErrors
  
  ${GetOptions} $R0 /DeleteLocalData= $DeleteLocalData
  ${If} $DeleteLocalData == ""
    StrCpy $DeleteLocalData 0
  ${EndIf}
  
  ${GetOptions} $R0 /DeleteLocalDb= $DeleteLocalDb
  ${If} $DeleteLocalDb == ""
    StrCpy $DeleteLocalDb 0
  ${EndIf}
  
  ${GetOptions} $R0 /ServerName= $SQLServerAddr
  
  ${GetOptions} $R0 /ServerAuthType= $SQLServerAuthType
  ${If} $SQLServerAuthType == ""
    StrCpy $SQLServerAuthType 0
  ${EndIf}
  
  ${GetOptions} $R0 /ServerUsername= $SQLServerUsername
 
  ${GetOptions} $R0 /ServerPassword= $SQLServerPassword
  
FunctionEnd