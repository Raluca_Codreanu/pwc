
!insertmacro MUI_LANGUAGE "English"

;---------------------------------
;Installer string resources

LangString InstallingDotNet ${LANG_ENGLISH} "Installing Microsoft .NET Framework 4.0"
LangString CreatingDesktopShortcut ${LANG_ENGLISH} "Creating Desktop Shortcut"
LangString DotNetInstallFailed ${LANG_ENGLISH} "Microsoft .NET Framework 4.0 installation has failed. Please try to manually install it."
LangString DotNetNotInstalled_HasAdminPermission ${LANG_ENGLISH} ".NET Framework 4.0 Full is not installed on your system. In order to continue you have to select the '.NET Framework 4.0' component."
LangString DotNetNotInstalled_DoesntHaveAdminPermission ${LANG_ENGLISH} ".NET Framework 4.0 Full is not installed on your system. Please ask an administrator to install it because you do not have the necessary privileges to do that."
LangString DotNet4FullNotInstalled_HasAdminPermission ${LANG_ENGLISH} ".NET Framework 4.0 Full is not installed on your system (only .NET Framework 4.0 Client Profile was detected). In order to continue you have to select the '.NET Framework 4.0' component."
LangString DotNet4FullNotInstalled_DoesntHaveAdminPermission ${LANG_ENGLISH} ".NET Framework 4.0 Full is not installed on your system (only .NET Framework 4.0 Client Profile was detected). Please ask an administrator to install it because you do not have the necessary privileges to do that."
LangString WritingSettings {$LANG_ENGLISH} "Writing settings ..."
LangString AgreeLicenseCheckBoxText {$LANG_ENGLISH} "I accept the terms of the License Agreement"

LangString DESC_MainSection ${LANG_ENGLISH} "Installs the core application files."
LangString DESC_dotNetSection ${LANG_ENGLISH} "Installs the Microsoft .NET Framework 4.0 if it isn't already installed. Requires a working internet connection.$\r$\n$\r$\nYou need Administrator privileges to install it."
LangString DESC_DesktopIconSection ${LANG_ENGLISH} "Places a shortcut on the desktop."
LangString DESC_FileTypeAssoc ${LANG_ENGLISH} "Sets Product Cost Manager Viewer as the default application for the specified file types."

LangString COMPONENTS_Main ${LANG_ENGLISH} "Program files"
LangString COMPONENTS_DotNet ${LANG_ENGLISH} ".NET Framework 4.0"
LangString COMPONENTS_DesktopIcon ${LANG_ENGLISH} "Desktop Icon"
LangString COMPONENTS_FileTypeAssoc ${LANG_ENGLISH} "File Type Associations"