
!insertmacro MUI_LANGUAGE "German"

;---------------------------------
;Installer string resources

LangString InstallingDotNet ${LANG_GERMAN} "Installieren Microsoft. NET Framework 4.0"
LangString CreatingDesktopShortcut ${LANG_GERMAN} "Desktop-Shortcut erstellen"
LangString DotNetInstallFailed ${LANG_GERMAN} "Microsoft. NET Framework 4.0-Installation ist fehlgeschlagen. Bitte versuchen Sie es manuell zu installieren."
LangString DotNetNotInstalled_HasAdminPermission ${LANG_GERMAN} ".NET Framework 4.0 Full ist nicht auf Ihrem System installiert. Um fortzufahren, m�ssen Sie die '.NET Framework 4.0' -Komponente ausw�hlen."
LangString DotNetNotInstalled_DoesntHaveAdminPermission ${LANG_GERMAN} ".NET Framework 4.0 Full ist nicht auf Ihrem System installiert. Bitte fragen Sie Ihren Systemadministrator, um das fehlende Framework zu installieren, da Sie nicht �ber die notwendigen Berechtigungen verf�gen."
LangString DotNet4FullNotInstalled_HasAdminPermission ${LANG_GERMAN} "Es ist kein .NET Framework 4.0 Full auf Ihrem Computer installiert (es wurde nur .NET Framework 4.0 Client Profile festgestellt). Um fortzufahren, m�ssen Sie die '.NET Framework 4.0'-Komponente ausw�hlen."
LangString DotNet4FullNotInstalled_DoesntHaveAdminPermission ${LANG_GERMAN} "Es ist kein .NET Framework 4.0 Full auf Ihrem Computer installiert (es wurde nur .NET Framework 4.0 Client Profile festgestellt). Bitte fragen Sie Ihren Systemadministrator, um das fehlende Framework zu installieren, da Sie nicht �ber die notwendigen Berechtigungen verf�gen."
LangString WritingSettings {$LANG_GERMAN} "Die Einstellungen werden gespeichert..."
LangString AgreeLicenseCheckBoxText {$LANG_GERMAN} "Ich akzeptiere die Bedingungen der Lizenzvereinbarung"

LangString DESC_MainSection ${LANG_GERMAN} "Installiert die Kernanwendungs-Dateien."
LangString DESC_dotNetSection ${LANG_GERMAN} "Installiert den Microsoft. NET Framework 4.0, wenn es nicht bereits installiert ist. Ben�tigt eine funktionierende Internetverbindung.$\r$\n$\r$\nSie brauchen Administratorrechte um ihn zu installieren."
LangString DESC_DesktopIconSection ${LANG_GERMAN} "Platziert eine Verkn�pfung auf den Desktop."
LangString DESC_FileTypeAssoc ${LANG_GERMAN} "Installiert den Product Cost Manager Viewer als Standard-Anwendung f�r die angegebenen Dateitypen"

LangString COMPONENTS_Main ${LANG_GERMAN} "Programmdateien"
LangString COMPONENTS_DotNet ${LANG_GERMAN} ".NET Framework 4.0"
LangString COMPONENTS_DesktopIcon ${LANG_GERMAN} "Desktop Icon"
LangString COMPONENTS_FileTypeAssoc ${LANG_GERMAN} "Dateitypzuordnungen"