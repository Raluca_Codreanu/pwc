######################################################################
#       Central Db Installer Script
#
#       This install script requires the following symbols to be defined via the /D commandline switch:
#
#       DBVERSION         N format version number
#
#       The symbols are not case sensitive and available in script using the ${symbol_name} syntax.
#       This script is usually run by the BuildPCMInstaller.cmd batch file.
#
#       Example:
#       makensis /DDBVERSION=1.46 CentralDbInstallerScript.nsi
#
######################################################################

;--------------------------------
;Constants

  !define ApplicationName "PCM Central Database"
  !define ApplicationNameLong "Product Cost Manager Central Database"
  !define Company "3C"
  !define OutputFileName "..\Output\ProductCostManager\PCMCentralDbSetup.exe"
  !define InputDir "..\Input"
  !define AppDataFolderPath "$LOCALAPPDATA\PCMData"
  
;--------------------------------
;General

  SetCompressor /FINAL /SOLID lzma
  Name '${ApplicationName}'
  OutFile ${OutputFileName}
  XPStyle on
  BrandingText "Product Cost Manager v${VERSION}, Database v${DBVERSION}"

;--------------------------------
; Version information for the installer output (.exe) file

  VIProductVersion ${VERSION}
  VIAddVersionKey ProductName "${ApplicationNameLong}"
  VIAddVersionKey ProductVersion "${VERSION}"
  VIAddVersionKey FileVersion "${VERSION}"
  VIAddVersionKey CompanyName "${Company}"
  VIAddVersionKey FileDescription "${ApplicationNameLong} Installer"
  VIAddVersionKey LegalCopyright "� ${Company}"
  VIAddVersionKey LegalTrademarks "Product Cost Manager is a trademark of ${Company}"
  
;--------------------------------
; Multi user installation setup

  ;Multi-user is needed to successfully request Admin privileges in both XP and Vista/7
  !define MULTIUSER_MUI
  !define MULTIUSER_EXECUTIONLEVEL Admin
  !define MULTIUSER_INIT_TEXT_ADMINREQUIRED "$(AdminPrivilegesRequired)"
  !include MultiUser.nsh

;--------------------------------
;Include scripts

  !include MUI2.nsh
  !include WordFunc.nsh
  !include LogicLib.nsh
  !include Common.nsh
  !include FileFunc.nsh
  !include defines.nsh
  !include WinVer.nsh

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_WELCOMEFINISHPAGE_BITMAP "WelcomeFinishBitmap.bmp" ;defines the image to display on welcome and finish pages
  !define MUI_ICON "app.ico"
  !define MUI_UNICON "app.ico"
  
  !insertmacro DetectSqlcmdPathMACRO ""
  !insertmacro FindSqlcmdPathMACRO ""

;--------------------------------
;Variables

  var SQLAuthentification       ;Used to authentificate with slqcmd.exe; -E - windows authentification, -U, -P - SQL Server authentification.
  var UserHasCreateDBPermission ;Indicates wheter the used Sql Server user has the necessary permission to create a db.
  var UserHasAlterDBPermission  ;Indicates wheter the used Sql Server user has the necessary permission to alter a db.
  var SqlcmdPath                ;The path where sqlcmd.exe is located.
  var TargetDbName              ;The name of the target database.
  var TargetDbNameQuoted        ;The name of the target database, quoted.
  var InstallDbMode             ;Indicates whether to create or upgrade the database; 0 - create, 1 - upgrade.
  var SkipDbBackup              ;Indicates whether to backup the database before installation.
  var SQLServerAddr             ;Local database server address.
  var SQLServerAuthType         ;Local db server authentication type; 0 for Windows Authentication, 1 for SQL Server Authentication.
  var SQLServerUsername         ;The username to use for connection to the local db server during installation.
  var SQLServerPassword         ;The password to use for connection to the local db server during installation.
  var BackupLocation            ;The location where to move the Db backup.
  var UseSSL                    ;Used to authentificate the SQL Server connection.

;--------------------------------
;Pages

  !define MUI_WELCOMEPAGE_TITLE_3LINES
  !insertmacro MUI_PAGE_WELCOME

  !define MUI_PAGE_CUSTOMFUNCTION_LEAVE ComponentsPageLeaveFunction
  !insertmacro MUI_PAGE_COMPONENTS
  
  Page custom DatabaseConnectionConfigPage DatabaseConnectionConfigPageLeave
  Page custom DatabaseInstallPage DatabaseInstallPageLeave
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
; --------------------------------
; Languages

  !define MUI_LANGDLL_ALLLANGUAGES
  !include CentralDbInstallerLanguage_en.nsh
  !include CentralDbInstallerLanguage_de.nsh

;----------------------------------
;Installer Sections

;The .NET Framework 4.0 section; must appear before the section that deploys the database (DbDeploySection).
Section "$(Components_DotNet)" DotNetFrameworkSection
  
  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\dotNetFx40_Full_setup.exe"
  ExecWait "$PLUGINSDIR\dotNetFx40_Full_setup.exe /passive /norestart" $R0
  ${If} $R0 != 0
  ${AndIf} $R0 != 1641
  ${AndIf} $R0 != 3010
    MessageBox MB_OK|MB_ICONSTOP "$(DotNetInstallFailed)"
  ${endif}

SectionEnd

;Deploys the central database
Section "$(Components_Database)" DbDeploySection
  SectionIn RO
  
  ;$R4 holds the message displayed during db install/upgrade
  ${If} $InstallDbMode == 0
    StrCpy $R4 "$(DbInstall_CreatingDb)"
  ${Else}
    StrCpy $R4 "$(DbInstall_UpgradingDb)"
  ${EndIf}

  DetailPrint $R4

  ;Prepare the connection params
  StrCpy $R0 ""
  StrCpy $R1 ""
  ${If} $SQLServerAuthType == "1"
    StrCpy $R0 '"$SQLServerUsername"'
    StrCpy $R1 '"$SQLServerPassword"'
  ${EndIf}

  ;Install the database
  GetTempFileName $R2
  DetailPrint $R4

  ; Extract the files needed for database deployment
  SetOutPath "$PLUGINSDIR"
  File /r "${InputDir}\CentralDbPackage\"

  ;If the O.S. is Windows XP add a key to the registry because SqlPackage.exe does not support this operating system.
  ${If} ${IsWinXP}
    nsExec::Exec 'REGEDIT /S $PLUGINSDIR\DeploymentUtility\Reg\crypt.reg'
  ${EndIf}

  ;Create a temp bat file that will invoke the database deployment app. This is necessary in order to capture the console output.
  FileOpen $9 "publish_db.bat" w
  FileWrite $9 "@echo off$\r$\n"
  FileWrite $9 'DeploymentUtility\SqlPackage.exe /Action:Publish /SourceFile:"Database\ZPKTool.Database.Central.dacpac" /TargetServerName:"$SQLServerAddr" /TargetDatabaseName:"$TargetDbName" /p:TreatVerificationErrorsAsWarnings=True /p:DropDmlTriggersNotInSource=False /p:AllowIncompatiblePlatform=True /p:IncludeCompositeObjects=True /p:BlockOnPossibleDataLoss=False'
  ${If} $SQLServerAuthType == "1"
    FileWrite $9 ' /TargetUser:$R0 /TargetPassword:$R1'
  ${EndIf}
  ${If} $UseSSL == "1"
   FileWrite $9 ' /TargetEncryptConnection:True'
  ${EndIf}
  ${If} $InstallDbMode == "0"
    FileWrite $9 ' /p:CreateNewDatabase=True'
  ${Else}
    FileWrite $9 ' /p:CreateNewDatabase=False'
    ${If} $SkipDbBackup == "0"
      FileWrite $9 ' /p:BackupDatabaseBeforeChanges=True'
    ${Else}
      FileWrite $9 ' /p:BackupDatabaseBeforeChanges=False'
    ${EndIf}
  ${EndIf}
  FileClose $9

  ;Execute the temp bat file and write the output in the log file.
  nsExec::Exec 'publish_db.bat 1>$R2 2>&1'
  Pop $R3

  ;Write the db installer exit code in the log file.
  FileOpen $9 "$R2" a
  FileSeek $9 0 END
  FileWrite $9 "$\r$\nDeployment Utility exit code: $R3"
  FileClose $9

  ;Rename the log file.
  StrCpy $0 "$TEMP\PCMCentralDbSetup.log"
  CopyFiles /SILENT $R2 $0
  Delete $R2

  ;Show error message if an error occurred.
  ${If} $R3 != 0
    MessageBox MB_YESNO|MB_DEFBUTTON2|MB_ICONSTOP "$(DbInstall_InstallFailed) ($0).\
        $\r$\n$\r$\n$(DbInstall_InstallFailed_OpenLog)" IDNO DontOpenLog
    ;Open the log file
    ExecShell "open" "$0"
    DontOpenLog:
  ${EndIf}
  
  ;Extract and execute the DataEncryption app depending on the authentication type.
  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\Utilities\DataEncryption.exe"

  ${If} $SQLServerAuthType == "1"
        nsExec::Exec '"DataEncryption.exe" /server="$SQLServerAddr" /database="$TargetDbName" /username="$SQLServerUsername" /password="$SQLServerPassword"'
    ${Else}
        nsExec::Exec '"DataEncryption.exe" /server="$SQLServerAddr" /database="$TargetDbName"'
  ${EndIf}
  
  ;Extract and execute the DocumentsCompression app depending on the authentication type.
  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\Utilities\DocumentsCompression.exe"

  ${If} $SQLServerAuthType == "1"
        nsExec::Exec '"DocumentsCompression.exe" /server="$SQLServerAddr" /database="$TargetDbName" /username="$SQLServerUsername" /password="$SQLServerPassword"'
    ${Else}
        nsExec::Exec '"DocumentsCompression.exe" /server="$SQLServerAddr" /database="$TargetDbName"'
  ${EndIf}
  
  ;Inform the user if a backup was created.
  ${If} $InstallDbMode == 1
  ${AndIf} $SkipDbBackup == 0
    ;Create a temp sql file to determine the full path of the database backup.
    FileOpen $9 "backup_info.sql" w
    FileWrite $9 ":EXIT(SELECT TOP 1 physical_device_name + CHAR(13) FROM msdb..backupset bckset INNER JOIN msdb..backupmediafamily bckfmly ON bckset.media_set_id = bckfmly.media_set_id WHERE database_name = '$TargetDbName' ORDER BY backup_finish_date DESC)"
    FileClose $9

    nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -i backup_info.sql -o backup_info'

    ;Read the full path of the backup from the created file, the result is on the 3rd row.
    FileOpen $9 "backup_info" r
    FileRead $9 $R5
    FileRead $9 $R5
    FileRead $9 $R5
    FileClose $9

    ;Delete the carriage return from the string with the full path of the backup.
    ${WordReplace} $R5 '$\r' '' '+' $R5

    ${If} $R5 != "$\r$\n"
      ;Surround the full path of the backup with inverted commas because it will be used in the command shell.
      StrCpy $R5 '"$R5"'

	  ${IfNot} ${Silent}  
        StrCpy $BackupLocation '"$INSTDIR\"'
	  ${EndIf}

      ;Create a temp sql file to move de db backup file from the default path to the application data folder.
      SetOutPath $PLUGINSDIR
      FileOpen $9 "move_backup.sql" w
      FileWrite $9 "EXEC sp_configure 'show advanced options', 1$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "EXEC sp_configure 'xp_cmdshell', 1$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "GO$\r$\n"
      FileWrite $9 "EXEC master..xp_cmdshell 'MOVE $R5 $BackupLocation'$\r$\n"
      FileWrite $9 "GO$\r$\n"
      FileWrite $9 "EXEC sp_configure 'xp_cmdshell', 0$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "EXEC sp_configure 'show advanced options', 0$\r$\n"
      FileWrite $9 "RECONFIGURE$\r$\n"
      FileWrite $9 "GO"
      FileClose $9

      ;It is possible that the move operation of the backup fails because of access permissions to the destination folder.
      nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -i move_backup.sql -o NUL'

      ;Copy the original full path of the backup so we can display it in case the move fails.
      StrCpy $R6 $R5

      ;Get the file name of the backup from the full backup path.
      ${GetFileName} $R5 $R5

      ;Delete the leftover '"' char from the backup location and file name.
      ${WordReplace} $BackupLocation '"' '' '+' $BackupLocation
      ${WordReplace} $R5 '"' '' '+' $R5
      ${WordReplace} $R6 '"' '' '+' $R6

      ${If} ${FileExists} $BackupLocation$R5
	  ${AndIfNot} ${Silent}
        MessageBox MB_OK|MB_ICONINFORMATION '$(DbInstall_BackupCreated) ("$BackupLocation$R5").'
      ${ElseIf} ${FileExists} $R6
	  ${AndIfNot} ${Silent}
        MessageBox MB_OK|MB_ICONINFORMATION '$(DbInstall_BackupCreated) ("$R6").$\n$(DbInstall_BackupCreatedFailedToMove)'
      ${EndIf}
    ${EndIf}
  ${EndIf}

  ;Make sure the plugins dir is not locked and will be deleted.
  SetOutPath $TEMP

SectionEnd

;--------------------------------
;Section Descriptions

  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${DbDeploySection} $(Components_Database_Description)
    !insertmacro MUI_DESCRIPTION_TEXT ${DotNetFrameworkSection} $(Components_DotNet_Description)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
  
;--------------------------------
;Installer Functions

Function ComponentsPageLeaveFunction
  ;If .NET Framework is not installed show a warning to tell the user that it needs to be installed
  ;in order to run the application.
  ${DetectDotNetFramework} $R1
  SectionGetFlags ${DotNetFrameworkSection} $R2
  IntOp $R3 $R2 & ${SF_SELECTED}

  ${If} $R3 != ${SF_SELECTED}             ;The ".NET Framework 4.0" component is not selected
    ${If} $R1 == ${DotNet_None}
      MessageBox MB_ICONSTOP "$(DotNetNotInstalled)"
      Abort
    ${ElseIf} $R1 == ${DotNet_v4_Client}
      MessageBox MB_ICONSTOP "$(DotNet4FullNotInstalled)"
      Abort
    ${EndIf}
  ${EndIf}
FunctionEnd

Function .onInit
  !insertmacro MULTIUSER_INIT

  StrCpy $UseRHasCreateDBPermission "0"
  StrCpy $UserHasAlterDBPermission "0"
  StrCpy $TargetDbName "ZPKToolCentral"
  StrCpy $TargetDbNameQuoted "'$TargetDbName'"
  StrCpy $SQLServerAddr "localhost\sqlexpress"
  StrCpy $SQLServerAuthType 0
  StrCpy $UseSSL 0
  StrCpy $InstallDbMode 1
  StrCpy $BackupLocation ""

  ${If} ${Silent}  
   ${GetParameters} $R0
   ClearErrors
   ${GetOptions} $R0 /SERVERNAME= $SQLServerAddr 
   ${If} $SQLServerAddr == ""
     MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_ServerNameEmpty)"
     Abort
   ${EndIf}
   
   ${GetOptions} $R0 /SQLAUTHTYPE= $SQLServerAuthType
   ${If} $SQLServerAuthType == ""
     StrCpy $SQLServerAuthType 0
   ${EndIf}
   
   ${GetOptions} $R0 /USERNAME= $SQLServerUsername 
   ${If} $SQLServerAuthType == 1
   ${AndIf} $SQLServerUsername == ""
     MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_UsernameEmpty)"
     Abort
   ${EndIf}

   ${GetOptions} $R0 /PASSWORD= $SQLServerPassword
   ${If} $SQLServerAuthType == 1
   ${AndIf} $SQLServerPassword == ""
     MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_PasswordEmpty)"
     Abort
   ${EndIf}
   
   ${GetOptions} $R0 /INSTALLDBMODE= $InstallDbMode
   ${If} $InstallDbMode == ""
     StrCpy $InstallDbMode 1
   ${EndIf}
   
   ${GetOptions} $R0 /SKIPDBBACKUP= $SkipDbBackup
   ${If} $SkipDbBackup == ""
     StrCpy $SkipDbBackup 0
   ${EndIf}
   
   ${GetOptions} $R0 /BACKUPLOCATION= $BackupLocation
   ${If} $InstallDbMode == 1
   ${AndIf} $SkipDbBackup == 0
   ${AndIf} $BackupLocation == ""
     MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_BackupLocationEmpty)"
	 Abort
   ${EndIf}
  ${EndIf}
  
  Call DetectSqlcmdPath
  StrCpy $SqlcmdPath $0
  ${If} $SqlcmdPath == ""
    MessageBox MB_OK|MB_ICONSTOP "$(SqlcmdNotFound)"
    SetErrorLevel 8
    Abort
  ${EndIf}
  
  ;Extract the installer helper app
  SetOutPath $PLUGINSDIR\InstallerHelper
  File "${InputDir}\Installers\InstallerHelper\*"
  
  ;If .NET 4 is already installed deselect its section.
  ${DetectDotNetFramework} $R1
  ${If} $R1 == ${DotNet_v4_Full}
    !insertmacro UnselectSection ${DotNetFrameworkSection}
  ${EndIf}

   !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

;--- Database Connection Configuration Page ---
  var DbConnectionConfigPage.ServerNameText
  var DbConnectionConfigPage.WinAuthCheck
  var DbConnectionConfigPage.SqlAuthCheck
  var DbConnectionConfigPage.SqlAuthUsername
  var DbConnectionConfigPage.SqlAuthPassword
  var DbConnectionConfigPage.UseSSL

Function DatabaseConnectionConfigPage

  !insertmacro MUI_HEADER_TEXT "$(DbConnectionConfigPage_Title)" "$(DbConnectionConfigPage_Subtitle)"

  nsDialogs::Create 1018

  ${NSD_CreateLabel} 0u 1u 75u 8u "$(DbConnectionConfigPage_SqlServer)"
  ${NSD_CreateText} 80u 0u 120u 13u "$SQLServerAddr"
  Pop $DbConnectionConfigPage.ServerNameText
  ${NSD_SetFocus} $DbConnectionConfigPage.ServerNameText
  
  ;The Authentication groupbox
  ${NSD_CreateGroupBox} 0u 15u 100% 72u "$(DbConnectionConfigPage_Authentication)"

  ;The Windows Authentication radio button
  ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(DbConnectionConfigPage_WindowsAuth)"
  Pop $DbConnectionConfigPage.WinAuthCheck
  GetFunctionAddress $0 OnAuthRbClick
  nsDialogs::OnClick $DbConnectionConfigPage.WinAuthCheck $0

  ;The SQL Server Authentication radio button
  ${NSD_CreateRadioButton} 5u 37u 90% 8u "$(DbConnectionConfigPage_SqlServerAuth)"
  Pop $DbConnectionConfigPage.SqlAuthCheck
  GetFunctionAddress $0 OnAuthRbClick
  nsDialogs::OnClick $DbConnectionConfigPage.SqlAuthCheck $0

  ;The Use SSL support checkbox
  ${NSD_CreateCheckBox} 1u 90u 90% 8u "$(DbConnectionConfigPage_UseSSL)"
  Pop $DbConnectionConfigPage.UseSSL
  ${NSD_SetState} $DbConnectionConfigPage.UseSSL $UseSSL
  
  ;Select the appropriate authentication radio button based on the authentication method set internally
  ${If} $SQLServerAuthType == "0"
    ${NSD_Check} $DbConnectionConfigPage.WinAuthCheck
  ${Else}
    ${NSD_Check} $DbConnectionConfigPage.SqlAuthCheck
  ${EndIf}

  ;The Username and Password inputs associated with the SQL Server Authentication radio button
  ${NSD_CreateLabel} 16u 51u 64u 8u "$(DbConnectionConfigPage_Username)"
  ${NSD_CreateText} 80u 50u 120u 13u "$SQLServerUsername"
  Pop $DbConnectionConfigPage.SqlAuthUsername

  ${NSD_CreateLabel} 16u 66u 64u 8u "$(DbConnectionConfigPage_Password)"
  ${NSD_CreatePassword} 80u 65u 120u 13u "$SQLServerPassword"
  Pop $DbConnectionConfigPage.SqlAuthPassword

  call OnAuthRbClick
  
  nsDialogs::Show

FunctionEnd

Function OnAuthRbClick

  ${NSD_GetState} $DbConnectionConfigPage.WinAuthCheck $0

  ;$1 contains the param for the EnableWindow call taht will enable or disable the other page controls
  ${if} $0 == "1"
    strcpy $1 "0"
  ${else}
    strcpy $1 "1"
  ${endif}

  EnableWindow $DbConnectionConfigPage.SqlAuthUsername $1
  EnableWindow $DbConnectionConfigPage.SqlAuthPassword $1

FunctionEnd

;----------------------------------

; This callback is triggered when the user leaves the Database Connection Configuration page
Function DatabaseConnectionConfigPageLeave

  ;Get the SQL Server name
  ${NSD_GetText} $DbConnectionConfigPage.ServerNameText $SQLServerAddr ;server name
  ${If} $SQLServerAddr == ""
    MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_ServerNameEmpty)"
    Abort
  ${EndIf}
  
  ;Get the sql server authentication type
  ${NSD_GetState} $DbConnectionConfigPage.WinAuthCheck $0
  ${If} $0 == 1
    StrCpy $SQLServerAuthType 0
    StrCpy $SQLServerUsername ""
    StrCpy $SQLServerPassword ""
  ${EndIf}

  ${NSD_GetState} $DbConnectionConfigPage.SqlAuthCheck $0
  ${If} $0 == 1
    StrCpy $SQLServerAuthType 1

    ${NSD_GetText} $DbConnectionConfigPage.SqlAuthUsername $SQLServerUsername
    ${NSD_GetText} $DbConnectionConfigPage.SqlAuthPassword $SQLServerPassword

    ${If} $SQLServerUsername == ""
      MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_UsernameEmpty)"
      Abort
    ${EndIf}

    ${If} $SQLServerPassword == ""
      MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_PasswordEmpty)"
      Abort
    ${EndIf}
  ${EndIf}

  ${If} $SQLServerAuthType == "1"
    StrCpy $SQLAuthentification ' -U $SQLServerUsername -P $SQLServerPassword'
  ${Else}
    StrCpy $SQLAuthentification ' -E'
  ${EndIf}
  
  ${NSD_GetState} $DbConnectionConfigPage.UseSSL $0
  ${If} $0 == "1"
    StrCpy $UseSSL "1"
  ${Else}
    StrCpy $UseSSL "0"
  ${EndIf}
  
  ${If} $UseSSL == "1"
    nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -N -q "EXIT"'
  ${Else}
    nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q "EXIT"'
  ${EndIf}
    
  Pop $0
  ${If} $0 == 1
    ${If} $UseSSL == "0"
      MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_ServerConnectionFail)"
      Abort
    ${Else}
      MessageBox MB_OK|MB_ICONSTOP "$(DbConnectionConfigPage_ServerConnectionFail) $(DbConnectionConfigPage_SSLCertificateInvalid)"
      Abort
    ${EndIf}
  ${EndIf}

  ;Check to see if the used SQL Server user has the necessary permission in order to create the application's db.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM fn_my_permissions(NULL, ""SERVER"") WHERE permission_name = ""CREATE ANY DATABASE"")" -o NUL'
  Pop $R2

  ${If} $R2 == "1"
    strcpy $UserHasCreateDBPermission "1"
  ${Else}
    strcpy $UserHasCreateDBPermission "0"
  ${EndIf}

  ;Check to see if the used SQL Server user has the necessary permission in order to modify the application's db.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM fn_my_permissions(NULL, ""SERVER"") WHERE permission_name = ""ALTER ANY DATABASE"")" -o NUL'
  Pop $R2

  ${If} $R2 == "1"
    strcpy $UserHasAlterDBPermission "1"
  ${Else}
    strcpy $UserHasAlterDBPermission "0"
  ${EndIf}

  ;Depending on the permissions that used SQL Server user has a specific message box is displayed.
  ${If} $UserHasCreateDBPermission == "0"
  ${AndIf} $UserHasAlterDBPermission == "0"
    MessageBox MB_OK|MB_ICONSTOP "$(NoCreateAlterDBPermissions)"
    Abort
  ${ElseIf} $UserHasCreateDBPermission == "0"
    MessageBox MB_OK|MB_ICONEXCLAMATION "$(NoCreateDBPermission)"
  ${ElseIf} $UserHasAlterDBPermission == "0"
    MessageBox MB_OK|MB_ICONEXCLAMATION "$(NoAlterDBPermission)"
  ${EndIf}

FunctionEnd

;----------- Start Database Install/Upgrade Page ----------
var DbInstallPage.CreateOption
var DbInstallPage.UpgradeOption
var DbInstallPage.SkipBackupCheck
var DbInstallPage.BrowseButton
var DbInstallPage.BackupPath

Function DatabaseInstallPage

  !insertmacro MUI_HEADER_TEXT "$(DatabaseInstallPage_Title)" "$(DatabaseInstallPage_Subtitle)"

  nsDialogs::Create 1018
  
  ;GroupBox to choose install options
  ${NSD_CreateGroupBox} 0 0 100% 40u "$(DatabaseInstallPage_GroupBoxTitle)"

  ;Radio button to create Db.
  ${NSD_CreateRadioButton} 5u 12u 90% 8u "$(DatabaseInstallPage_CreateDbRadio) (v${DBVERSION}) *"
  Pop $DbInstallPage.CreateOption
  ${If} $UserHasCreateDBPermission == "0"
    EnableWindow $DbInstallPage.CreateOption 0
  ${EndIf}
  GetFunctionAddress $0 OnDbRadioClick
  nsDialogs::OnClick $DbInstallPage.CreateOption $0

  ;Check to see if the target database exists and set the default install db mode.
  nsExec::Exec '"$SqlcmdPath\sqlcmd.exe" -S "$SQLServerAddr" $SQLAuthentification -q ":EXIT(SELECT COUNT(*) FROM master.sys.sysdatabases WHERE name = $TargetDbNameQuoted)" -o NUL'
  Pop $R2

  ${If} $R2 == "0"
    StrCpy $InstallDbMode 0
  ${Else}
    StrCpy $InstallDbMode 1
  ${EndIf}

  ;Execute InstallerHelper app, depending on the authentication type, to get the current db version.
  StrCpy $R3 0
  ${If} $InstallDbMode == 1
    ${If} $SQLServerAuthType == "1"
      nsExec::ExecToStack '"InstallerHelper.exe" dbv server="$SQLServerAddr" db="$TargetDbName" user="$SQLServerUsername" pass="$SQLServerPassword"'
    ${Else}
      nsExec::ExecToStack '"InstallerHelper.exe" dbv server="$SQLServerAddr" db="$TargetDbName"'
    ${EndIf}

    ;Get result.
    Pop $R3
    Pop $R3

    ;Remove extra characters.
    ${WordReplace} $R3 '$\n' '' '+' $R3
    ${WordReplace} $R3 '$\r' '' '+' $R3
  ${EndIf}

  ;Display the db. version only if it's the case.
  ;Radio button to upgrade.
  ${If} $R3 > 0
    ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(DatabaseInstallPage_UpgradeDbRadio) (v$R3 -> v${DBVERSION}) **"
  ${Else}
    ${NSD_CreateRadioButton} 5u 25u 90% 8u "$(DatabaseInstallPage_UpgradeDbRadio) **"
  ${EndIf}

  ;The upgrade db. option is only available if the db exists and the user has the rigth to upgrade it.
  Pop $DbInstallPage.UpgradeOption
  ${If} $UserHasAlterDBPermission == "0"
    EnableWindow $DbInstallPage.UpgradeOption 0
  ${EndIf}
  GetFunctionAddress $0 OnDbRadioClick
  nsDialogs::OnClick $DbInstallPage.UpgradeOption $0
  
  ${If} $InstallDbMode == 0
  ${AndIf} $UserHasCreateDBPermission == 1
    ${NSD_Check} $DbInstallPage.CreateOption
    EnableWindow $DbInstallPage.UpgradeOption 0
  ${EndIf}

  ${If} $InstallDbMode == 1
  ${AndIf} $UserHasAlterDBPermission == 1
    ${NSD_Check} $DbInstallPage.UpgradeOption
  ${EndIf}

  ;Checkbox to skip creating a Db backup.
  ${NSD_CreateCheckBox} 15u 45u 90% 8u "$(DatabaseInstallPage_SkipDbBackup)"
  Pop $DbInstallPage.SkipBackupCheck
  ${NSD_SetState} $DbInstallPage.SkipBackupCheck $SkipDbBackup
  GetFunctionAddress $0 OnSkipDbBackupCheckboxClick
  nsDialogs::OnClick $DbInstallPage.SkipBackupCheck $0

  ;Button to browse the destination of the Db backup.
  ${NSD_CreateButton} 15u 58u 50u 14u "$(DatabaseInstallPage_BrowseButton)"
  Pop $DbInstallPage.BrowseButton
  GetFunctionAddress $0 OnBrowseButtonClick
  nsDialogs::OnClick $DbInstallPage.BrowseButton $0

  ;Text field for the Db backup path.
  ${NSD_CreateText} 70u 59u 76% 12u "$(DatabaseInstallPage_BackupPath)"
  Pop $DbInstallPage.BackupPath
  EnableWindow $DbInstallPage.BackupPath 0

  ;Labels.
  ${NSD_CreateLabel} 0u 77u 15u 8u "*"
  ${NSD_CreateLabel} 15u 77u 90% 16u "$(DatabaseInstallPage_Star1)"
  ${NSD_CreateLabel} 0u 95u 15u 8u "**"
  ${NSD_CreateLabel} 15u 95u 90% 24u "$(DatabaseInstallPage_Star2)"
  ${NSD_CreateLabel} 0u 122u 15u 8u "***"
  ${NSD_CreateLabel} 15u 122u 90% 16u "$(DatabaseInstallPage_Star3)"

  ;This is called to set the default options depending on the Db install mode.
  ;It needs to be at the end because all the elements of the UI need to be created before calling it.
  Call OnDbRadioClick

  nsDialogs::Show

FunctionEnd

;The event of selecting a db install option.
Function OnDbRadioClick

  ${NSD_GetState} $DbInstallPage.CreateOption $0

  ${If} $0 = 0
    ${NSD_SetState} $DbInstallPage.SkipBackupCheck 0
    EnableWindow $DbInstallPage.SkipBackupCheck 1
  ${Else}
    ${NSD_SetState} $DbInstallPage.SkipBackupCheck 1
    EnableWindow $DbInstallPage.SkipBackupCheck 0
  ${EndIf}

  call OnSkipDbBackupCheckboxClick

FunctionEnd

;The event of clicking the skip Db backup creation checkbox.
Function OnSkipDbBackupCheckboxClick

  SendMessage $DbInstallPage.SkipBackupCheck ${BM_GETCHECK} 0 0 $0
  ${If} $0 = 0
    EnableWindow $DbInstallPage.BrowseButton 1
    ${NSD_SetText} $DbInstallPage.BackupPath "$(DatabaseInstallPage_BackupPath)"
    StrCpy $SkipDbBackup 0
  ${Else}
    EnableWindow $DbInstallPage.BrowseButton 0
    ${NSD_SetText} $DbInstallPage.BackupPath ""
    StrCpy $SkipDbBackup 1
  ${EndIf}

  call VerifyDbBackupConfig

FunctionEnd

;The event of clicking the Db backup location browse button.
Function OnBrowseButtonClick

  Dialogs::Folder "$(DatabaseInstallPage_BrowseFolderDialogTitle)" "$(DatabaseInstallPage_BrowseFolderDialogText)" "" ${VAR_INSTDIR}
  ${NSD_SetText} $DbInstallPage.BackupPath $INSTDIR
  StrCpy $BackupLocation $INSTDIR

  Call VerifyDbBackupConfig

FunctionEnd

;If creating a Db backup verify if the user has selected a destination.
Function VerifyDbBackupConfig

  ${NSD_GetText} $DbInstallPage.BackupPath $0
  GetDlgItem $1 $HWNDPARENT 1

  ${If} $SkipDbBackup = 0
  ${AndIf} $0 == "$(DatabaseInstallPage_BackupPath)"
    ;Disable the "Install" button.
    EnableWindow $1 0
  ${Else}
    ;Enable the "Install" button.
    EnableWindow $1 1
  ${EndIf}

FunctionEnd

Function DatabaseInstallPageLeave

  ${NSD_GetState} $DbInstallPage.CreateOption $0
  ${NSD_GetState} $DbInstallPage.UpgradeOption $1

  ${If} $0 == 1
    StrCpy $InstallDbMode 0
  ${Else}
    StrCpy $InstallDbMode 1
  ${EndIf}

  ${NSD_GetState} $DbInstallPage.SkipBackupCheck $SkipDbBackup
  
FunctionEnd
;----------- End Local Database Installation Page ----------