;--------------------------------
;Constants

  !define ApplicationName "Product Cost Manager Viewer"
  !define Company "3C"
  !define AppWebsite "www.productcostmanager.com"

  !define DefaultInstallDirName "Product Cost Manager Viewer"
  !define LnkName "Product Cost Manager Viewer.lnk"
  !define ExeName "PCM.exe"
  !define UninstallLnkName "Uninstall.lnk"
  !define GlobalSettingsFile "$INSTDIR\GlobalSettings.xml"
  !define ExeConfigPath "$INSTDIR\PCM.exe.config"
  !define LicenseFilePath ".\EULA.rtf"

  !define UninstallRegistryRootKey "Software\Microsoft\Windows\CurrentVersion\Uninstall\{F7F38F18-5CFA-4E04-89CA-2B307BEE819F}" ;the registry root key for the app uninstall information

  !define InputDir "..\InputViewer\"
  !define OutputFileName "..\Output\ProductCostManager\PCMViewerSetup.exe"

;--------------------------------
;General Installer Information

  SetCompressor /FINAL /SOLID lzma
  Name "${ApplicationName}"
  OutFile ${OutputFileName}
  BrandingText "${ApplicationName} v${VERSION}"
  XPStyle on

;--------------------------------
; Version information for the installer output (.exe) file

  VIProductVersion ${VERSION}
  VIAddVersionKey ProductName "${ApplicationName}"
  VIAddVersionKey ProductVersion "${VERSION}"
  VIAddVersionKey FileVersion "${VERSION}"
  VIAddVersionKey CompanyName "${Company}"
  VIAddVersionKey FileDescription "${ApplicationName} Installer"
  VIAddVersionKey LegalCopyright "� ${Company}"
  VIAddVersionKey LegalTrademarks "${ApplicationName} is a trademark of ${Company}"

;--------------------------------
; Multi user setup

  !define MULTIUSER_EXECUTIONLEVEL Highest
  !define MULTIUSER_INSTALLMODE_INSTDIR "${ApplicationName}"
  !include MultiUser.nsh

;--------------------------------
; Include scripts

  !include MUI2.nsh
  !include FileFunc.nsh
  !include WordFunc.nsh
  !include common.nsh
  
;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_WELCOMEFINISHPAGE_BITMAP "WelcomeFinishBitmap.bmp" ;defines the image to display on welcome and finish pages
  !define MUI_ICON "app.ico"
  !define MUI_UNICON "app.ico"
  
;----------------------------------
;Initialize the Uninstall Log (logging of installed files in order to delete them when uninstalling).
  !define INSTDIR_REG_ROOT "SHCTX"
  !define INSTDIR_REG_KEY "${UninstallRegistryRootKey}"

  ;The uninstaller executable name (without the extension). Must be set before including AdvUninstLog.nsh
  ;Use UNINST_EXE to get the full uninstaller file path (including the install folder path).
  !define UNINSTALL_LOG "Uninstall"

  !include AdvUninstLog.nsh
  !insertmacro UNATTENDED_UNINSTALL

;--------------------------------
;Variables

  var StartMenuFolder
  var UpdateMode                ;Indicates whether the installer is running in update mode (1 - update mode is on, 0 - off).
                                ;The Update mode is like the Silent mode except it uses some info provided in the command line.
  var CrtVersionUninstaller     ;Contains the path to the uninstaller of the currently installed app version. The "current" version will be uninstalled before installing the files of the new version.
  var CrtVersionInstallFolder   ;The folder where the current app version is installed (in case the app is already installed).

;--------------------------------
;Pages

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_LICENSEPAGE_CHECKBOX
  !define MUI_LICENSEPAGE_CHECKBOX_TEXT "$(AgreeLicenseCheckBoxText)"
  !insertmacro MUI_PAGE_LICENSE ${LicenseFilePath}

  !define MUI_PAGE_CUSTOMFUNCTION_LEAVE ComponenetsPageLeaveFunction
  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MUI_PAGE_COMPONENTS

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !insertmacro MUI_PAGE_DIRECTORY

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_STARTMENUPAGE_DEFAULTFOLDER "${ApplicationName}"
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

  !insertmacro MUI_PAGE_INSTFILES

  !define MUI_PAGE_CUSTOMFUNCTION_PRE SkipPageInUpdateMode
  !define MUI_FINISHPAGE_RUN $INSTDIR\${ExeName}
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

  !define MUI_LANGDLL_ALLLANGUAGES
  !include PCMViewerInstallerLanguage_en.nsh
  !include PCMViewerInstallerLanguage_de.nsh

;--------------------------------
;Reserve Files

  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  !insertmacro MUI_RESERVEFILE_LANGDLL
  ReserveFile "${NSISDIR}\Plugins\*.dll"

;-----------------------------------
;Installer Sections

;The Main section
Section "$(COMPONENTS_Main)" MainSection

  SectionIn RO

  ;Stop the app if it's running
  nsExec::Exec "taskkill /f /im ${ExeName}"
  Sleep 500  ;sleep necessary because sometimes the app dlls are not unlocked at the exact moment the Exec call returns

  ;Check if the app is already installed by looking for its uninstall information stored in registry
  ReadRegStr $0 SHCTX "${UninstallRegistryRootKey}" "UninstallString"
  ReadRegStr $1 SHCTX "${UninstallRegistryRootKey}" "DisplayVersion"

  ;Store the path to the uninstaller and the install path of the currently installed app so it can be uninstalled before installing the files of the new version
  StrCpy $CrtVersionUninstaller "$0"
  ReadRegStr $CrtVersionInstallFolder SHCTX "${UninstallRegistryRootKey}" "InstallLocation"

  ;Uninstall the currently installed app version, if it is the case
  ${If} $CrtVersionUninstaller != ""
    ;Invoke the uninstaller silently
    ClearErrors
    ExecWait '"$CrtVersionUninstaller" /S _?=$CrtVersionInstallFolder'
    ${IfNot} ${Errors}
      ;Manually delete the install folder and uninstaller .exe (the _? param specifies that it should be executed from the source folder and not copy itself in TEMP so it can delete the source folder).
      Delete "$CrtVersionUninstaller"
      RMDir "$CrtVersionInstallFolder"
    ${EndIf}
  ${EndIf}

  ;Extract the application files in the install destination folder
  SetOutPath "$INSTDIR"
  !insertmacro UNINSTALL.LOG_OPEN_INSTALL
    File /r "${InputDir}\App\"
  !insertmacro UNINSTALL.LOG_CLOSE_INSTALL

  ;Compute the installed size in $R9 so it can be written in the uninstall registry information
  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $R9 "0x%08X" $0
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    ;Create shortcuts (remove any that already exist)
    ${If} $StartMenuFolder != ""
      RMDir /r "$SMPROGRAMS\$StartMenuFolder"
    ${EndIf}
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${LnkName}" "$INSTDIR\${ExeName}"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${UninstallLnkName}" "${UNINST_EXE}"
  !insertmacro MUI_STARTMENU_WRITE_END

  ;Add uninstall information (for Control Panel -> Programs or Add/Remove Programs)
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayName" "${ApplicationName}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayIcon" "$INSTDIR\${ExeName}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "InstallLocation" "$INSTDIR"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "UninstallString" "${UNINST_EXE}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "QuietUninstallString" "$\"${UNINST_EXE}$\" /S"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "DisplayVersion" "${VERSION}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "URLInfoAbout" "${AppWebsite}"
  WriteRegStr SHCTX "${UninstallRegistryRootKey}" "Publisher" "${Company}"
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "EstimatedSize" "$R9"
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "NoModify" 1
  WriteRegDWORD SHCTX "${UninstallRegistryRootKey}" "NoRepair" 1

SectionEnd

;The .NET Framework 4.0 section
Section "$(COMPONENTS_DotNet)" DotNetFrameworkSection

  SetOutPath $PLUGINSDIR
  File "${InputDir}\Installers\dotNetFx40_Full_setup.exe"
  SetOutPath "$INSTDIR"

  DetailPrint "$(InstallingDotNet)"
  ExecWait "$PLUGINSDIR\dotNetFx40_Full_setup.exe /passive /norestart" $R0
  ${If} $R0 != 0
  ${AndIf} $R0 != 1641
  ${AndIf} $R0 != 3010
    MessageBox MB_OK|MB_ICONSTOP "$(DotNetInstallFailed)"
  ${Endif}

SectionEnd

;The Desktop Icon section
Section "$(COMPONENTS_DesktopIcon)" DesktopIconSection

  ;Create the desktop icon
  DetailPrint "$(CreatingDesktopShortcut)"
  CreateShortCut "$DESKTOP\${LnkName}" "$INSTDIR\${ExeName}"

SectionEnd

;The section that allows to select the file extensions to associate with the app
SectionGroup "$(COMPONENTS_FileTypeAssoc)" FileTypesAssociationsSection

  !insertmacro CreateFileAssociations

SectionGroupEnd

;This hidden section creates the app "profile" to which the file extensions selected above are linked, thus completing the file association process.
;It must always folow the 'FileTypesAssociationsSection' section.
Section "-RegisterFileExtensions" RegisterFileExtensionsSection

  !insertmacro CreateFileAssociationProfile

  ;Refresh the shell so the new file association icon(s) are displayed right away.
  !insertmacro RefreshShell

SectionEnd

;Hidden section that writes some settings in the app's settings file at the end of the installation process.
;This section should usually be last to make sure all setting related changes are written in the settings file.
Section "-Write settings" WriteSettingsSection

  DetailPrint "$(WritingSettings)"

  ;Write some of the data inputted during the installation into the application's settings file.
  ;If the file does not exist, create it; if necessary parts of it do not exist, create them.
  nsisXML::create
  nsisXML::load "${GlobalSettingsFile}"
  ${if} $0 == 0
    ;Failed to load the app settings file; the file does not exist or is invalid, either way we have to create a new settings file.
    nsisXML::create
    nsisXML::createProcessingInstruction "xml" 'version="1.0" encoding="utf-8"'
    nsisXML::appendChild
  ${endif}

  ;Save the current xml element in R1 before all operations that change $1
  StrCpy $R1 $1

  ;Select the SettingsFile root element. Create it if it does not exist.
  nsisXML::select '/SettingsFile'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the document element
    nsisXML::createElement "SettingsFile" ;create root element node (SettingsFile)
    nsisXML::appendChild                  ;add it to the document
    StrCpy $R1 $2                         ;move to the root element
  ${else}
    StrCpy $R1 $1    ; if the element exists, move to it
  ${endif}

  ;Select the Settings element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the SettingsFile element
    nsisXML::createElement "Settings"     ;create the Settings element
    nsisXML::appendChild                  ;add it to the SettingsFile element
    StrCpy $R1 $2                         ;move to the Settings element
  ${else}
    StrCpy $R1 $1    ; if the element exists, move to it
  ${endif}
  
  ;Select the StartInViewerMode Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="StartInViewerMode"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "StartInViewerMode"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}
  
  nsisXML::setText "True"
  
  ;Select the PCMViewerIsInstalled Setting element. Create it if it does not exist.
  nsisXML::select '/SettingsFile/Settings/Setting[@Name="PCMViewerIsInstalled"]/Value'
  ${if} $2 == 0
    StrCpy $1 $R1                         ;restore the Settings element
    nsisXML::createElement "Setting"      ;create the Setting element
    nsisXML::setAttribute "Name" "PCMViewerIsInstalled"
    nsisXML::setAttribute "Type" "System.Boolean"
    nsisXML::appendChild                  ;add the Setting element to the Settings element
    StrCpy $1 $2                          ;move to the Setting element
    nsisXML::createElement "Value"        ;create the Value element
    nsisXML::appendChild
  ${endif}
  
  nsisXML::setText "True"

  ;Log the creation of the global settings file, if it occurs.
  SetOutPath "$INSTDIR"
  !insertmacro UNINSTALL.LOG_OPEN_INSTALL
    nsisXML::save "${GlobalSettingsFile}"
  !insertmacro UNINSTALL.LOG_CLOSE_INSTALL

  ;Done with the app settings.
  ;Now write some data in the exe's config file (not app settings).
  nsisXML::create
  nsisXML::load "${ExeConfigPath}"
  ${if} $0 != 0
    ;Write the log file path
    nsisXML::select '/configuration/nlog/targets/target[@name="file"]'
    nsisXML::setAttribute "fileName" "$${specialfolder:folder=LocalApplicationData}\PCMData\ViewerApp.log"
    nsisXML::setAttribute "archiveFileName" "$${specialfolder:folder=LocalApplicationData}\PCMData\ViewerApp.{##}.log"

    nsisXML::save "${ExeConfigPath}"
  ${endif}

SectionEnd

;Hidden section that creates the log containing the installed files.
;THIS SECTION SHOULD ALWAYS BE LAST (in order to capture all the file related operations).
Section "-WriteFilesLog"

  SectionIn RO
  !insertmacro UNINSTALL.LOG_UPDATE_INSTALL

SectionEnd

;--------------------------------
;Installer Functions

Function ComponenetsPageLeaveFunction

  ;If the user does not have admin privileges and .NET Framework is not installed show a warning to tell him that .NET Framework needs to be installed in order to run the application.
  ${DetectDotNetFramework} $R1
  SectionGetFlags ${DotNetFrameworkSection} $R2

  ${If} $MultiUser.Privileges == Admin
  ${OrIf} $MultiUser.Privileges == Power
    IntOp $R3 $R2 & ${SF_SELECTED}
    ${If} $R3 != ${SF_SELECTED}             ;The ".NET Framework 4.0" component is not selected
      ${If} $R1 == ${DotNet_None}
        MessageBox MB_ICONSTOP "$(DotNetNotInstalled_HasAdminPermission)"
        Abort
      ${ElseIf} $R1 == ${DotNet_v4_Client}
        MessageBox MB_ICONSTOP "$(DotNet4FullNotInstalled_HasAdminPermission)"
        Abort
      ${EndIf}
    ${EndIf}
  ${Else}
    ;The user does not have Admin privileges -> .NET can not be installed (he ".NET Framework 4.0" component is disabled and un-selected)
    ${If} $R1 == ${DotNet_None}
      MessageBox MB_ICONSTOP "$(DotNetNotInstalled_DoesntHaveAdminPermission)"
      Abort
    ${ElseIf} $R1 == ${DotNet_v4_Client}
      MessageBox MB_ICONSTOP "$(DotNet4FullNotInstalled_DoesntHaveAdminPermission)"
      Abort
    ${EndIf}
  ${EndIf}

FunctionEnd

Function .onInit

  !insertmacro MULTIUSER_INIT
  !insertmacro UNINSTALL.LOG_PREPARE_INSTALL

  ${GetParameters} $R0
  ClearErrors

  ;check if the update mode is turned on
  ${GetOptions} $R0 /UPD $0
  IfErrors UpdateModeOff

  StrCpy $UpdateMode "1"
  
  ;get the dir where to install the update from the uninstall information of the currently installed version
  ReadRegStr $0 SHCTX "${UninstallRegistryRootKey}" "InstallLocation"
  ${if} $0 == ""
    SetErrorLevel 3
    goto AbortUpdate
  ${else}
    StrCpy $INSTDIR $0
  ${endif}

  ;Select/unselect sections to preserve the options made by the user on the previous installation.
  Call UpdateModeInitializeSections
  Goto Continue
  
  AbortUpdate:
    Abort
  
  UpdateModeOff:
    !insertmacro MUI_LANGDLL_DISPLAY
    StrCpy $UpdateMode "0"
    
    ;Disable and unselect the .NET Framework section if the user does not have admin privileges because the installation will not succeed.
    ${If} $MultiUser.Privileges != Admin
    ${AndIf} $MultiUser.Privileges != Power
      IntOp $0 0 | ${SF_RO}
      SectionSetFlags ${DotNetFrameworkSection} $0   ;.NET framework section
    ${Else}
      ;If .Net4 is already installed deselect it.
      ${DetectDotNetFramework} $R1
      ${If} $R1 == ${DotNet_v4_Full}
        !insertmacro UnselectSection ${DotNetFrameworkSection}
      ${EndIf}
    ${EndIf}

  Continue:

FunctionEnd

Function .onInstSuccess

  ;Start the app after a successful update
  ${If} $UpdateMode == 1
      Exec "$INSTDIR\${ExeName}"
  ${EndIf}

FunctionEnd


;Initializes the sections during the update mode
Function UpdateModeInitializeSections

  ;don't install the .NET framework
  !insertmacro UnselectSection ${DotNetFrameworkSection}
  !insertmacro UnselectSection ${DesktopIconSection}
  !insertmacro UnselectSection ${RegisterFileExtensionsSection}
  !insertmacro UnselectSection ${FileTypesAssociationsSection}

  ;The uninstaller of the current installed version is executed before installing the new version. This causes the desktop shortcut
  ;and file associations to be removed. Here we determine if a desktop shortcut was created and which file associations were created
  ;during the normal installation and activate their corresponding sections so they will be created again.
  ${If} ${FileExists} "$DESKTOP\${LnkName}"
    !insertmacro SelectSection ${DesktopIconSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.project" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocProjectSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.assembly" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocAssemblySec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.part" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocPartSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.rawpart" ""
  ${If} $0 == "ProductCostManagerFile"
    !insertmacro SelectSection ${FileAssocRawPartSec}
    !insertmacro SelectSection ${RegisterFileExtensionsSection}
  ${EndIf}

FunctionEnd

Function SkipPageInUpdateMode

  ${if} $UpdateMode == "1"
    Abort
  ${endif}
    
FunctionEnd

;--------------------------------
;Section Descriptions

  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${MainSection} $(DESC_MainSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${DotNetFrameworkSection} $(DESC_dotNetSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${DesktopIconSection} $(DESC_DesktopIconSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${FileTypesAssociationsSection} $(DESC_FileTypeAssoc)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  nsExec::Exec 'taskkill /f /im "${ExeName}"'
  Sleep 500 ; sleep necessary because sometimes the app dlls are not unlocked at the exact moment the Exec call returns

  ;Uninstall the app files
  !insertmacro UNINSTALL.LOG_BEGIN_UNINSTALL
  !insertmacro UNINSTALL.LOG_UNINSTALL "$INSTDIR"
  !insertmacro UNINSTALL.LOG_END_UNINSTALL

  ;Remove the shortcuts
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  ${If} $StartMenuFolder != ""
    RMDir /r "$SMPROGRAMS\$StartMenuFolder"
  ${EndIf}
  Delete "$DESKTOP\${LnkName}"

  ;Remove the uninstall registry key
  DeleteRegKey SHCTX "${UninstallRegistryRootKey}"

  !insertmacro DeleteFileAssociations

SectionEnd

Function un.onInit
  !insertmacro MULTIUSER_UNINIT
FunctionEnd