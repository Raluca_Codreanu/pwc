
!ifndef COMMON_INCLUDED
  !define COMMON_INCLUDED
  
!macro DetectSqlcmdPathMACRO un
;Detect the sqlcmd.exe path on both windows 32 and 64 bit. Returns the path in $0.
;TODO: rewrite the function as a macro
Function ${un}DetectSqlcmdPath

    ;Check the 32 bit registry
    SetRegView 32
    Call ${un}FindSqlcmdPath

    ;If the 32 bit search yielded nothing, check the 64 bit registry
    ${if} $0 == ""
        SetRegView 64
        Call ${un}FindSqlcmdPath
        SetRegView 32
    ${endif}
    
FunctionEnd
!macroend

!macro FindSqlcmdPathMACRO un
;Find the path where sqlcmd.exe is (usualy is in SqlServerFolder\90|100\Tools\binn)
Function ${un}FindSqlcmdPath
    Push $R0
    StrCpy $0 ""

    ;First try to find sqlcmd.exe of sql server 2005
    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\90\Tools\ClientSetup" "Path"
    ${if} ${FileExists} "$R0\sqlcmd.exe"
        StrCpy $0 $R0
    ${else}
        ;Try to find sqlcmd.exe of sql server 2008
        ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\100\Tools\ClientSetup" "Path"
        ${if} ${FileExists} "$R0\SQLCMD.EXE"
              StrCpy $0 $R0
        ${else}
            ;Try to find sqlcmd.exe of sql server 2012
	        ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\110\Tools\ClientSetup" "Path"
	        ${if} ${FileExists} "$R0\SQLCMD.EXE"
		        StrCpy $0 $R0
	        ${else}
	            ;Try to find sqlcmd.exe of sql server 2014
                ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\120\Tools\ClientSetup" "Path"
                ${if} ${FileExists} "$R0\SQLCMD.EXE"
                    StrCpy $0 $R0
                ${else}
                    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\120\Tools\ClientSetup" "ODBCToolsPath"
                    ${if} ${FileExists} "$R0\SQLCMD.EXE"
                        StrCpy $0 $R0
                    ${else}
		                ;Try to find sqlcmd.exe of sql server 2016
                        ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\130\Tools\ClientSetup" "Path"
		                ${if} ${FileExists} "$R0\SQLCMD.EXE"
		                    StrCpy $0 $R0
                        ${else}
                            ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\130\Tools\ClientSetup" "ODBCToolsPath"
		                    ${if} ${FileExists} "$R0\SQLCMD.EXE"
		                        StrCpy $0 $R0
							${else}
								;Try to find sqlcmd.exe of sql server 2017
								ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\140\Tools\ClientSetup" "Path"
								${if} ${FileExists} "$R0\SQLCMD.EXE"
									StrCpy $0 $R0
								${else}
									ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\140\Tools\ClientSetup" "ODBCToolsPath"
									${if} ${FileExists} "$R0\SQLCMD.EXE"
										StrCpy $0 $R0
									${else}
									    ;Try to find sqlcmd.exe of sql server 2019
										ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\150\Tools\ClientSetup" "Path"
										${if} ${FileExists} "$R0\SQLCMD.EXE"
											StrCpy $0 $R0
										${else}
											ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Microsoft SQL Server\150\Tools\ClientSetup" "ODBCToolsPath"
											${if} ${FileExists} "$R0\SQLCMD.EXE"
												StrCpy $0 $R0											
											${endif}
										${endif}
									${endif}
								${endif}
		                    ${endif}
		                ${endif}
		            ${endif}
	            ${endif}
	        ${endif}
	    ${endif}
    ${endif}

    Pop $R0

FunctionEnd
!macroend

;Refresh the shell so the new file association icon(s) are displayed right away.
!macro RefreshShell
  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'
!macroend

;Associate file extensions with the app. All files use the same 'ProductCostManagerFile' association.
!macro CreateFileAssociations
  Section ".folder" FileAssocFolderSec
    WriteRegStr SHCTX "Software\Classes\.folder" "" "ProductCostManagerFile"
  SectionEnd

  Section ".project" FileAssocProjectSec
    WriteRegStr SHCTX "Software\Classes\.project" "" "ProductCostManagerFile"
  SectionEnd

  Section ".assembly" FileAssocAssemblySec
    WriteRegStr SHCTX "Software\Classes\.assembly" "" "ProductCostManagerFile"
  SectionEnd

  Section ".part" FileAssocPartSec
    WriteRegStr SHCTX "Software\Classes\.part" "" "ProductCostManagerFile"
  SectionEnd

  Section ".rawpart" FileAssocRawPartSec
    WriteRegStr SHCTX "Software\Classes\.rawpart" "" "ProductCostManagerFile"
  SectionEnd
!macroend

;Creates the app "profile" to which the file extensions selected above are linked, thus completing the file association process.
;It must always folow the 'FileTypesAssociationsSection' section.
!macro CreateFileAssociationProfile
  WriteRegStr SHCTX "Software\Classes\ProductCostManagerFile" "" "Product Cost Manager File"
  WriteRegStr SHCTX "Software\Classes\ProductCostManagerFile\DefaultIcon" "" "$INSTDIR\${ExeName},0"
  WriteRegStr SHCTX "Software\Classes\ProductCostManagerFile\shell" "" "open"
  WriteRegStr SHCTX "Software\Classes\ProductCostManagerFile\shell\open\command" "" '"$INSTDIR\${ExeName}" "%1"'
!macroend

;Delete the application's file associations
!macro DeleteFileAssociations
  ;Delete the application's file associations
  ReadRegStr $0 SHCTX "Software\Classes\.folder" ""
  ${If} $0 == "ProductCostManagerFile"
    DeleteRegKey SHCTX "Software\Classes\.folder"
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.project" ""
  ${If} $0 == "ProductCostManagerFile"
    DeleteRegKey SHCTX "Software\Classes\.project"
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.assembly" ""
  ${If} $0 == "ProductCostManagerFile"
    DeleteRegKey SHCTX "Software\Classes\.assembly"
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.part" ""
  ${If} $0 == "ProductCostManagerFile"
    DeleteRegKey SHCTX "Software\Classes\.part"
  ${EndIf}

  ReadRegStr $0 SHCTX "Software\Classes\.rawpart" ""
  ${If} $0 == "ProductCostManagerFile"
    DeleteRegKey SHCTX "Software\Classes\.rawpart"
  ${EndIf}

  DeleteRegKey SHCTX "Software\Classes\ProductCostManagerFile"

  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.folder"
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.project"
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.assembly"
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.part"
  DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.rawpart"

  DeleteRegKey HKCR ".folder"
  DeleteRegKey HKCR ".project"
  DeleteRegKey HKCR ".assembly"
  DeleteRegKey HKCR ".part"
  DeleteRegKey HKCR ".rawpart"

  DeleteRegKey HKCR "folder_auto_file"
  DeleteRegKey HKCR "project_auto_file"
  DeleteRegKey HKCR "assembly_auto_file"
  DeleteRegKey HKCR "part_auto_file"
  DeleteRegKey HKCR "rawpart_auto_file"

  !insertmacro RefreshShell
!macroend

!define DetectDotNetFramework "!insertmacro DetectDotNet"

!define DotNet_None        "0"
!define DotNet_v4_Client   "1"
!define DotNet_v4_Full     "2"

; Return values:
;   - 0: .NET 4 not installed
;   - 1: .NET 4 Client installed
;   - 2: .NET 4 Full installed
var DetectedDotNetVer
!macro DetectDotNet DetectedVersion
  Push $R0
  StrCpy $DetectedDotNetVer ${DotNet_None}
  
  ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Install"
  ${If} $R0 = 1
    StrCpy $DetectedDotNetVer ${DotNet_v4_Full}
  ${Else}
    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client" "Install"
    ${If} $R0 = 1
      StrCpy $DetectedDotNetVer ${DotNet_v4_Client}
    ${EndIf}
  ${EndIf}
  
  Pop $R0
  StrCpy ${DetectedVersion} $DetectedDotNetVer
!macroend

; All content should be added above this line
!endif  ;!ifndef COMMON_INCLUDED

