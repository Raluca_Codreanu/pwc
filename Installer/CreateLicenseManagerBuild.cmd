@echo off

REM build_number is set by TeamCity
SET RELEASE_FOLDER=%build_number%
if "%RELEASE_FOLDER%" == "" GOTO ReleaseFolderNotSet


SET ZIP_PATH=""
IF EXIST "C:\Program Files\7-Zip\7z.exe" ( 
	SET ZIP_PATH="C:\Program Files\7-Zip\7z.exe" 
) ELSE IF EXIST "C:\Program Files (x86)\7-Zip\7z.exe" (	
	SET ZIP_PATH="C:\Program Files (x86)\7-Zip\7z.exe" )

IF %ZIP_PATH% == "" GOTO ZipNotFound

REM the folder containing the License Manager project build output (the compiled License Manager app)
SET BUILD_DIR=..\LicenseManager\ZPKTool.LicenseManagerUI\bin\Release

REM the folders containing the app files to be included in the release pack
SET INPUT_DIR=.\LicenseManagerInput
rmdir /s /q %INPUT_DIR%
mkdir %INPUT_DIR%

REM the folder where to put the release pack
SET OUTPUT_DIR=.\Output\LicenseManager
rmdir /s /q %OUTPUT_DIR%
mkdir %OUTPUT_DIR%

REM build the list of files and file patterns to be excluded from the build source
set BUILD_EXCLUDED_FILES=
setlocal EnableDelayedExpansion
for /f "delims=" %%v in (BuildDirExcludedFiles.txt) do (
 if {!BUILD_EXCLUDED_FILES!} EQU {} (set BUILD_EXCLUDED_FILES="%%v") else (set BUILD_EXCLUDED_FILES=!BUILD_EXCLUDED_FILES! "%%v")
)
setlocal DisableDelayedExpansion

REM The list of directories to be excluded from the build source
set BUILD_EXCLUDED_DIRS=.svn Logs

REM copy the app binaries to the input dir and archive them (zip) in the output dir
robocopy %BUILD_DIR% %INPUT_DIR%\LicenseManager /e /xf %BUILD_EXCLUDED_FILES% /xd %BUILD_EXCLUDED_DIRS%
%ZIP_PATH% a -tzip %OUTPUT_DIR%\LicenseManager.zip %INPUT_DIR%\LicenseManager
IF NOT %ERRORLEVEL% == 0 GOTO ZipError
rmdir /s /q %INPUT_DIR%

REM checkout the License Manager Releases folder from svn at revision 731 so it does not download all releases
rmdir /s /q .\LicenseManagerReleasesSvn
echo Checkout the releases root folder from svn
svn checkout -r 731 --username stefant https://svn.fortech.ro/ZPKToolTrac/svn/Deliveries/LicenseManager .\LicenseManagerReleasesSvn
IF NOT %ERRORLEVEL% == 0 GOTO SvnFail

echo Create release folder and copy the build into it
mkdir ".\LicenseManagerReleasesSvn\%RELEASE_FOLDER%"
xcopy /Y /R /S /I .\"%OUTPUT_DIR%"\* ".\LicenseManagerReleasesSvn\%RELEASE_FOLDER%\"

echo Add the release folder to svn and commit the changes
svn add ".\LicenseManagerReleasesSvn\%RELEASE_FOLDER%"
svn commit ".\LicenseManagerReleasesSvn" -m "License manager release %build_number%  was created (automated operation)."
IF NOT %ERRORLEVEL% == 0 GOTO SvnFail

GOTO End

:ReleaseFolderNotSet
echo ##teamcity[buildStatus status='FAILURE' text='{build.status.text}: the release folder name was not specified.']
EXIT /B 1

:ZipNotFound
echo ##teamcity[buildStatus status='FAILURE' text='{build.status.text}: 7Zip was not found in Program Files.']
EXIT /B 1

:ZipError
echo ##teamcity[buildStatus status='FAILURE' text='{build.status.text}: an error occurred while archiving the License Manager binaries.']
EXIT /B 1

:SvnFail
echo ##teamcity[buildStatus status='FAILURE' text='{build.status.text}: failed to copy the release to SVN.']
EXIT /B %ERRORLEVEL%

:End
echo Cleanup temporary files
rmdir /s /q .\LicenseManagerReleasesSvn
rmdir /s /q %OUTPUT_DIR%

EXIT /B %ERRORLEVEL%