@echo off

REM	The parameters expected by the batch file are:
REM	%1 - the app version number in the N.N.N.N format
REM

SET APP_VERSION=%1
IF NOT "%APP_VERSION%" == "" GOTO Start
echo "The 1st parameter (app version) was not specified."
GOTO End

:Start

REM the folder containing the PCM project build output (the compiled app)
SET BUILD_DIR=..\ZPKTool\ZPKTool.Gui\bin\Release

REM the folders containing the app files to be included in the installers
SET INPUT_DIR=.\Input
SET VIEWER_INPUT_DIR=.\InputViewer

REM the folder where to put the installers
SET OUTPUT_DIR=.\Output\ProductCostManager

REM copy the pdb files to the installers output dir
rmdir /s /q %OUTPUT_DIR%
mkdir %OUTPUT_DIR%
robocopy %BUILD_DIR% %OUTPUT_DIR%\Pdb *.pdb

REM build the list of files and file patterns to be excluded from the build source
set BUILD_EXCLUDED_FILES=
setlocal EnableDelayedExpansion
for /f "delims=" %%v in (BuildDirExcludedFiles.txt) do (
 if {!BUILD_EXCLUDED_FILES!} EQU {} (set BUILD_EXCLUDED_FILES="%%v") else (set BUILD_EXCLUDED_FILES=!BUILD_EXCLUDED_FILES! "%%v")
)
setlocal DisableDelayedExpansion

REM The list of directories to be excluded from the build source
set BUILD_EXCLUDED_DIRS=.svn Logs

REM copy the app files to the installer input dir
rmdir /s /q %INPUT_DIR%
robocopy %BUILD_DIR% %INPUT_DIR%\App /e /xf %BUILD_EXCLUDED_FILES% /xd %BUILD_EXCLUDED_DIRS%

REM copy the database related files to the app installer input dir
robocopy ..\ZPKTool\ZPKTool.Database\bin\Release %INPUT_DIR%\LocalDbPackage\Database /e /xf %BUILD_EXCLUDED_FILES% /xd %BUILD_EXCLUDED_DIRS%
robocopy ..\3rdParty\DAC %INPUT_DIR%\LocalDbPackage\DeploymentUtility /e
robocopy ..\ZPKTool\ZPKTool.Database.Central\bin\Release %INPUT_DIR%\CentralDbPackage\Database /e /xf %BUILD_EXCLUDED_FILES% /xd %BUILD_EXCLUDED_DIRS%
robocopy ..\3rdParty\DAC %INPUT_DIR%\CentralDbPackage\DeploymentUtility /e

REM Copy 3rd party installers in a separate folder to be extracted by the installer in temp
xcopy /Y ..\3rdParty\dotNET_4.0_Redist\dotNetFx40_Full_setup.exe %INPUT_DIR%\Installers\
xcopy /Y ..\3rdParty\SyncFramework\Redist\* %INPUT_DIR%\Installers\SyncFrameworkRedist\

REM Copy utilities in a separate folder to be extracted by the installer in temp
xcopy /Y ..\DataEncryption\DataEncryption\bin\Release\DataEncryption.exe %INPUT_DIR%\Installers\Utilities\
xcopy /Y ..\DocumentsCompression\DocumentsCompression\bin\Release\DocumentsCompression.exe %INPUT_DIR%\Installers\Utilities\
xcopy /Y ..\InstallerHelper\InstallerHelper\bin\Release\*.exe %INPUT_DIR%\Installers\InstallerHelper\
xcopy /Y ..\InstallerHelper\InstallerHelper\bin\Release\*.dll %INPUT_DIR%\Installers\InstallerHelper\
xcopy /Y ..\InstallerHelper\InstallerHelper\bin\Release\*.config %INPUT_DIR%\Installers\InstallerHelper\

REM Copy licenses
xcopy /Y "..\3rdParty\AvalonDock\AvalonDock License.txt" %INPUT_DIR%\App\Licenses\
xcopy /Y "..\3rdParty\FluidKit\FluidKit License.txt" %INPUT_DIR%\App\Licenses\
xcopy /Y "..\3rdParty\iTextSharp\iTextSharp License.txt" %INPUT_DIR%\App\Licenses\
xcopy /Y "..\3rdParty\NPOI\NPOI License.txt" %INPUT_DIR%\App\Licenses\
xcopy /Y "..\3rdParty\NPOI\Ionic_Utils_Zip_license.txt" %INPUT_DIR%\App\Licenses\
xcopy /Y ".\src\EULA.rtf" "%INPUT_DIR%\App\Licenses\"
rename "%INPUT_DIR%\App\Licenses\EULA.rtf" "Product Cost Manager License.rtf"

xcopy /Y "ReleaseNotes\ReleaseNotes.pdf" %INPUT_DIR%\App\
xcopy /Y "ReleaseNotes\ReleaseNotes.rtf" %INPUT_DIR%\App\

xcopy /Y "ConfigFiles\AdvancedSearchConfigurations.xml" %INPUT_DIR%\ConfigFiles\

REM copy the necessary files to the viewer installer input dir
rmdir /s /q %VIEWER_INPUT_DIR%
robocopy %BUILD_DIR% %VIEWER_INPUT_DIR%\App /e /xf %BUILD_EXCLUDED_FILES% /xd %BUILD_EXCLUDED_DIRS%
xcopy /Y ..\3rdParty\dotNET_4.0_Redist\dotNetFx40_Full_setup.exe %VIEWER_INPUT_DIR%\Installers\
robocopy %INPUT_DIR%\App\Licenses %VIEWER_INPUT_DIR%\App\Licenses *.* /e

REM get the database version
FOR /f %%i in ('..\InstallerHelper\InstallerHelper\bin\Release\InstallerHelper.exe adbv') DO SET DbVersion=%%i

@echo on
..\3rdParty\NSIS\makensis /Dversion=%APP_VERSION% /Ddbversion=%DbVersion% .\Src\PCMInstallerScript.nsi
..\3rdParty\NSIS\makensis /Dversion=%APP_VERSION% /Ddbversion=%DbVersion% .\Src\CentralDbInstallerScript.nsi
..\3rdParty\NSIS\makensis /Dversion=%APP_VERSION% .\Src\PCMViewerInstallerScript.nsi
call BuildViewerPack.cmd %APP_VERSION%
@echo off

RMDir /s /q %INPUT_DIR%\
RMDir /s /q %VIEWER_INPUT_DIR%\

IF NOT %ERRORLEVEL% == 0 GOTO Fail
goto End

:Fail
RMDir /s /q %OUTPUT_DIR%

:End
echo Product Cost Manager installer created.