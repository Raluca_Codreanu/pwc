@echo off

REM Build the application using the Release configuration
SET VS_PATH=
IF EXIST "c:\Program Files\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com"	(	
	SET VS_PATH="c:\Program Files\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com"
) ELSE IF EXIST "c:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com" (	
	SET VS_PATH="c:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.com" 
) ELSE IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\devenv.com" (	
	SET VS_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\devenv.com" 
)

@echo on
%VS_PATH% "..\ZPKTool\ZPKTool.sln" /Rebuild "Release|Any CPU"
%VS_PATH% "..\DataEncryption\DataEncryption.sln" /Rebuild "Release|Any CPU"
%VS_PATH% "..\DocumentsCompression\DocumentsCompression.sln" /Rebuild "Release|Any CPU"
%VS_PATH% "..\InstallerHelper\InstallerHelper.sln" /Rebuild "Release|Any CPU"
@echo off
if NOT %ERRORLEVEL% == 0 GOTO BuildError

REM Build the installer
SET VERSION=%1
IF "%VERSION%"=="" SET VERSION=1.0.0.0
CALL BuildPCMInstaller.cmd %VERSION%
GOTO End

:BuildError
echo Build failed.
GOTO End

:End
echo Done.