IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'a80c340f-01b9-47ec-8b7f-b644d33cc748')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Algerian Dinar', N'DZD', N'117.815', N'DZD', 1, 1, 'a80c340f-01b9-47ec-8b7f-b644d33cc748')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Algerian Dinar', Symbol=N'DZD', ExchangeRate=N'117.815', IsoCode=N'DZD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'a80c340f-01b9-47ec-8b7f-b644d33cc748'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'd8b5f7d7-d9c1-4a11-a698-73b69d189125')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Argentine Peso', N'ARS', N'10.3982', N'ARS', 1, 1, 'd8b5f7d7-d9c1-4a11-a698-73b69d189125')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Argentine Peso', Symbol=N'ARS', ExchangeRate=N'10.3982', IsoCode=N'ARS', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'd8b5f7d7-d9c1-4a11-a698-73b69d189125'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '35f04577-7869-48eb-b599-9529086e9460')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Australian Dollar', N'AUD', N'1.60952', N'AUD', 1, 1, '35f04577-7869-48eb-b599-9529086e9460')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Australian Dollar', Symbol=N'AUD', ExchangeRate=N'1.60952', IsoCode=N'AUD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '35f04577-7869-48eb-b599-9529086e9460'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '3f1a8f1d-a2d9-45f9-90c7-00e48197068e')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Belarussian Ruble', N'BYR', N'19700.9', N'BYR', 1, 1, '3f1a8f1d-a2d9-45f9-90c7-00e48197068e')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Belarussian Ruble', Symbol=N'BYR', ExchangeRate=N'19700.9', IsoCode=N'BYR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '3f1a8f1d-a2d9-45f9-90c7-00e48197068e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'fdc97b5d-fcc1-4d23-b630-427f90f37499')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Bosnian Mark', N'BAM', N'1.95545', N'BAM', 1, 1, 'fdc97b5d-fcc1-4d23-b630-427f90f37499')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Bosnian Mark', Symbol=N'BAM', ExchangeRate=N'1.95545', IsoCode=N'BAM', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'fdc97b5d-fcc1-4d23-b630-427f90f37499'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '210b727c-ea8c-4591-8b07-ac4e7823c8bc')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Brazilian Real', N'BRL', N'4.28088', N'BRL', 1, 1, '210b727c-ea8c-4591-8b07-ac4e7823c8bc')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Brazilian Real', Symbol=N'BRL', ExchangeRate=N'4.28088', IsoCode=N'BRL', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '210b727c-ea8c-4591-8b07-ac4e7823c8bc'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '9025593a-2a7a-45a9-a8eb-7ce7568b4188')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'British Pound', N'£', N'0.73232', N'GBP', 1, 1, '9025593a-2a7a-45a9-a8eb-7ce7568b4188')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'British Pound', Symbol=N'£', ExchangeRate=N'0.73232', IsoCode=N'GBP', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '9025593a-2a7a-45a9-a8eb-7ce7568b4188'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '345e7604-2707-43d7-8d6f-e6ab617416b3')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Bulgarian lev', N'BGN', N'1.95536', N'BGN', 1, 1, '345e7604-2707-43d7-8d6f-e6ab617416b3')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Bulgarian lev', Symbol=N'BGN', ExchangeRate=N'1.95536', IsoCode=N'BGN', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '345e7604-2707-43d7-8d6f-e6ab617416b3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '23cf775f-59bd-4a2c-9c8f-cd1ac41b4a91')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Canadian Dollar', N'CAD', N'1.48118', N'CAD', 1, 1, '23cf775f-59bd-4a2c-9c8f-cd1ac41b4a91')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Canadian Dollar', Symbol=N'CAD', ExchangeRate=N'1.48118', IsoCode=N'CAD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '23cf775f-59bd-4a2c-9c8f-cd1ac41b4a91'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '502c1724-6bea-4465-85b3-0af88f673bd0')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Chile Peso', N'CLP', N'773.5', N'CLP', 1, 1, '502c1724-6bea-4465-85b3-0af88f673bd0')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Chile Peso', Symbol=N'CLP', ExchangeRate=N'773.5', IsoCode=N'CLP', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '502c1724-6bea-4465-85b3-0af88f673bd0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '549050bb-9a5b-46e1-83e6-59a5eadc1ade')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Chinese yuan', N'CNY', N'7.09059', N'CNY', 1, 1, '549050bb-9a5b-46e1-83e6-59a5eadc1ade')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Chinese yuan', Symbol=N'CNY', ExchangeRate=N'7.09059', IsoCode=N'CNY', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '549050bb-9a5b-46e1-83e6-59a5eadc1ade'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '301843b3-b371-4cec-a2ee-747b2a226108')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Colombian Peso', N'COP', N'3425.82', N'COP', 1, 1, '301843b3-b371-4cec-a2ee-747b2a226108')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Colombian Peso', Symbol=N'COP', ExchangeRate=N'3425.82', IsoCode=N'COP', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '301843b3-b371-4cec-a2ee-747b2a226108'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '236e873e-294d-40a7-81a2-7b4811c137b2')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Croatian Kuna', N'HRK', N'7.5566', N'HRK', 1, 1, '236e873e-294d-40a7-81a2-7b4811c137b2')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Croatian Kuna', Symbol=N'HRK', ExchangeRate=N'7.5566', IsoCode=N'HRK', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '236e873e-294d-40a7-81a2-7b4811c137b2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '7b171e1b-5b9c-4800-9200-4c2286c0e7ed')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Czech Republic Koruna', N'CZK', N'27.0203', N'CZK', 1, 1, '7b171e1b-5b9c-4800-9200-4c2286c0e7ed')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Czech Republic Koruna', Symbol=N'CZK', ExchangeRate=N'27.0203', IsoCode=N'CZK', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '7b171e1b-5b9c-4800-9200-4c2286c0e7ed'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'aba4596b-5e69-4c88-b071-0fadcfd2e981')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Danish Krone', N'DKK', N'7.4613', N'DKK', 1, 1, 'aba4596b-5e69-4c88-b071-0fadcfd2e981')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Danish Krone', Symbol=N'DKK', ExchangeRate=N'7.4613', IsoCode=N'DKK', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'aba4596b-5e69-4c88-b071-0fadcfd2e981'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '27274504-9de6-48ce-acd2-2c97c853e02a')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Euro', N'€', N'1', N'EUR', 1, 1, '27274504-9de6-48ce-acd2-2c97c853e02a')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Euro', Symbol=N'€', ExchangeRate=N'1', IsoCode=N'EUR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '27274504-9de6-48ce-acd2-2c97c853e02a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'ef639f1a-578f-4201-9d1d-dc5ae816b861')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Hong Kong Dollar', N'HKD', N'8.64513', N'HKD', 1, 1, 'ef639f1a-578f-4201-9d1d-dc5ae816b861')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Hong Kong Dollar', Symbol=N'HKD', ExchangeRate=N'8.64513', IsoCode=N'HKD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'ef639f1a-578f-4201-9d1d-dc5ae816b861'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'f5d81604-7186-41a8-b71d-c4aa203920e9')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Hungarian Forint', N'HUF', N'313.838', N'HUF', 1, 1, 'f5d81604-7186-41a8-b71d-c4aa203920e9')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Hungarian Forint', Symbol=N'HUF', ExchangeRate=N'313.838', IsoCode=N'HUF', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'f5d81604-7186-41a8-b71d-c4aa203920e9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '16c4140b-213a-469b-bbf9-3610f0ba8797')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Indian rupees', N'INR', N'74.4283', N'INR', 1, 1, '16c4140b-213a-469b-bbf9-3610f0ba8797')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Indian rupees', Symbol=N'INR', ExchangeRate=N'74.4283', IsoCode=N'INR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '16c4140b-213a-469b-bbf9-3610f0ba8797'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '0360eb53-ecab-4165-af26-be072da35130')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Indonesian rupiahs', N'IDR', N'15646', N'IDR', 1, 1, '0360eb53-ecab-4165-af26-be072da35130')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Indonesian rupiahs', Symbol=N'IDR', ExchangeRate=N'15646', IsoCode=N'IDR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '0360eb53-ecab-4165-af26-be072da35130'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '31656899-a465-4ec3-9bdb-e6db66d313ce')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Israeli New Shekel', N'ILS', N'4.37874', N'ILS', 1, 1, '31656899-a465-4ec3-9bdb-e6db66d313ce')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Israeli New Shekel', Symbol=N'ILS', ExchangeRate=N'4.37874', IsoCode=N'ILS', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '31656899-a465-4ec3-9bdb-e6db66d313ce'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'a38926dc-4d15-41dc-ae37-8fe2ff97c824')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Japanese yen', N'JPY', N'133.08', N'JPY', 1, 1, 'a38926dc-4d15-41dc-ae37-8fe2ff97c824')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Japanese yen', Symbol=N'JPY', ExchangeRate=N'133.08', IsoCode=N'JPY', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'a38926dc-4d15-41dc-ae37-8fe2ff97c824'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'd7e2af7e-bd32-4500-8349-0e0a2736fc51')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Latvian lats', N'LVL', N'0.70269', N'LVL', 1, 1, 'd7e2af7e-bd32-4500-8349-0e0a2736fc51')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Latvian lats', Symbol=N'LVL', ExchangeRate=N'0.70269', IsoCode=N'LVL', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'd7e2af7e-bd32-4500-8349-0e0a2736fc51'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '5e9c4cb4-6e58-4e1a-b12d-5da40ffdf61a')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Lithuanian litas', N'LTL', N'3.4528', N'LTL', 1, 1, '5e9c4cb4-6e58-4e1a-b12d-5da40ffdf61a')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Lithuanian litas', Symbol=N'LTL', ExchangeRate=N'3.4528', IsoCode=N'LTL', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '5e9c4cb4-6e58-4e1a-b12d-5da40ffdf61a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '15a60f8c-b5a2-4122-b991-1af0077a6b0e')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Malaysian Ringgit', N'MYR', N'4.8191', N'MYR', 1, 1, '15a60f8c-b5a2-4122-b991-1af0077a6b0e')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Malaysian Ringgit', Symbol=N'MYR', ExchangeRate=N'4.8191', IsoCode=N'MYR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '15a60f8c-b5a2-4122-b991-1af0077a6b0e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'da76fbd7-82e3-49e2-b960-9be5da0e2949')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Mexican Peso', N'MXN', N'18.9009', N'MXN', 1, 1, 'da76fbd7-82e3-49e2-b960-9be5da0e2949')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Mexican Peso', Symbol=N'MXN', ExchangeRate=N'18.9009', IsoCode=N'MXN', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'da76fbd7-82e3-49e2-b960-9be5da0e2949'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'f57315b3-ad03-46c7-9564-57c5f485a9ba')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Norwegian Krone', N'NOK', N'9.26169', N'NOK', 1, 1, 'f57315b3-ad03-46c7-9564-57c5f485a9ba')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Norwegian Krone', Symbol=N'NOK', ExchangeRate=N'9.26169', IsoCode=N'NOK', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'f57315b3-ad03-46c7-9564-57c5f485a9ba'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'ceaed6c8-ebae-49ba-8200-8ecffdec0162')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Old Turkish Lira', N'TRY', N'3.38089', N'TRY', 1, 1, 'ceaed6c8-ebae-49ba-8200-8ecffdec0162')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Old Turkish Lira', Symbol=N'TRY', ExchangeRate=N'3.38089', IsoCode=N'TRY', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'ceaed6c8-ebae-49ba-8200-8ecffdec0162'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '296b656e-3b5a-4170-9258-d1a63f68f546')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Panama Balboa', N'PAB', N'1.11556', N'PAB', 1, 1, '296b656e-3b5a-4170-9258-d1a63f68f546')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Panama Balboa', Symbol=N'PAB', ExchangeRate=N'1.11556', IsoCode=N'PAB', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '296b656e-3b5a-4170-9258-d1a63f68f546'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'a0e5548f-ca4c-4285-8a97-084436cce682')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Peru Sol', N'PEN', N'3.50395', N'PEN', 1, 1, 'a0e5548f-ca4c-4285-8a97-084436cce682')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Peru Sol', Symbol=N'PEN', ExchangeRate=N'3.50395', IsoCode=N'PEN', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'a0e5548f-ca4c-4285-8a97-084436cce682'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'c60f7569-383e-4e80-834e-f33455988942')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Philippine Peso', N'PHP', N'52.2986', N'PHP', 1, 1, 'c60f7569-383e-4e80-834e-f33455988942')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Philippine Peso', Symbol=N'PHP', ExchangeRate=N'52.2986', IsoCode=N'PHP', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'c60f7569-383e-4e80-834e-f33455988942'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '4008af4c-84f5-4670-86eb-19bb6ad3faa8')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Polish Zloty', N'PLN', N'4.2247', N'PLN', 1, 1, '4008af4c-84f5-4670-86eb-19bb6ad3faa8')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Polish Zloty', Symbol=N'PLN', ExchangeRate=N'4.2247', IsoCode=N'PLN', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '4008af4c-84f5-4670-86eb-19bb6ad3faa8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '1332f371-ce11-45e5-91ee-db06b1bbc168')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Romanian Leu', N'RON', N'4.42448', N'RON', 1, 1, '1332f371-ce11-45e5-91ee-db06b1bbc168')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Romanian Leu', Symbol=N'RON', ExchangeRate=N'4.42448', IsoCode=N'RON', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '1332f371-ce11-45e5-91ee-db06b1bbc168'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'b551ece6-f8a3-40e8-811e-69336c1449be')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Russian Ruble', N'RUB', N'76.5394', N'RUB', 1, 1, 'b551ece6-f8a3-40e8-811e-69336c1449be')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Russian Ruble', Symbol=N'RUB', ExchangeRate=N'76.5394', IsoCode=N'RUB', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'b551ece6-f8a3-40e8-811e-69336c1449be'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '7bd59d95-dfed-4d75-abcf-6c166ad469cd')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Singapore Dollar', N'SGD', N'1.5909', N'SGD', 1, 1, '7bd59d95-dfed-4d75-abcf-6c166ad469cd')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Singapore Dollar', Symbol=N'SGD', ExchangeRate=N'1.5909', IsoCode=N'SGD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '7bd59d95-dfed-4d75-abcf-6c166ad469cd'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '1396bcd7-2988-4f36-a427-911cd2c7e31f')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'South African Rand', N'ZAR', N'15.5208', N'ZAR', 1, 1, '1396bcd7-2988-4f36-a427-911cd2c7e31f')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'South African Rand', Symbol=N'ZAR', ExchangeRate=N'15.5208', IsoCode=N'ZAR', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '1396bcd7-2988-4f36-a427-911cd2c7e31f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '9e74c06b-061e-408a-b32f-00b2c5c9fb6b')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'South Korean Won', N'KRW', N'1340.33', N'KRW', 1, 1, '9e74c06b-061e-408a-b32f-00b2c5c9fb6b')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'South Korean Won', Symbol=N'KRW', ExchangeRate=N'1340.33', IsoCode=N'KRW', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '9e74c06b-061e-408a-b32f-00b2c5c9fb6b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'a20fa490-4753-454e-b6a4-983f913a0a3f')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Swedish Krona', N'SEK', N'9.41862', N'SEK', 1, 1, 'a20fa490-4753-454e-b6a4-983f913a0a3f')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Swedish Krona', Symbol=N'SEK', ExchangeRate=N'9.41862', IsoCode=N'SEK', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'a20fa490-4753-454e-b6a4-983f913a0a3f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '6b3d7733-8fb5-4c8b-bf22-18b358289b19')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Swiss Franc', N'CHF', N'1.08599', N'CHF', 1, 1, '6b3d7733-8fb5-4c8b-bf22-18b358289b19')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Swiss Franc', Symbol=N'CHF', ExchangeRate=N'1.08599', IsoCode=N'CHF', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '6b3d7733-8fb5-4c8b-bf22-18b358289b19'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '53b9557c-6b56-4754-80f5-dd67705faa40')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Taiwanese U.S. dollars', N'TWD', N'36.4363', N'TWD', 1, 1, '53b9557c-6b56-4754-80f5-dd67705faa40')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Taiwanese U.S. dollars', Symbol=N'TWD', ExchangeRate=N'36.4363', IsoCode=N'TWD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '53b9557c-6b56-4754-80f5-dd67705faa40'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'd6115dea-95ea-4e47-8667-bb889ba9b102')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Thai Baht', N'THB', N'40.1622', N'THB', 1, 1, 'd6115dea-95ea-4e47-8667-bb889ba9b102')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Thai Baht', Symbol=N'THB', ExchangeRate=N'40.1622', IsoCode=N'THB', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'd6115dea-95ea-4e47-8667-bb889ba9b102'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'b00d2f66-c397-4f50-b4f2-2eb67fadde13')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Ukrainian hryvnia', N'UAH', N'24.2575', N'UAH', 1, 1, 'b00d2f66-c397-4f50-b4f2-2eb67fadde13')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Ukrainian hryvnia', Symbol=N'UAH', ExchangeRate=N'24.2575', IsoCode=N'UAH', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'b00d2f66-c397-4f50-b4f2-2eb67fadde13'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '297ac727-8265-429e-81a2-49fdb279ae58')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'US Dollar', N'$', N'1.11556', N'USD', 1, 1, '297ac727-8265-429e-81a2-49fdb279ae58')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'US Dollar', Symbol=N'$', ExchangeRate=N'1.11556', IsoCode=N'USD', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '297ac727-8265-429e-81a2-49fdb279ae58'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '413d399d-f37f-413d-b18c-ca94f502c4a3')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Venezuela Bolivar', N'VEF', N'7.01836', N'VEF', 1, 1, '413d399d-f37f-413d-b18c-ca94f502c4a3')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Venezuela Bolivar', Symbol=N'VEF', ExchangeRate=N'7.01836', IsoCode=N'VEF', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '413d399d-f37f-413d-b18c-ca94f502c4a3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = 'dd13ecff-f5c4-437f-ac12-512176c40fb1')
BEGIN
   INSERT dbo.Currencies (Name, Symbol, ExchangeRate, IsoCode, IsMasterData, IsStockMasterData, [Guid]) VALUES (N'Vietnamese Dong', N'VND', N'24713.1', N'VND', 1, 1, 'dd13ecff-f5c4-437f-ac12-512176c40fb1')
END
ELSE
BEGIN
   UPDATE dbo.Currencies SET Name=N'Vietnamese Dong', Symbol=N'VND', ExchangeRate=N'24713.1', IsoCode=N'VND', IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = 'dd13ecff-f5c4-437f-ac12-512176c40fb1'
END

