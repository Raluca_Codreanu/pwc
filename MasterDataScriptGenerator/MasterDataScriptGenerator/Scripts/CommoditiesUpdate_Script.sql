if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'36e4f3aa-a4b7-400c-90c7-1cdb2645163c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'36e4f3aa-a4b7-400c-90c7-1cdb2645163c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6fad3c82-62e5-4346-802d-fb47ec959c0d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BANJO BOLT', N'4134K023', 1.04754415678684, N'36e4f3aa-a4b7-400c-90c7-1cdb2645163c', '1', '6fad3c82-62e5-4346-802d-fb47ec959c0d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BANJO BOLT',PartNumber=N'4134K023',Price=1.04754415678684,ManufacturerGuid=N'36e4f3aa-a4b7-400c-90c7-1cdb2645163c',IsMasterData='1' WHERE Guid = '6fad3c82-62e5-4346-802d-fb47ec959c0d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'64f59553-d048-49d3-b6b4-3e3d0edea35e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'64f59553-d048-49d3-b6b4-3e3d0edea35e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bc0f2b50-1f36-4731-8e01-c611f3c6fc7e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'0095332', 1.35494797967578, N'64f59553-d048-49d3-b6b4-3e3d0edea35e', '1', 'bc0f2b50-1f36-4731-8e01-c611f3c6fc7e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'0095332',Price=1.35494797967578,ManufacturerGuid=N'64f59553-d048-49d3-b6b4-3e3d0edea35e',IsMasterData='1' WHERE Guid = 'bc0f2b50-1f36-4731-8e01-c611f3c6fc7e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c4d7c67f-bf3d-4d46-afe1-65658fb7a2d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c4d7c67f-bf3d-4d46-afe1-65658fb7a2d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8a1415f5-b310-458d-bc6c-f38f5f08b23e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'0L2315', 0.240745221388822, N'c4d7c67f-bf3d-4d46-afe1-65658fb7a2d9', '1', '8a1415f5-b310-458d-bc6c-f38f5f08b23e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'0L2315',Price=0.240745221388822,ManufacturerGuid=N'c4d7c67f-bf3d-4d46-afe1-65658fb7a2d9',IsMasterData='1' WHERE Guid = '8a1415f5-b310-458d-bc6c-f38f5f08b23e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'23276b32-8422-4fd9-b70e-a8b22c048dbb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'23276b32-8422-4fd9-b70e-a8b22c048dbb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '633c71a4-a333-47a3-a159-dfc48293d868')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'2314J207', 0.87466731187999, N'23276b32-8422-4fd9-b70e-a8b22c048dbb', '1', '633c71a4-a333-47a3-a159-dfc48293d868')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'2314J207',Price=0.87466731187999,ManufacturerGuid=N'23276b32-8422-4fd9-b70e-a8b22c048dbb',IsMasterData='1' WHERE Guid = '633c71a4-a333-47a3-a159-dfc48293d868'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b850b7c8-4978-49f2-b763-398c3c5fc7fd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b850b7c8-4978-49f2-b763-398c3c5fc7fd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fb6419fb-ab80-4bb0-b9b1-74faad06d33d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'2314J614', 2.39535446406968, N'b850b7c8-4978-49f2-b763-398c3c5fc7fd', '1', 'fb6419fb-ab80-4bb0-b9b1-74faad06d33d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'2314J614',Price=2.39535446406968,ManufacturerGuid=N'b850b7c8-4978-49f2-b763-398c3c5fc7fd',IsMasterData='1' WHERE Guid = 'fb6419fb-ab80-4bb0-b9b1-74faad06d33d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3579213d-53bd-4f4f-9f73-2259977303d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3579213d-53bd-4f4f-9f73-2259977303d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aacbc994-7a28-4e7d-b9ee-99e700bf7012')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3207150', 5.44398741834019, N'3579213d-53bd-4f4f-9f73-2259977303d3', '1', 'aacbc994-7a28-4e7d-b9ee-99e700bf7012')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3207150',Price=5.44398741834019,ManufacturerGuid=N'3579213d-53bd-4f4f-9f73-2259977303d3',IsMasterData='1' WHERE Guid = 'aacbc994-7a28-4e7d-b9ee-99e700bf7012'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30a71743-1380-419b-b8c0-d85feafb8330')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30a71743-1380-419b-b8c0-d85feafb8330')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '54a80a8f-270f-4201-b6a1-83b9d21570c0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'32184344', 0.51451729978224, N'30a71743-1380-419b-b8c0-d85feafb8330', '1', '54a80a8f-270f-4201-b6a1-83b9d21570c0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'32184344',Price=0.51451729978224,ManufacturerGuid=N'30a71743-1380-419b-b8c0-d85feafb8330',IsMasterData='1' WHERE Guid = '54a80a8f-270f-4201-b6a1-83b9d21570c0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd12b7afd-54df-4d02-a31d-6ad8ae446152')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd12b7afd-54df-4d02-a31d-6ad8ae446152')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1ba57860-3e6d-42f6-98d4-06769cdcb406')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'32184422', 0.685095572223566, N'd12b7afd-54df-4d02-a31d-6ad8ae446152', '1', '1ba57860-3e6d-42f6-98d4-06769cdcb406')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'32184422',Price=0.685095572223566,ManufacturerGuid=N'd12b7afd-54df-4d02-a31d-6ad8ae446152',IsMasterData='1' WHERE Guid = '1ba57860-3e6d-42f6-98d4-06769cdcb406'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4bbdfc36-62b3-4e2c-86ba-c8fb5e4ad6c0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4bbdfc36-62b3-4e2c-86ba-c8fb5e4ad6c0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f3abe145-2f41-4d40-acae-ce5222e5efe6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218A026', 4.79070892813937, N'4bbdfc36-62b3-4e2c-86ba-c8fb5e4ad6c0', '1', 'f3abe145-2f41-4d40-acae-ce5222e5efe6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218A026',Price=4.79070892813937,ManufacturerGuid=N'4bbdfc36-62b3-4e2c-86ba-c8fb5e4ad6c0',IsMasterData='1' WHERE Guid = 'f3abe145-2f41-4d40-acae-ce5222e5efe6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c24945ad-0234-4b64-bc21-954a287a2b58')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c24945ad-0234-4b64-bc21-954a287a2b58')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2cd23f2f-04e2-4d9d-b6ee-97a9d5046a91')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218J024', 1.1650133075248, N'c24945ad-0234-4b64-bc21-954a287a2b58', '1', '2cd23f2f-04e2-4d9d-b6ee-97a9d5046a91')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218J024',Price=1.1650133075248,ManufacturerGuid=N'c24945ad-0234-4b64-bc21-954a287a2b58',IsMasterData='1' WHERE Guid = '2cd23f2f-04e2-4d9d-b6ee-97a9d5046a91'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'76d847e0-04bf-49e2-a029-3c2160af9aa0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'76d847e0-04bf-49e2-a029-3c2160af9aa0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '098d7625-d629-4783-ae43-ca77fc0af007')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R009', 0.248729736269054, N'76d847e0-04bf-49e2-a029-3c2160af9aa0', '1', '098d7625-d629-4783-ae43-ca77fc0af007')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R009',Price=0.248729736269054,ManufacturerGuid=N'76d847e0-04bf-49e2-a029-3c2160af9aa0',IsMasterData='1' WHERE Guid = '098d7625-d629-4783-ae43-ca77fc0af007'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8858d841-4e4d-4cd6-855c-fbc7c0465596')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8858d841-4e4d-4cd6-855c-fbc7c0465596')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ba2de6f1-9166-40d1-8225-ba872dbbdaa8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R013', 0.194773772078393, N'8858d841-4e4d-4cd6-855c-fbc7c0465596', '1', 'ba2de6f1-9166-40d1-8225-ba872dbbdaa8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R013',Price=0.194773772078393,ManufacturerGuid=N'8858d841-4e4d-4cd6-855c-fbc7c0465596',IsMasterData='1' WHERE Guid = 'ba2de6f1-9166-40d1-8225-ba872dbbdaa8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a9fc1477-a430-48c0-ae84-618cc3898a87')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a9fc1477-a430-48c0-ae84-618cc3898a87')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b52d88e6-78e2-416a-aa4c-c8fb3c330862')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R036', 0.391967094120494, N'a9fc1477-a430-48c0-ae84-618cc3898a87', '1', 'b52d88e6-78e2-416a-aa4c-c8fb3c330862')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R036',Price=0.391967094120494,ManufacturerGuid=N'a9fc1477-a430-48c0-ae84-618cc3898a87',IsMasterData='1' WHERE Guid = 'b52d88e6-78e2-416a-aa4c-c8fb3c330862'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f1c48367-a806-479f-a521-22db9138362a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f1c48367-a806-479f-a521-22db9138362a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '36563154-f005-400d-9b9f-a704589b3c1b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R037', 0.224655214130172, N'f1c48367-a806-479f-a521-22db9138362a', '1', '36563154-f005-400d-9b9f-a704589b3c1b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R037',Price=0.224655214130172,ManufacturerGuid=N'f1c48367-a806-479f-a521-22db9138362a',IsMasterData='1' WHERE Guid = '36563154-f005-400d-9b9f-a704589b3c1b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'743c9f58-8abb-4005-9acd-9ca62fb125bf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'743c9f58-8abb-4005-9acd-9ca62fb125bf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e809c61a-455f-4144-9427-6aa3d7ec27f7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R038', 0.137914347931285, N'743c9f58-8abb-4005-9acd-9ca62fb125bf', '1', 'e809c61a-455f-4144-9427-6aa3d7ec27f7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R038',Price=0.137914347931285,ManufacturerGuid=N'743c9f58-8abb-4005-9acd-9ca62fb125bf',IsMasterData='1' WHERE Guid = 'e809c61a-455f-4144-9427-6aa3d7ec27f7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fbcd839e-696b-4f48-8acf-9e018659a409')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fbcd839e-696b-4f48-8acf-9e018659a409')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0c48aebd-474e-4372-b772-9c9c5fdc331d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R039', 0.0930316961045246, N'fbcd839e-696b-4f48-8acf-9e018659a409', '1', '0c48aebd-474e-4372-b772-9c9c5fdc331d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R039',Price=0.0930316961045246,ManufacturerGuid=N'fbcd839e-696b-4f48-8acf-9e018659a409',IsMasterData='1' WHERE Guid = '0c48aebd-474e-4372-b772-9c9c5fdc331d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'67197b28-6df6-4dc8-a485-76a03d03c1d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'67197b28-6df6-4dc8-a485-76a03d03c1d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ec15db2d-6730-4527-a465-0db9a931e3c4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R042', 0.10041132349383, N'67197b28-6df6-4dc8-a485-76a03d03c1d3', '1', 'ec15db2d-6730-4527-a465-0db9a931e3c4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R042',Price=0.10041132349383,ManufacturerGuid=N'67197b28-6df6-4dc8-a485-76a03d03c1d3',IsMasterData='1' WHERE Guid = 'ec15db2d-6730-4527-a465-0db9a931e3c4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ede41294-94d6-4138-9035-91655f7d96e0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ede41294-94d6-4138-9035-91655f7d96e0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1bf93df8-853e-42f8-8ecd-e9f1bce6eb32')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R044', 0.350834744737479, N'ede41294-94d6-4138-9035-91655f7d96e0', '1', '1bf93df8-853e-42f8-8ecd-e9f1bce6eb32')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R044',Price=0.350834744737479,ManufacturerGuid=N'ede41294-94d6-4138-9035-91655f7d96e0',IsMasterData='1' WHERE Guid = '1bf93df8-853e-42f8-8ecd-e9f1bce6eb32'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4a23e819-7302-4e3e-a6e7-316e0c3cea8e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4a23e819-7302-4e3e-a6e7-316e0c3cea8e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7fd86e51-15d0-4095-8669-bfda0433ff71')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R045', 2.26893297846601, N'4a23e819-7302-4e3e-a6e7-316e0c3cea8e', '1', '7fd86e51-15d0-4095-8669-bfda0433ff71')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R045',Price=2.26893297846601,ManufacturerGuid=N'4a23e819-7302-4e3e-a6e7-316e0c3cea8e',IsMasterData='1' WHERE Guid = '7fd86e51-15d0-4095-8669-bfda0433ff71'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b7add3fd-336e-451f-ac91-6aa228f2d97e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b7add3fd-336e-451f-ac91-6aa228f2d97e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3795f08-5648-4067-b688-a16bbf29b7fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R046', 0.556496491652553, N'b7add3fd-336e-451f-ac91-6aa228f2d97e', '1', 'd3795f08-5648-4067-b688-a16bbf29b7fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R046',Price=0.556496491652553,ManufacturerGuid=N'b7add3fd-336e-451f-ac91-6aa228f2d97e',IsMasterData='1' WHERE Guid = 'd3795f08-5648-4067-b688-a16bbf29b7fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'969a5cb6-d75f-43d6-94de-3f3d93e33d9f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'969a5cb6-d75f-43d6-94de-3f3d93e33d9f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8fb5e13e-5729-44cf-9c28-9d0bb217e85e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R048', 0.476893297846601, N'969a5cb6-d75f-43d6-94de-3f3d93e33d9f', '1', '8fb5e13e-5729-44cf-9c28-9d0bb217e85e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R048',Price=0.476893297846601,ManufacturerGuid=N'969a5cb6-d75f-43d6-94de-3f3d93e33d9f',IsMasterData='1' WHERE Guid = '8fb5e13e-5729-44cf-9c28-9d0bb217e85e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e31374bb-6560-4d29-970d-353904523f82')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e31374bb-6560-4d29-970d-353904523f82')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f3d0ae53-9523-44e8-b6fa-f1c22c8b0b18')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3218R054', 0.476893297846601, N'e31374bb-6560-4d29-970d-353904523f82', '1', 'f3d0ae53-9523-44e8-b6fa-f1c22c8b0b18')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3218R054',Price=0.476893297846601,ManufacturerGuid=N'e31374bb-6560-4d29-970d-353904523f82',IsMasterData='1' WHERE Guid = 'f3d0ae53-9523-44e8-b6fa-f1c22c8b0b18'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'51f7143c-f216-4966-beaa-04a5fd0f6dd6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'51f7143c-f216-4966-beaa-04a5fd0f6dd6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8fffbe53-4dcd-4e20-bf0c-090cc012663c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3355E003', 0.167553834986692, N'51f7143c-f216-4966-beaa-04a5fd0f6dd6', '1', '8fffbe53-4dcd-4e20-bf0c-090cc012663c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3355E003',Price=0.167553834986692,ManufacturerGuid=N'51f7143c-f216-4966-beaa-04a5fd0f6dd6',IsMasterData='1' WHERE Guid = '8fffbe53-4dcd-4e20-bf0c-090cc012663c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ca07acf2-62eb-4a15-8eb8-8e77a0c684b5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ca07acf2-62eb-4a15-8eb8-8e77a0c684b5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '36558d6c-788a-4ed9-96ed-3905e5a1f95f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3355E004', 0.671909024921365, N'ca07acf2-62eb-4a15-8eb8-8e77a0c684b5', '1', '36558d6c-788a-4ed9-96ed-3905e5a1f95f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3355E004',Price=0.671909024921365,ManufacturerGuid=N'ca07acf2-62eb-4a15-8eb8-8e77a0c684b5',IsMasterData='1' WHERE Guid = '36558d6c-788a-4ed9-96ed-3905e5a1f95f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'606a2a14-ddfa-4810-8518-4b5b07a98412')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'606a2a14-ddfa-4810-8518-4b5b07a98412')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7042cbfd-dc85-4bab-8e41-b9d092a0f3a0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3355E005', 0.169005564964917, N'606a2a14-ddfa-4810-8518-4b5b07a98412', '1', '7042cbfd-dc85-4bab-8e41-b9d092a0f3a0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3355E005',Price=0.169005564964917,ManufacturerGuid=N'606a2a14-ddfa-4810-8518-4b5b07a98412',IsMasterData='1' WHERE Guid = '7042cbfd-dc85-4bab-8e41-b9d092a0f3a0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a33ef13d-7741-491b-a153-2c19d6cc14be')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a33ef13d-7741-491b-a153-2c19d6cc14be')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '21f2f407-8812-49e8-ad00-f6f5aab3b04d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3380815', 3.23009920154851, N'a33ef13d-7741-491b-a153-2c19d6cc14be', '1', '21f2f407-8812-49e8-ad00-f6f5aab3b04d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3380815',Price=3.23009920154851,ManufacturerGuid=N'a33ef13d-7741-491b-a153-2c19d6cc14be',IsMasterData='1' WHERE Guid = '21f2f407-8812-49e8-ad00-f6f5aab3b04d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c334add1-d9b3-40e8-8036-bacc5ae5ee5f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c334add1-d9b3-40e8-8036-bacc5ae5ee5f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a8a444f1-26b9-4c86-af3c-e9567539130c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3413566', 0.931526736027099, N'c334add1-d9b3-40e8-8036-bacc5ae5ee5f', '1', 'a8a444f1-26b9-4c86-af3c-e9567539130c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3413566',Price=0.931526736027099,ManufacturerGuid=N'c334add1-d9b3-40e8-8036-bacc5ae5ee5f',IsMasterData='1' WHERE Guid = 'a8a444f1-26b9-4c86-af3c-e9567539130c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1be4c189-c905-4b92-89fc-99c821ce7bb3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1be4c189-c905-4b92-89fc-99c821ce7bb3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b4e3459f-6cd1-4e7b-9e67-7d911bbf1867')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'3426510', 0.508105492378418, N'1be4c189-c905-4b92-89fc-99c821ce7bb3', '1', 'b4e3459f-6cd1-4e7b-9e67-7d911bbf1867')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'3426510',Price=0.508105492378418,ManufacturerGuid=N'1be4c189-c905-4b92-89fc-99c821ce7bb3',IsMasterData='1' WHERE Guid = 'b4e3459f-6cd1-4e7b-9e67-7d911bbf1867'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2ce38a9d-0bc4-41b6-9975-44b1336d853b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2ce38a9d-0bc4-41b6-9975-44b1336d853b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd573556e-6bfc-4e3f-b5c2-48e4b66f048e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT', N'4134K016', 1.01621098475684, N'2ce38a9d-0bc4-41b6-9975-44b1336d853b', '1', 'd573556e-6bfc-4e3f-b5c2-48e4b66f048e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT',PartNumber=N'4134K016',Price=1.01621098475684,ManufacturerGuid=N'2ce38a9d-0bc4-41b6-9975-44b1336d853b',IsMasterData='1' WHERE Guid = 'd573556e-6bfc-4e3f-b5c2-48e4b66f048e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cc5e55c9-a622-4d69-ab66-e64681e019dc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cc5e55c9-a622-4d69-ab66-e64681e019dc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8f5bf228-8383-48f8-ad52-a9e99ebb37b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3456716', 0.438180498427293, N'cc5e55c9-a622-4d69-ab66-e64681e019dc', '1', '8f5bf228-8383-48f8-ad52-a9e99ebb37b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3456716',Price=0.438180498427293,ManufacturerGuid=N'cc5e55c9-a622-4d69-ab66-e64681e019dc',IsMasterData='1' WHERE Guid = '8f5bf228-8383-48f8-ad52-a9e99ebb37b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'02065fbe-c02a-4130-9dc3-c11c0d6466dc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'02065fbe-c02a-4130-9dc3-c11c0d6466dc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'be7e9c8c-e141-4fb6-a0fe-d0fd783c6c0c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3750351', 0.58069199128962, N'02065fbe-c02a-4130-9dc3-c11c0d6466dc', '1', 'be7e9c8c-e141-4fb6-a0fe-d0fd783c6c0c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3750351',Price=0.58069199128962,ManufacturerGuid=N'02065fbe-c02a-4130-9dc3-c11c0d6466dc',IsMasterData='1' WHERE Guid = 'be7e9c8c-e141-4fb6-a0fe-d0fd783c6c0c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0d236fc1-b8fd-48d8-ae7a-d57008fb81bc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0d236fc1-b8fd-48d8-ae7a-d57008fb81bc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '81757040-60a1-4816-b54e-fab01c979d03')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3825080', 0.226227921606581, N'0d236fc1-b8fd-48d8-ae7a-d57008fb81bc', '1', '81757040-60a1-4816-b54e-fab01c979d03')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3825080',Price=0.226227921606581,ManufacturerGuid=N'0d236fc1-b8fd-48d8-ae7a-d57008fb81bc',IsMasterData='1' WHERE Guid = '81757040-60a1-4816-b54e-fab01c979d03'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1f3335b7-ff61-445e-8afa-821f42181de8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1f3335b7-ff61-445e-8afa-821f42181de8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '655bfba9-ea54-42d3-90c1-55c3eb27665b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089276', 0.0413984998790225, N'1f3335b7-ff61-445e-8afa-821f42181de8', '1', '655bfba9-ea54-42d3-90c1-55c3eb27665b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089276',Price=0.0413984998790225,ManufacturerGuid=N'1f3335b7-ff61-445e-8afa-821f42181de8',IsMasterData='1' WHERE Guid = '655bfba9-ea54-42d3-90c1-55c3eb27665b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bbaaba6e-89a8-4a1c-8cc0-48ed0b0c2600')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bbaaba6e-89a8-4a1c-8cc0-48ed0b0c2600')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6b3df6db-56ab-419f-95c1-c14fe7f641a1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089283', 0.127631260585531, N'bbaaba6e-89a8-4a1c-8cc0-48ed0b0c2600', '1', '6b3df6db-56ab-419f-95c1-c14fe7f641a1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089283',Price=0.127631260585531,ManufacturerGuid=N'bbaaba6e-89a8-4a1c-8cc0-48ed0b0c2600',IsMasterData='1' WHERE Guid = '6b3df6db-56ab-419f-95c1-c14fe7f641a1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fc1abe16-e0ab-48cc-b80c-0a174b8e01dd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fc1abe16-e0ab-48cc-b80c-0a174b8e01dd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ce87ea7f-45e8-4464-962a-7cef06c4c276')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089291', 0.266150496007743, N'fc1abe16-e0ab-48cc-b80c-0a174b8e01dd', '1', 'ce87ea7f-45e8-4464-962a-7cef06c4c276')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089291',Price=0.266150496007743,ManufacturerGuid=N'fc1abe16-e0ab-48cc-b80c-0a174b8e01dd',IsMasterData='1' WHERE Guid = 'ce87ea7f-45e8-4464-962a-7cef06c4c276'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0e4c2a80-1743-4d0b-8fff-8a71d340e8f2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0e4c2a80-1743-4d0b-8fff-8a71d340e8f2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aff1cd9a-1a85-4ee6-a627-f02acf375a82')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089292', 0.627873215581902, N'0e4c2a80-1743-4d0b-8fff-8a71d340e8f2', '1', 'aff1cd9a-1a85-4ee6-a627-f02acf375a82')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089292',Price=0.627873215581902,ManufacturerGuid=N'0e4c2a80-1743-4d0b-8fff-8a71d340e8f2',IsMasterData='1' WHERE Guid = 'aff1cd9a-1a85-4ee6-a627-f02acf375a82'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5e6ca089-7724-4a7e-91c2-d59e264c2729')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5e6ca089-7724-4a7e-91c2-d59e264c2729')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e5ada9fc-d02f-4537-b7e6-ed205997f395')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089297', 0.29034599564481, N'5e6ca089-7724-4a7e-91c2-d59e264c2729', '1', 'e5ada9fc-d02f-4537-b7e6-ed205997f395')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089297',Price=0.29034599564481,ManufacturerGuid=N'5e6ca089-7724-4a7e-91c2-d59e264c2729',IsMasterData='1' WHERE Guid = 'e5ada9fc-d02f-4537-b7e6-ed205997f395'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f8421b82-c39c-4fd5-bc86-c544a109e1c9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f8421b82-c39c-4fd5-bc86-c544a109e1c9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '88d31418-6c77-48ef-8fab-f333939c934e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089310', 0.123034115654488, N'f8421b82-c39c-4fd5-bc86-c544a109e1c9', '1', '88d31418-6c77-48ef-8fab-f333939c934e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089310',Price=0.123034115654488,ManufacturerGuid=N'f8421b82-c39c-4fd5-bc86-c544a109e1c9',IsMasterData='1' WHERE Guid = '88d31418-6c77-48ef-8fab-f333939c934e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3e3e4cbd-8160-4cc9-8167-cd7b470a14b3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3e3e4cbd-8160-4cc9-8167-cd7b470a14b3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '37650b12-4a82-40a0-9964-441b0120594f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089314', 7.69416888458747, N'3e3e4cbd-8160-4cc9-8167-cd7b470a14b3', '1', '37650b12-4a82-40a0-9964-441b0120594f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089314',Price=7.69416888458747,ManufacturerGuid=N'3e3e4cbd-8160-4cc9-8167-cd7b470a14b3',IsMasterData='1' WHERE Guid = '37650b12-4a82-40a0-9964-441b0120594f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b53a9b00-9560-4b2f-8f5c-95916cdb8b25')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b53a9b00-9560-4b2f-8f5c-95916cdb8b25')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c36994d0-374f-4aa4-bc11-793b693b7110')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2543633', 0.0367771594483426, N'b53a9b00-9560-4b2f-8f5c-95916cdb8b25', '1', 'c36994d0-374f-4aa4-bc11-793b693b7110')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2543633',Price=0.0367771594483426,ManufacturerGuid=N'b53a9b00-9560-4b2f-8f5c-95916cdb8b25',IsMasterData='1' WHERE Guid = 'c36994d0-374f-4aa4-bc11-793b693b7110'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a0e06589-7da9-40b1-8f69-6f7e69a2ddd0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a0e06589-7da9-40b1-8f69-6f7e69a2ddd0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '133a2a23-f63e-47c4-b6ab-d0dbf4730d4a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3226237', 1.06036777159448, N'a0e06589-7da9-40b1-8f69-6f7e69a2ddd0', '1', '133a2a23-f63e-47c4-b6ab-d0dbf4730d4a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3226237',Price=1.06036777159448,ManufacturerGuid=N'a0e06589-7da9-40b1-8f69-6f7e69a2ddd0',IsMasterData='1' WHERE Guid = '133a2a23-f63e-47c4-b6ab-d0dbf4730d4a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bdc28b15-a5c1-476d-9153-101866d825a8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bdc28b15-a5c1-476d-9153-101866d825a8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2e813859-c3a6-4208-bdaa-aeb966aac1d6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3429918', 8.95233486571498, N'bdc28b15-a5c1-476d-9153-101866d825a8', '1', '2e813859-c3a6-4208-bdaa-aeb966aac1d6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3429918',Price=8.95233486571498,ManufacturerGuid=N'bdc28b15-a5c1-476d-9153-101866d825a8',IsMasterData='1' WHERE Guid = '2e813859-c3a6-4208-bdaa-aeb966aac1d6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c0789b23-ee7d-4623-867f-19e234949520')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c0789b23-ee7d-4623-867f-19e234949520')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '75013640-1ef2-480c-8115-28739ea8f897')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3603419', 0.598354706024679, N'c0789b23-ee7d-4623-867f-19e234949520', '1', '75013640-1ef2-480c-8115-28739ea8f897')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3603419',Price=0.598354706024679,ManufacturerGuid=N'c0789b23-ee7d-4623-867f-19e234949520',IsMasterData='1' WHERE Guid = '75013640-1ef2-480c-8115-28739ea8f897'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7e8c3b3e-4a2d-40a1-8db6-88f23a99dfc1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7e8c3b3e-4a2d-40a1-8db6-88f23a99dfc1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f1cb3d50-0ada-4869-a3e6-a5dd9fb586c5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3670843', 0.614565690781515, N'7e8c3b3e-4a2d-40a1-8db6-88f23a99dfc1', '1', 'f1cb3d50-0ada-4869-a3e6-a5dd9fb586c5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3670843',Price=0.614565690781515,ManufacturerGuid=N'7e8c3b3e-4a2d-40a1-8db6-88f23a99dfc1',IsMasterData='1' WHERE Guid = 'f1cb3d50-0ada-4869-a3e6-a5dd9fb586c5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b6de68ed-ab14-47bc-84d7-57c54732d043')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b6de68ed-ab14-47bc-84d7-57c54732d043')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '697c0e72-c9d9-4991-bfc7-09a2a1efba21')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3732348', 1.17178804742318, N'b6de68ed-ab14-47bc-84d7-57c54732d043', '1', '697c0e72-c9d9-4991-bfc7-09a2a1efba21')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3732348',Price=1.17178804742318,ManufacturerGuid=N'b6de68ed-ab14-47bc-84d7-57c54732d043',IsMasterData='1' WHERE Guid = '697c0e72-c9d9-4991-bfc7-09a2a1efba21'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'40b57fd7-19a2-404e-90ac-87f028064357')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'40b57fd7-19a2-404e-90ac-87f028064357')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '80886ad9-847a-4a87-b50f-3a2e41b0f67b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3732349', 1.19283813210743, N'40b57fd7-19a2-404e-90ac-87f028064357', '1', '80886ad9-847a-4a87-b50f-3a2e41b0f67b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3732349',Price=1.19283813210743,ManufacturerGuid=N'40b57fd7-19a2-404e-90ac-87f028064357',IsMasterData='1' WHERE Guid = '80886ad9-847a-4a87-b50f-3a2e41b0f67b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a9a8d5ee-25c6-4d0c-82ce-ea7e0d7cb8d6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a9a8d5ee-25c6-4d0c-82ce-ea7e0d7cb8d6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '772e826d-5f3c-414a-80b8-ae9da8da4019')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'3836627', 0.241954996370675, N'a9a8d5ee-25c6-4d0c-82ce-ea7e0d7cb8d6', '1', '772e826d-5f3c-414a-80b8-ae9da8da4019')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'3836627',Price=0.241954996370675,ManufacturerGuid=N'a9a8d5ee-25c6-4d0c-82ce-ea7e0d7cb8d6',IsMasterData='1' WHERE Guid = '772e826d-5f3c-414a-80b8-ae9da8da4019'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'632d7aca-98a9-4db7-ad2e-04977002befb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'632d7aca-98a9-4db7-ad2e-04977002befb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b41a9d5f-7cad-4757-9171-9a07a9e896da')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-FLANGE HD', N'2089298', 1.92354222114687, N'632d7aca-98a9-4db7-ad2e-04977002befb', '1', 'b41a9d5f-7cad-4757-9171-9a07a9e896da')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-FLANGE HD',PartNumber=N'2089298',Price=1.92354222114687,ManufacturerGuid=N'632d7aca-98a9-4db7-ad2e-04977002befb',IsMasterData='1' WHERE Guid = 'b41a9d5f-7cad-4757-9171-9a07a9e896da'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'81ff2830-019c-4f19-9564-b04164df7ca9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'81ff2830-019c-4f19-9564-b04164df7ca9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7a4d568e-2372-492b-8030-e6f032cecbcf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-HEX HEAD', N'1705632', 0.0393176869102347, N'81ff2830-019c-4f19-9564-b04164df7ca9', '1', '7a4d568e-2372-492b-8030-e6f032cecbcf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-HEX HEAD',PartNumber=N'1705632',Price=0.0393176869102347,ManufacturerGuid=N'81ff2830-019c-4f19-9564-b04164df7ca9',IsMasterData='1' WHERE Guid = '7a4d568e-2372-492b-8030-e6f032cecbcf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'10ef609c-4ce9-40e4-ba82-216b1c9b7b37')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'10ef609c-4ce9-40e4-ba82-216b1c9b7b37')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5614d973-6c1a-4fc1-b03b-ffe805382d12')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-HEX HEAD', N'2069185', 0.09496733607549, N'10ef609c-4ce9-40e4-ba82-216b1c9b7b37', '1', '5614d973-6c1a-4fc1-b03b-ffe805382d12')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-HEX HEAD',PartNumber=N'2069185',Price=0.09496733607549,ManufacturerGuid=N'10ef609c-4ce9-40e4-ba82-216b1c9b7b37',IsMasterData='1' WHERE Guid = '5614d973-6c1a-4fc1-b03b-ffe805382d12'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd5b7043a-cf70-4091-bb40-2ba51660189f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd5b7043a-cf70-4091-bb40-2ba51660189f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a2c5b537-f5fd-4a68-bf85-1d5dfcbeeba3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-HEX HEAD', N'9X6154', 0.0459714493104283, N'd5b7043a-cf70-4091-bb40-2ba51660189f', '1', 'a2c5b537-f5fd-4a68-bf85-1d5dfcbeeba3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-HEX HEAD',PartNumber=N'9X6154',Price=0.0459714493104283,ManufacturerGuid=N'd5b7043a-cf70-4091-bb40-2ba51660189f',IsMasterData='1' WHERE Guid = 'a2c5b537-f5fd-4a68-bf85-1d5dfcbeeba3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b102ddda-ce44-4f0b-b902-bcabadce1571')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b102ddda-ce44-4f0b-b902-bcabadce1571')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3b70cdfe-cbc3-4725-9396-42ec905b5d76')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-HEX HEAD', N'3792184', 0.675417372368739, N'b102ddda-ce44-4f0b-b902-bcabadce1571', '1', '3b70cdfe-cbc3-4725-9396-42ec905b5d76')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-HEX HEAD',PartNumber=N'3792184',Price=0.675417372368739,ManufacturerGuid=N'b102ddda-ce44-4f0b-b902-bcabadce1571',IsMasterData='1' WHERE Guid = '3b70cdfe-cbc3-4725-9396-42ec905b5d76'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7dd21704-e840-4257-91d8-a45564db02df')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7dd21704-e840-4257-91d8-a45564db02df')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a5da3dd3-20db-4cc6-95f3-ff345cd5e3e9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-SOCKET HEAD', N'2578465', 0.0977498185337527, N'7dd21704-e840-4257-91d8-a45564db02df', '1', 'a5da3dd3-20db-4cc6-95f3-ff345cd5e3e9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-SOCKET HEAD',PartNumber=N'2578465',Price=0.0977498185337527,ManufacturerGuid=N'7dd21704-e840-4257-91d8-a45564db02df',IsMasterData='1' WHERE Guid = 'a5da3dd3-20db-4cc6-95f3-ff345cd5e3e9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'951b196c-1765-4b8c-a346-45444f35b813')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'951b196c-1765-4b8c-a346-45444f35b813')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fa15d0be-56c7-4b81-afd2-1b824fa1a162')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'BOLT-SOCKET HEAD', N'8T1158', 0.0629082990563755, N'951b196c-1765-4b8c-a346-45444f35b813', '1', 'fa15d0be-56c7-4b81-afd2-1b824fa1a162')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'BOLT-SOCKET HEAD',PartNumber=N'8T1158',Price=0.0629082990563755,ManufacturerGuid=N'951b196c-1765-4b8c-a346-45444f35b813',IsMasterData='1' WHERE Guid = 'fa15d0be-56c7-4b81-afd2-1b824fa1a162'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b7fd54bf-ce4d-420d-ac50-d46248729730')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b7fd54bf-ce4d-420d-ac50-d46248729730')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7191f0d9-9b8d-4208-b949-68a943566936')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817152', 0.0319380595209291, N'b7fd54bf-ce4d-420d-ac50-d46248729730', '1', '7191f0d9-9b8d-4208-b949-68a943566936')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817152',Price=0.0319380595209291,ManufacturerGuid=N'b7fd54bf-ce4d-420d-ac50-d46248729730',IsMasterData='1' WHERE Guid = '7191f0d9-9b8d-4208-b949-68a943566936'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f27c7894-148e-492f-8c6d-de2add2a9809')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f27c7894-148e-492f-8c6d-de2add2a9809')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0eddd5ed-d3ee-44de-8a20-3e88e01481fe')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817A001', 0.155819017662715, N'f27c7894-148e-492f-8c6d-de2add2a9809', '1', '0eddd5ed-d3ee-44de-8a20-3e88e01481fe')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817A001',Price=0.155819017662715,ManufacturerGuid=N'f27c7894-148e-492f-8c6d-de2add2a9809',IsMasterData='1' WHERE Guid = '0eddd5ed-d3ee-44de-8a20-3e88e01481fe'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'15aff4a3-f6e9-4a5c-8abc-60f56dfda2ce')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'15aff4a3-f6e9-4a5c-8abc-60f56dfda2ce')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3efc28e5-8b80-4cd6-a70b-13f7c1cc6a1b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817A002', 0.0976288410355674, N'15aff4a3-f6e9-4a5c-8abc-60f56dfda2ce', '1', '3efc28e5-8b80-4cd6-a70b-13f7c1cc6a1b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817A002',Price=0.0976288410355674,ManufacturerGuid=N'15aff4a3-f6e9-4a5c-8abc-60f56dfda2ce',IsMasterData='1' WHERE Guid = '3efc28e5-8b80-4cd6-a70b-13f7c1cc6a1b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'56cd20ed-056d-46cf-9a17-0575573efec1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'56cd20ed-056d-46cf-9a17-0575573efec1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8b81ccf7-8057-4918-836e-9a31da251037')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817A004', 0.0434309218485362, N'56cd20ed-056d-46cf-9a17-0575573efec1', '1', '8b81ccf7-8057-4918-836e-9a31da251037')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817A004',Price=0.0434309218485362,ManufacturerGuid=N'56cd20ed-056d-46cf-9a17-0575573efec1',IsMasterData='1' WHERE Guid = '8b81ccf7-8057-4918-836e-9a31da251037'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e5b3bac8-fc7e-421f-b7d2-00902184af14')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e5b3bac8-fc7e-421f-b7d2-00902184af14')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2096e434-11b7-4cfc-b2e5-e2db4fe2ea50')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817A006', 0.0558916041616259, N'e5b3bac8-fc7e-421f-b7d2-00902184af14', '1', '2096e434-11b7-4cfc-b2e5-e2db4fe2ea50')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817A006',Price=0.0558916041616259,ManufacturerGuid=N'e5b3bac8-fc7e-421f-b7d2-00902184af14',IsMasterData='1' WHERE Guid = '2096e434-11b7-4cfc-b2e5-e2db4fe2ea50'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'75d026f5-fc58-4f2c-ac12-fc25e6f25449')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'75d026f5-fc58-4f2c-ac12-fc25e6f25449')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8036f8a0-9a47-43cf-a8d5-3b9992a1311f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CABLE TIE', N'2817A011', 0.144568110331478, N'75d026f5-fc58-4f2c-ac12-fc25e6f25449', '1', '8036f8a0-9a47-43cf-a8d5-3b9992a1311f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CABLE TIE',PartNumber=N'2817A011',Price=0.144568110331478,ManufacturerGuid=N'75d026f5-fc58-4f2c-ac12-fc25e6f25449',IsMasterData='1' WHERE Guid = '8036f8a0-9a47-43cf-a8d5-3b9992a1311f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9fa67064-a64e-4639-8652-967c8344ed9c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9fa67064-a64e-4639-8652-967c8344ed9c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '48697d19-3f7f-4025-b060-ea266b97de3c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'3309799', 0, N'9fa67064-a64e-4639-8652-967c8344ed9c', '1', '48697d19-3f7f-4025-b060-ea266b97de3c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'3309799',Price=0,ManufacturerGuid=N'9fa67064-a64e-4639-8652-967c8344ed9c',IsMasterData='1' WHERE Guid = '48697d19-3f7f-4025-b060-ea266b97de3c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7d3893f1-b9cf-4265-9f71-601478c9b28b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7d3893f1-b9cf-4265-9f71-601478c9b28b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fa782f27-aa12-44c3-a4c6-b74e57d59bf5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'0170002', 0.0277038470844423, N'7d3893f1-b9cf-4265-9f71-601478c9b28b', '1', 'fa782f27-aa12-44c3-a4c6-b74e57d59bf5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'0170002',Price=0.0277038470844423,ManufacturerGuid=N'7d3893f1-b9cf-4265-9f71-601478c9b28b',IsMasterData='1' WHERE Guid = 'fa782f27-aa12-44c3-a4c6-b74e57d59bf5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3014b044-b378-4518-a0cc-b6ea99a32a21')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3014b044-b378-4518-a0cc-b6ea99a32a21')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a94a3e8d-4744-4d68-b967-59358f31848e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'0170036', 0.0447616743285749, N'3014b044-b378-4518-a0cc-b6ea99a32a21', '1', 'a94a3e8d-4744-4d68-b967-59358f31848e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'0170036',Price=0.0447616743285749,ManufacturerGuid=N'3014b044-b378-4518-a0cc-b6ea99a32a21',IsMasterData='1' WHERE Guid = 'a94a3e8d-4744-4d68-b967-59358f31848e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f7dcf6ae-a972-4b88-b665-557d27f87ce7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f7dcf6ae-a972-4b88-b665-557d27f87ce7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '61106d64-542c-401b-a139-2692ddd3cccf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2721130', 0.0447616743285749, N'f7dcf6ae-a972-4b88-b665-557d27f87ce7', '1', '61106d64-542c-401b-a139-2692ddd3cccf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2721130',Price=0.0447616743285749,ManufacturerGuid=N'f7dcf6ae-a972-4b88-b665-557d27f87ce7',IsMasterData='1' WHERE Guid = '61106d64-542c-401b-a139-2692ddd3cccf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e501ca94-0b69-49e6-8f1d-5f72df1197e3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e501ca94-0b69-49e6-8f1d-5f72df1197e3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c4a71af4-68fa-4c55-9c58-361d8c2a87b8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2721332', 0.0384708444229373, N'e501ca94-0b69-49e6-8f1d-5f72df1197e3', '1', 'c4a71af4-68fa-4c55-9c58-361d8c2a87b8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2721332',Price=0.0384708444229373,ManufacturerGuid=N'e501ca94-0b69-49e6-8f1d-5f72df1197e3',IsMasterData='1' WHERE Guid = 'c4a71af4-68fa-4c55-9c58-361d8c2a87b8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'db34e01b-e2f1-4453-a32c-e59f43396e75')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'db34e01b-e2f1-4453-a32c-e59f43396e75')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '02f408aa-0be3-4027-9008-ccbeb6a15b5c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2721A029', 0.0312121945318171, N'db34e01b-e2f1-4453-a32c-e59f43396e75', '1', '02f408aa-0be3-4027-9008-ccbeb6a15b5c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2721A029',Price=0.0312121945318171,ManufacturerGuid=N'db34e01b-e2f1-4453-a32c-e59f43396e75',IsMasterData='1' WHERE Guid = '02f408aa-0be3-4027-9008-ccbeb6a15b5c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'00730940-085f-49c0-81f7-cecf9a56227e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'00730940-085f-49c0-81f7-cecf9a56227e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1280a4bb-b1de-40d1-b2a7-e4b0801e8bb2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2721A067', 0.183522864747157, N'00730940-085f-49c0-81f7-cecf9a56227e', '1', '1280a4bb-b1de-40d1-b2a7-e4b0801e8bb2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2721A067',Price=0.183522864747157,ManufacturerGuid=N'00730940-085f-49c0-81f7-cecf9a56227e',IsMasterData='1' WHERE Guid = '1280a4bb-b1de-40d1-b2a7-e4b0801e8bb2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0a347c51-1c4c-4d5c-9a6f-8742dd2d90aa')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0a347c51-1c4c-4d5c-9a6f-8742dd2d90aa')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8f1ec254-62a4-4cf4-bfcc-cca733a51a7f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2721A069', 0.253568836196467, N'0a347c51-1c4c-4d5c-9a6f-8742dd2d90aa', '1', '8f1ec254-62a4-4cf4-bfcc-cca733a51a7f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2721A069',Price=0.253568836196467,ManufacturerGuid=N'0a347c51-1c4c-4d5c-9a6f-8742dd2d90aa',IsMasterData='1' WHERE Guid = '8f1ec254-62a4-4cf4-bfcc-cca733a51a7f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0f4c4f40-acd2-4bef-a4a9-47e928f4901e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0f4c4f40-acd2-4bef-a4a9-47e928f4901e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9b0fbd43-e690-4c58-b692-d14122172b05')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2722A036', 0.0499637067505444, N'0f4c4f40-acd2-4bef-a4a9-47e928f4901e', '1', '9b0fbd43-e690-4c58-b692-d14122172b05')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2722A036',Price=0.0499637067505444,ManufacturerGuid=N'0f4c4f40-acd2-4bef-a4a9-47e928f4901e',IsMasterData='1' WHERE Guid = '9b0fbd43-e690-4c58-b692-d14122172b05'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ee470ae6-7665-4336-814d-e71ae522a679')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ee470ae6-7665-4336-814d-e71ae522a679')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5b76212d-0654-4d6f-a0e5-0e80c903ef00')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2722A040', 0.0752480038712799, N'ee470ae6-7665-4336-814d-e71ae522a679', '1', '5b76212d-0654-4d6f-a0e5-0e80c903ef00')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2722A040',Price=0.0752480038712799,ManufacturerGuid=N'ee470ae6-7665-4336-814d-e71ae522a679',IsMasterData='1' WHERE Guid = '5b76212d-0654-4d6f-a0e5-0e80c903ef00'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bdc6b9f1-1c6d-49e3-ac26-33891d60406b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bdc6b9f1-1c6d-49e3-ac26-33891d60406b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7a98425e-53de-498d-9772-7000a10d339b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2722A206', 0.0206871521896927, N'bdc6b9f1-1c6d-49e3-ac26-33891d60406b', '1', '7a98425e-53de-498d-9772-7000a10d339b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2722A206',Price=0.0206871521896927,ManufacturerGuid=N'bdc6b9f1-1c6d-49e3-ac26-33891d60406b',IsMasterData='1' WHERE Guid = '7a98425e-53de-498d-9772-7000a10d339b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a940e8e4-b83b-44ad-a5b6-2d80a7a9fe4f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a940e8e4-b83b-44ad-a5b6-2d80a7a9fe4f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '59df88bc-2bd9-4c5c-bf81-05b5b555c57b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CIRCLIP', N'2724227', 0.020082264698766, N'a940e8e4-b83b-44ad-a5b6-2d80a7a9fe4f', '1', '59df88bc-2bd9-4c5c-bf81-05b5b555c57b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CIRCLIP',PartNumber=N'2724227',Price=0.020082264698766,ManufacturerGuid=N'a940e8e4-b83b-44ad-a5b6-2d80a7a9fe4f',IsMasterData='1' WHERE Guid = '59df88bc-2bd9-4c5c-bf81-05b5b555c57b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6416e259-ad9e-4c30-a152-b8b9a1624f21')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6416e259-ad9e-4c30-a152-b8b9a1624f21')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3558de1c-c680-48f1-a882-d5b96b7ec5cd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLAMP AS-PIPE', N'3491140', 0.278248245826276, N'6416e259-ad9e-4c30-a152-b8b9a1624f21', '1', '3558de1c-c680-48f1-a882-d5b96b7ec5cd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLAMP AS-PIPE',PartNumber=N'3491140',Price=0.278248245826276,ManufacturerGuid=N'6416e259-ad9e-4c30-a152-b8b9a1624f21',IsMasterData='1' WHERE Guid = '3558de1c-c680-48f1-a882-d5b96b7ec5cd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8c9be3c9-0af3-467f-bad8-5ecc40c04454')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8c9be3c9-0af3-467f-bad8-5ecc40c04454')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ffd670a4-d545-40f8-b718-e0eb12054c4b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLAMP V-BAND', N'2412634', 3.40225018146625, N'8c9be3c9-0af3-467f-bad8-5ecc40c04454', '1', 'ffd670a4-d545-40f8-b718-e0eb12054c4b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLAMP V-BAND',PartNumber=N'2412634',Price=3.40225018146625,ManufacturerGuid=N'8c9be3c9-0af3-467f-bad8-5ecc40c04454',IsMasterData='1' WHERE Guid = 'ffd670a4-d545-40f8-b718-e0eb12054c4b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'213b83e2-0342-4dfe-a4e9-d85ee8a98837')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'213b83e2-0342-4dfe-a4e9-d85ee8a98837')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9b9821c1-9258-4434-8f66-95356860b301')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLAMP-BAND', N'1440367', 1.6295669005565, N'213b83e2-0342-4dfe-a4e9-d85ee8a98837', '1', '9b9821c1-9258-4434-8f66-95356860b301')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLAMP-BAND',PartNumber=N'1440367',Price=1.6295669005565,ManufacturerGuid=N'213b83e2-0342-4dfe-a4e9-d85ee8a98837',IsMasterData='1' WHERE Guid = '9b9821c1-9258-4434-8f66-95356860b301'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd02d6cbc-905c-46f1-80ad-27d9a3c08d66')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd02d6cbc-905c-46f1-80ad-27d9a3c08d66')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cf616686-1342-4685-982c-e2a5a8410b4f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0170050', 0.112388095814179, N'd02d6cbc-905c-46f1-80ad-27d9a3c08d66', '1', 'cf616686-1342-4685-982c-e2a5a8410b4f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0170050',Price=0.112388095814179,ManufacturerGuid=N'd02d6cbc-905c-46f1-80ad-27d9a3c08d66',IsMasterData='1' WHERE Guid = 'cf616686-1342-4685-982c-e2a5a8410b4f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2b63ee22-1538-4ff0-869f-ed6aac42e3e7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2b63ee22-1538-4ff0-869f-ed6aac42e3e7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '09a29386-246c-4100-90d7-eab9dead29b8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180063', 0.0675054439874183, N'2b63ee22-1538-4ff0-869f-ed6aac42e3e7', '1', '09a29386-246c-4100-90d7-eab9dead29b8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180063',Price=0.0675054439874183,ManufacturerGuid=N'2b63ee22-1538-4ff0-869f-ed6aac42e3e7',IsMasterData='1' WHERE Guid = '09a29386-246c-4100-90d7-eab9dead29b8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cc00563f-84ee-4318-b503-976e2caa6700')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cc00563f-84ee-4318-b503-976e2caa6700')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f3d51abc-c05d-42b2-8f28-590206bf8788')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180064', 0.0814178562787322, N'cc00563f-84ee-4318-b503-976e2caa6700', '1', 'f3d51abc-c05d-42b2-8f28-590206bf8788')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180064',Price=0.0814178562787322,ManufacturerGuid=N'cc00563f-84ee-4318-b503-976e2caa6700',IsMasterData='1' WHERE Guid = 'f3d51abc-c05d-42b2-8f28-590206bf8788'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30e914cc-4d53-4ca9-b8aa-ee137093719d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30e914cc-4d53-4ca9-b8aa-ee137093719d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '70d9b836-1f46-482d-95fd-54a816ecd204')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180067', 0.107790950883136, N'30e914cc-4d53-4ca9-b8aa-ee137093719d', '1', '70d9b836-1f46-482d-95fd-54a816ecd204')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180067',Price=0.107790950883136,ManufacturerGuid=N'30e914cc-4d53-4ca9-b8aa-ee137093719d',IsMasterData='1' WHERE Guid = '70d9b836-1f46-482d-95fd-54a816ecd204'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd6d501f0-a7fa-4172-bd55-c7bb17d4c1c8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd6d501f0-a7fa-4172-bd55-c7bb17d4c1c8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '563ed8cb-35e7-4e91-8093-f00f7fbc65e9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180069', 0.112509073312364, N'd6d501f0-a7fa-4172-bd55-c7bb17d4c1c8', '1', '563ed8cb-35e7-4e91-8093-f00f7fbc65e9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180069',Price=0.112509073312364,ManufacturerGuid=N'd6d501f0-a7fa-4172-bd55-c7bb17d4c1c8',IsMasterData='1' WHERE Guid = '563ed8cb-35e7-4e91-8093-f00f7fbc65e9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd665b769-e5a3-4942-b834-e63263d73c85')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd665b769-e5a3-4942-b834-e63263d73c85')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '42537c0e-2e23-4890-8739-ffbbf77ccb43')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180070', 0.103193805952093, N'd665b769-e5a3-4942-b834-e63263d73c85', '1', '42537c0e-2e23-4890-8739-ffbbf77ccb43')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180070',Price=0.103193805952093,ManufacturerGuid=N'd665b769-e5a3-4942-b834-e63263d73c85',IsMasterData='1' WHERE Guid = '42537c0e-2e23-4890-8739-ffbbf77ccb43'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fb494945-42fd-4031-a97d-c3be4597a646')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fb494945-42fd-4031-a97d-c3be4597a646')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f614aca4-e58b-4e6d-8ee6-11733dfe92a8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180072', 0.148439390273409, N'fb494945-42fd-4031-a97d-c3be4597a646', '1', 'f614aca4-e58b-4e6d-8ee6-11733dfe92a8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180072',Price=0.148439390273409,ManufacturerGuid=N'fb494945-42fd-4031-a97d-c3be4597a646',IsMasterData='1' WHERE Guid = 'f614aca4-e58b-4e6d-8ee6-11733dfe92a8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'55cf7758-77c4-4eb9-b9a2-1dffee9d8036')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'55cf7758-77c4-4eb9-b9a2-1dffee9d8036')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2772bb69-5953-45c1-98f2-d3a6ce1a6a07')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180074', 0.256835228647472, N'55cf7758-77c4-4eb9-b9a2-1dffee9d8036', '1', '2772bb69-5953-45c1-98f2-d3a6ce1a6a07')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180074',Price=0.256835228647472,ManufacturerGuid=N'55cf7758-77c4-4eb9-b9a2-1dffee9d8036',IsMasterData='1' WHERE Guid = '2772bb69-5953-45c1-98f2-d3a6ce1a6a07'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9008fb1a-d505-4b59-a89f-2b4ebe80ae23')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9008fb1a-d505-4b59-a89f-2b4ebe80ae23')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c56ee223-1854-4cdc-9661-880b782c3288')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0950911', 0.302443745463344, N'9008fb1a-d505-4b59-a89f-2b4ebe80ae23', '1', 'c56ee223-1854-4cdc-9661-880b782c3288')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0950911',Price=0.302443745463344,ManufacturerGuid=N'9008fb1a-d505-4b59-a89f-2b4ebe80ae23',IsMasterData='1' WHERE Guid = 'c56ee223-1854-4cdc-9661-880b782c3288'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a503f27c-12f2-434d-ad49-95af0c65c3c8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a503f27c-12f2-434d-ad49-95af0c65c3c8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dae9aba1-782b-497a-b19a-1a52b3356535')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2136A001', 0.110452455843213, N'a503f27c-12f2-434d-ad49-95af0c65c3c8', '1', 'dae9aba1-782b-497a-b19a-1a52b3356535')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2136A001',Price=0.110452455843213,ManufacturerGuid=N'a503f27c-12f2-434d-ad49-95af0c65c3c8',IsMasterData='1' WHERE Guid = 'dae9aba1-782b-497a-b19a-1a52b3356535'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e84e3020-61de-484e-8c33-46d1037049cd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e84e3020-61de-484e-8c33-46d1037049cd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '98e9e169-b7f6-4363-886a-83a9c6929e85')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481863', 0.0699249939511251, N'e84e3020-61de-484e-8c33-46d1037049cd', '1', '98e9e169-b7f6-4363-886a-83a9c6929e85')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481863',Price=0.0699249939511251,ManufacturerGuid=N'e84e3020-61de-484e-8c33-46d1037049cd',IsMasterData='1' WHERE Guid = '98e9e169-b7f6-4363-886a-83a9c6929e85'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'540a86d0-1b6c-4e4a-bc1d-f29caafe51c3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'540a86d0-1b6c-4e4a-bc1d-f29caafe51c3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fc58ae56-1176-4c51-bbbb-57a86de0181e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481873', 0.258286958625696, N'540a86d0-1b6c-4e4a-bc1d-f29caafe51c3', '1', 'fc58ae56-1176-4c51-bbbb-57a86de0181e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481873',Price=0.258286958625696,ManufacturerGuid=N'540a86d0-1b6c-4e4a-bc1d-f29caafe51c3',IsMasterData='1' WHERE Guid = 'fc58ae56-1176-4c51-bbbb-57a86de0181e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e9d621da-3ac6-426b-9b5c-ae563abb94b4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e9d621da-3ac6-426b-9b5c-ae563abb94b4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e996114d-e43f-4b97-afc7-d446366bc136')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481876', 0.0754899588676506, N'e9d621da-3ac6-426b-9b5c-ae563abb94b4', '1', 'e996114d-e43f-4b97-afc7-d446366bc136')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481876',Price=0.0754899588676506,ManufacturerGuid=N'e9d621da-3ac6-426b-9b5c-ae563abb94b4',IsMasterData='1' WHERE Guid = 'e996114d-e43f-4b97-afc7-d446366bc136'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'87b5caad-147c-4b07-ac3b-d78f962a27e2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'87b5caad-147c-4b07-ac3b-d78f962a27e2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4efb40fa-34a3-4430-b6ef-407c2ffad5ea')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481877', 0.0901282361480765, N'87b5caad-147c-4b07-ac3b-d78f962a27e2', '1', '4efb40fa-34a3-4430-b6ef-407c2ffad5ea')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481877',Price=0.0901282361480765,ManufacturerGuid=N'87b5caad-147c-4b07-ac3b-d78f962a27e2',IsMasterData='1' WHERE Guid = '4efb40fa-34a3-4430-b6ef-407c2ffad5ea'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'76426d91-e9b9-42a4-a60b-8287c089445e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'76426d91-e9b9-42a4-a60b-8287c089445e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '21a96aa5-94bc-4d14-ba3d-b6ee7eab95c2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481881', 0.282240503266392, N'76426d91-e9b9-42a4-a60b-8287c089445e', '1', '21a96aa5-94bc-4d14-ba3d-b6ee7eab95c2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481881',Price=0.282240503266392,ManufacturerGuid=N'76426d91-e9b9-42a4-a60b-8287c089445e',IsMasterData='1' WHERE Guid = '21a96aa5-94bc-4d14-ba3d-b6ee7eab95c2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8c1c43a9-c648-488a-bcf9-6249e392e85a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8c1c43a9-c648-488a-bcf9-6249e392e85a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7fd83114-863c-4697-92e1-9f49608f4d35')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481907', 1.47592547786112, N'8c1c43a9-c648-488a-bcf9-6249e392e85a', '1', '7fd83114-863c-4697-92e1-9f49608f4d35')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481907',Price=1.47592547786112,ManufacturerGuid=N'8c1c43a9-c648-488a-bcf9-6249e392e85a',IsMasterData='1' WHERE Guid = '7fd83114-863c-4697-92e1-9f49608f4d35'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'91b61d15-0583-4a54-992e-c646ecefd9c2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'91b61d15-0583-4a54-992e-c646ecefd9c2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3f365f6-b09b-4f26-ae93-b79968d5ca12')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481908', 1.50012097749819, N'91b61d15-0583-4a54-992e-c646ecefd9c2', '1', 'c3f365f6-b09b-4f26-ae93-b79968d5ca12')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481908',Price=1.50012097749819,ManufacturerGuid=N'91b61d15-0583-4a54-992e-c646ecefd9c2',IsMasterData='1' WHERE Guid = 'c3f365f6-b09b-4f26-ae93-b79968d5ca12'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'91982173-fa2b-41af-ab62-c13039ec103c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'91982173-fa2b-41af-ab62-c13039ec103c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1a98324b-7c0b-4def-bbda-e08e88b3a163')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481B020', 0.262642148560368, N'91982173-fa2b-41af-ab62-c13039ec103c', '1', '1a98324b-7c0b-4def-bbda-e08e88b3a163')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481B020',Price=0.262642148560368,ManufacturerGuid=N'91982173-fa2b-41af-ab62-c13039ec103c',IsMasterData='1' WHERE Guid = '1a98324b-7c0b-4def-bbda-e08e88b3a163'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'981d99c1-8437-4df6-b17a-9e66ddb2e7c9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'981d99c1-8437-4df6-b17a-9e66ddb2e7c9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6f086c18-ee2f-4b8d-9ebf-6f44be743cc1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481B023', 0.309823372852649, N'981d99c1-8437-4df6-b17a-9e66ddb2e7c9', '1', '6f086c18-ee2f-4b8d-9ebf-6f44be743cc1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481B023',Price=0.309823372852649,ManufacturerGuid=N'981d99c1-8437-4df6-b17a-9e66ddb2e7c9',IsMasterData='1' WHERE Guid = '6f086c18-ee2f-4b8d-9ebf-6f44be743cc1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'596260be-a287-4ae0-ac33-de81d0e68b47')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'596260be-a287-4ae0-ac33-de81d0e68b47')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '460f9bf0-1e29-4eac-b1a0-0c26a269960a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481B025', 0.400314541495282, N'596260be-a287-4ae0-ac33-de81d0e68b47', '1', '460f9bf0-1e29-4eac-b1a0-0c26a269960a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481B025',Price=0.400314541495282,ManufacturerGuid=N'596260be-a287-4ae0-ac33-de81d0e68b47',IsMasterData='1' WHERE Guid = '460f9bf0-1e29-4eac-b1a0-0c26a269960a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4de8a905-b8b5-4e62-b3a9-617f6c13a26d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4de8a905-b8b5-4e62-b3a9-617f6c13a26d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '221535a2-8ef3-4c84-ae95-c972b8543b30')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481B027', 0.287321558190177, N'4de8a905-b8b5-4e62-b3a9-617f6c13a26d', '1', '221535a2-8ef3-4c84-ae95-c972b8543b30')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481B027',Price=0.287321558190177,ManufacturerGuid=N'4de8a905-b8b5-4e62-b3a9-617f6c13a26d',IsMasterData='1' WHERE Guid = '221535a2-8ef3-4c84-ae95-c972b8543b30'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2cdb2d00-2716-4a80-827a-4097fd172c34')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2cdb2d00-2716-4a80-827a-4097fd172c34')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '76c4e3e6-daff-489a-8147-2cdf9a012eb3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481C002', 0.0292765545608517, N'2cdb2d00-2716-4a80-827a-4097fd172c34', '1', '76c4e3e6-daff-489a-8147-2cdf9a012eb3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481C002',Price=0.0292765545608517,ManufacturerGuid=N'2cdb2d00-2716-4a80-827a-4097fd172c34',IsMasterData='1' WHERE Guid = '76c4e3e6-daff-489a-8147-2cdf9a012eb3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b2e95aaa-2b01-401f-91d0-c613c5df9ebc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b2e95aaa-2b01-401f-91d0-c613c5df9ebc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '18f66900-3024-4463-b818-2fb111bbcf06')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D062', 0.177957899830632, N'b2e95aaa-2b01-401f-91d0-c613c5df9ebc', '1', '18f66900-3024-4463-b818-2fb111bbcf06')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D062',Price=0.177957899830632,ManufacturerGuid=N'b2e95aaa-2b01-401f-91d0-c613c5df9ebc',IsMasterData='1' WHERE Guid = '18f66900-3024-4463-b818-2fb111bbcf06'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a5454444-12bb-475b-9310-907921eb4a9b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a5454444-12bb-475b-9310-907921eb4a9b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e3f77c16-56ad-45dc-930a-cf47d10afb94')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D073', 0.416404548753932, N'a5454444-12bb-475b-9310-907921eb4a9b', '1', 'e3f77c16-56ad-45dc-930a-cf47d10afb94')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D073',Price=0.416404548753932,ManufacturerGuid=N'a5454444-12bb-475b-9310-907921eb4a9b',IsMasterData='1' WHERE Guid = 'e3f77c16-56ad-45dc-930a-cf47d10afb94'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c83c5547-fafe-49bf-8780-70910abf76b6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c83c5547-fafe-49bf-8780-70910abf76b6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '12a1d6cb-2de0-4496-9b06-26bb001b5443')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D074', 0.46576336801355, N'c83c5547-fafe-49bf-8780-70910abf76b6', '1', '12a1d6cb-2de0-4496-9b06-26bb001b5443')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D074',Price=0.46576336801355,ManufacturerGuid=N'c83c5547-fafe-49bf-8780-70910abf76b6',IsMasterData='1' WHERE Guid = '12a1d6cb-2de0-4496-9b06-26bb001b5443'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6ad2813c-246d-4c2b-8f7b-99ef06d3053d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6ad2813c-246d-4c2b-8f7b-99ef06d3053d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6dbf0e13-1fc4-4da4-847d-e58d6f7f0fc0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D075', 0.480401645293975, N'6ad2813c-246d-4c2b-8f7b-99ef06d3053d', '1', '6dbf0e13-1fc4-4da4-847d-e58d6f7f0fc0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D075',Price=0.480401645293975,ManufacturerGuid=N'6ad2813c-246d-4c2b-8f7b-99ef06d3053d',IsMasterData='1' WHERE Guid = '6dbf0e13-1fc4-4da4-847d-e58d6f7f0fc0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4d6aa23d-a18e-4d64-a3e5-6890e71682f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4d6aa23d-a18e-4d64-a3e5-6890e71682f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8276b8fd-82a5-41c5-b1b2-fcc70baeeaff')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D076', 0.683885797241713, N'4d6aa23d-a18e-4d64-a3e5-6890e71682f5', '1', '8276b8fd-82a5-41c5-b1b2-fcc70baeeaff')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D076',Price=0.683885797241713,ManufacturerGuid=N'4d6aa23d-a18e-4d64-a3e5-6890e71682f5',IsMasterData='1' WHERE Guid = '8276b8fd-82a5-41c5-b1b2-fcc70baeeaff'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f113fb43-16e0-4054-ad90-b878b5b2d6b2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f113fb43-16e0-4054-ad90-b878b5b2d6b2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '94500968-f60d-4eb8-a17d-e755286a7d7b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D082', 2.98209533026857, N'f113fb43-16e0-4054-ad90-b878b5b2d6b2', '1', '94500968-f60d-4eb8-a17d-e755286a7d7b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D082',Price=2.98209533026857,ManufacturerGuid=N'f113fb43-16e0-4054-ad90-b878b5b2d6b2',IsMasterData='1' WHERE Guid = '94500968-f60d-4eb8-a17d-e755286a7d7b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'26ca22bc-db72-4a05-9b68-cf038473a772')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'26ca22bc-db72-4a05-9b68-cf038473a772')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '25223423-9711-4b37-a313-effbdfb73b9f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D088', 1.25756109363658, N'26ca22bc-db72-4a05-9b68-cf038473a772', '1', '25223423-9711-4b37-a313-effbdfb73b9f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D088',Price=1.25756109363658,ManufacturerGuid=N'26ca22bc-db72-4a05-9b68-cf038473a772',IsMasterData='1' WHERE Guid = '25223423-9711-4b37-a313-effbdfb73b9f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0e065219-772b-460f-9712-6a3bee1f55cc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0e065219-772b-460f-9712-6a3bee1f55cc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '68abb5df-0415-411e-9415-0e93fe59e203')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D156', 0.391846116622308, N'0e065219-772b-460f-9712-6a3bee1f55cc', '1', '68abb5df-0415-411e-9415-0e93fe59e203')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D156',Price=0.391846116622308,ManufacturerGuid=N'0e065219-772b-460f-9712-6a3bee1f55cc',IsMasterData='1' WHERE Guid = '68abb5df-0415-411e-9415-0e93fe59e203'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bd2beeef-b386-42f0-8d87-f568e9f2e654')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bd2beeef-b386-42f0-8d87-f568e9f2e654')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd29f2507-5433-4816-a248-f105cfc9f1c1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D161', 0.422816356157755, N'bd2beeef-b386-42f0-8d87-f568e9f2e654', '1', 'd29f2507-5433-4816-a248-f105cfc9f1c1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D161',Price=0.422816356157755,ManufacturerGuid=N'bd2beeef-b386-42f0-8d87-f568e9f2e654',IsMasterData='1' WHERE Guid = 'd29f2507-5433-4816-a248-f105cfc9f1c1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2b2a3dc1-5975-4092-b489-4569f7a139f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2b2a3dc1-5975-4092-b489-4569f7a139f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2e313e08-d68d-42a3-a66b-48787ecb5a54')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D162', 0.579845148802323, N'2b2a3dc1-5975-4092-b489-4569f7a139f5', '1', '2e313e08-d68d-42a3-a66b-48787ecb5a54')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D162',Price=0.579845148802323,ManufacturerGuid=N'2b2a3dc1-5975-4092-b489-4569f7a139f5',IsMasterData='1' WHERE Guid = '2e313e08-d68d-42a3-a66b-48787ecb5a54'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0dea78d5-b491-4558-8831-562e330f1e05')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0dea78d5-b491-4558-8831-562e330f1e05')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8b8ab418-abc1-4b7f-ad6f-52a60ad1ccb6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D163', 0.252238083716429, N'0dea78d5-b491-4558-8831-562e330f1e05', '1', '8b8ab418-abc1-4b7f-ad6f-52a60ad1ccb6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D163',Price=0.252238083716429,ManufacturerGuid=N'0dea78d5-b491-4558-8831-562e330f1e05',IsMasterData='1' WHERE Guid = '8b8ab418-abc1-4b7f-ad6f-52a60ad1ccb6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dc5b4629-7f19-476a-8f71-eab3fc3e6741')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dc5b4629-7f19-476a-8f71-eab3fc3e6741')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '657909b2-f089-40e9-bfb4-e87af46a82e7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D208', 0.103193805952093, N'dc5b4629-7f19-476a-8f71-eab3fc3e6741', '1', '657909b2-f089-40e9-bfb4-e87af46a82e7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D208',Price=0.103193805952093,ManufacturerGuid=N'dc5b4629-7f19-476a-8f71-eab3fc3e6741',IsMasterData='1' WHERE Guid = '657909b2-f089-40e9-bfb4-e87af46a82e7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c9d8f3c6-2828-4b4d-a138-5d477fdbdc2c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c9d8f3c6-2828-4b4d-a138-5d477fdbdc2c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0d03bbaf-bb3a-48e9-85ef-2edd8fcee40f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D212', 0.0837164287442536, N'c9d8f3c6-2828-4b4d-a138-5d477fdbdc2c', '1', '0d03bbaf-bb3a-48e9-85ef-2edd8fcee40f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D212',Price=0.0837164287442536,ManufacturerGuid=N'c9d8f3c6-2828-4b4d-a138-5d477fdbdc2c',IsMasterData='1' WHERE Guid = '0d03bbaf-bb3a-48e9-85ef-2edd8fcee40f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8324ee55-0a2d-4e31-bdfd-743df3cfb9ce')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8324ee55-0a2d-4e31-bdfd-743df3cfb9ce')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0168c316-a5a4-4f48-82d5-c4d92f7fbf32')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D252', 0.117953060730704, N'8324ee55-0a2d-4e31-bdfd-743df3cfb9ce', '1', '0168c316-a5a4-4f48-82d5-c4d92f7fbf32')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D252',Price=0.117953060730704,ManufacturerGuid=N'8324ee55-0a2d-4e31-bdfd-743df3cfb9ce',IsMasterData='1' WHERE Guid = '0168c316-a5a4-4f48-82d5-c4d92f7fbf32'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'de9c87ab-e31a-4e0e-b29e-0c9a6afeb284')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'de9c87ab-e31a-4e0e-b29e-0c9a6afeb284')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '39a7f424-b98c-4fd4-9cf5-3e1ef0565c90')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D257', 0.120977498185338, N'de9c87ab-e31a-4e0e-b29e-0c9a6afeb284', '1', '39a7f424-b98c-4fd4-9cf5-3e1ef0565c90')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D257',Price=0.120977498185338,ManufacturerGuid=N'de9c87ab-e31a-4e0e-b29e-0c9a6afeb284',IsMasterData='1' WHERE Guid = '39a7f424-b98c-4fd4-9cf5-3e1ef0565c90'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4a920d72-88bb-4898-aaf8-1026c45ca13a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4a920d72-88bb-4898-aaf8-1026c45ca13a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd0eb2ee6-db79-4a7d-993a-830f335f9c81')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D302', 0.0434309218485362, N'4a920d72-88bb-4898-aaf8-1026c45ca13a', '1', 'd0eb2ee6-db79-4a7d-993a-830f335f9c81')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D302',Price=0.0434309218485362,ManufacturerGuid=N'4a920d72-88bb-4898-aaf8-1026c45ca13a',IsMasterData='1' WHERE Guid = 'd0eb2ee6-db79-4a7d-993a-830f335f9c81'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'133c6d96-a640-494c-83cd-975a33e9ff06')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'133c6d96-a640-494c-83cd-975a33e9ff06')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '76d3152d-251b-496d-b4b9-952fac8e614a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D314', 0.153641422695379, N'133c6d96-a640-494c-83cd-975a33e9ff06', '1', '76d3152d-251b-496d-b4b9-952fac8e614a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D314',Price=0.153641422695379,ManufacturerGuid=N'133c6d96-a640-494c-83cd-975a33e9ff06',IsMasterData='1' WHERE Guid = '76d3152d-251b-496d-b4b9-952fac8e614a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95863e70-fb3b-4c27-8da4-efe3c02efbdc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95863e70-fb3b-4c27-8da4-efe3c02efbdc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5aaef245-acc7-4e00-8474-2f5c213f1453')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D318', 0.269537865956932, N'95863e70-fb3b-4c27-8da4-efe3c02efbdc', '1', '5aaef245-acc7-4e00-8474-2f5c213f1453')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D318',Price=0.269537865956932,ManufacturerGuid=N'95863e70-fb3b-4c27-8da4-efe3c02efbdc',IsMasterData='1' WHERE Guid = '5aaef245-acc7-4e00-8474-2f5c213f1453'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'02448e68-8821-499b-9e8d-eecf4cb9a9b8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'02448e68-8821-499b-9e8d-eecf4cb9a9b8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4c1b2a33-a6be-4170-b1b0-d0d2e7c15063')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D319', 0.149528187757077, N'02448e68-8821-499b-9e8d-eecf4cb9a9b8', '1', '4c1b2a33-a6be-4170-b1b0-d0d2e7c15063')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D319',Price=0.149528187757077,ManufacturerGuid=N'02448e68-8821-499b-9e8d-eecf4cb9a9b8',IsMasterData='1' WHERE Guid = '4c1b2a33-a6be-4170-b1b0-d0d2e7c15063'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5fa977f1-a613-434f-afbf-b72c6bf87a60')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5fa977f1-a613-434f-afbf-b72c6bf87a60')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6cdb9bb8-4e4f-4771-a1cf-0ae180e6416e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D323', 0.151221872731672, N'5fa977f1-a613-434f-afbf-b72c6bf87a60', '1', '6cdb9bb8-4e4f-4771-a1cf-0ae180e6416e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D323',Price=0.151221872731672,ManufacturerGuid=N'5fa977f1-a613-434f-afbf-b72c6bf87a60',IsMasterData='1' WHERE Guid = '6cdb9bb8-4e4f-4771-a1cf-0ae180e6416e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8a947842-e753-4afe-9ef1-dc26a4ef78e5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8a947842-e753-4afe-9ef1-dc26a4ef78e5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '86f28108-f8a3-4bb5-bbe5-e8b835684615')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D326', 0.217759496733608, N'8a947842-e753-4afe-9ef1-dc26a4ef78e5', '1', '86f28108-f8a3-4bb5-bbe5-e8b835684615')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D326',Price=0.217759496733608,ManufacturerGuid=N'8a947842-e753-4afe-9ef1-dc26a4ef78e5',IsMasterData='1' WHERE Guid = '86f28108-f8a3-4bb5-bbe5-e8b835684615'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95368ffa-76b9-4e1e-9f12-27c183a7181d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95368ffa-76b9-4e1e-9f12-27c183a7181d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '140803ea-7e5b-499a-9664-31b2a8fa3107')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D405', 0.535567384466489, N'95368ffa-76b9-4e1e-9f12-27c183a7181d', '1', '140803ea-7e5b-499a-9664-31b2a8fa3107')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D405',Price=0.535567384466489,ManufacturerGuid=N'95368ffa-76b9-4e1e-9f12-27c183a7181d',IsMasterData='1' WHERE Guid = '140803ea-7e5b-499a-9664-31b2a8fa3107'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'41363a65-1163-4bf2-891f-94cdec002bd4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'41363a65-1163-4bf2-891f-94cdec002bd4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '05f4f37d-d040-48ae-9d76-6d32b596a5f1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D406', 0.155819017662715, N'41363a65-1163-4bf2-891f-94cdec002bd4', '1', '05f4f37d-d040-48ae-9d76-6d32b596a5f1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D406',Price=0.155819017662715,ManufacturerGuid=N'41363a65-1163-4bf2-891f-94cdec002bd4',IsMasterData='1' WHERE Guid = '05f4f37d-d040-48ae-9d76-6d32b596a5f1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b8746971-e5a5-4d24-a7b0-65fd36d7f6e0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b8746971-e5a5-4d24-a7b0-65fd36d7f6e0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'df855dff-661c-4f1a-996a-0c59b613558b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D407', 0.214251149286233, N'b8746971-e5a5-4d24-a7b0-65fd36d7f6e0', '1', 'df855dff-661c-4f1a-996a-0c59b613558b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D407',Price=0.214251149286233,ManufacturerGuid=N'b8746971-e5a5-4d24-a7b0-65fd36d7f6e0',IsMasterData='1' WHERE Guid = 'df855dff-661c-4f1a-996a-0c59b613558b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e2e295f9-a65a-4f7d-860a-0885a6f468d6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e2e295f9-a65a-4f7d-860a-0885a6f468d6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9fdf1621-996e-41df-bc95-7a33feba471a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D454', 0.0561335591579966, N'e2e295f9-a65a-4f7d-860a-0885a6f468d6', '1', '9fdf1621-996e-41df-bc95-7a33feba471a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D454',Price=0.0561335591579966,ManufacturerGuid=N'e2e295f9-a65a-4f7d-860a-0885a6f468d6',IsMasterData='1' WHERE Guid = '9fdf1621-996e-41df-bc95-7a33feba471a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'14011680-92e8-4cda-8952-e4c9f925c5e6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'14011680-92e8-4cda-8952-e4c9f925c5e6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e8536aa6-3678-4dfa-a238-d18a298589ec')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D456', 0.107911928381321, N'14011680-92e8-4cda-8952-e4c9f925c5e6', '1', 'e8536aa6-3678-4dfa-a238-d18a298589ec')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D456',Price=0.107911928381321,ManufacturerGuid=N'14011680-92e8-4cda-8952-e4c9f925c5e6',IsMasterData='1' WHERE Guid = 'e8536aa6-3678-4dfa-a238-d18a298589ec'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4c635622-6fff-411c-8eb0-c1239fbf1f84')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4c635622-6fff-411c-8eb0-c1239fbf1f84')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ecc8a503-c02b-43a0-9237-acf43cf3a69b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D501', 0.816598112751028, N'4c635622-6fff-411c-8eb0-c1239fbf1f84', '1', 'ecc8a503-c02b-43a0-9237-acf43cf3a69b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D501',Price=0.816598112751028,ManufacturerGuid=N'4c635622-6fff-411c-8eb0-c1239fbf1f84',IsMasterData='1' WHERE Guid = 'ecc8a503-c02b-43a0-9237-acf43cf3a69b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'124451c0-c4e9-4bee-af4e-35776db2bc17')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'124451c0-c4e9-4bee-af4e-35776db2bc17')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '250eb144-9272-41b1-a0a9-8f1e95df887c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D554', 0.0898862811517058, N'124451c0-c4e9-4bee-af4e-35776db2bc17', '1', '250eb144-9272-41b1-a0a9-8f1e95df887c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D554',Price=0.0898862811517058,ManufacturerGuid=N'124451c0-c4e9-4bee-af4e-35776db2bc17',IsMasterData='1' WHERE Guid = '250eb144-9272-41b1-a0a9-8f1e95df887c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'388a53bb-7daa-4cd5-80fb-8d7bfbdffadf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'388a53bb-7daa-4cd5-80fb-8d7bfbdffadf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c2939eb2-fd43-4d07-b57a-5bd38f7fc98d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D558', 0.056980401645294, N'388a53bb-7daa-4cd5-80fb-8d7bfbdffadf', '1', 'c2939eb2-fd43-4d07-b57a-5bd38f7fc98d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D558',Price=0.056980401645294,ManufacturerGuid=N'388a53bb-7daa-4cd5-80fb-8d7bfbdffadf',IsMasterData='1' WHERE Guid = 'c2939eb2-fd43-4d07-b57a-5bd38f7fc98d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'417fec0d-e40f-4e8d-b647-db98ce62e05e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'417fec0d-e40f-4e8d-b647-db98ce62e05e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '097e7114-6135-41b5-8ad2-aaaf02be17cb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D559', 0.0659327365110089, N'417fec0d-e40f-4e8d-b647-db98ce62e05e', '1', '097e7114-6135-41b5-8ad2-aaaf02be17cb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D559',Price=0.0659327365110089,ManufacturerGuid=N'417fec0d-e40f-4e8d-b647-db98ce62e05e',IsMasterData='1' WHERE Guid = '097e7114-6135-41b5-8ad2-aaaf02be17cb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cf810cb6-1f47-4343-a77b-040e1c4487a3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cf810cb6-1f47-4343-a77b-040e1c4487a3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5345940e-7785-48e1-ab26-9fec3af4b073')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2481D657', 0.1146866682797, N'cf810cb6-1f47-4343-a77b-040e1c4487a3', '1', '5345940e-7785-48e1-ab26-9fec3af4b073')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2481D657',Price=0.1146866682797,ManufacturerGuid=N'cf810cb6-1f47-4343-a77b-040e1c4487a3',IsMasterData='1' WHERE Guid = '5345940e-7785-48e1-ab26-9fec3af4b073'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fd8a0fa6-d480-4262-b791-f7c1a1a2862f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fd8a0fa6-d480-4262-b791-f7c1a1a2862f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '988115d7-5274-490d-8a6f-0383fcb1e91b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'2811A851', 0.0907331236390031, N'fd8a0fa6-d480-4262-b791-f7c1a1a2862f', '1', '988115d7-5274-490d-8a6f-0383fcb1e91b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'2811A851',Price=0.0907331236390031,ManufacturerGuid=N'fd8a0fa6-d480-4262-b791-f7c1a1a2862f',IsMasterData='1' WHERE Guid = '988115d7-5274-490d-8a6f-0383fcb1e91b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e69d9e21-07db-4041-afd5-7e306a6c2a90')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e69d9e21-07db-4041-afd5-7e306a6c2a90')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dc8745ea-10e6-4cd9-bdf4-807405316301')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'3570646', 0.0858940237115896, N'e69d9e21-07db-4041-afd5-7e306a6c2a90', '1', 'dc8745ea-10e6-4cd9-bdf4-807405316301')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'3570646',Price=0.0858940237115896,ManufacturerGuid=N'e69d9e21-07db-4041-afd5-7e306a6c2a90',IsMasterData='1' WHERE Guid = 'dc8745ea-10e6-4cd9-bdf4-807405316301'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e8def920-63d4-4e4e-b9da-1f02347dec91')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e8def920-63d4-4e4e-b9da-1f02347dec91')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c7239a67-2a82-433d-aa21-c86fca2e6905')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'3645A032', 0.689571739656424, N'e8def920-63d4-4e4e-b9da-1f02347dec91', '1', 'c7239a67-2a82-433d-aa21-c86fca2e6905')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'3645A032',Price=0.689571739656424,ManufacturerGuid=N'e8def920-63d4-4e4e-b9da-1f02347dec91',IsMasterData='1' WHERE Guid = 'c7239a67-2a82-433d-aa21-c86fca2e6905'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3e79f102-e532-424f-83b6-3d1bf4eb24cf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3e79f102-e532-424f-83b6-3d1bf4eb24cf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '13c0bed6-6ae9-485b-bb55-5c6b858bd50a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'36568143', 0.169852407452214, N'3e79f102-e532-424f-83b6-3d1bf4eb24cf', '1', '13c0bed6-6ae9-485b-bb55-5c6b858bd50a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'36568143',Price=0.169852407452214,ManufacturerGuid=N'3e79f102-e532-424f-83b6-3d1bf4eb24cf',IsMasterData='1' WHERE Guid = '13c0bed6-6ae9-485b-bb55-5c6b858bd50a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd2e2a13c-e7a9-4fca-8e4f-69fd4c85e251')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd2e2a13c-e7a9-4fca-8e4f-69fd4c85e251')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9eb4a7c0-6aad-446b-a54e-b07fb69891ec')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'3656A023', 0.0589160416162594, N'd2e2a13c-e7a9-4fca-8e4f-69fd4c85e251', '1', '9eb4a7c0-6aad-446b-a54e-b07fb69891ec')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'3656A023',Price=0.0589160416162594,ManufacturerGuid=N'd2e2a13c-e7a9-4fca-8e4f-69fd4c85e251',IsMasterData='1' WHERE Guid = '9eb4a7c0-6aad-446b-a54e-b07fb69891ec'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'105acac8-ee9d-423b-9dfa-8757404bc24e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'105acac8-ee9d-423b-9dfa-8757404bc24e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '22058b64-ec0f-4b3a-b32c-231cb5e76512')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'36631118', 0.0780304863295427, N'105acac8-ee9d-423b-9dfa-8757404bc24e', '1', '22058b64-ec0f-4b3a-b32c-231cb5e76512')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'36631118',Price=0.0780304863295427,ManufacturerGuid=N'105acac8-ee9d-423b-9dfa-8757404bc24e',IsMasterData='1' WHERE Guid = '22058b64-ec0f-4b3a-b32c-231cb5e76512'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'91d9f239-5364-451d-bdc1-5c444bd4be21')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'91d9f239-5364-451d-bdc1-5c444bd4be21')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a71af8fc-1891-4a3e-8b4d-39a547dd3624')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'4175A006', 1.1831599322526, N'91d9f239-5364-451d-bdc1-5c444bd4be21', '1', 'a71af8fc-1891-4a3e-8b4d-39a547dd3624')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'4175A006',Price=1.1831599322526,ManufacturerGuid=N'91d9f239-5364-451d-bdc1-5c444bd4be21',IsMasterData='1' WHERE Guid = 'a71af8fc-1891-4a3e-8b4d-39a547dd3624'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'16bd8291-96cc-4fb1-aff3-be2205137a5c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'16bd8291-96cc-4fb1-aff3-be2205137a5c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b607a99d-fb0e-47a5-a858-2b6936c35051')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'4D-9517', 0.110089523348657, N'16bd8291-96cc-4fb1-aff3-be2205137a5c', '1', 'b607a99d-fb0e-47a5-a858-2b6936c35051')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'4D-9517',Price=0.110089523348657,ManufacturerGuid=N'16bd8291-96cc-4fb1-aff3-be2205137a5c',IsMasterData='1' WHERE Guid = 'b607a99d-fb0e-47a5-a858-2b6936c35051'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1fc4bb6d-4200-4dab-a842-a5eed8f7121a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1fc4bb6d-4200-4dab-a842-a5eed8f7121a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '856c6c4e-8c48-45e7-a2de-38880cb59bd4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'5P4325', 0.423421243648681, N'1fc4bb6d-4200-4dab-a842-a5eed8f7121a', '1', '856c6c4e-8c48-45e7-a2de-38880cb59bd4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'5P4325',Price=0.423421243648681,ManufacturerGuid=N'1fc4bb6d-4200-4dab-a842-a5eed8f7121a',IsMasterData='1' WHERE Guid = '856c6c4e-8c48-45e7-a2de-38880cb59bd4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2f6948cd-66f6-4b21-a770-c0b3fc462428')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2f6948cd-66f6-4b21-a770-c0b3fc462428')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a866cde9-2fcc-47b3-a20a-a82376844ab8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'9M-4995', 0.133075248003871, N'2f6948cd-66f6-4b21-a770-c0b3fc462428', '1', 'a866cde9-2fcc-47b3-a20a-a82376844ab8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'9M-4995',Price=0.133075248003871,ManufacturerGuid=N'2f6948cd-66f6-4b21-a770-c0b3fc462428',IsMasterData='1' WHERE Guid = 'a866cde9-2fcc-47b3-a20a-a82376844ab8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c01ec0b3-d40d-4d43-afe3-157b80d0eb8e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c01ec0b3-d40d-4d43-afe3-157b80d0eb8e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '877b95d6-8ab0-4751-9f68-4b6cdcad8670')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'0180066', 0.10222598596661, N'c01ec0b3-d40d-4d43-afe3-157b80d0eb8e', '1', '877b95d6-8ab0-4751-9f68-4b6cdcad8670')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'0180066',Price=0.10222598596661,ManufacturerGuid=N'c01ec0b3-d40d-4d43-afe3-157b80d0eb8e',IsMasterData='1' WHERE Guid = '877b95d6-8ab0-4751-9f68-4b6cdcad8670'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0a889413-2eb2-46e3-b221-31fdb21bad54')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0a889413-2eb2-46e3-b221-31fdb21bad54')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '817bf027-0312-4337-ba5b-8b24d295e7ac')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'3914841', 0.296394870554077, N'0a889413-2eb2-46e3-b221-31fdb21bad54', '1', '817bf027-0312-4337-ba5b-8b24d295e7ac')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'3914841',Price=0.296394870554077,ManufacturerGuid=N'0a889413-2eb2-46e3-b221-31fdb21bad54',IsMasterData='1' WHERE Guid = '817bf027-0312-4337-ba5b-8b24d295e7ac'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'07125b75-5bc2-422b-9410-77dab958ddcc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'07125b75-5bc2-422b-9410-77dab958ddcc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '42a33e33-d38a-47b9-bdf2-11a06bb34746')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP', N'K138AF02', 0.172513912412291, N'07125b75-5bc2-422b-9410-77dab958ddcc', '1', '42a33e33-d38a-47b9-bdf2-11a06bb34746')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP',PartNumber=N'K138AF02',Price=0.172513912412291,ManufacturerGuid=N'07125b75-5bc2-422b-9410-77dab958ddcc',IsMasterData='1' WHERE Guid = '42a33e33-d38a-47b9-bdf2-11a06bb34746'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1f16f33a-72f4-4068-b330-5659f354a9db')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1f16f33a-72f4-4068-b330-5659f354a9db')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c2342ef8-7829-40ac-8cba-542ba68272b6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LADDER', N'1325789', 0.095693201064602, N'1f16f33a-72f4-4068-b330-5659f354a9db', '1', 'c2342ef8-7829-40ac-8cba-542ba68272b6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LADDER',PartNumber=N'1325789',Price=0.095693201064602,ManufacturerGuid=N'1f16f33a-72f4-4068-b330-5659f354a9db',IsMasterData='1' WHERE Guid = 'c2342ef8-7829-40ac-8cba-542ba68272b6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a50ca07d-c3c9-496f-8d0c-31b7ae37528e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a50ca07d-c3c9-496f-8d0c-31b7ae37528e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '34a33dc1-6978-4a8b-8884-58665598c30e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LADDER', N'3523968', 6.00048390999274, N'a50ca07d-c3c9-496f-8d0c-31b7ae37528e', '1', '34a33dc1-6978-4a8b-8884-58665598c30e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LADDER',PartNumber=N'3523968',Price=6.00048390999274,ManufacturerGuid=N'a50ca07d-c3c9-496f-8d0c-31b7ae37528e',IsMasterData='1' WHERE Guid = '34a33dc1-6978-4a8b-8884-58665598c30e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cc703500-b420-4dd4-b863-dd35ab47f310')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cc703500-b420-4dd4-b863-dd35ab47f310')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '36271835-edd5-4946-9b06-60cfc2eb4423')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'3316036', 3.71884829421728, N'cc703500-b420-4dd4-b863-dd35ab47f310', '1', '36271835-edd5-4946-9b06-60cfc2eb4423')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'3316036',Price=3.71884829421728,ManufacturerGuid=N'cc703500-b420-4dd4-b863-dd35ab47f310',IsMasterData='1' WHERE Guid = '36271835-edd5-4946-9b06-60cfc2eb4423'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f3744ce5-2b02-40a4-9b82-7152a9ff71be')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f3744ce5-2b02-40a4-9b82-7152a9ff71be')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '78537c24-9635-4344-b254-f9274ea40008')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'3587686', 2.21993709170094, N'f3744ce5-2b02-40a4-9b82-7152a9ff71be', '1', '78537c24-9635-4344-b254-f9274ea40008')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'3587686',Price=2.21993709170094,ManufacturerGuid=N'f3744ce5-2b02-40a4-9b82-7152a9ff71be',IsMasterData='1' WHERE Guid = '78537c24-9635-4344-b254-f9274ea40008'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'21f25cae-3d85-4444-a093-86600b164b52')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'21f25cae-3d85-4444-a093-86600b164b52')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f4ab2836-058f-41a1-954a-fb9b9e557290')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'3445360', 12.0372610694411, N'21f25cae-3d85-4444-a093-86600b164b52', '1', 'f4ab2836-058f-41a1-954a-fb9b9e557290')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'3445360',Price=12.0372610694411,ManufacturerGuid=N'21f25cae-3d85-4444-a093-86600b164b52',IsMasterData='1' WHERE Guid = 'f4ab2836-058f-41a1-954a-fb9b9e557290'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'82c4ca8f-8de6-4d48-97d9-e980760510c1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'82c4ca8f-8de6-4d48-97d9-e980760510c1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c69f905e-ef3f-47e0-b03b-252b6304ced8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'0964840', 0.244374546334382, N'82c4ca8f-8de6-4d48-97d9-e980760510c1', '1', 'c69f905e-ef3f-47e0-b03b-252b6304ced8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'0964840',Price=0.244374546334382,ManufacturerGuid=N'82c4ca8f-8de6-4d48-97d9-e980760510c1',IsMasterData='1' WHERE Guid = 'c69f905e-ef3f-47e0-b03b-252b6304ced8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6626ef59-385c-464f-9501-29f5e5f3cbbc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6626ef59-385c-464f-9501-29f5e5f3cbbc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'afba8e5e-46ae-4eac-ad7e-5f34def7d5d3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'1254124', 1.40333897894992, N'6626ef59-385c-464f-9501-29f5e5f3cbbc', '1', 'afba8e5e-46ae-4eac-ad7e-5f34def7d5d3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'1254124',Price=1.40333897894992,ManufacturerGuid=N'6626ef59-385c-464f-9501-29f5e5f3cbbc',IsMasterData='1' WHERE Guid = 'afba8e5e-46ae-4eac-ad7e-5f34def7d5d3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e5a7586c-1aa0-451d-bfde-e06284f2fda9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e5a7586c-1aa0-451d-bfde-e06284f2fda9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3c7167ee-bfc1-4a75-b41c-fa1bbb29453f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-LOOP', N'1343988', 0.172997822405033, N'e5a7586c-1aa0-451d-bfde-e06284f2fda9', '1', '3c7167ee-bfc1-4a75-b41c-fa1bbb29453f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-LOOP',PartNumber=N'1343988',Price=0.172997822405033,ManufacturerGuid=N'e5a7586c-1aa0-451d-bfde-e06284f2fda9',IsMasterData='1' WHERE Guid = '3c7167ee-bfc1-4a75-b41c-fa1bbb29453f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'55f71b58-b91c-4fda-9bc5-9e405577d2b3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'55f71b58-b91c-4fda-9bc5-9e405577d2b3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eaf24d49-72c7-42f3-995b-17e1262b1cdb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'3424723', 0.0919428986208565, N'55f71b58-b91c-4fda-9bc5-9e405577d2b3', '1', 'eaf24d49-72c7-42f3-995b-17e1262b1cdb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'3424723',Price=0.0919428986208565,ManufacturerGuid=N'55f71b58-b91c-4fda-9bc5-9e405577d2b3',IsMasterData='1' WHERE Guid = 'eaf24d49-72c7-42f3-995b-17e1262b1cdb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'82940974-aea3-47e3-927f-e93f865e2970')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'82940974-aea3-47e3-927f-e93f865e2970')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cdc2d5db-c743-48a5-a39d-fe2ff3bc83cd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'3460406', 0.135494797967578, N'82940974-aea3-47e3-927f-e93f865e2970', '1', 'cdc2d5db-c743-48a5-a39d-fe2ff3bc83cd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'3460406',Price=0.135494797967578,ManufacturerGuid=N'82940974-aea3-47e3-927f-e93f865e2970',IsMasterData='1' WHERE Guid = 'cdc2d5db-c743-48a5-a39d-fe2ff3bc83cd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2f742b70-92c2-4a09-8af4-79733b4577bc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2f742b70-92c2-4a09-8af4-79733b4577bc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a3952b5d-44ad-4a27-a9ed-adea58a3e653')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'3473429', 0.058069199128962, N'2f742b70-92c2-4a09-8af4-79733b4577bc', '1', 'a3952b5d-44ad-4a27-a9ed-adea58a3e653')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'3473429',Price=0.058069199128962,ManufacturerGuid=N'2f742b70-92c2-4a09-8af4-79733b4577bc',IsMasterData='1' WHERE Guid = 'a3952b5d-44ad-4a27-a9ed-adea58a3e653'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b72fe9e2-7923-4685-806d-05e7416b6a1a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b72fe9e2-7923-4685-806d-05e7416b6a1a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4272a278-eaf6-4b88-bb2d-53acbae8a7ab')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'3579545', 0.0846842487297363, N'b72fe9e2-7923-4685-806d-05e7416b6a1a', '1', '4272a278-eaf6-4b88-bb2d-53acbae8a7ab')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'3579545',Price=0.0846842487297363,ManufacturerGuid=N'b72fe9e2-7923-4685-806d-05e7416b6a1a',IsMasterData='1' WHERE Guid = '4272a278-eaf6-4b88-bb2d-53acbae8a7ab'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'42ea6ac8-aa92-4378-849b-37f914ca11d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'42ea6ac8-aa92-4378-849b-37f914ca11d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b861167a-0cae-4652-ba8e-e043dbbd9d9c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'2481D164', 0.392511492862328, N'42ea6ac8-aa92-4378-849b-37f914ca11d3', '1', 'b861167a-0cae-4652-ba8e-e043dbbd9d9c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'2481D164',Price=0.392511492862328,ManufacturerGuid=N'42ea6ac8-aa92-4378-849b-37f914ca11d3',IsMasterData='1' WHERE Guid = 'b861167a-0cae-4652-ba8e-e043dbbd9d9c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30f91f85-3b1a-454b-b9ba-cdc0a667c920')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30f91f85-3b1a-454b-b9ba-cdc0a667c920')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2c66b25d-c22e-4f06-833f-00e9af84ed48')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'2481D562', 0.0592789741108154, N'30f91f85-3b1a-454b-b9ba-cdc0a667c920', '1', '2c66b25d-c22e-4f06-833f-00e9af84ed48')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'2481D562',Price=0.0592789741108154,ManufacturerGuid=N'30f91f85-3b1a-454b-b9ba-cdc0a667c920',IsMasterData='1' WHERE Guid = '2c66b25d-c22e-4f06-833f-00e9af84ed48'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'295ea1bf-a819-49d7-bd95-071d65b09bd2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'295ea1bf-a819-49d7-bd95-071d65b09bd2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a3112421-2d91-47d0-a70d-7615b06e58f3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CLIP-PIPE', N'3665A005', 0.520203242196951, N'295ea1bf-a819-49d7-bd95-071d65b09bd2', '1', 'a3112421-2d91-47d0-a70d-7615b06e58f3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CLIP-PIPE',PartNumber=N'3665A005',Price=0.520203242196951,ManufacturerGuid=N'295ea1bf-a819-49d7-bd95-071d65b09bd2',IsMasterData='1' WHERE Guid = 'a3112421-2d91-47d0-a70d-7615b06e58f3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2aa071cf-61b8-4bcd-8eaf-3e2e0943edd8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2aa071cf-61b8-4bcd-8eaf-3e2e0943edd8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cf43e7f0-afb4-4197-a488-94a9c2254b49')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3354012', 2.03242196951367, N'2aa071cf-61b8-4bcd-8eaf-3e2e0943edd8', '1', 'cf43e7f0-afb4-4197-a488-94a9c2254b49')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3354012',Price=2.03242196951367,ManufacturerGuid=N'2aa071cf-61b8-4bcd-8eaf-3e2e0943edd8',IsMasterData='1' WHERE Guid = 'cf43e7f0-afb4-4197-a488-94a9c2254b49'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd7cc2a23-9a44-4043-8fb6-15382b8d34fe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd7cc2a23-9a44-4043-8fb6-15382b8d34fe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '682cfb98-97fa-465d-82da-15d5828b69dd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'0201397', 1.27026373094604, N'd7cc2a23-9a44-4043-8fb6-15382b8d34fe', '1', '682cfb98-97fa-465d-82da-15d5828b69dd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'0201397',Price=1.27026373094604,ManufacturerGuid=N'd7cc2a23-9a44-4043-8fb6-15382b8d34fe',IsMasterData='1' WHERE Guid = '682cfb98-97fa-465d-82da-15d5828b69dd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8b732b8b-8ad4-4c0e-85c7-9093deb797f3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8b732b8b-8ad4-4c0e-85c7-9093deb797f3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '66c8d5df-aba6-4dda-8d11-02f48905e82a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'0206002', 0.229857246552141, N'8b732b8b-8ad4-4c0e-85c7-9093deb797f3', '1', '66c8d5df-aba6-4dda-8d11-02f48905e82a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'0206002',Price=0.229857246552141,ManufacturerGuid=N'8b732b8b-8ad4-4c0e-85c7-9093deb797f3',IsMasterData='1' WHERE Guid = '66c8d5df-aba6-4dda-8d11-02f48905e82a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5093960d-a1e0-4112-ac57-2e2cc623a9f4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5093960d-a1e0-4112-ac57-2e2cc623a9f4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3ba9777-87c3-4d51-98c8-8def65f47237')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'0206008', 0.359303169610452, N'5093960d-a1e0-4112-ac57-2e2cc623a9f4', '1', 'c3ba9777-87c3-4d51-98c8-8def65f47237')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'0206008',Price=0.359303169610452,ManufacturerGuid=N'5093960d-a1e0-4112-ac57-2e2cc623a9f4',IsMasterData='1' WHERE Guid = 'c3ba9777-87c3-4d51-98c8-8def65f47237'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'39350c00-c0c4-43e5-ab50-2c349f32a203')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'39350c00-c0c4-43e5-ab50-2c349f32a203')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '35726831-386d-47b9-832f-5dade3b38600')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'0470672', 0.496007742559884, N'39350c00-c0c4-43e5-ab50-2c349f32a203', '1', '35726831-386d-47b9-832f-5dade3b38600')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'0470672',Price=0.496007742559884,ManufacturerGuid=N'39350c00-c0c4-43e5-ab50-2c349f32a203',IsMasterData='1' WHERE Guid = '35726831-386d-47b9-832f-5dade3b38600'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6354feee-3680-424a-8fab-5dba202dc998')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6354feee-3680-424a-8fab-5dba202dc998')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3b7d80d-1cea-467a-a15a-2b7fcad41b56')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'1758P0242', 8.27486087587709, N'6354feee-3680-424a-8fab-5dba202dc998', '1', 'c3b7d80d-1cea-467a-a15a-2b7fcad41b56')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'1758P0242',Price=8.27486087587709,ManufacturerGuid=N'6354feee-3680-424a-8fab-5dba202dc998',IsMasterData='1' WHERE Guid = 'c3b7d80d-1cea-467a-a15a-2b7fcad41b56'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0c398ca1-18f2-4ea8-bd2a-a5b81912fa10')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0c398ca1-18f2-4ea8-bd2a-a5b81912fa10')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '303a6fae-1046-48b8-8605-38882de819e8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'2441A601', 0.58069199128962, N'0c398ca1-18f2-4ea8-bd2a-a5b81912fa10', '1', '303a6fae-1046-48b8-8605-38882de819e8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'2441A601',Price=0.58069199128962,ManufacturerGuid=N'0c398ca1-18f2-4ea8-bd2a-a5b81912fa10',IsMasterData='1' WHERE Guid = '303a6fae-1046-48b8-8605-38882de819e8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3d881567-7e56-4edc-943b-24b4b1f3ff98')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3d881567-7e56-4edc-943b-24b4b1f3ff98')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '41161f3a-7671-41e1-bffa-eebc16908e49')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'2447A002', 1.23397048149044, N'3d881567-7e56-4edc-943b-24b4b1f3ff98', '1', '41161f3a-7671-41e1-bffa-eebc16908e49')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'2447A002',Price=1.23397048149044,ManufacturerGuid=N'3d881567-7e56-4edc-943b-24b4b1f3ff98',IsMasterData='1' WHERE Guid = '41161f3a-7671-41e1-bffa-eebc16908e49'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'571db6e0-4406-4b20-8886-1878ec8d08e7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'571db6e0-4406-4b20-8886-1878ec8d08e7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f1b42a07-fbdb-4784-a7be-c56565d6f5f6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'2461A203', 1.45172997822405, N'571db6e0-4406-4b20-8886-1878ec8d08e7', '1', 'f1b42a07-fbdb-4784-a7be-c56565d6f5f6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'2461A203',Price=1.45172997822405,ManufacturerGuid=N'571db6e0-4406-4b20-8886-1878ec8d08e7',IsMasterData='1' WHERE Guid = 'f1b42a07-fbdb-4784-a7be-c56565d6f5f6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'25da6ed4-3394-4783-88ee-828ef93598be')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'25da6ed4-3394-4783-88ee-828ef93598be')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f78c5de7-0f03-4498-9f17-b7699d3c7769')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'31723314', 2.28647471570288, N'25da6ed4-3394-4783-88ee-828ef93598be', '1', 'f78c5de7-0f03-4498-9f17-b7699d3c7769')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'31723314',Price=2.28647471570288,ManufacturerGuid=N'25da6ed4-3394-4783-88ee-828ef93598be',IsMasterData='1' WHERE Guid = 'f78c5de7-0f03-4498-9f17-b7699d3c7769'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a70f4b5e-0099-414a-a156-989f13d08631')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a70f4b5e-0099-414a-a156-989f13d08631')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9df93cea-97a3-473e-a53f-79fe2775f9e3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'31726241', 1.0041132349383, N'a70f4b5e-0099-414a-a156-989f13d08631', '1', '9df93cea-97a3-473e-a53f-79fe2775f9e3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'31726241',Price=1.0041132349383,ManufacturerGuid=N'a70f4b5e-0099-414a-a156-989f13d08631',IsMasterData='1' WHERE Guid = '9df93cea-97a3-473e-a53f-79fe2775f9e3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1c3250a5-f37f-4ed9-ba53-f5a2fb749a88')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1c3250a5-f37f-4ed9-ba53-f5a2fb749a88')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f2864492-02da-4e0d-af3a-33b6a2c65277')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172C008', 1.13718848294217, N'1c3250a5-f37f-4ed9-ba53-f5a2fb749a88', '1', 'f2864492-02da-4e0d-af3a-33b6a2c65277')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172C008',Price=1.13718848294217,ManufacturerGuid=N'1c3250a5-f37f-4ed9-ba53-f5a2fb749a88',IsMasterData='1' WHERE Guid = 'f2864492-02da-4e0d-af3a-33b6a2c65277'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b8124cf9-26c6-41e5-8364-dd1eaab2c8aa')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b8124cf9-26c6-41e5-8364-dd1eaab2c8aa')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3c522094-af93-4de9-b4c3-3f194a15babf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172H001', 1.71788047423179, N'b8124cf9-26c6-41e5-8364-dd1eaab2c8aa', '1', '3c522094-af93-4de9-b4c3-3f194a15babf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172H001',Price=1.71788047423179,ManufacturerGuid=N'b8124cf9-26c6-41e5-8364-dd1eaab2c8aa',IsMasterData='1' WHERE Guid = '3c522094-af93-4de9-b4c3-3f194a15babf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0920d600-eff9-474c-95ab-783f3b03f3b5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0920d600-eff9-474c-95ab-783f3b03f3b5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a95afc1e-6008-4a18-a338-b61ef2cea490')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172P031', 1.88011129929833, N'0920d600-eff9-474c-95ab-783f3b03f3b5', '1', 'a95afc1e-6008-4a18-a338-b61ef2cea490')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172P031',Price=1.88011129929833,ManufacturerGuid=N'0920d600-eff9-474c-95ab-783f3b03f3b5',IsMasterData='1' WHERE Guid = 'a95afc1e-6008-4a18-a338-b61ef2cea490'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'354ac7f9-4209-4ef6-aea1-867294e9ba0c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'354ac7f9-4209-4ef6-aea1-867294e9ba0c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '92530a14-5380-4553-afc5-f5b18f38d531')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172P032', 1.41495281877571, N'354ac7f9-4209-4ef6-aea1-867294e9ba0c', '1', '92530a14-5380-4553-afc5-f5b18f38d531')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172P032',Price=1.41495281877571,ManufacturerGuid=N'354ac7f9-4209-4ef6-aea1-867294e9ba0c',IsMasterData='1' WHERE Guid = '92530a14-5380-4553-afc5-f5b18f38d531'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'18464706-49a5-4ad5-b31f-e4e2328bd4b0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'18464706-49a5-4ad5-b31f-e4e2328bd4b0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '86c96c48-4b64-4901-963c-583df69baf55')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172T003', 1.39124122913138, N'18464706-49a5-4ad5-b31f-e4e2328bd4b0', '1', '86c96c48-4b64-4901-963c-583df69baf55')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172T003',Price=1.39124122913138,ManufacturerGuid=N'18464706-49a5-4ad5-b31f-e4e2328bd4b0',IsMasterData='1' WHERE Guid = '86c96c48-4b64-4901-963c-583df69baf55'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'36799b09-9fdd-43dd-8608-55c78e3d1514')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'36799b09-9fdd-43dd-8608-55c78e3d1514')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1fe53657-42d8-4610-853a-a900abfd7382')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3172X008', 3.77449794338253, N'36799b09-9fdd-43dd-8608-55c78e3d1514', '1', '1fe53657-42d8-4610-853a-a900abfd7382')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3172X008',Price=3.77449794338253,ManufacturerGuid=N'36799b09-9fdd-43dd-8608-55c78e3d1514',IsMasterData='1' WHERE Guid = '1fe53657-42d8-4610-853a-a900abfd7382'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'353a9399-c22b-45a7-b0ec-6924696c3122')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'353a9399-c22b-45a7-b0ec-6924696c3122')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aed1c7dd-9618-4567-8686-a6988ced3304')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3306141', 0.665376240019356, N'353a9399-c22b-45a7-b0ec-6924696c3122', '1', 'aed1c7dd-9618-4567-8686-a6988ced3304')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3306141',Price=0.665376240019356,ManufacturerGuid=N'353a9399-c22b-45a7-b0ec-6924696c3122',IsMasterData='1' WHERE Guid = 'aed1c7dd-9618-4567-8686-a6988ced3304'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61f45c7b-193f-4576-8b20-5b90870b3966')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61f45c7b-193f-4576-8b20-5b90870b3966')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a65a382d-b417-43d4-9ea5-a97ea90b12fc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3342A101', 0.471812242922816, N'61f45c7b-193f-4576-8b20-5b90870b3966', '1', 'a65a382d-b417-43d4-9ea5-a97ea90b12fc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3342A101',Price=0.471812242922816,ManufacturerGuid=N'61f45c7b-193f-4576-8b20-5b90870b3966',IsMasterData='1' WHERE Guid = 'a65a382d-b417-43d4-9ea5-a97ea90b12fc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6e88d440-5768-4cf2-9bf1-b0be46174dd8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6e88d440-5768-4cf2-9bf1-b0be46174dd8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f5f423e7-2c08-4642-a84b-12004fda4a29')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3342A701', 0.727195741592064, N'6e88d440-5768-4cf2-9bf1-b0be46174dd8', '1', 'f5f423e7-2c08-4642-a84b-12004fda4a29')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3342A701',Price=0.727195741592064,ManufacturerGuid=N'6e88d440-5768-4cf2-9bf1-b0be46174dd8',IsMasterData='1' WHERE Guid = 'f5f423e7-2c08-4642-a84b-12004fda4a29'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8fa98b8f-bb0a-4460-868f-305d1d0a7750')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8fa98b8f-bb0a-4460-868f-305d1d0a7750')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '27ec9487-5862-4c1c-b76f-840ab0809cee')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3345175', 2.21388821679168, N'8fa98b8f-bb0a-4460-868f-305d1d0a7750', '1', '27ec9487-5862-4c1c-b76f-840ab0809cee')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3345175',Price=2.21388821679168,ManufacturerGuid=N'8fa98b8f-bb0a-4460-868f-305d1d0a7750',IsMasterData='1' WHERE Guid = '27ec9487-5862-4c1c-b76f-840ab0809cee'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e42babf1-c63e-4fb8-bf17-938ff0334c68')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e42babf1-c63e-4fb8-bf17-938ff0334c68')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '056f5a4f-7292-45ef-bf06-fcc75afe3752')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3346A009', 0.423421243648681, N'e42babf1-c63e-4fb8-bf17-938ff0334c68', '1', '056f5a4f-7292-45ef-bf06-fcc75afe3752')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3346A009',Price=0.423421243648681,ManufacturerGuid=N'e42babf1-c63e-4fb8-bf17-938ff0334c68',IsMasterData='1' WHERE Guid = '056f5a4f-7292-45ef-bf06-fcc75afe3752'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4e6f868d-7715-4bfd-b173-758ff68870fa')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4e6f868d-7715-4bfd-b173-758ff68870fa')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b3d99ed2-b727-41a4-944e-01f04807116e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3346A011', 2.70989595935156, N'4e6f868d-7715-4bfd-b173-758ff68870fa', '1', 'b3d99ed2-b727-41a4-944e-01f04807116e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3346A011',Price=2.70989595935156,ManufacturerGuid=N'4e6f868d-7715-4bfd-b173-758ff68870fa',IsMasterData='1' WHERE Guid = 'b3d99ed2-b727-41a4-944e-01f04807116e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'99775ddf-61b3-4687-9765-4870654bc34a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'99775ddf-61b3-4687-9765-4870654bc34a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2dbd9091-1a0a-4837-95f3-7912be2f2bb0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3353A015', 0.931526736027099, N'99775ddf-61b3-4687-9765-4870654bc34a', '1', '2dbd9091-1a0a-4837-95f3-7912be2f2bb0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3353A015',Price=0.931526736027099,ManufacturerGuid=N'99775ddf-61b3-4687-9765-4870654bc34a',IsMasterData='1' WHERE Guid = '2dbd9091-1a0a-4837-95f3-7912be2f2bb0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'147b415d-57e6-4673-be0f-183dad8ffb87')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'147b415d-57e6-4673-be0f-183dad8ffb87')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'faaac7d5-fb82-43e8-8126-155f415ff4fe')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A018', 0.728163561577547, N'147b415d-57e6-4673-be0f-183dad8ffb87', '1', 'faaac7d5-fb82-43e8-8126-155f415ff4fe')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A018',Price=0.728163561577547,ManufacturerGuid=N'147b415d-57e6-4673-be0f-183dad8ffb87',IsMasterData='1' WHERE Guid = 'faaac7d5-fb82-43e8-8126-155f415ff4fe'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0f6d5aa8-a6d6-4b65-a161-cf73ed410bdf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0f6d5aa8-a6d6-4b65-a161-cf73ed410bdf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '45d810f5-fa09-4e20-babf-28bf050e1817')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A019', 0.740382288894266, N'0f6d5aa8-a6d6-4b65-a161-cf73ed410bdf', '1', '45d810f5-fa09-4e20-babf-28bf050e1817')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A019',Price=0.740382288894266,ManufacturerGuid=N'0f6d5aa8-a6d6-4b65-a161-cf73ed410bdf',IsMasterData='1' WHERE Guid = '45d810f5-fa09-4e20-babf-28bf050e1817'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c9ea5867-9b8f-4653-b9dd-962bb86bba26')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c9ea5867-9b8f-4653-b9dd-962bb86bba26')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '33244fa7-0927-42ad-b412-249075f1e43a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A021', 0.435518993467215, N'c9ea5867-9b8f-4653-b9dd-962bb86bba26', '1', '33244fa7-0927-42ad-b412-249075f1e43a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A021',Price=0.435518993467215,ManufacturerGuid=N'c9ea5867-9b8f-4653-b9dd-962bb86bba26',IsMasterData='1' WHERE Guid = '33244fa7-0927-42ad-b412-249075f1e43a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'22e3ba68-da33-4a8f-b0f8-74e4df8c2913')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'22e3ba68-da33-4a8f-b0f8-74e4df8c2913')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4f57729e-2ee7-44c2-b6d5-51bffe5d32a6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A023', 0.689571739656424, N'22e3ba68-da33-4a8f-b0f8-74e4df8c2913', '1', '4f57729e-2ee7-44c2-b6d5-51bffe5d32a6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A023',Price=0.689571739656424,ManufacturerGuid=N'22e3ba68-da33-4a8f-b0f8-74e4df8c2913',IsMasterData='1' WHERE Guid = '4f57729e-2ee7-44c2-b6d5-51bffe5d32a6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'117b2fdd-90ed-4cd8-824c-f8b7b7fcc744')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'117b2fdd-90ed-4cd8-824c-f8b7b7fcc744')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aa196ff9-7119-45d7-8612-54713075f3c2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A025', 0.819259617711106, N'117b2fdd-90ed-4cd8-824c-f8b7b7fcc744', '1', 'aa196ff9-7119-45d7-8612-54713075f3c2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A025',Price=0.819259617711106,ManufacturerGuid=N'117b2fdd-90ed-4cd8-824c-f8b7b7fcc744',IsMasterData='1' WHERE Guid = 'aa196ff9-7119-45d7-8612-54713075f3c2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd82c816a-71d1-4a6b-9a08-ac769d629b07')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd82c816a-71d1-4a6b-9a08-ac769d629b07')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '76031291-a977-4007-a68b-772412c6e166')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A032', 1.42753447858698, N'd82c816a-71d1-4a6b-9a08-ac769d629b07', '1', '76031291-a977-4007-a68b-772412c6e166')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A032',Price=1.42753447858698,ManufacturerGuid=N'd82c816a-71d1-4a6b-9a08-ac769d629b07',IsMasterData='1' WHERE Guid = '76031291-a977-4007-a68b-772412c6e166'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c8a843e4-91af-4fb2-8556-62db74bc18e8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c8a843e4-91af-4fb2-8556-62db74bc18e8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd0d9e2b0-be6f-433d-a967-e63b10f77473')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A033', 1.28236148076458, N'c8a843e4-91af-4fb2-8556-62db74bc18e8', '1', 'd0d9e2b0-be6f-433d-a967-e63b10f77473')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A033',Price=1.28236148076458,ManufacturerGuid=N'c8a843e4-91af-4fb2-8556-62db74bc18e8',IsMasterData='1' WHERE Guid = 'd0d9e2b0-be6f-433d-a967-e63b10f77473'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8d0ffe97-0bfc-47ac-bf5b-f12b937a4639')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8d0ffe97-0bfc-47ac-bf5b-f12b937a4639')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9b9d4ede-f18b-4dd8-8c47-601616796660')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A034', 1.57270747640939, N'8d0ffe97-0bfc-47ac-bf5b-f12b937a4639', '1', '9b9d4ede-f18b-4dd8-8c47-601616796660')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A034',Price=1.57270747640939,ManufacturerGuid=N'8d0ffe97-0bfc-47ac-bf5b-f12b937a4639',IsMasterData='1' WHERE Guid = '9b9d4ede-f18b-4dd8-8c47-601616796660'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9baa4318-9f2d-4586-bef9-e22647f6abb4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9baa4318-9f2d-4586-bef9-e22647f6abb4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '85ce1f98-64e2-4951-9fc0-514092a5994f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355A035', 3.99225744011614, N'9baa4318-9f2d-4586-bef9-e22647f6abb4', '1', '85ce1f98-64e2-4951-9fc0-514092a5994f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355A035',Price=3.99225744011614,ManufacturerGuid=N'9baa4318-9f2d-4586-bef9-e22647f6abb4',IsMasterData='1' WHERE Guid = '85ce1f98-64e2-4951-9fc0-514092a5994f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4d7b9627-0096-444a-b2c0-c47d1669dceb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4d7b9627-0096-444a-b2c0-c47d1669dceb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ffb2d595-ee58-46ae-bdfd-b16ad9a63c7c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355C007', 5.60125816598113, N'4d7b9627-0096-444a-b2c0-c47d1669dceb', '1', 'ffb2d595-ee58-46ae-bdfd-b16ad9a63c7c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355C007',Price=5.60125816598113,ManufacturerGuid=N'4d7b9627-0096-444a-b2c0-c47d1669dceb',IsMasterData='1' WHERE Guid = 'ffb2d595-ee58-46ae-bdfd-b16ad9a63c7c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9d0f913b-3cce-43b0-9b45-e8eb02942457')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9d0f913b-3cce-43b0-9b45-e8eb02942457')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7b4c174f-52d0-4654-976f-850faf9a4d76')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355E009', 0.933946285990806, N'9d0f913b-3cce-43b0-9b45-e8eb02942457', '1', '7b4c174f-52d0-4654-976f-850faf9a4d76')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355E009',Price=0.933946285990806,ManufacturerGuid=N'9d0f913b-3cce-43b0-9b45-e8eb02942457',IsMasterData='1' WHERE Guid = '7b4c174f-52d0-4654-976f-850faf9a4d76'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9fd491c4-6d2b-4468-ba40-b3ea57920671')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9fd491c4-6d2b-4468-ba40-b3ea57920671')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '43a48c62-0898-497f-9766-e7d15193d05c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3355M004', 0.757319138640213, N'9fd491c4-6d2b-4468-ba40-b3ea57920671', '1', '43a48c62-0898-497f-9766-e7d15193d05c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3355M004',Price=0.757319138640213,ManufacturerGuid=N'9fd491c4-6d2b-4468-ba40-b3ea57920671',IsMasterData='1' WHERE Guid = '43a48c62-0898-497f-9766-e7d15193d05c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'46a408d1-77d8-4208-8ffa-5248c13c33e8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'46a408d1-77d8-4208-8ffa-5248c13c33e8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd9fa2123-494e-49c2-b0d5-e517d8ffb049')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3433917', 1.01621098475684, N'46a408d1-77d8-4208-8ffa-5248c13c33e8', '1', 'd9fa2123-494e-49c2-b0d5-e517d8ffb049')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3433917',Price=1.01621098475684,ManufacturerGuid=N'46a408d1-77d8-4208-8ffa-5248c13c33e8',IsMasterData='1' WHERE Guid = 'd9fa2123-494e-49c2-b0d5-e517d8ffb049'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e3281d88-3353-4983-99cf-b48e5f985442')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e3281d88-3353-4983-99cf-b48e5f985442')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e34f38f4-50ed-4596-8328-70565d29b10a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3433918', 1.01621098475684, N'e3281d88-3353-4983-99cf-b48e5f985442', '1', 'e34f38f4-50ed-4596-8328-70565d29b10a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3433918',Price=1.01621098475684,ManufacturerGuid=N'e3281d88-3353-4983-99cf-b48e5f985442',IsMasterData='1' WHERE Guid = 'e34f38f4-50ed-4596-8328-70565d29b10a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'089c0984-90a4-4483-b95a-e6d663f95a46')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'089c0984-90a4-4483-b95a-e6d663f95a46')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '41b1466a-42f4-4f3e-bd1a-c394f07759a1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTION', N'3769998', 18.4248729736269, N'089c0984-90a4-4483-b95a-e6d663f95a46', '1', '41b1466a-42f4-4f3e-bd1a-c394f07759a1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTION',PartNumber=N'3769998',Price=18.4248729736269,ManufacturerGuid=N'089c0984-90a4-4483-b95a-e6d663f95a46',IsMasterData='1' WHERE Guid = '41b1466a-42f4-4f3e-bd1a-c394f07759a1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'46b58590-8135-435d-b43f-2a87208bf30c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'46b58590-8135-435d-b43f-2a87208bf30c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bd2c7a21-ed0b-46a5-a8bb-0d951321c6eb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTOR', N'3382367', 0.791555770626663, N'46b58590-8135-435d-b43f-2a87208bf30c', '1', 'bd2c7a21-ed0b-46a5-a8bb-0d951321c6eb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTOR',PartNumber=N'3382367',Price=0.791555770626663,ManufacturerGuid=N'46b58590-8135-435d-b43f-2a87208bf30c',IsMasterData='1' WHERE Guid = 'bd2c7a21-ed0b-46a5-a8bb-0d951321c6eb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0c93c064-6eef-4268-8e88-9a7a106aa416')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0c93c064-6eef-4268-8e88-9a7a106aa416')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3d5e716-4ef0-48da-a8cb-600868804ece')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTOR', N'3295549', 1.83885797241713, N'0c93c064-6eef-4268-8e88-9a7a106aa416', '1', 'c3d5e716-4ef0-48da-a8cb-600868804ece')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTOR',PartNumber=N'3295549',Price=1.83885797241713,ManufacturerGuid=N'0c93c064-6eef-4268-8e88-9a7a106aa416',IsMasterData='1' WHERE Guid = 'c3d5e716-4ef0-48da-a8cb-600868804ece'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dee1bae9-2acc-456f-8f6d-5b480c60423e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dee1bae9-2acc-456f-8f6d-5b480c60423e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1b34dbe5-a85e-4b43-8f5c-e4eb13dad973')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'CONNECTOR', N'3369333', 1.06895717396564, N'dee1bae9-2acc-456f-8f6d-5b480c60423e', '1', '1b34dbe5-a85e-4b43-8f5c-e4eb13dad973')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'CONNECTOR',PartNumber=N'3369333',Price=1.06895717396564,ManufacturerGuid=N'dee1bae9-2acc-456f-8f6d-5b480c60423e',IsMasterData='1' WHERE Guid = '1b34dbe5-a85e-4b43-8f5c-e4eb13dad973'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ae6cd1bd-7fb3-46c6-be30-d6a87a37f26b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ae6cd1bd-7fb3-46c6-be30-d6a87a37f26b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4a72f791-3e68-42b5-a3be-7a31eaf7c63e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'0350010', 0.0532300992015485, N'ae6cd1bd-7fb3-46c6-be30-d6a87a37f26b', '1', '4a72f791-3e68-42b5-a3be-7a31eaf7c63e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'0350010',Price=0.0532300992015485,ManufacturerGuid=N'ae6cd1bd-7fb3-46c6-be30-d6a87a37f26b',IsMasterData='1' WHERE Guid = '4a72f791-3e68-42b5-a3be-7a31eaf7c63e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bbbe32ff-703e-4fe9-b8f8-3e84fd62337f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bbbe32ff-703e-4fe9-b8f8-3e84fd62337f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '451c97f5-6bba-4542-98a2-1b676b6cfae8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'0350018', 0.250544398741834, N'bbbe32ff-703e-4fe9-b8f8-3e84fd62337f', '1', '451c97f5-6bba-4542-98a2-1b676b6cfae8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'0350018',Price=0.250544398741834,ManufacturerGuid=N'bbbe32ff-703e-4fe9-b8f8-3e84fd62337f',IsMasterData='1' WHERE Guid = '451c97f5-6bba-4542-98a2-1b676b6cfae8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1d27896d-a5fd-4508-abd3-1d0a9b9b3bd7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1d27896d-a5fd-4508-abd3-1d0a9b9b3bd7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '53eb98e4-373b-4e67-b138-1b1aecb5335a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'2114A046', 0.116017420759739, N'1d27896d-a5fd-4508-abd3-1d0a9b9b3bd7', '1', '53eb98e4-373b-4e67-b138-1b1aecb5335a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'2114A046',Price=0.116017420759739,ManufacturerGuid=N'1d27896d-a5fd-4508-abd3-1d0a9b9b3bd7',IsMasterData='1' WHERE Guid = '53eb98e4-373b-4e67-b138-1b1aecb5335a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b6a79161-53a2-471a-976c-284e4ae70c95')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b6a79161-53a2-471a-976c-284e4ae70c95')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0018107b-74e7-4810-9b9b-345479b8e9d4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'2116A301', 0.0636341640454875, N'b6a79161-53a2-471a-976c-284e4ae70c95', '1', '0018107b-74e7-4810-9b9b-345479b8e9d4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'2116A301',Price=0.0636341640454875,ManufacturerGuid=N'b6a79161-53a2-471a-976c-284e4ae70c95',IsMasterData='1' WHERE Guid = '0018107b-74e7-4810-9b9b-345479b8e9d4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1889566c-48d9-4d95-b11b-4a7c0e3462e8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1889566c-48d9-4d95-b11b-4a7c0e3462e8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd323b049-0a8c-4520-9740-8013e5d9c58f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'2116A655', 0.0255262521171062, N'1889566c-48d9-4d95-b11b-4a7c0e3462e8', '1', 'd323b049-0a8c-4520-9740-8013e5d9c58f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'2116A655',Price=0.0255262521171062,ManufacturerGuid=N'1889566c-48d9-4d95-b11b-4a7c0e3462e8',IsMasterData='1' WHERE Guid = 'd323b049-0a8c-4520-9740-8013e5d9c58f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6dd5fc38-e6ab-4a72-a1d1-32be5636fb7b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6dd5fc38-e6ab-4a72-a1d1-32be5636fb7b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f5329d20-ed39-46a1-b594-80fe621faa69')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'2116A672', 0.154367287684491, N'6dd5fc38-e6ab-4a72-a1d1-32be5636fb7b', '1', 'f5329d20-ed39-46a1-b594-80fe621faa69')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'2116A672',Price=0.154367287684491,ManufacturerGuid=N'6dd5fc38-e6ab-4a72-a1d1-32be5636fb7b',IsMasterData='1' WHERE Guid = 'f5329d20-ed39-46a1-b594-80fe621faa69'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9176ba87-30fc-4c73-877f-acb2042428b1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9176ba87-30fc-4c73-877f-acb2042428b1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6a72b7f3-9064-408e-85be-9c1e50485ded')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'29990001', 0.246310186305347, N'9176ba87-30fc-4c73-877f-acb2042428b1', '1', '6a72b7f3-9064-408e-85be-9c1e50485ded')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'29990001',Price=0.246310186305347,ManufacturerGuid=N'9176ba87-30fc-4c73-877f-acb2042428b1',IsMasterData='1' WHERE Guid = '6a72b7f3-9064-408e-85be-9c1e50485ded'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6410edb5-3503-40ff-a369-57aa5d30be3d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6410edb5-3503-40ff-a369-57aa5d30be3d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1294b67f-3751-4553-9de9-47c932b904d7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'3244A009', 0.0982337285264941, N'6410edb5-3503-40ff-a369-57aa5d30be3d', '1', '1294b67f-3751-4553-9de9-47c932b904d7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'3244A009',Price=0.0982337285264941,ManufacturerGuid=N'6410edb5-3503-40ff-a369-57aa5d30be3d',IsMasterData='1' WHERE Guid = '1294b67f-3751-4553-9de9-47c932b904d7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4a837a8b-1816-498e-aa7e-d0501306c114')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4a837a8b-1816-498e-aa7e-d0501306c114')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7c8fc432-b92c-45d8-8bcb-2a4502717638')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'32455119', 0.159085410113719, N'4a837a8b-1816-498e-aa7e-d0501306c114', '1', '7c8fc432-b92c-45d8-8bcb-2a4502717638')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'32455119',Price=0.159085410113719,ManufacturerGuid=N'4a837a8b-1816-498e-aa7e-d0501306c114',IsMasterData='1' WHERE Guid = '7c8fc432-b92c-45d8-8bcb-2a4502717638'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f953803f-b55b-4b46-a90f-a4e8e1caedb2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f953803f-b55b-4b46-a90f-a4e8e1caedb2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c56b667d-0060-4953-9bf4-5d3d6124af5c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'3245A011', 0.12714735059279, N'f953803f-b55b-4b46-a90f-a4e8e1caedb2', '1', 'c56b667d-0060-4953-9bf4-5d3d6124af5c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'3245A011',Price=0.12714735059279,ManufacturerGuid=N'f953803f-b55b-4b46-a90f-a4e8e1caedb2',IsMasterData='1' WHERE Guid = 'c56b667d-0060-4953-9bf4-5d3d6124af5c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f7671c24-1aa8-416d-b334-6db87f53cfa0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f7671c24-1aa8-416d-b334-6db87f53cfa0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '09b38a05-05b1-4ad4-be95-84547acfba61')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'3246A001', 0.0619404790708928, N'f7671c24-1aa8-416d-b334-6db87f53cfa0', '1', '09b38a05-05b1-4ad4-be95-84547acfba61')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'3246A001',Price=0.0619404790708928,ManufacturerGuid=N'f7671c24-1aa8-416d-b334-6db87f53cfa0',IsMasterData='1' WHERE Guid = '09b38a05-05b1-4ad4-be95-84547acfba61'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'82a087e6-f232-480d-bd25-f20352b2a420')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'82a087e6-f232-480d-bd25-f20352b2a420')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a3ba148e-3b43-404b-bfe2-f494dcd68a8e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'3313A014', 0.162472780062908, N'82a087e6-f232-480d-bd25-f20352b2a420', '1', 'a3ba148e-3b43-404b-bfe2-f494dcd68a8e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'3313A014',Price=0.162472780062908,ManufacturerGuid=N'82a087e6-f232-480d-bd25-f20352b2a420',IsMasterData='1' WHERE Guid = 'a3ba148e-3b43-404b-bfe2-f494dcd68a8e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a23819e1-392d-4781-997d-deff6609b51d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a23819e1-392d-4781-997d-deff6609b51d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4793c0cb-17fd-42c5-a8ad-3049d0891d14')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'DOWEL', N'3313A036', 0.135857730462134, N'a23819e1-392d-4781-997d-deff6609b51d', '1', '4793c0cb-17fd-42c5-a8ad-3049d0891d14')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'DOWEL',PartNumber=N'3313A036',Price=0.135857730462134,ManufacturerGuid=N'a23819e1-392d-4781-997d-deff6609b51d',IsMasterData='1' WHERE Guid = '4793c0cb-17fd-42c5-a8ad-3049d0891d14'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8a01eb6f-d348-470d-92f2-cd1d108fa2be')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8a01eb6f-d348-470d-92f2-cd1d108fa2be')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd1bc3638-5f27-4c40-bbfd-3a8715993363')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'ELBOW', N'0201047', 0.604887490926688, N'8a01eb6f-d348-470d-92f2-cd1d108fa2be', '1', 'd1bc3638-5f27-4c40-bbfd-3a8715993363')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'ELBOW',PartNumber=N'0201047',Price=0.604887490926688,ManufacturerGuid=N'8a01eb6f-d348-470d-92f2-cd1d108fa2be',IsMasterData='1' WHERE Guid = 'd1bc3638-5f27-4c40-bbfd-3a8715993363'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'20c4c72b-76b6-493e-9848-2d4014e12bca')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'20c4c72b-76b6-493e-9848-2d4014e12bca')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '99161d1f-2825-49cc-adbd-9fd342ab514c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'ELBOW', N'2447375', 1.20977498185338, N'20c4c72b-76b6-493e-9848-2d4014e12bca', '1', '99161d1f-2825-49cc-adbd-9fd342ab514c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'ELBOW',PartNumber=N'2447375',Price=1.20977498185338,ManufacturerGuid=N'20c4c72b-76b6-493e-9848-2d4014e12bca',IsMasterData='1' WHERE Guid = '99161d1f-2825-49cc-adbd-9fd342ab514c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'434939cf-3c4a-435a-8476-53441b7ed01c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'434939cf-3c4a-435a-8476-53441b7ed01c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'da670fa6-8f74-497f-b15c-2d666d24a436')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'ELBOW', N'2447377', 1.51221872731672, N'434939cf-3c4a-435a-8476-53441b7ed01c', '1', 'da670fa6-8f74-497f-b15c-2d666d24a436')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'ELBOW',PartNumber=N'2447377',Price=1.51221872731672,ManufacturerGuid=N'434939cf-3c4a-435a-8476-53441b7ed01c',IsMasterData='1' WHERE Guid = 'da670fa6-8f74-497f-b15c-2d666d24a436'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2e56d969-7e8b-4f4e-9c3d-b7a16f70437d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2e56d969-7e8b-4f4e-9c3d-b7a16f70437d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c3da252-83da-4aeb-bdec-98437b1fc2a7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'ELBOW', N'4138A062', 2.63730946044036, N'2e56d969-7e8b-4f4e-9c3d-b7a16f70437d', '1', '8c3da252-83da-4aeb-bdec-98437b1fc2a7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'ELBOW',PartNumber=N'4138A062',Price=2.63730946044036,ManufacturerGuid=N'2e56d969-7e8b-4f4e-9c3d-b7a16f70437d',IsMasterData='1' WHERE Guid = '8c3da252-83da-4aeb-bdec-98437b1fc2a7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7229a6b8-0686-45ce-8dff-21e1ad4b084f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7229a6b8-0686-45ce-8dff-21e1ad4b084f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '19a8aef9-b75b-485e-b5a3-4f3b2c031009')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'KEY', N'0500005', 0.058069199128962, N'7229a6b8-0686-45ce-8dff-21e1ad4b084f', '1', '19a8aef9-b75b-485e-b5a3-4f3b2c031009')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'KEY',PartNumber=N'0500005',Price=0.058069199128962,ManufacturerGuid=N'7229a6b8-0686-45ce-8dff-21e1ad4b084f',IsMasterData='1' WHERE Guid = '19a8aef9-b75b-485e-b5a3-4f3b2c031009'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'673f8432-2c78-400b-889d-9a1ebc74e74a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'673f8432-2c78-400b-889d-9a1ebc74e74a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd0850305-8e01-460d-a0c9-2cb11516455a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'KEY', N'0500012', 0.157028792644568, N'673f8432-2c78-400b-889d-9a1ebc74e74a', '1', 'd0850305-8e01-460d-a0c9-2cb11516455a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'KEY',PartNumber=N'0500012',Price=0.157028792644568,ManufacturerGuid=N'673f8432-2c78-400b-889d-9a1ebc74e74a',IsMasterData='1' WHERE Guid = 'd0850305-8e01-460d-a0c9-2cb11516455a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f3803dda-4cf0-4e1d-8e63-48fa11e55145')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f3803dda-4cf0-4e1d-8e63-48fa11e55145')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2765d246-9145-4211-9a78-fc10293234fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'KEY', N'36271704', 0.268570045971449, N'f3803dda-4cf0-4e1d-8e63-48fa11e55145', '1', '2765d246-9145-4211-9a78-fc10293234fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'KEY',PartNumber=N'36271704',Price=0.268570045971449,ManufacturerGuid=N'f3803dda-4cf0-4e1d-8e63-48fa11e55145',IsMasterData='1' WHERE Guid = '2765d246-9145-4211-9a78-fc10293234fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f5a6389d-8791-4a1b-bb7c-570b4ea974ca')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f5a6389d-8791-4a1b-bb7c-570b4ea974ca')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '91401bb2-56d4-4824-8c81-169ded1be777')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'LOCKWIRE', N'31718105', 0.0202032421969514, N'f5a6389d-8791-4a1b-bb7c-570b4ea974ca', '1', '91401bb2-56d4-4824-8c81-169ded1be777')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'LOCKWIRE',PartNumber=N'31718105',Price=0.0202032421969514,ManufacturerGuid=N'f5a6389d-8791-4a1b-bb7c-570b4ea974ca',IsMasterData='1' WHERE Guid = '91401bb2-56d4-4824-8c81-169ded1be777'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5132c3aa-5354-490a-9bde-ea634600e510')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5132c3aa-5354-490a-9bde-ea634600e510')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '51667264-2a2c-41ae-8f7f-f981e0904375')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'LOCKWIRE', N'31718143', 0.048390999274135, N'5132c3aa-5354-490a-9bde-ea634600e510', '1', '51667264-2a2c-41ae-8f7f-f981e0904375')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'LOCKWIRE',PartNumber=N'31718143',Price=0.048390999274135,ManufacturerGuid=N'5132c3aa-5354-490a-9bde-ea634600e510',IsMasterData='1' WHERE Guid = '51667264-2a2c-41ae-8f7f-f981e0904375'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'942b08cf-7c95-4fa7-8765-c883e62a33a6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'942b08cf-7c95-4fa7-8765-c883e62a33a6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '07172b7e-16ba-43cd-b7a4-d705d4730e27')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NIPPLE', N'32726522', 0.332688120009678, N'942b08cf-7c95-4fa7-8765-c883e62a33a6', '1', '07172b7e-16ba-43cd-b7a4-d705d4730e27')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NIPPLE',PartNumber=N'32726522',Price=0.332688120009678,ManufacturerGuid=N'942b08cf-7c95-4fa7-8765-c883e62a33a6',IsMasterData='1' WHERE Guid = '07172b7e-16ba-43cd-b7a4-d705d4730e27'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9b9fcb54-83e1-41de-af96-27830adcee42')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9b9fcb54-83e1-41de-af96-27830adcee42')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4a5cdfd0-69dd-475c-9faf-95ba59ef22b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NIPPLE', N'32726528', 0.225381079119284, N'9b9fcb54-83e1-41de-af96-27830adcee42', '1', '4a5cdfd0-69dd-475c-9faf-95ba59ef22b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NIPPLE',PartNumber=N'32726528',Price=0.225381079119284,ManufacturerGuid=N'9b9fcb54-83e1-41de-af96-27830adcee42',IsMasterData='1' WHERE Guid = '4a5cdfd0-69dd-475c-9faf-95ba59ef22b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'40df9e46-459c-4a10-8718-22196d85a6a4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'40df9e46-459c-4a10-8718-22196d85a6a4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7c141d35-495f-41f3-b0ba-388a42fd11ce')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0571346', 0.0653278490200823, N'40df9e46-459c-4a10-8718-22196d85a6a4', '1', '7c141d35-495f-41f3-b0ba-388a42fd11ce')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0571346',Price=0.0653278490200823,ManufacturerGuid=N'40df9e46-459c-4a10-8718-22196d85a6a4',IsMasterData='1' WHERE Guid = '7c141d35-495f-41f3-b0ba-388a42fd11ce'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7ab916c6-29fd-409f-bf91-2249cc1696c5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7ab916c6-29fd-409f-bf91-2249cc1696c5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '89b84067-ad12-4b80-8a03-5d24d4535486')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576051', 0.0696830389547544, N'7ab916c6-29fd-409f-bf91-2249cc1696c5', '1', '89b84067-ad12-4b80-8a03-5d24d4535486')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576051',Price=0.0696830389547544,ManufacturerGuid=N'7ab916c6-29fd-409f-bf91-2249cc1696c5',IsMasterData='1' WHERE Guid = '89b84067-ad12-4b80-8a03-5d24d4535486'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8503dd4b-4863-4467-8601-8fe2f87083b6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8503dd4b-4863-4467-8601-8fe2f87083b6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '57817baf-2c2d-41c2-a783-2e9b89fc0102')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576054', 0.0303653520445197, N'8503dd4b-4863-4467-8601-8fe2f87083b6', '1', '57817baf-2c2d-41c2-a783-2e9b89fc0102')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576054',Price=0.0303653520445197,ManufacturerGuid=N'8503dd4b-4863-4467-8601-8fe2f87083b6',IsMasterData='1' WHERE Guid = '57817baf-2c2d-41c2-a783-2e9b89fc0102'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7c71b54e-5a35-4c07-a5e8-dad2a2a0edf1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7c71b54e-5a35-4c07-a5e8-dad2a2a0edf1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e636eeca-8658-4d49-a07a-06194203751b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576111', 0.0586740866198887, N'7c71b54e-5a35-4c07-a5e8-dad2a2a0edf1', '1', 'e636eeca-8658-4d49-a07a-06194203751b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576111',Price=0.0586740866198887,ManufacturerGuid=N'7c71b54e-5a35-4c07-a5e8-dad2a2a0edf1',IsMasterData='1' WHERE Guid = 'e636eeca-8658-4d49-a07a-06194203751b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5475f28f-a6ef-4d0c-b91a-73b5146ad299')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5475f28f-a6ef-4d0c-b91a-73b5146ad299')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6388cac9-34c2-4694-a797-e4049342618d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576112', 0.058795064118074, N'5475f28f-a6ef-4d0c-b91a-73b5146ad299', '1', '6388cac9-34c2-4694-a797-e4049342618d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576112',Price=0.058795064118074,ManufacturerGuid=N'5475f28f-a6ef-4d0c-b91a-73b5146ad299',IsMasterData='1' WHERE Guid = '6388cac9-34c2-4694-a797-e4049342618d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bc2121fb-28a5-4dbf-8b3b-8e20e6d402f0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bc2121fb-28a5-4dbf-8b3b-8e20e6d402f0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b20716ed-a2b0-4a88-beb7-d004aa09fdd7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576113', 0.0616985240745221, N'bc2121fb-28a5-4dbf-8b3b-8e20e6d402f0', '1', 'b20716ed-a2b0-4a88-beb7-d004aa09fdd7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576113',Price=0.0616985240745221,ManufacturerGuid=N'bc2121fb-28a5-4dbf-8b3b-8e20e6d402f0',IsMasterData='1' WHERE Guid = 'b20716ed-a2b0-4a88-beb7-d004aa09fdd7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ccc969a9-d27f-45d7-9e48-458ed2f2157f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ccc969a9-d27f-45d7-9e48-458ed2f2157f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '30e58be0-bd7d-43cf-9ae3-144aec5d2bd0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'0576116', 0.151221872731672, N'ccc969a9-d27f-45d7-9e48-458ed2f2157f', '1', '30e58be0-bd7d-43cf-9ae3-144aec5d2bd0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'0576116',Price=0.151221872731672,ManufacturerGuid=N'ccc969a9-d27f-45d7-9e48-458ed2f2157f',IsMasterData='1' WHERE Guid = '30e58be0-bd7d-43cf-9ae3-144aec5d2bd0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'15a8018a-8700-4e58-9440-3efba5b0bc9d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'15a8018a-8700-4e58-9440-3efba5b0bc9d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '883b3ea1-1849-4ab4-bbf2-c6d4fc965d12')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2188134', 0.0210500846842487, N'15a8018a-8700-4e58-9440-3efba5b0bc9d', '1', '883b3ea1-1849-4ab4-bbf2-c6d4fc965d12')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2188134',Price=0.0210500846842487,ManufacturerGuid=N'15a8018a-8700-4e58-9440-3efba5b0bc9d',IsMasterData='1' WHERE Guid = '883b3ea1-1849-4ab4-bbf2-c6d4fc965d12'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'abfcd978-316e-43f9-be92-f15a60ce95c3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'abfcd978-316e-43f9-be92-f15a60ce95c3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4bb8af36-c12f-47c2-b6a7-e8652fde5f2e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2211224', 0.0731913864021292, N'abfcd978-316e-43f9-be92-f15a60ce95c3', '1', '4bb8af36-c12f-47c2-b6a7-e8652fde5f2e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2211224',Price=0.0731913864021292,ManufacturerGuid=N'abfcd978-316e-43f9-be92-f15a60ce95c3',IsMasterData='1' WHERE Guid = '4bb8af36-c12f-47c2-b6a7-e8652fde5f2e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3f287c75-7420-4232-af7d-779f984c3ff9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3f287c75-7420-4232-af7d-779f984c3ff9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a789de4a-f385-4525-b186-4257fd923f49')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2211283', 0.0669005564964917, N'3f287c75-7420-4232-af7d-779f984c3ff9', '1', 'a789de4a-f385-4525-b186-4257fd923f49')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2211283',Price=0.0669005564964917,ManufacturerGuid=N'3f287c75-7420-4232-af7d-779f984c3ff9',IsMasterData='1' WHERE Guid = 'a789de4a-f385-4525-b186-4257fd923f49'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fad730b0-68cc-4c3c-931a-415ab60cfb5a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fad730b0-68cc-4c3c-931a-415ab60cfb5a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f9161269-6055-4a62-b039-ddd3b5149cc8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2211291', 0.0624243890636342, N'fad730b0-68cc-4c3c-931a-415ab60cfb5a', '1', 'f9161269-6055-4a62-b039-ddd3b5149cc8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2211291',Price=0.0624243890636342,ManufacturerGuid=N'fad730b0-68cc-4c3c-931a-415ab60cfb5a',IsMasterData='1' WHERE Guid = 'f9161269-6055-4a62-b039-ddd3b5149cc8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8dca7d56-fafe-45c2-91ba-c79ea30dcbcd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8dca7d56-fafe-45c2-91ba-c79ea30dcbcd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f6a8460c-dc12-48cc-b903-0fbad045def6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2238153', 0.0214130171788047, N'8dca7d56-fafe-45c2-91ba-c79ea30dcbcd', '1', 'f6a8460c-dc12-48cc-b903-0fbad045def6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2238153',Price=0.0214130171788047,ManufacturerGuid=N'8dca7d56-fafe-45c2-91ba-c79ea30dcbcd',IsMasterData='1' WHERE Guid = 'f6a8460c-dc12-48cc-b903-0fbad045def6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'031b25b0-0fde-4d5c-97e6-0a0c5b36339c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'031b25b0-0fde-4d5c-97e6-0a0c5b36339c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e45c37ef-fb9e-4085-8c71-b476377128ba')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A108', 0.0458504718122429, N'031b25b0-0fde-4d5c-97e6-0a0c5b36339c', '1', 'e45c37ef-fb9e-4085-8c71-b476377128ba')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A108',Price=0.0458504718122429,ManufacturerGuid=N'031b25b0-0fde-4d5c-97e6-0a0c5b36339c',IsMasterData='1' WHERE Guid = 'e45c37ef-fb9e-4085-8c71-b476377128ba'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e0b6f529-98cf-4899-bf89-1ad824fd1509')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e0b6f529-98cf-4899-bf89-1ad824fd1509')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '43dfde6e-6221-479d-a2d0-8034931ebaf9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A207', 0.00556496491652553, N'e0b6f529-98cf-4899-bf89-1ad824fd1509', '1', '43dfde6e-6221-479d-a2d0-8034931ebaf9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A207',Price=0.00556496491652553,ManufacturerGuid=N'e0b6f529-98cf-4899-bf89-1ad824fd1509',IsMasterData='1' WHERE Guid = '43dfde6e-6221-479d-a2d0-8034931ebaf9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6a59a7e5-05a6-4186-8d52-a2f4b07b1f5c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6a59a7e5-05a6-4186-8d52-a2f4b07b1f5c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4558a32e-25bc-44ea-ac13-f4b60459ea5d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A208', 0.009678199854827, N'6a59a7e5-05a6-4186-8d52-a2f4b07b1f5c', '1', '4558a32e-25bc-44ea-ac13-f4b60459ea5d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A208',Price=0.009678199854827,ManufacturerGuid=N'6a59a7e5-05a6-4186-8d52-a2f4b07b1f5c',IsMasterData='1' WHERE Guid = '4558a32e-25bc-44ea-ac13-f4b60459ea5d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ea2550ec-0b3d-4052-b4e2-724a13bc3107')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ea2550ec-0b3d-4052-b4e2-724a13bc3107')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '89612fa8-d4c8-4eac-92ef-19ff7d6ca57b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A209', 0.0165739172513912, N'ea2550ec-0b3d-4052-b4e2-724a13bc3107', '1', '89612fa8-d4c8-4eac-92ef-19ff7d6ca57b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A209',Price=0.0165739172513912,ManufacturerGuid=N'ea2550ec-0b3d-4052-b4e2-724a13bc3107',IsMasterData='1' WHERE Guid = '89612fa8-d4c8-4eac-92ef-19ff7d6ca57b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'22ef7b6a-fd9c-4fc7-96dc-0b5e09ac100f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'22ef7b6a-fd9c-4fc7-96dc-0b5e09ac100f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '176c4bf0-7b35-4597-b325-2f81d9e173fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A222', 0.067384466489233, N'22ef7b6a-fd9c-4fc7-96dc-0b5e09ac100f', '1', '176c4bf0-7b35-4597-b325-2f81d9e173fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A222',Price=0.067384466489233,ManufacturerGuid=N'22ef7b6a-fd9c-4fc7-96dc-0b5e09ac100f',IsMasterData='1' WHERE Guid = '176c4bf0-7b35-4597-b325-2f81d9e173fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'639ade4e-10fb-40a2-86f9-0eadaa6c569d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'639ade4e-10fb-40a2-86f9-0eadaa6c569d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ede18c94-540b-4219-9d23-ff7800835af5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A250', 0.931526736027099, N'639ade4e-10fb-40a2-86f9-0eadaa6c569d', '1', 'ede18c94-540b-4219-9d23-ff7800835af5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A250',Price=0.931526736027099,ManufacturerGuid=N'639ade4e-10fb-40a2-86f9-0eadaa6c569d',IsMasterData='1' WHERE Guid = 'ede18c94-540b-4219-9d23-ff7800835af5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'069cd4b8-e21c-4408-89e2-d65c5180e7af')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'069cd4b8-e21c-4408-89e2-d65c5180e7af')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '957d77a6-b3aa-47bc-9fb1-1c23828f27d6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A401', 0.0441567868376482, N'069cd4b8-e21c-4408-89e2-d65c5180e7af', '1', '957d77a6-b3aa-47bc-9fb1-1c23828f27d6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A401',Price=0.0441567868376482,ManufacturerGuid=N'069cd4b8-e21c-4408-89e2-d65c5180e7af',IsMasterData='1' WHERE Guid = '957d77a6-b3aa-47bc-9fb1-1c23828f27d6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2c11c847-b7fb-4f42-9188-084c640fd587')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2c11c847-b7fb-4f42-9188-084c640fd587')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '039e7ac7-40cb-43fe-91ec-e36cd805177b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A601', 0.0269779820953303, N'2c11c847-b7fb-4f42-9188-084c640fd587', '1', '039e7ac7-40cb-43fe-91ec-e36cd805177b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A601',Price=0.0269779820953303,ManufacturerGuid=N'2c11c847-b7fb-4f42-9188-084c640fd587',IsMasterData='1' WHERE Guid = '039e7ac7-40cb-43fe-91ec-e36cd805177b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'177cedae-6135-4137-bfda-e172886c7cc8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'177cedae-6135-4137-bfda-e172886c7cc8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3bea1e7a-6f77-4af8-98a6-48b7af53a6da')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A602', 0.0120009678199855, N'177cedae-6135-4137-bfda-e172886c7cc8', '1', '3bea1e7a-6f77-4af8-98a6-48b7af53a6da')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A602',Price=0.0120009678199855,ManufacturerGuid=N'177cedae-6135-4137-bfda-e172886c7cc8',IsMasterData='1' WHERE Guid = '3bea1e7a-6f77-4af8-98a6-48b7af53a6da'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9d727a82-ca5e-48f4-9897-6b9325ef3abc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9d727a82-ca5e-48f4-9897-6b9325ef3abc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7cd6e749-7242-43d4-b838-419328062ea1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A603', 0.0174207597386886, N'9d727a82-ca5e-48f4-9897-6b9325ef3abc', '1', '7cd6e749-7242-43d4-b838-419328062ea1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A603',Price=0.0174207597386886,ManufacturerGuid=N'9d727a82-ca5e-48f4-9897-6b9325ef3abc',IsMasterData='1' WHERE Guid = '7cd6e749-7242-43d4-b838-419328062ea1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c66b442d-921e-4a7c-99ab-e2f45b6b1193')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c66b442d-921e-4a7c-99ab-e2f45b6b1193')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ff83744b-c56f-4e2b-8143-cff2061a2280')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A604', 0.0280667795789983, N'c66b442d-921e-4a7c-99ab-e2f45b6b1193', '1', 'ff83744b-c56f-4e2b-8143-cff2061a2280')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A604',Price=0.0280667795789983,ManufacturerGuid=N'c66b442d-921e-4a7c-99ab-e2f45b6b1193',IsMasterData='1' WHERE Guid = 'ff83744b-c56f-4e2b-8143-cff2061a2280'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c1802b0d-4079-4ebc-80fb-e9e16019207b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c1802b0d-4079-4ebc-80fb-e9e16019207b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c5e2e223-e09a-461b-9bb5-f93cf75bd072')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A605', 0.0454875393176869, N'c1802b0d-4079-4ebc-80fb-e9e16019207b', '1', 'c5e2e223-e09a-461b-9bb5-f93cf75bd072')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A605',Price=0.0454875393176869,ManufacturerGuid=N'c1802b0d-4079-4ebc-80fb-e9e16019207b',IsMasterData='1' WHERE Guid = 'c5e2e223-e09a-461b-9bb5-f93cf75bd072'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4feeeadd-ea25-4d7f-8a1f-d91ed42d255f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4feeeadd-ea25-4d7f-8a1f-d91ed42d255f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '50b538e1-031d-463a-aabb-3457cd2dafc5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A607', 0.0954512460682313, N'4feeeadd-ea25-4d7f-8a1f-d91ed42d255f', '1', '50b538e1-031d-463a-aabb-3457cd2dafc5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A607',Price=0.0954512460682313,ManufacturerGuid=N'4feeeadd-ea25-4d7f-8a1f-d91ed42d255f',IsMasterData='1' WHERE Guid = '50b538e1-031d-463a-aabb-3457cd2dafc5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b318501a-dfa3-46e3-ba12-b2cda1cb5bbc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b318501a-dfa3-46e3-ba12-b2cda1cb5bbc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ed974f07-33eb-4eb4-81d6-ecb83193bba0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A633', 0.020082264698766, N'b318501a-dfa3-46e3-ba12-b2cda1cb5bbc', '1', 'ed974f07-33eb-4eb4-81d6-ecb83193bba0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A633',Price=0.020082264698766,ManufacturerGuid=N'b318501a-dfa3-46e3-ba12-b2cda1cb5bbc',IsMasterData='1' WHERE Guid = 'ed974f07-33eb-4eb4-81d6-ecb83193bba0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ae4dc7c9-72b8-44a6-8818-7a1834955079')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ae4dc7c9-72b8-44a6-8818-7a1834955079')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '155943da-75c7-4ee0-9ee4-81fd4b979dd2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A634', 0.0316961045245584, N'ae4dc7c9-72b8-44a6-8818-7a1834955079', '1', '155943da-75c7-4ee0-9ee4-81fd4b979dd2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A634',Price=0.0316961045245584,ManufacturerGuid=N'ae4dc7c9-72b8-44a6-8818-7a1834955079',IsMasterData='1' WHERE Guid = '155943da-75c7-4ee0-9ee4-81fd4b979dd2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'139f5711-ed09-4caa-8c37-a3ecf2cbe4a6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'139f5711-ed09-4caa-8c37-a3ecf2cbe4a6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9f9a9077-6403-4ae2-b9d9-2031bfcc5f95')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318A703', 0.0241954996370675, N'139f5711-ed09-4caa-8c37-a3ecf2cbe4a6', '1', '9f9a9077-6403-4ae2-b9d9-2031bfcc5f95')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318A703',Price=0.0241954996370675,ManufacturerGuid=N'139f5711-ed09-4caa-8c37-a3ecf2cbe4a6',IsMasterData='1' WHERE Guid = '9f9a9077-6403-4ae2-b9d9-2031bfcc5f95'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2110a173-d808-4c2b-a7d1-616f8d290aa8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2110a173-d808-4c2b-a7d1-616f8d290aa8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ae8f5f52-40f1-4170-a4fe-b012fefb04a0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318D205', 0.0145172997822405, N'2110a173-d808-4c2b-a7d1-616f8d290aa8', '1', 'ae8f5f52-40f1-4170-a4fe-b012fefb04a0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318D205',Price=0.0145172997822405,ManufacturerGuid=N'2110a173-d808-4c2b-a7d1-616f8d290aa8',IsMasterData='1' WHERE Guid = 'ae8f5f52-40f1-4170-a4fe-b012fefb04a0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0a00d757-301d-4386-9a19-de4aad760eb6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0a00d757-301d-4386-9a19-de4aad760eb6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd804609f-7171-4ea0-8b7a-e7aa3c7ce692')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318D208', 0.0192354222114687, N'0a00d757-301d-4386-9a19-de4aad760eb6', '1', 'd804609f-7171-4ea0-8b7a-e7aa3c7ce692')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318D208',Price=0.0192354222114687,ManufacturerGuid=N'0a00d757-301d-4386-9a19-de4aad760eb6',IsMasterData='1' WHERE Guid = 'd804609f-7171-4ea0-8b7a-e7aa3c7ce692'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a0e2ba41-8bea-4bc6-941a-f42a8d711bf4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a0e2ba41-8bea-4bc6-941a-f42a8d711bf4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4bb68e58-f15b-4f12-9a94-43a3a5331624')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318D212', 0.0450036293249456, N'a0e2ba41-8bea-4bc6-941a-f42a8d711bf4', '1', '4bb68e58-f15b-4f12-9a94-43a3a5331624')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318D212',Price=0.0450036293249456,ManufacturerGuid=N'a0e2ba41-8bea-4bc6-941a-f42a8d711bf4',IsMasterData='1' WHERE Guid = '4bb68e58-f15b-4f12-9a94-43a3a5331624'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b6b0b344-3226-4372-a0a5-30c5365714a9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b6b0b344-3226-4372-a0a5-30c5365714a9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '73229177-bf6f-4c61-a058-feea31637e9e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318D240', 0.0349624969755625, N'b6b0b344-3226-4372-a0a5-30c5365714a9', '1', '73229177-bf6f-4c61-a058-feea31637e9e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318D240',Price=0.0349624969755625,ManufacturerGuid=N'b6b0b344-3226-4372-a0a5-30c5365714a9',IsMasterData='1' WHERE Guid = '73229177-bf6f-4c61-a058-feea31637e9e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'11ba4104-5d3a-4e18-a737-f2ce40c10420')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'11ba4104-5d3a-4e18-a737-f2ce40c10420')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '918c6a94-e250-4c31-bb0b-3ec57be6ed5a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'2318F605', 0.058069199128962, N'11ba4104-5d3a-4e18-a737-f2ce40c10420', '1', '918c6a94-e250-4c31-bb0b-3ec57be6ed5a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'2318F605',Price=0.058069199128962,ManufacturerGuid=N'11ba4104-5d3a-4e18-a737-f2ce40c10420',IsMasterData='1' WHERE Guid = '918c6a94-e250-4c31-bb0b-3ec57be6ed5a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a5dd8a10-24a2-4335-ba23-0a7ac3daa651')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a5dd8a10-24a2-4335-ba23-0a7ac3daa651')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dae5a2b6-e7b2-44dd-b60b-993243b8374f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'3274A003', 0.160537140091943, N'a5dd8a10-24a2-4335-ba23-0a7ac3daa651', '1', 'dae5a2b6-e7b2-44dd-b60b-993243b8374f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'3274A003',Price=0.160537140091943,ManufacturerGuid=N'a5dd8a10-24a2-4335-ba23-0a7ac3daa651',IsMasterData='1' WHERE Guid = 'dae5a2b6-e7b2-44dd-b60b-993243b8374f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dd1e77b2-9105-402b-a278-92f0a8e9a9bf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dd1e77b2-9105-402b-a278-92f0a8e9a9bf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c26b97ec-0548-49d0-a725-cae639b91e55')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33221327', 0.0382288894265667, N'dd1e77b2-9105-402b-a278-92f0a8e9a9bf', '1', 'c26b97ec-0548-49d0-a725-cae639b91e55')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33221327',Price=0.0382288894265667,ManufacturerGuid=N'dd1e77b2-9105-402b-a278-92f0a8e9a9bf',IsMasterData='1' WHERE Guid = 'c26b97ec-0548-49d0-a725-cae639b91e55'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b4d54a4e-b747-43eb-bcfb-c9b402fa41f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b4d54a4e-b747-43eb-bcfb-c9b402fa41f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c153073c-d1bc-419b-80e2-5a75c8bcf14e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33221328', 0.03943866440842, N'b4d54a4e-b747-43eb-bcfb-c9b402fa41f5', '1', 'c153073c-d1bc-419b-80e2-5a75c8bcf14e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33221328',Price=0.03943866440842,ManufacturerGuid=N'b4d54a4e-b747-43eb-bcfb-c9b402fa41f5',IsMasterData='1' WHERE Guid = 'c153073c-d1bc-419b-80e2-5a75c8bcf14e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8be99567-d2ab-4c16-9009-741870b09642')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8be99567-d2ab-4c16-9009-741870b09642')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1fd48f30-3dab-4f8a-88f6-15aedf764dab')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33225412', 0.0500846842487297, N'8be99567-d2ab-4c16-9009-741870b09642', '1', '1fd48f30-3dab-4f8a-88f6-15aedf764dab')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33225412',Price=0.0500846842487297,ManufacturerGuid=N'8be99567-d2ab-4c16-9009-741870b09642',IsMasterData='1' WHERE Guid = '1fd48f30-3dab-4f8a-88f6-15aedf764dab'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f59d607c-8ea7-4c47-8250-669faf6ff18f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f59d607c-8ea7-4c47-8250-669faf6ff18f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '31ad65a5-a150-4786-9a73-997cbdaa0060')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33225413', 0.086015001209775, N'f59d607c-8ea7-4c47-8250-669faf6ff18f', '1', '31ad65a5-a150-4786-9a73-997cbdaa0060')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33225413',Price=0.086015001209775,ManufacturerGuid=N'f59d607c-8ea7-4c47-8250-669faf6ff18f',IsMasterData='1' WHERE Guid = '31ad65a5-a150-4786-9a73-997cbdaa0060'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ad0075a5-29c9-4725-a42c-eacf7494d251')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ad0075a5-29c9-4725-a42c-eacf7494d251')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b3317edb-552b-430f-a19f-d18cc9836140')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33225414', 0.0901282361480765, N'ad0075a5-29c9-4725-a42c-eacf7494d251', '1', 'b3317edb-552b-430f-a19f-d18cc9836140')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33225414',Price=0.0901282361480765,ManufacturerGuid=N'ad0075a5-29c9-4725-a42c-eacf7494d251',IsMasterData='1' WHERE Guid = 'b3317edb-552b-430f-a19f-d18cc9836140'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f4901714-9291-4e73-844d-8be55dd62121')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f4901714-9291-4e73-844d-8be55dd62121')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '26f636bf-115c-406e-8800-c01c5d2847fe')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'3334P004', 6.26663440600048, N'f4901714-9291-4e73-844d-8be55dd62121', '1', '26f636bf-115c-406e-8800-c01c5d2847fe')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'3334P004',Price=6.26663440600048,ManufacturerGuid=N'f4901714-9291-4e73-844d-8be55dd62121',IsMasterData='1' WHERE Guid = '26f636bf-115c-406e-8800-c01c5d2847fe'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'60d24d2b-8156-47ef-abe4-c9f54a5cd724')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'60d24d2b-8156-47ef-abe4-c9f54a5cd724')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a0a8f719-02f7-4d2a-85d9-65d9eb0903c3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33531115', 0.539559641906605, N'60d24d2b-8156-47ef-abe4-c9f54a5cd724', '1', 'a0a8f719-02f7-4d2a-85d9-65d9eb0903c3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33531115',Price=0.539559641906605,ManufacturerGuid=N'60d24d2b-8156-47ef-abe4-c9f54a5cd724',IsMasterData='1' WHERE Guid = 'a0a8f719-02f7-4d2a-85d9-65d9eb0903c3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'901d2e65-6c3e-4091-a3ce-bd4858b1f16d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'901d2e65-6c3e-4091-a3ce-bd4858b1f16d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '04000a86-dbdf-4948-b051-42ed367dd4b4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33531119', 0.520203242196951, N'901d2e65-6c3e-4091-a3ce-bd4858b1f16d', '1', '04000a86-dbdf-4948-b051-42ed367dd4b4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33531119',Price=0.520203242196951,ManufacturerGuid=N'901d2e65-6c3e-4091-a3ce-bd4858b1f16d',IsMasterData='1' WHERE Guid = '04000a86-dbdf-4948-b051-42ed367dd4b4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'79b1025a-d8ad-4879-8aec-81bff52131fc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'79b1025a-d8ad-4879-8aec-81bff52131fc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '32ac133a-f3b1-47b8-9639-c6a5dcc2eea1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33531137', 0.539559641906605, N'79b1025a-d8ad-4879-8aec-81bff52131fc', '1', '32ac133a-f3b1-47b8-9639-c6a5dcc2eea1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33531137',Price=0.539559641906605,ManufacturerGuid=N'79b1025a-d8ad-4879-8aec-81bff52131fc',IsMasterData='1' WHERE Guid = '32ac133a-f3b1-47b8-9639-c6a5dcc2eea1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0d1ec85e-efb6-460a-8cbf-e87d2c6439cd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0d1ec85e-efb6-460a-8cbf-e87d2c6439cd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f8da2ed9-eb7b-4798-91b8-1356686fabd8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33532128', 0.0552867166706992, N'0d1ec85e-efb6-460a-8cbf-e87d2c6439cd', '1', 'f8da2ed9-eb7b-4798-91b8-1356686fabd8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33532128',Price=0.0552867166706992,ManufacturerGuid=N'0d1ec85e-efb6-460a-8cbf-e87d2c6439cd',IsMasterData='1' WHERE Guid = 'f8da2ed9-eb7b-4798-91b8-1356686fabd8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6704ec41-9891-4290-9944-bd7c68514a23')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6704ec41-9891-4290-9944-bd7c68514a23')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '76679613-81c3-49c1-a6bd-91f34a657d5b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'NUT', N'33532129', 0.0654488265182676, N'6704ec41-9891-4290-9944-bd7c68514a23', '1', '76679613-81c3-49c1-a6bd-91f34a657d5b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'NUT',PartNumber=N'33532129',Price=0.0654488265182676,ManufacturerGuid=N'6704ec41-9891-4290-9944-bd7c68514a23',IsMasterData='1' WHERE Guid = '76679613-81c3-49c1-a6bd-91f34a657d5b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6b6dcefb-283a-4037-9275-fa173ed3da5a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6b6dcefb-283a-4037-9275-fa173ed3da5a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2c57cbe1-a82d-4d32-b11e-c0d8ab56978f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2413689', 0.405274618920881, N'6b6dcefb-283a-4037-9275-fa173ed3da5a', '1', '2c57cbe1-a82d-4d32-b11e-c0d8ab56978f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2413689',Price=0.405274618920881,ManufacturerGuid=N'6b6dcefb-283a-4037-9275-fa173ed3da5a',IsMasterData='1' WHERE Guid = '2c57cbe1-a82d-4d32-b11e-c0d8ab56978f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'129640e1-ade7-420b-b79a-bd331d47488f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'129640e1-ade7-420b-b79a-bd331d47488f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '22fab690-b711-47cc-b7fb-8cc1cbee8ec8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415638', 0.0614565690781515, N'129640e1-ade7-420b-b79a-bd331d47488f', '1', '22fab690-b711-47cc-b7fb-8cc1cbee8ec8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415638',Price=0.0614565690781515,ManufacturerGuid=N'129640e1-ade7-420b-b79a-bd331d47488f',IsMasterData='1' WHERE Guid = '22fab690-b711-47cc-b7fb-8cc1cbee8ec8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2a9f4468-e510-4182-86b6-83b002f7f2c4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2a9f4468-e510-4182-86b6-83b002f7f2c4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f0a91824-b6e2-4c92-9770-c4824491e873')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415714', 0.0302443745463344, N'2a9f4468-e510-4182-86b6-83b002f7f2c4', '1', 'f0a91824-b6e2-4c92-9770-c4824491e873')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415714',Price=0.0302443745463344,ManufacturerGuid=N'2a9f4468-e510-4182-86b6-83b002f7f2c4',IsMasterData='1' WHERE Guid = 'f0a91824-b6e2-4c92-9770-c4824491e873'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8eda7350-a508-44c8-b5e3-1e7d284c5bc7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8eda7350-a508-44c8-b5e3-1e7d284c5bc7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2fafed6d-e5f0-446a-a724-f00219a925b1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415715', 0.0214130171788047, N'8eda7350-a508-44c8-b5e3-1e7d284c5bc7', '1', '2fafed6d-e5f0-446a-a724-f00219a925b1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415715',Price=0.0214130171788047,ManufacturerGuid=N'8eda7350-a508-44c8-b5e3-1e7d284c5bc7',IsMasterData='1' WHERE Guid = '2fafed6d-e5f0-446a-a724-f00219a925b1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'45d04464-fb59-461b-a073-1c20b33b138a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'45d04464-fb59-461b-a073-1c20b33b138a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3c318475-dddd-482b-b568-072ec117d6cb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415786', 0.0846842487297363, N'45d04464-fb59-461b-a073-1c20b33b138a', '1', '3c318475-dddd-482b-b568-072ec117d6cb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415786',Price=0.0846842487297363,ManufacturerGuid=N'45d04464-fb59-461b-a073-1c20b33b138a',IsMasterData='1' WHERE Guid = '3c318475-dddd-482b-b568-072ec117d6cb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fedd9978-2335-4153-b53e-1507f83d2f7c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fedd9978-2335-4153-b53e-1507f83d2f7c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4228f4c1-9531-4a72-9398-d8a643f50978')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415B148', 0.027945802080813, N'fedd9978-2335-4153-b53e-1507f83d2f7c', '1', '4228f4c1-9531-4a72-9398-d8a643f50978')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415B148',Price=0.027945802080813,ManufacturerGuid=N'fedd9978-2335-4153-b53e-1507f83d2f7c',IsMasterData='1' WHERE Guid = '4228f4c1-9531-4a72-9398-d8a643f50978'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'28fbffe3-ca0b-4ada-963e-2882398a75ac')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'28fbffe3-ca0b-4ada-963e-2882398a75ac')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1cc81769-cdf3-46ad-b8db-65133a3c21e8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415B167', 0.0846842487297363, N'28fbffe3-ca0b-4ada-963e-2882398a75ac', '1', '1cc81769-cdf3-46ad-b8db-65133a3c21e8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415B167',Price=0.0846842487297363,ManufacturerGuid=N'28fbffe3-ca0b-4ada-963e-2882398a75ac',IsMasterData='1' WHERE Guid = '1cc81769-cdf3-46ad-b8db-65133a3c21e8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd117d84f-7908-42e5-8b08-c4cbedb0f4f1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd117d84f-7908-42e5-8b08-c4cbedb0f4f1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c299b4f-9289-4976-be11-ceea84ad4481')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H001', 0.0321800145172998, N'd117d84f-7908-42e5-8b08-c4cbedb0f4f1', '1', '8c299b4f-9289-4976-be11-ceea84ad4481')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H001',Price=0.0321800145172998,ManufacturerGuid=N'd117d84f-7908-42e5-8b08-c4cbedb0f4f1',IsMasterData='1' WHERE Guid = '8c299b4f-9289-4976-be11-ceea84ad4481'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b65a06d4-f621-4750-9a82-43d8432ce85e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b65a06d4-f621-4750-9a82-43d8432ce85e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e536fd3d-42da-4c65-9128-1243829aed14')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H002', 0.0654488265182676, N'b65a06d4-f621-4750-9a82-43d8432ce85e', '1', 'e536fd3d-42da-4c65-9128-1243829aed14')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H002',Price=0.0654488265182676,ManufacturerGuid=N'b65a06d4-f621-4750-9a82-43d8432ce85e',IsMasterData='1' WHERE Guid = 'e536fd3d-42da-4c65-9128-1243829aed14'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b725036d-8e7c-4cb4-8f14-a413b35008e7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b725036d-8e7c-4cb4-8f14-a413b35008e7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2df60630-1ea6-46c1-931b-c11fb752ec60')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H003', 0.0578272441325913, N'b725036d-8e7c-4cb4-8f14-a413b35008e7', '1', '2df60630-1ea6-46c1-931b-c11fb752ec60')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H003',Price=0.0578272441325913,ManufacturerGuid=N'b725036d-8e7c-4cb4-8f14-a413b35008e7',IsMasterData='1' WHERE Guid = '2df60630-1ea6-46c1-931b-c11fb752ec60'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ed7bad2a-43fc-47dd-921a-56c1d77af764')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ed7bad2a-43fc-47dd-921a-56c1d77af764')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f9e5207b-7ee8-4908-9469-fa090248c20b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H009', 0.0210500846842487, N'ed7bad2a-43fc-47dd-921a-56c1d77af764', '1', 'f9e5207b-7ee8-4908-9469-fa090248c20b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H009',Price=0.0210500846842487,ManufacturerGuid=N'ed7bad2a-43fc-47dd-921a-56c1d77af764',IsMasterData='1' WHERE Guid = 'f9e5207b-7ee8-4908-9469-fa090248c20b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7d3c9c7d-de11-47eb-8f0f-8164ac7b74d8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7d3c9c7d-de11-47eb-8f0f-8164ac7b74d8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e90f67f3-26fe-483f-b7ff-758863f24e2a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H011', 0.0678683764819743, N'7d3c9c7d-de11-47eb-8f0f-8164ac7b74d8', '1', 'e90f67f3-26fe-483f-b7ff-758863f24e2a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H011',Price=0.0678683764819743,ManufacturerGuid=N'7d3c9c7d-de11-47eb-8f0f-8164ac7b74d8',IsMasterData='1' WHERE Guid = 'e90f67f3-26fe-483f-b7ff-758863f24e2a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9176c678-71cf-487c-b84b-c71e464256b9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9176c678-71cf-487c-b84b-c71e464256b9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3835add1-b50e-4b02-871c-de7002184e8b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H014', 0.0509315267360271, N'9176c678-71cf-487c-b84b-c71e464256b9', '1', '3835add1-b50e-4b02-871c-de7002184e8b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H014',Price=0.0509315267360271,ManufacturerGuid=N'9176c678-71cf-487c-b84b-c71e464256b9',IsMasterData='1' WHERE Guid = '3835add1-b50e-4b02-871c-de7002184e8b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a532a15b-f839-4b0a-b251-d67ebaef79b1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a532a15b-f839-4b0a-b251-d67ebaef79b1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0528f155-84ea-4f33-9aef-de9d74df404c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H213', 0.254052746189209, N'a532a15b-f839-4b0a-b251-d67ebaef79b1', '1', '0528f155-84ea-4f33-9aef-de9d74df404c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H213',Price=0.254052746189209,ManufacturerGuid=N'a532a15b-f839-4b0a-b251-d67ebaef79b1',IsMasterData='1' WHERE Guid = '0528f155-84ea-4f33-9aef-de9d74df404c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f2c81dfb-bb32-4231-b75d-153b9f4993fc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f2c81dfb-bb32-4231-b75d-153b9f4993fc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8e1f7141-afd1-4305-97d6-1bb8bc316518')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H221', 0.0864989112025163, N'f2c81dfb-bb32-4231-b75d-153b9f4993fc', '1', '8e1f7141-afd1-4305-97d6-1bb8bc316518')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H221',Price=0.0864989112025163,ManufacturerGuid=N'f2c81dfb-bb32-4231-b75d-153b9f4993fc',IsMasterData='1' WHERE Guid = '8e1f7141-afd1-4305-97d6-1bb8bc316518'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd2c77c94-cf90-40c9-9b26-1690e15d0662')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd2c77c94-cf90-40c9-9b26-1690e15d0662')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ada8d575-4cd8-40a7-bfe0-5c565d6eb0f7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H461', 0.855552867166707, N'd2c77c94-cf90-40c9-9b26-1690e15d0662', '1', 'ada8d575-4cd8-40a7-bfe0-5c565d6eb0f7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H461',Price=0.855552867166707,ManufacturerGuid=N'd2c77c94-cf90-40c9-9b26-1690e15d0662',IsMasterData='1' WHERE Guid = 'ada8d575-4cd8-40a7-bfe0-5c565d6eb0f7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'de884e1f-31cd-4e37-b548-6e1ceb0c2ad7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'de884e1f-31cd-4e37-b548-6e1ceb0c2ad7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8e2f76d2-0764-4c15-953b-4ee3ff0bbf38')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H494', 0.028308734575369, N'de884e1f-31cd-4e37-b548-6e1ceb0c2ad7', '1', '8e2f76d2-0764-4c15-953b-4ee3ff0bbf38')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H494',Price=0.028308734575369,ManufacturerGuid=N'de884e1f-31cd-4e37-b548-6e1ceb0c2ad7',IsMasterData='1' WHERE Guid = '8e2f76d2-0764-4c15-953b-4ee3ff0bbf38'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5fe338db-46b7-4ad1-81a6-4bab1531bea0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5fe338db-46b7-4ad1-81a6-4bab1531bea0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a5b447e4-a3eb-492e-8141-6626a2e38028')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H507', 0.0737962738930559, N'5fe338db-46b7-4ad1-81a6-4bab1531bea0', '1', 'a5b447e4-a3eb-492e-8141-6626a2e38028')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H507',Price=0.0737962738930559,ManufacturerGuid=N'5fe338db-46b7-4ad1-81a6-4bab1531bea0',IsMasterData='1' WHERE Guid = 'a5b447e4-a3eb-492e-8141-6626a2e38028'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8aaeee78-8add-4eb2-850e-4547617f9459')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8aaeee78-8add-4eb2-850e-4547617f9459')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '23d854ad-e3f3-4634-bdc3-b8d5de255e8c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H511', 0.149407210258892, N'8aaeee78-8add-4eb2-850e-4547617f9459', '1', '23d854ad-e3f3-4634-bdc3-b8d5de255e8c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H511',Price=0.149407210258892,ManufacturerGuid=N'8aaeee78-8add-4eb2-850e-4547617f9459',IsMasterData='1' WHERE Guid = '23d854ad-e3f3-4634-bdc3-b8d5de255e8c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'12f866c1-45be-46c4-8e8f-a6b24336f8f3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'12f866c1-45be-46c4-8e8f-a6b24336f8f3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ddc6b593-8aa4-4122-b4ff-10f623d178b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H512', 0.0592789741108154, N'12f866c1-45be-46c4-8e8f-a6b24336f8f3', '1', 'ddc6b593-8aa4-4122-b4ff-10f623d178b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H512',Price=0.0592789741108154,ManufacturerGuid=N'12f866c1-45be-46c4-8e8f-a6b24336f8f3',IsMasterData='1' WHERE Guid = 'ddc6b593-8aa4-4122-b4ff-10f623d178b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'63ad6358-885f-47a2-ad5c-72dbc7c922ec')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'63ad6358-885f-47a2-ad5c-72dbc7c922ec')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2373cb46-34ef-4673-b674-a34045af7e7a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H516', 0.299056375514154, N'63ad6358-885f-47a2-ad5c-72dbc7c922ec', '1', '2373cb46-34ef-4673-b674-a34045af7e7a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H516',Price=0.299056375514154,ManufacturerGuid=N'63ad6358-885f-47a2-ad5c-72dbc7c922ec',IsMasterData='1' WHERE Guid = '2373cb46-34ef-4673-b674-a34045af7e7a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9654cbed-6c92-4662-a936-ff4bd22443d2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9654cbed-6c92-4662-a936-ff4bd22443d2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cc88aa5b-11cf-4bd3-95bf-46ca5b620131')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H519', 0.018993467215098, N'9654cbed-6c92-4662-a936-ff4bd22443d2', '1', 'cc88aa5b-11cf-4bd3-95bf-46ca5b620131')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H519',Price=0.018993467215098,ManufacturerGuid=N'9654cbed-6c92-4662-a936-ff4bd22443d2',IsMasterData='1' WHERE Guid = 'cc88aa5b-11cf-4bd3-95bf-46ca5b620131'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fe28cbce-f12d-41cb-8af2-b9cc09c6ad15')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fe28cbce-f12d-41cb-8af2-b9cc09c6ad15')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bc867959-e18e-4c76-80fa-88592b6a96b9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H523', 0.0162109847568352, N'fe28cbce-f12d-41cb-8af2-b9cc09c6ad15', '1', 'bc867959-e18e-4c76-80fa-88592b6a96b9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H523',Price=0.0162109847568352,ManufacturerGuid=N'fe28cbce-f12d-41cb-8af2-b9cc09c6ad15',IsMasterData='1' WHERE Guid = 'bc867959-e18e-4c76-80fa-88592b6a96b9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a56a5e6d-2cf3-4a5b-a190-4e1531e43e97')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a56a5e6d-2cf3-4a5b-a190-4e1531e43e97')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b38df2c8-08da-4343-a549-f9146e29d500')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H541', 0.0254052746189209, N'a56a5e6d-2cf3-4a5b-a190-4e1531e43e97', '1', 'b38df2c8-08da-4343-a549-f9146e29d500')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H541',Price=0.0254052746189209,ManufacturerGuid=N'a56a5e6d-2cf3-4a5b-a190-4e1531e43e97',IsMasterData='1' WHERE Guid = 'b38df2c8-08da-4343-a549-f9146e29d500'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95b396e0-c2ec-4367-aa7a-b92f63939b5f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95b396e0-c2ec-4367-aa7a-b92f63939b5f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd4fdcfb0-f670-4cb4-9bfa-4297a87212c7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H543', 0.00653278490200823, N'95b396e0-c2ec-4367-aa7a-b92f63939b5f', '1', 'd4fdcfb0-f670-4cb4-9bfa-4297a87212c7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H543',Price=0.00653278490200823,ManufacturerGuid=N'95b396e0-c2ec-4367-aa7a-b92f63939b5f',IsMasterData='1' WHERE Guid = 'd4fdcfb0-f670-4cb4-9bfa-4297a87212c7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c1cfd9a9-8951-43cc-b3f7-bddc36021f51')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c1cfd9a9-8951-43cc-b3f7-bddc36021f51')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c000fd2-25e7-4b96-bf68-ab814cc602f5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2415H548', 0.393176869102347, N'c1cfd9a9-8951-43cc-b3f7-bddc36021f51', '1', '8c000fd2-25e7-4b96-bf68-ab814cc602f5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2415H548',Price=0.393176869102347,ManufacturerGuid=N'c1cfd9a9-8951-43cc-b3f7-bddc36021f51',IsMasterData='1' WHERE Guid = '8c000fd2-25e7-4b96-bf68-ab814cc602f5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4e592fa2-be50-4517-87ec-a51ada8b0902')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4e592fa2-be50-4517-87ec-a51ada8b0902')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b460f7a8-7002-46f4-a72a-52962a11fd17')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2648A107', 0.0475441567868376, N'4e592fa2-be50-4517-87ec-a51ada8b0902', '1', 'b460f7a8-7002-46f4-a72a-52962a11fd17')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2648A107',Price=0.0475441567868376,ManufacturerGuid=N'4e592fa2-be50-4517-87ec-a51ada8b0902',IsMasterData='1' WHERE Guid = 'b460f7a8-7002-46f4-a72a-52962a11fd17'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'87489d7b-4dfc-4f60-a163-377ba7773f2a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'87489d7b-4dfc-4f60-a163-377ba7773f2a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2f28c340-4683-4aff-afef-bee236cde6f7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'6V-5048', 0.0324219695136705, N'87489d7b-4dfc-4f60-a163-377ba7773f2a', '1', '2f28c340-4683-4aff-afef-bee236cde6f7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'6V-5048',Price=0.0324219695136705,ManufacturerGuid=N'87489d7b-4dfc-4f60-a163-377ba7773f2a',IsMasterData='1' WHERE Guid = '2f28c340-4683-4aff-afef-bee236cde6f7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dcdf25c7-7881-4459-9602-e05ed1fda449')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dcdf25c7-7881-4459-9602-e05ed1fda449')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '282b627c-dd7c-46e7-9905-24e72ab15a4f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2741639', 0.114928623276071, N'dcdf25c7-7881-4459-9602-e05ed1fda449', '1', '282b627c-dd7c-46e7-9905-24e72ab15a4f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2741639',Price=0.114928623276071,ManufacturerGuid=N'dcdf25c7-7881-4459-9602-e05ed1fda449',IsMasterData='1' WHERE Guid = '282b627c-dd7c-46e7-9905-24e72ab15a4f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'48ca6c8f-e227-4282-9dd5-a07b838a32cf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'48ca6c8f-e227-4282-9dd5-a07b838a32cf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '00928ce3-5129-4310-934f-9330f827e75e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'8C3131', 0.01790466973143, N'48ca6c8f-e227-4282-9dd5-a07b838a32cf', '1', '00928ce3-5129-4310-934f-9330f827e75e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'8C3131',Price=0.01790466973143,ManufacturerGuid=N'48ca6c8f-e227-4282-9dd5-a07b838a32cf',IsMasterData='1' WHERE Guid = '00928ce3-5129-4310-934f-9330f827e75e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'af814778-1478-4301-8ce2-48d2b9884554')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'af814778-1478-4301-8ce2-48d2b9884554')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fc82c706-ab1f-4ee7-8e55-ce1a639a68f9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'3K0360', 0.009678199854827, N'af814778-1478-4301-8ce2-48d2b9884554', '1', 'fc82c706-ab1f-4ee7-8e55-ce1a639a68f9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'3K0360',Price=0.009678199854827,ManufacturerGuid=N'af814778-1478-4301-8ce2-48d2b9884554',IsMasterData='1' WHERE Guid = 'fc82c706-ab1f-4ee7-8e55-ce1a639a68f9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dd00ad45-ac0e-4235-8632-f2b98b577885')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dd00ad45-ac0e-4235-8632-f2b98b577885')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '24b03762-110c-4c8a-8c47-8f645b6ed722')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'1899377', 0.114202758286959, N'dd00ad45-ac0e-4235-8632-f2b98b577885', '1', '24b03762-110c-4c8a-8c47-8f645b6ed722')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'1899377',Price=0.114202758286959,ManufacturerGuid=N'dd00ad45-ac0e-4235-8632-f2b98b577885',IsMasterData='1' WHERE Guid = '24b03762-110c-4c8a-8c47-8f645b6ed722'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a9469c26-8eff-4a48-928d-e3042cae65e7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a9469c26-8eff-4a48-928d-e3042cae65e7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cbec0faf-0514-4653-8550-2cff8e3815e6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2272519', 0.0907331236390031, N'a9469c26-8eff-4a48-928d-e3042cae65e7', '1', 'cbec0faf-0514-4653-8550-2cff8e3815e6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2272519',Price=0.0907331236390031,ManufacturerGuid=N'a9469c26-8eff-4a48-928d-e3042cae65e7',IsMasterData='1' WHERE Guid = 'cbec0faf-0514-4653-8550-2cff8e3815e6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2eac62a0-60c3-4283-bf69-3c2c5a1d4f71')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2eac62a0-60c3-4283-bf69-3c2c5a1d4f71')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b9382ca9-c96b-41eb-860a-b1c6f449c593')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2284947', 0.0447616743285749, N'2eac62a0-60c3-4283-bf69-3c2c5a1d4f71', '1', 'b9382ca9-c96b-41eb-860a-b1c6f449c593')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2284947',Price=0.0447616743285749,ManufacturerGuid=N'2eac62a0-60c3-4283-bf69-3c2c5a1d4f71',IsMasterData='1' WHERE Guid = 'b9382ca9-c96b-41eb-860a-b1c6f449c593'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6e518032-fd20-4cce-9054-b059b0a549e1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6e518032-fd20-4cce-9054-b059b0a549e1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4ff71174-c498-4ed1-aafb-30629aca7fba')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2287103', 0.10041132349383, N'6e518032-fd20-4cce-9054-b059b0a549e1', '1', '4ff71174-c498-4ed1-aafb-30629aca7fba')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2287103',Price=0.10041132349383,ManufacturerGuid=N'6e518032-fd20-4cce-9054-b059b0a549e1',IsMasterData='1' WHERE Guid = '4ff71174-c498-4ed1-aafb-30629aca7fba'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3caf3c4a-538c-4ce7-a4ee-f81d854bcef5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3caf3c4a-538c-4ce7-a4ee-f81d854bcef5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2e977e8b-7668-40ae-8b44-a66d8ce13ca2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2S4078', 0.029034599564481, N'3caf3c4a-538c-4ce7-a4ee-f81d854bcef5', '1', '2e977e8b-7668-40ae-8b44-a66d8ce13ca2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2S4078',Price=0.029034599564481,ManufacturerGuid=N'3caf3c4a-538c-4ce7-a4ee-f81d854bcef5',IsMasterData='1' WHERE Guid = '2e977e8b-7668-40ae-8b44-a66d8ce13ca2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'96a93cc7-c296-4103-af2e-2075ad698fc5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'96a93cc7-c296-4103-af2e-2075ad698fc5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b90c54bb-4b6e-4e7b-b842-1845ec825c90')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'3D2824', 0.0163319622550206, N'96a93cc7-c296-4103-af2e-2075ad698fc5', '1', 'b90c54bb-4b6e-4e7b-b842-1845ec825c90')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'3D2824',Price=0.0163319622550206,ManufacturerGuid=N'96a93cc7-c296-4103-af2e-2075ad698fc5',IsMasterData='1' WHERE Guid = 'b90c54bb-4b6e-4e7b-b842-1845ec825c90'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e6e220b7-3fe1-4671-bfd8-12417dfa0ca1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e6e220b7-3fe1-4671-bfd8-12417dfa0ca1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0c18a44c-95aa-4686-87f6-6cb6f7bd3fd7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'4N1230', 0.42648197435277, N'e6e220b7-3fe1-4671-bfd8-12417dfa0ca1', '1', '0c18a44c-95aa-4686-87f6-6cb6f7bd3fd7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'4N1230',Price=0.42648197435277,ManufacturerGuid=N'e6e220b7-3fe1-4671-bfd8-12417dfa0ca1',IsMasterData='1' WHERE Guid = '0c18a44c-95aa-4686-87f6-6cb6f7bd3fd7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3a78e489-9d48-4c29-a34f-35300a4ee000')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3a78e489-9d48-4c29-a34f-35300a4ee000')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '800a3821-4e62-4bbc-86bf-7b5ef73c0270')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'6V8676', 0.141059762884104, N'3a78e489-9d48-4c29-a34f-35300a4ee000', '1', '800a3821-4e62-4bbc-86bf-7b5ef73c0270')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'6V8676',Price=0.141059762884104,ManufacturerGuid=N'3a78e489-9d48-4c29-a34f-35300a4ee000',IsMasterData='1' WHERE Guid = '800a3821-4e62-4bbc-86bf-7b5ef73c0270'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a18c3146-60d5-4889-8203-494d96c483da')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a18c3146-60d5-4889-8203-494d96c483da')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b41c46a6-f1c1-48f7-9b5d-8614851d362a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'6V9027', 0.0145172997822405, N'a18c3146-60d5-4889-8203-494d96c483da', '1', 'b41c46a6-f1c1-48f7-9b5d-8614851d362a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'6V9027',Price=0.0145172997822405,ManufacturerGuid=N'a18c3146-60d5-4889-8203-494d96c483da',IsMasterData='1' WHERE Guid = 'b41c46a6-f1c1-48f7-9b5d-8614851d362a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'73639a1d-6dfb-4e08-a8e0-6a9e90405972')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'73639a1d-6dfb-4e08-a8e0-6a9e90405972')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f141ac43-4a5c-43c4-b481-4267256f3806')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'2287089', 0.038712799419308, N'73639a1d-6dfb-4e08-a8e0-6a9e90405972', '1', 'f141ac43-4a5c-43c4-b481-4267256f3806')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'2287089',Price=0.038712799419308,ManufacturerGuid=N'73639a1d-6dfb-4e08-a8e0-6a9e90405972',IsMasterData='1' WHERE Guid = 'f141ac43-4a5c-43c4-b481-4267256f3806'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'894d7429-75c9-4141-ba7c-1cfe705a8623')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'894d7429-75c9-4141-ba7c-1cfe705a8623')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '74f8dadd-15ce-4b80-be9e-50f080b09d71')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'O RING', N'4J5477', 0.00725864989112025, N'894d7429-75c9-4141-ba7c-1cfe705a8623', '1', '74f8dadd-15ce-4b80-be9e-50f080b09d71')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'O RING',PartNumber=N'4J5477',Price=0.00725864989112025,ManufacturerGuid=N'894d7429-75c9-4141-ba7c-1cfe705a8623',IsMasterData='1' WHERE Guid = '74f8dadd-15ce-4b80-be9e-50f080b09d71'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'151e1dcd-435e-4799-ab1b-74caf3760080')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'151e1dcd-435e-4799-ab1b-74caf3760080')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b4abad57-5311-47e0-abec-c7b3e81fea5c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'0566002', 0.0176627147350593, N'151e1dcd-435e-4799-ab1b-74caf3760080', '1', 'b4abad57-5311-47e0-abec-c7b3e81fea5c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'0566002',Price=0.0176627147350593,ManufacturerGuid=N'151e1dcd-435e-4799-ab1b-74caf3760080',IsMasterData='1' WHERE Guid = 'b4abad57-5311-47e0-abec-c7b3e81fea5c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5c328631-1702-4edb-bf6f-d80278325ba6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5c328631-1702-4edb-bf6f-d80278325ba6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5385f97f-cd7f-4633-927a-ce576af1d8e8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'0566003', 0.0656907815146383, N'5c328631-1702-4edb-bf6f-d80278325ba6', '1', '5385f97f-cd7f-4633-927a-ce576af1d8e8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'0566003',Price=0.0656907815146383,ManufacturerGuid=N'5c328631-1702-4edb-bf6f-d80278325ba6',IsMasterData='1' WHERE Guid = '5385f97f-cd7f-4633-927a-ce576af1d8e8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bc2297b7-dbbc-4fc6-b96c-9f3c2c2f1c9e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bc2297b7-dbbc-4fc6-b96c-9f3c2c2f1c9e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6defde48-a46f-4a38-bbc3-5ef961b0b286')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'0566004', 0.0747640938785386, N'bc2297b7-dbbc-4fc6-b96c-9f3c2c2f1c9e', '1', '6defde48-a46f-4a38-bbc3-5ef961b0b286')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'0566004',Price=0.0747640938785386,ManufacturerGuid=N'bc2297b7-dbbc-4fc6-b96c-9f3c2c2f1c9e',IsMasterData='1' WHERE Guid = '6defde48-a46f-4a38-bbc3-5ef961b0b286'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'371378b5-db7a-4bf7-993c-a8c323286ab3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'371378b5-db7a-4bf7-993c-a8c323286ab3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c777193d-c548-49f2-831e-1856fc8e4827')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'0566007', 0.180498427292524, N'371378b5-db7a-4bf7-993c-a8c323286ab3', '1', 'c777193d-c548-49f2-831e-1856fc8e4827')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'0566007',Price=0.180498427292524,ManufacturerGuid=N'371378b5-db7a-4bf7-993c-a8c323286ab3',IsMasterData='1' WHERE Guid = 'c777193d-c548-49f2-831e-1856fc8e4827'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'84025aa6-4e81-4fe5-8be9-add314af7192')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'84025aa6-4e81-4fe5-8be9-add314af7192')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6119db85-72af-4f2c-8fc0-7f1c6eab8c4c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'33811112', 0.038712799419308, N'84025aa6-4e81-4fe5-8be9-add314af7192', '1', '6119db85-72af-4f2c-8fc0-7f1c6eab8c4c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'33811112',Price=0.038712799419308,ManufacturerGuid=N'84025aa6-4e81-4fe5-8be9-add314af7192',IsMasterData='1' WHERE Guid = '6119db85-72af-4f2c-8fc0-7f1c6eab8c4c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3d7b8502-3b64-40e5-a116-a3975a9b39db')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3d7b8502-3b64-40e5-a116-a3975a9b39db')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b17d6279-0702-4597-86fb-e105af02f243')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'33811113', 0.0281877570771836, N'3d7b8502-3b64-40e5-a116-a3975a9b39db', '1', 'b17d6279-0702-4597-86fb-e105af02f243')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'33811113',Price=0.0281877570771836,ManufacturerGuid=N'3d7b8502-3b64-40e5-a116-a3975a9b39db',IsMasterData='1' WHERE Guid = 'b17d6279-0702-4597-86fb-e105af02f243'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'34bcb11f-566a-4f97-a0c8-e11c8bbe6bb5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'34bcb11f-566a-4f97-a0c8-e11c8bbe6bb5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e191ee12-c1c2-4c56-ac5e-81fd90cd0017')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'33811114', 0.0292765545608517, N'34bcb11f-566a-4f97-a0c8-e11c8bbe6bb5', '1', 'e191ee12-c1c2-4c56-ac5e-81fd90cd0017')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'33811114',Price=0.0292765545608517,ManufacturerGuid=N'34bcb11f-566a-4f97-a0c8-e11c8bbe6bb5',IsMasterData='1' WHERE Guid = 'e191ee12-c1c2-4c56-ac5e-81fd90cd0017'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'375486de-5a7b-42e0-86c5-635c3a90aedd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'375486de-5a7b-42e0-86c5-635c3a90aedd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c608b2a0-1c8a-4fa5-b3b4-fa05efcb1fcf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'OLIVE', N'33812116', 0.038712799419308, N'375486de-5a7b-42e0-86c5-635c3a90aedd', '1', 'c608b2a0-1c8a-4fa5-b3b4-fa05efcb1fcf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'OLIVE',PartNumber=N'33812116',Price=0.038712799419308,ManufacturerGuid=N'375486de-5a7b-42e0-86c5-635c3a90aedd',IsMasterData='1' WHERE Guid = 'c608b2a0-1c8a-4fa5-b3b4-fa05efcb1fcf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4b2b2b8e-3a4f-4596-b88b-86dfd6b353a2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4b2b2b8e-3a4f-4596-b88b-86dfd6b353a2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3496729e-65e4-4145-a6ae-020d95bcf8fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'2112A066', 0.029034599564481, N'4b2b2b8e-3a4f-4596-b88b-86dfd6b353a2', '1', '3496729e-65e4-4145-a6ae-020d95bcf8fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'2112A066',Price=0.029034599564481,ManufacturerGuid=N'4b2b2b8e-3a4f-4596-b88b-86dfd6b353a2',IsMasterData='1' WHERE Guid = '3496729e-65e4-4145-a6ae-020d95bcf8fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fc27ef4a-11d3-41bb-a090-f58925ce2670')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fc27ef4a-11d3-41bb-a090-f58925ce2670')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f3271feb-fa83-4cb8-b51c-37433e606b5a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'2116053', 0.0112509073312364, N'fc27ef4a-11d3-41bb-a090-f58925ce2670', '1', 'f3271feb-fa83-4cb8-b51c-37433e606b5a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'2116053',Price=0.0112509073312364,ManufacturerGuid=N'fc27ef4a-11d3-41bb-a090-f58925ce2670',IsMasterData='1' WHERE Guid = 'f3271feb-fa83-4cb8-b51c-37433e606b5a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'df555caa-b313-4604-aae9-a151f1bc2d91')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'df555caa-b313-4604-aae9-a151f1bc2d91')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f66a9478-db3f-41fb-ad2e-de6dfd437da3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'2116087', 0.0185095572223566, N'df555caa-b313-4604-aae9-a151f1bc2d91', '1', 'f66a9478-db3f-41fb-ad2e-de6dfd437da3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'2116087',Price=0.0185095572223566,ManufacturerGuid=N'df555caa-b313-4604-aae9-a151f1bc2d91',IsMasterData='1' WHERE Guid = 'f66a9478-db3f-41fb-ad2e-de6dfd437da3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'15c89a46-d588-4901-a9b1-ae291721e032')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'15c89a46-d588-4901-a9b1-ae291721e032')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b026cc7e-1396-405a-87f6-dc1766aa3080')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'3211C001', 0.311396080329059, N'15c89a46-d588-4901-a9b1-ae291721e032', '1', 'b026cc7e-1396-405a-87f6-dc1766aa3080')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'3211C001',Price=0.311396080329059,ManufacturerGuid=N'15c89a46-d588-4901-a9b1-ae291721e032',IsMasterData='1' WHERE Guid = 'b026cc7e-1396-405a-87f6-dc1766aa3080'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7dfd8fc4-d6b4-4462-935c-44fd76c03ea5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7dfd8fc4-d6b4-4462-935c-44fd76c03ea5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4327aae3-88bd-4830-be6e-0ece5d079269')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'32722105', 0.51790466973143, N'7dfd8fc4-d6b4-4462-935c-44fd76c03ea5', '1', '4327aae3-88bd-4830-be6e-0ece5d079269')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'32722105',Price=0.51790466973143,ManufacturerGuid=N'7dfd8fc4-d6b4-4462-935c-44fd76c03ea5',IsMasterData='1' WHERE Guid = '4327aae3-88bd-4830-be6e-0ece5d079269'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0c324ea1-3293-42d5-b3c4-4ae1c3bd2c56')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0c324ea1-3293-42d5-b3c4-4ae1c3bd2c56')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '851a3995-41f9-4633-9ec3-4a6d6b7803b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PIN', N'3272A014', 1.88724897169127, N'0c324ea1-3293-42d5-b3c4-4ae1c3bd2c56', '1', '851a3995-41f9-4633-9ec3-4a6d6b7803b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PIN',PartNumber=N'3272A014',Price=1.88724897169127,ManufacturerGuid=N'0c324ea1-3293-42d5-b3c4-4ae1c3bd2c56',IsMasterData='1' WHERE Guid = '851a3995-41f9-4633-9ec3-4a6d6b7803b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'769e77be-9eaa-454c-82ba-fb5fa596c4c1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'769e77be-9eaa-454c-82ba-fb5fa596c4c1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b9828d4a-2f6a-4dfd-b36d-6e72c26ce101')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'4147V006', 0.38011129929833, N'769e77be-9eaa-454c-82ba-fb5fa596c4c1', '1', 'b9828d4a-2f6a-4dfd-b36d-6e72c26ce101')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'4147V006',Price=0.38011129929833,ManufacturerGuid=N'769e77be-9eaa-454c-82ba-fb5fa596c4c1',IsMasterData='1' WHERE Guid = 'b9828d4a-2f6a-4dfd-b36d-6e72c26ce101'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95fdbcef-7800-4093-9fc4-a9225f06ff12')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95fdbcef-7800-4093-9fc4-a9225f06ff12')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '474f857c-498e-4454-8982-f76b3df13a1c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650002', 1.51221872731672, N'95fdbcef-7800-4093-9fc4-a9225f06ff12', '1', '474f857c-498e-4454-8982-f76b3df13a1c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650002',Price=1.51221872731672,ManufacturerGuid=N'95fdbcef-7800-4093-9fc4-a9225f06ff12',IsMasterData='1' WHERE Guid = '474f857c-498e-4454-8982-f76b3df13a1c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'da32efac-2a61-496b-9544-804e597d19b8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'da32efac-2a61-496b-9544-804e597d19b8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2b0c19c8-0fb5-4637-a7da-406182af5615')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650203', 0.15932736511009, N'da32efac-2a61-496b-9544-804e597d19b8', '1', '2b0c19c8-0fb5-4637-a7da-406182af5615')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650203',Price=0.15932736511009,ManufacturerGuid=N'da32efac-2a61-496b-9544-804e597d19b8',IsMasterData='1' WHERE Guid = '2b0c19c8-0fb5-4637-a7da-406182af5615'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'66b0e4dd-d9b9-456e-8fb8-7cf047c95a38')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'66b0e4dd-d9b9-456e-8fb8-7cf047c95a38')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0383ffd8-2e40-4414-994c-315b9d19aa1c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650204', 0.113355915799661, N'66b0e4dd-d9b9-456e-8fb8-7cf047c95a38', '1', '0383ffd8-2e40-4414-994c-315b9d19aa1c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650204',Price=0.113355915799661,ManufacturerGuid=N'66b0e4dd-d9b9-456e-8fb8-7cf047c95a38',IsMasterData='1' WHERE Guid = '0383ffd8-2e40-4414-994c-315b9d19aa1c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'28cff223-fd0d-479a-8e82-f84ba6650083')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'28cff223-fd0d-479a-8e82-f84ba6650083')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '26584b0d-b88e-41b6-b45f-823a3e745674')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650586', 0.0714977014275345, N'28cff223-fd0d-479a-8e82-f84ba6650083', '1', '26584b0d-b88e-41b6-b45f-823a3e745674')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650586',Price=0.0714977014275345,ManufacturerGuid=N'28cff223-fd0d-479a-8e82-f84ba6650083',IsMasterData='1' WHERE Guid = '26584b0d-b88e-41b6-b45f-823a3e745674'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1b7dc15b-1ed0-4794-9d9f-b665e8c2c699')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1b7dc15b-1ed0-4794-9d9f-b665e8c2c699')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2448cb35-e5da-4c16-b90d-50541a9850a9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650588', 0.136462617953061, N'1b7dc15b-1ed0-4794-9d9f-b665e8c2c699', '1', '2448cb35-e5da-4c16-b90d-50541a9850a9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650588',Price=0.136462617953061,ManufacturerGuid=N'1b7dc15b-1ed0-4794-9d9f-b665e8c2c699',IsMasterData='1' WHERE Guid = '2448cb35-e5da-4c16-b90d-50541a9850a9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'105dcbfe-079d-4ad9-b58e-7782ffdfff6c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'105dcbfe-079d-4ad9-b58e-7782ffdfff6c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '77dae07a-794f-4b05-8e99-f78c75fb4ec0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650593', 0.0567384466489233, N'105dcbfe-079d-4ad9-b58e-7782ffdfff6c', '1', '77dae07a-794f-4b05-8e99-f78c75fb4ec0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650593',Price=0.0567384466489233,ManufacturerGuid=N'105dcbfe-079d-4ad9-b58e-7782ffdfff6c',IsMasterData='1' WHERE Guid = '77dae07a-794f-4b05-8e99-f78c75fb4ec0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'482757ea-87e4-4054-9d4b-2211febd0012')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'482757ea-87e4-4054-9d4b-2211febd0012')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '01b17dec-5380-48c7-8789-5fc2db8ff253')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650594', 0.0608516815872248, N'482757ea-87e4-4054-9d4b-2211febd0012', '1', '01b17dec-5380-48c7-8789-5fc2db8ff253')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650594',Price=0.0608516815872248,ManufacturerGuid=N'482757ea-87e4-4054-9d4b-2211febd0012',IsMasterData='1' WHERE Guid = '01b17dec-5380-48c7-8789-5fc2db8ff253'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4aa60d37-889b-4c93-a472-a152e7bc1a7d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4aa60d37-889b-4c93-a472-a152e7bc1a7d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '938e34b4-e8d2-4046-91cb-bd86455caa4b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650692', 0.0793612388095814, N'4aa60d37-889b-4c93-a472-a152e7bc1a7d', '1', '938e34b4-e8d2-4046-91cb-bd86455caa4b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650692',Price=0.0793612388095814,ManufacturerGuid=N'4aa60d37-889b-4c93-a472-a152e7bc1a7d',IsMasterData='1' WHERE Guid = '938e34b4-e8d2-4046-91cb-bd86455caa4b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8d97b532-526b-4a2b-908d-49359d21adfc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8d97b532-526b-4a2b-908d-49359d21adfc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0a76d07f-7782-45c0-8cfe-c9cddd8d55d6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650710', 0.0633922090491169, N'8d97b532-526b-4a2b-908d-49359d21adfc', '1', '0a76d07f-7782-45c0-8cfe-c9cddd8d55d6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650710',Price=0.0633922090491169,ManufacturerGuid=N'8d97b532-526b-4a2b-908d-49359d21adfc',IsMasterData='1' WHERE Guid = '0a76d07f-7782-45c0-8cfe-c9cddd8d55d6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e8e925c1-1120-45e5-a0a1-a3e5ccf514ac')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e8e925c1-1120-45e5-a0a1-a3e5ccf514ac')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1568e429-0b76-4ad4-8cd3-edcc4cd7693a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'0650722', 0.321074280183886, N'e8e925c1-1120-45e5-a0a1-a3e5ccf514ac', '1', '1568e429-0b76-4ad4-8cd3-edcc4cd7693a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'0650722',Price=0.321074280183886,ManufacturerGuid=N'e8e925c1-1120-45e5-a0a1-a3e5ccf514ac',IsMasterData='1' WHERE Guid = '1568e429-0b76-4ad4-8cd3-edcc4cd7693a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ed5f8516-62a9-4f95-81cb-ed7b9b25e3d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ed5f8516-62a9-4f95-81cb-ed7b9b25e3d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f2c53942-4234-44e7-bfa7-c66079c209e1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'1535294', 0.914589886281152, N'ed5f8516-62a9-4f95-81cb-ed7b9b25e3d3', '1', 'f2c53942-4234-44e7-bfa7-c66079c209e1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'1535294',Price=0.914589886281152,ManufacturerGuid=N'ed5f8516-62a9-4f95-81cb-ed7b9b25e3d3',IsMasterData='1' WHERE Guid = 'f2c53942-4234-44e7-bfa7-c66079c209e1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8bbbee3f-ca35-46aa-8565-3ad240707c04')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8bbbee3f-ca35-46aa-8565-3ad240707c04')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5ee5fcd4-0bbf-41ea-8186-e1cd48f239c4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2431131', 0.0545608516815872, N'8bbbee3f-ca35-46aa-8565-3ad240707c04', '1', '5ee5fcd4-0bbf-41ea-8186-e1cd48f239c4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2431131',Price=0.0545608516815872,ManufacturerGuid=N'8bbbee3f-ca35-46aa-8565-3ad240707c04',IsMasterData='1' WHERE Guid = '5ee5fcd4-0bbf-41ea-8186-e1cd48f239c4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b1b6dbe7-580a-41d6-a9bf-03528e8f01fe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b1b6dbe7-580a-41d6-a9bf-03528e8f01fe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9a7db9f5-ef33-4d7c-8048-eeb234cd31b9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2431154', 0.111420275828696, N'b1b6dbe7-580a-41d6-a9bf-03528e8f01fe', '1', '9a7db9f5-ef33-4d7c-8048-eeb234cd31b9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2431154',Price=0.111420275828696,ManufacturerGuid=N'b1b6dbe7-580a-41d6-a9bf-03528e8f01fe',IsMasterData='1' WHERE Guid = '9a7db9f5-ef33-4d7c-8048-eeb234cd31b9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'219239d2-cee1-419a-88c2-9d3ad99317b7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'219239d2-cee1-419a-88c2-9d3ad99317b7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0f6e9338-2398-4a1c-9d4c-ab322f5b5df1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2431A016', 0.148318412775224, N'219239d2-cee1-419a-88c2-9d3ad99317b7', '1', '0f6e9338-2398-4a1c-9d4c-ab322f5b5df1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2431A016',Price=0.148318412775224,ManufacturerGuid=N'219239d2-cee1-419a-88c2-9d3ad99317b7',IsMasterData='1' WHERE Guid = '0f6e9338-2398-4a1c-9d4c-ab322f5b5df1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1cf1c2f4-a37f-42c9-be0a-5d178ec6458f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1cf1c2f4-a37f-42c9-be0a-5d178ec6458f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd84f1701-eab6-4df9-b46b-579ee5297ba7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2431A103', 0.162109847568352, N'1cf1c2f4-a37f-42c9-be0a-5d178ec6458f', '1', 'd84f1701-eab6-4df9-b46b-579ee5297ba7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2431A103',Price=0.162109847568352,ManufacturerGuid=N'1cf1c2f4-a37f-42c9-be0a-5d178ec6458f',IsMasterData='1' WHERE Guid = 'd84f1701-eab6-4df9-b46b-579ee5297ba7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2178eb79-271d-4c2e-930e-db425c1b554a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2178eb79-271d-4c2e-930e-db425c1b554a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2199cc37-fdbf-4189-a85d-5e727118738a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A108', 0.0345995644810065, N'2178eb79-271d-4c2e-930e-db425c1b554a', '1', '2199cc37-fdbf-4189-a85d-5e727118738a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A108',Price=0.0345995644810065,ManufacturerGuid=N'2178eb79-271d-4c2e-930e-db425c1b554a',IsMasterData='1' WHERE Guid = '2199cc37-fdbf-4189-a85d-5e727118738a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aad228c9-ae01-4a77-bf48-3aa0531b3bd7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aad228c9-ae01-4a77-bf48-3aa0531b3bd7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a28ef908-7e96-4e17-be3b-040575fe7d26')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A109', 0.0368981369465279, N'aad228c9-ae01-4a77-bf48-3aa0531b3bd7', '1', 'a28ef908-7e96-4e17-be3b-040575fe7d26')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A109',Price=0.0368981369465279,ManufacturerGuid=N'aad228c9-ae01-4a77-bf48-3aa0531b3bd7',IsMasterData='1' WHERE Guid = 'a28ef908-7e96-4e17-be3b-040575fe7d26'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e1fada6d-2e18-4acc-bdd9-551d9b499df8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e1fada6d-2e18-4acc-bdd9-551d9b499df8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4f0dc74a-b308-4609-9a2f-947e30d41f56')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A112', 0.029034599564481, N'e1fada6d-2e18-4acc-bdd9-551d9b499df8', '1', '4f0dc74a-b308-4609-9a2f-947e30d41f56')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A112',Price=0.029034599564481,ManufacturerGuid=N'e1fada6d-2e18-4acc-bdd9-551d9b499df8',IsMasterData='1' WHERE Guid = '4f0dc74a-b308-4609-9a2f-947e30d41f56'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'628e7d88-5328-453e-a7e7-0493502118c4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'628e7d88-5328-453e-a7e7-0493502118c4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4bbde426-47df-446d-b22a-6c229e916e5c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A118', 0.0561335591579966, N'628e7d88-5328-453e-a7e7-0493502118c4', '1', '4bbde426-47df-446d-b22a-6c229e916e5c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A118',Price=0.0561335591579966,ManufacturerGuid=N'628e7d88-5328-453e-a7e7-0493502118c4',IsMasterData='1' WHERE Guid = '4bbde426-47df-446d-b22a-6c229e916e5c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a99085a5-81f8-420b-9ff3-c506778aa489')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a99085a5-81f8-420b-9ff3-c506778aa489')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '658ce1e2-af3c-40bf-88d4-688b3428bae8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A124', 0.0792402613113961, N'a99085a5-81f8-420b-9ff3-c506778aa489', '1', '658ce1e2-af3c-40bf-88d4-688b3428bae8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A124',Price=0.0792402613113961,ManufacturerGuid=N'a99085a5-81f8-420b-9ff3-c506778aa489',IsMasterData='1' WHERE Guid = '658ce1e2-af3c-40bf-88d4-688b3428bae8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c259cc00-f7de-4927-b846-0fc576bbb7b6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c259cc00-f7de-4927-b846-0fc576bbb7b6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9619e496-1abb-4f30-813f-c9857b3bb6fa')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A207', 0.187394144689088, N'c259cc00-f7de-4927-b846-0fc576bbb7b6', '1', '9619e496-1abb-4f30-813f-c9857b3bb6fa')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A207',Price=0.187394144689088,ManufacturerGuid=N'c259cc00-f7de-4927-b846-0fc576bbb7b6',IsMasterData='1' WHERE Guid = '9619e496-1abb-4f30-813f-c9857b3bb6fa'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fe11791d-62b5-4f37-9184-a697f8ca47ba')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fe11791d-62b5-4f37-9184-a697f8ca47ba')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '49c9a8e5-cbbd-4629-9cdf-49279ce29d36')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A304', 0.124848778127268, N'fe11791d-62b5-4f37-9184-a697f8ca47ba', '1', '49c9a8e5-cbbd-4629-9cdf-49279ce29d36')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A304',Price=0.124848778127268,ManufacturerGuid=N'fe11791d-62b5-4f37-9184-a697f8ca47ba',IsMasterData='1' WHERE Guid = '49c9a8e5-cbbd-4629-9cdf-49279ce29d36'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7853563f-5581-4329-8153-2b0eb7fe9f02')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7853563f-5581-4329-8153-2b0eb7fe9f02')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '17a13602-5015-4cf6-accd-779bb56f8861')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A307', 0.20445197193322, N'7853563f-5581-4329-8153-2b0eb7fe9f02', '1', '17a13602-5015-4cf6-accd-779bb56f8861')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A307',Price=0.20445197193322,ManufacturerGuid=N'7853563f-5581-4329-8153-2b0eb7fe9f02',IsMasterData='1' WHERE Guid = '17a13602-5015-4cf6-accd-779bb56f8861'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c743e706-9748-4e6b-9227-d10d4e430d71')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c743e706-9748-4e6b-9227-d10d4e430d71')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8335500d-3eb9-4944-aedd-73db78e8cb15')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2485A519', 0.0943624485845633, N'c743e706-9748-4e6b-9227-d10d4e430d71', '1', '8335500d-3eb9-4944-aedd-73db78e8cb15')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2485A519',Price=0.0943624485845633,ManufacturerGuid=N'c743e706-9748-4e6b-9227-d10d4e430d71',IsMasterData='1' WHERE Guid = '8335500d-3eb9-4944-aedd-73db78e8cb15'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'97b56073-859a-40c9-913a-d673a5025cd5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'97b56073-859a-40c9-913a-d673a5025cd5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7f042358-c8ce-4e0a-bdb6-3844063d0eee')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487094', 0.904911686426325, N'97b56073-859a-40c9-913a-d673a5025cd5', '1', '7f042358-c8ce-4e0a-bdb6-3844063d0eee')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487094',Price=0.904911686426325,ManufacturerGuid=N'97b56073-859a-40c9-913a-d673a5025cd5',IsMasterData='1' WHERE Guid = '7f042358-c8ce-4e0a-bdb6-3844063d0eee'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'49358946-d02a-41ae-be3d-0310919a607b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'49358946-d02a-41ae-be3d-0310919a607b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cadc8aac-b904-4b8f-9e1d-f4389f75dbc7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487204', 0.0567384466489233, N'49358946-d02a-41ae-be3d-0310919a607b', '1', 'cadc8aac-b904-4b8f-9e1d-f4389f75dbc7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487204',Price=0.0567384466489233,ManufacturerGuid=N'49358946-d02a-41ae-be3d-0310919a607b',IsMasterData='1' WHERE Guid = 'cadc8aac-b904-4b8f-9e1d-f4389f75dbc7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ab2aaf7d-6139-47d1-a497-7ee9f328166a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ab2aaf7d-6139-47d1-a497-7ee9f328166a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e701fcf4-0def-42f4-b379-87e4ac99c710')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487207', 0.0163319622550206, N'ab2aaf7d-6139-47d1-a497-7ee9f328166a', '1', 'e701fcf4-0def-42f4-b379-87e4ac99c710')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487207',Price=0.0163319622550206,ManufacturerGuid=N'ab2aaf7d-6139-47d1-a497-7ee9f328166a',IsMasterData='1' WHERE Guid = 'e701fcf4-0def-42f4-b379-87e4ac99c710'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'358afa25-5884-46c8-b2bb-c9543e1a407f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'358afa25-5884-46c8-b2bb-c9543e1a407f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2574c71d-2ae5-425f-bf08-00a239314a8e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A009', 0.171183159932253, N'358afa25-5884-46c8-b2bb-c9543e1a407f', '1', '2574c71d-2ae5-425f-bf08-00a239314a8e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A009',Price=0.171183159932253,ManufacturerGuid=N'358afa25-5884-46c8-b2bb-c9543e1a407f',IsMasterData='1' WHERE Guid = '2574c71d-2ae5-425f-bf08-00a239314a8e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd62dc966-3683-41c2-88a0-f88a646f7e10')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd62dc966-3683-41c2-88a0-f88a646f7e10')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '950c6da8-2959-48ef-9f86-36070bf0ec45')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A018', 0.545245584321316, N'd62dc966-3683-41c2-88a0-f88a646f7e10', '1', '950c6da8-2959-48ef-9f86-36070bf0ec45')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A018',Price=0.545245584321316,ManufacturerGuid=N'd62dc966-3683-41c2-88a0-f88a646f7e10',IsMasterData='1' WHERE Guid = '950c6da8-2959-48ef-9f86-36070bf0ec45'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'87ed4346-cef5-4bb7-980e-bf93a9d0bf9d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'87ed4346-cef5-4bb7-980e-bf93a9d0bf9d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e0839862-f765-4864-a210-5e540443a05a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A022', 0.476046455359303, N'87ed4346-cef5-4bb7-980e-bf93a9d0bf9d', '1', 'e0839862-f765-4864-a210-5e540443a05a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A022',Price=0.476046455359303,ManufacturerGuid=N'87ed4346-cef5-4bb7-980e-bf93a9d0bf9d',IsMasterData='1' WHERE Guid = 'e0839862-f765-4864-a210-5e540443a05a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bb2f8c2a-2fdf-4221-ac59-9602af68d7a3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bb2f8c2a-2fdf-4221-ac59-9602af68d7a3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f24e9e77-2de2-46c1-b8d2-71cf2268be11')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A207', 0.362932494556013, N'bb2f8c2a-2fdf-4221-ac59-9602af68d7a3', '1', 'f24e9e77-2de2-46c1-b8d2-71cf2268be11')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A207',Price=0.362932494556013,ManufacturerGuid=N'bb2f8c2a-2fdf-4221-ac59-9602af68d7a3',IsMasterData='1' WHERE Guid = 'f24e9e77-2de2-46c1-b8d2-71cf2268be11'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bb52abe2-14e1-41a2-95a1-8fb4ce6e9c3e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bb52abe2-14e1-41a2-95a1-8fb4ce6e9c3e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c35ad1ba-8c72-4809-9500-a6a4f5816248')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A208', 0.229857246552141, N'bb52abe2-14e1-41a2-95a1-8fb4ce6e9c3e', '1', 'c35ad1ba-8c72-4809-9500-a6a4f5816248')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A208',Price=0.229857246552141,ManufacturerGuid=N'bb52abe2-14e1-41a2-95a1-8fb4ce6e9c3e',IsMasterData='1' WHERE Guid = 'c35ad1ba-8c72-4809-9500-a6a4f5816248'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cfe1b285-74eb-409e-aa3e-9371508a800d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cfe1b285-74eb-409e-aa3e-9371508a800d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '73eef61d-48c6-4cfc-89d3-1de08cdd6493')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487A401', 0.0520203242196951, N'cfe1b285-74eb-409e-aa3e-9371508a800d', '1', '73eef61d-48c6-4cfc-89d3-1de08cdd6493')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487A401',Price=0.0520203242196951,ManufacturerGuid=N'cfe1b285-74eb-409e-aa3e-9371508a800d',IsMasterData='1' WHERE Guid = '73eef61d-48c6-4cfc-89d3-1de08cdd6493'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'acc7f0b6-7870-4c97-8918-7db27a07880b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'acc7f0b6-7870-4c97-8918-7db27a07880b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4286c3f5-284a-4ce1-91a1-51840ab702a3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2487Y005', 0.215460924268086, N'acc7f0b6-7870-4c97-8918-7db27a07880b', '1', '4286c3f5-284a-4ce1-91a1-51840ab702a3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2487Y005',Price=0.215460924268086,ManufacturerGuid=N'acc7f0b6-7870-4c97-8918-7db27a07880b',IsMasterData='1' WHERE Guid = '4286c3f5-284a-4ce1-91a1-51840ab702a3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f01cd767-8d07-4eb7-98d9-b2daed5b4413')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f01cd767-8d07-4eb7-98d9-b2daed5b4413')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7b496d22-85c2-4d5d-b676-f655999a6ca2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'2800A033', 2.09291071860634, N'f01cd767-8d07-4eb7-98d9-b2daed5b4413', '1', '7b496d22-85c2-4d5d-b676-f655999a6ca2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'2800A033',Price=2.09291071860634,ManufacturerGuid=N'f01cd767-8d07-4eb7-98d9-b2daed5b4413',IsMasterData='1' WHERE Guid = '7b496d22-85c2-4d5d-b676-f655999a6ca2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a060292b-6178-43a2-811c-34c6be1adae5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a060292b-6178-43a2-811c-34c6be1adae5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '83f4cada-d9c3-4784-b394-0ebdf7deac22')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3211A005', 0.119283813210743, N'a060292b-6178-43a2-811c-34c6be1adae5', '1', '83f4cada-d9c3-4784-b394-0ebdf7deac22')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3211A005',Price=0.119283813210743,ManufacturerGuid=N'a060292b-6178-43a2-811c-34c6be1adae5',IsMasterData='1' WHERE Guid = '83f4cada-d9c3-4784-b394-0ebdf7deac22'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9aac7933-067c-4e46-a3af-3d57953224f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9aac7933-067c-4e46-a3af-3d57953224f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5423aaf9-4fdc-4a51-b61d-0cd1f4892f98')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3211P001', 0.353012339704815, N'9aac7933-067c-4e46-a3af-3d57953224f5', '1', '5423aaf9-4fdc-4a51-b61d-0cd1f4892f98')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3211P001',Price=0.353012339704815,ManufacturerGuid=N'9aac7933-067c-4e46-a3af-3d57953224f5',IsMasterData='1' WHERE Guid = '5423aaf9-4fdc-4a51-b61d-0cd1f4892f98'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7653f01b-3bf1-42fd-a6a8-c044bf834e78')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7653f01b-3bf1-42fd-a6a8-c044bf834e78')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f0e4903d-03dc-4136-80a2-94cce86261f4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3211P002', 0.281030728284539, N'7653f01b-3bf1-42fd-a6a8-c044bf834e78', '1', 'f0e4903d-03dc-4136-80a2-94cce86261f4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3211P002',Price=0.281030728284539,ManufacturerGuid=N'7653f01b-3bf1-42fd-a6a8-c044bf834e78',IsMasterData='1' WHERE Guid = 'f0e4903d-03dc-4136-80a2-94cce86261f4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6d6ecd79-f10e-431a-a236-287c5780e6b1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6d6ecd79-f10e-431a-a236-287c5780e6b1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4500e942-f082-44d1-bbf0-17ab6ce99441')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3212P002', 0.25792402613114, N'6d6ecd79-f10e-431a-a236-287c5780e6b1', '1', '4500e942-f082-44d1-bbf0-17ab6ce99441')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3212P002',Price=0.25792402613114,ManufacturerGuid=N'6d6ecd79-f10e-431a-a236-287c5780e6b1',IsMasterData='1' WHERE Guid = '4500e942-f082-44d1-bbf0-17ab6ce99441'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6f1fbed6-c625-4b5e-b9ec-5453e498febd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6f1fbed6-c625-4b5e-b9ec-5453e498febd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b49b5f41-75a5-4b18-ab30-e0391b46a354')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3212P008', 0.637914347931285, N'6f1fbed6-c625-4b5e-b9ec-5453e498febd', '1', 'b49b5f41-75a5-4b18-ab30-e0391b46a354')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3212P008',Price=0.637914347931285,ManufacturerGuid=N'6f1fbed6-c625-4b5e-b9ec-5453e498febd',IsMasterData='1' WHERE Guid = 'b49b5f41-75a5-4b18-ab30-e0391b46a354'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bbb823e5-cb77-4b28-b70e-d0009b7c6b99')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bbb823e5-cb77-4b28-b70e-d0009b7c6b99')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd6d30d93-8a7c-4010-bff4-69e20570f045')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3212V004', 0.447616743285749, N'bbb823e5-cb77-4b28-b70e-d0009b7c6b99', '1', 'd6d30d93-8a7c-4010-bff4-69e20570f045')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3212V004',Price=0.447616743285749,ManufacturerGuid=N'bbb823e5-cb77-4b28-b70e-d0009b7c6b99',IsMasterData='1' WHERE Guid = 'd6d30d93-8a7c-4010-bff4-69e20570f045'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c6b6ffc0-895d-4266-8823-c1a8296c3141')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c6b6ffc0-895d-4266-8823-c1a8296c3141')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bba4eceb-03b1-4dbf-890d-0ebc6716f822')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3212V005', 0.303653520445197, N'c6b6ffc0-895d-4266-8823-c1a8296c3141', '1', 'bba4eceb-03b1-4dbf-890d-0ebc6716f822')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3212V005',Price=0.303653520445197,ManufacturerGuid=N'c6b6ffc0-895d-4266-8823-c1a8296c3141',IsMasterData='1' WHERE Guid = 'bba4eceb-03b1-4dbf-890d-0ebc6716f822'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'98c25ce4-d574-4b82-821f-ee56793da95a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'98c25ce4-d574-4b82-821f-ee56793da95a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '87fb60bf-ec59-49e9-84c6-df8e09c8e09d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3212V006', 0.312726832809098, N'98c25ce4-d574-4b82-821f-ee56793da95a', '1', '87fb60bf-ec59-49e9-84c6-df8e09c8e09d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3212V006',Price=0.312726832809098,ManufacturerGuid=N'98c25ce4-d574-4b82-821f-ee56793da95a',IsMasterData='1' WHERE Guid = '87fb60bf-ec59-49e9-84c6-df8e09c8e09d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'16f5b798-9d01-4687-8fc3-43260e49f3ff')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'16f5b798-9d01-4687-8fc3-43260e49f3ff')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3a5c554-0027-4490-a985-8b67773808bc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32141316', 0.203242196951367, N'16f5b798-9d01-4687-8fc3-43260e49f3ff', '1', 'c3a5c554-0027-4490-a985-8b67773808bc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32141316',Price=0.203242196951367,ManufacturerGuid=N'16f5b798-9d01-4687-8fc3-43260e49f3ff',IsMasterData='1' WHERE Guid = 'c3a5c554-0027-4490-a985-8b67773808bc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'93d8b44e-358d-4f11-a477-9b005847a20e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'93d8b44e-358d-4f11-a477-9b005847a20e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c1f683d9-8c49-4573-9f74-bb5466e5ccb7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3215A004', 0.183885797241713, N'93d8b44e-358d-4f11-a477-9b005847a20e', '1', 'c1f683d9-8c49-4573-9f74-bb5466e5ccb7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3215A004',Price=0.183885797241713,ManufacturerGuid=N'93d8b44e-358d-4f11-a477-9b005847a20e',IsMasterData='1' WHERE Guid = 'c1f683d9-8c49-4573-9f74-bb5466e5ccb7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30adeaa6-2b77-4f26-946e-cc485b4dd606')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30adeaa6-2b77-4f26-946e-cc485b4dd606')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3768acd-daa2-42bf-a9ed-6c27ade5feed')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3215A005', 0.242801838857972, N'30adeaa6-2b77-4f26-946e-cc485b4dd606', '1', 'd3768acd-daa2-42bf-a9ed-6c27ade5feed')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3215A005',Price=0.242801838857972,ManufacturerGuid=N'30adeaa6-2b77-4f26-946e-cc485b4dd606',IsMasterData='1' WHERE Guid = 'd3768acd-daa2-42bf-a9ed-6c27ade5feed'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'43b3d39a-c91a-4ee5-9822-930d6e93603f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'43b3d39a-c91a-4ee5-9822-930d6e93603f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd1f9c9bb-049f-4a13-8857-2ca1d197b60e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32161114', 0.304863295427051, N'43b3d39a-c91a-4ee5-9822-930d6e93603f', '1', 'd1f9c9bb-049f-4a13-8857-2ca1d197b60e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32161114',Price=0.304863295427051,ManufacturerGuid=N'43b3d39a-c91a-4ee5-9822-930d6e93603f',IsMasterData='1' WHERE Guid = 'd1f9c9bb-049f-4a13-8857-2ca1d197b60e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ab15b783-8cf1-405e-a33c-feb17cf78045')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ab15b783-8cf1-405e-a33c-feb17cf78045')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eab92713-e9b8-478e-b8cb-cc1068294788')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32168212', 0.275344785869828, N'ab15b783-8cf1-405e-a33c-feb17cf78045', '1', 'eab92713-e9b8-478e-b8cb-cc1068294788')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32168212',Price=0.275344785869828,ManufacturerGuid=N'ab15b783-8cf1-405e-a33c-feb17cf78045',IsMasterData='1' WHERE Guid = 'eab92713-e9b8-478e-b8cb-cc1068294788'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9ec6e916-8d45-4285-bb12-60422f90ea45')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9ec6e916-8d45-4285-bb12-60422f90ea45')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ba30a3e9-cea2-4633-b569-0af474834ca0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32184226', 0.130897653036535, N'9ec6e916-8d45-4285-bb12-60422f90ea45', '1', 'ba30a3e9-cea2-4633-b569-0af474834ca0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32184226',Price=0.130897653036535,ManufacturerGuid=N'9ec6e916-8d45-4285-bb12-60422f90ea45',IsMasterData='1' WHERE Guid = 'ba30a3e9-cea2-4633-b569-0af474834ca0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cb77dae7-a5ac-48ea-a86e-6201ece5e3fb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cb77dae7-a5ac-48ea-a86e-6201ece5e3fb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0af09c43-9cfa-4625-a28a-b85842fd5272')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32186405', 0.26699733849504, N'cb77dae7-a5ac-48ea-a86e-6201ece5e3fb', '1', '0af09c43-9cfa-4625-a28a-b85842fd5272')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32186405',Price=0.26699733849504,ManufacturerGuid=N'cb77dae7-a5ac-48ea-a86e-6201ece5e3fb',IsMasterData='1' WHERE Guid = '0af09c43-9cfa-4625-a28a-b85842fd5272'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'71f469af-b7e5-4c55-b601-950d31d76bfc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'71f469af-b7e5-4c55-b601-950d31d76bfc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ce43c94d-7b46-441c-bb6c-adc357da55b9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32416117', 0.152431647713525, N'71f469af-b7e5-4c55-b601-950d31d76bfc', '1', 'ce43c94d-7b46-441c-bb6c-adc357da55b9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32416117',Price=0.152431647713525,ManufacturerGuid=N'71f469af-b7e5-4c55-b601-950d31d76bfc',IsMasterData='1' WHERE Guid = 'ce43c94d-7b46-441c-bb6c-adc357da55b9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f8acde78-b056-4cfb-a9e6-5a41e0b082f0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f8acde78-b056-4cfb-a9e6-5a41e0b082f0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfb0b8e5-da12-40a7-861b-373a8dbb2013')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32416118', 0.0782724413259134, N'f8acde78-b056-4cfb-a9e6-5a41e0b082f0', '1', 'bfb0b8e5-da12-40a7-861b-373a8dbb2013')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32416118',Price=0.0782724413259134,ManufacturerGuid=N'f8acde78-b056-4cfb-a9e6-5a41e0b082f0',IsMasterData='1' WHERE Guid = 'bfb0b8e5-da12-40a7-861b-373a8dbb2013'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'40c6f3a9-9256-4afb-a62b-a53fb07da978')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'40c6f3a9-9256-4afb-a62b-a53fb07da978')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6fe99246-e5ff-4def-9567-1d03f59c593c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32416119', 0.164529397532059, N'40c6f3a9-9256-4afb-a62b-a53fb07da978', '1', '6fe99246-e5ff-4def-9567-1d03f59c593c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32416119',Price=0.164529397532059,ManufacturerGuid=N'40c6f3a9-9256-4afb-a62b-a53fb07da978',IsMasterData='1' WHERE Guid = '6fe99246-e5ff-4def-9567-1d03f59c593c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'68016da9-fcfc-406c-8d27-323bd1691fb7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'68016da9-fcfc-406c-8d27-323bd1691fb7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a4a32a96-14c7-4a61-aac3-4b169f73261d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32417117', 0.0525042342124365, N'68016da9-fcfc-406c-8d27-323bd1691fb7', '1', 'a4a32a96-14c7-4a61-aac3-4b169f73261d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32417117',Price=0.0525042342124365,ManufacturerGuid=N'68016da9-fcfc-406c-8d27-323bd1691fb7',IsMasterData='1' WHERE Guid = 'a4a32a96-14c7-4a61-aac3-4b169f73261d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9ed89099-2d11-4647-89f2-08bc4d86c6fc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9ed89099-2d11-4647-89f2-08bc4d86c6fc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ab058672-e9f2-470f-a5fb-d630572bf296')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32417134', 0.0704089039438664, N'9ed89099-2d11-4647-89f2-08bc4d86c6fc', '1', 'ab058672-e9f2-470f-a5fb-d630572bf296')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32417134',Price=0.0704089039438664,ManufacturerGuid=N'9ed89099-2d11-4647-89f2-08bc4d86c6fc',IsMasterData='1' WHERE Guid = 'ab058672-e9f2-470f-a5fb-d630572bf296'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b5ec3d32-4cbf-4db8-ab78-04c4e89d7b34')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b5ec3d32-4cbf-4db8-ab78-04c4e89d7b34')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f7044e95-7ed8-453e-ad24-f84b0d6402ef')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32418122', 0.12533268812001, N'b5ec3d32-4cbf-4db8-ab78-04c4e89d7b34', '1', 'f7044e95-7ed8-453e-ad24-f84b0d6402ef')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32418122',Price=0.12533268812001,ManufacturerGuid=N'b5ec3d32-4cbf-4db8-ab78-04c4e89d7b34',IsMasterData='1' WHERE Guid = 'f7044e95-7ed8-453e-ad24-f84b0d6402ef'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'63a5a049-31f3-4bd5-a9e6-873def40fad0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'63a5a049-31f3-4bd5-a9e6-873def40fad0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3e73f10-47b9-4bb9-839a-722f7491c5b5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3241A011', 0.0691991289620131, N'63a5a049-31f3-4bd5-a9e6-873def40fad0', '1', 'd3e73f10-47b9-4bb9-839a-722f7491c5b5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3241A011',Price=0.0691991289620131,ManufacturerGuid=N'63a5a049-31f3-4bd5-a9e6-873def40fad0',IsMasterData='1' WHERE Guid = 'd3e73f10-47b9-4bb9-839a-722f7491c5b5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cf4c479f-5a94-4c18-a6a3-2bf225e5dd62')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cf4c479f-5a94-4c18-a6a3-2bf225e5dd62')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '67b28126-8c37-4237-89c8-9985160cc6df')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3241A012', 0.166706992499395, N'cf4c479f-5a94-4c18-a6a3-2bf225e5dd62', '1', '67b28126-8c37-4237-89c8-9985160cc6df')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3241A012',Price=0.166706992499395,ManufacturerGuid=N'cf4c479f-5a94-4c18-a6a3-2bf225e5dd62',IsMasterData='1' WHERE Guid = '67b28126-8c37-4237-89c8-9985160cc6df'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'973c7257-52ce-4bdf-a341-b6f0691497f7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'973c7257-52ce-4bdf-a341-b6f0691497f7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '526760c0-a1e5-43c9-8203-6456cef40dca')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3272A032', 2.96394870554077, N'973c7257-52ce-4bdf-a341-b6f0691497f7', '1', '526760c0-a1e5-43c9-8203-6456cef40dca')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3272A032',Price=2.96394870554077,ManufacturerGuid=N'973c7257-52ce-4bdf-a341-b6f0691497f7',IsMasterData='1' WHERE Guid = '526760c0-a1e5-43c9-8203-6456cef40dca'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ff37c46a-17f9-489b-9404-a593dc6e17c5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ff37c46a-17f9-489b-9404-a593dc6e17c5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfe45e45-164e-4335-bd6d-f5c7952fd179')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A003', 0.0851681587224776, N'ff37c46a-17f9-489b-9404-a593dc6e17c5', '1', 'bfe45e45-164e-4335-bd6d-f5c7952fd179')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A003',Price=0.0851681587224776,ManufacturerGuid=N'ff37c46a-17f9-489b-9404-a593dc6e17c5',IsMasterData='1' WHERE Guid = 'bfe45e45-164e-4335-bd6d-f5c7952fd179'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'78fec88c-7b06-4492-8c5f-bf63c50db90a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'78fec88c-7b06-4492-8c5f-bf63c50db90a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1c43d6d5-7f23-41e8-b596-37f9b9b833b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A004', 0.122429228163562, N'78fec88c-7b06-4492-8c5f-bf63c50db90a', '1', '1c43d6d5-7f23-41e8-b596-37f9b9b833b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A004',Price=0.122429228163562,ManufacturerGuid=N'78fec88c-7b06-4492-8c5f-bf63c50db90a',IsMasterData='1' WHERE Guid = '1c43d6d5-7f23-41e8-b596-37f9b9b833b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1d71cb48-553a-4b3a-8c6b-698663e1d937')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1d71cb48-553a-4b3a-8c6b-698663e1d937')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c17b02f-2d31-4936-a16b-472da0c17900')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A005', 0.188361964674571, N'1d71cb48-553a-4b3a-8c6b-698663e1d937', '1', '8c17b02f-2d31-4936-a16b-472da0c17900')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A005',Price=0.188361964674571,ManufacturerGuid=N'1d71cb48-553a-4b3a-8c6b-698663e1d937',IsMasterData='1' WHERE Guid = '8c17b02f-2d31-4936-a16b-472da0c17900'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'13b25fbb-7d46-4d26-a502-fdc30f05e67e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'13b25fbb-7d46-4d26-a502-fdc30f05e67e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '028824d6-9ae4-47ec-aa10-000a8df44f5f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A006', 0.296394870554077, N'13b25fbb-7d46-4d26-a502-fdc30f05e67e', '1', '028824d6-9ae4-47ec-aa10-000a8df44f5f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A006',Price=0.296394870554077,ManufacturerGuid=N'13b25fbb-7d46-4d26-a502-fdc30f05e67e',IsMasterData='1' WHERE Guid = '028824d6-9ae4-47ec-aa10-000a8df44f5f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ab584bf7-893c-4373-89cf-75f67a7fe3bb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ab584bf7-893c-4373-89cf-75f67a7fe3bb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2914ef41-b637-43b2-9107-1a960e3bf2f3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A007', 0.0936365835954512, N'ab584bf7-893c-4373-89cf-75f67a7fe3bb', '1', '2914ef41-b637-43b2-9107-1a960e3bf2f3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A007',Price=0.0936365835954512,ManufacturerGuid=N'ab584bf7-893c-4373-89cf-75f67a7fe3bb',IsMasterData='1' WHERE Guid = '2914ef41-b637-43b2-9107-1a960e3bf2f3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'989e8942-d538-4ebc-9683-740d6c04d9f0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'989e8942-d538-4ebc-9683-740d6c04d9f0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fc6f138b-1947-4dc0-9f67-cefd6fafb605')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3278A101', 0.375030244374546, N'989e8942-d538-4ebc-9683-740d6c04d9f0', '1', 'fc6f138b-1947-4dc0-9f67-cefd6fafb605')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3278A101',Price=0.375030244374546,ManufacturerGuid=N'989e8942-d538-4ebc-9683-740d6c04d9f0',IsMasterData='1' WHERE Guid = 'fc6f138b-1947-4dc0-9f67-cefd6fafb605'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7d0b5eda-4e96-4ed2-9480-4237238c42f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7d0b5eda-4e96-4ed2-9480-4237238c42f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c533dd93-1ee6-440f-8fb8-16395b14570e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3282A003', 4.08903943866441, N'7d0b5eda-4e96-4ed2-9480-4237238c42f5', '1', 'c533dd93-1ee6-440f-8fb8-16395b14570e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3282A003',Price=4.08903943866441,ManufacturerGuid=N'7d0b5eda-4e96-4ed2-9480-4237238c42f5',IsMasterData='1' WHERE Guid = 'c533dd93-1ee6-440f-8fb8-16395b14570e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3d58d577-b381-4bf1-be42-36e02f52f870')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3d58d577-b381-4bf1-be42-36e02f52f870')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0d479a55-e424-4781-9904-1a8eb04ea739')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'33141124', 0.280667795789983, N'3d58d577-b381-4bf1-be42-36e02f52f870', '1', '0d479a55-e424-4781-9904-1a8eb04ea739')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'33141124',Price=0.280667795789983,ManufacturerGuid=N'3d58d577-b381-4bf1-be42-36e02f52f870',IsMasterData='1' WHERE Guid = '0d479a55-e424-4781-9904-1a8eb04ea739'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'085e0faf-1074-4826-b651-3a25e112f788')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'085e0faf-1074-4826-b651-3a25e112f788')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '795bbd3f-570d-4d89-9a8e-74d7c242c366')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'33147124', 0.920396806194048, N'085e0faf-1074-4826-b651-3a25e112f788', '1', '795bbd3f-570d-4d89-9a8e-74d7c242c366')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'33147124',Price=0.920396806194048,ManufacturerGuid=N'085e0faf-1074-4826-b651-3a25e112f788',IsMasterData='1' WHERE Guid = '795bbd3f-570d-4d89-9a8e-74d7c242c366'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'59fc7f6e-43a2-4f82-b2af-401037543b2d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'59fc7f6e-43a2-4f82-b2af-401037543b2d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cd31b07d-9e19-416d-8dbc-fe14d7868359')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3314A103', 0.107186063392209, N'59fc7f6e-43a2-4f82-b2af-401037543b2d', '1', 'cd31b07d-9e19-416d-8dbc-fe14d7868359')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3314A103',Price=0.107186063392209,ManufacturerGuid=N'59fc7f6e-43a2-4f82-b2af-401037543b2d',IsMasterData='1' WHERE Guid = 'cd31b07d-9e19-416d-8dbc-fe14d7868359'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'50c235b2-0c4b-4860-a5d6-64cdfa7cb8d5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'50c235b2-0c4b-4860-a5d6-64cdfa7cb8d5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f504e3da-62cc-4a03-b83e-a018a751a66b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'33157142', 0.0431889668521655, N'50c235b2-0c4b-4860-a5d6-64cdfa7cb8d5', '1', 'f504e3da-62cc-4a03-b83e-a018a751a66b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'33157142',Price=0.0431889668521655,ManufacturerGuid=N'50c235b2-0c4b-4860-a5d6-64cdfa7cb8d5',IsMasterData='1' WHERE Guid = 'f504e3da-62cc-4a03-b83e-a018a751a66b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5d6b6523-3137-4723-af42-5f24d8d5519a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5d6b6523-3137-4723-af42-5f24d8d5519a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '505a873b-84f6-4ce6-8cea-145f1fd649b4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'33157143', 0.0431889668521655, N'5d6b6523-3137-4723-af42-5f24d8d5519a', '1', '505a873b-84f6-4ce6-8cea-145f1fd649b4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'33157143',Price=0.0431889668521655,ManufacturerGuid=N'5d6b6523-3137-4723-af42-5f24d8d5519a',IsMasterData='1' WHERE Guid = '505a873b-84f6-4ce6-8cea-145f1fd649b4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ec0593d4-9957-421a-80a1-4fa373db9402')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ec0593d4-9957-421a-80a1-4fa373db9402')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6aee9a68-9be5-497e-916e-c845690972b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3746B001', 0.0168158722477619, N'ec0593d4-9957-421a-80a1-4fa373db9402', '1', '6aee9a68-9be5-497e-916e-c845690972b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3746B001',Price=0.0168158722477619,ManufacturerGuid=N'ec0593d4-9957-421a-80a1-4fa373db9402',IsMasterData='1' WHERE Guid = '6aee9a68-9be5-497e-916e-c845690972b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2f9364b4-3414-459d-b92c-da1c859c2ea3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2f9364b4-3414-459d-b92c-da1c859c2ea3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7923e96f-6230-4889-bb04-40445f0ab284')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3746B004', 0.0195983547060247, N'2f9364b4-3414-459d-b92c-da1c859c2ea3', '1', '7923e96f-6230-4889-bb04-40445f0ab284')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3746B004',Price=0.0195983547060247,ManufacturerGuid=N'2f9364b4-3414-459d-b92c-da1c859c2ea3',IsMasterData='1' WHERE Guid = '7923e96f-6230-4889-bb04-40445f0ab284'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'eb2208e6-8d24-4c17-832c-6745d16bedc0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'eb2208e6-8d24-4c17-832c-6745d16bedc0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '696b9ecd-9242-4493-9fa6-ba156ebfa8b6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3746B007', 0.0572223566416647, N'eb2208e6-8d24-4c17-832c-6745d16bedc0', '1', '696b9ecd-9242-4493-9fa6-ba156ebfa8b6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3746B007',Price=0.0572223566416647,ManufacturerGuid=N'eb2208e6-8d24-4c17-832c-6745d16bedc0',IsMasterData='1' WHERE Guid = '696b9ecd-9242-4493-9fa6-ba156ebfa8b6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'54d89075-1cb3-480a-8a8c-e845a9c17c67')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'54d89075-1cb3-480a-8a8c-e845a9c17c67')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '798aeb8b-6b98-4aef-99a9-6a2e3ed12c38')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3746B008', 0.0734333413984999, N'54d89075-1cb3-480a-8a8c-e845a9c17c67', '1', '798aeb8b-6b98-4aef-99a9-6a2e3ed12c38')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3746B008',Price=0.0734333413984999,ManufacturerGuid=N'54d89075-1cb3-480a-8a8c-e845a9c17c67',IsMasterData='1' WHERE Guid = '798aeb8b-6b98-4aef-99a9-6a2e3ed12c38'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a79d365b-b45c-43eb-9985-92b0df39fc2d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a79d365b-b45c-43eb-9985-92b0df39fc2d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cb7ae911-25e6-4c24-b0e7-819c142a39ff')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'37813113', 0.168642632470361, N'a79d365b-b45c-43eb-9985-92b0df39fc2d', '1', 'cb7ae911-25e6-4c24-b0e7-819c142a39ff')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'37813113',Price=0.168642632470361,ManufacturerGuid=N'a79d365b-b45c-43eb-9985-92b0df39fc2d',IsMasterData='1' WHERE Guid = 'cb7ae911-25e6-4c24-b0e7-819c142a39ff'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ff44bc57-4f08-4a48-b24f-12aac13e52d6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ff44bc57-4f08-4a48-b24f-12aac13e52d6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2a64f18a-66d2-408c-b3d3-4aac5fdc99ce')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'4147V005', 0.713767239293491, N'ff44bc57-4f08-4a48-b24f-12aac13e52d6', '1', '2a64f18a-66d2-408c-b3d3-4aac5fdc99ce')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'4147V005',Price=0.713767239293491,ManufacturerGuid=N'ff44bc57-4f08-4a48-b24f-12aac13e52d6',IsMasterData='1' WHERE Guid = '2a64f18a-66d2-408c-b3d3-4aac5fdc99ce'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bb72acb7-4ce8-409c-bccc-3f1ba79ba165')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bb72acb7-4ce8-409c-bccc-3f1ba79ba165')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '88573d18-4b48-4bc9-996b-45e5974f0604')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'4147V011', 0.881079119283813, N'bb72acb7-4ce8-409c-bccc-3f1ba79ba165', '1', '88573d18-4b48-4bc9-996b-45e5974f0604')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'4147V011',Price=0.881079119283813,ManufacturerGuid=N'bb72acb7-4ce8-409c-bccc-3f1ba79ba165',IsMasterData='1' WHERE Guid = '88573d18-4b48-4bc9-996b-45e5974f0604'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bfd661c5-70eb-41bd-9aed-357cd30d1d21')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bfd661c5-70eb-41bd-9aed-357cd30d1d21')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '43f3be51-f9ab-4034-a059-f96249849c60')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'5W5009', 0.169368497459473, N'bfd661c5-70eb-41bd-9aed-357cd30d1d21', '1', '43f3be51-f9ab-4034-a059-f96249849c60')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'5W5009',Price=0.169368497459473,ManufacturerGuid=N'bfd661c5-70eb-41bd-9aed-357cd30d1d21',IsMasterData='1' WHERE Guid = '43f3be51-f9ab-4034-a059-f96249849c60'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'782f0ceb-f0c3-4361-b48e-989aaf9f2865')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'782f0ceb-f0c3-4361-b48e-989aaf9f2865')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ef8940e9-8eb3-4872-8a8c-055fe3ce58a3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3361280', 0.846842487297363, N'782f0ceb-f0c3-4361-b48e-989aaf9f2865', '1', 'ef8940e9-8eb3-4872-8a8c-055fe3ce58a3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3361280',Price=0.846842487297363,ManufacturerGuid=N'782f0ceb-f0c3-4361-b48e-989aaf9f2865',IsMasterData='1' WHERE Guid = 'ef8940e9-8eb3-4872-8a8c-055fe3ce58a3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'73504b1b-d8e2-4e1c-bf04-bc752cb00c05')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'73504b1b-d8e2-4e1c-bf04-bc752cb00c05')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6a6a4d6e-3944-41c6-8014-06036d49c426')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3377168', 1.08879748366804, N'73504b1b-d8e2-4e1c-bf04-bc752cb00c05', '1', '6a6a4d6e-3944-41c6-8014-06036d49c426')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3377168',Price=1.08879748366804,ManufacturerGuid=N'73504b1b-d8e2-4e1c-bf04-bc752cb00c05',IsMasterData='1' WHERE Guid = '6a6a4d6e-3944-41c6-8014-06036d49c426'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'964f8ce1-3f76-45cc-b468-296fc5e2a139')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'964f8ce1-3f76-45cc-b468-296fc5e2a139')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '68836187-65c9-486d-8f69-b0af8018052a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'3548322', 0.221388821679168, N'964f8ce1-3f76-45cc-b468-296fc5e2a139', '1', '68836187-65c9-486d-8f69-b0af8018052a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'3548322',Price=0.221388821679168,ManufacturerGuid=N'964f8ce1-3f76-45cc-b468-296fc5e2a139',IsMasterData='1' WHERE Guid = '68836187-65c9-486d-8f69-b0af8018052a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4e66e3ea-0fae-46e7-9ed4-a8fe5a417e5b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4e66e3ea-0fae-46e7-9ed4-a8fe5a417e5b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9294bc4d-2a73-4d35-acef-d7e4b6b34fbf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG', N'32815118', 0.443140575852891, N'4e66e3ea-0fae-46e7-9ed4-a8fe5a417e5b', '1', '9294bc4d-2a73-4d35-acef-d7e4b6b34fbf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG',PartNumber=N'32815118',Price=0.443140575852891,ManufacturerGuid=N'4e66e3ea-0fae-46e7-9ed4-a8fe5a417e5b',IsMasterData='1' WHERE Guid = '9294bc4d-2a73-4d35-acef-d7e4b6b34fbf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7a2cffbb-7805-464d-9169-9a297da24a2d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7a2cffbb-7805-464d-9169-9a297da24a2d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '605e16f9-f45f-4146-a1fa-47c90df4e67c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'PLUG-STOR', N'9S4185', 0.226227921606581, N'7a2cffbb-7805-464d-9169-9a297da24a2d', '1', '605e16f9-f45f-4146-a1fa-47c90df4e67c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'PLUG-STOR',PartNumber=N'9S4185',Price=0.226227921606581,ManufacturerGuid=N'7a2cffbb-7805-464d-9169-9a297da24a2d',IsMasterData='1' WHERE Guid = '605e16f9-f45f-4146-a1fa-47c90df4e67c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f2fe8955-123f-46bc-aee0-e6acdf5da263')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f2fe8955-123f-46bc-aee0-e6acdf5da263')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '71c749d1-4b02-4fdb-81f4-abd06bfede43')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'RIVET', N'2126151', 0.0198403097023954, N'f2fe8955-123f-46bc-aee0-e6acdf5da263', '1', '71c749d1-4b02-4fdb-81f4-abd06bfede43')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'RIVET',PartNumber=N'2126151',Price=0.0198403097023954,ManufacturerGuid=N'f2fe8955-123f-46bc-aee0-e6acdf5da263',IsMasterData='1' WHERE Guid = '71c749d1-4b02-4fdb-81f4-abd06bfede43'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1aac5a33-025f-468a-9a6b-757c03244bcd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1aac5a33-025f-468a-9a6b-757c03244bcd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2037aa6b-4b8b-4632-a71c-b0b9d69b5c9d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'RIVET', N'2127A001', 0.0228647471570288, N'1aac5a33-025f-468a-9a6b-757c03244bcd', '1', '2037aa6b-4b8b-4632-a71c-b0b9d69b5c9d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'RIVET',PartNumber=N'2127A001',Price=0.0228647471570288,ManufacturerGuid=N'1aac5a33-025f-468a-9a6b-757c03244bcd',IsMasterData='1' WHERE Guid = '2037aa6b-4b8b-4632-a71c-b0b9d69b5c9d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'44f30212-8808-4b45-a1f7-f1a80991e1bc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'44f30212-8808-4b45-a1f7-f1a80991e1bc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1026218c-4e99-4cb2-9c10-051e3741d1dc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'RIVET', N'2127A601', 0.112146140817808, N'44f30212-8808-4b45-a1f7-f1a80991e1bc', '1', '1026218c-4e99-4cb2-9c10-051e3741d1dc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'RIVET',PartNumber=N'2127A601',Price=0.112146140817808,ManufacturerGuid=N'44f30212-8808-4b45-a1f7-f1a80991e1bc',IsMasterData='1' WHERE Guid = '1026218c-4e99-4cb2-9c10-051e3741d1dc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'db9e69ff-6329-45dd-86c2-dc2e053a8645')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'db9e69ff-6329-45dd-86c2-dc2e053a8645')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eb998b2b-6cf0-419b-a54d-0ee75351df47')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A011', 0.593636583595451, N'db9e69ff-6329-45dd-86c2-dc2e053a8645', '1', 'eb998b2b-6cf0-419b-a54d-0ee75351df47')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A011',Price=0.593636583595451,ManufacturerGuid=N'db9e69ff-6329-45dd-86c2-dc2e053a8645',IsMasterData='1' WHERE Guid = 'eb998b2b-6cf0-419b-a54d-0ee75351df47'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'811a369f-85bd-45b6-90a0-933a4dddc760')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'811a369f-85bd-45b6-90a0-933a4dddc760')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3ad67fa6-aa65-4716-9904-1aa453716d30')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A012', 0.685579482216308, N'811a369f-85bd-45b6-90a0-933a4dddc760', '1', '3ad67fa6-aa65-4716-9904-1aa453716d30')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A012',Price=0.685579482216308,ManufacturerGuid=N'811a369f-85bd-45b6-90a0-933a4dddc760',IsMasterData='1' WHERE Guid = '3ad67fa6-aa65-4716-9904-1aa453716d30'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5a4a65af-8bf1-4a94-8f78-9bbcb3260e17')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5a4a65af-8bf1-4a94-8f78-9bbcb3260e17')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ab4c72e1-72c8-41cd-8a94-5699a8955d04')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096033', 0.0162109847568352, N'5a4a65af-8bf1-4a94-8f78-9bbcb3260e17', '1', 'ab4c72e1-72c8-41cd-8a94-5699a8955d04')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096033',Price=0.0162109847568352,ManufacturerGuid=N'5a4a65af-8bf1-4a94-8f78-9bbcb3260e17',IsMasterData='1' WHERE Guid = 'ab4c72e1-72c8-41cd-8a94-5699a8955d04'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8579712d-6b7b-44a2-a3b7-e32ac5a253d4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8579712d-6b7b-44a2-a3b7-e32ac5a253d4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b556c860-aa0f-4ab5-a05d-0cee590a7c28')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096234', 0.0360512944592306, N'8579712d-6b7b-44a2-a3b7-e32ac5a253d4', '1', 'b556c860-aa0f-4ab5-a05d-0cee590a7c28')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096234',Price=0.0360512944592306,ManufacturerGuid=N'8579712d-6b7b-44a2-a3b7-e32ac5a253d4',IsMasterData='1' WHERE Guid = 'b556c860-aa0f-4ab5-a05d-0cee590a7c28'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'df052775-493d-4117-ab39-0874997926f5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'df052775-493d-4117-ab39-0874997926f5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6c61d885-c762-4164-af78-2bb56079bfb5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096803', 0.166706992499395, N'df052775-493d-4117-ab39-0874997926f5', '1', '6c61d885-c762-4164-af78-2bb56079bfb5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096803',Price=0.166706992499395,ManufacturerGuid=N'df052775-493d-4117-ab39-0874997926f5',IsMasterData='1' WHERE Guid = '6c61d885-c762-4164-af78-2bb56079bfb5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61726a00-da40-4d9d-8705-91e4652292f2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61726a00-da40-4d9d-8705-91e4652292f2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dad4b2b5-050d-4550-8dcb-27f66af8e2ed')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096833', 0.148802322767965, N'61726a00-da40-4d9d-8705-91e4652292f2', '1', 'dad4b2b5-050d-4550-8dcb-27f66af8e2ed')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096833',Price=0.148802322767965,ManufacturerGuid=N'61726a00-da40-4d9d-8705-91e4652292f2',IsMasterData='1' WHERE Guid = 'dad4b2b5-050d-4550-8dcb-27f66af8e2ed'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c37f413f-14d6-4f5d-bd06-70f149d3ff62')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c37f413f-14d6-4f5d-bd06-70f149d3ff62')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3f21a28-5d92-429b-99ed-3c084ef055ec')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096835', 0.126663440600048, N'c37f413f-14d6-4f5d-bd06-70f149d3ff62', '1', 'd3f21a28-5d92-429b-99ed-3c084ef055ec')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096835',Price=0.126663440600048,ManufacturerGuid=N'c37f413f-14d6-4f5d-bd06-70f149d3ff62',IsMasterData='1' WHERE Guid = 'd3f21a28-5d92-429b-99ed-3c084ef055ec'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fcccc0b3-7251-4aac-a9e3-198c0a3a8833')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fcccc0b3-7251-4aac-a9e3-198c0a3a8833')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3b81b3fe-4390-4fcc-bb78-b7e339449d13')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0096838', 0.103072828453908, N'fcccc0b3-7251-4aac-a9e3-198c0a3a8833', '1', '3b81b3fe-4390-4fcc-bb78-b7e339449d13')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0096838',Price=0.103072828453908,ManufacturerGuid=N'fcccc0b3-7251-4aac-a9e3-198c0a3a8833',IsMasterData='1' WHERE Guid = '3b81b3fe-4390-4fcc-bb78-b7e339449d13'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e6e447ba-afd4-4caf-bf82-fbb0a9d3327f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e6e447ba-afd4-4caf-bf82-fbb0a9d3327f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7eb86321-2d9a-4f4b-a44a-0a5aa8691946')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0726504', 0.0359303169610452, N'e6e447ba-afd4-4caf-bf82-fbb0a9d3327f', '1', '7eb86321-2d9a-4f4b-a44a-0a5aa8691946')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0726504',Price=0.0359303169610452,ManufacturerGuid=N'e6e447ba-afd4-4caf-bf82-fbb0a9d3327f',IsMasterData='1' WHERE Guid = '7eb86321-2d9a-4f4b-a44a-0a5aa8691946'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9dc0ee95-b509-4040-b427-8f23cfe976b0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9dc0ee95-b509-4040-b427-8f23cfe976b0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cc2af8dd-635a-49d2-9acd-2c4e3c7bad13')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746259', 0.0210500846842487, N'9dc0ee95-b509-4040-b427-8f23cfe976b0', '1', 'cc2af8dd-635a-49d2-9acd-2c4e3c7bad13')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746259',Price=0.0210500846842487,ManufacturerGuid=N'9dc0ee95-b509-4040-b427-8f23cfe976b0',IsMasterData='1' WHERE Guid = 'cc2af8dd-635a-49d2-9acd-2c4e3c7bad13'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1ea5d936-3329-40e0-8858-d145ab44d274')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1ea5d936-3329-40e0-8858-d145ab44d274')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8ad00d03-eb26-4e6d-a59e-cd7a9b3209f5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746355', 0.0278248245826276, N'1ea5d936-3329-40e0-8858-d145ab44d274', '1', '8ad00d03-eb26-4e6d-a59e-cd7a9b3209f5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746355',Price=0.0278248245826276,ManufacturerGuid=N'1ea5d936-3329-40e0-8858-d145ab44d274',IsMasterData='1' WHERE Guid = '8ad00d03-eb26-4e6d-a59e-cd7a9b3209f5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f50d8314-f67a-4a10-9ac3-09625b4348a8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f50d8314-f67a-4a10-9ac3-09625b4348a8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3f5670b4-65c4-4f04-9b24-769b98072187')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746356', 0.0341156544882652, N'f50d8314-f67a-4a10-9ac3-09625b4348a8', '1', '3f5670b4-65c4-4f04-9b24-769b98072187')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746356',Price=0.0341156544882652,ManufacturerGuid=N'f50d8314-f67a-4a10-9ac3-09625b4348a8',IsMasterData='1' WHERE Guid = '3f5670b4-65c4-4f04-9b24-769b98072187'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd9f5ce5e-6c91-4887-9d5d-1b2bfe4e8bbe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd9f5ce5e-6c91-4887-9d5d-1b2bfe4e8bbe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8b2ab3b2-c7ca-4a22-8f78-3e77f1c092e9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746451', 0.144326155335108, N'd9f5ce5e-6c91-4887-9d5d-1b2bfe4e8bbe', '1', '8b2ab3b2-c7ca-4a22-8f78-3e77f1c092e9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746451',Price=0.144326155335108,ManufacturerGuid=N'd9f5ce5e-6c91-4887-9d5d-1b2bfe4e8bbe',IsMasterData='1' WHERE Guid = '8b2ab3b2-c7ca-4a22-8f78-3e77f1c092e9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'363d932c-2e73-49c8-bca7-4ff7fcadf8c9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'363d932c-2e73-49c8-bca7-4ff7fcadf8c9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2ec64a52-927a-4d03-b1fb-c991fd843b06')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746456', 0.0245584321316235, N'363d932c-2e73-49c8-bca7-4ff7fcadf8c9', '1', '2ec64a52-927a-4d03-b1fb-c991fd843b06')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746456',Price=0.0245584321316235,ManufacturerGuid=N'363d932c-2e73-49c8-bca7-4ff7fcadf8c9',IsMasterData='1' WHERE Guid = '2ec64a52-927a-4d03-b1fb-c991fd843b06'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'04f1dd4f-d694-44e0-8b50-b4a512e6f6f8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'04f1dd4f-d694-44e0-8b50-b4a512e6f6f8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9d912368-3ad3-4672-a354-f79d56a0c5b7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746460', 0.0868618436970724, N'04f1dd4f-d694-44e0-8b50-b4a512e6f6f8', '1', '9d912368-3ad3-4672-a354-f79d56a0c5b7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746460',Price=0.0868618436970724,ManufacturerGuid=N'04f1dd4f-d694-44e0-8b50-b4a512e6f6f8',IsMasterData='1' WHERE Guid = '9d912368-3ad3-4672-a354-f79d56a0c5b7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1e0e7585-d6e0-48ec-8838-5e213e4aa027')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1e0e7585-d6e0-48ec-8838-5e213e4aa027')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0befb49d-be1c-441e-88c5-a9bc21372b37')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0746853', 0.077425598838616, N'1e0e7585-d6e0-48ec-8838-5e213e4aa027', '1', '0befb49d-be1c-441e-88c5-a9bc21372b37')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0746853',Price=0.077425598838616,ManufacturerGuid=N'1e0e7585-d6e0-48ec-8838-5e213e4aa027',IsMasterData='1' WHERE Guid = '0befb49d-be1c-441e-88c5-a9bc21372b37'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'42f7711d-1423-4fc1-aa99-8de686684a6a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'42f7711d-1423-4fc1-aa99-8de686684a6a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a4f70864-c2db-4fd8-a3ca-4af86944ba90')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'0748661', 0.0417372368739415, N'42f7711d-1423-4fc1-aa99-8de686684a6a', '1', 'a4f70864-c2db-4fd8-a3ca-4af86944ba90')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'0748661',Price=0.0417372368739415,ManufacturerGuid=N'42f7711d-1423-4fc1-aa99-8de686684a6a',IsMasterData='1' WHERE Guid = 'a4f70864-c2db-4fd8-a3ca-4af86944ba90'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e4482ffa-9a87-4e6c-a098-70afdceaad31')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e4482ffa-9a87-4e6c-a098-70afdceaad31')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '09558007-bec7-496d-ac55-156b422c7fb9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2152A051', 0.0375030244374546, N'e4482ffa-9a87-4e6c-a098-70afdceaad31', '1', '09558007-bec7-496d-ac55-156b422c7fb9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2152A051',Price=0.0375030244374546,ManufacturerGuid=N'e4482ffa-9a87-4e6c-a098-70afdceaad31',IsMasterData='1' WHERE Guid = '09558007-bec7-496d-ac55-156b422c7fb9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cdc8f2f5-468b-4b2a-9df1-1bb0833d24cf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cdc8f2f5-468b-4b2a-9df1-1bb0833d24cf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8cfc3a79-9ad6-4b9b-8a2d-fa97dc3e37da')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2152B056', 0.087103798693443, N'cdc8f2f5-468b-4b2a-9df1-1bb0833d24cf', '1', '8cfc3a79-9ad6-4b9b-8a2d-fa97dc3e37da')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2152B056',Price=0.087103798693443,ManufacturerGuid=N'cdc8f2f5-468b-4b2a-9df1-1bb0833d24cf',IsMasterData='1' WHERE Guid = '8cfc3a79-9ad6-4b9b-8a2d-fa97dc3e37da'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a5266c7a-bcd1-4ea7-9db6-b404e56430d5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a5266c7a-bcd1-4ea7-9db6-b404e56430d5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c1549f0f-c09f-4364-b044-68f462880799')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2154002', 0.0118557948221631, N'a5266c7a-bcd1-4ea7-9db6-b404e56430d5', '1', 'c1549f0f-c09f-4364-b044-68f462880799')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2154002',Price=0.0118557948221631,ManufacturerGuid=N'a5266c7a-bcd1-4ea7-9db6-b404e56430d5',IsMasterData='1' WHERE Guid = 'c1549f0f-c09f-4364-b044-68f462880799'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b771baed-12b9-432a-b701-2b9077e486f8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b771baed-12b9-432a-b701-2b9077e486f8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8691f572-f80f-4f9e-b426-8e1a9b7d4b42')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2172175', 0.229857246552141, N'b771baed-12b9-432a-b701-2b9077e486f8', '1', '8691f572-f80f-4f9e-b426-8e1a9b7d4b42')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2172175',Price=0.229857246552141,ManufacturerGuid=N'b771baed-12b9-432a-b701-2b9077e486f8',IsMasterData='1' WHERE Guid = '8691f572-f80f-4f9e-b426-8e1a9b7d4b42'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'301e0ef9-ae80-4fec-a765-487bd9e2bbe1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'301e0ef9-ae80-4fec-a765-487bd9e2bbe1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '28c4bad2-56e0-4461-bb19-1923e1729c31')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2172586', 0.156181950157271, N'301e0ef9-ae80-4fec-a765-487bd9e2bbe1', '1', '28c4bad2-56e0-4461-bb19-1923e1729c31')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2172586',Price=0.156181950157271,ManufacturerGuid=N'301e0ef9-ae80-4fec-a765-487bd9e2bbe1',IsMasterData='1' WHERE Guid = '28c4bad2-56e0-4461-bb19-1923e1729c31'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7d53a041-1abe-4b48-8366-7fe50faff998')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7d53a041-1abe-4b48-8366-7fe50faff998')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9be68f9f-973f-4bef-a8b1-572e5bc1ccd7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2182110', 0.066658601500121, N'7d53a041-1abe-4b48-8366-7fe50faff998', '1', '9be68f9f-973f-4bef-a8b1-572e5bc1ccd7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2182110',Price=0.066658601500121,ManufacturerGuid=N'7d53a041-1abe-4b48-8366-7fe50faff998',IsMasterData='1' WHERE Guid = '9be68f9f-973f-4bef-a8b1-572e5bc1ccd7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ad5f8391-99d7-4fdc-af3f-870bdff20541')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ad5f8391-99d7-4fdc-af3f-870bdff20541')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b22ea026-c287-416c-b9c1-f44155b52388')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2182122', 0.167553834986692, N'ad5f8391-99d7-4fdc-af3f-870bdff20541', '1', 'b22ea026-c287-416c-b9c1-f44155b52388')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2182122',Price=0.167553834986692,ManufacturerGuid=N'ad5f8391-99d7-4fdc-af3f-870bdff20541',IsMasterData='1' WHERE Guid = 'b22ea026-c287-416c-b9c1-f44155b52388'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5df9ac47-29b2-493e-8cb0-254c703c280e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5df9ac47-29b2-493e-8cb0-254c703c280e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '51dba5b3-5f3f-4924-8b55-48bcfc3dcba4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2184415', 0.168642632470361, N'5df9ac47-29b2-493e-8cb0-254c703c280e', '1', '51dba5b3-5f3f-4924-8b55-48bcfc3dcba4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2184415',Price=0.168642632470361,ManufacturerGuid=N'5df9ac47-29b2-493e-8cb0-254c703c280e',IsMasterData='1' WHERE Guid = '51dba5b3-5f3f-4924-8b55-48bcfc3dcba4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b6417ee9-08e6-403d-b681-71b8442ae680')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b6417ee9-08e6-403d-b681-71b8442ae680')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '005249b6-a985-400d-af92-7fddd7281ede')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2185103', 0.10924268086136, N'b6417ee9-08e6-403d-b681-71b8442ae680', '1', '005249b6-a985-400d-af92-7fddd7281ede')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2185103',Price=0.10924268086136,ManufacturerGuid=N'b6417ee9-08e6-403d-b681-71b8442ae680',IsMasterData='1' WHERE Guid = '005249b6-a985-400d-af92-7fddd7281ede'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'04348406-1e35-456c-a54a-9cecf8da6213')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'04348406-1e35-456c-a54a-9cecf8da6213')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5c105764-e8f0-4777-92ea-5df89a988353')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2185110', 1.33075248003871, N'04348406-1e35-456c-a54a-9cecf8da6213', '1', '5c105764-e8f0-4777-92ea-5df89a988353')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2185110',Price=1.33075248003871,ManufacturerGuid=N'04348406-1e35-456c-a54a-9cecf8da6213',IsMasterData='1' WHERE Guid = '5c105764-e8f0-4777-92ea-5df89a988353'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dbc89664-17e2-4efe-8aee-099777909f2f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dbc89664-17e2-4efe-8aee-099777909f2f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9e1e54c1-6eb4-484d-b361-e65fce58eae9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2186431', 1.6682796999758, N'dbc89664-17e2-4efe-8aee-099777909f2f', '1', '9e1e54c1-6eb4-484d-b361-e65fce58eae9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2186431',Price=1.6682796999758,ManufacturerGuid=N'dbc89664-17e2-4efe-8aee-099777909f2f',IsMasterData='1' WHERE Guid = '9e1e54c1-6eb4-484d-b361-e65fce58eae9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a69faaf8-cef4-4aef-b7fb-a008bc73e650')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a69faaf8-cef4-4aef-b7fb-a008bc73e650')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '08be69b7-5e02-4b79-aa6d-dfd7331df8c0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2231226', 0.0410113718848294, N'a69faaf8-cef4-4aef-b7fb-a008bc73e650', '1', '08be69b7-5e02-4b79-aa6d-dfd7331df8c0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2231226',Price=0.0410113718848294,ManufacturerGuid=N'a69faaf8-cef4-4aef-b7fb-a008bc73e650',IsMasterData='1' WHERE Guid = '08be69b7-5e02-4b79-aa6d-dfd7331df8c0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bfb15362-8ded-4ae0-b030-8c1a2d8c8b23')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bfb15362-8ded-4ae0-b030-8c1a2d8c8b23')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '38b874b6-1f96-4ff0-96c3-40ac9704e063')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2232142', 0.0401645293975321, N'bfb15362-8ded-4ae0-b030-8c1a2d8c8b23', '1', '38b874b6-1f96-4ff0-96c3-40ac9704e063')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2232142',Price=0.0401645293975321,ManufacturerGuid=N'bfb15362-8ded-4ae0-b030-8c1a2d8c8b23',IsMasterData='1' WHERE Guid = '38b874b6-1f96-4ff0-96c3-40ac9704e063'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ad01a9a9-3567-470f-8cb5-6b3623b2f936')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ad01a9a9-3567-470f-8cb5-6b3623b2f936')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8d4cb9b3-138c-4cf0-9c96-9d4fd4272163')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2234043', 0.0685942414710864, N'ad01a9a9-3567-470f-8cb5-6b3623b2f936', '1', '8d4cb9b3-138c-4cf0-9c96-9d4fd4272163')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2234043',Price=0.0685942414710864,ManufacturerGuid=N'ad01a9a9-3567-470f-8cb5-6b3623b2f936',IsMasterData='1' WHERE Guid = '8d4cb9b3-138c-4cf0-9c96-9d4fd4272163'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1b9ca906-dead-498e-a57a-43374be76caa')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1b9ca906-dead-498e-a57a-43374be76caa')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e37f3e6c-e062-436d-8ccf-63bf7fca18df')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D027', 0.0226227921606581, N'1b9ca906-dead-498e-a57a-43374be76caa', '1', 'e37f3e6c-e062-436d-8ccf-63bf7fca18df')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D027',Price=0.0226227921606581,ManufacturerGuid=N'1b9ca906-dead-498e-a57a-43374be76caa',IsMasterData='1' WHERE Guid = 'e37f3e6c-e062-436d-8ccf-63bf7fca18df'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61065f02-b024-4fd4-8658-df04e76d3af0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61065f02-b024-4fd4-8658-df04e76d3af0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '09fdf787-20da-4634-9c32-8c469a3fd40b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D038', 0.0280667795789983, N'61065f02-b024-4fd4-8658-df04e76d3af0', '1', '09fdf787-20da-4634-9c32-8c469a3fd40b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D038',Price=0.0280667795789983,ManufacturerGuid=N'61065f02-b024-4fd4-8658-df04e76d3af0',IsMasterData='1' WHERE Guid = '09fdf787-20da-4634-9c32-8c469a3fd40b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fd32c735-343d-4a78-a786-c125a9bcc728')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fd32c735-343d-4a78-a786-c125a9bcc728')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd1c70f80-08d0-4b21-96ba-96ff10bf518c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D039', 0.0154851197677232, N'fd32c735-343d-4a78-a786-c125a9bcc728', '1', 'd1c70f80-08d0-4b21-96ba-96ff10bf518c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D039',Price=0.0154851197677232,ManufacturerGuid=N'fd32c735-343d-4a78-a786-c125a9bcc728',IsMasterData='1' WHERE Guid = 'd1c70f80-08d0-4b21-96ba-96ff10bf518c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5a14fadc-b704-41bc-8331-91758b97a5fb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5a14fadc-b704-41bc-8331-91758b97a5fb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '021594fc-679f-4617-b8ac-6c5b40d85b4f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D059', 0.0260101621098476, N'5a14fadc-b704-41bc-8331-91758b97a5fb', '1', '021594fc-679f-4617-b8ac-6c5b40d85b4f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D059',Price=0.0260101621098476,ManufacturerGuid=N'5a14fadc-b704-41bc-8331-91758b97a5fb',IsMasterData='1' WHERE Guid = '021594fc-679f-4617-b8ac-6c5b40d85b4f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd541e140-0fad-4383-b1f5-6eca2dbfb59d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd541e140-0fad-4383-b1f5-6eca2dbfb59d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c15df075-aa22-45a2-baab-d9c2140acf8a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D060', 0.029397532059037, N'd541e140-0fad-4383-b1f5-6eca2dbfb59d', '1', 'c15df075-aa22-45a2-baab-d9c2140acf8a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D060',Price=0.029397532059037,ManufacturerGuid=N'd541e140-0fad-4383-b1f5-6eca2dbfb59d',IsMasterData='1' WHERE Guid = 'c15df075-aa22-45a2-baab-d9c2140acf8a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e9857ce8-800b-493d-95ea-ecd9e36d3a63')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e9857ce8-800b-493d-95ea-ecd9e36d3a63')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3465cb50-e825-4496-8a5e-e6eea40c8852')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D065', 0.0652068715218969, N'e9857ce8-800b-493d-95ea-ecd9e36d3a63', '1', '3465cb50-e825-4496-8a5e-e6eea40c8852')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D065',Price=0.0652068715218969,ManufacturerGuid=N'e9857ce8-800b-493d-95ea-ecd9e36d3a63',IsMasterData='1' WHERE Guid = '3465cb50-e825-4496-8a5e-e6eea40c8852'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6720e9f0-6062-4993-9fab-141a3100b805')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6720e9f0-6062-4993-9fab-141a3100b805')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cc2019ca-b2be-4103-a3a8-a0c6d932ac7d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D081', 0.898499879022502, N'6720e9f0-6062-4993-9fab-141a3100b805', '1', 'cc2019ca-b2be-4103-a3a8-a0c6d932ac7d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D081',Price=0.898499879022502,ManufacturerGuid=N'6720e9f0-6062-4993-9fab-141a3100b805',IsMasterData='1' WHERE Guid = 'cc2019ca-b2be-4103-a3a8-a0c6d932ac7d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6aabd00f-a404-4bdb-b1b2-b5cac4a6fb00')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6aabd00f-a404-4bdb-b1b2-b5cac4a6fb00')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '291c0ca5-50b3-412d-b7ab-2f479b147d06')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D083', 0.115412533268812, N'6aabd00f-a404-4bdb-b1b2-b5cac4a6fb00', '1', '291c0ca5-50b3-412d-b7ab-2f479b147d06')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D083',Price=0.115412533268812,ManufacturerGuid=N'6aabd00f-a404-4bdb-b1b2-b5cac4a6fb00',IsMasterData='1' WHERE Guid = '291c0ca5-50b3-412d-b7ab-2f479b147d06'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'54e41dc7-b8cf-4a89-83ef-206c103f46d6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'54e41dc7-b8cf-4a89-83ef-206c103f46d6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '92c283dc-8695-4b98-84b4-bb7aba1c8a34')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D089', 0.0758528913622066, N'54e41dc7-b8cf-4a89-83ef-206c103f46d6', '1', '92c283dc-8695-4b98-84b4-bb7aba1c8a34')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D089',Price=0.0758528913622066,ManufacturerGuid=N'54e41dc7-b8cf-4a89-83ef-206c103f46d6',IsMasterData='1' WHERE Guid = '92c283dc-8695-4b98-84b4-bb7aba1c8a34'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0cf9350d-a929-4cba-9e13-3d74c11f6996')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0cf9350d-a929-4cba-9e13-3d74c11f6996')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7a3eb650-406d-417d-80b8-ce02f4ce81c2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D105', 0.0869828211952577, N'0cf9350d-a929-4cba-9e13-3d74c11f6996', '1', '7a3eb650-406d-417d-80b8-ce02f4ce81c2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D105',Price=0.0869828211952577,ManufacturerGuid=N'0cf9350d-a929-4cba-9e13-3d74c11f6996',IsMasterData='1' WHERE Guid = '7a3eb650-406d-417d-80b8-ce02f4ce81c2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'65f3aad9-af51-46f9-8c47-327b7a9cb21a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'65f3aad9-af51-46f9-8c47-327b7a9cb21a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b06ac5f4-b947-4c51-9856-4185214f025a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D106', 0.112992983305105, N'65f3aad9-af51-46f9-8c47-327b7a9cb21a', '1', 'b06ac5f4-b947-4c51-9856-4185214f025a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D106',Price=0.112992983305105,ManufacturerGuid=N'65f3aad9-af51-46f9-8c47-327b7a9cb21a',IsMasterData='1' WHERE Guid = 'b06ac5f4-b947-4c51-9856-4185214f025a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4c8020ee-5e9e-4bc3-a9fe-d6c68c201b6d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4c8020ee-5e9e-4bc3-a9fe-d6c68c201b6d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f52e4612-2593-46d0-a7b3-f5696590466f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D110', 0.331478345027825, N'4c8020ee-5e9e-4bc3-a9fe-d6c68c201b6d', '1', 'f52e4612-2593-46d0-a7b3-f5696590466f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D110',Price=0.331478345027825,ManufacturerGuid=N'4c8020ee-5e9e-4bc3-a9fe-d6c68c201b6d',IsMasterData='1' WHERE Guid = 'f52e4612-2593-46d0-a7b3-f5696590466f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fe326626-1fbd-4420-90e0-8b01c5bef190')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fe326626-1fbd-4420-90e0-8b01c5bef190')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8cae3575-1ae6-44bb-a143-bacd65169f59')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D559', 0.0616985240745221, N'fe326626-1fbd-4420-90e0-8b01c5bef190', '1', '8cae3575-1ae6-44bb-a143-bacd65169f59')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D559',Price=0.0616985240745221,ManufacturerGuid=N'fe326626-1fbd-4420-90e0-8b01c5bef190',IsMasterData='1' WHERE Guid = '8cae3575-1ae6-44bb-a143-bacd65169f59'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3d74f1cb-d00f-4332-8128-627af190b059')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3d74f1cb-d00f-4332-8128-627af190b059')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '84b22464-c902-4b57-a837-5c6023ba411f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D560', 0.087103798693443, N'3d74f1cb-d00f-4332-8128-627af190b059', '1', '84b22464-c902-4b57-a837-5c6023ba411f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D560',Price=0.087103798693443,ManufacturerGuid=N'3d74f1cb-d00f-4332-8128-627af190b059',IsMasterData='1' WHERE Guid = '84b22464-c902-4b57-a837-5c6023ba411f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd3d042b6-0e42-433d-a20f-b3e02a216b8e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd3d042b6-0e42-433d-a20f-b3e02a216b8e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '166f7264-f0e3-4837-b49a-aef39e1203de')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D569', 0.181829179772562, N'd3d042b6-0e42-433d-a20f-b3e02a216b8e', '1', '166f7264-f0e3-4837-b49a-aef39e1203de')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D569',Price=0.181829179772562,ManufacturerGuid=N'd3d042b6-0e42-433d-a20f-b3e02a216b8e',IsMasterData='1' WHERE Guid = '166f7264-f0e3-4837-b49a-aef39e1203de'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8d343667-a8b5-4899-acfe-f72a3da6c87d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8d343667-a8b5-4899-acfe-f72a3da6c87d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '681cf1e2-b911-49d5-9804-928c7ea7ca65')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D578', 0.103677715944834, N'8d343667-a8b5-4899-acfe-f72a3da6c87d', '1', '681cf1e2-b911-49d5-9804-928c7ea7ca65')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D578',Price=0.103677715944834,ManufacturerGuid=N'8d343667-a8b5-4899-acfe-f72a3da6c87d',IsMasterData='1' WHERE Guid = '681cf1e2-b911-49d5-9804-928c7ea7ca65'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ff33d3b3-f88a-4ca2-9755-1af6e4636746')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ff33d3b3-f88a-4ca2-9755-1af6e4636746')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '64b1030c-137b-491f-b086-72e1296f5bcc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D636', 0.337769174933462, N'ff33d3b3-f88a-4ca2-9755-1af6e4636746', '1', '64b1030c-137b-491f-b086-72e1296f5bcc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D636',Price=0.337769174933462,ManufacturerGuid=N'ff33d3b3-f88a-4ca2-9755-1af6e4636746',IsMasterData='1' WHERE Guid = '64b1030c-137b-491f-b086-72e1296f5bcc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'15c96a7e-251a-4318-82ee-848b4d507184')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'15c96a7e-251a-4318-82ee-848b4d507184')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '384532e3-c955-467a-ae97-d64bf56da448')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311E027', 0.0257682071134769, N'15c96a7e-251a-4318-82ee-848b4d507184', '1', '384532e3-c955-467a-ae97-d64bf56da448')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311E027',Price=0.0257682071134769,ManufacturerGuid=N'15c96a7e-251a-4318-82ee-848b4d507184',IsMasterData='1' WHERE Guid = '384532e3-c955-467a-ae97-d64bf56da448'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a9e74cab-9591-420d-9f0b-c0f6f239b1b4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a9e74cab-9591-420d-9f0b-c0f6f239b1b4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '83d4ada9-c1f6-497d-a236-924db4b737f0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311F055', 0.0266150496007743, N'a9e74cab-9591-420d-9f0b-c0f6f239b1b4', '1', '83d4ada9-c1f6-497d-a236-924db4b737f0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311F055',Price=0.0266150496007743,ManufacturerGuid=N'a9e74cab-9591-420d-9f0b-c0f6f239b1b4',IsMasterData='1' WHERE Guid = '83d4ada9-c1f6-497d-a236-924db4b737f0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0cdc39b8-7814-4398-a6aa-6b52968254e8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0cdc39b8-7814-4398-a6aa-6b52968254e8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '43710ac9-a5a0-49f7-a89f-3b1a9fa519be')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311F069', 0.067384466489233, N'0cdc39b8-7814-4398-a6aa-6b52968254e8', '1', '43710ac9-a5a0-49f7-a89f-3b1a9fa519be')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311F069',Price=0.067384466489233,ManufacturerGuid=N'0cdc39b8-7814-4398-a6aa-6b52968254e8',IsMasterData='1' WHERE Guid = '43710ac9-a5a0-49f7-a89f-3b1a9fa519be'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'099f0feb-7a75-4c76-a654-0d684cc80eeb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'099f0feb-7a75-4c76-a654-0d684cc80eeb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '192f6b8e-8e9b-46d7-8321-8963d344539d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311F387', 8.27486087587709, N'099f0feb-7a75-4c76-a654-0d684cc80eeb', '1', '192f6b8e-8e9b-46d7-8321-8963d344539d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311F387',Price=8.27486087587709,ManufacturerGuid=N'099f0feb-7a75-4c76-a654-0d684cc80eeb',IsMasterData='1' WHERE Guid = '192f6b8e-8e9b-46d7-8321-8963d344539d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd5964dae-8de6-42bd-a0d8-6b8a55dce2a5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd5964dae-8de6-42bd-a0d8-6b8a55dce2a5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '661aa0cc-95ac-4f88-ad39-863a363abd13')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314A105', 0.165134285022986, N'd5964dae-8de6-42bd-a0d8-6b8a55dce2a5', '1', '661aa0cc-95ac-4f88-ad39-863a363abd13')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314A105',Price=0.165134285022986,ManufacturerGuid=N'd5964dae-8de6-42bd-a0d8-6b8a55dce2a5',IsMasterData='1' WHERE Guid = '661aa0cc-95ac-4f88-ad39-863a363abd13'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30ecc964-feff-42b1-8a08-1229fbb4c20b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30ecc964-feff-42b1-8a08-1229fbb4c20b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '67dbff55-4148-48a3-b3e4-eff1214e441a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314A106', 0.186910234696346, N'30ecc964-feff-42b1-8a08-1229fbb4c20b', '1', '67dbff55-4148-48a3-b3e4-eff1214e441a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314A106',Price=0.186910234696346,ManufacturerGuid=N'30ecc964-feff-42b1-8a08-1229fbb4c20b',IsMasterData='1' WHERE Guid = '67dbff55-4148-48a3-b3e4-eff1214e441a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f54d88f3-1e49-4586-8377-3f645c27e928')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f54d88f3-1e49-4586-8377-3f645c27e928')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd40dd1c9-f1d0-49f2-a141-0a5ac55c75c4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C034', 0.00544398741834019, N'f54d88f3-1e49-4586-8377-3f645c27e928', '1', 'd40dd1c9-f1d0-49f2-a141-0a5ac55c75c4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C034',Price=0.00544398741834019,ManufacturerGuid=N'f54d88f3-1e49-4586-8377-3f645c27e928',IsMasterData='1' WHERE Guid = 'd40dd1c9-f1d0-49f2-a141-0a5ac55c75c4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd87bb8a7-d4e0-4fea-a75c-710e2179be69')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd87bb8a7-d4e0-4fea-a75c-710e2179be69')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3cd9047-8e0f-4181-ade4-afd1dfc47df2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C038', 0.423421243648681, N'd87bb8a7-d4e0-4fea-a75c-710e2179be69', '1', 'd3cd9047-8e0f-4181-ade4-afd1dfc47df2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C038',Price=0.423421243648681,ManufacturerGuid=N'd87bb8a7-d4e0-4fea-a75c-710e2179be69',IsMasterData='1' WHERE Guid = 'd3cd9047-8e0f-4181-ade4-afd1dfc47df2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'60815a76-5f9f-4646-828d-9df18fa7cba3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'60815a76-5f9f-4646-828d-9df18fa7cba3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5c9ff513-7a01-4e44-be90-39db7598d450')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C044', 0.0217759496733608, N'60815a76-5f9f-4646-828d-9df18fa7cba3', '1', '5c9ff513-7a01-4e44-be90-39db7598d450')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C044',Price=0.0217759496733608,ManufacturerGuid=N'60815a76-5f9f-4646-828d-9df18fa7cba3',IsMasterData='1' WHERE Guid = '5c9ff513-7a01-4e44-be90-39db7598d450'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7f144391-65ec-4a2b-8377-e1b45ab37a01')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7f144391-65ec-4a2b-8377-e1b45ab37a01')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0c47f638-f4ac-4f54-9e7d-e84a45a3388b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C049', 0.0463343818049843, N'7f144391-65ec-4a2b-8377-e1b45ab37a01', '1', '0c47f638-f4ac-4f54-9e7d-e84a45a3388b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C049',Price=0.0463343818049843,ManufacturerGuid=N'7f144391-65ec-4a2b-8377-e1b45ab37a01',IsMasterData='1' WHERE Guid = '0c47f638-f4ac-4f54-9e7d-e84a45a3388b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'76fdc5bd-5009-4ca7-bc1f-6c5613afdf3e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'76fdc5bd-5009-4ca7-bc1f-6c5613afdf3e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f44153c6-1d43-4bcd-9d74-c3b7f0b1a0a4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C051', 0.604887490926688, N'76fdc5bd-5009-4ca7-bc1f-6c5613afdf3e', '1', 'f44153c6-1d43-4bcd-9d74-c3b7f0b1a0a4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C051',Price=0.604887490926688,ManufacturerGuid=N'76fdc5bd-5009-4ca7-bc1f-6c5613afdf3e',IsMasterData='1' WHERE Guid = 'f44153c6-1d43-4bcd-9d74-c3b7f0b1a0a4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6876d0b1-2312-4ff3-9308-f4ff98112aa1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6876d0b1-2312-4ff3-9308-f4ff98112aa1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '02fcf6c8-b3e3-4d30-86bd-81cebc875dfd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314C369', 0.29034599564481, N'6876d0b1-2312-4ff3-9308-f4ff98112aa1', '1', '02fcf6c8-b3e3-4d30-86bd-81cebc875dfd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314C369',Price=0.29034599564481,ManufacturerGuid=N'6876d0b1-2312-4ff3-9308-f4ff98112aa1',IsMasterData='1' WHERE Guid = '02fcf6c8-b3e3-4d30-86bd-81cebc875dfd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b56e655f-35cd-40fe-a444-1228e6a21d20')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b56e655f-35cd-40fe-a444-1228e6a21d20')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3735f65a-bce3-420d-b9e7-74624f6d2f60')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314D106', 0.0318170820227438, N'b56e655f-35cd-40fe-a444-1228e6a21d20', '1', '3735f65a-bce3-420d-b9e7-74624f6d2f60')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314D106',Price=0.0318170820227438,ManufacturerGuid=N'b56e655f-35cd-40fe-a444-1228e6a21d20',IsMasterData='1' WHERE Guid = '3735f65a-bce3-420d-b9e7-74624f6d2f60'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3d79e2fe-cb31-42df-ab7c-71c5d8a18e6a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3d79e2fe-cb31-42df-ab7c-71c5d8a18e6a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f02318a5-5c17-482b-a630-6f93829467e8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314E002', 0.0135494797967578, N'3d79e2fe-cb31-42df-ab7c-71c5d8a18e6a', '1', 'f02318a5-5c17-482b-a630-6f93829467e8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314E002',Price=0.0135494797967578,ManufacturerGuid=N'3d79e2fe-cb31-42df-ab7c-71c5d8a18e6a',IsMasterData='1' WHERE Guid = 'f02318a5-5c17-482b-a630-6f93829467e8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2f4acd7d-935a-4a73-9b2f-4d3a5dcfd927')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2f4acd7d-935a-4a73-9b2f-4d3a5dcfd927')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '63a421d0-1fd9-4554-8f1f-62fc0f4cf99d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314E004', 0.212315509315267, N'2f4acd7d-935a-4a73-9b2f-4d3a5dcfd927', '1', '63a421d0-1fd9-4554-8f1f-62fc0f4cf99d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314E004',Price=0.212315509315267,ManufacturerGuid=N'2f4acd7d-935a-4a73-9b2f-4d3a5dcfd927',IsMasterData='1' WHERE Guid = '63a421d0-1fd9-4554-8f1f-62fc0f4cf99d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'684cf4e5-0d5c-4a6f-a768-3e01d18352a9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'684cf4e5-0d5c-4a6f-a768-3e01d18352a9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5dcb3d10-bdc5-4394-92ce-c70158312a0f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314E007', 0.0150012097749819, N'684cf4e5-0d5c-4a6f-a768-3e01d18352a9', '1', '5dcb3d10-bdc5-4394-92ce-c70158312a0f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314E007',Price=0.0150012097749819,ManufacturerGuid=N'684cf4e5-0d5c-4a6f-a768-3e01d18352a9',IsMasterData='1' WHERE Guid = '5dcb3d10-bdc5-4394-92ce-c70158312a0f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4fbb0277-1aa3-4549-b728-fe6a7867acdf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4fbb0277-1aa3-4549-b728-fe6a7867acdf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7378f336-2f0b-4be9-bef6-b3c884e69c06')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F001', 0.0128236148076458, N'4fbb0277-1aa3-4549-b728-fe6a7867acdf', '1', '7378f336-2f0b-4be9-bef6-b3c884e69c06')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F001',Price=0.0128236148076458,ManufacturerGuid=N'4fbb0277-1aa3-4549-b728-fe6a7867acdf',IsMasterData='1' WHERE Guid = '7378f336-2f0b-4be9-bef6-b3c884e69c06'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'709ec7a1-666b-48d1-ba8e-d4f472fdc00d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'709ec7a1-666b-48d1-ba8e-d4f472fdc00d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '00410aa7-16bf-406e-bcd8-9d9827a5495c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F002', 0.0135494797967578, N'709ec7a1-666b-48d1-ba8e-d4f472fdc00d', '1', '00410aa7-16bf-406e-bcd8-9d9827a5495c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F002',Price=0.0135494797967578,ManufacturerGuid=N'709ec7a1-666b-48d1-ba8e-d4f472fdc00d',IsMasterData='1' WHERE Guid = '00410aa7-16bf-406e-bcd8-9d9827a5495c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ff8662b0-3708-469e-b879-963344b9ff05')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ff8662b0-3708-469e-b879-963344b9ff05')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5cdd3e37-6dfd-4b55-95aa-d49eeb19c546')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F003', 0.0241954996370675, N'ff8662b0-3708-469e-b879-963344b9ff05', '1', '5cdd3e37-6dfd-4b55-95aa-d49eeb19c546')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F003',Price=0.0241954996370675,ManufacturerGuid=N'ff8662b0-3708-469e-b879-963344b9ff05',IsMasterData='1' WHERE Guid = '5cdd3e37-6dfd-4b55-95aa-d49eeb19c546'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'79122095-a957-4a73-b6c3-b952eea64281')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'79122095-a957-4a73-b6c3-b952eea64281')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1851986d-40f0-4482-8673-3000d46f4f10')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F004', 0.0241954996370675, N'79122095-a957-4a73-b6c3-b952eea64281', '1', '1851986d-40f0-4482-8673-3000d46f4f10')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F004',Price=0.0241954996370675,ManufacturerGuid=N'79122095-a957-4a73-b6c3-b952eea64281',IsMasterData='1' WHERE Guid = '1851986d-40f0-4482-8673-3000d46f4f10'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'535ef474-c4b3-48c7-87bc-d57aeab807f9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'535ef474-c4b3-48c7-87bc-d57aeab807f9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '34d97ec5-80de-4b4b-aaf8-8f49a74d9f05')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F005', 0.0241954996370675, N'535ef474-c4b3-48c7-87bc-d57aeab807f9', '1', '34d97ec5-80de-4b4b-aaf8-8f49a74d9f05')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F005',Price=0.0241954996370675,ManufacturerGuid=N'535ef474-c4b3-48c7-87bc-d57aeab807f9',IsMasterData='1' WHERE Guid = '34d97ec5-80de-4b4b-aaf8-8f49a74d9f05'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'57e382db-2c32-4dff-b4c9-3b883741d1ef')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'57e382db-2c32-4dff-b4c9-3b883741d1ef')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eca7370a-8993-4901-8ac4-f1776be5c035')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F006', 0.0142753447858698, N'57e382db-2c32-4dff-b4c9-3b883741d1ef', '1', 'eca7370a-8993-4901-8ac4-f1776be5c035')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F006',Price=0.0142753447858698,ManufacturerGuid=N'57e382db-2c32-4dff-b4c9-3b883741d1ef',IsMasterData='1' WHERE Guid = 'eca7370a-8993-4901-8ac4-f1776be5c035'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2a151dd7-1f70-437e-abb4-535b77efd31e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2a151dd7-1f70-437e-abb4-535b77efd31e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1ce49ee3-35d3-4d20-a97f-feb7174c89fe')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F007', 0.0228647471570288, N'2a151dd7-1f70-437e-abb4-535b77efd31e', '1', '1ce49ee3-35d3-4d20-a97f-feb7174c89fe')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F007',Price=0.0228647471570288,ManufacturerGuid=N'2a151dd7-1f70-437e-abb4-535b77efd31e',IsMasterData='1' WHERE Guid = '1ce49ee3-35d3-4d20-a97f-feb7174c89fe'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b292ba8c-b0f0-439e-ab72-4ba916251346')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b292ba8c-b0f0-439e-ab72-4ba916251346')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '64e2077e-f714-4f15-b45c-9a231569c511')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F008', 0.0362932494556013, N'b292ba8c-b0f0-439e-ab72-4ba916251346', '1', '64e2077e-f714-4f15-b45c-9a231569c511')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F008',Price=0.0362932494556013,ManufacturerGuid=N'b292ba8c-b0f0-439e-ab72-4ba916251346',IsMasterData='1' WHERE Guid = '64e2077e-f714-4f15-b45c-9a231569c511'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8e4a5c2e-0f5b-4eb1-bfac-9339a0b07a52')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8e4a5c2e-0f5b-4eb1-bfac-9339a0b07a52')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '05650ac7-53f3-4711-80bd-f4eb3d2e1c12')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F009', 0.0256472296152916, N'8e4a5c2e-0f5b-4eb1-bfac-9339a0b07a52', '1', '05650ac7-53f3-4711-80bd-f4eb3d2e1c12')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F009',Price=0.0256472296152916,ManufacturerGuid=N'8e4a5c2e-0f5b-4eb1-bfac-9339a0b07a52',IsMasterData='1' WHERE Guid = '05650ac7-53f3-4711-80bd-f4eb3d2e1c12'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1efc9fc2-173c-42a2-8ee5-c06c43186fee')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1efc9fc2-173c-42a2-8ee5-c06c43186fee')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '81ed0f87-e534-4a7d-9027-c8b93592500a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F010', 0.376844906847326, N'1efc9fc2-173c-42a2-8ee5-c06c43186fee', '1', '81ed0f87-e534-4a7d-9027-c8b93592500a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F010',Price=0.376844906847326,ManufacturerGuid=N'1efc9fc2-173c-42a2-8ee5-c06c43186fee',IsMasterData='1' WHERE Guid = '81ed0f87-e534-4a7d-9027-c8b93592500a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'918cbcfd-3db5-46a5-9cae-4de8f8253a66')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'918cbcfd-3db5-46a5-9cae-4de8f8253a66')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c01ea94-3da0-4222-8491-de2453fe638c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F036', 0.0362932494556013, N'918cbcfd-3db5-46a5-9cae-4de8f8253a66', '1', '8c01ea94-3da0-4222-8491-de2453fe638c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F036',Price=0.0362932494556013,ManufacturerGuid=N'918cbcfd-3db5-46a5-9cae-4de8f8253a66',IsMasterData='1' WHERE Guid = '8c01ea94-3da0-4222-8491-de2453fe638c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3143caac-160b-4031-9564-d396d654ab4c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3143caac-160b-4031-9564-d396d654ab4c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '49797ad9-fb3a-4b88-bd6f-38365638af9f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314F037', 0.0344785869828212, N'3143caac-160b-4031-9564-d396d654ab4c', '1', '49797ad9-fb3a-4b88-bd6f-38365638af9f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314F037',Price=0.0344785869828212,ManufacturerGuid=N'3143caac-160b-4031-9564-d396d654ab4c',IsMasterData='1' WHERE Guid = '49797ad9-fb3a-4b88-bd6f-38365638af9f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2134efb3-e0b1-47fa-9c4d-553bba12b687')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2134efb3-e0b1-47fa-9c4d-553bba12b687')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2ace6e58-35ba-4bbc-9113-8e99ef5a856a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H001', 0.048390999274135, N'2134efb3-e0b1-47fa-9c4d-553bba12b687', '1', '2ace6e58-35ba-4bbc-9113-8e99ef5a856a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H001',Price=0.048390999274135,ManufacturerGuid=N'2134efb3-e0b1-47fa-9c4d-553bba12b687',IsMasterData='1' WHERE Guid = '2ace6e58-35ba-4bbc-9113-8e99ef5a856a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cf5c53cc-2432-487a-b829-2b3e9c903830')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cf5c53cc-2432-487a-b829-2b3e9c903830')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7f77d51d-c97e-4f4e-9b28-7761a8ddf3b8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H002', 0.0319380595209291, N'cf5c53cc-2432-487a-b829-2b3e9c903830', '1', '7f77d51d-c97e-4f4e-9b28-7761a8ddf3b8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H002',Price=0.0319380595209291,ManufacturerGuid=N'cf5c53cc-2432-487a-b829-2b3e9c903830',IsMasterData='1' WHERE Guid = '7f77d51d-c97e-4f4e-9b28-7761a8ddf3b8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fc70044d-e8f6-4f04-88cb-0b92b5bffd9a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fc70044d-e8f6-4f04-88cb-0b92b5bffd9a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'efbf0839-b8d8-4ce9-96d4-695ec90a6f2e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H003', 0.0349624969755625, N'fc70044d-e8f6-4f04-88cb-0b92b5bffd9a', '1', 'efbf0839-b8d8-4ce9-96d4-695ec90a6f2e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H003',Price=0.0349624969755625,ManufacturerGuid=N'fc70044d-e8f6-4f04-88cb-0b92b5bffd9a',IsMasterData='1' WHERE Guid = 'efbf0839-b8d8-4ce9-96d4-695ec90a6f2e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'687a792d-4526-42c0-8310-b2b3ebc70e29')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'687a792d-4526-42c0-8310-b2b3ebc70e29')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8d455ea7-a0d8-4213-b950-20e7d1f5e654')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H004', 0.0339946769900798, N'687a792d-4526-42c0-8310-b2b3ebc70e29', '1', '8d455ea7-a0d8-4213-b950-20e7d1f5e654')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H004',Price=0.0339946769900798,ManufacturerGuid=N'687a792d-4526-42c0-8310-b2b3ebc70e29',IsMasterData='1' WHERE Guid = '8d455ea7-a0d8-4213-b950-20e7d1f5e654'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b4f40c29-8e53-42a4-b0f6-1ed82c6204cc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b4f40c29-8e53-42a4-b0f6-1ed82c6204cc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c4cfe9e7-5112-4db7-ba90-599b78846e5d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H005', 0.0391967094120494, N'b4f40c29-8e53-42a4-b0f6-1ed82c6204cc', '1', 'c4cfe9e7-5112-4db7-ba90-599b78846e5d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H005',Price=0.0391967094120494,ManufacturerGuid=N'b4f40c29-8e53-42a4-b0f6-1ed82c6204cc',IsMasterData='1' WHERE Guid = 'c4cfe9e7-5112-4db7-ba90-599b78846e5d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f1cc3fa8-1fe5-48c2-83e3-787aa3ef4ec8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f1cc3fa8-1fe5-48c2-83e3-787aa3ef4ec8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd45bae79-e28d-4c7a-91ef-8a282dc3df6a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H006', 0.0450036293249456, N'f1cc3fa8-1fe5-48c2-83e3-787aa3ef4ec8', '1', 'd45bae79-e28d-4c7a-91ef-8a282dc3df6a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H006',Price=0.0450036293249456,ManufacturerGuid=N'f1cc3fa8-1fe5-48c2-83e3-787aa3ef4ec8',IsMasterData='1' WHERE Guid = 'd45bae79-e28d-4c7a-91ef-8a282dc3df6a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1ab13b05-d82d-449f-875c-beb589103305')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1ab13b05-d82d-449f-875c-beb589103305')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '96adeccf-f600-4350-a4de-3b1468c63812')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H007', 0.0452455843213162, N'1ab13b05-d82d-449f-875c-beb589103305', '1', '96adeccf-f600-4350-a4de-3b1468c63812')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H007',Price=0.0452455843213162,ManufacturerGuid=N'1ab13b05-d82d-449f-875c-beb589103305',IsMasterData='1' WHERE Guid = '96adeccf-f600-4350-a4de-3b1468c63812'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4f22a0b0-6944-4b4d-816b-8599ac2b003b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4f22a0b0-6944-4b4d-816b-8599ac2b003b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b43aa0ad-8199-44b8-9d0e-878c9cee1e56')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H008', 0.048390999274135, N'4f22a0b0-6944-4b4d-816b-8599ac2b003b', '1', 'b43aa0ad-8199-44b8-9d0e-878c9cee1e56')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H008',Price=0.048390999274135,ManufacturerGuid=N'4f22a0b0-6944-4b4d-816b-8599ac2b003b',IsMasterData='1' WHERE Guid = 'b43aa0ad-8199-44b8-9d0e-878c9cee1e56'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'228f58d8-944d-418e-b217-ae09b94f1999')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'228f58d8-944d-418e-b217-ae09b94f1999')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '448515b1-b9d3-4420-9315-b81b7b1bc1f0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H010', 0.09678199854827, N'228f58d8-944d-418e-b217-ae09b94f1999', '1', '448515b1-b9d3-4420-9315-b81b7b1bc1f0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H010',Price=0.09678199854827,ManufacturerGuid=N'228f58d8-944d-418e-b217-ae09b94f1999',IsMasterData='1' WHERE Guid = '448515b1-b9d3-4420-9315-b81b7b1bc1f0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'78153e75-75d4-4dad-b071-41c4200f2daf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'78153e75-75d4-4dad-b071-41c4200f2daf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b97587ee-1097-4f15-9113-9a3f49f73ed8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H011', 0.0509315267360271, N'78153e75-75d4-4dad-b071-41c4200f2daf', '1', 'b97587ee-1097-4f15-9113-9a3f49f73ed8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H011',Price=0.0509315267360271,ManufacturerGuid=N'78153e75-75d4-4dad-b071-41c4200f2daf',IsMasterData='1' WHERE Guid = 'b97587ee-1097-4f15-9113-9a3f49f73ed8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'192dd6ef-ddd2-4db1-b3b5-70c0673e54ac')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'192dd6ef-ddd2-4db1-b3b5-70c0673e54ac')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '55a30546-b979-43fd-87d5-fa545f82f8ff')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H012', 0.0815388337769175, N'192dd6ef-ddd2-4db1-b3b5-70c0673e54ac', '1', '55a30546-b979-43fd-87d5-fa545f82f8ff')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H012',Price=0.0815388337769175,ManufacturerGuid=N'192dd6ef-ddd2-4db1-b3b5-70c0673e54ac',IsMasterData='1' WHERE Guid = '55a30546-b979-43fd-87d5-fa545f82f8ff'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8e4bddf5-6d96-4b25-a2e9-79426b8b0b18')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8e4bddf5-6d96-4b25-a2e9-79426b8b0b18')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '71387d06-5b68-41e4-b715-cec68a4f588a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H013', 0.0725864989112025, N'8e4bddf5-6d96-4b25-a2e9-79426b8b0b18', '1', '71387d06-5b68-41e4-b715-cec68a4f588a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H013',Price=0.0725864989112025,ManufacturerGuid=N'8e4bddf5-6d96-4b25-a2e9-79426b8b0b18',IsMasterData='1' WHERE Guid = '71387d06-5b68-41e4-b715-cec68a4f588a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'74d91970-0ca4-445d-a1af-4d5004a721ec')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'74d91970-0ca4-445d-a1af-4d5004a721ec')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '29118501-ce78-49ce-bb78-99fbf724eba9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H015', 0.0846842487297363, N'74d91970-0ca4-445d-a1af-4d5004a721ec', '1', '29118501-ce78-49ce-bb78-99fbf724eba9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H015',Price=0.0846842487297363,ManufacturerGuid=N'74d91970-0ca4-445d-a1af-4d5004a721ec',IsMasterData='1' WHERE Guid = '29118501-ce78-49ce-bb78-99fbf724eba9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e55feaca-169e-4f9b-a66d-0dcc3e14ebbd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e55feaca-169e-4f9b-a66d-0dcc3e14ebbd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e67b435b-e355-4e0d-a0db-63ed82ba696a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H017', 0.108879748366804, N'e55feaca-169e-4f9b-a66d-0dcc3e14ebbd', '1', 'e67b435b-e355-4e0d-a0db-63ed82ba696a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H017',Price=0.108879748366804,ManufacturerGuid=N'e55feaca-169e-4f9b-a66d-0dcc3e14ebbd',IsMasterData='1' WHERE Guid = 'e67b435b-e355-4e0d-a0db-63ed82ba696a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dbf74b5a-4418-4d72-9f96-3a2615416161')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dbf74b5a-4418-4d72-9f96-3a2615416161')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '380a799b-3a01-4546-b52b-1da13176cef9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H018', 0.149770142753448, N'dbf74b5a-4418-4d72-9f96-3a2615416161', '1', '380a799b-3a01-4546-b52b-1da13176cef9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H018',Price=0.149770142753448,ManufacturerGuid=N'dbf74b5a-4418-4d72-9f96-3a2615416161',IsMasterData='1' WHERE Guid = '380a799b-3a01-4546-b52b-1da13176cef9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'acc86ba0-a709-40f5-acbd-017d3b2188d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'acc86ba0-a709-40f5-acbd-017d3b2188d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f31c56cc-c05c-44df-9a94-0af8ea333396')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H019', 0.108879748366804, N'acc86ba0-a709-40f5-acbd-017d3b2188d9', '1', 'f31c56cc-c05c-44df-9a94-0af8ea333396')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H019',Price=0.108879748366804,ManufacturerGuid=N'acc86ba0-a709-40f5-acbd-017d3b2188d9',IsMasterData='1' WHERE Guid = 'f31c56cc-c05c-44df-9a94-0af8ea333396'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b234e73e-e5d1-433d-aa35-23203189e395')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b234e73e-e5d1-433d-aa35-23203189e395')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '83f7afe5-0d24-4bd7-90f6-e1669561abb8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H020', 0.157270747640939, N'b234e73e-e5d1-433d-aa35-23203189e395', '1', '83f7afe5-0d24-4bd7-90f6-e1669561abb8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H020',Price=0.157270747640939,ManufacturerGuid=N'b234e73e-e5d1-433d-aa35-23203189e395',IsMasterData='1' WHERE Guid = '83f7afe5-0d24-4bd7-90f6-e1669561abb8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b109940a-c1b2-40c7-ac9b-4534289eb793')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b109940a-c1b2-40c7-ac9b-4534289eb793')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '541c7d63-9a98-42f2-9c5d-280778513b28')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H021', 0.151463827728043, N'b109940a-c1b2-40c7-ac9b-4534289eb793', '1', '541c7d63-9a98-42f2-9c5d-280778513b28')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H021',Price=0.151463827728043,ManufacturerGuid=N'b109940a-c1b2-40c7-ac9b-4534289eb793',IsMasterData='1' WHERE Guid = '541c7d63-9a98-42f2-9c5d-280778513b28'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d6d1ed5-cb8c-4aae-aa40-a55284b4484f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d6d1ed5-cb8c-4aae-aa40-a55284b4484f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c84406a9-f665-4f6d-bd0a-d56e5e6ad904')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H022', 0.145172997822405, N'2d6d1ed5-cb8c-4aae-aa40-a55284b4484f', '1', 'c84406a9-f665-4f6d-bd0a-d56e5e6ad904')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H022',Price=0.145172997822405,ManufacturerGuid=N'2d6d1ed5-cb8c-4aae-aa40-a55284b4484f',IsMasterData='1' WHERE Guid = 'c84406a9-f665-4f6d-bd0a-d56e5e6ad904'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5d08097f-ac48-4d70-8a29-2459e8377e64')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5d08097f-ac48-4d70-8a29-2459e8377e64')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'af244255-ed96-4db9-9f9b-b4c413080b7b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H023', 0.19356399709654, N'5d08097f-ac48-4d70-8a29-2459e8377e64', '1', 'af244255-ed96-4db9-9f9b-b4c413080b7b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H023',Price=0.19356399709654,ManufacturerGuid=N'5d08097f-ac48-4d70-8a29-2459e8377e64',IsMasterData='1' WHERE Guid = 'af244255-ed96-4db9-9f9b-b4c413080b7b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bcbf6c5f-dd26-4a31-95ba-c4a5ca04232f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bcbf6c5f-dd26-4a31-95ba-c4a5ca04232f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8a7a68f4-c0f6-4cfe-b140-b0d883a7c061')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H025', 0.192959109605613, N'bcbf6c5f-dd26-4a31-95ba-c4a5ca04232f', '1', '8a7a68f4-c0f6-4cfe-b140-b0d883a7c061')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H025',Price=0.192959109605613,ManufacturerGuid=N'bcbf6c5f-dd26-4a31-95ba-c4a5ca04232f',IsMasterData='1' WHERE Guid = '8a7a68f4-c0f6-4cfe-b140-b0d883a7c061'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7b54a1ca-d0e6-4695-a923-212877412344')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7b54a1ca-d0e6-4695-a923-212877412344')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1f0c15cd-c378-422c-8593-98d8d8bdf64e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H026', 0.293249455601258, N'7b54a1ca-d0e6-4695-a923-212877412344', '1', '1f0c15cd-c378-422c-8593-98d8d8bdf64e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H026',Price=0.293249455601258,ManufacturerGuid=N'7b54a1ca-d0e6-4695-a923-212877412344',IsMasterData='1' WHERE Guid = '1f0c15cd-c378-422c-8593-98d8d8bdf64e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d8e3a5c-3349-4ca3-9b20-638d91a31735')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d8e3a5c-3349-4ca3-9b20-638d91a31735')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dbeda335-6bfe-4cf6-b383-7d9e4759efed')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H030', 0.362932494556013, N'2d8e3a5c-3349-4ca3-9b20-638d91a31735', '1', 'dbeda335-6bfe-4cf6-b383-7d9e4759efed')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H030',Price=0.362932494556013,ManufacturerGuid=N'2d8e3a5c-3349-4ca3-9b20-638d91a31735',IsMasterData='1' WHERE Guid = 'dbeda335-6bfe-4cf6-b383-7d9e4759efed'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c89ba662-99e8-4e9a-b9e0-15b2edeea3eb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c89ba662-99e8-4e9a-b9e0-15b2edeea3eb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '492b0f2a-5958-4fad-9324-887d66e0f850')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H031', 0.0604887490926688, N'c89ba662-99e8-4e9a-b9e0-15b2edeea3eb', '1', '492b0f2a-5958-4fad-9324-887d66e0f850')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H031',Price=0.0604887490926688,ManufacturerGuid=N'c89ba662-99e8-4e9a-b9e0-15b2edeea3eb',IsMasterData='1' WHERE Guid = '492b0f2a-5958-4fad-9324-887d66e0f850'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e701cdd6-b86f-4343-97b7-9c5d4180cf0b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e701cdd6-b86f-4343-97b7-9c5d4180cf0b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd852b4b8-bd75-4cdb-a9df-1ae727d61f37')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H032', 0.037986934430196, N'e701cdd6-b86f-4343-97b7-9c5d4180cf0b', '1', 'd852b4b8-bd75-4cdb-a9df-1ae727d61f37')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H032',Price=0.037986934430196,ManufacturerGuid=N'e701cdd6-b86f-4343-97b7-9c5d4180cf0b',IsMasterData='1' WHERE Guid = 'd852b4b8-bd75-4cdb-a9df-1ae727d61f37'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'482c5b1a-7565-4257-a1d5-d8f5ffa4484e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'482c5b1a-7565-4257-a1d5-d8f5ffa4484e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '381d7849-120c-4d41-9ad0-d720177ec558')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H033', 0.048390999274135, N'482c5b1a-7565-4257-a1d5-d8f5ffa4484e', '1', '381d7849-120c-4d41-9ad0-d720177ec558')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H033',Price=0.048390999274135,ManufacturerGuid=N'482c5b1a-7565-4257-a1d5-d8f5ffa4484e',IsMasterData='1' WHERE Guid = '381d7849-120c-4d41-9ad0-d720177ec558'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'37bb431b-e08d-494d-a16a-52c888fbeb5c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'37bb431b-e08d-494d-a16a-52c888fbeb5c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6a7f7504-f078-49a1-ba2c-442941a94d05')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H034', 0.0498427292523591, N'37bb431b-e08d-494d-a16a-52c888fbeb5c', '1', '6a7f7504-f078-49a1-ba2c-442941a94d05')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H034',Price=0.0498427292523591,ManufacturerGuid=N'37bb431b-e08d-494d-a16a-52c888fbeb5c',IsMasterData='1' WHERE Guid = '6a7f7504-f078-49a1-ba2c-442941a94d05'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c3a7cb2b-7453-4abf-9a9f-078f9c36995a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c3a7cb2b-7453-4abf-9a9f-078f9c36995a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a4bd14ae-825a-4880-9124-37ae4d4cd2fb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H036', 0.0554076941688846, N'c3a7cb2b-7453-4abf-9a9f-078f9c36995a', '1', 'a4bd14ae-825a-4880-9124-37ae4d4cd2fb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H036',Price=0.0554076941688846,ManufacturerGuid=N'c3a7cb2b-7453-4abf-9a9f-078f9c36995a',IsMasterData='1' WHERE Guid = 'a4bd14ae-825a-4880-9124-37ae4d4cd2fb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ac8e5fdc-b971-4826-a897-c59669a7c203')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ac8e5fdc-b971-4826-a897-c59669a7c203')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '919efe9a-3e53-4f2d-896a-7f051b9d3c82')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H038', 0.09678199854827, N'ac8e5fdc-b971-4826-a897-c59669a7c203', '1', '919efe9a-3e53-4f2d-896a-7f051b9d3c82')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H038',Price=0.09678199854827,ManufacturerGuid=N'ac8e5fdc-b971-4826-a897-c59669a7c203',IsMasterData='1' WHERE Guid = '919efe9a-3e53-4f2d-896a-7f051b9d3c82'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8efaeff5-c8d3-403d-9520-ae1d7e12ad23')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8efaeff5-c8d3-403d-9520-ae1d7e12ad23')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '60605b31-db44-4234-8af6-fd279c282d73')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H039', 0.0846842487297363, N'8efaeff5-c8d3-403d-9520-ae1d7e12ad23', '1', '60605b31-db44-4234-8af6-fd279c282d73')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H039',Price=0.0846842487297363,ManufacturerGuid=N'8efaeff5-c8d3-403d-9520-ae1d7e12ad23',IsMasterData='1' WHERE Guid = '60605b31-db44-4234-8af6-fd279c282d73'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6826b374-fe17-46fc-b984-660fdeb781f0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6826b374-fe17-46fc-b984-660fdeb781f0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4e400757-53c5-4156-9c2a-d8c4266935bc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H041', 0.0719816114202758, N'6826b374-fe17-46fc-b984-660fdeb781f0', '1', '4e400757-53c5-4156-9c2a-d8c4266935bc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H041',Price=0.0719816114202758,ManufacturerGuid=N'6826b374-fe17-46fc-b984-660fdeb781f0',IsMasterData='1' WHERE Guid = '4e400757-53c5-4156-9c2a-d8c4266935bc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a6100d9b-2209-4bbf-bde4-034c27a68913')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a6100d9b-2209-4bbf-bde4-034c27a68913')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '41498e89-56a7-4a06-ba66-0f615a653d11')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H042', 0.157270747640939, N'a6100d9b-2209-4bbf-bde4-034c27a68913', '1', '41498e89-56a7-4a06-ba66-0f615a653d11')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H042',Price=0.157270747640939,ManufacturerGuid=N'a6100d9b-2209-4bbf-bde4-034c27a68913',IsMasterData='1' WHERE Guid = '41498e89-56a7-4a06-ba66-0f615a653d11'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aff9e76d-d4a9-4cd8-8391-e51f4725f54e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aff9e76d-d4a9-4cd8-8391-e51f4725f54e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7dd5ad42-35ac-4f44-8e77-0da28a6906cc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H043', 0.102709895959352, N'aff9e76d-d4a9-4cd8-8391-e51f4725f54e', '1', '7dd5ad42-35ac-4f44-8e77-0da28a6906cc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H043',Price=0.102709895959352,ManufacturerGuid=N'aff9e76d-d4a9-4cd8-8391-e51f4725f54e',IsMasterData='1' WHERE Guid = '7dd5ad42-35ac-4f44-8e77-0da28a6906cc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'514b3c46-51b4-47a5-a88d-7bb881fa211b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'514b3c46-51b4-47a5-a88d-7bb881fa211b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eecd376f-5ca8-44c3-a528-b433a4db907e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H045', 0.108879748366804, N'514b3c46-51b4-47a5-a88d-7bb881fa211b', '1', 'eecd376f-5ca8-44c3-a528-b433a4db907e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H045',Price=0.108879748366804,ManufacturerGuid=N'514b3c46-51b4-47a5-a88d-7bb881fa211b',IsMasterData='1' WHERE Guid = 'eecd376f-5ca8-44c3-a528-b433a4db907e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'30872b54-251a-4f09-8082-876620540431')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'30872b54-251a-4f09-8082-876620540431')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '95faee5b-8724-4e18-bc33-2f84b9a27115')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H050', 0.181466247278006, N'30872b54-251a-4f09-8082-876620540431', '1', '95faee5b-8724-4e18-bc33-2f84b9a27115')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H050',Price=0.181466247278006,ManufacturerGuid=N'30872b54-251a-4f09-8082-876620540431',IsMasterData='1' WHERE Guid = '95faee5b-8724-4e18-bc33-2f84b9a27115'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4b231cb4-c18e-4f9f-9ba4-df2a0cd55edb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4b231cb4-c18e-4f9f-9ba4-df2a0cd55edb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4e34d619-58d2-4a9d-ba2e-f5a24e0ce795')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H052', 0.157270747640939, N'4b231cb4-c18e-4f9f-9ba4-df2a0cd55edb', '1', '4e34d619-58d2-4a9d-ba2e-f5a24e0ce795')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H052',Price=0.157270747640939,ManufacturerGuid=N'4b231cb4-c18e-4f9f-9ba4-df2a0cd55edb',IsMasterData='1' WHERE Guid = '4e34d619-58d2-4a9d-ba2e-f5a24e0ce795'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6f714bf4-d690-4ff9-b252-98ad660e055f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6f714bf4-d690-4ff9-b252-98ad660e055f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6ee068ae-49ce-4dea-a199-b7831774794b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H064', 0.0604887490926688, N'6f714bf4-d690-4ff9-b252-98ad660e055f', '1', '6ee068ae-49ce-4dea-a199-b7831774794b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H064',Price=0.0604887490926688,ManufacturerGuid=N'6f714bf4-d690-4ff9-b252-98ad660e055f',IsMasterData='1' WHERE Guid = '6ee068ae-49ce-4dea-a199-b7831774794b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'334a9786-15a5-4742-9dc6-8b28b741a688')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'334a9786-15a5-4742-9dc6-8b28b741a688')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '092a9faf-0a98-49c0-abdf-411bfe88eebc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H102', 0.0362932494556013, N'334a9786-15a5-4742-9dc6-8b28b741a688', '1', '092a9faf-0a98-49c0-abdf-411bfe88eebc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H102',Price=0.0362932494556013,ManufacturerGuid=N'334a9786-15a5-4742-9dc6-8b28b741a688',IsMasterData='1' WHERE Guid = '092a9faf-0a98-49c0-abdf-411bfe88eebc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'394a0002-8d4b-4e2b-bf1b-3cae5d10e580')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'394a0002-8d4b-4e2b-bf1b-3cae5d10e580')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9224273a-6171-4303-b000-d5a2135a0e66')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H607', 0.0326639245100411, N'394a0002-8d4b-4e2b-bf1b-3cae5d10e580', '1', '9224273a-6171-4303-b000-d5a2135a0e66')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H607',Price=0.0326639245100411,ManufacturerGuid=N'394a0002-8d4b-4e2b-bf1b-3cae5d10e580',IsMasterData='1' WHERE Guid = '9224273a-6171-4303-b000-d5a2135a0e66'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4b313408-c780-4546-8709-fc6c5fab4b39')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4b313408-c780-4546-8709-fc6c5fab4b39')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '89440db7-ad41-4a1f-a311-28259b5196ce')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H759', 0.169368497459473, N'4b313408-c780-4546-8709-fc6c5fab4b39', '1', '89440db7-ad41-4a1f-a311-28259b5196ce')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H759',Price=0.169368497459473,ManufacturerGuid=N'4b313408-c780-4546-8709-fc6c5fab4b39',IsMasterData='1' WHERE Guid = '89440db7-ad41-4a1f-a311-28259b5196ce'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f98defca-15bb-4e90-a693-5d7cc9614eb6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f98defca-15bb-4e90-a693-5d7cc9614eb6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0f00cb51-08a7-4fac-88ed-befa1cccfa3c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J001', 0.0488749092668764, N'f98defca-15bb-4e90-a693-5d7cc9614eb6', '1', '0f00cb51-08a7-4fac-88ed-befa1cccfa3c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J001',Price=0.0488749092668764,ManufacturerGuid=N'f98defca-15bb-4e90-a693-5d7cc9614eb6',IsMasterData='1' WHERE Guid = '0f00cb51-08a7-4fac-88ed-befa1cccfa3c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8e26e9d4-9f1e-4e16-a36f-9d0eff484b82')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8e26e9d4-9f1e-4e16-a36f-9d0eff484b82')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '099c6b71-738d-46ab-9c67-12612ab443b9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J002', 0.0803290587950641, N'8e26e9d4-9f1e-4e16-a36f-9d0eff484b82', '1', '099c6b71-738d-46ab-9c67-12612ab443b9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J002',Price=0.0803290587950641,ManufacturerGuid=N'8e26e9d4-9f1e-4e16-a36f-9d0eff484b82',IsMasterData='1' WHERE Guid = '099c6b71-738d-46ab-9c67-12612ab443b9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e57a434a-7b4a-4554-8dd8-4083273f4e05')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e57a434a-7b4a-4554-8dd8-4083273f4e05')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '37e18b7b-1bd2-4477-b0fa-1229997d39b5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J003', 0.0578272441325913, N'e57a434a-7b4a-4554-8dd8-4083273f4e05', '1', '37e18b7b-1bd2-4477-b0fa-1229997d39b5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J003',Price=0.0578272441325913,ManufacturerGuid=N'e57a434a-7b4a-4554-8dd8-4083273f4e05',IsMasterData='1' WHERE Guid = '37e18b7b-1bd2-4477-b0fa-1229997d39b5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ade07c94-334a-4b0d-9b6c-a6a34aa3af38')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ade07c94-334a-4b0d-9b6c-a6a34aa3af38')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a6c68309-ec3f-48d0-be5b-060dcaace26b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J004', 0.0846842487297363, N'ade07c94-334a-4b0d-9b6c-a6a34aa3af38', '1', 'a6c68309-ec3f-48d0-be5b-060dcaace26b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J004',Price=0.0846842487297363,ManufacturerGuid=N'ade07c94-334a-4b0d-9b6c-a6a34aa3af38',IsMasterData='1' WHERE Guid = 'a6c68309-ec3f-48d0-be5b-060dcaace26b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e790a557-d5ff-42bf-b36e-715922117f0d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e790a557-d5ff-42bf-b36e-715922117f0d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7b7faec3-b925-4923-8655-87c9ff5e2da8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J005', 0.0620614565690781, N'e790a557-d5ff-42bf-b36e-715922117f0d', '1', '7b7faec3-b925-4923-8655-87c9ff5e2da8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J005',Price=0.0620614565690781,ManufacturerGuid=N'e790a557-d5ff-42bf-b36e-715922117f0d',IsMasterData='1' WHERE Guid = '7b7faec3-b925-4923-8655-87c9ff5e2da8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fa8d2a7f-7978-4657-ba15-43f3137b80d2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fa8d2a7f-7978-4657-ba15-43f3137b80d2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c39d7472-4fb5-4b60-be79-55152e202786')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J006', 0.0780304863295427, N'fa8d2a7f-7978-4657-ba15-43f3137b80d2', '1', 'c39d7472-4fb5-4b60-be79-55152e202786')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J006',Price=0.0780304863295427,ManufacturerGuid=N'fa8d2a7f-7978-4657-ba15-43f3137b80d2',IsMasterData='1' WHERE Guid = 'c39d7472-4fb5-4b60-be79-55152e202786'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8757e975-0c75-4237-bf62-2cc6c91ab5da')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8757e975-0c75-4237-bf62-2cc6c91ab5da')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '96727807-9b5c-4e0f-b06d-a1f1405ddf08')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J007', 0.0782724413259134, N'8757e975-0c75-4237-bf62-2cc6c91ab5da', '1', '96727807-9b5c-4e0f-b06d-a1f1405ddf08')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J007',Price=0.0782724413259134,ManufacturerGuid=N'8757e975-0c75-4237-bf62-2cc6c91ab5da',IsMasterData='1' WHERE Guid = '96727807-9b5c-4e0f-b06d-a1f1405ddf08'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2c829385-4b59-4ced-8fad-72dfb86a9d39')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2c829385-4b59-4ced-8fad-72dfb86a9d39')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a9b16c0d-4548-4044-aaa5-ef82ea5efaf8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J008', 0.120977498185338, N'2c829385-4b59-4ced-8fad-72dfb86a9d39', '1', 'a9b16c0d-4548-4044-aaa5-ef82ea5efaf8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J008',Price=0.120977498185338,ManufacturerGuid=N'2c829385-4b59-4ced-8fad-72dfb86a9d39',IsMasterData='1' WHERE Guid = 'a9b16c0d-4548-4044-aaa5-ef82ea5efaf8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8763dc31-8d5e-49d4-b4dd-0c9ef07f39ca')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8763dc31-8d5e-49d4-b4dd-0c9ef07f39ca')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2acec200-9573-418d-b8c9-d5be4c6ffe10')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J009', 0.108879748366804, N'8763dc31-8d5e-49d4-b4dd-0c9ef07f39ca', '1', '2acec200-9573-418d-b8c9-d5be4c6ffe10')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J009',Price=0.108879748366804,ManufacturerGuid=N'8763dc31-8d5e-49d4-b4dd-0c9ef07f39ca',IsMasterData='1' WHERE Guid = '2acec200-9573-418d-b8c9-d5be4c6ffe10'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ac810683-e461-477a-a970-721025e441c4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ac810683-e461-477a-a970-721025e441c4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6c03d05f-199e-45d7-94c7-9ef107389cf4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J010', 0.0942414710863779, N'ac810683-e461-477a-a970-721025e441c4', '1', '6c03d05f-199e-45d7-94c7-9ef107389cf4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J010',Price=0.0942414710863779,ManufacturerGuid=N'ac810683-e461-477a-a970-721025e441c4',IsMasterData='1' WHERE Guid = '6c03d05f-199e-45d7-94c7-9ef107389cf4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a8c19d22-1462-4fd6-b8bd-4892a982f694')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a8c19d22-1462-4fd6-b8bd-4892a982f694')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'de8d9b47-8dfa-489f-a1be-5746267b6854')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J011', 0.104161625937576, N'a8c19d22-1462-4fd6-b8bd-4892a982f694', '1', 'de8d9b47-8dfa-489f-a1be-5746267b6854')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J011',Price=0.104161625937576,ManufacturerGuid=N'a8c19d22-1462-4fd6-b8bd-4892a982f694',IsMasterData='1' WHERE Guid = 'de8d9b47-8dfa-489f-a1be-5746267b6854'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2639e4b2-e1c0-4bc6-9784-e630b1b9f39b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2639e4b2-e1c0-4bc6-9784-e630b1b9f39b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3cd65f30-c8b8-4cb0-81ed-77994c9ebe88')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J012', 0.157270747640939, N'2639e4b2-e1c0-4bc6-9784-e630b1b9f39b', '1', '3cd65f30-c8b8-4cb0-81ed-77994c9ebe88')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J012',Price=0.157270747640939,ManufacturerGuid=N'2639e4b2-e1c0-4bc6-9784-e630b1b9f39b',IsMasterData='1' WHERE Guid = '3cd65f30-c8b8-4cb0-81ed-77994c9ebe88'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'270ea1c6-dae2-453f-89d0-0c12953e2d8a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'270ea1c6-dae2-453f-89d0-0c12953e2d8a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bb94dbc1-b353-44af-85a4-b54d9bc5fb3a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J013', 0.105734333413985, N'270ea1c6-dae2-453f-89d0-0c12953e2d8a', '1', 'bb94dbc1-b353-44af-85a4-b54d9bc5fb3a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J013',Price=0.105734333413985,ManufacturerGuid=N'270ea1c6-dae2-453f-89d0-0c12953e2d8a',IsMasterData='1' WHERE Guid = 'bb94dbc1-b353-44af-85a4-b54d9bc5fb3a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'55df5e5f-b1b4-4e08-805d-c89fa1262786')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'55df5e5f-b1b4-4e08-805d-c89fa1262786')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c3ce2bf7-58d9-4b14-8f50-2c9b2420e0ef')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J014', 0.123639003145415, N'55df5e5f-b1b4-4e08-805d-c89fa1262786', '1', 'c3ce2bf7-58d9-4b14-8f50-2c9b2420e0ef')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J014',Price=0.123639003145415,ManufacturerGuid=N'55df5e5f-b1b4-4e08-805d-c89fa1262786',IsMasterData='1' WHERE Guid = 'c3ce2bf7-58d9-4b14-8f50-2c9b2420e0ef'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8cc47ef8-6093-479e-98b4-796e52bcb971')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8cc47ef8-6093-479e-98b4-796e52bcb971')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1349ddaf-9783-48d9-811c-b3fa9fb2edf8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J015', 0.133075248003871, N'8cc47ef8-6093-479e-98b4-796e52bcb971', '1', '1349ddaf-9783-48d9-811c-b3fa9fb2edf8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J015',Price=0.133075248003871,ManufacturerGuid=N'8cc47ef8-6093-479e-98b4-796e52bcb971',IsMasterData='1' WHERE Guid = '1349ddaf-9783-48d9-811c-b3fa9fb2edf8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7067bfda-2691-47b7-b32f-97aa08959130')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7067bfda-2691-47b7-b32f-97aa08959130')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1963726b-a66d-43e8-b55d-38f745d123e6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J016', 0.169368497459473, N'7067bfda-2691-47b7-b32f-97aa08959130', '1', '1963726b-a66d-43e8-b55d-38f745d123e6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J016',Price=0.169368497459473,ManufacturerGuid=N'7067bfda-2691-47b7-b32f-97aa08959130',IsMasterData='1' WHERE Guid = '1963726b-a66d-43e8-b55d-38f745d123e6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'88704fe9-5b4a-48df-b827-690308004d77')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'88704fe9-5b4a-48df-b827-690308004d77')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '04f61db7-dca1-4ea1-b18b-70c5ca57b3fc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J017', 0.229857246552141, N'88704fe9-5b4a-48df-b827-690308004d77', '1', '04f61db7-dca1-4ea1-b18b-70c5ca57b3fc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J017',Price=0.229857246552141,ManufacturerGuid=N'88704fe9-5b4a-48df-b827-690308004d77',IsMasterData='1' WHERE Guid = '04f61db7-dca1-4ea1-b18b-70c5ca57b3fc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0b6fe952-c0f3-49ed-82fd-33801ec29a29')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0b6fe952-c0f3-49ed-82fd-33801ec29a29')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bc79efd4-61ef-4f43-a571-a3d4af10041f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J018', 0.19356399709654, N'0b6fe952-c0f3-49ed-82fd-33801ec29a29', '1', 'bc79efd4-61ef-4f43-a571-a3d4af10041f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J018',Price=0.19356399709654,ManufacturerGuid=N'0b6fe952-c0f3-49ed-82fd-33801ec29a29',IsMasterData='1' WHERE Guid = 'bc79efd4-61ef-4f43-a571-a3d4af10041f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b5419397-2e0e-4bd9-9ff4-97f9a7f2aeea')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b5419397-2e0e-4bd9-9ff4-97f9a7f2aeea')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5276cec0-e240-419f-98ee-b5329a971767')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J019', 0.120977498185338, N'b5419397-2e0e-4bd9-9ff4-97f9a7f2aeea', '1', '5276cec0-e240-419f-98ee-b5329a971767')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J019',Price=0.120977498185338,ManufacturerGuid=N'b5419397-2e0e-4bd9-9ff4-97f9a7f2aeea',IsMasterData='1' WHERE Guid = '5276cec0-e240-419f-98ee-b5329a971767'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6e5e2f23-cfae-4738-a6c2-01e640fad336')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6e5e2f23-cfae-4738-a6c2-01e640fad336')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfc197b1-c517-4ce4-a452-07da9fc28ac4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J020', 0.167795789983063, N'6e5e2f23-cfae-4738-a6c2-01e640fad336', '1', 'bfc197b1-c517-4ce4-a452-07da9fc28ac4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J020',Price=0.167795789983063,ManufacturerGuid=N'6e5e2f23-cfae-4738-a6c2-01e640fad336',IsMasterData='1' WHERE Guid = 'bfc197b1-c517-4ce4-a452-07da9fc28ac4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e3817a18-ad98-4eaa-b333-c0b73e96bf14')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e3817a18-ad98-4eaa-b333-c0b73e96bf14')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4c4ae75e-14c6-4074-919b-2c8fdd30d1ec')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J029', 0.266150496007743, N'e3817a18-ad98-4eaa-b333-c0b73e96bf14', '1', '4c4ae75e-14c6-4074-919b-2c8fdd30d1ec')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J029',Price=0.266150496007743,ManufacturerGuid=N'e3817a18-ad98-4eaa-b333-c0b73e96bf14',IsMasterData='1' WHERE Guid = '4c4ae75e-14c6-4074-919b-2c8fdd30d1ec'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'87f2b2ed-5d1b-47c9-bede-4dce38a0a635')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'87f2b2ed-5d1b-47c9-bede-4dce38a0a635')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bb9eebe5-51f0-40dd-bc7f-83046f4eabd5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J031', 0.112992983305105, N'87f2b2ed-5d1b-47c9-bede-4dce38a0a635', '1', 'bb9eebe5-51f0-40dd-bc7f-83046f4eabd5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J031',Price=0.112992983305105,ManufacturerGuid=N'87f2b2ed-5d1b-47c9-bede-4dce38a0a635',IsMasterData='1' WHERE Guid = 'bb9eebe5-51f0-40dd-bc7f-83046f4eabd5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b5855e5b-27f5-409b-94ee-1ee8428f780f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b5855e5b-27f5-409b-94ee-1ee8428f780f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '144e215c-545e-4e9b-8d38-957738d0e3f4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J033', 0.0846842487297363, N'b5855e5b-27f5-409b-94ee-1ee8428f780f', '1', '144e215c-545e-4e9b-8d38-957738d0e3f4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J033',Price=0.0846842487297363,ManufacturerGuid=N'b5855e5b-27f5-409b-94ee-1ee8428f780f',IsMasterData='1' WHERE Guid = '144e215c-545e-4e9b-8d38-957738d0e3f4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fc94852e-6c6d-44f9-ab34-28e67cd5b103')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fc94852e-6c6d-44f9-ab34-28e67cd5b103')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '189a36ef-3b90-42a2-af21-0170ebd0f197')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J034', 0.09678199854827, N'fc94852e-6c6d-44f9-ab34-28e67cd5b103', '1', '189a36ef-3b90-42a2-af21-0170ebd0f197')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J034',Price=0.09678199854827,ManufacturerGuid=N'fc94852e-6c6d-44f9-ab34-28e67cd5b103',IsMasterData='1' WHERE Guid = '189a36ef-3b90-42a2-af21-0170ebd0f197'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'df9f4f27-c2ac-43a6-8f00-2efceac27d28')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'df9f4f27-c2ac-43a6-8f00-2efceac27d28')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7223c790-dab5-4fe7-8abe-62a9e71f8bce')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J035', 0.09678199854827, N'df9f4f27-c2ac-43a6-8f00-2efceac27d28', '1', '7223c790-dab5-4fe7-8abe-62a9e71f8bce')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J035',Price=0.09678199854827,ManufacturerGuid=N'df9f4f27-c2ac-43a6-8f00-2efceac27d28',IsMasterData='1' WHERE Guid = '7223c790-dab5-4fe7-8abe-62a9e71f8bce'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'521c17ac-8e9e-4924-8da2-6c0f7699efc7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'521c17ac-8e9e-4924-8da2-6c0f7699efc7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd083bd70-cb4f-4912-9986-ee9b9161877c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J036', 0.108879748366804, N'521c17ac-8e9e-4924-8da2-6c0f7699efc7', '1', 'd083bd70-cb4f-4912-9986-ee9b9161877c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J036',Price=0.108879748366804,ManufacturerGuid=N'521c17ac-8e9e-4924-8da2-6c0f7699efc7',IsMasterData='1' WHERE Guid = 'd083bd70-cb4f-4912-9986-ee9b9161877c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'70f008f9-09f4-44eb-8132-0cf3806bf13d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'70f008f9-09f4-44eb-8132-0cf3806bf13d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9ad911b7-f1e2-4dad-9dd0-7395abf680aa')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J037', 0.0907331236390031, N'70f008f9-09f4-44eb-8132-0cf3806bf13d', '1', '9ad911b7-f1e2-4dad-9dd0-7395abf680aa')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J037',Price=0.0907331236390031,ManufacturerGuid=N'70f008f9-09f4-44eb-8132-0cf3806bf13d',IsMasterData='1' WHERE Guid = '9ad911b7-f1e2-4dad-9dd0-7395abf680aa'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'73ec4dfe-0bd3-40c3-81d0-b136d841d5ac')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'73ec4dfe-0bd3-40c3-81d0-b136d841d5ac')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0a808a84-0405-4f84-a56d-f81dd5430850')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J038', 0.0958141785627873, N'73ec4dfe-0bd3-40c3-81d0-b136d841d5ac', '1', '0a808a84-0405-4f84-a56d-f81dd5430850')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J038',Price=0.0958141785627873,ManufacturerGuid=N'73ec4dfe-0bd3-40c3-81d0-b136d841d5ac',IsMasterData='1' WHERE Guid = '0a808a84-0405-4f84-a56d-f81dd5430850'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b7977fb6-fb85-405e-87cf-a78bbdc00904')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b7977fb6-fb85-405e-87cf-a78bbdc00904')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2b6b8e20-8d37-4e7e-aee9-98b2348b9920')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J039', 0.108879748366804, N'b7977fb6-fb85-405e-87cf-a78bbdc00904', '1', '2b6b8e20-8d37-4e7e-aee9-98b2348b9920')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J039',Price=0.108879748366804,ManufacturerGuid=N'b7977fb6-fb85-405e-87cf-a78bbdc00904',IsMasterData='1' WHERE Guid = '2b6b8e20-8d37-4e7e-aee9-98b2348b9920'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9f7008b1-f699-4a9c-bd29-b8803aaa6217')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9f7008b1-f699-4a9c-bd29-b8803aaa6217')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '48896331-4984-47e7-93e8-17ec7c0d9a70')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J040', 0.106944108395838, N'9f7008b1-f699-4a9c-bd29-b8803aaa6217', '1', '48896331-4984-47e7-93e8-17ec7c0d9a70')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J040',Price=0.106944108395838,ManufacturerGuid=N'9f7008b1-f699-4a9c-bd29-b8803aaa6217',IsMasterData='1' WHERE Guid = '48896331-4984-47e7-93e8-17ec7c0d9a70'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bac9db1b-ec6c-43bc-9697-ebca7e8379d8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bac9db1b-ec6c-43bc-9697-ebca7e8379d8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8370c230-ea79-41a8-84c8-716570814820')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J046', 0.142632470360513, N'bac9db1b-ec6c-43bc-9697-ebca7e8379d8', '1', '8370c230-ea79-41a8-84c8-716570814820')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J046',Price=0.142632470360513,ManufacturerGuid=N'bac9db1b-ec6c-43bc-9697-ebca7e8379d8',IsMasterData='1' WHERE Guid = '8370c230-ea79-41a8-84c8-716570814820'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b8013eb3-fbc8-4c24-9578-7de09a701f36')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b8013eb3-fbc8-4c24-9578-7de09a701f36')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd6a1022c-7ec6-4b84-bb1d-55b0ad77399f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J049', 0.181466247278006, N'b8013eb3-fbc8-4c24-9578-7de09a701f36', '1', 'd6a1022c-7ec6-4b84-bb1d-55b0ad77399f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J049',Price=0.181466247278006,ManufacturerGuid=N'b8013eb3-fbc8-4c24-9578-7de09a701f36',IsMasterData='1' WHERE Guid = 'd6a1022c-7ec6-4b84-bb1d-55b0ad77399f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1e64c7ee-d885-46df-9a98-0b523f636b6e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1e64c7ee-d885-46df-9a98-0b523f636b6e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b302f024-cd8d-4597-acc2-16118ada38e0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J051', 0.266150496007743, N'1e64c7ee-d885-46df-9a98-0b523f636b6e', '1', 'b302f024-cd8d-4597-acc2-16118ada38e0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J051',Price=0.266150496007743,ManufacturerGuid=N'1e64c7ee-d885-46df-9a98-0b523f636b6e',IsMasterData='1' WHERE Guid = 'b302f024-cd8d-4597-acc2-16118ada38e0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'dfbe811b-80e5-4344-b2a9-c9649710ee2b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'dfbe811b-80e5-4344-b2a9-c9649710ee2b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fd7a6cda-f6d2-49ee-865a-5bb64c184eb8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J203', 0.0947253810791193, N'dfbe811b-80e5-4344-b2a9-c9649710ee2b', '1', 'fd7a6cda-f6d2-49ee-865a-5bb64c184eb8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J203',Price=0.0947253810791193,ManufacturerGuid=N'dfbe811b-80e5-4344-b2a9-c9649710ee2b',IsMasterData='1' WHERE Guid = 'fd7a6cda-f6d2-49ee-865a-5bb64c184eb8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4f48eb91-9557-4b71-96a5-ea508d461fb2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4f48eb91-9557-4b71-96a5-ea508d461fb2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a6284088-543d-4fe0-a81e-f48db47ad010')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J204', 0.0846842487297363, N'4f48eb91-9557-4b71-96a5-ea508d461fb2', '1', 'a6284088-543d-4fe0-a81e-f48db47ad010')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J204',Price=0.0846842487297363,ManufacturerGuid=N'4f48eb91-9557-4b71-96a5-ea508d461fb2',IsMasterData='1' WHERE Guid = 'a6284088-543d-4fe0-a81e-f48db47ad010'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2433d67d-c1a0-4ff9-ab0a-42aafe79fa53')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2433d67d-c1a0-4ff9-ab0a-42aafe79fa53')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c570e400-c8e6-402e-a189-221795579fdf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J205', 0.09678199854827, N'2433d67d-c1a0-4ff9-ab0a-42aafe79fa53', '1', 'c570e400-c8e6-402e-a189-221795579fdf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J205',Price=0.09678199854827,ManufacturerGuid=N'2433d67d-c1a0-4ff9-ab0a-42aafe79fa53',IsMasterData='1' WHERE Guid = 'c570e400-c8e6-402e-a189-221795579fdf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'83b92719-9f64-439c-98ef-1be62a2604cb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'83b92719-9f64-439c-98ef-1be62a2604cb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd2ba177e-9eb1-491f-bf56-624169cc6311')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J207', 0.87466731187999, N'83b92719-9f64-439c-98ef-1be62a2604cb', '1', 'd2ba177e-9eb1-491f-bf56-624169cc6311')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J207',Price=0.87466731187999,ManufacturerGuid=N'83b92719-9f64-439c-98ef-1be62a2604cb',IsMasterData='1' WHERE Guid = 'd2ba177e-9eb1-491f-bf56-624169cc6311'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'34310b1c-33e0-4ca4-82e2-7bfcb34703af')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'34310b1c-33e0-4ca4-82e2-7bfcb34703af')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd540191d-6e8a-4d3b-9791-644a06fff748')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J503', 1.790466973143, N'34310b1c-33e0-4ca4-82e2-7bfcb34703af', '1', 'd540191d-6e8a-4d3b-9791-644a06fff748')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J503',Price=1.790466973143,ManufacturerGuid=N'34310b1c-33e0-4ca4-82e2-7bfcb34703af',IsMasterData='1' WHERE Guid = 'd540191d-6e8a-4d3b-9791-644a06fff748'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b99fd248-3a4d-4edc-bdd9-f701b8abbd57')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b99fd248-3a4d-4edc-bdd9-f701b8abbd57')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ec3d1dc9-cb0a-4e9d-aa13-f938fed11c7d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J599', 0.775102830873458, N'b99fd248-3a4d-4edc-bdd9-f701b8abbd57', '1', 'ec3d1dc9-cb0a-4e9d-aa13-f938fed11c7d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J599',Price=0.775102830873458,ManufacturerGuid=N'b99fd248-3a4d-4edc-bdd9-f701b8abbd57',IsMasterData='1' WHERE Guid = 'ec3d1dc9-cb0a-4e9d-aa13-f938fed11c7d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b213ccb5-0e3f-48f9-8ca4-16ae7faccff5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b213ccb5-0e3f-48f9-8ca4-16ae7faccff5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '85eea841-6b01-4540-a0d9-8db6a492238b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J602', 0.145172997822405, N'b213ccb5-0e3f-48f9-8ca4-16ae7faccff5', '1', '85eea841-6b01-4540-a0d9-8db6a492238b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J602',Price=0.145172997822405,ManufacturerGuid=N'b213ccb5-0e3f-48f9-8ca4-16ae7faccff5',IsMasterData='1' WHERE Guid = '85eea841-6b01-4540-a0d9-8db6a492238b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'46d259ba-91a3-466e-8083-89094bb9bbca')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'46d259ba-91a3-466e-8083-89094bb9bbca')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd78838c4-b2a0-48ce-a961-6eeb93bd33f6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J603', 0.0919428986208565, N'46d259ba-91a3-466e-8083-89094bb9bbca', '1', 'd78838c4-b2a0-48ce-a961-6eeb93bd33f6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J603',Price=0.0919428986208565,ManufacturerGuid=N'46d259ba-91a3-466e-8083-89094bb9bbca',IsMasterData='1' WHERE Guid = 'd78838c4-b2a0-48ce-a961-6eeb93bd33f6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6b4ee304-1d39-49c1-a313-973a85ae79ff')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6b4ee304-1d39-49c1-a313-973a85ae79ff')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7aa2a925-6a21-4755-9f99-ea089e7e3768')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J604', 0.133075248003871, N'6b4ee304-1d39-49c1-a313-973a85ae79ff', '1', '7aa2a925-6a21-4755-9f99-ea089e7e3768')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J604',Price=0.133075248003871,ManufacturerGuid=N'6b4ee304-1d39-49c1-a313-973a85ae79ff',IsMasterData='1' WHERE Guid = '7aa2a925-6a21-4755-9f99-ea089e7e3768'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'df701b27-267f-4b3a-9a74-a215cd254e95')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'df701b27-267f-4b3a-9a74-a215cd254e95')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bdc3c84d-a2fb-41e3-9172-985cc7c158c6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J606', 0.169368497459473, N'df701b27-267f-4b3a-9a74-a215cd254e95', '1', 'bdc3c84d-a2fb-41e3-9172-985cc7c158c6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J606',Price=0.169368497459473,ManufacturerGuid=N'df701b27-267f-4b3a-9a74-a215cd254e95',IsMasterData='1' WHERE Guid = 'bdc3c84d-a2fb-41e3-9172-985cc7c158c6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd04cdcc6-dd58-42a0-90e1-e793482ded2d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd04cdcc6-dd58-42a0-90e1-e793482ded2d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a5d57150-a4a9-4d77-be91-3aba358ea180')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J607', 0.119525768207113, N'd04cdcc6-dd58-42a0-90e1-e793482ded2d', '1', 'a5d57150-a4a9-4d77-be91-3aba358ea180')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J607',Price=0.119525768207113,ManufacturerGuid=N'd04cdcc6-dd58-42a0-90e1-e793482ded2d',IsMasterData='1' WHERE Guid = 'a5d57150-a4a9-4d77-be91-3aba358ea180'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'69d9bd4c-868d-44e8-8784-b3c112ee333c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'69d9bd4c-868d-44e8-8784-b3c112ee333c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e3facd33-111d-497a-aad8-2edc8d1c002b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J611', 0.192596177111057, N'69d9bd4c-868d-44e8-8784-b3c112ee333c', '1', 'e3facd33-111d-497a-aad8-2edc8d1c002b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J611',Price=0.192596177111057,ManufacturerGuid=N'69d9bd4c-868d-44e8-8784-b3c112ee333c',IsMasterData='1' WHERE Guid = 'e3facd33-111d-497a-aad8-2edc8d1c002b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6ff2593f-4f86-4b8a-b3fc-ef136951c6ef')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6ff2593f-4f86-4b8a-b3fc-ef136951c6ef')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b04d5958-eb71-4e2a-b9dd-3527308af055')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J622', 0.614928623276071, N'6ff2593f-4f86-4b8a-b3fc-ef136951c6ef', '1', 'b04d5958-eb71-4e2a-b9dd-3527308af055')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J622',Price=0.614928623276071,ManufacturerGuid=N'6ff2593f-4f86-4b8a-b3fc-ef136951c6ef',IsMasterData='1' WHERE Guid = 'b04d5958-eb71-4e2a-b9dd-3527308af055'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9e023eac-4a26-4051-988f-00000b97effd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9e023eac-4a26-4051-988f-00000b97effd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7e166377-2cdc-4d1f-81e7-bdc7112570f8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J634', 0.126300508105492, N'9e023eac-4a26-4051-988f-00000b97effd', '1', '7e166377-2cdc-4d1f-81e7-bdc7112570f8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J634',Price=0.126300508105492,ManufacturerGuid=N'9e023eac-4a26-4051-988f-00000b97effd',IsMasterData='1' WHERE Guid = '7e166377-2cdc-4d1f-81e7-bdc7112570f8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ce8e39a5-f6dd-40b4-bc5e-0af32904dc1c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ce8e39a5-f6dd-40b4-bc5e-0af32904dc1c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f22e5997-9179-42e8-93e4-864dd3eab254')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J637', 0.157270747640939, N'ce8e39a5-f6dd-40b4-bc5e-0af32904dc1c', '1', 'f22e5997-9179-42e8-93e4-864dd3eab254')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J637',Price=0.157270747640939,ManufacturerGuid=N'ce8e39a5-f6dd-40b4-bc5e-0af32904dc1c',IsMasterData='1' WHERE Guid = 'f22e5997-9179-42e8-93e4-864dd3eab254'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a84fbad6-b582-4b2c-8b2c-f4f4d6d98906')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a84fbad6-b582-4b2c-8b2c-f4f4d6d98906')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7838cf54-46c2-469c-bc31-b423fb4502ce')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J639', 0.170941204935882, N'a84fbad6-b582-4b2c-8b2c-f4f4d6d98906', '1', '7838cf54-46c2-469c-bc31-b423fb4502ce')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J639',Price=0.170941204935882,ManufacturerGuid=N'a84fbad6-b582-4b2c-8b2c-f4f4d6d98906',IsMasterData='1' WHERE Guid = '7838cf54-46c2-469c-bc31-b423fb4502ce'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'428a1fa1-215a-4e04-87f5-56442750270f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'428a1fa1-215a-4e04-87f5-56442750270f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1f600dec-92a2-4da1-937c-bc9ca388b6f4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J704', 0.123034115654488, N'428a1fa1-215a-4e04-87f5-56442750270f', '1', '1f600dec-92a2-4da1-937c-bc9ca388b6f4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J704',Price=0.123034115654488,ManufacturerGuid=N'428a1fa1-215a-4e04-87f5-56442750270f',IsMasterData='1' WHERE Guid = '1f600dec-92a2-4da1-937c-bc9ca388b6f4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b45214ef-8c4c-4c70-8dbf-e57604796a7b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b45214ef-8c4c-4c70-8dbf-e57604796a7b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b54e069f-f9cc-4361-8550-ec9d0f1f7959')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J705', 0.186063392209049, N'b45214ef-8c4c-4c70-8dbf-e57604796a7b', '1', 'b54e069f-f9cc-4361-8550-ec9d0f1f7959')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J705',Price=0.186063392209049,ManufacturerGuid=N'b45214ef-8c4c-4c70-8dbf-e57604796a7b',IsMasterData='1' WHERE Guid = 'b54e069f-f9cc-4361-8550-ec9d0f1f7959'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8acee9bf-2fc8-442e-83ad-d26f661afe93')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8acee9bf-2fc8-442e-83ad-d26f661afe93')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2b32e71c-9735-49a1-8f3e-09db285c50ea')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J707', 0.19356399709654, N'8acee9bf-2fc8-442e-83ad-d26f661afe93', '1', '2b32e71c-9735-49a1-8f3e-09db285c50ea')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J707',Price=0.19356399709654,ManufacturerGuid=N'8acee9bf-2fc8-442e-83ad-d26f661afe93',IsMasterData='1' WHERE Guid = '2b32e71c-9735-49a1-8f3e-09db285c50ea'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd97713bf-17de-4f8a-bd8f-c4acbd234091')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd97713bf-17de-4f8a-bd8f-c4acbd234091')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a3ea0fff-66b3-41ff-a1f3-7faa72208982')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J709', 0.20965400435519, N'd97713bf-17de-4f8a-bd8f-c4acbd234091', '1', 'a3ea0fff-66b3-41ff-a1f3-7faa72208982')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J709',Price=0.20965400435519,ManufacturerGuid=N'd97713bf-17de-4f8a-bd8f-c4acbd234091',IsMasterData='1' WHERE Guid = 'a3ea0fff-66b3-41ff-a1f3-7faa72208982'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aeef4446-5a81-40ea-8f0c-b3c06ae089d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aeef4446-5a81-40ea-8f0c-b3c06ae089d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c2fc1ba3-27dd-4fef-82ec-8671019c35eb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J710', 0.270384708444229, N'aeef4446-5a81-40ea-8f0c-b3c06ae089d9', '1', 'c2fc1ba3-27dd-4fef-82ec-8671019c35eb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J710',Price=0.270384708444229,ManufacturerGuid=N'aeef4446-5a81-40ea-8f0c-b3c06ae089d9',IsMasterData='1' WHERE Guid = 'c2fc1ba3-27dd-4fef-82ec-8671019c35eb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd66e0600-211b-498e-8af8-543b2d0e4f29')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd66e0600-211b-498e-8af8-543b2d0e4f29')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '232754df-b588-4ce0-96df-b819c20562df')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J712', 0.254052746189209, N'd66e0600-211b-498e-8af8-543b2d0e4f29', '1', '232754df-b588-4ce0-96df-b819c20562df')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J712',Price=0.254052746189209,ManufacturerGuid=N'd66e0600-211b-498e-8af8-543b2d0e4f29',IsMasterData='1' WHERE Guid = '232754df-b588-4ce0-96df-b819c20562df'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd7130742-bb7e-4a83-817f-7f7c33d4ee48')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd7130742-bb7e-4a83-817f-7f7c33d4ee48')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '637bcdb9-798b-40f1-bed0-8418214118ed')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J713', 0.195741592063876, N'd7130742-bb7e-4a83-817f-7f7c33d4ee48', '1', '637bcdb9-798b-40f1-bed0-8418214118ed')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J713',Price=0.195741592063876,ManufacturerGuid=N'd7130742-bb7e-4a83-817f-7f7c33d4ee48',IsMasterData='1' WHERE Guid = '637bcdb9-798b-40f1-bed0-8418214118ed'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'085bd17d-c979-4598-ad28-6e6244fafa60')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'085bd17d-c979-4598-ad28-6e6244fafa60')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e0244284-9298-4504-80cf-a2ad67e8de71')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K157', 0.698161142027583, N'085bd17d-c979-4598-ad28-6e6244fafa60', '1', 'e0244284-9298-4504-80cf-a2ad67e8de71')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K157',Price=0.698161142027583,ManufacturerGuid=N'085bd17d-c979-4598-ad28-6e6244fafa60',IsMasterData='1' WHERE Guid = 'e0244284-9298-4504-80cf-a2ad67e8de71'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a62a8d0f-ac93-4265-b5da-585b4f8f11e9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a62a8d0f-ac93-4265-b5da-585b4f8f11e9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '27d3c2c2-def3-4714-b00d-0aace65afb0f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K158', 0.0846842487297363, N'a62a8d0f-ac93-4265-b5da-585b4f8f11e9', '1', '27d3c2c2-def3-4714-b00d-0aace65afb0f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K158',Price=0.0846842487297363,ManufacturerGuid=N'a62a8d0f-ac93-4265-b5da-585b4f8f11e9',IsMasterData='1' WHERE Guid = '27d3c2c2-def3-4714-b00d-0aace65afb0f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c47c3595-7cb6-45c5-b883-2d9a4c9086f8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c47c3595-7cb6-45c5-b883-2d9a4c9086f8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '31289c48-5957-411b-a628-ba6f73d9122a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K159', 0.0846842487297363, N'c47c3595-7cb6-45c5-b883-2d9a4c9086f8', '1', '31289c48-5957-411b-a628-ba6f73d9122a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K159',Price=0.0846842487297363,ManufacturerGuid=N'c47c3595-7cb6-45c5-b883-2d9a4c9086f8',IsMasterData='1' WHERE Guid = '31289c48-5957-411b-a628-ba6f73d9122a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8b095b60-0237-4999-a4ab-1280345f5639')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8b095b60-0237-4999-a4ab-1280345f5639')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '05df7f67-6bde-4456-a293-42a6084206f9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K161', 0.0518993467215098, N'8b095b60-0237-4999-a4ab-1280345f5639', '1', '05df7f67-6bde-4456-a293-42a6084206f9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K161',Price=0.0518993467215098,ManufacturerGuid=N'8b095b60-0237-4999-a4ab-1280345f5639',IsMasterData='1' WHERE Guid = '05df7f67-6bde-4456-a293-42a6084206f9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9d243637-9231-4d95-a245-e6ab4dc1b768')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9d243637-9231-4d95-a245-e6ab4dc1b768')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd29d4c3d-0e44-487a-be09-5ab39a243851')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K164', 0.120977498185338, N'9d243637-9231-4d95-a245-e6ab4dc1b768', '1', 'd29d4c3d-0e44-487a-be09-5ab39a243851')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K164',Price=0.120977498185338,ManufacturerGuid=N'9d243637-9231-4d95-a245-e6ab4dc1b768',IsMasterData='1' WHERE Guid = 'd29d4c3d-0e44-487a-be09-5ab39a243851'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'391ac927-5976-44ba-8b8f-022a125c1624')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'391ac927-5976-44ba-8b8f-022a125c1624')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5401ff00-127c-4468-94fd-a6c11abf3512')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K166', 0.133075248003871, N'391ac927-5976-44ba-8b8f-022a125c1624', '1', '5401ff00-127c-4468-94fd-a6c11abf3512')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K166',Price=0.133075248003871,ManufacturerGuid=N'391ac927-5976-44ba-8b8f-022a125c1624',IsMasterData='1' WHERE Guid = '5401ff00-127c-4468-94fd-a6c11abf3512'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'39022752-e2a3-4e53-9105-9a0d98d8bd57')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'39022752-e2a3-4e53-9105-9a0d98d8bd57')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '63d20a0d-4201-4157-bef1-490e9b0f06c9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K201', 0.201185579482216, N'39022752-e2a3-4e53-9105-9a0d98d8bd57', '1', '63d20a0d-4201-4157-bef1-490e9b0f06c9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K201',Price=0.201185579482216,ManufacturerGuid=N'39022752-e2a3-4e53-9105-9a0d98d8bd57',IsMasterData='1' WHERE Guid = '63d20a0d-4201-4157-bef1-490e9b0f06c9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6918e49e-de32-4945-b2d8-9ceffc1b8b37')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6918e49e-de32-4945-b2d8-9ceffc1b8b37')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '54d448ae-fbff-4e38-848e-de243712bff8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K202', 0.230583111541253, N'6918e49e-de32-4945-b2d8-9ceffc1b8b37', '1', '54d448ae-fbff-4e38-848e-de243712bff8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K202',Price=0.230583111541253,ManufacturerGuid=N'6918e49e-de32-4945-b2d8-9ceffc1b8b37',IsMasterData='1' WHERE Guid = '54d448ae-fbff-4e38-848e-de243712bff8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e8686c95-696c-4f1c-9675-498ae6236d78')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e8686c95-696c-4f1c-9675-498ae6236d78')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9d70cedb-b9e4-49ae-a6f7-e55897a4bbee')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K203', 0.266150496007743, N'e8686c95-696c-4f1c-9675-498ae6236d78', '1', '9d70cedb-b9e4-49ae-a6f7-e55897a4bbee')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K203',Price=0.266150496007743,ManufacturerGuid=N'e8686c95-696c-4f1c-9675-498ae6236d78',IsMasterData='1' WHERE Guid = '9d70cedb-b9e4-49ae-a6f7-e55897a4bbee'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'57257bb8-f4ac-4e1f-aa14-1575262f9594')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'57257bb8-f4ac-4e1f-aa14-1575262f9594')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '69ab6ebc-cc75-4548-8548-1bac427b44a7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K206', 9.88386160174208, N'57257bb8-f4ac-4e1f-aa14-1575262f9594', '1', '69ab6ebc-cc75-4548-8548-1bac427b44a7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K206',Price=9.88386160174208,ManufacturerGuid=N'57257bb8-f4ac-4e1f-aa14-1575262f9594',IsMasterData='1' WHERE Guid = '69ab6ebc-cc75-4548-8548-1bac427b44a7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fd41e88b-f626-4c63-a83b-b63b63b5b597')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fd41e88b-f626-4c63-a83b-b63b63b5b597')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bc89ff27-3eb9-4438-abd9-757358016b30')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K207', 3.84708444229373, N'fd41e88b-f626-4c63-a83b-b63b63b5b597', '1', 'bc89ff27-3eb9-4438-abd9-757358016b30')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K207',Price=3.84708444229373,ManufacturerGuid=N'fd41e88b-f626-4c63-a83b-b63b63b5b597',IsMasterData='1' WHERE Guid = 'bc89ff27-3eb9-4438-abd9-757358016b30'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a0572d56-ef06-465d-a3dd-0f6f8f7fdd8c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a0572d56-ef06-465d-a3dd-0f6f8f7fdd8c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f79694bc-7fa7-4da3-99a7-4ace61e0bc84')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K208', 0.318533752721994, N'a0572d56-ef06-465d-a3dd-0f6f8f7fdd8c', '1', 'f79694bc-7fa7-4da3-99a7-4ace61e0bc84')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K208',Price=0.318533752721994,ManufacturerGuid=N'a0572d56-ef06-465d-a3dd-0f6f8f7fdd8c',IsMasterData='1' WHERE Guid = 'f79694bc-7fa7-4da3-99a7-4ace61e0bc84'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3cde567a-38fc-4a50-a867-5219dec52b01')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3cde567a-38fc-4a50-a867-5219dec52b01')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e2b96b73-0f6f-4738-9bac-c381e7ae64b4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K211', 3.38736994918945, N'3cde567a-38fc-4a50-a867-5219dec52b01', '1', 'e2b96b73-0f6f-4738-9bac-c381e7ae64b4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K211',Price=3.38736994918945,ManufacturerGuid=N'3cde567a-38fc-4a50-a867-5219dec52b01',IsMasterData='1' WHERE Guid = 'e2b96b73-0f6f-4738-9bac-c381e7ae64b4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a208c7a7-e7dd-4f9c-baf4-1419dca784e3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a208c7a7-e7dd-4f9c-baf4-1419dca784e3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9449984c-409d-476c-84a7-dd1d6fbd6d2e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K213', 3.61263005081055, N'a208c7a7-e7dd-4f9c-baf4-1419dca784e3', '1', '9449984c-409d-476c-84a7-dd1d6fbd6d2e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K213',Price=3.61263005081055,ManufacturerGuid=N'a208c7a7-e7dd-4f9c-baf4-1419dca784e3',IsMasterData='1' WHERE Guid = '9449984c-409d-476c-84a7-dd1d6fbd6d2e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'35503fd7-b500-4b35-b087-06df0eff61c3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'35503fd7-b500-4b35-b087-06df0eff61c3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a92147cf-6691-4245-9eb2-dd5869c7e76c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K234', 0.59496733607549, N'35503fd7-b500-4b35-b087-06df0eff61c3', '1', 'a92147cf-6691-4245-9eb2-dd5869c7e76c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K234',Price=0.59496733607549,ManufacturerGuid=N'35503fd7-b500-4b35-b087-06df0eff61c3',IsMasterData='1' WHERE Guid = 'a92147cf-6691-4245-9eb2-dd5869c7e76c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'953af45e-0a1b-4c30-aafa-cb30062eab6f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'953af45e-0a1b-4c30-aafa-cb30062eab6f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bcad5210-2809-4d72-8b27-54fc5ea79b2b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K234', 0.59496733607549, N'953af45e-0a1b-4c30-aafa-cb30062eab6f', '1', 'bcad5210-2809-4d72-8b27-54fc5ea79b2b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K234',Price=0.59496733607549,ManufacturerGuid=N'953af45e-0a1b-4c30-aafa-cb30062eab6f',IsMasterData='1' WHERE Guid = 'bcad5210-2809-4d72-8b27-54fc5ea79b2b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9a78adb5-2ff5-4006-8d83-1a5e772f8907')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9a78adb5-2ff5-4006-8d83-1a5e772f8907')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1a954443-e0cc-4547-a5bc-576d26a4f8a9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K444', 0.380595209291072, N'9a78adb5-2ff5-4006-8d83-1a5e772f8907', '1', '1a954443-e0cc-4547-a5bc-576d26a4f8a9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K444',Price=0.380595209291072,ManufacturerGuid=N'9a78adb5-2ff5-4006-8d83-1a5e772f8907',IsMasterData='1' WHERE Guid = '1a954443-e0cc-4547-a5bc-576d26a4f8a9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'378b3bd0-88f9-4369-b90a-2a014370b1c0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'378b3bd0-88f9-4369-b90a-2a014370b1c0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1acbe310-fe3b-42d6-b590-17b7d4342eaf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K448', 0.444955238325671, N'378b3bd0-88f9-4369-b90a-2a014370b1c0', '1', '1acbe310-fe3b-42d6-b590-17b7d4342eaf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K448',Price=0.444955238325671,ManufacturerGuid=N'378b3bd0-88f9-4369-b90a-2a014370b1c0',IsMasterData='1' WHERE Guid = '1acbe310-fe3b-42d6-b590-17b7d4342eaf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c23ad6e4-050c-45a4-b25b-019c8190bd29')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c23ad6e4-050c-45a4-b25b-019c8190bd29')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6508ff15-1b70-4ae7-bd60-bd13a0db2ad1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314K454', 0.572102588918461, N'c23ad6e4-050c-45a4-b25b-019c8190bd29', '1', '6508ff15-1b70-4ae7-bd60-bd13a0db2ad1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314K454',Price=0.572102588918461,ManufacturerGuid=N'c23ad6e4-050c-45a4-b25b-019c8190bd29',IsMasterData='1' WHERE Guid = '6508ff15-1b70-4ae7-bd60-bd13a0db2ad1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1a3b6291-c81f-43c6-bfa8-0599a681788f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1a3b6291-c81f-43c6-bfa8-0599a681788f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '81e97bc0-a91d-4ccc-bb01-55bfc0c24624')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314N043', 0.119767723203484, N'1a3b6291-c81f-43c6-bfa8-0599a681788f', '1', '81e97bc0-a91d-4ccc-bb01-55bfc0c24624')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314N043',Price=0.119767723203484,ManufacturerGuid=N'1a3b6291-c81f-43c6-bfa8-0599a681788f',IsMasterData='1' WHERE Guid = '81e97bc0-a91d-4ccc-bb01-55bfc0c24624'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'04e7f712-dccb-4130-bc42-d3eea0eda944')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'04e7f712-dccb-4130-bc42-d3eea0eda944')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7b3736bd-f098-4b41-a786-a52383311552')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314W009', 0.09678199854827, N'04e7f712-dccb-4130-bc42-d3eea0eda944', '1', '7b3736bd-f098-4b41-a786-a52383311552')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314W009',Price=0.09678199854827,ManufacturerGuid=N'04e7f712-dccb-4130-bc42-d3eea0eda944',IsMasterData='1' WHERE Guid = '7b3736bd-f098-4b41-a786-a52383311552'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e53f6452-539f-4880-b5f1-bf4baab7f8d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e53f6452-539f-4880-b5f1-bf4baab7f8d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1695ed82-6d65-4d52-a566-cccee5868658')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2315A059', 0.0354464069683039, N'e53f6452-539f-4880-b5f1-bf4baab7f8d9', '1', '1695ed82-6d65-4d52-a566-cccee5868658')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2315A059',Price=0.0354464069683039,ManufacturerGuid=N'e53f6452-539f-4880-b5f1-bf4baab7f8d9',IsMasterData='1' WHERE Guid = '1695ed82-6d65-4d52-a566-cccee5868658'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'315aa211-bc63-45fe-a022-6a36aba725f2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'315aa211-bc63-45fe-a022-6a36aba725f2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0703d96f-ec04-4efa-ba2f-885397fa267c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2315A157', 0.177353012339705, N'315aa211-bc63-45fe-a022-6a36aba725f2', '1', '0703d96f-ec04-4efa-ba2f-885397fa267c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2315A157',Price=0.177353012339705,ManufacturerGuid=N'315aa211-bc63-45fe-a022-6a36aba725f2',IsMasterData='1' WHERE Guid = '0703d96f-ec04-4efa-ba2f-885397fa267c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6fe224fc-a7e1-42ff-a29d-5c5c88378ddf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6fe224fc-a7e1-42ff-a29d-5c5c88378ddf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2aace173-a616-4da6-a90b-b0222061152b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2315C076', 0.428623276070651, N'6fe224fc-a7e1-42ff-a29d-5c5c88378ddf', '1', '2aace173-a616-4da6-a90b-b0222061152b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2315C076',Price=0.428623276070651,ManufacturerGuid=N'6fe224fc-a7e1-42ff-a29d-5c5c88378ddf',IsMasterData='1' WHERE Guid = '2aace173-a616-4da6-a90b-b0222061152b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8a8d42be-00ef-470d-a385-44392499246d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8a8d42be-00ef-470d-a385-44392499246d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '70b5886e-38de-43e4-be93-c83c1f0cdc0a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2315C117', 0.284660053230099, N'8a8d42be-00ef-470d-a385-44392499246d', '1', '70b5886e-38de-43e4-be93-c83c1f0cdc0a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2315C117',Price=0.284660053230099,ManufacturerGuid=N'8a8d42be-00ef-470d-a385-44392499246d',IsMasterData='1' WHERE Guid = '70b5886e-38de-43e4-be93-c83c1f0cdc0a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'12ccfc44-a7e6-46bc-ac5e-758f9e2c2b80')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'12ccfc44-a7e6-46bc-ac5e-758f9e2c2b80')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '034dbeed-3cfe-474c-9de9-5f604d675b4c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316A213', 0.462617953060731, N'12ccfc44-a7e6-46bc-ac5e-758f9e2c2b80', '1', '034dbeed-3cfe-474c-9de9-5f604d675b4c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316A213',Price=0.462617953060731,ManufacturerGuid=N'12ccfc44-a7e6-46bc-ac5e-758f9e2c2b80',IsMasterData='1' WHERE Guid = '034dbeed-3cfe-474c-9de9-5f604d675b4c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1d3f381a-11e8-4109-8d72-69d9454db93d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1d3f381a-11e8-4109-8d72-69d9454db93d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '24b07a85-fd83-4b4f-824e-d445d98cd953')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316A215', 0.0650858940237116, N'1d3f381a-11e8-4109-8d72-69d9454db93d', '1', '24b07a85-fd83-4b4f-824e-d445d98cd953')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316A215',Price=0.0650858940237116,ManufacturerGuid=N'1d3f381a-11e8-4109-8d72-69d9454db93d',IsMasterData='1' WHERE Guid = '24b07a85-fd83-4b4f-824e-d445d98cd953'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a4f2c6f8-3c08-44e3-9a52-31414bceaa98')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a4f2c6f8-3c08-44e3-9a52-31414bceaa98')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5d9db02b-7183-4f2a-9c11-aa855a7b98a1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316A317', 0.103677715944834, N'a4f2c6f8-3c08-44e3-9a52-31414bceaa98', '1', '5d9db02b-7183-4f2a-9c11-aa855a7b98a1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316A317',Price=0.103677715944834,ManufacturerGuid=N'a4f2c6f8-3c08-44e3-9a52-31414bceaa98',IsMasterData='1' WHERE Guid = '5d9db02b-7183-4f2a-9c11-aa855a7b98a1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2617e185-7feb-42f3-8857-dcb6ac435fef')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2617e185-7feb-42f3-8857-dcb6ac435fef')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c6fc63d9-f4fd-4f99-b5bf-33dd96098717')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316A519', 0.169368497459473, N'2617e185-7feb-42f3-8857-dcb6ac435fef', '1', 'c6fc63d9-f4fd-4f99-b5bf-33dd96098717')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316A519',Price=0.169368497459473,ManufacturerGuid=N'2617e185-7feb-42f3-8857-dcb6ac435fef',IsMasterData='1' WHERE Guid = 'c6fc63d9-f4fd-4f99-b5bf-33dd96098717'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6110c868-5922-41d1-87e6-6efe1758b0ff')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6110c868-5922-41d1-87e6-6efe1758b0ff')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9f49292a-2a6b-45bd-842d-8b173b975876')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T051', 0.0246794096298089, N'6110c868-5922-41d1-87e6-6efe1758b0ff', '1', '9f49292a-2a6b-45bd-842d-8b173b975876')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T051',Price=0.0246794096298089,ManufacturerGuid=N'6110c868-5922-41d1-87e6-6efe1758b0ff',IsMasterData='1' WHERE Guid = '9f49292a-2a6b-45bd-842d-8b173b975876'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d6de274-0da3-4d99-81a7-540726207797')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d6de274-0da3-4d99-81a7-540726207797')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ad78720c-546c-48bf-b21d-6b044dca1c07')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T052', 0.0381079119283813, N'2d6de274-0da3-4d99-81a7-540726207797', '1', 'ad78720c-546c-48bf-b21d-6b044dca1c07')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T052',Price=0.0381079119283813,ManufacturerGuid=N'2d6de274-0da3-4d99-81a7-540726207797',IsMasterData='1' WHERE Guid = 'ad78720c-546c-48bf-b21d-6b044dca1c07'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'20eef0fd-6c4e-489c-9ec1-e9b19d643370')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'20eef0fd-6c4e-489c-9ec1-e9b19d643370')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7d1fb24f-7394-4653-97e5-42b88c4efe37')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T135', 0.0362932494556013, N'20eef0fd-6c4e-489c-9ec1-e9b19d643370', '1', '7d1fb24f-7394-4653-97e5-42b88c4efe37')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T135',Price=0.0362932494556013,ManufacturerGuid=N'20eef0fd-6c4e-489c-9ec1-e9b19d643370',IsMasterData='1' WHERE Guid = '7d1fb24f-7394-4653-97e5-42b88c4efe37'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d0ba6cc-e5b0-419b-8e44-c05337022f98')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d0ba6cc-e5b0-419b-8e44-c05337022f98')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dc11fce7-8811-4b65-be96-df14016e4a42')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T244', 0.0331478345027825, N'2d0ba6cc-e5b0-419b-8e44-c05337022f98', '1', 'dc11fce7-8811-4b65-be96-df14016e4a42')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T244',Price=0.0331478345027825,ManufacturerGuid=N'2d0ba6cc-e5b0-419b-8e44-c05337022f98',IsMasterData='1' WHERE Guid = 'dc11fce7-8811-4b65-be96-df14016e4a42'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'786c9b54-c893-46bc-b4de-31f29ceb3597')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'786c9b54-c893-46bc-b4de-31f29ceb3597')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1fa3ef5a-0c53-42f7-969e-ce1eac595d3b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T535', 0.12920396806194, N'786c9b54-c893-46bc-b4de-31f29ceb3597', '1', '1fa3ef5a-0c53-42f7-969e-ce1eac595d3b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T535',Price=0.12920396806194,ManufacturerGuid=N'786c9b54-c893-46bc-b4de-31f29ceb3597',IsMasterData='1' WHERE Guid = '1fa3ef5a-0c53-42f7-969e-ce1eac595d3b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2ca2d707-4bbf-44ee-bf31-755df50dcdbe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2ca2d707-4bbf-44ee-bf31-755df50dcdbe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8ea5153a-0ddc-4421-b08b-52f1f7f68c78')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2316T545', 0.239656423905154, N'2ca2d707-4bbf-44ee-bf31-755df50dcdbe', '1', '8ea5153a-0ddc-4421-b08b-52f1f7f68c78')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2316T545',Price=0.239656423905154,ManufacturerGuid=N'2ca2d707-4bbf-44ee-bf31-755df50dcdbe',IsMasterData='1' WHERE Guid = '8ea5153a-0ddc-4421-b08b-52f1f7f68c78'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'638fc4fb-ad0b-424c-a309-b0e553f7b7a6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'638fc4fb-ad0b-424c-a309-b0e553f7b7a6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4218c0a2-9823-4777-92eb-9241f130bab1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3211C002', 0.0342366319864505, N'638fc4fb-ad0b-424c-a309-b0e553f7b7a6', '1', '4218c0a2-9823-4777-92eb-9241f130bab1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3211C002',Price=0.0342366319864505,ManufacturerGuid=N'638fc4fb-ad0b-424c-a309-b0e553f7b7a6',IsMasterData='1' WHERE Guid = '4218c0a2-9823-4777-92eb-9241f130bab1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0cea902d-8b7a-4662-9034-05e4da851fce')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0cea902d-8b7a-4662-9034-05e4da851fce')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e4077801-b02e-4c9f-a5ab-6046fc6ae560')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32162203', 0.0965400435518993, N'0cea902d-8b7a-4662-9034-05e4da851fce', '1', 'e4077801-b02e-4c9f-a5ab-6046fc6ae560')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32162203',Price=0.0965400435518993,ManufacturerGuid=N'0cea902d-8b7a-4662-9034-05e4da851fce',IsMasterData='1' WHERE Guid = 'e4077801-b02e-4c9f-a5ab-6046fc6ae560'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'76e0cca6-6caa-4948-af89-02437f854e78')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'76e0cca6-6caa-4948-af89-02437f854e78')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2c639775-06d7-4d0a-893e-79712199ff87')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32162331', 0.470360512944592, N'76e0cca6-6caa-4948-af89-02437f854e78', '1', '2c639775-06d7-4d0a-893e-79712199ff87')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32162331',Price=0.470360512944592,ManufacturerGuid=N'76e0cca6-6caa-4948-af89-02437f854e78',IsMasterData='1' WHERE Guid = '2c639775-06d7-4d0a-893e-79712199ff87'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6518bfd2-0376-4234-90ed-c23bea7db65a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6518bfd2-0376-4234-90ed-c23bea7db65a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd363054f-244a-4a05-b2f4-25f63d15e0cf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32166219', 0.29034599564481, N'6518bfd2-0376-4234-90ed-c23bea7db65a', '1', 'd363054f-244a-4a05-b2f4-25f63d15e0cf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32166219',Price=0.29034599564481,ManufacturerGuid=N'6518bfd2-0376-4234-90ed-c23bea7db65a',IsMasterData='1' WHERE Guid = 'd363054f-244a-4a05-b2f4-25f63d15e0cf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5ce11237-8988-4732-93e4-5bf2be537975')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5ce11237-8988-4732-93e4-5bf2be537975')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cda38cc4-168c-4781-a0f1-03f40551a8a0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32166221', 0.326639245100411, N'5ce11237-8988-4732-93e4-5bf2be537975', '1', 'cda38cc4-168c-4781-a0f1-03f40551a8a0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32166221',Price=0.326639245100411,ManufacturerGuid=N'5ce11237-8988-4732-93e4-5bf2be537975',IsMasterData='1' WHERE Guid = 'cda38cc4-168c-4781-a0f1-03f40551a8a0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6c478bae-c455-4fe1-bebe-12da8af4f0e5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6c478bae-c455-4fe1-bebe-12da8af4f0e5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '68910910-5d96-477c-8204-f1632bb7d04c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32166222', 0.435518993467215, N'6c478bae-c455-4fe1-bebe-12da8af4f0e5', '1', '68910910-5d96-477c-8204-f1632bb7d04c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32166222',Price=0.435518993467215,ManufacturerGuid=N'6c478bae-c455-4fe1-bebe-12da8af4f0e5',IsMasterData='1' WHERE Guid = '68910910-5d96-477c-8204-f1632bb7d04c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aa582650-bf48-40e8-b1d0-f377658f2b1f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aa582650-bf48-40e8-b1d0-f377658f2b1f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7ef1e8ac-2f7b-4f79-aac3-08f6233dfa00')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32166329', 0.375030244374546, N'aa582650-bf48-40e8-b1d0-f377658f2b1f', '1', '7ef1e8ac-2f7b-4f79-aac3-08f6233dfa00')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32166329',Price=0.375030244374546,ManufacturerGuid=N'aa582650-bf48-40e8-b1d0-f377658f2b1f',IsMasterData='1' WHERE Guid = '7ef1e8ac-2f7b-4f79-aac3-08f6233dfa00'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d9e465c-f9d3-412f-a66f-fdf325ae2603')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d9e465c-f9d3-412f-a66f-fdf325ae2603')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c82eaa50-7976-4d7a-a95b-bb0ba73cdee7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32181442', 0.308492620372611, N'2d9e465c-f9d3-412f-a66f-fdf325ae2603', '1', 'c82eaa50-7976-4d7a-a95b-bb0ba73cdee7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32181442',Price=0.308492620372611,ManufacturerGuid=N'2d9e465c-f9d3-412f-a66f-fdf325ae2603',IsMasterData='1' WHERE Guid = 'c82eaa50-7976-4d7a-a95b-bb0ba73cdee7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e8e9ffca-3eb1-4943-9b0e-d407b6cf2575')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e8e9ffca-3eb1-4943-9b0e-d407b6cf2575')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '577e1c43-f245-4585-95df-ee27f79b777a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32186137', 0.169368497459473, N'e8e9ffca-3eb1-4943-9b0e-d407b6cf2575', '1', '577e1c43-f245-4585-95df-ee27f79b777a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32186137',Price=0.169368497459473,ManufacturerGuid=N'e8e9ffca-3eb1-4943-9b0e-d407b6cf2575',IsMasterData='1' WHERE Guid = '577e1c43-f245-4585-95df-ee27f79b777a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a0f4a82f-ab50-468f-9fcb-ad38f7874d2e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a0f4a82f-ab50-468f-9fcb-ad38f7874d2e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2a3cfc28-9464-4c1a-ac05-746b4d45c4be')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32186139', 0.145172997822405, N'a0f4a82f-ab50-468f-9fcb-ad38f7874d2e', '1', '2a3cfc28-9464-4c1a-ac05-746b4d45c4be')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32186139',Price=0.145172997822405,ManufacturerGuid=N'a0f4a82f-ab50-468f-9fcb-ad38f7874d2e',IsMasterData='1' WHERE Guid = '2a3cfc28-9464-4c1a-ac05-746b4d45c4be'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'15c8f94e-acda-4f24-a42d-1958e84d6db9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'15c8f94e-acda-4f24-a42d-1958e84d6db9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '86a42ebe-c5c6-4baf-ba0a-f69db213b4cc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32186145', 0.181466247278006, N'15c8f94e-acda-4f24-a42d-1958e84d6db9', '1', '86a42ebe-c5c6-4baf-ba0a-f69db213b4cc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32186145',Price=0.181466247278006,ManufacturerGuid=N'15c8f94e-acda-4f24-a42d-1958e84d6db9',IsMasterData='1' WHERE Guid = '86a42ebe-c5c6-4baf-ba0a-f69db213b4cc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5bb2221c-404c-41ea-9355-107982bda16e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5bb2221c-404c-41ea-9355-107982bda16e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2a7e7a32-b7f4-4624-977a-199a95cac918')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'32186146', 0.205661746915074, N'5bb2221c-404c-41ea-9355-107982bda16e', '1', '2a7e7a32-b7f4-4624-977a-199a95cac918')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'32186146',Price=0.205661746915074,ManufacturerGuid=N'5bb2221c-404c-41ea-9355-107982bda16e',IsMasterData='1' WHERE Guid = '2a7e7a32-b7f4-4624-977a-199a95cac918'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1af2dee1-2a16-467d-916a-fe9ec7fae168')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1af2dee1-2a16-467d-916a-fe9ec7fae168')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c0eb4d40-1009-415c-b8e2-63e059e4ed0e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A014', 0.09678199854827, N'1af2dee1-2a16-467d-916a-fe9ec7fae168', '1', 'c0eb4d40-1009-415c-b8e2-63e059e4ed0e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A014',Price=0.09678199854827,ManufacturerGuid=N'1af2dee1-2a16-467d-916a-fe9ec7fae168',IsMasterData='1' WHERE Guid = 'c0eb4d40-1009-415c-b8e2-63e059e4ed0e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4e991cd2-a8f8-4ce2-9e17-f8d6d7f7a4e8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4e991cd2-a8f8-4ce2-9e17-f8d6d7f7a4e8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd707f4ae-ba8b-4bfd-a3ad-2871a5ab16d6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A016', 0.0969029760464554, N'4e991cd2-a8f8-4ce2-9e17-f8d6d7f7a4e8', '1', 'd707f4ae-ba8b-4bfd-a3ad-2871a5ab16d6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A016',Price=0.0969029760464554,ManufacturerGuid=N'4e991cd2-a8f8-4ce2-9e17-f8d6d7f7a4e8',IsMasterData='1' WHERE Guid = 'd707f4ae-ba8b-4bfd-a3ad-2871a5ab16d6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'401539ae-7f1c-496a-aca8-b120f7fbfe54')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'401539ae-7f1c-496a-aca8-b120f7fbfe54')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3e4827b7-dc6b-4702-9f5e-05163ff5861c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A017', 0.786837648197435, N'401539ae-7f1c-496a-aca8-b120f7fbfe54', '1', '3e4827b7-dc6b-4702-9f5e-05163ff5861c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A017',Price=0.786837648197435,ManufacturerGuid=N'401539ae-7f1c-496a-aca8-b120f7fbfe54',IsMasterData='1' WHERE Guid = '3e4827b7-dc6b-4702-9f5e-05163ff5861c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61fec6c7-dd45-4f31-bd3a-c9cda281d202')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61fec6c7-dd45-4f31-bd3a-c9cda281d202')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8490ee95-c0c0-457a-b36e-0c36b73a7527')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A018', 0.336196467457053, N'61fec6c7-dd45-4f31-bd3a-c9cda281d202', '1', '8490ee95-c0c0-457a-b36e-0c36b73a7527')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A018',Price=0.336196467457053,ManufacturerGuid=N'61fec6c7-dd45-4f31-bd3a-c9cda281d202',IsMasterData='1' WHERE Guid = '8490ee95-c0c0-457a-b36e-0c36b73a7527'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'510eaedc-492b-4607-a6c7-bc921442bd41')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'510eaedc-492b-4607-a6c7-bc921442bd41')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5c948e8b-6f7e-4f85-afd4-f01c847fa0a4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A019', 1.05552867166707, N'510eaedc-492b-4607-a6c7-bc921442bd41', '1', '5c948e8b-6f7e-4f85-afd4-f01c847fa0a4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A019',Price=1.05552867166707,ManufacturerGuid=N'510eaedc-492b-4607-a6c7-bc921442bd41',IsMasterData='1' WHERE Guid = '5c948e8b-6f7e-4f85-afd4-f01c847fa0a4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9ecb10b7-144f-4c01-802c-8fecac70edf3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9ecb10b7-144f-4c01-802c-8fecac70edf3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '862bb07a-ff89-48c3-bda5-2dbb826b6588')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A025', 0.289741108153883, N'9ecb10b7-144f-4c01-802c-8fecac70edf3', '1', '862bb07a-ff89-48c3-bda5-2dbb826b6588')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A025',Price=0.289741108153883,ManufacturerGuid=N'9ecb10b7-144f-4c01-802c-8fecac70edf3',IsMasterData='1' WHERE Guid = '862bb07a-ff89-48c3-bda5-2dbb826b6588'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ab150b88-b642-410e-8f9e-dfc04e7252ee')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ab150b88-b642-410e-8f9e-dfc04e7252ee')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a75ceac4-9f0f-431d-a666-557e5df17bda')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218A512', 0.525889184611662, N'ab150b88-b642-410e-8f9e-dfc04e7252ee', '1', 'a75ceac4-9f0f-431d-a666-557e5df17bda')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218A512',Price=0.525889184611662,ManufacturerGuid=N'ab150b88-b642-410e-8f9e-dfc04e7252ee',IsMasterData='1' WHERE Guid = 'a75ceac4-9f0f-431d-a666-557e5df17bda'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cab6c9a8-0619-4c83-a132-ff1e05123de9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cab6c9a8-0619-4c83-a132-ff1e05123de9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4a864b6c-ba38-4961-bb61-60425fa14f5f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218H006', 0.147834502782482, N'cab6c9a8-0619-4c83-a132-ff1e05123de9', '1', '4a864b6c-ba38-4961-bb61-60425fa14f5f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218H006',Price=0.147834502782482,ManufacturerGuid=N'cab6c9a8-0619-4c83-a132-ff1e05123de9',IsMasterData='1' WHERE Guid = '4a864b6c-ba38-4961-bb61-60425fa14f5f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c7b5dc23-aed1-48ea-b0f6-23c1be85c6c8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c7b5dc23-aed1-48ea-b0f6-23c1be85c6c8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd2815450-0e87-437d-a3a9-63e3cc5ef714')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218J013', 0.172997822405033, N'c7b5dc23-aed1-48ea-b0f6-23c1be85c6c8', '1', 'd2815450-0e87-437d-a3a9-63e3cc5ef714')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218J013',Price=0.172997822405033,ManufacturerGuid=N'c7b5dc23-aed1-48ea-b0f6-23c1be85c6c8',IsMasterData='1' WHERE Guid = 'd2815450-0e87-437d-a3a9-63e3cc5ef714'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'eb79137b-6744-4524-ab83-535781647e56')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'eb79137b-6744-4524-ab83-535781647e56')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a8499a5e-c193-4058-83be-75df6ae12307')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218J025', 0.326639245100411, N'eb79137b-6744-4524-ab83-535781647e56', '1', 'a8499a5e-c193-4058-83be-75df6ae12307')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218J025',Price=0.326639245100411,ManufacturerGuid=N'eb79137b-6744-4524-ab83-535781647e56',IsMasterData='1' WHERE Guid = 'a8499a5e-c193-4058-83be-75df6ae12307'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0f922396-ec81-4b27-8725-2d15623dfee4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0f922396-ec81-4b27-8725-2d15623dfee4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4070ab96-0d97-4ae8-9241-5c023d8d7b87')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3218J026', 0.577546576336801, N'0f922396-ec81-4b27-8725-2d15623dfee4', '1', '4070ab96-0d97-4ae8-9241-5c023d8d7b87')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3218J026',Price=0.577546576336801,ManufacturerGuid=N'0f922396-ec81-4b27-8725-2d15623dfee4',IsMasterData='1' WHERE Guid = '4070ab96-0d97-4ae8-9241-5c023d8d7b87'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f731eb02-3f8a-41f6-9f61-73034c29a32a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f731eb02-3f8a-41f6-9f61-73034c29a32a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bd896dfe-e59a-47c3-b96d-09fce5a21213')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3278A019', 1.60900072586499, N'f731eb02-3f8a-41f6-9f61-73034c29a32a', '1', 'bd896dfe-e59a-47c3-b96d-09fce5a21213')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3278A019',Price=1.60900072586499,ManufacturerGuid=N'f731eb02-3f8a-41f6-9f61-73034c29a32a',IsMasterData='1' WHERE Guid = 'bd896dfe-e59a-47c3-b96d-09fce5a21213'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd548c0a4-156e-4eee-a02b-88c6095b364c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd548c0a4-156e-4eee-a02b-88c6095b364c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5b97e1a4-eafa-4b48-8f9a-09ab53ce1343')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3283A001', 0.133075248003871, N'd548c0a4-156e-4eee-a02b-88c6095b364c', '1', '5b97e1a4-eafa-4b48-8f9a-09ab53ce1343')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3283A001',Price=0.133075248003871,ManufacturerGuid=N'd548c0a4-156e-4eee-a02b-88c6095b364c',IsMasterData='1' WHERE Guid = '5b97e1a4-eafa-4b48-8f9a-09ab53ce1343'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fcf32fac-aa92-43e3-9dc3-2ddb1ecca97e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fcf32fac-aa92-43e3-9dc3-2ddb1ecca97e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0f0661e3-baec-4862-a8f5-541f22ec1aab')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'3212V003', 0.133075248003871, N'fcf32fac-aa92-43e3-9dc3-2ddb1ecca97e', '1', '0f0661e3-baec-4862-a8f5-541f22ec1aab')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'3212V003',Price=0.133075248003871,ManufacturerGuid=N'fcf32fac-aa92-43e3-9dc3-2ddb1ecca97e',IsMasterData='1' WHERE Guid = '0f0661e3-baec-4862-a8f5-541f22ec1aab'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'98a53c49-fc87-4f48-892d-8db58ca47f92')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'98a53c49-fc87-4f48-892d-8db58ca47f92')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd8d76855-b89a-483c-bcae-879d6a6f17d7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2311D062', 0.030123397048149, N'98a53c49-fc87-4f48-892d-8db58ca47f92', '1', 'd8d76855-b89a-483c-bcae-879d6a6f17d7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2311D062',Price=0.030123397048149,ManufacturerGuid=N'98a53c49-fc87-4f48-892d-8db58ca47f92',IsMasterData='1' WHERE Guid = 'd8d76855-b89a-483c-bcae-879d6a6f17d7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'474218ae-2616-437f-b9b4-d78041b5d99d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'474218ae-2616-437f-b9b4-d78041b5d99d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1e62206f-ede3-46fc-aac4-d7a85027c20d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314H027', 1.01621098475684, N'474218ae-2616-437f-b9b4-d78041b5d99d', '1', '1e62206f-ede3-46fc-aac4-d7a85027c20d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314H027',Price=1.01621098475684,ManufacturerGuid=N'474218ae-2616-437f-b9b4-d78041b5d99d',IsMasterData='1' WHERE Guid = '1e62206f-ede3-46fc-aac4-d7a85027c20d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'43554755-cf4c-4f6f-b373-19f2590a2062')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'43554755-cf4c-4f6f-b373-19f2590a2062')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ec60cf6a-a10d-4f24-b9b7-c8df2b924a6d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2314J716', 0.250302443745463, N'43554755-cf4c-4f6f-b373-19f2590a2062', '1', 'ec60cf6a-a10d-4f24-b9b7-c8df2b924a6d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2314J716',Price=0.250302443745463,ManufacturerGuid=N'43554755-cf4c-4f6f-b373-19f2590a2062',IsMasterData='1' WHERE Guid = 'ec60cf6a-a10d-4f24-b9b7-c8df2b924a6d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8fa6c22b-c801-4f25-9d70-920efce80b79')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8fa6c22b-c801-4f25-9d70-920efce80b79')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a421b544-c0fe-43b0-aff9-f905d9d6d4eb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SCREW', N'2321693', 0.0955722235664166, N'8fa6c22b-c801-4f25-9d70-920efce80b79', '1', 'a421b544-c0fe-43b0-aff9-f905d9d6d4eb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SCREW',PartNumber=N'2321693',Price=0.0955722235664166,ManufacturerGuid=N'8fa6c22b-c801-4f25-9d70-920efce80b79',IsMasterData='1' WHERE Guid = 'a421b544-c0fe-43b0-aff9-f905d9d6d4eb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4d5b75e9-4cdd-4fbe-aa0f-96b3c61156a9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4d5b75e9-4cdd-4fbe-aa0f-96b3c61156a9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0b1d58e3-3ed7-4a20-9d98-3ebc07de5893')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'3311K015', 0.0648439390273409, N'4d5b75e9-4cdd-4fbe-aa0f-96b3c61156a9', '1', '0b1d58e3-3ed7-4a20-9d98-3ebc07de5893')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'3311K015',Price=0.0648439390273409,ManufacturerGuid=N'4d5b75e9-4cdd-4fbe-aa0f-96b3c61156a9',IsMasterData='1' WHERE Guid = '0b1d58e3-3ed7-4a20-9d98-3ebc07de5893'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'37a01335-8c39-4aa8-b2e0-929324ae81a9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'37a01335-8c39-4aa8-b2e0-929324ae81a9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b6d0f53d-e3b6-4fd3-8852-3e1382fb1cf8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'33121416', 0.0689571739656424, N'37a01335-8c39-4aa8-b2e0-929324ae81a9', '1', 'b6d0f53d-e3b6-4fd3-8852-3e1382fb1cf8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'33121416',Price=0.0689571739656424,ManufacturerGuid=N'37a01335-8c39-4aa8-b2e0-929324ae81a9',IsMasterData='1' WHERE Guid = 'b6d0f53d-e3b6-4fd3-8852-3e1382fb1cf8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8790a0b3-d0bb-4f87-b540-660469f7b462')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8790a0b3-d0bb-4f87-b540-660469f7b462')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8d2f1e6f-9bce-46c7-bbff-451dc7fe0be5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'33121417', 0.0313331720300024, N'8790a0b3-d0bb-4f87-b540-660469f7b462', '1', '8d2f1e6f-9bce-46c7-bbff-451dc7fe0be5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'33121417',Price=0.0313331720300024,ManufacturerGuid=N'8790a0b3-d0bb-4f87-b540-660469f7b462',IsMasterData='1' WHERE Guid = '8d2f1e6f-9bce-46c7-bbff-451dc7fe0be5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0f0ff1fc-0cd5-4ae9-8e0a-b8003bc8e13a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0f0ff1fc-0cd5-4ae9-8e0a-b8003bc8e13a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '638f7afd-dbb2-40d0-a1c9-442f3f509eb0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'3339612', 1.52431647713525, N'0f0ff1fc-0cd5-4ae9-8e0a-b8003bc8e13a', '1', '638f7afd-dbb2-40d0-a1c9-442f3f509eb0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'3339612',Price=1.52431647713525,ManufacturerGuid=N'0f0ff1fc-0cd5-4ae9-8e0a-b8003bc8e13a',IsMasterData='1' WHERE Guid = '638f7afd-dbb2-40d0-a1c9-442f3f509eb0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61247a72-6b11-4d54-9dfa-fd2e4cb78855')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61247a72-6b11-4d54-9dfa-fd2e4cb78855')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '60ca837d-3a33-401e-a92a-0dfabceb3a2e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'3339613', 1.45172997822405, N'61247a72-6b11-4d54-9dfa-fd2e4cb78855', '1', '60ca837d-3a33-401e-a92a-0dfabceb3a2e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'3339613',Price=1.45172997822405,ManufacturerGuid=N'61247a72-6b11-4d54-9dfa-fd2e4cb78855',IsMasterData='1' WHERE Guid = '60ca837d-3a33-401e-a92a-0dfabceb3a2e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b5afd3e5-7879-4ed1-bb71-ab963d2ebfb7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b5afd3e5-7879-4ed1-bb71-ab963d2ebfb7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b308db4b-a9aa-4bc1-a9e3-df615cb2e8f6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'3426507', 1.9356399709654, N'b5afd3e5-7879-4ed1-bb71-ab963d2ebfb7', '1', 'b308db4b-a9aa-4bc1-a9e3-df615cb2e8f6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'3426507',Price=1.9356399709654,ManufacturerGuid=N'b5afd3e5-7879-4ed1-bb71-ab963d2ebfb7',IsMasterData='1' WHERE Guid = 'b308db4b-a9aa-4bc1-a9e3-df615cb2e8f6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'500388f1-2d8d-4af6-8a8a-c57d4527b95d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'500388f1-2d8d-4af6-8a8a-c57d4527b95d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '114d1063-4a38-4480-a9d3-c442a838e9b9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SHIM', N'3426508', 2.58891846116622, N'500388f1-2d8d-4af6-8a8a-c57d4527b95d', '1', '114d1063-4a38-4480-a9d3-c442a838e9b9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SHIM',PartNumber=N'3426508',Price=2.58891846116622,ManufacturerGuid=N'500388f1-2d8d-4af6-8a8a-c57d4527b95d',IsMasterData='1' WHERE Guid = '114d1063-4a38-4480-a9d3-c442a838e9b9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'11cf6c98-a1e3-47fb-b8b2-650b78d07e40')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'11cf6c98-a1e3-47fb-b8b2-650b78d07e40')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e494ba94-57aa-4358-ba44-29ca59c2885f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3316254', 0.471812242922816, N'11cf6c98-a1e3-47fb-b8b2-650b78d07e40', '1', 'e494ba94-57aa-4358-ba44-29ca59c2885f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3316254',Price=0.471812242922816,ManufacturerGuid=N'11cf6c98-a1e3-47fb-b8b2-650b78d07e40',IsMasterData='1' WHERE Guid = 'e494ba94-57aa-4358-ba44-29ca59c2885f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1f05f5ba-5d7b-4aec-bbce-910a267068e7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1f05f5ba-5d7b-4aec-bbce-910a267068e7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b26879fb-ab83-4ea9-afe4-24571b7c1d22')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3400223', 0.713767239293491, N'1f05f5ba-5d7b-4aec-bbce-910a267068e7', '1', 'b26879fb-ab83-4ea9-afe4-24571b7c1d22')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3400223',Price=0.713767239293491,ManufacturerGuid=N'1f05f5ba-5d7b-4aec-bbce-910a267068e7',IsMasterData='1' WHERE Guid = 'b26879fb-ab83-4ea9-afe4-24571b7c1d22'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3f4b2b9c-a2a2-4361-a3a9-d59e30d14160')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3f4b2b9c-a2a2-4361-a3a9-d59e30d14160')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1bc27181-95e3-4088-a8ed-6481b80ab4a0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3401190', 0.604887490926688, N'3f4b2b9c-a2a2-4361-a3a9-d59e30d14160', '1', '1bc27181-95e3-4088-a8ed-6481b80ab4a0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3401190',Price=0.604887490926688,ManufacturerGuid=N'3f4b2b9c-a2a2-4361-a3a9-d59e30d14160',IsMasterData='1' WHERE Guid = '1bc27181-95e3-4088-a8ed-6481b80ab4a0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'73494eab-587d-4aa0-ab79-ac19bac8b2ef')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'73494eab-587d-4aa0-ab79-ac19bac8b2ef')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '231b14e9-305e-4e78-a4f8-632c2fc4d4d8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3836037', 0.919428986208565, N'73494eab-587d-4aa0-ab79-ac19bac8b2ef', '1', '231b14e9-305e-4e78-a4f8-632c2fc4d4d8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3836037',Price=0.919428986208565,ManufacturerGuid=N'73494eab-587d-4aa0-ab79-ac19bac8b2ef',IsMasterData='1' WHERE Guid = '231b14e9-305e-4e78-a4f8-632c2fc4d4d8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d6ff5f5-d123-4893-8074-f64def5747ff')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d6ff5f5-d123-4893-8074-f64def5747ff')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd7f479ca-c2c3-423d-807e-26051b7d213a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0330600', 0.624243890636342, N'2d6ff5f5-d123-4893-8074-f64def5747ff', '1', 'd7f479ca-c2c3-423d-807e-26051b7d213a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0330600',Price=0.624243890636342,ManufacturerGuid=N'2d6ff5f5-d123-4893-8074-f64def5747ff',IsMasterData='1' WHERE Guid = 'd7f479ca-c2c3-423d-807e-26051b7d213a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7d06a12e-63e8-4f47-b939-8d0926980a3f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7d06a12e-63e8-4f47-b939-8d0926980a3f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd23c08df-1330-43ee-b6d8-6fed4f7392b2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0330752', 0.544398741834019, N'7d06a12e-63e8-4f47-b939-8d0926980a3f', '1', 'd23c08df-1330-43ee-b6d8-6fed4f7392b2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0330752',Price=0.544398741834019,ManufacturerGuid=N'7d06a12e-63e8-4f47-b939-8d0926980a3f',IsMasterData='1' WHERE Guid = 'd23c08df-1330-43ee-b6d8-6fed4f7392b2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ffa86bea-24b7-4509-9a3f-a96fb36ed401')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ffa86bea-24b7-4509-9a3f-a96fb36ed401')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '12a3539a-265e-4b6e-8613-36d4b3c79a80')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0330759', 0.154609242680861, N'ffa86bea-24b7-4509-9a3f-a96fb36ed401', '1', '12a3539a-265e-4b6e-8613-36d4b3c79a80')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0330759',Price=0.154609242680861,ManufacturerGuid=N'ffa86bea-24b7-4509-9a3f-a96fb36ed401',IsMasterData='1' WHERE Guid = '12a3539a-265e-4b6e-8613-36d4b3c79a80'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'710056be-4628-415d-867f-5d2f8a39fddc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'710056be-4628-415d-867f-5d2f8a39fddc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a31d59e6-6d7f-426d-9146-47181c9f0c95')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0910081', 0.568594241471086, N'710056be-4628-415d-867f-5d2f8a39fddc', '1', 'a31d59e6-6d7f-426d-9146-47181c9f0c95')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0910081',Price=0.568594241471086,ManufacturerGuid=N'710056be-4628-415d-867f-5d2f8a39fddc',IsMasterData='1' WHERE Guid = 'a31d59e6-6d7f-426d-9146-47181c9f0c95'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bb13cee8-756e-42b7-a6a9-ccdb45b01884')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bb13cee8-756e-42b7-a6a9-ccdb45b01884')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c05c5719-4971-49c5-9a97-2a224ceb381b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0919997', 0.423421243648681, N'bb13cee8-756e-42b7-a6a9-ccdb45b01884', '1', 'c05c5719-4971-49c5-9a97-2a224ceb381b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0919997',Price=0.423421243648681,ManufacturerGuid=N'bb13cee8-756e-42b7-a6a9-ccdb45b01884',IsMasterData='1' WHERE Guid = 'c05c5719-4971-49c5-9a97-2a224ceb381b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a9783044-9eb6-475d-a287-7947f3afc88f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a9783044-9eb6-475d-a287-7947f3afc88f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0677911f-3b62-41dd-9e31-05da1bda9fe6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920433', 0.0685942414710864, N'a9783044-9eb6-475d-a287-7947f3afc88f', '1', '0677911f-3b62-41dd-9e31-05da1bda9fe6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920433',Price=0.0685942414710864,ManufacturerGuid=N'a9783044-9eb6-475d-a287-7947f3afc88f',IsMasterData='1' WHERE Guid = '0677911f-3b62-41dd-9e31-05da1bda9fe6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd472495b-d79d-4590-a2b4-60b6551648d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd472495b-d79d-4590-a2b4-60b6551648d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '011d9563-3ddc-44e3-b19f-cc3c8c424461')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920459', 0.135252842971207, N'd472495b-d79d-4590-a2b4-60b6551648d3', '1', '011d9563-3ddc-44e3-b19f-cc3c8c424461')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920459',Price=0.135252842971207,ManufacturerGuid=N'd472495b-d79d-4590-a2b4-60b6551648d3',IsMasterData='1' WHERE Guid = '011d9563-3ddc-44e3-b19f-cc3c8c424461'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'821a580a-cbba-44ed-b016-8842396ffa5c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'821a580a-cbba-44ed-b016-8842396ffa5c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '45bde473-2688-4741-837b-99c87f3c3c63')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920464', 0.332688120009678, N'821a580a-cbba-44ed-b016-8842396ffa5c', '1', '45bde473-2688-4741-837b-99c87f3c3c63')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920464',Price=0.332688120009678,ManufacturerGuid=N'821a580a-cbba-44ed-b016-8842396ffa5c',IsMasterData='1' WHERE Guid = '45bde473-2688-4741-837b-99c87f3c3c63'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'03e0c7ab-7be3-4811-afb8-94e48e4bf403')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'03e0c7ab-7be3-4811-afb8-94e48e4bf403')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6d6b0249-fc21-4a68-9215-85b0bc5fd622')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920465', 0.0603677715944834, N'03e0c7ab-7be3-4811-afb8-94e48e4bf403', '1', '6d6b0249-fc21-4a68-9215-85b0bc5fd622')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920465',Price=0.0603677715944834,ManufacturerGuid=N'03e0c7ab-7be3-4811-afb8-94e48e4bf403',IsMasterData='1' WHERE Guid = '6d6b0249-fc21-4a68-9215-85b0bc5fd622'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b92f0921-808f-4fe5-9754-e5db267d2461')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b92f0921-808f-4fe5-9754-e5db267d2461')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2cc9b56c-784c-4bf7-ae08-abe6bf0bb899')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920481', 0.0719816114202758, N'b92f0921-808f-4fe5-9754-e5db267d2461', '1', '2cc9b56c-784c-4bf7-ae08-abe6bf0bb899')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920481',Price=0.0719816114202758,ManufacturerGuid=N'b92f0921-808f-4fe5-9754-e5db267d2461',IsMasterData='1' WHERE Guid = '2cc9b56c-784c-4bf7-ae08-abe6bf0bb899'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'09fc5296-b590-44c7-af0a-89a2f1064c7d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'09fc5296-b590-44c7-af0a-89a2f1064c7d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0881a0aa-e9db-4dab-a14a-d3e8e990d79f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920490', 0.399225744011614, N'09fc5296-b590-44c7-af0a-89a2f1064c7d', '1', '0881a0aa-e9db-4dab-a14a-d3e8e990d79f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920490',Price=0.399225744011614,ManufacturerGuid=N'09fc5296-b590-44c7-af0a-89a2f1064c7d',IsMasterData='1' WHERE Guid = '0881a0aa-e9db-4dab-a14a-d3e8e990d79f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1fd1f618-25cb-45dd-8b6c-d41a1b5c7ddc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1fd1f618-25cb-45dd-8b6c-d41a1b5c7ddc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '460041c3-bc38-4362-a801-f409ac767d6e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'0920496', 0.0497217517541737, N'1fd1f618-25cb-45dd-8b6c-d41a1b5c7ddc', '1', '460041c3-bc38-4362-a801-f409ac767d6e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'0920496',Price=0.0497217517541737,ManufacturerGuid=N'1fd1f618-25cb-45dd-8b6c-d41a1b5c7ddc',IsMasterData='1' WHERE Guid = '460041c3-bc38-4362-a801-f409ac767d6e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7a6079ce-c274-4a85-9583-931356c14a8f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7a6079ce-c274-4a85-9583-931356c14a8f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfcdd146-c48c-4251-bbe2-df81625f3bfd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'1911782', 1.02830873457537, N'7a6079ce-c274-4a85-9583-931356c14a8f', '1', 'bfcdd146-c48c-4251-bbe2-df81625f3bfd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'1911782',Price=1.02830873457537,ManufacturerGuid=N'7a6079ce-c274-4a85-9583-931356c14a8f',IsMasterData='1' WHERE Guid = 'bfcdd146-c48c-4251-bbe2-df81625f3bfd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2875666b-855b-40bb-a87b-1d470d55a190')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2875666b-855b-40bb-a87b-1d470d55a190')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b57a77f7-85be-44a6-816f-200054dd4ee8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'2131467', 0.350834744737479, N'2875666b-855b-40bb-a87b-1d470d55a190', '1', 'b57a77f7-85be-44a6-816f-200054dd4ee8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'2131467',Price=0.350834744737479,ManufacturerGuid=N'2875666b-855b-40bb-a87b-1d470d55a190',IsMasterData='1' WHERE Guid = 'b57a77f7-85be-44a6-816f-200054dd4ee8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'55253098-3a0e-4498-9005-4904d7efa70c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'55253098-3a0e-4498-9005-4904d7efa70c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cdc4d98f-d146-4a25-9fa1-ab2089e99510')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3278A022', 1.25816598112751, N'55253098-3a0e-4498-9005-4904d7efa70c', '1', 'cdc4d98f-d146-4a25-9fa1-ab2089e99510')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3278A022',Price=1.25816598112751,ManufacturerGuid=N'55253098-3a0e-4498-9005-4904d7efa70c',IsMasterData='1' WHERE Guid = 'cdc4d98f-d146-4a25-9fa1-ab2089e99510'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'71a768f7-67c3-4729-bd1b-a840cbe13055')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'71a768f7-67c3-4729-bd1b-a840cbe13055')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5767c09f-8518-4910-b5a2-eea4b15052e0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A022', 0.168158722477619, N'71a768f7-67c3-4729-bd1b-a840cbe13055', '1', '5767c09f-8518-4910-b5a2-eea4b15052e0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A022',Price=0.168158722477619,ManufacturerGuid=N'71a768f7-67c3-4729-bd1b-a840cbe13055',IsMasterData='1' WHERE Guid = '5767c09f-8518-4910-b5a2-eea4b15052e0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2ff85d44-98ea-41ba-82db-9e8651c532fc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2ff85d44-98ea-41ba-82db-9e8651c532fc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '87f876e9-d659-45a8-9126-a6dae252678c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A029', 0.205661746915074, N'2ff85d44-98ea-41ba-82db-9e8651c532fc', '1', '87f876e9-d659-45a8-9126-a6dae252678c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A029',Price=0.205661746915074,ManufacturerGuid=N'2ff85d44-98ea-41ba-82db-9e8651c532fc',IsMasterData='1' WHERE Guid = '87f876e9-d659-45a8-9126-a6dae252678c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'371b7a5b-639c-4f32-b238-242be3f9ccbc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'371b7a5b-639c-4f32-b238-242be3f9ccbc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9db74c83-8c55-4d90-b15e-7442b8c6f3c8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A031', 0.356883619646746, N'371b7a5b-639c-4f32-b238-242be3f9ccbc', '1', '9db74c83-8c55-4d90-b15e-7442b8c6f3c8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A031',Price=0.356883619646746,ManufacturerGuid=N'371b7a5b-639c-4f32-b238-242be3f9ccbc',IsMasterData='1' WHERE Guid = '9db74c83-8c55-4d90-b15e-7442b8c6f3c8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b34ded00-daab-4d9a-963f-92d809a8e66f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b34ded00-daab-4d9a-963f-92d809a8e66f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a3ad25f4-7b3f-448d-a1d3-4a445d430cf0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A033', 0.793370433099444, N'b34ded00-daab-4d9a-963f-92d809a8e66f', '1', 'a3ad25f4-7b3f-448d-a1d3-4a445d430cf0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A033',Price=0.793370433099444,ManufacturerGuid=N'b34ded00-daab-4d9a-963f-92d809a8e66f',IsMasterData='1' WHERE Guid = 'a3ad25f4-7b3f-448d-a1d3-4a445d430cf0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6b891f42-eca4-4345-8846-5b0207a5bedc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6b891f42-eca4-4345-8846-5b0207a5bedc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c2df0e7f-3e4d-46c6-81a4-7fb1f412ba83')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A036', 0.592789741108154, N'6b891f42-eca4-4345-8846-5b0207a5bedc', '1', 'c2df0e7f-3e4d-46c6-81a4-7fb1f412ba83')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A036',Price=0.592789741108154,ManufacturerGuid=N'6b891f42-eca4-4345-8846-5b0207a5bedc',IsMasterData='1' WHERE Guid = 'c2df0e7f-3e4d-46c6-81a4-7fb1f412ba83'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'181b8189-2c2f-41f8-aaab-dc3ebec46c21')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'181b8189-2c2f-41f8-aaab-dc3ebec46c21')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '267134ce-c117-486c-8bb2-7af37dd96008')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A038', 2.40745221388822, N'181b8189-2c2f-41f8-aaab-dc3ebec46c21', '1', '267134ce-c117-486c-8bb2-7af37dd96008')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A038',Price=2.40745221388822,ManufacturerGuid=N'181b8189-2c2f-41f8-aaab-dc3ebec46c21',IsMasterData='1' WHERE Guid = '267134ce-c117-486c-8bb2-7af37dd96008'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'51d27d06-a7c3-48ce-800e-353dae7add66')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'51d27d06-a7c3-48ce-800e-353dae7add66')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '59f0cb42-e69e-4497-b32d-086cd99c81e5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A039', 0.907331236390031, N'51d27d06-a7c3-48ce-800e-353dae7add66', '1', '59f0cb42-e69e-4497-b32d-086cd99c81e5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A039',Price=0.907331236390031,ManufacturerGuid=N'51d27d06-a7c3-48ce-800e-353dae7add66',IsMasterData='1' WHERE Guid = '59f0cb42-e69e-4497-b32d-086cd99c81e5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3f5c3973-84cc-4e68-87ac-15cdf8a0fac7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3f5c3973-84cc-4e68-87ac-15cdf8a0fac7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'df201eae-a391-4ecd-8735-25aacf994ec0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311A051', 0.9678199854827, N'3f5c3973-84cc-4e68-87ac-15cdf8a0fac7', '1', 'df201eae-a391-4ecd-8735-25aacf994ec0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311A051',Price=0.9678199854827,ManufacturerGuid=N'3f5c3973-84cc-4e68-87ac-15cdf8a0fac7',IsMasterData='1' WHERE Guid = 'df201eae-a391-4ecd-8735-25aacf994ec0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'35874677-9ece-4f7c-b2e1-cb04f7f3dce9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'35874677-9ece-4f7c-b2e1-cb04f7f3dce9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '785615ef-36e9-4a6e-847c-26388617ac34')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3311X002', 0.80304863295427, N'35874677-9ece-4f7c-b2e1-cb04f7f3dce9', '1', '785615ef-36e9-4a6e-847c-26388617ac34')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3311X002',Price=0.80304863295427,ManufacturerGuid=N'35874677-9ece-4f7c-b2e1-cb04f7f3dce9',IsMasterData='1' WHERE Guid = '785615ef-36e9-4a6e-847c-26388617ac34'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fcdf3497-41e3-412a-97f8-fa61a1afc3cd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fcdf3497-41e3-412a-97f8-fa61a1afc3cd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '11a4c870-dce0-4c2f-9e69-9d631555fb9f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312A306', 0.175417372368739, N'fcdf3497-41e3-412a-97f8-fa61a1afc3cd', '1', '11a4c870-dce0-4c2f-9e69-9d631555fb9f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312A306',Price=0.175417372368739,ManufacturerGuid=N'fcdf3497-41e3-412a-97f8-fa61a1afc3cd',IsMasterData='1' WHERE Guid = '11a4c870-dce0-4c2f-9e69-9d631555fb9f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'100595b6-785c-43a2-8f2a-462fe54da8b3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'100595b6-785c-43a2-8f2a-462fe54da8b3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '59aa6677-a804-4377-9f42-ce0962cef462')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312F008', 0.57706266634406, N'100595b6-785c-43a2-8f2a-462fe54da8b3', '1', '59aa6677-a804-4377-9f42-ce0962cef462')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312F008',Price=0.57706266634406,ManufacturerGuid=N'100595b6-785c-43a2-8f2a-462fe54da8b3',IsMasterData='1' WHERE Guid = '59aa6677-a804-4377-9f42-ce0962cef462'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'895b4dd0-57cc-4c14-94f5-bb1591f0e611')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'895b4dd0-57cc-4c14-94f5-bb1591f0e611')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f658d963-0127-4702-b865-855b14b0af32')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312F011', 1.39124122913138, N'895b4dd0-57cc-4c14-94f5-bb1591f0e611', '1', 'f658d963-0127-4702-b865-855b14b0af32')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312F011',Price=1.39124122913138,ManufacturerGuid=N'895b4dd0-57cc-4c14-94f5-bb1591f0e611',IsMasterData='1' WHERE Guid = 'f658d963-0127-4702-b865-855b14b0af32'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'66b2aac4-2221-4e59-8f9f-73d1bdb4e63c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'66b2aac4-2221-4e59-8f9f-73d1bdb4e63c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '56a1da2e-2d7e-4045-ab46-9c69770b2714')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312F012', 0.481006532784902, N'66b2aac4-2221-4e59-8f9f-73d1bdb4e63c', '1', '56a1da2e-2d7e-4045-ab46-9c69770b2714')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312F012',Price=0.481006532784902,ManufacturerGuid=N'66b2aac4-2221-4e59-8f9f-73d1bdb4e63c',IsMasterData='1' WHERE Guid = '56a1da2e-2d7e-4045-ab46-9c69770b2714'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2b725056-7932-4706-90e7-072119076d29')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2b725056-7932-4706-90e7-072119076d29')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fb6623a4-4519-4cd9-a727-3594415032a1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312K019', 0.496007742559884, N'2b725056-7932-4706-90e7-072119076d29', '1', 'fb6623a4-4519-4cd9-a727-3594415032a1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312K019',Price=0.496007742559884,ManufacturerGuid=N'2b725056-7932-4706-90e7-072119076d29',IsMasterData='1' WHERE Guid = 'fb6623a4-4519-4cd9-a727-3594415032a1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'660c6161-b948-47c3-9ab7-f85eda07a574')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'660c6161-b948-47c3-9ab7-f85eda07a574')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '12aed00c-00ba-438f-bbaa-5439a178a4f4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312K026', 0.28308734575369, N'660c6161-b948-47c3-9ab7-f85eda07a574', '1', '12aed00c-00ba-438f-bbaa-5439a178a4f4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312K026',Price=0.28308734575369,ManufacturerGuid=N'660c6161-b948-47c3-9ab7-f85eda07a574',IsMasterData='1' WHERE Guid = '12aed00c-00ba-438f-bbaa-5439a178a4f4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f9eac11d-fd28-4bbc-8594-c3d45c5321ee')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f9eac11d-fd28-4bbc-8594-c3d45c5321ee')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'faed8be0-3c34-4878-ad29-716b94fe9c59')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3312K301', 0.204572949431406, N'f9eac11d-fd28-4bbc-8594-c3d45c5321ee', '1', 'faed8be0-3c34-4878-ad29-716b94fe9c59')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3312K301',Price=0.204572949431406,ManufacturerGuid=N'f9eac11d-fd28-4bbc-8594-c3d45c5321ee',IsMasterData='1' WHERE Guid = 'faed8be0-3c34-4878-ad29-716b94fe9c59'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4b902f5b-7070-4caa-8fc0-6d99f189ca27')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4b902f5b-7070-4caa-8fc0-6d99f189ca27')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '83d1886f-9db9-482a-81fb-6d0969af0352')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33134418', 0.249455601258166, N'4b902f5b-7070-4caa-8fc0-6d99f189ca27', '1', '83d1886f-9db9-482a-81fb-6d0969af0352')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33134418',Price=0.249455601258166,ManufacturerGuid=N'4b902f5b-7070-4caa-8fc0-6d99f189ca27',IsMasterData='1' WHERE Guid = '83d1886f-9db9-482a-81fb-6d0969af0352'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d21b412-812b-4ae9-86a3-64cd904a62b0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d21b412-812b-4ae9-86a3-64cd904a62b0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '07958735-bcaa-4d77-bcbf-b2b36681b02e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33134419', 0.447616743285749, N'2d21b412-812b-4ae9-86a3-64cd904a62b0', '1', '07958735-bcaa-4d77-bcbf-b2b36681b02e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33134419',Price=0.447616743285749,ManufacturerGuid=N'2d21b412-812b-4ae9-86a3-64cd904a62b0',IsMasterData='1' WHERE Guid = '07958735-bcaa-4d77-bcbf-b2b36681b02e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ac9e8141-1f17-4e5f-8f84-49d11d91d30c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ac9e8141-1f17-4e5f-8f84-49d11d91d30c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ca3de681-e119-4bf2-90af-9cdf2fd22286')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33134427', 0.0621824340672635, N'ac9e8141-1f17-4e5f-8f84-49d11d91d30c', '1', 'ca3de681-e119-4bf2-90af-9cdf2fd22286')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33134427',Price=0.0621824340672635,ManufacturerGuid=N'ac9e8141-1f17-4e5f-8f84-49d11d91d30c',IsMasterData='1' WHERE Guid = 'ca3de681-e119-4bf2-90af-9cdf2fd22286'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4cbbd519-d25e-458c-8de0-9190f27a1591')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4cbbd519-d25e-458c-8de0-9190f27a1591')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '48b07588-e242-4a15-a9dd-182bef816475')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33135115', 0.29034599564481, N'4cbbd519-d25e-458c-8de0-9190f27a1591', '1', '48b07588-e242-4a15-a9dd-182bef816475')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33135115',Price=0.29034599564481,ManufacturerGuid=N'4cbbd519-d25e-458c-8de0-9190f27a1591',IsMasterData='1' WHERE Guid = '48b07588-e242-4a15-a9dd-182bef816475'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b3c59782-6ae4-4cff-bb15-a2a049cc552f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b3c59782-6ae4-4cff-bb15-a2a049cc552f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd913fd81-2fc3-4ae0-9225-fed713175445')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33135413', 0.118195015727075, N'b3c59782-6ae4-4cff-bb15-a2a049cc552f', '1', 'd913fd81-2fc3-4ae0-9225-fed713175445')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33135413',Price=0.118195015727075,ManufacturerGuid=N'b3c59782-6ae4-4cff-bb15-a2a049cc552f',IsMasterData='1' WHERE Guid = 'd913fd81-2fc3-4ae0-9225-fed713175445'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fc0ef541-3aae-4402-8534-2952dddc691c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fc0ef541-3aae-4402-8534-2952dddc691c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4c0bb9eb-65df-4896-838a-104ca9140ab4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33135423', 0.10924268086136, N'fc0ef541-3aae-4402-8534-2952dddc691c', '1', '4c0bb9eb-65df-4896-838a-104ca9140ab4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33135423',Price=0.10924268086136,ManufacturerGuid=N'fc0ef541-3aae-4402-8534-2952dddc691c',IsMasterData='1' WHERE Guid = '4c0bb9eb-65df-4896-838a-104ca9140ab4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'36edc8d6-e5b2-4b58-9c76-f7a452add90e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'36edc8d6-e5b2-4b58-9c76-f7a452add90e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd928cdae-163a-4663-8c28-7c2e82e6fbd2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313A015', 0.278248245826276, N'36edc8d6-e5b2-4b58-9c76-f7a452add90e', '1', 'd928cdae-163a-4663-8c28-7c2e82e6fbd2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313A015',Price=0.278248245826276,ManufacturerGuid=N'36edc8d6-e5b2-4b58-9c76-f7a452add90e',IsMasterData='1' WHERE Guid = 'd928cdae-163a-4663-8c28-7c2e82e6fbd2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'37f05357-f3c3-4754-8b5a-2b1b071ff8c0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'37f05357-f3c3-4754-8b5a-2b1b071ff8c0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '87ffbed6-fc15-4c43-b0dd-7bebd4090433')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313A023', 0.277764335833535, N'37f05357-f3c3-4754-8b5a-2b1b071ff8c0', '1', '87ffbed6-fc15-4c43-b0dd-7bebd4090433')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313A023',Price=0.277764335833535,ManufacturerGuid=N'37f05357-f3c3-4754-8b5a-2b1b071ff8c0',IsMasterData='1' WHERE Guid = '87ffbed6-fc15-4c43-b0dd-7bebd4090433'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'447c6074-7707-4195-9440-ac48a88c04d8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'447c6074-7707-4195-9440-ac48a88c04d8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eee7828b-1f09-4e2f-85df-b17daf194e14')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313A027', 0.114928623276071, N'447c6074-7707-4195-9440-ac48a88c04d8', '1', 'eee7828b-1f09-4e2f-85df-b17daf194e14')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313A027',Price=0.114928623276071,ManufacturerGuid=N'447c6074-7707-4195-9440-ac48a88c04d8',IsMasterData='1' WHERE Guid = 'eee7828b-1f09-4e2f-85df-b17daf194e14'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'081b1887-a933-4593-906f-8ac6d740041f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'081b1887-a933-4593-906f-8ac6d740041f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'deb32b15-fa59-402f-a5be-67ed0414a5dc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313A029', 0.205661746915074, N'081b1887-a933-4593-906f-8ac6d740041f', '1', 'deb32b15-fa59-402f-a5be-67ed0414a5dc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313A029',Price=0.205661746915074,ManufacturerGuid=N'081b1887-a933-4593-906f-8ac6d740041f',IsMasterData='1' WHERE Guid = 'deb32b15-fa59-402f-a5be-67ed0414a5dc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'84422a44-dedf-4f5b-b8f9-816c3a7fc4d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'84422a44-dedf-4f5b-b8f9-816c3a7fc4d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '614b7f2d-e52b-4747-8921-826a0c62d0e7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313A032', 0.0629082990563755, N'84422a44-dedf-4f5b-b8f9-816c3a7fc4d9', '1', '614b7f2d-e52b-4747-8921-826a0c62d0e7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313A032',Price=0.0629082990563755,ManufacturerGuid=N'84422a44-dedf-4f5b-b8f9-816c3a7fc4d9',IsMasterData='1' WHERE Guid = '614b7f2d-e52b-4747-8921-826a0c62d0e7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2b01428a-a82f-4603-b295-fb12b4c4191b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2b01428a-a82f-4603-b295-fb12b4c4191b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4a25cbd9-fdb6-4055-b855-30e8a5c97d60')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313E722', 0.337527219937092, N'2b01428a-a82f-4603-b295-fb12b4c4191b', '1', '4a25cbd9-fdb6-4055-b855-30e8a5c97d60')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313E722',Price=0.337527219937092,ManufacturerGuid=N'2b01428a-a82f-4603-b295-fb12b4c4191b',IsMasterData='1' WHERE Guid = '4a25cbd9-fdb6-4055-b855-30e8a5c97d60'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'126aeb48-e544-4918-acff-4492f30c939f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'126aeb48-e544-4918-acff-4492f30c939f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '934d9a52-a2b7-406f-8dd0-407833d551fb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313E731', 0.508105492378418, N'126aeb48-e544-4918-acff-4492f30c939f', '1', '934d9a52-a2b7-406f-8dd0-407833d551fb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313E731',Price=0.508105492378418,ManufacturerGuid=N'126aeb48-e544-4918-acff-4492f30c939f',IsMasterData='1' WHERE Guid = '934d9a52-a2b7-406f-8dd0-407833d551fb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a74594d1-2da2-44cb-b16b-9d790414b87a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a74594d1-2da2-44cb-b16b-9d790414b87a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9115b99e-a053-4e5b-b78d-86db75913852')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3313E732', 0.297241713041374, N'a74594d1-2da2-44cb-b16b-9d790414b87a', '1', '9115b99e-a053-4e5b-b78d-86db75913852')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3313E732',Price=0.297241713041374,ManufacturerGuid=N'a74594d1-2da2-44cb-b16b-9d790414b87a',IsMasterData='1' WHERE Guid = '9115b99e-a053-4e5b-b78d-86db75913852'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'96d186fe-5876-4089-b465-1f39260ab838')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'96d186fe-5876-4089-b465-1f39260ab838')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a334a120-908f-4ee8-bf03-6f5d46f40143')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3321A011', 0.67747398983789, N'96d186fe-5876-4089-b465-1f39260ab838', '1', 'a334a120-908f-4ee8-bf03-6f5d46f40143')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3321A011',Price=0.67747398983789,ManufacturerGuid=N'96d186fe-5876-4089-b465-1f39260ab838',IsMasterData='1' WHERE Guid = 'a334a120-908f-4ee8-bf03-6f5d46f40143'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'28e32721-6b11-42d5-b05e-152221b58e7b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'28e32721-6b11-42d5-b05e-152221b58e7b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ad027f66-1fce-46a3-9b1c-7dc918d4a82f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33246117', 0.445681103314783, N'28e32721-6b11-42d5-b05e-152221b58e7b', '1', 'ad027f66-1fce-46a3-9b1c-7dc918d4a82f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33246117',Price=0.445681103314783,ManufacturerGuid=N'28e32721-6b11-42d5-b05e-152221b58e7b',IsMasterData='1' WHERE Guid = 'ad027f66-1fce-46a3-9b1c-7dc918d4a82f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7c67413c-76ea-4d30-9223-5d145ffb5404')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7c67413c-76ea-4d30-9223-5d145ffb5404')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cb193296-a9b7-472e-9856-624b43832e6c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33246123', 0.379022501814663, N'7c67413c-76ea-4d30-9223-5d145ffb5404', '1', 'cb193296-a9b7-472e-9856-624b43832e6c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33246123',Price=0.379022501814663,ManufacturerGuid=N'7c67413c-76ea-4d30-9223-5d145ffb5404',IsMasterData='1' WHERE Guid = 'cb193296-a9b7-472e-9856-624b43832e6c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ef3d3235-6514-48a7-8933-a029634cb65a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ef3d3235-6514-48a7-8933-a029634cb65a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '23697c7c-2536-4fc3-a23b-dacd6504ce86')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'33247125', 0.641180740382289, N'ef3d3235-6514-48a7-8933-a029634cb65a', '1', '23697c7c-2536-4fc3-a23b-dacd6504ce86')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'33247125',Price=0.641180740382289,ManufacturerGuid=N'ef3d3235-6514-48a7-8933-a029634cb65a',IsMasterData='1' WHERE Guid = '23697c7c-2536-4fc3-a23b-dacd6504ce86'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5dfb1fef-0cb9-4ac0-8768-847562df98d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5dfb1fef-0cb9-4ac0-8768-847562df98d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfbbe088-3d16-455d-92bd-16c6c361dfe0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3329700', 0.798451488023228, N'5dfb1fef-0cb9-4ac0-8768-847562df98d3', '1', 'bfbbe088-3d16-455d-92bd-16c6c361dfe0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3329700',Price=0.798451488023228,ManufacturerGuid=N'5dfb1fef-0cb9-4ac0-8768-847562df98d3',IsMasterData='1' WHERE Guid = 'bfbbe088-3d16-455d-92bd-16c6c361dfe0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8cd3b6e2-d207-49c8-b874-285dbc68ef2c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8cd3b6e2-d207-49c8-b874-285dbc68ef2c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '13891e09-c273-47f7-86ee-0671104f7bf8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3611A022', 2.27437696588435, N'8cd3b6e2-d207-49c8-b874-285dbc68ef2c', '1', '13891e09-c273-47f7-86ee-0671104f7bf8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3611A022',Price=2.27437696588435,ManufacturerGuid=N'8cd3b6e2-d207-49c8-b874-285dbc68ef2c',IsMasterData='1' WHERE Guid = '13891e09-c273-47f7-86ee-0671104f7bf8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'baefb997-eba0-4035-8aba-77b9641452a1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'baefb997-eba0-4035-8aba-77b9641452a1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'afb3fedc-b034-4cb7-9243-3955ef6e2d2c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3615X012', 0.544398741834019, N'baefb997-eba0-4035-8aba-77b9641452a1', '1', 'afb3fedc-b034-4cb7-9243-3955ef6e2d2c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3615X012',Price=0.544398741834019,ManufacturerGuid=N'baefb997-eba0-4035-8aba-77b9641452a1',IsMasterData='1' WHERE Guid = 'afb3fedc-b034-4cb7-9243-3955ef6e2d2c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'89bca5a5-cd91-4ed5-887b-049032e9ec5d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'89bca5a5-cd91-4ed5-887b-049032e9ec5d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e1f36f32-e268-41ec-b0b0-df291a007b44')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3621A017', 1.42753447858698, N'89bca5a5-cd91-4ed5-887b-049032e9ec5d', '1', 'e1f36f32-e268-41ec-b0b0-df291a007b44')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3621A017',Price=1.42753447858698,ManufacturerGuid=N'89bca5a5-cd91-4ed5-887b-049032e9ec5d',IsMasterData='1' WHERE Guid = 'e1f36f32-e268-41ec-b0b0-df291a007b44'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8dc65315-a3e0-4124-956d-b7085c627552')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8dc65315-a3e0-4124-956d-b7085c627552')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7940099e-0fa0-4c19-a744-7c8de3c828af')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3661A106', 0.535688361964675, N'8dc65315-a3e0-4124-956d-b7085c627552', '1', '7940099e-0fa0-4c19-a744-7c8de3c828af')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3661A106',Price=0.535688361964675,ManufacturerGuid=N'8dc65315-a3e0-4124-956d-b7085c627552',IsMasterData='1' WHERE Guid = '7940099e-0fa0-4c19-a744-7c8de3c828af'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4c7b1630-efb1-4c60-8a9f-5be3121e24b0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4c7b1630-efb1-4c60-8a9f-5be3121e24b0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9222e7c6-3e03-4f46-8bb8-de01f0e2b05f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3477888', 1.0041132349383, N'4c7b1630-efb1-4c60-8a9f-5be3121e24b0', '1', '9222e7c6-3e03-4f46-8bb8-de01f0e2b05f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3477888',Price=1.0041132349383,ManufacturerGuid=N'4c7b1630-efb1-4c60-8a9f-5be3121e24b0',IsMasterData='1' WHERE Guid = '9222e7c6-3e03-4f46-8bb8-de01f0e2b05f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f27f3fa5-d3a8-4b10-b550-3a549bc22fca')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f27f3fa5-d3a8-4b10-b550-3a549bc22fca')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1e00b962-6e7b-4810-8b81-5023423c172c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPACER', N'3617A041', 3.24219695136705, N'f27f3fa5-d3a8-4b10-b550-3a549bc22fca', '1', '1e00b962-6e7b-4810-8b81-5023423c172c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPACER',PartNumber=N'3617A041',Price=3.24219695136705,ManufacturerGuid=N'f27f3fa5-d3a8-4b10-b550-3a549bc22fca',IsMasterData='1' WHERE Guid = '1e00b962-6e7b-4810-8b81-5023423c172c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'9e0b3b69-9aeb-498e-953b-7196ac65ff45')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'9e0b3b69-9aeb-498e-953b-7196ac65ff45')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6a4a6feb-cc45-4262-acea-f5334b40fbcd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPLIT PIN', N'0610042', 0.00435518993467215, N'9e0b3b69-9aeb-498e-953b-7196ac65ff45', '1', '6a4a6feb-cc45-4262-acea-f5334b40fbcd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPLIT PIN',PartNumber=N'0610042',Price=0.00435518993467215,ManufacturerGuid=N'9e0b3b69-9aeb-498e-953b-7196ac65ff45',IsMasterData='1' WHERE Guid = '6a4a6feb-cc45-4262-acea-f5334b40fbcd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'509e95e0-4bd5-4ecc-9fb9-904cf340967e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'509e95e0-4bd5-4ecc-9fb9-904cf340967e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9d5e9a20-9d60-423b-a4c5-38ce7ae6053e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPLIT PIN', N'2117091', 0.0231067021533995, N'509e95e0-4bd5-4ecc-9fb9-904cf340967e', '1', '9d5e9a20-9d60-423b-a4c5-38ce7ae6053e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPLIT PIN',PartNumber=N'2117091',Price=0.0231067021533995,ManufacturerGuid=N'509e95e0-4bd5-4ecc-9fb9-904cf340967e',IsMasterData='1' WHERE Guid = '9d5e9a20-9d60-423b-a4c5-38ce7ae6053e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b30fdd98-5c94-4ec3-8bd9-6237a0978a65')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b30fdd98-5c94-4ec3-8bd9-6237a0978a65')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4b3c423d-3f16-4c2b-9c86-89ae790f0219')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'SPLIT PIN', N'3411E002', 0.0330268570045971, N'b30fdd98-5c94-4ec3-8bd9-6237a0978a65', '1', '4b3c423d-3f16-4c2b-9c86-89ae790f0219')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'SPLIT PIN',PartNumber=N'3411E002',Price=0.0330268570045971,ManufacturerGuid=N'b30fdd98-5c94-4ec3-8bd9-6237a0978a65',IsMasterData='1' WHERE Guid = '4b3c423d-3f16-4c2b-9c86-89ae790f0219'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'300e1f0c-ec94-44b2-9a85-25ce22418ab8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'300e1f0c-ec94-44b2-9a85-25ce22418ab8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aa9d65f7-5612-4d38-b478-e5fb326288a4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'0827813', 0.880716186789257, N'300e1f0c-ec94-44b2-9a85-25ce22418ab8', '1', 'aa9d65f7-5612-4d38-b478-e5fb326288a4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'0827813',Price=0.880716186789257,ManufacturerGuid=N'300e1f0c-ec94-44b2-9a85-25ce22418ab8',IsMasterData='1' WHERE Guid = 'aa9d65f7-5612-4d38-b478-e5fb326288a4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'da07e874-8adf-499a-9e35-ef5f94ab0467')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'da07e874-8adf-499a-9e35-ef5f94ab0467')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '63b5ac2f-2a76-47c0-bea0-5610b741ff57')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'1758P0227', 2.54052746189209, N'da07e874-8adf-499a-9e35-ef5f94ab0467', '1', '63b5ac2f-2a76-47c0-bea0-5610b741ff57')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'1758P0227',Price=2.54052746189209,ManufacturerGuid=N'da07e874-8adf-499a-9e35-ef5f94ab0467',IsMasterData='1' WHERE Guid = '63b5ac2f-2a76-47c0-bea0-5610b741ff57'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5871a701-5cc8-4804-b390-0a02e48bd625')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5871a701-5cc8-4804-b390-0a02e48bd625')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '66dfeb33-d631-45c3-a37d-03fa0c915eb3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2217575', 0.0544398741834019, N'5871a701-5cc8-4804-b390-0a02e48bd625', '1', '66dfeb33-d631-45c3-a37d-03fa0c915eb3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2217575',Price=0.0544398741834019,ManufacturerGuid=N'5871a701-5cc8-4804-b390-0a02e48bd625',IsMasterData='1' WHERE Guid = '66dfeb33-d631-45c3-a37d-03fa0c915eb3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f279474d-ddf2-4948-ab0b-cd109e7568d1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f279474d-ddf2-4948-ab0b-cd109e7568d1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '637d848f-aaf5-440a-9341-79738c60bc1e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C058', 1.41120251633196, N'f279474d-ddf2-4948-ab0b-cd109e7568d1', '1', '637d848f-aaf5-440a-9341-79738c60bc1e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C058',Price=1.41120251633196,ManufacturerGuid=N'f279474d-ddf2-4948-ab0b-cd109e7568d1',IsMasterData='1' WHERE Guid = '637d848f-aaf5-440a-9341-79738c60bc1e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2573a444-319f-48a1-90c8-17e0a4b78ee9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2573a444-319f-48a1-90c8-17e0a4b78ee9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a1050f02-aade-4bb9-9990-d16ff5a4951b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C062', 0.0623034115654488, N'2573a444-319f-48a1-90c8-17e0a4b78ee9', '1', 'a1050f02-aade-4bb9-9990-d16ff5a4951b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C062',Price=0.0623034115654488,ManufacturerGuid=N'2573a444-319f-48a1-90c8-17e0a4b78ee9',IsMasterData='1' WHERE Guid = 'a1050f02-aade-4bb9-9990-d16ff5a4951b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd4af6d84-a6f2-4754-85ca-79f6cf7c22d1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd4af6d84-a6f2-4754-85ca-79f6cf7c22d1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '400ed43a-59ed-4aaf-a65c-698dce6576bb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C063', 0.163319622550206, N'd4af6d84-a6f2-4754-85ca-79f6cf7c22d1', '1', '400ed43a-59ed-4aaf-a65c-698dce6576bb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C063',Price=0.163319622550206,ManufacturerGuid=N'd4af6d84-a6f2-4754-85ca-79f6cf7c22d1',IsMasterData='1' WHERE Guid = '400ed43a-59ed-4aaf-a65c-698dce6576bb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ef4a0e78-5f99-4358-b668-4e69b93c57ed')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ef4a0e78-5f99-4358-b668-4e69b93c57ed')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '303a8d88-1429-4fe2-8a08-03cd497f83de')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C064', 0.0537140091942899, N'ef4a0e78-5f99-4358-b668-4e69b93c57ed', '1', '303a8d88-1429-4fe2-8a08-03cd497f83de')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C064',Price=0.0537140091942899,ManufacturerGuid=N'ef4a0e78-5f99-4358-b668-4e69b93c57ed',IsMasterData='1' WHERE Guid = '303a8d88-1429-4fe2-8a08-03cd497f83de'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'110c3eef-086a-4a16-b6a7-812b8633d5a6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'110c3eef-086a-4a16-b6a7-812b8633d5a6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b2629f83-fe57-4547-9589-b1012730c147')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C065', 0.108879748366804, N'110c3eef-086a-4a16-b6a7-812b8633d5a6', '1', 'b2629f83-fe57-4547-9589-b1012730c147')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C065',Price=0.108879748366804,ManufacturerGuid=N'110c3eef-086a-4a16-b6a7-812b8633d5a6',IsMasterData='1' WHERE Guid = 'b2629f83-fe57-4547-9589-b1012730c147'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'68918ca7-1aec-4c38-9e50-64d05e04ac81')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'68918ca7-1aec-4c38-9e50-64d05e04ac81')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0023867b-24b2-45a1-b82c-57c035627cef')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C069', 0.208323251875151, N'68918ca7-1aec-4c38-9e50-64d05e04ac81', '1', '0023867b-24b2-45a1-b82c-57c035627cef')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C069',Price=0.208323251875151,ManufacturerGuid=N'68918ca7-1aec-4c38-9e50-64d05e04ac81',IsMasterData='1' WHERE Guid = '0023867b-24b2-45a1-b82c-57c035627cef'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cdd56ee1-937b-4789-b230-2c6e0e00a635')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cdd56ee1-937b-4789-b230-2c6e0e00a635')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '58ce8b0b-4fd7-4455-a344-c062332f2b3c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C077', 0.0725864989112025, N'cdd56ee1-937b-4789-b230-2c6e0e00a635', '1', '58ce8b0b-4fd7-4455-a344-c062332f2b3c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C077',Price=0.0725864989112025,ManufacturerGuid=N'cdd56ee1-937b-4789-b230-2c6e0e00a635',IsMasterData='1' WHERE Guid = '58ce8b0b-4fd7-4455-a344-c062332f2b3c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'779313a4-b680-498f-9679-86f7f325fbb8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'779313a4-b680-498f-9679-86f7f325fbb8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '87b1952f-176b-4f6e-aa3e-1e0979346fa1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C078', 0.0725864989112025, N'779313a4-b680-498f-9679-86f7f325fbb8', '1', '87b1952f-176b-4f6e-aa3e-1e0979346fa1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C078',Price=0.0725864989112025,ManufacturerGuid=N'779313a4-b680-498f-9679-86f7f325fbb8',IsMasterData='1' WHERE Guid = '87b1952f-176b-4f6e-aa3e-1e0979346fa1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6f431756-8312-4d21-8c1d-975169e0c481')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6f431756-8312-4d21-8c1d-975169e0c481')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b3d2231c-89c8-400d-8168-17f82be96942')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C079', 0.0733123639003145, N'6f431756-8312-4d21-8c1d-975169e0c481', '1', 'b3d2231c-89c8-400d-8168-17f82be96942')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C079',Price=0.0733123639003145,ManufacturerGuid=N'6f431756-8312-4d21-8c1d-975169e0c481',IsMasterData='1' WHERE Guid = 'b3d2231c-89c8-400d-8168-17f82be96942'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a893b3f3-a68a-478f-a8ae-1464d987764b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a893b3f3-a68a-478f-a8ae-1464d987764b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fd0178ab-59cc-4e75-a34a-dd6ea2248bb8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C080', 0.188966852165497, N'a893b3f3-a68a-478f-a8ae-1464d987764b', '1', 'fd0178ab-59cc-4e75-a34a-dd6ea2248bb8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C080',Price=0.188966852165497,ManufacturerGuid=N'a893b3f3-a68a-478f-a8ae-1464d987764b',IsMasterData='1' WHERE Guid = 'fd0178ab-59cc-4e75-a34a-dd6ea2248bb8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7da1f972-409f-47f9-a5ef-3fb74ef64e47')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7da1f972-409f-47f9-a5ef-3fb74ef64e47')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '81938a9e-877d-4bfe-a5dd-6380380446f8')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C081', 0.211952576820711, N'7da1f972-409f-47f9-a5ef-3fb74ef64e47', '1', '81938a9e-877d-4bfe-a5dd-6380380446f8')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C081',Price=0.211952576820711,ManufacturerGuid=N'7da1f972-409f-47f9-a5ef-3fb74ef64e47',IsMasterData='1' WHERE Guid = '81938a9e-877d-4bfe-a5dd-6380380446f8'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'abf3c19f-b026-48af-bc42-263e8e11a398')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'abf3c19f-b026-48af-bc42-263e8e11a398')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '942934f9-0f8e-4359-b031-d3bec558a4e4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C083', 0.187515122187273, N'abf3c19f-b026-48af-bc42-263e8e11a398', '1', '942934f9-0f8e-4359-b031-d3bec558a4e4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C083',Price=0.187515122187273,ManufacturerGuid=N'abf3c19f-b026-48af-bc42-263e8e11a398',IsMasterData='1' WHERE Guid = '942934f9-0f8e-4359-b031-d3bec558a4e4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2597a621-f3ab-4648-b8ab-1b6fe5c756bc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2597a621-f3ab-4648-b8ab-1b6fe5c756bc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2aa90f9d-9aaf-46b3-9804-7dfc613a04f7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C087', 0.135010887974837, N'2597a621-f3ab-4648-b8ab-1b6fe5c756bc', '1', '2aa90f9d-9aaf-46b3-9804-7dfc613a04f7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C087',Price=0.135010887974837,ManufacturerGuid=N'2597a621-f3ab-4648-b8ab-1b6fe5c756bc',IsMasterData='1' WHERE Guid = '2aa90f9d-9aaf-46b3-9804-7dfc613a04f7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b220673c-ba84-4705-9aca-da135a8db006')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b220673c-ba84-4705-9aca-da135a8db006')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a4fd0a3d-d66e-44ee-b557-8ac7b5986b29')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C088', 0.137430437938543, N'b220673c-ba84-4705-9aca-da135a8db006', '1', 'a4fd0a3d-d66e-44ee-b557-8ac7b5986b29')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C088',Price=0.137430437938543,ManufacturerGuid=N'b220673c-ba84-4705-9aca-da135a8db006',IsMasterData='1' WHERE Guid = 'a4fd0a3d-d66e-44ee-b557-8ac7b5986b29'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'927975e1-aad2-4fdb-b346-a7edafdf2e37')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'927975e1-aad2-4fdb-b346-a7edafdf2e37')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c14a06c1-9d22-478d-a143-171e241ac16b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313C504', 0.696225502056617, N'927975e1-aad2-4fdb-b346-a7edafdf2e37', '1', 'c14a06c1-9d22-478d-a143-171e241ac16b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313C504',Price=0.696225502056617,ManufacturerGuid=N'927975e1-aad2-4fdb-b346-a7edafdf2e37',IsMasterData='1' WHERE Guid = 'c14a06c1-9d22-478d-a143-171e241ac16b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'efb546fa-8357-4832-a5d6-1f03b985221f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'efb546fa-8357-4832-a5d6-1f03b985221f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ca7e9a36-5fbf-4b89-b9e6-39c87846f522')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313F059', 0.048390999274135, N'efb546fa-8357-4832-a5d6-1f03b985221f', '1', 'ca7e9a36-5fbf-4b89-b9e6-39c87846f522')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313F059',Price=0.048390999274135,ManufacturerGuid=N'efb546fa-8357-4832-a5d6-1f03b985221f',IsMasterData='1' WHERE Guid = 'ca7e9a36-5fbf-4b89-b9e6-39c87846f522'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'27ffb8c1-3c24-4a30-86c7-174d8cfdb76c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'27ffb8c1-3c24-4a30-86c7-174d8cfdb76c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aa4cb5ca-3c72-4405-879e-57682ed313eb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313F068', 0.108637793370433, N'27ffb8c1-3c24-4a30-86c7-174d8cfdb76c', '1', 'aa4cb5ca-3c72-4405-879e-57682ed313eb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313F068',Price=0.108637793370433,ManufacturerGuid=N'27ffb8c1-3c24-4a30-86c7-174d8cfdb76c',IsMasterData='1' WHERE Guid = 'aa4cb5ca-3c72-4405-879e-57682ed313eb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8779bcfd-a9fb-4e57-be42-d859c5c6395c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8779bcfd-a9fb-4e57-be42-d859c5c6395c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7d095000-84f8-4993-93ed-15429379dd11')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313F079', 0.0725864989112025, N'8779bcfd-a9fb-4e57-be42-d859c5c6395c', '1', '7d095000-84f8-4993-93ed-15429379dd11')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313F079',Price=0.0725864989112025,ManufacturerGuid=N'8779bcfd-a9fb-4e57-be42-d859c5c6395c',IsMasterData='1' WHERE Guid = '7d095000-84f8-4993-93ed-15429379dd11'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4680a946-b210-4c01-a563-76522970022d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4680a946-b210-4c01-a563-76522970022d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '31903be8-4e68-46e1-863a-37d85b7b829b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H228', 1.04887490926688, N'4680a946-b210-4c01-a563-76522970022d', '1', '31903be8-4e68-46e1-863a-37d85b7b829b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H228',Price=1.04887490926688,ManufacturerGuid=N'4680a946-b210-4c01-a563-76522970022d',IsMasterData='1' WHERE Guid = '31903be8-4e68-46e1-863a-37d85b7b829b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'29474f5a-3b4b-43d5-afc1-ec790718000c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'29474f5a-3b4b-43d5-afc1-ec790718000c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8b20224d-997f-4467-8e9e-3403807ad187')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H229', 0.09678199854827, N'29474f5a-3b4b-43d5-afc1-ec790718000c', '1', '8b20224d-997f-4467-8e9e-3403807ad187')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H229',Price=0.09678199854827,ManufacturerGuid=N'29474f5a-3b4b-43d5-afc1-ec790718000c',IsMasterData='1' WHERE Guid = '8b20224d-997f-4467-8e9e-3403807ad187'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b61f49be-a164-457c-8368-292f85bb8e9a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b61f49be-a164-457c-8368-292f85bb8e9a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0f998fd2-a4b0-4540-abd4-d9f3e32d8285')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H233', 0.0877086861843697, N'b61f49be-a164-457c-8368-292f85bb8e9a', '1', '0f998fd2-a4b0-4540-abd4-d9f3e32d8285')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H233',Price=0.0877086861843697,ManufacturerGuid=N'b61f49be-a164-457c-8368-292f85bb8e9a',IsMasterData='1' WHERE Guid = '0f998fd2-a4b0-4540-abd4-d9f3e32d8285'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c9203372-a434-46c0-b226-9df04de4761b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c9203372-a434-46c0-b226-9df04de4761b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f65310bd-9e3d-49df-9916-111d1c756a62')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H236', 0.14710863779337, N'c9203372-a434-46c0-b226-9df04de4761b', '1', 'f65310bd-9e3d-49df-9916-111d1c756a62')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H236',Price=0.14710863779337,ManufacturerGuid=N'c9203372-a434-46c0-b226-9df04de4761b',IsMasterData='1' WHERE Guid = 'f65310bd-9e3d-49df-9916-111d1c756a62'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1609dcbf-9f24-4741-b642-43503d836dba')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1609dcbf-9f24-4741-b642-43503d836dba')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd3d50cc2-eb14-446f-aa86-5647cae764d9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H238', 0.157270747640939, N'1609dcbf-9f24-4741-b642-43503d836dba', '1', 'd3d50cc2-eb14-446f-aa86-5647cae764d9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H238',Price=0.157270747640939,ManufacturerGuid=N'1609dcbf-9f24-4741-b642-43503d836dba',IsMasterData='1' WHERE Guid = 'd3d50cc2-eb14-446f-aa86-5647cae764d9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd3ba5e3f-3e4e-479b-b541-8f719274d528')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd3ba5e3f-3e4e-479b-b541-8f719274d528')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2ac11003-1a82-4a67-93af-9404715f0ba4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H239', 0.338736994918945, N'd3ba5e3f-3e4e-479b-b541-8f719274d528', '1', '2ac11003-1a82-4a67-93af-9404715f0ba4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H239',Price=0.338736994918945,ManufacturerGuid=N'd3ba5e3f-3e4e-479b-b541-8f719274d528',IsMasterData='1' WHERE Guid = '2ac11003-1a82-4a67-93af-9404715f0ba4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c7bbcb3a-ff9f-4b20-9da4-5f2f2ae68b62')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c7bbcb3a-ff9f-4b20-9da4-5f2f2ae68b62')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ab86ea32-0392-4775-adc8-ebb5421477f5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H242', 0.472417130413743, N'c7bbcb3a-ff9f-4b20-9da4-5f2f2ae68b62', '1', 'ab86ea32-0392-4775-adc8-ebb5421477f5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H242',Price=0.472417130413743,ManufacturerGuid=N'c7bbcb3a-ff9f-4b20-9da4-5f2f2ae68b62',IsMasterData='1' WHERE Guid = 'ab86ea32-0392-4775-adc8-ebb5421477f5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7bd97798-27cf-4dcc-a9bb-eb18b6e15cab')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7bd97798-27cf-4dcc-a9bb-eb18b6e15cab')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '528ea085-3f6e-4dcb-af83-19234c7665eb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H243', 0.177836922332446, N'7bd97798-27cf-4dcc-a9bb-eb18b6e15cab', '1', '528ea085-3f6e-4dcb-af83-19234c7665eb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H243',Price=0.177836922332446,ManufacturerGuid=N'7bd97798-27cf-4dcc-a9bb-eb18b6e15cab',IsMasterData='1' WHERE Guid = '528ea085-3f6e-4dcb-af83-19234c7665eb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'93675d64-6d55-4f7b-8dae-556c1fc3f7a2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'93675d64-6d55-4f7b-8dae-556c1fc3f7a2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f28eaa12-0fc9-4ca8-8c0a-8af3b90fb8cf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H245', 0.907331236390031, N'93675d64-6d55-4f7b-8dae-556c1fc3f7a2', '1', 'f28eaa12-0fc9-4ca8-8c0a-8af3b90fb8cf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H245',Price=0.907331236390031,ManufacturerGuid=N'93675d64-6d55-4f7b-8dae-556c1fc3f7a2',IsMasterData='1' WHERE Guid = 'f28eaa12-0fc9-4ca8-8c0a-8af3b90fb8cf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'300c5ffb-502e-4744-bde3-a0b7857a787f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'300c5ffb-502e-4744-bde3-a0b7857a787f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ba9b486a-a977-42b0-a44d-71ac18f9f879')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H321', 0.0835954512460682, N'300c5ffb-502e-4744-bde3-a0b7857a787f', '1', 'ba9b486a-a977-42b0-a44d-71ac18f9f879')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H321',Price=0.0835954512460682,ManufacturerGuid=N'300c5ffb-502e-4744-bde3-a0b7857a787f',IsMasterData='1' WHERE Guid = 'ba9b486a-a977-42b0-a44d-71ac18f9f879'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0a31936f-e284-4e72-84c1-d51dddab0339')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0a31936f-e284-4e72-84c1-d51dddab0339')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c00c2379-1397-43ff-9364-d1d442cd4305')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H331', 0.181466247278006, N'0a31936f-e284-4e72-84c1-d51dddab0339', '1', 'c00c2379-1397-43ff-9364-d1d442cd4305')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H331',Price=0.181466247278006,ManufacturerGuid=N'0a31936f-e284-4e72-84c1-d51dddab0339',IsMasterData='1' WHERE Guid = 'c00c2379-1397-43ff-9364-d1d442cd4305'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'875999b6-d66e-4d0f-bcf1-2fa30d0245ae')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'875999b6-d66e-4d0f-bcf1-2fa30d0245ae')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '099ed898-5265-4395-a5bc-3366b2ccb459')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H333', 0.111299298330511, N'875999b6-d66e-4d0f-bcf1-2fa30d0245ae', '1', '099ed898-5265-4395-a5bc-3366b2ccb459')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H333',Price=0.111299298330511,ManufacturerGuid=N'875999b6-d66e-4d0f-bcf1-2fa30d0245ae',IsMasterData='1' WHERE Guid = '099ed898-5265-4395-a5bc-3366b2ccb459'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fae81288-2d70-4b19-821c-0329c572d236')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fae81288-2d70-4b19-821c-0329c572d236')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e84c2239-d1bb-4e68-b3f0-66fd9f17b996')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M058', 0.0725864989112025, N'fae81288-2d70-4b19-821c-0329c572d236', '1', 'e84c2239-d1bb-4e68-b3f0-66fd9f17b996')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M058',Price=0.0725864989112025,ManufacturerGuid=N'fae81288-2d70-4b19-821c-0329c572d236',IsMasterData='1' WHERE Guid = 'e84c2239-d1bb-4e68-b3f0-66fd9f17b996'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c3ba93db-147c-483a-a061-65f1953f2f17')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c3ba93db-147c-483a-a061-65f1953f2f17')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9abfbedf-3d01-450c-acbf-239e5257d631')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M059', 0.0604887490926688, N'c3ba93db-147c-483a-a061-65f1953f2f17', '1', '9abfbedf-3d01-450c-acbf-239e5257d631')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M059',Price=0.0604887490926688,ManufacturerGuid=N'c3ba93db-147c-483a-a061-65f1953f2f17',IsMasterData='1' WHERE Guid = '9abfbedf-3d01-450c-acbf-239e5257d631'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7e7dddef-dee6-40ea-bc6f-b30781770ac3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7e7dddef-dee6-40ea-bc6f-b30781770ac3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b47f5aa9-1aef-4a88-b93b-5da2ae945879')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M068', 0.0919428986208565, N'7e7dddef-dee6-40ea-bc6f-b30781770ac3', '1', 'b47f5aa9-1aef-4a88-b93b-5da2ae945879')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M068',Price=0.0919428986208565,ManufacturerGuid=N'7e7dddef-dee6-40ea-bc6f-b30781770ac3',IsMasterData='1' WHERE Guid = 'b47f5aa9-1aef-4a88-b93b-5da2ae945879'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd391ecd7-121a-482e-bd7b-c286c855eea1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd391ecd7-121a-482e-bd7b-c286c855eea1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '291342cc-13c6-4c29-8481-567161c64b0f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M077', 0.101621098475684, N'd391ecd7-121a-482e-bd7b-c286c855eea1', '1', '291342cc-13c6-4c29-8481-567161c64b0f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M077',Price=0.101621098475684,ManufacturerGuid=N'd391ecd7-121a-482e-bd7b-c286c855eea1',IsMasterData='1' WHERE Guid = '291342cc-13c6-4c29-8481-567161c64b0f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1a71f03f-7689-450e-9677-cc48fea60879')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1a71f03f-7689-450e-9677-cc48fea60879')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'da5489da-811b-44ff-80f7-b751ae911d3a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M079', 0.0910960561335592, N'1a71f03f-7689-450e-9677-cc48fea60879', '1', 'da5489da-811b-44ff-80f7-b751ae911d3a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M079',Price=0.0910960561335592,ManufacturerGuid=N'1a71f03f-7689-450e-9677-cc48fea60879',IsMasterData='1' WHERE Guid = 'da5489da-811b-44ff-80f7-b751ae911d3a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2a460df1-df07-4cb4-904c-f6e6b814664b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2a460df1-df07-4cb4-904c-f6e6b814664b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '68b5cc26-a070-4944-8d83-5e85ed1e4288')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M081', 0.108879748366804, N'2a460df1-df07-4cb4-904c-f6e6b814664b', '1', '68b5cc26-a070-4944-8d83-5e85ed1e4288')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M081',Price=0.108879748366804,ManufacturerGuid=N'2a460df1-df07-4cb4-904c-f6e6b814664b',IsMasterData='1' WHERE Guid = '68b5cc26-a070-4944-8d83-5e85ed1e4288'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ebab3ee5-c69f-4765-a97f-9c5658fd66b2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ebab3ee5-c69f-4765-a97f-9c5658fd66b2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a1394869-f6ac-44b4-a32e-5454f69825ac')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M082', 0.134164045487539, N'ebab3ee5-c69f-4765-a97f-9c5658fd66b2', '1', 'a1394869-f6ac-44b4-a32e-5454f69825ac')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M082',Price=0.134164045487539,ManufacturerGuid=N'ebab3ee5-c69f-4765-a97f-9c5658fd66b2',IsMasterData='1' WHERE Guid = 'a1394869-f6ac-44b4-a32e-5454f69825ac'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7feb6530-00b6-4ee6-9f78-508dddf5609c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7feb6530-00b6-4ee6-9f78-508dddf5609c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '41e938a9-8efa-424e-a77b-0e8ffdf4584a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M083', 0.133075248003871, N'7feb6530-00b6-4ee6-9f78-508dddf5609c', '1', '41e938a9-8efa-424e-a77b-0e8ffdf4584a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M083',Price=0.133075248003871,ManufacturerGuid=N'7feb6530-00b6-4ee6-9f78-508dddf5609c',IsMasterData='1' WHERE Guid = '41e938a9-8efa-424e-a77b-0e8ffdf4584a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5a857e85-6f31-4c77-ab10-a245c4069d55')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5a857e85-6f31-4c77-ab10-a245c4069d55')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6c3dddc9-fff4-48f0-b4b1-a91a024ea01b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M085', 0.350834744737479, N'5a857e85-6f31-4c77-ab10-a245c4069d55', '1', '6c3dddc9-fff4-48f0-b4b1-a91a024ea01b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M085',Price=0.350834744737479,ManufacturerGuid=N'5a857e85-6f31-4c77-ab10-a245c4069d55',IsMasterData='1' WHERE Guid = '6c3dddc9-fff4-48f0-b4b1-a91a024ea01b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1ce42a8c-ed30-42ca-8cc9-f5168799ea65')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1ce42a8c-ed30-42ca-8cc9-f5168799ea65')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1c6ab5bd-8bb1-4e09-9efd-a4adb81504ea')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M087', 0.210258891846117, N'1ce42a8c-ed30-42ca-8cc9-f5168799ea65', '1', '1c6ab5bd-8bb1-4e09-9efd-a4adb81504ea')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M087',Price=0.210258891846117,ManufacturerGuid=N'1ce42a8c-ed30-42ca-8cc9-f5168799ea65',IsMasterData='1' WHERE Guid = '1c6ab5bd-8bb1-4e09-9efd-a4adb81504ea'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8254391a-1411-472f-9ea8-fbfa56659dd2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8254391a-1411-472f-9ea8-fbfa56659dd2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '37579c75-d058-404c-914c-ca2bc072f95b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M100', 0.150616985240745, N'8254391a-1411-472f-9ea8-fbfa56659dd2', '1', '37579c75-d058-404c-914c-ca2bc072f95b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M100',Price=0.150616985240745,ManufacturerGuid=N'8254391a-1411-472f-9ea8-fbfa56659dd2',IsMasterData='1' WHERE Guid = '37579c75-d058-404c-914c-ca2bc072f95b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5b630646-dad4-4827-bd49-2d019463ac06')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5b630646-dad4-4827-bd49-2d019463ac06')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fd9f8b88-586c-410a-b03f-6583e23984ea')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M101', 0.188240987176385, N'5b630646-dad4-4827-bd49-2d019463ac06', '1', 'fd9f8b88-586c-410a-b03f-6583e23984ea')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M101',Price=0.188240987176385,ManufacturerGuid=N'5b630646-dad4-4827-bd49-2d019463ac06',IsMasterData='1' WHERE Guid = 'fd9f8b88-586c-410a-b03f-6583e23984ea'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0e8c1e21-d4f6-4025-88bd-0f4b0ee5ac61')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0e8c1e21-d4f6-4025-88bd-0f4b0ee5ac61')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '91da9010-343d-4b7d-9b13-f7530e67cc9b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M103', 0.807887732881684, N'0e8c1e21-d4f6-4025-88bd-0f4b0ee5ac61', '1', '91da9010-343d-4b7d-9b13-f7530e67cc9b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M103',Price=0.807887732881684,ManufacturerGuid=N'0e8c1e21-d4f6-4025-88bd-0f4b0ee5ac61',IsMasterData='1' WHERE Guid = '91da9010-343d-4b7d-9b13-f7530e67cc9b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ba93bad6-2714-4f20-bdf1-9f78fccf33a4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ba93bad6-2714-4f20-bdf1-9f78fccf33a4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8c1d6584-83f3-4ecd-9c64-4d993ac04b70')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M105', 0.28332930075006, N'ba93bad6-2714-4f20-bdf1-9f78fccf33a4', '1', '8c1d6584-83f3-4ecd-9c64-4d993ac04b70')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M105',Price=0.28332930075006,ManufacturerGuid=N'ba93bad6-2714-4f20-bdf1-9f78fccf33a4',IsMasterData='1' WHERE Guid = '8c1d6584-83f3-4ecd-9c64-4d993ac04b70'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8dc92683-3cea-479b-997e-3b875753f2c0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8dc92683-3cea-479b-997e-3b875753f2c0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b478e89c-a2fa-4a5b-9499-c69ccba691d2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M125', 0.399225744011614, N'8dc92683-3cea-479b-997e-3b875753f2c0', '1', 'b478e89c-a2fa-4a5b-9499-c69ccba691d2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M125',Price=0.399225744011614,ManufacturerGuid=N'8dc92683-3cea-479b-997e-3b875753f2c0',IsMasterData='1' WHERE Guid = 'b478e89c-a2fa-4a5b-9499-c69ccba691d2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f684a1ac-8bb0-4917-b7d3-e0e515d17fb7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f684a1ac-8bb0-4917-b7d3-e0e515d17fb7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '40eebe81-8de2-4ff9-8b22-2e5a1fc98169')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M131', 0.666465037503024, N'f684a1ac-8bb0-4917-b7d3-e0e515d17fb7', '1', '40eebe81-8de2-4ff9-8b22-2e5a1fc98169')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M131',Price=0.666465037503024,ManufacturerGuid=N'f684a1ac-8bb0-4917-b7d3-e0e515d17fb7',IsMasterData='1' WHERE Guid = '40eebe81-8de2-4ff9-8b22-2e5a1fc98169'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'64ebd2b5-b2b1-4a0f-b523-5f99fd848138')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'64ebd2b5-b2b1-4a0f-b523-5f99fd848138')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2026ec2d-6f5b-45ff-ab9e-77b1d40ec2d0')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M359', 0.151221872731672, N'64ebd2b5-b2b1-4a0f-b523-5f99fd848138', '1', '2026ec2d-6f5b-45ff-ab9e-77b1d40ec2d0')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M359',Price=0.151221872731672,ManufacturerGuid=N'64ebd2b5-b2b1-4a0f-b523-5f99fd848138',IsMasterData='1' WHERE Guid = '2026ec2d-6f5b-45ff-ab9e-77b1d40ec2d0'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'deb58618-1ce4-44cc-8b5c-fdadd6ecaa80')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'deb58618-1ce4-44cc-8b5c-fdadd6ecaa80')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '13ccafeb-8153-4467-8dd5-caf28e23d407')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M369', 0.222598596661021, N'deb58618-1ce4-44cc-8b5c-fdadd6ecaa80', '1', '13ccafeb-8153-4467-8dd5-caf28e23d407')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M369',Price=0.222598596661021,ManufacturerGuid=N'deb58618-1ce4-44cc-8b5c-fdadd6ecaa80',IsMasterData='1' WHERE Guid = '13ccafeb-8153-4467-8dd5-caf28e23d407'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'47f32ee5-00af-43df-a99e-8ddc2dc21d3d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'47f32ee5-00af-43df-a99e-8ddc2dc21d3d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ed62e0c1-a787-4bf8-a132-fbda345e4744')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M377', 0.10585531091217, N'47f32ee5-00af-43df-a99e-8ddc2dc21d3d', '1', 'ed62e0c1-a787-4bf8-a132-fbda345e4744')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M377',Price=0.10585531091217,ManufacturerGuid=N'47f32ee5-00af-43df-a99e-8ddc2dc21d3d',IsMasterData='1' WHERE Guid = 'ed62e0c1-a787-4bf8-a132-fbda345e4744'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'135379d7-07b4-450a-8ee2-3b03b708bc47')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'135379d7-07b4-450a-8ee2-3b03b708bc47')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'abb3039f-fbd5-487a-890a-fcf4056adf61')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M378', 0.107065085894024, N'135379d7-07b4-450a-8ee2-3b03b708bc47', '1', 'abb3039f-fbd5-487a-890a-fcf4056adf61')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M378',Price=0.107065085894024,ManufacturerGuid=N'135379d7-07b4-450a-8ee2-3b03b708bc47',IsMasterData='1' WHERE Guid = 'abb3039f-fbd5-487a-890a-fcf4056adf61'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7818975b-e051-4537-aab8-e90e4b9e7347')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7818975b-e051-4537-aab8-e90e4b9e7347')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8e7b89b6-cf36-4c67-af57-7692afad2a75')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M380', 0.124969755625454, N'7818975b-e051-4537-aab8-e90e4b9e7347', '1', '8e7b89b6-cf36-4c67-af57-7692afad2a75')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M380',Price=0.124969755625454,ManufacturerGuid=N'7818975b-e051-4537-aab8-e90e4b9e7347',IsMasterData='1' WHERE Guid = '8e7b89b6-cf36-4c67-af57-7692afad2a75'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f9b3a269-a745-4c76-b1d4-95246f8f93b8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f9b3a269-a745-4c76-b1d4-95246f8f93b8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6cbe06a1-743b-4713-b35e-54737fd7efc6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M383', 0.332688120009678, N'f9b3a269-a745-4c76-b1d4-95246f8f93b8', '1', '6cbe06a1-743b-4713-b35e-54737fd7efc6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M383',Price=0.332688120009678,ManufacturerGuid=N'f9b3a269-a745-4c76-b1d4-95246f8f93b8',IsMasterData='1' WHERE Guid = '6cbe06a1-743b-4713-b35e-54737fd7efc6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'09b76dbf-c75d-4ba2-9a28-a2188a260172')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'09b76dbf-c75d-4ba2-9a28-a2188a260172')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3f1e81ef-389a-48c5-b0f8-dbae23a382f6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313M387', 0.205661746915074, N'09b76dbf-c75d-4ba2-9a28-a2188a260172', '1', '3f1e81ef-389a-48c5-b0f8-dbae23a382f6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313M387',Price=0.205661746915074,ManufacturerGuid=N'09b76dbf-c75d-4ba2-9a28-a2188a260172',IsMasterData='1' WHERE Guid = '3f1e81ef-389a-48c5-b0f8-dbae23a382f6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e6cb46b8-9f24-4b66-854d-a910affaee4a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e6cb46b8-9f24-4b66-854d-a910affaee4a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '054e0849-bafe-4ba5-960a-81293f713446')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313N106', 0.441567868376482, N'e6cb46b8-9f24-4b66-854d-a910affaee4a', '1', '054e0849-bafe-4ba5-960a-81293f713446')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313N106',Price=0.441567868376482,ManufacturerGuid=N'e6cb46b8-9f24-4b66-854d-a910affaee4a',IsMasterData='1' WHERE Guid = '054e0849-bafe-4ba5-960a-81293f713446'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b379c9c1-cceb-4910-80c0-6678c7fb6838')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b379c9c1-cceb-4910-80c0-6678c7fb6838')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3fa90824-406e-4959-8e40-26c8de6e4d81')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313N107', 0.181466247278006, N'b379c9c1-cceb-4910-80c0-6678c7fb6838', '1', '3fa90824-406e-4959-8e40-26c8de6e4d81')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313N107',Price=0.181466247278006,ManufacturerGuid=N'b379c9c1-cceb-4910-80c0-6678c7fb6838',IsMasterData='1' WHERE Guid = '3fa90824-406e-4959-8e40-26c8de6e4d81'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'47e8200e-c619-49c6-b3c5-632132cf7701')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'47e8200e-c619-49c6-b3c5-632132cf7701')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '40064a67-be66-43e1-8d09-81d5459e4d26')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313N112', 0.205661746915074, N'47e8200e-c619-49c6-b3c5-632132cf7701', '1', '40064a67-be66-43e1-8d09-81d5459e4d26')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313N112',Price=0.205661746915074,ManufacturerGuid=N'47e8200e-c619-49c6-b3c5-632132cf7701',IsMasterData='1' WHERE Guid = '40064a67-be66-43e1-8d09-81d5459e4d26'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8239a061-c839-418c-8889-8b8da7b7354c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8239a061-c839-418c-8889-8b8da7b7354c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3753bfdf-33a8-44fc-bfa2-eda96eb65f41')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313T080', 0.266150496007743, N'8239a061-c839-418c-8889-8b8da7b7354c', '1', '3753bfdf-33a8-44fc-bfa2-eda96eb65f41')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313T080',Price=0.266150496007743,ManufacturerGuid=N'8239a061-c839-418c-8889-8b8da7b7354c',IsMasterData='1' WHERE Guid = '3753bfdf-33a8-44fc-bfa2-eda96eb65f41'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a575bb4f-2a0c-416d-bf2d-dd7dc503fe96')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a575bb4f-2a0c-416d-bf2d-dd7dc503fe96')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aefaa0ba-0a9d-48d1-8219-688ed136e51b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P004', 0.0679893539801597, N'a575bb4f-2a0c-416d-bf2d-dd7dc503fe96', '1', 'aefaa0ba-0a9d-48d1-8219-688ed136e51b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P004',Price=0.0679893539801597,ManufacturerGuid=N'a575bb4f-2a0c-416d-bf2d-dd7dc503fe96',IsMasterData='1' WHERE Guid = 'aefaa0ba-0a9d-48d1-8219-688ed136e51b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2a2fa739-0da1-431a-96d4-80f1dcd51401')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2a2fa739-0da1-431a-96d4-80f1dcd51401')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '98ee899a-bdea-4a69-a047-f0e409fb807d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P005', 0.413259133801113, N'2a2fa739-0da1-431a-96d4-80f1dcd51401', '1', '98ee899a-bdea-4a69-a047-f0e409fb807d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P005',Price=0.413259133801113,ManufacturerGuid=N'2a2fa739-0da1-431a-96d4-80f1dcd51401',IsMasterData='1' WHERE Guid = '98ee899a-bdea-4a69-a047-f0e409fb807d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95efb509-f342-4122-ae88-9236942ab543')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95efb509-f342-4122-ae88-9236942ab543')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9d1d7082-9330-4670-8fc7-c0b7c66f6b34')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P006', 0.112388095814179, N'95efb509-f342-4122-ae88-9236942ab543', '1', '9d1d7082-9330-4670-8fc7-c0b7c66f6b34')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P006',Price=0.112388095814179,ManufacturerGuid=N'95efb509-f342-4122-ae88-9236942ab543',IsMasterData='1' WHERE Guid = '9d1d7082-9330-4670-8fc7-c0b7c66f6b34'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aae5c229-1586-4880-8f9a-cabd46f90ed1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aae5c229-1586-4880-8f9a-cabd46f90ed1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c8e81d3d-5865-4e50-8e06-68e44bf6e3d3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P007', 0.0646019840309702, N'aae5c229-1586-4880-8f9a-cabd46f90ed1', '1', 'c8e81d3d-5865-4e50-8e06-68e44bf6e3d3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P007',Price=0.0646019840309702,ManufacturerGuid=N'aae5c229-1586-4880-8f9a-cabd46f90ed1',IsMasterData='1' WHERE Guid = 'c8e81d3d-5865-4e50-8e06-68e44bf6e3d3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f6843b01-10e0-42f1-9034-90208a5a22db')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f6843b01-10e0-42f1-9034-90208a5a22db')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5b28bdc0-270d-4c82-8bfd-92c6a975eef2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P008', 0.120977498185338, N'f6843b01-10e0-42f1-9034-90208a5a22db', '1', '5b28bdc0-270d-4c82-8bfd-92c6a975eef2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P008',Price=0.120977498185338,ManufacturerGuid=N'f6843b01-10e0-42f1-9034-90208a5a22db',IsMasterData='1' WHERE Guid = '5b28bdc0-270d-4c82-8bfd-92c6a975eef2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6cbc9ca5-d494-4ed3-a52f-43a8f51ebeb8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6cbc9ca5-d494-4ed3-a52f-43a8f51ebeb8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd7fb4d73-47ae-475c-8f85-6a7875eb4e62')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P013', 0.145172997822405, N'6cbc9ca5-d494-4ed3-a52f-43a8f51ebeb8', '1', 'd7fb4d73-47ae-475c-8f85-6a7875eb4e62')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P013',Price=0.145172997822405,ManufacturerGuid=N'6cbc9ca5-d494-4ed3-a52f-43a8f51ebeb8',IsMasterData='1' WHERE Guid = 'd7fb4d73-47ae-475c-8f85-6a7875eb4e62'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'415f7dbc-1af6-4592-b1d4-90ea8c45eb50')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'415f7dbc-1af6-4592-b1d4-90ea8c45eb50')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfb50f16-a396-42af-9fe9-adb92b4b39fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3211P022', 0.382530849262037, N'415f7dbc-1af6-4592-b1d4-90ea8c45eb50', '1', 'bfb50f16-a396-42af-9fe9-adb92b4b39fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3211P022',Price=0.382530849262037,ManufacturerGuid=N'415f7dbc-1af6-4592-b1d4-90ea8c45eb50',IsMasterData='1' WHERE Guid = 'bfb50f16-a396-42af-9fe9-adb92b4b39fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1a9718ef-0413-45b4-9c19-81cc4267c7a7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1a9718ef-0413-45b4-9c19-81cc4267c7a7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '60cb7334-232c-4913-b6f5-651989a4ac4b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3214A013', 1.45172997822405, N'1a9718ef-0413-45b4-9c19-81cc4267c7a7', '1', '60cb7334-232c-4913-b6f5-651989a4ac4b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3214A013',Price=1.45172997822405,ManufacturerGuid=N'1a9718ef-0413-45b4-9c19-81cc4267c7a7',IsMasterData='1' WHERE Guid = '60cb7334-232c-4913-b6f5-651989a4ac4b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'07aab717-8e93-47c4-8dab-ebd52213fc1e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'07aab717-8e93-47c4-8dab-ebd52213fc1e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c2c3acbd-fccc-46ed-b8d8-9d68930e40e2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3214A016', 1.50012097749819, N'07aab717-8e93-47c4-8dab-ebd52213fc1e', '1', 'c2c3acbd-fccc-46ed-b8d8-9d68930e40e2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3214A016',Price=1.50012097749819,ManufacturerGuid=N'07aab717-8e93-47c4-8dab-ebd52213fc1e',IsMasterData='1' WHERE Guid = 'c2c3acbd-fccc-46ed-b8d8-9d68930e40e2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'20dacd7b-6a6d-4fe6-9a06-7983aab929f3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'20dacd7b-6a6d-4fe6-9a06-7983aab929f3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '95d15f7f-7ad7-489d-896c-d9efd94ec079')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3214A017', 1.46382772804258, N'20dacd7b-6a6d-4fe6-9a06-7983aab929f3', '1', '95d15f7f-7ad7-489d-896c-d9efd94ec079')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3214A017',Price=1.46382772804258,ManufacturerGuid=N'20dacd7b-6a6d-4fe6-9a06-7983aab929f3',IsMasterData='1' WHERE Guid = '95d15f7f-7ad7-489d-896c-d9efd94ec079'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2ce19364-cd5c-4c1b-a2e0-75538be65539')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2ce19364-cd5c-4c1b-a2e0-75538be65539')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bbec9d97-0232-4188-9d9c-013f0a234328')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3214A018', 1.18557948221631, N'2ce19364-cd5c-4c1b-a2e0-75538be65539', '1', 'bbec9d97-0232-4188-9d9c-013f0a234328')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3214A018',Price=1.18557948221631,ManufacturerGuid=N'2ce19364-cd5c-4c1b-a2e0-75538be65539',IsMasterData='1' WHERE Guid = 'bbec9d97-0232-4188-9d9c-013f0a234328'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'91769f97-79d2-4794-98de-f30e9635f046')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'91769f97-79d2-4794-98de-f30e9635f046')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3ca985e3-5fd3-4bfd-889e-40da7d034e7a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3241A028', 0.117348173239777, N'91769f97-79d2-4794-98de-f30e9635f046', '1', '3ca985e3-5fd3-4bfd-889e-40da7d034e7a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3241A028',Price=0.117348173239777,ManufacturerGuid=N'91769f97-79d2-4794-98de-f30e9635f046',IsMasterData='1' WHERE Guid = '3ca985e3-5fd3-4bfd-889e-40da7d034e7a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2d7cc20d-7b81-4366-9b2a-0ac2b8d8652f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2d7cc20d-7b81-4366-9b2a-0ac2b8d8652f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'eea20e6e-f16b-464a-a2b8-91b071800d22')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3241A032', 1.54851197677232, N'2d7cc20d-7b81-4366-9b2a-0ac2b8d8652f', '1', 'eea20e6e-f16b-464a-a2b8-91b071800d22')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3241A032',Price=1.54851197677232,ManufacturerGuid=N'2d7cc20d-7b81-4366-9b2a-0ac2b8d8652f',IsMasterData='1' WHERE Guid = 'eea20e6e-f16b-464a-a2b8-91b071800d22'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95637780-e302-426d-b055-4108eaaefe96')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95637780-e302-426d-b055-4108eaaefe96')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ed62fc00-63cf-4ead-94a9-cf3c0d9a51f3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3254M002', 0.248487781272683, N'95637780-e302-426d-b055-4108eaaefe96', '1', 'ed62fc00-63cf-4ead-94a9-cf3c0d9a51f3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3254M002',Price=0.248487781272683,ManufacturerGuid=N'95637780-e302-426d-b055-4108eaaefe96',IsMasterData='1' WHERE Guid = 'ed62fc00-63cf-4ead-94a9-cf3c0d9a51f3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4b1c0b51-e5c9-4c30-9342-7eb5abb9f100')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4b1c0b51-e5c9-4c30-9342-7eb5abb9f100')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd052a07c-9c0b-4d86-89db-e9c2b716ec1c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3254T001', 0.429470118557948, N'4b1c0b51-e5c9-4c30-9342-7eb5abb9f100', '1', 'd052a07c-9c0b-4d86-89db-e9c2b716ec1c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3254T001',Price=0.429470118557948,ManufacturerGuid=N'4b1c0b51-e5c9-4c30-9342-7eb5abb9f100',IsMasterData='1' WHERE Guid = 'd052a07c-9c0b-4d86-89db-e9c2b716ec1c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'873a8406-8c3b-42fc-8092-9b08498ee319')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'873a8406-8c3b-42fc-8092-9b08498ee319')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '22ca79d3-1b6b-4aff-a099-a535db987437')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'32754435', 0.139729010404065, N'873a8406-8c3b-42fc-8092-9b08498ee319', '1', '22ca79d3-1b6b-4aff-a099-a535db987437')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'32754435',Price=0.139729010404065,ManufacturerGuid=N'873a8406-8c3b-42fc-8092-9b08498ee319',IsMasterData='1' WHERE Guid = '22ca79d3-1b6b-4aff-a099-a535db987437'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'61779280-b2d0-42f5-a837-24fb4fe8db70')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'61779280-b2d0-42f5-a837-24fb4fe8db70')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3b171efc-88d0-4746-86d0-b6ae236c0748')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3275A002', 0.197072344543915, N'61779280-b2d0-42f5-a837-24fb4fe8db70', '1', '3b171efc-88d0-4746-86d0-b6ae236c0748')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3275A002',Price=0.197072344543915,ManufacturerGuid=N'61779280-b2d0-42f5-a837-24fb4fe8db70',IsMasterData='1' WHERE Guid = '3b171efc-88d0-4746-86d0-b6ae236c0748'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f44dc158-19ca-470a-b637-30875f49953f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f44dc158-19ca-470a-b637-30875f49953f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3b15f366-bf4f-4ea1-9dcd-d51d1d84a4ee')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3278H002', 0.51088797483668, N'f44dc158-19ca-470a-b637-30875f49953f', '1', '3b15f366-bf4f-4ea1-9dcd-d51d1d84a4ee')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3278H002',Price=0.51088797483668,ManufacturerGuid=N'f44dc158-19ca-470a-b637-30875f49953f',IsMasterData='1' WHERE Guid = '3b15f366-bf4f-4ea1-9dcd-d51d1d84a4ee'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'455e84f7-b7d3-4aec-8947-56b94166fe6e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'455e84f7-b7d3-4aec-8947-56b94166fe6e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7fa26d87-701e-46b2-8809-9f61603e3fcd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'3278H006', 0.677111057343334, N'455e84f7-b7d3-4aec-8947-56b94166fe6e', '1', '7fa26d87-701e-46b2-8809-9f61603e3fcd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'3278H006',Price=0.677111057343334,ManufacturerGuid=N'455e84f7-b7d3-4aec-8947-56b94166fe6e',IsMasterData='1' WHERE Guid = '7fa26d87-701e-46b2-8809-9f61603e3fcd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cca08fd7-4bb4-4f1d-b61c-b133402ed6f9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cca08fd7-4bb4-4f1d-b61c-b133402ed6f9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9f9076da-046b-4f51-931c-c2eed68a652c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313F077', 0.508105492378418, N'cca08fd7-4bb4-4f1d-b61c-b133402ed6f9', '1', '9f9076da-046b-4f51-931c-c2eed68a652c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313F077',Price=0.508105492378418,ManufacturerGuid=N'cca08fd7-4bb4-4f1d-b61c-b133402ed6f9',IsMasterData='1' WHERE Guid = '9f9076da-046b-4f51-931c-c2eed68a652c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6274cd30-134b-4ff9-97fb-a0a55af229dc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6274cd30-134b-4ff9-97fb-a0a55af229dc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c76db21c-80f7-4e8f-a2df-d0cae6e031f3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'STUD', N'2313H244', 0.127026373094604, N'6274cd30-134b-4ff9-97fb-a0a55af229dc', '1', 'c76db21c-80f7-4e8f-a2df-d0cae6e031f3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'STUD',PartNumber=N'2313H244',Price=0.127026373094604,ManufacturerGuid=N'6274cd30-134b-4ff9-97fb-a0a55af229dc',IsMasterData='1' WHERE Guid = 'c76db21c-80f7-4e8f-a2df-d0cae6e031f3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e836c88c-65ea-4f83-9116-94333202e210')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e836c88c-65ea-4f83-9116-94333202e210')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '39c4b455-f49a-4905-a9ce-f32fa9191081')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'TERMINAL', N'2815512', 0.09678199854827, N'e836c88c-65ea-4f83-9116-94333202e210', '1', '39c4b455-f49a-4905-a9ce-f32fa9191081')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'TERMINAL',PartNumber=N'2815512',Price=0.09678199854827,ManufacturerGuid=N'e836c88c-65ea-4f83-9116-94333202e210',IsMasterData='1' WHERE Guid = '39c4b455-f49a-4905-a9ce-f32fa9191081'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'45168dbb-d94a-4a07-9fa7-c3cf4e15a391')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'45168dbb-d94a-4a07-9fa7-c3cf4e15a391')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '54e6d8f4-eec2-43a9-9216-006ebed04cc5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'TERMINAL', N'2815651', 0.07706266634406, N'45168dbb-d94a-4a07-9fa7-c3cf4e15a391', '1', '54e6d8f4-eec2-43a9-9216-006ebed04cc5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'TERMINAL',PartNumber=N'2815651',Price=0.07706266634406,ManufacturerGuid=N'45168dbb-d94a-4a07-9fa7-c3cf4e15a391',IsMasterData='1' WHERE Guid = '54e6d8f4-eec2-43a9-9216-006ebed04cc5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c39118db-c5ac-4a09-a696-6997734913fa')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c39118db-c5ac-4a09-a696-6997734913fa')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0f77e38b-610b-4ef9-be89-b55b79447b3a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'TERMINAL', N'2815703', 0.0955722235664166, N'c39118db-c5ac-4a09-a696-6997734913fa', '1', '0f77e38b-610b-4ef9-be89-b55b79447b3a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'TERMINAL',PartNumber=N'2815703',Price=0.0955722235664166,ManufacturerGuid=N'c39118db-c5ac-4a09-a696-6997734913fa',IsMasterData='1' WHERE Guid = '0f77e38b-610b-4ef9-be89-b55b79447b3a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f346bf13-3800-4249-ad9e-a08838a00a0d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f346bf13-3800-4249-ad9e-a08838a00a0d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e7ef06d1-2e9a-402c-a72b-f938e70c7487')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'TERMINAL', N'2815784', 4.31889668521655, N'f346bf13-3800-4249-ad9e-a08838a00a0d', '1', 'e7ef06d1-2e9a-402c-a72b-f938e70c7487')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'TERMINAL',PartNumber=N'2815784',Price=4.31889668521655,ManufacturerGuid=N'f346bf13-3800-4249-ad9e-a08838a00a0d',IsMasterData='1' WHERE Guid = 'e7ef06d1-2e9a-402c-a72b-f938e70c7487'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c7b3d5d9-6465-4871-ba09-cde00733b146')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c7b3d5d9-6465-4871-ba09-cde00733b146')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1a9b5925-5066-4fb1-9d18-51d99bef2f45')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THIMBLE', N'33137409', 0.0354464069683039, N'c7b3d5d9-6465-4871-ba09-cde00733b146', '1', '1a9b5925-5066-4fb1-9d18-51d99bef2f45')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THIMBLE',PartNumber=N'33137409',Price=0.0354464069683039,ManufacturerGuid=N'c7b3d5d9-6465-4871-ba09-cde00733b146',IsMasterData='1' WHERE Guid = '1a9b5925-5066-4fb1-9d18-51d99bef2f45'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'64bf33a6-7bcb-4ef5-9cb1-ade9a83d5488')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'64bf33a6-7bcb-4ef5-9cb1-ade9a83d5488')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4c45d83a-86b6-48ca-930e-d65c8b06243d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THIMBLE', N'33142111', 0.029397532059037, N'64bf33a6-7bcb-4ef5-9cb1-ade9a83d5488', '1', '4c45d83a-86b6-48ca-930e-d65c8b06243d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THIMBLE',PartNumber=N'33142111',Price=0.029397532059037,ManufacturerGuid=N'64bf33a6-7bcb-4ef5-9cb1-ade9a83d5488',IsMasterData='1' WHERE Guid = '4c45d83a-86b6-48ca-930e-d65c8b06243d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cfd74dc1-54f6-455f-8b90-ef242f1386ed')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cfd74dc1-54f6-455f-8b90-ef242f1386ed')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '922846b1-c8ef-4e07-a93f-77953c621b4b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THRUST BLOCK', N'32418313', 2.28647471570288, N'cfd74dc1-54f6-455f-8b90-ef242f1386ed', '1', '922846b1-c8ef-4e07-a93f-77953c621b4b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THRUST BLOCK',PartNumber=N'32418313',Price=2.28647471570288,ManufacturerGuid=N'cfd74dc1-54f6-455f-8b90-ef242f1386ed',IsMasterData='1' WHERE Guid = '922846b1-c8ef-4e07-a93f-77953c621b4b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cbd882dd-67a1-4308-a2ad-479920ac692f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cbd882dd-67a1-4308-a2ad-479920ac692f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fbf0b96d-c1cc-4c50-8afb-c97b70bc2ec9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THRUST BLOCK', N'3241A008', 3.58383740624244, N'cbd882dd-67a1-4308-a2ad-479920ac692f', '1', 'fbf0b96d-c1cc-4c50-8afb-c97b70bc2ec9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THRUST BLOCK',PartNumber=N'3241A008',Price=3.58383740624244,ManufacturerGuid=N'cbd882dd-67a1-4308-a2ad-479920ac692f',IsMasterData='1' WHERE Guid = 'fbf0b96d-c1cc-4c50-8afb-c97b70bc2ec9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd3cfc36f-061b-414f-b8a0-fd4ffe77fa83')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd3cfc36f-061b-414f-b8a0-fd4ffe77fa83')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9d49557c-4d0d-4f4f-bc99-8a1bef348a44')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THRUST BLOCK', N'3241N001', 1.75707718364384, N'd3cfc36f-061b-414f-b8a0-fd4ffe77fa83', '1', '9d49557c-4d0d-4f4f-bc99-8a1bef348a44')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THRUST BLOCK',PartNumber=N'3241N001',Price=1.75707718364384,ManufacturerGuid=N'd3cfc36f-061b-414f-b8a0-fd4ffe77fa83',IsMasterData='1' WHERE Guid = '9d49557c-4d0d-4f4f-bc99-8a1bef348a44'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fce55508-46a5-477e-b2cf-35008feef98d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fce55508-46a5-477e-b2cf-35008feef98d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7d722e4c-bf0a-49d5-bd10-1613a40c806c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'THRUST BLOCK', N'3242A001', 2.44495523832567, N'fce55508-46a5-477e-b2cf-35008feef98d', '1', '7d722e4c-bf0a-49d5-bd10-1613a40c806c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'THRUST BLOCK',PartNumber=N'3242A001',Price=2.44495523832567,ManufacturerGuid=N'fce55508-46a5-477e-b2cf-35008feef98d',IsMasterData='1' WHERE Guid = '7d722e4c-bf0a-49d5-bd10-1613a40c806c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a73c1c92-f5fd-4e7a-b239-8d12b28bba1e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a73c1c92-f5fd-4e7a-b239-8d12b28bba1e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '190cd351-04eb-4fd5-aa1c-9312a1438f3b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'UNION', N'3589934', 1.54851197677232, N'a73c1c92-f5fd-4e7a-b239-8d12b28bba1e', '1', '190cd351-04eb-4fd5-aa1c-9312a1438f3b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'UNION',PartNumber=N'3589934',Price=1.54851197677232,ManufacturerGuid=N'a73c1c92-f5fd-4e7a-b239-8d12b28bba1e',IsMasterData='1' WHERE Guid = '190cd351-04eb-4fd5-aa1c-9312a1438f3b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7e1f92b0-cb26-4e13-8b86-7f1b49cf5bcf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7e1f92b0-cb26-4e13-8b86-7f1b49cf5bcf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '07518693-919c-4745-949c-cf710248cbfa')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'UNION', N'0206003', 0.184369707234454, N'7e1f92b0-cb26-4e13-8b86-7f1b49cf5bcf', '1', '07518693-919c-4745-949c-cf710248cbfa')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'UNION',PartNumber=N'0206003',Price=0.184369707234454,ManufacturerGuid=N'7e1f92b0-cb26-4e13-8b86-7f1b49cf5bcf',IsMasterData='1' WHERE Guid = '07518693-919c-4745-949c-cf710248cbfa'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'c88595b8-99b9-4ba1-b81f-7b6af41733bf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'c88595b8-99b9-4ba1-b81f-7b6af41733bf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fffb2025-0db5-436a-bf5b-f2aa930cfa1a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'UNION', N'3588288', 4.6576336801355, N'c88595b8-99b9-4ba1-b81f-7b6af41733bf', '1', 'fffb2025-0db5-436a-bf5b-f2aa930cfa1a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'UNION',PartNumber=N'3588288',Price=4.6576336801355,ManufacturerGuid=N'c88595b8-99b9-4ba1-b81f-7b6af41733bf',IsMasterData='1' WHERE Guid = 'fffb2025-0db5-436a-bf5b-f2aa930cfa1a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a4de1861-2807-4918-80c6-1d67cb0ef38b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a4de1861-2807-4918-80c6-1d67cb0ef38b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '0e8b386a-4546-4d06-8f6a-8a97470d8da4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'UNION', N'3613450', 4.59714493104283, N'a4de1861-2807-4918-80c6-1d67cb0ef38b', '1', '0e8b386a-4546-4d06-8f6a-8a97470d8da4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'UNION',PartNumber=N'3613450',Price=4.59714493104283,ManufacturerGuid=N'a4de1861-2807-4918-80c6-1d67cb0ef38b',IsMasterData='1' WHERE Guid = '0e8b386a-4546-4d06-8f6a-8a97470d8da4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0d50ba10-f6a8-4e70-92c8-3b76fb9c18f8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0d50ba10-f6a8-4e70-92c8-3b76fb9c18f8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a606218c-8ecd-4d7a-b1a8-fdfc0157495c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'VALVE', N'41374013', 3.96806194047907, N'0d50ba10-f6a8-4e70-92c8-3b76fb9c18f8', '1', 'a606218c-8ecd-4d7a-b1a8-fdfc0157495c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'VALVE',PartNumber=N'41374013',Price=3.96806194047907,ManufacturerGuid=N'0d50ba10-f6a8-4e70-92c8-3b76fb9c18f8',IsMasterData='1' WHERE Guid = 'a606218c-8ecd-4d7a-b1a8-fdfc0157495c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e0370925-dbaf-4f1a-bca6-e1990ce55cf2')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e0370925-dbaf-4f1a-bca6-e1990ce55cf2')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b1fc23f9-b611-4bab-a3d0-795132ab7650')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'VALVE', N'2658207', 2.96394870554077, N'e0370925-dbaf-4f1a-bca6-e1990ce55cf2', '1', 'b1fc23f9-b611-4bab-a3d0-795132ab7650')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'VALVE',PartNumber=N'2658207',Price=2.96394870554077,ManufacturerGuid=N'e0370925-dbaf-4f1a-bca6-e1990ce55cf2',IsMasterData='1' WHERE Guid = 'b1fc23f9-b611-4bab-a3d0-795132ab7650'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7a11c6dc-22a5-4a0e-977b-a44c29ff11e9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7a11c6dc-22a5-4a0e-977b-a44c29ff11e9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2a521325-3104-4b1c-868f-fc8fa56136a6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'VALVE', N'2658301', 3.18170820227438, N'7a11c6dc-22a5-4a0e-977b-a44c29ff11e9', '1', '2a521325-3104-4b1c-868f-fc8fa56136a6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'VALVE',PartNumber=N'2658301',Price=3.18170820227438,ManufacturerGuid=N'7a11c6dc-22a5-4a0e-977b-a44c29ff11e9',IsMasterData='1' WHERE Guid = '2a521325-3104-4b1c-868f-fc8fa56136a6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aa452a14-841b-4077-98dd-b13d84d8d21c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aa452a14-841b-4077-98dd-b13d84d8d21c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f152d841-986e-4f7e-96a9-bac6feec33b6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'VALVE', N'2658A001', 3.50834744737479, N'aa452a14-841b-4077-98dd-b13d84d8d21c', '1', 'f152d841-986e-4f7e-96a9-bac6feec33b6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'VALVE',PartNumber=N'2658A001',Price=3.50834744737479,ManufacturerGuid=N'aa452a14-841b-4077-98dd-b13d84d8d21c',IsMasterData='1' WHERE Guid = 'f152d841-986e-4f7e-96a9-bac6feec33b6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f0049b61-1d40-4292-a886-7bffd006ebd4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f0049b61-1d40-4292-a886-7bffd006ebd4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bda56990-2632-4114-b52e-6758084ccc8d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'VALVE', N'6V7238', 3.70191144447133, N'f0049b61-1d40-4292-a886-7bffd006ebd4', '1', 'bda56990-2632-4114-b52e-6758084ccc8d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'VALVE',PartNumber=N'6V7238',Price=3.70191144447133,ManufacturerGuid=N'f0049b61-1d40-4292-a886-7bffd006ebd4',IsMasterData='1' WHERE Guid = 'bda56990-2632-4114-b52e-6758084ccc8d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2f4ed754-5913-478e-bfb6-e12840d271d3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2f4ed754-5913-478e-bfb6-e12840d271d3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2ced7a31-55c0-47d8-957a-1b8e199f6b2a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3321A003', 0.543309944350351, N'2f4ed754-5913-478e-bfb6-e12840d271d3', '1', '2ced7a31-55c0-47d8-957a-1b8e199f6b2a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3321A003',Price=0.543309944350351,ManufacturerGuid=N'2f4ed754-5913-478e-bfb6-e12840d271d3',IsMasterData='1' WHERE Guid = '2ced7a31-55c0-47d8-957a-1b8e199f6b2a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8540234b-b3c5-4406-94c3-d4df83e68109')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8540234b-b3c5-4406-94c3-d4df83e68109')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'df871b99-7288-4504-8138-c90243805f31')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3488608', 0.78151463827728, N'8540234b-b3c5-4406-94c3-d4df83e68109', '1', 'df871b99-7288-4504-8138-c90243805f31')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3488608',Price=0.78151463827728,ManufacturerGuid=N'8540234b-b3c5-4406-94c3-d4df83e68109',IsMasterData='1' WHERE Guid = 'df871b99-7288-4504-8138-c90243805f31'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'372f35cf-9ee0-4930-b4d7-8f817becd901')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'372f35cf-9ee0-4930-b4d7-8f817becd901')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '54a0861e-39ec-4425-8698-3f415e369389')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920002', 0.00459714493104283, N'372f35cf-9ee0-4930-b4d7-8f817becd901', '1', '54a0861e-39ec-4425-8698-3f415e369389')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920002',Price=0.00459714493104283,ManufacturerGuid=N'372f35cf-9ee0-4930-b4d7-8f817becd901',IsMasterData='1' WHERE Guid = '54a0861e-39ec-4425-8698-3f415e369389'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'89808bd9-5edd-4eee-8a1e-ddce56cab8c8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'89808bd9-5edd-4eee-8a1e-ddce56cab8c8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4ded6ee2-1b57-492e-9c9c-d3019e6bd54e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920004', 0.00556496491652553, N'89808bd9-5edd-4eee-8a1e-ddce56cab8c8', '1', '4ded6ee2-1b57-492e-9c9c-d3019e6bd54e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920004',Price=0.00556496491652553,ManufacturerGuid=N'89808bd9-5edd-4eee-8a1e-ddce56cab8c8',IsMasterData='1' WHERE Guid = '4ded6ee2-1b57-492e-9c9c-d3019e6bd54e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a292e11d-4c7e-40ea-b88b-6347a55e9bb4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a292e11d-4c7e-40ea-b88b-6347a55e9bb4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b4160bf6-76a3-4cab-b1b1-bcd99cdff362')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920007', 0.0116138398257924, N'a292e11d-4c7e-40ea-b88b-6347a55e9bb4', '1', 'b4160bf6-76a3-4cab-b1b1-bcd99cdff362')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920007',Price=0.0116138398257924,ManufacturerGuid=N'a292e11d-4c7e-40ea-b88b-6347a55e9bb4',IsMasterData='1' WHERE Guid = 'b4160bf6-76a3-4cab-b1b1-bcd99cdff362'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0ce306fd-2021-4b16-b493-1bded3b7b776')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0ce306fd-2021-4b16-b493-1bded3b7b776')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4545b561-35dc-47e1-85cd-ba8cb96dda8b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920009', 0.0452455843213162, N'0ce306fd-2021-4b16-b493-1bded3b7b776', '1', '4545b561-35dc-47e1-85cd-ba8cb96dda8b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920009',Price=0.0452455843213162,ManufacturerGuid=N'0ce306fd-2021-4b16-b493-1bded3b7b776',IsMasterData='1' WHERE Guid = '4545b561-35dc-47e1-85cd-ba8cb96dda8b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd4f44728-a797-4ddb-b7d1-aaa17330b255')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd4f44728-a797-4ddb-b7d1-aaa17330b255')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dac7d523-7c9d-4e69-8c63-03a6ba5be438')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920054', 0.0333897894991532, N'd4f44728-a797-4ddb-b7d1-aaa17330b255', '1', 'dac7d523-7c9d-4e69-8c63-03a6ba5be438')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920054',Price=0.0333897894991532,ManufacturerGuid=N'd4f44728-a797-4ddb-b7d1-aaa17330b255',IsMasterData='1' WHERE Guid = 'dac7d523-7c9d-4e69-8c63-03a6ba5be438'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'489e4a4f-c6c3-4d6f-9fb1-41c6ba803e74')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'489e4a4f-c6c3-4d6f-9fb1-41c6ba803e74')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1ce23b3a-1432-4c44-8e23-014480244847')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920101', 0.018993467215098, N'489e4a4f-c6c3-4d6f-9fb1-41c6ba803e74', '1', '1ce23b3a-1432-4c44-8e23-014480244847')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920101',Price=0.018993467215098,ManufacturerGuid=N'489e4a4f-c6c3-4d6f-9fb1-41c6ba803e74',IsMasterData='1' WHERE Guid = '1ce23b3a-1432-4c44-8e23-014480244847'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'98786cb4-c843-4a22-b6ad-102be4247aec')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'98786cb4-c843-4a22-b6ad-102be4247aec')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'f21db49a-1ef5-4f00-a0d7-f34580acac38')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920106', 0.0263730946044036, N'98786cb4-c843-4a22-b6ad-102be4247aec', '1', 'f21db49a-1ef5-4f00-a0d7-f34580acac38')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920106',Price=0.0263730946044036,ManufacturerGuid=N'98786cb4-c843-4a22-b6ad-102be4247aec',IsMasterData='1' WHERE Guid = 'f21db49a-1ef5-4f00-a0d7-f34580acac38'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'95c4d7e0-abd8-4a2f-9b14-2ed2dfdaf3f0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'95c4d7e0-abd8-4a2f-9b14-2ed2dfdaf3f0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1c4a3b59-19b3-47e5-b5fd-71aa04e657cf')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920146', 0.010041132349383, N'95c4d7e0-abd8-4a2f-9b14-2ed2dfdaf3f0', '1', '1c4a3b59-19b3-47e5-b5fd-71aa04e657cf')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920146',Price=0.010041132349383,ManufacturerGuid=N'95c4d7e0-abd8-4a2f-9b14-2ed2dfdaf3f0',IsMasterData='1' WHERE Guid = '1c4a3b59-19b3-47e5-b5fd-71aa04e657cf'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'478fffa9-66f4-419f-9a20-abcbdf40848e')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'478fffa9-66f4-419f-9a20-abcbdf40848e')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dae20a41-8c49-420e-b383-84b989f879bc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920154', 1.99612872005807, N'478fffa9-66f4-419f-9a20-abcbdf40848e', '1', 'dae20a41-8c49-420e-b383-84b989f879bc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920154',Price=1.99612872005807,ManufacturerGuid=N'478fffa9-66f4-419f-9a20-abcbdf40848e',IsMasterData='1' WHERE Guid = 'dae20a41-8c49-420e-b383-84b989f879bc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0543409b-900a-4d3b-aba2-344d8e97b75f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0543409b-900a-4d3b-aba2-344d8e97b75f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '45773170-d82c-4e62-950c-d22cd99fe165')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920155', 0.00653278490200823, N'0543409b-900a-4d3b-aba2-344d8e97b75f', '1', '45773170-d82c-4e62-950c-d22cd99fe165')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920155',Price=0.00653278490200823,ManufacturerGuid=N'0543409b-900a-4d3b-aba2-344d8e97b75f',IsMasterData='1' WHERE Guid = '45773170-d82c-4e62-950c-d22cd99fe165'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a45d8e93-316c-4c5a-8f05-18c89a1785c4')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a45d8e93-316c-4c5a-8f05-18c89a1785c4')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '80c14593-3c4b-4525-ad13-b6e60b1e9a66')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920443', 1.49165255262521, N'a45d8e93-316c-4c5a-8f05-18c89a1785c4', '1', '80c14593-3c4b-4525-ad13-b6e60b1e9a66')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920443',Price=1.49165255262521,ManufacturerGuid=N'a45d8e93-316c-4c5a-8f05-18c89a1785c4',IsMasterData='1' WHERE Guid = '80c14593-3c4b-4525-ad13-b6e60b1e9a66'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5c863855-71af-45f9-b663-597a322c8358')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5c863855-71af-45f9-b663-597a322c8358')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5d64b27b-b04a-48aa-9f66-6e7a9519226e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920467', 0.0402855068957174, N'5c863855-71af-45f9-b663-597a322c8358', '1', '5d64b27b-b04a-48aa-9f66-6e7a9519226e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920467',Price=0.0402855068957174,ManufacturerGuid=N'5c863855-71af-45f9-b663-597a322c8358',IsMasterData='1' WHERE Guid = '5d64b27b-b04a-48aa-9f66-6e7a9519226e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd6394126-f12e-441e-b60a-ff31227b8715')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd6394126-f12e-441e-b60a-ff31227b8715')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '654e80d7-bc73-47ee-afcf-02add7d3d09e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920469', 0.0407694168884587, N'd6394126-f12e-441e-b60a-ff31227b8715', '1', '654e80d7-bc73-47ee-afcf-02add7d3d09e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920469',Price=0.0407694168884587,ManufacturerGuid=N'd6394126-f12e-441e-b60a-ff31227b8715',IsMasterData='1' WHERE Guid = '654e80d7-bc73-47ee-afcf-02add7d3d09e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e63a3753-685b-4c77-8e72-ffbe4dc41d4f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e63a3753-685b-4c77-8e72-ffbe4dc41d4f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '7bdefc5d-b41b-4631-b494-852023643bd9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920471', 0.0489958867650617, N'e63a3753-685b-4c77-8e72-ffbe4dc41d4f', '1', '7bdefc5d-b41b-4631-b494-852023643bd9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920471',Price=0.0489958867650617,ManufacturerGuid=N'e63a3753-685b-4c77-8e72-ffbe4dc41d4f',IsMasterData='1' WHERE Guid = '7bdefc5d-b41b-4631-b494-852023643bd9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a461dea0-ce3f-4ce7-a94d-29f5a61b6e11')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a461dea0-ce3f-4ce7-a94d-29f5a61b6e11')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ac13a556-707d-43b7-9bde-e8c95875e794')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0920497', 0.0102830873457537, N'a461dea0-ce3f-4ce7-a94d-29f5a61b6e11', '1', 'ac13a556-707d-43b7-9bde-e8c95875e794')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0920497',Price=0.0102830873457537,ManufacturerGuid=N'a461dea0-ce3f-4ce7-a94d-29f5a61b6e11',IsMasterData='1' WHERE Guid = 'ac13a556-707d-43b7-9bde-e8c95875e794'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'40cb92c5-34f4-4438-9da9-9d6314ffbbd6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'40cb92c5-34f4-4438-9da9-9d6314ffbbd6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '21bc4aaa-cad7-4b9c-adea-227eb65b8c03')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0921173', 0.0362932494556013, N'40cb92c5-34f4-4438-9da9-9d6314ffbbd6', '1', '21bc4aaa-cad7-4b9c-adea-227eb65b8c03')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0921173',Price=0.0362932494556013,ManufacturerGuid=N'40cb92c5-34f4-4438-9da9-9d6314ffbbd6',IsMasterData='1' WHERE Guid = '21bc4aaa-cad7-4b9c-adea-227eb65b8c03'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0b65edb3-774a-416f-9dbd-d866768c4699')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0b65edb3-774a-416f-9dbd-d866768c4699')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4b6b0fe8-77ff-423b-8fb6-d54180e0db0d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'0921176', 0.00229857246552141, N'0b65edb3-774a-416f-9dbd-d866768c4699', '1', '4b6b0fe8-77ff-423b-8fb6-d54180e0db0d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'0921176',Price=0.00229857246552141,ManufacturerGuid=N'0b65edb3-774a-416f-9dbd-d866768c4699',IsMasterData='1' WHERE Guid = '4b6b0fe8-77ff-423b-8fb6-d54180e0db0d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd1fe560e-34e0-4e21-80c1-4cb10fd9dacb')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd1fe560e-34e0-4e21-80c1-4cb10fd9dacb')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '31f83ed8-e29c-4256-ab8a-51527808fc3c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A006', 0.00399225744011614, N'd1fe560e-34e0-4e21-80c1-4cb10fd9dacb', '1', '31f83ed8-e29c-4256-ab8a-51527808fc3c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A006',Price=0.00399225744011614,ManufacturerGuid=N'd1fe560e-34e0-4e21-80c1-4cb10fd9dacb',IsMasterData='1' WHERE Guid = '31f83ed8-e29c-4256-ab8a-51527808fc3c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1255e627-9ec7-4ec3-bc63-df778a96be8f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1255e627-9ec7-4ec3-bc63-df778a96be8f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'edb2a1f4-9f74-4a21-b398-e25a0257db50')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A008', 0.00604887490926688, N'1255e627-9ec7-4ec3-bc63-df778a96be8f', '1', 'edb2a1f4-9f74-4a21-b398-e25a0257db50')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A008',Price=0.00604887490926688,ManufacturerGuid=N'1255e627-9ec7-4ec3-bc63-df778a96be8f',IsMasterData='1' WHERE Guid = 'edb2a1f4-9f74-4a21-b398-e25a0257db50'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7ae184fa-909b-4879-a018-932550b19c26')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7ae184fa-909b-4879-a018-932550b19c26')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '798a35c4-248d-412f-a4f1-0a77c70eecd5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A010', 0.0120977498185338, N'7ae184fa-909b-4879-a018-932550b19c26', '1', '798a35c4-248d-412f-a4f1-0a77c70eecd5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A010',Price=0.0120977498185338,ManufacturerGuid=N'7ae184fa-909b-4879-a018-932550b19c26',IsMasterData='1' WHERE Guid = '798a35c4-248d-412f-a4f1-0a77c70eecd5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8249a0c6-953f-4cf1-85bb-161cce07abe0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8249a0c6-953f-4cf1-85bb-161cce07abe0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c4200b61-a191-42a8-a879-7a9234817fe3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A016', 0.03762400193564, N'8249a0c6-953f-4cf1-85bb-161cce07abe0', '1', 'c4200b61-a191-42a8-a879-7a9234817fe3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A016',Price=0.03762400193564,ManufacturerGuid=N'8249a0c6-953f-4cf1-85bb-161cce07abe0',IsMasterData='1' WHERE Guid = 'c4200b61-a191-42a8-a879-7a9234817fe3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4474fde6-886e-436e-a4a5-20b0b0737cb8')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4474fde6-886e-436e-a4a5-20b0b0737cb8')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9fb61bdf-808e-404e-abd1-5c1de8a53f69')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A018', 2.23808371642874, N'4474fde6-886e-436e-a4a5-20b0b0737cb8', '1', '9fb61bdf-808e-404e-abd1-5c1de8a53f69')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A018',Price=2.23808371642874,ManufacturerGuid=N'4474fde6-886e-436e-a4a5-20b0b0737cb8',IsMasterData='1' WHERE Guid = '9fb61bdf-808e-404e-abd1-5c1de8a53f69'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'decdd70f-a83c-44a6-a311-95c6c689114b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'decdd70f-a83c-44a6-a311-95c6c689114b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5adb0d3d-72c1-4034-8658-854b1224986b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A506', 0.0725864989112025, N'decdd70f-a83c-44a6-a311-95c6c689114b', '1', '5adb0d3d-72c1-4034-8658-854b1224986b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A506',Price=0.0725864989112025,ManufacturerGuid=N'decdd70f-a83c-44a6-a311-95c6c689114b',IsMasterData='1' WHERE Guid = '5adb0d3d-72c1-4034-8658-854b1224986b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6fbd1193-6618-4aa4-9bcf-8a6ff7ceb062')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6fbd1193-6618-4aa4-9bcf-8a6ff7ceb062')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '205bbf29-95e3-4f6d-b813-d3333cdb62fa')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A507', 0.00701669489474958, N'6fbd1193-6618-4aa4-9bcf-8a6ff7ceb062', '1', '205bbf29-95e3-4f6d-b813-d3333cdb62fa')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A507',Price=0.00701669489474958,ManufacturerGuid=N'6fbd1193-6618-4aa4-9bcf-8a6ff7ceb062',IsMasterData='1' WHERE Guid = '205bbf29-95e3-4f6d-b813-d3333cdb62fa'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'f9bc57a8-a0ab-49c3-be32-c86a4520dc81')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'f9bc57a8-a0ab-49c3-be32-c86a4520dc81')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9ddcdb18-2739-43f2-b0e0-5e78f6122ac7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A508', 0.169368497459473, N'f9bc57a8-a0ab-49c3-be32-c86a4520dc81', '1', '9ddcdb18-2739-43f2-b0e0-5e78f6122ac7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A508',Price=0.169368497459473,ManufacturerGuid=N'f9bc57a8-a0ab-49c3-be32-c86a4520dc81',IsMasterData='1' WHERE Guid = '9ddcdb18-2739-43f2-b0e0-5e78f6122ac7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5730c5e1-c29f-4300-beae-e817dc51d4e6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5730c5e1-c29f-4300-beae-e817dc51d4e6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '1654082a-232d-46c7-a1b0-4145bf542396')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A513', 0.289499153157513, N'5730c5e1-c29f-4300-beae-e817dc51d4e6', '1', '1654082a-232d-46c7-a1b0-4145bf542396')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A513',Price=0.289499153157513,ManufacturerGuid=N'5730c5e1-c29f-4300-beae-e817dc51d4e6',IsMasterData='1' WHERE Guid = '1654082a-232d-46c7-a1b0-4145bf542396'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'87848983-2a40-41bd-bebe-c97d2711bead')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'87848983-2a40-41bd-bebe-c97d2711bead')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '2510184e-44e4-4d03-af31-27e2178b8acb')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A514', 0.0327849020082265, N'87848983-2a40-41bd-bebe-c97d2711bead', '1', '2510184e-44e4-4d03-af31-27e2178b8acb')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A514',Price=0.0327849020082265,ManufacturerGuid=N'87848983-2a40-41bd-bebe-c97d2711bead',IsMasterData='1' WHERE Guid = '2510184e-44e4-4d03-af31-27e2178b8acb'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'708401d0-88c4-4277-ac16-05b2cd495779')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'708401d0-88c4-4277-ac16-05b2cd495779')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b277ff32-db21-4b0e-bc2d-18808cab02fc')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2131A901', 0.0427050568594241, N'708401d0-88c4-4277-ac16-05b2cd495779', '1', 'b277ff32-db21-4b0e-bc2d-18808cab02fc')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2131A901',Price=0.0427050568594241,ManufacturerGuid=N'708401d0-88c4-4277-ac16-05b2cd495779',IsMasterData='1' WHERE Guid = 'b277ff32-db21-4b0e-bc2d-18808cab02fc'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'22194f06-60c3-413b-9c63-3ab4a8b35ba0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'22194f06-60c3-413b-9c63-3ab4a8b35ba0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '79540c7f-0944-456f-86b4-29d1244f7b61')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2134A008', 0.00302443745463344, N'22194f06-60c3-413b-9c63-3ab4a8b35ba0', '1', '79540c7f-0944-456f-86b4-29d1244f7b61')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2134A008',Price=0.00302443745463344,ManufacturerGuid=N'22194f06-60c3-413b-9c63-3ab4a8b35ba0',IsMasterData='1' WHERE Guid = '79540c7f-0944-456f-86b4-29d1244f7b61'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'98262fb2-3f75-41c0-8193-1550f7dd1ca5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'98262fb2-3f75-41c0-8193-1550f7dd1ca5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5c6d2769-829e-4d5d-8cc3-eedbb717aafd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2134A010', 0.00834744737478829, N'98262fb2-3f75-41c0-8193-1550f7dd1ca5', '1', '5c6d2769-829e-4d5d-8cc3-eedbb717aafd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2134A010',Price=0.00834744737478829,ManufacturerGuid=N'98262fb2-3f75-41c0-8193-1550f7dd1ca5',IsMasterData='1' WHERE Guid = '5c6d2769-829e-4d5d-8cc3-eedbb717aafd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fdf79deb-0a85-4bd4-9088-29defe478110')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fdf79deb-0a85-4bd4-9088-29defe478110')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '8cd6f548-1576-4304-95e1-1a8820e5fee5')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2134A609', 0.0140333897894992, N'fdf79deb-0a85-4bd4-9088-29defe478110', '1', '8cd6f548-1576-4304-95e1-1a8820e5fee5')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2134A609',Price=0.0140333897894992,ManufacturerGuid=N'fdf79deb-0a85-4bd4-9088-29defe478110',IsMasterData='1' WHERE Guid = '8cd6f548-1576-4304-95e1-1a8820e5fee5'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'628a2517-22aa-4bfe-ae3a-ab3c13bc1655')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'628a2517-22aa-4bfe-ae3a-ab3c13bc1655')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a2dfbb99-9934-4fda-887c-c746cfe032de')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2135A222', 0.188966852165497, N'628a2517-22aa-4bfe-ae3a-ab3c13bc1655', '1', 'a2dfbb99-9934-4fda-887c-c746cfe032de')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2135A222',Price=0.188966852165497,ManufacturerGuid=N'628a2517-22aa-4bfe-ae3a-ab3c13bc1655',IsMasterData='1' WHERE Guid = 'a2dfbb99-9934-4fda-887c-c746cfe032de'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'36ac590b-3808-4b77-ac1b-1c8384207e6f')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'36ac590b-3808-4b77-ac1b-1c8384207e6f')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '00f5301f-b3c4-426f-833e-ba446e148f1a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411105', 3.04863295427051, N'36ac590b-3808-4b77-ac1b-1c8384207e6f', '1', '00f5301f-b3c4-426f-833e-ba446e148f1a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411105',Price=3.04863295427051,ManufacturerGuid=N'36ac590b-3808-4b77-ac1b-1c8384207e6f',IsMasterData='1' WHERE Guid = '00f5301f-b3c4-426f-833e-ba446e148f1a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e6d372d6-017a-4568-bc50-a787e66f3f4b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e6d372d6-017a-4568-bc50-a787e66f3f4b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '866dd50a-8bac-428c-9cfb-8b3c19982a8e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411118', 0.0342366319864505, N'e6d372d6-017a-4568-bc50-a787e66f3f4b', '1', '866dd50a-8bac-428c-9cfb-8b3c19982a8e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411118',Price=0.0342366319864505,ManufacturerGuid=N'e6d372d6-017a-4568-bc50-a787e66f3f4b',IsMasterData='1' WHERE Guid = '866dd50a-8bac-428c-9cfb-8b3c19982a8e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'68f224ca-3a19-4dd2-9f57-3ac4abda0da0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'68f224ca-3a19-4dd2-9f57-3ac4abda0da0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dce0241d-737d-4cc9-a5a6-ea257becc044')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411141', 1.81466247278006, N'68f224ca-3a19-4dd2-9f57-3ac4abda0da0', '1', 'dce0241d-737d-4cc9-a5a6-ea257becc044')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411141',Price=1.81466247278006,ManufacturerGuid=N'68f224ca-3a19-4dd2-9f57-3ac4abda0da0',IsMasterData='1' WHERE Guid = 'dce0241d-737d-4cc9-a5a6-ea257becc044'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6820dc84-f9fa-425e-bfb6-0de9c2767d31')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6820dc84-f9fa-425e-bfb6-0de9c2767d31')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ce322430-8e29-4ff2-aef3-d9c3c86233e7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411154', 0.0344785869828212, N'6820dc84-f9fa-425e-bfb6-0de9c2767d31', '1', 'ce322430-8e29-4ff2-aef3-d9c3c86233e7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411154',Price=0.0344785869828212,ManufacturerGuid=N'6820dc84-f9fa-425e-bfb6-0de9c2767d31',IsMasterData='1' WHERE Guid = 'ce322430-8e29-4ff2-aef3-d9c3c86233e7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'afdab279-1935-4911-812b-bf3afe681b9d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'afdab279-1935-4911-812b-bf3afe681b9d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bb06267a-ee37-48dc-84f2-e17799abe471')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411157', 0.0123397048149044, N'afdab279-1935-4911-812b-bf3afe681b9d', '1', 'bb06267a-ee37-48dc-84f2-e17799abe471')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411157',Price=0.0123397048149044,ManufacturerGuid=N'afdab279-1935-4911-812b-bf3afe681b9d',IsMasterData='1' WHERE Guid = 'bb06267a-ee37-48dc-84f2-e17799abe471'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'bdf78a04-bdb1-4fd0-bb76-dd297c4a8072')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'bdf78a04-bdb1-4fd0-bb76-dd297c4a8072')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3df6211b-fa7e-41de-bd5d-6ada75eff4be')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411B122', 0.048390999274135, N'bdf78a04-bdb1-4fd0-bb76-dd297c4a8072', '1', '3df6211b-fa7e-41de-bd5d-6ada75eff4be')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411B122',Price=0.048390999274135,ManufacturerGuid=N'bdf78a04-bdb1-4fd0-bb76-dd297c4a8072',IsMasterData='1' WHERE Guid = '3df6211b-fa7e-41de-bd5d-6ada75eff4be'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6246cec8-5d7e-44db-8bb3-31c862bc11b1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6246cec8-5d7e-44db-8bb3-31c862bc11b1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b40be504-2bbd-4a23-8dc9-6fa86d0adfb2')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D006', 0.0459714493104283, N'6246cec8-5d7e-44db-8bb3-31c862bc11b1', '1', 'b40be504-2bbd-4a23-8dc9-6fa86d0adfb2')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D006',Price=0.0459714493104283,ManufacturerGuid=N'6246cec8-5d7e-44db-8bb3-31c862bc11b1',IsMasterData='1' WHERE Guid = 'b40be504-2bbd-4a23-8dc9-6fa86d0adfb2'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1716d40f-5db2-4747-9bf2-360b04a54729')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1716d40f-5db2-4747-9bf2-360b04a54729')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd04f3749-7399-4a75-8d0a-720b06c6c746')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D007', 0.0682313089765304, N'1716d40f-5db2-4747-9bf2-360b04a54729', '1', 'd04f3749-7399-4a75-8d0a-720b06c6c746')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D007',Price=0.0682313089765304,ManufacturerGuid=N'1716d40f-5db2-4747-9bf2-360b04a54729',IsMasterData='1' WHERE Guid = 'd04f3749-7399-4a75-8d0a-720b06c6c746'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd695b5cd-2696-4416-8d9f-11aa6bea28f3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd695b5cd-2696-4416-8d9f-11aa6bea28f3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '556f2f02-3b7d-4138-852d-5b06a76a1b77')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D008', 0.0809339462859908, N'd695b5cd-2696-4416-8d9f-11aa6bea28f3', '1', '556f2f02-3b7d-4138-852d-5b06a76a1b77')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D008',Price=0.0809339462859908,ManufacturerGuid=N'd695b5cd-2696-4416-8d9f-11aa6bea28f3',IsMasterData='1' WHERE Guid = '556f2f02-3b7d-4138-852d-5b06a76a1b77'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'59bf644d-06c6-4421-8aed-04fc943d1daf')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'59bf644d-06c6-4421-8aed-04fc943d1daf')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'ef5128ea-1cff-475b-84ed-dc6095b01d3a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D010', 0.103435760948464, N'59bf644d-06c6-4421-8aed-04fc943d1daf', '1', 'ef5128ea-1cff-475b-84ed-dc6095b01d3a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D010',Price=0.103435760948464,ManufacturerGuid=N'59bf644d-06c6-4421-8aed-04fc943d1daf',IsMasterData='1' WHERE Guid = 'ef5128ea-1cff-475b-84ed-dc6095b01d3a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b00c6df5-b3be-4e2c-93a7-d144b83a7263')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b00c6df5-b3be-4e2c-93a7-d144b83a7263')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4d738bce-61b4-4b08-bc98-f8e44a32b247')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D012', 0.112267118315993, N'b00c6df5-b3be-4e2c-93a7-d144b83a7263', '1', '4d738bce-61b4-4b08-bc98-f8e44a32b247')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D012',Price=0.112267118315993,ManufacturerGuid=N'b00c6df5-b3be-4e2c-93a7-d144b83a7263',IsMasterData='1' WHERE Guid = '4d738bce-61b4-4b08-bc98-f8e44a32b247'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e4f5f19b-bdc5-44d9-b249-eb46a8ccdb30')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e4f5f19b-bdc5-44d9-b249-eb46a8ccdb30')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd1ccd1e0-314b-43ab-99e0-d947213f1fef')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D013', 0.118557948221631, N'e4f5f19b-bdc5-44d9-b249-eb46a8ccdb30', '1', 'd1ccd1e0-314b-43ab-99e0-d947213f1fef')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D013',Price=0.118557948221631,ManufacturerGuid=N'e4f5f19b-bdc5-44d9-b249-eb46a8ccdb30',IsMasterData='1' WHERE Guid = 'd1ccd1e0-314b-43ab-99e0-d947213f1fef'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a8a2931f-54c6-477f-87b8-81ee7f21977b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a8a2931f-54c6-477f-87b8-81ee7f21977b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a41c47f1-afd9-4f3f-a117-a18367c7300d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D016', 0.125211710621824, N'a8a2931f-54c6-477f-87b8-81ee7f21977b', '1', 'a41c47f1-afd9-4f3f-a117-a18367c7300d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D016',Price=0.125211710621824,ManufacturerGuid=N'a8a2931f-54c6-477f-87b8-81ee7f21977b',IsMasterData='1' WHERE Guid = 'a41c47f1-afd9-4f3f-a117-a18367c7300d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e177d077-3eba-40b8-b5ba-dbea89578ea9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e177d077-3eba-40b8-b5ba-dbea89578ea9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '059145c7-49e2-416e-b7d1-e6bb491c7f39')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D018', 0.547181224292282, N'e177d077-3eba-40b8-b5ba-dbea89578ea9', '1', '059145c7-49e2-416e-b7d1-e6bb491c7f39')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D018',Price=0.547181224292282,ManufacturerGuid=N'e177d077-3eba-40b8-b5ba-dbea89578ea9',IsMasterData='1' WHERE Guid = '059145c7-49e2-416e-b7d1-e6bb491c7f39'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8c2f7a40-a8f8-4afd-b4f0-3351069f9d23')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8c2f7a40-a8f8-4afd-b4f0-3351069f9d23')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '30ac1e73-68c2-432a-bfe5-16b22a827067')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411D021', 1.03242196951367, N'8c2f7a40-a8f8-4afd-b4f0-3351069f9d23', '1', '30ac1e73-68c2-432a-bfe5-16b22a827067')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411D021',Price=1.03242196951367,ManufacturerGuid=N'8c2f7a40-a8f8-4afd-b4f0-3351069f9d23',IsMasterData='1' WHERE Guid = '30ac1e73-68c2-432a-bfe5-16b22a827067'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4adb8152-91c2-4dba-abe4-443d1523c626')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4adb8152-91c2-4dba-abe4-443d1523c626')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e177080d-0448-4978-b2c6-8d6ab464d1d6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415928', 0.0941204935881926, N'4adb8152-91c2-4dba-abe4-443d1523c626', '1', 'e177080d-0448-4978-b2c6-8d6ab464d1d6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415928',Price=0.0941204935881926,ManufacturerGuid=N'4adb8152-91c2-4dba-abe4-443d1523c626',IsMasterData='1' WHERE Guid = 'e177080d-0448-4978-b2c6-8d6ab464d1d6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7b5edf72-2732-4194-9899-29985dce99a1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7b5edf72-2732-4194-9899-29985dce99a1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cfa462a0-ffb8-4b17-b1e3-9896591c0a70')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415931', 0.144326155335108, N'7b5edf72-2732-4194-9899-29985dce99a1', '1', 'cfa462a0-ffb8-4b17-b1e3-9896591c0a70')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415931',Price=0.144326155335108,ManufacturerGuid=N'7b5edf72-2732-4194-9899-29985dce99a1',IsMasterData='1' WHERE Guid = 'cfa462a0-ffb8-4b17-b1e3-9896591c0a70'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'd958c64a-349f-47cb-9c84-c7867c1d1cde')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'd958c64a-349f-47cb-9c84-c7867c1d1cde')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '16a5154d-8749-41ce-92bb-9f036904eb00')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D002', 0.100169368497459, N'd958c64a-349f-47cb-9c84-c7867c1d1cde', '1', '16a5154d-8749-41ce-92bb-9f036904eb00')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D002',Price=0.100169368497459,ManufacturerGuid=N'd958c64a-349f-47cb-9c84-c7867c1d1cde',IsMasterData='1' WHERE Guid = '16a5154d-8749-41ce-92bb-9f036904eb00'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'a67bed82-1280-44d3-91fc-2c0ec122c4b1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'a67bed82-1280-44d3-91fc-2c0ec122c4b1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '272a913c-0625-4837-b1e0-097ac61460a4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D003', 0.0966610210500847, N'a67bed82-1280-44d3-91fc-2c0ec122c4b1', '1', '272a913c-0625-4837-b1e0-097ac61460a4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D003',Price=0.0966610210500847,ManufacturerGuid=N'a67bed82-1280-44d3-91fc-2c0ec122c4b1',IsMasterData='1' WHERE Guid = '272a913c-0625-4837-b1e0-097ac61460a4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'5062de59-9b15-4292-acc0-33407fe9cd1d')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'5062de59-9b15-4292-acc0-33407fe9cd1d')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '154df7a5-424d-4052-9c5f-f049b5cfa664')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D051', 0.1146866682797, N'5062de59-9b15-4292-acc0-33407fe9cd1d', '1', '154df7a5-424d-4052-9c5f-f049b5cfa664')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D051',Price=0.1146866682797,ManufacturerGuid=N'5062de59-9b15-4292-acc0-33407fe9cd1d',IsMasterData='1' WHERE Guid = '154df7a5-424d-4052-9c5f-f049b5cfa664'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e5b1a5af-b61c-4246-8c0d-ebc9f20d6639')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e5b1a5af-b61c-4246-8c0d-ebc9f20d6639')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e1a6a7fc-a763-4d46-a507-a6e22445b84f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D053', 0.244858456327123, N'e5b1a5af-b61c-4246-8c0d-ebc9f20d6639', '1', 'e1a6a7fc-a763-4d46-a507-a6e22445b84f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D053',Price=0.244858456327123,ManufacturerGuid=N'e5b1a5af-b61c-4246-8c0d-ebc9f20d6639',IsMasterData='1' WHERE Guid = 'e1a6a7fc-a763-4d46-a507-a6e22445b84f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'fba33617-befe-4458-92c4-6bbee971afbc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'fba33617-befe-4458-92c4-6bbee971afbc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'df8adcb8-2ce5-4020-8d24-8867bdbea810')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D104', 1.1872731671909, N'fba33617-befe-4458-92c4-6bbee971afbc', '1', 'df8adcb8-2ce5-4020-8d24-8867bdbea810')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D104',Price=1.1872731671909,ManufacturerGuid=N'fba33617-befe-4458-92c4-6bbee971afbc',IsMasterData='1' WHERE Guid = 'df8adcb8-2ce5-4020-8d24-8867bdbea810'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'872c5c0d-66a6-4fc6-aaa9-e7e814e2dcef')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'872c5c0d-66a6-4fc6-aaa9-e7e814e2dcef')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3a745145-bb5a-43b7-a200-e7df4e0c7c19')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D108', 0.255504476167433, N'872c5c0d-66a6-4fc6-aaa9-e7e814e2dcef', '1', '3a745145-bb5a-43b7-a200-e7df4e0c7c19')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D108',Price=0.255504476167433,ManufacturerGuid=N'872c5c0d-66a6-4fc6-aaa9-e7e814e2dcef',IsMasterData='1' WHERE Guid = '3a745145-bb5a-43b7-a200-e7df4e0c7c19'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'90cf8c7b-409a-4c9d-83e3-015f44a932bd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'90cf8c7b-409a-4c9d-83e3-015f44a932bd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '111c0fc6-73a6-48f2-87e9-dd3fcfdffe6d')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D152', 0.233365593999516, N'90cf8c7b-409a-4c9d-83e3-015f44a932bd', '1', '111c0fc6-73a6-48f2-87e9-dd3fcfdffe6d')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D152',Price=0.233365593999516,ManufacturerGuid=N'90cf8c7b-409a-4c9d-83e3-015f44a932bd',IsMasterData='1' WHERE Guid = '111c0fc6-73a6-48f2-87e9-dd3fcfdffe6d'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'80ff36ec-a9f7-45f4-8d9a-22bb4792dc8b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'80ff36ec-a9f7-45f4-8d9a-22bb4792dc8b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '74994ed1-f5c8-4f13-8284-64ec0d38fd84')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D209', 0.816961045245584, N'80ff36ec-a9f7-45f4-8d9a-22bb4792dc8b', '1', '74994ed1-f5c8-4f13-8284-64ec0d38fd84')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D209',Price=0.816961045245584,ManufacturerGuid=N'80ff36ec-a9f7-45f4-8d9a-22bb4792dc8b',IsMasterData='1' WHERE Guid = '74994ed1-f5c8-4f13-8284-64ec0d38fd84'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'99135126-f927-4368-9110-fb42c93f7b6c')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'99135126-f927-4368-9110-fb42c93f7b6c')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a992c5a8-13c6-4b63-9075-43ffd6aceef6')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2415D253', 0.231067021533995, N'99135126-f927-4368-9110-fb42c93f7b6c', '1', 'a992c5a8-13c6-4b63-9075-43ffd6aceef6')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2415D253',Price=0.231067021533995,ManufacturerGuid=N'99135126-f927-4368-9110-fb42c93f7b6c',IsMasterData='1' WHERE Guid = 'a992c5a8-13c6-4b63-9075-43ffd6aceef6'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'49265761-61f9-4314-8f81-15da2f39aae3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'49265761-61f9-4314-8f81-15da2f39aae3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '4c70da6d-72b1-45f9-bae9-4b4362a9d806')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2656454', 0.786353738204694, N'49265761-61f9-4314-8f81-15da2f39aae3', '1', '4c70da6d-72b1-45f9-bae9-4b4362a9d806')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2656454',Price=0.786353738204694,ManufacturerGuid=N'49265761-61f9-4314-8f81-15da2f39aae3',IsMasterData='1' WHERE Guid = '4c70da6d-72b1-45f9-bae9-4b4362a9d806'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'60ba00ae-d300-421e-b07d-b4a90ed2de90')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'60ba00ae-d300-421e-b07d-b4a90ed2de90')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'a8b17d63-c408-4b47-a59c-b21686e80976')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3241N002', 2.23808371642874, N'60ba00ae-d300-421e-b07d-b4a90ed2de90', '1', 'a8b17d63-c408-4b47-a59c-b21686e80976')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3241N002',Price=2.23808371642874,ManufacturerGuid=N'60ba00ae-d300-421e-b07d-b4a90ed2de90',IsMasterData='1' WHERE Guid = 'a8b17d63-c408-4b47-a59c-b21686e80976'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'adbd2b06-0e4d-425e-ac6b-ec2f343272fe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'adbd2b06-0e4d-425e-ac6b-ec2f343272fe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'c92f950d-aadb-44d3-99cb-9e0174bc17ee')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33114411', 0.127026373094604, N'adbd2b06-0e4d-425e-ac6b-ec2f343272fe', '1', 'c92f950d-aadb-44d3-99cb-9e0174bc17ee')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33114411',Price=0.127026373094604,ManufacturerGuid=N'adbd2b06-0e4d-425e-ac6b-ec2f343272fe',IsMasterData='1' WHERE Guid = 'c92f950d-aadb-44d3-99cb-9e0174bc17ee'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'8740316e-e1c7-4eba-9622-036de7f8b555')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'8740316e-e1c7-4eba-9622-036de7f8b555')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '316be8d4-e824-46ca-814a-66c41402d5c4')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33114412', 0.0578272441325913, N'8740316e-e1c7-4eba-9622-036de7f8b555', '1', '316be8d4-e824-46ca-814a-66c41402d5c4')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33114412',Price=0.0578272441325913,ManufacturerGuid=N'8740316e-e1c7-4eba-9622-036de7f8b555',IsMasterData='1' WHERE Guid = '316be8d4-e824-46ca-814a-66c41402d5c4'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'aba77af3-3339-400f-a4c4-898a4267b10b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'aba77af3-3339-400f-a4c4-898a4267b10b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bd38e5a1-175e-42e5-866a-0e9ed99fd91b')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33115117', 0.0203242196951367, N'aba77af3-3339-400f-a4c4-898a4267b10b', '1', 'bd38e5a1-175e-42e5-866a-0e9ed99fd91b')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33115117',Price=0.0203242196951367,ManufacturerGuid=N'aba77af3-3339-400f-a4c4-898a4267b10b',IsMasterData='1' WHERE Guid = 'bd38e5a1-175e-42e5-866a-0e9ed99fd91b'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'671a6e4a-5a25-44b6-83ef-6c8bdf807683')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'671a6e4a-5a25-44b6-83ef-6c8bdf807683')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '39e073b9-8f70-4e34-9b48-77fe5cd7479e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33117127', 0.029397532059037, N'671a6e4a-5a25-44b6-83ef-6c8bdf807683', '1', '39e073b9-8f70-4e34-9b48-77fe5cd7479e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33117127',Price=0.029397532059037,ManufacturerGuid=N'671a6e4a-5a25-44b6-83ef-6c8bdf807683',IsMasterData='1' WHERE Guid = '39e073b9-8f70-4e34-9b48-77fe5cd7479e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'b8757510-c4e2-4dca-ad30-72fd2291d359')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'b8757510-c4e2-4dca-ad30-72fd2291d359')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '99c57004-4063-44ef-bd3a-3c6c0e61e04a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33117434', 0.314541495281878, N'b8757510-c4e2-4dca-ad30-72fd2291d359', '1', '99c57004-4063-44ef-bd3a-3c6c0e61e04a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33117434',Price=0.314541495281878,ManufacturerGuid=N'b8757510-c4e2-4dca-ad30-72fd2291d359',IsMasterData='1' WHERE Guid = '99c57004-4063-44ef-bd3a-3c6c0e61e04a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'000237e1-aedf-47ce-928a-7235199fb47b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'000237e1-aedf-47ce-928a-7235199fb47b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5a666fd8-7d79-4c47-bec4-676d21c3fb83')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3311A011', 0.0225018146624728, N'000237e1-aedf-47ce-928a-7235199fb47b', '1', '5a666fd8-7d79-4c47-bec4-676d21c3fb83')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3311A011',Price=0.0225018146624728,ManufacturerGuid=N'000237e1-aedf-47ce-928a-7235199fb47b',IsMasterData='1' WHERE Guid = '5a666fd8-7d79-4c47-bec4-676d21c3fb83'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1d026da3-2cd0-41d5-94fe-8fe159b0840b')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1d026da3-2cd0-41d5-94fe-8fe159b0840b')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '75b71e1c-34b7-4e95-b44f-26bc1c682cf3')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3311A015', 0.01790466973143, N'1d026da3-2cd0-41d5-94fe-8fe159b0840b', '1', '75b71e1c-34b7-4e95-b44f-26bc1c682cf3')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3311A015',Price=0.01790466973143,ManufacturerGuid=N'1d026da3-2cd0-41d5-94fe-8fe159b0840b',IsMasterData='1' WHERE Guid = '75b71e1c-34b7-4e95-b44f-26bc1c682cf3'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6fce8b97-5782-4392-bc7a-bba7052058d1')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6fce8b97-5782-4392-bc7a-bba7052058d1')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '80c2787b-f4a0-4659-a906-7395c16ba06f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3311A052', 0.223808371642874, N'6fce8b97-5782-4392-bc7a-bba7052058d1', '1', '80c2787b-f4a0-4659-a906-7395c16ba06f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3311A052',Price=0.223808371642874,ManufacturerGuid=N'6fce8b97-5782-4392-bc7a-bba7052058d1',IsMasterData='1' WHERE Guid = '80c2787b-f4a0-4659-a906-7395c16ba06f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'1b719849-4695-4fcc-86a9-0d0af7638455')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'1b719849-4695-4fcc-86a9-0d0af7638455')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'aa13a544-14ca-4df1-b303-2771633bdc8e')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3311K011', 0.200096781998548, N'1b719849-4695-4fcc-86a9-0d0af7638455', '1', 'aa13a544-14ca-4df1-b303-2771633bdc8e')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3311K011',Price=0.200096781998548,ManufacturerGuid=N'1b719849-4695-4fcc-86a9-0d0af7638455',IsMasterData='1' WHERE Guid = 'aa13a544-14ca-4df1-b303-2771633bdc8e'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'0b0399db-7bbd-46cb-aed6-554dbeebd9ed')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'0b0399db-7bbd-46cb-aed6-554dbeebd9ed')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b04e37e7-cf5b-44dd-a9a7-f8b4d1d287e7')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33123125', 0.126179530607307, N'0b0399db-7bbd-46cb-aed6-554dbeebd9ed', '1', 'b04e37e7-cf5b-44dd-a9a7-f8b4d1d287e7')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33123125',Price=0.126179530607307,ManufacturerGuid=N'0b0399db-7bbd-46cb-aed6-554dbeebd9ed',IsMasterData='1' WHERE Guid = 'b04e37e7-cf5b-44dd-a9a7-f8b4d1d287e7'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'db405da9-63fa-4271-ac81-7c17dd2e3efd')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'db405da9-63fa-4271-ac81-7c17dd2e3efd')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '902f02bf-0b60-4e75-85e4-4881b0231814')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33124118', 0.0846842487297363, N'db405da9-63fa-4271-ac81-7c17dd2e3efd', '1', '902f02bf-0b60-4e75-85e4-4881b0231814')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33124118',Price=0.0846842487297363,ManufacturerGuid=N'db405da9-63fa-4271-ac81-7c17dd2e3efd',IsMasterData='1' WHERE Guid = '902f02bf-0b60-4e75-85e4-4881b0231814'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ed65306d-06b3-4a2a-b22f-b2e99d66ba14')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ed65306d-06b3-4a2a-b22f-b2e99d66ba14')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'd15161af-e409-4d78-814e-dd3d548cf3d9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33135117', 0.498427292523591, N'ed65306d-06b3-4a2a-b22f-b2e99d66ba14', '1', 'd15161af-e409-4d78-814e-dd3d548cf3d9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33135117',Price=0.498427292523591,ManufacturerGuid=N'ed65306d-06b3-4a2a-b22f-b2e99d66ba14',IsMasterData='1' WHERE Guid = 'd15161af-e409-4d78-814e-dd3d548cf3d9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'377c1a7a-9191-4dd6-88cc-f68791751ff0')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'377c1a7a-9191-4dd6-88cc-f68791751ff0')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '3234f05a-763a-4ea6-8ccd-3f8af8478590')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3314P004', 0.0373820469392693, N'377c1a7a-9191-4dd6-88cc-f68791751ff0', '1', '3234f05a-763a-4ea6-8ccd-3f8af8478590')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3314P004',Price=0.0373820469392693,ManufacturerGuid=N'377c1a7a-9191-4dd6-88cc-f68791751ff0',IsMasterData='1' WHERE Guid = '3234f05a-763a-4ea6-8ccd-3f8af8478590'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'29f34387-7004-4a78-a6c9-8dbc7cd8ba06')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'29f34387-7004-4a78-a6c9-8dbc7cd8ba06')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'cc44dca4-7db2-4a5b-bf72-79dd888c2794')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'33415118', 0.0812968787805468, N'29f34387-7004-4a78-a6c9-8dbc7cd8ba06', '1', 'cc44dca4-7db2-4a5b-bf72-79dd888c2794')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'33415118',Price=0.0812968787805468,ManufacturerGuid=N'29f34387-7004-4a78-a6c9-8dbc7cd8ba06',IsMasterData='1' WHERE Guid = 'cc44dca4-7db2-4a5b-bf72-79dd888c2794'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e8423b9e-85b1-4c61-bc58-2bbab97b344a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e8423b9e-85b1-4c61-bc58-2bbab97b344a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b7522e60-7b1d-4dc1-9328-6d2adee0b2ed')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3369965', 0.858940237115896, N'e8423b9e-85b1-4c61-bc58-2bbab97b344a', '1', 'b7522e60-7b1d-4dc1-9328-6d2adee0b2ed')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3369965',Price=0.858940237115896,ManufacturerGuid=N'e8423b9e-85b1-4c61-bc58-2bbab97b344a',IsMasterData='1' WHERE Guid = 'b7522e60-7b1d-4dc1-9328-6d2adee0b2ed'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'7b724d91-c5e7-4b39-98c8-ee4d643ab2da')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'7b724d91-c5e7-4b39-98c8-ee4d643ab2da')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '9e5ef22e-2a7d-4a64-8d7f-e25222caa741')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3369966', 0.943624485845633, N'7b724d91-c5e7-4b39-98c8-ee4d643ab2da', '1', '9e5ef22e-2a7d-4a64-8d7f-e25222caa741')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3369966',Price=0.943624485845633,ManufacturerGuid=N'7b724d91-c5e7-4b39-98c8-ee4d643ab2da',IsMasterData='1' WHERE Guid = '9e5ef22e-2a7d-4a64-8d7f-e25222caa741'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'2c42d9ff-762f-43a4-8960-316286bedf3a')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'2c42d9ff-762f-43a4-8960-316286bedf3a')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '5383e49e-6a19-4b01-a3fd-f3d19afbbf9c')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3683158', 1.12509073312364, N'2c42d9ff-762f-43a4-8960-316286bedf3a', '1', '5383e49e-6a19-4b01-a3fd-f3d19afbbf9c')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3683158',Price=1.12509073312364,ManufacturerGuid=N'2c42d9ff-762f-43a4-8960-316286bedf3a',IsMasterData='1' WHERE Guid = '5383e49e-6a19-4b01-a3fd-f3d19afbbf9c'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3efb8ae1-e59c-48c9-89b2-d2e91e8ddcbe')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3efb8ae1-e59c-48c9-89b2-d2e91e8ddcbe')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '684d58fb-7fbf-4493-9b5c-da367edf451f')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3381A003', 0.0739172513912412, N'3efb8ae1-e59c-48c9-89b2-d2e91e8ddcbe', '1', '684d58fb-7fbf-4493-9b5c-da367edf451f')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3381A003',Price=0.0739172513912412,ManufacturerGuid=N'3efb8ae1-e59c-48c9-89b2-d2e91e8ddcbe',IsMasterData='1' WHERE Guid = '684d58fb-7fbf-4493-9b5c-da367edf451f'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'37199a2d-44e1-4c42-9378-b69d9cac64a6')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'37199a2d-44e1-4c42-9378-b69d9cac64a6')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '161732a5-01e5-4c4a-a673-6b9c111d6bb1')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'2411162', 1.0585531091217, N'37199a2d-44e1-4c42-9378-b69d9cac64a6', '1', '161732a5-01e5-4c4a-a673-6b9c111d6bb1')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'2411162',Price=1.0585531091217,ManufacturerGuid=N'37199a2d-44e1-4c42-9378-b69d9cac64a6',IsMasterData='1' WHERE Guid = '161732a5-01e5-4c4a-a673-6b9c111d6bb1'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'541951ff-4382-44e0-8fd3-d2354ca4d1c5')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'541951ff-4382-44e0-8fd3-d2354ca4d1c5')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'e5024239-e54a-4325-ad0d-8f32addf11fd')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'3312K018', 0.360512944592306, N'541951ff-4382-44e0-8fd3-d2354ca4d1c5', '1', 'e5024239-e54a-4325-ad0d-8f32addf11fd')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'3312K018',Price=0.360512944592306,ManufacturerGuid=N'541951ff-4382-44e0-8fd3-d2354ca4d1c5',IsMasterData='1' WHERE Guid = 'e5024239-e54a-4325-ad0d-8f32addf11fd'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'75b0d5b7-6cc5-4643-8b2a-77e8f5fcc4a7')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'75b0d5b7-6cc5-4643-8b2a-77e8f5fcc4a7')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6dca9785-547a-4b71-8b5f-9d19d3e09529')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER', N'K134AF01', 0.0362932494556013, N'75b0d5b7-6cc5-4643-8b2a-77e8f5fcc4a7', '1', '6dca9785-547a-4b71-8b5f-9d19d3e09529')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER',PartNumber=N'K134AF01',Price=0.0362932494556013,ManufacturerGuid=N'75b0d5b7-6cc5-4643-8b2a-77e8f5fcc4a7',IsMasterData='1' WHERE Guid = '6dca9785-547a-4b71-8b5f-9d19d3e09529'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'6b6d6c01-2526-4d4b-8f6b-6c5a398eb8dc')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'6b6d6c01-2526-4d4b-8f6b-6c5a398eb8dc')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '37c5607b-7912-417a-8d32-d1146829b68a')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'1984767', 5.86740866198887, N'6b6d6c01-2526-4d4b-8f6b-6c5a398eb8dc', '1', '37c5607b-7912-417a-8d32-d1146829b68a')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'1984767',Price=5.86740866198887,ManufacturerGuid=N'6b6d6c01-2526-4d4b-8f6b-6c5a398eb8dc',IsMasterData='1' WHERE Guid = '37c5607b-7912-417a-8d32-d1146829b68a'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'85d6755b-a3e5-4965-bbe4-cf1356ae2b87')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'85d6755b-a3e5-4965-bbe4-cf1356ae2b87')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '14cccbda-0251-4344-b098-4024bc3ff2e9')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'5P8245', 0.0145172997822405, N'85d6755b-a3e5-4965-bbe4-cf1356ae2b87', '1', '14cccbda-0251-4344-b098-4024bc3ff2e9')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'5P8245',Price=0.0145172997822405,ManufacturerGuid=N'85d6755b-a3e5-4965-bbe4-cf1356ae2b87',IsMasterData='1' WHERE Guid = '14cccbda-0251-4344-b098-4024bc3ff2e9'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'4dc3eb8d-bcea-4181-9ae9-a1f4dc3d11c3')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'4dc3eb8d-bcea-4181-9ae9-a1f4dc3d11c3')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '6ed91840-9150-4fd8-8ff4-f5422c8c4d93')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'5P8247', 0.168884587466731, N'4dc3eb8d-bcea-4181-9ae9-a1f4dc3d11c3', '1', '6ed91840-9150-4fd8-8ff4-f5422c8c4d93')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'5P8247',Price=0.168884587466731,ManufacturerGuid=N'4dc3eb8d-bcea-4181-9ae9-a1f4dc3d11c3',IsMasterData='1' WHERE Guid = '6ed91840-9150-4fd8-8ff4-f5422c8c4d93'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'ed40b1c1-4a00-4d02-9477-8e1422a52105')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'ed40b1c1-4a00-4d02-9477-8e1422a52105')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'fcd672bf-aaf5-4ac2-a2c5-de9b0f6e0f30')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'9N0869', 0.048390999274135, N'ed40b1c1-4a00-4d02-9477-8e1422a52105', '1', 'fcd672bf-aaf5-4ac2-a2c5-de9b0f6e0f30')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'9N0869',Price=0.048390999274135,ManufacturerGuid=N'ed40b1c1-4a00-4d02-9477-8e1422a52105',IsMasterData='1' WHERE Guid = 'fcd672bf-aaf5-4ac2-a2c5-de9b0f6e0f30'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'3b999475-87d9-4a0d-af1c-99ca3dbb61d9')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'3b999475-87d9-4a0d-af1c-99ca3dbb61d9')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'dd1dc0c3-5340-44a0-b32a-f68ea21d2f57')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'0966166', 0.0110089523348657, N'3b999475-87d9-4a0d-af1c-99ca3dbb61d9', '1', 'dd1dc0c3-5340-44a0-b32a-f68ea21d2f57')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'0966166',Price=0.0110089523348657,ManufacturerGuid=N'3b999475-87d9-4a0d-af1c-99ca3dbb61d9',IsMasterData='1' WHERE Guid = 'dd1dc0c3-5340-44a0-b32a-f68ea21d2f57'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'cb5382a8-cf54-406b-8069-ca0899e98c56')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'cb5382a8-cf54-406b-8069-ca0899e98c56')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'b9a7dee9-1f54-41b4-a842-b2e45f5c4389')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-HARD', N'2993424', 5.86740866198887, N'cb5382a8-cf54-406b-8069-ca0899e98c56', '1', 'b9a7dee9-1f54-41b4-a842-b2e45f5c4389')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-HARD',PartNumber=N'2993424',Price=5.86740866198887,ManufacturerGuid=N'cb5382a8-cf54-406b-8069-ca0899e98c56',IsMasterData='1' WHERE Guid = 'b9a7dee9-1f54-41b4-a842-b2e45f5c4389'
end

if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = N'e2424982-c615-48ed-90d8-7b37d09eec72')
begin
   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, N'e2424982-c615-48ed-90d8-7b37d09eec72')
end

if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = 'bfda7cf0-43b5-4692-8444-c2db4a8c9e03')
begin
   INSERT dbo.Commodities (Name, PartNumber, Price, ManufacturerGuid, IsMasterData, Guid) VALUES (N'WASHER-LOCK', N'3100674', 1.0404064843939, N'e2424982-c615-48ed-90d8-7b37d09eec72', '1', 'bfda7cf0-43b5-4692-8444-c2db4a8c9e03')
end
else
begin
   UPDATE dbo.Commodities SET Name=N'WASHER-LOCK',PartNumber=N'3100674',Price=1.0404064843939,ManufacturerGuid=N'e2424982-c615-48ed-90d8-7b37d09eec72',IsMasterData='1' WHERE Guid = 'bfda7cf0-43b5-4692-8444-c2db4a8c9e03'
end

