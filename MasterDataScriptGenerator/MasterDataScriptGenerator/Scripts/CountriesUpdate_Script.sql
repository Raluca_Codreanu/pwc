IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '5643170a-4fcb-41fd-9bd5-d06f45a1f359')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 1.05470309418403, 6.000615303, 6.606268243, 9.12097245, 9.787690908, 5.30498496, 0.723068446, 0.010632922, 0.086774645, 8.377997173, 89.55555555, 0.1224, 1, '5643170a-4fcb-41fd-9bd5-d06f45a1f359')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Argentina', N'd8b5f7d7-d9c1-4a11-a698-73b69d189125', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'5643170a-4fcb-41fd-9bd5-d06f45a1f359', 1, '57794541-3e71-4cc1-8c2a-e6130bc09418')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=1.05470309418403,UnskilledLaborCost=6.000615303,SkilledLaborCost=6.606268243,ForemanCost=9.12097245,TechnicianCost=9.787690908,EngineerCost=5.30498496,EnergyCost=0.723068446,AirCost=0.010632922,WaterCost=0.086774645,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=89.55555555,InterestRate=0.1224,IsScrambled=1 WHERE [Guid] = '5643170a-4fcb-41fd-9bd5-d06f45a1f359'
   UPDATE dbo.Countries SET Name=N'Argentina',CurrencyGuid=N'd8b5f7d7-d9c1-4a11-a698-73b69d189125',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '57794541-3e71-4cc1-8c2a-e6130bc09418'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '23bd232e-c49e-4e84-9616-51d2ad6fc501')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 16.55470471, 26.97680818, 90.16103226, 19.5140212, 15.67737542, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 2.479861131, 0.015, 1, '23bd232e-c49e-4e84-9616-51d2ad6fc501')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Burgenland', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'23bd232e-c49e-4e84-9616-51d2ad6fc501', 1, '6aa8938d-b249-4db9-abf7-ac754f79b68e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=16.55470471,SkilledLaborCost=26.97680818,ForemanCost=90.16103226,TechnicianCost=19.5140212,EngineerCost=15.67737542,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=2.479861131,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = '23bd232e-c49e-4e84-9616-51d2ad6fc501'
   UPDATE dbo.Countries SET Name=N'Austria Burgenland',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6aa8938d-b249-4db9-abf7-ac754f79b68e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ce3f6f05-3030-43c3-9cb7-8f2656e914cf')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 90.25252337, 46.06707015, 56.54949983, 56.25256605, 4.363632889, 0.666008556, 0.013018596, 0.229922563, 1.84, 39.0, 0.015, 1, 'ce3f6f05-3030-43c3-9cb7-8f2656e914cf')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Country Average', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'ce3f6f05-3030-43c3-9cb7-8f2656e914cf', 1, '189111b2-5344-4c0b-aec8-d881ff86b68e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=90.25252337,SkilledLaborCost=46.06707015,ForemanCost=56.54949983,TechnicianCost=56.25256605,EngineerCost=4.363632889,EnergyCost=0.666008556,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=1.84,OfficeAreaRentalCost=39.0,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = 'ce3f6f05-3030-43c3-9cb7-8f2656e914cf'
   UPDATE dbo.Countries SET Name=N'Austria Country Average',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '189111b2-5344-4c0b-aec8-d881ff86b68e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'fe878b5c-4669-447c-b040-3df003e6841e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 26.17413852, 36.72560257, 59.01115369, 39.07848022, 75.10461492, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 2.479861131, 0.015, 1, 'fe878b5c-4669-447c-b040-3df003e6841e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Kärnten', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'fe878b5c-4669-447c-b040-3df003e6841e', 1, '3059b41e-a4ff-44cc-bcc3-9bcc15f18ea3')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=26.17413852,SkilledLaborCost=36.72560257,ForemanCost=59.01115369,TechnicianCost=39.07848022,EngineerCost=75.10461492,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=2.479861131,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = 'fe878b5c-4669-447c-b040-3df003e6841e'
   UPDATE dbo.Countries SET Name=N'Austria Kärnten',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '3059b41e-a4ff-44cc-bcc3-9bcc15f18ea3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'cda474ad-1fd7-42cb-a008-20976958fe67')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 26.68888264, 76.06470329, 59.2810149, 39.98293429, 75.33386017, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 2.479861131, 0.015, 1, 'cda474ad-1fd7-42cb-a008-20976958fe67')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Niederösterreich', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'cda474ad-1fd7-42cb-a008-20976958fe67', 1, '1172ec99-d2cc-44da-9e81-dd0cff75aad7')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=26.68888264,SkilledLaborCost=76.06470329,ForemanCost=59.2810149,TechnicianCost=39.98293429,EngineerCost=75.33386017,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=2.479861131,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = 'cda474ad-1fd7-42cb-a008-20976958fe67'
   UPDATE dbo.Countries SET Name=N'Austria Niederösterreich',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1172ec99-d2cc-44da-9e81-dd0cff75aad7'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f3c00a95-3123-4347-8b43-91cab44b9689')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 46.15089178, 90.21549206, 19.50010161, 79.48267024, 61.38257427, 0.025367414, 0.013018596, 0.229922563, 8.21259693, 7.255347565, 0.015, 1, 'f3c00a95-3123-4347-8b43-91cab44b9689')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Salzburg', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f3c00a95-3123-4347-8b43-91cab44b9689', 1, '1414fde1-db27-40ca-ae25-00222558c5fe')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=46.15089178,SkilledLaborCost=90.21549206,ForemanCost=19.50010161,TechnicianCost=79.48267024,EngineerCost=61.38257427,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=8.21259693,OfficeAreaRentalCost=7.255347565,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = 'f3c00a95-3123-4347-8b43-91cab44b9689'
   UPDATE dbo.Countries SET Name=N'Austria Salzburg',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1414fde1-db27-40ca-ae25-00222558c5fe'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'aea7b928-f08e-481a-9cc2-823653793f55')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 26.91398493, 76.46172496, 59.46584185, 39.69586761, 10.16576946, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 2.479861131, 0.015, 1, 'aea7b928-f08e-481a-9cc2-823653793f55')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Styria', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'aea7b928-f08e-481a-9cc2-823653793f55', 1, '521ebede-330b-4c02-a893-22be5620c358')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=26.91398493,SkilledLaborCost=76.46172496,ForemanCost=59.46584185,TechnicianCost=39.69586761,EngineerCost=10.16576946,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=2.479861131,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = 'aea7b928-f08e-481a-9cc2-823653793f55'
   UPDATE dbo.Countries SET Name=N'Austria Styria',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '521ebede-330b-4c02-a893-22be5620c358'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '80973969-e153-4d4c-bd07-0f87e0732e04')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 46.93447099, 76.17109912, 19.05207809, 79.90980209, 61.44450636, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 66.0, 0.015, 1, '80973969-e153-4d4c-bd07-0f87e0732e04')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Tyrol', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'80973969-e153-4d4c-bd07-0f87e0732e04', 1, '87521ba6-0db6-4fce-a554-088559abad12')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=46.93447099,SkilledLaborCost=76.17109912,ForemanCost=19.05207809,TechnicianCost=79.90980209,EngineerCost=61.44450636,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=66.0,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = '80973969-e153-4d4c-bd07-0f87e0732e04'
   UPDATE dbo.Countries SET Name=N'Austria Tyrol',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '87521ba6-0db6-4fce-a554-088559abad12'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3f7e6f60-305a-46ee-af4f-9b9e7d3159de')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.44, 0.33, 0.825, 26.97680818, 36.89897897, 59.021466, 39.0403381, 75.86019402, 0.025367414, 0.013018596, 0.229922563, 5.543295132, 2.479861131, 0.015, 1, '3f7e6f60-305a-46ee-af4f-9b9e7d3159de')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Austria Vorarlberg', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3f7e6f60-305a-46ee-af4f-9b9e7d3159de', 1, 'c8f14b46-aa45-415a-8285-f5e4f1895fce')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.44,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.825,UnskilledLaborCost=26.97680818,SkilledLaborCost=36.89897897,ForemanCost=59.021466,TechnicianCost=39.0403381,EngineerCost=75.86019402,EnergyCost=0.025367414,AirCost=0.013018596,WaterCost=0.229922563,ProductionAreaRentalCost=5.543295132,OfficeAreaRentalCost=2.479861131,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = '3f7e6f60-305a-46ee-af4f-9b9e7d3159de'
   UPDATE dbo.Countries SET Name=N'Austria Vorarlberg',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c8f14b46-aa45-415a-8285-f5e4f1895fce'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a8e03eca-9135-466b-bb22-64069ac005db')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.53, 0.23, 0.17, 0.8443, 60.71270296, 56.58064277, 16.73482895, 36.33083495, 59.93797216, 0.955008056, 0.013031422, 0.107521163, 5.555550439, 55.84, 0.0025, 1, 'a8e03eca-9135-466b-bb22-64069ac005db')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Belgium', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a8e03eca-9135-466b-bb22-64069ac005db', 1, '46b3afcb-0de3-41f4-ae66-95f1472fb342')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.53,ShiftCharge2ShiftModel=0.23,ShiftCharge3ShiftModel=0.17,LaborAvailability=0.8443,UnskilledLaborCost=60.71270296,SkilledLaborCost=56.58064277,ForemanCost=16.73482895,TechnicianCost=36.33083495,EngineerCost=59.93797216,EnergyCost=0.955008056,AirCost=0.013031422,WaterCost=0.107521163,ProductionAreaRentalCost=5.555550439,OfficeAreaRentalCost=55.84,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a8e03eca-9135-466b-bb22-64069ac005db'
   UPDATE dbo.Countries SET Name=N'Belgium',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '46b3afcb-0de3-41f4-ae66-95f1472fb342'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6a3f7a53-7832-44bb-b0bc-71dd9167c77d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.53, 0.23, 0.17, 0.8443, 26.25855234, 76.928206, 59.94695595, 39.99277411, 10.04546978, 0.099882314, 0.013031422, 0.107521163, 1.0, 7.0, 0.0025, 1, '6a3f7a53-7832-44bb-b0bc-71dd9167c77d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Belgium Brüssel', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6a3f7a53-7832-44bb-b0bc-71dd9167c77d', 1, '6a041ca7-1b32-40ee-b033-6be5f8d48b86')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.53,ShiftCharge2ShiftModel=0.23,ShiftCharge3ShiftModel=0.17,LaborAvailability=0.8443,UnskilledLaborCost=26.25855234,SkilledLaborCost=76.928206,ForemanCost=59.94695595,TechnicianCost=39.99277411,EngineerCost=10.04546978,EnergyCost=0.099882314,AirCost=0.013031422,WaterCost=0.107521163,ProductionAreaRentalCost=1.0,OfficeAreaRentalCost=7.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '6a3f7a53-7832-44bb-b0bc-71dd9167c77d'
   UPDATE dbo.Countries SET Name=N'Belgium Brüssel',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6a041ca7-1b32-40ee-b033-6be5f8d48b86'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8848a08d-cbe7-4ee3-826e-73622b8b7ddf')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.53, 0.23, 0.17, 0.8443, 46.25373582, 90.7874826, 19.80851531, 79.70305023, 91.01263727, 0.099882314, 0.013031422, 0.107521163, 5.0, 4.480203676, 0.0025, 1, '8848a08d-cbe7-4ee3-826e-73622b8b7ddf')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Belgium Flandern', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'8848a08d-cbe7-4ee3-826e-73622b8b7ddf', 1, '5c768138-130c-4a41-80cd-320639325f56')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.53,ShiftCharge2ShiftModel=0.23,ShiftCharge3ShiftModel=0.17,LaborAvailability=0.8443,UnskilledLaborCost=46.25373582,SkilledLaborCost=90.7874826,ForemanCost=19.80851531,TechnicianCost=79.70305023,EngineerCost=91.01263727,EnergyCost=0.099882314,AirCost=0.013031422,WaterCost=0.107521163,ProductionAreaRentalCost=5.0,OfficeAreaRentalCost=4.480203676,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '8848a08d-cbe7-4ee3-826e-73622b8b7ddf'
   UPDATE dbo.Countries SET Name=N'Belgium Flandern',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5c768138-130c-4a41-80cd-320639325f56'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9ba23a9d-3bad-4307-a7fb-ccda82754cf8')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.53, 0.23, 0.17, 0.8443, 26.70909965, 36.65718658, 99.74826462, 49.68577798, 35.976707, 0.099882314, 0.013031422, 0.107521163, 1.0, 4.480203676, 0.0025, 1, '9ba23a9d-3bad-4307-a7fb-ccda82754cf8')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Belgium Wallonien', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9ba23a9d-3bad-4307-a7fb-ccda82754cf8', 1, 'bd12fcdf-2bbe-4d75-bc72-7e087ca3376a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.53,ShiftCharge2ShiftModel=0.23,ShiftCharge3ShiftModel=0.17,LaborAvailability=0.8443,UnskilledLaborCost=26.70909965,SkilledLaborCost=36.65718658,ForemanCost=99.74826462,TechnicianCost=49.68577798,EngineerCost=35.976707,EnergyCost=0.099882314,AirCost=0.013031422,WaterCost=0.107521163,ProductionAreaRentalCost=1.0,OfficeAreaRentalCost=4.480203676,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '9ba23a9d-3bad-4307-a7fb-ccda82754cf8'
   UPDATE dbo.Countries SET Name=N'Belgium Wallonien',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bd12fcdf-2bbe-4d75-bc72-7e087ca3376a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '7427b3d7-13fe-44d1-b8bd-7ab7959d943d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 5.389890674, 1.941559805, 8.234737989, 2.985659935, 60.58624966, 0.050198604, 0.012069457, 0.440615184, 8.565211411, 29.80416856, 0.2, 1, '7427b3d7-13fe-44d1-b8bd-7ab7959d943d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Belo Horizonte', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'7427b3d7-13fe-44d1-b8bd-7ab7959d943d', 1, '93a886bb-2092-4a2a-afe6-8b6fba029b92')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=5.389890674,SkilledLaborCost=1.941559805,ForemanCost=8.234737989,TechnicianCost=2.985659935,EngineerCost=60.58624966,EnergyCost=0.050198604,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=8.565211411,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '7427b3d7-13fe-44d1-b8bd-7ab7959d943d'
   UPDATE dbo.Countries SET Name=N'Brazil Belo Horizonte',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '93a886bb-2092-4a2a-afe6-8b6fba029b92'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6e5fc1d8-fb65-42e6-b622-e189abd8d8fa')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 5.277491927, 1.684533208, 8.122469898, 2.283609814, 60.33174494, 0.078910932, 0.012069457, 0.440615184, 2.541807369, 666.4222267, 0.2, 1, '6e5fc1d8-fb65-42e6-b622-e189abd8d8fa')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Country Average', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6e5fc1d8-fb65-42e6-b622-e189abd8d8fa', 1, 'dcf9bf7b-2fd5-4980-b3d2-21342ce6eede')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=5.277491927,SkilledLaborCost=1.684533208,ForemanCost=8.122469898,TechnicianCost=2.283609814,EngineerCost=60.33174494,EnergyCost=0.078910932,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=2.541807369,OfficeAreaRentalCost=666.4222267,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '6e5fc1d8-fb65-42e6-b622-e189abd8d8fa'
   UPDATE dbo.Countries SET Name=N'Brazil Country Average',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'dcf9bf7b-2fd5-4980-b3d2-21342ce6eede'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '49971ff5-e109-47ca-a623-c5a7ead9fc14')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 5.17470997, 1.872588221, 8.170803141, 2.165187164, 60.32752238, 0.075156182, 0.012069457, 0.440615184, 8.025200635, 29.80416856, 0.2, 1, '49971ff5-e109-47ca-a623-c5a7ead9fc14')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Porto Alegre', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'49971ff5-e109-47ca-a623-c5a7ead9fc14', 1, '2532f39a-ceff-440d-987a-1e2f289a3347')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=5.17470997,SkilledLaborCost=1.872588221,ForemanCost=8.170803141,TechnicianCost=2.165187164,EngineerCost=60.32752238,EnergyCost=0.075156182,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=8.025200635,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '49971ff5-e109-47ca-a623-c5a7ead9fc14'
   UPDATE dbo.Countries SET Name=N'Brazil Porto Alegre',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '2532f39a-ceff-440d-987a-1e2f289a3347'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '66edbb1b-8fcc-4afe-a792-151f5cc8a7a3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 5.057465587, 5.585952371, 1.8375895, 8.384193959, 3.198677405, 0.049311382, 0.012069457, 0.440615184, 8.986580126, 29.80416856, 0.2, 1, '66edbb1b-8fcc-4afe-a792-151f5cc8a7a3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Recife', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'66edbb1b-8fcc-4afe-a792-151f5cc8a7a3', 1, 'c50b2bd1-775d-4ca5-ae42-be205a89cc54')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=5.057465587,SkilledLaborCost=5.585952371,ForemanCost=1.8375895,TechnicianCost=8.384193959,EngineerCost=3.198677405,EnergyCost=0.049311382,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=8.986580126,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '66edbb1b-8fcc-4afe-a792-151f5cc8a7a3'
   UPDATE dbo.Countries SET Name=N'Brazil Recife',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c50b2bd1-775d-4ca5-ae42-be205a89cc54'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c2b2ad30-3b98-4f6d-bd6d-f701ac14877f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 1.872588221, 8.092872267, 2.606214009, 4.468080938, 96.04228536, 0.050198604, 0.012069457, 0.440615184, 4.24137477, 29.80416856, 0.2, 1, 'c2b2ad30-3b98-4f6d-bd6d-f701ac14877f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Rio de Janeiro', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'c2b2ad30-3b98-4f6d-bd6d-f701ac14877f', 1, '51ce7473-a941-42be-88e8-d64060fa8942')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=1.872588221,SkilledLaborCost=8.092872267,ForemanCost=2.606214009,TechnicianCost=4.468080938,EngineerCost=96.04228536,EnergyCost=0.050198604,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=4.24137477,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = 'c2b2ad30-3b98-4f6d-bd6d-f701ac14877f'
   UPDATE dbo.Countries SET Name=N'Brazil Rio de Janeiro',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '51ce7473-a941-42be-88e8-d64060fa8942'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '95dcb2a8-dd41-4eee-ac8f-ea9bcd4aca2d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 5.747145738, 1.034200857, 8.000457297, 2.460689016, 7.133827312, 0.049311382, 0.012069457, 0.440615184, 8.911157341, 29.80416856, 0.2, 1, '95dcb2a8-dd41-4eee-ac8f-ea9bcd4aca2d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Salvador', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'95dcb2a8-dd41-4eee-ac8f-ea9bcd4aca2d', 1, '6345b59c-ddbe-48cb-a616-8be62b0cc0ec')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=5.747145738,SkilledLaborCost=1.034200857,ForemanCost=8.000457297,TechnicianCost=2.460689016,EngineerCost=7.133827312,EnergyCost=0.049311382,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=8.911157341,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '95dcb2a8-dd41-4eee-ac8f-ea9bcd4aca2d'
   UPDATE dbo.Countries SET Name=N'Brazil Salvador',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6345b59c-ddbe-48cb-a616-8be62b0cc0ec'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3d84b04b-ac1e-40c5-991e-a4cfdc972294')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.38, 0.4, 0.84375, 8.5940529, 8.405560737, 4.662482739, 3.574798923, 16.04178315, 0.050198604, 0.012069457, 0.440615184, 2.728188231, 29.80416856, 0.2, 1, '3d84b04b-ac1e-40c5-991e-a4cfdc972294')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Brazil Sao Paolo', N'210b727c-ea8c-4591-8b07-ac4e7823c8bc', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3d84b04b-ac1e-40c5-991e-a4cfdc972294', 1, 'cf7ee040-5de2-42c3-afda-072d1e165074')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.38,ShiftCharge3ShiftModel=0.4,LaborAvailability=0.84375,UnskilledLaborCost=8.5940529,SkilledLaborCost=8.405560737,ForemanCost=4.662482739,TechnicianCost=3.574798923,EngineerCost=16.04178315,EnergyCost=0.050198604,AirCost=0.012069457,WaterCost=0.440615184,ProductionAreaRentalCost=2.728188231,OfficeAreaRentalCost=29.80416856,InterestRate=0.2,IsScrambled=1 WHERE [Guid] = '3d84b04b-ac1e-40c5-991e-a4cfdc972294'
   UPDATE dbo.Countries SET Name=N'Brazil Sao Paolo',CurrencyGuid=N'210b727c-ea8c-4591-8b07-ac4e7823c8bc',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cf7ee040-5de2-42c3-afda-072d1e165074'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '28059b37-157d-45c4-9675-988e94ee1f2f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.29, 0.5, 0.91, 0.775, 66.71633328, 86.32295199, 49.46840859, 50.78284704, 51.77080595, 0.036246748, 0.006413102, 6.93323501, 8.211874883, 45.05555553, 0.01, 1, '28059b37-157d-45c4-9675-988e94ee1f2f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Canada', N'23cf775f-59bd-4a2c-9c8f-cd1ac41b4a91', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'28059b37-157d-45c4-9675-988e94ee1f2f', 1, '49d408ef-1e09-45a0-b811-a1b8069bc6b0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.29,ShiftCharge2ShiftModel=0.5,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.775,UnskilledLaborCost=66.71633328,SkilledLaborCost=86.32295199,ForemanCost=49.46840859,TechnicianCost=50.78284704,EngineerCost=51.77080595,EnergyCost=0.036246748,AirCost=0.006413102,WaterCost=6.93323501,ProductionAreaRentalCost=8.211874883,OfficeAreaRentalCost=45.05555553,InterestRate=0.01,IsScrambled=1 WHERE [Guid] = '28059b37-157d-45c4-9675-988e94ee1f2f'
   UPDATE dbo.Countries SET Name=N'Canada',CurrencyGuid=N'23cf775f-59bd-4a2c-9c8f-cd1ac41b4a91',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '49d408ef-1e09-45a0-b811-a1b8069bc6b0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a5551d19-0efe-45df-8902-25975cff7b2d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.9556, 3.77532474, 96.53771514, 26.80756725, 69.10893883, 25.23686369, 0.055174942, 0.010632922, 0.182488122, 1.062878498, 76.49285862, 0.045, 1, 'a5551d19-0efe-45df-8902-25975cff7b2d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Chile', N'502c1724-6bea-4465-85b3-0af88f673bd0', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a5551d19-0efe-45df-8902-25975cff7b2d', 1, '1188467a-fbbd-456a-a7b9-5fcfc68a008e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.9556,UnskilledLaborCost=3.77532474,SkilledLaborCost=96.53771514,ForemanCost=26.80756725,TechnicianCost=69.10893883,EngineerCost=25.23686369,EnergyCost=0.055174942,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=76.49285862,InterestRate=0.045,IsScrambled=1 WHERE [Guid] = 'a5551d19-0efe-45df-8902-25975cff7b2d'
   UPDATE dbo.Countries SET Name=N'Chile',CurrencyGuid=N'502c1724-6bea-4465-85b3-0af88f673bd0',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1188467a-fbbd-456a-a7b9-5fcfc68a008e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '2d609887-f74f-493d-92d8-6ab4a0d734f7')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.9556, 3.77532474, 96.53771514, 26.80756725, 69.10893883, 25.23686369, 0.055174942, 0.010632922, 0.182488122, 1.062878498, 89.42222222, 0.045, 1, '2d609887-f74f-493d-92d8-6ab4a0d734f7')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Chile Country', N'502c1724-6bea-4465-85b3-0af88f673bd0', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'2d609887-f74f-493d-92d8-6ab4a0d734f7', 1, 'ad602a6a-984e-4782-94dd-bd180d2c69a8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.9556,UnskilledLaborCost=3.77532474,SkilledLaborCost=96.53771514,ForemanCost=26.80756725,TechnicianCost=69.10893883,EngineerCost=25.23686369,EnergyCost=0.055174942,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=89.42222222,InterestRate=0.045,IsScrambled=1 WHERE [Guid] = '2d609887-f74f-493d-92d8-6ab4a0d734f7'
   UPDATE dbo.Countries SET Name=N'Chile Country',CurrencyGuid=N'502c1724-6bea-4465-85b3-0af88f673bd0',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'ad602a6a-984e-4782-94dd-bd180d2c69a8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd8478dbe-379e-421b-9f65-fb67f6577413')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 5.080490929, 1.993513291, 2.791954374, 3.934553665, 86.6714692, 0.045327572, 0.010260963, 0.575951976, 9.771621417, 73.8, 0.0025, 1, 'd8478dbe-379e-421b-9f65-fb67f6577413')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Beijing', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'd8478dbe-379e-421b-9f65-fb67f6577413', 1, '58df5589-0981-44dd-b34f-f186e26874a6')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=5.080490929,SkilledLaborCost=1.993513291,ForemanCost=2.791954374,TechnicianCost=3.934553665,EngineerCost=86.6714692,EnergyCost=0.045327572,AirCost=0.010260963,WaterCost=0.575951976,ProductionAreaRentalCost=9.771621417,OfficeAreaRentalCost=73.8,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'd8478dbe-379e-421b-9f65-fb67f6577413'
   UPDATE dbo.Countries SET Name=N'China Beijing',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '58df5589-0981-44dd-b34f-f186e26874a6'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'bca7c44f-672c-43e9-bbff-caee213de8a6')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.610886106, 6.769281222, 9.386274108, 5.041074786, 8.296334962, 0.028077952, 0.010260963, 0.046649096, 6.714054541, 90.47338162, 0.0025, 1, 'bca7c44f-672c-43e9-bbff-caee213de8a6')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Chengdu', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'bca7c44f-672c-43e9-bbff-caee213de8a6', 1, '73ad8a8e-3173-46c9-a3a6-913d42220d1e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.610886106,SkilledLaborCost=6.769281222,ForemanCost=9.386274108,TechnicianCost=5.041074786,EngineerCost=8.296334962,EnergyCost=0.028077952,AirCost=0.010260963,WaterCost=0.046649096,ProductionAreaRentalCost=6.714054541,OfficeAreaRentalCost=90.47338162,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'bca7c44f-672c-43e9-bbff-caee213de8a6'
   UPDATE dbo.Countries SET Name=N'China Chengdu',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '73ad8a8e-3173-46c9-a3a6-913d42220d1e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'aad95d06-20b8-4a57-8a03-eb2ba5e808ff')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.160293156, 6.858507232, 9.340703278, 5.818399069, 8.783471767, 0.04472922, 0.010260963, 0.046649096, 9.663390748, 36.45152691, 0.06, 1, 'aad95d06-20b8-4a57-8a03-eb2ba5e808ff')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Chongqing', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'aad95d06-20b8-4a57-8a03-eb2ba5e808ff', 1, 'd6429e47-b5f2-441d-932c-5e90d7e8a4dc')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.160293156,SkilledLaborCost=6.858507232,ForemanCost=9.340703278,TechnicianCost=5.818399069,EngineerCost=8.783471767,EnergyCost=0.04472922,AirCost=0.010260963,WaterCost=0.046649096,ProductionAreaRentalCost=9.663390748,OfficeAreaRentalCost=36.45152691,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = 'aad95d06-20b8-4a57-8a03-eb2ba5e808ff'
   UPDATE dbo.Countries SET Name=N'China Chongqing',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd6429e47-b5f2-441d-932c-5e90d7e8a4dc'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '84e649a4-076b-4155-88a0-dbb26d62c1b3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.228554429, 6.391438703, 9.502435024, 5.40403545, 2.568099365, 0.203798066, 0.005951358, 0.573787645, 1.073582204, 2.25740568, 0.06, 1, '84e649a4-076b-4155-88a0-dbb26d62c1b3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Country', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'84e649a4-076b-4155-88a0-dbb26d62c1b3', 1, 'e6e10f1b-2795-46f4-8119-10cd02064d9b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.228554429,SkilledLaborCost=6.391438703,ForemanCost=9.502435024,TechnicianCost=5.40403545,EngineerCost=2.568099365,EnergyCost=0.203798066,AirCost=0.005951358,WaterCost=0.573787645,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=2.25740568,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '84e649a4-076b-4155-88a0-dbb26d62c1b3'
   UPDATE dbo.Countries SET Name=N'China Country',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e6e10f1b-2795-46f4-8119-10cd02064d9b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9aa30898-0851-44d5-8772-bef66f00189f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.603576035, 9.256327216, 5.333100009, 5.557022867, 4.979238015, 0.010017978, 0.005951358, 0.08563025, 1.073582204, 7.371475067, 0.06, 1, '9aa30898-0851-44d5-8772-bef66f00189f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Dalian', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9aa30898-0851-44d5-8772-bef66f00189f', 1, 'bf849c57-092d-455c-8bc0-061eec10d24d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.603576035,SkilledLaborCost=9.256327216,ForemanCost=5.333100009,TechnicianCost=5.557022867,EngineerCost=4.979238015,EnergyCost=0.010017978,AirCost=0.005951358,WaterCost=0.08563025,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=7.371475067,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '9aa30898-0851-44d5-8772-bef66f00189f'
   UPDATE dbo.Countries SET Name=N'China Dalian',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bf849c57-092d-455c-8bc0-061eec10d24d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd2f575fa-c1d3-4c11-b955-b66b521143de')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.901389013, 9.282810772, 1.054423277, 1.832472327, 7.797105579, 0.044003274, 0.005951358, 0.023968019, 1.073582204, 55.93745274, 0.06, 1, 'd2f575fa-c1d3-4c11-b955-b66b521143de')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Guangzhou', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'd2f575fa-c1d3-4c11-b955-b66b521143de', 1, 'e09debcf-b7d1-4e6f-aac4-2a1436e0bbd8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.901389013,SkilledLaborCost=9.282810772,ForemanCost=1.054423277,TechnicianCost=1.832472327,EngineerCost=7.797105579,EnergyCost=0.044003274,AirCost=0.005951358,WaterCost=0.023968019,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=55.93745274,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = 'd2f575fa-c1d3-4c11-b955-b66b521143de'
   UPDATE dbo.Countries SET Name=N'China Guangzhou',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e09debcf-b7d1-4e6f-aac4-2a1436e0bbd8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'cf0e26e1-b5af-4b12-b9b7-5f14e1a001ca')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.228554429, 6.881267344, 9.947971764, 5.40403545, 2.857537869, 0.038684351, 0.005951358, 0.023968019, 1.073582204, 2.25740568, 0.06, 1, 'cf0e26e1-b5af-4b12-b9b7-5f14e1a001ca')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Hangzhou', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'cf0e26e1-b5af-4b12-b9b7-5f14e1a001ca', 1, '38c20cce-80e2-42eb-ae25-41d749b86d3b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.228554429,SkilledLaborCost=6.881267344,ForemanCost=9.947971764,TechnicianCost=5.40403545,EngineerCost=2.857537869,EnergyCost=0.038684351,AirCost=0.005951358,WaterCost=0.023968019,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=2.25740568,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = 'cf0e26e1-b5af-4b12-b9b7-5f14e1a001ca'
   UPDATE dbo.Countries SET Name=N'China Hangzhou',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '38c20cce-80e2-42eb-ae25-41d749b86d3b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1aa0c30c-75d5-40c6-b783-f1bc648f0b86')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 0.965028493, 6.721015619, 6.902410923, 9.447124412, 5.954955446, 0.058336832, 0.005951358, 0.042130631, 1.073582204, 2.25740568, 0.06, 1, '1aa0c30c-75d5-40c6-b783-f1bc648f0b86')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Qingdao', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1aa0c30c-75d5-40c6-b783-f1bc648f0b86', 1, 'c454837d-99b9-4a6c-9b07-496819e5bac4')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=0.965028493,SkilledLaborCost=6.721015619,ForemanCost=6.902410923,TechnicianCost=9.447124412,EngineerCost=5.954955446,EnergyCost=0.058336832,AirCost=0.005951358,WaterCost=0.042130631,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=2.25740568,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '1aa0c30c-75d5-40c6-b783-f1bc648f0b86'
   UPDATE dbo.Countries SET Name=N'China Qingdao',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c454837d-99b9-4a6c-9b07-496819e5bac4'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '533f0a42-ed27-4d37-b99e-b47bb99c37e9')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 1.091697438, 2.066180198, 7.537927659, 66.48590219, 69.42792606, 0.288011206, 0.005951358, 0.046649096, 1.783223825, 32.10389698, 0.06, 1, '533f0a42-ed27-4d37-b99e-b47bb99c37e9')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Shanghai City', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'533f0a42-ed27-4d37-b99e-b47bb99c37e9', 1, 'ed57f516-3249-47fd-9a7f-83f32c3958be')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=1.091697438,SkilledLaborCost=2.066180198,ForemanCost=7.537927659,TechnicianCost=66.48590219,EngineerCost=69.42792606,EnergyCost=0.288011206,AirCost=0.005951358,WaterCost=0.046649096,ProductionAreaRentalCost=1.783223825,OfficeAreaRentalCost=32.10389698,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '533f0a42-ed27-4d37-b99e-b47bb99c37e9'
   UPDATE dbo.Countries SET Name=N'China Shanghai City',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'ed57f516-3249-47fd-9a7f-83f32c3958be'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3b264738-92ef-4a48-9dfe-10aa0a73834a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.912769128, 9.679611685, 5.328280328, 1.28180815, 3.917403496, 0.328855292, 0.005951358, 0.573787645, 5.29059776, 4.57422463, 0.06, 1, '3b264738-92ef-4a48-9dfe-10aa0a73834a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Shanghai Suburbs', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3b264738-92ef-4a48-9dfe-10aa0a73834a', 1, '57ff82eb-497c-4158-8f2b-13acbfb9f968')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.912769128,SkilledLaborCost=9.679611685,ForemanCost=5.328280328,TechnicianCost=1.28180815,EngineerCost=3.917403496,EnergyCost=0.328855292,AirCost=0.005951358,WaterCost=0.573787645,ProductionAreaRentalCost=5.29059776,OfficeAreaRentalCost=4.57422463,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '3b264738-92ef-4a48-9dfe-10aa0a73834a'
   UPDATE dbo.Countries SET Name=N'China Shanghai Suburbs',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '57ff82eb-497c-4158-8f2b-13acbfb9f968'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4572c0e9-9786-4102-b45c-dcc3a40c68e8')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 0.156430577, 6.2422731, 9.1608159, 5.295031446, 1.544463936, 0.044003274, 0.005951358, 0.023968019, 1.073582204, 25.54785911, 0.06, 1, '4572c0e9-9786-4102-b45c-dcc3a40c68e8')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Shenzhen', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4572c0e9-9786-4102-b45c-dcc3a40c68e8', 1, '549c1eb6-bbac-4b09-a99b-321110a0a3dc')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=0.156430577,SkilledLaborCost=6.2422731,ForemanCost=9.1608159,TechnicianCost=5.295031446,EngineerCost=1.544463936,EnergyCost=0.044003274,AirCost=0.005951358,WaterCost=0.023968019,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=25.54785911,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '4572c0e9-9786-4102-b45c-dcc3a40c68e8'
   UPDATE dbo.Countries SET Name=N'China Shenzhen',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '549c1eb6-bbac-4b09-a99b-321110a0a3dc'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '87a1faca-3211-4df1-8644-91f15599c6b3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.60895598, 9.22218065, 5.842945811, 1.700416969, 4.41433907, 0.032070314, 0.005951358, 0.055918019, 1.073582204, 2.25740568, 0.06, 1, '87a1faca-3211-4df1-8644-91f15599c6b3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Suzhou', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'87a1faca-3211-4df1-8644-91f15599c6b3', 1, '60145dfb-ffae-4818-8157-f6f47b5358d9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.60895598,SkilledLaborCost=9.22218065,ForemanCost=5.842945811,TechnicianCost=1.700416969,EngineerCost=4.41433907,EnergyCost=0.032070314,AirCost=0.005951358,WaterCost=0.055918019,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=2.25740568,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '87a1faca-3211-4df1-8644-91f15599c6b3'
   UPDATE dbo.Countries SET Name=N'China Suzhou',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '60145dfb-ffae-4818-8157-f6f47b5358d9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'cac6cf00-2da5-44c7-a454-c29a8280e914')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.633036339, 6.587826547, 9.784023127, 5.133508338, 2.683871272, 0.065368732, 0.005951358, 0.575951976, 1.073582204, 56.35041931, 0.06, 1, 'cac6cf00-2da5-44c7-a454-c29a8280e914')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Tianjin', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'cac6cf00-2da5-44c7-a454-c29a8280e914', 1, 'e5df6299-2d81-4d68-9152-17d2bed4212e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.633036339,SkilledLaborCost=6.587826547,ForemanCost=9.784023127,TechnicianCost=5.133508338,EngineerCost=2.683871272,EnergyCost=0.065368732,AirCost=0.005951358,WaterCost=0.575951976,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=56.35041931,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = 'cac6cf00-2da5-44c7-a454-c29a8280e914'
   UPDATE dbo.Countries SET Name=N'China Tianjin',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e5df6299-2d81-4d68-9152-17d2bed4212e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '15eaf7e5-77a6-446f-89af-13749bd87866')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.2, 0.14, 0.85, 6.160293156, 6.219834852, 9.8948171, 5.098211843, 8.54585132, 0.050491652, 0.005951358, 0.042130631, 1.073582204, 2.25740568, 0.06, 1, '15eaf7e5-77a6-446f-89af-13749bd87866')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'China Wuhan', N'549050bb-9a5b-46e1-83e6-59a5eadc1ade', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'15eaf7e5-77a6-446f-89af-13749bd87866', 1, '055bd59f-b8dd-45a8-a004-063516f904ab')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.85,UnskilledLaborCost=6.160293156,SkilledLaborCost=6.219834852,ForemanCost=9.8948171,TechnicianCost=5.098211843,EngineerCost=8.54585132,EnergyCost=0.050491652,AirCost=0.005951358,WaterCost=0.042130631,ProductionAreaRentalCost=1.073582204,OfficeAreaRentalCost=2.25740568,InterestRate=0.06,IsScrambled=1 WHERE [Guid] = '15eaf7e5-77a6-446f-89af-13749bd87866'
   UPDATE dbo.Countries SET Name=N'China Wuhan',CurrencyGuid=N'549050bb-9a5b-46e1-83e6-59a5eadc1ade',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '055bd59f-b8dd-45a8-a004-063516f904ab'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '24678dfa-4013-479b-b6c3-d57d16d15bad')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.9854, 5.0768246, 5.566258611, 1.162157319, 8.183492476, 3.996497839, 0.01623062, 0.010632922, 0.182488122, 9.378229911, 50.94563424, 0.0325, 1, '24678dfa-4013-479b-b6c3-d57d16d15bad')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Colombia', N'301843b3-b371-4cec-a2ee-747b2a226108', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'24678dfa-4013-479b-b6c3-d57d16d15bad', 1, '918db1f4-893e-4b43-902d-6fac9b5f4613')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.9854,UnskilledLaborCost=5.0768246,SkilledLaborCost=5.566258611,ForemanCost=1.162157319,TechnicianCost=8.183492476,EngineerCost=3.996497839,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=50.94563424,InterestRate=0.0325,IsScrambled=1 WHERE [Guid] = '24678dfa-4013-479b-b6c3-d57d16d15bad'
   UPDATE dbo.Countries SET Name=N'Colombia',CurrencyGuid=N'301843b3-b371-4cec-a2ee-747b2a226108',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '918db1f4-893e-4b43-902d-6fac9b5f4613'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '5dadfdfe-3873-4910-8d83-3b3d72f15a0d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.9854, 5.336226601, 5.246026217, 1.130690103, 8.362654523, 7.439077149, 0.01623062, 0.010632922, 0.0, 9.378229911, 11.42222226, 0.0325, 1, '5dadfdfe-3873-4910-8d83-3b3d72f15a0d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Colombia Country', N'301843b3-b371-4cec-a2ee-747b2a226108', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'5dadfdfe-3873-4910-8d83-3b3d72f15a0d', 1, '13704724-d657-4a3d-9f52-772220af306d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.9854,UnskilledLaborCost=5.336226601,SkilledLaborCost=5.246026217,ForemanCost=1.130690103,TechnicianCost=8.362654523,EngineerCost=7.439077149,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.0,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=11.42222226,InterestRate=0.0325,IsScrambled=1 WHERE [Guid] = '5dadfdfe-3873-4910-8d83-3b3d72f15a0d'
   UPDATE dbo.Countries SET Name=N'Colombia Country',CurrencyGuid=N'301843b3-b371-4cec-a2ee-747b2a226108',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '13704724-d657-4a3d-9f52-772220af306d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e77d1225-6ef6-47d6-9307-13bee2805347')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.85, 0.41, 0.9925, 8.193264949, 2.075645546, 4.934348815, 3.730652827, 16.85061319, 0.287778366, 0.011671845, 0.305071794, 1.8, 36.8, 0.0625, 1, 'e77d1225-6ef6-47d6-9307-13bee2805347')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Croatia', N'236e873e-294d-40a7-81a2-7b4811c137b2', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e77d1225-6ef6-47d6-9307-13bee2805347', 1, 'd57cffeb-6158-431a-b0f0-02fe8e5b6b13')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.85,ShiftCharge3ShiftModel=0.41,LaborAvailability=0.9925,UnskilledLaborCost=8.193264949,SkilledLaborCost=2.075645546,ForemanCost=4.934348815,TechnicianCost=3.730652827,EngineerCost=16.85061319,EnergyCost=0.287778366,AirCost=0.011671845,WaterCost=0.305071794,ProductionAreaRentalCost=1.8,OfficeAreaRentalCost=36.8,InterestRate=0.0625,IsScrambled=1 WHERE [Guid] = 'e77d1225-6ef6-47d6-9307-13bee2805347'
   UPDATE dbo.Countries SET Name=N'Croatia',CurrencyGuid=N'236e873e-294d-40a7-81a2-7b4811c137b2',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd57cffeb-6158-431a-b0f0-02fe8e5b6b13'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1dd0c01f-3e32-43fa-b58e-3b377a240176')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.14, 0.74, 0.33, 0.75, 56.4678745, 86.71056989, 36.70406158, 99.64241692, 65.90607517, 0.804777519, 0.021009321, 0.76, 1.257192734, 2.411626744, 0.0025, 1, '1dd0c01f-3e32-43fa-b58e-3b377a240176')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Cyprus', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1dd0c01f-3e32-43fa-b58e-3b377a240176', 1, 'cabb5cb5-20c7-4b9a-9064-8f93fac77384')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.14,ShiftCharge2ShiftModel=0.74,ShiftCharge3ShiftModel=0.33,LaborAvailability=0.75,UnskilledLaborCost=56.4678745,SkilledLaborCost=86.71056989,ForemanCost=36.70406158,TechnicianCost=99.64241692,EngineerCost=65.90607517,EnergyCost=0.804777519,AirCost=0.021009321,WaterCost=0.76,ProductionAreaRentalCost=1.257192734,OfficeAreaRentalCost=2.411626744,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '1dd0c01f-3e32-43fa-b58e-3b377a240176'
   UPDATE dbo.Countries SET Name=N'Cyprus',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cabb5cb5-20c7-4b9a-9064-8f93fac77384'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '71af41ea-1a12-4759-9b5e-502ab35e86f1')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.38, 0.92, 0.14, 0.8167, 9.833850534, 1.137908156, 1.097246726, 8.115833841, 2.623689927, 0.603772596, 0.013044249, 0.918829863, 1.89, 39.89, 0.0005, 1, '71af41ea-1a12-4759-9b5e-502ab35e86f1')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Czech Republic Country Average', N'7b171e1b-5b9c-4800-9200-4c2286c0e7ed', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'71af41ea-1a12-4759-9b5e-502ab35e86f1', 1, '07edf053-b74f-42cf-a4df-c5bc9339e677')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.38,ShiftCharge2ShiftModel=0.92,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.8167,UnskilledLaborCost=9.833850534,SkilledLaborCost=1.137908156,ForemanCost=1.097246726,TechnicianCost=8.115833841,EngineerCost=2.623689927,EnergyCost=0.603772596,AirCost=0.013044249,WaterCost=0.918829863,ProductionAreaRentalCost=1.89,OfficeAreaRentalCost=39.89,InterestRate=0.0005,IsScrambled=1 WHERE [Guid] = '71af41ea-1a12-4759-9b5e-502ab35e86f1'
   UPDATE dbo.Countries SET Name=N'Czech Republic Country Average',CurrencyGuid=N'7b171e1b-5b9c-4800-9200-4c2286c0e7ed',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '07edf053-b74f-42cf-a4df-c5bc9339e677'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '39318e29-8e87-499a-81ab-d5af04d43d3b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.38, 0.92, 0.14, 0.8167, 2.99409058, 4.831890461, 7.05599179, 60.37058177, 86.76430731, 0.070975714, 0.013044249, 0.918829863, 1.5, 90.8, 0.0005, 1, '39318e29-8e87-499a-81ab-d5af04d43d3b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Czech Republic Prag', N'7b171e1b-5b9c-4800-9200-4c2286c0e7ed', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'39318e29-8e87-499a-81ab-d5af04d43d3b', 1, '96a7bc93-1bc6-4e82-9d00-80c169eb509b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.38,ShiftCharge2ShiftModel=0.92,ShiftCharge3ShiftModel=0.14,LaborAvailability=0.8167,UnskilledLaborCost=2.99409058,SkilledLaborCost=4.831890461,ForemanCost=7.05599179,TechnicianCost=60.37058177,EngineerCost=86.76430731,EnergyCost=0.070975714,AirCost=0.013044249,WaterCost=0.918829863,ProductionAreaRentalCost=1.5,OfficeAreaRentalCost=90.8,InterestRate=0.0005,IsScrambled=1 WHERE [Guid] = '39318e29-8e87-499a-81ab-d5af04d43d3b'
   UPDATE dbo.Countries SET Name=N'Czech Republic Prag',CurrencyGuid=N'7b171e1b-5b9c-4800-9200-4c2286c0e7ed',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '96a7bc93-1bc6-4e82-9d00-80c169eb509b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '35ecef20-adc1-4721-a33d-309208634ee8')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.59, 0.5, 0.11, 0.935, 69.64497991, 19.41707095, 79.18845164, 25.0102757, 80.29105247, 0.234773719, 0.013210989, 0.745800453, 8.097821649, 49.8, 0.002, 1, '35ecef20-adc1-4721-a33d-309208634ee8')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Denmark', N'aba4596b-5e69-4c88-b071-0fadcfd2e981', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'35ecef20-adc1-4721-a33d-309208634ee8', 1, 'cf552638-b8a7-4ebd-b03a-159c8cd0e9b9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.59,ShiftCharge2ShiftModel=0.5,ShiftCharge3ShiftModel=0.11,LaborAvailability=0.935,UnskilledLaborCost=69.64497991,SkilledLaborCost=19.41707095,ForemanCost=79.18845164,TechnicianCost=25.0102757,EngineerCost=80.29105247,EnergyCost=0.234773719,AirCost=0.013210989,WaterCost=0.745800453,ProductionAreaRentalCost=8.097821649,OfficeAreaRentalCost=49.8,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '35ecef20-adc1-4721-a33d-309208634ee8'
   UPDATE dbo.Countries SET Name=N'Denmark',CurrencyGuid=N'aba4596b-5e69-4c88-b071-0fadcfd2e981',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cf552638-b8a7-4ebd-b03a-159c8cd0e9b9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f3dc3807-9bab-456d-8672-36e1509a73e4')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.51, 0.81, 0.18, 0.9925, 8.821703552, 2.858027131, 4.416714913, 7.18711848, 56.43431418, 0.400007266, 0.010042917, 0.747942446, 1.5, 46.89, 0.0025, 1, 'f3dc3807-9bab-456d-8672-36e1509a73e4')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Estonia', N'3fed97b6-bc8a-4f50-867b-a2d04a39aa2d', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f3dc3807-9bab-456d-8672-36e1509a73e4', 1, '26951cb6-16b2-4e76-a0f0-365a9358450a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.51,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.18,LaborAvailability=0.9925,UnskilledLaborCost=8.821703552,SkilledLaborCost=2.858027131,ForemanCost=4.416714913,TechnicianCost=7.18711848,EngineerCost=56.43431418,EnergyCost=0.400007266,AirCost=0.010042917,WaterCost=0.747942446,ProductionAreaRentalCost=1.5,OfficeAreaRentalCost=46.89,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'f3dc3807-9bab-456d-8672-36e1509a73e4'
   UPDATE dbo.Countries SET Name=N'Estonia',CurrencyGuid=N'3fed97b6-bc8a-4f50-867b-a2d04a39aa2d',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '26951cb6-16b2-4e76-a0f0-365a9358450a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c76cb9f9-db1b-4500-a36b-8992b7c82568')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.48, 0.64, 0.7861, 46.31178547, 90.58876461, 19.42672097, 50.18423995, 91.98374513, 0.090500797, 0.009388781, 6.89, 4.8, 25.0, 0.0025, 1, 'c76cb9f9-db1b-4500-a36b-8992b7c82568')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Finland', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'c76cb9f9-db1b-4500-a36b-8992b7c82568', 1, 'e96008a3-c643-47ae-9263-ed954b3b7c57')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.48,ShiftCharge3ShiftModel=0.64,LaborAvailability=0.7861,UnskilledLaborCost=46.31178547,SkilledLaborCost=90.58876461,ForemanCost=19.42672097,TechnicianCost=50.18423995,EngineerCost=91.98374513,EnergyCost=0.090500797,AirCost=0.009388781,WaterCost=6.89,ProductionAreaRentalCost=4.8,OfficeAreaRentalCost=25.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'c76cb9f9-db1b-4500-a36b-8992b7c82568'
   UPDATE dbo.Countries SET Name=N'Finland',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e96008a3-c643-47ae-9263-ed954b3b7c57'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4d1bcee8-206f-4527-b5ef-973064468599')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.67, 0.57, 6.09, 0.8571, 46.76011281, 76.3185107, 19.15194315, 79.1282838, 61.58126803, 0.450008166, 0.008452468, 0.819749398, 1.0, 24.89, 0.0025, 1, '4d1bcee8-206f-4527-b5ef-973064468599')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'France', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4d1bcee8-206f-4527-b5ef-973064468599', 1, '474c1e36-f66e-4b2b-aacd-fe767a78d5c8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.67,ShiftCharge2ShiftModel=0.57,ShiftCharge3ShiftModel=6.09,LaborAvailability=0.8571,UnskilledLaborCost=46.76011281,SkilledLaborCost=76.3185107,ForemanCost=19.15194315,TechnicianCost=79.1282838,EngineerCost=61.58126803,EnergyCost=0.450008166,AirCost=0.008452468,WaterCost=0.819749398,ProductionAreaRentalCost=1.0,OfficeAreaRentalCost=24.89,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '4d1bcee8-206f-4527-b5ef-973064468599'
   UPDATE dbo.Countries SET Name=N'France',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '474c1e36-f66e-4b2b-aacd-fe767a78d5c8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4f1f41f3-0385-4426-960b-1c64fdef6b3f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 46.67256975, 76.78441574, 19.75305109, 79.59857361, 61.87905228, 0.095382213, 0.014737307, 0.364630697, 8.2, 96.0, 0.002, 1, '4f1f41f3-0385-4426-960b-1c64fdef6b3f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany  Brandenburg', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4f1f41f3-0385-4426-960b-1c64fdef6b3f', 1, '0b995bf5-568e-4570-a4e3-1833ffbc06a1')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=46.67256975,SkilledLaborCost=76.78441574,ForemanCost=19.75305109,TechnicianCost=79.59857361,EngineerCost=61.87905228,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.2,OfficeAreaRentalCost=96.0,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '4f1f41f3-0385-4426-960b-1c64fdef6b3f'
   UPDATE dbo.Countries SET Name=N'Germany  Brandenburg',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0b995bf5-568e-4570-a4e3-1833ffbc06a1'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd9c84cce-2afb-4c9a-bcea-35963e8b38d4')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.22, 0.22, 0.84, 0.7688, 36.34090352, 69.48869099, 89.30039973, 65.17103308, 11.23005058, 0.095382213, 0.014737307, 0.364630697, 2.5, 86.39, 0.002, 1, 'd9c84cce-2afb-4c9a-bcea-35963e8b38d4')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Baden-Württemb.', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'd9c84cce-2afb-4c9a-bcea-35963e8b38d4', 1, 'f04ccd28-a9ca-4818-88b0-25a8060b2850')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.22,ShiftCharge2ShiftModel=0.22,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=36.34090352,SkilledLaborCost=69.48869099,ForemanCost=89.30039973,TechnicianCost=65.17103308,EngineerCost=11.23005058,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=2.5,OfficeAreaRentalCost=86.39,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = 'd9c84cce-2afb-4c9a-bcea-35963e8b38d4'
   UPDATE dbo.Countries SET Name=N'Germany Baden-Württemb.',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f04ccd28-a9ca-4818-88b0-25a8060b2850'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '79e258fa-75de-41ec-951d-5056ca973d39')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 26.00543388, 36.24996259, 56.3, 49.83389386, 35.67608099, 0.095382213, 0.014737307, 0.346852034, 8.4, 56.0, 0.002, 1, '79e258fa-75de-41ec-951d-5056ca973d39')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Bayern', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'79e258fa-75de-41ec-951d-5056ca973d39', 1, '6fc28005-bd64-47b7-bf6c-1cee4846b16e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=26.00543388,SkilledLaborCost=36.24996259,ForemanCost=56.3,TechnicianCost=49.83389386,EngineerCost=35.67608099,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.346852034,ProductionAreaRentalCost=8.4,OfficeAreaRentalCost=56.0,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '79e258fa-75de-41ec-951d-5056ca973d39'
   UPDATE dbo.Countries SET Name=N'Germany Bayern',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6fc28005-bd64-47b7-bf6c-1cee4846b16e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3d326a5a-0208-4a9a-a575-5914a2440829')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.4, 0.4, 0.84, 0.7688, 16.17664455, 26.1115445, 76.89139127, 19.11854179, 15.236915, 0.095382213, 0.014737307, 0.364630697, 8.377997173, 7.274243651, 0.002, 1, '3d326a5a-0208-4a9a-a575-5914a2440829')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Berlin', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3d326a5a-0208-4a9a-a575-5914a2440829', 1, '1d561767-42b1-44a2-9896-7e017b9d4ef8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.4,ShiftCharge2ShiftModel=0.4,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=16.17664455,SkilledLaborCost=26.1115445,ForemanCost=76.89139127,TechnicianCost=19.11854179,EngineerCost=15.236915,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=7.274243651,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '3d326a5a-0208-4a9a-a575-5914a2440829'
   UPDATE dbo.Countries SET Name=N'Germany Berlin',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1d561767-42b1-44a2-9896-7e017b9d4ef8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '96e2d4a1-7cb6-414c-bd3f-a607fb8f0d11')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.54, 0.34, 0.53, 0.7688, 36.26972285, 90.81961353, 89.62155031, 50.27135277, 51.67701204, 0.095382213, 0.014737307, 0.364630697, 4.414582325, 66.213734, 0.002, 1, '96e2d4a1-7cb6-414c-bd3f-a607fb8f0d11')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Bremen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'96e2d4a1-7cb6-414c-bd3f-a607fb8f0d11', 1, 'd22f3cb3-6491-4b13-b47d-f31ae03df2c2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.54,ShiftCharge2ShiftModel=0.34,ShiftCharge3ShiftModel=0.53,LaborAvailability=0.7688,UnskilledLaborCost=36.26972285,SkilledLaborCost=90.81961353,ForemanCost=89.62155031,TechnicianCost=50.27135277,EngineerCost=51.67701204,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=4.414582325,OfficeAreaRentalCost=66.213734,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '96e2d4a1-7cb6-414c-bd3f-a607fb8f0d11'
   UPDATE dbo.Countries SET Name=N'Germany Bremen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd22f3cb3-6491-4b13-b47d-f31ae03df2c2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'db829937-8c3e-48f5-ae7b-993c0e1d9f2c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 3.32, 60.3, 96.899, 19.83105861, 15.80562951, 0.157773436, 0.014737307, 0.364630697, 8.377997173, 11.42222261, 0.002, 1, 'db829937-8c3e-48f5-ae7b-993c0e1d9f2c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Country Average', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'db829937-8c3e-48f5-ae7b-993c0e1d9f2c', 1, '3a6d3c9e-da7e-4a9d-a579-7fd5137802d1')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=3.32,SkilledLaborCost=60.3,ForemanCost=96.899,TechnicianCost=19.83105861,EngineerCost=15.80562951,EnergyCost=0.157773436,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=11.42222261,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = 'db829937-8c3e-48f5-ae7b-993c0e1d9f2c'
   UPDATE dbo.Countries SET Name=N'Germany Country Average',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '3a6d3c9e-da7e-4a9d-a579-7fd5137802d1'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4d2a929a-256e-46d2-bedc-02f6c5bcd802')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.62, 0.84, 0.7688, 3.32, 60.8, 60.8, 19.452831, 15.1884921, 0.095382213, 0.014737307, 0.364630697, 2.945334652, 60.1623062, 0.002, 1, '4d2a929a-256e-46d2-bedc-02f6c5bcd802')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Country Average East', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4d2a929a-256e-46d2-bedc-02f6c5bcd802', 1, '19374712-1954-430a-b2e3-4915dbf68bb3')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.62,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=3.32,SkilledLaborCost=60.8,ForemanCost=60.8,TechnicianCost=19.452831,EngineerCost=15.1884921,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=2.945334652,OfficeAreaRentalCost=60.1623062,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '4d2a929a-256e-46d2-bedc-02f6c5bcd802'
   UPDATE dbo.Countries SET Name=N'Germany Country Average East',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '19374712-1954-430a-b2e3-4915dbf68bb3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3506b546-5caf-4852-ae2f-ccefe68a6fe0')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.41, 0.78, 0.7688, 3.32, 66.6, 56.87, 39.41918865, 75.31793397, 0.095382213, 0.014737307, 0.364630697, 1.319976691, 4.047840754, 0.002, 1, '3506b546-5caf-4852-ae2f-ccefe68a6fe0')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Country Average West', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3506b546-5caf-4852-ae2f-ccefe68a6fe0', 1, 'e32ed4c2-a8fa-423b-8e92-426fc3e51320')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.41,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=3.32,SkilledLaborCost=66.6,ForemanCost=56.87,TechnicianCost=39.41918865,EngineerCost=75.31793397,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=1.319976691,OfficeAreaRentalCost=4.047840754,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '3506b546-5caf-4852-ae2f-ccefe68a6fe0'
   UPDATE dbo.Countries SET Name=N'Germany Country Average West',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e32ed4c2-a8fa-423b-8e92-426fc3e51320'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6825e18a-d6a8-4e84-b27b-f3b183f6f2fb')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.32, 0.32, 0.84, 0.7688, 46.52045582, 90.46395836, 19.29143858, 79.5029667, 91.52400756, 0.095382213, 0.014737307, 0.364630697, 3.996768806, 16.0, 0.002, 1, '6825e18a-d6a8-4e84-b27b-f3b183f6f2fb')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Hamburg', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6825e18a-d6a8-4e84-b27b-f3b183f6f2fb', 1, 'e9aa7020-d043-43dd-aa5d-c25c9cb24ad4')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.32,ShiftCharge2ShiftModel=0.32,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=46.52045582,SkilledLaborCost=90.46395836,ForemanCost=19.29143858,TechnicianCost=79.5029667,EngineerCost=91.52400756,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=3.996768806,OfficeAreaRentalCost=16.0,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '6825e18a-d6a8-4e84-b27b-f3b183f6f2fb'
   UPDATE dbo.Countries SET Name=N'Germany Hamburg',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e9aa7020-d043-43dd-aa5d-c25c9cb24ad4'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1d796cce-af71-484e-8ff8-917addbe4ee2')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.11, 0.8, 0.78, 0.7688, 26.69158461, 36.57985304, 99.93756373, 49.16515663, 75.48292805, 0.095382213, 0.014737307, 0.364630697, 2.8, 36.8, 0.002, 1, '1d796cce-af71-484e-8ff8-917addbe4ee2')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Hessen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1d796cce-af71-484e-8ff8-917addbe4ee2', 1, 'f62e8302-f288-4b2b-ab1f-5bea7dda6a96')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.11,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=26.69158461,SkilledLaborCost=36.57985304,ForemanCost=99.93756373,TechnicianCost=49.16515663,EngineerCost=75.48292805,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=2.8,OfficeAreaRentalCost=36.8,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '1d796cce-af71-484e-8ff8-917addbe4ee2'
   UPDATE dbo.Countries SET Name=N'Germany Hessen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f62e8302-f288-4b2b-ab1f-5bea7dda6a96'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3087510b-4ca2-4559-9cbb-1a4cbc3f5683')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.2, 0.82, 0.84, 0.7688, 16.77736499, 26.71005286, 76.11816594, 19.05320156, 55.93887723, 0.095382213, 0.014737307, 0.364630697, 8.377997173, 7.274243651, 0.002, 1, '3087510b-4ca2-4559-9cbb-1a4cbc3f5683')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Mecklenburg Vorpom', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3087510b-4ca2-4559-9cbb-1a4cbc3f5683', 1, '05ef62ae-cca1-4c03-9722-1ac165a91043')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.2,ShiftCharge2ShiftModel=0.82,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=16.77736499,SkilledLaborCost=26.71005286,ForemanCost=76.11816594,TechnicianCost=19.05320156,EngineerCost=55.93887723,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=7.274243651,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '3087510b-4ca2-4559-9cbb-1a4cbc3f5683'
   UPDATE dbo.Countries SET Name=N'Germany Mecklenburg Vorpom',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '05ef62ae-cca1-4c03-9722-1ac165a91043'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'af800342-db63-4b26-a2b0-b6de0b08f657')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.22, 0.22, 0.84, 0.7688, 26.50992783, 76.66538036, 59.11461035, 39.38784158, 10.95404726, 0.095382213, 0.014737307, 0.364630697, 66.97941912, 36.11400728, 0.002, 1, 'af800342-db63-4b26-a2b0-b6de0b08f657')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Niedersachsen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'af800342-db63-4b26-a2b0-b6de0b08f657', 1, 'cd3db060-ce07-4608-ab39-5cd66b79c89f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.22,ShiftCharge2ShiftModel=0.22,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=26.50992783,SkilledLaborCost=76.66538036,ForemanCost=59.11461035,TechnicianCost=39.38784158,EngineerCost=10.95404726,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=66.97941912,OfficeAreaRentalCost=36.11400728,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = 'af800342-db63-4b26-a2b0-b6de0b08f657'
   UPDATE dbo.Countries SET Name=N'Germany Niedersachsen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cd3db060-ce07-4608-ab39-5cd66b79c89f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9065da9e-4acf-4d58-ac54-ee90bfc0f83f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.2, 0.22, 0.84, 0.7688, 26.6032259, 36.97501408, 99.5264278, 49.92767708, 35.9238543, 0.095382213, 0.014737307, 0.364630697, 8.1, 16.3, 0.002, 1, '9065da9e-4acf-4d58-ac54-ee90bfc0f83f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Nordrhein-Westfalen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9065da9e-4acf-4d58-ac54-ee90bfc0f83f', 1, 'c7992e33-39c0-449d-a6f1-a4853f97f3bf')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.2,ShiftCharge2ShiftModel=0.22,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=26.6032259,SkilledLaborCost=36.97501408,ForemanCost=99.5264278,TechnicianCost=49.92767708,EngineerCost=35.9238543,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.1,OfficeAreaRentalCost=16.3,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '9065da9e-4acf-4d58-ac54-ee90bfc0f83f'
   UPDATE dbo.Countries SET Name=N'Germany Nordrhein-Westfalen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c7992e33-39c0-449d-a6f1-a4853f97f3bf'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3e6d6146-dd06-4175-a6fd-f48a0f7ca11a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.22, 0.22, 0.84, 0.7688, 26.01532783, 36.47633275, 99.50058815, 49.67832319, 35.41501661, 0.095382213, 0.014737307, 0.364630697, 2.479861131, 60.89866265, 0.002, 1, '3e6d6146-dd06-4175-a6fd-f48a0f7ca11a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Rheinland Pfalz', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3e6d6146-dd06-4175-a6fd-f48a0f7ca11a', 1, 'a529f5d3-42e8-427d-a967-85c861f45862')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.22,ShiftCharge2ShiftModel=0.22,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=26.01532783,SkilledLaborCost=36.47633275,ForemanCost=99.50058815,TechnicianCost=49.67832319,EngineerCost=35.41501661,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=2.479861131,OfficeAreaRentalCost=60.89866265,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '3e6d6146-dd06-4175-a6fd-f48a0f7ca11a'
   UPDATE dbo.Countries SET Name=N'Germany Rheinland Pfalz',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'a529f5d3-42e8-427d-a967-85c861f45862'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ed670ace-bb6f-45f5-8d2c-4c9daf9eb370')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 46.07556407, 76.45724651, 59.21865934, 39.86062297, 10.3602934, 0.095382213, 0.014737307, 0.364630697, 1.370211296, 3.044987046, 0.002, 1, 'ed670ace-bb6f-45f5-8d2c-4c9daf9eb370')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Saarland', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'ed670ace-bb6f-45f5-8d2c-4c9daf9eb370', 1, 'f5bae276-2966-456b-8a93-374ba907d176')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=46.07556407,SkilledLaborCost=76.45724651,ForemanCost=59.21865934,TechnicianCost=39.86062297,EngineerCost=10.3602934,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=1.370211296,OfficeAreaRentalCost=3.044987046,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = 'ed670ace-bb6f-45f5-8d2c-4c9daf9eb370'
   UPDATE dbo.Countries SET Name=N'Germany Saarland',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f5bae276-2966-456b-8a93-374ba907d176'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1524251d-c2a2-496b-bb95-2e0dcd59e5de')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 16.13203485, 26.2186455, 76.3259187, 19.86090885, 15.79607639, 0.095382213, 0.014737307, 0.364630697, 8.377997173, 7.274243651, 0.002, 1, '1524251d-c2a2-496b-bb95-2e0dcd59e5de')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Sachsen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1524251d-c2a2-496b-bb95-2e0dcd59e5de', 1, 'f7e6f097-a02e-4b62-9f02-791fd94def8b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=16.13203485,SkilledLaborCost=26.2186455,ForemanCost=76.3259187,TechnicianCost=19.86090885,EngineerCost=15.79607639,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=7.274243651,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '1524251d-c2a2-496b-bb95-2e0dcd59e5de'
   UPDATE dbo.Countries SET Name=N'Germany Sachsen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f7e6f097-a02e-4b62-9f02-791fd94def8b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8746dc7f-8ed0-4999-9766-4709a677f440')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.68, 0.68, 0.78, 0.7688, 16.72141439, 26.25223431, 76.1422553, 19.32590116, 15.03842209, 0.095382213, 0.014737307, 0.364630697, 8.377997173, 7.274243651, 0.002, 1, '8746dc7f-8ed0-4999-9766-4709a677f440')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Sachsen Anhalt', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'8746dc7f-8ed0-4999-9766-4709a677f440', 1, 'dc5baaef-5a1d-4d40-8ad8-f156e585a673')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.68,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.78,LaborAvailability=0.7688,UnskilledLaborCost=16.72141439,SkilledLaborCost=26.25223431,ForemanCost=76.1422553,TechnicianCost=19.32590116,EngineerCost=15.03842209,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.377997173,OfficeAreaRentalCost=7.274243651,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '8746dc7f-8ed0-4999-9766-4709a677f440'
   UPDATE dbo.Countries SET Name=N'Germany Sachsen Anhalt',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'dc5baaef-5a1d-4d40-8ad8-f156e585a673'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4d9f38cb-dcfe-443c-8983-3fa2f939279e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.18, 0.62, 0.84, 0.7688, 86.43990142, 46.74682197, 69.89097974, 29.94208848, 45.93499498, 0.095382213, 0.014737307, 0.364630697, 8.21259693, 7.255347565, 0.002, 1, '4d9f38cb-dcfe-443c-8983-3fa2f939279e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Schleswig Holstein', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4d9f38cb-dcfe-443c-8983-3fa2f939279e', 1, 'f5c3b6ec-acc2-40d3-ba15-9b302c01799b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.18,ShiftCharge2ShiftModel=0.62,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=86.43990142,SkilledLaborCost=46.74682197,ForemanCost=69.89097974,TechnicianCost=29.94208848,EngineerCost=45.93499498,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.21259693,OfficeAreaRentalCost=7.255347565,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = '4d9f38cb-dcfe-443c-8983-3fa2f939279e'
   UPDATE dbo.Countries SET Name=N'Germany Schleswig Holstein',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f5c3b6ec-acc2-40d3-ba15-9b302c01799b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f44d23bd-313e-41b1-bcd6-96c745d42618')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.2, 0.82, 0.84, 0.7688, 56.5831182, 86.91847522, 76.08922771, 59.37091726, 95.69705142, 0.095382213, 0.014737307, 0.364630697, 8.65071298, 3.271135913, 0.002, 1, 'f44d23bd-313e-41b1-bcd6-96c745d42618')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Germany Thüringen', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f44d23bd-313e-41b1-bcd6-96c745d42618', 1, 'caccf2f9-5061-4852-83a6-617114491472')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.2,ShiftCharge2ShiftModel=0.82,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.7688,UnskilledLaborCost=56.5831182,SkilledLaborCost=86.91847522,ForemanCost=76.08922771,TechnicianCost=59.37091726,EngineerCost=95.69705142,EnergyCost=0.095382213,AirCost=0.014737307,WaterCost=0.364630697,ProductionAreaRentalCost=8.65071298,OfficeAreaRentalCost=3.271135913,InterestRate=0.002,IsScrambled=1 WHERE [Guid] = 'f44d23bd-313e-41b1-bcd6-96c745d42618'
   UPDATE dbo.Countries SET Name=N'Germany Thüringen',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'caccf2f9-5061-4852-83a6-617114491472'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0d164796-811c-47be-8af7-3050f86f66d4')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.71324627, 36.39885229, 99.24946176, 49.04083459, 35.56637846, 0.16, 0.012454243, 6.160390275, 8.335349931, 86.24095486, 0.005, 1, '0d164796-811c-47be-8af7-3050f86f66d4')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain Country Average', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'0d164796-811c-47be-8af7-3050f86f66d4', 1, '99a2ddc6-4a9b-40c9-bdb2-3d4cec4f5073')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.71324627,SkilledLaborCost=36.39885229,ForemanCost=99.24946176,TechnicianCost=49.04083459,EngineerCost=35.56637846,EnergyCost=0.16,AirCost=0.012454243,WaterCost=6.160390275,ProductionAreaRentalCost=8.335349931,OfficeAreaRentalCost=86.24095486,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '0d164796-811c-47be-8af7-3050f86f66d4'
   UPDATE dbo.Countries SET Name=N'Great Britain Country Average',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '99a2ddc6-4a9b-40c9-bdb2-3d4cec4f5073'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4493d01c-d57e-49af-b03f-001e0f7b791d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 26.00293136, 36.68849299, 99.58989159, 49.0711358, 35.25607856, 0.039271864, 0.012454243, 0.309075344, 1.403831937, 96.46695744, 0.005, 1, '4493d01c-d57e-49af-b03f-001e0f7b791d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain East Midlands', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'4493d01c-d57e-49af-b03f-001e0f7b791d', 1, '4b60a926-a160-487f-bf77-e2b1ce675b87')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=26.00293136,SkilledLaborCost=36.68849299,ForemanCost=99.58989159,TechnicianCost=49.0711358,EngineerCost=35.25607856,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=0.309075344,ProductionAreaRentalCost=1.403831937,OfficeAreaRentalCost=96.46695744,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '4493d01c-d57e-49af-b03f-001e0f7b791d'
   UPDATE dbo.Countries SET Name=N'Great Britain East Midlands',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4b60a926-a160-487f-bf77-e2b1ce675b87'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1311f5b7-fd12-4914-9379-50d6d51e9930')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.91684081, 36.4188225, 59.22137202, 39.27179434, 10.63402008, 0.039271864, 0.012454243, 6.960180445, 3.414592132, 346.0555553, 0.005, 1, '1311f5b7-fd12-4914-9379-50d6d51e9930')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain London', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'1311f5b7-fd12-4914-9379-50d6d51e9930', 1, 'df9ab35f-3138-472f-b8b2-da589e21f958')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.91684081,SkilledLaborCost=36.4188225,ForemanCost=59.22137202,TechnicianCost=39.27179434,EngineerCost=10.63402008,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=6.960180445,ProductionAreaRentalCost=3.414592132,OfficeAreaRentalCost=346.0555553,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '1311f5b7-fd12-4914-9379-50d6d51e9930'
   UPDATE dbo.Countries SET Name=N'Great Britain London',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'df9ab35f-3138-472f-b8b2-da589e21f958'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '040c071d-9500-4e70-a27e-899811b604d3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.07357376, 46.44606156, 90.1098623, 89.51158075, 85.59151673, 0.039271864, 0.012454243, 0.309075344, 8.299041556, 96.17376402, 0.005, 1, '040c071d-9500-4e70-a27e-899811b604d3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain North East England', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'040c071d-9500-4e70-a27e-899811b604d3', 1, 'be18c2b2-d931-42d3-ae1b-ed32c724a586')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.07357376,SkilledLaborCost=46.44606156,ForemanCost=90.1098623,TechnicianCost=89.51158075,EngineerCost=85.59151673,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=0.309075344,ProductionAreaRentalCost=8.299041556,OfficeAreaRentalCost=96.17376402,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '040c071d-9500-4e70-a27e-899811b604d3'
   UPDATE dbo.Countries SET Name=N'Great Britain North East England',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'be18c2b2-d931-42d3-ae1b-ed32c724a586'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'dc9aa76c-8023-45c0-b87e-fa518f398c5a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.79731693, 46.484827, 69.31148573, 29.65400552, 45.4114188, 0.039271864, 0.012454243, 6.701108283, 1.647268032, 56.78990415, 0.005, 1, 'dc9aa76c-8023-45c0-b87e-fa518f398c5a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain North West England', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'dc9aa76c-8023-45c0-b87e-fa518f398c5a', 1, 'dfe45178-50eb-422d-a37d-c260472af96b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.79731693,SkilledLaborCost=46.484827,ForemanCost=69.31148573,TechnicianCost=29.65400552,EngineerCost=45.4114188,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=6.701108283,ProductionAreaRentalCost=1.647268032,OfficeAreaRentalCost=56.78990415,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = 'dc9aa76c-8023-45c0-b87e-fa518f398c5a'
   UPDATE dbo.Countries SET Name=N'Great Britain North West England',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'dfe45178-50eb-422d-a37d-c260472af96b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c535c534-6128-471a-a884-226cf5815558')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 46.78783168, 76.14822797, 19.12035305, 79.31083328, 61.23090564, 0.039271864, 0.012454243, 6.674293811, 2.61165512, 26.93705824, 0.005, 1, 'c535c534-6128-471a-a884-226cf5815558')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain South East England', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'c535c534-6128-471a-a884-226cf5815558', 1, 'e103e4ca-f4e7-4333-80ea-8eef277f9d1f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=46.78783168,SkilledLaborCost=76.14822797,ForemanCost=19.12035305,TechnicianCost=79.31083328,EngineerCost=61.23090564,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=6.674293811,ProductionAreaRentalCost=2.61165512,OfficeAreaRentalCost=26.93705824,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = 'c535c534-6128-471a-a884-226cf5815558'
   UPDATE dbo.Countries SET Name=N'Great Britain South East England',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e103e4ca-f4e7-4333-80ea-8eef277f9d1f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '569b322c-71e4-4a4f-9f7a-0b21eb15108d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.11070692, 46.23481863, 69.37880552, 29.23321925, 45.93200136, 0.039271864, 0.012454243, 9.454761841, 1.735608653, 16.86178753, 0.005, 1, '569b322c-71e4-4a4f-9f7a-0b21eb15108d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain South West England', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'569b322c-71e4-4a4f-9f7a-0b21eb15108d', 1, 'bfc29e06-8983-4b64-9ffa-372c5ec98f9b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.11070692,SkilledLaborCost=46.23481863,ForemanCost=69.37880552,TechnicianCost=29.23321925,EngineerCost=45.93200136,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=9.454761841,ProductionAreaRentalCost=1.735608653,OfficeAreaRentalCost=16.86178753,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '569b322c-71e4-4a4f-9f7a-0b21eb15108d'
   UPDATE dbo.Countries SET Name=N'Great Britain South West England',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bfc29e06-8983-4b64-9ffa-372c5ec98f9b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'afcaa3f8-bfed-4a22-a0b6-f5d5ce9a7c34')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.29566056, 46.65629169, 69.01876366, 89.20560652, 25.0957464, 0.039271864, 0.012454243, 0.309075344, 5.743178897, 16.0354067, 0.005, 1, 'afcaa3f8-bfed-4a22-a0b6-f5d5ce9a7c34')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain Wales', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'afcaa3f8-bfed-4a22-a0b6-f5d5ce9a7c34', 1, 'bcb54f81-898b-4980-ad3b-b9e1a0761cf9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.29566056,SkilledLaborCost=46.65629169,ForemanCost=69.01876366,TechnicianCost=89.20560652,EngineerCost=25.0957464,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=0.309075344,ProductionAreaRentalCost=5.743178897,OfficeAreaRentalCost=16.0354067,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = 'afcaa3f8-bfed-4a22-a0b6-f5d5ce9a7c34'
   UPDATE dbo.Countries SET Name=N'Great Britain Wales',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bcb54f81-898b-4980-ad3b-b9e1a0761cf9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e212fdac-4ea0-4dbd-8429-92e4c9d8125a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.91, 0.81, 0.48, 0.8333, 86.92050678, 36.8547248, 69.97840512, 29.70735995, 45.01549357, 0.039271864, 0.012454243, 6.67979891, 1.403831937, 96.6214458, 0.005, 1, 'e212fdac-4ea0-4dbd-8429-92e4c9d8125a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Great Britain Yorkshire', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'e212fdac-4ea0-4dbd-8429-92e4c9d8125a', 1, '2420feb2-e300-4ba0-8375-3d87a063213b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.91,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8333,UnskilledLaborCost=86.92050678,SkilledLaborCost=36.8547248,ForemanCost=69.97840512,TechnicianCost=29.70735995,EngineerCost=45.01549357,EnergyCost=0.039271864,AirCost=0.012454243,WaterCost=6.67979891,ProductionAreaRentalCost=1.403831937,OfficeAreaRentalCost=96.6214458,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = 'e212fdac-4ea0-4dbd-8429-92e4c9d8125a'
   UPDATE dbo.Countries SET Name=N'Great Britain Yorkshire',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '2420feb2-e300-4ba0-8375-3d87a063213b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a3751439-ca0a-4aca-b1a8-e1067ea277a3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.14, 0.74, 0.33, 1.0047, 8.521682792, 2.274409038, 3.391073276, 7.060320431, 56.84447, 0.16006616, 0.012505548, 6.367049905, 9.89, 79.89, 0.0025, 1, 'a3751439-ca0a-4aca-b1a8-e1067ea277a3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Greece', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a3751439-ca0a-4aca-b1a8-e1067ea277a3', 1, 'ef9dc6c8-6f64-4a41-8eff-aa6a064b2b1c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.14,ShiftCharge2ShiftModel=0.74,ShiftCharge3ShiftModel=0.33,LaborAvailability=1.0047,UnskilledLaborCost=8.521682792,SkilledLaborCost=2.274409038,ForemanCost=3.391073276,TechnicianCost=7.060320431,EngineerCost=56.84447,EnergyCost=0.16006616,AirCost=0.012505548,WaterCost=6.367049905,ProductionAreaRentalCost=9.89,OfficeAreaRentalCost=79.89,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a3751439-ca0a-4aca-b1a8-e1067ea277a3'
   UPDATE dbo.Countries SET Name=N'Greece',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'ef9dc6c8-6f64-4a41-8eff-aa6a064b2b1c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '599ec315-1d07-4940-a278-b9839105b663')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.76, 0.59, 0.85, 1.02608068055556, 9.181010647, 8.812630917, 8.812630917, 3.948596567, 46.62540655, 6.43243006, 0.005476789, 0.694851851, 46.32108637, 896.4222261, 0.061, 1, '599ec315-1d07-4940-a278-b9839105b663')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Hong Kong', N'ef639f1a-578f-4201-9d1d-dc5ae816b861', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'599ec315-1d07-4940-a278-b9839105b663', 1, 'f809c750-e81f-4809-904a-187e929e5bee')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.76,ShiftCharge2ShiftModel=0.59,ShiftCharge3ShiftModel=0.85,LaborAvailability=1.02608068055556,UnskilledLaborCost=9.181010647,SkilledLaborCost=8.812630917,ForemanCost=8.812630917,TechnicianCost=3.948596567,EngineerCost=46.62540655,EnergyCost=6.43243006,AirCost=0.005476789,WaterCost=0.694851851,ProductionAreaRentalCost=46.32108637,OfficeAreaRentalCost=896.4222261,InterestRate=0.061,IsScrambled=1 WHERE [Guid] = '599ec315-1d07-4940-a278-b9839105b663'
   UPDATE dbo.Countries SET Name=N'Hong Kong',CurrencyGuid=N'ef639f1a-578f-4201-9d1d-dc5ae816b861',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f809c750-e81f-4809-904a-187e929e5bee'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '127173f4-7491-4190-b337-2c802000d5e3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.12, 0.94, 0.43, 0.955, 2.188958085, 4.435559, 3.764161334, 60.10842194, 86.64747316, 0.297777096, 0.013146858, 0.512342693, 5.8, 39.55555538, 0.03, 1, '127173f4-7491-4190-b337-2c802000d5e3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Hungary Country Average', N'f5d81604-7186-41a8-b71d-c4aa203920e9', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'127173f4-7491-4190-b337-2c802000d5e3', 1, '5750a0c1-93b6-41af-bfb6-a4b86ae0bfc2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.12,ShiftCharge2ShiftModel=0.94,ShiftCharge3ShiftModel=0.43,LaborAvailability=0.955,UnskilledLaborCost=2.188958085,SkilledLaborCost=4.435559,ForemanCost=3.764161334,TechnicianCost=60.10842194,EngineerCost=86.64747316,EnergyCost=0.297777096,AirCost=0.013146858,WaterCost=0.512342693,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=39.55555538,InterestRate=0.03,IsScrambled=1 WHERE [Guid] = '127173f4-7491-4190-b337-2c802000d5e3'
   UPDATE dbo.Countries SET Name=N'Hungary Country Average',CurrencyGuid=N'f5d81604-7186-41a8-b71d-c4aa203920e9',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5750a0c1-93b6-41af-bfb6-a4b86ae0bfc2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e5cf6782-3980-4206-8d8d-fe21c1d6131a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.766215064, 0.388814517, 6.512856681, 6.108281263, 5.717754449, 0.011376468, 0.004399388, 0.807937679, 6.640597528, 8.382857616, 0.0775, 1, 'e5cf6782-3980-4206-8d8d-fe21c1d6131a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Andhra Pradesh', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e5cf6782-3980-4206-8d8d-fe21c1d6131a', 1, 'a1a50938-c8d3-452c-ba6e-2650335b98cf')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.766215064,SkilledLaborCost=0.388814517,ForemanCost=6.512856681,TechnicianCost=6.108281263,EngineerCost=5.717754449,EnergyCost=0.011376468,AirCost=0.004399388,WaterCost=0.807937679,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=8.382857616,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'e5cf6782-3980-4206-8d8d-fe21c1d6131a'
   UPDATE dbo.Countries SET Name=N'India Andhra Pradesh',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'a1a50938-c8d3-452c-ba6e-2650335b98cf'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '12259a01-537a-4960-b3bc-bbb4ec470210')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.967184722, 0.25573587, 6.10013051, 6.431347164, 5.455804209, 0.087023271, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, '12259a01-537a-4960-b3bc-bbb4ec470210')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Assam', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'12259a01-537a-4960-b3bc-bbb4ec470210', 1, '0e312250-952f-4248-960a-79e608a95649')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.967184722,SkilledLaborCost=0.25573587,ForemanCost=6.10013051,TechnicianCost=6.431347164,EngineerCost=5.455804209,EnergyCost=0.087023271,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '12259a01-537a-4960-b3bc-bbb4ec470210'
   UPDATE dbo.Countries SET Name=N'India Assam',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0e312250-952f-4248-960a-79e608a95649'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c3a8349a-4fbb-4cd9-a586-80e91c13374f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 6.200133499, 6.205102293, 9.989865772, 5.185536305, 2.911902099, 0.022000694, 0.004399388, 0.794006569, 6.640597528, 25.82108365, 0.0775, 1, 'c3a8349a-4fbb-4cd9-a586-80e91c13374f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Bombay Mumbai', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'c3a8349a-4fbb-4cd9-a586-80e91c13374f', 1, '5c9ba2b5-3b9a-4067-b836-ccaabe6ac00d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=6.200133499,SkilledLaborCost=6.205102293,ForemanCost=9.989865772,TechnicianCost=5.185536305,EngineerCost=2.911902099,EnergyCost=0.022000694,AirCost=0.004399388,WaterCost=0.794006569,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=25.82108365,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'c3a8349a-4fbb-4cd9-a586-80e91c13374f'
   UPDATE dbo.Countries SET Name=N'India Bombay Mumbai',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5c9ba2b5-3b9a-4067-b836-ccaabe6ac00d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e5a26379-4a13-4fcc-a2fb-65d1a309702c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.906323302, 0.217064943, 6.391803785, 6.133932172, 5.046465713, 0.013346522, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, 'e5a26379-4a13-4fcc-a2fb-65d1a309702c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Country', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e5a26379-4a13-4fcc-a2fb-65d1a309702c', 1, 'd581fcc4-ca96-4a3d-81f2-d58bd333d044')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.906323302,SkilledLaborCost=0.217064943,ForemanCost=6.391803785,TechnicianCost=6.133932172,EngineerCost=5.046465713,EnergyCost=0.013346522,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'e5a26379-4a13-4fcc-a2fb-65d1a309702c'
   UPDATE dbo.Countries SET Name=N'India Country',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd581fcc4-ca96-4a3d-81f2-d58bd333d044'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9d7eae2f-be16-4832-adc7-a1169278865c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.873249328, 0.256193663, 6.969841329, 6.480365958, 9.131596973, 0.09621644, 0.004399388, 6.603568951, 6.640597528, 660.5555555, 0.0775, 1, '9d7eae2f-be16-4832-adc7-a1169278865c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Delhi', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9d7eae2f-be16-4832-adc7-a1169278865c', 1, '7ec31d8c-dbe0-4b9d-982b-51f2304a6812')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.873249328,SkilledLaborCost=0.256193663,ForemanCost=6.969841329,TechnicianCost=6.480365958,EngineerCost=9.131596973,EnergyCost=0.09621644,AirCost=0.004399388,WaterCost=6.603568951,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=660.5555555,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '9d7eae2f-be16-4832-adc7-a1169278865c'
   UPDATE dbo.Countries SET Name=N'India Delhi',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7ec31d8c-dbe0-4b9d-982b-51f2304a6812'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e3a7573f-8e5f-45d5-8bb5-ffe44265617a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.388814517, 6.160592349, 6.857266777, 9.803246861, 1.388767898, 0.093114989, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, 'e3a7573f-8e5f-45d5-8bb5-ffe44265617a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Gujarat', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e3a7573f-8e5f-45d5-8bb5-ffe44265617a', 1, '8a26d9fb-7ba1-4ed9-9df3-15fc93acfce1')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.388814517,SkilledLaborCost=6.160592349,ForemanCost=6.857266777,TechnicianCost=9.803246861,EngineerCost=1.388767898,EnergyCost=0.093114989,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'e3a7573f-8e5f-45d5-8bb5-ffe44265617a'
   UPDATE dbo.Countries SET Name=N'India Gujarat',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '8a26d9fb-7ba1-4ed9-9df3-15fc93acfce1'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '716dd443-3855-4f5a-8495-b9e1bfabaa40')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.217064943, 6.699169765, 9.004541297, 9.660324881, 1.947606402, 0.063419418, 0.004399388, 6.040023301, 6.640597528, 9.56056181, 0.0775, 1, '716dd443-3855-4f5a-8495-b9e1bfabaa40')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Haryana', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'716dd443-3855-4f5a-8495-b9e1bfabaa40', 1, 'e19ae2f6-150f-4efe-bdb1-a8a1d04dde63')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.217064943,SkilledLaborCost=6.699169765,ForemanCost=9.004541297,TechnicianCost=9.660324881,EngineerCost=1.947606402,EnergyCost=0.063419418,AirCost=0.004399388,WaterCost=6.040023301,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '716dd443-3855-4f5a-8495-b9e1bfabaa40'
   UPDATE dbo.Countries SET Name=N'India Haryana',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e19ae2f6-150f-4efe-bdb1-a8a1d04dde63'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '164904f2-5520-4246-a567-76f6aaafbb4b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.357647693, 6.774979436, 6.973691814, 9.987178006, 5.389776497, 0.062839118, 0.004399388, 0.864077809, 6.640597528, 9.56056181, 0.0775, 1, '164904f2-5520-4246-a567-76f6aaafbb4b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Himachal Pradesh', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'164904f2-5520-4246-a567-76f6aaafbb4b', 1, '51fad214-6a33-4717-8514-4a98d4607310')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.357647693,SkilledLaborCost=6.774979436,ForemanCost=6.973691814,TechnicianCost=9.987178006,EngineerCost=5.389776497,EnergyCost=0.062839118,AirCost=0.004399388,WaterCost=0.864077809,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '164904f2-5520-4246-a567-76f6aaafbb4b'
   UPDATE dbo.Countries SET Name=N'India Himachal Pradesh',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '51fad214-6a33-4717-8514-4a98d4607310'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'b40d0c52-d681-4aae-9902-f15617eb9286')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.388814517, 6.160592349, 6.857266777, 9.660324881, 1.388767898, 0.014577152, 0.004399388, 0.991455064, 6.640597528, 66.8827394, 0.0775, 1, 'b40d0c52-d681-4aae-9902-f15617eb9286')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Karnataka', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'b40d0c52-d681-4aae-9902-f15617eb9286', 1, '97d1d177-13fa-41d2-9a3b-198975c74e24')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.388814517,SkilledLaborCost=6.160592349,ForemanCost=6.857266777,TechnicianCost=9.660324881,EngineerCost=1.388767898,EnergyCost=0.014577152,AirCost=0.004399388,WaterCost=0.991455064,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=66.8827394,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'b40d0c52-d681-4aae-9902-f15617eb9286'
   UPDATE dbo.Countries SET Name=N'India Karnataka',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '97d1d177-13fa-41d2-9a3b-198975c74e24'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '78390816-441c-4481-b44b-7b0125639a8e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.343374221, 0.994612054, 6.094749719, 6.200133499, 9.136501579, 0.09870311, 0.004399388, 0.297547879, 6.640597528, 9.56056181, 0.0775, 1, '78390816-441c-4481-b44b-7b0125639a8e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Kerala', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'78390816-441c-4481-b44b-7b0125639a8e', 1, '0d5baa46-2505-4f45-bd49-118f43077fe8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.343374221,SkilledLaborCost=0.994612054,ForemanCost=6.094749719,TechnicianCost=6.200133499,EngineerCost=9.136501579,EnergyCost=0.09870311,AirCost=0.004399388,WaterCost=0.297547879,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '78390816-441c-4481-b44b-7b0125639a8e'
   UPDATE dbo.Countries SET Name=N'India Kerala',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0d5baa46-2505-4f45-bd49-118f43077fe8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '52e3c1fb-d3a8-4272-81c6-e17912153363')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.408665752, 0.314383933, 6.292482715, 6.930858132, 5.056625212, 0.088522814, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, '52e3c1fb-d3a8-4272-81c6-e17912153363')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Madhya Pradesh', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'52e3c1fb-d3a8-4272-81c6-e17912153363', 1, 'cf144407-07d2-48ed-b9e8-b26ea4ef6729')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.408665752,SkilledLaborCost=0.314383933,ForemanCost=6.292482715,TechnicianCost=6.930858132,EngineerCost=5.056625212,EnergyCost=0.088522814,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '52e3c1fb-d3a8-4272-81c6-e17912153363'
   UPDATE dbo.Countries SET Name=N'India Madhya Pradesh',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cf144407-07d2-48ed-b9e8-b26ea4ef6729'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '43e6a63e-1f62-4aab-8937-c8539b583e06')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 5.246026217, 1.19134081, 8.833757711, 2.930865672, 60.57912218, 0.092585654, 0.004399388, 0.891455064, 6.640597528, 99.57900357, 0.0775, 1, '43e6a63e-1f62-4aab-8937-c8539b583e06')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Maharashtra', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'43e6a63e-1f62-4aab-8937-c8539b583e06', 1, 'f8ed62f0-aa69-4ac4-a4dc-e3ff475f417e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=5.246026217,SkilledLaborCost=1.19134081,ForemanCost=8.833757711,TechnicianCost=2.930865672,EngineerCost=60.57912218,EnergyCost=0.092585654,AirCost=0.004399388,WaterCost=0.891455064,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=99.57900357,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '43e6a63e-1f62-4aab-8937-c8539b583e06'
   UPDATE dbo.Countries SET Name=N'India Maharashtra',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f8ed62f0-aa69-4ac4-a4dc-e3ff475f417e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '020c9889-0e9a-4bd9-9e8a-3c927cb95779')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.570796388, 0.198734014, 6.436389266, 6.590925795, 9.563867821, 0.018569922, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, '020c9889-0e9a-4bd9-9e8a-3c927cb95779')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Punjab', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'020c9889-0e9a-4bd9-9e8a-3c927cb95779', 1, '0e8af3f6-45af-4a35-be37-e2136d5dd395')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.570796388,SkilledLaborCost=0.198734014,ForemanCost=6.436389266,TechnicianCost=6.590925795,EngineerCost=9.563867821,EnergyCost=0.018569922,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '020c9889-0e9a-4bd9-9e8a-3c927cb95779'
   UPDATE dbo.Countries SET Name=N'India Punjab',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0e8af3f6-45af-4a35-be37-e2136d5dd395'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '81eb70fb-b1d6-4f19-8944-bcb631163e0a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.472191348, 0.515861983, 6.602011476, 6.811539621, 9.482384794, 0.072413242, 0.004399388, 0.559149286, 6.640597528, 9.56056181, 0.0775, 1, '81eb70fb-b1d6-4f19-8944-bcb631163e0a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Rajasthan', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'81eb70fb-b1d6-4f19-8944-bcb631163e0a', 1, 'c08a0818-d326-44ff-ab32-fcdb2eb1d4be')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.472191348,SkilledLaborCost=0.515861983,ForemanCost=6.602011476,TechnicianCost=6.811539621,EngineerCost=9.482384794,EnergyCost=0.072413242,AirCost=0.004399388,WaterCost=0.559149286,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '81eb70fb-b1d6-4f19-8944-bcb631163e0a'
   UPDATE dbo.Countries SET Name=N'India Rajasthan',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c08a0818-d326-44ff-ab32-fcdb2eb1d4be'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '2a14dfbe-1d48-49a2-b182-7bed7bd6f91d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.873249328, 0.558398674, 6.876477226, 6.852275001, 9.180521704, 0.07913678, 0.004399388, 0.414255064, 1.358005809, 7.948157132, 0.0775, 1, '2a14dfbe-1d48-49a2-b182-7bed7bd6f91d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Tamil Nadu', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'2a14dfbe-1d48-49a2-b182-7bed7bd6f91d', 1, 'd9c3e563-e453-4033-803a-265972b3fd59')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.873249328,SkilledLaborCost=0.558398674,ForemanCost=6.876477226,TechnicianCost=6.852275001,EngineerCost=9.180521704,EnergyCost=0.07913678,AirCost=0.004399388,WaterCost=0.414255064,ProductionAreaRentalCost=1.358005809,OfficeAreaRentalCost=7.948157132,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = '2a14dfbe-1d48-49a2-b182-7bed7bd6f91d'
   UPDATE dbo.Countries SET Name=N'India Tamil Nadu',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd9c3e563-e453-4033-803a-265972b3fd59'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e07a402b-4d15-4da9-8913-3f605190af3d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.887711597, 6.590925795, 9.031577903, 9.261669231, 1.835139332, 0.081909291, 0.004399388, 0.654627149, 6.640597528, 9.56056181, 0.0775, 1, 'e07a402b-4d15-4da9-8913-3f605190af3d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India Uttaranchal', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e07a402b-4d15-4da9-8913-3f605190af3d', 1, '53f56be6-ceeb-4d37-ab53-928133e1939a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.887711597,SkilledLaborCost=6.590925795,ForemanCost=9.031577903,TechnicianCost=9.261669231,EngineerCost=1.835139332,EnergyCost=0.081909291,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=9.56056181,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'e07a402b-4d15-4da9-8913-3f605190af3d'
   UPDATE dbo.Countries SET Name=N'India Uttaranchal',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '53f56be6-ceeb-4d37-ab53-928133e1939a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'db267122-6dca-46ce-a894-138719b5d55a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.45, 0.45, 0.91, 0.85, 0.235389695, 0.488904335, 6.820480538, 6.655693347, 5.801462962, 0.056727334, 0.004399388, 0.654627149, 6.640597528, 26.34611208, 0.0775, 1, 'db267122-6dca-46ce-a894-138719b5d55a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'India West Bengal', N'16c4140b-213a-469b-bbf9-3610f0ba8797', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'db267122-6dca-46ce-a894-138719b5d55a', 1, '7236b5ce-3688-4414-bed3-00538e0f4d7a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.45,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=0.235389695,SkilledLaborCost=0.488904335,ForemanCost=6.820480538,TechnicianCost=6.655693347,EngineerCost=5.801462962,EnergyCost=0.056727334,AirCost=0.004399388,WaterCost=0.654627149,ProductionAreaRentalCost=6.640597528,OfficeAreaRentalCost=26.34611208,InterestRate=0.0775,IsScrambled=1 WHERE [Guid] = 'db267122-6dca-46ce-a894-138719b5d55a'
   UPDATE dbo.Countries SET Name=N'India West Bengal',CurrencyGuid=N'16c4140b-213a-469b-bbf9-3610f0ba8797',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7236b5ce-3688-4414-bed3-00538e0f4d7a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8f2cc23f-c503-4dcb-afc4-694f6848ee27')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.86, 0.9, 0.65, 0.85, 9.33680386, 5.870780959, 1.570296383, 2.009218759, 66.40812626, 0.013578641, 0.00489961, 0.902899205, 6.860241014, 16.97701891, 0.075, 1, '8f2cc23f-c503-4dcb-afc4-694f6848ee27')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Indonesia', N'0360eb53-ecab-4165-af26-be072da35130', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'8f2cc23f-c503-4dcb-afc4-694f6848ee27', 1, '4415629a-cf3a-4795-a7b3-6b046d6a29ca')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.86,ShiftCharge2ShiftModel=0.9,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.85,UnskilledLaborCost=9.33680386,SkilledLaborCost=5.870780959,ForemanCost=1.570296383,TechnicianCost=2.009218759,EngineerCost=66.40812626,EnergyCost=0.013578641,AirCost=0.00489961,WaterCost=0.902899205,ProductionAreaRentalCost=6.860241014,OfficeAreaRentalCost=16.97701891,InterestRate=0.075,IsScrambled=1 WHERE [Guid] = '8f2cc23f-c503-4dcb-afc4-694f6848ee27'
   UPDATE dbo.Countries SET Name=N'Indonesia',CurrencyGuid=N'0360eb53-ecab-4165-af26-be072da35130',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4415629a-cf3a-4795-a7b3-6b046d6a29ca'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '848dd307-2f69-4f1b-87b0-9706355bcbe6')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.86, 0.9, 0.65, 0.85, 9.828236054, 1.091903716, 2.548720536, 4.187430398, 16.4467936, 0.048931639, 0.00489961, 0.902899205, 6.860241014, 9.668676654, 0.075, 1, '848dd307-2f69-4f1b-87b0-9706355bcbe6')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Indonesia Nord Sumatra', N'0360eb53-ecab-4165-af26-be072da35130', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'848dd307-2f69-4f1b-87b0-9706355bcbe6', 1, '4f7e685c-18e8-40a7-9c6e-b5c579b1cdde')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.86,ShiftCharge2ShiftModel=0.9,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.85,UnskilledLaborCost=9.828236054,SkilledLaborCost=1.091903716,ForemanCost=2.548720536,TechnicianCost=4.187430398,EngineerCost=16.4467936,EnergyCost=0.048931639,AirCost=0.00489961,WaterCost=0.902899205,ProductionAreaRentalCost=6.860241014,OfficeAreaRentalCost=9.668676654,InterestRate=0.075,IsScrambled=1 WHERE [Guid] = '848dd307-2f69-4f1b-87b0-9706355bcbe6'
   UPDATE dbo.Countries SET Name=N'Indonesia Nord Sumatra',CurrencyGuid=N'0360eb53-ecab-4165-af26-be072da35130',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4f7e685c-18e8-40a7-9c6e-b5c579b1cdde'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0db2926a-a63e-482a-9031-b06ed2675533')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.86, 0.9, 0.65, 0.85, 9.791160316, 5.675153646, 1.976680353, 8.201380967, 66.40812626, 0.048931639, 0.00489961, 0.902899205, 6.860241014, 9.668676654, 0.075, 1, '0db2926a-a63e-482a-9031-b06ed2675533')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Indonesia South-Sumatra', N'0360eb53-ecab-4165-af26-be072da35130', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'0db2926a-a63e-482a-9031-b06ed2675533', 1, '99238d3a-f9ab-4bd2-be7c-e19382304383')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.86,ShiftCharge2ShiftModel=0.9,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.85,UnskilledLaborCost=9.791160316,SkilledLaborCost=5.675153646,ForemanCost=1.976680353,TechnicianCost=8.201380967,EngineerCost=66.40812626,EnergyCost=0.048931639,AirCost=0.00489961,WaterCost=0.902899205,ProductionAreaRentalCost=6.860241014,OfficeAreaRentalCost=9.668676654,InterestRate=0.075,IsScrambled=1 WHERE [Guid] = '0db2926a-a63e-482a-9031-b06ed2675533'
   UPDATE dbo.Countries SET Name=N'Indonesia South-Sumatra',CurrencyGuid=N'0360eb53-ecab-4165-af26-be072da35130',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '99238d3a-f9ab-4bd2-be7c-e19382304383'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8d8fc761-8693-40e7-9cba-b16f4fa1c94c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.86, 0.9, 0.65, 0.85, 6.377220292, 9.973003385, 5.194283564, 1.362078211, 3.787871611, 0.048931639, 0.00489961, 0.902899205, 6.860241014, 45.55555555, 0.075, 1, '8d8fc761-8693-40e7-9cba-b16f4fa1c94c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Indonesia West Java', N'0360eb53-ecab-4165-af26-be072da35130', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'8d8fc761-8693-40e7-9cba-b16f4fa1c94c', 1, 'be961907-99dc-495e-a3f2-c5ee23ae54e1')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.86,ShiftCharge2ShiftModel=0.9,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.85,UnskilledLaborCost=6.377220292,SkilledLaborCost=9.973003385,ForemanCost=5.194283564,TechnicianCost=1.362078211,EngineerCost=3.787871611,EnergyCost=0.048931639,AirCost=0.00489961,WaterCost=0.902899205,ProductionAreaRentalCost=6.860241014,OfficeAreaRentalCost=45.55555555,InterestRate=0.075,IsScrambled=1 WHERE [Guid] = '8d8fc761-8693-40e7-9cba-b16f4fa1c94c'
   UPDATE dbo.Countries SET Name=N'Indonesia West Java',CurrencyGuid=N'0360eb53-ecab-4165-af26-be072da35130',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'be961907-99dc-495e-a3f2-c5ee23ae54e1'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e4f60e14-0bad-4c13-8a61-c3f768daebf7')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 96.23715159, 56.70014457, 46.0193359, 90.86629654, 79.35976359, 0.099104552, 0.011030535, 0.591301732, 5.8, 86.0, 0.0025, 1, 'e4f60e14-0bad-4c13-8a61-c3f768daebf7')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland Border', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e4f60e14-0bad-4c13-8a61-c3f768daebf7', 1, 'cf4914e6-cf25-49f2-98af-601eae12cc51')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=96.23715159,SkilledLaborCost=56.70014457,ForemanCost=46.0193359,TechnicianCost=90.86629654,EngineerCost=79.35976359,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=0.591301732,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=86.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'e4f60e14-0bad-4c13-8a61-c3f768daebf7'
   UPDATE dbo.Countries SET Name=N'Ireland Border',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cf4914e6-cf25-49f2-98af-601eae12cc51'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a1970938-09b6-42b3-8652-06e60c2b0c72')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 56.47914251, 86.82642061, 36.84959404, 99.65725874, 95.29856199, 0.473775586, 0.012133588, 6.166729329, 5.0, 25.42222261, 0.0025, 1, 'a1970938-09b6-42b3-8652-06e60c2b0c72')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland Country Average', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a1970938-09b6-42b3-8652-06e60c2b0c72', 1, 'a150174b-661b-4335-abc5-4955a802083b')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=56.47914251,SkilledLaborCost=86.82642061,ForemanCost=36.84959404,TechnicianCost=99.65725874,EngineerCost=95.29856199,EnergyCost=0.473775586,AirCost=0.012133588,WaterCost=6.166729329,ProductionAreaRentalCost=5.0,OfficeAreaRentalCost=25.42222261,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a1970938-09b6-42b3-8652-06e60c2b0c72'
   UPDATE dbo.Countries SET Name=N'Ireland Country Average',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'a150174b-661b-4335-abc5-4955a802083b'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'adc15f8c-07d6-4bb2-b422-64f71d6b297a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 86.3949069, 46.92008155, 69.83380956, 89.91959614, 25.80976949, 0.099104552, 0.011030535, 6.77, 5.8, 86.0, 0.0025, 1, 'adc15f8c-07d6-4bb2-b422-64f71d6b297a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland Dublin', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'adc15f8c-07d6-4bb2-b422-64f71d6b297a', 1, '5b89b658-9ea9-497a-8dec-f42f9a997839')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=86.3949069,SkilledLaborCost=46.92008155,ForemanCost=69.83380956,TechnicianCost=89.91959614,EngineerCost=25.80976949,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=6.77,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=86.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'adc15f8c-07d6-4bb2-b422-64f71d6b297a'
   UPDATE dbo.Countries SET Name=N'Ireland Dublin',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5b89b658-9ea9-497a-8dec-f42f9a997839'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '84446e72-b877-4e74-acb5-4f473dfd27af')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 56.68907268, 86.17648025, 36.12949494, 99.67308823, 95.22877586, 0.099104552, 0.011030535, 0.591301732, 5.8, 86.0, 0.0025, 1, '84446e72-b877-4e74-acb5-4f473dfd27af')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland Mid East', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'84446e72-b877-4e74-acb5-4f473dfd27af', 1, '27394f4e-eb74-458a-8f82-b9c27bbacf62')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=56.68907268,SkilledLaborCost=86.17648025,ForemanCost=36.12949494,TechnicianCost=99.67308823,EngineerCost=95.22877586,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=0.591301732,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=86.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '84446e72-b877-4e74-acb5-4f473dfd27af'
   UPDATE dbo.Countries SET Name=N'Ireland Mid East',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '27394f4e-eb74-458a-8f82-b9c27bbacf62'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e624a7b9-0af9-4d73-b160-d72849bc622a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 56.135289, 86.9556016, 36.19164211, 99.49672181, 65.27476584, 0.099104552, 0.011030535, 0.591301732, 5.8, 86.0, 0.0025, 1, 'e624a7b9-0af9-4d73-b160-d72849bc622a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland MidWest', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e624a7b9-0af9-4d73-b160-d72849bc622a', 1, 'f6690b8e-c087-452a-85e6-08ad3f228532')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=56.135289,SkilledLaborCost=86.9556016,ForemanCost=36.19164211,TechnicianCost=99.49672181,EngineerCost=65.27476584,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=0.591301732,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=86.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'e624a7b9-0af9-4d73-b160-d72849bc622a'
   UPDATE dbo.Countries SET Name=N'Ireland MidWest',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f6690b8e-c087-452a-85e6-08ad3f228532'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'bb064af1-9397-4bc5-93d4-166d03a9e224')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 86.45540066, 46.91699176, 90.64784637, 89.79557078, 25.07157269, 0.099104552, 0.011030535, 6.866729329, 9.8, 7.010682078, 0.0025, 1, 'bb064af1-9397-4bc5-93d4-166d03a9e224')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland Northern Island', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'bb064af1-9397-4bc5-93d4-166d03a9e224', 1, '6b220bc0-3204-44bf-839d-de51417d5776')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=86.45540066,SkilledLaborCost=46.91699176,ForemanCost=90.64784637,TechnicianCost=89.79557078,EngineerCost=25.07157269,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=6.866729329,ProductionAreaRentalCost=9.8,OfficeAreaRentalCost=7.010682078,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'bb064af1-9397-4bc5-93d4-166d03a9e224'
   UPDATE dbo.Countries SET Name=N'Ireland Northern Island',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6b220bc0-3204-44bf-839d-de51417d5776'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f5553b47-94e9-41be-a3ca-e208bcb9791c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 96.92496892, 16.75442301, 46.12753108, 69.12441915, 50.01140777, 0.099104552, 0.011030535, 0.591301732, 5.0, 66.0, 0.0025, 1, 'f5553b47-94e9-41be-a3ca-e208bcb9791c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland SouthEast', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f5553b47-94e9-41be-a3ca-e208bcb9791c', 1, '2342ac65-1ab2-4cfb-b769-a28d8d7c88ff')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=96.92496892,SkilledLaborCost=16.75442301,ForemanCost=46.12753108,TechnicianCost=69.12441915,EngineerCost=50.01140777,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=0.591301732,ProductionAreaRentalCost=5.0,OfficeAreaRentalCost=66.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'f5553b47-94e9-41be-a3ca-e208bcb9791c'
   UPDATE dbo.Countries SET Name=N'Ireland SouthEast',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '2342ac65-1ab2-4cfb-b769-a28d8d7c88ff'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '83ffcc22-e048-4bc5-8b4f-aa368fa8b066')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.7958, 96.31393141, 16.22359336, 46.83091179, 69.06577368, 79.67710722, 0.099104552, 0.011030535, 0.591301732, 5.8, 86.0, 0.0025, 1, '83ffcc22-e048-4bc5-8b4f-aa368fa8b066')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ireland West', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'83ffcc22-e048-4bc5-8b4f-aa368fa8b066', 1, 'fec329ff-9060-4813-9f86-d483e22caf48')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.7958,UnskilledLaborCost=96.31393141,SkilledLaborCost=16.22359336,ForemanCost=46.83091179,TechnicianCost=69.06577368,EngineerCost=79.67710722,EnergyCost=0.099104552,AirCost=0.011030535,WaterCost=0.591301732,ProductionAreaRentalCost=5.8,OfficeAreaRentalCost=86.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '83ffcc22-e048-4bc5-8b4f-aa368fa8b066'
   UPDATE dbo.Countries SET Name=N'Ireland West',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'fec329ff-9060-4813-9f86-d483e22caf48'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e40b11a9-9b2a-4cc8-b046-51d23879804f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.99, 0.29, 0.35, 0.9556, 8.410483154, 4.801337971, 3.966200232, 66.75298893, 26.05158866, 0.058609864, 0.012826203, 6.919095616, 5.039375817, 45.55555555, 0.01, 1, 'e40b11a9-9b2a-4cc8-b046-51d23879804f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Israel', N'31656899-a465-4ec3-9bdb-e6db66d313ce', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e40b11a9-9b2a-4cc8-b046-51d23879804f', 1, 'b449d9f8-c8cb-433d-8b4e-15e09f16e366')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.99,ShiftCharge2ShiftModel=0.29,ShiftCharge3ShiftModel=0.35,LaborAvailability=0.9556,UnskilledLaborCost=8.410483154,SkilledLaborCost=4.801337971,ForemanCost=3.966200232,TechnicianCost=66.75298893,EngineerCost=26.05158866,EnergyCost=0.058609864,AirCost=0.012826203,WaterCost=6.919095616,ProductionAreaRentalCost=5.039375817,OfficeAreaRentalCost=45.55555555,InterestRate=0.01,IsScrambled=1 WHERE [Guid] = 'e40b11a9-9b2a-4cc8-b046-51d23879804f'
   UPDATE dbo.Countries SET Name=N'Israel',CurrencyGuid=N'31656899-a465-4ec3-9bdb-e6db66d313ce',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'b449d9f8-c8cb-433d-8b4e-15e09f16e366'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '106870ee-f4b9-4b4e-a355-f616436b5911')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.94, 0.34, 0.67, 0.98, 56.15876032, 86.6848658, 76.08804616, 59.23859496, 95.24379102, 0.813770876, 0.017071676, 6.03474722, 1.252956009, 31.55555538, 0.0025, 1, '106870ee-f4b9-4b4e-a355-f616436b5911')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Italy Country Average', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'106870ee-f4b9-4b4e-a355-f616436b5911', 1, 'e6c11e10-3792-47ba-a1e5-64458cf75bf8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.94,ShiftCharge2ShiftModel=0.34,ShiftCharge3ShiftModel=0.67,LaborAvailability=0.98,UnskilledLaborCost=56.15876032,SkilledLaborCost=86.6848658,ForemanCost=76.08804616,TechnicianCost=59.23859496,EngineerCost=95.24379102,EnergyCost=0.813770876,AirCost=0.017071676,WaterCost=6.03474722,ProductionAreaRentalCost=1.252956009,OfficeAreaRentalCost=31.55555538,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '106870ee-f4b9-4b4e-a355-f616436b5911'
   UPDATE dbo.Countries SET Name=N'Italy Country Average',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e6c11e10-3792-47ba-a1e5-64458cf75bf8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a87a0b89-22d0-4052-86e9-8e023f8f8d16')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 3.257360377, 56.36897859, 90.10838889, 19.24496112, 95.53495354, 0.024615993, 0.014314043, 0.188494669, 7.383808832, 86.39644591, 0, 1, 'a87a0b89-22d0-4052-86e9-8e023f8f8d16')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Aichi', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a87a0b89-22d0-4052-86e9-8e023f8f8d16', 1, '5052f548-c7c9-4ec2-b238-feeb2c97020d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=3.257360377,SkilledLaborCost=56.36897859,ForemanCost=90.10838889,TechnicianCost=19.24496112,EngineerCost=95.53495354,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.188494669,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'a87a0b89-22d0-4052-86e9-8e023f8f8d16'
   UPDATE dbo.Countries SET Name=N'Japan Aichi',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5052f548-c7c9-4ec2-b238-feeb2c97020d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6b758f09-22d6-4b97-8b2f-2196f267d577')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 8.302898103, 3.589248282, 56.90564836, 26.37436356, 69.98887739, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '6b758f09-22d6-4b97-8b2f-2196f267d577')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Aomori', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6b758f09-22d6-4b97-8b2f-2196f267d577', 1, '06c93a92-f855-45db-9fd5-2eb3c85dc5dd')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=8.302898103,SkilledLaborCost=3.589248282,ForemanCost=56.90564836,TechnicianCost=26.37436356,EngineerCost=69.98887739,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '6b758f09-22d6-4b97-8b2f-2196f267d577'
   UPDATE dbo.Countries SET Name=N'Japan Aomori',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '06c93a92-f855-45db-9fd5-2eb3c85dc5dd'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '63d213e2-3404-47de-a345-a9d842255adf')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 7.088951137, 56.70102275, 90.56544258, 89.0058828, 55.58420086, 0.024615993, 0.014314043, 0.506402673, 7.383808832, 86.39644591, 0, 1, '63d213e2-3404-47de-a345-a9d842255adf')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Chiba', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'63d213e2-3404-47de-a345-a9d842255adf', 1, 'd365bbc3-58f0-4fe8-b255-4ead3a7b48d2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=7.088951137,SkilledLaborCost=56.70102275,ForemanCost=90.56544258,TechnicianCost=89.0058828,EngineerCost=55.58420086,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.506402673,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '63d213e2-3404-47de-a345-a9d842255adf'
   UPDATE dbo.Countries SET Name=N'Japan Chiba',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd365bbc3-58f0-4fe8-b255-4ead3a7b48d2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1a663cd3-29c2-4389-a830-2d238fb01796')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 4.61180593, 66.46481321, 46.2015178, 69.56408091, 39.4750061, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '1a663cd3-29c2-4389-a830-2d238fb01796')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Chugogku', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1a663cd3-29c2-4389-a830-2d238fb01796', 1, '9db11373-af62-41bf-995e-d5ab3c7e2f0a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=4.61180593,SkilledLaborCost=66.46481321,ForemanCost=46.2015178,TechnicianCost=69.56408091,EngineerCost=39.4750061,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '1a663cd3-29c2-4389-a830-2d238fb01796'
   UPDATE dbo.Countries SET Name=N'Japan Chugogku',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9db11373-af62-41bf-995e-d5ab3c7e2f0a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '249a91dd-08fc-42f3-bdf1-1b88de61ed99')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 56.08499852, 86.18444338, 36.69599887, 90.41024433, 79.81364362, 0.830332996, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '249a91dd-08fc-42f3-bdf1-1b88de61ed99')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Country', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'249a91dd-08fc-42f3-bdf1-1b88de61ed99', 1, '282f986f-e297-48e1-81a8-0e65054427cf')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=56.08499852,SkilledLaborCost=86.18444338,ForemanCost=36.69599887,TechnicianCost=90.41024433,EngineerCost=79.81364362,EnergyCost=0.830332996,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '249a91dd-08fc-42f3-bdf1-1b88de61ed99'
   UPDATE dbo.Countries SET Name=N'Japan Country',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '282f986f-e297-48e1-81a8-0e65054427cf'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9cb93f42-2d1f-4f9a-a849-8950c2368260')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 4.420221069, 60.14032148, 26.39865326, 76.81521584, 29.1448046, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '9cb93f42-2d1f-4f9a-a849-8950c2368260')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Hokkaido', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9cb93f42-2d1f-4f9a-a849-8950c2368260', 1, '04383004-543e-4f4d-9997-d4d511a00b8c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=4.420221069,SkilledLaborCost=60.14032148,ForemanCost=26.39865326,TechnicianCost=76.81521584,EngineerCost=29.1448046,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '9cb93f42-2d1f-4f9a-a849-8950c2368260'
   UPDATE dbo.Countries SET Name=N'Japan Hokkaido',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '04383004-543e-4f4d-9997-d4d511a00b8c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '584d05a2-9d77-4984-ab55-cd4f662be285')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 4.881634698, 66.71160496, 26.27192687, 90.9122414, 49.87353861, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '584d05a2-9d77-4984-ab55-cd4f662be285')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Hokuniku', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'584d05a2-9d77-4984-ab55-cd4f662be285', 1, '5de70df4-89eb-48bc-a24f-0763d743e526')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=4.881634698,SkilledLaborCost=66.71160496,ForemanCost=26.27192687,TechnicianCost=90.9122414,EngineerCost=49.87353861,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '584d05a2-9d77-4984-ab55-cd4f662be285'
   UPDATE dbo.Countries SET Name=N'Japan Hokuniku',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5de70df4-89eb-48bc-a24f-0763d743e526'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9598d643-c841-4ac0-8d50-a965cd45a138')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 7.413128002, 16.03365873, 69.67049602, 29.39787029, 15.63825443, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '9598d643-c841-4ac0-8d50-a965cd45a138')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Kanagawa', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9598d643-c841-4ac0-8d50-a965cd45a138', 1, '0766b8a9-00b6-430d-a240-d78fb1dd1b24')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=7.413128002,SkilledLaborCost=16.03365873,ForemanCost=69.67049602,TechnicianCost=29.39787029,EngineerCost=15.63825443,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '9598d643-c841-4ac0-8d50-a965cd45a138'
   UPDATE dbo.Countries SET Name=N'Japan Kanagawa',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0766b8a9-00b6-430d-a240-d78fb1dd1b24'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '100e94bf-5316-44eb-a5ff-7ee779ecd6da')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 7.370827657, 16.44034692, 99.61640129, 49.81807176, 85.78523617, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '100e94bf-5316-44eb-a5ff-7ee779ecd6da')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Kanto', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'100e94bf-5316-44eb-a5ff-7ee779ecd6da', 1, '85fcffd6-e6ef-4125-9cde-35b458526c56')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=7.370827657,SkilledLaborCost=16.44034692,ForemanCost=99.61640129,TechnicianCost=49.81807176,EngineerCost=85.78523617,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '100e94bf-5316-44eb-a5ff-7ee779ecd6da'
   UPDATE dbo.Countries SET Name=N'Japan Kanto',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '85fcffd6-e6ef-4125-9cde-35b458526c56'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e9c24f35-6f36-43ee-8dce-20733cb8830b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 3.540157447, 56.36897859, 90.41492829, 19.80889142, 95.27919364, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, 'e9c24f35-6f36-43ee-8dce-20733cb8830b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Kinki', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e9c24f35-6f36-43ee-8dce-20733cb8830b', 1, 'e31bef1c-3ae8-4c88-bd1e-9718e315616a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=3.540157447,SkilledLaborCost=56.36897859,ForemanCost=90.41492829,TechnicianCost=19.80889142,EngineerCost=95.27919364,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'e9c24f35-6f36-43ee-8dce-20733cb8830b'
   UPDATE dbo.Countries SET Name=N'Japan Kinki',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e31bef1c-3ae8-4c88-bd1e-9718e315616a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'eb742327-1dc4-445d-91ca-f11cbad35491')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 2.57532334, 60.00969299, 86.5343185, 36.42827332, 19.59124174, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, 'eb742327-1dc4-445d-91ca-f11cbad35491')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Kyushu Okinawa', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'eb742327-1dc4-445d-91ca-f11cbad35491', 1, '7d58ab05-f1db-4c01-8759-ce6ba101e362')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=2.57532334,SkilledLaborCost=60.00969299,ForemanCost=86.5343185,TechnicianCost=36.42827332,EngineerCost=19.59124174,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'eb742327-1dc4-445d-91ca-f11cbad35491'
   UPDATE dbo.Countries SET Name=N'Japan Kyushu Okinawa',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7d58ab05-f1db-4c01-8759-ce6ba101e362'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f92665c0-af3c-4bc6-ab05-4c828b98746b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 8.871407173, 3.832362222, 56.65013869, 26.16001336, 69.49991001, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, 'f92665c0-af3c-4bc6-ab05-4c828b98746b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Okinawa', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f92665c0-af3c-4bc6-ab05-4c828b98746b', 1, 'c2cc2cfc-4fb2-4d0c-9c81-8e69ee88dc9d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=8.871407173,SkilledLaborCost=3.832362222,ForemanCost=56.65013869,TechnicianCost=26.16001336,EngineerCost=69.49991001,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'f92665c0-af3c-4bc6-ab05-4c828b98746b'
   UPDATE dbo.Countries SET Name=N'Japan Okinawa',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c2cc2cfc-4fb2-4d0c-9c81-8e69ee88dc9d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0130f801-9b94-4145-99ef-de8cc66edee9')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 7.554740719, 56.23425448, 90.35546323, 89.88880455, 55.28659922, 0.024615993, 0.014314043, 9.968669612, 7.383808832, 86.39644591, 0, 1, '0130f801-9b94-4145-99ef-de8cc66edee9')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Osaka', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'0130f801-9b94-4145-99ef-de8cc66edee9', 1, '6dd58971-26c3-4b3d-9c1a-a947d1facb63')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=7.554740719,SkilledLaborCost=56.23425448,ForemanCost=90.35546323,TechnicianCost=89.88880455,EngineerCost=55.28659922,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=9.968669612,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '0130f801-9b94-4145-99ef-de8cc66edee9'
   UPDATE dbo.Countries SET Name=N'Japan Osaka',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6dd58971-26c3-4b3d-9c1a-a947d1facb63'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ccacced2-7b3a-4a36-862e-7edb58fd23a8')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 4.598942954, 66.72093465, 46.30066505, 69.10765756, 49.77779377, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, 'ccacced2-7b3a-4a36-862e-7edb58fd23a8')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Shikoku', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'ccacced2-7b3a-4a36-862e-7edb58fd23a8', 1, 'c4114926-5bb6-46b9-ab03-3d6a14d32b5e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=4.598942954,SkilledLaborCost=66.72093465,ForemanCost=46.30066505,TechnicianCost=69.10765756,EngineerCost=49.77779377,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'ccacced2-7b3a-4a36-862e-7edb58fd23a8'
   UPDATE dbo.Countries SET Name=N'Japan Shikoku',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c4114926-5bb6-46b9-ab03-3d6a14d32b5e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '4e789fb3-4e62-47d9-a4ff-12285dc80bf1')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 4.590754668, 66.97280456, 26.29984637, 90.20714424, 49.42300281, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, '4e789fb3-4e62-47d9-a4ff-12285dc80bf1')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Tohoku', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'4e789fb3-4e62-47d9-a4ff-12285dc80bf1', 1, 'c7bfff53-b5a3-42f8-8719-3d5e78c7038c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=4.590754668,SkilledLaborCost=66.97280456,ForemanCost=26.29984637,TechnicianCost=90.20714424,EngineerCost=49.42300281,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = '4e789fb3-4e62-47d9-a4ff-12285dc80bf1'
   UPDATE dbo.Countries SET Name=N'Japan Tohoku',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c7bfff53-b5a3-42f8-8719-3d5e78c7038c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e14081c2-370b-4477-8350-3cb0e6f1de27')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 3.993558212, 96.23552572, 76.74465931, 59.54270072, 65.37357921, 0.024615993, 0.014314043, 0.142492173, 7.383808832, 86.39644591, 0, 1, 'e14081c2-370b-4477-8350-3cb0e6f1de27')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Tokai', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e14081c2-370b-4477-8350-3cb0e6f1de27', 1, '7eb46872-6aac-4e92-9ebe-eee58948b883')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=3.993558212,SkilledLaborCost=96.23552572,ForemanCost=76.74465931,TechnicianCost=59.54270072,EngineerCost=65.37357921,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=0.142492173,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=86.39644591,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'e14081c2-370b-4477-8350-3cb0e6f1de27'
   UPDATE dbo.Countries SET Name=N'Japan Tokai',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7eb46872-6aac-4e92-9ebe-eee58948b883'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a6365927-dc05-4251-8ef4-cdd843385128')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.52, 0.42, 0.74, 0.8854, 66.0778422, 26.54182319, 19.12708154, 50.16088436, 75.2222367, 0.024615993, 0.014314043, 6.651361071, 7.383808832, 206.4222226, 0, 1, 'a6365927-dc05-4251-8ef4-cdd843385128')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Japan Tokyo', N'a38926dc-4d15-41dc-ae37-8fe2ff97c824', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'a6365927-dc05-4251-8ef4-cdd843385128', 1, 'd0c8f06b-8f08-4937-8407-d6f30120139f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.52,ShiftCharge2ShiftModel=0.42,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8854,UnskilledLaborCost=66.0778422,SkilledLaborCost=26.54182319,ForemanCost=19.12708154,TechnicianCost=50.16088436,EngineerCost=75.2222367,EnergyCost=0.024615993,AirCost=0.014314043,WaterCost=6.651361071,ProductionAreaRentalCost=7.383808832,OfficeAreaRentalCost=206.4222226,InterestRate=0,IsScrambled=1 WHERE [Guid] = 'a6365927-dc05-4251-8ef4-cdd843385128'
   UPDATE dbo.Countries SET Name=N'Japan Tokyo',CurrencyGuid=N'a38926dc-4d15-41dc-ae37-8fe2ff97c824',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd0c8f06b-8f08-4937-8407-d6f30120139f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '580cdbc5-9850-4b48-b1ef-107e2b0b96a0')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.39, 0.95, 0.11, 0.85, 9.549499064, 5.670701398, 1.936365766, 1.494944122, 1.494949983, 0.837776256, 0.011697497, 0.288180703, 5.4, 26.0, 0.0025, 1, '580cdbc5-9850-4b48-b1ef-107e2b0b96a0')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Latvia', N'd7e2af7e-bd32-4500-8349-0e0a2736fc51', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'580cdbc5-9850-4b48-b1ef-107e2b0b96a0', 1, '9471c9ad-13c0-47ff-9b0a-868688081bb2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.39,ShiftCharge2ShiftModel=0.95,ShiftCharge3ShiftModel=0.11,LaborAvailability=0.85,UnskilledLaborCost=9.549499064,SkilledLaborCost=5.670701398,ForemanCost=1.936365766,TechnicianCost=1.494944122,EngineerCost=1.494949983,EnergyCost=0.837776256,AirCost=0.011697497,WaterCost=0.288180703,ProductionAreaRentalCost=5.4,OfficeAreaRentalCost=26.0,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '580cdbc5-9850-4b48-b1ef-107e2b0b96a0'
   UPDATE dbo.Countries SET Name=N'Latvia',CurrencyGuid=N'd7e2af7e-bd32-4500-8349-0e0a2736fc51',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9471c9ad-13c0-47ff-9b0a-868688081bb2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '56861f72-5a0c-4f0e-8951-45bfe96b8515')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.81, 0.41, 0.48, 0.8977, 8.935028068, 2.37368505, 4.564712342, 7.760619811, 56.81995769, 0.23773316, 0.011671845, 6.98, 5.87, 76.89, 0.0075, 1, '56861f72-5a0c-4f0e-8951-45bfe96b8515')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Lithuania', N'5e9c4cb4-6e58-4e1a-b12d-5da40ffdf61a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'56861f72-5a0c-4f0e-8951-45bfe96b8515', 1, 'cb23571c-e3d8-4644-b765-fab51560bf16')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.81,ShiftCharge2ShiftModel=0.41,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.8977,UnskilledLaborCost=8.935028068,SkilledLaborCost=2.37368505,ForemanCost=4.564712342,TechnicianCost=7.760619811,EngineerCost=56.81995769,EnergyCost=0.23773316,AirCost=0.011671845,WaterCost=6.98,ProductionAreaRentalCost=5.87,OfficeAreaRentalCost=76.89,InterestRate=0.0075,IsScrambled=1 WHERE [Guid] = '56861f72-5a0c-4f0e-8951-45bfe96b8515'
   UPDATE dbo.Countries SET Name=N'Lithuania',CurrencyGuid=N'5e9c4cb4-6e58-4e1a-b12d-5da40ffdf61a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cb23571c-e3d8-4644-b765-fab51560bf16'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f437a088-12d8-40bf-854e-372efc4ca6cd')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.8, 0.8, 0.2, 0.9636, 59.70549797, 49.37691769, 55.58598596, 61.62852791, 38.78639861, 0.556009106, 0.011235754, 0.292159904, 4.414582325, 38.55555538, 0.0025, 1, 'f437a088-12d8-40bf-854e-372efc4ca6cd')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Luxembourg', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f437a088-12d8-40bf-854e-372efc4ca6cd', 1, '941c9e35-4659-4742-bf22-5433a2c1052f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.8,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.9636,UnskilledLaborCost=59.70549797,SkilledLaborCost=49.37691769,ForemanCost=55.58598596,TechnicianCost=61.62852791,EngineerCost=38.78639861,EnergyCost=0.556009106,AirCost=0.011235754,WaterCost=0.292159904,ProductionAreaRentalCost=4.414582325,OfficeAreaRentalCost=38.55555538,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'f437a088-12d8-40bf-854e-372efc4ca6cd'
   UPDATE dbo.Countries SET Name=N'Luxembourg',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '941c9e35-4659-4742-bf22-5433a2c1052f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'b21d9f8f-b524-45c7-984d-c49eda0172a4')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.59, 0.89, 0.85, 0.981182046006944, 1.043087973, 2.04043838, 7.219064529, 66.65241555, 69.10650009, 0.013851034, 0.005079176, 0.255587165, 9.037095126, 5.690382046, 0.03, 1, 'b21d9f8f-b524-45c7-984d-c49eda0172a4')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Malaysia Country Average', N'15a60f8c-b5a2-4122-b991-1af0077a6b0e', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'b21d9f8f-b524-45c7-984d-c49eda0172a4', 1, '2be53052-b49f-46e3-9067-b1df315db67a')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.59,ShiftCharge2ShiftModel=0.89,ShiftCharge3ShiftModel=0.85,LaborAvailability=0.981182046006944,UnskilledLaborCost=1.043087973,SkilledLaborCost=2.04043838,ForemanCost=7.219064529,TechnicianCost=66.65241555,EngineerCost=69.10650009,EnergyCost=0.013851034,AirCost=0.005079176,WaterCost=0.255587165,ProductionAreaRentalCost=9.037095126,OfficeAreaRentalCost=5.690382046,InterestRate=0.03,IsScrambled=1 WHERE [Guid] = 'b21d9f8f-b524-45c7-984d-c49eda0172a4'
   UPDATE dbo.Countries SET Name=N'Malaysia Country Average',CurrencyGuid=N'15a60f8c-b5a2-4122-b991-1af0077a6b0e',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '2be53052-b49f-46e3-9067-b1df315db67a'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3dbededd-0847-4aad-8d45-ae8adfb9d527')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.59, 0.89, 0.85, 0.981182046006944, 5.431905465, 1.948672897, 4.383072702, 7.145956399, 46.93111015, 0.072374679, 0.005079176, 0.58311465, 9.037095126, 95.0, 0.03, 1, '3dbededd-0847-4aad-8d45-ae8adfb9d527')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Malaysia Kuala Lumpur', N'15a60f8c-b5a2-4122-b991-1af0077a6b0e', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3dbededd-0847-4aad-8d45-ae8adfb9d527', 1, 'bdfe2493-f908-49ad-83b8-65d109debb27')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.59,ShiftCharge2ShiftModel=0.89,ShiftCharge3ShiftModel=0.85,LaborAvailability=0.981182046006944,UnskilledLaborCost=5.431905465,SkilledLaborCost=1.948672897,ForemanCost=4.383072702,TechnicianCost=7.145956399,EngineerCost=46.93111015,EnergyCost=0.072374679,AirCost=0.005079176,WaterCost=0.58311465,ProductionAreaRentalCost=9.037095126,OfficeAreaRentalCost=95.0,InterestRate=0.03,IsScrambled=1 WHERE [Guid] = '3dbededd-0847-4aad-8d45-ae8adfb9d527'
   UPDATE dbo.Countries SET Name=N'Malaysia Kuala Lumpur',CurrencyGuid=N'15a60f8c-b5a2-4122-b991-1af0077a6b0e',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bdfe2493-f908-49ad-83b8-65d109debb27'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '60e92dd7-3ab8-410c-bf0a-353b76dba674')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.59, 0.89, 0.85, 0.981182046006944, 2.294143905, 7.203136359, 16.63397546, 46.3440049, 95.55053325, 0.072374679, 0.005079176, 0.171927341, 9.037095126, 5.690382046, 0.03, 1, '60e92dd7-3ab8-410c-bf0a-353b76dba674')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Malaysia Singapore', N'15a60f8c-b5a2-4122-b991-1af0077a6b0e', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'60e92dd7-3ab8-410c-bf0a-353b76dba674', 1, '42fa4964-5841-4194-8e34-cf838c5c7db8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.59,ShiftCharge2ShiftModel=0.89,ShiftCharge3ShiftModel=0.85,LaborAvailability=0.981182046006944,UnskilledLaborCost=2.294143905,SkilledLaborCost=7.203136359,ForemanCost=16.63397546,TechnicianCost=46.3440049,EngineerCost=95.55053325,EnergyCost=0.072374679,AirCost=0.005079176,WaterCost=0.171927341,ProductionAreaRentalCost=9.037095126,OfficeAreaRentalCost=5.690382046,InterestRate=0.03,IsScrambled=1 WHERE [Guid] = '60e92dd7-3ab8-410c-bf0a-353b76dba674'
   UPDATE dbo.Countries SET Name=N'Malaysia Singapore',CurrencyGuid=N'15a60f8c-b5a2-4122-b991-1af0077a6b0e',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '42fa4964-5841-4194-8e34-cf838c5c7db8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '65d44106-9b72-46c4-9125-09ebe8b55826')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.59, 0.89, 0.85, 0.981182046006944, 5.1829859, 1.20622903, 4.115973175, 3.93935937, 26.96625763, 0.072374679, 0.005079176, 0.58311465, 9.037095126, 5.690382046, 0.03, 1, '65d44106-9b72-46c4-9125-09ebe8b55826')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Malaysia Suburb', N'15a60f8c-b5a2-4122-b991-1af0077a6b0e', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'65d44106-9b72-46c4-9125-09ebe8b55826', 1, '09f58127-33b8-4ab8-8378-50f89c391821')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.59,ShiftCharge2ShiftModel=0.89,ShiftCharge3ShiftModel=0.85,LaborAvailability=0.981182046006944,UnskilledLaborCost=5.1829859,SkilledLaborCost=1.20622903,ForemanCost=4.115973175,TechnicianCost=3.93935937,EngineerCost=26.96625763,EnergyCost=0.072374679,AirCost=0.005079176,WaterCost=0.58311465,ProductionAreaRentalCost=9.037095126,OfficeAreaRentalCost=5.690382046,InterestRate=0.03,IsScrambled=1 WHERE [Guid] = '65d44106-9b72-46c4-9125-09ebe8b55826'
   UPDATE dbo.Countries SET Name=N'Malaysia Suburb',CurrencyGuid=N'15a60f8c-b5a2-4122-b991-1af0077a6b0e',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '09f58127-33b8-4ab8-8378-50f89c391821'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f465ab75-15a4-449f-b7b6-45126fb0103b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.85, 0.41, 0.8063, 3.1956193, 60.01686247, 96.68054555, 86.00308906, 69.62947476, 0.140000736, 0.020547578, 6.84, 1.323841901, 1.646030943, 0.0025, 1, 'f465ab75-15a4-449f-b7b6-45126fb0103b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Malta', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f465ab75-15a4-449f-b7b6-45126fb0103b', 1, '2c9d9f6f-2ae9-4141-9088-37f63c9e70e2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.85,ShiftCharge3ShiftModel=0.41,LaborAvailability=0.8063,UnskilledLaborCost=3.1956193,SkilledLaborCost=60.01686247,ForemanCost=96.68054555,TechnicianCost=86.00308906,EngineerCost=69.62947476,EnergyCost=0.140000736,AirCost=0.020547578,WaterCost=6.84,ProductionAreaRentalCost=1.323841901,OfficeAreaRentalCost=1.646030943,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'f465ab75-15a4-449f-b7b6-45126fb0103b'
   UPDATE dbo.Countries SET Name=N'Malta',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '2c9d9f6f-2ae9-4141-9088-37f63c9e70e2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9b4eba50-3b2f-4ee3-b3bb-cce01d36a4c2')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.8, 0.8, 0.8, 0.9581, 5.028699018, 5.698235137, 4.88024786, 3.133921096, 96.15854963, 0.062148923, 0.007157021, 0.743431711, 9.74715514, 49.42222222, 0.035, 1, '9b4eba50-3b2f-4ee3-b3bb-cce01d36a4c2')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Mexico', N'da76fbd7-82e3-49e2-b960-9be5da0e2949', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9b4eba50-3b2f-4ee3-b3bb-cce01d36a4c2', 1, '8e98e269-fc93-498f-a06b-77b92fdfa184')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.8,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.8,LaborAvailability=0.9581,UnskilledLaborCost=5.028699018,SkilledLaborCost=5.698235137,ForemanCost=4.88024786,TechnicianCost=3.133921096,EngineerCost=96.15854963,EnergyCost=0.062148923,AirCost=0.007157021,WaterCost=0.743431711,ProductionAreaRentalCost=9.74715514,OfficeAreaRentalCost=49.42222222,InterestRate=0.035,IsScrambled=1 WHERE [Guid] = '9b4eba50-3b2f-4ee3-b3bb-cce01d36a4c2'
   UPDATE dbo.Countries SET Name=N'Mexico',CurrencyGuid=N'da76fbd7-82e3-49e2-b960-9be5da0e2949',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '8e98e269-fc93-498f-a06b-77b92fdfa184'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '19be482f-ed69-45cc-a763-f52ab588a51d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.85, 0.75, 0.98, 0.6944, 86.97701412, 36.50335182, 36.45312651, 59.07263158, 29.33577942, 0.259003096, 0.010389225, 0.358295712, 60.50764599, 12.84, 0.015, 1, '19be482f-ed69-45cc-a763-f52ab588a51d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Norway', N'f57315b3-ad03-46c7-9564-57c5f485a9ba', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'19be482f-ed69-45cc-a763-f52ab588a51d', 1, '8ca84a5f-4f87-48de-9f1a-90f0fe73a75c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.85,ShiftCharge2ShiftModel=0.75,ShiftCharge3ShiftModel=0.98,LaborAvailability=0.6944,UnskilledLaborCost=86.97701412,SkilledLaborCost=36.50335182,ForemanCost=36.45312651,TechnicianCost=59.07263158,EngineerCost=29.33577942,EnergyCost=0.259003096,AirCost=0.010389225,WaterCost=0.358295712,ProductionAreaRentalCost=60.50764599,OfficeAreaRentalCost=12.84,InterestRate=0.015,IsScrambled=1 WHERE [Guid] = '19be482f-ed69-45cc-a763-f52ab588a51d'
   UPDATE dbo.Countries SET Name=N'Norway',CurrencyGuid=N'f57315b3-ad03-46c7-9564-57c5f485a9ba',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '8ca84a5f-4f87-48de-9f1a-90f0fe73a75c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '335966a2-6fd6-4d4e-aab4-fa1f82389d00')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.930208333333333, 1.900894625, 6.828059665, 9.958913227, 9.174093695, 9.242454785, 0.071353476, 0.010632922, 0.307894995, 9.378229911, 5.724042407, 0.0119, 1, '335966a2-6fd6-4d4e-aab4-fa1f82389d00')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Panama', N'296b656e-3b5a-4170-9258-d1a63f68f546', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'335966a2-6fd6-4d4e-aab4-fa1f82389d00', 1, '35eb4cc7-99d2-48b7-8c39-dd3e0f9cd65e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.930208333333333,UnskilledLaborCost=1.900894625,SkilledLaborCost=6.828059665,ForemanCost=9.958913227,TechnicianCost=9.174093695,EngineerCost=9.242454785,EnergyCost=0.071353476,AirCost=0.010632922,WaterCost=0.307894995,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=5.724042407,InterestRate=0.0119,IsScrambled=1 WHERE [Guid] = '335966a2-6fd6-4d4e-aab4-fa1f82389d00'
   UPDATE dbo.Countries SET Name=N'Panama',CurrencyGuid=N'296b656e-3b5a-4170-9258-d1a63f68f546',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '35eb4cc7-99d2-48b7-8c39-dd3e0f9cd65e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '2c8371a0-88de-440a-a6b0-a37fb275893a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.2326, 1.900894625, 6.828059665, 9.958913227, 9.174093695, 9.242454785, 0.071353476, 0.010632922, 0.307894995, 9.378229911, 5.724042407, 0.0119, 1, '2c8371a0-88de-440a-a6b0-a37fb275893a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Panama Country', N'296b656e-3b5a-4170-9258-d1a63f68f546', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'2c8371a0-88de-440a-a6b0-a37fb275893a', 1, '0ea12d69-82c6-4fa4-92f0-ae9e96ed82f7')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.2326,UnskilledLaborCost=1.900894625,SkilledLaborCost=6.828059665,ForemanCost=9.958913227,TechnicianCost=9.174093695,EngineerCost=9.242454785,EnergyCost=0.071353476,AirCost=0.010632922,WaterCost=0.307894995,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=5.724042407,InterestRate=0.0119,IsScrambled=1 WHERE [Guid] = '2c8371a0-88de-440a-a6b0-a37fb275893a'
   UPDATE dbo.Countries SET Name=N'Panama Country',CurrencyGuid=N'296b656e-3b5a-4170-9258-d1a63f68f546',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0ea12d69-82c6-4fa4-92f0-ae9e96ed82f7'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6211c34a-7390-4049-892d-c1444bb83a50')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.8359375, 5.952511651, 5.92451247, 1.996253534, 8.362654523, 7.324930839, 0.01623062, 0.010632922, 0.182488122, 1.062878498, 36.55555538, 0.04, 1, '6211c34a-7390-4049-892d-c1444bb83a50')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Peru', N'a0e5548f-ca4c-4285-8a97-084436cce682', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6211c34a-7390-4049-892d-c1444bb83a50', 1, '7a1339f4-bb28-41bb-bb30-693b145d5881')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.8359375,UnskilledLaborCost=5.952511651,SkilledLaborCost=5.92451247,ForemanCost=1.996253534,TechnicianCost=8.362654523,EngineerCost=7.324930839,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=36.55555538,InterestRate=0.04,IsScrambled=1 WHERE [Guid] = '6211c34a-7390-4049-892d-c1444bb83a50'
   UPDATE dbo.Countries SET Name=N'Peru',CurrencyGuid=N'a0e5548f-ca4c-4285-8a97-084436cce682',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7a1339f4-bb28-41bb-bb30-693b145d5881'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '7c4712e0-b5c3-4fc2-afce-0f6426a6ddb7')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.8359375, 1.087541847, 1.74500572, 8.33251994, 4.081178115, 66.08768164, 0.01623062, 0.010632922, 0.182488122, 1.062878498, 86.28802913, 0.04, 1, '7c4712e0-b5c3-4fc2-afce-0f6426a6ddb7')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Peru Country', N'a0e5548f-ca4c-4285-8a97-084436cce682', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'7c4712e0-b5c3-4fc2-afce-0f6426a6ddb7', 1, 'b864b16f-57ee-4577-8e8c-08f697fa943c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.8359375,UnskilledLaborCost=1.087541847,SkilledLaborCost=1.74500572,ForemanCost=8.33251994,TechnicianCost=4.081178115,EngineerCost=66.08768164,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=86.28802913,InterestRate=0.04,IsScrambled=1 WHERE [Guid] = '7c4712e0-b5c3-4fc2-afce-0f6426a6ddb7'
   UPDATE dbo.Countries SET Name=N'Peru Country',CurrencyGuid=N'a0e5548f-ca4c-4285-8a97-084436cce682',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'b864b16f-57ee-4577-8e8c-08f697fa943c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd34c90fe-0b8a-4c8c-824d-5a4391ff8d40')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.19, 0.19, 0.65, 0.9625, 6.111749605, 6.60334257, 5.00752802, 5.368441652, 2.134406763, 0.686874746, 0.004950914, 0.586951705, 6.759428927, 99.55555538, 0.035, 1, 'd34c90fe-0b8a-4c8c-824d-5a4391ff8d40')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Philippines', N'c60f7569-383e-4e80-834e-f33455988942', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'd34c90fe-0b8a-4c8c-824d-5a4391ff8d40', 1, 'bf8eeda7-766b-47d4-9758-f29ef5f6f25c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.19,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.9625,UnskilledLaborCost=6.111749605,SkilledLaborCost=6.60334257,ForemanCost=5.00752802,TechnicianCost=5.368441652,EngineerCost=2.134406763,EnergyCost=0.686874746,AirCost=0.004950914,WaterCost=0.586951705,ProductionAreaRentalCost=6.759428927,OfficeAreaRentalCost=99.55555538,InterestRate=0.035,IsScrambled=1 WHERE [Guid] = 'd34c90fe-0b8a-4c8c-824d-5a4391ff8d40'
   UPDATE dbo.Countries SET Name=N'Philippines',CurrencyGuid=N'c60f7569-383e-4e80-834e-f33455988942',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bf8eeda7-766b-47d4-9758-f29ef5f6f25c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'dd29ceeb-a6f0-4444-b2a8-75580fb99bab')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.11, 0.21, 0.88, 0.8542, 1.545321658, 8.243517846, 2.885291589, 4.238216582, 60.43184124, 0.450008166, 0.012030979, 0.526257684, 5.55, 15.55555538, 0.025, 1, 'dd29ceeb-a6f0-4444-b2a8-75580fb99bab')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Poland', N'4008af4c-84f5-4670-86eb-19bb6ad3faa8', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'dd29ceeb-a6f0-4444-b2a8-75580fb99bab', 1, '4c10f8c0-792b-43f3-8bef-f8eb1468a6cc')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.11,ShiftCharge2ShiftModel=0.21,ShiftCharge3ShiftModel=0.88,LaborAvailability=0.8542,UnskilledLaborCost=1.545321658,SkilledLaborCost=8.243517846,ForemanCost=2.885291589,TechnicianCost=4.238216582,EngineerCost=60.43184124,EnergyCost=0.450008166,AirCost=0.012030979,WaterCost=0.526257684,ProductionAreaRentalCost=5.55,OfficeAreaRentalCost=15.55555538,InterestRate=0.025,IsScrambled=1 WHERE [Guid] = 'dd29ceeb-a6f0-4444-b2a8-75580fb99bab'
   UPDATE dbo.Countries SET Name=N'Poland',CurrencyGuid=N'4008af4c-84f5-4670-86eb-19bb6ad3faa8',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4c10f8c0-792b-43f3-8bef-f8eb1468a6cc'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9f5e44aa-7f71-4598-a1f6-db16efc6d678')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.28, 0.2, 0.94, 0.8396, 60.29382547, 96.19492888, 86.29180705, 36.30109512, 29.56842989, 0.854778616, 0.011235754, 0.292159904, 5.0, 19.42222226, 0.0025, 1, '9f5e44aa-7f71-4598-a1f6-db16efc6d678')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Portugal', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9f5e44aa-7f71-4598-a1f6-db16efc6d678', 1, 'e256e801-3b81-49bc-9d00-bf5ad919efbe')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.28,ShiftCharge2ShiftModel=0.2,ShiftCharge3ShiftModel=0.94,LaborAvailability=0.8396,UnskilledLaborCost=60.29382547,SkilledLaborCost=96.19492888,ForemanCost=86.29180705,TechnicianCost=36.30109512,EngineerCost=29.56842989,EnergyCost=0.854778616,AirCost=0.011235754,WaterCost=0.292159904,ProductionAreaRentalCost=5.0,OfficeAreaRentalCost=19.42222226,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '9f5e44aa-7f71-4598-a1f6-db16efc6d678'
   UPDATE dbo.Countries SET Name=N'Portugal',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e256e801-3b81-49bc-9d00-bf5ad919efbe'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '497213d3-4fbe-4d1d-b9c8-b9562f27faf6')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.71, 0.68, 0.62, 1.0433, 9.113993303, 5.21698669, 5.508665297, 1.498566974, 4.253500662, 0.603772556, 0.009594, 0.892778, 1.9, 39.89, 0.04, 1, '497213d3-4fbe-4d1d-b9c8-b9562f27faf6')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Romania Country Average', N'1332f371-ce11-45e5-91ee-db06b1bbc168', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'497213d3-4fbe-4d1d-b9c8-b9562f27faf6', 1, '9ee01759-0356-4270-9b4c-7553985febd1')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.71,ShiftCharge2ShiftModel=0.68,ShiftCharge3ShiftModel=0.62,LaborAvailability=1.0433,UnskilledLaborCost=9.113993303,SkilledLaborCost=5.21698669,ForemanCost=5.508665297,TechnicianCost=1.498566974,EngineerCost=4.253500662,EnergyCost=0.603772556,AirCost=0.009594,WaterCost=0.892778,ProductionAreaRentalCost=1.9,OfficeAreaRentalCost=39.89,InterestRate=0.04,IsScrambled=1 WHERE [Guid] = '497213d3-4fbe-4d1d-b9c8-b9562f27faf6'
   UPDATE dbo.Countries SET Name=N'Romania Country Average',CurrencyGuid=N'1332f371-ce11-45e5-91ee-db06b1bbc168',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9ee01759-0356-4270-9b4c-7553985febd1'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6c0c8b9d-bf3e-4cdb-998a-ea300b9446ad')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.35, 0.98, 0.860243055555556, 9.558443642, 6.17870369, 9.873960864, 9.922612976, 9.848723733, 0.086312198, 0.009132257, 0.510224048, 4.030829262, 87.05555553, 0.055, 1, '6c0c8b9d-bf3e-4cdb-998a-ea300b9446ad')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Russia Country Average', N'b551ece6-f8a3-40e8-811e-69336c1449be', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'6c0c8b9d-bf3e-4cdb-998a-ea300b9446ad', 1, 'cc2a89c4-63da-4785-8728-1ad36e9fd346')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.35,ShiftCharge3ShiftModel=0.98,LaborAvailability=0.860243055555556,UnskilledLaborCost=9.558443642,SkilledLaborCost=6.17870369,ForemanCost=9.873960864,TechnicianCost=9.922612976,EngineerCost=9.848723733,EnergyCost=0.086312198,AirCost=0.009132257,WaterCost=0.510224048,ProductionAreaRentalCost=4.030829262,OfficeAreaRentalCost=87.05555553,InterestRate=0.055,IsScrambled=1 WHERE [Guid] = '6c0c8b9d-bf3e-4cdb-998a-ea300b9446ad'
   UPDATE dbo.Countries SET Name=N'Russia Country Average',CurrencyGuid=N'b551ece6-f8a3-40e8-811e-69336c1449be',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cc2a89c4-63da-4785-8728-1ad36e9fd346'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1a4e7887-fe8e-463f-91c6-a9d160ad63d2')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.36, 0.19, 0.45, 0.833333333333333, 26.02086785, 36.92827079, 99.2183789, 49.02868203, 35.1881886, 0.099690942, 0.011697497, 0.318014669, 1.960921893, 26.91597819, 0.005, 1, '1a4e7887-fe8e-463f-91c6-a9d160ad63d2')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Scotland Country Average', N'9025593a-2a7a-45a9-a8eb-7ce7568b4188', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1a4e7887-fe8e-463f-91c6-a9d160ad63d2', 1, 'cadd1efa-2d2a-4cc5-8433-ad0508ca4647')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.36,ShiftCharge2ShiftModel=0.19,ShiftCharge3ShiftModel=0.45,LaborAvailability=0.833333333333333,UnskilledLaborCost=26.02086785,SkilledLaborCost=36.92827079,ForemanCost=99.2183789,TechnicianCost=49.02868203,EngineerCost=35.1881886,EnergyCost=0.099690942,AirCost=0.011697497,WaterCost=0.318014669,ProductionAreaRentalCost=1.960921893,OfficeAreaRentalCost=26.91597819,InterestRate=0.005,IsScrambled=1 WHERE [Guid] = '1a4e7887-fe8e-463f-91c6-a9d160ad63d2'
   UPDATE dbo.Countries SET Name=N'Scotland Country Average',CurrencyGuid=N'9025593a-2a7a-45a9-a8eb-7ce7568b4188',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cadd1efa-2d2a-4cc5-8433-ad0508ca4647'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9b43ca44-3de2-4867-881d-50c6a2bd3f8d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.58, 0.78, 0.54, 0.8229, 1.89268, 8.538754754, 8.553816298, 3.89201629, 66.97995751, 0.879000266, 0.012107936, 0.77814284, 5.025031669, 76.8, 0.0025, 1, '9b43ca44-3de2-4867-881d-50c6a2bd3f8d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Slovenia', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9b43ca44-3de2-4867-881d-50c6a2bd3f8d', 1, 'c65554e4-c782-4588-bf25-f5166b5b8762')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.58,ShiftCharge2ShiftModel=0.78,ShiftCharge3ShiftModel=0.54,LaborAvailability=0.8229,UnskilledLaborCost=1.89268,SkilledLaborCost=8.538754754,ForemanCost=8.553816298,TechnicianCost=3.89201629,EngineerCost=66.97995751,EnergyCost=0.879000266,AirCost=0.012107936,WaterCost=0.77814284,ProductionAreaRentalCost=5.025031669,OfficeAreaRentalCost=76.8,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '9b43ca44-3de2-4867-881d-50c6a2bd3f8d'
   UPDATE dbo.Countries SET Name=N'Slovenia',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c65554e4-c782-4588-bf25-f5166b5b8762'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9db57de2-81e5-404d-ab8a-bd79b5f4f502')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.89, 0.79, 0.61, 0.85, 6.151275086, 9.348082695, 6.688734874, 5.447702927, 4.661322774, 0.046112375, 0.010145527, 6.0, 9.23059447, 66.42222226, 0.05, 1, '9db57de2-81e5-404d-ab8a-bd79b5f4f502')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'South Africa', N'1396bcd7-2988-4f36-a427-911cd2c7e31f', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'9db57de2-81e5-404d-ab8a-bd79b5f4f502', 1, '22360abf-e703-42f3-a500-c1f7f8c7e9d0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.89,ShiftCharge2ShiftModel=0.79,ShiftCharge3ShiftModel=0.61,LaborAvailability=0.85,UnskilledLaborCost=6.151275086,SkilledLaborCost=9.348082695,ForemanCost=6.688734874,TechnicianCost=5.447702927,EngineerCost=4.661322774,EnergyCost=0.046112375,AirCost=0.010145527,WaterCost=6.0,ProductionAreaRentalCost=9.23059447,OfficeAreaRentalCost=66.42222226,InterestRate=0.05,IsScrambled=1 WHERE [Guid] = '9db57de2-81e5-404d-ab8a-bd79b5f4f502'
   UPDATE dbo.Countries SET Name=N'South Africa',CurrencyGuid=N'1396bcd7-2988-4f36-a427-911cd2c7e31f',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '22360abf-e703-42f3-a500-c1f7f8c7e9d0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '748d0231-f64b-48e8-bad9-0084c4d75e3f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.55, 0.91, 0.891900193576389, 8.206442608, 3.853044701, 3.054978251, 26.89380635, 66.06462843, 0.010566219, 0.004283952, 0.419561891, 1.989037404, 36.76014078, 0.025, 1, '748d0231-f64b-48e8-bad9-0084c4d75e3f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'South Korea Country Average', N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'748d0231-f64b-48e8-bad9-0084c4d75e3f', 1, '1d37cb7f-38aa-4463-b5d7-80e86efe7e26')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.55,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.891900193576389,UnskilledLaborCost=8.206442608,SkilledLaborCost=3.853044701,ForemanCost=3.054978251,TechnicianCost=26.89380635,EngineerCost=66.06462843,EnergyCost=0.010566219,AirCost=0.004283952,WaterCost=0.419561891,ProductionAreaRentalCost=1.989037404,OfficeAreaRentalCost=36.76014078,InterestRate=0.025,IsScrambled=1 WHERE [Guid] = '748d0231-f64b-48e8-bad9-0084c4d75e3f'
   UPDATE dbo.Countries SET Name=N'South Korea Country Average',CurrencyGuid=N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1d37cb7f-38aa-4463-b5d7-80e86efe7e26'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3710c563-14cf-4ba7-ac69-4d0670bf350a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.55, 0.91, 0.891900193576389, 8.535438674, 3.477843688, 56.02992839, 86.78436743, 69.04497138, 0.010566219, 0.004283952, 0.419561891, 1.989037404, 15.55555555, 0.025, 1, '3710c563-14cf-4ba7-ac69-4d0670bf350a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'South Korea Seoul City', N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3710c563-14cf-4ba7-ac69-4d0670bf350a', 1, 'e7160e11-93b1-414a-81df-92220e5a23a9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.55,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.891900193576389,UnskilledLaborCost=8.535438674,SkilledLaborCost=3.477843688,ForemanCost=56.02992839,TechnicianCost=86.78436743,EngineerCost=69.04497138,EnergyCost=0.010566219,AirCost=0.004283952,WaterCost=0.419561891,ProductionAreaRentalCost=1.989037404,OfficeAreaRentalCost=15.55555555,InterestRate=0.025,IsScrambled=1 WHERE [Guid] = '3710c563-14cf-4ba7-ac69-4d0670bf350a'
   UPDATE dbo.Countries SET Name=N'South Korea Seoul City',CurrencyGuid=N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e7160e11-93b1-414a-81df-92220e5a23a9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '60e2f2a9-9181-43fe-ae64-95189a597307')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.65, 0.55, 0.91, 0.891900193576389, 8.49340411, 3.040032376, 96.73009665, 86.09373713, 76.38047307, 0.010566219, 0.004283952, 0.419561891, 1.989037404, 66.17243724, 0.025, 1, '60e2f2a9-9181-43fe-ae64-95189a597307')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'South Korea Soul Suburb', N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'60e2f2a9-9181-43fe-ae64-95189a597307', 1, '7b9cc0be-b22a-43c6-ad34-479905cbe808')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.65,ShiftCharge2ShiftModel=0.55,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.891900193576389,UnskilledLaborCost=8.49340411,SkilledLaborCost=3.040032376,ForemanCost=96.73009665,TechnicianCost=86.09373713,EngineerCost=76.38047307,EnergyCost=0.010566219,AirCost=0.004283952,WaterCost=0.419561891,ProductionAreaRentalCost=1.989037404,OfficeAreaRentalCost=66.17243724,InterestRate=0.025,IsScrambled=1 WHERE [Guid] = '60e2f2a9-9181-43fe-ae64-95189a597307'
   UPDATE dbo.Countries SET Name=N'South Korea Soul Suburb',CurrencyGuid=N'9e74c06b-061e-408a-b32f-00b2c5c9fb6b',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7b9cc0be-b22a-43c6-ad34-479905cbe808'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'cd9d6ea7-8e4c-412f-bc83-3ed5ba32c879')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.24, 0.34, 0.43, 0.9, 76.59351812, 99.97267621, 49.85544845, 55.70635339, 21.67545673, 0.353776316, 0.012505548, 0.384278634, 2.0, 25.55555538, 0.0025, 1, 'cd9d6ea7-8e4c-412f-bc83-3ed5ba32c879')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Spain', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'cd9d6ea7-8e4c-412f-bc83-3ed5ba32c879', 1, '1e39fb29-5fae-40de-8d81-ea45330fc535')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.24,ShiftCharge2ShiftModel=0.34,ShiftCharge3ShiftModel=0.43,LaborAvailability=0.9,UnskilledLaborCost=76.59351812,SkilledLaborCost=99.97267621,ForemanCost=49.85544845,TechnicianCost=55.70635339,EngineerCost=21.67545673,EnergyCost=0.353776316,AirCost=0.012505548,WaterCost=0.384278634,ProductionAreaRentalCost=2.0,OfficeAreaRentalCost=25.55555538,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'cd9d6ea7-8e4c-412f-bc83-3ed5ba32c879'
   UPDATE dbo.Countries SET Name=N'Spain',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1e39fb29-5fae-40de-8d81-ea45330fc535'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f6c07aa9-70b7-46a3-8252-5b87567a9ff3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.38, 0.82, 0.74, 0.8636, 46.73593829, 76.84302542, 19.00716924, 79.37838189, 61.17961529, 0.303778006, 0.009478564, 0.629061978, 8.078145187, 80.8, 0.0075, 1, 'f6c07aa9-70b7-46a3-8252-5b87567a9ff3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Sweden', N'a20fa490-4753-454e-b6a4-983f913a0a3f', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'f6c07aa9-70b7-46a3-8252-5b87567a9ff3', 1, '1dda3ea8-be5f-4336-ba44-0d6969083967')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.38,ShiftCharge2ShiftModel=0.82,ShiftCharge3ShiftModel=0.74,LaborAvailability=0.8636,UnskilledLaborCost=46.73593829,SkilledLaborCost=76.84302542,ForemanCost=19.00716924,TechnicianCost=79.37838189,EngineerCost=61.17961529,EnergyCost=0.303778006,AirCost=0.009478564,WaterCost=0.629061978,ProductionAreaRentalCost=8.078145187,OfficeAreaRentalCost=80.8,InterestRate=0.0075,IsScrambled=1 WHERE [Guid] = 'f6c07aa9-70b7-46a3-8252-5b87567a9ff3'
   UPDATE dbo.Countries SET Name=N'Sweden',CurrencyGuid=N'a20fa490-4753-454e-b6a4-983f913a0a3f',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1dda3ea8-be5f-4336-ba44-0d6969083967'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3420cd6c-36e8-4267-bfbd-e831b7341357')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.81, 0.48, 0.7389, 3.175796127, 69.89987436, 90.75951978, 59.68457439, 29.84305062, 0.069923727, 0.013210989, 0.953232893, 7.984023818, 20.84, 0, 1, '3420cd6c-36e8-4267-bfbd-e831b7341357')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Switzerland', N'6b3d7733-8fb5-4c8b-bf22-18b358289b19', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'3420cd6c-36e8-4267-bfbd-e831b7341357', 1, '286ed5d8-64c9-42c8-bb88-777dbd5f96b7')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.81,ShiftCharge3ShiftModel=0.48,LaborAvailability=0.7389,UnskilledLaborCost=3.175796127,SkilledLaborCost=69.89987436,ForemanCost=90.75951978,TechnicianCost=59.68457439,EngineerCost=29.84305062,EnergyCost=0.069923727,AirCost=0.013210989,WaterCost=0.953232893,ProductionAreaRentalCost=7.984023818,OfficeAreaRentalCost=20.84,InterestRate=0,IsScrambled=1 WHERE [Guid] = '3420cd6c-36e8-4267-bfbd-e831b7341357'
   UPDATE dbo.Countries SET Name=N'Switzerland',CurrencyGuid=N'6b3d7733-8fb5-4c8b-bf22-18b358289b19',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '286ed5d8-64c9-42c8-bb88-777dbd5f96b7'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'be5f09a8-fcb2-4944-87db-0a82fbf780b3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.76, 0.29, 0.1, 0.929166666666667, 9.900642275, 5.167968012, 8.747206671, 2.242549142, 96.70933728, 0.045823254, 0.004989393, 0.453833005, 1.056420781, 45.42222261, 0.0188, 1, 'be5f09a8-fcb2-4944-87db-0a82fbf780b3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Taiwan', N'53b9557c-6b56-4754-80f5-dd67705faa40', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'be5f09a8-fcb2-4944-87db-0a82fbf780b3', 1, '73814ab3-5364-469d-9426-3ebd8059f98f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.76,ShiftCharge2ShiftModel=0.29,ShiftCharge3ShiftModel=0.1,LaborAvailability=0.929166666666667,UnskilledLaborCost=9.900642275,SkilledLaborCost=5.167968012,ForemanCost=8.747206671,TechnicianCost=2.242549142,EngineerCost=96.70933728,EnergyCost=0.045823254,AirCost=0.004989393,WaterCost=0.453833005,ProductionAreaRentalCost=1.056420781,OfficeAreaRentalCost=45.42222261,InterestRate=0.0188,IsScrambled=1 WHERE [Guid] = 'be5f09a8-fcb2-4944-87db-0a82fbf780b3'
   UPDATE dbo.Countries SET Name=N'Taiwan',CurrencyGuid=N'53b9557c-6b56-4754-80f5-dd67705faa40',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '73814ab3-5364-469d-9426-3ebd8059f98f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'bb6f340f-47f8-4852-8032-a000c15c581d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.9, 0.99, 0.65, 0.968912973524306, 0.243275132, 0.798561337, 6.048493751, 9.355596, 9.563440184, 0.816267442, 0.005310048, 0.300343655, 5.848506737, 90.05555553, 0.0188, 1, 'bb6f340f-47f8-4852-8032-a000c15c581d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Thailand', N'd6115dea-95ea-4e47-8667-bb889ba9b102', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'bb6f340f-47f8-4852-8032-a000c15c581d', 1, '0328ee4e-02a3-4d3e-8335-2caa39e3f5bc')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.9,ShiftCharge2ShiftModel=0.99,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.968912973524306,UnskilledLaborCost=0.243275132,SkilledLaborCost=0.798561337,ForemanCost=6.048493751,TechnicianCost=9.355596,EngineerCost=9.563440184,EnergyCost=0.816267442,AirCost=0.005310048,WaterCost=0.300343655,ProductionAreaRentalCost=5.848506737,OfficeAreaRentalCost=90.05555553,InterestRate=0.0188,IsScrambled=1 WHERE [Guid] = 'bb6f340f-47f8-4852-8032-a000c15c581d'
   UPDATE dbo.Countries SET Name=N'Thailand',CurrencyGuid=N'd6115dea-95ea-4e47-8667-bb889ba9b102',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0328ee4e-02a3-4d3e-8335-2caa39e3f5bc'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '7779158d-e7d4-4363-82c3-b99240486a2f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.9, 0.99, 0.65, 0.968912973524306, 9.457625544, 1.322473796, 2.7278085, 4.652956404, 16.98287768, 0.049920805, 0.005310048, 0.300343655, 5.848506737, 96.56477342, 0.0225, 1, '7779158d-e7d4-4363-82c3-b99240486a2f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Thailand Bangkok', N'd6115dea-95ea-4e47-8667-bb889ba9b102', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'7779158d-e7d4-4363-82c3-b99240486a2f', 1, '5b71f380-4d9f-43c5-bfe0-4fa5cf29ad18')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.9,ShiftCharge2ShiftModel=0.99,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.968912973524306,UnskilledLaborCost=9.457625544,SkilledLaborCost=1.322473796,ForemanCost=2.7278085,TechnicianCost=4.652956404,EngineerCost=16.98287768,EnergyCost=0.049920805,AirCost=0.005310048,WaterCost=0.300343655,ProductionAreaRentalCost=5.848506737,OfficeAreaRentalCost=96.56477342,InterestRate=0.0225,IsScrambled=1 WHERE [Guid] = '7779158d-e7d4-4363-82c3-b99240486a2f'
   UPDATE dbo.Countries SET Name=N'Thailand Bangkok',CurrencyGuid=N'd6115dea-95ea-4e47-8667-bb889ba9b102',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5b71f380-4d9f-43c5-bfe0-4fa5cf29ad18'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '753002f5-7169-4f48-998f-fdeded96933d')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.9, 0.99, 0.65, 0.968912973524306, 9.081455601, 5.003802971, 1.424969542, 8.30800352, 60.98822902, 0.049920805, 0.005310048, 0.300343655, 5.848506737, 96.56477342, 0.0225, 1, '753002f5-7169-4f48-998f-fdeded96933d')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Thailand Payao Phrae Nan', N'd6115dea-95ea-4e47-8667-bb889ba9b102', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'753002f5-7169-4f48-998f-fdeded96933d', 1, 'e3c7a0a9-4f44-41ad-99e7-867d4b150ea0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.9,ShiftCharge2ShiftModel=0.99,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.968912973524306,UnskilledLaborCost=9.081455601,SkilledLaborCost=5.003802971,ForemanCost=1.424969542,TechnicianCost=8.30800352,EngineerCost=60.98822902,EnergyCost=0.049920805,AirCost=0.005310048,WaterCost=0.300343655,ProductionAreaRentalCost=5.848506737,OfficeAreaRentalCost=96.56477342,InterestRate=0.0225,IsScrambled=1 WHERE [Guid] = '753002f5-7169-4f48-998f-fdeded96933d'
   UPDATE dbo.Countries SET Name=N'Thailand Payao Phrae Nan',CurrencyGuid=N'd6115dea-95ea-4e47-8667-bb889ba9b102',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e3c7a0a9-4f44-41ad-99e7-867d4b150ea0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '395306e9-a426-4c29-8373-23bd1689fe22')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.9, 0.99, 0.65, 0.968912973524306, 9.604349542, 1.065446608, 2.286681056, 4.223731418, 56.18331737, 0.049920805, 0.005310048, 0.300343655, 5.848506737, 96.56477342, 0.0225, 1, '395306e9-a426-4c29-8373-23bd1689fe22')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Thailand Phuket', N'd6115dea-95ea-4e47-8667-bb889ba9b102', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'395306e9-a426-4c29-8373-23bd1689fe22', 1, 'f469b900-e02b-4591-adcf-ee41655cda51')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.9,ShiftCharge2ShiftModel=0.99,ShiftCharge3ShiftModel=0.65,LaborAvailability=0.968912973524306,UnskilledLaborCost=9.604349542,SkilledLaborCost=1.065446608,ForemanCost=2.286681056,TechnicianCost=4.223731418,EngineerCost=56.18331737,EnergyCost=0.049920805,AirCost=0.005310048,WaterCost=0.300343655,ProductionAreaRentalCost=5.848506737,OfficeAreaRentalCost=96.56477342,InterestRate=0.0225,IsScrambled=1 WHERE [Guid] = '395306e9-a426-4c29-8373-23bd1689fe22'
   UPDATE dbo.Countries SET Name=N'Thailand Phuket',CurrencyGuid=N'd6115dea-95ea-4e47-8667-bb889ba9b102',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f469b900-e02b-4591-adcf-ee41655cda51'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '842a6b79-e09a-4e5d-a5d8-084b3655b727')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.78, 0.52, 0.84, 0.698989898989899, 76.63162988, 99.4376179, 49.60523136, 55.53176036, 21.6305472, 0.630007866, 0.012915987, 0.826719403, 8.67, 61.42222222, 0.0025, 1, '842a6b79-e09a-4e5d-a5d8-084b3655b727')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'The Netherlands', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'842a6b79-e09a-4e5d-a5d8-084b3655b727', 1, '4caccb8c-4654-46a0-ae06-0e1f02f1284c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.78,ShiftCharge2ShiftModel=0.52,ShiftCharge3ShiftModel=0.84,LaborAvailability=0.698989898989899,UnskilledLaborCost=76.63162988,SkilledLaborCost=99.4376179,ForemanCost=49.60523136,TechnicianCost=55.53176036,EngineerCost=21.6305472,EnergyCost=0.630007866,AirCost=0.012915987,WaterCost=0.826719403,ProductionAreaRentalCost=8.67,OfficeAreaRentalCost=61.42222222,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '842a6b79-e09a-4e5d-a5d8-084b3655b727'
   UPDATE dbo.Countries SET Name=N'The Netherlands',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4caccb8c-4654-46a0-ae06-0e1f02f1284c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1e5e7c85-b601-45e5-b46b-2b96003b3982')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.34, 0.3, 0.73, 0.9956, 2.770890097, 4.962926383, 7.452435562, 66.42457684, 26.19086508, 0.260786906, 0.007233979, 9.586053187, 9.663390748, 71.42222261, 0.045, 1, '1e5e7c85-b601-45e5-b46b-2b96003b3982')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Turkey', N'ceaed6c8-ebae-49ba-8200-8ecffdec0162', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'1e5e7c85-b601-45e5-b46b-2b96003b3982', 1, '1d90beb6-2290-4293-a9c4-b86026d9f260')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.34,ShiftCharge2ShiftModel=0.3,ShiftCharge3ShiftModel=0.73,LaborAvailability=0.9956,UnskilledLaborCost=2.770890097,SkilledLaborCost=4.962926383,ForemanCost=7.452435562,TechnicianCost=66.42457684,EngineerCost=26.19086508,EnergyCost=0.260786906,AirCost=0.007233979,WaterCost=9.586053187,ProductionAreaRentalCost=9.663390748,OfficeAreaRentalCost=71.42222261,InterestRate=0.045,IsScrambled=1 WHERE [Guid] = '1e5e7c85-b601-45e5-b46b-2b96003b3982'
   UPDATE dbo.Countries SET Name=N'Turkey',CurrencyGuid=N'ceaed6c8-ebae-49ba-8200-8ecffdec0162',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1d90beb6-2290-4293-a9c4-b86026d9f260'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '23b2d944-5ae2-4a7b-9ff0-a408a54ab4f7')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.95, 0.45, 0.91, 0.85, 6.612353803, 9.052871228, 9.470146398, 5.029638774, 1.424274833, 0.071418042, 0.011671845, 0.305071794, 1.062878498, 95.8, 0.065, 1, '23b2d944-5ae2-4a7b-9ff0-a408a54ab4f7')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ukraina Country Average', N'b00d2f66-c397-4f50-b4f2-2eb67fadde13', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'23b2d944-5ae2-4a7b-9ff0-a408a54ab4f7', 1, '141628a8-97c0-43e9-992b-0628c1f577bf')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.95,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=6.612353803,SkilledLaborCost=9.052871228,ForemanCost=9.470146398,TechnicianCost=5.029638774,EngineerCost=1.424274833,EnergyCost=0.071418042,AirCost=0.011671845,WaterCost=0.305071794,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=95.8,InterestRate=0.065,IsScrambled=1 WHERE [Guid] = '23b2d944-5ae2-4a7b-9ff0-a408a54ab4f7'
   UPDATE dbo.Countries SET Name=N'Ukraina Country Average',CurrencyGuid=N'b00d2f66-c397-4f50-b4f2-2eb67fadde13',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '141628a8-97c0-43e9-992b-0628c1f577bf'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e67d911e-6931-4ce0-8a6e-c74b030f0281')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.95, 0.45, 0.91, 0.85, 9.034975899, 9.668411505, 9.737188653, 5.606588711, 8.745970941, 0.037095126, 0.002860243, 0.409824346, 1.062878498, 96.45664298, 0.065, 1, 'e67d911e-6931-4ce0-8a6e-c74b030f0281')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Ukraine Kiew', N'27274504-9de6-48ce-acd2-2c97c853e02a', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'e67d911e-6931-4ce0-8a6e-c74b030f0281', 1, '7a6ebc59-272b-457a-bd52-a5d23f1f975e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.95,ShiftCharge2ShiftModel=0.45,ShiftCharge3ShiftModel=0.91,LaborAvailability=0.85,UnskilledLaborCost=9.034975899,SkilledLaborCost=9.668411505,ForemanCost=9.737188653,TechnicianCost=5.606588711,EngineerCost=8.745970941,EnergyCost=0.037095126,AirCost=0.002860243,WaterCost=0.409824346,ProductionAreaRentalCost=1.062878498,OfficeAreaRentalCost=96.45664298,InterestRate=0.065,IsScrambled=1 WHERE [Guid] = 'e67d911e-6931-4ce0-8a6e-c74b030f0281'
   UPDATE dbo.Countries SET Name=N'Ukraine Kiew',CurrencyGuid=N'27274504-9de6-48ce-acd2-2c97c853e02a',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7a6ebc59-272b-457a-bd52-a5d23f1f975e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6498a9dd-e9b6-4262-870b-3072f1bdf07a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.250231194, 60.51897647, 26.85841764, 76.57194992, 95.13996599, 0.086312198, 0.007477676, 0.479440634, 2.301429603, 26.83429863, 0.0025, 1, '6498a9dd-e9b6-4262-870b-3072f1bdf07a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'6498a9dd-e9b6-4262-870b-3072f1bdf07a', 1, '3d4208cd-0789-41a1-a2cb-deadf590dbac')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.250231194,SkilledLaborCost=60.51897647,ForemanCost=26.85841764,TechnicianCost=76.57194992,EngineerCost=95.13996599,EnergyCost=0.086312198,AirCost=0.007477676,WaterCost=0.479440634,ProductionAreaRentalCost=2.301429603,OfficeAreaRentalCost=26.83429863,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '6498a9dd-e9b6-4262-870b-3072f1bdf07a'
   UPDATE dbo.Countries SET Name=N'USA',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '3d4208cd-0789-41a1-a2cb-deadf590dbac'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1df71fe9-031f-441d-a267-097e902f7cbf')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 4.41828207, 4.588536741, 86.32958609, 76.75581485, 79.77162141, 0.015984071, 0.007477676, 0.426660824, 1.227538893, 7.209468664, 0.0025, 1, '1df71fe9-031f-441d-a267-097e902f7cbf')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Alabama', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'1df71fe9-031f-441d-a267-097e902f7cbf', 1, 'c6193c3a-09fc-44cb-b39f-f16a7a774bba')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=4.41828207,SkilledLaborCost=4.588536741,ForemanCost=86.32958609,TechnicianCost=76.75581485,EngineerCost=79.77162141,EnergyCost=0.015984071,AirCost=0.007477676,WaterCost=0.426660824,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '1df71fe9-031f-441d-a267-097e902f7cbf'
   UPDATE dbo.Countries SET Name=N'USA Alabama',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c6193c3a-09fc-44cb-b39f-f16a7a774bba'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '871f930c-25dc-4415-9a1b-1907061ced1e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 66.30743046, 96.5584436, 36.43291468, 59.13817139, 25.8765134, 0.758220406, 0.007477676, 6.742988889, 1.227538893, 7.209468664, 0.0025, 1, '871f930c-25dc-4415-9a1b-1907061ced1e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Alaska', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'871f930c-25dc-4415-9a1b-1907061ced1e', 1, 'bf7a295b-a61f-40cc-bba7-95d797328cb3')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=66.30743046,SkilledLaborCost=96.5584436,ForemanCost=36.43291468,TechnicianCost=59.13817139,EngineerCost=25.8765134,EnergyCost=0.758220406,AirCost=0.007477676,WaterCost=6.742988889,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '871f930c-25dc-4415-9a1b-1907061ced1e'
   UPDATE dbo.Countries SET Name=N'USA Alaska',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bf7a295b-a61f-40cc-bba7-95d797328cb3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c4b3541c-5bc3-426a-960b-5d4fac4fcc3e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.215352418, 7.945481531, 86.90714835, 46.50319183, 65.8254882, 0.087299098, 0.007477676, 0.724042407, 1.961150539, 56.61077878, 0.0025, 1, 'c4b3541c-5bc3-426a-960b-5d4fac4fcc3e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Arizona', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'c4b3541c-5bc3-426a-960b-5d4fac4fcc3e', 1, '9120fa6b-0aaa-4292-a1cb-b96c7fe83a93')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.215352418,SkilledLaborCost=7.945481531,ForemanCost=86.90714835,TechnicianCost=46.50319183,EngineerCost=65.8254882,EnergyCost=0.087299098,AirCost=0.007477676,WaterCost=0.724042407,ProductionAreaRentalCost=1.961150539,OfficeAreaRentalCost=56.61077878,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'c4b3541c-5bc3-426a-960b-5d4fac4fcc3e'
   UPDATE dbo.Countries SET Name=N'USA Arizona',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9120fa6b-0aaa-4292-a1cb-b96c7fe83a93'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'b4a53409-6156-4d1e-8dad-c6d9803e203e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.500991525, 3.500991525, 16.21561784, 76.83257222, 89.41126309, 0.058283281, 0.007477676, 0.516150068, 1.227538893, 7.209468664, 0.0025, 1, 'b4a53409-6156-4d1e-8dad-c6d9803e203e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Arkansas', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'b4a53409-6156-4d1e-8dad-c6d9803e203e', 1, 'd34eab73-3601-4432-8919-4d05377889f4')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.500991525,SkilledLaborCost=3.500991525,ForemanCost=16.21561784,TechnicianCost=76.83257222,EngineerCost=89.41126309,EnergyCost=0.058283281,AirCost=0.007477676,WaterCost=0.516150068,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'b4a53409-6156-4d1e-8dad-c6d9803e203e'
   UPDATE dbo.Countries SET Name=N'USA Arkansas',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd34eab73-3601-4432-8919-4d05377889f4'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a38392f5-e22a-4398-bb28-e981b3142e0c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.586326382, 60.22104412, 26.44741783, 59.99280411, 55.25765211, 0.024847373, 0.007477676, 0.138722688, 1.265822611, 90.73527783, 0.0025, 1, 'a38392f5-e22a-4398-bb28-e981b3142e0c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Carlifornia', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'a38392f5-e22a-4398-bb28-e981b3142e0c', 1, '16142132-a47f-4cbe-883c-00e417430a33')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.586326382,SkilledLaborCost=60.22104412,ForemanCost=26.44741783,TechnicianCost=59.99280411,EngineerCost=55.25765211,EnergyCost=0.024847373,AirCost=0.007477676,WaterCost=0.138722688,ProductionAreaRentalCost=1.265822611,OfficeAreaRentalCost=90.73527783,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a38392f5-e22a-4398-bb28-e981b3142e0c'
   UPDATE dbo.Countries SET Name=N'USA Carlifornia',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '16142132-a47f-4cbe-883c-00e417430a33'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'bc4a67ff-61c0-4dc1-aaa0-1cf5b25411e1')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.816597959, 7.352833664, 26.76131777, 99.26384615, 50.88804842, 0.099040488, 0.007477676, 0.377946385, 5.905575051, 16.87908944, 0.0025, 1, 'bc4a67ff-61c0-4dc1-aaa0-1cf5b25411e1')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Colorado', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'bc4a67ff-61c0-4dc1-aaa0-1cf5b25411e1', 1, '1d61fa58-29b8-4191-aa6a-fd9f257e7128')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.816597959,SkilledLaborCost=7.352833664,ForemanCost=26.76131777,TechnicianCost=99.26384615,EngineerCost=50.88804842,EnergyCost=0.099040488,AirCost=0.007477676,WaterCost=0.377946385,ProductionAreaRentalCost=5.905575051,OfficeAreaRentalCost=16.87908944,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'bc4a67ff-61c0-4dc1-aaa0-1cf5b25411e1'
   UPDATE dbo.Countries SET Name=N'USA Colorado',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '1d61fa58-29b8-4191-aa6a-fd9f257e7128'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '69d79fed-40f8-4716-9b62-0dd310e8bd1c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 16.38569879, 36.37360974, 55.98686701, 45.42448142, 71.73462497, 0.023207535, 0.007477676, 0.128109017, 1.227538893, 19.1144126, 0.0025, 1, '69d79fed-40f8-4716-9b62-0dd310e8bd1c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Columbia', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'69d79fed-40f8-4716-9b62-0dd310e8bd1c', 1, '90ff56cc-0833-40ba-a7c5-ee94128bbb4f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=16.38569879,SkilledLaborCost=36.37360974,ForemanCost=55.98686701,TechnicianCost=45.42448142,EngineerCost=71.73462497,EnergyCost=0.023207535,AirCost=0.007477676,WaterCost=0.128109017,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=19.1144126,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '69d79fed-40f8-4716-9b62-0dd310e8bd1c'
   UPDATE dbo.Countries SET Name=N'USA Columbia',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '90ff56cc-0833-40ba-a7c5-ee94128bbb4f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e647d972-e31c-4195-b7aa-b700b5327042')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.99279035, 7.223761927, 36.20931378, 59.86329671, 79.48062155, 0.028643957, 0.007477676, 6.076476852, 1.227538893, 99.7716771, 0.0025, 1, 'e647d972-e31c-4195-b7aa-b700b5327042')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Connecticut', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'e647d972-e31c-4195-b7aa-b700b5327042', 1, '9a2471c9-4423-4f9c-8051-81f4b8b8d2d8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.99279035,SkilledLaborCost=7.223761927,ForemanCost=36.20931378,TechnicianCost=59.86329671,EngineerCost=79.48062155,EnergyCost=0.028643957,AirCost=0.007477676,WaterCost=6.076476852,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=99.7716771,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'e647d972-e31c-4195-b7aa-b700b5327042'
   UPDATE dbo.Countries SET Name=N'USA Connecticut',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9a2471c9-4423-4f9c-8051-81f4b8b8d2d8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '91a9b09b-3784-41b8-8889-c9db3f53965a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.794314704, 7.212056456, 46.77324132, 69.69494625, 50.23911072, 0.07760052, 0.007477676, 0.912660462, 1.227538893, 86.93197662, 0.0025, 1, '91a9b09b-3784-41b8-8889-c9db3f53965a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Delaware', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'91a9b09b-3784-41b8-8889-c9db3f53965a', 1, '421e4a4a-7ec9-4d6c-b689-5a36f77851b8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.794314704,SkilledLaborCost=7.212056456,ForemanCost=46.77324132,TechnicianCost=69.69494625,EngineerCost=50.23911072,EnergyCost=0.07760052,AirCost=0.007477676,WaterCost=0.912660462,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=86.93197662,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '91a9b09b-3784-41b8-8889-c9db3f53965a'
   UPDATE dbo.Countries SET Name=N'USA Delaware',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '421e4a4a-7ec9-4d6c-b689-5a36f77851b8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '354e9fb9-a030-48ba-804e-2d4d02190037')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.72183654, 3.562220244, 26.47176146, 90.21226649, 39.09690942, 0.081904448, 0.007477676, 0.519404355, 8.070807495, 26.65327306, 0.0025, 1, '354e9fb9-a030-48ba-804e-2d4d02190037')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Florida', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'354e9fb9-a030-48ba-804e-2d4d02190037', 1, '0502bd1e-6ce9-4daf-a6da-c9c12fd51f4d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.72183654,SkilledLaborCost=3.562220244,ForemanCost=26.47176146,TechnicianCost=90.21226649,EngineerCost=39.09690942,EnergyCost=0.081904448,AirCost=0.007477676,WaterCost=0.519404355,ProductionAreaRentalCost=8.070807495,OfficeAreaRentalCost=26.65327306,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '354e9fb9-a030-48ba-804e-2d4d02190037'
   UPDATE dbo.Countries SET Name=N'USA Florida',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0502bd1e-6ce9-4daf-a6da-c9c12fd51f4d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd8573c22-9d02-4ee7-b46f-92ebad45c2d6')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.044987046, 3.377678214, 26.75725472, 69.6021259, 49.25637914, 0.052899341, 0.007477676, 0.512639008, 9.575726981, 56.13417679, 0.0025, 1, 'd8573c22-9d02-4ee7-b46f-92ebad45c2d6')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Georgia', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'd8573c22-9d02-4ee7-b46f-92ebad45c2d6', 1, 'ea63b329-d2e1-4454-8e31-461e50040e94')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.044987046,SkilledLaborCost=3.377678214,ForemanCost=26.75725472,TechnicianCost=69.6021259,EngineerCost=49.25637914,EnergyCost=0.052899341,AirCost=0.007477676,WaterCost=0.512639008,ProductionAreaRentalCost=9.575726981,OfficeAreaRentalCost=56.13417679,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'd8573c22-9d02-4ee7-b46f-92ebad45c2d6'
   UPDATE dbo.Countries SET Name=N'USA Georgia',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'ea63b329-d2e1-4454-8e31-461e50040e94'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'bd670835-0424-4a56-9a13-cdeeaee74088')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.031635561, 60.47283041, 26.83429863, 19.6164749, 50.06429896, 0.883720969, 0.007477676, 0.10517503, 1.227538893, 7.209468664, 0.0025, 1, 'bd670835-0424-4a56-9a13-cdeeaee74088')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Hawai', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'bd670835-0424-4a56-9a13-cdeeaee74088', 1, 'c979d503-9570-470a-b1d0-212af58c7131')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.031635561,SkilledLaborCost=60.47283041,ForemanCost=26.83429863,TechnicianCost=19.6164749,EngineerCost=50.06429896,EnergyCost=0.883720969,AirCost=0.007477676,WaterCost=0.10517503,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'bd670835-0424-4a56-9a13-cdeeaee74088'
   UPDATE dbo.Countries SET Name=N'USA Hawai',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c979d503-9570-470a-b1d0-212af58c7131'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ac5ee93b-3cee-4c52-aaed-d098d3689051')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.872132315, 3.191291508, 16.44875776, 69.69965588, 50.46891056, 0.060320431, 0.007477676, 0.151216635, 1.227538893, 7.209468664, 0.0025, 1, 'ac5ee93b-3cee-4c52-aaed-d098d3689051')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Idaho', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'ac5ee93b-3cee-4c52-aaed-d098d3689051', 1, '101b85b6-db27-465e-958a-1a1d1e2680e9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.872132315,SkilledLaborCost=3.191291508,ForemanCost=16.44875776,TechnicianCost=69.69965588,EngineerCost=50.46891056,EnergyCost=0.060320431,AirCost=0.007477676,WaterCost=0.151216635,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'ac5ee93b-3cee-4c52-aaed-d098d3689051'
   UPDATE dbo.Countries SET Name=N'USA Idaho',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '101b85b6-db27-465e-958a-1a1d1e2680e9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ab0f298a-77c9-4a49-bfd9-e2a21c5868ed')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.287939027, 7.807565017, 26.41914113, 90.50955168, 95.6227789, 0.040707581, 0.007477676, 0.985312277, 9.327073417, 46.6581669, 0.0025, 1, 'ab0f298a-77c9-4a49-bfd9-e2a21c5868ed')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Illinois', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'ab0f298a-77c9-4a49-bfd9-e2a21c5868ed', 1, '9610d666-284d-48ad-83e0-748c49e5161e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.287939027,SkilledLaborCost=7.807565017,ForemanCost=26.41914113,TechnicianCost=90.50955168,EngineerCost=95.6227789,EnergyCost=0.040707581,AirCost=0.007477676,WaterCost=0.985312277,ProductionAreaRentalCost=9.327073417,OfficeAreaRentalCost=46.6581669,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'ab0f298a-77c9-4a49-bfd9-e2a21c5868ed'
   UPDATE dbo.Countries SET Name=N'USA Illinois',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9610d666-284d-48ad-83e0-748c49e5161e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8c9d332f-1c4b-4e82-bcc7-d9c701498e4b')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.733186359, 66.03823084, 26.73677701, 90.58179577, 29.86457915, 0.041860071, 0.007477676, 0.312690444, 1.227538893, 66.70214799, 0.0025, 1, '8c9d332f-1c4b-4e82-bcc7-d9c701498e4b')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Indiana', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'8c9d332f-1c4b-4e82-bcc7-d9c701498e4b', 1, '77bfaf4d-ef21-4985-ac2a-3340b90a63d7')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.733186359,SkilledLaborCost=66.03823084,ForemanCost=26.73677701,TechnicianCost=90.58179577,EngineerCost=29.86457915,EnergyCost=0.041860071,AirCost=0.007477676,WaterCost=0.312690444,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=66.70214799,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '8c9d332f-1c4b-4e82-bcc7-d9c701498e4b'
   UPDATE dbo.Countries SET Name=N'USA Indiana',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '77bfaf4d-ef21-4985-ac2a-3340b90a63d7'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'dadfb139-86b1-46d9-a89f-6ef64e76d4eb')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 60.80826124, 16.40165116, 89.93443065, 39.51417708, 45.85446844, 0.091018211, 0.007477676, 0.708382358, 1.227538893, 7.209468664, 0.0025, 1, 'dadfb139-86b1-46d9-a89f-6ef64e76d4eb')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Iowa', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'dadfb139-86b1-46d9-a89f-6ef64e76d4eb', 1, '04451783-58fd-4636-879f-7d55a2668781')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=60.80826124,SkilledLaborCost=16.40165116,ForemanCost=89.93443065,TechnicianCost=39.51417708,EngineerCost=45.85446844,EnergyCost=0.091018211,AirCost=0.007477676,WaterCost=0.708382358,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'dadfb139-86b1-46d9-a89f-6ef64e76d4eb'
   UPDATE dbo.Countries SET Name=N'USA Iowa',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '04451783-58fd-4636-879f-7d55a2668781'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1b7bc900-258c-403a-ab05-8b652004aadc')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.805795624, 86.79446162, 86.64636184, 36.68827407, 29.13162256, 0.056604858, 0.007477676, 0.380978021, 1.227538893, 7.209468664, 0.0025, 1, '1b7bc900-258c-403a-ab05-8b652004aadc')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Kansas', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'1b7bc900-258c-403a-ab05-8b652004aadc', 1, 'ec4abc33-be57-44d9-9edb-d293e6bef3d0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.805795624,SkilledLaborCost=86.79446162,ForemanCost=86.64636184,TechnicianCost=36.68827407,EngineerCost=29.13162256,EnergyCost=0.056604858,AirCost=0.007477676,WaterCost=0.380978021,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '1b7bc900-258c-403a-ab05-8b652004aadc'
   UPDATE dbo.Countries SET Name=N'USA Kansas',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'ec4abc33-be57-44d9-9edb-d293e6bef3d0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'd10c8af6-83e9-4de2-b1e0-3651e6ddfc4e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.715580285, 3.787472324, 86.6458615, 46.13976544, 49.17203769, 0.097635491, 0.007477676, 0.58130038, 1.227538893, 7.209468664, 0.0025, 1, 'd10c8af6-83e9-4de2-b1e0-3651e6ddfc4e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Kentucky', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'd10c8af6-83e9-4de2-b1e0-3651e6ddfc4e', 1, 'bd9867c8-c54f-4905-9591-5bd752e8b87c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.715580285,SkilledLaborCost=3.787472324,ForemanCost=86.6458615,TechnicianCost=46.13976544,EngineerCost=49.17203769,EnergyCost=0.097635491,AirCost=0.007477676,WaterCost=0.58130038,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'd10c8af6-83e9-4de2-b1e0-3651e6ddfc4e'
   UPDATE dbo.Countries SET Name=N'USA Kentucky',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bd9867c8-c54f-4905-9591-5bd752e8b87c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3901732f-d456-424a-8da4-605881aedd85')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.761278233, 60.42766281, 26.18866977, 90.13211263, 65.76793489, 0.027978511, 0.007477676, 0.638494204, 1.227538893, 7.209468664, 0.0025, 1, '3901732f-d456-424a-8da4-605881aedd85')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Lousiana', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'3901732f-d456-424a-8da4-605881aedd85', 1, 'a3044cc2-8041-4a43-9d25-a3f66cc5444d')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.761278233,SkilledLaborCost=60.42766281,ForemanCost=26.18866977,TechnicianCost=90.13211263,EngineerCost=65.76793489,EnergyCost=0.027978511,AirCost=0.007477676,WaterCost=0.638494204,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '3901732f-d456-424a-8da4-605881aedd85'
   UPDATE dbo.Countries SET Name=N'USA Lousiana',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'a3044cc2-8041-4a43-9d25-a3f66cc5444d'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '880a248f-3ea5-45a8-b4b6-63f3ac5aa316')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.790619779, 3.691660872, 86.98287739, 19.39198857, 49.17675661, 0.046967688, 0.007477676, 0.852666602, 1.227538893, 7.209468664, 0.0025, 1, '880a248f-3ea5-45a8-b4b6-63f3ac5aa316')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Maine', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'880a248f-3ea5-45a8-b4b6-63f3ac5aa316', 1, '7cc6cbe6-4f3c-4e34-bd8c-35cfcd40cc77')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.790619779,SkilledLaborCost=3.691660872,ForemanCost=86.98287739,TechnicianCost=19.39198857,EngineerCost=49.17675661,EnergyCost=0.046967688,AirCost=0.007477676,WaterCost=0.852666602,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '880a248f-3ea5-45a8-b4b6-63f3ac5aa316'
   UPDATE dbo.Countries SET Name=N'USA Maine',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '7cc6cbe6-4f3c-4e34-bd8c-35cfcd40cc77'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6b9eac1e-dbd9-4082-baf7-e2d99726016f')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 60.70187969, 66.63809468, 46.67828097, 59.50600225, 95.36197378, 0.034504762, 0.007477676, 6.831945655, 5.749717142, 16.65858935, 0.0025, 1, '6b9eac1e-dbd9-4082-baf7-e2d99726016f')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Maryland', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'6b9eac1e-dbd9-4082-baf7-e2d99726016f', 1, '868ba3b2-06c6-405b-a1f5-2aef64aabde8')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=60.70187969,SkilledLaborCost=66.63809468,ForemanCost=46.67828097,TechnicianCost=59.50600225,EngineerCost=95.36197378,EnergyCost=0.034504762,AirCost=0.007477676,WaterCost=6.831945655,ProductionAreaRentalCost=5.749717142,OfficeAreaRentalCost=16.65858935,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '6b9eac1e-dbd9-4082-baf7-e2d99726016f'
   UPDATE dbo.Countries SET Name=N'USA Maryland',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '868ba3b2-06c6-405b-a1f5-2aef64aabde8'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '72438f66-346a-4d05-ab86-f0edb27b6070')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.442744732, 60.52012239, 46.59318025, 69.17321268, 95.40940939, 0.120625006, 0.007477676, 6.160599386, 1.632079643, 99.05124125, 0.0025, 1, '72438f66-346a-4d05-ab86-f0edb27b6070')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Massachusetts', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'72438f66-346a-4d05-ab86-f0edb27b6070', 1, '8ad38257-98b2-49c1-8a7d-6be94db860e5')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.442744732,SkilledLaborCost=60.52012239,ForemanCost=46.59318025,TechnicianCost=69.17321268,EngineerCost=95.40940939,EnergyCost=0.120625006,AirCost=0.007477676,WaterCost=6.160599386,ProductionAreaRentalCost=1.632079643,OfficeAreaRentalCost=99.05124125,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '72438f66-346a-4d05-ab86-f0edb27b6070'
   UPDATE dbo.Countries SET Name=N'USA Massachusetts',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '8ad38257-98b2-49c1-8a7d-6be94db860e5'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '625313ab-d59c-4ca4-8502-ea44c8f23ea0')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.136463069, 66.3845007, 26.26091352, 36.16006646, 50.14846828, 0.013499318, 0.007477676, 0.487064578, 9.140757037, 96.6360619, 0.0025, 1, '625313ab-d59c-4ca4-8502-ea44c8f23ea0')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Michigan', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'625313ab-d59c-4ca4-8502-ea44c8f23ea0', 1, '5738e5df-fe8f-4d6d-9149-f2b13fab5489')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.136463069,SkilledLaborCost=66.3845007,ForemanCost=26.26091352,TechnicianCost=36.16006646,EngineerCost=50.14846828,EnergyCost=0.013499318,AirCost=0.007477676,WaterCost=0.487064578,ProductionAreaRentalCost=9.140757037,OfficeAreaRentalCost=96.6360619,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '625313ab-d59c-4ca4-8502-ea44c8f23ea0'
   UPDATE dbo.Countries SET Name=N'USA Michigan',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '5738e5df-fe8f-4d6d-9149-f2b13fab5489'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '14eb8f3f-6dd8-46ce-a524-945a90a5c9aa')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.92665609, 60.14922237, 26.4206964, 90.91949579, 49.85228157, 0.024996188, 0.007477676, 0.888322505, 1.227538893, 7.209468664, 0.0025, 1, '14eb8f3f-6dd8-46ce-a524-945a90a5c9aa')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Minnesota', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'14eb8f3f-6dd8-46ce-a524-945a90a5c9aa', 1, '6093a10d-6b84-4086-b865-8ad14a5b48c3')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.92665609,SkilledLaborCost=60.14922237,ForemanCost=26.4206964,TechnicianCost=90.91949579,EngineerCost=49.85228157,EnergyCost=0.024996188,AirCost=0.007477676,WaterCost=0.888322505,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '14eb8f3f-6dd8-46ce-a524-945a90a5c9aa'
   UPDATE dbo.Countries SET Name=N'USA Minnesota',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6093a10d-6b84-4086-b865-8ad14a5b48c3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0368dccf-e074-400b-9533-86e5cdda30d7')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.007274859, 3.097372821, 86.53572105, 46.67828097, 89.202283, 0.066451308, 0.007477676, 6.221606776, 1.227538893, 7.209468664, 0.0025, 1, '0368dccf-e074-400b-9533-86e5cdda30d7')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Mississippi', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'0368dccf-e074-400b-9533-86e5cdda30d7', 1, 'f1e29101-b187-454e-b0ec-4e564d637e3f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.007274859,SkilledLaborCost=3.097372821,ForemanCost=86.53572105,TechnicianCost=46.67828097,EngineerCost=89.202283,EnergyCost=0.066451308,AirCost=0.007477676,WaterCost=6.221606776,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '0368dccf-e074-400b-9533-86e5cdda30d7'
   UPDATE dbo.Countries SET Name=N'USA Mississippi',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f1e29101-b187-454e-b0ec-4e564d637e3f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0496ae05-4a53-4d5a-95e5-439d52dfbb8a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.381071123, 60.14922237, 86.50126176, 76.26108379, 49.95430824, 0.042251171, 0.007477676, 0.177918042, 1.227538893, 66.39440954, 0.0025, 1, '0496ae05-4a53-4d5a-95e5-439d52dfbb8a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Missouri', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'0496ae05-4a53-4d5a-95e5-439d52dfbb8a', 1, '4d645a3b-80c2-4b35-bdb2-0075d76f9d38')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.381071123,SkilledLaborCost=60.14922237,ForemanCost=86.50126176,TechnicianCost=76.26108379,EngineerCost=49.95430824,EnergyCost=0.042251171,AirCost=0.007477676,WaterCost=0.177918042,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=66.39440954,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '0496ae05-4a53-4d5a-95e5-439d52dfbb8a'
   UPDATE dbo.Countries SET Name=N'USA Missouri',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4d645a3b-80c2-4b35-bdb2-0075d76f9d38'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '74439c39-752c-4591-b852-3b10836e8fda')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.920805022, 7.606776352, 26.60113294, 76.91906067, 5.022947003, 0.097120735, 0.007477676, 0.553388691, 1.227538893, 7.209468664, 0.0025, 1, '74439c39-752c-4591-b852-3b10836e8fda')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Montana', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'74439c39-752c-4591-b852-3b10836e8fda', 1, '4f9bb1ee-c1ea-41d2-a739-59d120d60c43')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.920805022,SkilledLaborCost=7.606776352,ForemanCost=26.60113294,TechnicianCost=76.91906067,EngineerCost=5.022947003,EnergyCost=0.097120735,AirCost=0.007477676,WaterCost=0.553388691,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '74439c39-752c-4591-b852-3b10836e8fda'
   UPDATE dbo.Countries SET Name=N'USA Montana',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4f9bb1ee-c1ea-41d2-a739-59d120d60c43'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'f6c7ada7-e866-4edd-915d-2a17574cf023')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.886538221, 7.546486447, 46.05404654, 90.91949579, 29.61034745, 0.025555288, 0.007477676, 0.372454715, 1.227538893, 7.209468664, 0.0025, 1, 'f6c7ada7-e866-4edd-915d-2a17574cf023')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Nebraska', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'f6c7ada7-e866-4edd-915d-2a17574cf023', 1, '259ef9ae-836d-4b21-b2cf-5c204366efb0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.886538221,SkilledLaborCost=7.546486447,ForemanCost=46.05404654,TechnicianCost=90.91949579,EngineerCost=29.61034745,EnergyCost=0.025555288,AirCost=0.007477676,WaterCost=0.372454715,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'f6c7ada7-e866-4edd-915d-2a17574cf023'
   UPDATE dbo.Countries SET Name=N'USA Nebraska',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '259ef9ae-836d-4b21-b2cf-5c204366efb0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '7bd71931-658f-46cb-a21d-af2e69740259')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.72183654, 60.84995099, 46.94897412, 76.75309957, 39.82512864, 0.013623192, 0.007477676, 0.851351714, 1.088033081, 96.51613658, 0.0025, 1, '7bd71931-658f-46cb-a21d-af2e69740259')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Nevada', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'7bd71931-658f-46cb-a21d-af2e69740259', 1, 'a0d92d38-dd66-453d-9d4b-f84d92f4af47')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.72183654,SkilledLaborCost=60.84995099,ForemanCost=46.94897412,TechnicianCost=76.75309957,EngineerCost=39.82512864,EnergyCost=0.013623192,AirCost=0.007477676,WaterCost=0.851351714,ProductionAreaRentalCost=1.088033081,OfficeAreaRentalCost=96.51613658,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '7bd71931-658f-46cb-a21d-af2e69740259'
   UPDATE dbo.Countries SET Name=N'USA Nevada',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'a0d92d38-dd66-453d-9d4b-f84d92f4af47'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '86f0005d-351e-4d8b-90da-857f4ed55e09')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 60.11046475, 60.78626466, 26.52316545, 36.6209576, 39.21500511, 0.026656093, 0.007477676, 6.954125036, 1.227538893, 7.209468664, 0.0025, 1, '86f0005d-351e-4d8b-90da-857f4ed55e09')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA New Hampshire', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'86f0005d-351e-4d8b-90da-857f4ed55e09', 1, '51347810-1431-4a41-b5cd-1145c4e87ac6')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=60.11046475,SkilledLaborCost=60.78626466,ForemanCost=26.52316545,TechnicianCost=36.6209576,EngineerCost=39.21500511,EnergyCost=0.026656093,AirCost=0.007477676,WaterCost=6.954125036,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '86f0005d-351e-4d8b-90da-857f4ed55e09'
   UPDATE dbo.Countries SET Name=N'USA New Hampshire',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '51347810-1431-4a41-b5cd-1145c4e87ac6'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'fa1f6a26-5475-42f3-a1ee-9b268ff5b03a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.75925916, 7.953885602, 36.95067293, 59.29858262, 65.59341063, 0.061064793, 0.007477676, 0.235857523, 5.536038553, 16.8223133, 0.0025, 1, 'fa1f6a26-5475-42f3-a1ee-9b268ff5b03a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA New Jersey', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'fa1f6a26-5475-42f3-a1ee-9b268ff5b03a', 1, 'f0a6ac90-1894-4790-96ec-3b92719affa2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.75925916,SkilledLaborCost=7.953885602,ForemanCost=36.95067293,TechnicianCost=59.29858262,EngineerCost=65.59341063,EnergyCost=0.061064793,AirCost=0.007477676,WaterCost=0.235857523,ProductionAreaRentalCost=5.536038553,OfficeAreaRentalCost=16.8223133,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'fa1f6a26-5475-42f3-a1ee-9b268ff5b03a'
   UPDATE dbo.Countries SET Name=N'USA New Jersey',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f0a6ac90-1894-4790-96ec-3b92719affa2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '445d1e84-a1e8-4832-a04e-d9edd6ba7528')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.433754791, 60.23146458, 16.63143325, 76.9328782, 95.87147758, 0.095331521, 0.007477676, 0.539147395, 1.227538893, 7.209468664, 0.0025, 1, '445d1e84-a1e8-4832-a04e-d9edd6ba7528')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA New Mexiko', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'445d1e84-a1e8-4832-a04e-d9edd6ba7528', 1, '6f8670c4-e84d-4229-a7eb-6cc6684178ee')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.433754791,SkilledLaborCost=60.23146458,ForemanCost=16.63143325,TechnicianCost=76.9328782,EngineerCost=95.87147758,EnergyCost=0.095331521,AirCost=0.007477676,WaterCost=0.539147395,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '445d1e84-a1e8-4832-a04e-d9edd6ba7528'
   UPDATE dbo.Countries SET Name=N'USA New Mexiko',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6f8670c4-e84d-4229-a7eb-6cc6684178ee'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1b62499e-57c4-455a-8321-2a3b7ba69351')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.62682427, 3.736412493, 36.6209576, 76.51924516, 79.87094271, 0.046495025, 0.007477676, 0.673086933, 8.330364751, 43.42222222, 0.0025, 1, '1b62499e-57c4-455a-8321-2a3b7ba69351')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA New York', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'1b62499e-57c4-455a-8321-2a3b7ba69351', 1, '8dea2b05-8c3a-442c-b2c7-1b5f05b211ce')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.62682427,SkilledLaborCost=3.736412493,ForemanCost=36.6209576,TechnicianCost=76.51924516,EngineerCost=79.87094271,EnergyCost=0.046495025,AirCost=0.007477676,WaterCost=0.673086933,ProductionAreaRentalCost=8.330364751,OfficeAreaRentalCost=43.42222222,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '1b62499e-57c4-455a-8321-2a3b7ba69351'
   UPDATE dbo.Countries SET Name=N'USA New York',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '8dea2b05-8c3a-442c-b2c7-1b5f05b211ce'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '8ff50ca4-8a3e-42d8-9340-2753c2227179')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.65786345, 66.92828119, 86.28090682, 76.63481471, 49.23360129, 0.091483131, 0.007477676, 0.622978424, 1.227538893, 56.10825226, 0.0025, 1, '8ff50ca4-8a3e-42d8-9340-2753c2227179')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA North Carolina', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'8ff50ca4-8a3e-42d8-9340-2753c2227179', 1, '0c498b3b-3c3a-45ce-a285-d32bb82ca0f9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.65786345,SkilledLaborCost=66.92828119,ForemanCost=86.28090682,TechnicianCost=76.63481471,EngineerCost=49.23360129,EnergyCost=0.091483131,AirCost=0.007477676,WaterCost=0.622978424,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=56.10825226,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '8ff50ca4-8a3e-42d8-9340-2753c2227179'
   UPDATE dbo.Countries SET Name=N'USA North Carolina',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '0c498b3b-3c3a-45ce-a285-d32bb82ca0f9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c08940ae-892e-4d52-adce-0b03b43aaf3e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.586326382, 7.442744732, 86.25856587, 86.53523432, 29.00407475, 0.082203518, 0.007477676, 0.186821218, 1.227538893, 7.209468664, 0.0025, 1, 'c08940ae-892e-4d52-adce-0b03b43aaf3e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA North Dakota', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'c08940ae-892e-4d52-adce-0b03b43aaf3e', 1, '196eeddf-d15e-4fc1-a26c-cbe7ae8f25b2')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.586326382,SkilledLaborCost=7.442744732,ForemanCost=86.25856587,TechnicianCost=86.53523432,EngineerCost=29.00407475,EnergyCost=0.082203518,AirCost=0.007477676,WaterCost=0.186821218,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'c08940ae-892e-4d52-adce-0b03b43aaf3e'
   UPDATE dbo.Countries SET Name=N'USA North Dakota',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '196eeddf-d15e-4fc1-a26c-cbe7ae8f25b2'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a4e5b1e1-3838-498f-8f50-55566255f1e2')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.060320431, 66.03823084, 86.90767324, 90.91432606, 29.43529732, 0.056420781, 0.007477676, 0.610546307, 1.227538893, 66.59278601, 0.0025, 1, 'a4e5b1e1-3838-498f-8f50-55566255f1e2')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Ohio', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'a4e5b1e1-3838-498f-8f50-55566255f1e2', 1, 'f9b229b8-321b-4122-9bc8-3da08bb694b7')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.060320431,SkilledLaborCost=66.03823084,ForemanCost=86.90767324,TechnicianCost=90.91432606,EngineerCost=29.43529732,EnergyCost=0.056420781,AirCost=0.007477676,WaterCost=0.610546307,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=66.59278601,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a4e5b1e1-3838-498f-8f50-55566255f1e2'
   UPDATE dbo.Countries SET Name=N'USA Ohio',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f9b229b8-321b-4122-9bc8-3da08bb694b7'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'baccdcb9-0931-41e1-8d14-1b5fd01ce36a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.858349093, 7.187423686, 26.96617321, 76.8805142, 49.04664643, 0.040978861, 0.007477676, 0.291711481, 1.227538893, 7.209468664, 0.0025, 1, 'baccdcb9-0931-41e1-8d14-1b5fd01ce36a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Oklahoma', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'baccdcb9-0931-41e1-8d14-1b5fd01ce36a', 1, '059c2c23-76a9-4b60-8784-84ea480b8698')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.858349093,SkilledLaborCost=7.187423686,ForemanCost=26.96617321,TechnicianCost=76.8805142,EngineerCost=49.04664643,EnergyCost=0.040978861,AirCost=0.007477676,WaterCost=0.291711481,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'baccdcb9-0931-41e1-8d14-1b5fd01ce36a'
   UPDATE dbo.Countries SET Name=N'USA Oklahoma',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '059c2c23-76a9-4b60-8784-84ea480b8698'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '3269e336-d07a-4549-8b0f-0c7a2b1a541c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.388932417, 7.187423686, 86.09915253, 76.19921452, 79.09460218, 0.046627251, 0.007477676, 0.716754172, 1.227538893, 56.54563233, 0.0025, 1, '3269e336-d07a-4549-8b0f-0c7a2b1a541c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Oregon', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'3269e336-d07a-4549-8b0f-0c7a2b1a541c', 1, 'c3301dcc-c217-4754-9ea8-05cc36585fe0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.388932417,SkilledLaborCost=7.187423686,ForemanCost=86.09915253,TechnicianCost=76.19921452,EngineerCost=79.09460218,EnergyCost=0.046627251,AirCost=0.007477676,WaterCost=0.716754172,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=56.54563233,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '3269e336-d07a-4549-8b0f-0c7a2b1a541c'
   UPDATE dbo.Countries SET Name=N'USA Oregon',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c3301dcc-c217-4754-9ea8-05cc36585fe0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '89c69ffa-05f5-4547-b132-ed9855a8e8d0')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.231194302, 3.771774082, 36.12973229, 69.11242091, 49.78057603, 0.066640838, 0.007477676, 6.92329797, 5.082004786, 86.96551809, 0.0025, 1, '89c69ffa-05f5-4547-b132-ed9855a8e8d0')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Pennsylvania', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'89c69ffa-05f5-4547-b132-ed9855a8e8d0', 1, '567d1664-a0c0-40c5-b301-908f519d4fa4')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.231194302,SkilledLaborCost=3.771774082,ForemanCost=36.12973229,TechnicianCost=69.11242091,EngineerCost=49.78057603,EnergyCost=0.066640838,AirCost=0.007477676,WaterCost=6.92329797,ProductionAreaRentalCost=5.082004786,OfficeAreaRentalCost=86.96551809,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '89c69ffa-05f5-4547-b132-ed9855a8e8d0'
   UPDATE dbo.Countries SET Name=N'USA Pennsylvania',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '567d1664-a0c0-40c5-b301-908f519d4fa4'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ab02300f-a26f-4782-8f25-4e197b979c57')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 4.288378439, 2.206025501, 66.8379766, 60.41159384, 76.75309957, 0.0358621358621311, 0.007477676, 0.536928046, 1.227538893, 7.209468664, 0.0025, 1, 'ab02300f-a26f-4782-8f25-4e197b979c57')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Puerto Rico', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'ab02300f-a26f-4782-8f25-4e197b979c57', 1, 'cf5c2b8e-0ff0-491b-ab40-bb2d0d956079')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=4.288378439,SkilledLaborCost=2.206025501,ForemanCost=66.8379766,TechnicianCost=60.41159384,EngineerCost=76.75309957,EnergyCost=0.0358621358621311,AirCost=0.007477676,WaterCost=0.536928046,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'ab02300f-a26f-4782-8f25-4e197b979c57'
   UPDATE dbo.Countries SET Name=N'USA Puerto Rico',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'cf5c2b8e-0ff0-491b-ab40-bb2d0d956079'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0b7a4d61-8de9-44d0-90e8-7765c6e1607a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 2.456700287, 60.80059362, 46.02149525, 19.53043919, 95.42605233, 0.047022057, 0.007477676, 0.417279604, 1.227538893, 7.209468664, 0.0025, 1, '0b7a4d61-8de9-44d0-90e8-7765c6e1607a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Rhode Island', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'0b7a4d61-8de9-44d0-90e8-7765c6e1607a', 1, '135917d2-eb9a-492a-a925-383b19c3ec4c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=2.456700287,SkilledLaborCost=60.80059362,ForemanCost=46.02149525,TechnicianCost=19.53043919,EngineerCost=95.42605233,EnergyCost=0.047022057,AirCost=0.007477676,WaterCost=0.417279604,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '0b7a4d61-8de9-44d0-90e8-7765c6e1607a'
   UPDATE dbo.Countries SET Name=N'USA Rhode Island',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '135917d2-eb9a-492a-a925-383b19c3ec4c'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'e9eaa1e1-7529-4b17-a8b7-99fdc35d3624')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.5771116, 3.367335937, 46.07224987, 76.88790729, 50.46060995, 0.060684311, 0.007477676, 0.128109017, 1.227538893, 7.209468664, 0.0025, 1, 'e9eaa1e1-7529-4b17-a8b7-99fdc35d3624')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA South Carolina', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'e9eaa1e1-7529-4b17-a8b7-99fdc35d3624', 1, '664fa0c3-bae5-4f66-9849-224b80d79ac9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.5771116,SkilledLaborCost=3.367335937,ForemanCost=46.07224987,TechnicianCost=76.88790729,EngineerCost=50.46060995,EnergyCost=0.060684311,AirCost=0.007477676,WaterCost=0.128109017,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'e9eaa1e1-7529-4b17-a8b7-99fdc35d3624'
   UPDATE dbo.Countries SET Name=N'USA South Carolina',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '664fa0c3-bae5-4f66-9849-224b80d79ac9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ab66e728-1375-4af7-a2fa-16bf39fd134a')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 60.0981403441436, 3.397310269, 26.03475657, 29.5269866562511, 89.57441156, 0.09203798, 0.007477676, 6.049352316, 1.227538893, 7.209468664, 0.0025, 1, 'ab66e728-1375-4af7-a2fa-16bf39fd134a')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA South Dakota', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'ab66e728-1375-4af7-a2fa-16bf39fd134a', 1, 'c75694a8-5623-4597-ba80-7628d3a86bd3')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=60.0981403441436,SkilledLaborCost=3.397310269,ForemanCost=26.03475657,TechnicianCost=29.5269866562511,EngineerCost=89.57441156,EnergyCost=0.09203798,AirCost=0.007477676,WaterCost=6.049352316,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'ab66e728-1375-4af7-a2fa-16bf39fd134a'
   UPDATE dbo.Countries SET Name=N'USA South Dakota',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'c75694a8-5623-4597-ba80-7628d3a86bd3'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '08f03119-1694-49e2-a5dc-92bb7429547c')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.985321126, 7.052496304, 86.78771093, 76.63481471, 39.44886229, 0.035039818, 0.007477676, 0.147668968, 9.654484205, 96.73853903, 0.0025, 1, '08f03119-1694-49e2-a5dc-92bb7429547c')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Tennesse', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'08f03119-1694-49e2-a5dc-92bb7429547c', 1, 'fcbf2db0-7b5f-4a67-90bc-fff9362fa375')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.985321126,SkilledLaborCost=7.052496304,ForemanCost=86.78771093,TechnicianCost=76.63481471,EngineerCost=39.44886229,EnergyCost=0.035039818,AirCost=0.007477676,WaterCost=0.147668968,ProductionAreaRentalCost=9.654484205,OfficeAreaRentalCost=96.73853903,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '08f03119-1694-49e2-a5dc-92bb7429547c'
   UPDATE dbo.Countries SET Name=N'USA Tennesse',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'fcbf2db0-7b5f-4a67-90bc-fff9362fa375'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'a90bc304-679b-4581-a6ec-9847c915b6f9')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.782809746, 3.18306451, 26.95277718, 99.41067126, 95.28150624, 0.015851981, 0.007477676, 6.050422941, 5.493552878, 86.19026051, 0.0025, 1, 'a90bc304-679b-4581-a6ec-9847c915b6f9')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Texas', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'a90bc304-679b-4581-a6ec-9847c915b6f9', 1, '9367048e-7b54-4b32-b874-0d0d90a85632')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.782809746,SkilledLaborCost=3.18306451,ForemanCost=26.95277718,TechnicianCost=99.41067126,EngineerCost=95.28150624,EnergyCost=0.015851981,AirCost=0.007477676,WaterCost=6.050422941,ProductionAreaRentalCost=5.493552878,OfficeAreaRentalCost=86.19026051,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'a90bc304-679b-4581-a6ec-9847c915b6f9'
   UPDATE dbo.Countries SET Name=N'USA Texas',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '9367048e-7b54-4b32-b874-0d0d90a85632'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'baa8a321-180d-4ba2-86f7-3fe3da1c58d3')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.92665609, 3.362273975, 26.49949887, 59.66145568, 39.60098556, 0.066096041, 0.007477676, 0.919607585, 5.530362946, 56.07160018, 0.0025, 1, 'baa8a321-180d-4ba2-86f7-3fe3da1c58d3')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Utah', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'baa8a321-180d-4ba2-86f7-3fe3da1c58d3', 1, '542a5b6e-a6b6-42d7-a6e1-fc823e5af7b0')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.92665609,SkilledLaborCost=3.362273975,ForemanCost=26.49949887,TechnicianCost=59.66145568,EngineerCost=39.60098556,EnergyCost=0.066096041,AirCost=0.007477676,WaterCost=0.919607585,ProductionAreaRentalCost=5.530362946,OfficeAreaRentalCost=56.07160018,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'baa8a321-180d-4ba2-86f7-3fe3da1c58d3'
   UPDATE dbo.Countries SET Name=N'USA Utah',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '542a5b6e-a6b6-42d7-a6e1-fc823e5af7b0'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '6c700ba7-a006-4510-bd4e-f36f51a96369')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.055852366, 3.766106479, 46.66223291, 36.03260958, 49.95467635, 0.016088184, 0.007477676, 6.858557621, 1.227538893, 7.209468664, 0.0025, 1, '6c700ba7-a006-4510-bd4e-f36f51a96369')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Vermont', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'6c700ba7-a006-4510-bd4e-f36f51a96369', 1, 'd38e04cd-3db5-4cf3-a0f4-4c1135fc4208')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.055852366,SkilledLaborCost=3.766106479,ForemanCost=46.66223291,TechnicianCost=36.03260958,EngineerCost=49.95467635,EnergyCost=0.016088184,AirCost=0.007477676,WaterCost=6.858557621,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '6c700ba7-a006-4510-bd4e-f36f51a96369'
   UPDATE dbo.Countries SET Name=N'USA Vermont',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'd38e04cd-3db5-4cf3-a0f4-4c1135fc4208'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '801a7baf-1efa-4e85-8cef-fee7e9989f41')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 26.9383386345371, 69.4534725044962, 35.4307912594618, 51.7353243930421, 48.250871618028, 0.0358621358621311, 0.007477676, 6.710839829, 1.227538893, 7.209468664, 0.0025, 1, '801a7baf-1efa-4e85-8cef-fee7e9989f41')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Virgin Island', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'801a7baf-1efa-4e85-8cef-fee7e9989f41', 1, '737a0c48-a1d4-4f3b-9647-269d835cbdaf')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=26.9383386345371,SkilledLaborCost=69.4534725044962,ForemanCost=35.4307912594618,TechnicianCost=51.7353243930421,EngineerCost=48.250871618028,EnergyCost=0.0358621358621311,AirCost=0.007477676,WaterCost=6.710839829,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '801a7baf-1efa-4e85-8cef-fee7e9989f41'
   UPDATE dbo.Countries SET Name=N'USA Virgin Island',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '737a0c48-a1d4-4f3b-9647-269d835cbdaf'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '9ad44f62-fe27-420d-b554-18c3b1b90420')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 3.324912432, 60.9203798, 46.83451376, 19.56344012, 95.87147758, 0.023483371, 0.007477676, 0.650665853, 1.227538893, 7.209468664, 0.0025, 1, '9ad44f62-fe27-420d-b554-18c3b1b90420')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Virginia', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'9ad44f62-fe27-420d-b554-18c3b1b90420', 1, 'e662e919-cc73-497a-86c0-f9de14b682d9')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=3.324912432,SkilledLaborCost=60.9203798,ForemanCost=46.83451376,TechnicianCost=19.56344012,EngineerCost=95.87147758,EnergyCost=0.023483371,AirCost=0.007477676,WaterCost=0.650665853,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '9ad44f62-fe27-420d-b554-18c3b1b90420'
   UPDATE dbo.Countries SET Name=N'USA Virginia',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'e662e919-cc73-497a-86c0-f9de14b682d9'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '038db42c-f548-4ee8-b1aa-f236044353f4')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.661685621, 66.54419346, 46.15863831, 19.47609794, 65.48172177, 0.064920765, 0.007477676, 0.902775757, 1.227538893, 36.28010905, 0.0025, 1, '038db42c-f548-4ee8-b1aa-f236044353f4')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Washinton', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'038db42c-f548-4ee8-b1aa-f236044353f4', 1, '61ef4229-15dc-4349-8b3c-811fcaaf5c4e')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.661685621,SkilledLaborCost=66.54419346,ForemanCost=46.15863831,TechnicianCost=19.47609794,EngineerCost=65.48172177,EnergyCost=0.064920765,AirCost=0.007477676,WaterCost=0.902775757,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=36.28010905,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '038db42c-f548-4ee8-b1aa-f236044353f4'
   UPDATE dbo.Countries SET Name=N'USA Washinton',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '61ef4229-15dc-4349-8b3c-811fcaaf5c4e'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'ef50efbd-a689-4417-9d51-c172e90669ed')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 60.60697653, 4.438419013, 86.12352599, 76.54189554, 49.34863771, 0.03935881, 0.007477676, 6.13274793, 1.227538893, 7.209468664, 0.0025, 1, 'ef50efbd-a689-4417-9d51-c172e90669ed')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA West Virginia', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'ef50efbd-a689-4417-9d51-c172e90669ed', 1, '6260a773-a854-49ce-b6c8-508251910053')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=60.60697653,SkilledLaborCost=4.438419013,ForemanCost=86.12352599,TechnicianCost=76.54189554,EngineerCost=49.34863771,EnergyCost=0.03935881,AirCost=0.007477676,WaterCost=6.13274793,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = 'ef50efbd-a689-4417-9d51-c172e90669ed'
   UPDATE dbo.Countries SET Name=N'USA West Virginia',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '6260a773-a854-49ce-b6c8-508251910053'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '0f0a6ecb-3a76-41bb-9879-1f9c1866ecd8')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 7.661685621, 60.08445439, 46.83451376, 46.71216947, 19.70133922, 0.040023328, 0.007477676, 0.78722688, 1.227538893, 7.209468664, 0.0025, 1, '0f0a6ecb-3a76-41bb-9879-1f9c1866ecd8')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Wisconsin', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'0f0a6ecb-3a76-41bb-9879-1f9c1866ecd8', 1, '4eeac7eb-9d8b-4ce5-a703-58ac4a35f125')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=7.661685621,SkilledLaborCost=60.08445439,ForemanCost=46.83451376,TechnicianCost=46.71216947,EngineerCost=19.70133922,EnergyCost=0.040023328,AirCost=0.007477676,WaterCost=0.78722688,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '0f0a6ecb-3a76-41bb-9879-1f9c1866ecd8'
   UPDATE dbo.Countries SET Name=N'USA Wisconsin',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = '4eeac7eb-9d8b-4ce5-a703-58ac4a35f125'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '1f58ed58-d952-465d-96c7-94445d29d7ae')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.61, 0.61, 0.31, 0.776909722222222, 66.33526283, 7.395554333, 26.7201369, 90.08611312, 79.07240424, 0.07684241, 0.007477676, 0.185107324, 1.227538893, 7.209468664, 0.0025, 1, '1f58ed58-d952-465d-96c7-94445d29d7ae')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'USA Wyioming', N'297ac727-8265-429e-81a2-49fdb279ae58', N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7', N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4', N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'4044d191-26d2-4349-b6c9-8dc43d1bc39d', N'1f58ed58-d952-465d-96c7-94445d29d7ae', 1, 'db844f11-5ec4-4af4-9dc0-9aa5ecaea29f')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.61,ShiftCharge2ShiftModel=0.61,ShiftCharge3ShiftModel=0.31,LaborAvailability=0.776909722222222,UnskilledLaborCost=66.33526283,SkilledLaborCost=7.395554333,ForemanCost=26.7201369,TechnicianCost=90.08611312,EngineerCost=79.07240424,EnergyCost=0.07684241,AirCost=0.007477676,WaterCost=0.185107324,ProductionAreaRentalCost=1.227538893,OfficeAreaRentalCost=7.209468664,InterestRate=0.0025,IsScrambled=1 WHERE [Guid] = '1f58ed58-d952-465d-96c7-94445d29d7ae'
   UPDATE dbo.Countries SET Name=N'USA Wyioming',CurrencyGuid=N'297ac727-8265-429e-81a2-49fdb279ae58',WeightUnitGuid=N'896f8c5b-37a7-49bd-8117-f8bcc05b07d7',LengthUnitGuid=N'a5c8beaa-445f-4659-9ae0-91a65e97e5e4',VolumeUnitGuid=N'5a95ad1d-b4dd-4eb8-a8f7-d52bcb9fc625',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'4044d191-26d2-4349-b6c9-8dc43d1bc39d',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'db844f11-5ec4-4af4-9dc0-9aa5ecaea29f'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'b94c0459-38f2-4ec3-88b0-67d90ab98364')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.838107638888889, 5.756815227, 1.784672011, 8.597184791, 2.176024052, 60.61854288, 0.01623062, 0.010632922, 0.182488122, 9.378229911, 55.19403873, 0.1536, 1, 'b94c0459-38f2-4ec3-88b0-67d90ab98364')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Venezuela Caracas', N'413d399d-f37f-413d-b18c-ca94f502c4a3', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'b94c0459-38f2-4ec3-88b0-67d90ab98364', 1, 'bdd3c750-f5af-4026-915c-d24d3c05cabe')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.838107638888889,UnskilledLaborCost=5.756815227,SkilledLaborCost=1.784672011,ForemanCost=8.597184791,TechnicianCost=2.176024052,EngineerCost=60.61854288,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=55.19403873,InterestRate=0.1536,IsScrambled=1 WHERE [Guid] = 'b94c0459-38f2-4ec3-88b0-67d90ab98364'
   UPDATE dbo.Countries SET Name=N'Venezuela Caracas',CurrencyGuid=N'413d399d-f37f-413d-b18c-ca94f502c4a3',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'bdd3c750-f5af-4026-915c-d24d3c05cabe'
END

IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = 'c2689829-4319-49cf-a8b4-7e781afcd42e')
BEGIN
   INSERT dbo.CountrySettings (ShiftCharge1ShiftModel, ShiftCharge2ShiftModel, ShiftCharge3ShiftModel, LaborAvailability, UnskilledLaborCost, SkilledLaborCost, ForemanCost, TechnicianCost, EngineerCost, EnergyCost, AirCost, WaterCost, ProductionAreaRentalCost, OfficeAreaRentalCost, InterestRate, IsScrambled, [Guid]) VALUES (0.1, 0.8, 0.2, 0.838107638888889, 1.494745336, 9.891411319, 6.663355967, 4.643499933, 9.873255349, 0.01623062, 0.010632922, 0.182488122, 9.378229911, 45.42222226, 0.1536, 1, 'c2689829-4319-49cf-a8b4-7e781afcd42e')
   INSERT dbo.Countries (Name, CurrencyGuid, WeightUnitGuid, LengthUnitGuid, VolumeUnitGuid, TimeUnitGuid, FloorUnitGuid, SettingsGuid, IsStockMasterData, [Guid]) VALUES (N'Venezuela Country', N'413d399d-f37f-413d-b18c-ca94f502c4a3', N'0d0846bf-7742-4729-b26e-6a996e42b8df', N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a', N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7', N'a8cd8e32-7801-4c3c-98e0-ddde86b580af', N'a01a96c9-39ba-4c1d-8deb-705b08722de0', N'c2689829-4319-49cf-a8b4-7e781afcd42e', 1, 'f0c02d5d-1c1e-405b-89f8-e0e25a10731c')
END
ELSE
BEGIN
   UPDATE dbo.CountrySettings SET ShiftCharge1ShiftModel=0.1,ShiftCharge2ShiftModel=0.8,ShiftCharge3ShiftModel=0.2,LaborAvailability=0.838107638888889,UnskilledLaborCost=1.494745336,SkilledLaborCost=9.891411319,ForemanCost=6.663355967,TechnicianCost=4.643499933,EngineerCost=9.873255349,EnergyCost=0.01623062,AirCost=0.010632922,WaterCost=0.182488122,ProductionAreaRentalCost=9.378229911,OfficeAreaRentalCost=45.42222226,InterestRate=0.1536,IsScrambled=1 WHERE [Guid] = 'c2689829-4319-49cf-a8b4-7e781afcd42e'
   UPDATE dbo.Countries SET Name=N'Venezuela Country',CurrencyGuid=N'413d399d-f37f-413d-b18c-ca94f502c4a3',WeightUnitGuid=N'0d0846bf-7742-4729-b26e-6a996e42b8df',LengthUnitGuid=N'a697a334-9d0e-4e51-a8c0-41fd2d5dd68a',VolumeUnitGuid=N'f1073aa1-f81f-4351-9bd8-9d7db06e01d7',TimeUnitGuid=N'a8cd8e32-7801-4c3c-98e0-ddde86b580af',FloorUnitGuid=N'a01a96c9-39ba-4c1d-8deb-705b08722de0',SettingsGuid=SettingsGuid, IsStockMasterData=1 WHERE [Guid] = 'f0c02d5d-1c1e-405b-89f8-e0e25a10731c'
END

delete from CountrySettings where [Guid] in (select SettingsGuid from CountryStates where CountryGuid='2968b5ed-6bed-4eae-a115-bba161fd87ea')
delete from CountryStates where CountryGuid='2968b5ed-6bed-4eae-a115-bba161fd87ea'
DELETE FROM dbo.Countries WHERE Guid = '2968b5ed-6bed-4eae-a115-bba161fd87ea'
DELETE FROM dbo.CountrySettings WHERE Guid = 'a75e3852-fd09-4afb-bf25-525e1cce23af'
