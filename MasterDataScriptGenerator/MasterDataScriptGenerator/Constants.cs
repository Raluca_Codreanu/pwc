﻿// -----------------------------------------------------------------------
// <copyright file="Constants.cs" company="Fortech">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace MasterDataScriptGenerator
{

    /// <summary>
    /// Master Data Constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Docs file relative path
        /// </summary>
        public const string DocsPath = "\\..\\..\\MasterData";

        /// <summary>
        /// Scripts file relative path
        /// </summary>
        public const string ScriptsPath = "\\..\\..\\Scripts";

        /// <summary>s
        /// Current CountrySettings Count
        /// </summary>
        public const int CountrySettingsCount = 219;

        /// <summary>s
        /// Current Currencies Count
        /// </summary>
        public const int CurrenciesCount = 45;

        /// <summary>
        /// Current RawMaterials Count
        /// </summary>
        public const int RawMaterialsCount = 914;

        /// <summary>
        /// Current Machines Count
        /// </summary>
        public const int MachinesCount = 1150;

        /// <summary>
        /// Current Manufacturers Count
        /// </summary>
        public const int ManufacturersCount = 50;

        /// <summary>
        /// Materials Classifications Count
        /// </summary>
        public const int MaterialsClassificationsCount = 150;

        /// <summary>
        /// Commodities Count
        /// </summary>
        public const int CommoditiesCount = 908;

        /// <summary>
        /// Master Data Countries xls File Path
        /// </summary>
        public const string MDCountriesXLSFilePath = DocsPath + "\\MDCountries.xlsx";

        /// <summary>
        /// Master Data Currencies xls File Path
        /// </summary>
        public const string MDCurrenciesXLSFilePath = DocsPath + "\\MDCurrencies.xlsx";

        /// <summary>
        /// Master Data Machines xls File Path
        /// </summary>
        public const string MDMachinesXLSFilePath = DocsPath + "\\MDMachines.xlsx";

        /// <summary>
        /// Master Data RawMaterials xls File Path
        /// </summary>
        public const string MDRawMaterialsXLSFilePath = DocsPath + "\\MDRawMaterials.xlsx";

        /// <summary>
        /// Master Data Commodities xls File Path
        /// </summary>
        public const string MDCommoditiesXLSFilePath = DocsPath + "\\MDCommodities.xlsx";

        /// <summary>
        /// Master Data Manufacturers xls File Path
        /// </summary>
        public const string MDManufacturersXLSFilePath = DocsPath + "\\MDManufacturers.xlsx";

        /// <summary>
        /// Generated Update Countries Script File Path
        /// </summary>
        public const string CountriesUpdateScriptPath = ScriptsPath + "\\CountriesUpdate_Script.sql";

        /// <summary>
        /// Generated Update Currencies Script File Path
        /// </summary>
        public const string CurrenciesUpdateScriptPath = ScriptsPath + "\\CurrenciesUpdate_Script.sql";

        /// <summary>
        /// Generated Update RawMaterials Script File Path
        /// </summary>
        public const string RawMaterialsUpdateScriptPath = ScriptsPath + "\\RawMaterialsUpdate_Script.sql";

        /// <summary>
        /// Generated Update Commodities Script File Path
        /// </summary>
        public const string CommoditiesUpdateScriptPath = ScriptsPath + "\\CommoditiesUpdate_Script.sql";

        /// <summary>
        /// Log File Path
        /// </summary>
        public const string UpdateLogFilePath = ScriptsPath + "\\log.txt";
    }
}