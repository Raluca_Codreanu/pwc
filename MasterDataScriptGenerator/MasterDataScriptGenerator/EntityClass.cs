﻿// -----------------------------------------------------------------------
// <copyright file="EntityClass.cs" company="Fortech">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace MasterDataScriptGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// EntityClass structure
    /// </summary>
    public class EntityClass
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Entity Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Entity Guid
        /// </summary>
        public string GUID { get; set; }

        /// <summary>
        /// Gets or sets the Entity parent Guid
        /// </summary>
        public string ParentGUID { get; set; }

        /// <summary>
        /// Gets or sets the Entity Class 1 Guid
        /// </summary>
        public Guid Class1 { get; set; }

        /// <summary>
        /// Gets or sets the Entity Class 2 Guid
        /// </summary>
        public Guid Class2 { get; set; }

        /// <summary>
        /// Gets or sets the Entity Class 3 Guid
        /// </summary>
        public Guid Class3 { get; set; }

        /// <summary>
        /// Gets or sets the Entity Class 4 Guid
        /// </summary>
        public Guid Class4 { get; set; }

        /// <summary>
        /// Gets or sets the Entity ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Entity Level
        /// </summary>
        public int Level { get; set; }

        #endregion Properties
    }
}