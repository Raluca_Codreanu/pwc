﻿// -----------------------------------------------------------------------
// <copyright file="QueryBuilder.cs" company="Fortech">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace MasterDataScriptGenerator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using ZPKTool.Data;

    /// <summary>
    /// "UPDATE table SET col=val,col=val WHERE col = value";
    /// "INSERT table (col, col, col) VALUES (val, val, val)";
    /// "DELETE FROM table WHERE col = value);
    /// </summary>
    public static class QueryBuilder
    {
        #region Update/Insert Queries

        /// <summary>
        /// Gets the country update/insert query
        /// </summary>
        /// <param name="settingsFields">The settings field names</param>
        /// <param name="settingValues">The setting values inserted in the fields</param>
        /// <param name="settingFieldsCount">The number of settings columns inserted</param>
        /// <param name="settingsGuid">The GUID of the inserted setting</param>
        /// <param name="countryFields">The countries field names</param>
        /// <param name="countryValues">The countries values inserted in the fields</param>
        /// <param name="countryFieldsCount">The number of countries columns inserted</param>
        /// <param name="countryGuid">The GUID of the inserted country</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForCountries(
            List<string> settingsFields,
            List<string> settingValues,
            int settingFieldsCount,
            Guid settingsGuid,
            List<string> countryFields,
            List<string> countryValues,
            int countryFieldsCount,
            Guid countryGuid,
            StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("IF NOT EXISTS (SELECT [Guid] FROM dbo.CountrySettings WHERE [Guid] = '" + settingsGuid + "')");

            // INSERT statement
            script.WriteLine("BEGIN");

            // Country Setting
            query += "   INSERT dbo.CountrySettings (";
            for (int i = 0; i < settingFieldsCount; i++)
            {
                query += settingsFields[i] + ", ";
            }

            query += "[Guid]) VALUES (";

            for (int i = 0; i < settingFieldsCount; i++)
            {
                query += settingValues[i] + ", ";
            }

            query += "'" + settingsGuid + "')";

            script.WriteLine(query);
            query = string.Empty;

            // Country
            query += "   INSERT dbo.Countries (";
            for (int i = 0; i < countryFieldsCount; i++)
            {
                query += countryFields[i] + ", ";
            }

            query += "IsStockMasterData, [Guid]) VALUES (";

            for (int i = 0; i < countryFieldsCount; i++)
            {
                query += countryValues[i] + ", ";
            }

            query += "1, '" + countryGuid + "')";

            script.WriteLine(query);
            script.WriteLine("END");
            script.WriteLine("ELSE");

            // UPDATE statement
            script.WriteLine("BEGIN");

            // Country Setting
            query = string.Empty;
            query += "   UPDATE dbo.CountrySettings SET ";
            for (int i = 0; i < settingFieldsCount - 1; i++)
            {
                query += settingsFields[i] + "=" + settingValues[i] + ",";
            }

            query += settingsFields[settingFieldsCount - 1] + "=" + settingValues[settingFieldsCount - 1] + " WHERE [Guid] = '" + settingsGuid + "'";
            script.WriteLine(query);

            // Country
            query = string.Empty;
            query += "   UPDATE dbo.Countries SET ";
            for (int i = 0; i < countryFieldsCount - 1; i++)
            {
                query += countryFields[i] + "=" + countryValues[i] + ",";
            }

            query += countryFields[countryFieldsCount - 1] + "=" + countryFields[countryFieldsCount - 1] + ", IsStockMasterData=1 WHERE [Guid] = '" + countryGuid + "'";
            script.WriteLine(query);

            script.WriteLine("END");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the currencies update/insert query
        /// </summary>
        /// <param name="currencyFields">The field name</param>
        /// <param name="currencyValues">The values inserted in the fields</param>
        /// <param name="currencyFieldsCount">The number of columns inserted</param>
        /// <param name="currencyGuid">The currency guid</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForCurrencies(List<string> currencyFields, List<string> currencyValues, int currencyFieldsCount, Guid currencyGuid, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("IF NOT EXISTS (SELECT [Guid] FROM dbo.Currencies WHERE [Guid] = '" + currencyGuid + "')");

            // INSERT statement
            script.WriteLine("BEGIN");

            query += "   INSERT dbo.Currencies (";
            for (int i = 0; i < currencyFieldsCount; i++)
            {
                query += currencyFields[i] + ", ";
            }

            query += "IsMasterData, IsStockMasterData, [Guid]) VALUES (";

            for (int i = 0; i < currencyFieldsCount; i++)
            {
                query += currencyValues[i] + ", ";
            }

            query += "1, 1, '" + currencyGuid + "')";

            script.WriteLine(query);
            script.WriteLine("END");
            script.WriteLine("ELSE");

            // UPDATE statement
            script.WriteLine("BEGIN");
            query = string.Empty;
            query += "   UPDATE dbo.Currencies SET ";
            for (int i = 0; i < currencyFieldsCount - 1; i++)
            {
                query += currencyFields[i] + "=" + currencyValues[i] + ", ";
            }

            query += currencyFields[currencyFieldsCount - 1] + "=" + currencyValues[currencyFieldsCount - 1] + ", IsMasterData=1, IsStockMasterData=1 WHERE [Guid] = '" + currencyGuid + "'";
            script.WriteLine(query);
            script.WriteLine("END");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the raw materials update/insert query
        /// </summary>
        /// <param name="materialFields">The material field names</param>
        /// <param name="materialValues">The material values inserted in the fields</param>
        /// <param name="materialFieldsCount">The number of material columns inserted</param>
        /// <param name="materialGuid">The GUID of the inserted material</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForRawMaterials(List<string> materialFields, List<string> materialValues, int materialFieldsCount, Guid materialGuid, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.RawMaterials WHERE Guid = '" + materialGuid + "')");

            // INSERT statement
            script.WriteLine("begin");

            query += "   INSERT dbo.RawMaterials (";
            for (int i = 0; i < materialFieldsCount; i++)
            {
                query += materialFields[i] + ", ";
            }

            query += "IsStockMasterData, Guid) VALUES (";

            for (int i = 0; i < materialFieldsCount; i++)
            {
                query += materialValues[i] + ", ";
            }

            query += "1, '" + materialGuid + "')";

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            query += "   UPDATE dbo.RawMaterials SET ";
            for (int i = 0; i < materialFieldsCount - 1; i++)
            {
                query += materialFields[i] + "=" + materialValues[i] + ",";
            }

            query += materialFields[materialFieldsCount - 1] + "=" + materialValues[materialFieldsCount - 1] + ", IsStockMasterData=1 WHERE Guid = '" + materialGuid + "'";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the machine update/insert query
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="machineGuid">The GUID of the inserted machine</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForMachines(List<string> fields, List<string> values, int fieldsCount, Guid machineGuid, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Machines WHERE Guid = '" + machineGuid + "')");

            // INSERT statement
            script.WriteLine("begin");

            query += "   INSERT dbo.Machines (";
            for (int i = 0; i < fieldsCount; i++)
            {
                query += fields[i] + ", ";
            }

            query += "IsStockMasterData, Guid) VALUES (";

            for (int i = 0; i < fieldsCount; i++)
            {
                query += values[i] + ", ";
            }

            query += "1, '" + machineGuid + "')";

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            query += "   UPDATE dbo.Machines SET ";
            for (var i = 0; i < fieldsCount - 1; i++)
            {
                query += fields[i] + "=" + values[i] + ",";
            }

            query += fields[fieldsCount - 1] + "=" + values[fieldsCount - 1] + ", IsStockMasterData=1 WHERE Guid = '" + machineGuid + "'";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the customers update/insert query
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="customerGuid">The GUID of the inserted customer</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForCustomers(List<string> fields, List<string> values, int fieldsCount, Guid customerGuid, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Customers WHERE Guid = '" + customerGuid + "')");

            // INSERT statement
            script.WriteLine("begin");

            query += "   INSERT dbo.Customers (";
            for (int i = 0; i < fieldsCount; i++)
            {
                query += fields[i] + ", ";
            }

            query += "IsStockMasterData, Guid) VALUES (";

            for (int i = 0; i < fieldsCount; i++)
            {
                query += values[i] + ", ";
            }

            query += "1, '" + customerGuid + "')";

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            query += "   UPDATE dbo.Customers SET ";
            for (int i = 0; i < fieldsCount - 1; i++)
            {
                query += fields[i] + "=" + values[i] + ",";
            }

            query += fields[fieldsCount - 1] + "=" + values[fieldsCount - 1] + ", IsStockMasterData=1 WHERE Guid = '" + customerGuid + "'";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the manufacturers update/insert query
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="manufacturerGuid">The GUID of the inserted manufacturer</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForManufacturers(List<string> fields, List<string> values, int fieldsCount, Guid manufacturerGuid, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = '" + manufacturerGuid + "')");

            // INSERT statement
            script.WriteLine("begin");

            query += "   INSERT dbo.Manufacturers (";
            for (int i = 0; i < fieldsCount; i++)
            {
                query += fields[i] + ", ";
            }

            query += "IsStockMasterData, Guid) VALUES (";

            for (int i = 0; i < fieldsCount; i++)
            {
                query += values[i] + ", ";
            }

            query += "1, '" + manufacturerGuid + "')";

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            query += "   UPDATE dbo.Manufacturers SET ";
            for (int i = 0; i < fieldsCount - 1; i++)
            {
                query += fields[i] + "=" + values[i] + ",";
            }

            query += fields[fieldsCount - 1] + "=" + values[fieldsCount - 1] + ", IsStockMasterData=1 WHERE Guid = '" + manufacturerGuid + "'";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// GGets the commodities update/insert query
        /// </summary>
        /// <param name="commodityFields">he field names</param>
        /// <param name="commodityValues">The values inserted in the fields</param>
        /// <param name="commodityFieldsCount">he number of columns inserted</param>
        /// <param name="commodityGuid">The GUID of the inserted commodity</param>
        /// <param name="script">The script writer</param>
        public static void GetUpdateOrInsertQueryForCommodities(List<string> commodityFields, List<string> commodityValues, int commodityFieldsCount, Guid commodityGuid, StreamWriter script)
        {
            string query = string.Empty;

            // INSERT Manufacturer if not exists
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Manufacturers WHERE Guid = " + commodityValues[3] + ")");
            script.WriteLine("begin");
            query += "   INSERT dbo.Manufacturers (Name, IsMasterData, Guid) VALUES (N'MLS', 1, " + commodityValues[3] + ")";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();

            // Update Commodity
            query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Commodities WHERE Guid = '" + commodityGuid + "')");

            // INSERT statement
            script.WriteLine("begin");

            query += "   INSERT dbo.Commodities (";
            for (int i = 0; i < commodityFieldsCount; i++)
            {
                query += commodityFields[i] + ", ";
            }

            query += "IsMasterData, Guid) VALUES (";

            for (int i = 0; i < commodityFieldsCount; i++)
            {
                query += commodityValues[i] + ", ";
            }

            query += "'1', '" + commodityGuid + "')";

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            query += "   UPDATE dbo.Commodities SET ";
            for (int i = 0; i < commodityFieldsCount - 1; i++)
            {
                query += commodityFields[i] + "=" + commodityValues[i] + ",";
            }

            query += commodityFields[commodityFieldsCount - 1] + "=" + commodityValues[commodityFieldsCount - 1] + ",IsMasterData='1' WHERE Guid = '" + commodityGuid + "'";
            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the raw materials classifications update query
        /// </summary>
        /// <param name="material">The material updated.</param>
        /// <param name="script">The script writer.</param>
        public static void GetUpdateOrInsertQueryForRawMaterialClassifications(EntityClass material, StreamWriter script)
        {
            string query = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.MaterialsClassification WHERE Guid = '" + material.GUID + "')");

            // INSERT statement
            script.WriteLine("begin");
            if (string.IsNullOrEmpty(material.ParentGUID))
            {
                query += "   INSERT dbo.MaterialsClassification (Name, Guid, IsReleased, ParentGuid) VALUES (N'" + material.Name + "', '" + material.GUID + "', 0, NULL)";
            }
            else
            {
                query += "   INSERT dbo.MaterialsClassification (Name, Guid, IsReleased, ParentGuid) VALUES (N'" + material.Name + "', '" + material.GUID + "', 0, '" + material.ParentGUID + "')";
            }

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine("else");

            // UPDATE statement
            script.WriteLine("begin");
            query = string.Empty;
            if (string.IsNullOrEmpty(material.ParentGUID))
            {
                query += "   UPDATE dbo.MaterialsClassification SET ParentGuid = NULL WHERE [Guid] = '" + material.GUID + "'";
            }
            else
            {
                query += "   UPDATE dbo.MaterialsClassification SET ParentGuid = '" + material.ParentGUID + "' WHERE [Guid] = '" + material.GUID + "'";
            }

            script.WriteLine(query);
            script.WriteLine("end");
            script.WriteLine();
        }

        /// <summary>
        /// Gets the machines classifications update query
        /// </summary>
        /// <param name="machine">The machine updated.</param>
        /// <param name="script">The script writer.</param>
        public static void GetUpdateOrInsertQueryForMachineClassifications(EntityClass machine, StreamWriter script)
        {
            var query = string.Empty;
            script.WriteLine("IF NOT EXISTS (SELECT Guid FROM dbo.MachinesClassification WHERE Guid = '" + machine.GUID + "')");

            // INSERT statement
            script.WriteLine("BEGIN");
            if (string.IsNullOrEmpty(machine.ParentGUID))
            {
                query += "   INSERT dbo.MachinesClassification ([Name], [Guid], ParentGuid) VALUES ('" + machine.Name + "', '" + machine.GUID + "', NULL)";
            }
            else
            {
                query += "   INSERT dbo.MachinesClassification ([Name], [Guid], ParentGuid) VALUES ('" + machine.Name + "', '" + machine.GUID + "', '" + machine.ParentGUID + "')";
            }

            script.WriteLine(query);
            script.WriteLine("END");
            script.WriteLine("ELSE");

            // UPDATE statement
            script.WriteLine("BEGIN");
            query = string.Empty;
            if (string.IsNullOrEmpty(machine.ParentGUID))
            {
                query += "   UPDATE dbo.MachinesClassification SET Name = N'" + machine.Name + "', ParentGuid = NULL WHERE [Guid] = '" + machine.GUID + "'";
            }
            else
            {
                query += "   UPDATE dbo.MachinesClassification SET Name = N'" + machine.Name + "', ParentGuid = '" + machine.ParentGUID + "' WHERE [Guid] = '" + machine.GUID + "'";
            }

            script.WriteLine(query);
            script.WriteLine("END");
            script.WriteLine();
        }

        public static void GetUpdateQueryForMachinePicture(Guid machineGuid, string picturePath, string pictureName, StreamWriter script, int pitureIndex)
        {
            if (string.IsNullOrWhiteSpace(picturePath) || string.IsNullOrWhiteSpace(pictureName))
            {
                var machineMediaId = "@machine" + pitureIndex + "MediaId";
                script.WriteLine("IF EXISTS (SELECT Guid FROM dbo.Machines WHERE Guid = '" + machineGuid + "')");
                script.WriteLine("BEGIN");
                script.WriteLine("   DECLARE " + machineMediaId + " uniqueidentifier");
                script.WriteLine("   SET " + machineMediaId + " =(SELECT MediaGuid FROM dbo.Machines WHERE Guid = '" + machineGuid + "')");
                script.WriteLine("   IF " + machineMediaId + " IS NOT NULL");
                script.WriteLine("   BEGIN");
                script.WriteLine("      UPDATE dbo.Machines SET MediaGuid=NULL WHERE Guid = '" + machineGuid + "'");
                script.WriteLine("      DELETE FROM dbo.Media WHERE Guid = " + machineMediaId);
                script.WriteLine("   END");
                script.WriteLine("END");
                script.WriteLine();
            }
            else
            {
                var mediaGuid = Guid.NewGuid();

                var imageName = "@image" + pitureIndex;
                var mediaSize = "@media" + pitureIndex + "Size";
                var machineMediaId = "@machine" + pitureIndex + "MediaId";
                script.WriteLine(
                    "IF EXISTS (SELECT Guid FROM dbo.Machines WHERE Guid = '" +
                    machineGuid + "')");
                script.WriteLine("BEGIN");
                script.WriteLine("   DECLARE " + imageName + " varbinary(max), " + mediaSize + " varbinary(max), " +
                                 machineMediaId + " uniqueidentifier");
                script.WriteLine("   SET " + imageName + " =(SELECT * FROM OPENROWSET(BULK N'" + picturePath + "\\" + pictureName + "', SINGLE_BLOB) as tempImage)");
                script.WriteLine("   SET " + machineMediaId + " =(SELECT MediaGuid FROM dbo.Machines WHERE Guid = '" + machineGuid + "')");
                script.WriteLine("   IF " + machineMediaId + " IS NOT NULL");
                script.WriteLine("   BEGIN");
                script.WriteLine("      SET " + mediaSize + " = (SELECT Size FROM dbo.Media WHERE Guid = " + machineMediaId + ")");
                script.WriteLine("      IF " + mediaSize + " <> DATALENGTH(" + imageName + ")");
                script.WriteLine("      BEGIN");
                script.WriteLine("         UPDATE dbo.Media SET Type=0, Content=" + imageName + ", Size=DATALENGTH(" + imageName + "), OriginalFileName='" + pictureName +
                                 "', IsMasterData='True' WHERE Guid = " + machineMediaId);
                script.WriteLine("      END");
                script.WriteLine("   END");
                script.WriteLine("   ELSE");
                script.WriteLine("   BEGIN");
                script.WriteLine("      INSERT INTO Media(Guid, Type, Content, Size, OriginalFileName, IsMasterData) VALUES('" +
                    mediaGuid + "', 0, " + imageName + ", DATALENGTH(" + imageName + "), '" + pictureName + "', 'True')");
                script.WriteLine("      UPDATE dbo.Machines SET MediaGuid='" + mediaGuid + "' WHERE Guid = '" + machineGuid + "'");
                script.WriteLine("   END");
                script.WriteLine("END");
                script.WriteLine();

                //var imageName = "@image" + pitureIndex;
                //var mediaSize = "@media" + pitureIndex + "Size";
                //var machineMediaId = "@machine" + pitureIndex + "MediaId";
                //script.WriteLine(
                //    "IF EXISTS (SELECT Guid FROM dbo.Machines WHERE Guid = '" +
                //    machineGuid + "')");
                //script.WriteLine("BEGIN");
                //script.WriteLine("   DECLARE " + mediaSize + " varbinary(max), " + machineMediaId + " uniqueidentifier");
                //script.WriteLine("   SET " + machineMediaId + " =(SELECT MediaGuid FROM dbo.Machines WHERE Guid = '" + machineGuid + "')");
                //script.WriteLine("   IF " + machineMediaId + " IS NOT NULL");
                //script.WriteLine("   BEGIN");
                //script.WriteLine("         UPDATE dbo.Media SET Type=0, Content = (SELECT * FROM OPENROWSET(BULK N'" + picturePath + "\\" + pictureName + "', SINGLE_BLOB) as tempImage), OriginalFileName='" + pictureName +
                //                 "', IsMasterData='True' WHERE Guid = " + machineMediaId);
                //script.WriteLine("         UPDATE dbo.Media SET [Size]=DATALENGTH([Content]) WHERE Guid = " + machineMediaId);
                //script.WriteLine("   END");
                //script.WriteLine("   ELSE");
                //script.WriteLine("   BEGIN");
                //script.WriteLine("      INSERT INTO dbo.Media(Guid, Type, Content, Size, OriginalFileName, IsMasterData) SELECT '" + mediaGuid + "', 0, (SELECT * FROM OPENROWSET(BULK N'" + picturePath + "\\" + pictureName + "', SINGLE_BLOB) as tempImage), 0, '" + pictureName + "', 'True'");
                //script.WriteLine("      UPDATE dbo.Media SET [Size]=DATALENGTH([Content]) WHERE Guid = '" + mediaGuid + "'");
                //script.WriteLine("      UPDATE dbo.Machines SET MediaGuid='" + mediaGuid + "' WHERE Guid = '" + machineGuid + "'");
                //script.WriteLine("   END");
                //script.WriteLine("END");
                //script.WriteLine();

            }
        }

        #endregion Update/Insert Queries

        #region Insert Queries

        /// <summary>
        /// Builds the insert query
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="guid">The GUID of the inserted entry</param>
        /// <param name="tableName">Table name value</param>
        /// <param name="script">The script writer</param>
        public static void GetInsertQuery(List<string> fields, List<string> values, int fieldsCount, Guid guid, string tableName, StreamWriter script)
        {
            string insertquery = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo." + tableName + " WHERE Guid = '" + guid + "')");
            script.WriteLine("begin");

            insertquery += "   INSERT dbo." + tableName + "(";
            for (int i = 0; i < fieldsCount; i++)
            {
                insertquery += fields[i] + ", ";
            }

            insertquery += "Guid) VALUES (";

            for (int i = 0; i < fieldsCount; i++)
            {
                insertquery += values[i] + ", ";
            }

            insertquery += "N'" + guid + "')";

            script.WriteLine(insertquery);
            script.WriteLine("end");
        }

        /// <summary>
        /// Gets the machines insert query
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="guid">The GUID of the inserted entry</param>
        /// <param name="manufacturerGuid">The manufacturer guid</param>
        /// <param name="script">The script writer</param>
        public static void GetInsertQueryForMachines(List<string> fields, List<string> values, int fieldsCount, Guid guid, Guid manufacturerGuid, StreamWriter script)
        {
            string insertquery = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.Machines WHERE Guid = '" + guid + "')");
            script.WriteLine("begin");

            if (manufacturerGuid != Guid.Empty)
            {
                insertquery += "   SET @manID = (SELECT ID FROM dbo.Manufacturers WHERE Guid = '" + manufacturerGuid + "')";
                script.WriteLine(insertquery);
                insertquery = string.Empty;

                insertquery += "   INSERT dbo.Machines(ManufacturerID, ";
                for (int i = 0; i < fieldsCount; i++)
                {
                    insertquery += fields[i] + ", ";
                }

                insertquery += "Guid) VALUES (@manID, ";

                for (int i = 0; i < fieldsCount; i++)
                {
                    insertquery += values[i] + ", ";
                }

                insertquery += "N'" + guid + "')";
            }
            else
            {
                insertquery += "   INSERT dbo.Machines(";
                for (int i = 0; i < fieldsCount; i++)
                {
                    insertquery += fields[i] + ", ";
                }

                insertquery += "Guid) VALUES (";

                for (int i = 0; i < fieldsCount; i++)
                {
                    insertquery += values[i] + ", ";
                }

                insertquery += "N'" + guid + "')";
            }

            script.WriteLine(insertquery);
            script.WriteLine("end");
        }

        /// <summary>
        /// Gets the country and country settings insert query
        /// </summary>
        /// <param name="settingsFields">The settings fields</param>
        /// <param name="settingValues">The setting values</param>
        /// <param name="settingFieldsCount">The number of fields in countrysettings</param>
        /// <param name="settingsGuid">The settings GUID</param>
        /// <param name="countryFields">The country fields</param>
        /// <param name="countryValues">The country values</param>
        /// <param name="countryFieldsCount">The number of fields in country</param>
        /// <param name="countryGuid">The country GUID</param>
        /// <param name="script">The script writer</param>
        public static void GetCountryAndCountrySettingsInsertQuery(
            List<string> settingsFields,
            List<string> settingValues,
            int settingFieldsCount,
            Guid settingsGuid,
            List<string> countryFields,
            List<string> countryValues,
            int countryFieldsCount,
            Guid countryGuid,
            StreamWriter script)
        {
            string insertquery = string.Empty;
            script.WriteLine("if not exists (SELECT Guid FROM dbo.CountrySettings WHERE Guid = '" + settingsGuid + "')");
            script.WriteLine("begin");

            // Country Setting
            insertquery += "   INSERT dbo.CountrySettings (";
            for (int i = 0; i < settingFieldsCount; i++)
            {
                insertquery += settingsFields[i] + ", ";
            }

            insertquery += "Guid) VALUES (";

            for (int i = 0; i < settingFieldsCount; i++)
            {
                insertquery += settingValues[i] + ", ";
            }

            insertquery += "N'" + settingsGuid + "')";

            script.WriteLine(insertquery);
            insertquery = string.Empty;

            // get CountrySetting ID
            script.WriteLine("   SET @lastID = (SELECT MAX(ID) FROM dbo.CountrySettings)");

            // Country
            insertquery += "   INSERT dbo.Countries (";
            for (int i = 0; i < countryFieldsCount; i++)
            {
                insertquery += countryFields[i] + ", ";
            }

            insertquery += "Guid) VALUES (";

            for (int i = 0; i < countryFieldsCount; i++)
            {
                insertquery += countryValues[i] + ", ";
            }

            insertquery += "N'" + countryGuid + "')";

            script.WriteLine(insertquery);
            script.WriteLine("end");
        }

        #endregion Insert Queries

        #region Update Queries

        /// <summary>
        /// Builds the update query.
        /// </summary>
        /// <param name="fields">The field names</param>
        /// <param name="values">The values inserted in the fields</param>
        /// <param name="fieldsCount">The number of columns inserted</param>
        /// <param name="guid">The GUID of the updated entry</param>
        /// <param name="tableName">Table name</param>
        /// <returns>the update query</returns>
        public static string GetUpdateQuery(List<string> fields, List<string> values, int fieldsCount, Guid guid, string tableName)
        {
            string updatequery = string.Empty;
            updatequery += "UPDATE dbo." + tableName + " SET ";
            for (int i = 0; i < fieldsCount - 1; i++)
            {
                updatequery += fields[i] + "=" + values[i] + ",";
            }

            updatequery += fields[fieldsCount - 1] + "=" + values[fieldsCount - 1] + " WHERE Guid = '" + guid + "'";
            return updatequery;
        }

        #endregion Update Queries

        #region Delete Queries

        /// <summary>
        /// Builds the delete query.
        /// </summary>
        /// <param name="guid">The GUID of the deleted entry</param>
        /// <param name="tableName">Table name</param>
        /// <returns>the delete query for current table</returns>
        public static string GetDeleteQuery(Guid guid, string tableName)
        {
            string deletequery = "DELETE FROM dbo." + tableName + " WHERE Guid = '" + guid + "'";
            return deletequery;
        }

        /// <summary>
        /// Builds a specific query for the delete country states command
        /// </summary>
        /// <param name="guid">The GUID of the deleted entry</param>
        /// <returns>the delete query for country state</returns>
        public static string GetDeleteCountryStateQuery(Guid guid)
        {
            string deletequery = "delete from CountrySettings where [Guid] in (select SettingsGuid from CountryStates where CountryGuid='" + guid + "')";
            deletequery += "\n";
            deletequery += "delete from CountryStates where CountryGuid='" + guid + "'";
            return deletequery;
        }

        #endregion Delete Queries

        #region Converters

        /// <summary>
        /// Converts the decimal to SQL insert value.
        /// </summary>
        /// <param name="value">The value</param>
        /// <param name="scrambleValue">Specifies if the data should be scrambled</param>
        /// <returns>the converted value</returns>
        public static string ConvertDecimalToSQLInsertValue(object value, bool scrambleValue)
        {
            if (value == null || value.ToString() == "NULL")
            {
                return "NULL";
            }
            else
            {
                value = value.ToString().Trim();

                if (scrambleValue)
                {
                    value = MasterDataScrambler.ScrambleNumber(Convert.ToDecimal(value));
                }

                return value.ToString();
            }
        }

        /// <summary>
        /// Converts the string to SQL insert value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="scrambleValue">Specifies if the data should be scrambled</param>
        /// <returns>the converted value</returns>
        public static string ConvertStringToSQLInsertValue(object value, bool scrambleValue)
        {
            double doubleVal = -1;

            if (value == null || value.ToString() == "NULL" || (double.TryParse(value.ToString(), out doubleVal) && doubleVal.Equals(0)))
            {
                return "NULL";
            }
            else
            {
                value = value.ToString().Trim();

                if (scrambleValue)
                {
                    value = MasterDataScrambler.ScrambleText(Convert.ToString(value));
                }

                return "N'" + value + "'";
            }
        }

        /// <summary>
        /// Converts the decimal to SQL update value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="scrambleValue">Specifies if the data should be scrambled</param>
        /// <returns>the converted value</returns>
        public static string ConvertDecimalToSQLUpdateValue(object value, bool scrambleValue)
        {
            if (value == null || value.ToString() == "NULL")
            {
                return "NULL";
            }
            else
            {
                value = value.ToString().Trim();

                if (scrambleValue)
                {
                    value = MasterDataScrambler.ScrambleNumber(Convert.ToDecimal(value));
                }

                return value.ToString();
            }
        }

        /// <summary>
        /// Converts the string to SQL update value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="scrambleValue">Specifies if the data should be scrambled</param>
        /// <returns>the converted value</returns>
        public static string ConvertStringToSQLUpdateValue(object value, bool scrambleValue)
        {
            if (value == null || value.ToString() == "NULL")
            {
                return "NULL";
            }
            else
            {
                value = value.ToString().Trim();

                if (scrambleValue)
                {
                    value = MasterDataScrambler.ScrambleText(Convert.ToString(value));
                }

                return "N'" + value + "'";
            }
        }

        #endregion Converters
    }
}