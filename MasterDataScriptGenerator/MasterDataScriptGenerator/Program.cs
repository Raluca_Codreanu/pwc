﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Fortech">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;


namespace MasterDataScriptGenerator
{


    /// <summary>
    /// Program class.
    /// </summary>
    internal class Program
    {
        private const string MachinesPath = "\\..\\..\\Docs\\MDMachines1.xlsx";

        /// <summary>
        /// The maximum width an image can have. If an image has a larger width it is, usually, proportionally resized to this value.
        /// </summary>
        public const int MaxImageWidth = 800;

        #region Main Method

        /// <summary>
        /// Application Main method
        /// </summary>
        /// <param name="args">maine args</param>
        private static void Main(string[] args)
        {
            bool validChoice = false;
            string procced = string.Empty;

            Program program = new Program();
            program.InitializeDBConnection();
            do
            {
                try
                {
                    program.UpdateSelectionMenu(program);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error has ocured:");
                    Console.WriteLine("'" + ex.Message + "'");
                }
                finally
                {
                    Console.ReadLine();

                    do
                    {
                        Console.Clear();
                        Console.WriteLine("Do you want to perform other operations? ");
                        Console.WriteLine("(Y)es.");
                        Console.WriteLine("(N)o.");

                        procced = Console.ReadLine();

                        switch (procced.ToLower())
                        {
                            case "y":
                                {
                                    validChoice = true;
                                    break;
                                }

                            case "n":
                                {
                                    validChoice = true;
                                    break;
                                }

                            default:
                                break;
                        }
                    }
                    while (!validChoice);
                }
            }
            while (procced.ToLower().Equals("y"));
        }

        #endregion Main Method

        #region Initialize

        /// <summary>
        /// Update Selection MasterData Type Menu
        /// </summary>
        /// <param name="program">program instance</param>
        private void UpdateSelectionMenu(Program program)
        {
            string choice = string.Empty;

            Console.Clear();
            Console.WriteLine("Please Choose an operation: ");
            Console.WriteLine("1. Update Countries.");
            Console.WriteLine("2. Update RawMaterials.");
            Console.WriteLine("3. Update Machines.");
            Console.WriteLine("4. Update Manufacturers.");
            Console.WriteLine("5. Update Currencies.");
            Console.WriteLine("6. Update Commodities.");
            Console.WriteLine("7. Get Machine pictures.");
            Console.WriteLine("8. Close application.");

            choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    {
                        program.StartWork(Constants.MDCountriesXLSFilePath);
                        break;
                    }

                case "2":
                    {
                        program.StartWork(Constants.MDRawMaterialsXLSFilePath);
                        break;
                    }

                case "3":
                    {
                        program.StartWork(Constants.MDMachinesXLSFilePath);
                        break;
                    }

                case "4":
                    {
                        program.StartWork(Constants.MDManufacturersXLSFilePath);
                        break;
                    }

                case "5":
                    {
                        program.StartWork(Constants.MDCurrenciesXLSFilePath);
                        break;
                    }

                case "6":
                    {
                        program.StartWork(Constants.MDCommoditiesXLSFilePath);
                        break;
                    }

                case "7":
                    {
                        program.StartWork(MachinesPath);
                        break;
                    }


                case "8":
                    {
                        Environment.Exit(1);
                        break;
                    }

                default:
                    {
                        Console.Clear();
                        program.UpdateSelectionMenu(program);
                        break;
                    }
            }
        }

        /// <summary>
        /// Manage MasterData Updates
        /// </summary>
        /// <param name="xlsPath">Source excel file path</param>
        private void StartWork(string xlsPath)
        {
            FileInfo dataFile = new FileInfo(Environment.CurrentDirectory + xlsPath);
            if (!dataFile.Exists)
            {
                throw new FileNotFoundException();
            }

            try
            {
                using (ExcelPackage package = new ExcelPackage(dataFile))
                {
                    switch (xlsPath)
                    {
                        case Constants.MDCountriesXLSFilePath:
                            {
                                this.ReadCountrySettingXLSX(package.Workbook);
                                break;
                            }

                        case Constants.MDRawMaterialsXLSFilePath:
                            {
                                this.ReadMaterialsXLSX(package.Workbook);
                                break;
                            }

                        case Constants.MDMachinesXLSFilePath:
                            {
                                this.ReadMachinesXLSX(package.Workbook);
                                break;
                            }

                        case Constants.MDManufacturersXLSFilePath:
                            {
                                this.ReadManufacturersXLSX(package.Workbook);
                                break;
                            }

                        case Constants.MDCurrenciesXLSFilePath:
                            {
                                this.ReadCurrenciesXLSX(package.Workbook);
                                break;
                            }

                        case Constants.MDCommoditiesXLSFilePath:
                            {
                                this.ReadCommoditiesXLSX(package.Workbook);
                                break;
                            }

                        case MachinesPath:
                            {
                                this.SerializeMachinesImages(package.Workbook);
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Initialize connection to DataBase
        /// </summary>
        private void InitializeDBConnection()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.DataSource = "System.Data.SqlClient";
            localConnParams.InitialCatalog = "ZPKTool";
            localConnParams.DataSource = "localhost\\SQLEXPRESS";
            localConnParams.UserId = "PROimpensUser";
            localConnParams.Password = "iRl9VE3o1n1OlxzcTaOA0g==";
            localConnParams.UseWindowsAuthentication = true;
            localConnParams.PasswordEncryptionKey = "BVf4jSKZ762fo1FyKd79WWeGITZMLRW7H0At2ES11otN1VZueoifQBD6";

            DbConnectionInfo centralConnParams = new DbConnectionInfo();
            centralConnParams.DataSource = "System.Data.SqlClient";
            centralConnParams.InitialCatalog = "ZPKToolCentral";
            centralConnParams.DataSource = "localhost\\SQLEXPRESS";
            centralConnParams.UserId = "PROimpensUser";
            centralConnParams.Password = "iRl9VE3o1n1OlxzcTaOA0g==";
            centralConnParams.PasswordEncryptionKey = "BVf4jSKZ762fo1FyKd79WWeGITZMLRW7H0At2ES11otN1VZueoifQBD6";
            centralConnParams.UseWindowsAuthentication = true;

            ConnectionConfiguration.Initialize(localConnParams, centralConnParams);
        }

        #endregion Initialize

        #region Read from xlsx

        /// <summary>
        /// Read Commodities xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadCommoditiesXLSX(ExcelWorkbook excelWorkbook)
        {
            ExcelWorksheet xlsCommodities = excelWorkbook.Worksheets["Sheet1"];
            if (xlsCommodities == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Collection<Commodity> databaseCommodities = dc.CommodityRepository.GetAllMasterData();

            List<Guid> guids = databaseCommodities.Select(p => p.Guid).ToList();
            int attached = 0;
            int deleted = 0;

            System.IO.StreamWriter script = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.CommoditiesUpdateScriptPath);
            System.IO.StreamWriter log = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.UpdateLogFilePath);
            List<Guid> attachedGuids = new List<Guid>();

            try
            {
                // parse the xls, generate the insert queries for the items not present int the db
                for (int i = 2; i <= Constants.CommoditiesCount + 1; i++)
                {
                    List<string> commodityValues = new List<string>();
                    List<string> commodityFields = new List<string>();
                    int commodityFieldsCount = 0;

                    Guid commodityGuid = new Guid(Convert.ToString(xlsCommodities.Cells["A" + i].Value));

                    // Name
                    object value;

                    value = xlsCommodities.Cells["B" + i].Value;
                    commodityFields.Add("Name");
                    commodityValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    commodityFieldsCount++;

                    // Part Number
                    value = xlsCommodities.Cells["C" + i].Value;
                    commodityFields.Add("PartNumber");
                    commodityValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    commodityFieldsCount++;

                    // Price
                    value = xlsCommodities.Cells["D" + i].Value;
                    commodityFields.Add("Price");
                    commodityValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    commodityFieldsCount++;

                    // Supplier Guid
                    value = xlsCommodities.Cells["E" + i].Value;
                    commodityFields.Add("ManufacturerGuid");
                    commodityValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                    commodityFieldsCount++;

                    QueryBuilder.GetUpdateOrInsertQueryForCommodities(commodityFields, commodityValues, commodityFieldsCount, commodityGuid, script);

                    Console.WriteLine("Attached commodity " + Convert.ToString(xlsCommodities.Cells["B" + i].Value));
                    attached++;

                    attachedGuids.Add(commodityGuid);
                }

                // delete section
                foreach (Commodity commodity in databaseCommodities)
                {
                    if (!attachedGuids.Contains(commodity.Guid))
                    {
                        string deletequery = QueryBuilder.GetDeleteQuery(commodity.Guid, "Commodities");
                        script.WriteLine(deletequery);
                        Console.WriteLine("Deleted commodity " + commodity.Name);
                        deleted++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("Attached = " + attached + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Read Currencies xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadCurrenciesXLSX(ExcelWorkbook excelWorkbook)
        {
            ExcelWorksheet xlsCurrencies = excelWorkbook.Worksheets["Sheet1"];
            if (xlsCurrencies == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            // Collection<Currency> databaseCurrencies = dc.CurrencyRepository.GetAll();
            Collection<Currency> databaseCurrencies = new Collection<Currency>();

            List<Guid> guids = databaseCurrencies.Select(p => p.Guid).ToList();
            int attached = 0;
            int deleted = 0;

            System.IO.StreamWriter script = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.CurrenciesUpdateScriptPath);
            System.IO.StreamWriter log = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.UpdateLogFilePath);
            List<Guid> attachedGuids = new List<Guid>();

            try
            {
                // parse the xls, generate the insert queries for the items not present int the db
                for (int i = 2; i <= Constants.CurrenciesCount + 1; i++)
                {
                    List<string> currencyValues = new List<string>();
                    List<string> currencyFields = new List<string>();
                    int currencyFieldsCount = 0;

                    Guid currencyGuid = new Guid(Convert.ToString(xlsCurrencies.Cells["C" + i].Value));

                    // Name
                    object value;

                    value = xlsCurrencies.Cells["A" + i].Value;
                    currencyFields.Add("Name");
                    currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    currencyFieldsCount++;

                    // Symbol
                    value = xlsCurrencies.Cells["D" + i].Value;
                    currencyFields.Add("Symbol");
                    currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    currencyFieldsCount++;

                    // ExchangeRate
                    value = xlsCurrencies.Cells["B" + i].Value;
                    currencyFields.Add("ExchangeRate");
                    currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    currencyFieldsCount++;

                    value = xlsCurrencies.Cells["E" + i].Value;
                    currencyFields.Add("IsoCode");
                    currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    currencyFieldsCount++;

                    QueryBuilder.GetUpdateOrInsertQueryForCurrencies(currencyFields, currencyValues, currencyFieldsCount, currencyGuid, script);

                    Console.WriteLine("Attached currency " + Convert.ToString(xlsCurrencies.Cells["A" + i].Value));
                    attached++;

                    attachedGuids.Add(currencyGuid);
                }

                // delete section
                foreach (Currency currency in databaseCurrencies)
                {
                    if (!attachedGuids.Contains(currency.Guid))
                    {
                        string deletequery = QueryBuilder.GetDeleteCountryStateQuery(currency.Guid);
                        script.WriteLine(deletequery);
                        deletequery = QueryBuilder.GetDeleteQuery(currency.Guid, "Currencies");
                        script.WriteLine(deletequery);
                        Console.WriteLine("Deleted currency " + currency.Name);
                        deleted++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("Attached = " + attached + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Read Countries xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadCountrySettingXLSX(ExcelWorkbook excelWorkbook)
        {
            ExcelWorksheet xlsCountries = excelWorkbook.Worksheets["Sheet1"];

            if (xlsCountries == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Collection<Country> databaseCountries = dc.CountryRepository.GetAll();

            List<Guid> guids = databaseCountries.Select(p => p.CountrySetting.Guid).ToList();
            int atached = 0;
            int deleted = 0;

            System.IO.StreamWriter script = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.CountriesUpdateScriptPath);
            System.IO.StreamWriter log = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.UpdateLogFilePath);
            List<Guid> atachedGuids = new List<Guid>();

            try
            {
                // parse the xls, generate the insert queries for the items not present int the db
                for (int i = 2; i <= Constants.CountrySettingsCount + 1; i++)
                {
                    Guid countryGuid = new Guid(Convert.ToString(xlsCountries.Cells["B" + i].Value));

                    Guid settingsGuid = new Guid(Convert.ToString(xlsCountries.Cells["C" + i].Value));

                    if (!atachedGuids.Contains(settingsGuid))
                    {
                        List<string> countryValues = new List<string>();
                        List<string> countryFields = new List<string>();
                        List<string> settingsValues = new List<string>();
                        List<string> settingsFields = new List<string>();
                        int countryFieldsCount = 0;
                        int settingsFieldsCount = 0;
                        object value;

                        // Country Table //

                        // Name
                        value = xlsCountries.Cells["A" + i].Value;

                        countryFields.Add("Name");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                        countryFieldsCount++;

                        // CurrencyGuid
                        value = xlsCountries.Cells["D" + i].Value;

                        countryFields.Add("CurrencyGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // WeightUnitGuid
                        value = xlsCountries.Cells["E" + i].Value;

                        countryFields.Add("WeightUnitGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // LengthUnitGuid
                        value = xlsCountries.Cells["F" + i].Value;

                        countryFields.Add("LengthUnitGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // VolumeUnitGuid
                        value = xlsCountries.Cells["G" + i].Value;

                        countryFields.Add("VolumeUnitGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // TimeUnitGuid
                        value = xlsCountries.Cells["H" + i].Value;

                        countryFields.Add("TimeUnitGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // FloorUnitGuid
                        value = xlsCountries.Cells["I" + i].Value;

                        countryFields.Add("FloorUnitGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(new Guid(Convert.ToString(value)), false));
                        countryFieldsCount++;

                        // SettingsGuid
                        countryFields.Add("SettingsGuid");
                        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(settingsGuid, false));
                        countryFieldsCount++;

                        // CountrySetting Table //

                        // ShiftCharge1ShiftModel (Scrambled)
                        value = xlsCountries.Cells["J" + i].Value;

                        settingsFields.Add("ShiftCharge1ShiftModel");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // ShiftCharge2ShiftModel (Scrambled)
                        value = xlsCountries.Cells["K" + i].Value;

                        settingsFields.Add("ShiftCharge2ShiftModel");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // ShiftCharge3ShiftModel (Scrambled)
                        value = xlsCountries.Cells["L" + i].Value;

                        settingsFields.Add("ShiftCharge3ShiftModel");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // LaborAvailability
                        value = xlsCountries.Cells["M" + i].Value;

                        settingsFields.Add("LaborAvailability");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                        settingsFieldsCount++;

                        // UnskilledLaborCost (Scrambled)
                        value = xlsCountries.Cells["N" + i].Value;

                        settingsFields.Add("UnskilledLaborCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // SkilledLaborCost (Scrambled)
                        value = xlsCountries.Cells["O" + i].Value;

                        settingsFields.Add("SkilledLaborCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // ForemanCost (Scrambled)
                        value = xlsCountries.Cells["P" + i].Value;

                        settingsFields.Add("ForemanCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // TechnicianCost (Scrambled)
                        value = xlsCountries.Cells["Q" + i].Value;

                        settingsFields.Add("TechnicianCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // EngineerCost (Scrambled)
                        value = xlsCountries.Cells["R" + i].Value;

                        settingsFields.Add("EngineerCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // EnergyCost (Scrambled)
                        value = xlsCountries.Cells["S" + i].Value;

                        settingsFields.Add("EnergyCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // AirCost
                        value = xlsCountries.Cells["T" + i].Value;

                        settingsFields.Add("AirCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                        settingsFieldsCount++;

                        // WaterCost (Scrambled)
                        value = xlsCountries.Cells["U" + i].Value;

                        settingsFields.Add("WaterCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // ProductionAreaRentalCost (Scrambled)
                        value = xlsCountries.Cells["V" + i].Value;

                        settingsFields.Add("ProductionAreaRentalCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // OfficeAreaRentalCost (Scrambled)
                        value = xlsCountries.Cells["W" + i].Value;

                        settingsFields.Add("OfficeAreaRentalCost");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                        settingsFieldsCount++;

                        // InterestRate
                        value = xlsCountries.Cells["X" + i].Value;

                        settingsFields.Add("InterestRate");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                        settingsFieldsCount++;

                        // IsScrambled = 1
                        settingsFields.Add("IsScrambled");
                        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
                        settingsFieldsCount++;

                        QueryBuilder.GetUpdateOrInsertQueryForCountries(settingsFields, settingsValues, settingsFieldsCount, settingsGuid, countryFields, countryValues, countryFieldsCount, countryGuid, script);

                        Console.WriteLine("Attached country " + Convert.ToString(xlsCountries.Cells["A" + i].Value));

                        atached++;
                        atachedGuids.Add(settingsGuid);
                    }
                }

                // delete section
                foreach (Country country in databaseCountries)
                {
                    if (!atachedGuids.Contains(country.CountrySetting.Guid))
                    {
                        string deletequery = QueryBuilder.GetDeleteCountryStateQuery(country.Guid);
                        script.WriteLine(deletequery);
                        deletequery = QueryBuilder.GetDeleteQuery(country.Guid, "Countries");
                        script.WriteLine(deletequery);
                        deletequery = QueryBuilder.GetDeleteQuery(country.CountrySetting.Guid, "CountrySettings");
                        script.WriteLine(deletequery);
                        Console.WriteLine("Deleted country " + country.Name);
                        deleted++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("Atached = " + atached + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Read RawMaterials xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadMaterialsXLSX(ExcelWorkbook excelWorkbook)
        {
            ExcelWorksheet materials = excelWorkbook.Worksheets["Sheet1"];
            if (materials == null)
            {
                return;
            }

            ExcelWorksheet xlsClassifications = excelWorkbook.Worksheets["Sheet2"];
            if (xlsClassifications == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            Collection<MaterialsClassification> databaseClassifications = dc.MaterialsClassificationRepository.LoadClassificationTree();
            List<EntityClass> materialClasses = new List<EntityClass>();

            for (int i = 1; i <= Constants.MaterialsClassificationsCount; i++)
            {
                EntityClass material = new EntityClass();

                object value;
                value = xlsClassifications.Cells["A" + i].Value;
                material.Name = value.ToString();

                value = xlsClassifications.Cells["B" + i].Value;
                material.GUID = value.ToString();

                value = xlsClassifications.Cells["C" + i].Value;
                material.ID = int.Parse(value.ToString());

                value = xlsClassifications.Cells["D" + i].Value;
                material.Level = int.Parse(value.ToString());

                Guid classGuid = new Guid(Convert.ToString(material.GUID));
                MaterialsClassification classification = databaseClassifications.Where(p => p.Guid == classGuid).FirstOrDefault();

                if (classification.Parent == null)
                {
                    material.Class1 = classification.Guid;
                }
                else if (classification.Parent.Parent == null)
                {
                    material.Class2 = classification.Guid;
                    material.Class1 = classification.Parent.Guid;
                }
                else if (classification.Parent.Parent.Parent == null)
                {
                    material.Class3 = classification.Guid;
                    material.Class2 = classification.Parent.Guid;
                    material.Class1 = classification.Parent.Parent.Guid;
                }
                else if (classification.Parent.Parent.Parent.Parent == null)
                {
                    material.Class4 = classification.Guid;
                    material.Class3 = classification.Parent.Guid;
                    material.Class2 = classification.Parent.Parent.Guid;
                    material.Class1 = classification.Parent.Parent.Parent.Guid;
                }
                else
                {
                    material.Class4 = classification.Parent.Guid;
                    material.Class3 = classification.Parent.Parent.Guid;
                    material.Class2 = classification.Parent.Parent.Parent.Guid;
                    material.Class1 = classification.Parent.Parent.Parent.Parent.Guid;
                }

                materialClasses.Add(material);
            }

            materialClasses = this.GetMaterialParrents(materialClasses);

            Collection<RawMaterial> masterMaterialsDB = dc.RawMaterialRepository.GetAllMasterData();

            List<Guid> materialGuids = masterMaterialsDB.Select(p => p.Guid).ToList();
            int attached = 0;
            int deleted = 0;
            System.IO.StreamWriter script = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.RawMaterialsUpdateScriptPath);
            System.IO.StreamWriter log = new System.IO.StreamWriter(Environment.CurrentDirectory + Constants.UpdateLogFilePath);
            List<Guid> attachedGuids = new List<Guid>();

            foreach (var item in materialClasses)
            {
                Console.WriteLine(item.Name + " - " + item.GUID + " - " + item.ParentGUID);

                QueryBuilder.GetUpdateOrInsertQueryForRawMaterialClassifications(item, script);
            }

            try
            {
                // parse the xls, find the db entries which will be updated
                for (int i = 2; i <= Constants.RawMaterialsCount + 1; i++)
                {
                    object value = materials.Cells["S" + i].Value;
                    Guid guid = new Guid(value.ToString());

                    RawMaterial material = masterMaterialsDB.Where(p => p.Guid == guid).FirstOrDefault();
                    List<string> values = new List<string>();
                    List<string> fields = new List<string>();
                    int fieldsCount = 0;

                    // Name
                    value = materials.Cells["A" + i].Value;
                    fields.Add("Name");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Norm Name
                    value = materials.Cells["B" + i].Value;
                    fields.Add("NormName");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Price (Scrambled)
                    value = materials.Cells["J" + i].Value;
                    fields.Add("Price");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Classifications
                    value = materials.Cells["U" + i].Value;
                    if (value != null)
                    {
                        EntityClass matClass = materialClasses.Where(p => p.ID == int.Parse(value.ToString())).FirstOrDefault();
                        if (matClass != null)
                        {
                            if (!matClass.Class1.Equals(Guid.Empty))
                            {
                                fields.Add("ClassificationLevel1Guid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(matClass.Class1, false));
                                fieldsCount++;
                            }

                            if (!matClass.Class2.Equals(Guid.Empty))
                            {
                                fields.Add("ClassificationLevel2Guid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(matClass.Class2, false));
                                fieldsCount++;
                            }

                            if (!matClass.Class3.Equals(Guid.Empty))
                            {
                                fields.Add("ClassificationLevel3Guid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(matClass.Class3, false));
                                fieldsCount++;
                            }

                            if (!matClass.Class4.Equals(Guid.Empty))
                            {
                                fields.Add("ClassificationLevel4Guid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(matClass.Class4, false));
                                fieldsCount++;
                            }
                        }
                    }

                    ////// Class 1
                    ////value = materials.Cells["E" + i].Value;
                    ////fields.Add("ClassificationLevel1Guid");
                    ////if (int.TryParse(value.ToString(), out classID))
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(materialClasses[classID].GUID, false));
                    ////}
                    ////else
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    ////}

                    ////nrFields++;

                    ////// Class 2
                    ////value = materials.Cells["F" + i].Value;
                    ////fields.Add("ClassificationLevel2Guid");
                    ////if (int.TryParse(value.ToString(), out classID))
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(materialClasses[classID].GUID, false));
                    ////}
                    ////else
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    ////}

                    ////nrFields++;

                    ////// Class 3
                    ////value = materials.Cells["G" + i].Value;
                    ////fields.Add("ClassificationLevel3Guid");
                    ////if (int.TryParse(value.ToString(), out classID))
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(materialClasses[classID].GUID, false));
                    ////}
                    ////else
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    ////}

                    ////nrFields++;

                    ////// Class 4
                    ////value = materials.Cells["H" + i].Value;
                    ////fields.Add("ClassificationLevel4Guid");
                    ////if (int.TryParse(value.ToString(), out classID))
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(materialClasses[classID].GUID, false));
                    ////}
                    ////else
                    ////{
                    ////    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    ////}

                    ////nrFields++;

                    // Remarks
                    value = materials.Cells["I" + i].Value;
                    fields.Add("Remarks");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Name UK
                    value = materials.Cells["C" + i].Value;
                    fields.Add("NameUK");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Name US
                    value = materials.Cells["D" + i].Value;
                    fields.Add("NameUS");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // YieldStrength (Scrambled)
                    value = materials.Cells["K" + i].Value;
                    fields.Add("YieldStrength");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // RuptureStrength (Scrambled)
                    value = materials.Cells["L" + i].Value;
                    fields.Add("RuptureStrength");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Density (Scrambled)
                    value = materials.Cells["M" + i].Value;
                    fields.Add("Density");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // MaxElongation (Scrambled)
                    value = materials.Cells["N" + i].Value;
                    fields.Add("MaxElongation");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // GlassTemp (Scrambled)
                    value = materials.Cells["O" + i].Value;
                    fields.Add("GlassTransitionTemperature");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Rx (Scrambled)
                    value = materials.Cells["P" + i].Value;
                    fields.Add("Rx");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Rm (Scrambled)
                    value = materials.Cells["Q" + i].Value;
                    fields.Add("Rm");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
                    fieldsCount++;

                    // IsMasterData
                    value = materials.Cells["R" + i].Value;
                    fields.Add("IsMasterData");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    if (fieldsCount > 0)
                    {
                        fields.Add("IsScrambled");
                        values.Add(QueryBuilder.ConvertDecimalToSQLUpdateValue(1, false));
                        fieldsCount++;

                        QueryBuilder.GetUpdateOrInsertQueryForRawMaterials(fields, values, fieldsCount, guid, script);
                        Console.WriteLine("Attached " + Convert.ToString(materials.Cells["A" + i].Value));
                        attached++;

                        attachedGuids.Add(guid);
                    }
                }

                // delete section
                foreach (RawMaterial material in masterMaterialsDB)
                {
                    if (!attachedGuids.Contains(material.Guid))
                    {
                        string deletequery = QueryBuilder.GetDeleteQuery(material.Guid, "RawMaterials");

                        script.WriteLine(deletequery);
                        Console.WriteLine("Deleted " + material.Name);
                        deleted++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("Attached = " + attached + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Read Machines xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadMachinesXLSX(ExcelWorkbook excelWorkbook)
        {
            var xlsMachines = excelWorkbook.Worksheets["Sheet1"];
            if (xlsMachines == null)
            {
                return;
            }

            var xlsClassifications = excelWorkbook.Worksheets["Sheet2"];
            if (xlsClassifications == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Collection<MachinesClassification> databaseClassifications = dc.MachinesClassificationRepository.LoadClassificationTree();
            var machineClasses = new List<EntityClass>();
            for (var i = 1; i <= 150; i++)
            {
                var machineClass = new EntityClass();

                object value = xlsClassifications.Cells["A" + i].Value;
                machineClass.Name = value.ToString();

                value = xlsClassifications.Cells["B" + i].Value;
                var classGuid = new Guid(Convert.ToString(value));
                var classification = databaseClassifications.FirstOrDefault(p => p.Guid == classGuid);

                if (classification == null) continue;
                machineClass.GUID = classification.Guid.ToString();

                if (classification.Parent == null)
                {
                    machineClass.Class1 = classification.Guid;
                    machineClass.ParentGUID = null;
                }
                else if (classification.Parent.Parent == null)
                {
                    machineClass.Class2 = classification.Guid;
                    machineClass.Class1 = classification.Parent.Guid;
                    machineClass.ParentGUID = machineClass.Class1.ToString();
                }
                else if (classification.Parent.Parent.Parent == null)
                {
                    machineClass.Class3 = classification.Guid;
                    machineClass.Class2 = classification.Parent.Guid;
                    machineClass.Class1 = classification.Parent.Parent.Guid;
                    machineClass.ParentGUID = machineClass.Class2.ToString();
                }
                else
                {
                    machineClass.Class4 = classification.Guid;
                    machineClass.Class3 = classification.Parent.Guid;
                    machineClass.Class2 = classification.Parent.Parent.Guid;
                    machineClass.Class1 = classification.Parent.Parent.Parent.Guid;
                    machineClass.ParentGUID = machineClass.Class3.ToString();
                }

                machineClasses.Add(machineClass);
            }

            var newManufacturers = new List<string>();

            List<Machine> masterDataMachines = new List<Machine>(dc.MachineRepository.GetAllMasterData());
            Collection<Manufacturer> manufacturers = dc.ManufacturerRepository.GetAllMasterData();

            var manTuples = manufacturers.Select(manufacturer => new TestClass() { Manufacturer = manufacturer, Count = 0 }).ToList();

            var machineGuids = masterDataMachines.ConvertAll(p => p.Guid);
            var inserts = 0;
            var deleted = 0;
            var script = new System.IO.StreamWriter(@"D:\\update_script.sql");
            var log = new System.IO.StreamWriter(@"D:\\log.txt");
            var updatedIDs = new List<Guid>();

            try
            {
                //foreach (var machineClass in machineClasses)
                //{
                //    QueryBuilder.GetUpdateOrInsertQueryForMachineClassifications(machineClass, script);
                //}

                // parse the xls, generate the insert queries for the items not present int the db
                for (var i = 2; i <= 1385; i++)
                {
                    // FundamentalSetupInvestment (Scrambled)
                    var machman = xlsMachines.Cells["AH" + i].Value.ToString();

                    var tupleItem = manTuples.FirstOrDefault(t => t.Manufacturer.Name == machman);
                    if (tupleItem != null)
                    {
                        tupleItem.Count++;
                    }
                    else
                    {
                        if (!newManufacturers.Contains(machman))
                        {
                            newManufacturers.Add(machman);
                        }
                    }

                    var guid = new Guid(xlsMachines.Cells["B" + i].Value.ToString());

                    var values = new List<string>();
                    var fields = new List<string>();
                    var fieldsCount = 0;

                    // Manufacturer
                    var value = xlsMachines.Cells["C" + i].Value;
                    if (value != null)
                    {
                        var manufacturer = manufacturers.FirstOrDefault(p => p.Name == value.ToString());
                        if (manufacturer != null)
                        {
                            // Name
                            fields.Add("ManufacturerGuid");
                            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(manufacturer.Guid, false));
                            fieldsCount++;
                        }
                    }

                    // Name
                    value = xlsMachines.Cells["A" + i].Value;
                    fields.Add("Name");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Classifications
                    value = xlsMachines.Cells["D" + i].Value;
                    if (value != null)
                    {
                        var manclass = machineClasses.FirstOrDefault(p => p.Name == value.ToString());
                        if (manclass != null)
                        {
                            if (!manclass.Class1.Equals(Guid.Empty))
                            {
                                fields.Add("MainClassificationGuid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(manclass.Class1, false));
                                fieldsCount++;
                            }

                            if (!manclass.Class2.Equals(Guid.Empty))
                            {
                                fields.Add("TypeClassificationGuid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(manclass.Class2, false));
                                fieldsCount++;
                            }

                            if (!manclass.Class3.Equals(Guid.Empty))
                            {
                                fields.Add("SubClassificationGuid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(manclass.Class3, false));
                                fieldsCount++;
                            }

                            if (!manclass.Class4.Equals(Guid.Empty))
                            {
                                fields.Add("ClassificationLevel4Guid");
                                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(manclass.Class4, false));
                                fieldsCount++;
                            }
                        }
                    }

                    // DescriptionOfFunction
                    value = xlsMachines.Cells["E" + i].Value;
                    fields.Add("DescriptionOfFunction");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // DepreciationPeriod
                    value = xlsMachines.Cells["F" + i].Value;
                    fields.Add("DepreciationPeriod");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // DepreciationRate
                    value = xlsMachines.Cells["G" + i].Value;
                    fields.Add("DepreciationRate");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // ManufacturingYear
                    value = xlsMachines.Cells["H" + i].Value;
                    fields.Add("ManufacturingYear");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // MachineInvestment (Scrambled)
                    value = xlsMachines.Cells["I" + i].Value;
                    fields.Add("MachineInvestment");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // SetupInvestment (Scrambled)
                    value = xlsMachines.Cells["K" + i].Value;
                    fields.Add("SetupInvestment");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // AdditionalEquipmentInvestment (Scrambled)
                    value = xlsMachines.Cells["L" + i].Value;
                    fields.Add("AdditionalEquipmentInvestment");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // FundamentalSetupInvestment (Scrambled)
                    value = xlsMachines.Cells["J" + i].Value;
                    fields.Add("FundamentalSetupInvestment");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Kvalue
                    value = xlsMachines.Cells["N" + i].Value;
                    fields.Add("KValue");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    var calculatewithkvalue = 0;
                    if (value != null)
                    {
                        calculatewithkvalue = 1;
                    }

                    // CalculatewithKValue
                    fields.Add("CalculatewithKValue");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(calculatewithkvalue, false));
                    fieldsCount++;

                    // FullLoadRate
                    value = xlsMachines.Cells["O" + i].Value;
                    fields.Add("FullLoadRate");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // OEE
                    value = xlsMachines.Cells["P" + i].Value;
                    fields.Add("OEE");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // ManualConsumableCostPercentage
                    value = xlsMachines.Cells["Q" + i].Value;
                    fields.Add("ManualConsumableCostPercentage");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // FloorSize (Scrambled)
                    value = xlsMachines.Cells["R" + i].Value;
                    fields.Add("FloorSize");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // WorkspaceArea (Scrambled)
                    value = xlsMachines.Cells["S" + i].Value;
                    fields.Add("WorkspaceArea");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // PowerConsumption (Scrambled)
                    value = xlsMachines.Cells["T" + i].Value;
                    fields.Add("PowerConsumption");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
                    fieldsCount++;

                    // Availability
                    value = xlsMachines.Cells["U" + i].Value;
                    fields.Add("Availability");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
                    fieldsCount++;

                    // mounting cube lenght
                    value = xlsMachines.Cells["V" + i].Value;
                    fields.Add("MountingCubeLength");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // mountig cube width [mm]
                    value = xlsMachines.Cells["W" + i].Value;
                    fields.Add("MountingCubeWidth");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // mounting cube height[mm]
                    value = xlsMachines.Cells["X" + i].Value;
                    fields.Add("MountingCubeHeight");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Max diameter Rod / Chuck max. thickness [mm]
                    value = xlsMachines.Cells["Y" + i].Value;
                    fields.Add("MaxDiameterRodPerChuckMaxThickness");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Max Parts Weight/capacity [kg] / [l]
                    value = xlsMachines.Cells["Z" + i].Value;
                    fields.Add("MaxPartsWeightPerCapacity");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // locking force [kN]
                    value = xlsMachines.Cells["AA" + i].Value;
                    fields.Add("LockingForce");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // press capacity [kN ]
                    value = xlsMachines.Cells["AB" + i].Value;
                    fields.Add("PressCapacity");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // maximum speed [rpm]
                    value = xlsMachines.Cells["AC" + i].Value;
                    fields.Add("MaximumSpeed");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // rapid feed / cutting speed [m/min]
                    value = xlsMachines.Cells["AD" + i].Value;
                    fields.Add("RapidFeedPerCuttingSpeed");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // stroke rate [1/min]
                    value = xlsMachines.Cells["AE" + i].Value;
                    fields.Add("StrokeRate");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Other
                    value = xlsMachines.Cells["AF" + i].Value;
                    fields.Add("Other");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // IsMasterData = 1
                    fields.Add("IsMasterData");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(1, false));
                    fieldsCount++;

                    // IsScrambled = 1
                    fields.Add("IsScrambled");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(1, false));
                    fieldsCount++;

                    QueryBuilder.GetUpdateOrInsertQueryForMachines(fields, values, fieldsCount, guid, script);
                    Console.WriteLine("Inserted new machine" + i);
                    inserts++;

                    updatedIDs.Add(guid);
                }

                script.WriteLine();
                script.WriteLine("New manufacturers :");
                foreach (var newMan in newManufacturers)
                {
                    script.WriteLine(newMan);
                }

                script.WriteLine();
                script.WriteLine("Old manufacturers :");
                foreach (var item in manTuples)
                {
                    script.WriteLine(item.Manufacturer.Name + " : " + item.Count);
                }

                // delete section
                foreach (var machine in masterDataMachines)
                {
                    if (!updatedIDs.Contains(machine.Guid))
                    {
                        var deletequery = QueryBuilder.GetDeleteQuery(machine.Guid, "Machines");

                        script.WriteLine(deletequery);
                        Console.WriteLine("Delete " + machine.Name);
                        deleted++;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("\nInserts/Updates = " + inserts + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Read Manufacturers xlsx and generate update sql script
        /// </summary>
        /// <param name="excelWorkbook">ExcelWorkbook instance</param>
        private void ReadManufacturersXLSX(ExcelWorkbook excelWorkbook)
        {
            ExcelWorksheet workSheet = excelWorkbook.Worksheets["Manufacturers Central"];
            if (workSheet == null)
            {
                return;
            }

            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            List<Manufacturer> manufacturers = new List<Manufacturer>(dc.ManufacturerRepository.GetAllMasterData());

            int updates = 0;
            int deleted = 0;
            System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");
            System.IO.StreamWriter log = new System.IO.StreamWriter(@"D:\\log.txt");
            List<Guid> updatedIDs = new List<Guid>();

            try
            {
                // parse the xls, find the db entries which will be updated
                for (int i = 82; i <= 93; i++)
                {
                    object value = workSheet.Cells["D" + i].Value;
                    Guid guid = new Guid(value.ToString());

                    List<string> values = new List<string>();
                    List<string> fields = new List<string>();
                    int fieldsCount = 0;

                    // Name
                    value = workSheet.Cells["A" + i].Value;
                    fields.Add("Name");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // Description
                    value = value = workSheet.Cells["B" + i].Value;
                    fields.Add("Description");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;

                    // IsMasterData = 1
                    fields.Add("IsMasterData");
                    values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
                    fieldsCount++;

                    QueryBuilder.GetUpdateOrInsertQueryForManufacturers(fields, values, fieldsCount, guid, script);

                    updatedIDs.Add(guid);
                }

                foreach (Manufacturer manufacturer in manufacturers)
                {
                    if (!updatedIDs.Contains(manufacturer.Guid))
                    {
                        string deletequery = QueryBuilder.GetDeleteQuery(manufacturer.Guid, "Manufacturers");

                        script.WriteLine(deletequery);
                        Console.WriteLine("Delete " + manufacturer.Name);
                        deleted++;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ! Please check the log file");
                log.WriteLine(ex.StackTrace);
            }
            finally
            {
                script.Close();
                log.Close();
            }

            Console.WriteLine("Updates = " + updates + "\nDeleted = " + deleted);
        }

        /// <summary>
        /// Found Parents for existing materials
        /// </summary>
        /// <param name="materialClasses">database materials</param>
        /// <returns>materials with parents added</returns>
        private List<EntityClass> GetMaterialParrents(List<EntityClass> materialClasses)
        {
            for (int i = 0; i < materialClasses.Count; i++)
            {
                if (!materialClasses[i].Level.Equals(1))
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        if (materialClasses[i].Level.Equals(materialClasses[j].Level + 1))
                        {
                            materialClasses[i].ParentGUID = materialClasses[j].GUID;
                            break;
                        }
                    }
                }
            }

            return materialClasses;
        }

        private void SerializeMachinesImages(ExcelWorkbook excelWorkbook)
        {
            var xlsMachines = excelWorkbook.Worksheets["Sheet1"];
            if (xlsMachines == null)
            {
                return;
            }

            using (var file = new System.IO.StreamWriter(@"D:\\InsertMachinesPictures.sql"))
            {
                var picturesCount = 0;
                var machinesWithNoPicture = 0;
                for (var i = 2; i <= 1385; i++)
                {
                    var guid = new Guid(xlsMachines.Cells["B" + i].Value.ToString());
                    var path = xlsMachines.Cells["AG" + i].Value as string;
                    if (path != null)
                    {
                        if (i == 997)
                        {

                        }
                        var files = Directory.EnumerateFiles(@"D:\\MachinesXX\\", (path ?? string.Empty) + "*",
                                                             SearchOption.AllDirectories);

                        var machinePath = files.FirstOrDefault();

                        if (i == 1217 || i == 1218)
                        {
                            path = "PIC_M0472";

                            files = Directory.EnumerateFiles(@"D:\\MachinesXX\\", (path ?? string.Empty) + "*",
                                                             SearchOption.AllDirectories);
                            machinePath = files.FirstOrDefault();
                        }

                        if (machinePath == null) continue;

                        var pictureNameStartIndex = machinePath.IndexOf("PIC", System.StringComparison.Ordinal);
                        var pictureName = machinePath.Substring(pictureNameStartIndex,
                                                                machinePath.Length - pictureNameStartIndex);

                        var imagePath = @"$(MDMachinesPicsPath)";

                        QueryBuilder.GetUpdateQueryForMachinePicture(guid, imagePath, pictureName, file, i);
                        Console.WriteLine(i);
                        picturesCount++;
                    }
                    else
                    {
                        QueryBuilder.GetUpdateQueryForMachinePicture(guid, string.Empty, string.Empty, file, i);
                        Console.WriteLine(i + " - does not have a picture.");
                        machinesWithNoPicture++;
                    }
                }

                Console.WriteLine("Pictures count : " + picturesCount);
                Console.WriteLine("Machines with no picture : " + machinesWithNoPicture);

                file.Close();
            }

            Console.WriteLine("Done!");
        }

        public string ImageToString(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return string.Empty;
            }

            var data = File.ReadAllBytes(path);
            return Convert.ToBase64String(data);
        }

        #endregion Read from xlsx

        #region Read from DB

        /// <summary>
        /// Read Manufacturers from DB
        /// </summary>
        private void ReadManufacturersDB()
        {
            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Collection<Manufacturer> manufacturersDB = dc.ManufacturerRepository.GetAllMasterData();
            System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            foreach (Manufacturer manufacturer in manufacturersDB)
            {
                List<string> values = new List<string>();
                List<string> fields = new List<string>();
                int fieldsCount = 0;
                object value;

                // Name
                value = manufacturer.Name;
                fields.Add("Name");
                values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                fieldsCount++;

                // Description
                if (manufacturer.Description != null)
                {
                    value = manufacturer.Description;
                    fields.Add("Description");
                    values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
                    fieldsCount++;
                }

                // IsMasterData = 1
                fields.Add("IsMasterData");
                values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
                fieldsCount++;

                QueryBuilder.GetUpdateOrInsertQueryForManufacturers(fields, values, fieldsCount, manufacturer.Guid, script);
            }

            script.Close();
        }

        /// <summary>
        /// Read Customers from DB
        /// </summary>
        private void ReadCustomersDB()
        {
            ////    ContextManager contextManager = PersistenceFactory.CreateCentralContextManager();
            ////    List<Customer> dbCustomers = contextManager.EntityPersistence.GetAll<Customer>();
            ////    System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            ////    foreach (Customer customer in dbCustomers)
            ////    {
            ////        List<string> values = new List<string>();
            ////        List<string> fields = new List<string>();
            ////        int nrFields = 0;
            ////        object value;

            ////        // Name
            ////        value = customer.Name;
            ////        fields.Add("Name");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // Description
            ////        if (customer.Description != null)
            ////        {
            ////            value = customer.Description;
            ////            fields.Add("Description");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        // CountryName
            ////        if (customer.Country != null)
            ////        {
            ////            value = customer.Country;
            ////            fields.Add("CountryName");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        // IsMasterData = 1
            ////        fields.Add("IsMasterData");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrFields++;

            ////        QueryBuilder.GetUpdateOrInsertQueryForCustomers(fields, values, nrFields, customer.Guid, script);
            ////    }

            ////    script.Close();
        }

        /// <summary>
        /// Read Machines from DB
        /// </summary>
        private void ReadMachinesDB()
        {
            ////    ContextManager contextManager = PersistenceFactory.CreateCentralContextManager();
            ////    List<Machine> dbMachines = contextManager.MachinesPersistence.GetAll<Machine>();
            ////    System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            ////    int machineIndex = dbMachines.Max(p => p.Index).Value;

            ////    foreach (Machine machine in dbMachines)
            ////    {
            ////        List<string> values = new List<string>();
            ////        List<string> fields = new List<string>();
            ////        int nrFields = 0;
            ////        object value;

            ////        value = machine.Name;
            ////        fields.Add("Name");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        //Manufacturer
            ////        if (machine.Manufacturer != null)
            ////        {
            ////            value = machine.Manufacturer.Guid;
            ////            fields.Add("ManufacturerGuid");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        //Classifications
            ////        if (machine.MainClassification != null)
            ////        {
            ////            value = machine.MainClassification.Guid;
            ////            fields.Add("MainClassificationGuid");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        if (machine.TypeClassification != null)
            ////        {
            ////            value = machine.TypeClassification.Guid;
            ////            fields.Add("TypeClassificationGuid");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        if (machine.SubClassification != null)
            ////        {
            ////            value = machine.SubClassification.Guid;
            ////            fields.Add("SubClassificationGuid");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        if (machine.ClassificationLevel4 != null)
            ////        {
            ////            value = machine.ClassificationLevel4.Guid;
            ////            fields.Add("ClassificationLevel4Guid");
            ////            values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrFields++;
            ////        }

            ////        // DescriptionOfFunction
            ////        value = machine.DescriptionOfFunction;
            ////        fields.Add("DescriptionOfFunction");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // DepreciationPeriod
            ////        value = machine.DepreciationPeriod;
            ////        fields.Add("DepreciationPeriod");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // DepreciationRate
            ////        value = machine.DepreciationRate;
            ////        fields.Add("DepreciationRate");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // ManufacturingYear
            ////        value = machine.ManufacturingYear;
            ////        fields.Add("ManufacturingYear");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // MachineInvestment (Scrambled)
            ////        value = machine.MachineInvestment;
            ////        fields.Add("MachineInvestment");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // SetupInvestment (Scrambled)
            ////        value = machine.SetupInvestment;
            ////        fields.Add("SetupInvestment");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // AdditionalEquipmentInvestment (Scrambled)
            ////        value = machine.AdditionalEquipmentInvestment;
            ////        fields.Add("AdditionalEquipmentInvestment");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // FundamentalSetupInvestment (Scrambled)
            ////        value = machine.FundamentalSetupInvestment;
            ////        fields.Add("FundamentalSetupInvestment");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // Kvalue
            ////        value = machine.KValue;
            ////        fields.Add("KValue");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // CalculatewithKValue
            ////        bool boolValue = machine.CalculateWithKValue.Value;
            ////        if (boolValue == true)
            ////        {
            ////            value = 1;
            ////        }
            ////        else
            ////        {
            ////            value = 0;
            ////        }

            ////        fields.Add("CalculatewithKValue");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // FullLoadRate
            ////        value = machine.FullLoadRate;
            ////        fields.Add("FullLoadRate");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // OEE
            ////        value = machine.OEE;
            ////        fields.Add("OEE");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // ManualConsumableCostPercentage
            ////        value = machine.ManualConsumableCostPercentage;
            ////        fields.Add("ManualConsumableCostPercentage");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // FloorSize (Scrambled)
            ////        value = machine.FloorSize;
            ////        fields.Add("FloorSize");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // WorkspaceArea (Scrambled)
            ////        value = machine.WorkspaceArea;
            ////        fields.Add("WorkspaceArea");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // PowerConsumption (Scrambled)
            ////        value = machine.PowerConsumption;
            ////        fields.Add("PowerConsumption");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrFields++;

            ////        // Availability
            ////        value = machine.Availability;
            ////        fields.Add("Availability");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // mounting cube lenght
            ////        value = machine.MountingCubeLength;
            ////        fields.Add("MountingCubeLength");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // mountig cube width [mm]
            ////        value = machine.MountingCubeWidth;
            ////        fields.Add("MountingCubeWidth");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // mounting cube height[mm]
            ////        value = machine.MountingCubeHeight;
            ////        fields.Add("MountingCubeHeight");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // Max diameter Rod / Chuck max. thickness [mm]
            ////        value = machine.MaxDiameterRodPerChuckMaxThickness;
            ////        fields.Add("MaxDiameterRodPerChuckMaxThickness");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // Max Parts Weight/capacity [kg] / [l]
            ////        value = machine.MaxPartsWeightPerCapacity;
            ////        fields.Add("MaxPartsWeightPerCapacity");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // locking force [kN]
            ////        value = machine.LockingForce;
            ////        fields.Add("LockingForce");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // press capacity [kN ]
            ////        value = machine.PressCapacity;
            ////        fields.Add("PressCapacity");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // maximum speed [rpm]
            ////        value = machine.MaximumSpeed;
            ////        fields.Add("MaximumSpeed");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // rapid feed / cutting speed [m/min]
            ////        value = machine.RapidFeedPerCuttingSpeed;
            ////        fields.Add("RapidFeedPerCuttingSpeed");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // stroke rate [1/min]
            ////        value = machine.StrokeRate;
            ////        fields.Add("StrokeRate");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // Other
            ////        value = machine.Other;
            ////        fields.Add("Other");
            ////        values.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrFields++;

            ////        // IsMasterData = 1
            ////        fields.Add("IsMasterData");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrFields++;

            ////        // IsScrambled = 1
            ////        fields.Add("IsScrambled");
            ////        values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrFields++;

            ////        if (machine.Index == null)
            ////        {
            ////            machineIndex++;
            ////            fields.Add("[Index]");
            ////            values.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(machineIndex, false));
            ////            nrFields++;
            ////        }

            ////        QueryBuilder.GetUpdateOrInsertQueryForMachines(fields, values, nrFields, machine.Guid, script);
            ////    }

            ////    script.Close();
        }

        /// <summary>
        /// Read Rawmaterials from DB
        /// </summary>
        private void ReadRawMaterialsDB()
        {
            ////    ContextManager contextManager = PersistenceFactory.CreateCentralContextManager();
            ////    List<RawMaterial> dbMaterials = contextManager.RawMaterialsPersistence.GetAll<RawMaterial>().OrderBy(p => p.NormName).ToList();
            ////    System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            ////    foreach (RawMaterial material in dbMaterials)
            ////    {
            ////        List<string> materialValues = new List<string>();
            ////        List<string> materialFields = new List<string>();
            ////        int nrMaterialFields = 0;
            ////        object value;

            ////        value = material.Name;
            ////        materialFields.Add("Name");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrMaterialFields++;

            ////        // Norm Name
            ////        value = material.NormName;
            ////        materialFields.Add("NormName");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrMaterialFields++;

            ////        // Price (Scrambled)
            ////        value = material.Price;
            ////        materialFields.Add("Price");
            ////        materialValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // Class 1
            ////        if (material.MaterialsClassificationL1 != null)
            ////        {
            ////            value = material.MaterialsClassificationL1.Guid;
            ////            materialFields.Add("ClassificationLevel1Guid");
            ////            materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrMaterialFields++;
            ////        }

            ////        // Class 2
            ////        if (material.MaterialsClassificationL2 != null)
            ////        {
            ////            value = material.MaterialsClassificationL2.Guid;
            ////            materialFields.Add("ClassificationLevel2Guid");
            ////            materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrMaterialFields++;
            ////        }

            ////        // Class 3
            ////        if (material.MaterialsClassificationL3 != null)
            ////        {
            ////            value = material.MaterialsClassificationL3.Guid;
            ////            materialFields.Add("ClassificationLevel3Guid");
            ////            materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrMaterialFields++;
            ////        }

            ////        // Class 4
            ////        if (material.MaterialsClassificationL4 != null)
            ////        {
            ////            value = material.MaterialsClassificationL4.Guid;
            ////            materialFields.Add("ClassificationLevel4Guid");
            ////            materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////            nrMaterialFields++;
            ////        }

            ////        // Remarks
            ////        value = material.Remarks;
            ////        materialFields.Add("Remarks");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrMaterialFields++;

            ////        // Name UK
            ////        value = material.NameUK;
            ////        materialFields.Add("NameUK");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrMaterialFields++;

            ////        // Name US
            ////        value = material.NameUS;
            ////        materialFields.Add("NameUS");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrMaterialFields++;

            ////        // YieldStrength (Scrambled)
            ////        value = material.YieldStrength;
            ////        materialFields.Add("YieldStrength");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // RuptureStrength (Scrambled)
            ////        value = material.RuptureStrength;
            ////        materialFields.Add("RuptureStrength");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // Density (Scrambled)
            ////        value = material.Density;
            ////        materialFields.Add("Density");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // MaxElongation (Scrambled)
            ////        value = material.MaxElongation;
            ////        materialFields.Add("MaxElongation");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // GlassTemp (Scrambled)
            ////        value = material.GlassTransitionTemperature;
            ////        materialFields.Add("GlassTransitionTemperature");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // Rx (Scrambled)
            ////        value = material.Rx;
            ////        materialFields.Add("Rx");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // Rm (Scrambled)
            ////        value = material.Rm;
            ////        materialFields.Add("Rm");
            ////        materialValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, true));
            ////        nrMaterialFields++;

            ////        // IsMasterData
            ////        materialFields.Add("IsMasterData");
            ////        materialValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrMaterialFields++;

            ////        // IsScrambled = 1
            ////        materialFields.Add("IsScrambled");
            ////        materialValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrMaterialFields++;

            ////        QueryBuilder.GetUpdateOrInsertQueryForRawMaterials(materialFields, materialValues, nrMaterialFields, material.Guid, script);
            ////    }

            ////    script.Close();
        }

        /// <summary>
        /// Read Currencies from DB
        /// </summary>
        private void ReadCurrenciesDB()
        {
            ////    ContextManager contextManager = PersistenceFactory.CreateCentralContextManager();
            ////    List<Currency> dbCurrencies = contextManager.CountryPersistence.GetAll<Currency>().OrderBy(p => p.Name).ToList();
            ////    System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            ////    foreach (Currency currency in dbCurrencies)
            ////    {
            ////        List<string> currencyValues = new List<string>();
            ////        List<string> currencyFields = new List<string>();
            ////        int nrCurrencyFields = 0;
            ////        object value;

            ////        value = currency.Name;
            ////        currencyFields.Add("Name");
            ////        currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCurrencyFields++;

            ////        // Symbol
            ////        value = currency.Symbol;
            ////        currencyFields.Add("Symbol");
            ////        currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCurrencyFields++;

            ////        // ExchangeRate
            ////        value = currency.ExchangeRate;
            ////        currencyFields.Add("ExchangeRate");
            ////        currencyValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCurrencyFields++;

            ////        QueryBuilder.GetUpdateOrInsertQueryForCurrencies(currencyFields, currencyValues, nrCurrencyFields, currency.Guid, script);
            ////    }

            ////    script.Close();
        }

        /// <summary>
        /// Read Country Settings from DB
        /// </summary>
        private void ReadCountrySettingsDB()
        {
            ////    ContextManager contextManager = PersistenceFactory.CreateCentralContextManager();
            ////    List<Country> dbCountries = contextManager.CountryPersistence.GetAll<Country>().OrderBy(p => p.Name).ToList();
            ////    System.IO.StreamWriter script = new System.IO.StreamWriter(@"D:\\update_script.sql");

            ////    foreach (Country country in dbCountries)
            ////    {
            ////        List<string> countryValues = new List<string>();
            ////        List<string> countryFields = new List<string>();
            ////        List<string> settingsValues = new List<string>();
            ////        List<string> settingsFields = new List<string>();
            ////        int nrCountryFields = 0;
            ////        int nrSettingsFields = 0;
            ////        object value;

            ////        // Country //
            ////        // Name
            ////        value = country.Name;
            ////        countryFields.Add("Name");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // CurrencyGuid
            ////        value = country.Currency.Guid;
            ////        countryFields.Add("CurrencyGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // WeightUnitID
            ////        value = country.WeightMeasurementUnit.Guid;
            ////        countryFields.Add("WeightUnitGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // LengthUnitID
            ////        value = country.LengthMeasurementUnit.Guid;
            ////        countryFields.Add("LengthUnitGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // VolumeUnitID
            ////        value = country.VolumeMeasurementUnit.Guid;
            ////        countryFields.Add("VolumeUnitGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // TimeUnitID
            ////        value = country.TimeMeasurementUnit.Guid;
            ////        countryFields.Add("TimeUnitGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // FloorUnitID
            ////        value = country.FloorMeasurementUnit.Guid;
            ////        countryFields.Add("FloorUnitGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // SettingsID
            ////        value = country.CountrySetting.Guid;
            ////        countryFields.Add("SettingsGuid");
            ////        countryValues.Add(QueryBuilder.ConvertStringToSQLInsertValue(value, false));
            ////        nrCountryFields++;

            ////        // CountrySetting //

            ////        // ShiftCharge1ShiftModel (Scrambled)
            ////        value = country.CountrySetting.ShiftCharge1ShiftModel;
            ////        settingsFields.Add("ShiftCharge1ShiftModel");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // ShiftCharge2ShiftModel (Scrambled)
            ////        value = country.CountrySetting.ShiftCharge2ShiftModel;
            ////        settingsFields.Add("ShiftCharge2ShiftModel");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // ShiftCharge3ShiftModel (Scrambled)
            ////        value = country.CountrySetting.ShiftCharge3ShiftModel;
            ////        settingsFields.Add("ShiftCharge3ShiftModel");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // LaborAvailability
            ////        value = country.CountrySetting.LaborAvailability;
            ////        settingsFields.Add("LaborAvailability");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrSettingsFields++;

            ////        // UnskilledLaborCost (Scrambled)
            ////        value = country.CountrySetting.UnskilledLaborCost;
            ////        settingsFields.Add("UnskilledLaborCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // SkilledLaborCost (Scrambled)
            ////        value = country.CountrySetting.SkilledLaborCost;
            ////        settingsFields.Add("SkilledLaborCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // ForemanCost (Scrambled)
            ////        value = country.CountrySetting.ForemanCost;
            ////        settingsFields.Add("ForemanCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // TechnicianCost (Scrambled)
            ////        value = country.CountrySetting.TechnicianCost;
            ////        settingsFields.Add("TechnicianCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // EngineerCost (Scrambled)
            ////        value = country.CountrySetting.EngineerCost;
            ////        settingsFields.Add("EngineerCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // EnergyCost (Scrambled)
            ////        value = country.CountrySetting.EnergyCost;
            ////        settingsFields.Add("EnergyCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // AirCost
            ////        value = country.CountrySetting.AirCost;
            ////        settingsFields.Add("AirCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrSettingsFields++;

            ////        // WaterCost (Scrambled)
            ////        value = country.CountrySetting.WaterCost;
            ////        settingsFields.Add("WaterCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // ProductionAreaRentalCost (Scrambled)
            ////        value = country.CountrySetting.ProductionAreaRentalCost;
            ////        settingsFields.Add("ProductionAreaRentalCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // OfficeAreaRentalCost (Scrambled)
            ////        value = country.CountrySetting.OfficeAreaRentalCost;
            ////        settingsFields.Add("OfficeAreaRentalCost");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, true));
            ////        nrSettingsFields++;

            ////        // InterestRate
            ////        value = country.CountrySetting.InterestRate;
            ////        settingsFields.Add("InterestRate");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(value, false));
            ////        nrSettingsFields++;

            ////        // IsScrambled = 1
            ////        settingsFields.Add("IsScrambled");
            ////        settingsValues.Add(QueryBuilder.ConvertDecimalToSQLInsertValue(1, false));
            ////        nrSettingsFields++;

            ////        QueryBuilder.GetUpdateOrInsertQueryForCountries(settingsFields, settingsValues, nrSettingsFields, country.CountrySetting.Guid,
            ////                                                        countryFields, countryValues, nrCountryFields, country.Guid, script);
            ////    }

            ////    script.Close();
        }

        #endregion Read from DB
    }

    public class TestClass
    {
        public Manufacturer Manufacturer { get; set; }

        public int Count { get; set; }
    }
}