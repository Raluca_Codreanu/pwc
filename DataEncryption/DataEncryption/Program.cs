﻿using System;
using System.Data.SqlClient;

namespace DataEncryption
{
    class Program
    {
        /// <summary>
        /// The password used to encrypt the users data.
        /// </summary>
        public const string EncryptionPassword = "E295{qS7";

        static void Main(string[] args)
        {
            var server = string.Empty;
            var database = string.Empty;
            var autenthicationMode = AutenthicationMode.Windows;
            var username = string.Empty;
            var password = string.Empty;

            // Read the parameters.
            foreach (var arg in args)
            {
                if (arg.StartsWith("/server=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    server = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/database=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    database = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/username=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    username = GetArgumentValue(arg);
                }
                else if (arg.StartsWith("/password=", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    password = GetArgumentValue(arg);
                }
            }

            // Check if all the necessary parameters were supplied.
            if (string.IsNullOrWhiteSpace(server))
            {
                Console.WriteLine("The server name is missing. Cannot continue ...");
            }
            else if (string.IsNullOrWhiteSpace(database))
            {
                Console.WriteLine("The database name is missing. Cannot continue ...");
            }
            else if (string.IsNullOrWhiteSpace(username)
                && !string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("The username is missing but the password is present. Cannot continue ...");
            }
            else if (!string.IsNullOrWhiteSpace(username)
                && string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("The password is missing but the username is present. Cannot continue ...");
            }
            else
            {
                // If the username and password were provided use the SQL Server authentication mode.
                if (!string.IsNullOrWhiteSpace(username)
                    && !string.IsNullOrWhiteSpace(password))
                {
                    autenthicationMode = AutenthicationMode.SQLServer;
                }

                // Build the connection string.
                var connectionString = string.Format(@"Server={0};Database={1};", server, database);
                if (autenthicationMode == AutenthicationMode.Windows)
                {
                    connectionString += "Trusted_Connection=True;";
                }
                else
                {
                    connectionString += string.Format(@"User Id={1};Password={2};", username, password);
                }

                try
                {
                    // Encrypt the users data.
                    if (EncryptUsersData(connectionString))
                    {
                        Console.WriteLine("The user data was already encrypted.");
                    }
                    else
                    {
                        Console.WriteLine("The user data was successfully encrypted.");
                    }

                    // Encrypt the global settings data.
                    EncryptGlobalSettings(connectionString);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        /// <summary>
        /// Encrypts the users username and roles.
        /// </summary>
        /// <param name="sqlConnString">The SQL connection string.</param>
        /// <returns>True if the data is already encrypted, false otherwise.</returns>
        private static bool EncryptUsersData(string sqlConnString)
        {
            bool isDataEncrypted = false;

            using (SqlConnection connection = new SqlConnection(sqlConnString))
            {
                connection.Open();

                // Test to see if the usernames are encrypted by trying to decrypt one of them.
                using (SqlCommand sqlCommandSelectUsername = new SqlCommand("SELECT TOP 1 Username FROM Users", connection))
                {
                    var username = sqlCommandSelectUsername.ExecuteScalar().ToString();

                    try
                    {
                        Encryption.Decrypt(username, EncryptionPassword);
                        isDataEncrypted = true;
                    }
                    catch
                    {
                    }
                }

                if (!isDataEncrypted)
                {
                    using (SqlCommand sqlCommand = new SqlCommand("SELECT Guid, Username, Roles FROM Users", connection))
                    {
                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            using (var updateConnection = new SqlConnection(sqlConnString))
                            {
                                updateConnection.Open();
                                using (SqlTransaction transaction = updateConnection.BeginTransaction())
                                {
                                    try
                                    {
                                        while (reader.Read())
                                        {
                                            var username = reader["Username"].ToString();
                                            var roles = reader["Roles"].ToString();
                                            var userId = reader["Guid"].ToString();
                                            string usernameEncrypt = Encryption.Encrypt(username, EncryptionPassword);
                                            string rolesEncrypt = Encryption.Encrypt(int.Parse(roles).ToString(), EncryptionPassword);
                                            SqlCommand sqlCommandUpdate = new SqlCommand("UPDATE Users SET Username = @usernameEncrypt, Roles = @rolesEncrypt WHERE Guid = @userId", updateConnection);
                                            sqlCommandUpdate.Transaction = transaction;
                                            var parameterUserId = new SqlParameter("@userId", userId);
                                            var parameterUsername = new SqlParameter("@usernameEncrypt", usernameEncrypt);
                                            var parameterRoles = new SqlParameter("@rolesEncrypt", rolesEncrypt);
                                            sqlCommandUpdate.Parameters.Add(parameterUserId);
                                            sqlCommandUpdate.Parameters.Add(parameterUsername);
                                            sqlCommandUpdate.Parameters.Add(parameterRoles);
                                            sqlCommandUpdate.ExecuteNonQuery();
                                        }

                                        transaction.Commit();
                                    }
                                    catch
                                    {
                                        transaction.Rollback();
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return isDataEncrypted;
        }

        /// <summary>
        /// Encrypts the global settings keys and the values.
        /// </summary>
        /// <param name="sqlConnString">The SQL connection string.</param>
        private static void EncryptGlobalSettings(string sqlConnString)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnString))
            {
                connection.Open();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT Guid, [Key], Value FROM GlobalSettings", connection))
                {
                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        using (var updateConnection = new SqlConnection(sqlConnString))
                        {
                            updateConnection.Open();
                            using (SqlTransaction transaction = updateConnection.BeginTransaction())
                            {
                                try
                                {
                                    while (reader.Read())
                                    {
                                        var key = reader["Key"].ToString();
                                        var value = reader["Value"].ToString();
                                        var keyEncrypt = key;
                                        var valueEncrypt = value;
                                        try
                                        {
                                            Encryption.Decrypt(key, EncryptionPassword);
                                        }
                                        catch
                                        {
                                            keyEncrypt = Encryption.Encrypt(key, EncryptionPassword);
                                        }

                                        try
                                        {
                                            Encryption.Decrypt(value, EncryptionPassword);
                                        }
                                        catch
                                        {
                                            valueEncrypt = Encryption.Encrypt(value, EncryptionPassword);
                                        }

                                        if (!keyEncrypt.Equals(key)
                                            || !valueEncrypt.Equals(value))
                                        {
                                            var settingId = reader["Guid"].ToString();
                                            SqlCommand sqlCommandUpdate = new SqlCommand("UPDATE GlobalSettings SET [Key] = @keyEncrypt, Value = @valueEncrypt WHERE Guid = @settingId", updateConnection);
                                            sqlCommandUpdate.Transaction = transaction;
                                            var parameterId = new SqlParameter("@settingId", settingId);
                                            var parameterKey = new SqlParameter("@keyEncrypt", keyEncrypt);
                                            var parameterValue = new SqlParameter("@valueEncrypt", valueEncrypt);
                                            sqlCommandUpdate.Parameters.Add(parameterId);
                                            sqlCommandUpdate.Parameters.Add(parameterKey);
                                            sqlCommandUpdate.Parameters.Add(parameterValue);
                                            sqlCommandUpdate.ExecuteNonQuery();
                                        }
                                    }

                                    transaction.Commit();
                                }
                                catch
                                {
                                    transaction.Rollback();
                                    throw;
                                }
                            }
                        }
                    }
                }
            }
        }

        static string GetArgumentValue(string arg)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                return string.Empty;
            }

            return arg.Replace(arg.Remove(arg.IndexOf('=') + 1), string.Empty);
        }
    }
}