﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Defines global constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The max value of the textboxes if they contain numbers.
        /// </summary>
        public const decimal MaxNumberValue = 1000000000m;

        /// <summary>
        /// The max value for the smaller text boxes.
        /// </summary>
        public const decimal MaxSmallValue = 999999.99m;

        /// <summary>
        /// Font size for titles across the application.
        /// </summary>
        public const double TitleSize = 28;

        /// <summary>
        /// Font size for subtitles across the application.
        /// </summary>
        public const double SubtitleSize = 16;

        /// <summary>
        /// Font size for normal text across the application.
        /// </summary>
        public const double LabelSize = 12;

        /// <summary>
        /// Font size for text in input boxes across the application.
        /// </summary>
        public const double InputTextSize = 12;

        /// <summary>
        /// Font size for group box titles across the application.
        /// </summary>
        public const double GroupBoxTitleSize = 12;

        /// <summary>
        /// Font size for tab tiles across the application.
        /// </summary>
        public const double TabLabelSize = 12;

        // TODO - internationalize the labels in the selection

        /// <summary>
        /// The filter used by open file dialogs to browse for documents.
        /// Must not contain new line characters.
        /// </summary>
        public const string DialogFilterForOpenDocument =
            "PDF Files" + "|" + FileExtensionsFilters.PDFFiles + "|" +
            "Microsof Word Files" + "|" + FileExtensionsFilters.MSWordFiles + "|" + 
            "Microsof Excel Files" + "|" + FileExtensionsFilters.MSExcelFiles + "|" + 
            "Microsof Powerpoint Files" + "|" + FileExtensionsFilters.MSPowerPointFiles + "|" + 
            "OpenDocument Files" + "|" + FileExtensionsFilters.OpenDocumentFiles + "|" + 
            "All Documents" + "|" + FileExtensionsFilters.AllDocuments + "|" +
            "All Files" + "|" + FileExtensionsFilters.AllFiles;

        /// <summary>
        /// Filter used by a dialog to browse all files.
        /// Must not contain new line characters.
        /// </summary>
        public const string DialogFilterAllFiles = "All Files|*.*";

        /// <summary>
        /// The filter for open file dialogs that displays all media supported by the application.
        /// Must not contain new line characters.
        /// </summary>
        public const string DialogFilterAllMedia = "Image and Video Files (*.jpeg,*.jpg,*.gif,*.png,*.avi;*.wmv;*.asf;*.mpg)|*.jpg;*.jpeg;*.gif;*.png;*.avi;*.wmv;*.asf;*.mpg|Image Files (*.jpeg,*.jpg,*.gif,*.png)|*.jpg;*.jpeg;*.gif;*.png|Video Files (*.avi;*.wmv;*.asf;*.mpg)|*.avi;*.wmv;*.asf;*.mpg|JPEG Files(*.jpg;*.jpeg)|*.jpg;*.jpeg|GIF Files(*.gif)|*.gif|PNG Files(*.png)|*.png|AVI Files(*.avi)|*.avi|WMV Files(*.wmv)|*.wmv|ASF Files(*.asf)|*.asf|MPG Files(*.mpg)|*.mpg";

        /// <summary>
        /// The filter for open file dialogs that displays all images supported by the application.
        /// Must not contain new line characters.
        /// </summary>
        public const string DialogFilterImages = "Image Files (*.jpeg,*.jpg,*.gif,*.png)|*.jpg;*.jpeg;*.gif;*.png|JPEG Files(*.jpg;*.jpeg)|*.jpg;*.jpeg|GIF Files(*.gif)|*.gif|PNG Files(*.png)|*.png";
                
        /// <summary>
        /// The file extension filter for MS Excel documents.
        /// </summary>
        public const string DialogFilterForExcelDocs = "Microsoft Excel Files (*.xls,*.xlsx)|*.xls;*.xlsx;";

        /// <summary>
        /// The file extension filter for PDF documents.
        /// </summary>
        public const string DialogFilterForPDFDocs = "PDF Files (*.pdf)|*.pdf;";

        /// <summary>
        /// The maximum size a video can have in the application, in bytes.
        /// </summary>
        public const int MaxVideoSize = 52428800; // 50MB

        /// <summary>
        /// The maximum size a document can have in the application, in bytes.
        /// </summary>
        public const int MaxDocumentSize = 52428800; // 50MB

        /// <summary>
        /// The name of the folder on the disk where the video files will be cached.
        /// </summary>
        public const string VideoCacheFolderName = "VideoCache";

        /// <summary>
        /// Encryption key used for decrypting the password
        /// </summary>
        public const string PasswordEncryptionKey = "BVf4jSKZ762fo1FyKd79WWeGITZMLRW7H0At2ES11otN1VZueoifQBD6";

        /// <summary>
        /// The default padding applied to buttons in the application for a consistent look.
        /// </summary>
        public static readonly Thickness DefaultButtonPadding = new Thickness(9, 1, 9, 1);
                
        #region Process constants
        /// <summary>
        /// The default value for the Setup Time property of a process step.
        /// </summary>
        public const int DefaultSetupsPerBatch = 1;

        /// <summary>
        /// The default value for the Setup Time property of a process step.
        /// </summary>
        public const decimal DefaultSetupTime = 0;

        /// <summary>
        /// The default value for the Max Downtime property of a process step.
        /// </summary>
        public const decimal DefaultMaxDowntime = 0;

        /// <summary>
        /// The default value for the Amount of Scrap property of a process step.
        /// It represents a percentage so it should be between 0 and 1 
        /// having at most 4 decimal places (10% is 0.1, etc).
        /// </summary>
        public const decimal DefaultScrapAmount = 0;

        /// <summary>
        /// The default value for the Rework Ratio property of a process step.
        /// It represents a percentage so it should be between 0 and 1 
        /// having at most 4 decimal places (10% is 0.1, etc).
        /// </summary>
        public const decimal DefaultReworkRatio = 0;

        #endregion
                
        /// <summary>
        /// Defines extension filters for file dialogs.
        /// </summary>
        public static class FileExtensionsFilters
        {
            /// <summary>
            /// Acrobat reader files
            /// </summary>
            public const string PDFFiles = "*.pdf";

            /// <summary>
            /// The extensions of Microsoft Word files.
            /// </summary>
            public const string MSWordFiles = "*.doc;*.docx";

            /// <summary>
            /// The extensions of Microsoft Excel files.
            /// </summary>
            public const string MSExcelFiles = "*.xls;*.xlsx";

            /// <summary>
            /// The extensions of Microsoft Powerpoint files.
            /// </summary>
            public const string MSPowerPointFiles = "*.ppt;*.pptx";

            /// <summary>
            /// The extensions of Open Document files.
            /// </summary>
            public const string OpenDocumentFiles = "*.odt;*.ods;*.odp";

            /// <summary>
            /// Filter for all documents
            /// </summary>
            public const string AllDocuments = "*.pdf;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.odt;*.ods;*.odp";

            /// <summary>
            /// All files regardless of extension
            /// </summary>
            public const string AllFiles = "*.*";
        }
    }
}
