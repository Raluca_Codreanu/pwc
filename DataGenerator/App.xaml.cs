﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;
using log4net.Repository.Hierarchy;
using ZPKTool.Business;
using ZPKTool.Business.Managers;
using ZPKTool.Gui.Properties;
using ZPKTool.Gui.Screens;
using ZPKTool.Util;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The application's Entry-Point class.
    /// </summary>
    public partial class App : System.Windows.Application
    {
        /// <summary>
        /// Activates a window (focuses and switches input to it). This is an call to unmanaged code - avoid it if possible.
        /// </summary>
        /// <param name="wndHandle">The window handle.</param>
        /// <returns>True if the window was activated, false otherwise.</returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr wndHandle);

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Do not allow more than one instance of the application to run.
            System.Diagnostics.Process thisProc = System.Diagnostics.Process.GetCurrentProcess();
            System.Diagnostics.Process[] runningProcs = System.Diagnostics.Process.GetProcessesByName(thisProc.ProcessName);
            if (runningProcs.Length > 1)
            {
                // Focus the window of the running app instance and close this instance.
                SetForegroundWindow(runningProcs[0].MainWindowHandle);
                Application.Current.Shutdown();
                return;
            }

            base.OnStartup(e);

            ConfigureLogging();
            ConfigureDbAccess();
            CleanVideoCache();          

            DisplayInitialWindow();
           
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Exit"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.Windows.ExitEventArgs"/> that contains the event data.</param>
        protected override void OnExit(ExitEventArgs e)
        {
            Logging.Logger.Debug("----------------------------------------");
            Logging.Logger.Debug("- Shutting down the application        -");
            Logging.Logger.Debug("----------------------------------------");

            base.OnExit(e);
        }

        #region Initialization

        /// <summary>
        /// Configures the logger
        /// </summary>
        private void ConfigureLogging()
        {
            string exeName = Assembly.GetEntryAssembly().ManifestModule.Name;
            Util.Logging.InitializeLogger(exeName);
        }

        /// <summary>
        /// Configures the database access.
        /// The order of the method calls cannot change: firts the connection strings need to be initialized, then the context
        /// </summary>
        private void ConfigureDbAccess()
        {
            ConnectionParams localConnParams = new ConnectionParams();
            localConnParams.Provider = Settings.Default.Connection_Provider;
            localConnParams.InitialCatalog = Settings.Default.Connection_InitialCatalog;
            localConnParams.DataSource = Settings.Default.Connection_DataSource;
            localConnParams.UserId = Settings.Default.Connection_UserId;
            localConnParams.Password = Settings.Default.Connection_PasswordEncrypted;
            localConnParams.IsPasswordEncrypted = true;
            localConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;

            ConnectionParams centralConnParams = new ConnectionParams();
            centralConnParams.Provider = Settings.Default.CentralConnection_Provider;
            centralConnParams.InitialCatalog = Settings.Default.CentralConnection_InitialCatalog;
            centralConnParams.DataSource = Settings.Default.CentralConnection_DataSource;
            centralConnParams.UserId = Settings.Default.CentralConnection_UserId;
            centralConnParams.Password = Settings.Default.CentralConnection_PasswordEncrypted;
            centralConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;
            centralConnParams.IsPasswordEncrypted = true;

            Business.Managers.SettingsManager.Instance.InitializeContext(localConnParams, centralConnParams);
        }
        #endregion Initialization

        /// <summary>
        /// Preloads some data to increase the application's responsiveness after startup.
        /// </summary>
        private void PreloadData()
        {
            // Initialize the precompiled queries from the persistence module.
            PreloadManager.Instance.PreloadBusinessData();

            // The initialization above allocates a lot of objects so force a collection before the UI is displayed to free some memory.
            GC.Collect();
            GC.WaitForPendingFinalizers();            
        }

        /// <summary>
        /// Decide which is the intial screen and display it.
        /// </summary>
        private void DisplayInitialWindow()
        {
            
                //// get the licence information
                ZPKTool.Data.LicenseInformation licenseInfo = ZPKTool.Business.Managers.SecurityManager.Instance.CheckTrialAndLicense();
                              
                ApplicationController.Instance.ShowLoginWindow();
              
        }

        /// <summary>
        /// Remove the content of the VideoCache folder
        /// </summary>
        private void CleanVideoCache()
        {
            string folderPath = string.Format("{0}\\{1}", Configuration.ApplicationDataFolderPath, Constants.VideoCacheFolderName);
            try
            {
                if (System.IO.Directory.Exists(folderPath))
                {
                    System.IO.Directory.Delete(folderPath, true);
                }

                System.IO.Directory.CreateDirectory(folderPath);
            }
            catch (Exception ex)
            {
                Logging.Logger.Error("Caught an exception while clearing the cache video older: " + ex.ToString());
            }
        }

        /// <summary>
        /// Handles the DispatcherUnhandledException event of the Application.
        /// This event is triggered when an application exception is not handled.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Threading.DispatcherUnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logging.Logger.Error("An unhandled exception reached the UI dispatcher.", e.Exception);
            e.Handled = true;
        }
    }
}
