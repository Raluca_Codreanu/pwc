﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Threading;
using Microsoft.Win32;
using ZPKTool.Business.Managers;
using ZPKTool.Data;
using ZPKTool.Data.Interfaces;
using ZPKTool.Gui.Controls;
using ZPKTool.Util;
using System.Windows.Controls.Primitives;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Helper class containing useful methods used wll over the application
    /// </summary>
    public static class UIUtils
    {
        /// <summary>
        /// Extract an image embeded in the assembly.
        /// </summary>
        /// <param name="fileName">The name of the image file, including the extension.</param>
        /// <returns>An image or null if it was not found.</returns>
        public static BitmapImage GetImageFromResources(string fileName)
        {
            // TODO: this method should be replaced with Application.Current.Resources["image name"]
            try
            {
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();

                string uri = "pack://application:,,,/DataGenerator;component/resources/images/" + fileName;
                bitmapImage.UriSource = new Uri(uri, UriKind.Absolute);

                bitmapImage.EndInit();
                return bitmapImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the thousand separator.
        /// </summary>
        /// <returns>The thousand separator.</returns>
        public static char GetThousandSeparator()
        {
            return CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator.ToCharArray()[0];
        }

        /// <summary>
        /// Determines whether the specified key causes data grid selection change or not.
        /// </summary>
        /// <param name="key">The specified key.</param>
        /// <returns><c>true</c> if it causes; otherwise, <c>false</c>.</returns>
        public static bool IsCausingDataGridSelectionChange(System.Windows.Input.Key key)
        {
            if (key == System.Windows.Input.Key.Up ||
                key == System.Windows.Input.Key.Down ||
                key == System.Windows.Input.Key.PageUp ||
                key == System.Windows.Input.Key.PageDown ||
                key == System.Windows.Input.Key.Enter ||
                key == System.Windows.Input.Key.Return)
            {
                return true;
            }

            return false;
        }
      

        /// <summary>
        /// Focus an UI element. If calling the Focus() method of the element doe not work then use this method.
        /// </summary>
        /// <param name="element">The element.</param>
        public static void FocusElement(UIElement element)
        {
            if (element != null)
            {
                bool focused = element.Focus();
                if (!focused)
                {
                    ThreadStart ts = new ThreadStart(delegate
                        {
                            element.Focus();
                        });
                    element.Dispatcher.BeginInvoke(DispatcherPriority.Input, ts);
                }
            }
        }

        
        /// <summary>
        /// Calculates a maximum font size that will fit in a given space.
        /// </summary>
        /// <param name="maximumFontSize">The maximum (and default) font size</param>
        /// <param name="minimumFontSize">The minimum size the font is capped at</param>
        /// <param name="reductionStep">The step to de-increment font size with. A higher step is less expensive, whereas a lower step sizes font with greater accuracy</param>
        /// <param name="text">The string to measure</param>
        /// <param name="fontFamily">The font family the string provided should be measured in</param>
        /// <param name="containerAreaSize">The total area of the containing area for font placement, specified as a size</param>
        /// <param name="containerInnerMargin">An internal margin to specify the distance the font should keep from the edges of the container</param>
        /// <returns>The caculated maximum font size</returns>
        public static double CalculateMaximumFontSize(
            string text,
            Brush foreground,
            double maximumFontSize,
            double minimumFontSize,
            double reductionStep,
            FontFamily fontFamily,
            Size containerAreaSize,
            Thickness containerInnerMargin)
        {
            // set fontsize to maimumfont size
            Double fontSize = maximumFontSize;

            // holds formatted text - Culture info is setup for US-Engish, this can be changed for different language sets
            FormattedText formattedText = new FormattedText(
                text,
                Thread.CurrentThread.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(fontFamily.ToString()),
                fontSize,
                foreground);

            // hold the maximum internal space allocation as an absolute value
            Double maximumInternalWidth = containerAreaSize.Width - (containerInnerMargin.Left + containerInnerMargin.Right);

            // if measured font is too big for container size, with regard for internal margin
            if (formattedText.WidthIncludingTrailingWhitespace > maximumInternalWidth)
            {
                do
                {
                    // reduce font size by step
                    fontSize -= reductionStep;

                    // set fontsize ready for re-measure
                    formattedText.SetFontSize(fontSize);
                }
                while ((formattedText.WidthIncludingTrailingWhitespace > maximumInternalWidth) && (fontSize > minimumFontSize));
            }

            // return ammended fontsize
            return fontSize;
        }

        #region Extensions

        /// <summary>
        /// Selects the item equivalent to a given object.
        /// The equivalence is decided based on the equality of property specified by the <paramref name="comparisonProperty"/> parameter.
        /// The source items and the given object must have the same type.
        /// <para />
        /// An example usage scenario is: you have a combo box populated with MeasurementUnit instances and you have a different MeasurementUnit instance
        /// and you want to have it selected; the instance is equivalent to one of the combo box's items but it is not the same object so
        /// setting the combo box's SelectedValue property to the MeasurementUnit instance won't work;
        /// calling SelectItem(MeasurementUnitInstance, u => u.Symbol) will select it.
        /// </summary>
        /// <typeparam name="T">The type of items in the selector's source.</typeparam>
        /// <param name="selector">The selector control.</param>
        /// <param name="item">The item to select.</param>
        /// <param name="comparisonProperty">The property used to establish the equivalence of two instances of T.</param>
        public static void SelectItem<T>(this Selector selector, T item, Expression<Func<T, object>> comparisonProperty)
        {
            if (selector == null)
            {
                return;
            }

            if (item == null)
            {
                selector.SelectedItem = null;
                return;
            }

            // Extract the comparison property from the linq expression
            System.Reflection.PropertyInfo comparisonProp = null;
            if (comparisonProperty != null && comparisonProperty.Body != null)
            {
                System.Linq.Expressions.Expression body = comparisonProperty.Body;
                if (body.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression mexp = (MemberExpression)body;
                    comparisonProp = (System.Reflection.PropertyInfo)mexp.Member;
                    if (comparisonProp == null)
                    {
                        throw new ArgumentException("The comarisonProperty expression was invalid.");
                    }
                }
                else if (body.NodeType == ExpressionType.Convert)
                {
                    UnaryExpression exp = (UnaryExpression)body;
                    MemberExpression mexp = (MemberExpression)exp.Operand;
                    comparisonProp = (System.Reflection.PropertyInfo)mexp.Member;
                    if (comparisonProp == null)
                    {
                        throw new ArgumentException("The comarisonProperty expression was invalid.");
                    }
                }
                else
                {
                    throw new ArgumentException("The comarisonProperty expression was invalid.");
                }
            }
            else
            {
                throw new ArgumentException("The comarisonProperty expression was null.");
            }

            IEnumerable<T> comboboxItems = null;
            if (selector.ItemsSource is IEnumerable<T>)
            {
                comboboxItems = (IEnumerable<T>)selector.ItemsSource;
            }
            else
            {
                try
                {
                    comboboxItems = selector.Items.Cast<T>();
                }
                catch
                {
                    // the combobox does not contain items of type T in its Items property.
                    Logging.Logger.Debug("Failed to detect the item collection of the combo box.");
                }
            }

            // Go over the combo box item collection and compare the value of the comparison property for each collection item and the item param.
            object itemVal = comparisonProp.GetValue(item, null);
            if (comboboxItems != null && itemVal != null)
            {
                foreach (T srcItem in comboboxItems)
                {
                    object srcItemVal = comparisonProp.GetValue(srcItem, null);
                    if (srcItemVal != null)
                    {
                        if (srcItemVal.Equals(itemVal))
                        {
                            selector.SelectedItem = srcItem;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Selects the item equivalent to a given object.
        /// The equality is tested using the Guid property.
        /// </summary>
        /// <typeparam name="T">The type of items in the combo.</typeparam>
        /// <param name="selector">The selector control.</param>
        /// <param name="item">The item to select.</param>
        public static void SelectItem<T>(this Selector selector, T item) where T : IGloballyIdentifiable
        {
            selector.SelectItem<T>(item, it => it.Guid);
        }

        #endregion Extensions
    }
}
