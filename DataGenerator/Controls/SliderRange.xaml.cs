﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// doubleeraction logic for RangeSlider.xaml
    /// </summary>
    public partial class SliderRange : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SliderRange"/> class.
        /// </summary>
        public SliderRange()
        {
            InitializeComponent();
            UpperSlider.IsSnapToTickEnabled = true;
            LowerSlider.IsSnapToTickEnabled = true;
            UpperSlider.TickFrequency = 1;
            LowerSlider.TickFrequency = 1;
            this.Loaded += Slider_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of the Slider control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        void Slider_Loaded(object sender, RoutedEventArgs e)
        {
            LowerSlider.ValueChanged += LowerSlider_ValueChanged;
            UpperSlider.ValueChanged += UpperSlider_ValueChanged;
        }

        /// <summary>
        /// Handles the ValueChanged event of the LowerSlider control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Double&gt;"/> instance containing the event data.</param>
        private void LowerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            UpperSlider.Value = (double)Math.Max(UpperSlider.Value, LowerSlider.Value);
        }

        /// <summary>
        /// Handles the ValueChanged event of the UpperSlider control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Double&gt;"/> instance containing the event data.</param>
        private void UpperSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            LowerSlider.Value = (double)Math.Min(UpperSlider.Value, LowerSlider.Value);
        }

        /// <summary>
        /// Gets or sets the minimum.
        /// </summary>
        /// <value>The minimum.</value>
        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        /// <summary>
        /// DependencyProperty MinimumProperty
        /// </summary>
        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(double), typeof(SliderRange), new UIPropertyMetadata(0d));

        /// <summary>
        /// Gets or sets the lower value.
        /// </summary>
        /// <value>The lower value.</value>
        public double LowerValue
        {
            get { return (double)GetValue(LowerValueProperty); }
            set { SetValue(LowerValueProperty, value); }
        }

        /// <summary>
        /// DependencyProperty LowerValueProperty
        /// </summary>
        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(double), typeof(SliderRange), new UIPropertyMetadata(0d));

        /// <summary>
        /// Gets or sets the upper value.
        /// </summary>
        /// <value>The upper value.</value>
        public double UpperValue
        {
            get { return (double)GetValue(UpperValueProperty); }
            set { SetValue(UpperValueProperty, value); }
        }

        /// <summary>
        /// DependencyProperty UpperValueProperty
        /// </summary>
        public static readonly DependencyProperty UpperValueProperty =
            DependencyProperty.Register("UpperValue", typeof(double), typeof(SliderRange), new UIPropertyMetadata(0d));

        /// <summary>
        /// Gets or sets the maximum.
        /// </summary>
        /// <value>The maximum.</value>
        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        /// <summary>
        /// DependencyProperty MaximumProperty
        /// </summary>
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(double), typeof(SliderRange), new UIPropertyMetadata(1d));




    }
}
