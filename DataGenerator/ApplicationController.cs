﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Business;
using ZPKTool.Business.Managers;
using ZPKTool.Business.Synchronization;
using ZPKTool.Data;
using ZPKTool.Gui.Screens;
using ZPKTool.Gui.Utils;
using ZPKTool.Util;

namespace ZPKTool.Gui
{
    /// <summary>
    /// This class serves the components on the gui when they ask for another existing component on the gui
    /// This is a Singleton class.
    /// NOTICE: Constants should no longer be defined in this class. They were moved to ZPKTool.Gui.Constants.
    /// </summary>
    public class ApplicationController : Util.Singleton<ApplicationController>
    {
        /// <summary>
        /// The currently displayed window.
        /// </summary>
        private Window currentWindow;               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationController"/> class.
        /// </summary>
        public ApplicationController()
        {            
        }

        /// <summary>
        /// Gets the handle of the current main window.
        /// </summary>
        /// <returns>Pointer to a window handle.</returns>
        public IntPtr GetCurrentWindowHandle()
        {
            if (currentWindow == null)
            {
                return IntPtr.Zero;
            }

            System.Windows.Interop.WindowInteropHelper helper =
                new System.Windows.Interop.WindowInteropHelper(currentWindow);

            return helper.Handle;
        }

        
        /// <summary>
        /// Display the Login window.
        /// </summary>
        public void ShowLoginWindow()
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            currentWindow = loginWindow;
        }

        /// <summary>
        /// Display the Main window.
        /// </summary>
        public void ShowMainWindow()
        {
            MainWindow mainWindow = new MainWindow(true);
            mainWindow.Show();
            currentWindow = mainWindow;
            
        }
    }
}
