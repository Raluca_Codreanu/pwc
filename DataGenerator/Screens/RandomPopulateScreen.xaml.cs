﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Threading;
using System.IO;
using System.Data.Objects.DataClasses; 
using ZPKTool.Business;
using ZPKTool.Business.Managers;
using ZPKTool.Data;
using ZPKTool.Gui;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Screens;
using ZPKTool.Gui.Utils;
using ZPKTool.Util;
using ZPKTool.Data.Interfaces;

namespace ZPKTool.Gui.Screens
{
    /// <summary>
    /// Interaction logic for RandomPopulateScreen.xaml
    /// </summary>
    public partial class RandomPopulateScreen : UserControl
    {
        #region Attributes
        
        /// <summary>
        /// Random number instance
        /// </summary>
        public static Random newRandom = new Random();

        /// <summary>
        /// Background Worker 
        /// </summary>
        public BackgroundWorker worker;

        /// <summary>
        /// Percentage Completed
        /// </summary>
        private int percentageCompleted;

        /// <summary>
        /// Media directory path
        /// </summary>
        private string directoryPath;

        /// <summary>
        /// Reference to the main window.
        /// </summary>
        private MainWindow mainWindow;       

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the RandomPopulateScreen class.
        /// </summary>
        /// <param name="mainWnd">The main window.</param>
        public RandomPopulateScreen(MainWindow mainWnd)
        {
            this.InitializeComponent();
            this.mainWindow = mainWnd;
            CurrentUserLabel.Content = Convert.ToString(SecurityManager.Instance.CurrentUser.Name);
        }

        #endregion      

        #region Random_String_Number_Date_Generators

        /// <summary>
        /// Randoms the number.
        /// </summary>
        /// <param name="min">The min number .</param>
        /// <param name="max">The max number .</param>
        /// <returns> random number</returns>
        public static int RandomNumber(int min, int max)
        {
            // Use class-level Random so that when this
            // method is called many times, it still has
            // good Randoms.
            return newRandom.Next(min, max);
        }

        /// <summary>
        /// Generate Random decimal number
        /// </summary>
        /// <returns>the random decimal</returns>
        public static decimal GenerateRandomDecimal()
        {   // Create a int array to hold the random values. 
            Byte[] bytes = new Byte[] { 0, 0, 0, 0 };

            RNGCryptoServiceProvider gen = new RNGCryptoServiceProvider();

            // Fill the array with a random value. 
            gen.GetBytes(bytes);
            bytes[3] %= 29; // this must be between 0 and 28 (inclusive) 

            decimal d = new decimal((int)bytes[0], (int)bytes[1], (int)bytes[2], false, bytes[3]);
            d = d % 3;

            // round number with 2 decimals
            d = Math.Round(d, 2);

            return d;
        }

        /// <summary>
        /// Generates a random string with a random length
        /// </summary>
        /// <param name="sizemax">The maximum size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        public static string RandomString(int sizemax, bool lowerCase)
        {
            int size = 4;

            if (sizemax > 4)
            {
                size = RandomNumber(4, sizemax);
            }

            StringBuilder builder = new StringBuilder();
            char ch;
            Random random = new Random();

            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor((26 * random.NextDouble()) + 65)));
                builder.Append(ch);
            }

            if (lowerCase)
            {
                return builder.ToString().ToLower();
            }

            return builder.ToString();
        }

        /// <summary>
        /// Calcs the date.
        /// </summary>
        /// <param name="minDate">The min date.</param>
        /// <param name="maxDate">The max date.</param>
        /// <returns>Random DateTime</returns>
        public static DateTime CalcDate(DateTime minDate, DateTime maxDate)
        {
            // This assumes _rand is a private variable of type System.Random.It calculates a new timespan 
            // that can be added to the minimum date which will create a new date btw 
            // the min & max
            TimeSpan timeSpan = maxDate - minDate;
            Random rand = new Random();
            TimeSpan randomSpan = new TimeSpan((long)(timeSpan.Ticks * rand.NextDouble()));

            return minDate + randomSpan;
        }
        #endregion       

        #region Generators
        /// <summary>
        /// Gets the country settings of the entity to which the process belongs.
        /// Don't be lazy to save the its result in a varable for reuse, especially if using it in loops.
        /// </summary>
        /// <param name="parentEntity">The parent entity.</param>
        /// <returns>The parent's CountrSetting instance.</returns>
        /// <exception cref="ZPKException">Thrown if the parent entity is not supported. Should not actually happen.</exception>
        public CountrySetting GetParentEntityCountrySettings(object parentEntity)
        {
            CountrySetting countrySettings = null;
            if (parentEntity is Part)
            {
                countrySettings = ((Part)parentEntity).CountrySettings;
            }
            else
                if (parentEntity is Assembly)
                {
                    countrySettings = ((Assembly)parentEntity).CountrySettings;
                }
                else
                {
                    throw new ZPKException("This parent entity is not handled: " + parentEntity.GetType().FullName);
                }

            return countrySettings;
        }

        /// <summary>
        /// Gets the process.
        /// </summary>
        /// <param name="parentEntity">The parent entity.</param>
        /// <returns>the process</returns>
        public Process GetProcess(object parentEntity)
        {
            Process process = null;
            if (parentEntity is Part)
            {
                process = ((Part)parentEntity).Process;
            }
            else
                if (parentEntity is Assembly)
                {
                    process = ((Assembly)parentEntity).Process;
                }
                else
                {
                    throw new ZPKException("This parent entity is not handled: " + parentEntity.GetType().FullName);
                }

            return process;
        }

        /// <summary>
        /// Adds the media collection.
        /// </summary>
        /// <typeparam name="T">Generic type that implements IEntityWithMediaCollection</typeparam>
        /// <param name="item">The name of the generic type object </param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        public void AddMediaCollection<T>(T item, RandomProjectStruct randomProjectStruct) where T : IEntityWithMediaCollection
        {
            byte[] mediaBytes;

            if (randomProjectStruct.DirectoryPath != null)
            {
                string directorPath = randomProjectStruct.DirectoryPath;

                string[] filePaths = Directory.GetFiles(directorPath, "*.*", SearchOption.AllDirectories);

                int numberMedia = RandomNumber(1, filePaths.Length);

                while (numberMedia != 0)
                {
                    string originalFilePath = filePaths.ElementAt(RandomNumber(0, filePaths.Length));

                    string originalFileName = Path.GetFileName(originalFilePath);

                    if (!System.IO.File.Exists(originalFileName))
                    {
                        string e = Path.GetExtension(originalFileName);

                        mediaBytes = File.ReadAllBytes(originalFilePath);

                        if (mediaBytes.Length != 0)
                        {
                            Media media = new Media();
                            media.Size = mediaBytes.Length;
                            media.OriginalFileName = originalFileName;
                            media.Content = mediaBytes;
                            media.Owner = SecurityManager.Instance.CurrentUser;

                            if ((e == ".jpg") || (e == ".png") || (e == ".bmp") || (e == ".jpeg") || (e == ".gif"))
                            {
                                media.Type = (short)MediaType.Image;
                            }
                            else if ((e == ".avi") || (e == ".flv") || (e == ".mpg") || (e == ".mov") || (e == ".wmv") || (e == ".asf"))
                            {
                                media.Type = (short)MediaType.Video;
                            }
                            else if ((e == ".doc") || (e == ".docx") || (e == ".ppt") || (e == ".pps") || (e == ".pdf") || (e == ".xls") || (e == ".xlsx"))
                            {
                                media.Type = (short)MediaType.Document;
                            }

                            item.MediaCollection.Add(media);
                        }
                    }

                    numberMedia--;
                }
            }
        }

        /// <summary>
        /// Adds the media.
        /// </summary>
        /// <typeparam name="T"> Generic type that implements IEntityWithMediaCollection </typeparam>
        /// <param name="item">The name of the generic type object</param>
        /// <param name="randomProjectStruct">The random project struct that contains the information from the GUI</param>
        public void AddMedia<T>(T item, RandomProjectStruct randomProjectStruct) where T : IEntityWithMedia
        {
            byte[] mediaBytes;

            String directorPath = "";

            if (randomProjectStruct.DirectoryPath != null)
            {
                directorPath = randomProjectStruct.DirectoryPath;

                string[] filePaths = Directory.GetFiles(directorPath, "*.*", SearchOption.AllDirectories);

                string originalFilePath = filePaths.ElementAt(RandomNumber(0, filePaths.Length));

                string originalFileName = Path.GetFileName(originalFilePath);

                if (!System.IO.File.Exists(originalFileName))
                {
                    string e = Path.GetExtension(originalFileName);

                    mediaBytes = File.ReadAllBytes(originalFilePath);

                    if (mediaBytes.Length != 0)
                    {
                        Media media = new Media();
                        media.Size = mediaBytes.Length;
                        media.OriginalFileName = originalFileName;
                        media.Content = mediaBytes;
                        media.Owner = SecurityManager.Instance.CurrentUser;

                        if ((e == ".jpg") || (e == ".png") || (e == ".bmp") || (e == ".jpeg") || (e == ".gif"))
                        {
                            media.Type = (short)MediaType.Image;
                        }
                        else if ((e == ".avi") || (e == ".flv") || (e == ".mpg") || (e == ".mov") || (e == ".wmv") || (e == ".asf"))
                        {
                            media.Type = (short)MediaType.Video;
                        }
                        else if ((e == ".doc") || (e == ".docx") || (e == ".ppt") || (e == ".pps") || (e == ".pdf") || (e == ".") || (e == ".xls") || (e == ".xlsx"))
                        {
                            media.Type = (short)MediaType.Document;
                        }

                        item.Media = media;
                    }
                }
            }
        }

        /// <summary>
        /// Generates the machines.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateMachines(object parent, RandomProjectStruct randomProjectStruct)
        {
            int nr = 1;

            if (parent is Part)
            {
                nr = RandomNumber(randomProjectStruct.NrMinMachines, randomProjectStruct.NrMaxMachines);
            }
            else
            {
                nr = RandomNumber(randomProjectStruct.NrMinMachines1, randomProjectStruct.NrMaxMachines1);
            }

            for (int i = 1; i <= nr; i++)
            {
                GenerateMachine(parent, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the machine.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateMachine(object parent, RandomProjectStruct randomProjectStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;

            Machine newMachine = new Machine();

            AddMedia(newMachine, randomProjectStruct);

            newMachine.Owner = SecurityManager.Instance.CurrentUser;

            newMachine.Name = RandomString(15, false);
            newMachine.ProcessStep = parent as ProcessStep;
            newMachine.RegistrationNumber = Convert.ToString(RandomNumber(1, int.MaxValue));
            newMachine.DescriptionOfFunction = RandomString(10, false);
            newMachine.ManufacturingYear = (short)RandomNumber(2000, 2010);
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(15, false);
            manufacturer.Name = RandomString(15, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            newMachine.Manufacturer = manufacturer;
            newMachine.FloorSize = GenerateRandomDecimal();
            newMachine.WorkspaceArea = GenerateRandomDecimal();
            newMachine.PowerConsumption = GenerateRandomDecimal();
            newMachine.AirConsumption = GenerateRandomDecimal();
            newMachine.WaterConsumption = GenerateRandomDecimal();
            newMachine.FullLoadRate = GenerateRandomDecimal();
            newMachine.MaxCapacity = RandomNumber(0, int.MaxValue);
            newMachine.Availability = (decimal)RandomNumber(0, 10000) * 0.0001m;
            newMachine.OEE = (decimal)RandomNumber(0, 10000) * 0.0001m;
            newMachine.MachineInvestment = (decimal)RandomNumber(0, 10000) * 0.0001m;
            newMachine.SetupInvestment = GenerateRandomDecimal();
            newMachine.AdditionalEquipmentInvestment = GenerateRandomDecimal();
            newMachine.FundamentalSetupInvestment = GenerateRandomDecimal();
            newMachine.InvestmentRemarks = RandomString(10, false);
            newMachine.ExternalWorkCost = GenerateRandomDecimal();
            newMachine.MaterialCost = GenerateRandomDecimal();
            newMachine.ConsumablesCost = GenerateRandomDecimal();
            newMachine.ManualConsumableCostPercentage = GenerateRandomDecimal();
            newMachine.ManualConsumableCostCalc = true;
            newMachine.RepairsCost = GenerateRandomDecimal();
            newMachine.KValue = (decimal)RandomNumber(0, 10000) * 0.0001m;
            newMachine.CalculateWithKValue = true;

            EntityManager.Instance.AddEntity(newMachine, entityContext);
        }

        /// <summary>
        /// Generates the projects.
        /// </summary>
        /// <param name="randomProjectStruct">The random project struct.</param>
        /// <param name="nworker">The nworker.</param>
        private void GenerateProjects(RandomProjectStruct randomProjectStruct, BackgroundWorker nworker)
        {
            percentageCompleted = 0;

            // random number representing the number of projects that will be created
            int nr = RandomNumber(randomProjectStruct.NrMinProjects, randomProjectStruct.NrMaxProjects);

            for (int i = 1; i <= nr; i++)
            {
                percentageCompleted = (i * 100) / nr;
                nworker.ReportProgress(percentageCompleted);

                // creates  new project
                GenerateProject(randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the commodities.
        /// </summary>
        /// <param name="parentPart">The parent part.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateCommodities(object parentPart, RandomProjectStruct randomProjectStruct)
        {
            int nr = RandomNumber(randomProjectStruct.NrMinCommodities, randomProjectStruct.NrMaxCommodities);

            for (int i = 1; i <= nr; i++)
            {
                GenerateCommodity(parentPart, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the commodity.
        /// </summary>
        /// <param name="parentPart">The parent part.</param>
        /// <param name="randomStruct">The random struct.</param>
        private void GenerateCommodity(object parentPart, RandomProjectStruct randomStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;
            BasicSetting settings = SettingsManager.Instance.GetBasicSettings(ZPKTool.Data.EntityContext.LocalDatabase);
            Commodity commodity = new Commodity();
            commodity.Name = RandomString(15, false);

            AddMedia(commodity, randomStruct);

            if (parentPart is Part)
            {
                commodity.Part = parentPart as Part;
            }
            else if (parentPart is ProcessStep)
            {
                commodity.ProcessStep = parentPart as ProcessStep;
            }

            List<MeasurementUnit> unitss = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);

            commodity.WeightUnitBase = unitss.ElementAt(RandomNumber(0, unitss.Count - 1));

            commodity.Owner = SecurityManager.Instance.CurrentUser;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(30, false);
            manufacturer.Name = RandomString(15, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            commodity.Manufacturer = manufacturer;
            commodity.Manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            commodity.PartNumber = Convert.ToString(RandomNumber(1, int.MaxValue));
            commodity.Price = GenerateRandomDecimal();
            commodity.RejectRatio = GenerateRandomDecimal();
            commodity.Weight = GenerateRandomDecimal();
            commodity.Amount = RandomNumber(1, int.MaxValue);
            commodity.Remarks = RandomString(20, false);
            EntityManager.Instance.AddEntity(commodity, entityContext);
        }

        /// <summary>
        /// Generates the raw materials.
        /// </summary>
        /// <param name="parentPart">The parent part.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateRawMaterials(object parentPart, RandomProjectStruct randomProjectStruct)
        {   // random number representing the number of assemblies that will be created
            int nr = RandomNumber(randomProjectStruct.NrMinRawMaterials, randomProjectStruct.NrMaxRawMaterials);

            for (int i = 1; i <= nr; i++)
            {
                GenerateRawMaterial(parentPart, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the row material.
        /// </summary>
        /// <param name="parentPart">The parent part.</param>
        /// <param name="randomStruct">The random struct.</param>
        private void GenerateRawMaterial(object parentPart, RandomProjectStruct randomStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;
            BasicSetting settings = SettingsManager.Instance.GetBasicSettings(ZPKTool.Data.EntityContext.LocalDatabase);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = RandomString(4, false);

            AddMedia(rawMaterial, randomStruct);

            if (parentPart is Part)
            {
                rawMaterial.Part = parentPart as Part;
            }
            else if (parentPart is ProcessStep)
            {
                rawMaterial.ProcessStep = parentPart as ProcessStep;
            }

            rawMaterial.Type = 0;
            rawMaterial.Owner = SecurityManager.Instance.CurrentUser;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(30, false);
            manufacturer.Name = RandomString(15, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            rawMaterial.Manufacturer = manufacturer;
            rawMaterial.Manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            rawMaterial.NormName = RandomString(15, false);
            List<MeasurementUnit> units = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);
            rawMaterial.PriceUnitBase = units.ElementAt(RandomNumber(0, units.Count - 1));
            List<RawMaterialDeliveryType> deliveryTypes =
            EntityManager.Instance.GetAllRawMaterialDeliveryTypes(entityContext);
            rawMaterial.DeliveryType = deliveryTypes.ElementAt(RandomNumber(0, deliveryTypes.Count));
            rawMaterial.Price = GenerateRandomDecimal();
            rawMaterial.ParentWeight = GenerateRandomDecimal();
            rawMaterial.QuantityUnitBase = units.ElementAt(RandomNumber(0, units.Count - 1));
            rawMaterial.Quantity = GenerateRandomDecimal();
            rawMaterial.ScrapRefundRatio = (decimal)RandomNumber(1, 10000) * 0.0001m;
            rawMaterial.Loss = (decimal)RandomNumber(1, 10000) * 0.0001m;
            rawMaterial.RejectRatio = (decimal)RandomNumber(1, 10000) * 0.0001m;
            rawMaterial.Remarks = RandomString(20, true);

            List<MeasurementUnit> unitss = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);

            rawMaterial.ParentWeightUnitBase = unitss.ElementAt(RandomNumber(0, units.Count - 1));

            EntityManager.Instance.AddEntity(rawMaterial, entityContext);
        }

        /// <summary>
        /// Generates the parts.
        /// </summary>
        /// <param name="parentProject">The parent project.</param>
        /// <param name="parentAssembly">The parent assembly.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateParts(Project parentProject, Assembly parentAssembly, RandomProjectStruct randomProjectStruct)
        {
            // random number representing the number of assemblies that will be created
            int nr = RandomNumber(randomProjectStruct.NrMinParts, randomProjectStruct.NrMaxParts);
            for (int i = 1; i <= nr; i++)
            {
                GeneratePart(parentProject, parentAssembly, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the parts.
        /// </summary>
        /// <param name="parentProject">The parent project.</param>
        /// <param name="parentAssembly">The parent assembly.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GeneratePart(Project parentProject, Assembly parentAssembly, RandomProjectStruct randomProjectStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;

            BasicSetting settings = SettingsManager.Instance.GetBasicSettings(ZPKTool.Data.EntityContext.LocalDatabase);

            Part newPart = new Part();

            newPart.Project = parentProject;
            if (parentAssembly != null)
            {
                newPart.Assembly = parentAssembly;
            }

            AddMedia(newPart, randomProjectStruct);
            AddMediaCollection(newPart, randomProjectStruct);

            newPart.Name = RandomString(15, false);
            newPart.Owner = SecurityManager.Instance.CurrentUser;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(10, false);
            manufacturer.Name = RandomString(15, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            newPart.Manufacturer = manufacturer;
            newPart.Manufacturer.IsMasterData = newPart.IsMasterData;
            newPart.Manufacturer.Owner = newPart.Owner;

            newPart.Name = RandomString(15, false);
            newPart.Number = Convert.ToString(RandomNumber(1, int.MaxValue));
            newPart.Description = RandomString(10, false);
            newPart.BatchFrequency = RandomNumber(1, int.MaxValue);
            newPart.YearlyProductionQuantity = RandomNumber(1, int.MaxValue);
            newPart.LifeTime = RandomNumber(1, int.MaxValue);

            newPart.PurchasePrice = GenerateRandomDecimal();
            newPart.TargetPrice = GenerateRandomDecimal();
            newPart.AssetRate = GenerateRandomDecimal();
            newPart.SBMActive = true;
            newPart.IsExternal = false;
            newPart.AdditionalRemarks = RandomString(20, false);
            newPart.AdditionalDescription = RandomString(10, false);
            newPart.ManufacturerDescription = RandomString(10, false);

            List<Country> countriess = SettingsManager.Instance.GetAllCountriesFull(entityContext);

            newPart.ManufacturingCountry = (Country)countriess.ElementAt(RandomNumber(1, countriess.Count));

            newPart.CalculatorUser = SecurityManager.Instance.CurrentUser;

            newPart.CalculationStatus = 1;

            CountrySetting countrySettings = null;

            Country country = (Country)countriess.ElementAt(1);
            countrySettings = country.CountrySetting.Copy();
            newPart.CountrySettings = countrySettings;

            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            newPart.OverheadSettings = overheadSettings;
            newPart.OverheadSettings.CommodityMargin = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.CommodityOverhead = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ConsumableMargin = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ConsumableOverhead = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ExternalWorkMargin = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ExternalWorkOverhead = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.LogisticOHValue = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ManufacturingMargin = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.ManufacturingOverhead = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.MaterialMargin = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.MaterialOverhead = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.OtherCostOHValue = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            newPart.OverheadSettings.PackagingOHValue = RandomNumber(1, 10000) * 0.0001m;
            newPart.OverheadSettings.SalesAndAdministrationOHValue = RandomNumber(1, 10000) * 0.0001m;
            newPart.CalculationAccuracy = 1;
            newPart.CalculationApproach = 1;

            short val = (short)RandomNumber(0, Enum.GetValues(typeof(PartDeliveryType)).Length);
            newPart.DelivertType = val;

            newPart.EstimatedCost = GenerateRandomDecimal();

            Process process = new Process();

            newPart.Process = process;
            newPart.Process.IsMasterData = newPart.IsMasterData;
            newPart.Process.Owner = newPart.Owner;

            // set the 6 properties needed for calculations
            newPart.DevelopmentCost = 0;
            newPart.ProjectInvest = 0;
            newPart.OtherCost = 0;
            newPart.PackagingCost = 0;
            newPart.LogisticCost = 0;
            newPart.PaymentCost = 0;

            EntityManager.Instance.AddEntity(newPart, entityContext);

            GenerateRawMaterials(newPart, randomProjectStruct);
            GenerateCommodities(newPart, randomProjectStruct);
            GenerateProcessSteps(newPart, randomProjectStruct);
        }

        /// <summary>
        /// Generates the assemblies.
        /// </summary>
        /// <param name="parentProject">The parent project.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateAssemblies(Project parentProject, RandomProjectStruct randomProjectStruct)
        {
            // random number representing the number of assemblies that will be created
            int nr = RandomNumber(randomProjectStruct.NrMinAssemblies, randomProjectStruct.NrMaxAssemblies);

            int NrAssembliesDepth = RandomNumber(randomProjectStruct.NrMinAssemblies, randomProjectStruct.NrMaxAssemblies);

            for (int i = 1; i <= nr; i++)
            {
                GenerateAssembly(parentProject, null, NrAssembliesDepth, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the assemblies.
        /// </summary>
        /// <param name="parentProject">The parent project.</param>
        /// <param name="parentAssembly">The parent assembly.</param>
        /// <param name="nrAssembliesDepth">The nr assemblies depth.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateAssembly(Project parentProject, Assembly parentAssembly, int numberAssembliesDepth, RandomProjectStruct randomProjectStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;
            BasicSetting settings = SettingsManager.Instance.GetBasicSettings(ZPKTool.Data.EntityContext.LocalDatabase);

            Assembly assembly = new Assembly();
            assembly.Project = parentProject;

            if (parentAssembly != null)
            {
                assembly.ParentAssembly = parentAssembly;
            }

            AddMedia(assembly, randomProjectStruct);
            AddMediaCollection(assembly, randomProjectStruct);

            assembly.Name = RandomString(15, false);
            assembly.Number = Convert.ToString(RandomNumber(1, int.MaxValue));
            assembly.OtherCost = GenerateRandomDecimal();
            List<Country> countriess = SettingsManager.Instance.GetAllCountriesFull(entityContext);
            assembly.AssemblingCountry = (Country)countriess.ElementAt(1);
            CountrySetting countrySettings = null;
            Country country = (Country)countriess.ElementAt(RandomNumber(1, countriess.Count));
            countrySettings = country.CountrySetting.Copy();
            assembly.CountrySettings = countrySettings;
            assembly.Owner = SecurityManager.Instance.CurrentUser;
            assembly.Project = parentProject;
            assembly.ProjectInvest = GenerateRandomDecimal();
            assembly.YearlyProductionQuantity = RandomNumber(1, int.MaxValue);
            assembly.PaymentCost = GenerateRandomDecimal();
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = RandomString(15, true);
            manufacturer.Description = RandomString(10, true);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            assembly.Manufacturer = manufacturer;
            assembly.Manufacturer.Owner = assembly.Owner;
            assembly.BatchFrequency = RandomNumber(1, int.MaxValue);
            assembly.YearlyProductionQuantity = RandomNumber(1, int.MaxValue);
            assembly.LifeTime = RandomNumber(1, int.MaxValue);
            assembly.DevelopmentCost = 0;
            assembly.ProjectInvest = 0;
            assembly.OtherCost = 0;
            assembly.PackagingCost = 0;
            assembly.LogisticCost = 0;
            assembly.PaymentCost = 0;
            assembly.Process = new Process();
            assembly.Process.Owner = SecurityManager.Instance.CurrentUser;
            assembly.Process.IsMasterData = assembly.IsMasterData;
            assembly.Process.Owner = assembly.Owner;
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            assembly.OverheadSettings = overheadSettings;
            assembly.OverheadSettings.CommodityMargin = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.CommodityOverhead = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ConsumableMargin = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ConsumableOverhead = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ExternalWorkMargin = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ExternalWorkOverhead = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.LogisticOHValue = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ManufacturingMargin = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.ManufacturingOverhead = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.MaterialMargin = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.MaterialOverhead = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.OtherCostOHValue = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            assembly.OverheadSettings.PackagingOHValue = RandomNumber(1, 10000) * 0.0001m;
            assembly.OverheadSettings.SalesAndAdministrationOHValue = RandomNumber(1, 10000) * 0.0001m;
            EntityManager.Instance.SaveAssembly(assembly);

            // generates process steps
            this.GenerateProcessSteps(assembly, randomProjectStruct);

            // generates the parts
            this.GenerateParts(parentProject, assembly, randomProjectStruct);

            numberAssembliesDepth -= 1;

            if (numberAssembliesDepth != 0)
            {
                GenerateAssembly(parentProject, assembly, numberAssembliesDepth, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the projects.
        /// </summary>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateProject(RandomProjectStruct randomProjectStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;
            
            // new project
            Project project = new Project();
                                   
            project.Name = "proj" + RandomString(10, false);
            project.Owner = SecurityManager.Instance.CurrentUser;
            
            AddMedia(project, randomProjectStruct);
            AddMediaCollection(project, randomProjectStruct);
            project.Status = 1;
            Customer customer = new Customer();
            customer.Name = RandomString(15, true);
            customer.Owner = project.Owner;
            customer.City = RandomString(15, false);
            List<Country> countries = SettingsManager.Instance.GetAllCountriesFull(EntityContext.LocalDatabase);
            customer.Country = countries.ElementAt(RandomNumber(1, countries.Count - 1));
            customer.Description = RandomString(10, true);
            customer.Email = RandomString(5, false) + "@" + customer.Name + ".com";
            customer.Fax = Convert.ToString(RandomNumber(1000, 9999));
            customer.Mobile = Convert.ToString(RandomNumber(1000, 9999));
            customer.Phone = Convert.ToString(RandomNumber(1000, 9999));
            customer.WebAddress = "www." + customer.Name + ".ro";
            customer.StreetAddress = RandomString(30, false);
            customer.ZipCode = Convert.ToString(RandomNumber(1000, 9999));
            customer.Number = Convert.ToString(RandomNumber(1, 100));
            project.Customer = customer;
            project.CustomerDescription = RandomString(30, false);
            project.StartDate = CalcDate(System.DateTime.Now, System.DateTime.Now.AddYears(20));
            project.EndDate = CalcDate(project.StartDate.Value, project.StartDate.Value.AddYears(20));
          
            short val = (short)RandomNumber(0, Enum.GetValues(typeof(ProjectStatus)).Length);
            project.Status = val;
          
            project.YearlyProductionQuantity = RandomNumber(1, int.MaxValue);
            project.Number = Convert.ToString(RandomNumber(1, int.MaxValue));
            project.ProductName = RandomString(15, false);
            project.ProductNumber = Convert.ToString(RandomNumber(1, int.MaxValue));
            project.ProjectLeader = SecurityManager.Instance.CurrentUser;
            project.Remarks = RandomString(10, false);
            project.DepreciationPeriod = RandomNumber(1, int.MaxValue);
            project.YearlyProductionQuantity = RandomNumber(1, int.MaxValue);
            project.LifeTime = RandomNumber(1, int.MaxValue);
            project.Partner = RandomString(10, false);
            project.DepreciationRate = (decimal)RandomNumber(0, 10000) * 0.0001m;
            List<User> users = SecurityManager.Instance.GetAllUsers(entityContext);
            int pos = RandomNumber(1, users.Count);
            project.ResponsibleCalculator = users.ElementAt(pos - 1);
            pos = RandomNumber(1, users.Count);
            project.ProjectLeader = users.ElementAt(pos - 1);
            project.ProductDescription = RandomString(10, false);
            OverheadSetting overheadSettings = new OverheadSetting();
            overheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            project.OverheadSettings = overheadSettings;
            project.OverheadSettings.CommodityMargin = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.CommodityOverhead = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ConsumableMargin = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ConsumableOverhead = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ExternalWorkMargin = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ExternalWorkOverhead = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ManufacturingMargin = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.ManufacturingOverhead = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.MaterialMargin = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.MaterialOverhead = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.OtherCostOHValue = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.Owner = SecurityManager.Instance.CurrentUser;
            project.OverheadSettings.PackagingOHValue = RandomNumber(1, 10000) * 0.0001m;
            project.OverheadSettings.SalesAndAdministrationOHValue = RandomNumber(1, 10000) * 0.0001m;

            // updates the DB 
            EntityManager.Instance.SaveProject(project);

            // generates assemblies
            this.GenerateAssemblies(project, randomProjectStruct);

            // generates parts
            this.GenerateParts(project, null, randomProjectStruct);
         }

        /// <summary>
        /// Generates the consumables.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateConsumables(object parent, RandomProjectStruct randomProjectStruct)
        {
            int nr = RandomNumber(randomProjectStruct.NrMinConsumables, randomProjectStruct.NrMaxConsumables);

            for (int i = 1; i <= nr; i++)
            {
                GenerateConsumable(parent, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the consumable.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomStruct">The random struct.</param>
        private void GenerateConsumable(object parent, RandomProjectStruct randomStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;
            Consumable newConsumable = new Consumable();
            newConsumable.ProcessStep = parent as ProcessStep;
            newConsumable.Owner = SecurityManager.Instance.CurrentUser;
            newConsumable.Name = RandomString(15, false);
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(30, false);
            manufacturer.Name = RandomString(15, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            newConsumable.Manufacturer = manufacturer;
            newConsumable.Price = GenerateRandomDecimal();
            List<MeasurementUnit> units = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);
            if (units.Count > 2)
            {
                newConsumable.AmountUnitBase = units.ElementAt(RandomNumber(0, units.Count - 1));
            }
            else
            {
                newConsumable.AmountUnitBase = units.ElementAt(0);
            }

            newConsumable.Amount = GenerateRandomDecimal();
            newConsumable.Description = RandomString(30, true);
            if (units.Count > 2)
            {
                newConsumable.PriceUnitBase = units.ElementAt(RandomNumber(0, units.Count - 1));
            }
            else
            {
                newConsumable.PriceUnitBase = units.ElementAt(0);
            }

            EntityManager.Instance.AddEntity(newConsumable, entityContext);
        }

        /// <summary>
        /// Generates the diw.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomStruct">The random struct.</param>
        private void GenerateDie(object parent, RandomProjectStruct randomStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;

            Die newDie = new Die();
            newDie.Owner = SecurityManager.Instance.CurrentUser;
            newDie.Name = RandomString(5, false);

            AddMedia(newDie, randomStruct);

            newDie.ProcessStep = parent as ProcessStep;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Description = RandomString(10, false);
            manufacturer.Name = RandomString(4, false);
            manufacturer.Owner = SecurityManager.Instance.CurrentUser;
            newDie.Manufacturer = manufacturer;
            newDie.Owner = SecurityManager.Instance.CurrentUser;

            short val = (short)RandomNumber(0, Enum.GetValues(typeof(DieType)).Length);
            newDie.Type = val;

            newDie.Investment = GenerateRandomDecimal();
            newDie.ReusableInvest = GenerateRandomDecimal();
            newDie.LifeTime = GenerateRandomDecimal();
            newDie.IsWearInPercentage = true;
            newDie.Wear = GenerateRandomDecimal();
            newDie.Maintenance = GenerateRandomDecimal();
            newDie.AllocationRatio = GenerateRandomDecimal();
            newDie.Remark = RandomString(30, false);
            newDie.DiesetsNumberPaidByCustomer = RandomNumber(0, int.MaxValue);
            newDie.CostAllocationBasedOnPartsPerLifeTime = true;
            newDie.CostAllocationNumberOfParts = RandomNumber(0, int.MaxValue);

            int n = RandomNumber(0, 1);

            if (n == 0)
            {
                newDie.IsMaintenanceInPercentage = false;
            }
            else
            {
                newDie.IsMaintenanceInPercentage = true;
            }

            EntityManager.Instance.AddEntity(newDie, entityContext);
        }

        /// <summary>
        /// Generats the dies.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateDies(object parent, RandomProjectStruct randomProjectStruct)
        {
            int nr = RandomNumber(randomProjectStruct.NrMinDies, randomProjectStruct.NrMaxDies);
            for (int i = 1; i <= nr; i++)
            {
                GenerateDie(parent, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the process steps.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateProcessStep(object parent, RandomProjectStruct randomProjectStruct)
        {
            EntityContext entityContext = EntityContext.LocalDatabase;

            // Initialize a new process step and set its owner.
            ProcessStep newStep = null;

            if (parent is Assembly)
            {
                newStep = new AssemblyProcessStep();
                newStep.Owner = SecurityManager.Instance.CurrentUser;

                // Set the new step's index, assuming that the step is added on the last position.
                Process process = GetProcess(parent);
                int index = 0;
                if (process.Steps.Count > 0)
                {
                    index = process.Steps.Max(ps => ps.Index) + 1;
                }

                AddMedia(newStep, randomProjectStruct);

                ProcessStepsClassification psc = new ProcessStepsClassification();
                psc.Name = RandomString(15, false);
                newStep.Type = psc;
                newStep.Index = index;
                newStep.Name = RandomString(15, false);
                newStep.Price = GenerateRandomDecimal();
                newStep.ProcessTime = GenerateRandomDecimal();
                List<MeasurementUnit> units = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);
                newStep.ProcessTimeUnit = units.ElementAt(RandomNumber(1, units.Count - 1));
                newStep.ProductionDaysPerWeek = GenerateRandomDecimal();
                newStep.ProductionEngineers = GenerateRandomDecimal();
                newStep.ProductionForeman = GenerateRandomDecimal();
                newStep.ProductionSkilledLabour = GenerateRandomDecimal();
                newStep.ProductionTechnicians = GenerateRandomDecimal();
                newStep.ProductionUnskilledLabour = GenerateRandomDecimal();
                newStep.ProductionWeeksPerYear = GenerateRandomDecimal();
                newStep.Rework = (decimal)RandomNumber(1, 10000) * 0.0001m;
                newStep.ScrapAmount = (decimal)RandomNumber(1, 10000) * 0.0001m;
                newStep.SetupEngineers = GenerateRandomDecimal();
                newStep.SetupForeman = GenerateRandomDecimal();
                newStep.SetupSkilledLabour = GenerateRandomDecimal();
                newStep.SetupsPerBatch = RandomNumber(1, int.MaxValue);
                newStep.SetupTechnicians = GenerateRandomDecimal();
                newStep.SetupTime = GenerateRandomDecimal();
                newStep.SetupTimeUnit = units.ElementAt(RandomNumber(1, units.Count - 1));
                newStep.SetupUnskilledLabour = GenerateRandomDecimal();
                newStep.ShiftCostExceedRatio = GenerateRandomDecimal();
                newStep.ShiftsPerWeek = GenerateRandomDecimal();
                newStep.Accuracy = (short)RandomNumber(1, short.MaxValue);
                newStep.CycleTime = GenerateRandomDecimal();
                newStep.PartsPerCycle = RandomNumber(1, int.MaxValue);
                newStep.HoursPerShift = GenerateRandomDecimal();
                newStep.BatchSize = RandomNumber(1, int.MaxValue);
                newStep.MaxDownTime = GenerateRandomDecimal();
                newStep.ExtraShiftsNumber = GenerateRandomDecimal();
                List<MeasurementUnit> unitss = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);

                newStep.CycleTimeUnit = units.ElementAt(RandomNumber(1, units.Count - 1));
                newStep.MaxDownTimeUnit = units.ElementAt(RandomNumber(1, units.Count - 1));

                process.Steps.Add(newStep);

                EntityManager.Instance.AddEntity(newStep, entityContext);

                // generates machines for this new step
                GenerateMachines(newStep, randomProjectStruct);

                // commodity
                GenerateCommodities(newStep, randomProjectStruct);

                // consumable
                GenerateConsumables(newStep, randomProjectStruct);

                // rowmaterial
                GenerateRawMaterials(newStep, randomProjectStruct);
            }

            if (parent is Part)
            {
                newStep = new PartProcessStep();
                newStep.Owner = SecurityManager.Instance.CurrentUser;

                // Set the new step's index, assuming that the step is added on the last position.
                Process process = GetProcess(parent);
                int index = 0;
                if (process.Steps.Count > 0)
                {
                    index = process.Steps.Max(ps => ps.Index) + 1;
                }

                AddMedia(newStep, randomProjectStruct);

                newStep.Index = index;
                newStep.Name = RandomString(15, false);
                newStep.Price = GenerateRandomDecimal();
                newStep.ProcessTime = GenerateRandomDecimal();
                List<MeasurementUnit> units = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);
                newStep.ProcessTimeUnit = units.ElementAt(RandomNumber(0, units.Count - 1));
                newStep.ProductionDaysPerWeek = GenerateRandomDecimal();
                newStep.ProductionEngineers = GenerateRandomDecimal();
                newStep.ProductionForeman = GenerateRandomDecimal();
                newStep.ProductionSkilledLabour = GenerateRandomDecimal();
                newStep.ProductionTechnicians = GenerateRandomDecimal();
                newStep.ProductionUnskilledLabour = GenerateRandomDecimal();
                newStep.ProductionWeeksPerYear = GenerateRandomDecimal();
                newStep.Rework = (decimal)RandomNumber(1, 10000) * 0.0001m;
                newStep.ScrapAmount = (decimal)RandomNumber(1, 10000) * 0.0001m;
                newStep.SetupEngineers = GenerateRandomDecimal();
                newStep.SetupForeman = GenerateRandomDecimal();
                newStep.SetupSkilledLabour = GenerateRandomDecimal();
                newStep.SetupsPerBatch = RandomNumber(1, int.MaxValue);
                newStep.SetupTechnicians = GenerateRandomDecimal();
                newStep.SetupTime = GenerateRandomDecimal();
                newStep.SetupTimeUnit = units.ElementAt(RandomNumber(1, units.Count - 1));
                newStep.SetupUnskilledLabour = GenerateRandomDecimal();
                newStep.ShiftCostExceedRatio = GenerateRandomDecimal();
                newStep.ShiftsPerWeek = GenerateRandomDecimal();
                newStep.Accuracy = (short)RandomNumber(1, short.MaxValue);
                newStep.CycleTime = GenerateRandomDecimal();
                newStep.PartsPerCycle = RandomNumber(1, int.MaxValue);
                newStep.HoursPerShift = GenerateRandomDecimal();
                newStep.BatchSize = RandomNumber(1, int.MaxValue);
                newStep.MaxDownTime = GenerateRandomDecimal();
                newStep.ExtraShiftsNumber = GenerateRandomDecimal();

                List<MeasurementUnit> unitss = SettingsManager.Instance.GetBaseMeasurementUnits(entityContext);

                newStep.CycleTimeUnit = units.ElementAt(RandomNumber(0, units.Count - 1));
                newStep.MaxDownTimeUnit = units.ElementAt(RandomNumber(0, units.Count - 1));

                process.Steps.Add(newStep);

                EntityManager.Instance.AddEntity(newStep, entityContext);

                // generate machines for this new step
                GenerateMachines(newStep, randomProjectStruct);

                // generate dies for this new step
                GenerateDies(newStep, randomProjectStruct);

                // generate consumable for the new step
                GenerateConsumables(newStep, randomProjectStruct);
            }
        }

        /// <summary>
        /// Generates the process steps.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="randomProjectStruct">The random project struct.</param>
        private void GenerateProcessSteps(object parent, RandomProjectStruct randomProjectStruct)
        {
            int nr = 0;

            if (parent is Part)
            {
                nr = RandomNumber(randomProjectStruct.NrMinProcesses, randomProjectStruct.NrMaxProcesses);
            }
            else
            {
                nr = RandomNumber(randomProjectStruct.NrMinProcesses1, randomProjectStruct.NrMaxProcesses1);
            }

            for (int i = 1; i <= nr; i++)
            {
                GenerateProcessStep(parent, randomProjectStruct);
            }
        }
            
        #endregion

        #region ButtonEvents
        /// <summary>
        /// Handles the Click event of the Generate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            RandomProjectStruct randomProjectStruct = new RandomProjectStruct();
            randomProjectStruct.NrMinProjects = Convert.ToInt32(minProjects.Content);
            randomProjectStruct.NrMaxProjects = Convert.ToInt32(maxProjects.Content);
            randomProjectStruct.NrMinAssemblies = Convert.ToInt32(minAssemblies.Content);
            randomProjectStruct.NrMaxAssemblies = Convert.ToInt32(maxAssemblies.Content);
            randomProjectStruct.NrMinParts = Convert.ToInt32(minParts.Content);
            randomProjectStruct.NrMaxParts = Convert.ToInt32(maxParts.Content);
            randomProjectStruct.NrMinProcesses = Convert.ToInt32(minProcessSteps1.Content);
            randomProjectStruct.NrMaxProcesses = Convert.ToInt32(maxProcessSteps1.Content);
            randomProjectStruct.NrMinProcesses1 = Convert.ToInt32(minProcessSteps2.Content);
            randomProjectStruct.NrMaxProcesses1 = Convert.ToInt32(maxProcessSteps2.Content);
            randomProjectStruct.NrMinMachines = Convert.ToInt32(minMachines.Content);
            randomProjectStruct.NrMaxMachines = Convert.ToInt32(maxMachines.Content);
            randomProjectStruct.NrMinMachines1 = Convert.ToInt32(minMachines2.Content);
            randomProjectStruct.NrMaxMachines1 = Convert.ToInt32(maxMachines2.Content);
            randomProjectStruct.NrMinDies = Convert.ToInt32(minDies.Content);
            randomProjectStruct.NrMaxDies = Convert.ToInt32(maxDies.Content);
            randomProjectStruct.NrMinConsumables = Convert.ToInt32(minConsumables.Content);
            randomProjectStruct.NrMaxConsumables = Convert.ToInt32(maxConsumables.Content);
            randomProjectStruct.NrMinConsumables1 = Convert.ToInt32(minConsumables2.Content);
            randomProjectStruct.NrMaxConsumables1 = Convert.ToInt32(maxConsumables2.Content);
            randomProjectStruct.NrMinRawMaterials = Convert.ToInt32(minRawMaterials.Content);
            randomProjectStruct.NrMaxRawMaterials = Convert.ToInt32(maxRawMaterials.Content);
            randomProjectStruct.NrMinCommodities = Convert.ToInt32(minCommodities.Content);
            randomProjectStruct.NrMaxCommodities = Convert.ToInt32(maxCommodities.Content);
            randomProjectStruct.DirectoryPath = directoryPath;

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Action a = delegate
                {
                    Generate.IsEnabled = false;
                };

                this.Dispatcher.Invoke(a, null);
              
                BackgroundWorker nworker = s as BackgroundWorker;
                GenerateProjects(randomProjectStruct, nworker);
            };

            worker.ProgressChanged += delegate(object s, ProgressChangedEventArgs args)
            {
                Action a = delegate
                {
                    progressBar.Progress = args.ProgressPercentage;
                };

                this.Dispatcher.Invoke(a, null);
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                progressBar.Visibility = Visibility.Hidden;

                Action a = delegate
                {
                    Generate.IsEnabled = true;
                };

                this.Dispatcher.Invoke(a, null);
            };

            worker.RunWorkerAsync();
            progressBar.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles the Click event of the SetMedia control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SetMedia_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            directoryPath = dialog.SelectedPath;
        }

        #endregion
              
        #region Struct
        /// <summary>
        /// Random project Structure 
        /// </summary>
        public struct RandomProjectStruct
        {
            /// <summary>
            /// Gets or sets the nr min projects.
            /// </summary>
            /// <value>The nr min projects.</value>
            public int NrMinProjects
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max projects.
            /// </summary>
            /// <value>The nr max projects.</value>
            public int NrMaxProjects
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min assemblies.
            /// </summary>
            /// <value>The nr min assemblies.</value>
            public int NrMinAssemblies
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max assemblies.
            /// </summary>
            /// <value>The nr max assemblies.</value>
            public int NrMaxAssemblies
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min parts.
            /// </summary>
            /// <value>The nr min parts.</value>
            public int NrMinParts
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max parts.
            /// </summary>
            /// <value>The nr max parts.</value>
            public int NrMaxParts
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min processes.
            /// </summary>
            /// <value>The nr min processes.</value>
            public int NrMinProcesses
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max processes.
            /// </summary>
            /// <value>The nr max processes.</value>
            public int NrMaxProcesses
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min machines.
            /// </summary>
            /// <value>The nr min machines.</value>
            public int NrMinMachines
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max machines.
            /// </summary>
            /// <value>The nr max machines.</value>
            public int NrMaxMachines
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min dies.
            /// </summary>
            /// <value>The nr min dies.</value>
            public int NrMinDies
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max dies.
            /// </summary>
            /// <value>The nr max dies.</value>
            public int NrMaxDies
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min consumables.
            /// </summary>
            /// <value>The nr min consumables.</value>
            public int NrMinConsumables
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max consumables.
            /// </summary>
            /// <value>The nr max consumables.</value>
            public int NrMaxConsumables
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max machines1.
            /// </summary>
            /// <value>The nr max machines1.</value>
            public int NrMaxMachines1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min machines1.
            /// </summary>
            /// <value>The nr min machines1.</value>
            public int NrMinMachines1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min commodities.
            /// </summary>
            /// <value>The nr min commodities.</value>
            public int NrMinCommodities
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max commodities.
            /// </summary>
            /// <value>The nr max commodities.</value>
            public int NrMaxCommodities
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min consumables.
            /// </summary>
            /// <value>The nr min consumables.</value>
            public int NrMinConsumables1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max consumables.
            /// </summary>
            /// <value>The nr max consumables.</value>
            public int NrMaxConsumables1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min processes1.
            /// </summary>
            /// <value>The nr min processes1.</value>
            public int NrMinProcesses1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max processes1.
            /// </summary>
            /// <value>The nr max processes1.</value>
            public int NrMaxProcesses1
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr min raw materials.
            /// </summary>
            /// <value>The nr min raw materials.</value>
            public int NrMinRawMaterials
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the nr max rawmaterials.
            /// </summary>
            /// <value>The nr max rawmaterials.</value>
            public int NrMaxRawMaterials
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the directory path.
            /// </summary>
            /// <value>The directory path.</value>
            public string DirectoryPath
            {
                get;
                set;
            }
        }
        #endregion
    }     
}
