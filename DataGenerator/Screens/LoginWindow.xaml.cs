﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using ZPKTool.Business;
using ZPKTool.Business.Managers;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Properties;
using ZPKTool.Gui.Utils;
using ZPKTool.Util;
using ZPKTool.Business.Synchronization;

namespace ZPKTool.Gui.Screens
{
    /// <summary>
    /// This class represents the Login Screen
    /// </summary>
    public partial class LoginWindow
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginWindow"/> class.
        /// </summary>
        public LoginWindow()
        {
            this.InitializeComponent();

            UserNameTextBox.Focus();

                             

#if DEBUG
            UserNameTextBox.Text = "admin";
            PasswordTextBox.Password = "admin";
#endif
        }

        /// <summary>
        /// Perform the login process.
        /// </summary>
        private void DoLogin()
        {
            string userName = UserNameTextBox.Text;
            string password =PasswordTextBox.Password;            

            // Check central server state
           
            bool centralServerIsOnline = Business.Utils.BusinessUtils.CheckCentralServerState();
          
            // Authenticate user.
            User user = SecurityManager.Instance.Login(userName, password);
                       
            ApplicationController.Instance.ShowMainWindow();

            this.Close();
              
            }
                 
        /// <summary>
        /// Event raised when the OK button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
          DoLogin();         
        }

        /// <summary>
        /// Event raised when a key is pressed on the password textbox
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void PasswordTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {   // if the pressed key is the Return key then try to log in
                DoLogin();
            }
        }

        /// <summary>
        /// Event raised when a key is pressed inside the username textbox
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void UserNameTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {   // if the pressed key is the Return key then try to log in
                DoLogin();
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the PasswordTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void PasswordTextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            this.PasswordTextBox.SelectAll();
        }
    }
}