﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace ZPKTool.Gui.Screens
{
    /// <summary>
    /// Defines a way to provide content for the tool area that is on the rght side of the main screen.
    /// </summary>
    public interface IToolProvider
    {
        /// <summary>
        /// Gets the control to be displayed the tool area of the main screen.
        /// Repeated calls return the same reference.
        /// </summary>
        /// <returns>The tool control.</returns>
        Control GetTool();
    }
}
