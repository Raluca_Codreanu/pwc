﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;
using ZPKTool.Gui;
using ZPKTool.Gui.Controls;
using ZPKTool.Data;
using ZPKTool.Business.Managers;
using ZPKTool.Business;
using ZPKTool.Util;

namespace ZPKTool.Gui.Screens
{
    /// <summary>
    /// Represents the main screen of the application
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// The margin of the main menu's top level items.
        /// </summary>
        private static readonly Thickness MainMenuTopLevelItemMargin = new Thickness(5, 0, 5, 0);

        /// <summary>
        /// Used to cover the projects tree when it must be disabled.
        /// </summary>
        private Grid mainTreeCover = new Grid();

        /// <summary>
        /// Used to cover the main content area when it must be disabled.
        /// </summary>
        private Grid mainContentCover = new Grid();

        #region Constructor & Related

        /// <summary>
        /// Prevents a default instance of the <see cref="MainWindow"/> class from being created.
        /// </summary>
        private MainWindow()
        {
            this.InitializeComponent();

            Brush backgroundCover = Application.Current.Resources["WaitForWorkerBackground"] as Brush;
            this.mainTreeCover.Background = backgroundCover;
            this.mainContentCover.Background = backgroundCover;
            Grid.SetColumnSpan(mainTreeCover, 100);
            Grid.SetRowSpan(mainTreeCover, 100);
            Grid.SetColumnSpan(mainContentCover, 100);
            Grid.SetRowSpan(mainContentCover, 100);

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="showWelcomeScreen">If true the main window will display the welcome screen, otherwise
        /// it will display the main screen.</param>
        public MainWindow(bool showWelcomeScreen)
            : this()
        {
            if (showWelcomeScreen)
            {
                ShowWelcomeScreen();
            }
            
        }      

        #endregion

        /// <summary>
        /// Display the welcome screen.
        /// </summary>
        public void ShowWelcomeScreen()
        {
            //RandomPopulateZPK
            RandomPopulateScreen randomPopulateScreen = new RandomPopulateScreen(this);
            randomPopulateScreen.VerticalAlignment = VerticalAlignment.Stretch;
            randomPopulateScreen.HorizontalAlignment = HorizontalAlignment.Stretch;

            LoadScreen(randomPopulateScreen);
        }                
         
        /// <summary>
        /// Loads a screen into the main window.
        /// </summary>
        /// <param name="screen">The screen to load.</param>
        private void LoadScreen(UIElement screen)
        {
            UnloadCurrentScreen();


            // Add the new content in the window
            this.WindowContent.Children.Add(screen);
        }

        /// <summary>
        /// Unloads the current screen from the Main Window.
        /// </summary>
        private void UnloadCurrentScreen()
        {
            if (this.WindowContent.Children.Count > 0)
            {
                // Trigger the OnBeforeUnload event of the current screen.
                object crtContent = this.WindowContent.Children[0];
                if (crtContent is IMainWindowScreen)
                {
                    ((IMainWindowScreen)crtContent).OnBeforeUnload();
                }

                // Remove the screen
                this.WindowContent.Children.Clear();
            }
        }
    
        #region Events

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Window.WindowState = WindowState.Maximized;
        }
               
        #endregion

      
    }
}