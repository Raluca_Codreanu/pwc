﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Screens
{
    /// <summary>
    /// Defines functionality common to all screens loaded into the main window.
    /// </summary>
    public interface IMainWindowScreen
    {
        /// <summary>
        /// Called before the screen is unloaded.
        /// </summary>
        void OnBeforeUnload();
    }
}